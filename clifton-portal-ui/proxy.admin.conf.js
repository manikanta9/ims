const ADMIN_URI = '/client/';
const PROXY_HOST = 'http://localhost:14200';
const PROXY_TARGET = 'https://localhost:2443';
// const PROXY_TARGET = 'https://portal-admin.qa-msp.paraport.com';
// const PROXY_TARGET = 'https://portal-admin.paraport.com';

module.exports = [
	// Fix ng serve not correctly serving assets from base href directory
	{
		context: [
			`${ADMIN_URI}assets/**`,
			`${ADMIN_URI}*.js`,
			`${ADMIN_URI}*.js.map`,
			`${ADMIN_URI}*.css`,
			`${ADMIN_URI}*.woff`,
			`${ADMIN_URI}*.woff2`,
			`${ADMIN_URI}*.tff`,
			`${ADMIN_URI}*.svg`,
			`${ADMIN_URI}*.eof`
		],
		target: PROXY_HOST,
		secure: false,
		logLevel: 'info',
		pathRewrite: {
			[`^${ADMIN_URI}`]: '/'
		}
	},
	// Proxy API requests to running Portal Admin
	{
		context: ['**/*.json', '/core/**'],
		target: PROXY_TARGET,
		changeOrigin: true,
		secure: false,
		logLevel: 'debug',
		autoRewrite: true,
		protocolRewrite: 'https',
		onProxyRes: function(proxyRes, req, res) {
			// Remove "Secure" flag from cookies
			const cookies = proxyRes.headers['set-cookie'];
			if (cookies) {
				const cookiesArr = Array.isArray(cookies) ? cookies : [cookies];
				proxyRes.headers['set-cookie'] = cookiesArr.map(cookie => cookie.replace(/;\s*?Secure/gi, ''));
			}
			// Disable HSTS
			delete proxyRes.headers['strict-transport-security'];
		}
	}
];
