const PROXY_TARGET = 'https://localhost:1443';
// const PROXY_TARGET = 'https://portal.qa-msp.paraport.com';
// const PROXY_TARGET = 'https://portal.parametricportfolio.com';

module.exports = [
	// Proxy API requests to running client portal
	{
		context: ['**/*.json', '**/login/**.js', '**/error/**.js'],
		target: PROXY_TARGET,
		changeOrigin: true,
		secure: false,
		logLevel: 'debug',
		autoRewrite: true,
		protocolRewrite: 'http',
		onProxyRes: function(proxyRes, req, res) {
			// Remove "Secure" flag from cookies
			const cookies = proxyRes.headers['set-cookie'];
			if (cookies) {
				const cookiesArr = Array.isArray(cookies) ? cookies : [cookies];
				proxyRes.headers['set-cookie'] = cookiesArr.map(cookie => cookie.replace(/;\s*?Secure/gi, ''));
			}
			// Disable HSTS
			delete proxyRes.headers['strict-transport-security'];
		}
	}
];
