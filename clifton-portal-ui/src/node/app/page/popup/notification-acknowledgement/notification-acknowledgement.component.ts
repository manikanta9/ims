import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { DialogComponent } from 'app/core/component/dialog/dialog.component';
import { MessageService } from 'app/core/component/text/message/message.service';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import * as router from 'app/core/state/reducers/router.actions';
import { UrlUtils } from 'app/core/util/url.utils';
import { PageModalInjectionToken } from 'app/page/page.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalNotification } from 'app/portal/notification/portal-notification';
import { PortalNotificationService } from 'app/portal/notification/portal-notification.service';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { AcknowledgeNotificationAction } from 'app/portal/state/reducers/notification-acknowledgements.actions';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-notification-acknowledgement',
	templateUrl: './notification-acknowledgement.component.html',
})
export class NotificationAcknowledgementComponent implements OnInit, OnDestroy {

	@ViewChild(DialogComponent) public dialog: DialogComponent;

	public errorMessage: string | null;
	public redirectUrl: string | null = environment.loginRedirectTarget;
	public loading = false;

	public submitEmitter$: EventEmitter<any> = new EventEmitter();
	public notificationAcknowledgement$: Observable<PortalNotification | undefined> = this.store.select(pageStore.getFirstNotificationAcknowledgement);

	private returnUrl = '/';

	public acknowledgeRequester: RequestLoader<void, { id: number, acknowledged: boolean }> = new RequestLoader({
		preModifier: obs$ => obs$.do(() => {
			this.errorMessage = null;
			this.startLoading();
		}),
		initiator: (portalNotification: { id: number, acknowledged: boolean }) => this.portalNotificationService.saveClientPortalNotificationAcknowledgement(portalNotification.id!, portalNotification.acknowledged)
			.do((response: { acknowledgement: boolean | undefined; portalNotificationId: number }) => {
				this.store.dispatch(new AcknowledgeNotificationAction(portalNotification));
				this.finishLoading();
			}),
		errorHandler: (error: PortalResponse) => this.handleError(error),
		subConfig: {comp: this, callback: () => null},

	});


	private queryParams$: Observable<ParamMap> = this.store.select(pageStore.getQueryParams);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalSecurityService: PortalSecurityService,
	            private messageService: MessageService,
	            private portalNotificationService: PortalNotificationService) {
		ManagedSubscription.of(this.queryParams$, (params: ParamMap) => this.updateReturnUrl(params)).attach(this);

	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.messageService.push(PageModalInjectionToken, true);
	}


	public ngOnDestroy(): void {
		this.messageService.push(PageModalInjectionToken, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startLoading(): void {
		this.loading = true;
		this.errorMessage = null;
	}


	private finishLoading(): void {
		this.loading = false;
	}


	private updateReturnUrl(params: ParamMap): void {
		this.returnUrl = decodeURIComponent(params.get('returnUrl') || '/');
	}


	private handleError(error: PortalResponse): void {
		if (error.errorMessage) {
			this.errorMessage = error.errorMessage;
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
	}


	public navigateToReturnUrl(): void {
		const returnUrlInfo = UrlUtils.getUrlInfo(this.returnUrl);
		this.store.dispatch(router.go(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
	}
}
