import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { StringUtils } from 'app/core/util/string.utils';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-forgot-password-request',
	templateUrl: './forgot-password-request.component.html',
})
export class ForgotPasswordRequestComponent implements OnInit, OnDestroy {

	public errorMessage: string | null;
	public loading = false;

	public onSubmit$: EventEmitter<any> = new EventEmitter<any>();

	private resetRequestSub: Subscription;
	private resetRequest$: Observable<PortalResponse> = this.onSubmit$
		.do(() => this.startLoading())
		.switchMap(() => this.resetPassword())
		.catch((error: PortalResponse, request$: Observable<PortalResponse>) => {
			this.handleError(error);
			return request$;
		})
		.do(() => this.finishLoading());


	public username = '';


	constructor(public activeModal: NgbActiveModal,
	            public portalSecurityService: PortalSecurityService,
	            private formBuilder: FormBuilder,
	            private modalDialogService: ModalDialogService) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public ngOnInit(): void {
		this.resetRequestSub = this.resetRequest$.subscribe((result: PortalResponse) => {
			if (result.success) {
				this.modalDialogService.showMessage('You should received an email shortly with a link to reset your password.', 'Password Reset')
					.result
					.catch(() => {}) // Swallow dismissals
					.then(() => this.activeModal.close());
			}
			else {
				this.handleError(result);
			}
		});
	}


	public ngOnDestroy(): void {
		this.resetRequestSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private resetPassword(): Observable<PortalResponse> {
		const usernameValue = this.getUsername();
		if (!StringUtils.isEmpty(usernameValue)) {
			return this.portalSecurityService.requestPasswordReset(usernameValue);
		}
		return Observable.of(new PortalResponse());
	}


	private getUsername(): string {
		const control = this.resetPasswordForm.get('username');
		return control && control.value ? control.value : this.username;
	}


	private startLoading(): void {
		this.loading = true;
		this.errorMessage = null;
	}


	private finishLoading(): void {
		this.loading = false;
	}


	private handleError(error: PortalResponse): void {
		const usernameValue = this.getUsername();
		if (!StringUtils.isEmpty(usernameValue) && (error.errorMessage || error.message)) {
			this.errorMessage = `No user was found for [${usernameValue}]`;
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
		this.loading = false;
	}


	public resetPasswordForm: FormGroup = this.formBuilder.group({
		username: [{value: '', disabled: false}, [Validators.required, Validators.email]],
	});
}
