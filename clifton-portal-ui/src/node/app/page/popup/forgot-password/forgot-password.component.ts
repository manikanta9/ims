import { Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ParamMap } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { PasswordValidation } from 'app/page/popup/change-password/change-password.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


interface ForgotChangeFormValue {
	newPassword: any;
	newPasswordConfirmation: any;
}


@Component({
	selector: 'ppa-forgot-password',
	templateUrl: './forgot-password.component.html',
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

	@Input() public errorMessage: string;
	private passwordResetKey: string;

	private static readonly VALIDATION_MESSAGES: { [fieldName in keyof ForgotChangeFormValue]: { [errorType: string]: string } } = {
		newPassword: {
			required: 'A new password is required.',
			minlength: 'Password must be at least 8 characters long.',
			maxlength: 'Password cannot be more than 30 characters long.',
		},
		newPasswordConfirmation: {required: 'Please confirm your new password.'},
	};

	public formErrors: { [fieldName in keyof ForgotChangeFormValue]: string } = {
		newPassword: '',
		newPasswordConfirmation: '',
	};


	public successEmitter$: EventEmitter<any> = new EventEmitter<any>();

	// Observers
	public loading$: Subject<boolean> = new BehaviorSubject(false);
	public forgotPasswordForm$: ReplaySubject<FormGroup> = new ReplaySubject(1);
	private queryParams$: Observable<ParamMap> = this.store.select(pageStore.getQueryParams);

	// Subscriptions
	private forgotPasswordFormSub: Subscription;
	private forgotPasswordFormChangeSub: Subscription;


	constructor(private store: Store<pageStore.PageStore>,
				private formBuilder: FormBuilder,
				public portalSecurityService: PortalSecurityService,
				private modalDialogService: ModalDialogService,
				private ngbActiveModal: NgbActiveModal) {
		ManagedSubscription.of(this.queryParams$, (params: ParamMap) => this.updatePasswordResetKey(params)).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public ngOnInit(): void {
		this.forgotPasswordForm$.next(this.formBuilder.group({
			newPassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
			newPasswordConfirmation: [null, Validators.required],
		} as ForgotChangeFormValue, {
			validator: PasswordValidation.MatchPassword, // your validation method
		}));

		this.forgotPasswordFormSub = this.forgotPasswordForm$.subscribe(forgotPasswordForm => {
			if (this.forgotPasswordFormChangeSub) {
				this.forgotPasswordFormChangeSub.unsubscribe();
			}
			this.forgotPasswordFormChangeSub = forgotPasswordForm.valueChanges.subscribe((formValue: PasswordValidation) => this.onValueChanged(formValue, forgotPasswordForm));
		});
	}


	public ngOnDestroy(): void {
		this.forgotPasswordFormSub.unsubscribe();
		if (this.forgotPasswordFormChangeSub) {
			this.forgotPasswordFormChangeSub.unsubscribe();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public attemptPasswordChange(form: FormGroup): void {
		this.startLoading();
		this.portalSecurityService.resetPassword(this.passwordResetKey, form.value.newPassword).first()
			.subscribe((response: PortalResponse) => {
				this.finishLoading();
				if (response.success) {
					this.modalDialogService.showMessage('Your password was successfully updated.', 'Password Update')
						.result
						.catch(() => {}) // Swallow dismissals
						.then(() => this.ngbActiveModal.close());
				}
				else {
					this.resetChangePassword(form);
					this.handleError(response);
				}
			});

	}


	public resetChangePassword(form: FormGroup): void {
		form.reset();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private handleError(error: PortalResponse): void {
		if (error.errorMessage) {
			this.errorMessage = error.errorMessage;
		}
		else if (error.message) {
			this.errorMessage = error.message;
		}
		else if (error.loginFailed) {
			this.errorMessage = 'Invalid login credentials';
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
	}


	private startLoading(): void {
		this.loading$.next(true);
	}


	private finishLoading(): void {
		this.loading$.next(false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private updatePasswordResetKey(params: ParamMap): void {
		this.passwordResetKey = decodeURIComponent(params.get('passwordResetKey') || '/');
	}


	private onValueChanged(formValue: PasswordValidation, userProfileForm: FormGroup) {
		const formErrors: any = this.formErrors;
		const validationMessages: { [fieldName: string]: { [errorType: string]: string } } = ForgotPasswordComponent.VALIDATION_MESSAGES;
		for (const field of Object.getOwnPropertyNames(this.formErrors)) {
			// Reset any existing error messages
			formErrors[field] = '';
			const control = userProfileForm.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = validationMessages[field];
				for (const key of Object.getOwnPropertyNames(control.errors)) {
					formErrors[field] += messages[key] + ' ';
				}
			}
		}
	}
}
