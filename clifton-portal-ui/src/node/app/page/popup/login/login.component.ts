import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { DialogComponent } from 'app/core/component/dialog/dialog.component';
import { MessageService } from 'app/core/component/text/message/message.service';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import * as router from 'app/core/state/reducers/router.actions';
import { UrlUtils } from 'app/core/util/url.utils';
import { PageModalInjectionToken } from 'app/page/page.component';
import { ForgotPasswordRequestComponent } from 'app/page/popup/forgot-password/forgot-password-request.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalAuthQuery } from 'app/portal/security/portal-auth.query';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { environment } from 'environments/environment';


@Component({
	selector: 'ppa-login',
	templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit, OnDestroy {

	@ViewChild(DialogComponent) public dialog: DialogComponent;
	public loginForm: FormGroup = this.formBuilder.group({
		j_username: ['', Validators.required],
		j_password: ['', Validators.required],
		redirect: [environment.loginRedirectTarget],
		security_remember_me: [void 0],
	});

	public loginRequester: RequestLoader<PortalSecurityUser, PortalAuthQuery> = new RequestLoader({
		preModifier: obs$ => obs$.do(() => this.errorMessage = null),
		initiator: loginQuery => this.portalSecurityService.login(loginQuery)
			.do((response: PortalResponse) => {
				if (!response.success) {
					throw response;
				}
			})
			.mergeMap((response: PortalResponse) => this.portalSecurityService.getCurrentUser()),
		errorHandler: (error: PortalResponse) => this.handleError(error),
		subConfig: {comp: this, callback: (user: PortalSecurityUser) => this.handleAuthenticationResult(user)},
	});

	public titleElement$ = this.store.select(pageStore.getPageElementSingle('login.title'));

	public errorMessage: Nullable<string>;
	private returnUrl = '/';


	constructor(private formBuilder: FormBuilder,
	            private messageService: MessageService,
	            private store: Store<pageStore.PageStore>,
	            private portalSecurityService: PortalSecurityService,
	            private modalDialogService: ModalDialogService) {
		ManagedSubscription.of(this.store.select(pageStore.getQueryParams), (params: ParamMap) => this.updateReturnUrl(params)).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.messageService.push(PageModalInjectionToken, true);
	}


	public ngOnDestroy(): void {
		this.messageService.push(PageModalInjectionToken, false);
	}


	public onForgotPassword(): void {
		this.dialog.hide();
		const dialogRef = this.modalDialogService.showComponent(ForgotPasswordRequestComponent, {backdrop: 'static'});
		dialogRef.result
			.catch(() => {}) // Swallow dismissals
			.then(() => this.dialog.show());

		const control = this.loginForm.get('j_username');
		if (control && control.value) {
			dialogRef.componentInstance.username = control.value;
		}

	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private updateReturnUrl(params: ParamMap): void {
		this.returnUrl = decodeURIComponent(params.get('returnUrl') || '/');
	}


	private handleError(error: PortalResponse): void {
		if (error.errorMessage) {
			this.errorMessage = error.errorMessage;
		}
		else if (error.loginFailed) {
			this.errorMessage = 'Invalid login credentials';
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
	}


	private handleAuthenticationResult(loginUser: PortalSecurityUser): void {
		this.dialog.hide();
		this.navigateToReturnUrl();
	}


	private navigateToReturnUrl(): void {
		const returnUrlInfo = UrlUtils.getUrlInfo(this.returnUrl);
		this.store.dispatch(router.replace(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
	}
}
