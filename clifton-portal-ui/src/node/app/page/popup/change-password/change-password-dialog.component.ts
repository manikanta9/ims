import { Component, OnDestroy, OnInit } from '@angular/core';
import { ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import * as router from 'app/core/state/reducers/router.actions';
import { StringUtils } from 'app/core/util/string.utils';
import { UrlUtils } from 'app/core/util/url.utils';
import { ChangePasswordComponent } from 'app/page/popup/change-password/change-password.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-change-password-dialog',
	templateUrl: './change-password-dialog.component.html',
})
export class ChangePasswordDialogComponent implements OnInit, OnDestroy {

	private returnUrl: string;

	private queryParams$: Observable<ParamMap> = this.store.select(pageStore.getQueryParams);


	constructor(private modalDialogService: ModalDialogService, private store: Store<pageStore.PageStore>) {
		const dialogRef = this.modalDialogService.showComponent(ChangePasswordComponent, {backdrop: 'static', windowClass: 'modal-password-window', keyboard: false});
		dialogRef.componentInstance.modal = true;
		ManagedSubscription.of(this.queryParams$, (params: ParamMap) => this.updateReturnUrl(params)).attach(this);
		ManagedSubscription.of(dialogRef.componentInstance.successEmitter$, () => {
			dialogRef.dismiss();
			this.navigateToReturnUrl();
		}).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {}


	public ngOnDestroy(): void {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private updateReturnUrl(params: ParamMap): void {
		this.returnUrl = decodeURIComponent(params.get('returnUrl') || '/');
	}


	private navigateToReturnUrl(): void {
		if (!StringUtils.isEmpty(this.returnUrl) && '/' !== this.returnUrl) {
			const returnUrlInfo = UrlUtils.getUrlInfo(this.returnUrl);
			this.store.dispatch(router.replace(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
		}
	}
}
