import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { FormUtils } from 'app/core/util/form.utils';
import { StringUtils } from 'app/core/util/string.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { PageElementInfo } from 'app/portal/state/reducers/page-element.reducer';
import * as user from 'app/portal/state/reducers/user.actions';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


interface PasswordChangeFormValue {
	id: any;
	username: any;
	currentPassword: any;
	newPassword: any;
	newPasswordConfirmation: any;
}


export class PasswordValidation {

	public static MatchPassword(AC: AbstractControl) {
		const newPasswordField = AC.get('newPassword');
		const newPasswordConfirmationField = AC.get('newPasswordConfirmation');
		const password = newPasswordField != null ? newPasswordField.value : null; // to get value in input tag
		const confirmPassword = newPasswordConfirmationField != null ? newPasswordConfirmationField.value : null; // to get value in input tag
		if (newPasswordConfirmationField != null) {
			if (password !== confirmPassword) {
				newPasswordConfirmationField.setErrors({MatchPassword: true});
			}
			else if (!newPasswordConfirmationField.pristine) {
				newPasswordConfirmationField.setErrors(null);
			}
		}
	}
}


// tslint:disable-next-line:max-classes-per-file : Temporary resolution for multiple classes in this file
@Component({
	selector: 'ppa-change-password',
	templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit, OnDestroy {

	public modal = false;
	public errorMessage: Nullable<string>;

	private static readonly VALIDATION_MESSAGES: { [fieldName in keyof PasswordChangeFormValue]: { [errorType: string]: string } } = {
		id: {required: 'The user ID is required.'},
		username: {required: 'The user ID is required.'},
		currentPassword: {required: 'Your current password is required.'},
		newPassword: {
			required: 'A new password is required.',
			minlength: 'Password must be at least 8 characters long.',
			maxlength: 'Password cannot be more than 30 characters long.',
		},
		newPasswordConfirmation: {required: 'Please confirm your new password.'},
	};
	public userProfile: PortalSecurityUser;
	public formErrors: { [fieldName in keyof PasswordChangeFormValue]: string } = {
		id: '',
		username: '',
		currentPassword: '',
		newPassword: '',
		newPasswordConfirmation: '',
	};


	public successEmitter$: EventEmitter<any> = new EventEmitter<any>();

	// Observers
	public loading$: Subject<boolean> = new BehaviorSubject(false);
	public changePasswordForm$: Subject<FormGroup> = new Subject();
	public emailAddressEls$ = this.store.select(pageStore.getPageElementMulti('profile.change-request.email.address'));
	public portalNameEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));
	private userProfileRequestEmitter$: Subject<any> = new Subject();


	// Observables
	private userProfileRequest$: Observable<PortalSecurityUser> = this.userProfileRequestEmitter$
		.do(() => this.startLoading())
		.switchMap(() => this.portalSecurityService.getCurrentUser())
		.do((currentUser: PortalSecurityUser) => this.store.dispatch(new user.RefreshUserAction(currentUser)))
		.do(() => this.finishLoading());
	public currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);


	// Subscriptions
	private userProfileSub: Subscription;
	private changePasswordFormSub: Subscription;
	private changePasswordFormChangeSub: Subscription;


	constructor(private store: Store<pageStore.PageStore>,
	            private formBuilder: FormBuilder,
	            private portalSecurityService: PortalSecurityService,
	            private modalDialogService: ModalDialogService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public ngOnInit(): void {
		this.userProfileSub = this.userProfileRequest$.subscribe((currentUser: PortalSecurityUser) => {
			this.userProfile = currentUser;
			this.changePasswordForm$.next(this.formBuilder.group({
				id: [currentUser.id, Validators.required],
				username: [{value: currentUser.username, disabled: true}, Validators.required],
				currentPassword: ['', Validators.required],
				newPassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
				newPasswordConfirmation: [null, Validators.required],
			} as PasswordChangeFormValue, {
				validator: PasswordValidation.MatchPassword, // your validation method
			}));
		});
		this.changePasswordFormSub = this.changePasswordForm$.subscribe(changePasswordForm => {
			if (this.changePasswordFormChangeSub) {
				this.changePasswordFormChangeSub.unsubscribe();
			}
			this.changePasswordFormChangeSub = changePasswordForm.valueChanges.subscribe((formValue: PasswordChangeFormValue) => this.onValueChanged(formValue, changePasswordForm));
		});
		this.userProfileRequestEmitter$.next();
	}


	public ngOnDestroy(): void {
		this.userProfileSub.unsubscribe();
		this.changePasswordFormSub.unsubscribe();
		if (this.changePasswordFormChangeSub) {
			this.changePasswordFormChangeSub.unsubscribe();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public attemptPasswordChange(form: FormGroup): void {
		this.startLoading();
		this.portalSecurityService.updateUserPassword(form.value.currentPassword, form.value.newPassword)
			.subscribe((response: PortalResponse) => {
				if (response.success) {
					this.userProfileRequestEmitter$.next();
					const dialogRef = this.modalDialogService.showMessage('Your password was successfully updated.', 'Password Update');
					dialogRef.result.then(() => this.successEmitter$.emit());
				}
				else {
					this.handleError(response);
					this.finishLoading();
				}
			});
	}


	public resetChangePassword(form: FormGroup): void {
		form.reset(this.userProfile);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public generateEmailLinkHtml(portalName: string, emailAddressEls: PageElementInfo[], currentUser?: PortalSecurityUser): string {
		const emailLinks = emailAddressEls
			.map(emailAddressEl => FormUtils.generateEmailLink({
				mailto: emailAddressEl.value,
				text: emailAddressEl.value,
				subject: `${portalName} Forgotten Password / ${currentUser && currentUser.label || 'Unauthenticated User'}`,
			}))
			.map(linkEl => linkEl.outerHTML);
		return StringUtils.concatWithConjunction(emailLinks, 'or');
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startLoading(): void {
		this.loading$.next(true);
	}


	private finishLoading(): void {
		this.loading$.next(false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private handleError(error: PortalResponse): void {
		if (error.errorMessage) {
			this.errorMessage = error.errorMessage;
		}
		else if (error.message) {
			this.errorMessage = error.message;
		}
		else if (error.loginFailed) {
			this.errorMessage = 'Invalid login credentials';
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
	}


	private onValueChanged(formValue: PasswordChangeFormValue, userProfileForm: FormGroup) {
		const formErrors: any = this.formErrors;
		const validationMessages: { [fieldName: string]: { [errorType: string]: string } } = ChangePasswordComponent.VALIDATION_MESSAGES;
		for (const field of Object.getOwnPropertyNames(this.formErrors)) {
			// Reset any existing error messages
			formErrors[field] = '';
			const control = userProfileForm.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = validationMessages[field];
				for (const key of Object.getOwnPropertyNames(control.errors)) {
					formErrors[field] += messages[key] + ' ';
				}
			}
		}
	}
}
