import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.actions';
import * as pageStore from 'app/page/shared/state/page.store';


@Component({
	selector: 'ppa-terms-of-use-review',
	templateUrl: './terms-of-use-review.component.html',
})
export class TermsOfUseReviewComponent {

	constructor(private store: Store<pageStore.PageStore>) {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public onClose() {
		this.store.dispatch(router.go('/'));
	}
}
