import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { DialogComponent } from 'app/core/component/dialog/dialog.component';
import { MessageService } from 'app/core/component/text/message/message.service';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import * as router from 'app/core/state/reducers/router.actions';
import { UrlUtils } from 'app/core/util/url.utils';
import { PageModalInjectionToken } from 'app/page/page.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import * as user from 'app/portal/state/reducers/user.reducer';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-terms-of-use',
	templateUrl: './terms-of-use.component.html',
})
export class TermsOfUseComponent implements OnInit, OnDestroy {

	@ViewChild(DialogComponent) public dialog: DialogComponent;

	public errorMessage: string | null;
	public redirectUrl: string | null = environment.loginRedirectTarget;
	public loading = false;

	public submitEmitter$: EventEmitter<any> = new EventEmitter();
	public portalNameEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));

	private returnUrl = '/';
	private acceptRequest$: Observable<PortalSecurityUser> = this.submitEmitter$
		.do(() => this.startLoading())
		.switchMap(() => this.submitAcceptance())
		.mergeMap((acceptanceResponse: PortalResponse) => this.getCurrentUser())
		.do((currentUser: PortalSecurityUser) => this.store.dispatch(new user.LoginAction(currentUser)))
		.catch((error: PortalResponse, request$: Observable<PortalSecurityUser>) => this.handleError(error, request$))
		.do(() => this.finishLoading());
	private queryParams$: Observable<ParamMap> = this.store.select(pageStore.getQueryParams);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalSecurityService: PortalSecurityService,
	            private messageService: MessageService,
	            private modalDialogService: ModalDialogService) {
		ManagedSubscription.of(this.queryParams$, (params: ParamMap) => this.updateReturnUrl(params)).attach(this);
		ManagedSubscription.of(this.acceptRequest$, (loginUser: PortalSecurityUser) => this.handleAuthenticationResult(loginUser)).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.messageService.push(PageModalInjectionToken, true);
	}


	public ngOnDestroy(): void {
		this.messageService.push(PageModalInjectionToken, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public onCancel(portalName: string): void {
		this.modalDialogService.showWarning(`In order to access the ${portalName} please accept the Terms of Use.`);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private startLoading(): void {
		this.loading = true;
		this.errorMessage = null;
	}


	private finishLoading(): void {
		this.loading = false;
	}


	private updateReturnUrl(params: ParamMap): void {
		this.returnUrl = decodeURIComponent(params.get('returnUrl') || '/');
	}


	public submitAcceptance(): Observable<PortalResponse> {
		return this.portalSecurityService.acceptTermsOfUse();
	}


	private handleError(error: PortalResponse, request$: Observable<PortalSecurityUser>): Observable<PortalSecurityUser> {
		if (error.errorMessage) {
			this.errorMessage = error.errorMessage;
		}
		else if (error.termsOfUseAcceptanceRequired) {
			this.errorMessage = 'The Terms of Use must be accepted to use the application';
		}
		else {
			this.errorMessage = 'An unspecified error occurred';
		}
		this.loading = false;
		// Return the original observable so that subscribers are not terminated by the exception. This will cause the current item in the observable stream to be ignored.
		return request$;
	}


	private getCurrentUser(): Observable<PortalSecurityUser> {
		return this.portalSecurityService.getCurrentUser();
	}


	private handleAuthenticationResult(currentUser?: PortalSecurityUser): void {
		if (currentUser != null && !currentUser.termsOfUseAcceptanceRequired) {
			this.dialog.hide();
			this.navigateToReturnUrl();
		}
	}


	private navigateToReturnUrl(): void {
		const returnUrlInfo = UrlUtils.getUrlInfo(this.returnUrl);
		this.store.dispatch(router.replace(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
	}
}
