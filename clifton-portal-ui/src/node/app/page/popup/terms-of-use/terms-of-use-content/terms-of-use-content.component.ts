import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';


@Component({
	selector: 'ppa-terms-of-use-content',
	templateUrl: './terms-of-use-content.component.html',
})
export class TermsOfUseContentComponent {

	public termsOfUseEl$ = this.store.select(pageStore.getPageElementSingle('portal.terms-of-use'));


	constructor(private store: Store<pageStore.PageStore>) {}
}
