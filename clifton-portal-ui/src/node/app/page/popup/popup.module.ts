import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'app/core/core.module';
import { ChangePasswordDialogComponent } from 'app/page/popup/change-password/change-password-dialog.component';
import { ChangePasswordComponent } from 'app/page/popup/change-password/change-password.component';
import { ForgotPasswordDialogComponent } from 'app/page/popup/forgot-password/forgot-password-dialog.component';
import { ForgotPasswordRequestComponent } from 'app/page/popup/forgot-password/forgot-password-request.component';
import { ForgotPasswordComponent } from 'app/page/popup/forgot-password/forgot-password.component';
import { LoginComponent } from 'app/page/popup/login/login.component';
import { NotificationAcknowledgementComponent } from 'app/page/popup/notification-acknowledgement/notification-acknowledgement.component';
import { TermsOfUseContentComponent } from 'app/page/popup/terms-of-use/terms-of-use-content/terms-of-use-content.component';
import { TermsOfUseReviewComponent } from 'app/page/popup/terms-of-use/terms-of-use-review.component';
import { TermsOfUseComponent } from 'app/page/popup/terms-of-use/terms-of-use.component';
import { PopoverModule } from 'ngx-bootstrap/popover';


const INTERNAL_DIRECTIVES: Type<any>[] = [
	ChangePasswordComponent,
	ChangePasswordDialogComponent,
	ForgotPasswordComponent,
	ForgotPasswordDialogComponent,
	ForgotPasswordRequestComponent,
];
const EXPORTED_DIRECTIVES: Type<any>[] = [
	LoginComponent,
	TermsOfUseComponent,
	TermsOfUseContentComponent,
	TermsOfUseReviewComponent,
	NotificationAcknowledgementComponent,
];


@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		FormsModule,
		ReactiveFormsModule,
		PopoverModule,
	],
	declarations: [
		...INTERNAL_DIRECTIVES,
		...EXPORTED_DIRECTIVES,
	],
	exports: [
		...EXPORTED_DIRECTIVES,
	],
	entryComponents: [
		ChangePasswordComponent,
		ForgotPasswordComponent,
		TermsOfUseContentComponent,
		ForgotPasswordRequestComponent,
		NotificationAcknowledgementComponent,
	],
})
export class PopupModule {
}
