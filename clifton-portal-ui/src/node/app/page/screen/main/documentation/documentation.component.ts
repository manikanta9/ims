import { Component, OnDestroy, OnInit } from '@angular/core';
import { Column } from 'app/core/component/table/column';
import { DateUtils } from 'app/core/util/date.utils';
import { PortalFileListComponent } from 'app/page/shared/component/portal-file-list/portal-file-list.component';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterValue } from 'app/page/shared/filter/config/filter-value';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-documentation',
	templateUrl: './documentation.component.html',
})
export class DocumentationComponent implements OnInit, OnDestroy {

	private static readonly CATEGORY_NAME = 'Documentation';

	public columnList: Column[] = [
		{
			header: '',
			image: (report: PortalFile) => PortalFileListComponent.getFileTypeIcon(report.documentFileType!),
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity != null ? report.assignedPortalEntity.id : void 0),
			imageTitle: 'documentFileType',
		},
		{
			header: 'File',
			property: 'displayName',
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity != null ? report.assignedPortalEntity.id : void 0),
			style: {'white-space': 'normal'},
		},
		{header: 'Document Type', property: 'fileCategory.label', style: {'white-space': 'normal'}},
		{header: 'Organization', property: 'assignedPortalEntity.parentPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Investing Entity', property: 'assignedPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Date', property: 'reportDateFormatted'},
	];
	public query: PortalFileQuery | null = null;
	public useDetailView$: Observable<boolean> = this.filterToolbarService.getFilterMode()
		.map((filterMode: FilterMode) => filterMode === FilterMode.DETAIL);

	private filterValueSub: Subscription;


	constructor(private portalFileDownloadService: PortalFileDownloadService,
	            private filterToolbarService: FilterToolbarService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.filterToolbarService.enableFilter({
			modeControlEnabled: true,
			group: FilterGroup.STANDARD,
			filters: {
				relationship: {autoSelect: true, required: true, allowAll: true},
				entity: {autoSelect: true, required: true, allowAll: true},
				category: {categoryName: DocumentationComponent.CATEGORY_NAME, allowAll: true, filterMode: FilterMode.DETAIL},
				startDate: {defaultDate: null, filterMode: FilterMode.DETAIL},
				endDate: {defaultDate: DateUtils.now(), filterMode: FilterMode.DETAIL},
			},
		});
		this.filterValueSub = this.filterToolbarService.getFilterValues()
			.subscribe((value: FilterValue) => this.query = this.filterToolbarService.convertToPortalFileQuery(value, DocumentationComponent.CATEGORY_NAME));
	}


	public ngOnDestroy(): void {
		this.filterToolbarService.disableFilter();
		this.filterValueSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// TODO: Move this to either column spec or portal-file-list if possible?
	public downloadFile(id: number, assignedPortalEntityId?: number): void {
		this.portalFileDownloadService.downloadFile({portalFileId: id, assignedPortalEntityId});
	}
}
