import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalPaginatedQuery } from 'app/portal/api/portal-paginated-query';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-overview',
	templateUrl: './overview.component.html',
})
export class OverviewComponent implements OnInit, OnDestroy {

	public recentlyPublishedLoading = false;
	public relationshipManagerLoading = false;
	public getFieldValue = this.portalEntityService.getFieldValue;

	// Observers
	public hasContactsAccessGroup$: Observable<boolean> = this.store.select(pageStore.isSimulatingUser)
		.switchMap(simulatingUser => this.store.select(simulatingUser
			? pageStore.getSimulatedUserHasAccessGroup('Contact Us')
			: pageStore.hasAccessGroup('Contact Us')));
	private recentlyPublishedRequestEmitter$: Subject<any> = new Subject();
	public recentlyPublishedList$: Subject<PortalFile[]> = new ReplaySubject<PortalFile[]>(1);
	private relationshipManagerRequestEmitter$: Subject<any> = new Subject();
	public relationshipManagerList$: Subject<PortalEntity[]> = new ReplaySubject<PortalEntity[]>(1);

	// Observables
	private recentlyPublishedRequest$: Observable<PortalFile[]> = this.recentlyPublishedRequestEmitter$
		.do(() => this.startRecentlyPublishedLoading())
		.switchMap(() => this.getRecentlyPublishedList())
		.do(() => this.finishRecentlyPublishedLoading());
	private relationshipManagerRequest$: Observable<PortalFile[]> = this.relationshipManagerRequestEmitter$
		.do(() => this.startRelationshipManagerLoading())
		.switchMap(() => this.getRelationshipManagerList())
		.do(() => this.finishRelationshipManagerLoading());

	// Subscriptions
	private recentlyPublishedSub: Subscription;
	private relationshipManagerSub: Subscription;


	constructor(private store: Store<pageStore.PageStore>,
	            private portalFileService: PortalFileService,
	            private portalEntityService: PortalEntityService,
	            private portalFileDownloadService: PortalFileDownloadService) {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.recentlyPublishedSub = this.recentlyPublishedRequest$.subscribe((fileList: PortalFile[]) => this.recentlyPublishedList$.next(fileList));
		this.relationshipManagerSub = this.relationshipManagerRequest$.subscribe((rmList: PortalEntity[]) => this.relationshipManagerList$.next(rmList));
		this.recentlyPublishedRequestEmitter$.next();
		this.relationshipManagerRequestEmitter$.next();
	}


	public ngOnDestroy(): void {
		this.recentlyPublishedSub.unsubscribe();
		this.relationshipManagerSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public downloadFile(portalFileId: number, assignedPortalEntityId?: number): void {
		this.portalFileDownloadService.downloadFile({portalFileId, assignedPortalEntityId});
	}


	public formatFileDisplayName(file: PortalFile): string {
		// when the name template is not defined, do not add file category (redundant to name)
		const displayName = file.displayName || '';
		return file.fileCategory!.displayNameTemplate
			? `${displayName} - ${file.fileCategory!.name}`
			: displayName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private getRecentlyPublishedList(): Observable<PortalFile[]> {
		return this.portalFileService.get2({
			defaultDisplay: true,
			fileNameAndPathPopulated: true,
			excludeCategoryNames: ['Help', 'Contact Us'],
			orderBy: [{field: 'publishDate', direction: 'DESC'}],
			requestedProperties: ['portalFileId', 'publishDate', 'assignedPortalEntity.id', 'assignedPortalEntity.label', 'displayName', 'fileCategory.name', 'fileCategory.displayNameTemplate'],
			// TODO: Why do we have to have this here? Review rules for how response will be structured based off of parameters, or switch to Jackson
			requestedPropertiesRoot: 'data',
			limit: 6,
		} as PortalPaginatedQuery<PortalFileQuery>);
	}


	private getRelationshipManagerList(): Observable<PortalEntity[]> {
		return this.portalFileService.getFileSourceEntityList({
			fileGroupName: 'Relationship Manager',
			requestedProperties: ['label', 'fieldValueList.portalEntityFieldType.name', 'fieldValueList.value'],
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startRecentlyPublishedLoading(): void {
		this.recentlyPublishedLoading = true;
	}


	private finishRecentlyPublishedLoading(): void {
		this.recentlyPublishedLoading = false;
	}


	private startRelationshipManagerLoading(): void {
		this.relationshipManagerLoading = true;
	}


	private finishRelationshipManagerLoading(): void {
		this.relationshipManagerLoading = false;
	}
}
