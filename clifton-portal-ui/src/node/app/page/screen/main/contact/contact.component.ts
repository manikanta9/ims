import { Component, OnDestroy, OnInit } from '@angular/core';
import { SnakeCasePipe } from 'app/core/component/text/snake-case.pipe';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { ApiUtils } from 'app/portal/util/api.utils';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-contact',
	templateUrl: './contact.component.html',
})
export class ContactComponent implements OnInit, OnDestroy {

	public loading = false;
	public getFieldValue = this.portalEntityService.getFieldValue;

	// Observers
	private rmRequestEmitter$: Subject<any> = new Subject();
	public rmList$: Subject<PortalEntity[]> = new ReplaySubject<PortalEntity[]>(1);

	// Observables
	private rmRequest$: Observable<PortalEntity[]> = this.rmRequestEmitter$
		.do(() => this.startLoading())
		.switchMap(() => this.loadRmList())
		.do(() => this.finishLoading());

	// Subscriptions
	private rmListSub: Subscription;


	constructor(private portalFileService: PortalFileService,
	            private portalEntityService: PortalEntityService,
	            private pipe: SnakeCasePipe) {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.rmListSub = this.rmRequest$.subscribe((rmList: PortalEntity[]) => this.rmList$.next(rmList));
		this.rmRequestEmitter$.next();
	}


	public ngOnDestroy(): void {
		this.rmListSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startLoading(): void {
		this.loading = true;
	}


	private finishLoading(): void {
		this.loading = false;
	}


	private loadRmList(): Observable<PortalEntity[]> {
		return this.portalFileService.getFileSourceEntityList({
			fileGroupName: 'Relationship Manager',
			requestedProperties: ['label', 'fieldValueList.portalEntityFieldType.name', 'fieldValueList.value'],
		});
	}


	public getPortraitURL(relationshipManager: PortalEntity): string {
		const portalFileId = this.getFieldValue(relationshipManager, 'PortalFileId');
		if (portalFileId) {
			return ApiUtils.getAsApiUriWithPrefix('PortraitFile.json?portalFileId=' + this.getFieldValue(relationshipManager, 'PortalFileId'));
		}
		else {
			return 'assets/images/parametric-square.png';
		}
	}
}
