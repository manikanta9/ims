import { Component, OnDestroy, OnInit } from '@angular/core';
import { Column } from 'app/core/component/table/column';
import { DateUtils } from 'app/core/util/date.utils';
import { PortalFileListComponent } from 'app/page/shared/component/portal-file-list/portal-file-list.component';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterValue } from 'app/page/shared/filter/config/filter-value';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-invoices',
	templateUrl: './invoices.component.html',
})
export class InvoicesComponent implements OnInit, OnDestroy {

	private static readonly CATEGORY_NAME = 'Invoices';
	private static readonly FIELD_AMOUNT_DUE_PATTERN: RegExp = / Amount Due: (.*)/;
	private static readonly FIELD_INVOICE_NUMBER_PATTERN: RegExp = /(?:Invoice #: | #)([^ ]*)/;

	public columnList: Column[] = [
		{
			header: '',
			image: (report: PortalFile) => PortalFileListComponent.getFileTypeIcon(report.documentFileType!),
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity != null ? report.assignedPortalEntity.id : void 0),
			imageTitle: 'documentFileType',
		},
		{
			header: 'Invoice #',
			property: InvoicesComponent.formatInvoiceNumberField,
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity ? report.assignedPortalEntity.id : void 0),
		},
		{header: 'Document Type', property: 'fileCategory.label', style: {'white-space': 'normal'}},
		{header: 'Organization', property: 'assignedPortalEntity.parentPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Investing Entity', property: 'assignedPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Status', property: 'displayText'},
		{header: 'Amount', property: InvoicesComponent.formatAmountField},
		{header: 'Date', property: 'reportDateFormatted'},
	];
	public query: PortalFileQuery | null = null;
	public useDetailView$: Observable<boolean> = this.filterToolbarService.getFilterMode()
		.map((filterMode: FilterMode) => filterMode === FilterMode.DETAIL);

	private filterValueSub: Subscription;


	constructor(private portalFileDownloadService: PortalFileDownloadService,
	            private filterToolbarService: FilterToolbarService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.filterToolbarService.enableFilter({
			modeControlEnabled: true,
			group: FilterGroup.STANDARD,
			filters: {
				relationship: {autoSelect: true, required: true, allowAll: true},
				entity: {autoSelect: true, required: true, allowAll: true},
				category: {categoryName: InvoicesComponent.CATEGORY_NAME, allowAll: true, filterMode: FilterMode.DETAIL},
				startDate: {defaultDate: DateUtils.getRelativeDate(-1, 'years'), filterMode: FilterMode.DETAIL},
				endDate: {defaultDate: DateUtils.now(), filterMode: FilterMode.DETAIL},
			},
		});
		this.filterValueSub = this.filterToolbarService.getFilterValues()
			.subscribe((value: FilterValue) => this.query = this.filterToolbarService.convertToPortalFileQuery(value, InvoicesComponent.CATEGORY_NAME));
	}


	public ngOnDestroy(): void {
		this.filterToolbarService.disableFilter();
		this.filterValueSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// TODO: Move this to either column spec or portal-file-list if possible?
	public downloadFile(id: number, assignedPortalEntityId?: number): void {
		this.portalFileDownloadService.downloadFile({portalFileId: id, assignedPortalEntityId});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static formatAmountField(file: PortalFile): string {
		// Parse out the amount due from display name
		const matchResult: RegExpExecArray | null = InvoicesComponent.FIELD_AMOUNT_DUE_PATTERN.exec(file.displayName || '');
		return (matchResult && matchResult[1]) || '';
	}


	private static formatInvoiceNumberField(file: PortalFile): string {
		// Parse out the invoice number from the display name
		const matchResult: RegExpExecArray | null = InvoicesComponent.FIELD_INVOICE_NUMBER_PATTERN.exec(file.displayName || '');
		return (matchResult && matchResult[1]) || 'N/A';
	}
}

