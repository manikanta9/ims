import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Column } from 'app/core/component/table/column';
import { PortalFileListComponent } from 'app/page/shared/component/portal-file-list/portal-file-list.component';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import * as portalStore from 'app/portal/state/portal.store';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-help',
	templateUrl: './help.component.html',
})
export class HelpComponent implements OnInit, OnDestroy {

	public columnList: Column[] = [
		{
			header: '',
			image: (report: PortalFile) => PortalFileListComponent.getFileTypeIcon(report.documentFileType!),
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity ? report.assignedPortalEntity.id : void 0),
			imageTitle: 'documentFileType',
		},
		{
			header: 'File',
			property: 'displayName',
			click: (report: PortalFile) => this.downloadFile(report.portalFileId!, report.assignedPortalEntity ? report.assignedPortalEntity.id : void 0),
			style: {'white-space': 'normal'},
		},
		{header: 'Document Type', property: 'fileCategory.label', style: {'white-space': 'normal'}},
		{header: 'Organization', property: 'assignedPortalEntity.parentPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Investing Entity', property: 'assignedPortalEntity.label', maxLength: 120, style: {'white-space': 'normal'}},
		{header: 'Date', property: 'reportDateFormatted'},
	];
	private isAdmin$: Observable<boolean | undefined> = this.store.select(portalStore.isSimulatingUser)
		.switchMap(simulatingUser => this.store.select(simulatingUser
			? portalStore.getSimulatedUserIsAdmin
			: portalStore.isAdmin));
	public query$: Observable<PortalFileQuery> = this.isAdmin$
		.map(isAdmin => ({
			categoryName: 'Help',
			defaultDisplay: true,
			...isAdmin ? {} : {excludeCategorySubsetName: 'Help - Portal Administrators'},
		}));


	constructor(private store: Store<portalStore.PortalStore>,
	            private portalFileDownloadService: PortalFileDownloadService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
	}


	public ngOnDestroy(): void {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// TODO: Move this to either column spec or portal-file-list if possible?
	public downloadFile(id: number, assignedPortalEntityId?: number): void {
		this.portalFileDownloadService.downloadFile({portalFileId: id, assignedPortalEntityId});
	}
}
