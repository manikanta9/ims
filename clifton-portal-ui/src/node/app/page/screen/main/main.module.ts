import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SnakeCasePipe } from 'app/core/component/text/snake-case.pipe';
import { CoreModule } from 'app/core/core.module';
import { ContactComponent } from 'app/page/screen/main/contact/contact.component';
import { DocumentationComponent } from 'app/page/screen/main/documentation/documentation.component';
import { FeedbackComponent } from 'app/page/screen/main/feedback/feedback.component';
import { HelpComponent } from 'app/page/screen/main/help/help.component';
import { InvoicesComponent } from 'app/page/screen/main/invoices/invoices.component';
import { MainComponent } from 'app/page/screen/main/main.component';
import { OverviewComponent } from 'app/page/screen/main/overview/overview.component';
import { ReportingComponent } from 'app/page/screen/main/reporting/reporting.component';
import { StrategyOverviewComponent } from 'app/page/screen/main/strategy-overview/strategy-overview.component';
import { ComponentModule } from 'app/page/shared/component/component.module';


const ROUTES: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: '',
				pathMatch: 'full',
				redirectTo: 'overview',
			},
			{
				path: 'overview',
				component: OverviewComponent,
				data: {
					message: 'Welcome to Parametric\'s institutional client portal! Parametric is introducing a client portal providing enhanced login security, access to a vast selection of client specific documents and your key Parametric representative. We hope you enjoy your experience!',
				},
			},
			{
				path: 'documentation',
				component: DocumentationComponent,
				data: {
					message: 'Documentation',
				},
			},
			{
				path: 'reporting',
				component: ReportingComponent,
			},
			{
				path: 'invoices',
				component: InvoicesComponent,
				data: {
					message: 'Invoices',
				},
			},
			{
				path: 'strategy-overview',
				component: StrategyOverviewComponent,
			},
			{
				path: 'contact',
				component: ContactComponent,
				data: {
					message: 'Your relationship manager understands your unique portfolio needs from relationship inception and remains a client resource for providing ongoing solutions for unique situations throughout the life of the client relationship. The relationship manager collaborates with the investment team to ensure that client service meets or exceeds your expectations. Please reach out to your relationship manager today!',
				},
			},
			{
				path: 'help',
				component: HelpComponent,
			},
			{
				path: 'feedback',
				component: FeedbackComponent,
				data: {
					message: 'Thank you for visiting the client portal. In the months to come, we will continue to evolve this experience to provide additional Resources, Tools, and client account management capabilities. Feedback from our clients will help us continue to enhance your experience.',
				},
			},
		],
	},
];


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		ComponentModule,
		CoreModule,

		// Library modules
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
	],
	declarations: [
		ContactComponent,
		DocumentationComponent,
		FeedbackComponent,
		HelpComponent,
		InvoicesComponent,
		MainComponent,
		OverviewComponent,
		ReportingComponent,
		StrategyOverviewComponent,
	],
	providers: [
		SnakeCasePipe,
	],
})
export class MainModule {
	public static readonly ROUTES: Routes = ROUTES;
}
