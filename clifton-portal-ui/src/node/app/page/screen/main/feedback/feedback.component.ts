import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestLoader } from 'app/core/api/request-loader';
import { PortalFeedbackQuestion } from 'app/portal/feedback/portal-feedback-question';
import { PortalFeedbackResult } from 'app/portal/feedback/portal-feedback-result';
import { PortalFeedbackResultEntry } from 'app/portal/feedback/portal-feedback-result-entry';
import { PortalFeedbackService } from 'app/portal/feedback/portal-feedback.service';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-feedback',
	templateUrl: './feedback.component.html',
	animations: [
		// Animations.getEnterAnimation({enterTimingFn: 'ease-in-out', exitTimingFn: 'ease-in-out'}),
	],
})
export class FeedbackComponent implements OnInit, OnDestroy {

	public feedbackQuestionLoadRequester: RequestLoader<PortalFeedbackQuestion[]> = new RequestLoader({
		initiator: () => this.portalFeedbackService.getFeedbackQuestionList({
			requestedProperties: ['id', 'questionText', 'selectableOption', 'optionList.label'],
		}),
		initialValue: null,
	});
	public feedbackQuestionSaveRequester: RequestLoader<PortalFeedbackResultEntry, PortalFeedbackResultEntry> =
		new RequestLoader((entry: PortalFeedbackResultEntry) => this.portalFeedbackService.saveFeedbackResultEntry(entry));

	public questionList: PortalFeedbackQuestion[] = [];
	public questionForm: FormGroup = this.formBuilder.group({
		resultList: this.formBuilder.array([]),
	});

	public submitted = false;
	public error: string;

	private questionLoadSub: Subscription;
	private questionSaveSub: Subscription;


	constructor(private portalFeedbackService: PortalFeedbackService,
				private formBuilder: FormBuilder) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.questionLoadSub = this.feedbackQuestionLoadRequester.response$.subscribe(questionList => {
			this.questionList = questionList;
			this.resetQuestionForm();
		});
		this.questionSaveSub = this.feedbackQuestionSaveRequester.response$.subscribe(entry => {
			this.questionForm.reset();
			this.submitted = true;
		});
	}


	public ngOnDestroy(): void {
		this.questionLoadSub.unsubscribe();
		this.questionSaveSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public onSubmit(): void {
		const rawFeedbackResultEntry: PortalFeedbackResultEntry = this.questionForm.value;
		const feedbackResultEntry: PortalFeedbackResultEntry = {
			resultList: rawFeedbackResultEntry.resultList!.filter((result: PortalFeedbackResult) => !!result.resultText),
		};

		if (!feedbackResultEntry.resultList!.length) {
			this.questionForm.setErrors({isEmpty: true});
		}
		else {
			this.feedbackQuestionSaveRequester.load(feedbackResultEntry);
		}
	}


	public onReset(): void {
		this.resetQuestionForm();
		this.questionForm.markAsPristine();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private resetQuestionForm(): void {
		this.questionForm.setControl('resultList',
			this.formBuilder.array(this.questionList.map(question =>
				this.formBuilder.group({
					feedbackQuestion: this.formBuilder.group({id: [question.id, Validators.required]}),
					resultText: [''],
				}),
			)),
		);
	}
}

