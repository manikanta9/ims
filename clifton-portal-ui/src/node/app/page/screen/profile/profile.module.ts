import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { CoreModule } from 'app/core/core.module';
import { ChangePasswordComponent } from 'app/page/popup/change-password/change-password.component';
import { ComponentModule } from 'app/page/shared/component/component.module';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ContactInformationComponent } from './contact-information/contact-information.component';
import { NotificationSettingsComponent } from './notification-settings/notification-settings.component';


const ROUTES: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'contact-information',
	},
	{
		path: 'contact-information',
		component: ContactInformationComponent,
	},
	{
		path: 'notification-settings',
		component: NotificationSettingsComponent,
	},
	{
		path: 'change-password',
		component: ChangePasswordComponent,
	},
];


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		CoreModule,

		// Library modules
		// TODO: Remove standard FormsModule?
		FormsModule,
		ReactiveFormsModule,
		PopoverModule,

		// Page modules
		ComponentModule,
	],
	declarations: [
		ContactInformationComponent,
		NotificationSettingsComponent,
	],
	exports: [
		ContactInformationComponent,
		NotificationSettingsComponent,
	],
})
export class ProfileModule {
	public static readonly ROUTES: Routes = ROUTES;
}
