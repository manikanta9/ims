import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { JsonUtils } from 'app/core/util/json.utils';
import { ObjectUtils } from 'app/core/util/object.utils';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityQuery } from 'app/portal/entity/portal-entity-query';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalNotificationSubscription } from 'app/portal/notification/portal-notification-subscription';
import { PortalNotificationSubscriptionEntry } from 'app/portal/notification/portal-notification-subscription-entry';
import { PortalNotificationSubscriptionFrequenciesData } from 'app/portal/notification/portal-notification-subscription-frequencies';
import { PortalNotificationService, PortalNotificationSubscriptionEntrySaveFacade } from 'app/portal/notification/portal-notification.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignmentQuery } from 'app/portal/security/portal-security-user-assignment.query';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


// TODO: Structure
@Component({
	selector: 'ppa-notification-settings',
	templateUrl: './notification-settings.component.html',
})
export class NotificationSettingsComponent implements OnInit, OnDestroy {

	private static readonly FILTER_TOOLBAR_CONFIG: FilterToolbarConfig = {
		group: FilterGroup.PROFILE,
		filters: {
			group: {autoSelect: true, allowAll: true},
			relationship: {autoSelect: true, allowAll: true},
			entity: {autoSelect: true, allowAll: true},
			account: {autoSelect: true, allowAll: true},
		},
	};

	public subscriptionForm = this.formBuilder.group({
		securityUser: this.formBuilder.group({id: ['', Validators.required]}),
		portalEntity: this.formBuilder.group({id: ['', Validators.required]}),
		notifications: this.formBuilder.array([
			this.formBuilder.group({header: [''], items: this.formBuilder.array([])}),
		]),
	});
	public availableEntityListGetter = (query: PortalEntityQuery) => this.getAvailableEntityList(query);
	public notificationHierarchyMap: Map<string, PortalNotificationSubscription[]>;

	// Requesters
	public subscriptionEntryRequester: RequestLoader<PortalNotificationSubscriptionEntry> = new RequestLoader({
		preModifier: obs$ => obs$.switchMap(() => this.selectedEntity$),
		initiator: (entity: PortalEntity) => this.isSimulatingUser$
			.switchMap(isSimulatingUser => isSimulatingUser
				? this.securityUser$.mergeMap(user => this.portalNotificationService.getNotificationSubscriptionEntryForUser(user.id!, entity.id!))
				: this.portalNotificationService.getNotificationSubscriptionEntry(entity.id!)),
	});
	public subscriptionSaveRequester: RequestLoader<PortalNotificationSubscriptionEntry, PortalNotificationSubscriptionEntrySaveFacade> = new RequestLoader({
		initiator: entry => this.portalNotificationService.saveNotificationSubscriptionEntry(entry),
		subConfig: {comp: this, callback: () => this.subscriptionEntryRequester.load()},
	});

	public filterEntity: PortalEntity;

	// Observables
	public isSimulatingUser$: Observable<boolean> = this.store.select(pageStore.isSimulatingUser);
	public selectedEntity$: Observable<PortalEntity> = this.filterToolbarService.getFilterValues()
		.map(value => value.entity!)
		.do(entity => this.filterEntity = entity)
		.filter(value => !!value);
	public securityUser$: Observable<PortalSecurityUser> = this.isSimulatingUser$
		.switchMap(isSimulatingUser => this.store.select(isSimulatingUser ? pageStore.getSimulatedUserInfo : pageStore.getUser))
		.filter(user => !!user) as Observable<PortalSecurityUser>;
	public subscriptionList$: Observable<PortalNotificationSubscription[]> = Observable
		.combineLatest(this.subscriptionEntryRequester.response$, this.securityUser$, this.selectedEntity$)
		.filter(([subscriptionEntry, securityUser, entity]) => !!(subscriptionEntry && securityUser && entity))
		.do(([subscriptionEntry, securityUser, entity]) => this.populateSubscriptionForm(subscriptionEntry.subscriptionList!, securityUser, entity))
		.map(([subscriptionEntry, securityUser, entity]) => subscriptionEntry.subscriptionList!);

	// Subscriptions
	public entryRequesterSub: Subscription;


	constructor(private formBuilder: FormBuilder,
	            private portalNotificationService: PortalNotificationService,
	            private portalEntityService: PortalEntityService,
	            private filterToolbarService: FilterToolbarService,
	            private store: Store<pageStore.PageStore>,
	            private modalDialogService: ModalDialogService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.entryRequesterSub = this.selectedEntity$.subscribe(() => this.subscriptionEntryRequester.load());
		this.filterToolbarService.enableFilter(NotificationSettingsComponent.FILTER_TOOLBAR_CONFIG);
	}


	public ngOnDestroy(): void {
		this.entryRequesterSub.unsubscribe();
		this.filterToolbarService.disableFilter();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private populateSubscriptionForm(subscriptionList: PortalNotificationSubscription[], securityUser: PortalSecurityUser, entity: PortalEntity): void {
		(this.subscriptionForm.get('securityUser') as FormGroup).patchValue({id: securityUser.id});
		(this.subscriptionForm.get('portalEntity') as FormGroup).patchValue({id: entity.id});
		this.populateNotificationList(subscriptionList);
	}


	private populateNotificationList(subscriptionList: PortalNotificationSubscription[]): void {
		// create the map
		this.notificationHierarchyMap = new Map<string, PortalNotificationSubscription[]>();
		subscriptionList.forEach((subscription: PortalNotificationSubscription) => {
			if (subscription.fileCategory && subscription.fileCategory.leaf) {
				const key = subscription.fileCategory.rootParent as PortalFileCategory;
				this.addToMap(this.notificationHierarchyMap, key.name!, subscription);
			}
			else {
				this.addToMap(this.notificationHierarchyMap, 'Other', subscription);
			}
		});

		// create the array structure
		const notificationsArray: any[] = [];

		this.notificationHierarchyMap.forEach((value, key, map) => {
			const itemsFormArray = this.buildHeaderFormArray(value, key) as FormArray;
			const notificationsFormGroup = this.formBuilder.group({header: key});
			notificationsFormGroup.setControl('items', itemsFormArray);
			notificationsArray.push(notificationsFormGroup);
		});

		// set the form array on the form control
		this.subscriptionForm.setControl('notifications', this.formBuilder.array(notificationsArray));
	}


	private buildHeaderFormArray(value: PortalNotificationSubscription[], key: string): FormArray {
		const rootProperties: any[] = [];
		if (value && Array.isArray(value)) {
			for (const subscription of value) {
				const subscriptionRequired = subscription.fileCategory && subscription.fileCategory.notificationSubscriptionRequired;
				const notificationProperties: any = {
					id: subscription.id,
					// tslint:disable-next-line:object-literal-key-quotes : Reserved word
					'class': 'com.clifton.portal.notification.subscription.PortalNotificationSubscription',
					notificationEnabled: new FormControl({value: subscription.notificationEnabled, disabled: subscriptionRequired}),
					subscriptionFrequency: subscription.subscriptionFrequency || '',
					userAssignment: {id: subscription.userAssignment!.id},
					portalEntity: {id: subscription.portalEntity!.id},
				};

				if (subscription.fileCategory) {
					notificationProperties.fileCategory = {
						id: subscription.fileCategory.id,
						description: subscription.fileCategory.description,
					};
				}

				if (subscription.notificationDefinitionType) {
					notificationProperties.notificationDefinitionType = {id: subscription.notificationDefinitionType.id};
				}
				rootProperties.push(this.formBuilder.group(notificationProperties));
			}
		}
		return this.formBuilder.array(rootProperties);
	}


	private addToMap(map: Map<string, any>, key: string, value: any): void {
		if (map.has(key)) {
			const values = map.get(key);
			if (values && Array.isArray(values)) {
				values.push(value);
			}
		}
		else {
			map.set(key, [value]);
		}
	}


	public saveSubscriptionForm(form: FormGroup): void {
		const message = `Are you sure you would like to make these notification changes for ${this.filterEntity.label}?`;
		const modal = this.modalDialogService.showConfirmation(message, 'Save Changes');
		modal.result
			.catch((dismissReason: any) => false)  /* Swallow modal dismissal */
			.then(val => {
				if (val && val !== 'no') {
					const notificationValues = form.value.notifications.map((header: any) => header.items);
					const mergedValues = [].concat.apply([], notificationValues);
					this.subscriptionSaveRequester.load({
						securityUser: {id: form.value.securityUser.id},
						portalEntity: {id: form.value.portalEntity.id},
						subscriptionList: JsonUtils.serialize(mergedValues.map((sub: PortalNotificationSubscription) => ObjectUtils.flatten(sub))),
					});
					this.subscriptionForm.markAsPristine();
				}
			});
	}


	public resetSubscriptionForm(): void {
		const headerList = this.subscriptionForm.get('notifications') as FormArray;
		for (let i = 0; i < headerList.length; i++) {
			const items = headerList.at(i).get('items') as FormArray;
			for (let j = 0; j < items.length; j++) {
				const formSubscription = items.at(j);

				this.notificationHierarchyMap.forEach(header =>
					header.forEach(sub => {
						if (sub.id === formSubscription.value.id && sub.notificationEnabled !== formSubscription.value.notificationEnabled) {
							formSubscription.patchValue({
								notificationEnabled: sub.notificationEnabled,
							});
						}
					}));
			}
		}
		this.subscriptionForm.markAsPristine();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private getAvailableEntityList(query: PortalSecurityUserAssignmentQuery): Observable<PortalEntity[]> {
		return this.store.select(pageStore.getUser)
			.mergeMap((user?: PortalSecurityUser) => this.portalEntityService.getEntityList({
				hasSecurityResourceAccess: true,
			}));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public hasNotificationFrequencySelection(subscription: PortalNotificationSubscription): boolean {
		return this.portalNotificationService.getSubscriptionFrequenciesForFileFrequency(subscription.fileCategory && subscription.fileCategory.portalFileFrequency!).length > 0;
	}


	public getNotificationFrequencyOptions(subscription: PortalNotificationSubscription): PortalNotificationSubscriptionFrequenciesData[] {
		return this.portalNotificationService.getSubscriptionFrequenciesForFileFrequency(subscription.fileCategory && subscription.fileCategory.portalFileFrequency!);
	}


	public getNotificationTitle(subscription: PortalNotificationSubscription): string {
		if (subscription.fileCategory && subscription.fileCategory.description) {
			return subscription.fileCategory.description;
		}
		else if (subscription.notificationDefinitionType && subscription.notificationDefinitionType.description) {
			return subscription.notificationDefinitionType.description;
		}
		return '';
	}
}
