import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { LogUtils } from 'app/core/log/log.utils';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { FormUtils } from 'app/core/util/form.utils';
import { StringUtils } from 'app/core/util/string.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { PageElementInfo } from 'app/portal/state/reducers/page-element.reducer';
import * as user from 'app/portal/state/reducers/user.actions';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


interface ProfileFormValue {
	id: any;
	emailAddress: any;
	firstName: any;
	lastName: any;
	title: any;
	companyName: any;
	phoneNumber: any;
}


@Component({
	selector: 'ppa-contact-information',
	templateUrl: './contact-information.component.html',
	/*
	 * Besides being more efficient and enforcing constraints that provide (generally) better design, the OnPush strategy has especially been chosen in this component to avoid a
	 * change-detection issue when submitting the form via an "enter" keypress. An ExpressionChangedAfterItHasBeenCheckedError error is thrown when submitting the form via "enter"
	 * as of Angular 4.1.3. This seems to be related to changing the state of the form while reloading the content for the form. Changing to a less-aggressive change-detection
	 * strategy avoids this error.
	 */
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactInformationComponent implements OnInit, OnDestroy {

	private static readonly VALIDATION_MESSAGES: { [fieldName in keyof ProfileFormValue]: { [errorType: string]: string } } = {
		id: {required: 'The user ID is required.'},
		emailAddress: {
			required: 'An email address is required.',
			email: 'Invalid email address.',
			maxlength: 'Email address cannot be more than 50 characters long.',
		},
		firstName: {required: 'A first name is required.', maxlength: 'First name cannot be more than 50 characters long.'},
		lastName: {required: 'A last name is required.', maxlength: 'Last name cannot be more than 50 characters long.'},
		title: {required: 'A title is required.', maxlength: 'Title cannot be more than 100 characters long.'},
		companyName: {required: 'A company is required.', maxlength: 'Company name cannot be more than 100 characters long.'},
		phoneNumber: {
			required: 'A phone number is required.',
			pattern: 'Phone numbers must be at least 10 digits long, including area code.',
			maxlength: 'Phone number cannot be more than 50 characters long.',
		},
	};

	public userProfile: PortalSecurityUser;
	public formErrors: { [fieldName in keyof ProfileFormValue]: string } = {
		id: '',
		emailAddress: '',
		firstName: '',
		lastName: '',
		title: '',
		companyName: '',
		phoneNumber: '',
	};

	public emailAddressEls$ = this.store.select(pageStore.getPageElementMulti('profile.change-request.email.address'));

	// Observers
	public loading$: Subject<boolean> = new BehaviorSubject(false);
	public userProfileForm$: Subject<FormGroup> = new Subject();
	private userProfileRequestEmitter$: Subject<any> = new Subject();

	// Observables
	private userProfileRequest$: Observable<PortalSecurityUser> = this.userProfileRequestEmitter$
		.do(() => this.startLoading())
		.switchMap(() => this.portalSecurityService.getCurrentUser())
		.do((currentUser: PortalSecurityUser) => this.store.dispatch(new user.RefreshUserAction(currentUser)))
		.catch((error: PortalResponse, request$: Observable<PortalSecurityUser>) => this.handleError(error, request$))
		.do(() => this.finishLoading());

	// Subscriptions
	private userProfileSub: Subscription;
	private userProfileFormSub: Subscription;
	private userProfileFormChangeSub: Subscription;


	constructor(private store: Store<pageStore.PageStore>,
	            private formBuilder: FormBuilder,
	            private portalSecurityService: PortalSecurityService,
	            private modalDialogService: ModalDialogService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.userProfileSub = this.userProfileRequest$.subscribe((currentUser: PortalSecurityUser) => {
			this.userProfile = currentUser;
			const profileForm = this.formBuilder.group({
				id: [currentUser.id, Validators.required],
				emailAddress: [{value: currentUser.emailAddress, disabled: true}, [Validators.required, Validators.email, Validators.maxLength(50)]],
				firstName: [currentUser.firstName, [Validators.required, Validators.maxLength(50)]],
				lastName: [currentUser.lastName, [Validators.required, Validators.maxLength(50)]],
				title: [currentUser.title, [Validators.required, Validators.maxLength(100)]],
				companyName: [currentUser.companyName, [Validators.required, Validators.maxLength(100)]],
				// Additional backend constraints may exist which are not enforced here
				phoneNumber: [currentUser.phoneNumber, [Validators.required, Validators.pattern(/^(?:[^\d]*\d[^\d]*){10,}$/), Validators.maxLength(50)]],
			} as ProfileFormValue);
			// profileForm.markAsDirty();
			this.userProfileForm$.next(profileForm);

		});
		this.userProfileFormSub = this.userProfileForm$.subscribe(userProfileForm => {
			if (this.userProfileFormChangeSub) {
				this.userProfileFormChangeSub.unsubscribe();
			}
			this.userProfileFormChangeSub = userProfileForm.valueChanges.subscribe((formValue: ProfileFormValue) => this.onValueChanged(formValue, userProfileForm));
		});
		this.userProfileRequestEmitter$.next();
	}


	public ngOnDestroy(): void {
		this.userProfileSub.unsubscribe();
		this.userProfileFormSub.unsubscribe();
		if (this.userProfileFormChangeSub) {
			this.userProfileFormChangeSub.unsubscribe();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public saveContactInformation(form: FormGroup): void {
		this.startLoading();
		this.portalSecurityService.saveCurrentUser(form.value as PortalSecurityUser)
			.catch(error => Observable.of(true)) // TODO: Error-handling here
			.subscribe(() => this.userProfileRequestEmitter$.next());
	}


	public resetContactInformation(form: FormGroup): void {
		form.reset(this.userProfile);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startLoading(): void {
		this.loading$.next(true);
	}


	private finishLoading(): void {
		this.loading$.next(false);
	}


	private handleError(error: PortalResponse, request$: Observable<any>) {
		// TODO: Handle error
		this.finishLoading();
		LogUtils.error('Encountered error during request', error);
		return request$;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private onValueChanged(formValue: ProfileFormValue, userProfileForm: FormGroup) {
		const formErrors: any = this.formErrors;
		const validationMessages: { [fieldName: string]: { [errorType: string]: string } } = ContactInformationComponent.VALIDATION_MESSAGES;
		for (const field of Object.getOwnPropertyNames(this.formErrors)) {
			// Reset any existing error messages
			formErrors[field] = '';
			const control = userProfileForm.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = validationMessages[field];
				for (const key of Object.getOwnPropertyNames(control.errors)) {
					formErrors[field] += messages[key] + ' ';
				}
			}
		}
	}


	public onEditEmail(emailAddressEls: PageElementInfo[]): void {
		const emailAddressLinks = emailAddressEls
			.map(emailAddressEl => FormUtils.generateEmailLink({
				mailto: emailAddressEl.value,
				text: emailAddressEl.value,
				subject: `Change Email Request: ${this.userProfile.emailAddress!}`,
			}))
			.map(link => link.outerHTML);
		const emailAddressHtml = StringUtils.concatWithConjunction(emailAddressLinks, 'or');
		const message = `Please reach out to ${emailAddressHtml} to adjust your email address. Provide your email address and we will assist you in this edit as this will adjust your username for login purposes.`;
		this.modalDialogService.showMessage(message, 'Request to Change Email');
	}
}
