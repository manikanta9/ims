import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from 'app/page/screen/admin/admin.component';
import { AdminGuard } from 'app/page/screen/admin/admin.guard';
import { UserManagementComponent } from 'app/page/screen/admin/user-management/user-management.component';


const routes: Routes = [
	{
		path: '',
		component: AdminComponent,
		canActivate: [AdminGuard],
		children: [
			{
				path: '',
				redirectTo: 'user-management',
			},
			{
				path: 'user-management',
				component: UserManagementComponent,
			},
		],
	},
];


@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: [],
})
export class AdminRoutingModule {
}
