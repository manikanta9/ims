import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AdminGuard implements CanActivate {

	private isSimulatingUser$: Observable<boolean> = this.store.select(pageStore.isSimulatingUser);
	private isAdmin$: Observable<boolean> = !environment.administrationMode
		// For external users, use the user's admin access
		? this.store.select(pageStore.isAdmin).map(value => !!value)
		// For internal users, use the simulated user's access, or if there is no simulated user then treat the user as an administrator
		: this.isSimulatingUser$.switchMapIf(simulating => simulating,
			this.store.select(pageStore.getSimulatedUserIsAdmin).map(value => !!value),
			Observable.of(true));


	constructor(private store: Store<pageStore.PageStore>) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.isAdmin$;
	}
}
