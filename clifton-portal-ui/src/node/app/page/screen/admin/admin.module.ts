import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'app/core/core.module';
import { AdminRoutingModule } from 'app/page/screen/admin/admin-routing.module';
import { AdminComponent } from 'app/page/screen/admin/admin.component';
import { AdminGuard } from 'app/page/screen/admin/admin.guard';
import { AddUserFormComponent } from 'app/page/screen/admin/user-management/components/add-user-form.component';
import { DeleteUserFormComponent } from 'app/page/screen/admin/user-management/components/delete-user-form.component';
import { UserDetailFormComponent } from 'app/page/screen/admin/user-management/components/user-detail-form.component';
import { UserListComponent } from 'app/page/screen/admin/user-management/components/user-list.component';
import { UserManagementComponent } from 'app/page/screen/admin/user-management/user-management.component';
import { ComponentModule } from 'app/page/shared/component/component.module';
import { PopoverModule } from 'ngx-bootstrap/popover';


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		// TODO: Remove standard FormsModule?
		FormsModule,
		ReactiveFormsModule,
		CoreModule,
		ComponentModule,

		// Page modules
		AdminRoutingModule,

		// Library modules
		PopoverModule,
	],
	declarations: [
		AdminComponent,
		UserManagementComponent,
		UserListComponent,
		AddUserFormComponent,
		DeleteUserFormComponent,
		UserDetailFormComponent,
		// UserManagementBComponent,
	],
	providers: [
		// TODO: Put this higher up
		AdminGuard,
	],
})
export class AdminModule {
	// public static forRoot(): ModuleWithProviders {
	// 	return {
	// 		ngModule: AdminModule,
	// 	};
	// }
}
