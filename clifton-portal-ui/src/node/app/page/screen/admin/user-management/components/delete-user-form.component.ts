import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormErrors } from 'app/core/form/form-errors';
import { FormValidationMessages } from 'app/core/form/form-validation-messages';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { FormUtils } from 'app/core/util/form.utils';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';


export interface DeleteUserFormValue {
	userAssignment: { id: number; };
	deleteReason: string;
}


@Component({
	selector: 'ppa-delete-user-form',
	templateUrl: './delete-user-form.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DeleteUserFormComponent implements OnInit, OnDestroy {

	private static readonly VALIDATION_MESSAGES: FormValidationMessages<DeleteUserFormValue> = {
		deleteReason: {
			required: 'A reason is required.',
			maxlength: 'The reason cannot be more than 50 characters long.',
		},
	};

	@Input() public userAssignment: PortalSecurityUserAssignment;
	@Output() public submitAction: EventEmitter<DeleteUserFormValue> = new EventEmitter();
	@Output() public cancelAction: EventEmitter<void> = new EventEmitter();

	public deleteUserForm: FormGroup = this.formBuilder.group({
		userAssignment: this.formBuilder.group({
			id: ['', Validators.required],
		}),
		deleteReason: ['', [Validators.required, Validators.maxLength(50)]],
	});
	public formErrors: FormErrors<DeleteUserFormValue> = new FormErrors();


	constructor(private formBuilder: FormBuilder) {
		ManagedSubscription
			.of(this.deleteUserForm.valueChanges,
				() => this.formErrors = FormUtils.getValidationErrors(this.deleteUserForm, DeleteUserFormComponent.VALIDATION_MESSAGES))
			.attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {}


	public ngOnDestroy(): void {}
}
