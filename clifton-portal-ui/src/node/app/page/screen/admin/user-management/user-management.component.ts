import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { TransformingObservable } from 'app/core/rxjs/observable/transforming-observable';
import { JsonUtils } from 'app/core/util/json.utils';
import { AddUserFormValue } from 'app/page/screen/admin/user-management/components/add-user-form.component';
import { DeleteUserFormValue } from 'app/page/screen/admin/user-management/components/delete-user-form.component';
import { UserDetailFormValue } from 'app/page/screen/admin/user-management/components/user-detail-form.component';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import { ManagedFilterToolbarConfig } from 'app/page/shared/filter/filter-toolbar/managed-filter-toolbar-config';
import * as pageStore from 'app/page/shared/state/page.store';
import { OrderBy } from 'app/portal/api/portal-order-by';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityResource } from 'app/portal/security/portal-security-resource';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserAssignmentQuery } from 'app/portal/security/portal-security-user-assignment.query';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { ApiUtils } from 'app/portal/util/api.utils';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';


enum UserState {
	NONE, ADD, CREATE, EDIT, DELETE,
}


@Component({
	selector: 'ppa-user-management',
	templateUrl: './user-management.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserManagementComponent implements OnInit, OnDestroy {

	private static readonly FILTER_CONFIG: FilterToolbarConfig = {
		group: FilterGroup.ADMIN,
		filters: {
			group: {autoSelect: true, allowAll: true},
			relationship: {autoSelect: true, allowAll: true},
			entity: {autoSelect: true, allowAll: true},
		},
	};


	public readonly STATE = UserState;
	public currentState$: BehaviorSubject<UserState> = new BehaviorSubject(UserState.NONE);
	public addingUsername = '';
	public filterEntity?: PortalEntity;
	public searchPattern?: string;
	public pageTitleEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));

	public filterEntity$: Observable<PortalEntity | undefined> = this.filterToolbarService.getFilterValues()
		.do(() => this.resetState())
		.map(value => value && value.entity || void 0)
		// TODO: Remove non-observable entity field
		.do(entity => this.filterEntity = entity)
		.shareReplay();
	public searchPatternSource: TransformingObservable<string | undefined, string | undefined> = new TransformingObservable(obs$ => obs$
		.debounceTime(300)
		.distinctUntilChanged(),
		new BehaviorSubject(undefined));

	public userAssignmentSortSource: TransformingObservable<string, OrderBy | undefined> = new TransformingObservable(obs$ => obs$
		.scan((previous, current) => this.determineSortOrder(previous, current), undefined)
		// Since multiple subscribers will be used, the result of the scan operator (which tracks state) must be shared to prevent being run multiple times
		.shareReplay(),
		new BehaviorSubject('userFirstLastName'));
	private userAssignmentQuery$: Observable<PortalSecurityUserAssignmentQuery | undefined> = Observable
		.combineLatest(
			this.filterEntity$,
			this.userAssignmentSortSource,
			this.searchPatternSource,
		)
		// TODO: Pull this out into a separate function to clean up too
		.map(([entity, sort, searchPattern]: [PortalEntity, OrderBy, string | undefined]) => entity ? {
			populateResourcePermissionMap: true,
			disabled: false,
			viewSecurityAdminRole: true,
			orderBy: sort,
			portalEntityIdOrRelatedEntity: entity.id,
			...searchPattern ? {searchPattern} : {},
		} : void 0);
	public userAssignmentRequester: RequestLoader<PortalSecurityUserAssignment[]> = new RequestLoader({
		preModifier: obs$ => obs$.mergeMapTo(this.userAssignmentQuery$).nonNull(),
		initiator: query => this.portalSecurityService.getUserAssignmentList(query),
		initialValue: null,
	});
	public userSaveRequester: RequestLoader<void, [PortalSecurityUser, PortalSecurityUserAssignment]> = new RequestLoader({
		initiator: ([user, userAssignment]) => this.portalSecurityService.saveUser(user)
			// Map to appropriate user assignment object
			.map(savedUser => ({
				...userAssignment,
				securityUser: {
					...userAssignment.securityUser,
					id: savedUser.id,
				},
				portalEntity: {
					...userAssignment.portalEntity,
					...(userAssignment.id == null && this.filterEntity) ? {id: this.filterEntity.id} : {},
				},
			}))
			// Save user assignment
			.mergeMap(modifiedUserAssignment => this.portalSecurityService.saveUserAssignment(modifiedUserAssignment)),
		subConfig: {comp: this, callback: () => this.resetState()},
	});
	public userPermissionsSaveRequester: RequestLoader<void, PortalSecurityUserAssignment> = new RequestLoader({
		initiator: userAssignment => this.portalSecurityService.saveUserAssignment(userAssignment),
		subConfig: {comp: this, callback: () => this.resetState()},
	});
	public userDeleteRequester: RequestLoader<PortalResponse, { id: number, reason: string }> = new RequestLoader({
		initiator: ({id, reason}) => this.portalSecurityService.deleteUserAssignment(id, reason),
		subConfig: {comp: this, callback: () => this.resetState()},
	});


	/**
	 * A specific User Assignment - the current selected user
	 */
	public selectedAssignment?: PortalSecurityUserAssignment;

	/**
	 * A specific User - retrieved from Add User (initial)
	 */
	public existingSecurityUser?: PortalSecurityUser;


	constructor(private portalSecurityService: PortalSecurityService,
	            private filterToolbarService: FilterToolbarService,
	            private store: Store<pageStore.PageStore>) {
		ManagedFilterToolbarConfig.of(this.filterToolbarService, UserManagementComponent.FILTER_CONFIG).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {}


	public ngOnDestroy(): void {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public userAssignmentHasSecurityResource(userAssignment: PortalSecurityUserAssignment, resource: PortalSecurityResource): boolean {
		return _.some(userAssignment.userAssignmentResourceList!, (assignmentResource: PortalSecurityUserAssignmentResource) => assignmentResource.securityResource!.id === resource.id && assignmentResource.accessAllowed);
	}


	public saveUserAssignmentForm(userForm: UserDetailFormValue): void {
		// only save the user when we are adding a new user that does not exist
		if (this.selectedAssignment || this.existingSecurityUser) {
			const assignment = this.extractUserAssignment(userForm);
			if (this.existingSecurityUser) {
				// adding a user that already exists for the selected filter
				assignment.portalEntity = {id: this.filterEntity!.id};
			}
			this.userPermissionsSaveRequester.load(assignment);
		}
		else {
			// adding a new user
			this.userSaveRequester.load([this.extractUser(userForm), this.extractUserAssignment(userForm)]);
		}
	}


	public deleteUserAssignment(userForm: DeleteUserFormValue): void {
		this.userDeleteRequester.load({id: userForm.userAssignment.id, reason: userForm.deleteReason});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public beginAddUser(): void {
		this.currentState$.next(UserState.ADD);
	}


	public checkExistingUser(event: any): void {
		const form = event.event;
		const pop = event.pop;
		const formErrors = event.errors;

		const userForm = form.value as AddUserFormValue;
		const username = userForm.user.username;


		this.portalSecurityService.getUserByUsernameWithNoAssignment(username, this.filterEntity!.id, true)
			.first()
			.subscribe((response: PortalResponse) => {
				if (!response.success) {
					const usernameControl = form.get('user')!.get('username')!;
					formErrors.setError('user.username', response.message);
					usernameControl.setErrors({error: true});

					// display the popover
					pop.show();
				}
				else {
					// response was successful, proceed to Add User
					const user: PortalSecurityUser = ApiUtils.getPortalResponseData(response);
					this.beginCreateUser(username, user);
				}
			});
	}


	public beginCreateUser(username: string, user?: PortalSecurityUser): void {
		this.resetState();
		this.addingUsername = username;
		if (user) {
			this.existingSecurityUser = user;
		}
		this.currentState$.next(UserState.CREATE);
	}


	public submitUserDetailForm(userForm: UserDetailFormValue): void {
		this.saveUserAssignmentForm(userForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 *  State Logic
	 */
	public resetState(): void {
		this.selectedAssignment = void 0;
		this.existingSecurityUser = void 0;
		this.currentState$.next(UserState.NONE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private extractUserAssignment(userForm: UserDetailFormValue): PortalSecurityUserAssignment {
		const assignmentResourceList = userForm.userAssignment.userAssignmentResourceList
			.map((assignmentResource: any) => ({
				id: assignmentResource.id,
				// tslint:disable-next-line:object-literal-key-quotes : Reserved word
				'class': 'com.clifton.portal.security.user.PortalSecurityUserAssignmentResource',
				accessAllowed: assignmentResource.accessAllowed,
				'securityResource.id': assignmentResource.resourceId,
			}));
		return {
			id: userForm.userAssignment.id,
			securityUser: {id: userForm.user.id},
			// The server processes the assignment resource list as serialized JSON, even when passed as form values
			userAssignmentResourceList: JsonUtils.serialize(assignmentResourceList) as any,
			assignmentRole: {id: userForm.userAssignment.roleId},
		};
	}


	private extractUser(userForm: UserDetailFormValue): PortalSecurityUser {
		return userForm.user;
	}


	private determineSortOrder(previous: OrderBy, current: string): OrderBy | undefined {
		// Swap direction if needed
		const prevOrderBy = Array.isArray(previous) ? previous[0] : void 0;
		const newDirection: 'ASC' | 'DESC' = (prevOrderBy && prevOrderBy.field === current && prevOrderBy.direction === 'ASC') ? 'DESC' : 'ASC';
		return current ? [{field: current, direction: newDirection}] : void 0;
	}
}
