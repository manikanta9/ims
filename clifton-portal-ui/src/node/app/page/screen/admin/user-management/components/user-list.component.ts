import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { OrderBy } from 'app/portal/api/portal-order-by';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';


@Component({
	selector: 'ppa-user-list',
	templateUrl: './user-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListComponent {
	@Input() public orderBy: OrderBy | undefined;
	@Input() public valueList: PortalSecurityUserAssignment[];
	@Output() public deleteAction: EventEmitter<PortalSecurityUserAssignment> = new EventEmitter();
	@Output() public editAction: EventEmitter<PortalSecurityUserAssignment> = new EventEmitter();
	@Output() public sortAction: EventEmitter<string> = new EventEmitter();
}
