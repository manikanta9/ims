import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormErrors } from 'app/core/form/form-errors';
import { FormValidationMessages } from 'app/core/form/form-validation-messages';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { FormUtils } from 'app/core/util/form.utils';
import { PortalEntity } from 'app/portal/entity/portal-entity';


export interface AddUserFormValue {
	user: { username: string; };
}


@Component({
	selector: 'ppa-add-user-form',
	templateUrl: './add-user-form.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddUserFormComponent implements OnInit, OnDestroy {

	public static readonly VALIDATION_MESSAGES: FormValidationMessages<AddUserFormValue> = {
		user: {
			username: {
				required: 'An email address is required.',
				email: 'Invalid email address.',
				maxlength: 'Email address cannot be more than 100 characters long.',
			},
		},
	};

	@Input() public filterEntity: PortalEntity;

	@Output() public submitAction: EventEmitter<{ event: FormGroup, pop: any, errors: FormErrors<AddUserFormValue> }> = new EventEmitter();
	@Output() public cancelAction: EventEmitter<void> = new EventEmitter();

	public addUserInitialForm: FormGroup = this.formBuilder.group({
		user: this.formBuilder.group({
			username: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
		}),
	});
	public formErrors: FormErrors<AddUserFormValue> = new FormErrors();


	constructor(private formBuilder: FormBuilder) {
		ManagedSubscription
			.of(this.addUserInitialForm.valueChanges,
				() => this.formErrors = FormUtils.getValidationErrors(this.addUserInitialForm, AddUserFormComponent.VALIDATION_MESSAGES))
			.attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {}


	public ngOnDestroy(): void {}
}
