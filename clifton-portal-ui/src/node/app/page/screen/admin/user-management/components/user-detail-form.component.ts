import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { FormErrors } from 'app/core/form/form-errors';
import { FormValidationMessages } from 'app/core/form/form-validation-messages';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { FormUtils } from 'app/core/util/form.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserRole } from 'app/portal/security/portal-security-user-role';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { Observable } from 'rxjs/Observable';


export interface UserDetailFormValue {
	user: {
		id: number;
		emailAddress: string;
		username: string;
		firstName: string;
		lastName: string;
		title: string;
		companyName: string;
		phoneNumber: string;
	};
	userAssignment: {
		id: number;
		userId: number;
		userAssignmentResourceList: {
			id: number;
			resourceId: number;
			resourceLeaf: boolean;
			resourceParentId?: number;
			accessAllowed: boolean;
		}[];
		roleId: number;
	};
}


@Component({
	selector: 'ppa-user-detail-form',
	templateUrl: './user-detail-form.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailFormComponent implements OnInit, OnDestroy {

	private static readonly VALIDATION_MESSAGES: FormValidationMessages<UserDetailFormValue> = {
		user: {
			id: {required: 'The user ID is required.'},
			username: {
				required: 'An email address is required.',
				email: 'Invalid email address.',
				maxlength: 'Email address cannot be more than 100 characters long.',
			},
			firstName: {required: 'A first name is required.', maxlength: 'First name cannot be more than 50 characters long.'},
			lastName: {required: 'A last name is required.', maxlength: 'Last name cannot be more than 50 characters long.'},
			title: {required: 'A title is required.', maxlength: 'Title cannot be more than 100 characters long.'},
			companyName: {required: 'A company is required.', maxlength: 'Company name cannot be more than 100 characters long.'},
			phoneNumber: {
				required: 'A phone number is required.',
				pattern: 'Phone numbers must be at least 10 digits long, including area code.',
				maxlength: 'Phone number cannot be more than 50 characters long.',
			},
		},
		userAssignment: {
			roleId: {required: 'A role is required.'},
		},
	};

	@Input() public newUsername?: string;
	@Input() public userAssignment?: PortalSecurityUserAssignment;
	@Input() public existingSecurityUser?: PortalSecurityUser;
	@Input() public filterEntity: PortalEntity;
	@Input() public newUser: boolean;
	@Output() public submitAction: EventEmitter<UserDetailFormValue> = new EventEmitter();
	@Output() public cancelAction: EventEmitter<void> = new EventEmitter();

	public adminRoleSelected = false;
	public addEditUserNoteEls$ = this.store.select(pageStore.getPageElementMulti('admin.user.add-edit.role-note'));

	/*
	 * Forms
	 */
	private requiredIfExistingUser = (control: AbstractControl) => this.newUser ? null : Validators.required(control);
	public userDetailForm: FormGroup = this.formBuilder.group({
		user: this.formBuilder.group({
			id: ['', this.requiredIfExistingUser],
			emailAddress: [{value: '', disabled: true}, [Validators.email, Validators.maxLength(100)]],
			username: [{value: '', disabled: true}, [Validators.required, Validators.email, Validators.maxLength(50)]],
			firstName: ['', [Validators.required, Validators.maxLength(50)]],
			lastName: ['', [Validators.required, Validators.maxLength(50)]],
			title: ['', [Validators.required, Validators.maxLength(100)]],
			companyName: ['', [Validators.required, Validators.maxLength(100)]],
			phoneNumber: ['', [Validators.required, Validators.pattern(/(.*[0-9].*){10,}/), Validators.maxLength(50)]],
		}),
		userAssignment: this.formBuilder.group({
			id: ['', this.requiredIfExistingUser],
			userId: ['', this.requiredIfExistingUser],
			userAssignmentResourceList: this.formBuilder.array([]),
			roleId: ['', Validators.required],
		}),
	});
	public formErrors: FormErrors<UserDetailFormValue> = new FormErrors();

	/*
	 * Requesters
	 */
	public roleListRequester: RequestLoader<PortalSecurityUserRole[]> = new RequestLoader({
		initiator: query => this.portalSecurityService.getUserRoleList({
			assignmentForPortalEntityId: this.userAssignment
				? this.userAssignment.portalEntity!.id
				: this.filterEntity.id,
		}),
		initialValue: null,
		subConfig: {comp: this},
	});
	public resourceListRequester: RequestLoader<PortalSecurityUserAssignmentResource[], number> = new RequestLoader({
		preModifier: obs$ => Observable.combineLatest(obs$, this.userDetailForm.get('userAssignment.roleId')!.valueChanges)
			.map(([obs, roleId]) => roleId),
		initiator: roleId => roleId
			? this.portalSecurityService.getUserAssignmentResourceList({
				roleId,
				...this.userAssignment
					? {userAssignmentId: this.userAssignment.id}
					: {portalEntityId: this.filterEntity.id},
			})
			: Observable.of([]),
		postModifier: obs$ => obs$.do(resourceList => this.setUserAssignmentResourceList(resourceList)),
		subConfig: {comp: this},
	});


	constructor(private formBuilder: FormBuilder,
	            private portalSecurityService: PortalSecurityService,
	            private store: Store<pageStore.PageStore>) {
		ManagedSubscription
			.of(this.userDetailForm.valueChanges,
				() => this.formErrors = FormUtils.getValidationErrors(this.userDetailForm, UserDetailFormComponent.VALIDATION_MESSAGES))
			.attach(this);
		// Update "administrator" flag on user role selection
		ManagedSubscription
			.of(Observable.combineLatest(this.roleListRequester.response$, this.userDetailForm.get('userAssignment.roleId')!.valueChanges)
				.map(([roleList, roleId]) => roleList.find(item => item.id === roleId)),
				selectedRole => this.adminRoleSelected = !!(selectedRole && selectedRole.portalSecurityAdministrator))
			.attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.resetUserAssignmentForm();
		this.resourceListRequester.load(this.userDetailForm.value.userAssignment.roleId);

		// TODO make this better.  It works for now.
		this.prepareForm(this.userDetailForm.get('user')!.get('firstName'));
		this.prepareForm(this.userDetailForm.get('user')!.get('lastName'));
		this.prepareForm(this.userDetailForm.get('user')!.get('title'));
		this.prepareForm(this.userDetailForm.get('user')!.get('companyName'));
		this.prepareForm(this.userDetailForm.get('user')!.get('phoneNumber'));
	}


	public ngOnDestroy(): void { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public evaluateCheckboxCascade(resource: PortalSecurityUserAssignmentResource): void {
		const resourceControlArray = this.userDetailForm.get('userAssignment.userAssignmentResourceList') as FormArray;
		if (resource.securityResource!.leaf) {
			// Child
			const parentControl: AbstractControl = this.getResourceControl(resource.securityResource!.parent!.id!, resourceControlArray);
			const siblingControls: AbstractControl[] = this.getResourceChildrenControlList(parentControl, resourceControlArray);
			// If any siblings are checked, check parent; otherwise uncheck parent
			const anySiblingsEnabled = !!siblingControls.find(control => !!control.value.accessAllowed);
			parentControl.patchValue({accessAllowed: anySiblingsEnabled});
		}
		else {
			// Parent
			const resourceControl: AbstractControl = this.getResourceControl(resource.securityResource!.id!, resourceControlArray);
			const children: AbstractControl[] = this.getResourceChildrenControlList(resourceControl, resourceControlArray);
			const enabled: boolean = resourceControl.value.accessAllowed;
			for (const child of children) {
				// Set all child toggles to the value of the parent control
				child.patchValue({accessAllowed: enabled});
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getTitle(name: string): string {
		const control = this.userDetailForm.get('user')!.get(name);
		if (control && control.disabled && (!this.newUser || this.existingSecurityUser)) {
			return 'You do not have the ability to edit another user\'s personal information after they have been set up.';
		}
		return '';
	}


	public getSecurityResourceCategories(resource: PortalSecurityUserAssignmentResource): string[] {
		// Ignore results if all match resource name
		const fileCategoryNames = (resource.fileCategoryList || []).map(category => category.name!);
		return fileCategoryNames.every(categoryName => categoryName === resource.securityResource!.name)
			? []
			: fileCategoryNames.sort();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private prepareForm(control: AbstractControl | null | undefined): void {
		if (control) {
			if (this.newUser && !this.existingSecurityUser) {
				control.enable();
			}
			else {
				control.disable();
			}
		}
	}


	private getResourceControl(resourceId: number, resourceControlArray: FormArray): AbstractControl {
		return resourceControlArray.controls.find((control: AbstractControl) => control.value.resourceId === resourceId)!;
	}


	private getResourceChildrenControlList(parentControl: AbstractControl, resourceControlArray: FormArray): AbstractControl[] {
		return resourceControlArray.controls.filter(control => control.value.resourceParentId === parentControl.value.resourceId);
	}


	private resetUserAssignmentForm(): void {
		this.userDetailForm.reset();

		if (this.userAssignment) {
			// Editing an existing user
			this.userDetailForm.patchValue({
				user: {
					id: this.userAssignment.securityUser!.id,
					emailAddress: this.userAssignment.securityUser!.emailAddress,
					username: this.userAssignment.securityUser!.username,
					firstName: this.userAssignment.securityUser!.firstName,
					lastName: this.userAssignment.securityUser!.lastName,
					title: this.userAssignment.securityUser!.title,
					companyName: this.userAssignment.securityUser!.companyName,
					phoneNumber: this.userAssignment.securityUser!.phoneNumber,
				},
				userAssignment: {
					id: this.userAssignment.id,
					userId: this.userAssignment.securityUser!.id,
					roleId: this.userAssignment.assignmentRole!.id,
				},
			});
			// TODO: Can we do this via patchValue?
			this.setUserAssignmentResourceList(this.userAssignment.userAssignmentResourceList!);
		}
		else {
			if (this.existingSecurityUser) {
				this.userDetailForm.patchValue({
					user: {
						id: this.existingSecurityUser.id,
						emailAddress: this.existingSecurityUser.emailAddress,
						username: this.existingSecurityUser.username,
						firstName: this.existingSecurityUser.firstName,
						lastName: this.existingSecurityUser.lastName,
						title: this.existingSecurityUser.title,
						companyName: this.existingSecurityUser.companyName,
						phoneNumber: this.existingSecurityUser.phoneNumber,
					},
				});
			}
			else {
				this.userDetailForm.patchValue({
					user: {username: this.newUsername},
				});
			}
		}
	}


	private setUserAssignmentResourceList(resourceList: PortalSecurityUserAssignmentResource[]): void {
		(this.userDetailForm.get('userAssignment') as FormGroup).setControl('userAssignmentResourceList', this.formBuilder.array(
			resourceList.map(assignmentResource => this.formBuilder.group({
				id: assignmentResource.id,
				resourceId: assignmentResource.securityResource!.id,
				resourceParentId: assignmentResource.securityResource!.parent ? assignmentResource.securityResource!.parent!.id : null,
				resourceLeaf: assignmentResource.securityResource!.leaf,
				accessAllowed: [{value: assignmentResource.accessAllowed || this.adminRoleSelected, disabled: this.adminRoleSelected}],
			})),
		));
	}
}
