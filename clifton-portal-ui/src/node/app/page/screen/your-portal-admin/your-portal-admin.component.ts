import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { TransformingObservable } from 'app/core/rxjs/observable/transforming-observable';
import * as pageStore from 'app/page/shared/state/page.store';
import { OrderBy } from 'app/portal/api/portal-order-by';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-your-portal-admin',
	templateUrl: './your-portal-admin.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class YourPortalAdminComponent {

	public currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.isSimulatingUser)
		.switchMap(isSimulating => this.store.select(isSimulating ? pageStore.getSimulatedUserInfo : pageStore.getUser));
	public userAssignmentSortSource: TransformingObservable<string, OrderBy | undefined> = new TransformingObservable(obs$ => obs$
		.scan((previous, current) => this.determineSortOrder(previous, current), undefined)
		// Since multiple subscribers will be used, the result of the scan operator (which tracks state) must be shared to prevent being run multiple times
		.shareReplay(),
		new BehaviorSubject('userFirstLastName'));
	public adminAssignmentRequester: RequestLoader<PortalSecurityUserAssignment[]> = new RequestLoader({
		preModifier: obs$ => obs$.mergeMapTo(Observable.combineLatest(this.store.select(pageStore.getSimulatedUserId), this.userAssignmentSortSource)),
		initiator: ([simulatedUserId, orderBy]: [number | undefined, OrderBy | undefined]) => simulatedUserId
			? this.portalSecurityService.getAdminUserAssignmentListForUser(simulatedUserId, orderBy)
			: this.portalSecurityService.getCurrentUserAdminUserAssignmentList(orderBy),
		initialValue: null,
	});

	public pageTitleEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));


	constructor(private store: Store<pageStore.PageStore>,
	            private portalSecurityService: PortalSecurityService) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private determineSortOrder(previous: OrderBy, current: string): OrderBy | undefined {
		// Swap direction if needed
		const prevOrderBy = Array.isArray(previous) ? previous[0] : void 0;
		const newDirection: 'ASC' | 'DESC' = (prevOrderBy && prevOrderBy.field === current && prevOrderBy.direction === 'ASC') ? 'DESC' : 'ASC';
		return current ? [{field: current, direction: newDirection}] : void 0;
	}
}
