import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';
import { OrderBy } from 'app/portal/api/portal-order-by';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';


@Component({
	selector: 'ppa-your-portal-admin-user-list',
	templateUrl: './your-portal-admin-user-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class YourPortalAdminUserListComponent {

	@Input() public username: string;
	@Input() public valueList: PortalSecurityUserAssignment[];
	@Input() public orderBy: OrderBy | undefined;
	@Output() public sortAction: EventEmitter<string> = new EventEmitter();

	public portalNameEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));


	public constructor(private store: Store<pageStore.PageStore>) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getEmailSubject(portalName: string): string {
		return `${portalName} / Portal Administrator Request - ${this.username}`;
	}
}
