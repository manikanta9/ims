import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { YourPortalAdminComponent } from 'app/page/screen/your-portal-admin/your-portal-admin.component';


const routes: Routes = [
	{
		path: '',
		component: YourPortalAdminComponent,
	},
];


@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: [],
})
export class YourPortalAdminRoutingModule {
}
