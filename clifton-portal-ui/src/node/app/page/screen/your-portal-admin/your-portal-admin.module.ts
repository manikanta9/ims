import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from 'app/core/core.module';
import { YourPortalAdminUserListComponent } from 'app/page/screen/your-portal-admin/components/your-portal-admin-user-list.component';
import { YourPortalAdminRoutingModule } from 'app/page/screen/your-portal-admin/your-portal-admin-routing.module';
import { YourPortalAdminComponent } from 'app/page/screen/your-portal-admin/your-portal-admin.component';
import { ComponentModule } from 'app/page/shared/component/component.module';


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		CoreModule,
		ComponentModule,

		// Page modules
		YourPortalAdminRoutingModule,
	],
	declarations: [
		YourPortalAdminComponent,
		YourPortalAdminUserListComponent,
	],
	providers: [],
})
export class YourPortalAdminModule {
}
