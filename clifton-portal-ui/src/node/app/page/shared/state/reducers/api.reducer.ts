import { ActionReducer } from '@ngrx/store';
import { ResponseResult } from 'app/core/api/response-result';
import { ResponseStatus } from 'app/core/api/response-status';
import { TypeUtils } from 'app/core/util/type.utils';
import * as api from 'app/page/shared/state/reducers/api.actions';
import * as _ from 'lodash';


export * from 'app/page/shared/state/reducers/api.actions';


// State
export const storeName = 'api';


export interface ApiState {
	[field: string]: ResponseResult<any>;
}


export const initialState: ApiState = {};


// Reducers
export const apiReducer: ActionReducer<ApiState> = (state: ApiState = initialState, action: api.Actions) => {
	switch (action.type) {
		case api.LOAD_START:
			return {...state, [action.payload.field]: {status: ResponseStatus.IN_PROCESS}};
		case api.LOAD_CANCEL:
			return {...state, [action.payload.field]: {status: ResponseStatus.CANCELLED}};
		case api.LOAD_FAIL:
			return {...state, [action.payload.field]: {status: ResponseStatus.FAILED, message: action.payload.message}};
		case api.LOAD_COMPLETE:
			const result = action.payload.result;
			const status = _.isEmpty(result) ? ResponseStatus.NO_RESULT : ResponseStatus.COMPLETED;
			return {...state, [action.payload.field]: {status, result}};
		case api.LOAD_RESET:
			return {...state, [action.payload.field]: {status: ResponseStatus.NOT_STARTED}};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export function getField(field: string): (state: ApiState) => any {
	return (state: ApiState) => state[field];
}
