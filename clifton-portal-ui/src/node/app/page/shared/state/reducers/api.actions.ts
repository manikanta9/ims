// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';


// Action names
export const LOAD_START = '[API] Load Start';
export const LOAD_CANCEL = '[API] Load Cancel';
export const LOAD_FAIL = '[API] Load Fail';
export const LOAD_COMPLETE = '[API] Load Complete';
export const LOAD_RESET = '[API] Load Reset';


// Action types
export class LoadStartAction implements Action {
	public readonly type = LOAD_START;


	constructor(public payload: { field: string, retriever: () => Observable<any> }) { }
}


export class LoadCancelAction implements Action {
	public readonly type = LOAD_CANCEL;


	constructor(public payload: { field: string }) { }
}


export class LoadFailAction implements Action {
	public readonly type = LOAD_FAIL;


	constructor(public payload: { field: string, message: string }) { }
}


export class LoadCompleteAction implements Action {
	public readonly type = LOAD_COMPLETE;


	constructor(public payload: { field: string, result: any }) { }
}


export class LoadResetAction implements Action {
	public readonly type = LOAD_RESET;


	constructor(public payload: { field: string }) { }
}


// Action type union
export type Actions =
	LoadStartAction
	| LoadCancelAction
	| LoadFailAction
	| LoadCompleteAction
	| LoadResetAction;
