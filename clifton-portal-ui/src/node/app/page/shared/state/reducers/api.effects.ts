import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import * as api from 'app/page/shared/state/reducers/api.actions';
import { Observable } from 'rxjs/Observable';
import { GroupedObservable } from 'rxjs/operator/groupBy';


@Injectable()
export class ApiEffects {

	constructor(private actions$: Actions) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Effect() public loadStart$: Observable<Action> = this.actions$.ofType(api.LOAD_START)
		// Split into observables by field
		.groupBy((action: api.LoadStartAction) => action.payload.field)
		.mergeMap((fieldAction: GroupedObservable<string, api.LoadStartAction>) =>
			// Start a request independent to the field
			fieldAction.switchMap((action: api.LoadStartAction) => action.payload.retriever())
				// Map the result to a complete action
				.map((result: any) => new api.LoadCompleteAction({field: fieldAction.key, result}))
				// Map errors to the failure action
				.catch((err: any) => Observable.of(new api.LoadFailAction({field: fieldAction.key, message: err as string})))
				// If a cancellation event is triggered, cancel the request
				.takeUntil(this.actions$.ofType(api.LOAD_CANCEL, api.LOAD_RESET)
					.filter((action: api.LoadCancelAction | api.LoadResetAction) => action.payload.field === fieldAction.key)));
}
