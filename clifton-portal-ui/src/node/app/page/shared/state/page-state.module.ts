import { NgModule } from '@angular/core';
import { StateService } from 'app/core/state/state.service';
import * as pageStore from 'app/page/shared/state/page.store';


@NgModule({
	imports: [],
	declarations: [],
})
export class PageStateModule {
	constructor(stateService: StateService) {
		stateService.addReducers(pageStore.pageReducerMap);
	}
}
