import * as portalStore from 'app/portal/state/portal.store';


export * from 'app/portal/state/portal.store';


// State
/* tslint:disable-next-line:no-empty-interface : Ignore empty interface until this is populated */
export interface PageStore extends portalStore.PortalStore {
}


// Map of reducer per store slice
export const pageReducerMap: portalStore.ReducerMap<PageStore> = {};


// TODO: Set initial state


// Memoized selectors
