import { Component, ElementRef, Input, QueryList, ViewChildren } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormUtils } from 'app/core/util/form.utils';
import { PortalFileListComponent } from 'app/page/shared/component/portal-file-list/portal-file-list.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalEmail } from 'app/portal/file/portal-email';
import { PortalFileCategoryEmailService } from 'app/portal/file/portal-file-category-email.service';
import { PortalFileHierarchy, PortalFileSubCategory } from 'app/portal/file/portal-file-hierarchy';
import * as _ from 'lodash';


@Component({
	selector: 'ppa-portal-file-summary-list',
	templateUrl: './portal-file-summary-list.component.html',
})
// TODO: Abstract this further into SummaryListComponent in core?
export class PortalFileSummaryListComponent {

	@Input() public username: string;
	@Input() public portalFileHierarchyList: PortalFileHierarchy[];
	@Input() public numSummaryColumns: number;
	@ViewChildren('reportSelector') public reportSelectorList: QueryList<ElementRef>;

	public popupEmailAddressEl$ = this.store.select(pageStore.getPageElementSingle('file.email.address'));
	public popupEmailMessageEl$ = this.store.select(pageStore.getPageElementSingle('file.email.message'));


	constructor(private portalFileDownloadService: PortalFileDownloadService,
	            private portalFileCategoryEmailService: PortalFileCategoryEmailService,
	            private store: Store<pageStore.PageStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public downloadFile(id: number, assignedPortalEntityId?: number): void {
		this.portalFileDownloadService.downloadFile({portalFileId: id, assignedPortalEntityId});
	}


	public getFileTypeIcon(fileType: string): string {
		return PortalFileListComponent.getFileTypeIcon(fileType);
	}


	public getSelectedIds(zipFiles: boolean): string[] {
		const result: any[] = [];
		this.portalFileHierarchyList.forEach(header =>
			header.categoryList.forEach(category =>
				category.subCategoryList.forEach(subCategory =>
					result.push(subCategory.fileList.filter(row => row.selected && ((!zipFiles && row.file.documentFileType === 'pdf') || zipFiles)).map(row => row.file.portalFileId)))));
		return _.flatten(result);
	}


	public getSelectedFileData(zipFiles: boolean): any[] {
		const result: any[] = [];

		let selectedRows: any[] = [];
		let validFileIds: any[] = [];
		this.portalFileHierarchyList.forEach(header =>
			header.categoryList.forEach(category =>
				category.subCategoryList.forEach(subCategory => {
					const selectedFileList = subCategory.fileList.filter(row => row.selected);
					validFileIds.push(selectedFileList.filter(row => (!zipFiles && row.file.documentFileType === 'pdf') || zipFiles).map(row => row.file.portalFileId));
					selectedRows.push(selectedFileList);
				})));

		selectedRows = _.flatten(selectedRows);
		validFileIds = _.flatten(validFileIds);

		result[0] = !zipFiles && validFileIds.length > 0;
		result[1] = selectedRows.length - validFileIds.length;
		result[2] = validFileIds;
		return result;
	}


	public popupEmail(subCategory: any, emailAddress?: string): void {
		if (subCategory.fileList && subCategory.fileList.length > 0) {
			const file = subCategory.fileList[0].file;
			if (file && file.fileCategory) {
				const entityId = file.assignedPortalEntity ? file.assignedPortalEntity.id : null;
				const categoryId = file.fileCategory.id;
				if (categoryId) {
					this.portalFileCategoryEmailService.get({
						categoryId,
						portalEntityId: entityId,
					}).then(([firstEmail]) => {
						const email: PortalEmail = emailAddress
							? {...firstEmail, toAddress: emailAddress} // Replace addressee with pre-specified value
							: firstEmail;
						window.location.href = this.buildMailto(email);
					});
				}
			}
		}
	}


	public getEmailTitle(subCategory: any): string {
		if (subCategory && subCategory.fileList.length > 0) {
			const file = subCategory.fileList[0].file;
			const categoryName = file.fileCategoryNameExpandedWithoutRoot!.split(' / ')[0];
			return `Email ${categoryName} Team`;
		}
		return '';
	}


	public onSelectAllChanged(eventValue: boolean, subCategory: PortalFileSubCategory): void {
		subCategory.fileList.forEach(file => {
			file.selected = eventValue;
		});
	}


	public checkIfAllSelected(subCategory: PortalFileSubCategory): void {
		subCategory.selected = subCategory.fileList.every(file => file.selected);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private buildMailto(email: PortalEmail): string {
		const toAddress = email.toAddress;
		const params = {
			cc: email.ccAddress,
			subject: `${email.subject} / ${this.username}`,
		};
		return `mailto:${toAddress}?${FormUtils.serialize(params)}`;
	}
}
