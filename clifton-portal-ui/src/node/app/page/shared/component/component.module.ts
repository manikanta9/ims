import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from 'app/core/core.module';
import { MessageBannerComponent } from 'app/page/shared/component/message-banner/message-banner.component';
import { PortalFileListComponent } from 'app/page/shared/component/portal-file-list/portal-file-list.component';
import { PortalFileSummaryListComponent } from 'app/page/shared/component/portal-file-summary-list/portal-file-summary-list.component';


const INTERNAL_DIRECTIVES: Type<any>[] = [];
const EXPORTED_DIRECTIVES: Type<any>[] = [
	MessageBannerComponent,
	PortalFileListComponent,
	PortalFileSummaryListComponent,
];


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		CoreModule,
		FormsModule,
		ReactiveFormsModule,
	],
	declarations: [
		...INTERNAL_DIRECTIVES,
		...EXPORTED_DIRECTIVES,
	],
	exports: [
		...EXPORTED_DIRECTIVES,
	],
})
export class ComponentModule {}
