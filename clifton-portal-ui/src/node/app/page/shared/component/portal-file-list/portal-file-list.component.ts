import { Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { BasePaginatedEntity } from 'app/core/api/base-paginated-entity';
import { RequestLoader } from 'app/core/api/request-loader';
import { Column } from 'app/core/component/table/column';
import { TableComponent } from 'app/core/component/table/table.component';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { PortalFileSummaryListComponent } from 'app/page/shared/component/portal-file-summary-list/portal-file-summary-list.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalPaginatedQuery } from 'app/portal/api/portal-paginated-query';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileHierarchy } from 'app/portal/file/portal-file-hierarchy';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { ApiUtils } from 'app/portal/util/api.utils';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-portal-file-list',
	templateUrl: './portal-file-list.component.html',
})
export class PortalFileListComponent implements OnInit, OnDestroy, OnChanges {

	@Input() public columnList: Column[];
	@Input() public query: PortalFileQuery;
	// TODO: Move validation from message component to this list component (or potentially source component, i.e. reporting)
	@Input() public validationMessage: string;
	@Input() public useDetailView = false;
	@Input() public numSummaryColumns = 2;
	public errorMessage?: string | null;

	@ViewChild('messageElement') public messageElement: ElementRef;
	@ViewChild('tableElement') public tableElement: TableComponent;
	@ViewChild('summaryElement') public summaryElement: PortalFileSummaryListComponent;

	public showError = true;
	public loadingMore = false;

	// Observers
	public fileDetailList$: Subject<PortalFile[]> = new ReplaySubject<PortalFile[]>(1);
	public fileSummaryList$: Subject<PortalFileHierarchy[]> = new ReplaySubject<PortalFileHierarchy[]>(1);

	// Observables
	public currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.isSimulatingUser)
		.switchMap(isSimulating => this.store.select(isSimulating ? pageStore.getSimulatedUserInfo : pageStore.getUser));
	public fileRequester: RequestLoader<BasePaginatedEntity<PortalFile> | PortalFile[]> = new RequestLoader({
		preModifier: obs$ => obs$
			.do((loadMore: boolean) => this.loadingMore = loadMore)
			.do(() => this.errorMessage = null),
		initiator: () => this.loadResults(),
	});


	// Subscriptions
	private fileRequestSub: Subscription;

	private start: number;
	public total: number;
	private listSize: number;
	public growingList: PortalFile[];


	constructor(private store: Store<pageStore.PageStore>,
	            private portalFileService: PortalFileService,
	            private portalFileDownloadService: PortalFileDownloadService,
	            private modalDialogService: ModalDialogService) {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.fileRequestSub = this.fileRequester.response$.subscribe(fileOrPagedList => {
			this.handleResults(fileOrPagedList);
			if (fileOrPagedList) {
				this.fileSummaryList$.next(this.portalFileService.getHierarchy(fileOrPagedList));
				this.fileDetailList$.next(this.growingList);
			}
		});
	}


	public ngOnDestroy(): void {
		this.fileRequestSub.unsubscribe();
	}


	public ngOnChanges(changes: SimpleChanges): void {
		for (const property of Object.keys(changes) as (keyof PortalFileListComponent)[]) {
			const value: any = changes[property].currentValue;
			switch (property) {
				case 'query':
					this.onQueryChange(value);
					break;
				case 'validationMessage':
					// this.onValidationMessageChange(changes[property].currentValue);
					break;
				case 'useDetailView':
					this.onDetailViewChange(value);
					break;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public static getFileTypeIcon(fileType: string): string {
		switch (fileType) {
			case 'doc':
			case 'docx':
				return 'assets/icons/word.png';
			case 'xls':
			case 'xlsx':
				return 'assets/icons/excel.png';
			case 'pdf':
				return 'assets/icons/pdf.png';
			case 'ppt':
			case 'pptx':
				return 'assets/icons/powerpoint.png';
			case 'msg':
				return 'assets/icons/email.png';
			case 'png':
			case 'gif':
			case 'jpg':
				return 'assets/icons/image.png';
			case 'xml':
				return 'assets/icons/xml.png';
			case 'avi':
			case 'flv':
			case 'mp4':
			case 'mpeg':
			case 'mpg':
			case 'wmv':
				return 'assets/icons/video.png';
			case 'mp3':
			case 'ogg':
			case 'wav':
			case 'wma':
				return 'assets/icons/music.png';
			default:
				return 'assets/icons/file.png';
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public downloadFileList(zipFiles: boolean): void {
		let fileIdList: string[] = [];

		// fields used to determine message to user if they want to export PDFs and non-PDF files are selected
		let isPdfSelected = false;
		let nonPdfFileCount = 0;
		if (this.useDetailView) {
			let selectedFiles: PortalFile[] = this.tableElement.getSelected() as PortalFile[];
			if (!zipFiles) {
				// only PDFs are allowed
				const originalSize = selectedFiles.length;
				selectedFiles = _.filter(selectedFiles, file => file.documentFileType === 'pdf');
				nonPdfFileCount = originalSize - selectedFiles.length;
				isPdfSelected = selectedFiles.length > 0;
			}
			fileIdList = _.map(selectedFiles, file => `${file.portalFileId}`);
		}
		else {
			const selectedFileData: any[] = this.summaryElement.getSelectedFileData(zipFiles);
			isPdfSelected = selectedFileData[0];
			nonPdfFileCount = selectedFileData[1];
			fileIdList = selectedFileData[2];
		}

		const message = this.getFileAlertMessage(zipFiles, nonPdfFileCount, isPdfSelected);
		if (message) {
			const modal = this.modalDialogService.showMessage(message, 'Export to PDF');
			modal.result
				.catch((dismissReason: any) => false)  /* Swallow modal dismissal */
				.then(val => {
					if (isPdfSelected && val && val === 'OK') {
						this.doDownload(fileIdList, zipFiles);
					}
				});
		}
		else {
			this.doDownload(fileIdList, zipFiles);
		}
	}


	private getFileAlertMessage(zipFiles: boolean, nonPdfFileCount: number, isPdfSelected: boolean): string | undefined {
		let message;
		if (!zipFiles) {
			const beginMessage = 'Only PDF files can be exported using this option. <br/>';
			if (!isPdfSelected) {
				message = beginMessage.concat('Please select a PDF file to continue.');
			}
			else if (nonPdfFileCount === 1) {
				message = beginMessage.concat(`${nonPdfFileCount} file is not a PDF file and will not be exported.`);
			}
			else if (nonPdfFileCount > 1) {
				message = beginMessage.concat(`${nonPdfFileCount} files are not PDF files and will not be exported.`);
			}
		}
		return message;
	}


	private doDownload(fileIdList: string[], zipFiles: boolean): void {
		this.portalFileDownloadService.downloadFileList({
			portalFileIds: fileIdList.join(','),
			zipFiles,
			assignedPortalEntityId: this.query.assignedPortalEntityId,
		});
	}


	public onLoadMoreClick(): void {
		this.fileRequester.load(true);
	}


	// if we are displaying all the items there are we need to hide the Load More button
	public hideLoadMore(listSize: number): boolean {
		return (listSize >= this.total);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private async onQueryChange(query: PortalFileQuery): Promise<void> {
		// reset 'Select All' checkbox logic
		if (this.tableElement) {
			this.tableElement.selectedAll = false;
		}

		// reset 'Load More' logic
		this.start = 0;
		this.total = 0;
		this.listSize = 0;
		this.growingList = [];

		this.fileRequester.load(false);
	}


	private onValidationMessageChange(message: string): void {
		// LogUtils.log('on message change', message);
		// if (!message) {
		// 	return;
		// }
		// this._validationMessage = message;
		// this.showError = true;
	}


	private onDetailViewChange(value: boolean): void {
		// this.loadResults();
		/*
		 * TODO:
		 * Temporary workaround to exception triggered on switching from the detail to the summary view. Instead of clearing, we should make sure that the query
		 * gets triggered properly before re-rendering the view. We should also make sure that we're rendering the correct data based on the view selected --
		 * e.g., we should not be storing "summary" data when we're making a "detail" query.
		 */
		this.fileSummaryList$.next([]);
		this.fileDetailList$.next([]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private loadResults(): Observable<BasePaginatedEntity<PortalFile> | PortalFile[]> {
		if (this.validationMessage || !this.query) {
			return Observable.of({start: 0, total: 0, rows: []});
		}

		let startIndex = 0;
		if (this.start >= 0 && this.total >= 0 && this.listSize >= 0) {
			startIndex = this.start + this.listSize;
		}

		const query: PortalPaginatedQuery<PortalFileQuery> | PortalFileQuery = this.getConfiguredQuery(startIndex);
		return ApiUtils.isPaginatedQuery(query) ? this.portalFileService.getPaginated(query) : this.portalFileService.get2(query);
	}


	private handleResults(fileOrPagedList: BasePaginatedEntity<PortalFile> | PortalFile[]): void {
		if (ApiUtils.isPaginatedEntity(fileOrPagedList)) {
			const pagedList: BasePaginatedEntity<PortalFile> = fileOrPagedList;
			if (!pagedList.rows || pagedList.rows.length < 1) {
				this.errorMessage = 'No results were found for the selected filters';
			}
			this.start = pagedList.start;
			this.total = pagedList.total;
			this.listSize = pagedList.rows.length;
			this.growingList = (this.growingList || []).concat(...pagedList.rows);
		}
		else if (!fileOrPagedList || fileOrPagedList.length < 1) {
			this.errorMessage = 'No results were found for the selected filters';
		}
	}


	private getConfiguredQuery(start?: number): PortalPaginatedQuery<PortalFileQuery> | PortalFileQuery {
		return {
			...this.query,
			...this.useDetailView ? {
				// Detail view
				start: start || 0,
				limit: 25,
				orderBy: [
					{field: 'reportDate', direction: 'DESC'},
					{field: 'portalFileId', direction: 'DESC'},
				],
				approved: true,
				requestedProperties: [
					'portalFileId', 'assignedPortalEntity.id', 'assignedPortalEntity.label', 'assignedPortalEntity.parentPortalEntity.label',
					'displayName', 'displayText', 'documentFileType', 'fileCategory.description',
					// Detail-dependent properties
					'fileCategory.label', 'reportDateFormatted',
					// TODO: Temporary workaround for race condition. The better solution for this will be to process detail and summary requests separately!
					'fileCategoryNameExpandedWithoutRoot', 'serviceName', 'fileCategory.id',
				],
			} : {
				// Summary view
				defaultDisplay: true,
				orderBy: [
					{field: 'assignedPortalEntityLabel', direction: 'ASC'},
					{field: 'fileCategoryOrder', direction: 'ASC'},
					{field: 'reportDate', direction: 'DESC'},
				],
				requestedProperties: [
					'portalFileId', 'assignedPortalEntity.id', 'assignedPortalEntity.label', 'assignedPortalEntity.parentPortalEntity.label', 'assignedPortalEntity.entityViewType.name',
					'displayName', 'displayText', 'documentFileType', 'fileCategory.description', 'entityViewType.name',
					// Summary-dependent properties
					'fileCategoryNameExpandedWithoutRoot', 'serviceName', 'fileCategory.id',
				],
			},
		};
	}
}
