import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.actions';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalNotification } from 'app/portal/notification/portal-notification';
import { PortalNotificationService } from 'app/portal/notification/portal-notification.service';
import { SetNotificationAcknowledgementsAction } from 'app/portal/state/reducers/notification-acknowledgements.actions';
import { Observable } from 'rxjs/Observable';


/**
 * Retrieves popups for acknowledgement before activating the guarded route.
 */
@Injectable()
export class NotificationAcknowledgementGuard implements CanActivate {

	private notificationPopup$: Observable<PortalNotification[] | undefined> = this.store.select(pageStore.getAllNotificationAcknowledgements);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalNotificationService: PortalNotificationService) { }


	public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return this.updateNotifications().map((portalNotifications: PortalNotification[]) => {
			const pending: boolean = portalNotifications.length > 0;
			if (pending) {
				this.store.dispatch(router.go(['notification-acknowledgement'], {returnUrl: state.url}));
			}
			return !pending;
		});
	}


	private updateNotifications(): Observable<PortalNotification[] | undefined> {
		return this.notificationPopup$
			.switchMap(elements => elements == null
				? this.portalNotificationService.getNotificationPopUpList()
					.do(portalNotifications => this.store.dispatch(new SetNotificationAcknowledgementsAction(portalNotifications)))
				: Observable.of(elements))
			.first();
	}
}
