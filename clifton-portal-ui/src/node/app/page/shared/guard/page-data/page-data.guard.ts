import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalPageElementService } from 'app/portal/page/portal-page-element.service';
import { PageElementHolder, SetPageElementsAction } from 'app/portal/state/reducers/page-element.reducer';
import { Observable } from 'rxjs/Observable';


/**
 * Retrieves relevant general page data before activating the guarded route.
 */
@Injectable()
export class PageDataGuard implements CanActivate {

	private pageElements$: Observable<PageElementHolder | undefined> = this.store.select(pageStore.getAllPageElements);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalPageElementService: PortalPageElementService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.updatePageData()
			.mapTo(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks for all user data elements and updates those which are not up to date.
	 */
	private updatePageData(): Observable<any> {
		return this.pageElements$
			.switchMap(elements => elements == null
				? this.portalPageElementService.getPageElementList().do(newElements => this.store.dispatch(new SetPageElementsAction(newElements)))
				: Observable.of(void 0))
			.first();
	}
}
