import { NgModule } from '@angular/core';
import { AuthGuard } from 'app/page/shared/guard/auth/auth.guard';
import { NotAuthGuard } from 'app/page/shared/guard/auth/not-auth.guard';
import { ChangePasswordGuard } from 'app/page/shared/guard/change-password/change-password.guard';
import { NotChangePasswordGuard } from 'app/page/shared/guard/change-password/not-change-password.guard';
import { NotNotificationAcknowledgementGuard } from 'app/page/shared/guard/notification-acknowledgement-guard/not-notification-acknowledgement-guard';
import { NotificationAcknowledgementGuard } from 'app/page/shared/guard/notification-acknowledgement-guard/notification-acknowledgement-guard';
import { PageDataGuard } from 'app/page/shared/guard/page-data/page-data.guard';
import { NotTermsOfUseGuard } from 'app/page/shared/guard/terms-of-use/not-terms-of-use.guard';
import { TermsOfUseGuard } from 'app/page/shared/guard/terms-of-use/terms-of-use.guard';
import { UserDataGuard } from 'app/page/shared/guard/user-data/user-data.guard';


@NgModule({
	imports: [],
	declarations: [],
	exports: [],
	providers: [
		AuthGuard,
		NotAuthGuard,
		TermsOfUseGuard,
		NotTermsOfUseGuard,
		ChangePasswordGuard,
		NotChangePasswordGuard,
		NotificationAcknowledgementGuard,
		NotNotificationAcknowledgementGuard,
		PageDataGuard,
		UserDataGuard,
	],
})
export class GuardModule {}
