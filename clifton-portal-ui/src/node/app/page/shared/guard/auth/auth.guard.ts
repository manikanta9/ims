import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.reducer';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import * as user from 'app/portal/state/reducers/user.reducer';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AuthGuard implements CanActivate {

	private currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalSecurityService: PortalSecurityService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		// Get stored user
		return this.currentUser$.first()
			// Fall-back to checking for valid cookie
			.switchMap((currentUser?: PortalSecurityUser) => (currentUser != null) ? Observable.of(currentUser) : this.getCurrentUser())
			// Handle results
			.map((currentUser?: PortalSecurityUser) => this.handleRedirect(currentUser, state));
	}


	private getCurrentUser(): Observable<PortalSecurityUser | undefined> {
		return this.portalSecurityService.getCurrentUser(false)
			.do(currentUser => this.store.dispatch(new user.LoginAction(currentUser)))
			.catch(error => Observable.of(void 0));
	}


	private handleRedirect(currentUser: PortalSecurityUser | undefined, state: RouterStateSnapshot): boolean {
		const needsLogin = currentUser == null;
		if (needsLogin) {
			this.store.dispatch(router.go(['login'], {returnUrl: state.url}));
		}
		return !needsLogin;
	}
}
