import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.actions';
import { UrlUtils } from 'app/core/util/url.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import * as user from 'app/portal/state/reducers/user.actions';
import { Observable } from 'rxjs/Observable';


@Injectable()
// TODO: Can we get rid of this and just apply the route change to the login page if needed?
export class NotAuthGuard implements CanActivate {

	private currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);


	constructor(private store: Store<pageStore.PageStore>,
				private portalSecurityService: PortalSecurityService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		// Get stored user
		return this.currentUser$.take(1)
			// Fall-back to checking for valid cookie
			.mergeMap((currentUser?: PortalSecurityUser) => (currentUser != null) ? Observable.of(currentUser) : this.getCurrentUser())
			// Handle results
			.map((currentUser?: PortalSecurityUser) => this.handleRedirect(currentUser, next));
	}


	private getCurrentUser(): Observable<PortalSecurityUser | undefined> {
		return this.portalSecurityService.getCurrentUser(false)
			.do(currentUser => this.store.dispatch(new user.LoginAction(currentUser)))
			.catch(error => Observable.of(void 0));
	}


	private handleRedirect(currentUser: PortalSecurityUser | undefined, next: ActivatedRouteSnapshot): boolean {
		const hasLogin = currentUser != null;
		if (hasLogin) {
			const returnUrl = decodeURIComponent(next.queryParamMap.get('returnUrl') || '/');
			const returnUrlInfo = UrlUtils.getUrlInfo(returnUrl);
			this.store.dispatch(router.replace(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
		}
		return !hasLogin;
	}
}
