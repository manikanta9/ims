import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.actions';
import { UrlUtils } from 'app/core/util/url.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class NotChangePasswordGuard implements CanActivate {

	private currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);


	constructor(private store: Store<pageStore.PageStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.currentUser$
			.take(1)
			.map((currentUser: PortalSecurityUser) => this.handleRedirect(currentUser, next));
	}


	private handleRedirect(currentUser: PortalSecurityUser, next: ActivatedRouteSnapshot): boolean {
		const mustChangePassword = currentUser.passwordResetRequired == null ? false : currentUser.passwordResetRequired;
		if (!mustChangePassword) {
			const returnUrl = decodeURIComponent(next.queryParamMap.get('returnUrl') || '/');
			const returnUrlInfo = UrlUtils.getUrlInfo(returnUrl);
			this.store.dispatch(router.replace(returnUrlInfo.pathname || '/', returnUrlInfo.queryParams));
		}
		return mustChangePassword;
	}
}
