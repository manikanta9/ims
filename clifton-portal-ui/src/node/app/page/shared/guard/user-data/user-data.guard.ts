import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { PortalPageElementService } from 'app/portal/page/portal-page-element.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { SetPageElementsAction } from 'app/portal/state/reducers/page-element.actions';
import * as user from 'app/portal/state/reducers/user.reducer';
import { Observable } from 'rxjs/Observable';


/**
 * Retrieves relevant user data before activating the guarded route.
 */
@Injectable()
export class UserDataGuard implements CanActivate {

	private accessGroups$: Observable<string[] | undefined> = this.store.select(pageStore.getAccessGroups);
	private isAdmin$: Observable<boolean | undefined> = this.store.select(pageStore.isAdmin);

	private isSimulatingUser$: Observable<boolean> = this.store.select(pageStore.isSimulatingUser);
	private simulatedUserInfo$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getSimulatedUserInfo);
	private simulatedUserIsAdmin$: Observable<boolean | undefined> = this.store.select(pageStore.getSimulatedUserIsAdmin);
	private simulatedUserAccessGroups$: Observable<string[] | undefined> = this.store.select(pageStore.getSimulatedUserAccessGroups);

	private isSimulatingEntity$: Observable<boolean> = this.store.select(pageStore.isSimulatingEntity);
	private simulatedEntityInfo$: Observable<PortalEntity | undefined> = this.store.select(pageStore.getSimulatedEntityInfo);
	private simulatedEntityAccessGroups$: Observable<string[] | undefined> = this.store.select(pageStore.getSimulatedEntityAccessGroups);


	constructor(private store: Store<pageStore.PageStore>,
	            private portalFileService: PortalFileService,
	            private portalSecurityService: PortalSecurityService,
	            private portalPageElementService: PortalPageElementService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.updateUserData()
			.mapTo(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks for all user data elements and updates those which are not up to date.
	 */
	private updateUserData(): Observable<any> {
		return Observable.forkJoin(
			this.accessGroups$.switchMap(accessGroups => accessGroups == null ? this.applyAccessGroups() : Observable.of(void 0)).first(),
			this.isAdmin$.switchMap(isAdmin => isAdmin == null ? this.applyAdminRights() : Observable.of(void 0)).first(),
			this.isSimulatingUser$.switchMap(isSimulatingUser => isSimulatingUser ? this.awaitSimulatedUserInfo() : Observable.of(void 0)).first(),
			this.isSimulatingEntity$.switchMap(isSimulatingEntity => isSimulatingEntity ? this.awaitSimulatedEntityInfo() : Observable.of(void 0)).first(),
			this.portalPageElementService.getPageElementList().first().do(elements => this.store.dispatch(new SetPageElementsAction(elements))),
		);
	}


	/**
	 * Returns an observable that waits until details for the given simulated user have been retrieved to complete.
	 */
	private awaitSimulatedUserInfo(): Observable<any> {
		return Observable.forkJoin(
			this.simulatedUserInfo$.nonNull().first(),
			this.simulatedUserIsAdmin$.nonNull().first(),
			this.simulatedUserAccessGroups$.nonNull().first(),
		);
	}


	/**
	 * Returns an observable that waits until details for the given simulated entity have been retrieved to complete.
	 */
	private awaitSimulatedEntityInfo(): Observable<any> {
		return Observable.forkJoin(
			this.simulatedEntityInfo$.nonNull().first(),
			this.simulatedEntityAccessGroups$.nonNull().first(),
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an observable which retrieves and updates the current user's access groups on subscription.
	 */
	private applyAccessGroups(): Observable<any> {
		return this.portalFileService.getFileCategoryList({
				requestedProperties: 'name',
				rootOnly: true,
				viewAsUserId: '', // Exclude "View As" parameter from request to get current user value
				viewAsEntityId: '', // Exclude "View As" parameter from request to get current user value
			})
			.map((fileCategoryList: PortalFileCategory[]) => fileCategoryList.map(fileCategory => fileCategory.name!))
			.do((accessGroups: string[]) => this.store.dispatch(new user.SetAccessGroupsAction(accessGroups)));
	}


	private applyAdminRights(): Observable<any> {
		return this.portalSecurityService.getCurrentUserIsAdmin()
			.do((isAdmin: boolean) => this.store.dispatch(new user.SetAdministratorAction(isAdmin)));
	}
}
