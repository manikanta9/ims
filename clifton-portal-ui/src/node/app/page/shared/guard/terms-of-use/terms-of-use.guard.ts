import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import * as router from 'app/core/state/reducers/router.actions';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class TermsOfUseGuard implements CanActivate {

	private currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);


	constructor(private store: Store<pageStore.PageStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.currentUser$
			.take(1)
			.map((currentUser: PortalSecurityUser) => this.handleRedirect(currentUser, state));
	}


	private handleRedirect(currentUser: PortalSecurityUser, state: RouterStateSnapshot): boolean {
		const needsTermsAcceptance = currentUser.termsOfUseAcceptanceRequired;
		if (needsTermsAcceptance) {
			this.store.dispatch(router.go(['terms-of-use'], {returnUrl: state.url}));
		}
		return !needsTermsAcceptance;
	}
}
