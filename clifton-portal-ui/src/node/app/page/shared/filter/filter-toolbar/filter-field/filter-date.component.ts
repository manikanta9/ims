import { ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { NgbDateParserFormatter, NgbDateStruct, NgbInputDatepicker, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { FieldValidationMessages } from 'app/core/form/field-validation-messages';
import { TransformingObservable } from 'app/core/rxjs/observable/transforming-observable';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { DateUtils } from 'app/core/util/date.utils';
import { FormUtils } from 'app/core/util/form.utils';
import { noop } from 'app/core/util/function.utils';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-filter-date',
	templateUrl: './filter-date.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: FilterDateComponent,
		multi: true,
	}],
})
// TODO: This is a generic component -- move this out of the filter package
export class FilterDateComponent implements ControlValueAccessor, OnInit, OnDestroy {

	private readonly validationMessages: FieldValidationMessages = {
		ngbDate: (errorObj: any) => Object.keys(errorObj).map(errorType => {
			const errorDetail: any = errorObj[errorType];
			switch (errorType) {
				case 'requiredBefore':
					return `The date must be on or after ${this.dateParserFormatter.format(errorDetail)}.`;
				case 'requiredAfter':
					return `The date must be on or before ${this.dateParserFormatter.format(errorDetail)}.`;
				case 'invalid':
				default:
					return 'Please enter a valid date in the format MM/DD/YYYY.';
			}
		}).join('<br/>'),
		other: 'Invalid date.',
	};

	@Input() public enabled: boolean;
	// TODO: Remove label and let referrers handle labelling
	@Input() public label: string;
	@Input() public labelStyle?: { [key: string]: string };
	@Input() public minDate?: Date;
	@Input() public maxDate?: Date;
	@ViewChild(NgModel) private inputModel: NgModel;
	@ViewChild('inputField') private inputField: ElementRef;
	@ViewChild(NgbPopover) private popover: NgbPopover;
	@ViewChild(NgbInputDatepicker) private datepicker: NgbInputDatepicker;

	public error: Nullable<string>;
	public dateValueSource: TransformingObservable<Nullable<Date>, Nullable<NgbDateStruct>> = new TransformingObservable(obs$ => obs$
		.map(date => this.plainDateToNgbDate(date))
		.distinctShallow());
	public onTouchedCallback: () => void = noop;
	public onChangeCallback: (value: Nullable<Date>) => void = noop;

	private focusIn$: Observable<Event> = Observable.merge(Observable.fromEvent<Event>(document, 'focusin'), Observable.fromEvent<Event>(document, 'click'));
	private focusOut$: Observable<Event> = Observable.fromEvent(document, 'focusout');
	private keyPress$: Observable<KeyboardEvent> = Observable.fromEvent(document, 'keypress');
	private valueSource: TransformingObservable<Nullable<Date>, Nullable<Date>> = new TransformingObservable(obs$ => obs$
		.switchMap(value => Observable
			.if(
				() => !this.isInputChildNode(document.activeElement),
				// The input element is not focused; emit immediately
				Observable.of(void 0),
				// Wait until the element is no longer focused or the 'enter' key is pressed to emit the value
				Observable.merge(
					this.focusOut$.filter((event: Event) => this.isInputChildNode(event.target as Node)),
					this.keyPress$.filter((keypress: KeyboardEvent) => keypress.key === 'Enter'),
				),
			)
			.first()
			.mapTo(value)
			.takeUntil(this.inputModel.control.statusChanges!.filter(status => status === 'INVALID'))));


	constructor(private elementRef: ElementRef,
				private dateParserFormatter: NgbDateParserFormatter) {
		ManagedSubscription.of(this.focusIn$, (event: MouseEvent) => this.handleFocusChange(event.target as Node)).attach(this);
		ManagedSubscription.of(this.valueSource, (value: Nullable<Date>) => this.onChangeCallback(value)).attach(this);
		ManagedSubscription.of(() => this.inputModel.control.statusChanges!, () => this.validate()).attach(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {}


	public ngOnDestroy(): void {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public writeValue(val: Nullable<Date>): void {
		this.dateValueSource.next(val);
	}


	public registerOnChange(fn: (value: Nullable<Date>) => void): void {
		this.onChangeCallback = fn;
	}


	public registerOnTouched(fn: () => void): void {
		this.onTouchedCallback = fn;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public clearValue(): void {
		const clearedValue = null;
		this.writeValue(clearedValue);
		this.onValueChange(clearedValue);
	}


	public onValueChange(dateValue: Nullable<NgbDateStruct>): void {
		// Translate date and set control value
		const date: Nullable<Date> = this.ngbDateToPlainDate(dateValue);
		const validDateOrNull: Nullable<Date> = (date && !Number.isNaN(date.getFullYear())) ? date : null;
		this.valueSource.next(validDateOrNull);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngbDateToPlainDate(ngbDate: Nullable<NgbDateStruct>): Nullable<Date> {
		return (ngbDate != null && ngbDate.year != null && ngbDate.month != null && ngbDate.day != null)
			? new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day)
			: null;
	}


	public plainDateToNgbDate(date: Nullable<Date>): Nullable<NgbDate> {
		return DateUtils.isValidDate(date)
			? new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate())
			: null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private validate(): void {
		this.error = FormUtils.getValidationError(this.inputModel.control, this.validationMessages);
		this.popover[this.error ? 'open' : 'close']();
	}


	private handleFocusChange(focusTarget: Node): void {
		if (!this.isChildNode(focusTarget) && this.datepicker.isOpen()) {
			// Close date picker when component is unfocused
			this.datepicker.close();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private isChildNode(node: Node): boolean {
		return (this.elementRef.nativeElement as Node).contains(node);
	}


	private isInputChildNode(node: Node): boolean {
		return (this.inputField.nativeElement as Node).contains(node);
	}
}
