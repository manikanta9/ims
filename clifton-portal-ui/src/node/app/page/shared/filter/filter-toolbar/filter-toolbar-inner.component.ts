import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { Animations } from 'app/core/animation/animations';
import { RequestLoader } from 'app/core/api/request-loader';
import { ArrayUtils } from 'app/core/util/array.utils';
import { CompareUtils } from 'app/core/util/compare.utils';
import { ExceptionUtils } from 'app/core/util/exception.utils';
import { StringUtils } from 'app/core/util/string.utils';
import { FilterSelectEvent } from 'app/page/shared/filter/combo/selected-value-event';
import { FilterEntityType } from 'app/page/shared/filter/config/filter-entity.enum';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterCategoryKey, FilterDateKey, FilterEntityKey, FilterKey } from 'app/page/shared/filter/config/filter-key';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterSelectedData, FilterSelectedDataTypes } from 'app/page/shared/filter/config/filter-selected-data';
import { FilterToolbarConfig, FilterToolbarFilters } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterValue } from 'app/page/shared/filter/config/filter-value';
import { EntityFilterConfig } from 'app/page/shared/filter/config/item/entity-filter-config';
import { FilterComboFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-combo-field-state';
import { FilterDateFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-date-field-state';
import { FilterFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-field-state';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalFileCategoryQuery } from 'app/portal/file/portal-file-category.query';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


const DEFAULT_COMBO_FIELD_STATE: FilterComboFieldState<never> = {enabled: false, valueList: []};
const DEFAULT_DATE_FIELD_VALUE: any = Object.create(null);
const DEFAULT_DATE_FIELD_STATE: FilterDateFieldState = {enabled: false, selectedValue: DEFAULT_DATE_FIELD_VALUE};

const DOCUMENT_TYPE_KEY: FilterCategoryKey = 'category';

const PORTAL_ENTITY_ALL_VALUES: PortalEntity = {id: -1, label: '- Select -'};
const PORTAL_CATEGORY_ALL_VALUES: PortalFileCategory = {id: -1, labelExpanded: '- Select -', leaf: true, description: ''};


@Component({
	selector: 'ppa-filter-toolbar-inner',
	templateUrl: './filter-toolbar-inner.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	animations: [Animations.enterAnimation, Animations.fadeInAnimation],
})
export class FilterToolbarInnerComponent implements OnChanges, OnDestroy {

	// Exposed template values
	public FilterMode: typeof FilterMode = FilterMode;

	@Input() public filterMode: FilterMode;
	@Input() public filterConfig: FilterToolbarConfig;
	@Input() public filterGroup: FilterGroup;
	@Input() public filterSelectedData: FilterSelectedData;
	@Input() public filterApiEntityData: PortalEntity[];
	@Output() public filterModeChange: EventEmitter<FilterMode> = new EventEmitter();
	@Output() public filterValueChange: Observable<FilterValue>;
	@Output() public selectedValueChange: EventEmitter<FilterSelectEvent> = new EventEmitter();

	public groupEntityFilterState$: BehaviorSubject<FilterComboFieldState<PortalEntity>> = new BehaviorSubject(DEFAULT_COMBO_FIELD_STATE);
	public relationshipEntityFilterState$: BehaviorSubject<FilterComboFieldState<PortalEntity>> = new BehaviorSubject(DEFAULT_COMBO_FIELD_STATE);
	public entityEntityFilterState$: BehaviorSubject<FilterComboFieldState<PortalEntity>> = new BehaviorSubject(DEFAULT_COMBO_FIELD_STATE);
	public accountEntityFilterState$: BehaviorSubject<FilterComboFieldState<PortalEntity>> = new BehaviorSubject(DEFAULT_COMBO_FIELD_STATE);
	public documentTypeFilterState$: BehaviorSubject<FilterComboFieldState<PortalFileCategory>> = new BehaviorSubject(DEFAULT_COMBO_FIELD_STATE);
	public startDateFilterState$: BehaviorSubject<FilterDateFieldState> = new BehaviorSubject(DEFAULT_DATE_FIELD_STATE);
	public endDateFilterState$: BehaviorSubject<FilterDateFieldState> = new BehaviorSubject(DEFAULT_DATE_FIELD_STATE);

	public breadcrumb: PortalEntity[] = [];
	public breadcrumbHtml = '';
	public documentTypeRequester: RequestLoader<PortalFileCategory[], PortalFileCategoryQuery> = new RequestLoader({
		preModifier: (obs$: Observable<PortalFileCategoryQuery>) => {
			// Only reload when a relevant change has been made
			let lastQueryParameters: { mode?: FilterMode, config?: FilterToolbarConfig, group?: FilterGroup, entityId?: number } = {};
			let newQueryParameters: typeof lastQueryParameters = {};
			return obs$
				.do(query => newQueryParameters = {
					mode: this.filterMode, config: this.filterConfig, group: this.filterGroup,
					entityId: query.limitToCategoriesForAssignedPortalEntityId,
				})
				.filter(query => !CompareUtils.isEqualShallow(lastQueryParameters, newQueryParameters))
				.do(query => lastQueryParameters = newQueryParameters);
		},
		initiator: query => this.portalFileService.getFileCategoryList(query),
	});
	private documentTypeSub: Subscription;


	constructor(private portalFileService: PortalFileService) {
		this.initSubscriptions();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private initSubscriptions(): void {
		// TODO: Note that this is done via constructor rather than ngOnInit since we need these before binding
		this.documentTypeSub = this.documentTypeRequester.response$
			.subscribe((valueList: PortalFileCategory[]) =>
				this.documentTypeFilterState$.next(this.evaluateDocumentTypeFilterState(valueList)));

		this.filterValueChange = Observable
			.combineLatest(
				// Trigger change detection on any state change
				this.groupEntityFilterState$,
				this.relationshipEntityFilterState$,
				this.entityEntityFilterState$,
				this.accountEntityFilterState$,
				this.documentTypeFilterState$,
				this.startDateFilterState$,
				this.endDateFilterState$,
			)
			// Wrap requester "loading" observable with debounce to prevent excessive requests and to allow change detection to take place before acting
			.debounceTime(50)
			.switchMap(() => this.documentTypeRequester.loading$.filter(loading => !loading))
			.debounceTime(50)
			// Get the latest category in case it has changed after loading
			.mergeMap(() => this.documentTypeFilterState$.first())
			// Get actual values
			.map(() => ({
				mode: this.filterMode,
				entity: this.getEntitySelectedValue(),
				category: this.getDocumentTypeSelectedValue(),
				startDate: this.getSelectedStartDate(),
				endDate: this.getSelectedEndDate(),
			}))
			.distinctShallow();
	}


	public ngOnDestroy(): void {
		this.documentTypeSub.unsubscribe();
	}


	public ngOnChanges(changes: SimpleChanges): void {
		let filterStateChanged = false;
		for (const field of Object.keys(changes) as (keyof FilterToolbarInnerComponent)[]) {
			switch (field) {
				case 'filterSelectedData':
					/*
					 * This component pushes selected values to its parent, which also pushes selected values back to this component. This is cyclic data flow.
					 * Cyclic change detection is prevented by default in synchronous tasks via Angular's change detection, but this constraint doesn't apply
					 * for asynchronous actions, such as observables with async schedulers. We need to compare the selected data to see if any changes have
					 * actually taken place.
					 */
					if (!CompareUtils.isEqualShallow(changes[field].previousValue, changes[field].currentValue)) {
						this.applySelectedData();
						filterStateChanged = true;
					}
					break;
				case 'filterMode':
				case 'filterConfig':
				case 'filterGroup':
				case 'filterApiEntityData':
					filterStateChanged = true;
					break;
				default:
			}
		}
		// Delay filter state evaluation until entity data has been retrieved
		if (filterStateChanged && this.filterApiEntityData) {
			this.updateAllFilterStates();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public onSelectedValueChange(filterKey: FilterKey, selectedValue: any): void {
		this.selectedValueChange.emit({filterGroup: this.filterGroup, filterKey, selectedValue});
	}


	public isFilterInMode(filterKey: FilterKey, filterMode?: FilterMode): boolean {
		const config = this.getFilterConfig(filterKey);
		return !!config && (config.filterMode === filterMode);
	}


	public activeFilterIsEmpty(filterKey: FilterEntityKey | FilterCategoryKey): boolean {
		const state: FilterComboFieldState<any> = this.getFilterState(filterKey) as FilterComboFieldState<any>;
		if (!state.enabled) {
			return false;
		}

		// Return true if no values exist or if the only value is an "all" value
		return state.valueList.length < 1
			|| (state.valueList.length === 1 && state.valueList[0] === PORTAL_ENTITY_ALL_VALUES)
			|| (state.valueList.length === 1 && state.valueList[0] === PORTAL_CATEGORY_ALL_VALUES);
	}


	public getFilterConfig<T extends keyof FilterToolbarFilters>(filterName: T): FilterToolbarFilters[T] | undefined {
		return this.filterConfig && this.filterConfig.filters[filterName] || void 0;
	}


	public getFilterState<T extends FilterKey>(filterKey: T): FilterFieldState<FilterSelectedDataTypes[T]> {
		return this.getFilterState$(filterKey).getValue();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private applySelectedData(): void {
		const updateState = <T extends FilterKey>(filterKey: T): void => {
			const selectedValue: FilterSelectedData[T] = (this.filterSelectedData || {})[filterKey];
			const state$ = this.getFilterState$(filterKey);
			state$.next({...state$.getValue(), enabled: this.isFilterPresent(filterKey), selectedValue});
		};

		const filterKeyList: FilterKey[] = ['group', 'relationship', 'entity', 'account', 'category', 'startDate', 'endDate'];
		for (const filterKey of filterKeyList) {
			updateState(filterKey);
		}
	}


	private updateAllFilterStates(): void {
		this.updateEntityFilterState();
		this.updateDocumentTypeFilterState();
		this.updateDateFilterState();
	}


	private updateEntityFilterState(): void {
		const filterStates: { [key: string]: FilterComboFieldState<PortalEntity> } = {};
		const filterKeys: FilterEntityType[] = ArrayUtils.getEnumValues(FilterEntityType);

		for (const entityType of filterKeys) {
			const filterKey = FilterEntityType.getFilterEntityTypeKey(entityType);
			if (this.isFilterPresent(filterKey)) {
				const filterState: FilterComboFieldState<PortalEntity> = this.evaluateEntityFilterState(entityType);
				filterStates[entityType] = filterState;
				this.getFilterState$(filterKey).next(filterState);
			}
		}
		const accountState = filterStates[FilterEntityType.ACCOUNT];
		if (accountState && accountState.selectedValue) {
			this.breadcrumb = this.findMatchingParent(accountState.selectedValue, filterStates, FilterEntityType.ENTITY);
			if (this.breadcrumb.length < 3) {
				this.breadcrumb = [];
			}
		}
	}


	private findMatchingParent(value: PortalEntity, filterStates: { [key: string]: FilterComboFieldState<PortalEntity> }, entityType: FilterEntityType): PortalEntity[] {
		if (value && value !== PORTAL_ENTITY_ALL_VALUES) {
			if (value.parentPortalEntity) {
				const parentPortalEntityId = value.parentPortalEntity.id;
				const filterState: FilterComboFieldState<PortalEntity> | undefined = filterStates[entityType];
				if (filterState) {
					const parentValueList = filterState.valueList;
					for (const parentValue of parentValueList) {
						if (parentValue.id === parentPortalEntityId) {
							const parentEntityType: FilterEntityType | null = FilterEntityType.getParentEntityType(entityType);
							return this.findMatchingParent(parentValue, filterStates, parentEntityType!).concat(value);
						}
					}
				}
				return [value];
			}
			else {
				return [value];
			}
		}
		return [];
	}


	private updateDocumentTypeFilterState(): void {
		// Guard-clause: Ignore state if not present
		if (!this.isFilterPresent(DOCUMENT_TYPE_KEY)) {
			return;
		}

		const selectedEntityValue: PortalEntity | undefined = this.getEntitySelectedValue();
		const selectedEntityId: number | undefined = selectedEntityValue ? selectedEntityValue.id : void 0;
		// Populate with filtered document types
		this.documentTypeRequester.load({
			limitToCategoriesForAssignedPortalEntityId: selectedEntityId,
			nameExpanded: this.getFilterConfig('category')!.categoryName,
			requestedProperties: ['id', 'leaf', 'labelExpanded', 'description'],
		});
	}


	private updateDateFilterState(): void {
		this.startDateFilterState$.next(this.evaluateDateFilterState('startDate'));
		this.endDateFilterState$.next(this.evaluateDateFilterState('endDate'));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private isFilterPresent(filterName: FilterKey): boolean {
		return this.isFilterInMode(filterName) || this.isFilterInMode(filterName, this.filterMode);
	}


	private getFilterSelectedValue<T extends FilterKey>(filterName: T): FilterSelectedDataTypes[T] | undefined {
		return this.isFilterPresent(filterName) ? this.getFilterState(filterName).selectedValue : void 0;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Entity Filters                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private evaluateEntityFilterState(entityType: FilterEntityType): FilterComboFieldState<PortalEntity> {
		const filterKey: FilterEntityKey = FilterEntityType.getFilterEntityTypeKey(entityType);
		if (!this.isEntityFilterEnabled(entityType)) {
			return this.generateComboFilterState(filterKey, false, []);
		}

		const filterConfig = this.getFilterConfig(filterKey)!;

		let hasParentValue = true;
		// Get filtered value list
		let parentValueList: PortalEntity[] | undefined = void 0;
		const parentValue: PortalEntity | undefined = this.getEntityParentValue(entityType);
		if (parentValue === PORTAL_ENTITY_ALL_VALUES) {
			/*
			 * Find the first ancestor with a selected value. If one exists, then filter values for this list to only the children of the values available for
			 * the parent of the current filter. Otherwise, show all values at this level, regardless of available parent selections.
			 */
			// Determine if an ancestor is selected
			hasParentValue = false;
			let nearestSelectedEntityType: Nullable<FilterEntityType> = entityType;
			while ((nearestSelectedEntityType = FilterEntityType.getParentEntityType(nearestSelectedEntityType)) != null) {
				const ancestorSelectedValue = this.getFilterSelectedValue(FilterEntityType.getFilterEntityTypeKey(nearestSelectedEntityType));
				if (ancestorSelectedValue && ancestorSelectedValue !== PORTAL_ENTITY_ALL_VALUES) {
					// Retrieve value list for parent filter
					const parentEntityType = FilterEntityType.getParentEntityType(entityType)!;
					parentValueList = this.getEntityValueList(FilterEntityType.getFilterEntityTypeKey(parentEntityType));
					break;
				}
			}
		}
		const valueList = this.getFilteredEntityData(entityType, parentValue && (parentValueList || [parentValue]));

		// Get the previously selected value to use
		const oldValueList = this.getEntityValueList(FilterEntityType.getFilterEntityTypeKey(entityType));
		const valueListHasChanged = !ArrayUtils.isEqual(valueList, oldValueList);
		let selectedValue: PortalEntity | undefined = valueListHasChanged ? void 0 : this.getFilterSelectedValue(filterKey);
		if (parentValue === undefined && selectedValue === PORTAL_ENTITY_ALL_VALUES) {
			hasParentValue = false;
		}
		/*
		 * Order of precedence for default values:
		 * 1. The first real value, if only a single one exists
		 * 2. The "all" value, if one exists
		 * 3. No default
		 */
		const realValueList = valueList
			.filter(value => value !== PORTAL_ENTITY_ALL_VALUES);
		const singleItem = realValueList.length === 1 && realValueList[0];

		const defaultValue = hasParentValue && singleItem ? singleItem : (filterConfig.allowAll ? PORTAL_ENTITY_ALL_VALUES : void 0);
		if (hasParentValue && singleItem && selectedValue === PORTAL_ENTITY_ALL_VALUES) {
			selectedValue = realValueList[0];
		}

		return this.generateComboFilterState(filterKey, true, valueList, selectedValue, defaultValue);
	}


	/**
	 * Determines if the given entity filter should be enabled.
	 *
	 * The constraints for the entity filter are as follows:
	 * - The filter is present
	 * - An unpopulated parent filter does not exist
	 *
	 * @param {FilterEntityType} entityType the type for the entity filter to check
	 * @return {boolean} <tt>true</tt> if the filter should be enabled, or <tt>false</tt> otherwise
	 */
	private isEntityFilterEnabled(entityType: FilterEntityType): boolean {
		// Determine if the filter is present
		const filterPresent = this.isFilterPresent(FilterEntityType.getFilterEntityTypeKey(entityType));

		// Determine if an unpopulated parent exists
		const parentType = FilterEntityType.getParentEntityType(entityType);
		const hasParentFilter = parentType != null && this.isFilterPresent(FilterEntityType.getFilterEntityTypeKey(parentType));
		const hasParentEntity = !!this.getEntityParentValue(entityType);
		const hasUnpopulatedParent = hasParentFilter && !hasParentEntity;

		return filterPresent && !hasUnpopulatedParent;
	}


	private getEntityParentValue(entityType: FilterEntityType): PortalEntity | undefined {
		const parentType = FilterEntityType.getParentEntityType(entityType);
		const parentEntity = parentType != null && this.getFilterSelectedValue(FilterEntityType.getFilterEntityTypeKey(parentType));
		return parentEntity || void 0;
	}


	/**
	 * Gets the filtered list of entities to be displayed for the given entity type at the given level.
	 *
	 * @param {FilterEntityType} entityType the type of entity to filter by
	 * @param {PortalEntity} parentValueList the list of parent values with which to filter all child entities
	 * @return {PortalEntity[]} the list of filtered entities
	 */
	private getFilteredEntityData(entityType: FilterEntityType, parentValueList?: PortalEntity[]): PortalEntity[] {
		// Get filtered list
		const entityTypeName = FilterEntityType.getFilterEntityTypeName(entityType);
		const filteredEntityData: PortalEntity[] = (this.filterApiEntityData || [])
			.filter((entity: PortalEntity) => !parentValueList
				|| (parentValueList.length === 1 && parentValueList[0] === PORTAL_ENTITY_ALL_VALUES)
				|| (entity.parentPortalEntity && parentValueList.some(parentValue => parentValue.id === entity.parentPortalEntity!.id)))
			.filter((entity: PortalEntity) => entityTypeName === entity.entitySourceSection!.entityType!.name)
			.sort((a, b) => a.label && b.label ? a.label.localeCompare(b.label) : 0);

		// Add "all" entry if empty or if configured
		const filterKey: FilterEntityKey = FilterEntityType.getFilterEntityTypeKey(entityType);
		const filterConfig: EntityFilterConfig | undefined = this.getFilterConfig(filterKey);
		if (filteredEntityData.length === 0 || (filterConfig && filterConfig.allowAll)) {
			filteredEntityData.unshift(PORTAL_ENTITY_ALL_VALUES);
		}

		return filteredEntityData;
	}


	private getEntitySelectedValue(): PortalEntity | undefined {
		// Get the last existing filter
		const triggerFilterValue: FilterEntityKey | undefined = ArrayUtils.getEnumValues(FilterEntityType)
			.reverse()
			.map((filterType): FilterEntityKey => FilterEntityType.getFilterEntityTypeKey(filterType))
			.find(filterKey => {
				// Guard-clause: Ignore if filter is not present
				if (!this.isFilterPresent(filterKey)) {
					return false;
				}

				const filterConfig = this.getFilterConfig(filterKey)!;
				const filterSelectedValue = this.getFilterSelectedValue(filterKey);
				if (filterSelectedValue === PORTAL_ENTITY_ALL_VALUES) {
					// If "all" placeholder selected, use parent filter instead
					return false;
				}
				else {
					// Use this filter if present or if required
					return !!(filterSelectedValue || filterConfig.required);
				}
			});

		// Get its value
		const selectedValue = triggerFilterValue && this.getFilterSelectedValue(triggerFilterValue) || void 0;
		return selectedValue === PORTAL_ENTITY_ALL_VALUES ? void 0 : selectedValue;
	}


	private getEntityValueList(key: FilterEntityKey): PortalEntity[] {
		const entityState = this.getFilterState(key) as FilterComboFieldState<PortalEntity>;
		return entityState.valueList.sort((a, b) => a.label && b.label ? a.label.localeCompare(b.label) : 0);
	}


	private getFilterState$<T extends FilterKey>(filterKey: T): BehaviorSubject<FilterFieldState<FilterSelectedDataTypes[T]>> {
		let filterState$: BehaviorSubject<FilterFieldState<FilterSelectedDataTypes[T]>>;
		// Indicate type constraint (invariant on FilterKey rather than covariant); this reduces the available types for filterKey to those in FilterKey
		const filterKeyTyped: FilterKey = filterKey;
		switch (filterKeyTyped) {
			case 'group':
				filterState$ = this.groupEntityFilterState$;
				break;
			case 'relationship':
				filterState$ = this.relationshipEntityFilterState$;
				break;
			case 'entity':
				filterState$ = this.entityEntityFilterState$;
				break;
			case 'account':
				filterState$ = this.accountEntityFilterState$;
				break;
			case 'category':
				filterState$ = this.documentTypeFilterState$;
				break;
			case 'startDate':
				filterState$ = this.startDateFilterState$;
				break;
			case 'endDate':
				filterState$ = this.endDateFilterState$;
				break;
			default:
				filterState$ = ExceptionUtils.assertUnreachable(filterKeyTyped, `Unable to find the state for the given filter type: ${filterKey}`);
		}
		return filterState$;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Document Type Filters                           ////////
	////////////////////////////////////////////////////////////////////////////


	private evaluateDocumentTypeFilterState(valueList: PortalFileCategory[]): FilterComboFieldState<PortalFileCategory> {
		const filterKey: FilterCategoryKey = DOCUMENT_TYPE_KEY;
		if (!this.isFilterPresent(filterKey)) {
			return this.generateComboFilterState(filterKey, false, []);
		}

		const selectedValue: PortalFileCategory | undefined = this.getFilterSelectedValue(filterKey);
		const filteredValueList = valueList.filter(category => category.leaf);

		// Add "all" entry if empty or if configured
		const filterConfig: EntityFilterConfig = this.getFilterConfig(filterKey)!;
		let defaultValue: PortalFileCategory | undefined = void 0;
		if (filteredValueList.length === 0 || filterConfig.allowAll) {
			filteredValueList.unshift(PORTAL_CATEGORY_ALL_VALUES);
			defaultValue = PORTAL_CATEGORY_ALL_VALUES;
		}

		return this.generateComboFilterState(filterKey, true, filteredValueList, selectedValue, defaultValue);
	}


	private getDocumentTypeSelectedValue(): PortalFileCategory | undefined {
		if (!this.isFilterPresent(DOCUMENT_TYPE_KEY)) {
			return void 0;
		}

		const selectedValue = this.getFilterSelectedValue(DOCUMENT_TYPE_KEY);
		return (selectedValue === PORTAL_CATEGORY_ALL_VALUES) ? void 0 : selectedValue;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Date Filters                                    ////////
	////////////////////////////////////////////////////////////////////////////


	private evaluateDateFilterState(filterKey: FilterDateKey): FilterDateFieldState {
		// If not enabled, then disable
		if (!this.isFilterPresent(filterKey)) {
			// Reset to default date value when config disables date field
			return this.generateDateFilterState(filterKey, false, DEFAULT_DATE_FIELD_VALUE);
		}

		// If no set value, then use default value
		const isDefaultState = this.getFilterState(filterKey).selectedValue === DEFAULT_DATE_FIELD_VALUE;
		const originalSelectedValue: Nullable<Date> = this.getFilterSelectedValue(filterKey);
		const selectedValue = isDefaultState ? this.getFilterConfig(filterKey)!.defaultDate : originalSelectedValue;

		// Get minimum date
		let minDate: Date | undefined;
		if (filterKey === 'endDate' && this.isFilterPresent('startDate')) {
			const minDateState = this.getFilterState('startDate').selectedValue;
			if (minDateState instanceof Date) {
				minDate = minDateState;
			}
		}

		return this.generateDateFilterState(filterKey, true, selectedValue, originalSelectedValue, minDate);
	}


	private getSelectedStartDate(): Nullable<Date> {
		return this.getFilterSelectedValue('startDate');
	}


	private getSelectedEndDate(): Nullable<Date> {
		return this.getFilterSelectedValue('endDate');
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Filter States                                   ////////
	////////////////////////////////////////////////////////////////////////////


	private generateComboFilterState<T extends FilterEntityKey | FilterCategoryKey>(filterKey: T, enabled: boolean, valueList: FilterSelectedDataTypes[T][], originalSelectedValue?: FilterSelectedDataTypes[T], defaultValue?: FilterSelectedDataTypes[T]): FilterComboFieldState<FilterSelectedDataTypes[T]> {
		// Select single value or seek previously selected value in list
		const selectedValue: FilterSelectedDataTypes[T] | undefined =
			(valueList.length === 1 && valueList[0])
			|| valueList.find(value => !!(originalSelectedValue && originalSelectedValue.id === value.id))
			|| defaultValue;

		// Emit value changes
		if (selectedValue !== originalSelectedValue) {
			this.selectedValueChange.emit({filterGroup: this.filterGroup, filterKey, selectedValue});
		}

		return {enabled, valueList, selectedValue};
	}


	private generateDateFilterState<T extends FilterDateKey>(filterKey: T, enabled: boolean, selectedValue?: FilterSelectedDataTypes[T], originalSelectedValue?: FilterSelectedDataTypes[T], minDate?: Date): FilterDateFieldState {
		// Handle date constraints
		const validSelectedValue: Nullable<FilterSelectedDataTypes[T]> = (minDate && selectedValue && minDate.getTime() > selectedValue.getTime())
			? minDate : selectedValue;

		// Emit value changes
		const applicableOriginalSelectedValue = originalSelectedValue === DEFAULT_DATE_FIELD_VALUE ? void 0 : originalSelectedValue;
		if (!CompareUtils.isEqualByProperty(validSelectedValue, applicableOriginalSelectedValue, 'getTime')) {
			this.selectedValueChange.emit({filterGroup: this.filterGroup, filterKey, selectedValue: validSelectedValue});
		}

		return {enabled, selectedValue: validSelectedValue, minDate};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getCategoryLabel(category: PortalFileCategory): string {
		const label: RegExpExecArray | null = /(?:[^\/]*\/)?\s*(.*)/.exec(category.labelExpanded || '');
		return label && label[1] || '';
	}


	public getCategoryDescription(category: PortalFileCategory): string {
		return category && category.description ? category.description! : '';
	}


	public getFilterClass(filterKey: FilterKey): () => string {
		const filterState = this.getFilterState(filterKey);

		return () => {
			return (filterState.selectedValue && filterState.selectedValue === PORTAL_ENTITY_ALL_VALUES || filterState.selectedValue === PORTAL_CATEGORY_ALL_VALUES) ? 'text-gray' : 'text-gray-dark';
		};
	}


	public getBreadCrumbHtml(crumb: PortalEntity): string {
		return StringUtils.truncate(crumb.label!, 35) + '&nbsp; &gt; &nbsp;';
	}
}
