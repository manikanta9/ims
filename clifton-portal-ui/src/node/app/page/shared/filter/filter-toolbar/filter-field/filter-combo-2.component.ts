import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FilterComboFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-combo-field-state';


@Component({
	selector: 'ppa-filter-combo-2',
	templateUrl: './filter-combo-2.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterCombo2Component<T> {
	@Input() public label: string;
	@Input() public valueLabel: string | ((value: T) => string) = 'label';
	@Input() public titleField?: string | ((value: T) => string);
	@Input() public filterState: FilterComboFieldState<T>;
	@Input() public ngClass1?: () => string;
	@Output() public valueChange: EventEmitter<T> = new EventEmitter();
}
