import { FilterFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-field-state';


export interface FilterComboFieldState<T> extends FilterFieldState<T> {
	valueList: T[];
}
