import { OnDestroy, OnInit } from '@angular/core';
import { ManagedLifecycleActionSet } from 'app/core/component/lifecycle/managed-lifecycle-action-set';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';


export class ManagedFilterToolbarConfig extends ManagedLifecycleActionSet<OnInit & OnDestroy> {
	protected constructor(private toolbarService: FilterToolbarService,
						  private config: FilterToolbarConfig) {
		super({
			beforeFns: {ngOnInit: () => this.toolbarService.enableFilter(config)},
			fns: {ngOnDestroy: () => this.toolbarService.disableFilter()},
		});
	}


	public static of(toolbarService: FilterToolbarService, config: FilterToolbarConfig): ManagedFilterToolbarConfig {
		return new ManagedFilterToolbarConfig(toolbarService, config);
	}
}
