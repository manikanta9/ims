import { FilterFieldState } from 'app/page/shared/filter/filter-toolbar/filter-state/filter-field-state';


export interface FilterDateFieldState extends FilterFieldState<Date | null> {
	minDate?: Date;
	maxDate?: Date;
}
