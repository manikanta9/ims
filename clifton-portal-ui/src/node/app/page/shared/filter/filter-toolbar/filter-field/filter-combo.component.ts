import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { CompareUtils } from 'app/core/util/compare.utils';
import * as _ from 'lodash';


@Component({
	selector: 'ppa-filter-combo',
	templateUrl: './filter-combo.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
// TODO: This is a generic component -- move this out of the filter package
export class FilterComboComponent<T> {

	@Input() public disabledText = 'Disabled';
	@Input() public emptyText = 'None available';
	@Input() public enabled: boolean;
	@Input() public label: string;
	@Input() public labelStyle?: { [key: string]: string };
	@Input() public selectedValue?: T;
	@Input() public valueLabel: string | ((value: T) => string);
	@Input() public titleField?: string | ((value: T) => string);
	@Input() public ngClass1?: () => string;
	@Input() public compareWith: (val1: T, val2: T) => boolean = (val1, val2) => val1 === val2;
	@Output() public valueChange: EventEmitter<T> = new EventEmitter();

	private _valueList: T[];

	@Input()
	public get valueList(): T[] {return this._valueList;}


	public set valueList(list: T[]) {
		this._valueList = list.sort((val1: T, val2: T) => CompareUtils.compareValues(this.getLabel(val1), this.getLabel(val2)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getLabel(value: T): string {
		return (typeof this.valueLabel === 'string') ? _.get(value, this.valueLabel) : this.valueLabel(value);
	}


	public getTitle(value: T): string {
		if (this.titleField != null) {
			return (typeof this.titleField === 'function') ? this.titleField(value) : _.get(value, this.titleField, '');
		}
		return '';
	}


}
