import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Animations } from 'app/core/animation/animations';
import { RequestLoader } from 'app/core/api/request-loader';
import { FilterSelectEvent } from 'app/page/shared/filter/combo/selected-value-event';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterSelectedData } from 'app/page/shared/filter/config/filter-selected-data';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterValue } from 'app/page/shared/filter/config/filter-value';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import { RemoveFilterToolbarSelectedValueAction, SetFilterToolbarSelectedValueAction } from 'app/page/shared/filter/state/filter.actions';
import * as filterStore from 'app/page/shared/filter/state/filter.store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-filter-toolbar',
	templateUrl: './filter-toolbar.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	animations: [Animations.enterAnimation],
})
export class FilterToolbarComponent {

	// Store-managed values
	public filterEnabled$: Observable<boolean> = this.store.select(filterStore.getFilterEnabled);
	public filterMode$: Observable<FilterMode> = this.filterToolbarService.getFilterMode();
	public filterConfig$: Observable<FilterToolbarConfig> = this.store.select(filterStore.getFilterConfig);
	public filterGroup$: Observable<FilterGroup> = this.store.select(filterStore.getFilterGroup);
	public filterSelectedData$: Observable<FilterSelectedData> = this.filterGroup$
		.switchMap((group: FilterGroup) => this.store.select(filterStore.getFilterGroupValues(group)));

	// API data retrievers
	public filterApiEntityDataRequester: RequestLoader<PortalEntity[]> = new RequestLoader({
		preModifier: obs$ => obs$.mergeMap(() => this.filterGroup$),
		initiator: (group: FilterGroup) => this.filterToolbarService.getApiEntityData(group),
		initialValue: null,
	});


	constructor(private store: Store<filterStore.FilterStore>,
	            private filterToolbarService: FilterToolbarService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public selectedValueChange({filterGroup, filterKey, selectedValue}: FilterSelectEvent): void {
		if (selectedValue != null) {
			this.store.dispatch(new SetFilterToolbarSelectedValueAction(filterGroup, filterKey, selectedValue));
		}
		else {
			this.store.dispatch(new RemoveFilterToolbarSelectedValueAction(filterGroup, filterKey));
		}
	}


	public filterModeChange(filterMode: FilterMode): void {
		this.filterToolbarService.setFilterMode(filterMode);
	}


	public filterValueChange(filterValue: FilterValue): void {
		this.filterToolbarService.setFilterValues(filterValue);
	}
}
