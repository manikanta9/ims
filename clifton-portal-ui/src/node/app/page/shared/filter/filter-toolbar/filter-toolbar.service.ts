import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { DateUtils } from 'app/core/util/date.utils';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { FilterValue } from 'app/page/shared/filter/config/filter-value';
import * as filter from 'app/page/shared/filter/state/filter.reducer';
import * as filterStore from 'app/page/shared/filter/state/filter.store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityQuery } from 'app/portal/entity/portal-entity-query';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';


@Injectable()
export class FilterToolbarService {

	private entityDataRequester: RequestLoader<PortalEntity[], null> = new RequestLoader({
		// Reset cached filters on user change via user switchMap, and then seek cached data
		preModifier: (obs$: Observable<null>) => obs$
			.mergeMap(() => this.store.select(filterStore.getUser))
			.switchMap(user => (!user ? Observable.never<FilterGroup>() : this.store.select(filterStore.getFilterGroup))
				.mergeMap(group => this.getApiEntityData(group).map(entityData => [group, entityData]))),
		// Retrieve data if not cached
		initiator: ([group, entityData]: [FilterGroup, PortalEntity[]]) => entityData ? Observable.of(entityData) : this.getEntityList(group),
		initialValue: null,
	});
	private filterValueSource: ReplaySubject<FilterValue> = new ReplaySubject(1);


	constructor(private store: Store<filterStore.FilterStore>,
	            private portalEntityService: PortalEntityService) {
		// Enable subscription
		this.entityDataRequester.response$.subscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public enableFilter(config?: FilterToolbarConfig): void {
		this.store.dispatch(new filter.EnableFilterToolbarAction());
		if (config) {
			this.store.dispatch(new filter.SetFilterToolbarConfigAction(config));
		}
	}


	public disableFilter(): void {
		this.store.dispatch(new filter.DisableFilterToolbarAction());
	}


	public isFilterEnabled(): Observable<boolean> {
		return this.store.select(filterStore.getFilterEnabled);
	}


	public getFilterValues(): Observable<FilterValue> {
		return this.filterValueSource.asObservable()
			.debounce(filterValue => this.entityDataRequester.loading$
				.filter(loading => !loading));
	}


	public setFilterValues(values: FilterValue): void {
		this.filterValueSource.next(values);
	}


	public getFilterMode(): Observable<FilterMode> {
		return this.store.select(filterStore.getFilterMode);
	}


	public setFilterMode(filterMode: FilterMode): void {
		this.store.dispatch(new filter.SetFilterToolbarModeAction(filterMode));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public convertToPortalFileQuery(value: FilterValue, category: string): PortalFileQuery {
		const query: PortalFileQuery = {};
		if (value.entity) {
			query.assignedPortalEntityId = value.entity.id;
		}
		if (value.category) {
			query.fileCategoryIdExpanded = value.category.id;
		}
		else {
			query.categoryName = category;
		}
		switch (value.mode) {
			case FilterMode.SUMMARY:
				query.defaultDisplay = true;
				break;
			case FilterMode.DETAIL:
				if (value.startDate) {
					query.minReportDate = DateUtils.formatDateShort(value.startDate);
				}
				if (value.endDate) {
					query.maxReportDate = DateUtils.formatDateShort(value.endDate);
				}
				break;
		}
		return query;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getApiEntityData(group: FilterGroup): Observable<PortalEntity[]> {
		return this.store.select(filterStore.getFilterEntityData(group));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private getEntityList(group: FilterGroup): Observable<PortalEntity[]> {
		const query: PortalEntityQuery = {
			requestedProperties: ['id', 'label', 'entitySourceSection.entityType.name', 'parentPortalEntity.id', 'entityViewType.name'],
		};
		switch (group) {
			case FilterGroup.PROFILE:
				query.hasSecurityResourceAccess = true;
				break;
			case FilterGroup.ADMIN:
				query.viewSecurityAdminRole = true;
				break;
		}
		return this.portalEntityService.getEntityList(query)
			// Cache data when retrieved
			.do(newEntityData => this.store.dispatch(new filter.SetFilterToolbarApiEntityDataAction(group, newEntityData)));
	}
}
