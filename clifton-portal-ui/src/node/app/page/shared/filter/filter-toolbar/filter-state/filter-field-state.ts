export interface FilterFieldState<T> {
	enabled: boolean;
	selectedValue?: T;
}
