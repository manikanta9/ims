// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterKey } from 'app/page/shared/filter/config/filter-key';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import { PortalEntity } from 'app/portal/entity/portal-entity';


// Action names
export const ENABLE_FILTER_TOOLBAR = '[Filter Toolbar] Enable';
export const DISABLE_FILTER_TOOLBAR = '[Filter Toolbar] Disable';
export const SET_FILTER_TOOLBAR_CONFIG = '[Filter Toolbar] Set Config';
export const SET_FILTER_TOOLBAR_MODE = '[Filter Toolbar] Set Mode';
export const SET_FILTER_TOOLBAR_SELECTED_VALUE = '[Filter Toolbar] Set Selected Value';
export const REMOVE_FILTER_TOOLBAR_SELECTED_VALUE = '[Filter Toolbar] Remove Selected Value';
export const RESET_FILTER_TOOLBAR_SELECTED_VALUES = '[Filter Toolbar] Reset Selected Values';
export const SET_FILTER_TOOLBAR_API_ENTITY_DATA = '[Filter Toolbar] Set API Entity Data';


// Action types
export class EnableFilterToolbarAction implements Action {
	public readonly type = ENABLE_FILTER_TOOLBAR;
}


export class DisableFilterToolbarAction implements Action {
	public readonly type = DISABLE_FILTER_TOOLBAR;
}


export class SetFilterToolbarConfigAction implements Action {
	public readonly type = SET_FILTER_TOOLBAR_CONFIG;


	constructor(public payload: FilterToolbarConfig) { }
}


export class SetFilterToolbarModeAction implements Action {
	public readonly type = SET_FILTER_TOOLBAR_MODE;


	constructor(public mode: FilterMode) { }
}


export class SetFilterToolbarSelectedValueAction implements Action {
	public readonly type = SET_FILTER_TOOLBAR_SELECTED_VALUE;


	constructor(public filterGroup: FilterGroup,
	            public filterName: FilterKey,
	            public value: any) { }
}


export class RemoveFilterToolbarSelectedValueAction implements Action {
	public readonly type = REMOVE_FILTER_TOOLBAR_SELECTED_VALUE;


	constructor(public filterGroup: FilterGroup,
	            public filterName: FilterKey) { }
}


export class ResetFilterToolbarSelectedValuesAction implements Action {
	public readonly type = RESET_FILTER_TOOLBAR_SELECTED_VALUES;


	constructor(public filterGroup: FilterGroup) { }
}


export class SetFilterToolbarApiEntityDataAction implements Action {
	public readonly type = SET_FILTER_TOOLBAR_API_ENTITY_DATA;


	constructor(public filterGroup: FilterGroup,
	            public data: PortalEntity[]) { }
}


// Action type union
export type Actions =
	| EnableFilterToolbarAction
	| DisableFilterToolbarAction
	| SetFilterToolbarConfigAction
	| SetFilterToolbarModeAction
	| SetFilterToolbarSelectedValueAction
	| RemoveFilterToolbarSelectedValueAction
	| ResetFilterToolbarSelectedValuesAction
	| SetFilterToolbarApiEntityDataAction
	;
