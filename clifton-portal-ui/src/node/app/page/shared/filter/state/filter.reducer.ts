import { ActionReducer } from '@ngrx/store';
import { TypeUtils } from 'app/core/util/type.utils';
import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterKey } from 'app/page/shared/filter/config/filter-key';
import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { FilterSelectedData } from 'app/page/shared/filter/config/filter-selected-data';
import { FilterToolbarConfig } from 'app/page/shared/filter/config/filter-toolbar-config';
import * as filter from 'app/page/shared/filter/state/filter.actions';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import * as _ from 'lodash';
import { createSelector } from 'reselect';


export * from 'app/page/shared/filter/state/filter.actions';


// State
export const storeName = 'filter';


export interface FilterState {
	enabled: boolean;
	config: FilterToolbarConfig;
	mode: FilterMode;
	entityData: { [filterGroup: number]: PortalEntity[] };
	selectedData: { [filterGroup: number]: FilterSelectedData };
}


export const initialState: FilterState = {
	enabled: false,
	config: {
		group: FilterGroup.STANDARD,
		filters: {},
	},
	mode: FilterMode.SUMMARY,
	entityData: {},
	selectedData: {},
};


// Reducers
export const filterReducer: ActionReducer<FilterState> = (state: FilterState = initialState, action: filter.Actions): FilterState => {
	switch (action.type) {
		case filter.ENABLE_FILTER_TOOLBAR:
			return {
				...state,
				enabled: true,
			};
		case filter.DISABLE_FILTER_TOOLBAR:
			return {
				...state,
				enabled: false,
			};
		case filter.SET_FILTER_TOOLBAR_CONFIG:
			return {
				...state,
				config: action.payload,
			};
		case filter.SET_FILTER_TOOLBAR_MODE:
			return {
				...state,
				mode: action.mode,
			};
		case filter.SET_FILTER_TOOLBAR_SELECTED_VALUE:
			return {
				...state,
				selectedData: {
					...state.selectedData,
					[action.filterGroup]: {
						...state.selectedData[action.filterGroup],
						[action.filterName]: action.value,
					},
				},
			};
		case filter.REMOVE_FILTER_TOOLBAR_SELECTED_VALUE:
			return {
				...state,
				selectedData: {
					...state.selectedData,
					[action.filterGroup]: _.omit(state.selectedData[action.filterGroup] || {}, action.filterName),
				},
			};
		case filter.RESET_FILTER_TOOLBAR_SELECTED_VALUES:
			return {
				...state,
				selectedData: {
					...state.selectedData,
					[action.filterGroup]: {},
				},
			};
		case filter.SET_FILTER_TOOLBAR_API_ENTITY_DATA:
			return {
				...state,
				entityData: {
					...state.entityData,
					[action.filterGroup]: action.data,
				},
			};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export const getEnabled: (state: FilterState) => boolean = (state: FilterState) => state.enabled;
export const getMode: (state: FilterState) => FilterMode = (state: FilterState) => state.mode;
export const getConfig: (state: FilterState) => FilterToolbarConfig = (state: FilterState) => state.config;
export const getGroup: (state: FilterState) => FilterGroup = (state: FilterState) => state.config.group;
export const getValues: (state: FilterState) => FilterState['selectedData'] = (state: FilterState) => state.selectedData;

export function getGroupValues(filterGroup: FilterGroup): (state: FilterState) => FilterSelectedData {
	return createSelector(getValues, (selectedData: FilterState['selectedData']) => selectedData[filterGroup]);
}

export function getFilterValue<T extends FilterKey>(filterGroup: FilterGroup, filterName: T): (state: FilterState) => FilterSelectedData[T] {
	return createSelector(getGroupValues(filterGroup), (groupValues: FilterSelectedData) => groupValues[filterName]);
}

export function getEntityData(filterGroup: FilterGroup): (state: FilterState) => PortalEntity[] {
	return (state: FilterState) => state.entityData[filterGroup];
}
