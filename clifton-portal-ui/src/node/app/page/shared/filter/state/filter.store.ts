import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterKey } from 'app/page/shared/filter/config/filter-key';
import * as filter from 'app/page/shared/filter/state/filter.reducer';
import * as pageStore from 'app/page/shared/state/page.store';
import { createSelector } from 'reselect';


export * from 'app/page/shared/state/page.store';


// State
export interface FilterStore extends pageStore.PageStore {
	filter: filter.FilterState;
}


// Map of reducer per store slice
export const filterReducerMap: pageStore.ReducerMap<FilterStore> = {
	filter: filter.filterReducer,
};


// TODO: Set initial state


// Memoized selectors
export const getFilterState: (state: FilterStore) => filter.FilterState = (state: FilterStore) => state.filter;

export const getFilterEnabled = createSelector(getFilterState, filter.getEnabled);
export const getFilterMode = createSelector(getFilterState, filter.getMode);
export const getFilterConfig = createSelector(getFilterState, filter.getConfig);
export const getFilterGroup = createSelector(getFilterState, filter.getGroup);
export const getFilterValues = createSelector(getFilterState, filter.getValues);
export const getFilterGroupValues = (filterGroup: FilterGroup) => createSelector(getFilterState, filter.getGroupValues(filterGroup));
export const getFilterValue = (filterGroup: FilterGroup, filterName: FilterKey) => createSelector(getFilterState, filter.getFilterValue(filterGroup, filterName));
export const getFilterEntityData = (filterGroup: FilterGroup) => createSelector(getFilterState, filter.getEntityData(filterGroup));
