import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterKey } from 'app/page/shared/filter/config/filter-key';
import { FilterSelectedDataTypes } from 'app/page/shared/filter/config/filter-selected-data';


export interface FilterSelectEvent {
	filterGroup: FilterGroup;
	filterKey: FilterKey;
	selectedValue?: FilterSelectedDataTypes[FilterKey];
}
