import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from 'app/core/core.module';
import { StateService } from 'app/core/state/state.service';
import { FilterCombo2Component } from 'app/page/shared/filter/filter-toolbar/filter-field/filter-combo-2.component';
import { FilterComboComponent } from 'app/page/shared/filter/filter-toolbar/filter-field/filter-combo.component';
import { FilterDateComponent } from 'app/page/shared/filter/filter-toolbar/filter-field/filter-date.component';
import { FilterToolbarInnerComponent } from 'app/page/shared/filter/filter-toolbar/filter-toolbar-inner.component';
import { FilterToolbarComponent } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.component';
import { FilterToolbarService } from 'app/page/shared/filter/filter-toolbar/filter-toolbar.service';
import * as filterStore from 'app/page/shared/filter/state/filter.store';


@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		FormsModule,

		NgbPopoverModule,
		NgbDatepickerModule,
	],
	declarations: [
		FilterCombo2Component,
		FilterComboComponent,
		FilterDateComponent,
		FilterToolbarComponent,
		FilterToolbarInnerComponent,
	],
	exports: [
		FilterToolbarComponent,
	],
})
export class FilterModule {
	constructor(stateService: StateService) {
		stateService.addReducers(filterStore.filterReducerMap);
	}


	public static forRoot(): ModuleWithProviders {
		return {
			ngModule: FilterModule,
			providers: [
				FilterToolbarService,
			],
		};
	}
}
