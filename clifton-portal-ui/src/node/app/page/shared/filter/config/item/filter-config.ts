import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';

export interface FilterConfig {
	filterMode?: FilterMode;
	label?: string;
}
