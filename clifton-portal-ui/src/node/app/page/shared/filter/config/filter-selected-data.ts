import { FilterCategoryKey, FilterDateKey, FilterEntityKey, FilterKey } from 'app/page/shared/filter/config/filter-key';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';


// Back type with interface to ensure keys match available filters
export type FilterSelectedDataTypes =
	& { [entityKey in FilterEntityKey]: PortalEntity; }
	& { [categoryKey in FilterCategoryKey]: PortalFileCategory; }
	& { [dateKey in FilterDateKey]: Date | null; };


export type FilterSelectedData = {
	[Field in FilterKey]?: FilterSelectedDataTypes[Field];
	};
