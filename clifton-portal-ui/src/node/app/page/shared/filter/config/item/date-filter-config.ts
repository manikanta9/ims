import { FilterConfig } from 'app/page/shared/filter/config/item/filter-config';


export interface DateFilterConfig extends FilterConfig {
	// A null value indicates that a default exists but that it should be blank
	defaultDate?: Date | null;
}
