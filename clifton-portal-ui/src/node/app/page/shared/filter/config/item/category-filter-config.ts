import { FilterConfig } from 'app/page/shared/filter/config/item/filter-config';


export interface CategoryFilterConfig extends FilterConfig {
	categoryName: string;
	allowAll?: boolean;
	extraParams?: { [key: string]: any };
}
