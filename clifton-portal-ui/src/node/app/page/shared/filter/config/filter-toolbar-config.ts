import { FilterGroup } from 'app/page/shared/filter/config/filter-group.enum';
import { FilterCategoryKey, FilterDateKey, FilterEntityKey } from 'app/page/shared/filter/config/filter-key';
import { CategoryFilterConfig } from 'app/page/shared/filter/config/item/category-filter-config';
import { DateFilterConfig } from 'app/page/shared/filter/config/item/date-filter-config';
import { EntityFilterConfig } from 'app/page/shared/filter/config/item/entity-filter-config';


export type FilterToolbarFilters =
	& { [entityKey in FilterEntityKey]?: EntityFilterConfig; }
	& { [categoryKey in FilterCategoryKey]?: CategoryFilterConfig; }
	& { [dateKey in FilterDateKey]?: DateFilterConfig; };


export interface FilterToolbarConfig {
	modeControlEnabled?: boolean;
	group: FilterGroup;
	filters: FilterToolbarFilters;
}
