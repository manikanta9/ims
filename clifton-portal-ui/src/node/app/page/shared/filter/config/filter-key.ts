export type FilterEntityKey = 'group' | 'relationship' | 'entity' | 'account';
export type FilterCategoryKey = 'category';
export type FilterDateKey = 'startDate' | 'endDate';
export type FilterKey = FilterEntityKey | FilterCategoryKey | FilterDateKey;
