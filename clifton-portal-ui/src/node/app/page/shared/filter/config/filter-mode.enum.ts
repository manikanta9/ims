export enum FilterMode {
	SUMMARY = 'summary',
	DETAIL = 'detail',
}
