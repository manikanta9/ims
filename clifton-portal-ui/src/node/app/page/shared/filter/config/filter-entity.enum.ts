import { FilterEntityKey } from 'app/page/shared/filter/config/filter-key';


/**
 * The entity filter types in order of increasing specificity.
 */
export enum FilterEntityType {
	GROUP,
	RELATIONSHIP,
	ENTITY,
	ACCOUNT,
}


interface FilterEntityTypeData {
	name: string;
	key: FilterEntityKey;
	parent: FilterEntityType | null;
}


const FILTER_ENTITY_TYPE_DATA: { [filterEntityType: number]: FilterEntityTypeData } = {
	[FilterEntityType.GROUP]: {
		name: 'Organization Group',
		key: 'group',
		parent: null,
	},
	[FilterEntityType.RELATIONSHIP]: {
		name: 'Client Relationship',
		key: 'relationship',
		parent: FilterEntityType.GROUP,
	},
	[FilterEntityType.ENTITY]: {
		name: 'Client',
		key: 'entity',
		parent: FilterEntityType.RELATIONSHIP,
	},
	[FilterEntityType.ACCOUNT]: {
		name: 'Client Account',
		key: 'account',
		parent: FilterEntityType.ENTITY,
	},
};

// tslint:disable-next-line:no-namespace : Namespace required to append field data to enum type
export namespace FilterEntityType {
	export function getParentEntityType(entityType: FilterEntityType): FilterEntityTypeData['parent'] {
		return FILTER_ENTITY_TYPE_DATA[entityType].parent;
	}


	export function getFilterEntityTypeName(entityType: FilterEntityType): string {
		return FILTER_ENTITY_TYPE_DATA[entityType].name;
	}


	export function getFilterEntityTypeKey(entityType: FilterEntityType): FilterEntityKey {
		return FILTER_ENTITY_TYPE_DATA[entityType].key;
	}
}
