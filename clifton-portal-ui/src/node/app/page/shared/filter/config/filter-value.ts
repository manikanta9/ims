import { FilterMode } from 'app/page/shared/filter/config/filter-mode.enum';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';


export interface FilterValue {
	mode: FilterMode;
	entity?: PortalEntity;
	category?: PortalFileCategory;
	startDate?: Date | null;
	endDate?: Date | null;
}
