import { FilterConfig } from 'app/page/shared/filter/config/item/filter-config';


export interface EntityFilterConfig extends FilterConfig {
	autoSelect?: boolean;
	allowAll?: boolean;
	extraParams?: { [key: string]: any };
	required?: boolean;
}
