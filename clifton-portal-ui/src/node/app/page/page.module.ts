import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { CanActivate, RouterModule, Routes } from '@angular/router';
import { CoreModule } from 'app/core/core.module';
import { ModalDialogModule } from 'app/core/modal/modal-dialog.module';
import { PageComponent } from 'app/page/page.component';
import { ChangePasswordDialogComponent } from 'app/page/popup/change-password/change-password-dialog.component';
import { ForgotPasswordDialogComponent } from 'app/page/popup/forgot-password/forgot-password-dialog.component';
import { LoginComponent } from 'app/page/popup/login/login.component';
import { NotificationAcknowledgementComponent } from 'app/page/popup/notification-acknowledgement/notification-acknowledgement.component';
import { PopupModule } from 'app/page/popup/popup.module';
import { TermsOfUseReviewComponent } from 'app/page/popup/terms-of-use/terms-of-use-review.component';
import { TermsOfUseComponent } from 'app/page/popup/terms-of-use/terms-of-use.component';
import { MainModule } from 'app/page/screen/main/main.module';
import { ProfileModule } from 'app/page/screen/profile/profile.module';
import { FilterModule } from 'app/page/shared/filter/filter.module';
import { AuthGuard } from 'app/page/shared/guard/auth/auth.guard';
import { NotAuthGuard } from 'app/page/shared/guard/auth/not-auth.guard';
import { ChangePasswordGuard } from 'app/page/shared/guard/change-password/change-password.guard';
import { NotChangePasswordGuard } from 'app/page/shared/guard/change-password/not-change-password.guard';
import { GuardModule } from 'app/page/shared/guard/guard.module';
import { NotNotificationAcknowledgementGuard } from 'app/page/shared/guard/notification-acknowledgement-guard/not-notification-acknowledgement-guard';
import { NotificationAcknowledgementGuard } from 'app/page/shared/guard/notification-acknowledgement-guard/notification-acknowledgement-guard';
import { PageDataGuard } from 'app/page/shared/guard/page-data/page-data.guard';
import { NotTermsOfUseGuard } from 'app/page/shared/guard/terms-of-use/not-terms-of-use.guard';
import { TermsOfUseGuard } from 'app/page/shared/guard/terms-of-use/terms-of-use.guard';
import { UserDataGuard } from 'app/page/shared/guard/user-data/user-data.guard';
import { PageStateModule } from 'app/page/shared/state/page-state.module';
import { StructureModule } from 'app/page/structure/structure.module';


interface ActivationGuardConfig {
	path: string;
	guard: Type<CanActivate>;
	antiGuard: Type<CanActivate>;
	component: Type<any>;
	children: Routes;
}


export function wrapWithActivationGuard(config: ActivationGuardConfig): Routes {
	return [
		{
			path: config.path,
			canActivate: [config.antiGuard],
			/*
			 * Include this route here so that the router discovers the path when searching valid routes for the primary outlet and then renders nothing within that
			 * outlet.
			 */
			children: [
				{
					path: '',
					component: config.component,
					outlet: 'popup',
				},
			],
		},
		{
			path: '',
			canActivate: [config.guard],
			children: config.children,
		},
	];
}


// Work around https://github.com/angular/angular/issues/17467
export function wrapWithActivationGuard1(config: ActivationGuardConfig): Routes {
	return wrapWithActivationGuard(config);
}

export function wrapWithActivationGuard2(config: ActivationGuardConfig): Routes {
	return wrapWithActivationGuard(config);
}

export function wrapWithActivationGuard3(config: ActivationGuardConfig): Routes {
	return wrapWithActivationGuard(config);
}

export function wrapWithActivationGuard4(config: ActivationGuardConfig): Routes {
	return wrapWithActivationGuard(config);
}

const ROUTES: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'main',
	},
	{
		path: '',
		component: PageComponent,
		canActivate: [PageDataGuard],
		children: [
			...wrapWithActivationGuard1({
				path: 'login',
				guard: AuthGuard,
				antiGuard: NotAuthGuard,
				component: LoginComponent,
				children: [{
					path: '',
					canActivate: [UserDataGuard],
					children: wrapWithActivationGuard2({
						path: 'change-password-dialog',
						guard: ChangePasswordGuard,
						antiGuard: NotChangePasswordGuard,
						component: ChangePasswordDialogComponent,
						children: wrapWithActivationGuard3({
							path: 'terms-of-use',
							guard: TermsOfUseGuard,
							antiGuard: NotTermsOfUseGuard,
							component: TermsOfUseComponent,
							children: wrapWithActivationGuard4({
								path: 'notification-acknowledgement',
								guard: NotificationAcknowledgementGuard,
								antiGuard: NotNotificationAcknowledgementGuard,
								component: NotificationAcknowledgementComponent,
								children: [
									{
										path: 'main',
										children: MainModule.ROUTES,
									},
									{
										path: 'profile',
										children: ProfileModule.ROUTES,
									},
									{
										path: 'admin',
										loadChildren: 'app/page/screen/admin/admin.module#AdminModule',
									},
									{
										path: 'your-portal-admin',
										loadChildren: 'app/page/screen/your-portal-admin/your-portal-admin.module#YourPortalAdminModule',
									},
								],
							}),
						}),
					}),
				}],
			}),
			{
				path: 'forgot-password',
				canActivate: [NotAuthGuard],
				children: [
					{
						path: '',
						component: ForgotPasswordDialogComponent,
						outlet: 'popup',
					},
				],
			},
			{
				path: 'terms-of-use-review',
				component: TermsOfUseReviewComponent,
			},
			{
				path: '**',
				redirectTo: 'main',
			},
		],
	},
];


@NgModule({
	imports: [
		// Framework modules
		CommonModule,
		RouterModule,

		// Library modules
		CoreModule,

		// Shared modules
		FilterModule.forRoot(),
		GuardModule,
		PageStateModule,
		ModalDialogModule,

		// Page modules
		MainModule,
		PopupModule,
		ProfileModule,
		StructureModule,
	],
	declarations: [
		PageComponent,
	],
	exports: [
		PageComponent,
	],
})
export class PageModule {
	public static readonly ROUTES: Routes = ROUTES;
}
