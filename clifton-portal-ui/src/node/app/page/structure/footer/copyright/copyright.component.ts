import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as pageStore from 'app/page/shared/state/page.store';


@Component({
	selector: 'ppa-copyright',
	templateUrl: './copyright.component.html',
})
export class CopyrightComponent {

	public readonly currentYear: number = new Date().getFullYear();
	public footerTextEl$ = this.store.select(pageStore.getPageElementSingle('portal.footer.text'));


	constructor(private store: Store<pageStore.PageStore>) {}
}
