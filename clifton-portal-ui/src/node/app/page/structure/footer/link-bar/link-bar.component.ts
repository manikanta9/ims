import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { TermsOfUseContentComponent } from 'app/page/popup/terms-of-use/terms-of-use-content/terms-of-use-content.component';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-link-bar',
	templateUrl: './link-bar.component.html',
})
export class LinkBarComponent {

	public currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);
	public emailAddressEls$ = this.store.select(pageStore.getPageElementMulti('portal.email.address'));
	public pageTitleEl$ = this.store.select(pageStore.getPageElementSingle('portal.name'));


	constructor(private store: Store<pageStore.PageStore>,
	            private modalDialogService: ModalDialogService) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public showTermsOfUse(): void {
		this.modalDialogService.showComponentOnModalTemplate(TermsOfUseContentComponent, 'Portal Terms of Use', {size: 'lg'});
	}
}
