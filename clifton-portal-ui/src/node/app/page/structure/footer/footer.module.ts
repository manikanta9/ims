import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from 'app/core/core.module';
import { CopyrightComponent } from 'app/page/structure/footer/copyright/copyright.component';
import { FooterComponent } from 'app/page/structure/footer/footer.component';
import { LinkBarComponent } from 'app/page/structure/footer/link-bar/link-bar.component';


@NgModule({
	imports: [
		CommonModule,
		CoreModule,
	],
	declarations: [
		LinkBarComponent,
		CopyrightComponent,
		FooterComponent,
	],
	exports: [
		FooterComponent,
	],
})
export class FooterModule {
}
