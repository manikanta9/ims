import { Component, Input } from '@angular/core';


@Component({
	selector: 'ppa-nav-bar',
	templateUrl: './nav-bar.component.html',
})
export class NavBarComponent {

	@Input() public loggedIn = false;
	@Input() public accessGroups: string[] | undefined;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public hasAccessGroup(group: string): boolean {
		return !!(this.accessGroups && this.accessGroups.includes(group));
	}
}
