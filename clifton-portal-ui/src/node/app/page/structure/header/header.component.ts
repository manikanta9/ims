import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { DisableAnimationsAction, EnableAnimationsAction } from 'app/core/state/reducers/config.actions';
import { ArrayUtils } from 'app/core/util/array.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import * as user from 'app/portal/state/reducers/user.actions';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@Component({
	selector: 'ppa-header',
	templateUrl: 'header.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnDestroy {

	public isSimulatingEntity$: Observable<boolean> = this.store.select(pageStore.isSimulatingEntity);
	public isSimulatingUser$: Observable<boolean> = this.store.select(pageStore.isSimulatingUser);

	// User data
	public accessGroups$: Observable<string[] | undefined> = this.store.select(pageStore.getAccessGroups)
		// If simulating a user, filter access groups to those of the simulated user
		.switchMap(accessGroups => this.isSimulatingUser$.switchMapIf(simulating => simulating,
			this.store.select(pageStore.getSimulatedUserAccessGroups)
				.map(simAccessGroups => ArrayUtils.getIntersection(accessGroups || [], simAccessGroups || [])),
			Observable.of(accessGroups)))
		// If simulating an entity, filter access groups to those of the simulated entity
		.switchMap(accessGroups => this.isSimulatingEntity$.switchMapIf(simulating => simulating,
			this.store.select(pageStore.getSimulatedEntityAccessGroups)
				.map(simAccessGroups => ArrayUtils.getIntersection(accessGroups || [], simAccessGroups || [])),
			Observable.of(accessGroups)));
	public currentUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getUser);
	public simulatedEntity$: Observable<PortalEntity | undefined> = this.store.select(pageStore.getSimulatedEntityInfo);
	public simulatedUser$: Observable<PortalSecurityUser | undefined> = this.store.select(pageStore.getSimulatedUserInfo);

	/**
	 * An observable whose result is indicative of whether the application is being viewed from the perspective of an administrator.
	 */
	public isAdmin$: Observable<boolean> = !environment.administrationMode
		// For external users, use the user's admin access
		? this.store.select(pageStore.isAdmin).map(value => !!value)
		// For internal users, use the simulated user's access, or if there is no simulated user then treat the user as an administrator
		: this.isSimulatingUser$.switchMapIf(simulating => simulating, this.store.select(pageStore.getSimulatedUserIsAdmin).map(value => !!value), Observable.of(true));

	// Environment data
	public administrationMode: boolean = environment.administrationMode;
	public animationsEnabled$: Observable<boolean> = this.store.select(pageStore.getAnimationsEnabled);
	public showAnimationsToggle = environment.production === false && environment.animationsEnabled;

	private logoutRequester: RequestLoader<PortalResponse> = new RequestLoader(() => this.portalSecurityService.logout());
	private logoutSub: Subscription;


	constructor(private store: Store<pageStore.PageStore>,
				private portalSecurityService: PortalSecurityService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.logoutSub = this.logoutRequester.response$.subscribe(() => this.store.dispatch(new user.LogoutAction()));
	}


	public ngOnDestroy(): void {
		this.logoutSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async logout(): Promise<void> {
		// this.logoutRequester.load();
		// TODO: Use the requester instead; need to figure out error-handling
		await this.portalSecurityService.logoutAsPromise();
		this.store.dispatch(new user.LogoutAction());
	}


	public toggleAnimations(enable: boolean): void {
		this.store.dispatch(enable ? new EnableAnimationsAction() : new DisableAnimationsAction());
	}
}
