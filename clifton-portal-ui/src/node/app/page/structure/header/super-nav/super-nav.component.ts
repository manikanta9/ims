import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';


@Component({
	selector: 'ppa-super-nav',
	templateUrl: 'super-nav.component.html',
})
export class SuperNavComponent {
	@Input() public username: string;
	@Input() public showMenu: boolean;
	@Input() public showProfile: boolean;
	@Input() public showAdmin: boolean;
	@Input() public showYourPortalAdmin: boolean;
	@Input() public showNotifications: boolean;
	@Input() public showAnimationsToggle: boolean;
	@Input() public animationsEnabled: boolean;
	@Input() public simulatedEntity?: PortalEntity;
	@Input() public simulatedUser?: PortalSecurityUser;
	@Input() public accessGroups: string[] | undefined;
	@Output() public logout: EventEmitter<void> = new EventEmitter();
	@Output() public toggleAnimations: EventEmitter<boolean> = new EventEmitter();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public hasAccessGroup(group: string): boolean {
		return !!(this.accessGroups && this.accessGroups.includes(group));
	}
}
