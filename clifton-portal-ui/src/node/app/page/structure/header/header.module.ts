import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'app/core/core.module';
import { FilterModule } from 'app/page/shared/filter/filter.module';
import { DataBarComponent } from 'app/page/structure/header/data-bar/data-bar.component';
import { HeaderComponent } from 'app/page/structure/header/header.component';
import { NavBarComponent } from 'app/page/structure/header/nav-bar/nav-bar.component';
import { SuperNavComponent } from 'app/page/structure/header/super-nav/super-nav.component';


@NgModule({
	imports: [
		// Core modules
		CommonModule,
		CoreModule,

		// Shared modules
		FilterModule,

		// Library modules
		FormsModule,
		RouterModule,
	],
	declarations: [
		DataBarComponent,
		HeaderComponent,
		NavBarComponent,
		SuperNavComponent,
	],
	exports: [
		HeaderComponent,
	],
	entryComponents: [],
})
export class HeaderModule {
}
