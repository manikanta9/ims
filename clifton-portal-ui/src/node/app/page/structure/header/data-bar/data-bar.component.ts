import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
	selector: 'ppa-data-bar',
	templateUrl: './data-bar.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataBarComponent {
}
