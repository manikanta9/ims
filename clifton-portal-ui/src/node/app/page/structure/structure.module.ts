import { NgModule } from '@angular/core';
import { FooterModule } from 'app/page/structure/footer/footer.module';
import { HeaderModule } from 'app/page/structure/header/header.module';


@NgModule({
	imports: [
		FooterModule,
		HeaderModule,
	],
	declarations: [],
	exports: [
		FooterModule,
		HeaderModule,
	],
})
export class StructureModule {
}
