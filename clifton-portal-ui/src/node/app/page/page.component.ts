import { ChangeDetectionStrategy, Component, InjectionToken } from '@angular/core';
import { MessageService } from 'app/core/component/text/message/message.service';
import { Observable } from 'rxjs/Observable';


export const PageModalInjectionToken = new InjectionToken<any>('PageModalInjectionToken');


@Component({
	selector: 'ppa-page',
	templateUrl: 'page.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageComponent {

	public enableBackdrop$: Observable<boolean> = this.messageService
		.observe<boolean>(PageModalInjectionToken);


	constructor(private messageService: MessageService) { }
}
