import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'app/core/http/page-not-found.component';
import { PageModule } from 'app/page/page.module';
import { environment } from 'environments/environment';


const routes: Routes = [
	{
		path: '',
		children: PageModule.ROUTES,
	},
	{
		path: '**',
		component: PageNotFoundComponent,
	},
];

const routeOptions: ExtraOptions = {
	// Required unless web-server supports wildcard page-serving
	useHash: environment.routeViaHash,
};


@NgModule({
	imports: [RouterModule.forRoot(routes, routeOptions)],
	exports: [RouterModule],
})
export class AppRoutingModule {
}

