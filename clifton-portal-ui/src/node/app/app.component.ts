import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as rootStore from 'app/core/state/reducers/root.store';
import { Observable } from 'rxjs/Observable';


@Component({
	selector: 'ppa-root',
	templateUrl: './app.component.html',
})
export class AppComponent {

	public animationsEnabled$: Observable<boolean> = this.store.select(rootStore.getAnimationsEnabled);


	constructor(private store: Store<rootStore.RootStore>) {}
}
