import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieXSRFStrategy, HttpModule, XSRFStrategy } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDatepickerModule, NgbModalModule, NgbPopoverConfig, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from 'app/app-routing.module';
import { AppComponent } from 'app/app.component';
import { CoreModule } from 'app/core/core.module';
import { StateModule } from 'app/core/state/state.module';
import { PageModule } from 'app/page/page.module';
import { PortalModule } from 'app/portal/portal.module';
import { environment } from 'environments/environment';
import 'lib/rxjs';
import { ModalModule } from 'ngx-bootstrap';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { CookieModule } from 'ngx-cookie';
import { Ng2Webstorage } from 'ngx-webstorage';


export function createCookieXsrfStrategy() {
	return new CookieXSRFStrategy(environment.xsrfCookieName, environment.xsrfHeaderName);
}


// Custom NgbPopover configuration
export function createNgbPopoverConfig(): NgbPopoverConfig {
	return {
		placement: 'top',
		triggers: 'manual',
		container: '-',
	};
}


@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		// Framework modules
		BrowserModule,
		FormsModule,
		HttpClientModule,
		HttpClientXsrfModule.withOptions({
			cookieName: environment.xsrfCookieName,
			headerName: environment.xsrfHeaderName,
		}),
		HttpModule, // TODO: Remove me
		environment.animationsEnabled ? BrowserAnimationsModule : NoopAnimationsModule,

		// Library modules
		// AgGridModule.withComponents([]),
		CookieModule.forRoot(),
		ModalModule.forRoot(),
		Ng2Webstorage,
		NgbDatepickerModule.forRoot(),
		NgbModalModule.forRoot(),
		NgbPopoverModule.forRoot(),
		PopoverModule.forRoot(),

		// Core modules
		AppRoutingModule,
		CoreModule.forRoot(),
		PageModule,
		PortalModule,
		StateModule.forRoot(),
	],
	providers: [
		// TODO: Remove me once HttpModule is removed
		{provide: XSRFStrategy, useFactory: createCookieXsrfStrategy},
		{provide: NgbPopoverConfig, useFactory: createNgbPopoverConfig},
	],
	bootstrap: [
		AppComponent,
	],
})
export class AppModule {}
