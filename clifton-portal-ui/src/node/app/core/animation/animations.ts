import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';


/**
 * Note: Functions whose values are used in metadata must *only* include a return statement. This is due to restrictions in Angular's AOT compile stage.
 * Violating this will break AOT compilation.
 *
 * See related issue: https://github.com/angular/angular/issues/13132
 * See related documentation: https://gist.github.com/chuckjaz/65dcc2fd5f4f5463e492ed0cb93bca60
 */


interface AnimationOptions {
	triggerName: string;
	enterState: string;
	enterTiming: string;
	enterTimingFn: string;
	exitState: string;
	exitTiming: string;
	exitTimingFn: string;
}


const DEFAULT_ANIMATION_TIMING = '100ms';


export function applyValueOverrides(options: AnimationOptions, optionOverrides?: Partial<AnimationOptions>): AnimationOptions {
	/*
	 * This is required for AOT compilation for object overrides since Object.assign and the spread operator (...{}) cannot be used when calculating
	 * metadata during AOT compilation.
	 */
	return !optionOverrides ? options : {
		triggerName: optionOverrides.triggerName == null ? options.triggerName : optionOverrides.triggerName,
		enterState: optionOverrides.enterState == null ? options.enterState : optionOverrides.enterState,
		enterTiming: optionOverrides.enterTiming == null ? options.enterTiming : optionOverrides.enterTiming,
		enterTimingFn: optionOverrides.enterTimingFn == null ? options.enterTimingFn : optionOverrides.enterTimingFn,
		exitState: optionOverrides.exitState == null ? options.exitState : optionOverrides.exitState,
		exitTiming: optionOverrides.exitTiming == null ? options.exitTiming : optionOverrides.exitTiming,
		exitTimingFn: optionOverrides.exitTimingFn == null ? options.exitTimingFn : optionOverrides.exitTimingFn,
	};
}


export class Animations {

	// Default configurations cause a number of issues with AOT compilation for metadata evaluation, so a default configuration has been omitted here
	public static readonly ENTER_CONFIG: AnimationOptions = {
		triggerName: 'enterAnimation',
		enterState: '*',
		enterTiming: DEFAULT_ANIMATION_TIMING,
		enterTimingFn: 'ease-out',
		exitState: 'void',
		exitTiming: DEFAULT_ANIMATION_TIMING,
		exitTimingFn: 'ease-in',
	};
	public static readonly FADE_IN_CONFIG: AnimationOptions = {
		triggerName: 'fadeInAnimation',
		enterState: '*',
		enterTiming: DEFAULT_ANIMATION_TIMING,
		enterTimingFn: 'ease-out',
		exitState: 'void',
		exitTiming: DEFAULT_ANIMATION_TIMING,
		exitTimingFn: 'ease-in',
	};

	public static readonly enterAnimation = Animations.getEnterAnimation();
	public static readonly fadeInAnimation = Animations.getFadeInAnimation();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static getEnterAnimation(optionOverrides?: Partial<AnimationOptions>): AnimationTriggerMetadata {
		return Animations.makeEnterAnimation(applyValueOverrides(Animations.ENTER_CONFIG, optionOverrides));
	}


	public static getFadeInAnimation(optionOverrides?: Partial<AnimationOptions>): AnimationTriggerMetadata {
		return Animations.makeFadeInAnimation(applyValueOverrides(Animations.FADE_IN_CONFIG, optionOverrides));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static makeEnterAnimation(options: AnimationOptions): AnimationTriggerMetadata {
		return trigger(options.triggerName, [
			state(options.exitState, style({
				transform: 'translateY(-35px) scale(1.01)',
				height: 0, 'padding-top': 0, 'padding-bottom': 0, 'margin-top': 0, 'margin-bottom': 0,
				opacity: 0, overflow: 'hidden', filter: 'blur(2px)',
			})),
			state(options.enterState, style({
				transform: 'translateY(0)',
				height: '*',
				opacity: 1, overflow: 'hidden',
			})),
			transition(`${options.exitState} => ${options.enterState}`, animate(`${options.enterTiming} ${options.enterTimingFn}`)),
			transition(`${options.enterState} => ${options.exitState}`, animate(`${options.exitTiming} ${options.exitTimingFn}`)),
		]);
	}


	private static makeFadeInAnimation(options: AnimationOptions): AnimationTriggerMetadata {
		return trigger(options.triggerName, [
			state(options.exitState, style({opacity: 0, filter: 'blur(2px)'})),
			state(options.enterState, style({opacity: 1})),
			transition(`${options.exitState} => ${options.enterState}`, animate(`${options.enterTiming} ${options.enterTimingFn}`)),
			transition(`${options.enterState} => ${options.exitState}`, animate(`${options.exitTiming} ${options.exitTimingFn}`)),
		]);
	}
}
