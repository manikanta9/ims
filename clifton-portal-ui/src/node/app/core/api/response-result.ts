import { ResponseStatus } from 'app/core/api/response-status';


export class ResponseResult<T> {
	public status: ResponseStatus = ResponseStatus.NOT_STARTED;
	public message?: string;
	public result?: T;
}
