export interface BasePaginatedEntity<T> {
	start: number;
	total: number;
	rows: T[];
}
