import { Query } from 'app/core/api/query';


/**
 * @deprecated
 */
export interface QueryService {
	/**
	 * @deprecated
	 */
	get(query?: Query): Promise<any>;
}
