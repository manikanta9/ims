import { Type } from '@angular/core';
import 'reflect-metadata';


export const JsonPropertyDecoratorKey = 'ppa:json-property';


export interface JsonPropertyMetadata {
	propertyName?: string;
	clazz?: Type<any>;
	// TODO: Implement/verify
	serializeFn?: (obj: any) => any | null | undefined;
	// TODO: Implement/verify
	deserialize?: (obj: any) => any | null | undefined;
}


/**
 * TODO: Doc that this decorator is necessary for type mappings to be applied on object generation
 *
 * @param metadata
 * @return {(target:Type<any>, propertyKey:string)=>void}
 * @constructor
 */
export function JsonProperty(metadata?: JsonPropertyMetadata): (target: any, propertyKey: string) => void {
	// Attach metadata to type rather than property so that all mappings can be derived from the class directly
	return (target: any, propertyKey: string): void => Reflect.metadata(`${JsonPropertyDecoratorKey}:${propertyKey}`, metadata)(target);
}
