import { OnDestroy, OnInit } from '@angular/core';
import { LogUtils } from 'app/core/log/log.utils';
import { TransformingObservable } from 'app/core/rxjs/observable/transforming-observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


type RequestInitiator<T, S> = (input: T) => Observable<S>;
type ObservableModifier<T, S> = (input: Observable<T>) => Observable<S>;
type ErrorHandler = (caughtError: any) => void;


interface RequestLoaderConfig<T, S = void, U = S, V = T> {
	/**
	 * The initiating request for a request loader. For example: <code>(query) => this.requestService.get(query)</code>
	 */
	initiator: RequestInitiator<S, T>;
	/**
	 * TODO
	 */
	preModifier?: ObservableModifier<U, S>;
	/**
	 * TODO
	 */
	postModifier?: ObservableModifier<T, V>;
	/**
	 * The handler to execute when an error has occurred.
	 */
	errorHandler?: ErrorHandler;
	/**
	 * TODO
	 */
	initialValue?: U;
	/**
	 * The subscription configuration. The callback given in this configuration will be used to generate a subscription via the provided component's lifecycle.
	 * This subscription will automatically be cleaned up when the component is destroyed.
	 */
	subConfig?: {
		/**
		 * The component whose lifecycle events shall be used to manage the subscription.
		 */
		comp: OnInit & OnDestroy,
		callback?: (value: V) => void,
	};
}


/**
 * The {@link RequestLoader} provides a mechanism for loading triggered requests.
 *
 * This type simplifies the request process when the request does not exist or the request results shall be shared across a domain. The loading status for
 * requests triggered by instances of this type may be tracked via the result of the {@link #loading$} {@link Observable}. On completion, an error handler will
 * be triggered and the provided <tt>errorHandler</tt>, if given, will be executed.
 *
 * Requests are cancellable via the {@link #cancel} function.
 *
 * @param <T> the resulting type for the initiator
 * @param <S> the input type for the initiator, if present
 * @param <U> the input type for all pre-modifiers, if present
 * @param <V> the resulting type after all post-modifiers, if present
 */
export class RequestLoader<T, S = any, U = S, V = T> {

	/**
	 * The pause value {@link RequestLoader} loading streams. The loader will take no action as long as this is the last injected value.
	 *
	 * {@link RequestLoader} instances will reload their content using the last injected value each time their refCount changes from 0 to 1 by default. In order
	 * to prevent this, a {@link RequestLoader} can either have no item yet injected, or it can be injected with the {@link RequestLoader#PAUSE_VALUE}.
	 */
	public static readonly PAUSE_VALUE = Object.create(null);

	private loadingSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
	private cancellerSource: Subject<never> = new Subject();
	private _loading$ = this.loadingSource.asObservable();
	private _response$: TransformingObservable<U, V>;


	public get loading$(): Observable<boolean> {return this._loading$;}


	public get response$(): Observable<V> {return this._response$;}


	/**
	 * TODO
	 *
	 * @param initiatorOrConfiguration the initiating request for the loader, or a configuration object containing the initiating request with modifiers.
	 */
	public constructor(initiatorOrConfiguration: RequestInitiator<S, T> | RequestLoaderConfig<T, S, U, V>) {
		const {initiator, preModifier = void 0, postModifier = void 0, errorHandler = void 0, initialValue = void 0, subConfig = void 0} =
			typeof initiatorOrConfiguration === 'function' ? {initiator: initiatorOrConfiguration} : initiatorOrConfiguration;

		// Build response observable
		this._response$ = new TransformingObservable((obs$: Observable<U>) => obs$
			.filter(value => value !== RequestLoader.PAUSE_VALUE)
			.let(preModifier || (newObs$ => newObs$ as Observable<any> as Observable<S>))
			.do(() => this.startLoading())
			.switchMap((input: S) => initiator(input)
				.takeUntil(this.cancellerSource.do(() => this.stopLoading())),
			)
			.do(() => this.stopLoading())
			.let(postModifier || (newObs$ => newObs$ as Observable<any> as Observable<V>))
			.catch((error: any, request$: Observable<V>) => this.handleError(error, request$, errorHandler))
			.shareReplay(),
			new BehaviorSubject<U>(RequestLoader.PAUSE_VALUE));

		// Apply managed subscription
		if (subConfig) {
			this._response$.subscribeManaged(subConfig.comp, subConfig.callback);
		}

		// Apply initial request
		if (typeof initiatorOrConfiguration === 'object' && initiatorOrConfiguration.hasOwnProperty('initialValue')) {
			this._response$.next(initialValue);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public load(input?: U): Observable<V> {
		this._response$.next(input);
		return this.response$.first();
	}


	public cancel(): void {
		this.cancellerSource.next();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private startLoading(): void {
		this.loadingSource.next(true);
	}


	private stopLoading(): void {
		this.loadingSource.next(false);
	}


	private handleError(error: any, request$: Observable<V>, errorHandler?: ErrorHandler): Observable<V> {
		if (errorHandler) {
			errorHandler(error);
		}
		else {
			// TODO: Additional error-handling
			LogUtils.error('I got an error!', error);
		}
		this.stopLoading();

		/*
		 * Return the original observable so that subscribers are not terminated by the exception. This will cause the current item in the observable stream to
		 * be ignored. However, a pause value must also be emitted before the observable is re-subscribed so that a duplicate request is not made.
		 */
		this.load(RequestLoader.PAUSE_VALUE);
		return request$;
		// Returning the original observable above is causing the application to repeat the same request ad infinitum -- need to find a good way around this
		// return Observable.of(error);
		// TODO: Returning nothing right now so that we can ignore errors...
		// return Observable.empty();
	}
}
