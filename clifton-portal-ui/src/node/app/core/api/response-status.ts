export enum ResponseStatus {
	NOT_STARTED = 0,
	IN_PROCESS = 1,
	CANCELLED = 2,
	FAILED = 3,
	COMPLETED = 4,
	NO_RESULT = 5,
}
