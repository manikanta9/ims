import { OnDestroy, OnInit } from '@angular/core';
import { ManagedLifecycleActionSet } from 'app/core/component/lifecycle/managed-lifecycle-action-set';
import { Observable } from 'rxjs/Observable';
import { ISubscription, Subscription } from 'rxjs/Subscription';


export class ManagedSubscription<T> extends ManagedLifecycleActionSet<OnInit & OnDestroy> implements ISubscription {

	public get closed() {return this.subscription ? this.subscription.closed : false;}

	private subscription: Subscription;


	protected constructor(obs$: Observable<T> | (() => Observable<T>), next?: (value: T) => void, error?: (error: any) => void, complete?: () => void) {
		super({
			beforeFns: {ngOnInit: () => this.subscription = (typeof obs$ === 'function' ? obs$() : obs$).subscribe(next, error, complete)},
			fns: {ngOnDestroy: () => this.subscription.unsubscribe()},
		});
	}


	public static of<S>(obs$: Observable<S> | (() => Observable<S>), next?: (value: S) => void, error?: (error: any) => void, complete?: () => void): ManagedSubscription<S> {
		return new ManagedSubscription(obs$, next, error, complete);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public unsubscribe(): void {
		this.subscription.unsubscribe();
	}
}
