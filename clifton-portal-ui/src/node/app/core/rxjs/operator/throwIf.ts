import { Observable } from 'rxjs/Observable';


Observable.prototype.throwIf = throwIf;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		throwIf: typeof throwIf;
	}
}


/**
 * Returns an Observable that emits an error if the given condition is <tt>true</tt>.
 *
 * @param condition a function determining whether the given error should be emitted
 * @param errorGenerator the error generator which generates the error that should be emitted when the given condition evaluates to <tt>true</tt>
 * @return an Observable that emits an error if the given condition is <tt>true</tt>
 */
export function throwIf<T>(this: Observable<T>,
						   condition: (value: T) => boolean | void,
						   errorGenerator: ((value: T) => Error)): Observable<T> {
	return this.do((value: T) => {
		if (condition(value)) {
			throw errorGenerator(value);
		}
	});
}
