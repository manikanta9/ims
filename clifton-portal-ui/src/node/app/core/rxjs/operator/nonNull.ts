import { Observable } from 'rxjs/Observable';


Observable.prototype.nonNull = nonNull;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		nonNull: typeof nonNull;
	}
}


/**
 * Returns an Observable that emits all items emitted by the source Observable that are not <tt>null</tt> or <tt>undefined</tt>.
 *
 * @return {Observable} an Observable that emits items from the source Observable that are not <tt>null</tt> or <tt>undefined</tt>.
 */
export function nonNull<T>(this: Observable<T>): Observable<T> {
	return this.filter((value: T) => value != null);
}
