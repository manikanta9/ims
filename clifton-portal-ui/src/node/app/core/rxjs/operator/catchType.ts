import { Type } from '@angular/core';
import { Observable, ObservableInput } from 'rxjs/Observable';


Observable.prototype.catchType = catchType;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		catchType: typeof catchType;
	}
}


/**
 * Returns an Observable that handles errors of the specified type.
 *
 * @param type the type of error to handle
 * @param selector the {@link Observable#catch}-type selector used to handle the given error
 * @return an Observable using the result of the given selector if an error was caught, or the original observable otherwise
 */
export function catchType<T, U extends Error, V>(this: Observable<T>, type: Type<U>, selector: (err: U, caught: Observable<T>) => ObservableInput<V>): Observable<T | V> {
	return this.catch((err: any, caught: Observable<T>) => {
		if (err instanceof type) {
			return selector(err, caught);
		}
		else {
			throw err;
		}
	});
}
