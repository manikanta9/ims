import { Observable } from 'rxjs/Observable';


Observable.prototype.truthy = truthy;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		truthy: typeof truthy;
	}
}


/**
 * Returns an Observable that emits truthy values only.
 *
 * @return an Observable that emits truthy values only
 */
export function truthy<T>(this: Observable<T>): Observable<T> {
	return this.filter(value => !!value);
}
