import { LogUtils } from 'app/core/log/log.utils';
import { Observable } from 'rxjs/Observable';


Observable.prototype.debug = debug;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		debug: typeof debug;
	}
}


/**
 * Returns an Observable that logs the given message and the current value to the console and then continues as usual.
 *
 * @param msgParts the message to be logged alongside the current stream value
 * @return an Observable that logs debug information to the console and then continues normally
 */
export function debug<T>(this: Observable<T>, ...msgParts: any[]): Observable<T> {
	return this.do((value: T) => LogUtils.log(...msgParts, value));
}
