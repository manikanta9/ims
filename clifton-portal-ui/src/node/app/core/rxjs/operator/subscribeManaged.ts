import { OnDestroy, OnInit } from '@angular/core';
import { ManagedSubscription } from 'app/core/rxjs/observer/managed-subscription';
import { Observable } from 'rxjs/Observable';


Observable.prototype.subscribeManaged = subscribeManaged;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		subscribeManaged: typeof subscribeManaged;
	}
}


/**
 * TODO
 *
 * @return
 */
export function subscribeManaged<T>(this: Observable<T>, component: OnInit & OnDestroy, next?: (value: T) => void, error?: (error: any) => void, complete?: () => void): ManagedSubscription<T> {
	const managedSub: ManagedSubscription<T> = ManagedSubscription.of(this, next, error, complete);
	managedSub.attach(component);
	return managedSub;
}
