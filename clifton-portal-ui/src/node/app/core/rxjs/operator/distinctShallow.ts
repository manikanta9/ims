import { CompareUtils } from 'app/core/util/compare.utils';
import { Observable } from 'rxjs/Observable';


Observable.prototype.distinctShallow = distinctShallow;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		distinctShallow: typeof distinctShallow;
	}
}


/**
 * Returns an Observable that emits all items emitted by the source Observable that are distinct by shallow comparison from the previous emitted item.
 *
 * @return {Observable} an Observable that emits items from the source Observable with distinct values
 */
export function distinctShallow<T>(this: Observable<T>): Observable<T> {
	return this.distinctUntilChanged((oldVal, newVal) => CompareUtils.isEqualShallow(oldVal, newVal));
}
