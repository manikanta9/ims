import { Observable, ObservableInput } from 'rxjs/Observable';


Observable.prototype.mergeMapIf = mergeMapIf;


declare module 'rxjs/Observable' {
	/* tslint:disable-next-line:no-shadowed-variable : TSLint bug -- type is not truly shadowed */
	interface Observable<T> {
		mergeMapIf: typeof mergeMapIf;
	}
}


export function mergeMapIf<T, U>(this: Observable<T>, condition: (value: T) => boolean | void, thenSource: Observable<U>): Observable<T | U>;
export function mergeMapIf<T, U, V>(this: Observable<T>, condition: (value: T) => boolean | void, thenSource: Observable<U>,
									elseSource: Observable<V>): Observable<U | V>;

/**
 * Returns an Observable that emits the result of its <tt>thenSource</tt> if the given condition is <tt>true</tt> and its <tt>elseSource</tt> otherwise. If no
 * <tt>elseSource</tt> is provided then the source observable will be used in its place.
 *
 * This operator uses the {@link mergeMap} operator to continue previous observables triggered by this operator.
 *
 * @param condition a function determining whether the <tt>thenSource</tt> or the <tt>elseSource</tt> should be used
 * @param thenSource the Observable to use if the condition evaluates to <tt>true</tt>
 * @param elseSource the Observable to use if the condition evaluates to <tt>false</tt>
 * @return an Observable that chains to the <tt>thenSource</tt> if the condition evaluates to <tt>true</tt>, and to the <tt>elseSource</tt> otherwise
 */
export function mergeMapIf<T, U, V>(this: Observable<T>,
									condition: (value: T) => boolean | void,
									thenSource: ObservableInput<U>,
									elseSource?: ObservableInput<V>): Observable<T | U | V> {
	const elseSourceValue: ObservableInput<V> | ObservableInput<T> = elseSource || this;
	return this.mergeMap<T, (T | U | V)>(value => condition(value) ? thenSource : elseSourceValue);
}
