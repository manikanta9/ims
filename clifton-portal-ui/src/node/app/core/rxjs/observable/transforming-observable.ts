import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


/**
 * An {@link Observable} backed by a {@link Subject} which may receive a different type than the stream output type.
 *
 * {@link Subject} objects act as multi-cast providers which receive a supplied value and then emit it to all observers. This type builds on this paradigm by
 * allowing intermediate transformation operators such that the emitted value is different than the supplied value.
 *
 * To use this class, execute all desired transformations using the constructor lambda parameter.
 *
 * @param <T> the source value type
 * @param <S> the output value type
 */
// TODO: Change to extends Subject and try to override next? Will TS allow us to change parameter type on next?
export class TransformingObservable<T, S> extends Observable<S> {

	private backingObserver: Observable<S>;


	/**
	 * @param operators a function which shall perform intermediate transformations to values before they are emitted
	 * @param backingSubject the backing subject which will be used to trigger the observable stream
	 */
	constructor(operators: (ob$: Observable<T>) => Observable<S>,
	            private backingSubject: Subject<T> = new Subject()) {
		super((observer: Observer<S>) => {
			const subscription: Subscription = this.backingObserver
				.subscribe((value: S) => observer.next(value));
			return () => subscription.unsubscribe();
		});
		// Attach the operators outside of the constructor so that they are added a single time, rather than once for each subscriber
		this.backingObserver = this.backingSubject.let(operators);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public next(value?: T): TransformingObservable<T, S> {
		this.backingSubject.next(value);
		return this;
	}
}
