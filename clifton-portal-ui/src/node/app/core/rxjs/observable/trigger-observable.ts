import { TransformingObservable } from 'app/core/rxjs/observable/transforming-observable';


/**
 * An {@link Observable} type which can be triggered with no value via its {@link #next} function.
 */
export class TriggerObservable<T = never> extends TransformingObservable<any, T> {
	public next(): TriggerObservable<T> {
		return super.next();
	}
}
