import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { addReducers, ReducerMap, RootStore } from 'app/core/state/reducers/root.store';


@Injectable()
export class StateService {

	constructor(private store: Store<RootStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * TODO
	 *
	 * @param reducers
	 */
	public addReducers<T extends RootStore>(reducers: ReducerMap<T>): void {
		this.store.replaceReducer(addReducers<T>(reducers));
	}
}
