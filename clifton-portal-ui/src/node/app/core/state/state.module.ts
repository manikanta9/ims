import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterStoreModule } from '@ngrx/router-store/src/router-store-module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as rootStore from 'app/core/state/reducers/root.store';
import { StateService } from 'app/core/state/state.service';
import { environment } from 'environments/environment';


@NgModule({
	imports: [
		// Library modules
		StoreModule.provideStore(rootStore.rootReducer),
		RouterStoreModule.connectRouter(),

		// Development modules
		environment.production ? [] : StoreDevtoolsModule.instrumentOnlyWithExtension({maxAge: 500}),
	],
	declarations: [],
})
/**
 * Note: The StateModule must <b>only</b> be imported by the application root module.
 *
 * This module imports providers from additional libraries that must only be imported a single time. Since Angular AOT does not resolve providers determined
 * from function calls (https://github.com/rangle/angular-2-aot-sandbox#aot-dos-and-donts) we are unable to reliably pass the provider elements to the "forRoot"
 * function caller.
 */
export class StateModule {
	public static forRoot(): ModuleWithProviders {
		return {
			ngModule: StateModule,
			providers: [
				StateService,
			],
		};
	}
}
