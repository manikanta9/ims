import { ActionReducer } from '@ngrx/store';
import * as root from 'app/core/state/reducers/root.actions';


export * from 'app/core/state/reducers/root.actions';


// Reducers
export function rootMetaReducer<T>(reducer: ActionReducer<T>): ActionReducer<T> {
	const nextReducer: ActionReducer<T> = (state: T, action: root.Actions): T => {
		switch (action.type) {
			case root.RESET_STATE:
				// Allow individual reducers to build their own state from defaults
				return reducer((void 0)!, action);
			default:
				// TS bug: Cannot assert never when only one case since the type is not a discriminated union
				// TypeUtils.assertNever(action);
				return reducer(state, action);
		}
	};
	return nextReducer;
}
