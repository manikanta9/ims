import { compose } from '@ngrx/core/compose';
import { Action, ActionReducer, combineReducers } from '@ngrx/store';
import * as config from 'app/core/state/reducers/config.reducer';
import * as root from 'app/core/state/reducers/root.reducer';
import * as router from 'app/core/state/reducers/router.reducer';
import { environment } from 'environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { createSelector } from 'reselect';


// State
export interface RootStore {
	router: router.RouterState;
	config: config.ConfigState;
}


// Map of reducer per store slice
const rootReducerMap: ReducerMap<RootStore> = {
	router: router.routerReducer,
	config: config.configReducer,
};


// Memoized selectors
export const getRouterState: (state: RootStore) => router.RouterState = (state: RootStore) => state.router;
export const getConfigState: (state: RootStore) => config.ConfigState = (state: RootStore) => state.config;

export const getQueryParams = createSelector(getRouterState, router.getQueryParams);
export const getAnimationsEnabled = createSelector(getConfigState, config.getAnimationsEnabled);


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


// Meta-reducers are applied to all reducers
const metaReducers: ((reducers: any) => ActionReducer<any>)[] = [
	...environment.production ? [storeFreeze] : [],
	root.rootMetaReducer,
	combineReducers,
];

// The produced app reducer function
const rootReducerFn: ActionReducer<RootStore> = addReducers(rootReducerMap);


/**
 * Type declaration for a map of reducers for a given state.
 */
export type ReducerMap<T> = Partial<{[sliceName in keyof T]: ActionReducer<T[sliceName]>}>;


/**
 * The map of active reducers.
 *
 * <b>Note:</b> This variable is mutable since it is used to track the latest set of active reducers. However, the object to which it is assigned should not be mutated in itself.
 * Rather, create a copy of the object.
 *
 * @type {ReducerMap<{}>}
 */
let activeReducers: ReducerMap<{}>;


/**
 * Generates a reducer by concatenating the given map of reducers to the existing reducers.
 *
 * @param reducers the reducers to be added
 * @return the composed reducer
 */
export function addReducers<T extends RootStore>(reducers: ReducerMap<T> = {}): ActionReducer<T> {
	activeReducers = Object.assign({}, activeReducers, reducers);
	return compose(...metaReducers)(activeReducers) as ActionReducer<T>;
}


// The named forwarding function (named function is required for AOT)
export function rootReducer(state: RootStore, action: Action): RootStore {
	return rootReducerFn(state, action);
}
