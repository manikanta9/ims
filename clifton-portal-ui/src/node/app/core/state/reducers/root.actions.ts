// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';


// Action names
export const RESET_STATE = '[Store] Reset';


// Action types
export class ResetStateAction implements Action {
	public readonly type = RESET_STATE;
}


// Action type union
export type Actions =
	| ResetStateAction
	;
