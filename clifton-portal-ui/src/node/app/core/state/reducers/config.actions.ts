// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';


// Action names
export const ENABLE_ANIMATIONS = '[Config] Enable Animations';
export const DISABLE_ANIMATIONS = '[Config] Disable Animations';


// Action types
export class EnableAnimationsAction implements Action {
	public readonly type = ENABLE_ANIMATIONS;
}


export class DisableAnimationsAction implements Action {
	public readonly type = DISABLE_ANIMATIONS;
}


// Action type union
export type Actions =
	| EnableAnimationsAction
	| DisableAnimationsAction
	;
