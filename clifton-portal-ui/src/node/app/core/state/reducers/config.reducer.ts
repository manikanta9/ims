import { ActionReducer } from '@ngrx/store';
import * as config from 'app/core/state/reducers/config.actions';
import { TypeUtils } from 'app/core/util/type.utils';
import { environment } from 'environments/environment';


export * from 'app/core/state/reducers/config.actions';


// State
export const storeName = 'config';


export interface ConfigState {
	animations: {
		enabled: boolean,
	};
}


export const initialState: ConfigState = {
	animations: {
		enabled: environment.animationsEnabled,
	},
};


// Reducers
export const configReducer: ActionReducer<ConfigState> = (state: ConfigState = initialState, action: config.Actions): ConfigState => {
	switch (action.type) {
		case config.ENABLE_ANIMATIONS:
			return {
				...state,
				animations: {
					...state.animations,
					enabled: true,
				},
			};
		case config.DISABLE_ANIMATIONS:
			return {
				...state,
				animations: {
					...state.animations,
					enabled: false,
				},
			};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export const getAnimationsEnabled: (state: ConfigState) => boolean = (state: ConfigState) => state.animations.enabled;
