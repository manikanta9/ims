import { RouterState } from '@ngrx/router-store/src/reducer';
import { UrlUtils } from 'app/core/util/url.utils';


export * from '@ngrx/router-store/src/reducer';
export * from '@ngrx/router-store/src/actions';


// Selectors
export const getQueryParams = (state: RouterState): any => UrlUtils.getQueryParams(state.path);
