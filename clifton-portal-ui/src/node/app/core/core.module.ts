import { CommonModule } from '@angular/common';
import { ErrorHandler, ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ContextDirective } from 'app/core/component/context/context.directive';
import { DialogComponent } from 'app/core/component/dialog/dialog.component';
import { AutoTooltipDirective } from 'app/core/component/text/auto-tooltip.directive';
import { MessageService } from 'app/core/component/text/message/message.service';
import { ReadMoreComponent } from 'app/core/component/text/read-more/read-more.component';
import { TextContentPipe } from 'app/core/component/text/text-content.pipe';
import { ElementFinderService } from 'app/core/dom/element-finder.service';
import { ErrorHandlerService } from 'app/core/exception/error-handler.service';
import { ComboComponent } from 'app/core/form/combo.component';
import { DateUsParserFormatter } from 'app/core/form/date-us-parser-formatter';
import { FormService } from 'app/core/form/form.service';
import { PageNotFoundComponent } from 'app/core/http/page-not-found.component';
import { ModalModule } from 'ngx-bootstrap';
import { LoadingContainerComponent } from './component/loading-container/loading-container.component';
import { LoadingSpinnerComponent } from './component/loading-spinner/loading-spinner.component';
import { TableComponent } from './component/table/table.component';
import { EmailLinkComponent } from './component/text/email-link/email-link.component';
import { ExternalLinkDirective } from './component/text/external-link/external-link.directive';
import { PhoneNumberPipe } from './component/text/phone-number.pipe';
import { SnakeCasePipe } from './component/text/snake-case.pipe';
import { ElementIdDirective } from './form/element-id.directive';


const CORE_DIRECTIVES = [
	AutoTooltipDirective,
	ComboComponent,
	ContextDirective,
	DialogComponent,
	ElementIdDirective,
	EmailLinkComponent,
	ExternalLinkDirective,
	LoadingContainerComponent,
	LoadingSpinnerComponent,
	PageNotFoundComponent,
	PhoneNumberPipe,
	ReadMoreComponent,
	SnakeCasePipe,
	TableComponent,
	TextContentPipe,
];


@NgModule({
	imports: [
		// Core modules
		CommonModule,

		// Library modules
		FormsModule,
		ModalModule,
	],
	declarations: [
		CORE_DIRECTIVES,
	],
	exports: [
		CORE_DIRECTIVES,
	],
})
export class CoreModule {
	public static forRoot(): ModuleWithProviders {
		return {
			ngModule: CoreModule,
			providers: [
				{provide: ErrorHandler, useClass: ErrorHandlerService},
				ElementFinderService,
				FormService,
				MessageService,
				{provide: NgbDateParserFormatter, useClass: DateUsParserFormatter},
			],
		};
	}
}
