import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { LogUtils } from 'app/core/log/log.utils';
import { ModalDirective, ModalOptions } from 'ngx-bootstrap';


@Component({
	selector: 'ppa-dialog',
	templateUrl: './dialog.component.html',
})
export class DialogComponent implements AfterViewInit {

	@Input() public autoShow = true;
	@Input() public allowClose = true;
	@Input() public allowSubmit = true;
	@Input() public allowCancel = true;
	@Input() public allowDecline = false;
	@Input() public header: string;
	@Input() public backdrop = true;
	@Input() public size: 'small' | 'medium' | 'large' = 'medium';
	@Input() public submitText = 'Submit';
	@Input() public cancelText = 'Cancel';
	@Input() public declineText = 'Decline';
	@Input() public submitTooltip: string;
	@Input() public cancelTooltip: string;
	@Input() public declineTooltip: string;
	@Input() public loading = false;
	@Input() public errorMessage: string;
	@Input() public submitEnabled = true;
	@Input() public cancelEnabled = true;
	@Input() public declineEnabled = false;
	// @Input() public config?: Object = {};

	@Output() public onSubmit = new EventEmitter<any>();
	@Output() public onCancel = new EventEmitter<any>();
	@Output() public onClose = new EventEmitter<any>();
	@Output() public onDecline = new EventEmitter<any>();

	@ViewChild(ModalDirective)
	public modalDialog: ModalDirective;
	@ViewChild('modalDialogWindow')
	public modalDialogWindow: ElementRef;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngAfterViewInit(): void {
		if (this.autoShow) {
			this.show();
		}

		this.onSubmit.subscribe(() => {
			LogUtils.log('ppa-dialog onsubmit emitted...');
		});
	}


	public show(): void {
		this.modalDialog.show();
	}


	public hide(): void {
		this.modalDialog.hide();
	}


	public getConfig(): ModalOptions {
		return {
			show: this.autoShow,
			keyboard: this.allowClose,
			backdrop: this.backdrop ? 'static' : false,
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private applyPosition(...args: any[]): void {
		const element: HTMLElement = this.modalDialogWindow.nativeElement;
		const topMargin: number = (window.innerHeight - element.offsetHeight) / 2;
		element.style.top = `${topMargin}px`;
	}
}
