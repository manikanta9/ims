import { Component, Input } from '@angular/core';
import { Animations } from 'app/core/animation/animations';


@Component({
	selector: 'ppa-loading-container',
	templateUrl: './loading-container.component.html',
	styles: [`
		:host {
			display: block;
			width: 100%;
			position: relative;
		}
	`],
	animations: [Animations.enterAnimation, Animations.fadeInAnimation],
})
export class LoadingContainerComponent {
	@Input() public loading = false;
	@Input() public renderHidden = false;
	@Input() public hiddenHeight = 0;
	@Input() public animationType: 'enter' | 'fade' | 'none' = 'enter';
}
