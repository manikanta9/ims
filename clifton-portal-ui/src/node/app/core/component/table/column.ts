export interface Column {
	click?: (value: object) => void;
	header: string;
	image?: string | ((value: object) => string);
	imageTitle?: string | ((value: object) => string);
	maxLength?: number; // Optional maximum length for the value of this column.  If exceeded, will truncate and append ...
	property?: string | ((value: object) => string);
	style?: { [prop: string]: string };
}
