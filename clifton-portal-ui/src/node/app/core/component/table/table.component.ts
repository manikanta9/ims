import { Component, Input } from '@angular/core';
import { Column } from 'app/core/component/table/column';
import { Row } from 'app/core/component/table/row';
import { StringUtils } from 'app/core/util/string.utils';
import * as _ from 'lodash';


@Component({
	selector: 'ppa-table',
	templateUrl: './table.component.html',
})
export class TableComponent {

	@Input() public columnList: Column[] = [];
	@Input() public enableSelect = false;


	@Input()
	set valueList(valueList: object[]) {
		// Convert value list into row list
		const oldRowList = this.rowList;
		this.rowList = _.map(valueList, value => ({value}));

		// Re-apply last selected values
		if (oldRowList && this.rowList && oldRowList.every(oldRow => this.rowList.some(row => oldRow.value === row.value))) {
			const lastSelectedValues: object[] = _.chain(oldRowList)
				.filter((row: Row) => row.selected)
				.map((row: Row) => row.value)
				.value();
			_.chain(this.rowList)
				.filter((row: Row) => lastSelectedValues.includes(row.value))
				.forEach((row: Row) => row.selected = true)
				.value();
			this.updateSelectBoxes();
		}
	}


	public rowList: Row[] = [];

	// ngModel fields
	public selectedAll: boolean;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// set each checkbox to the 'selectAll' value
	public onSelectAllChanged(eventValue: boolean): void {
		this.rowList.forEach(item => item.selected = eventValue);
	}


	public onSelectChanged(): void {
		this.updateSelectBoxes();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// check if all are selected or not and set the 'selectAll' value as appropriate (if all are selected, 'selectAll' should automatically be selected)
	public updateSelectBoxes(): void {
		// noinspection PointlessBooleanExpressionJS
		this.selectedAll = this.rowList.every(row => !!row.selected);
	}


	public getRowValue(row: Row, column: Column): any {
		const result = (typeof column.property === 'function') ? column.property(row.value) : _.get(row.value, column.property!) as string;
		return StringUtils.truncate(result || '', column.maxLength || Number.MAX_SAFE_INTEGER);
	}


	public getRowImageSource(row: Row, column: Column): string {
		return (typeof column.image === 'function') ? column.image(row.value) : _.get(row.value, column.image!) as string;
	}


	public getRowImageTitle(row: Row, column: Column): string {
		return (typeof column.imageTitle === 'function') ? column.imageTitle(row.value) : _.get(row.value, column.imageTitle!) as string;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getSelected(): object[] {
		return _.chain(this.rowList)
			.filter(row => row.selected)
			.map(row => row.value)
			.value();
	}
}
