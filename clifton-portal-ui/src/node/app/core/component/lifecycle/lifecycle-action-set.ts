import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, DoCheck, OnChanges, OnDestroy, OnInit } from '@angular/core';


export interface LifecycleActionSet {
	ngOnChanges: OnChanges['ngOnChanges'];
	ngOnInit: OnInit['ngOnInit'];
	ngDoCheck: DoCheck['ngDoCheck'];
	ngOnDestroy: OnDestroy['ngOnDestroy'];
	ngAfterContentInit: AfterContentInit['ngAfterContentInit'];
	ngAfterContentChecked: AfterContentChecked['ngAfterContentChecked'];
	ngAfterViewInit: AfterViewInit['ngAfterViewInit'];
	ngAfterViewChecked: AfterViewChecked['ngAfterViewChecked'];
}
