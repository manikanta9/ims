import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, DoCheck, OnChanges, OnDestroy, OnInit } from '@angular/core';


export type LifecycleInterface =
	| OnChanges
	| OnInit
	| DoCheck
	| OnDestroy
	| AfterContentInit
	| AfterContentChecked
	| AfterViewInit
	| AfterViewChecked
	;
