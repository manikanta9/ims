import { LifecycleActionSet } from 'app/core/component/lifecycle/lifecycle-action-set';


export type LifecycleEvent = keyof LifecycleActionSet;

