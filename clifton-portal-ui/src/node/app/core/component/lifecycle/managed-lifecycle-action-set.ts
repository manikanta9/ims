import { LifecycleActionSet } from 'app/core/component/lifecycle/lifecycle-action-set';
import { LifecycleInterface } from 'app/core/component/lifecycle/lifecycle-interface';
import { ComponentUtils } from 'app/core/util/component.utils';


export class ManagedLifecycleActionSet<T extends LifecycleInterface> {

	// Use KeyIntersection here to restrict keys from T to available keys in LifecycleActionSet
	private readonly beforeFns?: Partial<Pick<LifecycleActionSet, KeyIntersection<T, LifecycleActionSet>>>;
	private readonly fns?: Partial<Pick<LifecycleActionSet, KeyIntersection<T, LifecycleActionSet>>>;


	protected constructor(actionSet: {
		beforeFns?: ManagedLifecycleActionSet<T>['beforeFns'],
		fns?: ManagedLifecycleActionSet<T>['fns'],
	}) {
		this.beforeFns = Object.assign({}, actionSet.beforeFns);
		this.fns = Object.assign({}, actionSet.fns);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public attach(component: T): void {
		if (this.beforeFns) {
			for (const eventKey of Object.keys(this.beforeFns) as (keyof ManagedLifecycleActionSet<T>['beforeFns'])[]) {
				ComponentUtils.attachLifecycleEventBefore(component, eventKey, this.beforeFns[eventKey]);
			}
		}
		if (this.fns) {
			for (const eventKey of Object.keys(this.fns) as (keyof ManagedLifecycleActionSet<T>['fns'])[]) {
				ComponentUtils.attachLifecycleEvent(component, eventKey, this.fns[eventKey]);
			}
		}
	}
}
