import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';


@Directive({
	selector: '[ppaContext]',
})
/**
 * The {@link ContextDirective} structure directive provides contextual variables. This allows variables to be evaluated a single time within a context, preventing the need for expensive operations to be evaluated multiple times in some cases.
 *
 *
 * @example
 * // Take "getFilteredValueList" to be an expensive operation.
 * <div *ngIf="getFilteredValueList() && getFilteredValueList().length > 0">
 *     <div *ngFor="let value of getFilteredValueList()">
 *         <div>Value: {{ value }}</div>
 *     </div
 * </div>
 * <ng-template #emptyValueList>
 *     <div>No values exist</div>
 * </ng-template>
 *
 * // Can be replaced with:
 * <ng-container *ppaContext="let valueList = valueList; using: {valueList: getFilteredValueList().length}">
 *       <div *ngIf="valueList && valueList.length > 0">
 *         <div *ngFor="let value of valueList">
 *             <div>Value: {{ value }}</div>
 *         </div>
 *     </div>
 *     <ng-template #emptyValueList>
 *         <div>No values exist</div>
 *     </ng-template>
 * </ng-container>
 */
export class ContextDirective {

	private _context: { [key: string]: any };


	@Input()
	public set ppaContextUsing(value: { [key: string]: any }) {
		this._context = value;
		this.updateView();
	}


	constructor(private templateRef: TemplateRef<any>,
				private viewContainerRef: ViewContainerRef) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private updateView() {
		this.viewContainerRef.clear();
		this.viewContainerRef.createEmbeddedView(this.templateRef, this._context);
	}
}
