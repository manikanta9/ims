import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';


@Component({
	selector: 'ppa-read-more',
	templateUrl: 'read-more.component.html',
	styles: [`
		.collapsed {
			overflow: hidden;
		}
	`],
})
// TODO: Personalize this
export class ReadMoreComponent implements AfterViewInit {

	@Input() public text: string;
	@Input() public maxHeight = 100;

	public isCollapsed = false;
	public isCollapsable = false;

	@ViewChild('textBlock') private readonly textBlock: ElementRef;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngAfterViewInit(): void {
		const currentHeight = this.textBlock.nativeElement.offsetHeight;
		// Collapsible only if the contents make container exceed the max height
		if (currentHeight > this.maxHeight) {
			this.isCollapsed = true;
			this.isCollapsable = true;
		}
	}
}

