import { InjectionToken } from '@angular/core';


export interface Message {
	token: InjectionToken<any>;
	value: any;
}
