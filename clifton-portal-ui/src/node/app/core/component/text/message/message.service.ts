import { Injectable, InjectionToken } from '@angular/core';
import { Message } from 'app/core/component/text/message/message';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class MessageService {

	private readonly messageEmitter$ = new Subject<Message>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public push(token: InjectionToken<any> | any, value?: any): void {
		this.messageEmitter$.next({token, value});
	}


	public observe<T>(token: any): Observable<T> {
		return this.messageEmitter$
			.filter(messageObj => messageObj.token === token)
			.map(messageObj => messageObj.value);
	}


	public subscribe(token: any, action: (value?: any) => void, errorAction?: (error: any) => void, completeAction?: () => void): Subscription {
		return this.observe(token).subscribe(action, errorAction, completeAction);
	}
}
