import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
	name: 'ppaSnakeCase',
})
export class SnakeCasePipe implements PipeTransform {

	private static readonly CASE_BOUNDARY: RegExp = /(\w)(?=[A-Z])/g;
	private static readonly PRE_NUMERIC_BOUNDARY: RegExp = /((?=\w)[^0-9])(?=[0-9]+)/g;
	private static readonly POST_NUMERIC_BOUNDARY: RegExp = /([0-9])(?=[a-zA-Z])/g;
	private static readonly PREFIX_SPECIAL_CHARACTERS: RegExp = /^(_|[^\w])+/g;
	private static readonly SUFFIX_SPECIAL_CHARACTERS: RegExp = /(_|[^\w])+$/g;
	private static readonly SEPARATOR_SPECIAL_CHARACTERS: RegExp = /([_-]|[^\w])+(?=(?=\w)[^_-])/g;


	public transform(value: any, args?: any): any {
		const strValue = typeof value === 'string' ? value : String(value);
		return strValue
			.replace(SnakeCasePipe.CASE_BOUNDARY, '$1-')
			.replace(SnakeCasePipe.PRE_NUMERIC_BOUNDARY, '$1-')
			.replace(SnakeCasePipe.POST_NUMERIC_BOUNDARY, '$1-')
			.replace(SnakeCasePipe.PREFIX_SPECIAL_CHARACTERS, '')
			.replace(SnakeCasePipe.SUFFIX_SPECIAL_CHARACTERS, '')
			.replace(SnakeCasePipe.SEPARATOR_SPECIAL_CHARACTERS, '-')
			.toLowerCase();
	}
}
