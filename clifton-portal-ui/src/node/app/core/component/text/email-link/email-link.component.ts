import { Component, Input } from '@angular/core';
import { FormUtils } from 'app/core/util/form.utils';


@Component({
	selector: 'ppa-email-link',
	templateUrl: './email-link.component.html',
})
export class EmailLinkComponent {

	@Input()
	public text: string;

	@Input()
	public mailto: string;

	@Input()
	public target: {
		cc?: string;
		bcc?: string;
		subject?: string;
		body?: string;
	};

	@Input()
	public classList: string[] = [];


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getLinkTarget(): string {
		return `mailto:${this.mailto}?` + FormUtils.serialize(this.target);
	}
}
