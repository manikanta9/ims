import { SnakeCasePipe } from 'app/core/component/text/snake-case.pipe';


const SPECIAL_CHARACTERS = '!@#$%^&*()_+=[]{}\\|"\';:/?.>,<`~';
const TEST_STRING = `${SPECIAL_CHARACTERS}AbcDef I am a-test_string ${SPECIAL_CHARACTERS} abc${SPECIAL_CHARACTERS}abc 12345 ABC123` +
	` ABcd1234 123ABC 123abc abcDEF abc123def abc-123-def abc_123_def ${SPECIAL_CHARACTERS} - ${SPECIAL_CHARACTERS}`;
const TEST_STRING_RESULT = 'abc-def-i-am-a-test-string-abc-abc-12345-a-b-c-123' +
	'-a-bcd-1234-123-a-b-c-123-abc-abc-d-e-f-abc-123-def-abc-123-def-abc-123-def';

describe('SnakeCasePipe', () => {
	let pipe: SnakeCasePipe;
	beforeEach(() => pipe = new SnakeCasePipe());

	it('returns the empty string when empty', make('', ''));
	it('returns null when null', make(null, 'null'));
	it('returns undefined when undefined', make(void 0, 'undefined'));
	it('returns the string representation of an array', make(['abc', 'def'], 'abc-def'));
	it('returns the string representation of an object', make({}, 'object-object'));
	it('changes spaces to hyphens', make('a   a', 'a-a'));
	it('trims spaces', make('   a   ', 'a'));
	it('lowercases all characters', make('AAA aaa AAA', 'a-a-a-aaa-a-a-a'));
	it('splits TitleCase words', make('AaaAaaAaa AaaAaaAaa', 'aaa-aaa-aaa-aaa-aaa-aaa'));
	it('splits camelCase words', make('aaaAaaAaa aaaAaaAaa', 'aaa-aaa-aaa-aaa-aaa-aaa'));
	it('splits around numbers', make('a0a0a 0a0a0', 'a-0-a-0-a-0-a-0-a-0'));
	it('splits around special characters', make(`a${SPECIAL_CHARACTERS}a`, 'a-a'));
	it('does not include prefix or suffix special characters', make(`${SPECIAL_CHARACTERS} a ${SPECIAL_CHARACTERS}`, 'a'));
	it('returns the empty string with no boundaries exist', make(`${SPECIAL_CHARACTERS}-${SPECIAL_CHARACTERS}-${SPECIAL_CHARACTERS}-`, ''));
	it('returns a correctly formatted string', make(TEST_STRING, TEST_STRING_RESULT));

	function make(value: any, expected: any): () => void {
		return () => expect(pipe.transform(value)).toEqual(expected);
	}
});
