import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';


@Directive({
	/* tslint:disable-next-line:directive-selector */
	selector: 'a[href]:not([target])',
})
export class ExternalLinkDirective implements OnInit {

	constructor(private elementRef: ElementRef,
				private renderer: Renderer2) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		const el: HTMLAnchorElement = this.elementRef.nativeElement;
		if (!el.target) {
			this.renderer.setAttribute(el, 'target', '_blank');
		}
	}
}
