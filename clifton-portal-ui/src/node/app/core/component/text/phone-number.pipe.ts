import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
	name: 'ppaPhoneNumber',
})
export class PhoneNumberPipe implements PipeTransform {
	public transform(value: Nullable<string>, args?: any): any {
		// replace all dashes and dots with spaces to follow our phone number convention
		return (value || '').replace(/[-.]/g, ' ');
	}
}
