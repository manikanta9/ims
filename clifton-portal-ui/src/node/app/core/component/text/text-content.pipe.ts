import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
	name: 'ppaTextContent',
})
export class TextContentPipe implements PipeTransform {

	public transform(value: string, args?: any): any {
		return this.applySuperscript(value);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private applySuperscript(value: string): string {
		// if (value == null) {
		// 	// TODO: It looks like exceptions are getting swallowed up by a promise somewhere. Figure out where and then delete this block
		// 	LogUtils.warn('about to throw npe... for value', value);
		// 	return value.replace(/[®]/g, '<sup>$&</sup>');
		// }
		return value && value.replace(/[®]/g, '<sup>$&</sup>');
	}
}
