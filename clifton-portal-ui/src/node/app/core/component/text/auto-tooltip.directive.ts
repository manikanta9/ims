import { Directive, OnDestroy, OnInit } from '@angular/core';
import { LogUtils } from 'app/core/log/log.utils';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@Directive({
	selector: '[ppaAutoTooltip]',
})
/*
 * TODO:
 * This doesn't actually do anything right now. We need to make this display a tooltip on hoverover. The issue here is determining *how* we want to display the
 * tooltip. Do we want to create a new component on the page and then set position to be relative to cursor? Do we want to manually add HTML to the page? Do we
 * want to integrate with ngx-bootstrap tooltips?
 *
 * Whatever we do for the element, it would ideally be a flywheel element that wouldn't need to be regenerated each time that the tooltip is changed.
 */
export class AutoTooltipDirective implements OnInit, OnDestroy {

	private mouseMoveSub: Subscription;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		LogUtils.log('AutoTooltip oninit');
		const mouseMove$ = Observable.fromEvent<MouseEvent>(document, 'mousemove');
		this.mouseMoveSub = mouseMove$
			.throttleTime(200)
			.subscribe((event: MouseEvent) => this.handleMouseMove(event));
	}


	public ngOnDestroy(): void {
		LogUtils.log('destroying mouseMoveSub');
		this.mouseMoveSub.unsubscribe();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private handleMouseMove(event: MouseEvent) {
		// TODO: Something
	}
}
