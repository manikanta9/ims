import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
	selector: 'ppa-modal-dialog',
	templateUrl: './modal-dialog.component.html',
})
export class ModalDialogComponent {

	@Input() public dynamicComponent: any;

	@Input() public title: string;
	@Input() public message: string;
	@Input() public modalDisplayStyle: string;


	constructor(public activeModal: NgbActiveModal) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getDisplayStyle(): string {
		return this.modalDisplayStyle;
	}
}
