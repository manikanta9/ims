import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalDialogComponent } from 'app/core/modal/modal-dialog.component';


@Injectable()
export class ModalDialogService {

	constructor(private modalService: NgbModal) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public showError(error: string, title = 'Error'): NgbModalRef {
		return this.openModalDialog(error, title, 'error');
	}


	public showWarning(warning: string, title = 'Warning'): NgbModalRef {
		return this.openModalDialog(warning, title, 'warning');
	}


	public showMessage(message: string, title: string): NgbModalRef {
		return this.openModalDialog(message, title, 'info');
	}


	public showConfirmation(message: string, title: string): NgbModalRef {
		// TODO: Add support for confirmation call back
		return this.openModalDialog(message, title, 'confirmation');
	}


	public showComponent(content: any, options?: NgbModalOptions): NgbModalRef {
		return this.modalService.open(content, Object.assign({backdrop: 'static'}, options));
	}


	public showComponentOnModalTemplate(content: any, title: string, options?: NgbModalOptions): NgbModalRef {
		return this.openModalDialog('', title, 'plain', options, content);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private openModalDialog(message: string, title: string, modalDisplayStyle: string, options?: NgbModalOptions, displayComponent?: any): NgbModalRef {
		const modalRef = this.modalService.open(ModalDialogComponent, Object.assign({backdrop: 'static'}, options));

		modalRef.componentInstance.title = title;
		modalRef.componentInstance.message = message;
		modalRef.componentInstance.modalDisplayStyle = modalDisplayStyle;
		if (displayComponent) {
			modalRef.componentInstance.dynamicComponent = displayComponent;
		}
		return modalRef;
	}
}
