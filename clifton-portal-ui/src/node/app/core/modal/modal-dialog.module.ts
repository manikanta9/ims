import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalDialogComponent } from 'app/core/modal/modal-dialog.component';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';


@NgModule({
	imports: [
		CommonModule,

		// Library modules
		NgbModalModule,
	],
	declarations: [
		ModalDialogComponent,
	],
	providers: [
		ModalDialogService,
	],
	entryComponents: [ModalDialogComponent],
})
export class ModalDialogModule {
}
