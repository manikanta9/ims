export class StringUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the matched group within the given string for the given regular expression. This method assumes that the given regular expression has one and only
	 * one grouping.
	 *
	 * @param str the string to compare against the pattern
	 * @param pattern the regular expression with which to test the given string
	 * @return the first matched pattern group value, or <tt>null</tt> if the group was not matched
	 */
	public static getMatch(str: string, pattern: RegExp): string | null {
		const matches = pattern.exec(str);
		return (matches && matches[1]) ? matches[1] : null;
	}


	/**
	 * Determines if the given string is empty in a null-safe fashion.
	 *
	 * @param value the string to evaluate
	 * @return <tt>true</tt> if the string is <tt>null</tt>, <tt>undefined</tt>, or empty, or <tt>false</tt> otherwise
	 */
	public static isEmpty(value: string | null | undefined): boolean {
		return !value || value.length <= 0;
	}


	/**
	 * Truncates the given string to the provided maximum length. If the maximum length is exceeded, all remaining characters will be
	 * stripped and replaced with the string <tt>...</tt>.
	 *
	 * @param value the string to truncate
	 * @param maxLength the maximum length of the resulting string, not including the concatenated truncation indicator
	 * @return the truncated string with an appended truncation indicator, or the original string if the maximum length was not exceeded
	 */
	public static truncate(value: string, maxLength: number): string {
		return (value.length > maxLength)
			? `${value.substring(0, maxLength)}...`
			: value;
	}


	/**
	 * Concatenates all strings in the given array into a comma-delimited list, using commas and the specified conjunction prior to the final element. If only two elements are
	 * given, then no comma will be used, as according to standard Oxford comma rules.
	 *
	 * For example, <code>concatenateWithConjunction(['apples', 'oranges', 'pears'], 'or')</code> will return <code>'apples, oranges, or pears'</code>.
	 *
	 * @param values the strings to concatenate
	 * @param conjunction the conjunction to use before the final element
	 * @return the concatenated string
	 */
	public static concatWithConjunction(values: string[], conjunction: string = 'and'): string {
		let result;
		if (values.length === 0) {
			result = '';
		}
		else if (values.length === 1) {
			result = values[0];
		}
		else if (values.length === 2) {
			result = `${values[0]} ${conjunction} ${values[1]}`;
		}
		else {
			result = `${values.slice(0, values.length - 1).join(', ')}, ${conjunction} ${values[values.length - 1]}`;
		}
		return result;
	}
}
