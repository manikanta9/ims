import { LogUtils } from 'app/core/log/log.utils';
import { TypeUtils } from 'app/core/util/type.utils';
import * as _ from 'lodash';


export class DateUtils {

	//noinspection JSUnusedLocalSymbols
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static now(): Date {
		return new Date();
	}


	public static formatDateShort(date: Date | string): string {
		const dateObj = typeof date === 'string' ? DateUtils.parseDate(date) : date;
		const year = dateObj.getFullYear();
		const month = dateObj.getMonth() + 1;
		const day = dateObj.getDate();
		return `${month}/${day}/${year}`;
	}


	public static formatDateInputField(date: Date | string): string {
		const dateObj = typeof date === 'string' ? DateUtils.parseDate(date) : date;
		const year = _.padStart(`${dateObj.getFullYear()}`, 4, '0');
		const month = _.padStart(`${dateObj.getMonth() + 1}`, 2, '0');
		const day = _.padStart(`${dateObj.getDate()}`, 2, '0');
		return `${year}-${month}-${day}`;
	}


	public static parseDate(dateStr: string): Date {
		return new Date(dateStr);
	}


	public static getRelativeDate(increment: number, units: 'days' | 'months' | 'years' = 'days', baseDate: Date = new Date()): Date {
		const relativeDate = new Date(baseDate.getTime());
		switch (units) {
			case 'days':
				relativeDate.setDate(relativeDate.getDate() + increment);
				break;
			case 'months':
				relativeDate.setMonth(relativeDate.getMonth() + increment);
				break;
			case 'years':
				relativeDate.setFullYear(relativeDate.getFullYear() + increment);
				break;
			default:
				TypeUtils.assertNever(units);
				LogUtils.warn(`Unrecognized calendar unit type: ${units}. Defaulting to 'days'.`);
				relativeDate.setDate(relativeDate.getDate() + increment);
		}
		return relativeDate;
	}


	public static isValidDate(date: Nullable<Date>): date is Date {
		return date != null && !Number.isNaN(date.getFullYear());
	}
}
