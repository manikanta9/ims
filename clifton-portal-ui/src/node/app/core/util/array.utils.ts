import * as _ from 'lodash';


export class ArrayUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the unique set of values from the given array.
	 *
	 * If an identifier function is provided, that function will be used to retrieve the identifier used in equality comparisons. If no such function is provided, the given values
	 * themselves will be compared for uniqueness.
	 *
	 * @param values the array of elements from which a unique array will be extracted
	 * @param identifierFn the identifier function for determining the value corresponding to the element which shall be used to determine uniqueness
	 * @return {T[]} the array of unique elements from the source array
	 */
	public static getUnique<T>(values: T[], identifierFn: (val: T) => any = _.identity): T[] {
		return _.uniqBy(values, identifierFn);
	}


	/**
	 * Gets the numeric values of the entries for the given enum type.
	 *
	 * @param enumType the enum type
	 * @return {number[]} the list of numeric enum entry values
	 */
	public static getEnumValues(enumType: { [key: string]: any }): number[] {
		return Object.keys(enumType)
			.map(key => Number(enumType[key]))
			.filter(value => !isNaN(value));
	}


	/**
	 * Gets the names of the entries for the given enum type.
	 *
	 * @param enumType the enum type
	 * @return {string[]} the list of enum entry value names
	 */
	public static getEnumNames(enumType: { [key: string]: any }): string[] {
		return Object.keys(enumType)
			.filter(key => !isNaN(Number(enumType[key])));
	}


	/**
	 * Gets the intersection of the two provided lists.
	 *
	 * @param list1 the first list
	 * @param list2 the second list
	 * @return the list of items that exist in both lists using the order of the elements provided by <tt>list1</tt>
	 */
	public static getIntersection<T>(list1: T[], list2: T[]): T[] {
		// Remove same items from each list, pushing to new list, until at least one list is empty
		const dupList1: T[] = [...list1];
		const dupList2: T[] = [...list2];
		const newList: T[] = [];
		let val;
		while (val = dupList1.pop()) {
			const val2Index = dupList2.indexOf(val);
			if (val2Index > -1) {
				dupList2.splice(val2Index, 1);
				newList.push(val);
			}
		}
		return newList;
	}


	/**
	 * Returns <tt>true</tt> if the given lists each contain the exact same elements, regardless of order.
	 *
	 * @param list1 the first list
	 * @param list2 the second list
	 * @return <tt>true</tt> if the given lists each contain all of the same elements, or <tt>false</tt> otherwise
	 */
	public static isEqualUnordered<T>(list1: T[], list2: T[]): boolean {
		// Guard-clause
		if (list1.length !== list2.length) {
			return false;
		}

		// Remove same items from each list until empty
		const dupList1: T[] = [...list1];
		const dupList2: T[] = [...list2];
		let val;
		while (val = dupList1.pop()) {
			const val2Index = dupList2.indexOf(val);
			if (val2Index < 0) {
				return false;
			}
			dupList2.splice(val2Index, 1);
		}
		return dupList1.length === 0 && dupList2.length === 0;
	}


	/**
	 * Returns <tt>true</tt> if the given lists each contain the exact same elements in the same order.
	 *
	 * @param list1 the first list
	 * @param list2 the second list
	 * @return <tt>true</tt> if the given lists each contain all of the same elements, or <tt>false</tt> otherwise
	 */
	public static isEqual<T>(list1: T[], list2: T[]): boolean {
		// Guard-clause
		if (list1.length !== list2.length) {
			return false;
		}

		// Find any non-equal element
		const len = list1.length;
		for (let i = 0; i < len; i++) {
			if (list1[i] !== list2[i]) {
				return false;
			}
		}
		return true;
	}
}
