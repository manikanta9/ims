import { Injector, Type } from '@angular/core';
import { ModuleAlreadyLoadedException } from 'app/core/exception/module-already-loaded-exception';
import { TypeUtils } from 'app/core/util/type.utils';
import * as _ from 'lodash';


export class ExceptionUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Throws an exception if the given parent module is present. This should be used within module constructors to verify that a parent instance does not
	 * already exist for modules which should only be loaded once (e.g., modules with providers).
	 *
	 * To use this function, inject the module into its own constructor with the <code>@Optional()</code> and <code>@SkipSelf()</code> decorators. This will
	 * inject the parent injector's instance of the module if it has already been created. Then call this function using the injected value.
	 *
	 * @example
	 * <pre>
	 * @NgModule({
	 *     providers: [
	 *         MySharedService
	 *     ]
	 * })
	 * export class MySharedModule {
	 *     constructor(@Optional() @SkipSelf() instance: MySharedModule,
	 *                 @Optional() injector: Injector) {
	 *         ExceptionUtils.throwIfAlreadyLoaded(MySharedModule, instance, injector);
	 *     }
	 * }
	 * </pre>
	 *
	 * @param type the module type
	 * @param instance the instance of the module from the parent injector
	 * @param injector the parent injector
	 */
	public static throwIfAlreadyLoaded<T>(type: Type<T>, instance: T, injector?: Injector): void {
		if (instance) {
			// _moduleType is an undocumented property of NgModuleRef_ which points to the type of the module
			const injectorName = _.get(injector, '_moduleType.name', '');
			throw new ModuleAlreadyLoadedException(type.constructor.name, injectorName);
		}
	}


	/**
	 * Asserts that the a line of code is unreachable. If the statement is reached at runtime then an exception will be thrown.
	 *
	 * This function takes advantage of TypeScript type assertions. Compilation depends on control flow exhaustively handling available types for the given
	 * value.
	 *
	 * @param value the value whose types have been exhausted via control-flow
	 * @param errorMsg the error message
	 * @return throws exception
	 * @see TypeUtils#assertNever
	 */
	public static assertUnreachable(value: never, errorMsg = `Unexpected value: ${value}`): never {
		TypeUtils.assertNever(value);
		throw new Error(errorMsg);
	}
}
