import { LifecycleInterface } from 'app/core/component/lifecycle/lifecycle-interface';
import { FunctionUtils } from 'app/core/util/function.utils';


export class ComponentUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static attachLifecycleEvent<T extends LifecycleInterface, S extends keyof T>(component: T, event: S, fn: T[S]): void {
		(component as any)[event] = FunctionUtils.concat((component as any)[event], fn);
	}


	public static attachLifecycleEventBefore<T extends LifecycleInterface, S extends keyof T>(component: T, event: S, fn: T[S]): void {
		(component as any)[event] = FunctionUtils.concat(fn, (component as any)[event]);
	}
}
