import { URLSearchParams } from '@angular/http';
import { convertToParamMap, ParamMap, Params } from '@angular/router';
import { StringUtils } from 'app/core/util/string.utils';


export interface UrlInfo {
	pathname?: string;
	queryParams?: Params;
}


export class UrlUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Parses the given URL string into a {@link URL} object. The URL string must be an absolute URL, including protocol.
	 *
	 * @param fullUrl the TODO
	 * @return {URL}
	 */
	public static parseUrl(fullUrl: string): URL {
		let url: URL;
		if (URL.prototype) {
			url = new URL(fullUrl);
		}
		else {
			/* IE compatibility */
			// Get search params separately
			const searchParams = new URLSearchParams();
			const searchParamsStr: string = StringUtils.getMatch(fullUrl, /^[^#]+\?(.*)/) || '';
			const queryParams: Params = UrlUtils.parseQueryParams(searchParamsStr);
			for (const param of Object.keys(queryParams)) {
				searchParams.set(param, queryParams[param]);
			}
			url = {
				hash: StringUtils.getMatch(fullUrl, /#(.*)$/) || '',
				host: StringUtils.getMatch(fullUrl, /^\w*:\/\/([^\/#]*)/) || '',
				hostname: StringUtils.getMatch(fullUrl, /^\w*:\/\/([^\/#:]*)/) || '',
				href: fullUrl,
				origin: StringUtils.getMatch(fullUrl, /^(\w*:\/\/[^\/#]*)/) || '',
				password: '',
				pathname: StringUtils.getMatch(fullUrl, /\w*:\/\/[^\/#]*(\/[^#?]*)/) || '',
				port: StringUtils.getMatch(fullUrl, /\w*:\/\/[^\/#:](:[0-9]+)/) || '',
				protocol: StringUtils.getMatch(fullUrl, /^(\w*:)\/\/.*/) || '',
				search: StringUtils.getMatch(fullUrl, /^[^?]+(\?.*)/) || '',
				searchParams: searchParams!,
				username: '',
			};
		}
		return url;
	}


	public static parsePartialUrl(partialUrl: string): Partial<URL> {
		// Get search params separately
		const searchParams: URLSearchParams = new URLSearchParams();
		const searchParamsStr: string = StringUtils.getMatch(partialUrl, /^(?:[^#]+)?\?(.*)/) || '';
		const queryParams: Params = UrlUtils.parseQueryParams(searchParamsStr);
		for (const param of Object.keys(queryParams)) {
			searchParams.set(param, queryParams[param]);
		}
		return {
			hash: StringUtils.getMatch(partialUrl, /#(.*)$/) || void 0,
			host: StringUtils.getMatch(partialUrl, /^(?:\w*:\/\/)?([^\/#]*)/) || void 0,
			hostname: StringUtils.getMatch(partialUrl, /^(?:\w*:\/\/)?([^\/#:]*)/) || void 0,
			href: partialUrl,
			origin: StringUtils.getMatch(partialUrl, /^(?:\w*:\/\/[^\/#]*)/) || void 0,
			password: void 0,
			pathname: StringUtils.getMatch(partialUrl, /^(?:\w*:\/\/)?(?:[^\/#]*)?(\/[^#?]*)/) || void 0,
			port: StringUtils.getMatch(partialUrl, /^(?:\w*:\/\/)?(?:[^\/#:])?(:[0-9]+)/) || void 0,
			protocol: StringUtils.getMatch(partialUrl, /^(\w*:)(?:\/\/.*)?/) || void 0,
			search: StringUtils.getMatch(partialUrl, /^(?:[^?]+)?(\?.*)/) || void 0,
			searchParams,
			username: void 0,
		};
	}


	public static getUrlInfo(partialUrl: string): UrlInfo {
		const url = UrlUtils.parsePartialUrl(partialUrl);
		return {
			pathname: url.pathname,
			queryParams: UrlUtils.parseQueryParams((url.search || '').slice(1)),
		};
	}


	public static getQueryParams(uri: string): ParamMap {
		const queryParams: Params = {};

		// Determine if query string exists
		const queryParamStart = uri.indexOf('?');
		const hashStart = uri.indexOf('#');
		const hasQueryString = queryParamStart > -1 && (hashStart < 0 || hashStart > queryParamStart);

		if (hasQueryString) {
			// Get query string
			const queryStringAndHash = uri.slice(queryParamStart + 1);
			const queryHashStart = queryStringAndHash.indexOf('#');
			const queryString = (queryHashStart > -1) ? queryStringAndHash.slice(0, queryHashStart) : queryStringAndHash;

			// Extract key-value pairs
			if (queryString != null) {
				Object.assign(queryParams, UrlUtils.parseQueryParams(queryString));
			}
		}

		return convertToParamMap(queryParams);
	}


	public static parseQueryParams(queryString: string): Params {
		const queryParams: Params = {};
		for (const kvPair of queryString.split('&')) {
			const keyEnd = kvPair.indexOf('=');
			let key: string;
			let value: string | null;
			if (keyEnd > -1) {
				key = decodeURIComponent(kvPair.slice(0, keyEnd));
				value = decodeURIComponent(kvPair.slice(keyEnd + 1));
			}
			else {
				key = decodeURIComponent(kvPair);
				value = null;
			}
			queryParams[key] = value;
		}
		return queryParams;
	}
}
