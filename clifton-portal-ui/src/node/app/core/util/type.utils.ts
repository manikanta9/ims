export class TypeUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Asserts that the given value is of type <tt>never</tt>.
	 *
	 * <b>This is a compile-time assertion utility. There is no runtime behavior associated with this function.</b>
	 *
	 * TypeScript will assert that the given value is of type <tt>never</tt> wherever this function is called. <tt>never</tt>-assertions are useful when
	 * verifying that control flows exhaustively handle the available types.
	 *
	 * @example
	 * <pre>
	 * type Shape = 'triangle' | 'square' | 'rectangle' | 'circle';
	 *
	 * function printSides(shape: Shape): void {
	 *     switch (shape) {
	 *         case 'triangle':
	 *             LogUtils.log(3);
	 *             break;
	 *         case 'square':
	 *         case 'rectangle':
	 *             LogUtils.log(4);
	 *             break;
	 *         default:
	 *             // A TypeScript compiler error will be raised here because the 'circle' case has not been handled
	 *             TypeUtils.assertNever(shape);
	 *     }
	 * }
	 * </pre>
	 *
	 * @param value the value which shall be asserted as a <tt>never</tt>-value
	 */
	public static assertNever(value: never): void {
		// Do nothing
	}
}
