import { LogUtils } from 'app/core/log/log.utils';


export class CompareUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Performs a flexible emptiness evaluation for the given value.
	 * <p>
	 * Determines if the given value is an empty string, {@code null}, or {@code undefined}.
	 *
	 * @param value the value to evaluate
	 * @return {boolean} {@code true} if the given value is empty, {@code null}, or {@code undefined}, or {@code false} otherwise
	 */
	public static isEmptyValue(value?: any): boolean {
		return value === '' || value == null;
	}


	/**
	 * Determines if one and only one of the provided arguments are truthy.
	 *
	 * @param values the values to check
	 * @return {boolean} {@code true} if one and only one of the given values is truthy, or {@code false} otherwise
	 */
	public static mutuallyExclusive(...values: any[]): boolean {
		return values && (values.filter(val => !!val).length === 1);
	}


	/**
	 * Determines if the given objects are equal using the given identity field.
	 *
	 * If both objects are <tt>null</tt> then this function will return <tt>true</tt>.
	 *
	 * @param obj1 the first object
	 * @param obj2 the second object
	 * @param {S} idField the identity field which shall be used to compare the objects
	 * @return {boolean} <tt>true</tt> if the objects have the same value for the identity field, or <tt>false</tt> otherwise
	 */
	public static isEqualByField<T, S extends string>(obj1: Nullable<{[key in S]?: T}>, obj2: Nullable<{[key in S]?: T}>, idField: S): boolean {
		// Check if the objects have differing nullity
		if (obj1 == null) {
			return obj1 === obj2;
		}
		else if (obj2 == null) {
			return false;
		}

		// Compare object field values
		return obj1[idField] === obj2[idField];
	}


	/**
	 * Determines if the given objects are equal using the given identity property.
	 *
	 * If both objects are <tt>null</tt> then this function will return <tt>true</tt>.
	 *
	 * @param obj1 the first object
	 * @param obj2 the second object
	 * @param {S} propGetter the getter for the identity property which shall be used to compare the objects
	 * @return {boolean} <tt>true</tt> if the objects have the same value for the identity property, or <tt>false</tt> otherwise
	 */
	public static isEqualByProperty<T extends () => any, S extends string>(obj1: Nullable<{[key in S]: T}>, obj2: Nullable<{[key in S]: T}>, propGetter: S): boolean {
		// Check if the objects have differing nullity
		if (obj1 == null) {
			return obj1 === obj2;
		}
		else if (obj2 == null) {
			return false;
		}

		// Compare object property getter function call values
		return obj1[propGetter]() === obj2[propGetter]();
	}


	/**
	 * Compares the enumerable properties of the given objects for reference equality.
	 *
	 * If both objects are <tt>null</tt> then this function will return <tt>true</tt>.
	 *
	 * @param obj1 the first object
	 * @param obj2 the second object
	 * @return {boolean} <tt>true</tt> if the object properties are equal by reference, or <tt>false</tt> otherwise
	 */
	public static isEqualShallow(obj1: any, obj2: any): boolean {
		// Check if the properties have differing nullity
		if (obj1 == null) {
			return obj1 === obj2;
		}
		else if (obj2 == null) {
			return false;
		}

		// Validate value types
		if (typeof obj1 !== 'object' || typeof obj2 !== 'object') {
			LogUtils.warn('Attempted to perform a shallow object equality comparison on non-object values:', obj1, obj2);
			return false;
		}

		// Compare enumerable properties for reference equality
		const obj1Keys = Object.keys(obj1);
		const obj2Keys = Object.keys(obj2);
		return (obj1Keys.length === obj2Keys.length)
			&& obj1Keys.every(key => obj1[key] === obj2[key]);
	}


	/**
	 * Performs a locale-dependent comparison of the given string values. The returned value follows the usual <tt>compare</tt> pattern:
	 * <ul>
	 * <li>-1: The first value is <i>less than</i> the second value
	 * <li>0: The first value is <i>equal to</i> the second value
	 * <li>1: The first value is <i>greater than</i> the second value
	 * </ul>
	 *
	 * @param {string} val1 the first value
	 * @param {string} val2 the second value
	 * @return {number} the <tt>compare</tt> result for the first value compared with the second value
	 */
	public static compareValues(val1: string, val2: string): number {
		return val1.localeCompare(val2);
	}
}
