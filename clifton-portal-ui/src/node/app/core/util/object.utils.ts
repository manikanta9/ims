import { Type } from '@angular/core';
import { JsonUtils } from 'app/core/util/json.utils';
import * as flatnest from 'flatnest';
import * as _ from 'lodash';


interface ListGroupingRuleOptions<T> {
	keyField?: string;
	valueField?: string;
	groupingFn: (obj: T) => number | string | symbol;
	modifierFn?: (obj: any) => any;
}


export class ObjectUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static isPrimitive(obj: any): boolean {
		return !obj || _.includes(['string', 'undefined', 'boolean', 'number'], typeof obj);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a flattened version of the given object. This generates a flat single-level object by bringing all nested properties to the root level. The new
	 * key for each property is the string representation of the property's original dot-separated and/or index-referencing path.
	 *
	 * @param obj the object to flatten
	 * @return {{}} the generated flattened object
	 */
	public static flatten(obj: object): { [key: string]: any } {
		return flatnest.flatten(obj);
	}


	/**
	 * Generates the given object using the given configuration. The generated object has the prototype of the type specified, allowing type metadata to be
	 * applied for the resulting object.
	 *
	 * @param type the type of the object to generate
	 * @param config the configuration with which the object shall be instantiated
	 * @param extraConfigs the additional configuration objects to apply, in order of increasing priority
	 * @return {T} the generated object
	 */
	public static generate<T>(type: Type<T>, config: T, ...extraConfigs: Array<{[P in keyof T]?: T[P]}>): T {
		const fullConfig = _.assign({}, config, ...extraConfigs);
		// Disallow array-generation via this method
		return JsonUtils.transformToClient(fullConfig, type) as T;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Coalesces the given values and returns the first non-<tt>null</tt> value. If no values exist or no non-<tt>null</tt> value exists, <tt>null</tt> is returned.
	 * @param values the values to coalesce
	 * @return {any|null} the first non-<tt>null</tt> value, or <tt>null</tt> if no non-<tt>null</tt> value exists
	 */
	public static coalesce<T>(...values: T[]): T | null {
		if (values == null) {
			return null;
		}

		const firstNonNull: any = values.find(val => val != null);
		// Change undefined to null
		return firstNonNull == null ? null : firstNonNull;
	}


	/**
	 * Creates a hierarchy using the given list of grouping functions. Each consecutive function shall create another layer in the hierarchy.
	 *
	 * @example
	 * <pre>
	 * const valueList: any[] = [
	 *     {label: 'a', category: {label: 'cat 1', parent: {label: 'super-cat 1'}}},
	 *     {label: 'b', category: {label: 'cat 2', parent: {label: 'super-cat 1'}}},
	 *     {label: 'c', category: {label: 'cat 1', parent: {label: 'super-cat 2'}}},
	 *     {label: 'd', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}},
	 *     {label: 'e', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}}
	 * ];
	 * groupHierarchyBy(valueList, (value) => value.category.parent.label, (value) => value.category.label);
	 *
	 * // Result:
	 * // {
	 * //     'super-cat 1': {
	 * //         'cat 1': [
	 * //             {label: 'a', category: {label: 'cat 1', parent: {label: 'super-cat 1'}}}
	 * //         ],
	 * //         'cat 2': [
	 * //             {label: 'b', category: {label: 'cat 2', parent: {label: 'super-cat 1'}}}
	 * //         ]
	 * //     },
	 * //     'super-cat 2': [
	 * //         'cat 1': [
	 * //             {label: 'c', category: {label: 'cat 1', parent: {label: 'super-cat 2'}}}
	 * //         ],
	 * //         'cat 2': [
	 * //             {label: 'd', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}},
	 * //             {label: 'e', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}}
	 * //         ]
	 * //     ]
	 * // }
	 * </pre>
	 *
	 * @param obj the object to convert to a hierarchy
	 * @param groupingFns the grouping functions which shall produce the key at each level in the hierarchy, applied in the order provided
	 * @return {any} the hierarchy object
	 * @see ObjectUtils#groupHierarchyListBy
	 */
	public static groupHierarchyBy<T>(obj: T[], ...groupingFns: ((obj: T) => number | string | symbol)[]): any {
		if (_.isEmpty(groupingFns)) {
			return obj;
		}
		return _.chain(obj)
			.groupBy(_.head(groupingFns))
			.mapValues(value => ObjectUtils.groupHierarchyBy(value as Array<T>, ...groupingFns.slice(1)))
			.value();
	}


	/**
	 * Creates a hierarchy using the given list of grouping configurations. Each consecutive configuration shall create another layer in the hierarchy.
	 *
	 * This is similar to {@link ObjectUtils#groupHierarchyBy}, except that each grouping level is transformed into a list of objects. Each of these objects has a key field and a
	 * value field, which contains the list of objects grouped at that level. This allows each level to correspond to an individual entity type with a common schema.
	 *
	 * @example
	 * <pre>
	 * const valueList: any[] = [
	 *     {label: 'a', category: {label: 'cat 1', parent: {label: 'super-cat 1'}}},
	 *     {label: 'b', category: {label: 'cat 2', parent: {label: 'super-cat 1'}}},
	 *     {label: 'c', category: {label: 'cat 1', parent: {label: 'super-cat 2'}}},
	 *     {label: 'd', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}},
	 *     {label: 'e', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}}
	 * ];
	 * groupHierarchyListBy(valueList, {
	 *     keyField: 'superCategory',
	 *     valueField: 'children',
	 *     groupingFn: (value) => value.category.parent.label
	 * }, {
	 *     keyField: 'category',
	 *     valueField: 'valueList',
	 *     groupingFn: (value) => value.category.label
	 * });
	 *
	 * // Result:
	 * // [{
	 * //     superCategory: 'super-cat 1',
	 * //     children: [{
	 * //         category: 'cat 1',
	 * //         valueList: [
	 * //             {label: 'a', category: {label: 'cat 1', parent: {label: 'super-cat 1'}}}
	 * //         ]
	 * //     }, {
	 * //         category: 'cat 2',
	 * //         valueList: [
	 * //             {label: 'b', category: {label: 'cat 2', parent: {label: 'super-cat 1'}}}
	 * //         ]
	 * //     ]
	 * // }, {
	 * //     superCategory: 'super-cat 2',
	 * //     children: [{
	 * //         category: 'cat 1',
	 * //         valueList: [
	 * //             {label: 'c', category: {label: 'cat 1', parent: {label: 'super-cat 2'}}}
	 * //         ]
	 * //     }, {
	 * //         category: 'cat 2',
	 * //         valueList: [
	 * //             {label: 'd', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}},
	 * //             {label: 'e', category: {label: 'cat 2', parent: {label: 'super-cat 2'}}}
	 * //         ]
	 * //     ]
	 * // }]
	 * </pre>
	 *
	 * @param obj the object to convert to a list of hierarchy objects
	 * @param groupingRules
	 * @return {any} the list of hierarchy objects
	 * @see ObjectUtils#groupHierarchyBy
	 */
	public static groupHierarchyListBy<T>(obj: T[], ...groupingRules: ListGroupingRuleOptions<T>[]): any[] {
		const currentRule = _.head(groupingRules);
		if (!currentRule) {
			return obj;
		}
		return _.chain(obj)
			.groupBy(currentRule.groupingFn)
			.mapValues(value => ObjectUtils.groupHierarchyListBy(value as Array<T>, ...groupingRules.slice(1)))
			.mapValues((value, key) => ({
				[currentRule.keyField || 'key']: key,
				[currentRule.valueField || 'value']: value,
			}))
			.values()
			.map(result => currentRule.modifierFn ? currentRule.modifierFn(result) : result)
			.value();
	}
}

