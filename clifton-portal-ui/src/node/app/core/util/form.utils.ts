import { AbstractControl, FormControl } from '@angular/forms';
import { FieldValidationMessage } from 'app/core/form/field-validation-message';
import { FieldValidationMessages } from 'app/core/form/field-validation-messages';
import { FormErrors } from 'app/core/form/form-errors';
import { FormValidationMessages } from 'app/core/form/form-validation-messages';
import { LogUtils } from 'app/core/log/log.utils';
import { JsonUtils } from 'app/core/util/json.utils';
import { ObjectUtils } from 'app/core/util/object.utils';
import * as _ from 'lodash';


export interface EmailLink {
	mailto: string;
	text: string;
	classList?: string[];
	subject?: string;
	cc?: string | string[];
	bcc?: string | string[];
	body?: string;
}


export class FormUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static serialize(obj: object): string {
		const jsonObj: object = JsonUtils.transformToServer(obj);
		return FormUtils.getQueryString(jsonObj);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates an {@link HTMLFormElement} object with inputs and values for the given object.
	 *
	 * @param obj the object from which to generate a form element
	 * @param action the action to perform on form submission, if desired
	 * @return {HTMLFormElement} the generated form element
	 */
	public static generateForm(obj: object, action?: string): HTMLFormElement {
		const form: HTMLFormElement = document.createElement('form');
		form.method = 'POST';
		if (action) {
			form.action = action;
		}

		const flatObj = ObjectUtils.flatten(obj);
		for (const key of Object.keys(flatObj)) {
			const input: HTMLInputElement = document.createElement('input');
			input.name = key;
			input.value = flatObj[key];
			form.appendChild(input);
		}
		return form;
	}


	/**
	 * Generates a query string from the provided object.
	 *
	 * @param obj the object from which to generate the query string
	 * @return {string} the object transformed to a query string
	 */
	public static getQueryString(obj: object): string {
		const flatObj = ObjectUtils.flatten(obj);
		const queryString = Object.entries(flatObj)
			.filter(([key, value]) => value != null)
			.map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
			.join('&');
		return queryString;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates an email link element using the given data. The element can be appended directly or inserted using its HTML via the {@link HTMLElement#outerHTML} property.
	 */
	public static generateEmailLink(linkData: EmailLink): HTMLElement {
		const anchor = document.createElement('a');
		const linkParams = FormUtils.serialize({
			subject: linkData.subject,
			cc: Array.isArray(linkData.cc) ? linkData.cc.join(';') : linkData.cc,
			bcc: Array.isArray(linkData.bcc) ? linkData.bcc.join(';') : linkData.bcc,
			body: linkData.body,
		});
		anchor.href = `mailto:${linkData.mailto}?${linkParams}`;
		anchor.innerText = linkData.text;
		return anchor;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * TODO
	 *
	 * @param control
	 * @param validationMessages
	 * @return
	 */
	public static getValidationError<T>(control: AbstractControl, validationMessages: Nullable<FieldValidationMessages>): Nullable<string> {
		// Guard-clause: No errors or no validation messages
		if (!control || !control.dirty || control.valid || !validationMessages) {
			return null;
		}

		// Get the error message
		let errorMessage: string | null;
		if (typeof validationMessages === 'string') {
			// Use generic validation message
			errorMessage = validationMessages;
		}
		else if (Object.keys(control.errors).find(errorType => !!validationMessages[errorType])) {
			// Concatenate all applicable error messages with newlines
			errorMessage = Object.keys(control.errors)
				.filter(errorType => !!validationMessages[errorType]) // Ignore errors without validation messages
				.map(errorType => getErrorTypeMessage(validationMessages[errorType], control.errors![errorType]))
				.join('<br/>');
		}
		else if (validationMessages.other) {
			// Use default validation message
			errorMessage = validationMessages.other; // TODO: Re-apply
			// errorMessage = validationMessages.other as string;
		}
		else {
			// Validation messages exist, but no matching or default message was found
			LogUtils.warn('An error was found for a field with no matching validation messages. Field: ', control);
			errorMessage = null;
		}
		return errorMessage;

		/**
		 * Gets the message for the given error type using the provided {@link FieldValidationMessage}.
		 */
		function getErrorTypeMessage(validationMessage: FieldValidationMessage, errorValue: any): string {
			return typeof validationMessage === 'string' ? validationMessage : validationMessage(errorValue);
		}
	}


	/**
	 * TODO
	 *
	 * @param form
	 * @param validationMessages
	 * @return
	 */
	public static getValidationErrors<T>(form: AbstractControl, validationMessages: FormValidationMessages<T>): FormErrors<T> {
		const formErrors: FormErrors<T> = new FormErrors();
		const fields: string[] = Object.keys(ObjectUtils.flatten(validationMessages))
			.map(validationPath => /(.*)\.[^.]+/.exec(validationPath)![1]);
		for (const field of fields) {
			const control = form.get(field)! as FormControl;
			const controlValidationMessages: Nullable<FieldValidationMessages> = _.get(validationMessages, field);
			formErrors.setError(field, FormUtils.getValidationError(control, controlValidationMessages));
		}
		return formErrors;
	}
}
