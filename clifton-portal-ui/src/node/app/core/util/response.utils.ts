import { Response } from '@angular/http';
import { JsonUtils } from 'app/core/util/json.utils';


export class ResponseUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static readJson(response: Response): any {
		return JsonUtils.deserialize(response.text());
	}
}
