import { Type } from '@angular/core';
import { JsonPropertyDecoratorKey, JsonPropertyMetadata } from 'app/core/api/json-property.decorator';
import { DateUtils } from 'app/core/util/date.utils';
import { ObjectUtils } from 'app/core/util/object.utils';
import * as json5 from 'json5';
import * as _ from 'lodash';


export class JsonUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static deserialize(text: string | null): any {
		return json5.parse(text!); // Non-null type assertion here due to signature failure; nulls are properly handled by function, but do not exist in signature
	}


	public static serialize(obj: any): string {
		return JSON.stringify(obj);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static transformToServer(obj: any, defaultType?: Type<any>, serializeFn?: (obj: any) => any): any {
		let result: any;
		const value = serializeFn ? serializeFn(obj) : obj;
		if (ObjectUtils.isPrimitive(value)) {
			// Primitive: Simply return self
			result = value;
		}
		else if (_.isArray(value)) {
			result = JsonUtils.transformToServerArray(value);
		}
		else if (_.isDate(value)) {
			result = JsonUtils.transformToServerDate(value);
		}
		else {
			result = JsonUtils.transformToServerObject(value, defaultType);
		}
		return result;
	}


	private static transformToServerArray(obj: any[], defaultType?: Type<any>): any[] {
		// Array: Serialize each element individually, then return array
		return _.map(obj, element => JsonUtils.transformToServer(element, defaultType));
	}


	private static transformToServerDate(obj: Date): string {
		return DateUtils.formatDateShort(obj);
	}


	private static transformToServerObject(obj: any, defaultType?: Type<any>): any {
		// Object: Recursively serialize children
		const result: any = {};
		// Determine type of object
		const objectClass: Type<any> = obj.constructor || defaultType;
		for (const key of Object.keys(obj)) {
			// Recursively serialize all properties
			const propertyMetadata: JsonPropertyMetadata = objectClass && JsonUtils.getPropertyMetadata(objectClass, key) || {};
			const propertyName: string = propertyMetadata.propertyName || key;
			result[propertyName] = this.transformToServer(obj[key], propertyMetadata.clazz, propertyMetadata.serializeFn);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static transformToClient<T>(input: any, targetType?: Type<T>): T | T[] {
		// Guard-clause: No target type specified
		if (!targetType) {
			return input as any as T;
		}
		else if (ObjectUtils.isPrimitive(input)) {
			return input as any as T;
		}

		// Deserialize object
		let result: T | T[];
		if (_.isArray(input)) {
			result = JsonUtils.transformToClientArray(input, targetType);
		}
		else if (_.isEmpty(input)) {
			// Simple object with no properties
			result = new targetType();
		}
		else {
			result = JsonUtils.transformToClientObject(input, targetType);
		}
		return result as T;
	}


	private static transformToClientArray<T>(input: any[], targetType?: Type<T>): T[] {
		// Deserialize each array element
		// TODO: We could have nested arrays here. For simplicity with type-safety, we mark all values returned as being objects instead. Fixme
		return _.map(input, element => JsonUtils.transformToClient(element, targetType) as T);
	}


	private static transformToClientObject<T>(input: any, targetType: Type<T>): T {
		// Recursively deserialize object
		// Get metadata map, swapping property key and property mapping
		const metadata: { [key: string]: JsonPropertyMetadata } = JsonUtils.getClassMetadata(targetType);
		const metadataByMapping: { [key: string]: JsonPropertyMetadata } = {};
		for (const propertyKey of Object.keys(metadata)) {
			const propertyMetadata: JsonPropertyMetadata = metadata[propertyKey];
			if (propertyMetadata != null && propertyMetadata.propertyName) {
				metadataByMapping[propertyMetadata.propertyName] = {
					propertyName: propertyKey,
					clazz: propertyMetadata.clazz,
				};
			}
		}

		// Deserialize each element
		const result: any = new targetType();
		for (const key of Object.keys(input)) {
			const propertyMetadata: JsonPropertyMetadata = metadataByMapping[key];
			if (!propertyMetadata) {
				result[key] = this.transformToClient(input[key]);
			}
			else {
				const keyName: string = _.trimStart(propertyMetadata.propertyName, `${JsonPropertyDecoratorKey}:`);
				result[keyName] = this.transformToClient(input[key], propertyMetadata.clazz);
			}
		}
		return result as T;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static getClassMetadata(clazz: Type<any>): { [key: string]: JsonPropertyMetadata } {
		const keys: string[] = Reflect.getMetadataKeys(clazz.prototype);
		//noinspection CommaExpressionJS
		return _.reduce(keys, (accumulator: { [key: string]: JsonPropertyMetadata }, key: string) => (accumulator[key] = Reflect.getMetadata(key, clazz.prototype), accumulator), {});
	}


	private static getPropertyMetadata(clazz: Type<any>, propertyName: string): JsonPropertyMetadata {
		const metadata: JsonPropertyMetadata | null = Reflect.getMetadata(`${JsonPropertyDecoratorKey}:${propertyName}`, clazz.prototype);
		if (metadata && !metadata.clazz) {
			// Cache typed class object on JsonPropertyMetadata if not manually set
			// const propClazz: Type<any> = Reflect.getMetadata('design:type', clazz.prototype, propertyName);
			// TODO: Set this?
		}
		return metadata || {};
	}
}
