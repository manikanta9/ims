export class FunctionUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a function which is the concatenation of the provided functions in the order provided.
	 *
	 * The arguments passed to the resulting function will also be used as the parameters when calling each provided function. The return value is the result of
	 * the call to the final function.
	 *
	 * The functions array is filtered for non-null functions in the event that one or more listed functions has not been set.
	 *
	 * @param fnList the list of functions to concatenate
	 * @return a function which calls all of the given functions in the order provided
	 */
	// tslint:disable-next-line:ban-types : Generic function concatenation
	public static concat(...fnList: Function[]): Function {
		const nonNullFnList = fnList.filter(fn => fn != null);
		return function concatenatedFn(this: any) {
			let result: any;
			for (const fn of nonNullFnList) {
				result = fn.apply(this, arguments);
			}
			return result;
		};
	}
}


export function noop(): void {
	// Do nothing
}
