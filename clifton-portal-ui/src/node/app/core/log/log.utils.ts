import { LogLevel } from 'app/core/log/log-level.enum';
import { TypeUtils } from 'app/core/util/type.utils';
import { environment } from 'environments/environment';


export class LogUtils {

	private static readonly applicationName = 'Parametric Portal';
	private static readonly logLevel: LogLevel = environment.logLevel;
	private static readonly logContext: Console = console;


	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static log(...messageParts: any[]): void {
		LogUtils.info(...messageParts);
	}


	public static debug(...messageParts: any[]): void {
		LogUtils.logAt(LogLevel.DEBUG, ...messageParts);
	}


	public static info(...messageParts: any[]): void {
		LogUtils.logAt(LogLevel.INFO, ...messageParts);
	}


	public static warn(...messageParts: any[]): void {
		LogUtils.logAt(LogLevel.WARN, ...messageParts);
	}


	public static error(...messageParts: any[]): void {
		LogUtils.logAt(LogLevel.ERROR, ...messageParts);
	}


	public static logAt(level: LogLevel, ...messageParts: any[]): void {
		// Guard-clause: Prevent logging below level
		if (level > LogUtils.logLevel) {
			return;
		}

		// Determine logging type
		let logFn: (...msg: any[]) => void;
		const context = LogUtils.logContext;
		switch (level) {
			case LogLevel.ALL:
			case LogLevel.DEBUG:
				logFn = context.debug;
				break;
			case LogLevel.INFO:
				logFn = context.info;
				break;
			case LogLevel.WARN:
				logFn = context.warn;
				break;
			case LogLevel.ERROR:
				logFn = context.error;
				break;
			case LogLevel.NONE:
				logFn = (...msg) => {};
				break;
			default:
				TypeUtils.assertNever(level);
				console.warn(`Unrecognized log level: ${level}`);
				logFn = (...msg) => {};
		}

		// Write to log
		logFn.call(context, `[${LogUtils.applicationName}]`, ...messageParts);
	}
}
