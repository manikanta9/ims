import { Injectable } from '@angular/core';
import { LogUtils } from 'app/core/log/log.utils';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class ElementFinderService {

	/**
	 * Returns an observable whose payload is the first element matching the given selector. If the element already exists, then an observable will be generated instantly.
	 * Otherwise, the DOM will be watched until a suitable observable is found.
	 *
	 * Once the first element matching the given selector is found, the observable will complete.
	 *
	 * @param selector the selector to match
	 * @return {Observable<Element>} the observable whose payload shall be the first element matching the given selector
	 */
	public findFirstElement(selector: string): Observable<Element> {
		// Guard-clause: Element already exists
		const preExistingElement = document.querySelector(selector);
		if (preExistingElement) {
			return Observable.of(preExistingElement);
		}

		// Create observer to watch for first element creation
		return this.createElementObservable(selector)
			.take(1);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private createElementObservable(selector: string): Observable<Element> {
		return Observable.create((subscriber: Subscriber<Element>) => {
			const elementObserver = this.createElementMutationObserver(selector, (node: Element) => {
				subscriber.next(node);
				subscriber.complete();
			});

			// Begin watching DOM events
			LogUtils.log(`seeking selector ${selector}:`, document.querySelector(selector));
			elementObserver.observe(document, {childList: true, subtree: true});

			// TODO: Improve warning message for long watchers
			const longRunningWarningSub: Subscription = Observable.timer(5000)
				.mergeMap(() => Observable.interval(2000))
				.subscribe(i => LogUtils.warn(`findFirstElement observable got to ${i}`));

			// Tear-down logic
			return () => {
				elementObserver.disconnect();
				longRunningWarningSub.unsubscribe();
			};
		});
	}


	private createElementMutationObserver(selector: string, callback: (node: Element) => void): MutationObserver {
		// TODO: Need to validate that MutationObserver is available in browser; else probably want to fall-back to another solution, like DOM mutation events (not desirable!)
		return new MutationObserver((mutations: MutationRecord[], observer: MutationObserver) => {
			const node: Element | null = this.seekAddedElement(selector, mutations);
			if (node) {
				callback(node);
			}
		});
	}


	/**
	 * Seeks for the first added element matching the given selector in the list of mutations.
	 * @param selector the selector to match
	 * @param mutations the list of mutations
	 * @return {Element|null} the added element, or <tt>null</tt> if no such element was added
	 */
	private seekAddedElement(selector: string, mutations: MutationRecord[]): Element | null {
		let foundNode: Element | null = null;

		MutationSeekLoop:
			for (const mut of mutations) {
				LogUtils.log(`seeking selector ${selector}:`, document.querySelector(selector));
				LogUtils.log('checking for added nodes:', mut, mut.addedNodes);
				// Note: Cannot use "for ... of" iterator here with transpiler since ES5 (targeted version) does not support Symbol.iterator for iteration detection
				for (let node, i = 0; node = mut.addedNodes[i++];) {
					if (node instanceof Element) {
						if (node.matches(selector)) {
							foundNode = node;
							break MutationSeekLoop;
						}
					}
				}
			}

		return foundNode;
	}
}
