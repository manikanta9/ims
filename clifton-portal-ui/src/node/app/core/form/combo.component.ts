import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlContainer, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EntityQuery } from 'app/core/api/entity.query';
import { QueryService } from 'app/core/api/query.service';
import { CompareUtils } from 'app/core/util/compare.utils';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Scheduler } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';


export interface FieldDependency {
	fieldName: string;
	queryParam: string;
	fieldProperty?: string;
	/**
	 * If {@code true}, then the field corresponding to this dependency must be populated for the field to be populated. Defaults to {@code true}.
	 */
	required?: boolean;
}


@Component({
	selector: 'ppa-combo',
	templateUrl: './combo.component.html',
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: ComboComponent,
		multi: true,
	}],
})
export class ComboComponent<T> implements ControlValueAccessor, OnInit {

	@Input() public service?: QueryService;
	@Input() public retriever?: (queryData: any) => Observable<T[]>;
	@Input() public optionList?: T[];
	@Input() public query?: EntityQuery;
	@Input() public displayField?: string;
	@Input() public displayFn?: (value: T) => string;
	@Input() public titleField?: string;
	@Input() public value?: any;

	@Input() public fieldDependencies: FieldDependency[] = [];
	@Input() public includeBlank = false;
	@Input() public blankText = '';
	@Input() public filterFn: (option: T) => boolean = _.stubTrue;
	@Output() public change: EventEmitter<T | null> = new EventEmitter<T | null>();

	private _selectedValue: T | null;
	private changeHandler = (value: T | null): void => {};
	private touchHandler = (): void => {};
	private optionRetriever$: Subject<Observable<T[]>> = new Subject();

	public enabled = false;
	public optionList$: Observable<T[]> = this.optionRetriever$
		.switch()
		.map((optionList: T[]) => _.filter(optionList, this.filterFn))
		.share();


	public get selectedValue(): T | null {
		return this._selectedValue;
	}


	public set selectedValue(value: T | null) {
		const oldValue = this._selectedValue;
		this._selectedValue = value;
		if (!_.isEqual(oldValue, value)) {
			this.onChange(value);
		}
	}


	// Inject form container object (control container)
	public constructor(private controlContainer: ControlContainer) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		this.validateProperties();
		this.initCombobox();
		if (!_.isEmpty(this.fieldDependencies)) {
			this.activateFieldDependencies();
		}
		else {
			this.initOptionList();
		}
	}


	public writeValue(value: T): void {
		this.selectedValue = value;
	}


	public registerOnChange(fn: any): void {
		this.changeHandler = fn;
	}


	public registerOnTouched(fn: any): void {
		this.touchHandler = fn;
	}


	public onChange(value: T | null): void {
		this.change.emit(value);
		this.changeHandler(value);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getDisplayText(value: T): any {
		return (this.displayField == null)
			? this.displayFn ? this.displayFn(value) : value
			: _.get(value, this.displayField) as string;
	}


	public getTitleText(value: T): any {
		return (this.titleField)
			? _.get(value, this.titleField) as string
			: '';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the component properties.
	 */
	private validateProperties(): void {
		if (!CompareUtils.mutuallyExclusive(this.optionList, this.service, this.retriever)) {
			throw new Error('Either an option list, a service, or a retriever must not be supplied. Multiple properties may not be supplied simultaneously.');
		}
		if (this.displayField != null && this.displayFn != null) {
			throw new Error('A display field and a display function may not be provided simultaneously.');
		}
	}


	/**
	 * Initializes the combo box.
	 */
	private initCombobox(): void {
		this.optionList$.subscribe((optionList: T[]) => this.enabled = true);
		this.optionList$.subscribe((optionList: T[]) => {
			// Auto-select pre-selected items
			if (this.value) {
				this.selectedValue = this.value;
			}
			// Auto-select singular items
			else if (_.size(optionList) === 1) {
				this.selectedValue = optionList[0];
			}
		});
	}


	/**
	 * Initializes the option list.
	 */
	private initOptionList(): void {
		this.pushOptionList(this.getOptionList());
	}


	/**
	 * Applies subscriptions for field dependencies.
	 */
	private activateFieldDependencies(): void {
		// Subscribe to dependent field changes
		const dependentFieldNames = _.map(this.fieldDependencies || [], (dependency: FieldDependency) => dependency.fieldName);
		this.controlContainer.valueChanges!
		// Transform into object with relevant fields
			.map((formData: object) => _.chain(formData)
				.omitBy(_.isEmpty)
				.pick(formData, dependentFieldNames)
				.value())
			// Only emit on changed value
			.distinctUntilChanged(_.isEqual)
			/*
			 * Use async scheduler. If a synchronous scheduler is used, then effects will cascade down in a depth-first manner. This is relevant because the
			 * handler makes changes to the form, and each of these changes trigger an additional emit. If a synchronous scheduler is used, then the newly
			 * triggered emits will be handled before the handlers for other form-changes subscribers, such as other instances of this component, meaning that
			 * other subscribers may end up handling stale events.
			 */
			.observeOn(Scheduler.async)
			.subscribe(formData => this.handleDependentFieldChange(formData));

		// TODO: Automatically handled?
		// Handle initial result
		// this.handleDependentFieldChange(this.controlContainer.value);
	}


	/**
	 * Applies actions to be performed on the change of any field on which this field depends.
	 *
	 * @param formData
	 * @return {Promise<void>}
	 */
	private async handleDependentFieldChange(formData: object): Promise<void> {
		const requiredDependentFieldNames: string[] = _.chain(this.fieldDependencies || [])
			.filter((dependency: FieldDependency) => dependency.required == null || dependency.required)
			.map((dependency: FieldDependency) => dependency.fieldName)
			.value();
		const populatedFormFieldNames: string[] = _.chain(formData).omitBy(_.isEmpty).keys().value();
		const allRequiredDependentFieldsPopulated: boolean = _.isEmpty(_.difference(requiredDependentFieldNames, populatedFormFieldNames));

		// Clear current value
		this.selectedValue = null;
		if (!allRequiredDependentFieldsPopulated) {
			// At least one dependent field is not set
			this.pushOptionList();
			this.enabled = false;
		}
		else {
			// Refresh option list
			this.pushOptionList(this.getOptionList(this.getDependentFieldQueryData(formData)));
		}
	}


	/**
	 * Maps the given form data into the appropriate query data object based on {@link FieldDependency} mappings.
	 *
	 * @param formData the form data object
	 * @return {{}} the object containing option list query parameters based on current form data
	 */
	private getDependentFieldQueryData(formData: object): object {
		const queryData: any = {};
		for (const field of this.fieldDependencies) {
			if (formData.hasOwnProperty(field.fieldName)) {
				const fieldValue = (formData as any)[field.fieldName]!;
				queryData[field.queryParam] = field.fieldProperty ? _.get(fieldValue, field.fieldProperty) : fieldValue;
			}
		}
		return queryData;
	}


	/**
	 * Retrieves the list of options to be used.
	 *
	 * @param queryData the query data to be appended to the query, if a query is used to retrieve the option list
	 * @return {Observable<T[]>} an observable which shall return the option list
	 */
	private getOptionList(queryData?: object): Observable<T[]> {
		let optionListObservable: Observable<T[]>;
		if (this.service) {
			optionListObservable = Observable.fromPromise(this.service.get(Object.assign({}, this.query, queryData)));
		}
		else if (this.retriever) {
			optionListObservable = this.retriever(Object.assign({}, this.query, queryData));
		}
		else {
			optionListObservable = Observable.of(this.optionList!);
		}
		return optionListObservable;
	}


	/**
	 * Adds the given list-returning observable to the queue for the option list. On call, all previously-pushed arguments for this method will be ignored, even if they are
	 * observables that have not yet completed.
	 * <p>
	 * If no value is given then any pending option list retrievers will be ignored.
	 *
	 * @param listObservable an observable returning the list of options
	 */
	private pushOptionList(listObservable?: Observable<T[]>): void {
		this.optionRetriever$.next(listObservable || Observable.never());
	}
}

