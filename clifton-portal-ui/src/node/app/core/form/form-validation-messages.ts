/**
 * The list of validation messages for a form.
 */
import { FieldValidationMessages } from 'app/core/form/field-validation-messages';

export type FormValidationMessages<T> =
	/*
	 * TODO: TS 2.4.1 doesn't seem to perform excess property checking for index types here. Remove this message if/when a new version fixes this.
	 *
	 * E.g.:
	 * type A = { field1: { field2: string; }; };
	 * const b: FormValidationMessages<A> = {
	 *     fakeField: '', // Fails, as expected
	 *     field1: {
	 *         fakeField: '', // DOESN'T FAIL (unexpected)
	 *         field2: {
	 *             errorType: 'error message'
	 *         }
	 *     }
	 * };
	 */
	{ [field in keyof T]?: FieldValidationMessages | FormValidationMessages<T[field]>; };
