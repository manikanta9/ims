import { FieldValidationMessage } from 'app/core/form/field-validation-message';


/**
 * The list of validation messages for a single form control.
 *
 * If a string is provided, then that string will be used regardless of the error types present.
 *
 * If the <tt>other</tt> property is provided, it shall be used when an error exists but does not match any of the listed error types.
 */
export type FieldValidationMessages =
	| ({ [errorType: string]: FieldValidationMessage; } & { other?: string })
	| string;
