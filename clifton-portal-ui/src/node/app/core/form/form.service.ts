import { Injectable } from '@angular/core';
import { Query } from 'app/core/api/query';
import { FormUtils } from 'app/core/util/form.utils';
import { environment } from 'environments/environment';
import { CookieService } from 'ngx-cookie';


@Injectable()
export class FormService {

	private static readonly XSRF_COOKIE_NAME = environment.xsrfCookieName;
	private static readonly XSRF_PARAMETER_NAME = environment.xsrfParameterName;


	constructor(private cookieService: CookieService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public submit(uri: string, query: Query): void {
		const form: HTMLFormElement = FormUtils.generateForm(query, uri);
		document.body.appendChild(form);
		form.appendChild(this.createXsrfInputField());
		form.submit();
		document.body.removeChild(form);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private createXsrfInputField(): HTMLInputElement {
		const inputEl = document.createElement('input');
		inputEl.type = 'hidden';
		inputEl.name = FormService.XSRF_PARAMETER_NAME;
		inputEl.value = this.cookieService.get(FormService.XSRF_COOKIE_NAME);
		return inputEl;
	}
}
