import { Directive, ElementRef, Input, OnInit, Renderer2, ViewContainerRef } from '@angular/core';
import { LogUtils } from 'app/core/log/log.utils';


@Directive({
	/* tslint:disable-next-line:directive-selector : Apply to all "for" label attributes */
	selector: 'temporarilyDisabled',
	// selector: 'label[for],[id]',
})
/**
 * TODO: Describe how this creates unique IDs and why we need it (DOM validation constraint on unique ID, for-labels running into conflicts
 */
export class ElementIdDirective implements OnInit {

	private static readonly METADATA_FIELD = '_ppaElementIdContext';
	private static readonly METADATA_KEY_PREFIX = 'for-label-context-id';
	private static readonly METADATA_VALUE_PREFIX = 'ppa-unique';
	private static readonly METADATA_DELIMITER = ':';

	private static currentId = 0;

	/* tslint:disable-next-line:no-input-rename : Take input from HTML node attribute */
	@Input('for') private nodeFor: string;
	/* tslint:disable-next-line:no-input-rename : Take input from HTML node attribute */
	@Input('id') private nodeId: string;


	constructor(private elementRef: ElementRef,
	            private viewContainerRef: ViewContainerRef,
	            private renderer: Renderer2) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ngOnInit(): void {
		LogUtils.log('starting element id directive');
		const idLabel = this.getElementUniqueFieldValue();
		const metadataKey = this.getMetadataKey(idLabel);
		// Note: This is an undocumented field and may change
		const metadataHost = (this.viewContainerRef as any)._view;

		// Get unique ID
		const discoveredId: string | null = this.getContextIdMetadata(metadataHost, metadataKey);
		const uniqueId: string = discoveredId || this.generateMetadataUniqueValue(idLabel);
		if (discoveredId == null) {
			this.setContextIdMetadata(metadataHost, metadataKey, uniqueId);
		}

		this.renderer.setAttribute(this.elementRef.nativeElement, this.getElementUniqueField(), uniqueId);
		LogUtils.log('ending element id directive');
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the field on the injected element for which a unique value should be used.
	 */
	private getElementUniqueField(): string {
		let elementAttr: string;
		if (this.nodeFor) {
			elementAttr = 'for';
		}
		else if (this.nodeId) {
			elementAttr = 'id';
		}
		else {
			LogUtils.warn('No valid unique field was found for the element. Falling back to attempting to use the ID field.');
			elementAttr = 'id';
		}
		return elementAttr;
	}


	private getElementUniqueFieldValue(): string {
		const value = this.nodeFor || this.nodeId;
		if (value == null) {
			LogUtils.warn('No value was found for the component-unique field on the element.');
		}
		return value;
	}


	private getContextIdMetadata(host: any, key: string): string | null {
		return (host[ElementIdDirective.METADATA_FIELD] || {})[key] as string;
	}


	private setContextIdMetadata(host: any, key: string, value: string): void {
		// Generate metadata context if not present
		if (!host[ElementIdDirective.METADATA_FIELD]) {
			host[ElementIdDirective.METADATA_FIELD] = {};
		}
		host[ElementIdDirective.METADATA_FIELD][key] = value;
	}


	private getMetadataKey(label: string): string {
		return ElementIdDirective.METADATA_KEY_PREFIX + ElementIdDirective.METADATA_DELIMITER + label;

	}


	private generateMetadataUniqueValue(label: string): string {
		return ElementIdDirective.METADATA_VALUE_PREFIX
			+ ElementIdDirective.METADATA_DELIMITER + label
			+ ElementIdDirective.METADATA_DELIMITER + String(ElementIdDirective.currentId++);
	}
}
