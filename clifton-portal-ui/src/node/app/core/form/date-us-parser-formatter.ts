import { Injectable } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateISOParserFormatter } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-parser-formatter';


@Injectable()
export class DateUsParserFormatter extends NgbDateISOParserFormatter {

	public parse(value: string): NgbDateStruct {
		const [, month, day, year]: string[] = /^\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s*$/.exec(value || '') || [];
		// Guard-clause: Invalid input
		if (!this.isValidInteger(month) || !this.isValidInteger(day) || !this.isValidInteger(year)) {
			// Interface incorrectly excludes null return-type from declaration
			return null as any as NgbDateStruct;
		}

		return {
			year: this.toInteger(year),
			month: this.toInteger(month),
			day: this.toInteger(day),
		};
	}


	public format(date: NgbDateStruct): string {
		const isoDateStr: string = super.format(date);
		if (!isoDateStr) {
			return isoDateStr;
		}

		// Reformat using US date format
		const [, year, month, day]: (string | undefined)[] = /(\d+)-(\d+)-(\d+)/.exec(isoDateStr) || [];
		return `${month}/${day}/${year}`;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private isValidInteger(value: Nullable<string>): value is string {
		return !Number.isNaN(this.toInteger(value));
	}


	private toInteger(value: Nullable<string>): number {
		return Number.parseInt(value || '', 10);
	}
}
