/**
 * The validation message for a single field. If a function is provided, its argument will be the error value itself.
 */
export type FieldValidationMessage =
	| ((value: any) => string)
	| string;
