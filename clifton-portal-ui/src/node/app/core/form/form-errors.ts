import * as _ from 'lodash';


type ErrorObject<T> = {
	[field in keyof T]?: string | FormErrors<T[field]>;
	};


/**
 * The list of triggered errors for a form.
 */
export class FormErrors<T> {

	private backingObject: ErrorObject<T> = {};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public setError(path: string, message: Nullable<string>): void {
		_.set(this.backingObject, path, message);
	}


	public getError(path: string): Nullable<string> {
		return _.get(this.backingObject, path);
	}
}
