import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
	selector: 'ppa-page-not-found',
	templateUrl: './page-not-found.component.html',
})
export class PageNotFoundComponent {

	public htmlContent = 'We were unable to locate the requested page.';


	constructor(activatedRoute: ActivatedRoute) {
		// Retrieve HTML content from route data "message" field if provided
		if (activatedRoute.snapshot.data && activatedRoute.snapshot.data.message != null) {
			this.htmlContent = activatedRoute.snapshot.data.message;
		}
	}
}
