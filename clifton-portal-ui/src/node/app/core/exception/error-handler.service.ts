import { LocationStrategy } from '@angular/common';
import { ErrorHandler, Injectable, Injector } from '@angular/core';


@Injectable()
export class ErrorHandlerService extends ErrorHandler {


	// Inject nothing but the injector here to ensure that the service is loaded as quickly as possible
	constructor(private injector: Injector) {
		super();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public handleError(error: any): void {
		// const loggerService = this.injector.get(LoggerService);
		// loggerService.logAt(LogLevel.ERROR, this.getErrorTitle(error), error);
		super.handleError(error);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private getErrorTitle(error: any): string {
		const errMsg = this.getErrorMessage(error);
		return `(${this.getErrorPath()})${errMsg ? ` ${errMsg}` : ''}:`;
	}


	private getErrorPath(): string {
		return this.injector.get(LocationStrategy).path(true);
	}


	private getErrorMessage(error: any): string {
		return error && error.message ? error.message : '';
	}
}
