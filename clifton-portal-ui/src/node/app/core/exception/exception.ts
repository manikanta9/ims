/**
 * A generic exception type. This exception type may contain a nested inner exception.
 *
 * This class implements {@link Error} rather than explicitly extending it. This is because TypeScript class inheritance imposes the requirement that the parent
 * constructor is called. However, the {@link Error} constructor cannot be called in the usual sense (<code>Error.apply(this, arguments)</code>) to apply its
 * behavior on the current object. Rather, its constructor would simply return an instance of {@link Error}, regardless of the <code>this</code> context passed
 * in to the constructor.
 *
 * Instead of naturally extending {@link Error}, this class implements {@link Error} as an interface and then manually extends {@link Error}.
 */
export class Exception implements Error {
	public name: string;
	public message: string;
	public stack: string;

	private innerException?: Error;


	constructor(message?: string,
				innerException?: Error | string) {
		this.name = this.constructor.name;
		this.message = message || '';
		this.attachInnerException(innerException);
		this.attachStackTrace();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private attachInnerException(innerException?: Error | string): void {
		if (innerException) {
			if (innerException instanceof Error) {
				this.innerException = innerException;
			}
			else {
				this.innerException = new Error(innerException);
			}
		}
	}


	private attachStackTrace(): void {
		// Get stack trace
		if (typeof (Error as any).captureStackTrace === 'function') {
			// captureStackTrace is only available in some environments
			(Error as any).captureStackTrace(this, this.constructor);
		}
		else {
			this.stack = (new Error()).stack!;
		}
	}


	public toString(): string {
		return this.stack;
	}
}


// Manually inherit to prevent constructor-related issues from built-in types
Exception.prototype = Object.assign(Object.create(Error.prototype), Exception.prototype);
Exception.prototype.constructor = Exception;
