import { Exception } from 'app/core/exception/exception';


export class ModuleAlreadyLoadedException extends Exception {
	constructor(private moduleName: string,
				private parentInjectorName: string) {
		super(`${moduleName} has already been loaded${parentInjectorName ? ` in the context of ${parentInjectorName}` : ''}. Import core modules in the root module only.`);
	}
}
