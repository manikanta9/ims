import { Exception } from 'app/core/exception/exception';


export class RouteBlockedException extends Exception {
	constructor(reason: string) {
		super(`Route blocked: ${reason}`);
	}
}
