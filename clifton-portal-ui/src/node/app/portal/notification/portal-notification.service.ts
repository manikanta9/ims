import { Injectable } from '@angular/core';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityQuery } from 'app/portal/entity/portal-entity-query';
import { PortalFileFrequencies } from 'app/portal/file/portal-file-frequencies';
import { PortalNotification } from 'app/portal/notification/portal-notification';
import { PortalNotificationSubscriptionEntry } from 'app/portal/notification/portal-notification-subscription-entry';
import { PortalNotificationSubscriptionFrequencies, PortalNotificationSubscriptionFrequenciesData } from 'app/portal/notification/portal-notification-subscription-frequencies';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { ApiUtils } from 'app/portal/util/api.utils';
import { Observable } from 'rxjs/Observable';


export interface PortalNotificationSubscriptionEntrySaveFacade {
	portalEntity: PortalEntity;
	securityUser: PortalSecurityUser;
	subscriptionList: string;
}


@Injectable()
export class PortalNotificationService {

	constructor(private portalApiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getNotificationSubscriptionEntry(entityId: number): Observable<PortalNotificationSubscriptionEntry> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('NotificationSubscriptionEntry.json'), {portalEntityId: entityId} as PortalEntityQuery);
	}


	public getNotificationSubscriptionEntryForUser(userId: number, entityId: number): Observable<PortalNotificationSubscriptionEntry> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('NotificationSubscriptionEntryForUser.json'), {
			id: userId,
			portalEntityId: entityId,
		});
	}


	public saveNotificationSubscriptionEntry(subscriptionEntry: PortalNotificationSubscriptionEntrySaveFacade): Observable<PortalNotificationSubscriptionEntry> {
		return this.portalApiService.save2(ApiUtils.getAsApiUriWithPrefix('NotificationSubscriptionEntrySave.json'), subscriptionEntry as any /* Cast for compatible return type */);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Obtains the populated subscription frequency objects that are allowed for the given {@link PortalFileFrequencies} value.
	 */
	public getSubscriptionFrequenciesForFileFrequency(fileFrequency?: PortalFileFrequencies): PortalNotificationSubscriptionFrequenciesData[] {
		switch (fileFrequency) {
			case 'DAILY':
				return [
					PortalNotificationSubscriptionFrequencies.getData(PortalNotificationSubscriptionFrequencies.DAILY),
					PortalNotificationSubscriptionFrequencies.getData(PortalNotificationSubscriptionFrequencies.WEEKLY_FIRST_BUSINESS_DAY),
					PortalNotificationSubscriptionFrequencies.getData(PortalNotificationSubscriptionFrequencies.MONTHLY_FIRST_BUSINESS_DAY),
					PortalNotificationSubscriptionFrequencies.getData(PortalNotificationSubscriptionFrequencies.QUARTERLY_FIRST_BUSINESS_DAY),
				];
			default:
				return [];
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public getNotificationPopUpList(): Observable<PortalNotification[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('NotificationPopUpListFind.json'), {requestedProperties: ['id', 'notificationDefinition.acknowledgementType.cancelUsed', 'notificationDefinition.acknowledgementType.confirmUsed', 'notificationDefinition.acknowledgementType.declineUsed', 'subject', 'text', 'notificationDefinition.acknowledgementType.confirmButtonLabel', 'notificationDefinition.acknowledgementType.declineButtonLabel', 'notificationDefinition.acknowledgementType.cancelButtonLabel', 'notificationDefinition.acknowledgementType.confirmButtonTooltip', 'notificationDefinition.acknowledgementType.declineButtonTooltip', 'notificationDefinition.acknowledgementType.cancelButtonTooltip', 'notificationDefinition.acknowledgementType.cancelAcknowledgement']});
	}


	public saveClientPortalNotificationAcknowledgement(portalNotificationId: number, acknowledgement: boolean | undefined): Observable<{ acknowledgement: boolean | undefined; portalNotificationId: number }> {
		return this.portalApiService.save2(ApiUtils.getAsApiUriWithPrefix('NotificationAcknowledgementSave.json'), {
			portalNotificationId,
			acknowledgement,
		});
	}
}
