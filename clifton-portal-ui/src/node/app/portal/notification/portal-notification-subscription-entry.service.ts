import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalNotificationSubscriptionEntry } from 'app/portal/notification/portal-notification-subscription-entry';
import { PortalNotificationSubscriptionEntryQuery } from 'app/portal/notification/portal-notification-subscription-entry.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalNotificationSubscriptionEntryService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('NotificationSubscriptionEntry.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: PortalNotificationSubscriptionEntryQuery): Promise<PortalNotificationSubscriptionEntry[]> {
		return this.apiService.getData(PortalNotificationSubscriptionEntryService.URI, query, PortalNotificationSubscriptionEntry);
	}
}
