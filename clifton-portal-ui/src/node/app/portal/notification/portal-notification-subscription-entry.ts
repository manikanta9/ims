import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalNotificationSubscription } from 'app/portal/notification/portal-notification-subscription';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';


export class PortalNotificationSubscriptionEntry {
	public portalEntity?: PortalEntity;
	public securityUser?: PortalSecurityUser;
	public subscriptionList?: PortalNotificationSubscription[];
}
