import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalNotificationDefinitionType } from 'app/portal/notification/portal-notification-definition-type';
import { PortalNotificationSubscriptionFrequencies } from 'app/portal/notification/portal-notification-subscription-frequencies';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';


export class PortalNotificationSubscription extends BaseEntity {
	public coalesceDefinitionTypeFileCategoryName?: string;
	public label?: string;
	public notificationEnabled?: boolean;
	public subscriptionFrequency?: PortalNotificationSubscriptionFrequencies;
	public subscriptionFrequencyLabel?: string;
	@JsonProperty() public fileCategory?: PortalFileCategory;
	@JsonProperty() public notificationDefinitionType?: PortalNotificationDefinitionType;
	@JsonProperty() public userAssignment?: PortalSecurityUserAssignment;
	@JsonProperty() public portalEntity?: PortalEntity;
}
