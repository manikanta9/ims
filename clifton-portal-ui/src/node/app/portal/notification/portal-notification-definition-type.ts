import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalSecurityResource } from 'app/portal/security/portal-security-resource';


export class PortalNotificationDefinitionType extends BaseEntity {
	public description?: string;
	public fileNotification?: boolean;
	public label?: string;
	public name?: string;
	public subscriptionAllowed?: boolean;
	public subscriptionForFileCategory?: boolean;
	public triggeredByEvent?: boolean;
	@JsonProperty() public securityResource?: PortalSecurityResource;
}
