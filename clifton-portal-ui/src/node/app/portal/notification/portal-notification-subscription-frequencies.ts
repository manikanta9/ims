import { PortalNotificationSubscriptionFrequenciesData } from 'app/portal/notification/portal-notification-subscription-frequencies';


export enum PortalNotificationSubscriptionFrequencies {
	DAILY = '',
	WEEKLY_FIRST_BUSINESS_DAY = 'WEEKLY_FIRST_BUSINESS_DAY',
	MONTHLY_FIRST_BUSINESS_DAY = 'MONTHLY_FIRST_BUSINESS_DAY',
	QUARTERLY_FIRST_BUSINESS_DAY = 'QUARTERLY_FIRST_BUSINESS_DAY',
}


export interface PortalNotificationSubscriptionFrequenciesData {
	name: string;
	label: string;
	description: string;
}


const SUBSCRIPTION_FREQUENCY_DATA: { [key: string]: PortalNotificationSubscriptionFrequenciesData } = {
	[PortalNotificationSubscriptionFrequencies.DAILY]: {
		name: PortalNotificationSubscriptionFrequencies.DAILY,
		label: 'Daily',
		description: 'Receive any new notifications for this category on a daily basis.',
	},
	[PortalNotificationSubscriptionFrequencies.WEEKLY_FIRST_BUSINESS_DAY]: {
		name: PortalNotificationSubscriptionFrequencies.WEEKLY_FIRST_BUSINESS_DAY,
		label: 'First Business Day of the Week',
		description: 'Receive any new notifications for this category on the first business day of the week.',
	},
	[PortalNotificationSubscriptionFrequencies.MONTHLY_FIRST_BUSINESS_DAY]: {
		name: PortalNotificationSubscriptionFrequencies.MONTHLY_FIRST_BUSINESS_DAY,
		label: 'First Business Day of the Month',
		description: 'Receive any new notifications for this category on the first business day of the month.',
	},
	[PortalNotificationSubscriptionFrequencies.QUARTERLY_FIRST_BUSINESS_DAY]: {
		name: PortalNotificationSubscriptionFrequencies.QUARTERLY_FIRST_BUSINESS_DAY,
		label: 'First Business Day of the Quarter',
		description: 'Receive any new notifications for this category on the first business day of the quarter.',
	},
};


// tslint:disable-next-line:no-namespace : Namespace required to append field data to type
export namespace PortalNotificationSubscriptionFrequencies {
	export function getData<T extends PortalNotificationSubscriptionFrequencies>(subscriptionFrequencies: PortalNotificationSubscriptionFrequencies): PortalNotificationSubscriptionFrequenciesData {
		return SUBSCRIPTION_FREQUENCY_DATA[subscriptionFrequencies];
	}
}
