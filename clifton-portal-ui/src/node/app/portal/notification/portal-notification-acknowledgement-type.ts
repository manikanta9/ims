import { BaseEntity } from 'app/core/api/base-entity';


export class PortalNotificationAcknowledgementType extends BaseEntity {

	public name?: string;
	public description?: string;
	public confirmButtonLabel?: string;
	public confirmButtonTooltip?: string;
	public confirmEntity?: boolean;
	public declineButtonLabel?: string;
	public declineButtonTooltip?: string;
	public cancelButtonLabel?: string;
	public cancelButtonTooltip?: string;
	public cancelAcknowledgement?: boolean;
	public confirmUsed?: boolean;
	public declineUsed?: boolean;
	public cancelUsed?: boolean;

}
