import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalNotificationAcknowledgementType } from 'app/portal/notification/portal-notification-acknowledgement-type';
import { PortalNotificationDefinitionType } from 'app/portal/notification/portal-notification-definition-type';


export class PortalNotificationDefinition extends BaseEntity {
	public name?: string;
	public label?: string;
	public description?: string;
	public popUpNotification: boolean;
	@JsonProperty() public notificationDefinitionType?: PortalNotificationDefinitionType;
	@JsonProperty() public acknowledgementType?: PortalNotificationAcknowledgementType;

}
