import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalNotificationDefinition } from 'app/portal/notification/portal-notification-definition';


export class PortalNotification extends BaseEntity {

	public name?: string;
	public label?: string;
	public description?: string;
	public subject?: string;
	public text?: string;
	public acknowledged?: boolean;
	@JsonProperty() public notificationDefinition?: PortalNotificationDefinition;
}
