import { Injectable } from '@angular/core';
import { Query } from 'app/core/api/query';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityUserCurrentService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserCurrent.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: Query): Promise<PortalSecurityUser[]> {
		return this.apiService.getData(PortalSecurityUserCurrentService.URI, query, PortalSecurityUser);
	}
}
