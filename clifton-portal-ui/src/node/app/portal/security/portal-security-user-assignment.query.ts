import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalSecurityUserAssignmentQuery extends PortalSearchQuery {
	public assignmentRoleId?: number;
	public assignmentRolePortalSecurityAdministrator?: boolean;
	public disabled?: boolean;
	public emailAddress?: string;
	public portalEntityId?: number;
	public portalEntityIdOrRelatedEntity?: number;
	public portalSecurityUserId?: number;
	public portalSecurityUserRoleId?: number;
	public searchPattern?: string;
	public userCompanyName?: string;
	public userPhoneNumber?: string;
	public userSearchPattern?: string;
	public userTitle?: string;
	public username?: string;
	public viewAsEntityId?: number | '';
	public viewAsUserId?: number | '';
	public viewSecurityAdminRole?: boolean;

	// Non-search-form parameter
	public populateResourcePermissionMap?: boolean;
}
