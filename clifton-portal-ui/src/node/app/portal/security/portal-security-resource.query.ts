import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalSecurityResourceQuery extends PortalSearchQuery {
	public ids: number[];
	public nameEquals: string;
	public nameExpanded: string;
	public parentId: number;
	public rootOnly: boolean;
	public searchPattern: string;
}
