import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityUserRole } from 'app/portal/security/portal-security-user-role';
import { PortalSecurityUserRoleQuery } from 'app/portal/security/portal-security-user-role.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityUserRoleService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserRoleListFind.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: PortalSecurityUserRoleQuery): Promise<PortalSecurityUserRole[]> {
		return this.apiService.getData(PortalSecurityUserRoleService.URI, query, PortalSecurityUserRole);
	}
}
