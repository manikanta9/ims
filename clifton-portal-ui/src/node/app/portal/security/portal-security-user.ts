import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserRole } from 'app/portal/security/portal-security-user-role';
import { PortalEntity } from '../entity/portal-entity';


export class PortalSecurityUser extends PortalEntity {
	public companyName?: string;
	public emailAddress?: string;
	public firstName?: string;
	public labelLong?: string;
	public lastName?: string;
	public passwordResetRequired?: boolean;
	public phoneNumber?: string;
	public termsOfUseAcceptanceRequired?: boolean;
	public title?: string;
	public username?: string;
	// TODO: Verify that this is deserialized/serialized by client correctly
	@JsonProperty() public resourcePermissionMap?: { [resourceId: number]: PortalSecurityUserAssignmentResource };
	@JsonProperty() public userAssignmentResourceList?: PortalSecurityUserAssignmentResource[];
	@JsonProperty() public userRole?: PortalSecurityUserRole;
}
