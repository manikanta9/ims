import { BaseEntity } from 'app/core/api/base-entity';


export class PortalSecurityUserRole extends BaseEntity {
	public administrator?: boolean;
	public description?: string;
	public label?: string;
	public name?: string;
	public portalAdministrator?: boolean;
	public portalEntitySpecific?: boolean;
	public portalSecurityAdministrator?: boolean;
}
