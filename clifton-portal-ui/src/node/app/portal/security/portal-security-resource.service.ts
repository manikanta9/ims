import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityResource } from 'app/portal/security/portal-security-resource';
import { PortalSecurityResourceQuery } from 'app/portal/security/portal-security-resource.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityResourceService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('SecurityResourceListFind.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: PortalSecurityResourceQuery): Promise<PortalSecurityResource[]> {
		return this.apiService.getData(PortalSecurityResourceService.URI, query, PortalSecurityResource);
	}
}
