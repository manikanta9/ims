import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';
import { PortalSecurityUserAssignmentQuery } from 'app/portal/security/portal-security-user-assignment.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityUserAssignmentService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentListFind.json');
	private static readonly SAVE_URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentSave.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: PortalSecurityUserAssignmentQuery): Promise<PortalSecurityUserAssignment[]> {
		return this.apiService.getData(PortalSecurityUserAssignmentService.URI, query, PortalSecurityUserAssignment);
	}


	public async save(userAssignment: PortalSecurityUserAssignment): Promise<void> {
		await this.apiService.save(PortalSecurityUserAssignmentService.SAVE_URI, userAssignment, PortalSecurityUserAssignment);
		return void 0;
	}
}
