import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalSecurityResource } from 'app/portal/security/portal-security-resource';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';


export class PortalSecurityUserAssignmentResource extends BaseEntity {
	public label?: string;
	public accessAllowed?: boolean;
	@JsonProperty() public fileCategoryList?: PortalFileCategory[];
	@JsonProperty() public securityUserAssignment?: PortalSecurityUserAssignment;
	@JsonProperty() public securityUser?: PortalSecurityUser;
	@JsonProperty() public securityResource?: PortalSecurityResource;
}
