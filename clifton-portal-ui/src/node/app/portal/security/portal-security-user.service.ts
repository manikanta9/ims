import { Injectable } from '@angular/core';
import { EntityQuery } from 'app/core/api/entity.query';
import { QueryService } from 'app/core/api/query.service';
import { LogUtils } from 'app/core/log/log.utils';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityUserService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('SecurityUser.json');
	private static readonly SAVE_URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserSave.json');


	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: EntityQuery): Promise<PortalSecurityUser[]> {
		return this.apiService.getData(PortalSecurityUserService.URI, query, PortalSecurityUser);
	}


	public async save(user: PortalSecurityUser): Promise<void> {
		LogUtils.log('running save at URI', PortalSecurityUserService.SAVE_URI);
		await this.apiService.save(PortalSecurityUserService.SAVE_URI, user, PortalSecurityUser);
		return void 0;
	}
}
