import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserAssignmentResourceQuery } from 'app/portal/security/portal-security-user-assignment-resource.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalSecurityUserAssignmentResourceService implements QueryService {

	private static readonly EDIT_URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentResourceListForAssignmentAndRole.json');
	private static readonly ADD_URI = ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentResourceListForEntityAndRole.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query?: PortalSecurityUserAssignmentResourceQuery): Promise<PortalSecurityUserAssignmentResource[]> {
		if (query!.userAssignmentId) {
			return this.apiService.getData(PortalSecurityUserAssignmentResourceService.EDIT_URI, query, PortalSecurityUserAssignmentResource);
		}
		else {
			return this.apiService.getData(PortalSecurityUserAssignmentResourceService.ADD_URI, query, PortalSecurityUserAssignmentResource);
		}
	}
}
