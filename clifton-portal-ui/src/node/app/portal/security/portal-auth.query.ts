import { Query } from 'app/core/api/query';


export class PortalAuthQuery implements Query {
	public j_username: string;
	public j_password: string;
	public security_remember_me: boolean;
	public redirect?: string;
}
