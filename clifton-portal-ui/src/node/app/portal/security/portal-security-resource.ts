import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';


export class PortalSecurityResource extends BaseEntity {
	public name?: string;
	public nameExpanded?: string;
	public label?: string;
	public labelExpanded?: string;
	public description?: string;
	public leaf?: boolean;
	public level?: number;
	public maxDepth?: number;
	@JsonProperty() public parent?: PortalSecurityResource;
	@JsonProperty() public rootParent?: PortalSecurityResource;
}
