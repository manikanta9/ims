import { Injectable } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { LogUtils } from 'app/core/log/log.utils';
import * as routerActions from 'app/core/state/reducers/router.reducer';
import { UrlUtils } from 'app/core/util/url.utils';
import * as portalStore from 'app/portal/state/portal.store';
import * as simulation from 'app/portal/state/reducers/simulation.reducer';
import { SessionStorageService } from 'ngx-webstorage';


@Injectable()
export class PortalSimulationService {

	private static readonly SIMULATION_USER_QUERY_PARAM = 'viewAsUserId';
	private static readonly SIMULATION_ENTITY_QUERY_PARAM = 'viewAsEntityId';


	constructor(private store: Store<portalStore.PortalStore>,
	            private activatedRoute: ActivatedRoute,
	            private sessionStorageService: SessionStorageService) {
		// Begin listening to query params before session storage to allow query params to override session storage
		this.initializeQueryParamListen(PortalSimulationService.SIMULATION_USER_QUERY_PARAM);
		this.initializeQueryParamListen(PortalSimulationService.SIMULATION_ENTITY_QUERY_PARAM);
		this.initializeSessionStorageListen();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private initializeQueryParamListen(paramName: string): void {
		this.activatedRoute.queryParamMap
			.startWith(UrlUtils.getQueryParams(window.location.href))
			.map((params: ParamMap) => [params, params.get(paramName)] as [ParamMap, Nullable<string>])
			.filter(([params, value]: [ParamMap, Nullable<string>]) => value != null)
			.distinctUntilChanged(([oldParams, oldVal]: [ParamMap, string], [curParams, curVal]: [ParamMap, string]) => oldVal === curVal)
			.subscribe(([params, value]: [ParamMap, string]) => {
				const id: number | null = value ? Number(value) : null;
				// Guard-clause: Ignore invalid IDs
				if (id != null && Number.isNaN(id)) {
					LogUtils.warn('The given simulation user ID is not a valid ID:', value);
					return;
				}

				this.sessionStorageService.store(paramName, id);

				// Remove current token query param
				const queryParams: Params = params.keys
					.filter(key => key !== paramName)
					.reduce((obj, key) => obj[key] = params.get(key), {} as any);
				this.store.dispatch(routerActions.search(queryParams));
			});
	}


	private initializeSessionStorageListen(): void {
		// Listen for user ID changes
		this.sessionStorageService.observe(PortalSimulationService.SIMULATION_USER_QUERY_PARAM)
			.startWith(this.sessionStorageService.retrieve(PortalSimulationService.SIMULATION_USER_QUERY_PARAM))
			.distinctUntilChanged()
			.subscribe((id: Nullable<number>) => this.setSimulatedUserId(id));

		// Listen for entity ID changes
		this.sessionStorageService.observe(PortalSimulationService.SIMULATION_ENTITY_QUERY_PARAM)
			.startWith(this.sessionStorageService.retrieve(PortalSimulationService.SIMULATION_ENTITY_QUERY_PARAM))
			.distinctUntilChanged()
			.subscribe((id: Nullable<number>) => this.setSimulatedEntityId(id));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private setSimulatedUserId(id: Nullable<number>): void {
		const action = (id != null)
			? new simulation.SetSimulatedUserIdAction(id)
			: new simulation.ClearSimulatedUserAction();
		this.store.dispatch(action);
	}


	private setSimulatedEntityId(id: Nullable<number>): void {
		const action = (id != null)
			? new simulation.SetSimulatedEntityIdAction(id)
			: new simulation.ClearSimulatedEntityAction();
		this.store.dispatch(action);
	}
}
