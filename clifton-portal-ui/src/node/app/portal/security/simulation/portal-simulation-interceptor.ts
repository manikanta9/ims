import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { UrlUtils } from 'app/core/util/url.utils';
import * as portalStore from 'app/portal/state/portal.store';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalSimulationInterceptor implements HttpInterceptor {

	private static readonly SIMULATION_REQUEST_USER_PARAM = 'viewAsUserId';
	private static readonly SIMULATION_REQUEST_ENTITY_PARAM = 'viewAsEntityId';

	private simulationUserId$: Observable<number | undefined> = this.store.select(portalStore.getSimulatedUserId);
	private simulationEntityId$: Observable<number | undefined> = this.store.select(portalStore.getSimulatedEntityId);


	constructor(private store: Store<portalStore.PortalStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return Observable.of(req)
			// Apply user simulation
			.switchMap(simulatingReq => this.simulationUserId$
				.first()
				.map((id?: number) => this.applySimulationRequestParameter(simulatingReq, PortalSimulationInterceptor.SIMULATION_REQUEST_USER_PARAM, id)))
			// Apply entity assignment simulation
			.switchMap(simulatingReq => this.simulationEntityId$
				.first()
				.map((id?: number) => this.applySimulationRequestParameter(simulatingReq, PortalSimulationInterceptor.SIMULATION_REQUEST_ENTITY_PARAM, id)))
			.mergeMap((newReq: HttpRequest<any>) => next.handle(newReq));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private applySimulationRequestParameter(request: HttpRequest<any>, param: string, simulationId?: number): HttpRequest<any> {
		const requestParams: Params = UrlUtils.parseQueryParams(request.body || '');
		const requestHasQueryParam: boolean = Object.prototype.hasOwnProperty.call(requestParams, param);

		let result: HttpRequest<any> = request;
		if (simulationId != null && !requestHasQueryParam) {
			result = result.clone({
				params: request.params.set(param, String(simulationId)),
			});
		}
		return result;
	}
}
