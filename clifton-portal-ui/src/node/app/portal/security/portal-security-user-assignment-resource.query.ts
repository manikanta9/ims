import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalSecurityUserAssignmentResourceQuery extends PortalSearchQuery {
	public userAssignmentId?: number;
	public portalEntityId?: number;
	public roleId: number;
}
