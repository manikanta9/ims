import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalSecurityUserRoleQuery extends PortalSearchQuery {
	public administrator?: boolean;
	public assignmentForPortalEntityId?: number;
	public assignmentRoleOnly?: boolean;
	public description?: string;
	public entityViewTypeId?: number;
	public entityViewTypeIdOrNull?: number;
	public name?: string;
	public nameEquals?: string;
	public portalAdministrator?: boolean;
	public portalEntitySpecific?: boolean;
	public portalSecurityAdministrator?: boolean;
	public searchPattern?: string;
}
