import { Injectable } from '@angular/core';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { OrderBy } from 'app/portal/api/portal-order-by';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSearchQuery } from 'app/portal/api/portal-search-query';
import { PortalAuthQuery } from 'app/portal/security/portal-auth.query';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignment } from 'app/portal/security/portal-security-user-assignment';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserAssignmentResourceQuery } from 'app/portal/security/portal-security-user-assignment-resource.query';
import { PortalSecurityUserAssignmentQuery } from 'app/portal/security/portal-security-user-assignment.query';
import { PortalSecurityUserRole } from 'app/portal/security/portal-security-user-role';
import { PortalSecurityUserRoleQuery } from 'app/portal/security/portal-security-user-role.query';
import { ApiUtils } from 'app/portal/util/api.utils';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalSecurityService {

	constructor(private portalApiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////            Authentication Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	public acceptTermsOfUse(): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityCurrentUserTermsOfUseAccept.json'));
	}


	public login(query: PortalAuthQuery): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUri('j_security_check.json'), query);
	}


	public logout(): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUri('login/logout.js'));
	}


	public async logoutAsPromise(): Promise<PortalResponse> {
		// TODO: Get rid of this. Only here as a Promise to workaround error-handling from authn required right now.
		return this.portalApiService.getPortalResponseWithoutFailureHandling(ApiUtils.getAsApiUri('login/logout.js'));
	}


	public checkAuthentication(): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityUserCurrent.json'));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Current User Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	public getCurrentUser(handleRequestFailure = true): Observable<PortalSecurityUser> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserCurrent.json'), undefined, handleRequestFailure);
	}


	public saveCurrentUser(user: PortalSecurityUser): Observable<PortalSecurityUser> {
		return this.portalApiService.save2(ApiUtils.getAsApiUriWithPrefix('SecurityUserCurrentSave.json'), user);
	}


	public getCurrentUserIsAdmin(): Observable<boolean> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserCurrentSecurityAdmin.json'));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            User Methods                                    ////////
	////////////////////////////////////////////////////////////////////////////


	public getUser(id: number): Observable<Nullable<PortalSecurityUser>> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUser.json'), {id} as PortalSearchQuery);
	}


	public getUserByUsername(username: string, validateEmail = false): Observable<PortalSecurityUser> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserByUserName.json'), {
			username,
			validateEmail,
		} as PortalSearchQuery);
	}


	public getUserByUsernameWithNoAssignment(username: string, entityId?: number, validateEmail = false): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityUserByUserNameAndEntityValidateAssignment.json'), {
			username,
			entityId,
			validateEmail,
		} as PortalSearchQuery);
	}


	public getUserIsAdmin(id: number): Observable<boolean> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserSecurityAdmin.json'), {id});
	}


	public saveUser(user: PortalSecurityUser): Observable<PortalSecurityUser> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserSave.json'), user);
	}


	public updateUserPassword(oldPassword: string, password: string): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityCurrentUserPasswordUpdate.json'), {
			oldPassword,
			password,
		} as PortalSearchQuery);
	}


	public requestPasswordReset(userName: string): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityUserPasswordResetRequest.json'), {userName} as PortalSearchQuery);
	}


	public resetPassword(passwordResetKey: string, newPassword: string): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2WithoutFailureHandling(ApiUtils.getAsApiUriWithPrefix('SecurityUserPasswordReset.json'), {
			passwordResetKey,
			newPassword,
		} as PortalSearchQuery);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            User Role Methods                               ////////
	////////////////////////////////////////////////////////////////////////////


	public getUserRoleList(query?: PortalSecurityUserRoleQuery): Observable<PortalSecurityUserRole[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserRoleListFind.json'), query);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            User Assignment Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	public getUserAssignmentList(query?: PortalSecurityUserAssignmentQuery): Observable<PortalSecurityUserAssignment[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentListFind.json'), query);
	}


	public getCurrentUserAdminUserAssignmentList(orderBy?: OrderBy): Observable<PortalSecurityUserAssignment[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityCurrentUserAssignmentAdministratorList.json'), {orderBy});
	}


	public getAdminUserAssignmentListForUser(userId: number, orderBy?: OrderBy): Observable<PortalSecurityUserAssignment[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentAdministratorListForUser.json'), {
			userId,
			orderBy,
		});
	}


	public saveUserAssignment(userAssignment: PortalSecurityUserAssignment): Observable<PortalSecurityUserAssignment> {
		return this.portalApiService.save2(ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentSave.json'), userAssignment);
	}


	public deleteUserAssignment(id: number, reason: string): Observable<PortalResponse> {
		return this.portalApiService.getPortalResponse2(ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentDisable.json'), {id, reason});
	}


	////////////////////////////////////////////////////////////////////////////
	////////            User Assignment Resource Methods                ////////
	////////////////////////////////////////////////////////////////////////////


	public getUserAssignmentResourceList(query?: PortalSecurityUserAssignmentResourceQuery): Observable<PortalSecurityUserAssignmentResource[]> {
		const entityQuery: boolean = !!(query && query.userAssignmentId != null);
		const queryUri = entityQuery
				? ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentResourceListForAssignmentAndRole.json')
				: ApiUtils.getAsApiUriWithPrefix('SecurityUserAssignmentResourceListForEntityAndRole.json');
		return this.portalApiService.getData2(queryUri, query);
	}
}
