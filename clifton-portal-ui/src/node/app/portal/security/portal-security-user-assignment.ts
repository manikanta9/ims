import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityUserAssignmentResource } from 'app/portal/security/portal-security-user-assignment-resource';
import { PortalSecurityUserRole } from 'app/portal/security/portal-security-user-role';


export class PortalSecurityUserAssignment extends BaseEntity {
	public label?: string;
	public disabled?: boolean;
	public disabledReason?: string;
	@JsonProperty() public securityUser?: PortalSecurityUser;
	@JsonProperty() public portalEntity?: PortalEntity;
	@JsonProperty() public assignmentRole?: PortalSecurityUserRole;
	@JsonProperty() public disabledDate?: Date;
	// TODO: Clean me up
	// @JsonProperty({serializeFn: (obj: any[]) => JSON.stringify(_.map(obj, resource => ObjectUtils.flatten(resource)))})
	public userAssignmentResourceList?: PortalSecurityUserAssignmentResource[];
	// TODO: Verify that this is deserialized/serialized by client correctly
	@JsonProperty() public resourcePermissionMap?: { [resourceId: number]: PortalSecurityUserAssignmentResource };
}
