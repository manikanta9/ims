import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injector, NgModule, Optional, Provider, SkipSelf } from '@angular/core';
import { ModalDialogModule } from 'app/core/modal/modal-dialog.module';
import { ExceptionUtils } from 'app/core/util/exception.utils';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFeedbackService } from 'app/portal/feedback/portal-feedback.service';
import { PortalFileDownloadService } from 'app/portal/file/download/portal-file-download.service';
import { PortalFileCategoryEmailService } from 'app/portal/file/portal-file-category-email.service';
import { PortalFileCategoryService } from 'app/portal/file/portal-file-category.service';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { PortalNotificationSubscriptionEntryService } from 'app/portal/notification/portal-notification-subscription-entry.service';
import { PortalNotificationService } from 'app/portal/notification/portal-notification.service';
import { PortalPageElementService } from 'app/portal/page/portal-page-element.service';
import { PortalSecurityResourceService } from 'app/portal/security/portal-security-resource.service';
import { PortalSecurityUserAssignmentResourceService } from 'app/portal/security/portal-security-user-assignment-resource.service';
import { PortalSecurityUserAssignmentService } from 'app/portal/security/portal-security-user-assignment.service';
import { PortalSecurityUserCurrentService } from 'app/portal/security/portal-security-user-current.service';
import { PortalSecurityUserRoleService } from 'app/portal/security/portal-security-user-role.service';
import { PortalSecurityUserService } from 'app/portal/security/portal-security-user.service';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import { PortalSimulationInterceptor } from 'app/portal/security/simulation/portal-simulation-interceptor';
import { PortalSimulationService } from 'app/portal/security/simulation/portal-simulation.service';
import { PortalStateModule } from 'app/portal/state/portal-state.module';
import { environment } from 'environments/environment';


const SIMULATION_PROVIDERS: Provider[] = [
	PortalSimulationService,
	{provide: HTTP_INTERCEPTORS, useClass: PortalSimulationInterceptor, multi: true},
];


@NgModule({
	imports: [
		ModalDialogModule,
		PortalStateModule,
	],
	declarations: [],
	exports: [],
	providers: [
		...environment.administrationMode ? SIMULATION_PROVIDERS : [],
		PortalApiService,
		PortalEntityService,
		PortalFeedbackService,
		PortalFileCategoryEmailService,
		PortalFileCategoryService,
		PortalFileDownloadService,
		PortalFileService,
		PortalNotificationService,
		PortalNotificationSubscriptionEntryService,
		PortalPageElementService,
		PortalSecurityResourceService,
		PortalSecurityService,
		PortalSecurityUserAssignmentResourceService,
		PortalSecurityUserAssignmentService,
		PortalSecurityUserCurrentService,
		PortalSecurityUserRoleService,
		PortalSecurityUserService,
	],
})
export class PortalModule {
	constructor(@Optional() @SkipSelf() instance: PortalModule,
	            @Optional() injector: Injector) {
		ExceptionUtils.throwIfAlreadyLoaded(PortalModule, instance, injector);
		this.generateSingletons(injector);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private generateSingletons(injector: Injector): void {
		if (environment.administrationMode) {
			// Auto-generated services
			injector.get(PortalSimulationService);
		}
	}
}
