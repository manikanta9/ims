import * as rootStore from 'app/core/state/reducers/root.store';
import { PageElementMultipleTypes, PageElementSingleTypes } from 'app/portal/page/portal-page-element-type';
import * as notificationAcknowledgements from 'app/portal/state/reducers/notification-acknowledgements.reducer';
import * as pageElement from 'app/portal/state/reducers/page-element.reducer';
import * as simulation from 'app/portal/state/reducers/simulation.reducer';
import * as user from 'app/portal/state/reducers/user.reducer';
import { createSelector } from 'reselect';


export * from 'app/core/state/reducers/root.store';


// State
export interface PortalStore extends rootStore.RootStore {
	user: user.UserState;
	simulation: simulation.SimulationState;
	pageElement: pageElement.PageElementState;
	notificationAcknowledgements: notificationAcknowledgements.NotificationAcknowledgementState;
}


// Map of reducer per store slice
export const portalReducerMap: rootStore.ReducerMap<PortalStore> = {
	user: user.userReducer,
	simulation: simulation.simulationReducer,
	pageElement: pageElement.pageElementReducer,
	notificationAcknowledgements: notificationAcknowledgements.notificationAcknowledgementsReducer,
};


// TODO: Set initial state


// Memoized selectors
export const getUserState: (state: PortalStore) => user.UserState = (state: PortalStore) => state.user;
export const isAuthenticated = createSelector(getUserState, user.isAuthenticated);
export const getUser = createSelector(getUserState, user.getUser);
export const isAdmin = createSelector(getUserState, user.isAdmin);
export const getAccessGroups = createSelector(getUserState, user.getAccessGroups);
export const hasAccessGroup = (groupName: string) => createSelector(getUserState, user.hasAccessGroup(groupName));

// TODO: Rename user-state elements to RealUser, add user-state elements for real->simulated coalesced state
export const getSimulationState: (state: PortalStore) => simulation.SimulationState = (state: PortalStore) => state.simulation;
export const isSimulatingUser = createSelector(getSimulationState, simulation.isSimulatingUser);
export const getSimulatedUserId = createSelector(getSimulationState, simulation.getSimulatedUserId);
export const getSimulatedUserInfo = createSelector(getSimulationState, simulation.getSimulatedUserInfo);
export const getSimulatedUserIsAdmin = createSelector(getSimulationState, simulation.getSimulatedUserIsAdmin);
export const getSimulatedUserAccessGroups = createSelector(getSimulationState, simulation.getSimulatedUserAccessGroups);
export const getSimulatedUserHasAccessGroup = (groupName: string) => createSelector(getSimulationState, simulation.getSimulatedUserHasAccessGroup(groupName));
export const isSimulatingEntity = createSelector(getSimulationState, simulation.isSimulatingEntity);
export const getSimulatedEntityId = createSelector(getSimulationState, simulation.getSimulatedEntityId);
export const getSimulatedEntityInfo = createSelector(getSimulationState, simulation.getSimulatedEntityInfo);
export const getSimulatedEntityAccessGroups = createSelector(getSimulationState, simulation.getSimulatedEntityAccessGroups);
export const getSimulatedEntityHasAccessGroup = (groupName: string) => createSelector(getSimulationState, simulation.getSimulatedEntityHasAccessGroup(groupName));

export const getPageElementState: (state: PortalStore) => pageElement.PageElementState = (state: PortalStore) => state.pageElement;
export const getAllPageElements = createSelector(getPageElementState, pageElement.getAllPageElements);
export const getPageElementSingle = (elementKey: PageElementSingleTypes) => createSelector(getPageElementState, pageElement.getPageElementSingle(elementKey));
export const getPageElementMulti = (elementKey: PageElementMultipleTypes) => createSelector(getPageElementState, pageElement.getPageElementMulti(elementKey));


export const getNotificationAcknowledgmentState: (state: PortalStore) => notificationAcknowledgements.NotificationAcknowledgementState = (state: PortalStore) => state.notificationAcknowledgements;
export const getAllNotificationAcknowledgements = createSelector(getNotificationAcknowledgmentState, notificationAcknowledgements.getAllAcknowledgeNotifications);
export const getFirstNotificationAcknowledgement = createSelector(getNotificationAcknowledgmentState, notificationAcknowledgements.getFirstNotificationAcknowledgement);

