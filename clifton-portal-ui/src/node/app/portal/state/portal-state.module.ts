import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StateService } from 'app/core/state/state.service';
import * as portalStore from 'app/portal/state/portal.store';
import { SimulationEffects } from 'app/portal/state/reducers/simulation.effects';
import { UserEffects } from 'app/portal/state/reducers/user.effects';
import { environment } from 'environments/environment';


@NgModule({
	imports: [
		// Effects modules
		EffectsModule.run(UserEffects),
		...environment.administrationMode ? [EffectsModule.run(SimulationEffects)] : [],
	],
	declarations: [],
})
export class PortalStateModule {
	constructor(stateService: StateService) {
		stateService.addReducers(portalStore.portalReducerMap);
	}
}
