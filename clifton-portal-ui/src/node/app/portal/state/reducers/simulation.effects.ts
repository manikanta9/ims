import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityService } from 'app/portal/entity/portal-entity.service';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalFileService } from 'app/portal/file/portal-file.service';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import { PortalSecurityService } from 'app/portal/security/portal-security.service';
import * as portalStore from 'app/portal/state/portal.store';
import * as simulation from 'app/portal/state/reducers/simulation.reducer';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SimulationEffects {

	constructor(private actions$: Actions,
				private store: Store<portalStore.PortalStore>,
				private portalSecurityService: PortalSecurityService,
				private portalEntityService: PortalEntityService,
				private portalFileService: PortalFileService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Get user information when simulated user ID is set
	@Effect() public simulateUserEffect$: Observable<Action> = this.actions$
		.ofType(simulation.SET_SIMULATED_USER_ID)
		.map(toPayload)
		.debounce(() => this.store.select(portalStore.isAuthenticated).truthy()) // Wait until authenticated
		.switchMap((id?: number) => {
			if (id == null) {
				return Observable.empty();
			}

			// Update info
			return Observable.merge(
				// Update user info
				this.portalSecurityService.getUser(id)
					.map((simulatedUser: PortalSecurityUser) => new simulation.SetSimulatedUserInfoAction(simulatedUser))
					.first(),
				// Update "admin" status
				this.portalSecurityService.getUserIsAdmin(id)
					.map((isAdmin: boolean) => new simulation.SetSimulatedUserAdminAction(isAdmin))
					.first(),
				// Update access groups
				this.portalFileService.getFileCategoryList({requestedProperties: 'name', rootOnly: true, viewAsUserId: id})
					.map((fileCategoryList: PortalFileCategory[]) => fileCategoryList.map(fileCategory => fileCategory.name!))
					.map((accessGroups: string[]) => new simulation.SetSimulatedUserAccessGroupsAction(accessGroups))
					.first(),
			);
		});

	// Get entity information when simulated entity ID is set
	@Effect() public simulateEntityEffect$: Observable<Action> = this.actions$
		.ofType(simulation.SET_SIMULATED_ENTITY_ID)
		.map(toPayload)
		.debounce(() => this.store.select(portalStore.isAuthenticated).truthy()) // Wait until authenticated
		.switchMap((id?: number) => {
			if (id == null) {
				return Observable.empty();
			}

			// Update info
			return Observable.merge(
				// Update user info
				this.portalEntityService.getEntity(id)
					.map((simulatedEntity: PortalEntity) => new simulation.SetSimulatedEntityInfoAction(simulatedEntity))
					.first(),
				// Update access groups
				this.portalFileService.getFileCategoryList({requestedProperties: 'name', rootOnly: true, viewAsEntityId: id})
					.map((fileCategoryList: PortalFileCategory[]) => fileCategoryList.map(fileCategory => fileCategory.name!))
					.map((accessGroups: string[]) => new simulation.SetSimulatedEntityAccessGroupsAction(accessGroups))
					.first(),
			);
		});
}
