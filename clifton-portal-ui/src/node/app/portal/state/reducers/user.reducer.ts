import { ActionReducer } from '@ngrx/store';
import { TypeUtils } from 'app/core/util/type.utils';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import * as user from 'app/portal/state/reducers/user.actions';


export * from 'app/portal/state/reducers/user.actions';


// State
export const storeName = 'user';


export interface UserState {
	currentUser?: PortalSecurityUser;
	isAdministrator?: boolean;
	accessGroups?: string[];
}


export const initialState: UserState = {
	currentUser: void 0,
	isAdministrator: void 0,
	accessGroups: void 0,
};


// Reducers
export const userReducer: ActionReducer<UserState> = (state: UserState = initialState, action: user.Actions): UserState => {
	switch (action.type) {
		case user.LOGIN:
			return {...state, currentUser: action.payload};
		case user.REFRESH:
			return {...state, currentUser: action.payload};
		case user.LOGOUT:
			return {...state, currentUser: void 0};
		case user.SET_ADMIN:
			return {...state, isAdministrator: action.payload};
		case user.SET_ACCESS_GROUPS:
			return {...state, accessGroups: action.payload};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export const isAuthenticated = (state: UserState = initialState) => state.currentUser != null;
export const getUser = (state: UserState = initialState) => state.currentUser;
export const isAdmin = (state: UserState = initialState) => state.isAdministrator;
export const getAccessGroups = (state: UserState = initialState) => state.accessGroups;
export const hasAccessGroup = (group: string) => (state: UserState = initialState) => (state.accessGroups || []).includes(group);

