import { ActionReducer } from '@ngrx/store';
import { TypeUtils } from 'app/core/util/type.utils';
import { PortalNotification } from 'app/portal/notification/portal-notification';
import * as notificationAcknowledgements from 'app/portal/state/reducers/notification-acknowledgements.actions';


export * from 'app/portal/state/reducers/notification-acknowledgements.actions';


// State
export const storeName = 'notification-acknowledgements';


export interface NotificationAcknowledgementState {
	elements?: PortalNotification[];
}


export const initialState: NotificationAcknowledgementState = {
	elements: void 0,
};


// Reducers
export const notificationAcknowledgementsReducer: ActionReducer<NotificationAcknowledgementState> = (state: NotificationAcknowledgementState = initialState, action: notificationAcknowledgements.Actions): NotificationAcknowledgementState => {
	switch (action.type) {
		case '[Notification Acknowledgements] Set Notifications':
			return {...state, elements: action.payload};
		case '[Notification Acknowledgements] Clear Notifications':
			return {...state, elements: void 0};
		case '[Notification Acknowledgements] Acknowledge Notification':
			const notification: PortalNotification = action.payload;
			return {...state, elements: state.elements!.filter((element: { id:number,acknowledged:boolean }) => element.id !== notification.id)};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


export const getAllAcknowledgeNotifications = (state: NotificationAcknowledgementState = initialState) => state.elements;
export const getFirstNotificationAcknowledgement = (state: NotificationAcknowledgementState = initialState) => (state.elements && state.elements.length > 0) ? state.elements[0] : undefined;
export const isPendingNotifications = (state: NotificationAcknowledgementState = initialState) => state.elements!.length > 0;
