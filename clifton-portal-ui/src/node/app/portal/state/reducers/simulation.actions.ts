// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';


// Action names
export const SET_SIMULATED_USER_ID = '[Simulation] Set Simulated User ID';
export const SET_SIMULATED_USER_INFO = '[Simulation] Set Simulated User Info';
export const SET_SIMULATED_USER_ADMIN = '[Simulation] Set Simulated User Administrator';
export const SET_SIMULATED_USER_ACCESS_GROUPS = '[Simulation] Set Simulated User Access Groups';
export const CLEAR_SIMULATED_USER = '[Simulation] Clear Simulated User';

export const SET_SIMULATED_ENTITY_ID = '[Simulation] Set Simulated Entity ID';
export const SET_SIMULATED_ENTITY_INFO = '[Simulation] Set Simulated Entity Info';
export const SET_SIMULATED_ENTITY_ACCESS_GROUPS = '[Simulation] Set Simulated Entity Access Groups';
export const CLEAR_SIMULATED_ENTITY = '[Simulation] Clear Simulated Entity';


// Action types

/* User */
export class SetSimulatedUserIdAction implements Action {
	public readonly type = SET_SIMULATED_USER_ID;


	constructor(public payload?: number) { }
}


export class SetSimulatedUserInfoAction implements Action {
	public readonly type = SET_SIMULATED_USER_INFO;


	constructor(public payload?: PortalSecurityUser) { }
}


export class SetSimulatedUserAdminAction implements Action {
	public readonly type = SET_SIMULATED_USER_ADMIN;


	constructor(public payload: boolean) { }
}


export class SetSimulatedUserAccessGroupsAction implements Action {
	public readonly type = SET_SIMULATED_USER_ACCESS_GROUPS;


	constructor(public payload: string[]) { }
}


export class ClearSimulatedUserAction implements Action {
	public readonly type = CLEAR_SIMULATED_USER;
}


/* Entity */
export class SetSimulatedEntityIdAction implements Action {
	public readonly type = SET_SIMULATED_ENTITY_ID;


	constructor(public payload?: number) { }
}


export class SetSimulatedEntityInfoAction implements Action {
	public readonly type = SET_SIMULATED_ENTITY_INFO;


	constructor(public payload?: PortalEntity) { }
}


export class SetSimulatedEntityAccessGroupsAction implements Action {
	public readonly type = SET_SIMULATED_ENTITY_ACCESS_GROUPS;


	constructor(public payload: string[]) { }
}


export class ClearSimulatedEntityAction implements Action {
	public readonly type = CLEAR_SIMULATED_ENTITY;
}


// Action type union
export type Actions =
	| SetSimulatedUserIdAction
	| SetSimulatedUserInfoAction
	| SetSimulatedUserAdminAction
	| SetSimulatedUserAccessGroupsAction
	| ClearSimulatedUserAction
	| SetSimulatedEntityIdAction
	| SetSimulatedEntityInfoAction
	| SetSimulatedEntityAccessGroupsAction
	| ClearSimulatedEntityAction
	;
