// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { PortalNotification } from 'app/portal/notification/portal-notification';


// Action names
export const SET_NOTIFICATIONS = '[Notification Acknowledgements] Set Notifications';
export const CLEAR_NOTIFICATIONS = '[Notification Acknowledgements] Clear Notifications';
export const ACKNOWLEDGE_NOTIFICATION = '[Notification Acknowledgements] Acknowledge Notification';


// Action types
export class SetNotificationAcknowledgementsAction implements Action {
	public readonly type = SET_NOTIFICATIONS;


	constructor(public payload: PortalNotification[]) { }
}


export class ClearNotificationAcknowledgementsAction implements Action {
	public readonly type = CLEAR_NOTIFICATIONS;


	constructor() { }
}


export class AcknowledgeNotificationAction implements Action {
	public readonly type = ACKNOWLEDGE_NOTIFICATION;


	constructor(public payload: { id: number, acknowledged: boolean }) { }
}


// Action type union
export type Actions =
	| SetNotificationAcknowledgementsAction
	| ClearNotificationAcknowledgementsAction
	| AcknowledgeNotificationAction
	;
