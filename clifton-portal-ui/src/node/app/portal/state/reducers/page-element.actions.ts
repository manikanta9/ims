// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { PortalPageElement } from 'app/portal/page/portal-page-element';


// Action names
export const SET_PAGE_ELEMENTS = '[Page Elements] Set Page Elements';
export const CLEAR_PAGE_ELEMENTS = '[Page Elements] Clear Page Elements';


// Action types
export class SetPageElementsAction implements Action {
	public readonly type = SET_PAGE_ELEMENTS;


	constructor(public payload: PortalPageElement[]) { }
}


export class ClearPageElementsAction implements Action {
	public readonly type = CLEAR_PAGE_ELEMENTS;


	constructor() { }
}


// Action type union
export type Actions =
	| SetPageElementsAction
	| ClearPageElementsAction
	;
