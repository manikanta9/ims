import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import * as user from 'app/portal/state/reducers/user.reducer';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UserEffects {

	constructor(private actions$: Actions) {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Reset state on logout
	@Effect() public logoutEffect$: Observable<Action> = this.actions$
		.ofType(user.LOGOUT)
		.do(() => window.location.reload())
		.switchMapTo(Observable.empty());
}
