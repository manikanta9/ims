import { ActionReducer } from '@ngrx/store';
import { TypeUtils } from 'app/core/util/type.utils';
import { PortalEntityViewType } from 'app/portal/entity/portal-entity-view-type';
import { PageElementMultipleTypes, PageElementSingleTypes, PageElementTypes } from 'app/portal/page/portal-page-element-type';
import * as pageElement from 'app/portal/state/reducers/page-element.actions';
import * as _ from 'lodash';
import { createSelector } from 'reselect';


export * from 'app/portal/state/reducers/page-element.actions';


// State
export const storeName = 'page-element';


export interface PageElementState {
	elements?: PageElementHolder;
}


export const initialState: PageElementState = {
	elements: void 0,
};


export type PageElementHolder = {
	[key in PageElementTypes]?: PageElementInfo[];
};


export interface PageElementInfo {
	key: string;
	value: string;
	order: number | null;
	viewType?: PortalEntityViewType;
}


// Reducers
export const pageElementReducer: ActionReducer<PageElementState> = (state: PageElementState = initialState, action: pageElement.Actions): PageElementState => {
	switch (action.type) {
		case pageElement.SET_PAGE_ELEMENTS:
			const elementInfos: PageElementInfo[] = action.payload
				.map(element => ({key: element.type.key!, value: element.value, order: element.order, viewType: element.viewType}))
				.sort((element1, element2) => element1.order == null ? element2.order == null ? 0 : -1 : element2.order == null ? 1 : element1.order - element2.order);
			const elements = _.groupBy(elementInfos, element => element.key);
			return {...state, elements};
		case pageElement.CLEAR_PAGE_ELEMENTS:
			return {...state, elements: void 0};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export const getAllPageElements = (state: PageElementState = initialState) => state.elements;
export const getPageElements = (key: keyof PageElementHolder) => createSelector(getAllPageElements, elements => (elements && elements[key]) || []);
export const getPageElementSingle = (key: PageElementSingleTypes) => createSelector(getPageElements(key), elements => elements[0]);
export const getPageElementMulti = (key: PageElementMultipleTypes) => getPageElements(key);
