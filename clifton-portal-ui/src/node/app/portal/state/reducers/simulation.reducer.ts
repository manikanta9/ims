import { ActionReducer } from '@ngrx/store';
import { TypeUtils } from 'app/core/util/type.utils';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';
import * as simulation from 'app/portal/state/reducers/simulation.actions';


export * from 'app/portal/state/reducers/simulation.actions';


// State
export const storeName = 'simulation';


export interface SimulationState {
	user?: {
		id?: number;
		info?: PortalSecurityUser;
		isAdministrator?: boolean;
		accessGroups?: string[];
	};
	entity?: {
		id?: number;
		info?: PortalEntity;
		accessGroups?: string[];
	};
}


export const initialState: SimulationState = {
	user: void 0,
	entity: void 0,
};


// Reducers
export const simulationReducer: ActionReducer<SimulationState> = (state: SimulationState = initialState, action: simulation.Actions): SimulationState => {
	switch (action.type) {
		case simulation.SET_SIMULATED_USER_ID:
			return {...state, user: {id: action.payload}}; // Clear simulated user detail
		case simulation.SET_SIMULATED_USER_INFO:
			return {...state, user: {...state && state.user, info: action.payload}};
		case simulation.SET_SIMULATED_USER_ADMIN:
			return {...state, user: {...state && state.user, isAdministrator: action.payload}};
		case simulation.SET_SIMULATED_USER_ACCESS_GROUPS:
			return {...state, user: {...state && state.user, accessGroups: action.payload}};
		case simulation.CLEAR_SIMULATED_USER:
			return {...state, user: void 0};
		case simulation.SET_SIMULATED_ENTITY_ID:
			return {...state, entity: {id: action.payload}}; // Clear simulated user detail
		case simulation.SET_SIMULATED_ENTITY_INFO:
			return {...state, entity: {...state && state.entity, info: action.payload}};
		case simulation.SET_SIMULATED_ENTITY_ACCESS_GROUPS:
			return {...state, entity: {...state && state.entity, accessGroups: action.payload}};
		case simulation.CLEAR_SIMULATED_ENTITY:
			return {...state, entity: void 0};
		default:
			TypeUtils.assertNever(action);
			return state;
	}
};


// Selectors
export const isSimulatingUser = (state: SimulationState = initialState) => state.user != null;
export const getSimulatedUserId = (state: SimulationState = initialState) => isSimulatingUser(state) ? state.user!.id : void 0;
export const getSimulatedUserInfo = (state: SimulationState = initialState) => isSimulatingUser(state) ? state.user!.info : void 0;
export const getSimulatedUserIsAdmin = (state: SimulationState = initialState) => isSimulatingUser(state) ? state.user!.isAdministrator : void 0;
export const getSimulatedUserAccessGroups = (state: SimulationState = initialState) => isSimulatingUser(state) ? state.user!.accessGroups : void 0;
export const getSimulatedUserHasAccessGroup = (group: string) => (state: SimulationState = initialState) => isSimulatingUser(state) ? (state.user!.accessGroups || []).includes(group) : false;

export const isSimulatingEntity = (state: SimulationState = initialState) => state.entity != null;
export const getSimulatedEntityId = (state: SimulationState = initialState) => isSimulatingEntity(state) ? state.entity!.id : void 0;
export const getSimulatedEntityInfo = (state: SimulationState = initialState) => isSimulatingEntity(state) ? state.entity!.info : void 0;
export const getSimulatedEntityAccessGroups = (state: SimulationState = initialState) => isSimulatingEntity(state) ? state.entity!.accessGroups : void 0;
export const getSimulatedEntityHasAccessGroup = (group: string) => (state: SimulationState = initialState) => isSimulatingEntity(state) ? (state.entity!.accessGroups || []).includes(group) : false;
