// tslint:disable:max-classes-per-file : State action class
import { Action } from '@ngrx/store';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';


// Action names
export const LOGIN = '[User] Login';
export const REFRESH = '[User] Refresh';
export const LOGOUT = '[User] Logout';
export const SET_ADMIN = '[User] Set Administrator';
export const SET_ACCESS_GROUPS = '[User] Set Access Groups';


// Action types
export class LoginAction implements Action {
	public readonly type = LOGIN;


	constructor(public payload: PortalSecurityUser) { }
}


export class RefreshUserAction implements Action {
	public readonly type = REFRESH;


	constructor(public payload: PortalSecurityUser) { }
}


export class LogoutAction implements Action {
	public readonly type = LOGOUT;
}


export class SetAdministratorAction implements Action {
	public readonly type = SET_ADMIN;


	constructor(public payload: boolean) { }
}


export class SetAccessGroupsAction implements Action {
	public readonly type = SET_ACCESS_GROUPS;


	constructor(public payload: string[]) { }
}


// Action type union
export type Actions =
	| LoginAction
	| RefreshUserAction
	| LogoutAction
	| SetAdministratorAction
	| SetAccessGroupsAction
	;
