export type OrderBy = string | {
	field: string;
	direction?: 'ASC' | 'DESC';
}[];
