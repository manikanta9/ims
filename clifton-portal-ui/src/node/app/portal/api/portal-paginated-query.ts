import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export type PortalPaginatedQuery<T extends PortalSearchQuery> = {
	[key in keyof T]: T[key];
	} & {
	start?: number;
	/**
	 * The limit on the number of values in the response. This is only allowed for paginated queried to prevent values that would result in a paginated
	 * response. See {@link ApiUtils#isPaginatedQuery} for more details.
	 *
	 * Note that the base query could be limited to valid numbers (<tt>0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9</tt>), but this would not allow us to inherit from
	 * the base query, as inheritance cannot expand permitted types for an existing field.
	 */
	limit?: number;
};
