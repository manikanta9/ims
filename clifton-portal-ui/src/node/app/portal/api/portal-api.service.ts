import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Headers, Http, RequestOptionsArgs, Response } from '@angular/http';
import { Store } from '@ngrx/store';
import { BaseEntity } from 'app/core/api/base-entity';
import { BasePaginatedEntity } from 'app/core/api/base-paginated-entity';
import { LogUtils } from 'app/core/log/log.utils';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { FormUtils } from 'app/core/util/form.utils';
import { JsonUtils } from 'app/core/util/json.utils';
import { StringUtils } from 'app/core/util/string.utils';
import * as pageStore from 'app/page/shared/state/page.store';
import { PortalPaginatedQuery } from 'app/portal/api/portal-paginated-query';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSearchQuery } from 'app/portal/api/portal-search-query';
import * as user from 'app/portal/state/reducers/user.actions';
import { ApiUtils } from 'app/portal/util/api.utils';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalApiService {

	private static readonly RESPONSE_RESET_DELIMITER: string = '\n// RESET ';


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	constructor(private http: Http, // TODO: Remove me
	            private httpClient: HttpClient,
	            private modalService: ModalDialogService,
	            private store: Store<pageStore.PageStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private async get(uri: string, body?: string, options?: RequestOptionsArgs): Promise<Response> {
		return this.http.post(uri, body, options)
			.timeout(10000)
			.catch((error: Response | any, request$: Observable<Response>): Observable<Response> => this.handleException(error, {url: uri}))
			.toPromise();
	}


	public async getPortalResponse(uri: string, query?: PortalSearchQuery | null): Promise<PortalResponse> {
		return this.doGetPortalResponse(uri, query, true);
	}


	public async getPortalResponseWithoutFailureHandling(uri: string, query?: PortalSearchQuery | null): Promise<PortalResponse> {
		return this.doGetPortalResponse(uri, query, false);
	}


	private async doGetPortalResponse(uri: string, query?: PortalSearchQuery | null, handleFailure?: boolean): Promise<PortalResponse> {
		// Prepare request
		const body: string = (query != null) ? this.prepareBody(this.configureQuery(query)) : '';
		const headers: RequestOptionsArgs = this.prepareHeaders();

		// Execute request
		const response: Response = await this.get(uri, body, headers);
		const portalResponse: PortalResponse = this.readPortalResponse(response);
		if (handleFailure && !portalResponse.success) {
			return this.handleFailure(portalResponse, response).toPromise();
		}
		return portalResponse;
	}


	public async getData<T>(uri: string, query?: PortalSearchQuery | null, resultType?: Type<T>): Promise<T[]> {
		const portalResponse: PortalResponse = await this.getPortalResponse(uri, query);
		return portalResponse.data ? this.readData(portalResponse, resultType) : [];
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private get2(uri: string, body?: string, httpHeaders?: HttpHeaders): Observable<HttpResponse<string>> {
		return this.httpClient.post(uri, body, {
			observe: 'response',
			responseType: 'text',
			headers: httpHeaders,
		});
		// timeout is causing a strange issue right now where all requests seem to time out, even if they've succeeded; using built-in browser timeouts for now
		// .timeout(environment.requestTimeout)
	}


	private doGetPortalResponse2(uri: string, query?: object, handleFailure = true): Observable<PortalResponse> {
		return this.get2(uri, this.prepareBody(this.configureQuery(query)), new HttpHeaders({
				Accept: 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded',
			}))
			.mergeMap((response: HttpResponse<string>) => {
				const responseText = response.body ? this.cleanResponseText(response.body) : response.body;
				const responseJson = JsonUtils.deserialize(responseText) as PortalResponse;
				return (handleFailure && responseJson && !responseJson.success)
					? this.handleFailure(responseJson, response)
					: Observable.of(responseJson);
			})
			.catch((error: HttpResponse<string> | any, request$: Observable<PortalResponse>): Observable<PortalResponse> => {
				if (!handleFailure) {
					throw new Error(error.message || error);
				}
				return this.handleException(error, {url: uri});
			});
	}


	public getPortalResponse2WithoutFailureHandling(uri: string, query?: object): Observable<PortalResponse> {
		return this.doGetPortalResponse2(uri, query, false);
	}


	public getPortalResponse2(uri: string, query?: object): Observable<PortalResponse> {
		return this.doGetPortalResponse2(uri, query);
	}


	public getData2<T>(uri: string, query?: object, handleFailure = true): Observable<T> {
		return this.doGetPortalResponse2(uri, query, handleFailure)
			.map((response: PortalResponse) => ApiUtils.getPortalResponseData(response));
	}


	public getPaginatedData2<T>(uri: string, query?: PortalPaginatedQuery<object>): Observable<BasePaginatedEntity<T>> {
		return this.getPortalResponse2(uri, query)
			.map((response: PortalResponse) => ApiUtils.getPortalResponsePaginatedData(response));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// TODO: Can we get this to return an array every time instead?
	public async save<T>(uri: string, entity: T, entityType?: Type<T>): Promise<T[]> {
		let typedEntity: T = entity;
		if (entityType && !(entity instanceof entityType)) {
			LogUtils.log('Entity is not of required type:', entity, entityType, '- Serializing...');
			typedEntity = JsonUtils.transformToClient(entity, entityType) as T;
			LogUtils.log('Serialized entity:', typedEntity);
		}
		else {
			LogUtils.log('Entity is of required type:', entity, entityType);
		}
		return await this.getData(uri, typedEntity, entityType);
	}


	public save2<T>(uri: string, entity: T): Observable<T> {
		return this.getData2(uri, entity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private configureQuery(query?: PortalSearchQuery): PortalSearchQuery {
		// TODO: Move this into transform decorator on query object instead
		const configuredQuery: PortalSearchQuery = {...query};
		// Transform requested properties
		if (configuredQuery.requestedProperties) {
			if (Array.isArray(configuredQuery.requestedProperties)) {
				configuredQuery.requestedProperties = configuredQuery.requestedProperties.join('|');
			}
			if (configuredQuery.requestedPropertiesRoot == null) {
				configuredQuery.requestedPropertiesRoot = ApiUtils.isPaginatedQuery(configuredQuery) ? 'data.rows' : 'data';
			}
		}

		// Transform sorting parameter
		if (Array.isArray(configuredQuery.orderBy)) {
			configuredQuery.orderBy = configuredQuery.orderBy.map(orderByOperator => `${orderByOperator.field}:${orderByOperator.direction}`).join('#');
		}
		return configuredQuery;
	}


	private prepareBody(parameters: object): string {
		return FormUtils.serialize(parameters);
	}


	private prepareHeaders(): RequestOptionsArgs {
		const headers = new Headers();
		headers.append('Accept', 'application/json');
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		// TODO: Switch to JSON request format when available
		// headers.append('Content-Type', 'application/json');
		return {headers};
	}


	private cleanResponseText(responseText: string): string {
		// Clean response text, accounting for any reset delimiters
		const lastResetIndex = responseText.lastIndexOf(PortalApiService.RESPONSE_RESET_DELIMITER);
		return lastResetIndex > -1
			? responseText.substring(lastResetIndex + PortalApiService.RESPONSE_RESET_DELIMITER.length)
			: responseText;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private readPortalResponse(response: Response): PortalResponse {
		// Deserialize response
		const responseText = this.cleanResponseText(response.text());
		const responseObj = JsonUtils.deserialize(responseText);
		return JsonUtils.transformToClient(responseObj, PortalResponse) as PortalResponse;
	}


	private readData<T>(portalResponse: PortalResponse, type?: Type<T>): T[] {
		const data = portalResponse.data;
		let entities: any[];
		if (!data) {
			entities = [];
		}
		else if (_.isArray(data)) {
			entities = data;
		}
		else if (_.isArray((data as any).rows)) {
			const rowData = data as { rows: BaseEntity[] };
			entities = rowData.rows;
		}
		else {
			entities = [data];
		}
		return JsonUtils.transformToClient(entities, type) as T[];
	}


	private handleException(error: any, options?: any, e?: any): never {
		LogUtils.log('An exception occured will calling the server: ', error);
		const response: Response = error;
		let status: any = null;
		try {
			status = response.status;
		} catch (ex) {
			status = 'CANNOT READ STATUS';
		}
		if (status != null) {
			const baseMessage = `Error sending server request to:\n\n${options && options.url ? options.url : 'UNKNOWN URL'}\n\n`;
			let title: string;
			let msg: string;
			switch (status) {
				case 403:
					title = 'Access Denied';
					msg = baseMessage + 'You do not have permission to access requested resource.';
					break;
				case 404:
					title = 'Page Not Found';
					msg = baseMessage + 'Cannot locate requested resource.';
					break;
				case 500:
					title = 'Server Error';
					/*
					 * Note: We avoid the string "internal<space>error" here since the OWASP ZAP tool flags this as exposure of internal server errors
					 * (CWE-200). We do not want to expose internal server errors, but we do want to notify the user appropriately when one has occurred.
					 *
					 * See https://github.com/zaproxy/zap-extensions/blob/a027020e6269515fea528e97eb6bf06f3696c462/src/org/zaproxy/zap/extension/pscanrules/
					 * resources/application_errors.xml for error patterns, and https://github.com/zaproxy/zap-extensions/blob/
					 * a027020e6269515fea528e97eb6bf06f3696c462/src/org/zaproxy/zap/extension/pscanrules/ApplicationErrorScanner.java for the application error
					 * scanner plugin.
					 */
					msg = baseMessage + 'The server encountered an error that prevented it from fulfilling this request.';
					break;
				default:
					if (e) {
						title = 'Client Side Script Error';
						msg = baseMessage + String(e);
					}
					else {
						title = 'Server Error';
						msg = `${baseMessage}Response status: ${response.statusText}`;
					}
			}
			this.modalService.showError(msg, title);
		}
		throw new Error(error.message || error);
	}


	private handleFailure(portalResponse: PortalResponse, response: Response | HttpResponse<any>): Observable<any> {
		// only handle failures when the user is logged in
		if (!portalResponse.loginFailed) {
			if (portalResponse.authenticationRequired) {
				this.store.dispatch(new user.LogoutAction());
				return Observable.of(false);
			}
			else {
				this.handleResponseStatusAlert(portalResponse, response);
			}
			throw portalResponse;
		}
		return Observable.of(portalResponse);
	}


	private async handleResponseStatusAlert(portalResponse: PortalResponse, response: Response | HttpResponse<any>) {
		switch (response.status) {
			case 403:
				this.modalService.showError('You do not have permission to access requested resource.', 'Access Denied');
				break;
			case 404:
				this.modalService.showError('Resource not Found. The requested resource is not available.', 'Page not Found');
				break;
			default:
				let title: string;
				let msg: string;
				// 'validationError' check covers ValidationException, and 'errors' covers ServletRequestBindingException and FieldValidationException
				if (portalResponse.validationError || portalResponse.errors) {
					title = 'Validation Error(s)';
					const errorMessage = portalResponse.errorMessage || portalResponse.message;
					if (errorMessage) {
						msg = 'Please correct the following validation error:<br/><br/>'
							+ errorMessage.replace(/(\r\n|\n|\r)/gm, '<br/>');
					}
					else if (portalResponse.errors) {
						msg = 'Please correct the following error(s):<br/><br/>'
							+ portalResponse.errors
								.map(error => `<li>${error.msg}</li>`)
								.join('');
					}
					else {
						msg = `UNKNOWN RESULT FORMAT: ${response}`;
					}
				}
				else {
					title = 'Error';
					msg = 'An unexpected error has occurred.';
					// Store accesses are synchronous, so "msg" variable mutation will happen before continuing
					this.store.select(pageStore.getPageElementMulti('portal.email.address'))
						.take(1)
						.map(els => els
							.map(el => FormUtils.generateEmailLink({mailto: el.value, text: el.value}))
							.map(el => el.outerHTML))
						.map(emailAddressLinks => StringUtils.concatWithConjunction(emailAddressLinks, 'or'))
						.subscribe(emailAddressHtml => msg = `An unexpected error has occurred. Please close your browser, reopen, and try your action again. If the issue persists, please reach out to ${emailAddressHtml} to inform us of the error you are encountering.`);
				}
				this.modalService.showError(msg, title);
		}
	}
}
