import { BaseEntity } from 'app/core/api/base-entity';
import { BasePaginatedEntity } from 'app/core/api/base-paginated-entity';


export class PortalResponse {

	public success: boolean;
	public errorMessage?: string;
	public message?: string;
	public data?: BaseEntity | BaseEntity[] | { rows: BaseEntity[] } | BasePaginatedEntity<BaseEntity>;

	// Authentication fields
	public authenticationRequired?: boolean;
	public loginFailed?: boolean;
	public termsOfUseAcceptanceRequired?: boolean;

	public errors?: any[];
	public validationError?: string;
}
