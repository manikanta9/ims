import { Query } from 'app/core/api/query';
import { OrderBy } from 'app/portal/api/portal-order-by';


interface SearchRestriction {
	field: string;
	comparison: string;
	value: Primitive;
}


export class PortalSearchQuery implements Query {
	/**
	 * The root path for requested properties. This value is automatically populated if {@link #requestedProperties} is populated.
	 */
	public requestedPropertiesRoot?: string;
	public requestedProperties?: string[] | string;
	public orderBy?: OrderBy;
	public restrictionList?: SearchRestriction[];
}
