import { BaseEntity } from 'app/core/api/base-entity';


export class PortalEmail extends BaseEntity {
	public toAddress?: string;
	public toAddressList?: string[];
	public ccAddress?: string;
	public ccAddressList?: string[];
	public subject?: string;
}
