import { Injectable } from '@angular/core';
import { BasePaginatedEntity } from 'app/core/api/base-paginated-entity';
import { QueryService } from 'app/core/api/query.service';
import { ObjectUtils } from 'app/core/util/object.utils';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalPaginatedQuery } from 'app/portal/api/portal-paginated-query';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalFile } from 'app/portal/file/portal-file';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalFileCategoryQuery } from 'app/portal/file/portal-file-category.query';
import { PortalFileHierarchy, PortalFileRow } from 'app/portal/file/portal-file-hierarchy';
import { PortalFileQuery } from 'app/portal/file/portal-file.query';
import { ApiUtils } from 'app/portal/util/api.utils';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalFileService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('FileExtendedListFind.json');


	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @deprecated
	 */
	public async get(query?: PortalFileQuery): Promise<PortalFile[]> {
		return this.apiService.getData(PortalFileService.URI, query, PortalFile);
	}


	public get2(query?: PortalFileQuery): Observable<PortalFile[]> {
		return this.apiService.getData2(PortalFileService.URI, query);
	}


	public getPaginated(query?: PortalPaginatedQuery<PortalFileQuery>): Observable<BasePaginatedEntity<PortalFile> | PortalFile[]> {
		return this.apiService.getPaginatedData2(PortalFileService.URI, query);
	}


	public getFileSourceEntityList(query?: PortalFileQuery): Observable<PortalEntity[]> {
		return this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('FileSourceEntityListFind.json'), query);
	}


	public getFileCategoryList(query: PortalFileCategoryQuery): Observable<PortalFileCategory[]> {
		return this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('FileCategoryListFind.json'), query);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getHierarchy(fileOrPagedList: BasePaginatedEntity<PortalFile> | PortalFile[]): PortalFileHierarchy[] {
		const fileList = ApiUtils.isPaginatedEntity(fileOrPagedList) ? fileOrPagedList.rows : fileOrPagedList;
		const rowList: PortalFileRow[] = fileList.map(file => ({file, selected: false}));
		return ObjectUtils.groupHierarchyListBy(rowList,
			{
				keyField: 'name', valueField: 'categoryList',
				groupingFn: (file: PortalFileRow) => {
					const label = _.get(file.file, 'assignedPortalEntity.label', '');
					const serviceName = _.get(file.file, 'serviceName', '');
					return serviceName ? `${label} - ${serviceName}` : label;
				},
			},
			{
				keyField: 'name', valueField: 'subCategoryList',
				groupingFn: (file: PortalFileRow) => file.file.fileCategoryNameExpandedWithoutRoot && file.file.fileCategoryNameExpandedWithoutRoot.split(' / ')[0] || '',
			},
			{
				keyField: 'name', valueField: 'fileList',
				groupingFn: (file: PortalFileRow) => file.file.fileCategoryNameExpandedWithoutRoot && file.file.fileCategoryNameExpandedWithoutRoot.split(' / ')[1] || '',
				modifierFn: result => {
					// Set view type name
					const file = result.fileList[0] && result.fileList[0].file;
					const entityViewType = file && ((file.assignedPortalEntity && file.assignedPortalEntity.entityViewType) || file.entityViewType);
					result.viewTypeName = entityViewType && entityViewType.name;
					return result;
				},
			},
		);
	}
}
