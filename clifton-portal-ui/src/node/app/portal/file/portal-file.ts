import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityViewType } from 'app/portal/entity/portal-entity-view-type';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalEntity } from '../entity/portal-entity';


// TODO: Change all DTOs to interfaces
// TODO: Remove "JsonProperty" serialization/deserialization
export class PortalFile extends BaseEntity {
	public approved?: boolean;
	public approvedByUserId?: number;
	public coalescePortalFileFrequency?: string;
	public displayName?: string;
	public displayText?: string;
	public documentFileType?: string;
	public entitySourceLabel?: string;
	public entitySourceSection?: any;
	public file?: any;
	public fileCategoryNameExpandedWithoutRoot?: string;
	public fileFrequency?: string;
	public fileNameAndPath?: string;
	public fileSize?: number;
	public globalFile?: boolean;
	public label?: string;
	public portalFileId?: number;
	public reportDateFormatted?: string;
	public serviceName?: string;
	public sourceFkFieldId?: number;
	public uuid?: string;
	@JsonProperty() public approvalDate?: Date;
	@JsonProperty() public assignedPortalEntity?: PortalEntity;
	@JsonProperty() public endDate?: Date;
	@JsonProperty() public entityViewType?: PortalEntityViewType;
	@JsonProperty() public fileCategory?: PortalFileCategory;
	@JsonProperty() public postPortalEntity?: PortalEntity;
	@JsonProperty() public publishDate?: Date;
	@JsonProperty() public reportDate?: Date;
	// TODO: Remove "id" field
	@JsonProperty({propertyName: 'portalFileId'}) public id?: number;
}

