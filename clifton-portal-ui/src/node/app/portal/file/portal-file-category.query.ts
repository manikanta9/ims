import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalFileCategoryQuery extends PortalSearchQuery {
	public approvalRequired?: boolean;
	public categoryEmailAddress?: string;
	public categoryNameEquals?: string;
	public categoryOrder?: number;
	public displayNameTemplate?: string;
	public displayTextTemplate?: string;
	public fileCategoryType?: string;
	public fileTypeId?: number;
	public folderName?: string;
	public ids?: number[];
	public limitToCategoriesForAssignedPortalEntityId?: number;
	public nameExpanded?: string;
	public notificationInstant?: boolean;
	public notificationUsed?: boolean;
	public orderExpanded?: number;
	public parentId?: number;
	public parentIdExpanded?: number;
	public postEntityTypeId?: number;
	public rootOnly?: boolean;
	public searchPattern?: string;
	public securityResourceId?: number;
	public securityResourceIds?: number[];
	public sourceEntityTypeId?: number;
	public viewAsEntityId?: number | '';
	public viewAsUserId?: number | '';
}
