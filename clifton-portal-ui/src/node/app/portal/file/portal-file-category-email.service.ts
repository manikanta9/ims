import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalEmail } from 'app/portal/file/portal-email';
import { PortalFileCategoryEmailQuery } from 'app/portal/file/portal-file-category-email.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalFileCategoryEmailService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('EmailForCategoryAndEntity.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query: PortalFileCategoryEmailQuery): Promise<PortalEmail[]> {
		return this.apiService.getData(PortalFileCategoryEmailService.URI, query, PortalEmail);
	}

}
