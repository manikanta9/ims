import { Injectable } from '@angular/core';
import { FormService } from 'app/core/form/form.service';
import { ModalDialogService } from 'app/core/modal/modal-dialog.service';
import { StringUtils } from 'app/core/util/string.utils';
import { PortalFileDownloadQuery } from 'app/portal/file/download/portal-file-download.query';
import { PortalFileListDownloadQuery } from 'app/portal/file/download/portal-file-list-download.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalFileDownloadService {

	public constructor(private formService: FormService, private modalDialogService: ModalDialogService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public downloadFile(query: PortalFileDownloadQuery): void {
		this.formService.submit(ApiUtils.getAsApiUriWithPrefix('FileDownload.json'), query);
	}


	public downloadFileList(query: PortalFileListDownloadQuery): void {
		const fileCount = !StringUtils.isEmpty(query.portalFileIds) ? query.portalFileIds.split(',').length : 0;
		if (fileCount < 1) {
			this.modalDialogService.showWarning('Please select at least one file to download.', 'Selection');
		}
		else if (fileCount > 75) {
			this.modalDialogService.showWarning(`Only 75 files can be download at once, there are currently ${fileCount} files selected.  Please select fewer files and try again.`, 'Validation Error');
		}
		else {
			this.formService.submit(ApiUtils.getAsApiUriWithPrefix('FileListDownload.json'), query);
		}
	}
}
