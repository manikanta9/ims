import { Query } from 'app/core/api/query';


export class PortalFileDownloadQuery implements Query {
	public portalFileId: number;
	public assignedPortalEntityId?: number; // From the file itself what the assigned entity id was - used for tracking
}
