import { Query } from 'app/core/api/query';


export class PortalFileListDownloadQuery implements Query {
	public portalFileIds: string;
	public assignedPortalEntityId?: number;  // from the top filters since it's a list there could be multiple so we track what entity the user was looking at
	public zipFiles: boolean;
	// TODO: 1
}
