import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalFileCategory } from 'app/portal/file/portal-file-category';
import { PortalFileCategoryQuery } from 'app/portal/file/portal-file-category.query';
import { ApiUtils } from 'app/portal/util/api.utils';


@Injectable()
export class PortalFileCategoryService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('FileCategoryListFind.json');

	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public async get(query: PortalFileCategoryQuery): Promise<PortalFileCategory[]> {
		return this.apiService.getData(PortalFileCategoryService.URI, query, PortalFileCategory);
	}
}
