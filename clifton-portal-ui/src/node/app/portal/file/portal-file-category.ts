import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityType } from 'app/portal/entity/portal-entity-type';
import { PortalFileFrequencies } from 'app/portal/file/portal-file-frequencies';


export class PortalFileCategory extends BaseEntity {
	public approvalRequired?: boolean;
	public categoryEmailAddress?: string;
	public categoryOrder?: number;
	public defaultDaysToDisplay?: number;
	public description?: string;
	public displayNameTemplate?: string;
	public displayTextTemplate?: string;
	public fileCategoryType?: string;
	public folderName?: string;
	public folderNameExpanded?: string;
	public labelExpanded?: string;
	public leaf?: boolean;
	public level?: number;
	public maxDepth?: number;
	public name?: string;
	public nameExpanded?: string;
	public notificationInstant?: boolean;
	public notificationSubscriptionRequired?: boolean;
	public notificationUsed?: boolean;
	public portalFileFrequency?: PortalFileFrequencies;
	public securityResource?: any;
	@JsonProperty() public parent?: PortalFileCategory;
	@JsonProperty() public postEntityType?: PortalEntityType;
	@JsonProperty() public rootParent?: PortalFileCategory;
	@JsonProperty() public sourceEntityType?: PortalEntityType;
}
