export type PortalFileFrequencies =
	| 'FINAL'
	| 'DAILY'
	| 'WEEKLY'
	| 'MONTHLY'
	| 'QUARTER_TO_DATE'
	| 'QUARTERLY'
	| 'ANNUALLY';
