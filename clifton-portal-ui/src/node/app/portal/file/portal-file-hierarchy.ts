import { PortalFile } from 'app/portal/file/portal-file';


interface PortalFileCategory {
	name: string;
	subCategoryList: PortalFileSubCategory[];
}


export class PortalFileSubCategory {
	public name: string;
	public fileList: PortalFileRow[];
	public viewTypeName: string;
	public selected: boolean;
}


// tslint:disable-next-line:max-classes-per-file : Temporary resolution for multiple classes in this file
export class PortalFileRow {
	public file: PortalFile;
	public selected: boolean;
}


// tslint:disable-next-line:max-classes-per-file : Temporary resolution for multiple classes in this file
export class PortalFileHierarchy {
	public name: string;
	public categoryList: PortalFileCategory[];
}
