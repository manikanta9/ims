import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalFileCategoryEmailQuery extends PortalSearchQuery {
	public categoryId?: number;
	public portalEntityId?: number;
}
