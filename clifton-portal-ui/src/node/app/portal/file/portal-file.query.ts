import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalFileQuery extends PortalSearchQuery {
	// TODO: Find a way to serialize dates appropriately
	// public maxReportDate?: Date;
	// public minReportDate?: Date;
	public activeAssignedPortalEntity?: boolean;
	public approvalDate?: Date;
	public approved?: boolean;
	public approvedByUserId?: number;
	public assignedPortalEntityId?: number;
	public assignedPortalEntityLabel?: string;
	public assignedPortalEntityName?: string;
	public categoryId?: number;
	public categoryName?: string;
	public categorySubsetId?: number;
	public categorySubsetName?: string;
	public coalesceFileCreateUserId?: number;
	public defaultDisplay?: boolean;
	public displayName?: string;
	public displayNameEquals?: string;
	public displayText?: string;
	public entitySourceSectionId?: number;
	public entitySourceSectionIdPopulated?: boolean;
	public entitySourceSystemNameEquals?: string;
	public entitySourceTableNameEquals?: string;
	public excludeCategoryId?: number;
	public excludeCategoryName?: string;
	public excludeCategoryNames?: string[];
	public excludeCategorySubsetId?: number;
	public excludeCategorySubsetName?: string;
	public excludeCategorySubsetNames?: string[];
	public fileCategoryIdExpanded?: number;
	public fileCategoryOrder?: number;
	public fileCategoryOrderExpanded?: number;
	public fileGroupId?: number;
	public fileGroupIds?: number[];
	public fileGroupName?: string;
	public fileNameAndPathPopulated?: boolean;
	public fileType?: string;
	public maxReportDate?: string;
	public minReportDate?: string;
	public notificationDefinitionId?: number;
	public notificationUsed?: boolean;
	public postPortalEntityId?: number;
	public postPortalEntityIdPopulated?: boolean;
	public postPortalEntityLabel?: string;
	public publishDate?: Date;
	public reportDate?: Date;
	public securityResourceId?: number;
	public serviceName?: string;
	public sourceFieldTypeName?: string;
	public sourceFieldValue?: string;
	public sourceFieldValueUpdateDateMax?: Date;
	public sourceFieldValueUpdateDateMin?: Date;
	public sourceFkFieldId?: number;
	public sourceFkFieldIdPopulated?: boolean;
	public sourceFkFieldIds?: number[];

	public viewAsEntityId?: number | '';
	public viewAsUserId?: number | '';
}
