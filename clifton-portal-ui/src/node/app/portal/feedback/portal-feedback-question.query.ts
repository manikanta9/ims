import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalFeedbackQuestionQuery extends PortalSearchQuery {
	public disabled?: boolean;
	public orderExpanded?: number;
	public rootOnly?: boolean;
	public searchPattern?: string;

	// Additional parameters only available to specific services
	public populateOptions?: boolean;
}
