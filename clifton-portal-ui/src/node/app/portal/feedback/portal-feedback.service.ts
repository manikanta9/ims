import { Injectable } from '@angular/core';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalFeedbackQuestion } from 'app/portal/feedback/portal-feedback-question';
import { PortalFeedbackQuestionQuery } from 'app/portal/feedback/portal-feedback-question.query';
import { PortalFeedbackResult } from 'app/portal/feedback/portal-feedback-result';
import { PortalFeedbackResultEntry } from 'app/portal/feedback/portal-feedback-result-entry';
import { PortalFeedbackResultQuery } from 'app/portal/feedback/portal-feedback-result.query';
import { ApiUtils } from 'app/portal/util/api.utils';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalFeedbackService {

	constructor(private portalApiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getFeedbackQuestionList(query: PortalFeedbackQuestionQuery): Observable<PortalFeedbackQuestion[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('FeedbackQuestionListFind.json'), query);
	}


	public getFeedbackResultList(query: PortalFeedbackResultQuery): Observable<PortalFeedbackResult[]> {
		return this.portalApiService.getData2(ApiUtils.getAsApiUriWithPrefix('FeedbackResultListFind.json'), query);
	}


	public saveFeedbackResultEntry(entry: PortalFeedbackResultEntry): Observable<PortalFeedbackResultEntry> {
		return this.portalApiService.save2(ApiUtils.getAsApiUriWithPrefix('FeedbackResultEntrySave.json'), entry);
	}
}
