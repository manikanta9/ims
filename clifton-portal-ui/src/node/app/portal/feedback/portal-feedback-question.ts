import { BaseEntity } from 'app/core/api/base-entity';


export class PortalFeedbackQuestion extends BaseEntity {
	public disabled?: boolean;
	public displayOrder?: number;
	public questionText?: string;
	public selectableOption?: boolean;
	public leaf?: boolean;
	public level?: number;
	public maxDepth?: number;
	public nameExpanded?: string;
	public optionList?: Array<{
		displayOrder?: number;
		name?: string;
		label?: string;
		description?: string;
	}>;
	public parent?: PortalFeedbackQuestion;
	public rootParent?: PortalFeedbackQuestion;
}
