import { BaseEntity } from 'app/core/api/base-entity';
import { PortalFeedbackQuestion } from 'app/portal/feedback/portal-feedback-question';
import { PortalSecurityUser } from 'app/portal/security/portal-security-user';


export class PortalFeedbackResult extends BaseEntity {
	public feedbackQuestion?: PortalFeedbackQuestion;
	// TODO: Should be date
	public resultDate?: string;
	public resultText?: string;
	public securityUser?: PortalSecurityUser;
}
