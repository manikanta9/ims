import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


export class PortalFeedbackResultQuery extends PortalSearchQuery {
	public feedbackQuestionId?: number;
	public feedbackQuestionName?: string;
	public feedbackQuestionText?: string;
	public orderExpanded?: number;
	public resultText?: string;
	public userCompanyName?: string;
	public userPhoneNumber?: string;
	public userSearchPattern?: string;
	public userTitle?: string;
	public username?: string;
}
