import { BaseEntity } from 'app/core/api/base-entity';
import { PortalFeedbackResult } from 'app/portal/feedback/portal-feedback-result';


export class PortalFeedbackResultEntry extends BaseEntity {
	/**
	 * This is a virtual entity and the ID field will never be populated.
	 */
	public id?: never;
	public resultList?: PortalFeedbackResult[];
}
