import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RequestLoader } from 'app/core/api/request-loader';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalSearchQuery } from 'app/portal/api/portal-search-query';
import { PortalPageElement } from 'app/portal/page/portal-page-element';
import * as portalStore from 'app/portal/state/portal.store';
import { ApiUtils } from 'app/portal/util/api.utils';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalPageElementService {

	private static readonly DEFAULT_ELEMENT_LIST_QUERY: PortalSearchQuery = {requestedPropertiesRoot: 'data', requestedProperties: ['value', 'order', 'type.key', 'viewType.name']};

	private pageElementLoader: RequestLoader<PortalPageElement[], PortalSearchQuery> = new RequestLoader(query => this.loadPageElementList(query));


	constructor(private apiService: PortalApiService,
	            private store: Store<portalStore.PortalStore>) { }


	////////////////////////////////////////////////////////////////////////////
	////////            API Methods                                     ////////
	////////////////////////////////////////////////////////////////////////////


	public getPageElementList(query: PortalSearchQuery = PortalPageElementService.DEFAULT_ELEMENT_LIST_QUERY): Observable<PortalPageElement[]> {
		return this.pageElementLoader.load(query);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private loadPageElementList(query: PortalSearchQuery): Observable<PortalPageElement[]> {
		return Observable
			.combineLatest(
				this.store.select(portalStore.isAuthenticated),
				this.store.select(portalStore.getSimulatedUserId),
				this.store.select(portalStore.getSimulatedEntityId),
			)
			.first()
			.switchMap(([authenticated, viewAsUserId, viewAsEntityId]) => {
				let result$: Observable<PortalPageElement[]>;
				if (!authenticated) {
					result$ = this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('PageElementListDefault.json'), PortalPageElementService.DEFAULT_ELEMENT_LIST_QUERY);
				}
				else if (viewAsUserId || viewAsEntityId) {
					result$ = this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('PageElementListForUserAndEntity.json'), {
						...PortalPageElementService.DEFAULT_ELEMENT_LIST_QUERY, viewAsUserId, viewAsEntityId,
					});
				}
				else {
					result$ = this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('PageElementListFind.json'), PortalPageElementService.DEFAULT_ELEMENT_LIST_QUERY);
				}
				return result$;
			});
	}
}
