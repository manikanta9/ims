import { BaseEntity } from 'app/core/api/base-entity';


// Recognized types
export type PageElementSingleTypes =
	| 'portal.name'
	| 'portal.footer.text'
	| 'portal.terms-of-use'
	| 'login.title'
	| 'file.email.address'
	| 'file.email.message'
	;

export type PageElementMultipleTypes =
	| 'portal.email.address'
	| 'profile.change-request.email.address'
	| 'admin.user.add-edit.role-note'
	;

export type PageElementTypes = PageElementSingleTypes | PageElementMultipleTypes;


export class PortalPageElementType extends BaseEntity {
	public name?: PageElementTypes;
	public description?: string;
	public key?: string;
	public allowMultipleElementsOfType?: boolean;
}
