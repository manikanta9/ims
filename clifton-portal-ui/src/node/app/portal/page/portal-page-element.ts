import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityViewType } from 'app/portal/entity/portal-entity-view-type';
import { PortalPageElementType } from 'app/portal/page/portal-page-element-type';


export class PortalPageElement extends BaseEntity {
	@JsonProperty() public type: PortalPageElementType;
	@JsonProperty() public viewType: PortalEntityViewType;
	public value: string;
	public priority: number;
	public order: number;
}
