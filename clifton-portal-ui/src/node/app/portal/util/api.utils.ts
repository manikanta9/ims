import { BaseEntity } from 'app/core/api/base-entity';
import { BasePaginatedEntity } from 'app/core/api/base-paginated-entity';
import { LogUtils } from 'app/core/log/log.utils';
import { PortalPaginatedQuery } from 'app/portal/api/portal-paginated-query';
import { PortalResponse } from 'app/portal/api/portal-response';
import { PortalSearchQuery } from 'app/portal/api/portal-search-query';
import { environment } from 'environments/environment';


export class ApiUtils {

	//noinspection JSUnusedLocalSymbols: Private constructor
	private constructor() {}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the API request URI for the given endpoint URI. This wraps and/or modifies the given endpoint URI to conform to the target API, which may be
	 * configured by the active environment.
	 *
	 * @param endpointUri the endpoint URI to wrap
	 * @return {string} the request URI for the given endpoint URI
	 */
	public static getAsApiUri(endpointUri: string): string {
		return `${environment.baseApiUri}${environment.baseApiUri ? '/' : ''}${endpointUri}`;
	}


	/**
	 * Gets the API request URI for the given endpoint URI. This wraps and/or modifies the given endpoint URI to conform to the target API, which may be
	 * configured by the active environment. Any standard prefix for endpoint URIs will be added to the resulting string, as well.
	 *
	 * @param endpointUri the endpoint URI to wrap
	 * @return {string} the request URI for the given endpoint URI
	 */
	public static getAsApiUriWithPrefix(endpointUri: string): string {
		return `${environment.baseApiUri}${environment.baseApiUri ? '/' : ''}${environment.baseApiPrefix}${endpointUri}`;
	}


	/**
	 * Gets the resulting data from the given {@link PortalResponse} object.
	 *
	 * @param portalResponse the portal response from which to extract the resulting data
	 * @return the resulting data TODO: Include "null" return type
	 */
	public static getPortalResponseData<T>(portalResponse: PortalResponse): T {
		if (!portalResponse.hasOwnProperty('data')) {
			// No result
			// TODO: Should be throwing an error here?
			// throw new Error('No response data was found.');
			LogUtils.warn('Found no data for response.', portalResponse);
			return null as any;
		}

		const data = portalResponse.data;
		let result: any;
		if (data == null) {
			// No result
			return data as any;
		}
		else if ((data as any).rows) {
			// Extract data from com.clifton.core.dataaccess.PagingArrayList-wrapped responses
			const rowData = data as { rows: BaseEntity[] };
			result = rowData.rows;
		}
		else {
			result = data;
		}
		return result as T;
	}


	/**
	 * Gets the resulting data from the given {@link PortalResponse} object as a {@link BasePaginatedEntity}. This will throw an exception if the response does
	 * not include paginated data.
	 *
	 * See {@link #isPaginatedQuery} for information on what types of queries result in paginated responses.
	 *
	 * @param portalResponse the portal response from which to extract the resulting data
	 * @return the resulting data, wrapped in a {@link BasePaginatedEntity} interface TODO: Include "null" return type
	 */
	public static getPortalResponsePaginatedData<T>(portalResponse: PortalResponse): BasePaginatedEntity<T> {
		if (!portalResponse.hasOwnProperty('data')) {
			// No result
			// TODO: Should be throwing an error here?
			// throw new Error('No response data was found.');
			LogUtils.warn('Found no data for response.', portalResponse);
			return null as any;
		}

		const data: Nullable<BasePaginatedEntity<T> | T[]> = portalResponse.data as Nullable<BasePaginatedEntity<T> | T[]>;
		let result: BasePaginatedEntity<T>;
		if (data == null) {
			return data as any;
		}
		else if (Array.isArray(data)) {
			if (data.length > 0) {
				// The only valid time an array should be returned is when there are no results
				LogUtils.warn('Response cannot be parsed as paginated list. Using default pagination wrapper.', data);
			}
			// Use default paginated entity
			result = {start: 0, total: data.length, rows: data};
		}
		else if (data.rows == null || data.start == null || data.total == null) {
			LogUtils.warn('Response cannot be parsed as paginated list. Using default pagination wrapper.', data);
			const rows = (data.rows || (Array.isArray(data) && data) || []) as T[];
			result = {start: data.start || 0, total: data.total || 0, rows};
		}
		else {
			result = data;
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines whether the given query is a valid {@link PortalPaginatedQuery}. That is, this function determines whether or not the given query should be
	 * guaranteed under the standard backend rules to result in a paginated response.
	 *
	 * The backend rules for pagination are implemented in <tt>com.clifton.core.dataaccess.dao.hibernate.HibernateReadOnlyDAO.doFindBySearchCriteria</tt>. The
	 * rules indicate that a <tt>com.clifton.core.dataaccess.PagingArrayList</tt> object is returned when any of the following conditions are true:
	 * <ul>
	 * <li>The <tt>start</tt> query parameter is non-zero
	 * <li>The <tt>limit</tt> query parameter is 10 or greater and less than <tt>Integer.MAX_VALUE</tt>
	 * </ul>
	 *
	 * Backend service methods may also wrap their results in <tt>com.clifton.core.dataaccess.PagingArrayList</tt> objects manually and vice versa.
	 *
	 * @param {PortalSearchQuery | PortalPaginatedQuery<T>} query
	 * @return <tt>true</tt> if the given query is expected result in a paginated response, or <tt>false</tt> otherwise
	 */
	public static isPaginatedQuery<T>(query: PortalSearchQuery | PortalPaginatedQuery<T>): query is PortalPaginatedQuery<T> {
		const paginatedQuery = query as PortalPaginatedQuery<T>;
		return !!(paginatedQuery.start || paginatedQuery.limit && paginatedQuery.limit >= 10);
	}


	/**
	 * TODO
	 *
	 * @param entity
	 * @return
	 */
	public static isPaginatedEntity<T>(entity: T[] | BasePaginatedEntity<T>): entity is BasePaginatedEntity<T> {
		const paginatedEntity: BasePaginatedEntity<T> = entity as BasePaginatedEntity<T>;
		return (paginatedEntity && typeof paginatedEntity.start === 'number' && typeof paginatedEntity.total === 'number' && Array.isArray(paginatedEntity.rows));
	}
}
