import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityFieldType } from 'app/portal/entity/portal-entity-field-type';


export class PortalEntityType extends BaseEntity {
	public name?: string;
	public label?: string;
	public description?: string;
	public fileSource?: boolean;
	public postableEntity?: boolean;
	public securableEntity?: boolean;
	@JsonProperty() public fieldTypeList?: PortalEntityFieldType[];
}
