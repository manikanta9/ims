import { BaseEntity } from 'app/core/api/base-entity';


export class PortalEntityViewType extends BaseEntity {
	public name?: string;
	public description?: string;
	public defaultView?: boolean;
	public defaultContactEmailAddress?: string;
}
