import { Injectable } from '@angular/core';
import { QueryService } from 'app/core/api/query.service';
import { PortalApiService } from 'app/portal/api/portal-api.service';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityFieldValue } from 'app/portal/entity/portal-entity-field-value';
import { PortalEntityQuery } from 'app/portal/entity/portal-entity-query';
import { ApiUtils } from 'app/portal/util/api.utils';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PortalEntityService implements QueryService {

	private static readonly URI = ApiUtils.getAsApiUriWithPrefix('EntityListFind.json');


	constructor(private apiService: PortalApiService) { }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// TODO: Delete me
	/**
	 * @deprecated
	 */
	public async get(query?: PortalEntityQuery): Promise<PortalEntity[]> {
		return this.apiService.getData(PortalEntityService.URI, query, PortalEntity);
	}


	public getEntity(id: number): Observable<PortalEntity> {
		return this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('Entity.json'), {id});
	}


	public getEntityList(query?: PortalEntityQuery): Observable<PortalEntity[]> {
		return this.apiService.getData2(ApiUtils.getAsApiUriWithPrefix('EntityListFind.json'), query);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public getFieldValue(entity: PortalEntity, fieldName: string): string | null | undefined {
		const fieldValue: PortalEntityFieldValue | undefined = _.find(entity.fieldValueList || [],
			(value: PortalEntityFieldValue) => value.portalEntityFieldType && value.portalEntityFieldType.name === fieldName);
		return (fieldValue && fieldValue.value != null) ? fieldValue.value : null;
	}


	/**
	 * Determines if the given entities are directly related.
	 *
	 * @param ancestor the ancestor entity
	 * @param potentialDescendant the potential descendant of the given ancestor entity
	 * @return {boolean} <tt>true</tt> if the potential descendant is a descendant of the ancestor entity, or <tt>false</tt> otherwise
	 */
	public isDescendant(ancestor: PortalEntity, potentialDescendant: PortalEntity): boolean {
		let result = false;

		// Traverse up tree to find a matching entity ID
		let currentEntity: PortalEntity = potentialDescendant;
		do {
			if (currentEntity.id === ancestor.id) {
				result = true;
				break;
			}
		} while (currentEntity.parentPortalEntity != null && (currentEntity = currentEntity.parentPortalEntity));

		return result;
	}
}
