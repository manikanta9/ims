import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityType } from 'app/portal/entity/portal-entity-type';


export class PortalEntityFieldType extends BaseEntity {
	public name?: string;
	public label?: string;
	public description?: string;
	@JsonProperty() public portalEntityType?: PortalEntityType;
}
