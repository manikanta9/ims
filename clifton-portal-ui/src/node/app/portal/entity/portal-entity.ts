import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntityFieldValue } from 'app/portal/entity/portal-entity-field-value';
import { PortalEntitySourceSection } from 'app/portal/entity/portal-entity-source-section';
import { PortalEntityViewType } from 'app/portal/entity/portal-entity-view-type';


export class PortalEntity extends BaseEntity {
	public active?: boolean;
	public description?: string;
	public entityLabelWithSourceSection?: string;
	public folderName?: string;
	public folderNameExpanded?: string;
	public label?: string;
	public name?: string;
	public sourceFkFieldId?: number;
	@JsonProperty() public endDate?: Date;
	@JsonProperty() public entitySourceSection?: PortalEntitySourceSection;
	@JsonProperty() public entityViewType?: PortalEntityViewType;
	@JsonProperty() public fieldValueList?: PortalEntityFieldValue[];
	@JsonProperty() public parentPortalEntity?: PortalEntity;
	@JsonProperty() public startDate?: Date;
	@JsonProperty() public updateDate?: Date;
}
