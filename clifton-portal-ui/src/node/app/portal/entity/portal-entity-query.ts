import { PortalSearchQuery } from 'app/portal/api/portal-search-query';


// TODO: Change to interface
export class PortalEntityQuery extends PortalSearchQuery {
	public entitySourceSectionId?: number;
	public entityTypeId?: number;
	public entityTypeNameEquals?: string;
	public entityTypeOrChildEntityTypeId?: number;
	public excludeEntityTypeName?: string;
	public fileSource?: boolean;
	public hasSecurityResourceAccess?: boolean;
	public nameEquals?: string;
	public parentId?: number;
	public parentIdExpanded?: number;
	public parentIsNull?: boolean;
	public searchPattern?: string;
	public viewAsEntityId?: number | '';
	public viewAsUserId?: number | '';
	public viewSecurityAdminRole?: boolean;
}
