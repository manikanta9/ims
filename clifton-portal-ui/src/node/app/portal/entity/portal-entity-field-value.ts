import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntity } from 'app/portal/entity/portal-entity';
import { PortalEntityFieldType } from 'app/portal/entity/portal-entity-field-type';


export class PortalEntityFieldValue extends BaseEntity {
	public label?: string;
	public value?: string;
	@JsonProperty() public portalEntity?: PortalEntity;
	@JsonProperty() public portalEntityFieldType?: PortalEntityFieldType;
}
