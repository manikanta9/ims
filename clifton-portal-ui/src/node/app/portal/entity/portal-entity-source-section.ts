import { BaseEntity } from 'app/core/api/base-entity';
import { JsonProperty } from 'app/core/api/json-property.decorator';
import { PortalEntitySourceSystem } from 'app/portal/entity/portal-entity-source-system';
import { PortalEntityType } from 'app/portal/entity/portal-entity-type';


export class PortalEntitySourceSection extends BaseEntity {
	public label?: string;
	public name?: string;
	public sourceSystemTableName?: string;
	@JsonProperty() public childEntityType?: PortalEntityType;
	@JsonProperty() public entityType?: PortalEntityType;
	@JsonProperty() public sourceSystem?: PortalEntitySourceSystem;
}
