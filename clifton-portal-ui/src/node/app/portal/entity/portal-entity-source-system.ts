import { BaseEntity } from 'app/core/api/base-entity';


export class PortalEntitySourceSystem extends BaseEntity {
	public name?: string;
	public label?: string;
}
