import { LogLevel } from 'app/core/log/log-level.enum';
import { Environment } from 'environments/environment-config';
import { baseEnvironment } from 'environments/environment.base';


// Environment configuration for the production admin server
export const environment: Environment = {
	...baseEnvironment,
	administrationMode: true,
	baseApiUri: '..',
	logLevel: LogLevel.ALL,
	loginRedirectTarget: '/clientPortalSecurityUserCurrent.json',
	production: true,
};
