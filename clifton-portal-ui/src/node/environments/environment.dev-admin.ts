import { Environment } from 'environments/environment-config';
import { baseEnvironment } from 'environments/environment.base';


// Environment configuration for the production admin server
export const environment: Environment = {
	...baseEnvironment,
	administrationMode: true,
	production: false,
	baseApiUri: '..',
	loginRedirectTarget: '/clientPortalSecurityUserCurrent.json',
};
