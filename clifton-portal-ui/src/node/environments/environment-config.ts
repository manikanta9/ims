import { LogLevel } from 'app/core/log/log-level.enum';


// Environment configuration stub
export interface Environment {

	/**
	 * <tt>true</tt> if the environment is in <i>administration mode</i>, or <tt>false</tt> otherwise. <i>Administration mode</i> disables functionality not
	 * corresponding to internal users, while enabling administrative functionality, such as the capability to view the application from the perspective of
	 * another user.
	 */
	administrationMode: boolean;

	/**
	 * If <tt>true</tt>, the environment will be compiled with the animations module enabled. This allows animations. Otherwise, the environment will be
	 * compiled without animations.
	 */
	animationsEnabled: boolean;

	/**
	 * The prefix for all API requests. This is attached to requests which target custom endpoints, but does not apply to built-in endpoints, such as
	 * <tt>j_security_check.json</tt>. This can be used to configure different sets of endpoints for different environments using URI-mapping conventions.
	 */
	baseApiPrefix: string;

	/**
	 * The base URI for all requests.
	 */
	baseApiUri: string;

	/**
	 * The minimum level for log messages which shall be processed.
	 */
	logLevel: LogLevel;

	/**
	 * The redirect target to which successful login requests will redirect. This requires configurable login-redirect targets to be enabled for the server.
	 * For hosts using Spring-security, this can be done by configuring the given success handler property:
	 * <tt>org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler.targetUrlParameter</tt>
	 */
	loginRedirectTarget: string | null;

	/**
	 * <tt>true</tt> if this is a production environment, or <tt>false</tt> otherwise.
	 */
	production: boolean;

	/**
	 * The API request timeout in milliseconds.
	 */
	requestTimeout: number;

	/**
	 * If true, routing will be done via the URL fragment rather than the history API.
	 *
	 * This is necessary if the web server does not support resolving wildcard matching to the root page. It also can help with compatibility with older browsers. However, it also
	 * can cause issues with the page not reloading when entering a new URL into the location bar if the only change in the URL is within the fragment. This may or may not be the
	 * desired behavior.
	 */
	routeViaHash: boolean;

	/**
	 * The expected cookie name for CSRF protection.
	 */
	xsrfCookieName: string;

	/**
	 * The header name to attach to requests for CSRF protection.
	 */
	xsrfHeaderName: string;

	/**
	 * The form parameter name to use for CSRF protection.
	 */
	xsrfParameterName: string;
}
