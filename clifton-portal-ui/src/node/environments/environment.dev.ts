import { LogLevel } from 'app/core/log/log-level.enum';
import { Environment } from 'environments/environment-config';
import { baseEnvironment } from 'environments/environment.base';


// Environment configuration for development
export const environment: Environment = {
	...baseEnvironment,
	administrationMode: false,
	logLevel: LogLevel.ALL,
	production: false,
};
