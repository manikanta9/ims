import { LogLevel } from 'app/core/log/log-level.enum';
import { Environment } from 'environments/environment-config';


/**
 * The base environment configuration.
 *
 * Note: The type for this object is intentionally inferred, rather than declared using a Partial or any other mechanism, so that its existing keys can be known by users of the
 * object.
 */
export const baseEnvironment = {
	animationsEnabled: false,
	baseApiPrefix: 'clientPortal',
	baseApiUri: '',
	logLevel: LogLevel.ERROR,
	loginRedirectTarget: null,
	requestTimeout: 30000,
	routeViaHash: false,
	xsrfCookieName: 'XSRF-TOKEN',
	xsrfHeaderName: 'X-XSRF-TOKEN',
	xsrfParameterName: '_csrf',
};


/*
 * Verify type constraints for base environment. Using a partial here rather than in the original definition allows importers of baseEnvironment to be aware of
 * the exact list of keys that must be populated to match the complete Environment definition.
 *
 * The spread operator must be used here to allow us to validate types using excess property checking (EPC), which only applies to object literal assignments.
 * See documentation here: https://www.typescriptlang.org/docs/handbook/interfaces.html#excess-property-checks
 */
// noinspection JSUnusedLocalSymbols
const environmentTypeCheck: Partial<Environment> = {
	...baseEnvironment,
};
