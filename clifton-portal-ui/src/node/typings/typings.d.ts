/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
	id: string;
}

/*
 * Custom module declarations
 *
 * If type-definitions do not exist for a given package, add them here.
 * Official type-definitions repository: https://github.com/DefinitelyTyped/DefinitelyTyped
 * Type definitions are conventionally deployed to npm using the package name "@types/<package name>".
 */
