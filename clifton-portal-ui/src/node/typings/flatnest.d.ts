declare module 'flatnest' {
	function flatten(obj: object): { [key: string]: any };


	function nest(flatObj: object): { [key: string]: any };


	function seek(obj: object, path: string): any;
}
