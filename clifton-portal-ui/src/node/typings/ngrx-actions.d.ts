import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';


/*
 * TODO: Delete me when newer version @ngrx/store is released that resolves this issue
 *
 * This overrides the @ngrx/store Actions class function to fix a TS 2.4+-breaking bug. The class does not include a generic parameter, so the type of the element which it holds is
 * not declared. This leads to conflicts with Observable operators, such as switchMap. This might be fixed in @ngrx/store v4+.
 */
declare module '@ngrx/effects' {
	interface Actions {
		ofType(...keys: string[]): Observable<Action>;
	}
}
