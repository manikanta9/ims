/**
 * A nullable wrapper for the given type. This includes both <tt>null</tt> and <tt>undefined</tt> values.
 */
declare type Nullable<T> = T | null | undefined;

/**
 * A non-nullable type. This includes all primitive types besides <tt>null</tt> and <tt>undefined</tt>.
 */
declare type NonNull = string | number | boolean | symbol | object;

declare type Primitive = string | number | boolean | symbol | null | undefined;


/**
 * Produces the intersection of the keys of the given types. This is useful when filtering the keys of an extended type down to the keys of a super type, such
 * as when using {@link Pick}.
 *
 * Note that the definition of an <i>intersection type</i>, as given by the <a href="https://github.com/Microsoft/TypeScript/blob/master/doc/spec.md#3.5">
 * TypeScript spec</a>, declares that the intersection type combines the shape of all of the given types <i>simultaneously</i>. That is, the result has keys and
 * values that satisfy each of the given types. That often means that the result has some keys that are not in one or more of the constituent types. However, in
 * the case of union types (such as <tt>keyof</tt> types), this means that the result is truly the <i>intersection</i> of the available options, as only the
 * intersection can satisfy all constituent types.
 *
 * @example
 * <pre>
 * interface ShapeSetA {
 *     circle: any;
 *     triangle: any;
 *     square: any;
 *     rectangle: any;
 * }
 *
 * interface ShapeSetB {
 *     square: any;
 *     rectangle: any;
 *     trapezoid: any;
 * }
 *
 * const shapeIntersection: KeyIntersection<ShapeSetA, ShapeSetB> = 'circle'; // Error: 'circle' not in 'square' | 'rectangle'
 * </pre>
 */
declare type KeyIntersection<T, S, U = S, V = S, Q = S> = keyof T & keyof S & keyof U & keyof V & keyof Q;
