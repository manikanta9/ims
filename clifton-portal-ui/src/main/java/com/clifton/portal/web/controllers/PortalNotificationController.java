package com.clifton.portal.web.controllers;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationSearchForm;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.acknowledgement.PortalNotificationAcknowledgementService;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionEntry;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * The <code>PortalNotificationController</code> is a wrapper class for client portal requests to PortalNotificationSubscription related methods
 *
 * @author manderson
 */
@Controller
public class PortalNotificationController {

	private PortalNotificationAcknowledgementService portalNotificationAcknowledgementService;

	private PortalNotificationService portalNotificationService;

	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;

	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////           Portal Notification Subscription Methods             ////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalNotificationSubscriptionEntry")
	public PortalNotificationSubscriptionEntry getClientPortalNotificationSubscriptionEntry(int portalEntityId) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		return getPortalNotificationSubscriptionService().getPortalNotificationSubscriptionEntry(portalSecurityUser.getId(), portalEntityId);
	}


	@PortalSecureMethod
	@RequestMapping("clientPortalNotificationSubscriptionEntryForUser")
	public PortalNotificationSubscriptionEntry getClientPortalNotificationSubscriptionEntryForUser(short id, int portalEntityId) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUser(id);
		ValidationUtils.assertNotNull(portalSecurityUser, "User with ID not found: " + id);
		return getPortalNotificationSubscriptionService().getPortalNotificationSubscriptionEntry(portalSecurityUser.getId(), portalEntityId);
	}


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalNotificationSubscriptionEntrySave", method = RequestMethod.POST)
	public PortalNotificationSubscriptionEntry saveClientPortalNotificationSubscriptionEntry(PortalNotificationSubscriptionEntry bean) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		ValidationUtils.assertTrue(bean.getSecurityUser().equals(portalSecurityUser), "You can only edit your own notifications.");
		return getPortalNotificationSubscriptionService().savePortalNotificationSubscriptionEntry(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////               Pop Up Portal Notification Methods               ////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalNotificationPopUpListFind")
	public List<PortalNotification> getClientPortalNotificationPopUpList() {
		PortalNotificationSearchForm searchForm = new PortalNotificationSearchForm();
		searchForm.setPopUpNotification(true);
		searchForm.setAcknowledged(false);
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		searchForm.setRecipientUserId(portalSecurityUser.getId());
		return getPortalNotificationService().getPortalNotificationList(searchForm);
	}


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalNotificationAcknowledgementSave", method = RequestMethod.POST)
	public void saveClientPortalNotificationAcknowledgement(int portalNotificationId, Boolean acknowledgement) {
		PortalNotification notification = getPortalNotificationService().getPortalNotification(portalNotificationId);
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		ValidationUtils.assertTrue(notification.getRecipientUser().equals(portalSecurityUser), "You can only acknowledge your own notifications.");
		getPortalNotificationAcknowledgementService().savePortalNotificationAcknowledgement(portalNotificationId, acknowledgement);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationAcknowledgementService getPortalNotificationAcknowledgementService() {
		return this.portalNotificationAcknowledgementService;
	}


	public void setPortalNotificationAcknowledgementService(PortalNotificationAcknowledgementService portalNotificationAcknowledgementService) {
		this.portalNotificationAcknowledgementService = portalNotificationAcknowledgementService;
	}


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}


	public PortalNotificationSubscriptionService getPortalNotificationSubscriptionService() {
		return this.portalNotificationSubscriptionService;
	}


	public void setPortalNotificationSubscriptionService(PortalNotificationSubscriptionService portalNotificationSubscriptionService) {
		this.portalNotificationSubscriptionService = portalNotificationSubscriptionService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
