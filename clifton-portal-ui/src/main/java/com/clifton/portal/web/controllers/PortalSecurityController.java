package com.clifton.portal.web.controllers;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceSearchForm;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserManagementService;
import com.clifton.portal.security.user.PortalSecurityUserRole;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserRoleSearchForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortalSecurityController</code> is a wrapper class for client portal requests to PortalSecurity related methods
 *
 * @author manderson
 */
@Controller
public class PortalSecurityController {

	private PortalSecurityResourceService portalSecurityResourceService;
	private PortalSecurityUserService portalSecurityUserService;
	private PortalSecurityUserManagementService portalSecurityUserManagementService;

	////////////////////////////////////////////////////////////////////////////////
	/////////////             Portal Current User Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping("clientPortalSecurityUserCurrent")
	public PortalSecurityUser getClientPortalSecurityUserCurrent() {
		return getPortalSecurityUserService().getPortalSecurityUserCurrent();
	}


	@ModelAttribute("data")
	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping("clientPortalSecurityUserCurrentSecurityAdmin")
	public boolean isClientPortalSecurityUserCurrentSecurityAdmin() {
		return getPortalSecurityUserService().isPortalSecurityUserSecurityAdmin(getPortalSecurityUserService().getPortalSecurityUserCurrent());
	}


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping(value = "clientPortalSecurityCurrentUserPasswordUpdate", method = RequestMethod.POST)
	public void updateClientPortalSecurityCurrentUserPassword(String oldPassword, String password) {
		getPortalSecurityUserManagementService().updatePortalSecurityCurrentUserPassword(oldPassword, password);
	}


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping(value = "clientPortalSecurityUserCurrentSave", method = RequestMethod.POST)
	public PortalSecurityUser saveClientPortalSecurityUserCurrent(PortalSecurityUser bean) {
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		ValidationUtils.assertTrue(currentUser.equals(bean), "Access Denied.  You may only edit your own profile.");
		// Use Management Service Method to confirm fields they are editing
		return getPortalSecurityUserManagementService().savePortalSecurityUser(bean);
	}


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping(value = "clientPortalSecurityCurrentUserTermsOfUseAccept", method = RequestMethod.POST)
	public void acceptClientPortalSecurityCurrentUserTermsOfUse() {
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		getPortalSecurityUserManagementService().acceptPortalSecurityUserTermsOfUse(currentUser);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal Security User Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUser")
	public PortalSecurityUser getClientPortalSecurityUser(short id) {
		return getPortalSecurityUserService().getPortalSecurityUser(id);
	}


	@ModelAttribute("data")
	@PortalSecureMethod
	@RequestMapping("clientPortalSecurityUserSecurityAdmin")
	public boolean isClientPortalSecurityUserSecurityAdmin(short id) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUser(id);
		ValidationUtils.assertNotNull(portalSecurityUser, "User with ID not found: " + id);
		return getPortalSecurityUserService().isPortalSecurityUserSecurityAdmin(portalSecurityUser);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserByUserName")
	public PortalSecurityUser getClientPortalSecurityUserByUserName(String username, Boolean validateEmail) {
		return getPortalSecurityUserService().getPortalSecurityUserByUserName(username, validateEmail);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserByUserNameAndEntityValidateAssignment")
	public PortalSecurityUser getPortalSecurityUserByUserNameAndEntityValidateAssignment(String username, Integer entityId, Boolean validateEmail) {
		return getPortalSecurityUserService().getPortalSecurityUserByUserNameAndEntityValidateAssignment(username, entityId, validateEmail);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalSecurityUserSave", method = RequestMethod.POST)
	public PortalSecurityUser saveClientPortalSecurityUser(PortalSecurityUser bean) {
		if (!bean.isNewBean()) {
			throw new ValidationException("You do not have the ability to edit another user's personal information after they have been set up.");
		}
		return getPortalSecurityUserManagementService().savePortalSecurityUser(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////           Portal Security User Role Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserRoleListFind")
	public List<PortalSecurityUserRole> getClientPortalSecurityUserRoleList(PortalSecurityUserRoleSearchForm searchForm) {
		searchForm.setPortalEntitySpecific(true);
		return getPortalSecurityUserService().getPortalSecurityUserRoleList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////         Portal Security User Assignment Methods         ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserAssignment")
	public PortalSecurityUserAssignment getClientPortalSecurityUserAssignment(int id) {
		return getPortalSecurityUserService().getPortalSecurityUserAssignment(id);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserAssignmentListFind")
	public List<PortalSecurityUserAssignment> getClientPortalSecurityUserAssignmentList(PortalSecurityUserAssignmentSearchForm searchForm, boolean populateResourcePermissionMap) {
		return getPortalSecurityUserService().getPortalSecurityUserAssignmentList(searchForm, populateResourcePermissionMap);
	}


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping("clientPortalSecurityCurrentUserAssignmentAdministratorList")
	public List<PortalSecurityUserAssignment> getClientPortalSecurityCurrentUserAssignmentAdministratorList(String orderBy) {
		PortalSecurityUser currentUser = getClientPortalSecurityUserCurrent();
		return getPortalSecurityUserService().getPortalSecurityUserAssignmentAdministratorListForUser(currentUser, orderBy);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserAssignmentAdministratorListForUser")
	public List<PortalSecurityUserAssignment> getClientPortalSecurityUserAssignmentAdministratorListForUser(Short userId, String orderBy) {
		PortalSecurityUser user = getClientPortalSecurityUser(userId);
		return getPortalSecurityUserService().getPortalSecurityUserAssignmentAdministratorListForUser(user, orderBy);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalSecurityUserAssignmentSave", method = RequestMethod.POST)
	public PortalSecurityUserAssignment saveClientPortalSecurityUserAssignment(PortalSecurityUserAssignment bean) {
		return getPortalSecurityUserService().savePortalSecurityUserAssignment(bean);
	}


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalSecurityUserAssignmentDisable", method = RequestMethod.POST)
	public void disableClientPortalSecurityUserAssignment(int id, String reason) {
		PortalSecurityUserAssignment userAssignment = getPortalSecurityUserService().getPortalSecurityUserAssignment(id);
		userAssignment.setDisabledDate(new Date());
		userAssignment.setDisabledReason(reason);
		getPortalSecurityUserService().savePortalSecurityUserAssignment(userAssignment);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////        Portal Security User Assignment Resource Methods        ////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For an existing assignment and new role returns the list of permissions available.  Will merge existing list of permissions with the new list so we can retain ids and
	 * existing currently selected permissions (if still available). i.e. If a user was a Client User and is being changed to a Custodian, the new list will be limited, but if they
	 * had permissions for anything in that limited list before, it will remained on or off based on what the permissions were
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserAssignmentResourceListForAssignmentAndRole")
	public List<PortalSecurityUserAssignmentResource> getClientPortalSecurityUserAssignmentResourceListForAssignmentAndRole(int userAssignmentId, short roleId) {
		return getPortalSecurityUserService().getPortalSecurityUserAssignmentResourceListForAssignmentAndRole(userAssignmentId, roleId);
	}


	/**
	 * For a NEW assignment and role returns the list of permissions available given the portal entity id that would be used on the assignment when saved
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityUserAssignmentResourceListForEntityAndRole")
	public List<PortalSecurityUserAssignmentResource> getClientPortalSecurityUserAssignmentResourceListForEntityAndRole(int portalEntityId, short roleId) {
		return getPortalSecurityUserService().getPortalSecurityUserAssignmentResourceListForEntityAndRole(portalEntityId, roleId);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////           Portal Security Resource Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalSecurityAdminSecurity = true, denyPortalEntitySpecific = false, bypassWhenNonPortalEntitySpecific = true)
	@RequestMapping("clientPortalSecurityResourceListFind")
	public List<PortalSecurityResource> getClientPortalSecurityResourceList(PortalSecurityResourceSearchForm searchForm) {
		return getPortalSecurityResourceService().getPortalSecurityResourceList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Portal User Event Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping(value = "clientPortalSecurityUserPasswordReset", method = RequestMethod.POST)
	public void resetPortalSecurityUserPassword(String passwordResetKey, String newPassword) {
		getPortalSecurityUserManagementService().resetPortalSecurityUserPassword(passwordResetKey, newPassword);
	}


	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping(value = "clientPortalSecurityUserPasswordResetRequest", method = RequestMethod.POST)
	@ModelAttribute("data")
	public String requestPortalSecurityUserPasswordReset(String userName) {
		return getPortalSecurityUserManagementService().requestPortalSecurityUserPasswordReset(userName);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityResourceService getPortalSecurityResourceService() {
		return this.portalSecurityResourceService;
	}


	public void setPortalSecurityResourceService(PortalSecurityResourceService portalSecurityResourceService) {
		this.portalSecurityResourceService = portalSecurityResourceService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalSecurityUserManagementService getPortalSecurityUserManagementService() {
		return this.portalSecurityUserManagementService;
	}


	public void setPortalSecurityUserManagementService(PortalSecurityUserManagementService portalSecurityUserManagementService) {
		this.portalSecurityUserManagementService = portalSecurityUserManagementService;
	}
}
