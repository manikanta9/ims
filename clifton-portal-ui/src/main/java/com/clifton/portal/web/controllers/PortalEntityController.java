package com.clifton.portal.web.controllers;

import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>PortalEntityController</code> is a wrapper class for client portal requests to PortalEntity related methods
 *
 * @author manderson
 */
@Controller
public class PortalEntityController {


	private PortalEntityService portalEntityService;

	private PortalEntitySetupService portalEntitySetupService;


	////////////////////////////////////////////////////////////////////////////////
	////////////                 Portal Entity Methods                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Internal UI method for retrieving data for simulated entity assignments.
	 */
	@PortalSecureMethod
	@RequestMapping("clientPortalEntity")
	public PortalEntity getClientPortalEntity(int id) {
		return getPortalEntityService().getPortalEntity(id);
	}


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalEntityListFind")
	public List<PortalEntity> getClientPortalEntityList(PortalEntitySearchForm searchForm) {
		searchForm.setSecurableEntity(true);
		searchForm.setSecurableEntityHasApprovedFilesPosted(true);
		searchForm.setActive(true);
		return getPortalEntityService().getPortalEntityList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////               Portal Entity View Type Methods                /////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalEntityViewTypeList")
	public List<PortalEntityViewType> getClientPortalEntityViewTypeList(PortalEntityViewTypeSearchForm searchForm) {
		return getPortalEntitySetupService().getPortalEntityViewTypeList(searchForm, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}
}
