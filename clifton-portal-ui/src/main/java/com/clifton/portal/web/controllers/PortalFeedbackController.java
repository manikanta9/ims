package com.clifton.portal.web.controllers;

import com.clifton.portal.feedback.PortalFeedbackQuestion;
import com.clifton.portal.feedback.PortalFeedbackResultEntry;
import com.clifton.portal.feedback.PortalFeedbackService;
import com.clifton.portal.feedback.search.PortalFeedbackQuestionSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * The <code>PortalFeedbackController</code> is a wrapper class for client portal requests to PortalFeedback related methods
 *
 * @author manderson
 */
@Controller
public class PortalFeedbackController {

	private PortalFeedbackService portalFeedbackService;


	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal Feedback Question Methods           ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalFeedbackQuestionListFind")
	public List<PortalFeedbackQuestion> getClientPortalFeedbackQuestionList() {
		PortalFeedbackQuestionSearchForm searchForm = new PortalFeedbackQuestionSearchForm();
		searchForm.setDisabled(false);
		searchForm.setOrderBy("orderExpanded:asc");

		return getPortalFeedbackService().getPortalFeedbackQuestionList(searchForm, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Portal Feedback Entry Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalFeedbackResultEntrySave", method = RequestMethod.POST)
	public void saveClientPortalFeedbackResultEntry(PortalFeedbackResultEntry bean) {
		getPortalFeedbackService().savePortalFeedbackResultEntry(bean);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFeedbackService getPortalFeedbackService() {
		return this.portalFeedbackService;
	}


	public void setPortalFeedbackService(PortalFeedbackService portalFeedbackService) {
		this.portalFeedbackService = portalFeedbackService;
	}
}
