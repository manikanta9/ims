package com.clifton.portal.web.controllers;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.email.PortalEmail;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.tracking.PortalTrackingEventMethod;
import com.clifton.portal.tracking.PortalTrackingEventTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.List;


/**
 * The <code>PortalFileController</code> is a wrapper class for client portal requests to PortalFile related methods
 *
 * @author manderson
 */
@Controller
public class PortalFileController {

	private PortalEmailService portalEmailService;

	private PortalFileSetupService portalFileSetupService;
	private PortalFileService portalFileService;

	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Category Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalFileCategoryListFind")
	public List<PortalFileCategory> getClientPortalFileCategoryList(PortalFileCategorySearchForm searchForm) {
		searchForm.setLimitToCategoriesWithAvailableApprovedFiles(true);
		return getPortalFileSetupService().getPortalFileCategoryList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                    Portal Email Methods                   ///////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalEmailForCategoryAndEntity")
	public PortalEmail getClientPortalEmailForCategoryAndEntity(short categoryId, Integer portalEntityId) {
		return getPortalEmailService().getPortalEmailForCategoryAndEntity(categoryId, portalEntityId);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Download Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_DOWNLOAD)
	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalFileDownload", method = RequestMethod.POST)
	public FileWrapper downloadClientPortalFile(int portalFileId) {
		return getPortalFileService().downloadPortalFile(portalFileId, null);
	}


	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalPortraitFile", method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloadClientPortalPortraitFile(int portalFileId) throws IOException {
		PortalFile portalFile = getPortalFileService().getPortalFile(portalFileId);

		ValidationUtils.assertEquals(portalFile.getFileCategory().getRootParent().getName(), PortalFileCategory.CATEGORY_NAME_CONTACT_US, "Not a portal portrait");

		HttpHeaders headers = new HttpHeaders();
		MediaType contentType = MediaTypeFactory.getMediaType(portalFile.getFileName())
				.orElseThrow(() -> new RuntimeException(String.format("Unable to determine a media type for the file [%s].", portalFile.getFileName())));
		headers.setContentType(contentType);
		return new ResponseEntity<>(FileUtils.readFileBytes(getPortalFileService().downloadPortalFile(portalFileId, null).getFile()), headers, HttpStatus.OK);
	}


	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_DOWNLOAD)
	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping(value = "clientPortalFileListDownload", method = RequestMethod.POST)
	public FileWrapper downloadClientPortalFileList(Integer[] portalFileIds, boolean zipFiles) {
		return getPortalFileService().downloadPortalFileList(portalFileIds, null, zipFiles);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////               Portal File Extended Methods                 //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_VIEW)
	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalFileExtendedListFind")
	public List<PortalFileExtended> getClientPortalFileExtendedList(PortalFileExtendedSearchForm searchForm) {
		searchForm.setApproved(true);
		searchForm.setActiveAssignedPortalEntity(true);
		return getPortalFileService().getPortalFileExtendedList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////              Portal File Source Entity Methods               /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Should be used rarely - currently will be used by Client UI to get Relationship Manager and Team Email information Always get approved and currently
	 * active
	 */
	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_VIEW)
	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalFileSourceEntityListFind")
	public List<PortalEntity> getClientPortalFileSourceEntityList(PortalFileExtendedSearchForm searchForm) {
		searchForm.setApproved(true);
		searchForm.setDefaultDisplay(true);
		searchForm.setActiveAssignedPortalEntity(true);
		return getPortalFileService().getPortalFileSourceEntityList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalEmailService getPortalEmailService() {
		return this.portalEmailService;
	}


	public void setPortalEmailService(PortalEmailService portalEmailService) {
		this.portalEmailService = portalEmailService;
	}
}
