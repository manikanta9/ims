package com.clifton.portal.web.controllers;

import com.clifton.core.beans.BeanUtils;
import com.clifton.portal.page.PortalPageElement;
import com.clifton.portal.page.PortalPageElementService;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The {@link PortalPageElementController} provides {@link PortalPageElement} endpoints accessible from the portal UI.
 *
 * @author MikeH
 */
@Controller
public class PortalPageElementController {

	private PortalPageElementService portalPageElementService;
	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of {@link PortalPageElement} entities which apply globally. This is the list of elements which apply when the user is not logged in and is a publicly
	 * accessible endpoint.
	 */
	@PortalSecureMethod(disableSecurity = true)
	@RequestMapping("clientPortalPageElementListDefault")
	public List<PortalPageElement> getClientPortalPageElementListDefault() {
		return getPortalPageElementService().getPortalPageElementListDefault();
	}


	/**
	 * Gets the list of {@link PortalPageElement} entities which apply to the current user.
	 */
	@PortalSecureMethod(denyPortalEntitySpecific = false)
	@RequestMapping("clientPortalPageElementListFind")
	public List<PortalPageElement> getClientPortalPageElementListCurrentUser() {
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		return getPortalPageElementService().getPortalPageElementListForUser(BeanUtils.getBeanIdentity(currentUser));
	}


	/**
	 * Gets the list of {@link PortalPageElement} entities which apply to the given user and/or entity. This is used for user simulation.
	 */
	@PortalSecureMethod
	@RequestMapping("clientPortalPageElementListForUserAndEntity")
	public List<PortalPageElement> getClientPortalPageElementListForUserAndEntity(Short viewAsUserId, Short viewAsEntityId) {
		final List<PortalPageElement> elementList;
		if (viewAsEntityId != null) {
			elementList = getPortalPageElementService().getPortalPageElementListForEntity(viewAsEntityId);
		}
		else if (viewAsUserId != null) {
			elementList = getPortalPageElementService().getPortalPageElementListForUser(viewAsUserId);
		}
		else {
			elementList = getClientPortalPageElementListCurrentUser();
		}
		return elementList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalPageElementService getPortalPageElementService() {
		return this.portalPageElementService;
	}


	public void setPortalPageElementService(PortalPageElementService portalPageElementService) {
		this.portalPageElementService = portalPageElementService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
