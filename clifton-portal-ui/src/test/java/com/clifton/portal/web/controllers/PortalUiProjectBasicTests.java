package com.clifton.portal.web.controllers;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalUiProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "portal-ui";
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Controller");
		return basicProjectAllowedSuffixNamesList;
	}
}
