package com.clifton.portal.web.controllers;

import com.clifton.core.security.authorization.SecureClass;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalControllerTests</code> is used to test the the client UI controllers follow required naming conventions and security protocols
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalControllerTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getProjectPrefix() {
		return "portal";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNonReadControllerMethodsAnnotatedWithPost() {
		Map<String, Object> controllerMap = getApplicationContext().getBeansWithAnnotation(Controller.class);

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			Object controller = stringObjectEntry.getValue();

			Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());
			if (AnnotationUtils.isAnnotationPresent(clz, SecureClass.class)) {
				continue;
			}

			for (Method method : clz.getMethods()) {
				if (AnnotationUtils.isAnnotationPresent(method, SecureMethod.class)) {
					if (AnnotationUtils.getAnnotation(method, SecureMethod.class).adminSecurity()) {
						continue;
					}
				}
				if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class)) {
					if (isRequirePostRequestMappingForMethod(method.getName())) {
						RequestMapping annotation = AnnotationUtils.getAnnotation(method, RequestMapping.class);
						RequestMethod[] requestMethods = annotation.method();
						if (ArrayUtils.isEmpty(requestMethods) || requestMethods.length != 1 || requestMethods[0] != RequestMethod.POST) {
							results.append(method.toString()).append(StringUtils.NEW_LINE);
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "Missing POST method annotation property for RequestMapping annotations for the following method(s).  If not a save method please explicitly exclude, else please add missing annotation property.  The only type that can be supported is POST for save methods:" + StringUtils.NEW_LINE + results.toString());
	}


	private boolean isRequirePostRequestMappingForMethod(String methodName) {
		if (methodName.startsWith("get") || methodName.startsWith("is") || "downloadClientPortalPortraitFile".equals(methodName)) {
			return false;
		}
		return true;
	}


	/**
	 * Validates that all service methods have valid return types.
	 */
	@Test
	public void testServiceMethodValidReturnTypes() throws Exception {
		Map<String, Object> controllerMap = getApplicationContext().getBeansWithAnnotation(Controller.class);

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			if (!stringObjectEntry.getKey().startsWith(getProjectPrefix())) {
				continue;
			}
			Object controller = stringObjectEntry.getValue();

			Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());

			// public and non-abstract methods only
			BeanInfo beanInfo = Introspector.getBeanInfo(clz);

			for (Method method : clz.getMethods()) {
				// ignore Object methods
				if (MethodUtils.isMethodInClass(Object.class, method)) {
					continue;
				}
				if (processMethod(beanInfo, method)) {
					// By default, strings returned from handlers are interpreted as view names; an appropriate annotation must be added to include the string in the response body
					if (method.getReturnType().equals(String.class)
							&& !AnnotationUtils.isAnnotationPresent(method, ModelAttribute.class)
							&& !AnnotationUtils.isAnnotationPresent(method, ResponseBody.class)) {
						results.append(method.toString()).append(": Controller methods which return strings must be annotated with @ModelAttribute or @ResponseBody").append(StringUtils.NEW_LINE);
					}
					// Require return values for all "save" methods; each save method should return the saved entity
					if (method.getName().startsWith("save") && Void.TYPE.equals(method.getReturnType())) {
						if (!getIgnoreVoidSaveMethodSet().contains(method.getName())) {
							results.append(method.toString()).append(": Controller save methods must return a value").append(StringUtils.NEW_LINE);
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "Void Save Methods found for the following method(s):" + StringUtils.NEW_LINE + results.toString());
	}


	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoreVoidSaveMethodSet = new HashSet<>();
		// Not a Real Object - contains a list that is saved
		ignoreVoidSaveMethodSet.add("saveClientPortalFeedbackResultEntry");
		// Not a real object, creates tracking information and acknowledges the notification
		ignoreVoidSaveMethodSet.add("saveClientPortalNotificationAcknowledgement");
		return ignoreVoidSaveMethodSet;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean processMethod(BeanInfo beanInfo, Method method) {
		if (Modifier.isPublic(method.getModifiers()) && !Modifier.isAbstract(method.getModifiers()) && !MethodUtils.isGetterOrSetter(beanInfo, method)) {
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
