import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// for Windows service
	implementation("winrun4j:winrun4j:0.4.5")
	runtimeOnly("org.apache.hive:hive-jdbc:3.1.2") { exclude(module = "junit") }
	runtimeOnly("org.glassfish:javax.el:3.0.1-b11") // Hive JDBC has a dynamic version constraint ("[3.0.0,)"); pin to known working version
	runtimeOnly("org.apache.hadoop:hadoop-common:3.2.1")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	implementation(project(":clifton-core"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-bloomberg")) { usingVariant("server") }

	///////////////////////////////////////////////////////////////////////////
	///////////////            Test Dependencies               ////////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
}

// Enable Windows wrapper distributions
cliftonService {
	buildWindowsWrapper = true
}
