package com.clifton.bloomberg;


import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * The <code>BloombergServerRunner</code> a runner to start a local listener.  Used by the service wrapper.
 */
public class BloombergServerRunner {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/service-clifton-bloomberg-context.xml");
		context.registerShutdownHook(); // Register spring hook to shutdown the context upon java kill (SIGTERM when kill PID)
	}
}
