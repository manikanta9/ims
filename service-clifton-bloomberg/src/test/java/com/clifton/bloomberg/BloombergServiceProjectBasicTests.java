package com.clifton.bloomberg;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * The {@link BloombergServiceProjectBasicTests} are disabled due to the current package structure. The {@link BasicProjectTests}
 * find relevant beans for testing by using the package name - i.e. com.clifton.bloomberg, which is identical to the package name
 * used in the clifton-bloomberg module. These tests may currently fail, or may fail after future updates, because of beans from
 * the clifton-bloomberg module. Rather than adding skipMethods/skipServices, we'll leave this disabled until the package structure
 * can be updated (For example, using com.clifton.bloomberg.service)
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
@Disabled
public class BloombergServiceProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "bloomberg";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.context.");
		imports.add("org.boris.winrun4j.");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Enabler");
		basicProjectAllowedSuffixNamesList.add("Interceptor");
		return basicProjectAllowedSuffixNamesList;
	}
}
