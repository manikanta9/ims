dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Alfresco dependencies
	runtimeOnly("org.codehaus.xfire:xfire-core:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// Used for CMIS document management
	api("org.apache.chemistry.opencmis:chemistry-opencmis-client-impl:0.13.0") {
		exclude(module = "json-simple")
		exclude(module = "jsr181")
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-system"))
	api(project(":clifton-system:clifton-system-note"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-system")))
}
