package com.clifton.alfresco;


import com.clifton.core.dataaccess.file.FileUtils;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.ObjectIdImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.MimeTypes;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * https://wiki.alfresco.com/wiki/CMIS#CMIS_Service_URL
 * <p>
 * Alfresco 5.0 CMIS ATOM Service URL: http://[host]:[port]/alfresco/api/-default-/public/cmis/versions/1.1/atom
 */
@Disabled
public class AlfrescoTest {

	private static Session session;

	private static final int DEFAULT_BUFFER_SIZE = 8192;

	private static final String ALFRESCO_SSO_HEADER = "X-Alfresco-Remote-User";
	private static final String CMIS_ATOMPUB_URL = "http://docs-qa.paraport.com:80/alfresco/api/-default-/public/cmis/versions/1.1/atom";
	private static final String CMIS_HEADER = "org.apache.chemistry.opencmis.binding.header";
	private static final String CMIS_REPOSITORY_ID = "-default-";
	private static final String TCG_CONTRACT = "tcg:contract"; //"D:tcg:contract";
	private static final String TCG_TABLE_NAME = "tcg:tableName";
	private static final String TCG_FOREIGN_KEY = "tcg:foreignKey";
	private static final String TCG_DO_CONVERSION = "tcg:doConversion";
	private static final String TEST_USER = "stevenf";


	@BeforeAll
	public static void connectToAlfresco() {
		Map<String, String> parameter = new HashMap<>();
		/*
		  For whatever reason SessionParameter.HEADER does not exist in this version despite being documented
		  https://chemistry.apache.org/java/0.13.0/maven/apidocs/org/apache/chemistry/opencmis/commons/SessionParameter.html#HEADER
		  So I use the hard-coded value of the constant listed here:
		  https://chemistry.apache.org/java/0.13.0/maven/apidocs/constant-values.html#org.apache.chemistry.opencmis.commons.SessionParameter.HEADER
		  To pass multiple headers you need to append the index to the constant value (e.g.: .0, .1, .2, etc)
		 */
		//Header auth can be used if the clifton-alfresco-repo.amp has been applied to the instance
		//otherwise use basic auth.
//		parameter.put(CMIS_HEADER + ".0", ALFRESCO_SSO_HEADER + ":" + TEST_USER);
		parameter.put(SessionParameter.USER, "admin");
		parameter.put(SessionParameter.PASSWORD, "admin");
		parameter.put(SessionParameter.ATOMPUB_URL, CMIS_ATOMPUB_URL);
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		parameter.put(SessionParameter.REPOSITORY_ID, CMIS_REPOSITORY_ID);

		// create session
		// default factory implementation of client runtime
		SessionFactory f = SessionFactoryImpl.newInstance();
		session = f.createSession(parameter);
	}


	@Test
	public void testGetRepositoryInfo() {
		RepositoryInfo ri = session.getRepositoryInfo();
		Assertions.assertEquals(CMIS_REPOSITORY_ID, ri.getId());
		Folder folder = session.getRootFolder();
		System.out.println(folder.getId() + " : " + folder.getName());
		for (CmisObject obj : folder.getChildren()) {
			System.out.println(obj.getId() + " : " + obj.getName());
		}
	}


	@Test
	public void testCreateDocument() {
		try {
			Resource resource = new ClassPathResource("com/clifton/alfresco/Client Account List.pdf");
			Folder folder = (Folder) session.getObjectByPath("/Contract");
			ObjectId documentId = createDocument(resource.getFile(), folder);
			Assertions.assertNotNull(documentId);
			session.delete(documentId);
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}


	@Disabled //Temp ignore to deploy to QA.
	@Test
	public void createNewVersionTest() throws IOException {

		ObjectId folderID = session.getObjectByPath("/Contract");
		Assertions.assertNotNull(folderID);

		Resource x = new ClassPathResource("com/clifton/alfresco/CFS TEMPLATE - 2010.doc");

		String filename = "CFS Template 2010.doc";
		String mimeType = "application/msword";
		File file = x.getFile();
		long fileLength = file.length();
		InputStream inStream = new FileInputStream(file);
		Map<String, String> properties = new HashMap<>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "D:" + TCG_CONTRACT);
		properties.put(PropertyIds.NAME, filename);
		properties.put(TCG_TABLE_NAME, "BusinessContract");
		ContentStream contentStream = session.getObjectFactory().createContentStream(filename, fileLength, mimeType, inStream);
		ObjectId docID = session.createDocument(properties, folderID, contentStream, VersioningState.MAJOR, null, null, null);
		inStream.close();
		Assertions.assertNotNull(docID);

		Document document = (Document) session.getObject(docID);
		try {
			System.out.println(document.getContentStreamFileName());
			//Check out created document
			ObjectId checkedOutObjectId = document.checkOut();
			Document checkedOutDoc = (Document) session.getObject(checkedOutObjectId);
			String tableName = checkedOutDoc.getPropertyValue(TCG_TABLE_NAME);
			Assertions.assertEquals("BusinessContract", tableName);
			Assertions.assertEquals(document.getVersionSeriesId(), checkedOutDoc.getVersionSeriesId());

			InputStream is = checkedOutDoc.getContentStream().getStream();
			File tempFile = FileUtils.convertInputStreamToFile("myTest", "doc", is);
			Assertions.assertEquals(checkedOutDoc.getContentStreamLength(), tempFile.length());

			//Check in the doc
			contentStream = session.getObjectFactory().createContentStream(filename, fileLength, mimeType, new FileInputStream(file));
			ObjectId checkedInId = checkedOutDoc.checkIn(true, null, contentStream, "This is my checkin comment", null, null, null);
			Assertions.assertEquals(docID.getId().substring(0, docID.getId().indexOf(";")), checkedInId.getId().substring(0, checkedInId.getId().indexOf(";")));

			document.refresh();
			Assertions.assertFalse(document.isVersionSeriesCheckedOut());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (document.isVersionSeriesCheckedOut()) {
				document.cancelCheckOut();
			}
			document.deleteAllVersions();
		}
	}


	@Test
	public void testCheckoutDocument() throws IOException {
		Resource resource = new ClassPathResource("com/clifton/alfresco/Client Account List.pdf");
		Folder folder = (Folder) session.getObjectByPath("/Contract");
		ObjectId documentId = createDocument(resource.getFile(), folder);
		Document document = (Document) session.getObject(documentId);
		try {
			ObjectId checkedOutObjectId = document.checkOut();
			Assertions.assertFalse(document.isVersionSeriesCheckedOut());
			document.refresh();
			Assertions.assertTrue(document.isVersionSeriesCheckedOut());
			Assertions.assertNotNull(checkedOutObjectId);
			Document workingCopy = (Document) session.getObject(checkedOutObjectId);
			Assertions.assertTrue(workingCopy.isVersionSeriesCheckedOut());
			workingCopy.cancelCheckOut();
			Assertions.assertTrue(workingCopy.isVersionSeriesCheckedOut());
			document.refresh();
			Assertions.assertFalse(document.isVersionSeriesCheckedOut());
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
		finally {
			document.deleteAllVersions();
		}
	}


	@Test
	public void testDownloadDocument() throws IOException {
		Resource resource = new ClassPathResource("com/clifton/alfresco/Client Account List.pdf");
		Folder folder = (Folder) session.getObjectByPath("/Contract");
		ObjectId documentId = createDocument(resource.getFile(), folder);
		Assertions.assertNotNull(documentId);
		Document document = (Document) session.getObject(documentId);
		try {
			File tempFile = File.createTempFile("temp", ".pdf");
			tempFile.deleteOnExit();
			FileUtils.convertInputStreamToFile(tempFile.getAbsolutePath(), document.getContentStream().getStream());
			Assertions.assertTrue(tempFile.length() > 0);
			Assertions.assertTrue(tempFile.getName().contains(".pdf"));
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
		finally {
			document.deleteAllVersions();
		}
	}


//	@Test
//	public void testPdfConversion() {
//   Make an integration test.
//	}


	@Test
	public void queryTest() {
		//Each type in the repos is like a table, each object is a row, and each property is a column.
		String queryString = "SELECT * FROM tcg:contract where tcg:tableName='BusinessContract' and tcg:foreignKey='13124'";
		ItemIterable<QueryResult> results = session.query(queryString, false);

		//assertEquals(1, results.getTotalNumItems());
		for (QueryResult qResult : results) {
			System.out.println("Is Latest Version = " + qResult.getPropertyValueById(PropertyIds.IS_LATEST_VERSION));
			System.out.println("Is Latest Major Version = " + qResult.getPropertyValueById(PropertyIds.IS_LATEST_MAJOR_VERSION));
			System.out.println("Is Major Version = " + qResult.getPropertyValueById(PropertyIds.IS_MAJOR_VERSION));
			System.out.println("Is Version Series Checked out = " + qResult.getPropertyValueById(PropertyIds.IS_VERSION_SERIES_CHECKED_OUT));
			System.out.println("Version Series ID = " + qResult.getPropertyValueById(PropertyIds.VERSION_SERIES_ID));
			System.out.println("Object ID = " + qResult.getPropertyValueById(PropertyIds.OBJECT_ID));
			System.out.println("Name = " + qResult.getPropertyValueById(PropertyIds.NAME));
			System.out.println("Version Series Checked Out ID = " + qResult.getPropertyValueById(PropertyIds.VERSION_SERIES_CHECKED_OUT_ID));
			System.out.println("----------------------------------");
		}
	}


	private ObjectId createDocument(File file, Folder folder) {
		return createDocument(file, folder, new HashMap<>());
	}


	private ObjectId createDocument(File file, Folder folder, Map<String, Object> properties) {
		Assertions.assertNotNull(file);
		Assertions.assertNotNull(folder);
		ObjectId documentId = null;
		try {
			properties.put(PropertyIds.OBJECT_TYPE_ID, "D:" + TCG_CONTRACT);
			properties.put(PropertyIds.NAME, file.getName());
			ContentStream contentStream = new ContentStreamImpl(file.getName(), BigInteger.valueOf(file.length()), MimeTypes.getMIMEType(file), new FileInputStream(file));
			documentId = session.createDocument(properties, new ObjectIdImpl(folder.getId()), contentStream, VersioningState.MAJOR, null, null, null);
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
		return documentId;
	}
}
