package com.clifton.document.comparisons;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.document.DocumentFileAwareTestEntity;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.DocumentTestEntity;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class DocumentComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private DocumentManagementService documentManagementService;

	@Resource
	private DocumentSetupService documentSetupService;


	@Test
	public void testCheckedOutComparison() {
		DocumentCheckedOutComparison comparison = new DocumentCheckedOutComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(true));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is currently checked out)", context.getTrueMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(false));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is not currently checked out)", context.getFalseMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is not currently checked out)", context.getFalseMessage());
	}


	@Test
	public void testNotCheckedOutComparison() {
		DocumentNotCheckedOutComparison comparison = new DocumentNotCheckedOutComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(false));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is not currently checked out)", context.getTrueMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(true));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is currently checked out)", context.getFalseMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document is not currently checked out)", context.getTrueMessage());
	}


	@Test
	public void testExistsComparison() {
		DocumentExistsComparison comparison = new DocumentExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(false));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getTrueMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(true));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getTrueMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document does not exist)", context.getFalseMessage());
	}


	@Test
	public void testExistsComparison_MultipleFilesPerEntity() {
		DocumentExistsComparison comparison = new DocumentExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(1));
		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFile", 1)).thenReturn(getDocumentRecordMocked(false));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getTrueMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(3));
		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFile", 1)).thenReturn(getDocumentRecordMocked(false));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getTrueMessage());


		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(0));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document does not exist)", context.getFalseMessage());
	}


	@Test
	public void testNotExistsComparison_MultipleFilesPerEntity() {
		DocumentNotExistsComparison comparison = new DocumentNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(1));
		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFile", 1)).thenReturn(getDocumentRecordMocked(false));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getFalseMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(3));
		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFile", 1)).thenReturn(getDocumentRecordMocked(false));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getFalseMessage());


		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentFileAwareTestEntity", 1)).thenReturn(null);
		Mockito.when(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", (long) 1)).thenReturn(getDocumentFileListMocked(0));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentFileAwareTestEntity(), context));
		Assertions.assertEquals("(Document does not exist)", context.getTrueMessage());
	}


	@Test
	public void testNotExistsComparison() {
		DocumentNotExistsComparison comparison = new DocumentNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(false));
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getFalseMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(getDocumentRecordMocked(true));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document exists)", context.getFalseMessage());

		Mockito.when(this.documentManagementService.getDocumentRecord("DocumentTestEntity", 1)).thenReturn(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(getDocumentTestEntity(), context));
		Assertions.assertEquals("(Document does not exist)", context.getTrueMessage());
	}


	private DocumentTestEntity getDocumentTestEntity() {
		DocumentTestEntity dte = new DocumentTestEntity();
		dte.setId(1);
		return dte;
	}


	private DocumentFileAwareTestEntity getDocumentFileAwareTestEntity() {
		DocumentFileAwareTestEntity dte = new DocumentFileAwareTestEntity();
		dte.setId(1);
		return dte;
	}


	/**
	 * Used to return mocked DocumentRecords with tableName, fkFieldId, and checkout fields set based upon parameters
	 */
	private DocumentRecord getDocumentRecordMocked(boolean checkedOut) {
		DocumentRecord doc = new DocumentRecord();
		doc.setTableName("DocumentTestEntity");
		doc.setFkFieldId(1);
		doc.setVersionSeriesCheckedOut(checkedOut);
		return doc;
	}


	private List<DocumentFile> getDocumentFileListMocked(int count) {
		if (count == 0) {
			return null;
		}
		List<DocumentFile> documentFileList = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			DocumentFile documentFile = new DocumentFile();
			documentFile.setId(i + 1);
			documentFileList.add(documentFile);
		}
		return documentFileList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
