package com.clifton.document;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentWithFileSizeAware;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;

import java.util.Date;


public class DocumentTestEntity extends NamedEntity<Integer> implements DocumentWithRecordStampAware, DocumentWithFileSizeAware {

	private Date documentUpdateDate;
	private String documentUpdateUser;
	private String documentFileType;

	private Long fileSize;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	@Override
	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	@Override
	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	@Override
	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	@Override
	public Long getFileSize() {
		return this.fileSize;
	}


	@Override
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
}
