package com.clifton.document;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class DocumentProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "document";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		String methodName = method.getName();

		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("deleteDocumentSecurityRestriction");
		return skipMethods.contains(methodName);
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.text.SimpleDateFormat"); // DateUtils instead? left because of UTC time zone
		imports.add("javax.xml.parsers.");
		imports.add("java.net");
		imports.add("javax.servlet.http.HttpServletRequest");
		imports.add("org.apache.chemistry.opencmis.");
		imports.add("org.apache.commons.io.IOUtils");
		imports.add("org.apache.http");
		imports.add("org.springframework.security.core.context.SecurityContextHolder");
		imports.add("org.springframework.web.context.request.RequestContextHolder");
		imports.add("org.springframework.web.context.request.ServletRequestAttributes");
		imports.add("org.w3c.dom.");
		imports.add("com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails");
		imports.add("org.springframework.transaction.support.TransactionSynchronization");
	}
}
