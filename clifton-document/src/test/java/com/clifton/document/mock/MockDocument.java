package com.clifton.document.mock;


import com.clifton.document.DocumentRecord;


/**
 * The <code>MockDocument</code> ...
 *
 * @author manderson
 */
public class MockDocument extends DocumentRecord {

	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof MockDocument)) {
			return getDocumentId().equals(((MockDocument) obj).getDocumentId());
		}
		return false;
	}


	@Override
	public int hashCode() {
		return getDocumentId().hashCode();
	}
}
