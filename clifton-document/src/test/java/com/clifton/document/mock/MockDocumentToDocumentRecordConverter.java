package com.clifton.document.mock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.document.DocumentRecord;
import com.clifton.document.converters.DocumentToDocumentRecordConverter;


/**
 * The <code>MockDocumentToDocumentRecordConverter</code> ...
 *
 * @author manderson
 */
public class MockDocumentToDocumentRecordConverter implements DocumentToDocumentRecordConverter<MockDocument> {

	@Override
	public DocumentRecord convert(MockDocument from) {
		if (from == null) {
			return null;
		}
		DocumentRecord record = new DocumentRecord();
		BeanUtils.copyProperties(from, record);
		return record;
	}
}
