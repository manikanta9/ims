package com.clifton.document.mock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.DocumentContentStream;
import com.clifton.core.util.CollectionUtils;
import com.clifton.document.DocumentManagementProvider;
import com.clifton.document.DocumentRecord;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>MockDocumentManagementProviderImpl</code> provides a mock implementation
 * of the DocumentManagementProvider methods that can be used for testing.
 *
 * @author manderson
 */
@SuppressWarnings("unused")
public class MockDocumentManagementProviderImpl implements DocumentManagementProvider<MockDocument> {

	private Map<String, MockDocument> documentMap = new HashMap<>();


	@Override
	public DocumentContentStream getDocumentContentStream(String documentId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public DocumentContentStream getDocumentContentStream(MockDocument documentId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public MockDocument cancelCheckoutDocument(MockDocument document) {
		// Get the Original
		MockDocument originalDocument = this.documentMap.get(document.getVersionSeriesID());

		// Delete the Checked Out Version Copy
		this.documentMap.remove(document.getDocumentId());

		// Clear Check Out fields on the Original Document & Return it
		originalDocument.setCheckedOutUser(null);
		originalDocument.setVersionSeriesCheckedOut(false);
		originalDocument.setVersionSeriesCheckedOutId(null);
		originalDocument.setUpdateDate(new Date());
		originalDocument.setUpdateUser("TestUser");
		this.documentMap.put(originalDocument.getDocumentId(), originalDocument);
		return originalDocument;
	}


	@Override
	public MockDocument checkinDocument(DocumentRecord documentRecord) {
		// Get the Original
		MockDocument originalDocument = this.documentMap.get(documentRecord.getVersionSeriesID());

		Double version = new Double(originalDocument.getVersionLabel());
		if (documentRecord.isMajorVersion()) {
			version = version + 1;
		}
		else {
			version = version + 0.1;
		}

		// Check In the Checked Out Version Copy i.e. set version label & comment
		MockDocument newDocument = this.documentMap.get(documentRecord.getDocumentId());
		newDocument.setCheckinComment(documentRecord.getCheckinComment());
		newDocument.setVersionLabel(version + "");
		newDocument.setName(originalDocument.getName());
		this.documentMap.put(newDocument.getDocumentId(), newDocument);

		// Clear Check Out fields on the Original Document & Return it
		originalDocument.setCheckedOutUser(null);
		originalDocument.setVersionLabel(newDocument.getVersionLabel());
		originalDocument.setVersionSeriesCheckedOut(false);
		originalDocument.setVersionSeriesCheckedOutId(null);
		originalDocument.setUpdateDate(new Date());
		originalDocument.setUpdateUser("TestUser");
		this.documentMap.put(originalDocument.getDocumentId(), originalDocument);
		return originalDocument;
	}


	@Override
	public MockDocument checkoutDocument(MockDocument document) {
		// Get the Original
		MockDocument originalDocument = this.documentMap.get(document.getDocumentId());

		MockDocument pwc = new MockDocument();
		BeanUtils.copyProperties(originalDocument, pwc);
		pwc.setCheckedOutUser("TestUser");
		pwc.setVersionLabel("pwc");
		pwc.setTableName(document.getTableName());
		pwc.setFkFieldId(document.getFkFieldId());
		pwc.setDocumentId("document-" + CollectionUtils.getSize(this.documentMap.keySet()) + 3);
		pwc.setVersionSeriesID(originalDocument.getDocumentId());
		pwc.setLatestVersion(false);
		String[] nameExt = (originalDocument.getName()).split("\\.");
		pwc.setName(nameExt[0] + " (Working Copy)" + "." + nameExt[1]);
		pwc.setUpdateDate(new Date());
		pwc.setUpdateUser("TestUser");

		this.documentMap.put(pwc.getDocumentId(), pwc);

		originalDocument.setCheckedOutUser("TestUser");
		originalDocument.setVersionSeriesCheckedOut(true);
		originalDocument.setVersionSeriesCheckedOutId(pwc.getDocumentId());
		originalDocument.setUpdateDate(new Date());
		originalDocument.setUpdateUser("TestUser");
		this.documentMap.put(originalDocument.getDocumentId(), originalDocument);
		return originalDocument;
	}


	@Override
	public MockDocument copyDocument(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord) {
		// DO NOTHING
		return new MockDocument();
	}


	@Override
	public void copyDocumentContentStream(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord) {
		// DO NOTHING
	}


	@Override
	public MockDocument createDocument(DocumentRecord documentRecord) {
		MockDocument doc = new MockDocument();
		BeanUtils.copyProperties(documentRecord, doc);
		doc.setVersionLabel("1.0");
		doc.setCheckinComment("Initial Version");
		doc.setDocumentId("document-" + CollectionUtils.getSize(this.documentMap.keySet()) + 3);
		doc.setVersionSeriesID(doc.getDocumentId());
		doc.setLatestVersion(true);
		doc.setUpdateDate(new Date());
		doc.setUpdateUser("TestUser");
		this.documentMap.put(doc.getDocumentId(), doc);
		return doc;
	}


	@Override
	public void deleteDocument(MockDocument document) {
		Map<String, MockDocument> newMap = new HashMap<>();
		for (MockDocument doc : CollectionUtils.getIterable(this.documentMap.values())) {
			if (!document.getDocumentId().equals(doc.getVersionSeriesID())) {
				newMap.put(doc.getDocumentId(), doc);
			}
		}
		this.documentMap = newMap;
	}


	@Override
	public MockDocument getDocument(String documentId) {
		return this.documentMap.get(documentId);
	}


	@Override
	public MockDocument getRendition(String documentId, String type) {
		return getRendition(this.documentMap.get(documentId), type);
	}


	@Override
	public MockDocument getRendition(MockDocument document, String type) {
		return document;
	}


	@Override
	public MockDocument getDocumentByTableAndFkField(String tableName, long fkFieldId) {
		for (MockDocument doc : CollectionUtils.getIterable(this.documentMap.values())) {
			if (tableName.equals(doc.getTableName()) && MathUtils.isEqual(fkFieldId, doc.getFkFieldId()) && doc.isLatestVersion()) {
				return doc;
			}
		}
		return null;
	}


	@Override
	public List<MockDocument> getDocumentVersionList(MockDocument document) {
		List<MockDocument> docList = new ArrayList<>();
		for (MockDocument doc : CollectionUtils.getIterable(this.documentMap.values())) {
			if (document.getDocumentId().equals(doc.getVersionSeriesID())) {
				docList.add(doc);
			}
		}
		return docList;
	}


	@Override
	public String getDocumentWebdavUrl(DocumentRecord documentRecord) {
		return "TEST WEBDAV URL: " + documentRecord.getName();
	}


	@Override
	public MockDocument updateDocument(DocumentRecord documentRecord, String changedFileExtension) {
		return this.documentMap.get(documentRecord.getDocumentId());
	}


	@Override
	public void updateDocumentProperties(MockDocument document, String newTableName, long newFkFieldId) {
		// do nothing
	}
}
