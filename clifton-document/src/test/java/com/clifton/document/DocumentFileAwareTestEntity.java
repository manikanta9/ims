package com.clifton.document;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.beans.document.DocumentFilePropertyAware;


/**
 * The <code>DocumentFileAwareTestEntity</code> ...
 *
 * @author manderson
 */
public class DocumentFileAwareTestEntity extends NamedEntity<Integer> implements DocumentFileCountAware, DocumentFilePropertyAware {

	private Short documentFileCount;

	private String documentFileType;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}
}
