package com.clifton.document.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.document.DocumentRecord;
import com.clifton.document.DocumentTestEntity;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>DocumentSetupServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class DocumentSetupServiceImplTests {

	@Resource
	private DocumentSetupService documentSetupService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private UpdatableDAO<DocumentTestEntity> documentTestEntityDAO;


	@Test
	public void testInsertDefinition() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def = BeanUtils.cloneBean(def, false, false);
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testDeleteDefinition() {
		Assertions.assertThrows(Exception.class, () -> this.documentSetupService.deleteDocumentDefinition(MathUtils.SHORT_ONE));
	}


	@Test
	public void testUpdateDefinitionChangeTable() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setTable(this.systemSchemaService.getSystemTable(MathUtils.SHORT_TWO));
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testUpdateDefinitionOneDocumentPerEntity() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setOneDocumentPerEntity(false);
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testUpdateDefinitionName() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setName("Changing my name");
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testDefinitionUpdateNoRevisionsSupported() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setMajorRevisionSupported(false);
			def.setMinorRevisionSupported(false);
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testUpdateDefaultComment() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		def.setDefaultComment("Test comment");
		this.documentSetupService.saveDocumentDefinition(def);
	}


	@Test
	public void testUpdateDefinitionFolder() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		String originalFolder = def.getFolderStructure();
		def.setFolderStructure("IMS/Test Update");
		this.documentSetupService.saveDocumentDefinition(def);

		// Set it back so doesn't affect other tests
		def.setFolderStructure(originalFolder);
		this.documentSetupService.saveDocumentDefinition(def);
	}


	@Test
	public void testGetDocumentFolder() {
		Assertions.assertEquals("/IMS/Test", this.documentSetupService.getDocumentFolderStructure("DocumentTestEntity", null));
	}


	@Test
	public void testGetDocumentFolderNoMapping() {
		Assertions.assertThrows(Exception.class, () -> {
			// Should throw an exception, because there is no definition mapping for table SystemTable
			this.documentSetupService.getDocumentFolderStructure("SystemTable", null);
		});
	}


	@Test
	public void testUpdateDefinitionDocumentName() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		String originalName = def.getDocumentName();

		def.setDocumentName("${(bean.description)!}_Test");
		this.documentSetupService.saveDocumentDefinition(def);

		def.setDocumentName(originalName);
		this.documentSetupService.saveDocumentDefinition(def);
	}


	@Test
	public void testUpdateDefinitionDocumentNameWithBadSyntax() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setDocumentName("${bean.missingProperty}_Test");
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testGetDocumentNameNoEntities() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		// No Entities Exist for Samples:
		String result = this.documentSetupService.getDocumentNameSample(def.getId(), def.getDocumentName());
		Assertions.assertEquals("Syntax evaluated OK, however no [DocumentTestEntity] entities exist in the system to generate an actual sample.", result);
	}


	@Test
	public void testGetDocumentNames() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		def.setDocumentName("${(bean.name)!}_Test");
		def.setDocumentVersionName("${(bean.name)!}_v${document.versionLabel!}");
		this.documentSetupService.saveDocumentDefinition(def);

		// Add a new entity
		DocumentTestEntity dte1 = new DocumentTestEntity();
		dte1.setName("TEST Entity 1");
		dte1.setDescription("This is a test entity");
		this.documentTestEntityDAO.save(dte1);

		// Sample should now pull from Test Entity 1
		// Document Name
		String result = this.documentSetupService.getDocumentNameSample(def.getId(), def.getDocumentName());
		Assertions.assertEquals(dte1.getName() + "_Test", result);

		// Document Version Name
		result = this.documentSetupService.getDocumentNameSample(def.getId(), def.getDocumentVersionName());
		Assertions.assertEquals(dte1.getName() + "_v1.0", result);

		// Change Sample Text to Name_Description
		result = this.documentSetupService.getDocumentNameSample(def.getId(), "${(bean.name)!}_${(bean.description)!}");
		Assertions.assertEquals(dte1.getName() + "_" + dte1.getDescription(), result);

		// And another entity
		DocumentTestEntity dte2 = new DocumentTestEntity();
		dte2.setName("TEST Entity 2");
		dte2.setDescription("This is another test entity");
		this.documentTestEntityDAO.save(dte2);

		DocumentRecord record = new DocumentRecord();
		record.setTableName("DocumentTestEntity");
		record.setFkFieldId(2);
		record.setVersionLabel("2.1");
		record.setUpdateDate(DateUtils.toDate("05/01/2010 9:45 AM", DateUtils.DATE_FORMAT_SHORT));

		// Explicitly get the Document Name for Entity 2 - Should only be the name because that's the format saved in the DB
		Assertions.assertEquals(dte2.getName() + "_Test", this.documentSetupService.getDocumentName(record));

		// Change Document Version Syntax to Include Date
		def.setDocumentVersionName("${bean.name}_${((document.updateDate)?string(\"yyyy_MM_dd\"))!}_v${document.versionLabel!}");
		this.documentSetupService.saveDocumentDefinition(def);
		Assertions.assertEquals(dte2.getName() + "_2010_05_01_v2.1", this.documentSetupService.getDocumentVersionName(record));

		// Delete Test Entities
		this.documentTestEntityDAO.delete(dte1.getId());
		this.documentTestEntityDAO.delete(dte2.getId());
	}


	@Test
	public void testUpdateDefinitionDocumentVersionName() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
		def.setDocumentVersionName("${(bean.description)!}_Test");
		this.documentSetupService.saveDocumentDefinition(def);
	}


	@Test
	public void testUpdateDefinitionDocumentVersionNameWithBadSyntax() {
		Assertions.assertThrows(Exception.class, () -> {
			DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);
			def.setDocumentVersionName("${bean.missingProperty}_Test");
			this.documentSetupService.saveDocumentDefinition(def);
		});
	}


	@Test
	public void testGetDocumentVersionNameNoEntities() {
		DocumentDefinition def = this.documentSetupService.getDocumentDefinition(MathUtils.SHORT_ONE);

		// No Entities Exist for Samples:
		String result = this.documentSetupService.getDocumentNameSample(def.getId(), def.getDocumentVersionName());
		Assertions.assertEquals("Syntax evaluated OK, however no [DocumentTestEntity] entities exist in the system to generate an actual sample.", result);
	}
}
