package com.clifton.document;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentWithFileSizeAware;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.document.setup.DocumentDefinition;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * The <code>DocumentManagementServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class DocumentManagementServiceImplTests {

	@Resource
	private DocumentManagementService documentManagementService;

	@Resource
	private DocumentSetupService documentSetupService;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private UpdatableDAO<DocumentTestEntity> documentTestEntityDAO;

	@Resource
	private UpdatableDAO<DocumentFileAwareTestEntity> documentFileAwareTestEntityDAO;


	////////////////////////////////////////////////////////////////////////////////
	/////////              One Document Per Entity Tests                   /////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDocumentManagement_OnePerEntity() {
		DocumentTestEntity testBean = new DocumentTestEntity();
		testBean.setName("My Test Bean");
		this.documentTestEntityDAO.save(testBean);

		String tableName = "DocumentTestEntity";
		long fkFieldId = BeanUtils.getIdentityAsLong(testBean);

		DocumentDefinition def = this.documentSetupService.getDocumentDefinitionForEntity(tableName, fkFieldId);
		def.setDocumentName("${bean.name}_Test");
		def.setMajorCommentRequired(true);
		// Check Outs are valid only if bean name = "My Test Bean"
		def.setModifyCondition(this.systemConditionService.getSystemCondition(1));

		this.documentSetupService.saveDocumentDefinition(def);

		DocumentRecord docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
		AssertUtils.assertNull(docRecord, "Tests should begin with NO document for table name [%s] and fkFieldId [%i]", tableName, fkFieldId);

		// Test Initial Upload
		uploadDocumentAndValidate(tableName, fkFieldId, "src/test/java/com/clifton/document/TestDocument.docx", true);

		// Change the Bean Name so it fails checkout condition
		testBean.setName("Bad Bean Name");
		this.documentTestEntityDAO.save(testBean);

		// Check out the Document
		try {
			checkoutDocument(tableName, fkFieldId);
		}
		catch (Exception e) {
			// Expected exception - can't check out because of document modify condition
			Assertions.assertEquals("Unable to checkout document because: (name = Bad Bean Name instead of My Test Bean)", e.getMessage());

			testBean.setName("My Test Bean");
			this.documentTestEntityDAO.save(testBean);

			// Try to checkout again
			checkoutDocument(tableName, fkFieldId);

			docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
			String webDavUrl = this.documentManagementService.getDocumentRecordWebDavUrl(docRecord.getVersionSeriesCheckedOutId());
			Assertions.assertEquals("TEST WEBDAV URL: My Test Bean_Test (Working Copy).docx", webDavUrl);

			// Try to check it out again - should fail
			try {
				checkoutDocument(tableName, fkFieldId);
			}
			catch (Exception e2) {
				// Expected...continue on
				Assertions.assertEquals("Error during checkout of document [Document is currently checked out by [TestUser].]", e2.getMessage());

				// Try to copy checked out document - should throw an exception
				try {
					this.documentManagementService.copyDocumentRecord(tableName, fkFieldId, tableName, 100);
				}
				catch (Exception e3) {
					// Check message
					Assertions.assertEquals("Error copying document record within system [Unable to copy document [My Test Bean_Test.docx].  It is currently checked out by [TestUser].]", e3.getMessage());

					// Cancel Check Out
					cancelCheckoutDocument(tableName, fkFieldId);

					// Check Out the Document Again
					checkoutDocument(tableName, fkFieldId);

					// Upload an Update
					try {
						uploadDocumentAndValidate(tableName, fkFieldId, "src/test/java/com/clifton/document/TestDocumentUpdate.xlsx", false);
					}
					catch (Exception e4) {
						// Expected to Fail because extension is different than original file.. Continue On...
						Assertions.assertEquals("Error during upload of document [The original file for this entity was a [docx].  You cannot replace it with a [xlsx]]", e4.getMessage());

						// Make sure the Document is still checked out
						docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
						Assertions.assertEquals(tableName, docRecord.getTableName());
						Assertions.assertEquals(1, docRecord.getFkFieldId());
						Assertions.assertNotNull(docRecord.getCheckedOutUser());
						Assertions.assertNotNull(docRecord.getVersionSeriesCheckedOutId());

						testCheckinDocument(docRecord);

						// Validate Full Version List
						List<DocumentRecord> versionList = this.documentManagementService.getDocumentRecordVersionList(tableName, fkFieldId);
						Assertions.assertEquals(2, CollectionUtils.getSize(versionList));

						this.documentManagementService.deleteDocumentRecord(docRecord.getDocumentId(), tableName, fkFieldId, true);

						versionList = this.documentManagementService.getDocumentRecordVersionList(tableName, fkFieldId);
						Assertions.assertEquals(0, CollectionUtils.getSize(versionList));
						return;
					}
					Assertions.fail("Expected exception when trying to upload a document with a different extension than the document was first uploaded as.");
				}
				Assertions.fail("Expected exception when trying to check out a document that is already checked out.");
			}
			Assertions.fail("Expected exception during copy of a checked out document - as it is not allowed.");
		}
		Assertions.fail("Should not be able to check out document because of entity modify condition name = My Test Bean");
	}


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	/////      Multiple Document Per Entity Tests      /////
	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	@Test
	public void testDocumentManagement_MultiplePerEntityFileCount() {
		DocumentFileAwareTestEntity testBean = new DocumentFileAwareTestEntity();
		testBean.setName("My Test Bean");
		this.documentFileAwareTestEntityDAO.save(testBean);
		Assertions.assertTrue(testBean.getDocumentFileCount() == null);

		String typeOneDefinitionName = "Test - File Type 1";
		String typeTwoDefinitionName = "Test - File Type 2";

		int fkFieldId = testBean.getId();

		// Upload a New Document to Type 1
		DocumentFile fileBean1a = uploadDocumentFile(typeOneDefinitionName, fkFieldId, "src/test/java/com/clifton/document/TestDocument.docx", "My First Test Document");

		// Validate File Count was incremented on the test bean
		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals(Short.valueOf("1"), testBean.getDocumentFileCount());
		Assertions.assertEquals(1, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));
		// Validate File Properties updated on the Document File object
		fileBean1a = this.documentSetupService.getDocumentFile(fileBean1a.getId());
		Assertions.assertEquals(fileBean1a.getDocumentFileType(), "docx");

		// Now look up the actual file
		DocumentRecord docRecord1a = this.documentManagementService.getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1a.getId());
		AssertUtils.assertNotNull(docRecord1a, "This record should have a document");

		// Now add another file to this entity
		// Don't set a name so it picks up the actual file name
		DocumentFile fileBean1b = uploadDocumentFile(typeOneDefinitionName, fkFieldId, "src/test/java/com/clifton/document/TestDocument.docx", null);

		// Validate File Count was incremented on the test bean
		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals(Short.valueOf("2"), testBean.getDocumentFileCount());
		Assertions.assertEquals(2, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));
		// Validate File Properties updated on the Document File object
		fileBean1b = this.documentSetupService.getDocumentFile(fileBean1b.getId());
		Assertions.assertEquals(fileBean1b.getDocumentFileType(), "docx");
		Assertions.assertEquals("TestDocument", fileBean1b.getName());

		// Now look up the actual file
		DocumentRecord docRecord1b = this.documentManagementService.getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1b.getId());
		AssertUtils.assertNotNull(docRecord1b, "This record should have a document");

		// Check out the Document
		checkoutDocument(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1b.getId());
		docRecord1b = this.documentManagementService.getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1b.getId());
		String webDavUrl = this.documentManagementService.getDocumentRecordWebDavUrl(docRecord1b.getVersionSeriesCheckedOutId());
		Assertions.assertEquals("TEST WEBDAV URL: type 1 (Working Copy).docx", webDavUrl);

		Assertions.assertEquals("/IMS/Test Type 1", this.documentSetupService.getDocumentFolderStructure(DocumentFile.DOCUMENT_FILE_TABLE_NAME, BeanUtils.getIdentityAsLong(fileBean1b)));

		// Cancel Check Out
		cancelCheckoutDocument(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1b.getId());

		// Upload a New Document to Type 2
		DocumentFile fileBean2a = uploadDocumentFile(typeTwoDefinitionName, fkFieldId, "src/test/java/com/clifton/document/TestDocument.docx", "My Second type of Test Document");
		Assertions.assertEquals("/IMS/Test Type 2", this.documentSetupService.getDocumentFolderStructure(DocumentFile.DOCUMENT_FILE_TABLE_NAME, BeanUtils.getIdentityAsLong(fileBean2a)));

		// Validate File Count was incremented on the test bean
		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals(Short.valueOf("3"), testBean.getDocumentFileCount());
		Assertions.assertEquals(3, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));
		// Validate File Properties updated on the Document File object
		fileBean2a = this.documentSetupService.getDocumentFile(fileBean2a.getId());
		Assertions.assertEquals("docx", fileBean2a.getDocumentFileType());

		// Now look up the actual file
		DocumentRecord docRecord2a = this.documentManagementService.getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fileBean1a.getId());
		AssertUtils.assertNotNull(docRecord2a, "This record should have a document");

		// Delete 1b document file - Should delete file and file count total
		this.documentSetupService.deleteDocumentFile(fileBean1b.getId());

		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals(Short.valueOf("2"), testBean.getDocumentFileCount());

		// Now delete the original test entity - should also delete all the other documents
		this.documentFileAwareTestEntityDAO.delete(testBean);
		Assertions.assertEquals(0, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));
	}


	@Test
	public void testDocumentManagement_MultiplePerEntityFileProperty() {
		DocumentFileAwareTestEntity testBean = new DocumentFileAwareTestEntity();
		testBean.setName("My Test Bean2");
		this.documentFileAwareTestEntityDAO.save(testBean);
		Assertions.assertTrue(testBean.getDocumentFileCount() == null);
		Assertions.assertTrue(testBean.getDocumentFileType() == null);

		String definitionName = "Test - File Type 1";

		int fkFieldId = testBean.getId();

		// Upload a New Document to Type 1
		DocumentFile excelDocument = uploadDocumentFile(definitionName, fkFieldId, "src/test/java/com/clifton/document/TestExcelDocument.xlsx", "My First Test Document");

		// Validate File Type Property
		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals("xlsx", testBean.getDocumentFileType());

		// Now add another file to this entity
		DocumentFile wordDocument = uploadDocumentFile(definitionName, fkFieldId, "src/test/java/com/clifton/document/TestDocument.docx", null);

		// Validate File Type Property Cleared
		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertNull(testBean.getDocumentFileType());
		Assertions.assertEquals(2, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));

		// Delete word document file - Should update property to xlsx
		this.documentSetupService.deleteDocumentFile(wordDocument.getId());

		testBean = this.documentFileAwareTestEntityDAO.findByPrimaryKey(testBean.getId());
		Assertions.assertEquals("xlsx", testBean.getDocumentFileType());

		// Now delete the original test entity - should also delete all the other documents
		this.documentFileAwareTestEntityDAO.delete(testBean);
		Assertions.assertEquals(0, CollectionUtils.getSize(this.documentSetupService.getDocumentFileListForEntity("DocumentFileAwareTestEntity", BeanUtils.getIdentityAsLong(testBean))));
	}


	////////////////////////////////////////////////////////
	////////           Test Helper Methods          ////////
	////////////////////////////////////////////////////////


	private DocumentFile uploadDocumentFile(String definitionName, long fkFieldId, String filePath, String fileName) {
		DocumentFile documentFile = new DocumentFile();
		documentFile.setDefinition(this.documentSetupService.getDocumentDefinitionByName(definitionName));
		documentFile.setFkFieldId(fkFieldId);
		documentFile.setFile(new MultipartFileImpl(filePath));
		documentFile.setName(fileName);
		this.documentManagementService.uploadDocumentFile(documentFile);
		return documentFile;
	}


	private void uploadDocumentAndValidate(String tableName, long fkFieldId, String fileName, boolean newDoc) {
		DocumentRecord docRecord = new DocumentRecord();

		// Upload a New Document
		if (newDoc) {
			docRecord.setTableName(tableName);
			docRecord.setFkFieldId(fkFieldId);
		}
		else {
			docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
		}
		docRecord.setFile(new MultipartFileImpl(fileName));
		docRecord.setFileSize(docRecord.getFile().getSize());

		Date originalUpdateDate = docRecord.getUpdateDate();

		// Set wait time to ensure update date times are different
		sleep();

		this.documentManagementService.uploadDocumentRecord(docRecord);

		// Make sure the document exists now and validate fields are set.
		docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
		AssertUtils.assertNotNull(docRecord, "Test Document should exist for table name [%s] and fkFieldId [%d]", tableName, fkFieldId);
		Assertions.assertEquals(tableName, docRecord.getTableName());
		Assertions.assertEquals(fkFieldId, docRecord.getFkFieldId());
		Assertions.assertNull(docRecord.getCheckedOutUser());
		Assertions.assertEquals("Initial Version", docRecord.getCheckinComment());
		Assertions.assertEquals("1.0", docRecord.getVersionLabel());
		Assertions.assertEquals("/IMS/Test", docRecord.getFolderName());
		Assertions.assertEquals("My Test Bean_Test.docx", docRecord.getName());
		Assertions.assertEquals("docx", docRecord.getFileExtension());

		// Verify Update Date was actually updated
		Assertions.assertFalse(0 == DateUtils.compare(originalUpdateDate, docRecord.getUpdateDate(), true));

		// Verify Document Record Stamp Properties on Bean
		DocumentTestEntity testBean = this.documentTestEntityDAO.findByPrimaryKey(docRecord.getFkFieldId());
		Assertions.assertEquals("docx", testBean.getDocumentFileType());
		Assertions.assertEquals(docRecord.getUpdateDate(), testBean.getDocumentUpdateDate());
		Assertions.assertEquals(docRecord.getUpdateUser(), testBean.getDocumentUpdateUser());
		Assertions.assertEquals(docRecord.getFileSize(), testBean.getFileSize());
	}


	private void checkoutDocument(String tableName, long fkFieldId) {
		DocumentRecord docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
		Date originalUpdateDate = docRecord.getUpdateDate();

		// Set wait time to ensure update date times are different
		sleep();

		docRecord = this.documentManagementService.checkoutDocumentRecord(docRecord.getDocumentId(), tableName, fkFieldId);
		Assertions.assertEquals(tableName, docRecord.getTableName());
		Assertions.assertEquals(fkFieldId, docRecord.getFkFieldId());
		Assertions.assertNotNull(docRecord.getCheckedOutUser());
		Assertions.assertNotNull(docRecord.getVersionSeriesCheckedOutId());

		// Verify Update Date was actually updated
		Assertions.assertFalse(0 == DateUtils.compare(originalUpdateDate, docRecord.getUpdateDate(), true));

		DocumentWithRecordStampAware testBean;
		if (DocumentFile.DOCUMENT_FILE_TABLE_NAME.equals(tableName)) {
			testBean = this.documentSetupService.getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId));
		}
		else {
			testBean = this.documentTestEntityDAO.findByPrimaryKey(docRecord.getFkFieldId());
		}
		// Verify Document Record Stamp Properties on Bean
		Assertions.assertEquals("docx", testBean.getDocumentFileType());
		Assertions.assertEquals(docRecord.getUpdateDate(), testBean.getDocumentUpdateDate());
		Assertions.assertEquals(docRecord.getUpdateUser(), testBean.getDocumentUpdateUser());
		if (DocumentWithFileSizeAware.class.isAssignableFrom(testBean.getClass())) {
			Assertions.assertEquals(docRecord.getFileSize(), ((DocumentWithFileSizeAware) testBean).getFileSize());
		}
	}


	private void cancelCheckoutDocument(String tableName, long fkFieldId) {
		DocumentRecord docRecord = this.documentManagementService.getDocumentRecord(tableName, fkFieldId);
		Date originalUpdateDate = docRecord.getUpdateDate();

		// Set wait time to ensure update date times are different
		sleep();

		docRecord = this.documentManagementService.cancelCheckoutDocumentRecord(docRecord.getVersionSeriesCheckedOutId());
		Assertions.assertEquals(tableName, docRecord.getTableName());
		Assertions.assertEquals(fkFieldId, docRecord.getFkFieldId());
		Assertions.assertNull(docRecord.getCheckedOutUser());
		Assertions.assertNull(docRecord.getVersionSeriesCheckedOutId());

		// Verify Update Date was actually updated
		Assertions.assertFalse(0 == DateUtils.compare(originalUpdateDate, docRecord.getUpdateDate(), true));

		DocumentWithRecordStampAware testBean;
		if (DocumentFile.DOCUMENT_FILE_TABLE_NAME.equals(tableName)) {
			testBean = this.documentSetupService.getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId));
		}
		else {
			testBean = this.documentTestEntityDAO.findByPrimaryKey(docRecord.getFkFieldId());
		}
		// Verify Document Record Stamp Properties on Bean
		Assertions.assertEquals("docx", testBean.getDocumentFileType());
		Assertions.assertEquals(docRecord.getUpdateDate(), testBean.getDocumentUpdateDate());
		Assertions.assertEquals(docRecord.getUpdateUser(), testBean.getDocumentUpdateUser());
		if (DocumentWithFileSizeAware.class.isAssignableFrom(testBean.getClass())) {
			Assertions.assertEquals(docRecord.getFileSize(), ((DocumentWithFileSizeAware) testBean).getFileSize());
		}
	}


	private void testCheckinDocument(DocumentRecord docRecord) {
		Date originalUpdateDate = docRecord.getUpdateDate();

		// Set wait time to ensure update date times are different
		sleep();

		// Check In the Document & Validate Fields
		docRecord.setMajorVersion(false);
		docRecord.setCheckinComment(null);

		try {
			// Although Minor Revisions are not supported - manager auto selects the major version
			this.documentManagementService.checkinDocumentRecord(docRecord);
		}
		catch (Exception e) {
			// Exception Expected - Major Version required a comment
			docRecord.setCheckinComment("Test Update");
			this.documentManagementService.checkinDocumentRecord(docRecord);

			docRecord = this.documentManagementService.getDocumentRecord(docRecord.getTableName(), docRecord.getFkFieldId());
			Assertions.assertNull(docRecord.getCheckedOutUser());
			Assertions.assertNull(docRecord.getVersionSeriesCheckedOutId());
			Assertions.assertEquals("Test Update", docRecord.getCheckinComment());
			Assertions.assertEquals("2.0", docRecord.getVersionLabel());

			// Verify Update Date was actually updated
			Assertions.assertFalse(0 == DateUtils.compare(originalUpdateDate, docRecord.getUpdateDate(), true));

			// Verify Document Record Stamp Properties on Bean
			DocumentTestEntity testBean = this.documentTestEntityDAO.findByPrimaryKey(docRecord.getFkFieldId());
			Assertions.assertEquals("docx", testBean.getDocumentFileType());
			Assertions.assertEquals(docRecord.getUpdateDate(), testBean.getDocumentUpdateDate());
			Assertions.assertEquals(docRecord.getUpdateUser(), testBean.getDocumentUpdateUser());
			if (DocumentWithFileSizeAware.class.isAssignableFrom(testBean.getClass())) {
				Assertions.assertEquals(docRecord.getFileSize(), testBean.getFileSize());
			}


			return;
		}
		Assertions.fail("Expected exception because major revisions require a comment - (defined in the definition)");
	}


	private void sleep() {
		try {
			Thread.sleep(100);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
