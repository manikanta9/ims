package com.clifton.document;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.beans.document.DocumentWithFileSizeAware;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.cache.DocumentIdCache;
import com.clifton.document.converters.DocumentToDocumentRecordConverter;
import com.clifton.document.setup.DocumentDefinition;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentFileUpload;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.document.setup.locator.DocumentFileFkFieldLocator;
import com.clifton.document.setup.search.DocumentFileSearchForm;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>DocumentManagerServiceImpl</code> provides implementation of the document management methods.
 * It calls the document system specific methods defined by {@link DocumentManagementProvider}.
 *
 * @author manderson
 */
@Service
public class DocumentManagementServiceImpl<T> implements DocumentManagementService {

	private DocumentIdCache documentIdCache;

	private static final Map<String, String> FILE_EXTENSION_EQUAL_MAP = new ConcurrentHashMap<>();


	static {
		FILE_EXTENSION_EQUAL_MAP.put("doc", "docx");
		FILE_EXTENSION_EQUAL_MAP.put("docx", "doc");
		FILE_EXTENSION_EQUAL_MAP.put("xls", "xlsx");
		FILE_EXTENSION_EQUAL_MAP.put("xlsx", "xls");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private DocumentToDocumentRecordConverter<T> documentToDocumentRecordConverter;
	private DocumentManagementProvider<T> documentManagementProvider;
	private DocumentSetupService documentSetupService;

	private SecurityUserService securityUserService;
	private SystemBeanService systemBeanService;

	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////////
	////////////                   Download Methods                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadDocumentRecord(String documentId) {
		T document = getDocument(documentId);
		DocumentRecord documentRecord = getDocumentRecordForDocument(document);
		// Get the details from the document itself that is stored in Alfresco and ensure that the current
		// user has View Access to it
		getDocumentSetupService().validateDocumentViewCondition(documentRecord.getTableName(), documentRecord.getFkFieldId());
		try {
			String extension = FileUtils.getFileExtension(documentRecord.getName());
			if (StringUtils.isEmpty(extension)) {
				extension = "tmp";
			}
			// If it's not the latest version, and it's not the PWC, use the Version Name for Downloads
			File file = FileUtils.convertInputStreamToFile("temp", extension, getDocumentManagementProvider().getDocumentContentStream(document).getStream());
			String fileName = getDocumentSetupService().getDocumentName(documentRecord) + "." + extension;
			if (!documentRecord.isLatestVersion() && !(documentRecord.getDocumentId().equals(documentRecord.getVersionSeriesCheckedOutId()))) {
				fileName = getDocumentSetupService().getDocumentVersionName(documentRecord) + "." + extension;
			}
			return new FileWrapper(file, fileName, true);
		}
		catch (Exception e) {
			handleDocumentException(e, "Unable to download content of document");
		}
		return null;
	}


	@Override
	public FileWrapper downloadDocumentRecordConversion(String documentId, String type) {
		ValidationUtils.assertNotNull(type, "Type to convert document to is missing");
		T document = getDocument(documentId);
		T documentRendition = getDocumentManagementProvider().getRendition(document, type);
		//Convert the original document to a record in order to retrieve the original file name.
		DocumentRecord documentRecord = getDocumentRecordForDocument(document);
		try {
			String fileName = FileUtils.getFileNameWithoutExtension(documentRecord.getName());
			if (documentRendition != null) {
				File tempFile = FileUtils.convertInputStreamToFile("temp", type,
						getDocumentManagementProvider().getDocumentContentStream(documentRendition).getStream());
				return new FileWrapper(FileUtils.getPDFCopyWithCorrectTitle(tempFile, fileName, type, true), fileName + "." + type, true);
			}
			else {
				String extension = FileUtils.getFileExtension(documentRecord.getName());
				File tempFile = FileUtils.convertInputStreamToFile("temp", extension,
						getDocumentManagementProvider().getDocumentContentStream(document).getStream());
				return new FileWrapper(tempFile, fileName + "." + extension, true);
			}
		}
		catch (Exception e) {
			handleDocumentException(e, "Error downloading document record conversion.");
		}
		return null;
	}


	@Override
	public FileWrapper downloadDocumentRecordsAsZip(String tableName, long[] fkFieldIds) {
		if (fkFieldIds == null || fkFieldIds.length == 0) {
			throw new ValidationException("No files selected to download for Zip file.");
		}
		String fileName = "Files_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE);
		// Zipping
		List<FileWrapper> fileList = new ArrayList<>();
		for (Long fkFieldId : fkFieldIds) {
			DocumentRecord documentRecord = getDocumentRecord(tableName, fkFieldId);
			if (documentRecord != null && documentRecord.getDocumentId() != null) {
				fileList.add(downloadDocumentRecord(documentRecord.getDocumentId()));
			}
			else {
				throw new ValidationException("Cannot find document associated with " + (documentRecord != null ? documentRecord.getName() : "ID " + fkFieldId));
			}
		}
		String fileNameAndPath = FileUtils.combinePath(FileUtils.JAVA_TEMP_DIRECTORY, fileName + ".zip");
		FilePath file = FilePath.forPath(ZipUtils.zipFiles(fileList, fileNameAndPath).getPath());
		return new FileWrapper(file, FileUtils.getFileName(file.getPath()), true);
	}


	@Override
	public String getDocumentRecordWebDavUrl(String documentId) {
		DocumentRecord documentRecord = getDocumentRecordByDocumentId(documentId);
		if (StringUtils.isEmpty(documentRecord.getFolderName())) {
			documentRecord.setFolderName(getDocumentSetupService().getDocumentFolderStructure(documentRecord.getTableName(), documentRecord.getFkFieldId()));
		}
		return getDocumentManagementProvider().getDocumentWebdavUrl(documentRecord);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                   Document Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DocumentRecord getDocumentRecord(String tableName, long fkFieldId) {
		DocumentRecord record = null;
		try {
			record = getDocumentRecordForDocument(getDocumentByTableAndFkField(tableName, fkFieldId));
		}
		catch (Exception e) {
			handleDocumentException(e, "Error retrieving document record from system");
		}
		return record;
	}


	@Override
	public DocumentRecord getDocumentRecordByDocumentId(String documentId) {
		return getDocumentRecordForDocument(getDocument(documentId));
	}


	/**
	 * Admin only method that can be used in RARE cases where we need to "move" the file to new properties Assumes file location remains in same folder and can
	 * be used for cases where a table is renamed or for something like issue for SystemNotes were a note had single note attachment and should have been
	 * multiple
	 */
	@Override
	public String moveDocumentRecord(String fromTableName, long fromFkFieldId, String toTableName, long toFkFieldId) {
		// First Validates that From Exists
		T fromDoc = getDocumentByTableAndFkField(fromTableName, fromFkFieldId);
		if (fromDoc == null) {
			throw new ValidationException("Cannot find document for table: " + fromTableName + " and fk field id: " + fromFkFieldId);
		}

		// Then Validates that To Does Not Exist
		T toDoc = getDocumentByTableAndFkField(toTableName, toFkFieldId);
		if (toDoc != null) {
			throw new ValidationException("Document already exists for table: " + toTableName + " and fk field id: " + toFkFieldId);
		}
		DocumentRecord fromDocumentRecord = getDocumentRecordForDocument(fromDoc);
		String documentName = getDocumentSetupService().getDocumentName(fromDocumentRecord);
		getDocumentManagementProvider().updateDocumentProperties(fromDoc, toTableName, toFkFieldId);
		getDocumentIdCache().clearDocumentId(fromTableName, fromFkFieldId);
		getDocumentIdCache().clearDocumentId(toTableName, toFkFieldId);
		return documentName;
	}


	@Override
	public void copyDocumentRecord(String fromTableName, long fromFkFieldId, String toTableName, long toFkFieldId) {
		// First Validates that From Exists
		T fromDoc = getDocumentByTableAndFkField(fromTableName, fromFkFieldId);

		// Then Validates that To Does Not Exist
		T toDoc = getDocumentByTableAndFkField(toTableName, toFkFieldId);

		if (fromDoc != null && toDoc == null) {
			try {
				DocumentRecord fromDocRec = getDocumentRecordForDocument(fromDoc);

				// Validates that the from is not currently checked out
				if (fromDocRec.isVersionSeriesCheckedOut()) {
					throw new ValidationException("Unable to copy document [" + fromDocRec.getName() + "].  It is currently checked out by [" + fromDocRec.getCheckedOutUser() + "].");
				}

				String folderName = getDocumentSetupService().getDocumentFolderStructure(toTableName, toFkFieldId);

				DocumentRecord toDocRec = new DocumentRecord();
				toDocRec.setTableName(toTableName);
				toDocRec.setFkFieldId(toFkFieldId);
				String documentName = getDocumentSetupService().getDocumentName(toDocRec);
				toDocRec.setName(documentName + "." + FileUtils.getFileExtension(fromDocRec.getName()));
				toDocRec.setFolderName(folderName);

				T document = getDocumentManagementProvider().copyDocument(getDocumentRecordForDocument(fromDoc), toDocRec);
				saveDocumentAwareProperties(getDocumentRecordForDocument(document));
			}
			catch (Exception e) {
				handleDocumentException(e, "Error copying document record within system");
			}
		}
	}


	@Override
	public DocumentRecord checkoutDocumentRecord(String documentId, String tableName, long fkFieldId) {
		// Validate the Document Can be Checked Out based on Setup's DocumentModifyCondition
		getDocumentSetupService().validateDocumentModifyCondition(tableName, fkFieldId, "checkout");
		DocumentRecord record = null;
		try {
			T document = getDocumentManagementProvider().getDocument(documentId); //getDocumentByTableAndFkField(tableName, fkFieldId);
			AssertUtils.assertNotNull(document, "Document To Check Out Is Missing");
			DocumentRecord dr = getDocumentRecordForDocument(document);
			if (dr.isVersionSeriesCheckedOut()) {
				throw new ValidationException("Document is currently checked out by [" + dr.getCheckedOutUser() + "].");
			}
			document = getDocumentManagementProvider().checkoutDocument(document);
			record = getDocumentRecordForDocument(document);
			AssertUtils.assertTrue(record.isVersionSeriesCheckedOut(), "Document should be checked out");
			AssertUtils.assertFalse(StringUtils.isEmpty(record.getCheckedOutUser()), "Document Check out User should be set");
			saveDocumentAwareProperties(record);
		}
		catch (Exception e) {
			handleDocumentException(e, "Error during checkout of document");
		}
		return record;
	}


	@Override
	public DocumentRecord cancelCheckoutDocumentRecord(String documentId) {
		DocumentRecord record = null;
		try {
			T document = getDocument(documentId);
			document = getDocumentManagementProvider().cancelCheckoutDocument(document);
			record = getDocumentRecordForDocument(document);
			AssertUtils.assertFalse(record.isVersionSeriesCheckedOut(), "Document should not be checked out");
			AssertUtils.assertTrue(StringUtils.isEmpty(record.getCheckedOutUser()), "Document Check out User should not be set");
			saveDocumentAwareProperties(record);
		}
		catch (Exception e) {
			handleDocumentException(e, "Error cancelling checkout of document");
		}
		return record;
	}


	@Override
	public DocumentRecord checkinDocumentRecord(DocumentRecord documentRecord) {
		// First Need to Validate the Check In Comment And Revision Type
		DocumentDefinition def = getDocumentSetupService().getDocumentDefinitionForEntity(documentRecord.getTableName(), documentRecord.getFkFieldId());
		DocumentRecord record = null;
		if (!def.isMajorRevisionSupported() || !def.isMinorRevisionSupported()) {
			documentRecord.setMajorVersion(def.isMajorRevisionSupported());
		}
		if (StringUtils.isEmpty(documentRecord.getCheckinComment())) {
			if ((documentRecord.isMajorVersion() && def.isMajorCommentRequired()) || (!documentRecord.isMajorVersion() && def.isMinorCommentRequired())) {
				throw new FieldValidationException("Check-in comment is required for " + (documentRecord.isMajorVersion() ? " major " : " minor ") + "revisions", "checkInComment");
			}
		}

		try {
			T document = getDocumentManagementProvider().checkinDocument(documentRecord);
			record = getDocumentRecordForDocument(document);
			AssertUtils.assertFalse(record.isVersionSeriesCheckedOut(), "Document should not be checked out");
			AssertUtils.assertTrue(StringUtils.isEmpty(record.getCheckedOutUser()), "Document Check out User should not be set");
			saveDocumentAwareProperties(record);
		}
		catch (Exception e) {
			handleDocumentException(e, "Error during document checkin");
		}
		return record;
	}


	@Override
	public DocumentRecord uploadDocumentRecord(DocumentRecord document) {
		// Validate the Document Can be Checked Out based on Setup's DocumentModifyCondition
		getDocumentSetupService().validateDocumentModifyCondition(document.getTableName(), document.getFkFieldId(), "upload");
		return uploadDocumentRecordImpl(document);
	}


	private DocumentRecord uploadDocumentRecordImpl(DocumentRecord document) {
		MultipartFile thisFile = document.getFile();
		String uploadFileExtension = FileUtils.getFileExtension(thisFile.getOriginalFilename());

		try {
			// Uploading Changes to a Checked Out Version
			if (!StringUtils.isEmpty(document.getDocumentId())) {
				T existingDoc = getDocument(document.getDocumentId());

				// Verify the File Extension is the Same - Allow doc and docx to be considered equal
				DocumentRecord existingDocRec = getDocumentRecordForDocument(existingDoc);
				String existingFileExtension = existingDocRec.getFileExtension();
				boolean changeExtension = false;
				if (!StringUtils.isEmpty(existingFileExtension)) {
					if (!existingFileExtension.equalsIgnoreCase(uploadFileExtension)) {
						if (!FILE_EXTENSION_EQUAL_MAP.containsKey(existingFileExtension) || !uploadFileExtension.equalsIgnoreCase(FILE_EXTENSION_EQUAL_MAP.get(existingFileExtension))) {
							throw new ValidationException("The original file for this entity was a [" + existingFileExtension + "].  You cannot replace it with a [" + uploadFileExtension + "]");
						}
						changeExtension = true;
					}
				}
				document = getDocumentRecordForDocument(getDocumentManagementProvider().updateDocument(document, (changeExtension ? uploadFileExtension : null)));
			}
			// Uploading a New document for an entity
			else {
				String folderName = getDocumentSetupService().getDocumentFolderStructure(document.getTableName(), document.getFkFieldId());
				String documentName = getDocumentSetupService().getDocumentName(document) + "." + uploadFileExtension;
				document.setFolderName(folderName);
				document.setName(documentName);
				document = getDocumentRecordForDocument(getDocumentManagementProvider().createDocument(document));
			}
		}
		catch (Exception e) {
			handleDocumentException(e, "Error during upload of document");
		}
		saveDocumentAwareProperties(document);//
		return document;
	}


	/**
	 * NOTE CANNOT WRAP IN A TRANSACTION BECAUSE IF UPLOAD FAILS THE DATABASE WILL BE LOCKED
	 */
	@Override
	public void uploadDocumentFile(DocumentFile bean) {
		ValidationUtils.assertTrue(bean.isNewBean(), "Uploading Files and immediately creating the Document File object is supported only for new Document File objects only.");

		// Not sure why - possible because it's a file upload but the definition object itself isn't getting fully populated
		// id is there so just pull from db again
		bean.setDefinition(getDocumentSetupService().getDocumentDefinition(bean.getDefinition().getId()));

		MultipartFile thisFile = bean.getFile();

		// Validate the Document Can be Checked Out based on Setup's DocumentModifyCondition
		getDocumentSetupService().validateDocumentModifyConditionForDocumentFile(bean, "upload");

		// CREATE DOCUMENT FILE RECORD
		if (StringUtils.isEmpty(bean.getName())) {
			bean.setName(FileUtils.getFileNameWithoutExtension(thisFile.getOriginalFilename()));
		}
		bean.setFileSize(thisFile.getSize());
		getDocumentSetupService().saveDocumentFile(bean);

		try {
			// UPLOAD DOCUMENT ASSIGNED TO NEW DOCUMENT FILE ROW
			DocumentRecord document = new DocumentRecord();
			document.setTableName(bean.getDefinition().getFileSystemTableName());
			document.setFkFieldId(bean.getId());
			document.setFile(thisFile);
			uploadDocumentRecordImpl(document);
		}
		catch (Exception e) {
			// If upload fails - delete the record from the database
			getDocumentSetupService().deleteDocumentFile(bean.getId());
		}
	}


	/**
	 * Copies the list of {@link DocumentFile} associated with the source to the target.  For each DocumentFile will also
	 * copy the actual file associated with it to the new DocumentFile.
	 * <p>
	 * Should be used rarely - usually we can link an attachment to multiple records.
	 * Currently used for BillingInvoice attachments when the invoice is voided and regenerated, we copy that attachments so we have history and can keep/remove/or add attachments to the invoice without affecting history
	 */
	@Override
	@Transactional
	public void copyDocumentFileList(String tableName, Long sourceFkFieldId, Long targetFkFieldId) {
		List<DocumentFile> documentFileList = getDocumentSetupService().getDocumentFileListForEntity(tableName, sourceFkFieldId);
		if (!CollectionUtils.isEmpty(documentFileList)) {
			for (DocumentFile documentFile : documentFileList) {
				DocumentFile newDocumentFile = BeanUtils.cloneBean(documentFile, false, false);
				newDocumentFile.setFkFieldId(targetFkFieldId);
				getDocumentSetupService().saveDocumentFile(newDocumentFile);
				copyDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, documentFile.getId(), DocumentFile.DOCUMENT_FILE_TABLE_NAME, newDocumentFile.getId());
			}
		}
	}


	@Override
	public void deleteDocumentRecord(String documentId, String tableName, long fkFieldId, boolean doNotValidateModifyCondition) {
		try {
			if (!doNotValidateModifyCondition) {
				getDocumentSetupService().validateDocumentModifyCondition(tableName, fkFieldId, "delete");
			}
			getDocumentManagementProvider().deleteDocument(getDocumentManagementProvider().getDocument(documentId));
			// If skipping modify condition validation, then we are actually deleting the bean for this document - so no need to update it to clear the properties if it will just be deleted anyway
			if (!doNotValidateModifyCondition) {
				clearDocumentAwareProperties(tableName, fkFieldId);
			}
		}
		catch (Exception e) {
			handleDocumentException(e, "Error during document deletion");
		}
	}


	@Override
	public DocumentRecord restoreDocumentRecordVersion(DocumentRecord documentRecord) {

		//First get the latest version from the version list
		T documentToRestore = getDocumentByTableAndFkField(documentRecord.getTableName(), documentRecord.getFkFieldId());
		List<T> versionList = getDocumentManagementProvider().getDocumentVersionList(documentToRestore);
		DocumentRecord latestVersion = getDocumentRecordForDocument(CollectionUtils.getFirstElementStrict(versionList));

		ValidationUtils.assertNotNull(latestVersion, "Unable to restore document.  Failed to find the latest version.");

		// Now Try to Check It Out
		latestVersion = checkoutDocumentRecord(latestVersion.getDocumentId(), latestVersion.getTableName(), latestVersion.getFkFieldId());
		latestVersion = getDocumentRecordByDocumentId(latestVersion.getVersionSeriesCheckedOutId());

		// Then Get Version's File
		DocumentRecord restoreVersion = getDocumentRecordByDocumentId(documentRecord.getDocumentId());

		// Update Document Content
		getDocumentManagementProvider().copyDocumentContentStream(restoreVersion, latestVersion);

		// Set Comments, & Major/Minor
		latestVersion.setCheckinComment(documentRecord.getCheckinComment());
		latestVersion.setMajorVersion(documentRecord.isMajorVersion());

		// Check In
		return checkinDocumentRecord(latestVersion);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                 Document Version Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<DocumentRecord> getDocumentRecordVersionList(String tableName, long fkFieldId) {
		List<DocumentRecord> returnDocs = new ArrayList<>();
		try {
			T document = getDocumentByTableAndFkField(tableName, fkFieldId);
			List<T> versionList = getDocumentManagementProvider().getDocumentVersionList(document);
			for (T docVersion : CollectionUtils.getIterable(versionList)) {
				returnDocs.add(getDocumentRecordForDocument(docVersion));
			}
		}
		catch (Exception e) {
			handleDocumentException(e, "Error during document version retrieval");
		}
		return returnDocs;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////              Document Aware Update Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Updates the file type, document update user/update date, and file size properties (based on which interface the entity uses)
	 */
	private void saveDocumentAwareProperties(DocumentRecord record) {
		UpdatableDAO<IdentityObject> dao = (UpdatableDAO<IdentityObject>) getDaoLocator().locate(record.getTableName());
		boolean update = false;
		// Document File Type - applies to any DocumentAware
		if (DocumentAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
			DocumentAware bean = (DocumentAware) dao.findByPrimaryKey(record.getFkFieldId());
			if (StringUtils.isEmpty(bean.getDocumentFileType()) || !bean.getDocumentFileType().equals(record.getFileExtension())) {
				update = true;
				bean.setDocumentFileType(record.getFileExtension());
			}
			if (DocumentWithRecordStampAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
				update = true;
				((DocumentWithRecordStampAware) bean).setDocumentUpdateDate(record.getUpdateDate());
				((DocumentWithRecordStampAware) bean).setDocumentUpdateUser(record.getUpdateUser());
			}
			if (DocumentWithFileSizeAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
				if (!MathUtils.isEqual(record.getFileSize(), ((DocumentWithFileSizeAware) bean).getFileSize())) {
					update = true;
					((DocumentWithFileSizeAware) bean).setFileSize(record.getFileSize());
				}
			}
			if (update) {
				//Store the originally cached id.
				String documentId = getDocumentIdCache().getDocumentId(record.getTableName(), record.getFkFieldId());
				try {
					//Update the cache with the new record id so any observers will have the correct id.
					getDocumentIdCache().setDocumentId(record.getTableName(), record.getFkFieldId(), record.getDocumentId());
					dao.save(bean);
				}
				catch (Exception e) {
					//reset the id in the cache if something in the observers failed.
					getDocumentIdCache().setDocumentId(record.getTableName(), record.getFkFieldId(), documentId);
					throw new RuntimeException("Failed to save document properties: " + e.getMessage());
				}
			}
		}
		else {
			getDocumentIdCache().setDocumentId(record.getTableName(), record.getFkFieldId(), record.getDocumentId());
		}
	}


	/**
	 * Clears the file type, document update user/update date, and file size properties (based on which interface the entity uses)
	 */
	private void clearDocumentAwareProperties(String tableName, Long fkFieldId) {
		UpdatableDAO<IdentityObject> dao = (UpdatableDAO<IdentityObject>) getDaoLocator().locate(tableName);
		boolean update = false;
		if (DocumentAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
			DocumentAware bean = (DocumentAware) dao.findByPrimaryKey(fkFieldId);
			if (!StringUtils.isEmpty(bean.getDocumentFileType())) {
				update = true;
				bean.setDocumentFileType(null);
			}
			if (DocumentWithRecordStampAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
				update = true;
				((DocumentWithRecordStampAware) bean).setDocumentUpdateDate(null);
				((DocumentWithRecordStampAware) bean).setDocumentUpdateUser(null);
			}
			if (DocumentWithFileSizeAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
				update = true;
				((DocumentWithFileSizeAware) bean).setFileSize(null);
			}
			if (update) {
				DaoUtils.executeWithAllObserversDisabled(() -> dao.save(bean));
			}
		}
		//Now that the save has succeeded clear the document id cache
		getDocumentIdCache().clearDocumentId(tableName, fkFieldId);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Document Upload Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void uploadDocumentFileUpload(DocumentFileUpload documentFileUpload) {
		// validate input
		DocumentDefinition documentDefinition = getDocumentSetupService().getDocumentDefinition(documentFileUpload.getDefinitionId());
		ValidationUtils.assertNotNull(documentDefinition, "No Document Definition found.");

		FileDuplicateActions fileAction = documentFileUpload.getFileAction();
		ValidationUtils.assertNotNull(fileAction, "File action is required.");

		// retrieve locator bean and retrieve the foreign key field id for the DocumentFile
		SystemBean uploadLocatorBean = documentDefinition.getFileBulkUploadFkFieldIdLocatorBean();
		ValidationUtils.assertNotNull(uploadLocatorBean, String.format("Bulk Upload is not supported for [%s]", documentDefinition.getName()));

		Long documentFileFkFieldId = ((DocumentFileFkFieldLocator) getSystemBeanService().getBeanInstance(uploadLocatorBean)).getDocumentFileFkFieldIdFromFileName(documentFileUpload.getFile().getOriginalFilename());

		// process the file - use the appropriate file action handler and return the document to be saved (if any)
		processFileDuplicateAction(documentFileUpload, documentDefinition, documentFileFkFieldId);
	}


	protected void processFileDuplicateAction(DocumentFileUpload documentFileUpload, DocumentDefinition documentDefinition, Long documentFileFkFieldId) {
		// check if the file exists and handle appropriately
		DocumentFile documentFile = findDocumentFileExists(documentFileUpload, documentFileFkFieldId);
		if (documentFile != null) {
			switch (documentFileUpload.getFileAction()) {
				case ERROR:
					ValidationUtils.fail("Error - File [%s] already exists", documentFileUpload.getFile() != null ? documentFileUpload.getFile().getOriginalFilename() : StringUtils.EMPTY_STRING);
					break;
				case SKIP:
					// do nothing, skip this file
					break;
				case REPLACE:
					replaceDocumentFile(documentFileUpload, documentFile);
					break;
				default:
					ValidationUtils.fail("File Action [%s] is not supported.", documentFileUpload.getFileAction());
					break;
			}
		}
		else {
			documentFile = new DocumentFile();
			String fileName = documentFileUpload.getFile().getOriginalFilename();
			MultipartFile file = documentFileUpload.getFile();
			documentFile.setDefinition(documentDefinition);
			documentFile.setFile(file);
			documentFile.setFkFieldId(documentFileFkFieldId);
			documentFile.setName(FileUtils.getFileNameWithoutExtension(fileName));
			documentFile.setDocumentFileType(FileUtils.getFileExtension(fileName));
			documentFile.setFileSize(file.getSize());
			documentFile.setDocumentUpdateUser(getSecurityUserService().getSecurityUserCurrent().getUserName());
			documentFile.setDocumentUpdateDate(new Date());
			this.uploadDocumentFile(documentFile);
		}
	}


	protected void replaceDocumentFile(DocumentFileUpload documentFileUpload, DocumentFile documentFile) {
		Date updateDate = new Date();
		String updateUser = getSecurityUserService().getSecurityUserCurrent().getUserName();

		documentFile.setFile(documentFileUpload.getFile());
		documentFile.setFileSize(documentFileUpload.getFile().getSize());
		documentFile.setDocumentUpdateUser(updateUser);
		documentFile.setDocumentUpdateDate(updateDate);

		String fileSystemTableName = documentFile.getDefinition().getFileSystemTableName();
		Integer documentFileId = documentFile.getId();
		String documentId = getDocumentIdCache().getDocumentId(fileSystemTableName, documentFileId);
		DocumentRecord documentRecord = checkoutDocumentRecord(documentId, fileSystemTableName, documentFileId);
		ValidationUtils.assertNotNull(documentRecord, String.format("Could not checkout document record for file: %s", documentFile.getFile().getOriginalFilename()));

		documentRecord.setFile(documentFileUpload.getFile());
		documentRecord.setFileSize(documentFileUpload.getFile().getSize());
		documentRecord.setUpdateUser(updateUser);
		documentRecord.setUpdateDate(updateDate);

		checkinDocumentRecord(documentRecord);
	}


	/**
	 * Finds if document exists based on
	 * (1) definition id
	 * (2) foreign key field id
	 * (3) file name
	 * (4) file extension/type
	 *
	 * @param documentFileUpload
	 * @param documentFileFkFieldId
	 */
	protected DocumentFile findDocumentFileExists(DocumentFileUpload documentFileUpload, Long documentFileFkFieldId) {
		DocumentFileSearchForm searchForm = new DocumentFileSearchForm();
		searchForm.setDefinitionId(documentFileUpload.getDefinitionId());
		searchForm.setFkFieldId(documentFileFkFieldId);
		MultipartFile file = documentFileUpload.getFile();
		String fileName = file.getOriginalFilename();
		searchForm.setName(FileUtils.getFileNameWithoutExtension(fileName));
		searchForm.setDocumentFileType(FileUtils.getFileExtension(fileName));
		return CollectionUtils.getOnlyElement(getDocumentSetupService().getDocumentFileList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                    Helper Methods                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	private T getDocument(String documentId) {
		return getDocumentManagementProvider().getDocument(documentId);
	}


	private T getDocumentByTableAndFkField(String tableName, long fkFieldId) {
		T document;
		String documentId = getDocumentIdCache().getDocumentId(tableName, fkFieldId);
		if (documentId != null) {
			document = getDocument(documentId);
		}
		else {
			document = getDocumentManagementProvider().getDocumentByTableAndFkField(tableName, fkFieldId);
			if (document != null) {
				getDocumentIdCache().setDocumentId(tableName, fkFieldId, getDocumentRecordForDocument(document).getDocumentId());
			}
		}
		return document;
	}


	private DocumentRecord getDocumentRecordForDocument(T document) {
		return getDocumentToDocumentRecordConverter().convert(document);
	}


	private void handleDocumentException(Exception e, String userFriendlyMessage) {
		// If not a ValidationException - Log the Error
		userFriendlyMessage = userFriendlyMessage + " [" + (e == null ? null : e.getMessage()) + "]";
		//TODO: should not be logging AND throwing
		LogUtils.errorOrInfo(getClass(), userFriendlyMessage, e);
		throw new ValidationException(userFriendlyMessage, e);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentIdCache getDocumentIdCache() {
		return this.documentIdCache;
	}


	public void setDocumentIdCache(DocumentIdCache documentIdCache) {
		this.documentIdCache = documentIdCache;
	}


	public DocumentToDocumentRecordConverter<T> getDocumentToDocumentRecordConverter() {
		return this.documentToDocumentRecordConverter;
	}


	public void setDocumentToDocumentRecordConverter(DocumentToDocumentRecordConverter<T> documentToDocumentRecordConverter) {
		this.documentToDocumentRecordConverter = documentToDocumentRecordConverter;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}


	public DocumentManagementProvider<T> getDocumentManagementProvider() {
		return this.documentManagementProvider;
	}


	public void setDocumentManagementProvider(DocumentManagementProvider<T> documentManagementProvider) {
		if (documentManagementProvider != null) {
			this.documentManagementProvider = documentManagementProvider;
		}
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
