package com.clifton.document.setup;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.search.DocumentDefinitionSearchForm;
import com.clifton.document.setup.search.DocumentFileSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>DocumentSetupService</code> defines the methods for
 * setting up {@link DocumentDefinition}s.
 *
 * @author manderson
 */
public interface DocumentSetupService {

	////////////////////////////////////////////////////////////////////////////////
	//////////////          Document Definition Methods             ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentDefinition getDocumentDefinition(short id);


	public DocumentDefinition getDocumentDefinitionByName(String name);


	/**
	 * Returns the DocumentDefinition used for the given tableName/fkFieldId.  If tableName = DocumentFile
	 * looks up DocumentDefinition on the DocumentFile Object referenced by the fkFieldId, otherwise
	 * return the only definition assigned to the table as 1:1
	 */
	public DocumentDefinition getDocumentDefinitionForEntity(String tableName, Long fkFieldId);


	public List<DocumentDefinition> getDocumentDefinitionList(DocumentDefinitionSearchForm searchForm);


	public DocumentDefinition saveDocumentDefinition(DocumentDefinition bean);


	public void deleteDocumentDefinition(short id);


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Document File Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentFile getDocumentFile(int id);


	public List<DocumentFile> getDocumentFileList(DocumentFileSearchForm searchForm);


	public List<DocumentFile> getDocumentFileListForEntity(String tableName, Long fkFieldId);


	public DocumentFile saveDocumentFile(DocumentFile bean);


	public void deleteDocumentFile(int id);


	public void deleteDocumentFileListForEntity(String tableName, Long fkFieldId);


	////////////////////////////////////////////////////////////////////////////////
	///////////////              Document Methods                 //////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a sample DocumentName to be used for a {@link DocumentDefinition}
	 * <p>
	 * If no entities exist to validate against, will validate against an empty bean to test syntax, however
	 * will return "Syntax evaluated OK, however no [" + definition.getTable().getName() + "] entities exist in the system to generate an actual sample."
	 * <p>
	 * NOTE: Same "sample" method for looking at samples of document names and document version names
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentNameSample(short id, String documentName);


	/**
	 * Returns the DocumentName to be used for a particular entity.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentName(DocumentRecord record);


	/**
	 * Returns the DocumentVersionName to be used for a particular document record
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentVersionName(DocumentRecord record);


	/**
	 * Returns a sample DefaultComment to be used for a {@link DocumentDefinition}
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentDefaultCommentSample(short id, String defaultComment);


	/**
	 * Returns the DefaultComment to be used for a particular entity
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentDefaultComment(String tableName, long fkFieldId);


	/**
	 * Returns the folderStructure field for a {@link DocumentDefinition} determined
	 * by the tableName and fkFieldId parameter.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = DocumentDefinition.class)
	public String getDocumentFolderStructure(String tableName, Long fkFieldId);


	/**
	 * Determines the definition for the table/fkField id and evaluates view permissions
	 * <p>
	 * Evaluation is done in the following order:
	 * 1.  If explicit entries in DocumentSecurityRestriction - uses that
	 * 2.  Else If View condition on the definition is defined, uses that
	 * 3.  Else - user has read access to the table defined on the definition
	 *
	 * @throws ValidationException if current user doesn't have read access
	 */
	@DoNotAddRequestMapping
	public void validateDocumentViewCondition(String tableName, Long fkFieldId);


	/**
	 * If the entity associated with the document implements DocumentModifyConditionAware to see if there is a modify condition override
	 * If none, or the entity isn't DocumentModifyConditionAware, will use the modify condition on the definition
	 *
	 * @throws ValidationException if current user doesn't have modify access
	 */
	@DoNotAddRequestMapping
	public void validateDocumentModifyConditionForDocumentFile(DocumentFile documentFile, String action);


	/**
	 * Determines the definition for the table/fkField id and evaluates the modify condition if exists
	 *
	 * @throws ValidationException if current user doesn't have modify access
	 */
	@DoNotAddRequestMapping
	public void validateDocumentModifyCondition(String tableName, Long fkFieldId, String action);


	////////////////////////////////////////////////////////////////////////////////
	////////////          Document Security Restriction Methods       //////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<DocumentSecurityRestriction> getDocumentSecurityRestrictionListForDocument(String tableName, Integer fkFieldId);


	@SecureMethod(dtoClass = DocumentSecurityRestriction.class, permissions = SecurityPermission.PERMISSION_READ)
	public void linkDocumentSecurityRestrictionToSecurityUser(String tableName, Integer fkFieldId, Short securityUserId);


	@SecureMethod(dtoClass = DocumentSecurityRestriction.class, permissions = SecurityPermission.PERMISSION_READ)
	public void linkDocumentSecurityRestrictionToSecurityGroup(String tableName, Integer fkFieldId, Short securityGroupId);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_READ)
	public void deleteDocumentSecurityRestriction(int id);


	public void deleteDocumentSecurityRestrictionListForDocument(String tableName, Integer fkFieldId);
}
