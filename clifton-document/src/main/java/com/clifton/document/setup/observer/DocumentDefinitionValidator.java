package com.clifton.document.setup.observer;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.document.setup.DocumentDefinition;
import com.clifton.document.setup.DocumentSetupService;
import org.springframework.stereotype.Component;


/**
 * The <code>DocumentDefinitionValidator</code> validates changes to {@link DocumentDefinition}s
 *
 * @author manderson
 */
@Component
public class DocumentDefinitionValidator extends SelfRegisteringDaoValidator<DocumentDefinition> {

	private DocumentSetupService documentSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(DocumentDefinition bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("Creating new Document Definitions is not supported at this time.");
		}

		if (config.isDelete()) {
			throw new ValidationException("Deleting Document Definitions is not supported at this time.");
		}
		else if (config.isUpdate()) {
			DocumentDefinition originalBean = getOriginalBean(bean);
			if (!originalBean.getTable().equals(bean.getTable())) {
				throw new FieldValidationException("You cannot change the table for a document definition", "table.id");
			}
			if (!originalBean.getName().equals(bean.getName())) {
				throw new FieldValidationException("Definition names cannot be changed", "name");
			}

			if (bean.isOneDocumentPerEntity() != originalBean.isOneDocumentPerEntity()) {
				throw new FieldValidationException("At this time, you cannot change an definition from One Document Per Entity to not one document per entity and vice versa.", "oneDocumentPerEntity");
			}
			// NOTE: IF EVER RELEASE THE ABOVE RESTRICTION NEED TO VALIDATE UX ON ONE PER ENTITY = TRUE AND TABLE NAME

			// Validate Freemarker Syntax for DocumentName only if it has changed
			if (!originalBean.getDocumentName().equals(bean.getDocumentName())) {
				try {
					getDocumentSetupService().getDocumentNameSample(bean.getId(), bean.getDocumentName());
				}
				catch (Throwable e) {
					throw new FieldValidationException("Unable to generate a document name based upon the syntax given: [" + e.getMessage() + "]", "documentName");
				}
			}

			// Validate Freemarker Syntax for DocumentVersionName only if it has changed
			if (!originalBean.getDocumentVersionName().equals(bean.getDocumentVersionName())) {
				try {
					getDocumentSetupService().getDocumentNameSample(bean.getId(), bean.getDocumentVersionName());
				}
				catch (Throwable e) {
					throw new FieldValidationException("Unable to generate a document version name based upon the syntax given: [" + e.getMessage() + "]", "documentVersionName");
				}
			}

			// Validate Freemarker Syntax for Default Comment only if it has changed and is not blank
			if (!StringUtils.isEmpty(bean.getDefaultComment()) && !bean.getDefaultComment().equals(originalBean.getDefaultComment())) {
				try {
					getDocumentSetupService().getDocumentDefaultCommentSample(bean.getId(), bean.getDefaultComment());
				}
				catch (Exception e) {
					throw new FieldValidationException("Unable to generate a default comment based upon the syntax given: [" + e.getMessage() + "]", "defaultComment");
				}
			}

			// Validate at least one type of revision is supported
			if (!bean.isMajorRevisionSupported() && !bean.isMinorRevisionSupported()) {
				throw new ValidationException("You must select at least one revision type (major and/or minor) that is supported.");
			}

			// All Folder Structures should begin with directory /
			if (!bean.getFolderStructure().startsWith("/")) {
				bean.setFolderStructure("/" + bean.getFolderStructure());
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}
}
