package com.clifton.document.setup.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.document.setup.DocumentSecurityRestriction;
import org.springframework.stereotype.Component;


/**
 * The <code>DocumentDefinitionCache</code> class provides caching and retrieval from cache of DocumentDefinition objects
 * by table name.
 *
 * @author manderson
 */
@Component
public class DocumentSecurityRestrictionByDocumentCache extends SelfRegisteringCompositeKeyDaoListCache<DocumentSecurityRestriction, Short, Integer> {


	@Override
	protected String getBeanKey1Property() {
		return "table.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "fkFieldId";
	}


	@Override
	protected Short getBeanKey1Value(DocumentSecurityRestriction bean) {
		if (bean.getTable() != null) {
			return bean.getTable().getId();
		}
		return null;
	}


	@Override
	protected Integer getBeanKey2Value(DocumentSecurityRestriction bean) {
		return bean.getFkFieldId();
	}
}
