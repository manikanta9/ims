package com.clifton.document.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class DocumentDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,label,folderStructure,table.name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String label;

	@SearchField(searchField = "table.id", sortField = "table.name")
	private Short tableId;

	@SearchField(searchFieldPath = "table", searchField = "name")
	private String tableName;

	@SearchField(searchFieldPath = "table", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableNameEquals;

	@SearchField(searchFieldPath = "modifyCondition", searchField = "name")
	private String modifyConditionName;

	@SearchField
	private String folderStructure;

	@SearchField
	private Boolean oneDocumentPerEntity;

	@SearchField
	private Boolean minorRevisionSupported;

	@SearchField
	private Boolean minorCommentRequired;

	@SearchField
	private Boolean majorRevisionSupported;

	@SearchField
	private Boolean majorCommentRequired;

	@SearchField
	private Boolean redLineSupported;

	@SearchField
	private String documentName;

	@SearchField
	private String documentVersionName;

	@SearchField
	private String defaultComment;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getModifyConditionName() {
		return this.modifyConditionName;
	}


	public void setModifyConditionName(String modifyConditionName) {
		this.modifyConditionName = modifyConditionName;
	}


	public String getFolderStructure() {
		return this.folderStructure;
	}


	public void setFolderStructure(String folderStructure) {
		this.folderStructure = folderStructure;
	}


	public Boolean getOneDocumentPerEntity() {
		return this.oneDocumentPerEntity;
	}


	public void setOneDocumentPerEntity(Boolean oneDocumentPerEntity) {
		this.oneDocumentPerEntity = oneDocumentPerEntity;
	}


	public Boolean getMinorRevisionSupported() {
		return this.minorRevisionSupported;
	}


	public void setMinorRevisionSupported(Boolean minorRevisionSupported) {
		this.minorRevisionSupported = minorRevisionSupported;
	}


	public Boolean getMinorCommentRequired() {
		return this.minorCommentRequired;
	}


	public void setMinorCommentRequired(Boolean minorCommentRequired) {
		this.minorCommentRequired = minorCommentRequired;
	}


	public Boolean getMajorRevisionSupported() {
		return this.majorRevisionSupported;
	}


	public void setMajorRevisionSupported(Boolean majorRevisionSupported) {
		this.majorRevisionSupported = majorRevisionSupported;
	}


	public Boolean getMajorCommentRequired() {
		return this.majorCommentRequired;
	}


	public void setMajorCommentRequired(Boolean majorCommentRequired) {
		this.majorCommentRequired = majorCommentRequired;
	}


	public Boolean getRedLineSupported() {
		return this.redLineSupported;
	}


	public void setRedLineSupported(Boolean redLineSupported) {
		this.redLineSupported = redLineSupported;
	}


	public String getDocumentName() {
		return this.documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public String getDocumentVersionName() {
		return this.documentVersionName;
	}


	public void setDocumentVersionName(String documentVersionName) {
		this.documentVersionName = documentVersionName;
	}


	public String getDefaultComment() {
		return this.defaultComment;
	}


	public void setDefaultComment(String defaultComment) {
		this.defaultComment = defaultComment;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getTableNameEquals() {
		return this.tableNameEquals;
	}


	public void setTableNameEquals(String tableNameEquals) {
		this.tableNameEquals = tableNameEquals;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}
}
