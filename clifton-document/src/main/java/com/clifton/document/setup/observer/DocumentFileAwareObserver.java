package com.clifton.document.setup.observer;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentFileAware;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.document.setup.DocumentSetupService;
import org.springframework.stereotype.Component;


/**
 * The <code>DocumentFileAwareObserver</code> deletes all related files when the source DocumentFileAware bean is deleted from the system
 *
 * @author manderson
 */
@Component
public class DocumentFileAwareObserver<T extends DocumentFileAware> extends BaseDaoEventObserver<T> {

	private DocumentSetupService documentSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") T bean) {
		// NOTHING FOR NOW
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, @SuppressWarnings("unused") Throwable e) {
		if (e == null && DaoEventTypes.DELETE == event) {
			getDocumentSetupService().deleteDocumentFileListForEntity(dao.getConfiguration().getTableName(), BeanUtils.getIdentityAsLong(bean));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}
}
