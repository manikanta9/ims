package com.clifton.document.setup;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.search.DocumentDefinitionSearchForm;
import com.clifton.document.setup.search.DocumentFileSearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.document.DocumentModifyConditionAware;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>DocumentSetupServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class DocumentSetupServiceImpl implements DocumentSetupService {

	private AdvancedUpdatableDAO<DocumentDefinition, Criteria> documentDefinitionDAO;
	private AdvancedUpdatableDAO<DocumentFile, Criteria> documentFileDAO;
	private AdvancedUpdatableDAO<DocumentSecurityRestriction, Criteria> documentSecurityRestrictionDAO;

	private DaoNamedEntityCache<DocumentDefinition> documentDefinitionCache;
	private DaoSingleKeyCache<DocumentDefinition, Short> documentDefinitionOnePerTableCache;
	private DaoCompositeKeyListCache<DocumentSecurityRestriction, Short, Integer> documentSecurityRestrictionByDocumentCache;

	private DaoLocator daoLocator;

	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityUserService securityUserService;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSchemaService systemSchemaService;

	private TemplateConverter templateConverter;


	////////////////////////////////////////////////////////////////////////////////
	////////////            Document Definition Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DocumentDefinition getDocumentDefinition(short id) {
		return getDocumentDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public DocumentDefinition getDocumentDefinitionByName(String name) {
		return getDocumentDefinitionCache().getBeanForKeyValueStrict(getDocumentDefinitionDAO(), name);
	}


	private DocumentDefinition getDocumentDefinitionByTableName(String tableName) {
		return getDocumentDefinitionOnePerTableCache().getBeanForKeyValueStrict(getDocumentDefinitionDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId());
	}


	@Override
	public DocumentDefinition getDocumentDefinitionForEntity(String tableName, Long fkFieldId) {
		DocumentDefinition def;
		if (DocumentFile.DOCUMENT_FILE_TABLE_NAME.equals(tableName)) {
			def = getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId)).getDefinition();
		}
		else {
			// LOOK UP THE ONE THAT IS 1:1 FOR THE TABLE
			def = getDocumentDefinitionByTableName(tableName);
		}
		return def;
	}


	@Override
	public List<DocumentDefinition> getDocumentDefinitionList(DocumentDefinitionSearchForm searchForm) {
		return getDocumentDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public DocumentDefinition saveDocumentDefinition(DocumentDefinition bean) {
		return getDocumentDefinitionDAO().save(bean);
	}


	@Override
	public void deleteDocumentDefinition(short id) {
		getDocumentDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Document File Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DocumentFile getDocumentFile(int id) {
		return getDocumentFileDAO().findByPrimaryKey(id);
	}


	@Override
	public List<DocumentFile> getDocumentFileList(DocumentFileSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getDefinitionNameEquals())) {
			searchForm.setDefinitionId(getDocumentDefinitionByName(searchForm.getDefinitionNameEquals()).getId());
			searchForm.setDefinitionNameEquals(null);
		}
		return getDocumentFileDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<DocumentFile> getDocumentFileListForEntity(String tableName, Long fkFieldId) {
		DocumentFileSearchForm searchForm = new DocumentFileSearchForm();
		searchForm.setTableNameEquals(tableName);
		searchForm.setFkFieldId(fkFieldId);

		return getDocumentFileList(searchForm);
	}


	@Override
	@Transactional
	public DocumentFile saveDocumentFile(DocumentFile bean) {
		ValidationUtils.assertNotNull(bean.getName(), "File name is required", "name");
		ValidationUtils.assertTrue(bean.getName().length() <= 100, "File names cannot be over 100 characters in length.", "name");
		return getDocumentFileDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteDocumentFile(int id) {
		getDocumentFileDAO().delete(id);
	}


	@Override
	public void deleteDocumentFileListForEntity(String tableName, Long fkFieldId) {
		ValidationUtils.assertNotNull(tableName, "Table Name is required to delete all files for an entity.");
		ValidationUtils.assertNotNull(fkFieldId, "FKFieldID is required to delete all files for an entity.");
		getDocumentFileDAO().deleteList(getDocumentFileListForEntity(tableName, fkFieldId));
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////////            Document Methods               ////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDocumentName(DocumentRecord record) {
		DocumentDefinition def = getDocumentDefinitionForEntity(record.getTableName(), record.getFkFieldId());
		return getDocumentName(def, record, def.getDocumentName());
	}


	@Override
	public String getDocumentNameSample(short id, String documentName) {
		DocumentDefinition definition = getDocumentDefinition(id);
		DocumentRecord record = new DocumentRecord();
		record.setVersionLabel("1.0");
		record.setCreateDate(new Date());
		record.setCreateUser("SampleUser");
		record.setUpdateDate(new Date());
		record.setUpdateUser("SampleUser");
		return getDocumentName(definition, record, documentName);
	}


	@Override
	public String getDocumentVersionName(DocumentRecord record) {
		DocumentDefinition def = getDocumentDefinitionForEntity(record.getTableName(), record.getFkFieldId());
		return getDocumentName(def, record, def.getDocumentVersionName());
	}


	private String getDocumentName(DocumentDefinition definition, DocumentRecord documentRecord, String template) {
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(definition.getTable().getName());
		Long fkFieldId = (documentRecord.getFkFieldId() == 0 ? null : documentRecord.getFkFieldId());
		DocumentFile fileBean = null;
		if (!definition.isOneDocumentPerEntity()) {
			if (fkFieldId != null) {
				// Set the File Bean and change fkFieldId to the FkFieldID from the DocumentFile to lookup related entity
				fileBean = getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId));
				fkFieldId = fileBean.getFkFieldId();
			}
			else {
				fileBean = new DocumentFile();
			}
		}
		IdentityObject bean;
		boolean missingEntity = false;
		if (fkFieldId == null) {
			// If null - pull the first one - used for verifying the syntax
			bean = CollectionUtils.getFirstElement(dao.findAll());
		}
		else {
			bean = dao.findByPrimaryKey(fkFieldId);
		}
		if (bean == null && fkFieldId == null) {
			// If nothing exists and generating a sample (i.e. fkFieldId is NULL) - just use an empty bean
			bean = dao.newBean();
			missingEntity = true;
		}
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("bean", bean);
		config.addBeanToContext("document", documentRecord);
		if (fileBean != null) {
			config.addBeanToContext("fileBean", fileBean);
		}

		String result = getTemplateConverter().convert(config);
		if (missingEntity) {
			return "Syntax evaluated OK, however no [" + definition.getTable().getName() + "] entities exist in the system to generate an actual sample.";
		}
		return FileUtils.replaceInvalidCharacters(result, "_");
	}


	@Override
	public String getDocumentDefaultCommentSample(short id, String defaultComment) {
		DocumentDefinition definition = getDocumentDefinition(id);
		definition.setDefaultComment(defaultComment);
		return getDocumentDefaultCommentByDefinition(definition, null);
	}


	@Override
	public String getDocumentDefaultComment(String tableName, long fkFieldId) {
		DocumentDefinition def = getDocumentDefinitionForEntity(tableName, fkFieldId);
		return getDocumentDefaultCommentByDefinition(def, fkFieldId);
	}


	private String getDocumentDefaultCommentByDefinition(DocumentDefinition definition, Long fkFieldId) {
		String defaultComment = "";
		// No Default Comment
		if (!StringUtils.isEmpty(definition.getDefaultComment())) {
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(definition.getTable().getName());
			IdentityObject bean;
			boolean missingEntity = false;
			if (fkFieldId == null) {
				// If null - pull the first one - used for verifying the syntax
				bean = CollectionUtils.getFirstElement(dao.findAll());
			}
			else {
				bean = dao.findByPrimaryKey(fkFieldId);
			}
			if (bean == null && fkFieldId == null) {
				// If nothing exists and generating a sample (i.e. fkFieldId is NULL) - just use an empty bean
				bean = dao.newBean();
				missingEntity = true;
			}
			TemplateConfig config = new TemplateConfig(definition.getDefaultComment());
			config.addBeanToContext("bean", bean);
			defaultComment = getTemplateConverter().convert(config);
			if (missingEntity) {
				defaultComment = "Syntax evaluated OK, however no [" + definition.getTable().getName() + "] entities exist in the system to generate an actual sample.";
			}
		}
		return defaultComment;
	}


	@Override
	public String getDocumentFolderStructure(String tableName, Long fkFieldId) {
		DocumentDefinition def = getDocumentDefinitionForEntity(tableName, fkFieldId);
		return def.getFolderStructure();
	}


	private void validateDocumentViewConditionForDocumentFile(DocumentFile documentFile) {
		validateDocumentViewConditionImpl(documentFile.getDefinition(), documentFile.getFkFieldId());
	}


	@Override
	public void validateDocumentViewCondition(String tableName, Long fkFieldId) {
		DocumentDefinition definition = getDocumentDefinitionForEntity(tableName, fkFieldId);

		if (!definition.isOneDocumentPerEntity()) {
			ValidationUtils.assertNotNull(fkFieldId, "Unable to determine document file without document file id passed as the fk field id");
			// Set the File Bean and validate based on that bean
			DocumentFile fileBean = getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId));
			// Check Explicit Security - if False means no explicit security defined and need to check conditions and/or table access
			// Returns true if access is allowed and throws Validation Exception if explicit security and access is denied
			if (!validateDocumentSecurityRestrictionAccessForDocument(DocumentFile.DOCUMENT_FILE_TABLE_NAME, fkFieldId)) {
				validateDocumentViewConditionForDocumentFile(fileBean);
			}
		}
		// Otherwise 1:1 and use direct definition and FKFieldID, i.e. BusinessContract & BusinessContractID
		// Check Explicit Security - if False means no explicit security defined and need to check conditions and/or table access
		// Returns true if access is allowed and throws Validation Exception if explicit security and access is denied
		if (!validateDocumentSecurityRestrictionAccessForDocument(definition.getTable().getName(), fkFieldId)) {
			validateDocumentViewConditionImpl(definition, fkFieldId);
		}
	}


	private void validateDocumentViewConditionImpl(DocumentDefinition definition, Long fkFieldId) {
		SystemCondition viewCondition = definition.getViewCondition();
		if (viewCondition != null) {
			// Get the Bean
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(definition.getTable().getName());
			IdentityObject bean = dao.findByPrimaryKey(fkFieldId);

			String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(viewCondition, bean);
			if (!StringUtils.isEmpty(error)) {
				throw new ValidationException("Unable to view document because: " + error);
			}
		}
		else {
			SystemTable table = definition.getTable();
			if (table.getSecurityResource() == null) {
				throw new ValidationException("There is no security resource assigned to table [" + table.getName() + "].  Unable to determine user's read access to the document.");
			}

			if (!getSecurityAuthorizationService().isSecurityAccessAllowed(table.getSecurityResource().getName(), SecurityPermission.PERMISSION_READ)) {
				throw new ValidationException("Unable to view document because: No Read Access to Table [" + table.getName() + "] using Security Resource [" + table.getSecurityResource().getName()
						+ "].");
			}
		}
	}


	private boolean validateDocumentSecurityRestrictionAccessForDocument(String tableName, Long fkFieldId) {
		List<DocumentSecurityRestriction> restrictionList = getDocumentSecurityRestrictionListForDocument(tableName, MathUtils.getNumberAsInteger(fkFieldId));
		if (!CollectionUtils.isEmpty(restrictionList)) {
			boolean access = false;
			SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
			List<SecurityGroup> groupList = getSecurityUserService().getSecurityGroupListByUser(currentUser.getId());
			for (DocumentSecurityRestriction res : restrictionList) {
				if (res.getSecurityUser() != null) {
					if (res.getSecurityUser().equals(currentUser)) {
						access = true;
						break;
					}
				}
				else {
					for (SecurityGroup sg : CollectionUtils.getIterable(groupList)) {
						if (res.getSecurityGroup().equals(sg)) {
							access = true;
							break;
						}
					}
					if (access) {
						break;
					}
				}
			}
			if (!access) {
				throw new ValidationException(
						"Unable to view document because explicit security has been selected for this document and you are not explicitly granted access or a member of a group with access.");
			}
			return true;
		}
		return false;
	}


	@Override
	public void validateDocumentModifyConditionForDocumentFile(DocumentFile documentFile, String action) {
		validateDocumentModifyConditionImpl(documentFile.getDefinition(), documentFile.getFkFieldId(), action);
	}


	@Override
	public void validateDocumentModifyCondition(String tableName, Long fkFieldId, String action) {
		DocumentDefinition definition = getDocumentDefinitionForEntity(tableName, fkFieldId);

		if (!definition.isOneDocumentPerEntity()) {
			ValidationUtils.assertNotNull(fkFieldId, "Unable to determine document file without document file id passed as the fk field id");
			// Set the File Bean and validate based on that bean
			DocumentFile fileBean = getDocumentFile(MathUtils.getNumberAsInteger(fkFieldId));
			validateDocumentModifyConditionForDocumentFile(fileBean, action);
		}
		// Otherwise 1:1 and use direct definition and FKFieldID, i.e. BusinessContract & BusinessContractID
		validateDocumentModifyConditionImpl(definition, fkFieldId, action);
	}


	private void validateDocumentModifyConditionImpl(DocumentDefinition definition, Long fkFieldId, String action) {
		SystemCondition modifyCondition = null;

		// Get the Bean and check if it has it's own modify condition set
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(definition.getTable().getName());

		IdentityObject bean = dao.findByPrimaryKey(fkFieldId);
		if (bean instanceof DocumentModifyConditionAware) {
			modifyCondition = ((DocumentModifyConditionAware) bean).getDocumentModifyCondition();
		}
		// If no override, or doesn't implement that interface use the default
		if (modifyCondition == null) {
			modifyCondition = definition.getModifyCondition();
		}
		if (modifyCondition != null) {
			String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(modifyCondition, bean);
			if (!StringUtils.isEmpty(error)) {
				throw new ValidationException("Unable to " + action + " document because: " + error);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            Document Security Restriction Methods         ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<DocumentSecurityRestriction> getDocumentSecurityRestrictionListForDocument(String tableName, Integer fkFieldId) {
		return getDocumentSecurityRestrictionByDocumentCache().getBeanListForKeyValues(getDocumentSecurityRestrictionDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId(), MathUtils.getNumberAsInteger(fkFieldId));
	}


	@Override
	public void linkDocumentSecurityRestrictionToSecurityUser(String tableName, Integer fkFieldId, Short securityUserId) {
		validateDocumentSecurityRestrictionModify(tableName, fkFieldId);
		DocumentSecurityRestriction bean = new DocumentSecurityRestriction();
		bean.setTable(getSystemSchemaService().getSystemTableByName(tableName));
		bean.setFkFieldId(fkFieldId);
		bean.setSecurityUser(getSecurityUserService().getSecurityUser(securityUserId));
		saveDocumentSecurityRestriction(bean);
	}


	@Override
	public void linkDocumentSecurityRestrictionToSecurityGroup(String tableName, Integer fkFieldId, Short securityGroupId) {
		validateDocumentSecurityRestrictionModify(tableName, fkFieldId);
		DocumentSecurityRestriction bean = new DocumentSecurityRestriction();
		bean.setTable(getSystemSchemaService().getSystemTableByName(tableName));
		bean.setFkFieldId(fkFieldId);
		bean.setSecurityGroup(getSecurityUserService().getSecurityGroup(securityGroupId));
		saveDocumentSecurityRestriction(bean);
	}


	private void saveDocumentSecurityRestriction(DocumentSecurityRestriction bean) {
		ValidationUtils.assertNotNull(bean.getTable(), "Table is required");
		ValidationUtils.assertNotNull(bean.getFkFieldId(), "FkFieldID is required");
		ValidationUtils.assertMutuallyExclusive("User or Group must be selected, but not both", bean.getSecurityUser(), bean.getSecurityGroup());
		getDocumentSecurityRestrictionDAO().save(bean);
	}


	@Override
	public void deleteDocumentSecurityRestriction(int id) {
		DocumentSecurityRestriction bean = getDocumentSecurityRestrictionDAO().findByPrimaryKey(id);
		if (bean != null) {
			validateDocumentSecurityRestrictionModify(bean.getTable().getName(), bean.getFkFieldId());
			getDocumentSecurityRestrictionDAO().delete(id);
		}
	}


	@Override
	public void deleteDocumentSecurityRestrictionListForDocument(String tableName, Integer fkFieldId) {
		// No security call here, because this is only called when the document itself is being deleted
		getDocumentSecurityRestrictionDAO().deleteList(getDocumentSecurityRestrictionListForDocument(tableName, fkFieldId));
	}


	private void validateDocumentSecurityRestrictionModify(String tableName, Integer fkFieldId) {
		DocumentDefinition definition = getDocumentDefinitionForEntity(tableName, MathUtils.getNumberAsLong(fkFieldId));
		if (definition == null) {
			throw new ValidationException("Unable to determine document definition for table [" + tableName + "] and fkFieldId [" + fkFieldId + "].");
		}
		SystemTable table = definition.getTable();
		if (table.getSecurityResource() == null) {
			throw new ValidationException("There is no security resource assigned to table [" + table.getName() + "].  Unable to determine user's access to document security.");
		}

		if (!getSecurityAuthorizationService().isSecurityAccessAllowed(table.getSecurityResource().getName(), SecurityPermission.PERMISSION_FULL_CONTROL)) {
			throw new ValidationException("Unable to edit security for document because: No Full Control Access to Table [" + table.getName() + "] using Security Resource ["
					+ table.getSecurityResource().getName() + "].");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<DocumentDefinition, Criteria> getDocumentDefinitionDAO() {
		return this.documentDefinitionDAO;
	}


	public void setDocumentDefinitionDAO(AdvancedUpdatableDAO<DocumentDefinition, Criteria> documentDefinitionDAO) {
		this.documentDefinitionDAO = documentDefinitionDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoNamedEntityCache<DocumentDefinition> getDocumentDefinitionCache() {
		return this.documentDefinitionCache;
	}


	public void setDocumentDefinitionCache(DaoNamedEntityCache<DocumentDefinition> documentDefinitionCache) {
		this.documentDefinitionCache = documentDefinitionCache;
	}


	public AdvancedUpdatableDAO<DocumentFile, Criteria> getDocumentFileDAO() {
		return this.documentFileDAO;
	}


	public void setDocumentFileDAO(AdvancedUpdatableDAO<DocumentFile, Criteria> documentFileDAO) {
		this.documentFileDAO = documentFileDAO;
	}


	public DaoSingleKeyCache<DocumentDefinition, Short> getDocumentDefinitionOnePerTableCache() {
		return this.documentDefinitionOnePerTableCache;
	}


	public void setDocumentDefinitionOnePerTableCache(DaoSingleKeyCache<DocumentDefinition, Short> documentDefinitionOnePerTableCache) {
		this.documentDefinitionOnePerTableCache = documentDefinitionOnePerTableCache;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public AdvancedUpdatableDAO<DocumentSecurityRestriction, Criteria> getDocumentSecurityRestrictionDAO() {
		return this.documentSecurityRestrictionDAO;
	}


	public void setDocumentSecurityRestrictionDAO(AdvancedUpdatableDAO<DocumentSecurityRestriction, Criteria> documentSecurityRestrictionDAO) {
		this.documentSecurityRestrictionDAO = documentSecurityRestrictionDAO;
	}


	public DaoCompositeKeyListCache<DocumentSecurityRestriction, Short, Integer> getDocumentSecurityRestrictionByDocumentCache() {
		return this.documentSecurityRestrictionByDocumentCache;
	}


	public void setDocumentSecurityRestrictionByDocumentCache(DaoCompositeKeyListCache<DocumentSecurityRestriction, Short, Integer> documentSecurityRestrictionByDocumentCache) {
		this.documentSecurityRestrictionByDocumentCache = documentSecurityRestrictionByDocumentCache;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
