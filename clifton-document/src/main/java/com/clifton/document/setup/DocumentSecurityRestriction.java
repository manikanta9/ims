package com.clifton.document.setup;


import com.clifton.core.beans.BaseEntity;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>DocumentSecurityRestriction</code> is used to explicitly define a set of users
 * or user groups that have access to the document.
 *
 * @author manderson
 */
public class DocumentSecurityRestriction extends BaseEntity<Integer> {

	/**
	 * Table and FKField ID Define the document in Alfresco (i.e. BusinessContract/BusinessContractID)
	 * For those definitions set up to support multiple documents per entity
	 * The file security is defined on each file, so in those cases the table/fkFieldId would be DocumentFile/DocumentFileID
	 */
	private SystemTable table;

	@SoftLinkField(tableBeanPropertyName = "table")
	private Integer fkFieldId;

	/**
	 * Either a specific User is selected to grant access to the document
	 * OR a specific group is selected
	 */
	private SecurityUser securityUser;
	private SecurityGroup securityGroup;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSecurityLabel() {
		if (getSecurityUser() != null) {
			return "User: " + getSecurityUser().getLabel();
		}
		if (getSecurityGroup() != null) {
			return "Group: " + getSecurityGroup().getLabel();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public SecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(SecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}
}
