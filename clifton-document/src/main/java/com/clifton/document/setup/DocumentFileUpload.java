package com.clifton.document.setup;

import com.clifton.core.dataaccess.file.FileDuplicateActions;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author KellyJ
 */
public class DocumentFileUpload {

	/**
	 * The DocumentDefinition Id so we know where we are uploading.
	 */
	private Short definitionId;

	/**
	 * File that we are uploading
	 */
	private MultipartFile file;

	/**
	 * Used to determine what to do if the file already exists (Skip, Replace, Error Out)
	 */
	private FileDuplicateActions fileAction;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public FileDuplicateActions getFileAction() {
		return this.fileAction;
	}


	public void setFileAction(FileDuplicateActions fileAction) {
		this.fileAction = fileAction;
	}
}
