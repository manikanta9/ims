package com.clifton.document.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;

import java.util.Date;


/**
 * The <code>DocumentFileSearchForm</code> ...
 *
 * @author manderson
 */
public class DocumentFileSearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "definition.name,name")
	private String searchPattern;

	@SearchField(searchFieldPath = "definition.table", searchField = "name")
	private String tableName;

	@SearchField(searchFieldPath = "definition.table", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableNameEquals;

	@SearchField(searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchFieldPath = "definition", searchField = "name")
	private String definitionName;

	// Note: when populated, this is converted to the id search field definitionId
	@SearchField(searchFieldPath = "definition", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String definitionNameEquals;

	@SearchField(searchFieldPath = "definition", searchField = "label,name")
	private String definitionLabel;

	@SearchField
	private Long fkFieldId;

	@SearchField(searchField = "fkFieldId")
	private Long[] fkFieldIds;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String documentFileType;

	@SearchField
	private Long fileSize;

	@SearchField
	private Date documentUpdateDate;

	@SearchField
	private String documentUpdateUser;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getDefinitionLabel() {
		return this.definitionLabel;
	}


	public void setDefinitionLabel(String definitionLabel) {
		this.definitionLabel = definitionLabel;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getDefinitionNameEquals() {
		return this.definitionNameEquals;
	}


	public void setDefinitionNameEquals(String definitionNameEquals) {
		this.definitionNameEquals = definitionNameEquals;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDocumentFileType() {
		return this.documentFileType;
	}


	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public String getTableNameEquals() {
		return this.tableNameEquals;
	}


	public void setTableNameEquals(String tableNameEquals) {
		this.tableNameEquals = tableNameEquals;
	}


	public Long[] getFkFieldIds() {
		return this.fkFieldIds;
	}


	public void setFkFieldIds(Long[] fkFieldIds) {
		this.fkFieldIds = fkFieldIds;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}
}
