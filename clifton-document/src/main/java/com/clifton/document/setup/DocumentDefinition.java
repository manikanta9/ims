package com.clifton.document.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.document.DocumentRecord;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>DocumentDefinition</code> is used to define Documents that are
 * stored in the DocumentManagement system.  Definitions are defined per table
 * at this time.
 *
 * @author manderson
 */
@CacheByName
public class DocumentDefinition extends NamedEntity<Short> {

	private SystemTable table;

	/**
	 * If set, then validation is evaluated for the {@link DocumentAware}
	 * prior to allowing a document checkout.
	 */
	private SystemCondition modifyCondition;

	/**
	 * If set, then validation is evaluated for the {@link DocumentAware}
	 * prior to allowing a document to be downloaded for viewing
	 * <p>
	 * Not used if a specific document has restrictions defined in {@link DocumentSecurityRestriction}
	 * Default condition is Read Access to the table defined in this definition.
	 */
	private SystemCondition viewCondition;


	/**
	 * If set, then bulk upload of document files is supported
	 */
	private SystemBean fileBulkUploadFkFieldIdLocatorBean;


	/**
	 * Folder path for this document definition
	 */
	private String folderStructure;

	/**
	 * If true, then this table supports only one Document per
	 * row. (If false, then can attach multiple documents (See Investment Calendar Events, Billing Invoices))
	 * When multiple documents per entity - {@link DocumentRecord} is used to represent each attachment, so through document management
	 * system it is still one document per entity - because each attachment is linked through one DocumentRecord row in our system.
	 */
	private boolean oneDocumentPerEntity;

	/**
	 * The following fields are used for controlling checkins
	 */
	private boolean minorRevisionSupported;
	private boolean minorCommentRequired;

	private boolean majorRevisionSupported;
	private boolean majorCommentRequired;

	/**
	 * If true, (currently used for Contracts) drives checkbox availability during
	 * check ins - if set, prefixes comment with [RED_LINE]
	 * During document version retrievals, downloads, etc. the comment is parsed and the actual comment
	 * doesn't include this as far as the user is concerned, but the check box is either checked or not
	 */
	private boolean redLineSupported;

	/**
	 * Freemarker Syntax for creating the document name
	 */
	private String documentName;

	/**
	 * Freemarker Syntax for creating the document name when
	 * downloading versions. (Save As... default)
	 */
	private String documentVersionName;

	/**
	 * Freemarker Syntax for defaulting the check in comment
	 */
	private String defaultComment;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * This is the tableName that is used for UX in the file system.
	 * For 1 document per entity we use the actual table name.  However, for multiple documents per entity
	 * the UX is the DocumentFile table and id of the record in our system since there would only be one DocumentFile record
	 * for each file in the external system.
	 */
	public String getFileSystemTableName() {
		if (isOneDocumentPerEntity()) {
			return getTable().getName();
		}
		return DocumentFile.DOCUMENT_FILE_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public String getFolderStructure() {
		return this.folderStructure;
	}


	public void setFolderStructure(String folderStructure) {
		this.folderStructure = folderStructure;
	}


	public boolean isOneDocumentPerEntity() {
		return this.oneDocumentPerEntity;
	}


	public void setOneDocumentPerEntity(boolean oneDocumentPerEntity) {
		this.oneDocumentPerEntity = oneDocumentPerEntity;
	}


	public String getDocumentName() {
		return this.documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public boolean isMinorRevisionSupported() {
		return this.minorRevisionSupported;
	}


	public void setMinorRevisionSupported(boolean isMinorRevisionSupported) {
		this.minorRevisionSupported = isMinorRevisionSupported;
	}


	public boolean isMajorRevisionSupported() {
		return this.majorRevisionSupported;
	}


	public void setMajorRevisionSupported(boolean isMajorRevisionSupported) {
		this.majorRevisionSupported = isMajorRevisionSupported;
	}


	public boolean isMinorCommentRequired() {
		return this.minorCommentRequired;
	}


	public void setMinorCommentRequired(boolean isMinorCommentRequired) {
		this.minorCommentRequired = isMinorCommentRequired;
	}


	public boolean isMajorCommentRequired() {
		return this.majorCommentRequired;
	}


	public void setMajorCommentRequired(boolean isMajorCommentRequired) {
		this.majorCommentRequired = isMajorCommentRequired;
	}


	public SystemCondition getModifyCondition() {
		return this.modifyCondition;
	}


	public void setModifyCondition(SystemCondition modifyCondition) {
		this.modifyCondition = modifyCondition;
	}


	public String getDocumentVersionName() {
		return this.documentVersionName;
	}


	public void setDocumentVersionName(String documentVersionName) {
		this.documentVersionName = documentVersionName;
	}


	public String getDefaultComment() {
		return this.defaultComment;
	}


	public void setDefaultComment(String defaultComment) {
		this.defaultComment = defaultComment;
	}


	public boolean isRedLineSupported() {
		return this.redLineSupported;
	}


	public void setRedLineSupported(boolean redLineSupported) {
		this.redLineSupported = redLineSupported;
	}


	public SystemCondition getViewCondition() {
		return this.viewCondition;
	}


	public void setViewCondition(SystemCondition viewCondition) {
		this.viewCondition = viewCondition;
	}


	public SystemBean getFileBulkUploadFkFieldIdLocatorBean() {
		return this.fileBulkUploadFkFieldIdLocatorBean;
	}


	public void setFileBulkUploadFkFieldIdLocatorBean(SystemBean fileBulkUploadFkFieldIdLocatorBean) {
		this.fileBulkUploadFkFieldIdLocatorBean = fileBulkUploadFkFieldIdLocatorBean;
	}
}
