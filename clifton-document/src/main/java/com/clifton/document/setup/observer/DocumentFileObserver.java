package com.clifton.document.setup.observer;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.beans.document.DocumentFilePropertyAware;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.document.setup.DocumentFile;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The DocumentFileObserver updates file counts on inserts and deletes of DocumentFiles if the entity associated with the DocumentFile through
 * the definition table name implements {@see DocumentFileCountAware}
 *
 * @author manderson
 */
@Component
public class DocumentFileObserver extends SelfRegisteringDaoObserver<DocumentFile> {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<DocumentFile> dao, DaoEventTypes event, DocumentFile bean, Throwable e) {
		if (e == null) {
			UpdatableDAO<IdentityObject> awareDao = (UpdatableDAO<IdentityObject>) getDaoLocator().locate(bean.getDefinition().getTable().getName());
			// Document File Count Aware or Document File Property Aware - Get the Bean
			if (DocumentFileCountAware.class.isAssignableFrom(awareDao.getConfiguration().getBeanClass()) || DocumentFilePropertyAware.class.isAssignableFrom(awareDao.getConfiguration().getBeanClass())) {

				IdentityObject awareBean = awareDao.findByPrimaryKey(bean.getFkFieldId());
				// Note: When the DocumentFileCountAware bean is deleted, each DocumentFile is deleted - in that case, the above find by primary key result is null so the file count is not updated, which is good because the bean is deleted.
				if (awareBean != null) {
					boolean save = false;

					// Update Document File Count
					if (DocumentFileCountAware.class.isAssignableFrom(awareDao.getConfiguration().getBeanClass())) {
						save = isUpdateDocumentFileCountAware((DocumentFileCountAware) awareBean, event);
					}

					if (DocumentFilePropertyAware.class.isAssignableFrom(awareDao.getConfiguration().getBeanClass())) {
						save = isUpdateDocumentFilePropertyAware((DocumentFilePropertyAware) awareBean, dao, bean) || save;
					}

					if (save) {
						awareDao.save(awareBean);
					}
				}
			}
		}
	}


	private boolean isUpdateDocumentFileCountAware(DocumentFileCountAware awareBean, DaoEventTypes event) {
		if (event.isInsert() || event.isDelete()) {
			Short fileCount = (awareBean.getDocumentFileCount() == null ? 0 : awareBean.getDocumentFileCount());
			if (event.isDelete()) {
				fileCount--;
			}
			else {
				fileCount++;
			}
			awareBean.setDocumentFileCount(fileCount);
			return true;
		}
		return false;
	}


	private boolean isUpdateDocumentFilePropertyAware(DocumentFilePropertyAware awareBean, ReadOnlyDAO<DocumentFile> dao, DocumentFile bean) {
		List<DocumentFile> fileList = dao.findByFields(new String[]{"definition.id", "fkFieldId"}, new Object[]{bean.getDefinition().getId(), bean.getFkFieldId()});
		if (CollectionUtils.getSize(fileList) != 1) {
			if (!StringUtils.isEmpty(awareBean.getDocumentFileType())) {
				awareBean.setDocumentFileType(null);
				return true;
			}
		}
		else if (!StringUtils.isEqualIgnoreCase(fileList.get(0).getDocumentFileType(), awareBean.getDocumentFileType())) {
			awareBean.setDocumentFileType(fileList.get(0).getDocumentFileType());
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
