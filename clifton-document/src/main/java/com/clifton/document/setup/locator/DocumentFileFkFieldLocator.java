package com.clifton.document.setup.locator;

/**
 * Finds the foregin key id from a file name
 *
 * @author KellyJ
 */
public interface DocumentFileFkFieldLocator {

	public Long getDocumentFileFkFieldIdFromFileName(String fileName);
}
