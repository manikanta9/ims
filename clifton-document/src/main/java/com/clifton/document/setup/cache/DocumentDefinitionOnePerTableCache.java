package com.clifton.document.setup.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.document.setup.DocumentDefinition;
import org.springframework.stereotype.Component;


/**
 * The <code>DocumentDefinitionOnePerTableCache</code> class provides caching and retrieval from cache of DocumentDefinition objects
 * by table name that are flagged as OnePerEntity only.  There is current validation that restricts each table to have a definition with this
 * property only once.
 *
 * @author manderson
 */
@Component
public class DocumentDefinitionOnePerTableCache extends SelfRegisteringSingleKeyDaoCache<DocumentDefinition, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "table.id";
	}


	@Override
	protected Short getBeanKeyValue(DocumentDefinition bean) {
		if (bean.getTable() != null) {
			return bean.getTable().getId();
		}
		return null;
	}


	@Override
	protected DocumentDefinition lookupBean(ReadOnlyDAO<DocumentDefinition> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		DocumentDefinition bean = dao.findOneByFields(new String[]{"table.id", "oneDocumentPerEntity"}, new Object[]{keyProperties[0], true});
		if (bean == null && throwExceptionIfNotFound) {
			throw new ValidationException("Unable to find a one per entity Document Definition for table id [" + keyProperties[0] + "]");
		}
		return bean;
	}
}
