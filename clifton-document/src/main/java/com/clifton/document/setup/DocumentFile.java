package com.clifton.document.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentWithFileSizeAware;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.usedby.softlink.SoftLinkField;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


/**
 * The <code>DocumentFile</code> ...
 *
 * @author manderson
 */
public class DocumentFile extends NamedEntity<Integer> implements DocumentWithRecordStampAware, DocumentWithFileSizeAware {

	public static final String DOCUMENT_FILE_TABLE_NAME = "DocumentFile";

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Which definition this document is tied to
	 * also defines the source table for the linked entity
	 */
	private DocumentDefinition definition;

	/**
	 * ID field in the source table for the linked entity
	 */
	@SoftLinkField(tableBeanPropertyName = "definition.table")
	private Long fkFieldId;

	/**
	 * Document Properties
	 */
	private String documentFileType;

	/**
	 * File size in bytes
	 */
	private Long fileSize;

	/**
	 * Document Record Stamp Properties
	 */
	private Date documentUpdateDate;
	private String documentUpdateUser;

	/**
	 * Optional start/end dates for when file is considered "active"
	 */
	private Date startDate;
	private Date endDate;

	// Used to upload files to Documents - just for initial file upload
	@NonPersistentField
	private MultipartFile file;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(DocumentDefinition definition) {
		this.definition = definition;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	@Override
	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	@Override
	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	@Override
	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	@Override
	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	@Override
	public Long getFileSize() {
		return this.fileSize;
	}


	@Override
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
}
