package com.clifton.document;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentFileUpload;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>DocumentManagementService</code> defines the methods for working with Documents
 * within our system.
 * <p>
 *
 * @author manderson
 */
public interface DocumentManagementService {

	////////////////////////////////////////////////////////////////////////////////
	//////////////                 Download  Methods                ////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(securityResource = "DocumentRecord")
	public FileWrapper downloadDocumentRecord(String documentId);


	/**
	 * Download the document after converting it to specified type (extension)
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public FileWrapper downloadDocumentRecordConversion(String documentId, String type);


	/**
	 * Shortcut feature that enables list of DocumentRecord objects (retrieved by table and fk field ids) as one zip file
	 * If any file is missing an exception is thrown
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public FileWrapper downloadDocumentRecordsAsZip(String tableName, long[] fkFieldIds);


	/**
	 * Returns the WebDavUrl String used for editing documents online
	 */
	@ResponseBody
	@SecureMethod(securityResource = "DocumentRecord")
	public String getDocumentRecordWebDavUrl(String documentId);

	////////////////////////////////////////////////////////////////////////////
	////////////                 Document  Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the LATEST DocumentRecord associated with a given entity,
	 * determined by the tableName & entity Id
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord getDocumentRecord(String tableName, long fkFieldId);


	/**
	 * Returns a document based upon it's given documentId
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord getDocumentRecordByDocumentId(String documentId);


	/**
	 * Admin only method that can be used in RARE cases where we need to "move" the file to new properties
	 * Assumes file location remains in same folder
	 * and can be used for cases where a table is renamed or for something like issue for SystemNotes were a note had single note attachment and should have been multiple
	 */
	@ResponseBody
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public String moveDocumentRecord(String fromTableName, long fromFkFieldId, String toTableName, long toFkFieldId);


	/**
	 * Used to create a document from another document.
	 * <p>
	 * For example: Creating Contracts from a Template, or when creating Revisions of Contracts
	 * <p>
	 * NOTE: This method first checks that if the From Document Exists, and the To Document does not.
	 * If the From doesn't exist, or the To already has a document associated - then this method doesn't
	 * do anything!
	 */
	@SecureMethod(securityResource = "DocumentRecord", permissions = SecurityPermission.PERMISSION_CREATE)
	public void copyDocumentRecord(String fromTableName, long fromFkFieldId, String toTableName, long toFkFieldId);


	/**
	 * Check out a document so no one else can edit it.
	 * Returns the Latest DocumentRecord associated with that entity
	 * with the new Working Copy and checked out information set.
	 * Does not return the checked out document
	 */
	@RequestMapping("documentRecordCheckout")
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord checkoutDocumentRecord(String documentId, String tableName, long fkFieldId);


	/**
	 * Cancels check out of a document
	 * <p>
	 * The documentId passed MUST be the working copy document id.
	 * <p>
	 * Returns the Latest DocumentRecord associated with that entity
	 * with the new Working Copy and checked out information cleared.
	 */
	@RequestMapping("documentRecordCancelCheckout")
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord cancelCheckoutDocumentRecord(String documentId);


	/**
	 * Check in a document as a version of the given document
	 * <p>
	 * The documentId passed MUST be the working copy document id.
	 * <p>
	 * Returns the Latest DocumentRecord associated with that entity
	 * with the new Working Copy and checked out information cleared.
	 */
	@RequestMapping("documentRecordCheckin")
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord checkinDocumentRecord(DocumentRecord documentRecord);


	/**
	 * Upload a file for the selected document
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord uploadDocumentRecord(DocumentRecord document);


	/**
	 * Special case for new DocumentFile with attachment so we can upload the file and create the DocumentFile row all at once.
	 * <p>
	 * Actual file should be on the DocumentFile bean
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public void uploadDocumentFile(DocumentFile bean);


	/**
	 * Copies the list of {@link DocumentFile} associated with the source to the target.  For each DocumentFile will also
	 * copy the actual file associated with it to the new DocumentFile.
	 * <p>
	 * Should be used rarely - usually we can link an attachment to multiple records.
	 * Currently used for BillingInvoice attachments when the invoice is voided and regenerated, we copy that attachments so we have history and can keep/remove/or add attachments to the invoice without affecting history
	 */
	@DoNotAddRequestMapping
	public void copyDocumentFileList(String tableName, Long sourceFkFieldId, Long targetFkFieldId);


	/**
	 * Deletes the document associated with the given tableName & fkFieldId
	 *
	 * @param doNotValidateModifyCondition - When called from the UI and deleting just the document itself, then should validate modify condition.
	 *                                     When called from the DAO observer on the entity's delete - then don't validate modify conditions - just delete because the user already has permission to delete the entity in IMS
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public void deleteDocumentRecord(String documentId, String tableName, long fkFieldId, boolean doNotValidateModifyCondition);


	/**
	 * Restores given document version as the latest version.
	 * 1.  Checks Out Document
	 * 2.  Sets Document Content to given document
	 * 3.  Checks In Document with Comment & Major/Minor Selection
	 * 4.  Returns the latest version of the document (which is now contains the restored document contents)
	 */
	@RequestMapping("documentRecordRestore")
	@SecureMethod(securityResource = "DocumentRecord")
	public DocumentRecord restoreDocumentRecordVersion(DocumentRecord documentRecord);

	////////////////////////////////////////////////////////////////////////////
	//////////               Document Version  Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of all DocumentRecords for the Document determined by the tableName and fkFieldId
	 */
	@SecureMethod(securityResource = "DocumentRecord")
	public List<DocumentRecord> getDocumentRecordVersionList(String tableName, long fkFieldId);

	////////////////////////////////////////////////////////////////////////////////
	////////////              Document File Upload Methods            //////////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = DocumentFile.class)
	public void uploadDocumentFileUpload(DocumentFileUpload documentFileUpload);
}
