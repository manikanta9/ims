package com.clifton.document;


import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>DocumentRecordSearchForm</code> used to search/filter document records that can be pulled out of the DocumentManagementSystem
 * NOTE: Not all properties can be passed in searches, so many of the filters are done after the list retrieval from Alfresco
 *
 * @author Mary Anderson
 */
@SearchForm(hasOrmDtoClass = false)
public class DocumentRecordSearchForm extends BaseAuditableEntitySearchForm {

	private Short documentDefinitionId;

	private String definitionName;

	private String name;

	private String createUser;
	private String updateUser;

	private String checkedOutUser;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCreateUser() {
		return this.createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public String getUpdateUser() {
		return this.updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public Short getDocumentDefinitionId() {
		return this.documentDefinitionId;
	}


	public void setDocumentDefinitionId(Short documentDefinitionId) {
		this.documentDefinitionId = documentDefinitionId;
	}


	public String getCheckedOutUser() {
		return this.checkedOutUser;
	}


	public void setCheckedOutUser(String checkedOutUser) {
		this.checkedOutUser = checkedOutUser;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}
}
