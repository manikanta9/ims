package com.clifton.document.comparisons;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.beans.document.DocumentFileAware;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;

import java.util.List;


/**
 * The <code>DocumentComparison</code> is an abstract class used by all Document comparisons.  Defines dao lookup and document lookup methods that can be reused.
 * <p>
 * Note: If the bean is DocumentAware - then will look for the document (single document per entity first)
 * If nothing found, and the bean is DocumentFileAware - will get the list of attachments and run the comparison on the first document found.
 *
 * @author manderson
 */
public abstract class DocumentComparison<T extends IdentityObject> implements Comparison<T> {

	private DaoLocator daoLocator;
	private DocumentManagementService documentManagementService;
	private DocumentSetupService documentSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract boolean processComparison(T bean);


	public abstract String getTrueMessage();


	public abstract String getFalseMessage();


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		boolean result = processComparison(bean);
		result = (isReverse() ? !result : result);

		if (context != null) {
			if (result) {
				context.recordTrueMessage(getTrueMessage());
			}
			else {
				context.recordFalseMessage(getFalseMessage());
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected DocumentRecord getDocumentRecordForBean(T bean) {
		// Entity hasn't been created yet, so there wouldn't be a Document Record associated yet
		if (bean == null || bean.isNewBean()) {
			return null;
		}
		ReadOnlyDAO<T> dao = getDaoLocator().locate((Class<T>) bean.getClass());
		String tableName = dao.getConfiguration().getTableName();

		DocumentRecord result = null;

		// Supports Single File Per Entity...Check there first
		if (bean instanceof DocumentAware) {
			result = getDocumentManagementService().getDocumentRecord(tableName, (Integer) bean.getIdentity());
		}

		// If entity supports multiple files per document - then return first file found (if any)
		// Currently only System Notes supports both
		if (result == null && bean instanceof DocumentFileAware) {
			List<DocumentFile> documentFileList = getDocumentSetupService().getDocumentFileListForEntity(tableName, BeanUtils.getIdentityAsLong(bean));
			if (!CollectionUtils.isEmpty(documentFileList)) {
				result = getDocumentManagementService().getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, documentFileList.get(0).getId());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}
}
