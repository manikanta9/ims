package com.clifton.document.comparisons;


/**
 * The <code>DocumentNotCheckedOutComparison</code> returns true if the entity does not have a document
 * associated with it or the document associated is not currently checked out.
 *
 * @author manderson
 */
public class DocumentNotCheckedOutComparison extends DocumentCheckedOutComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
