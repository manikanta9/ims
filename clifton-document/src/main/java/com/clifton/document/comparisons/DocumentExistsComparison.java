package com.clifton.document.comparisons;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>DocumentExistsComparison</code> returns true if the entity has
 * a Document associated with it.
 *
 * @author manderson
 */
public class DocumentExistsComparison extends DocumentComparison<IdentityObject> {

	@Override
	public boolean processComparison(IdentityObject bean) {
		return (getDocumentRecordForBean(bean) != null);
	}


	@Override
	public String getTrueMessage() {
		if (isReverse()) {
			return "(Document does not exist)";
		}
		return "(Document exists)";
	}


	@Override
	public String getFalseMessage() {
		if (isReverse()) {
			return "(Document exists)";
		}
		return "(Document does not exist)";
	}
}
