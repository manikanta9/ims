package com.clifton.document.comparisons;


/**
 * The <code>DocumentNotExistsComparison</code> ...
 *
 * @author manderson
 */
public class DocumentNotExistsComparison extends DocumentExistsComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
