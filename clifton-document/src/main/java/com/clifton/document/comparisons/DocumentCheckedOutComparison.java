package com.clifton.document.comparisons;


import com.clifton.core.beans.IdentityObject;
import com.clifton.document.DocumentRecord;


/**
 * The <code>DocumentCheckedOutComparison</code> returns true if the entity has a document
 * associated with it and the document is currently checked out.
 *
 * @author manderson
 */
public class DocumentCheckedOutComparison extends DocumentComparison<IdentityObject> {

	@Override
	public boolean processComparison(IdentityObject bean) {
		boolean result = false;
		DocumentRecord documentRecord = getDocumentRecordForBean(bean);
		if (documentRecord != null) {
			result = documentRecord.isVersionSeriesCheckedOut();
		}
		return result;
	}


	@Override
	public String getTrueMessage() {
		if (isReverse()) {
			return "(Document is not currently checked out)";
		}
		return "(Document is currently checked out)";
	}


	@Override
	public String getFalseMessage() {
		if (isReverse()) {
			return "(Document is currently checked out)";
		}
		return "(Document is not currently checked out)";
	}
}
