package com.clifton.document.cmis.converters;


import com.clifton.core.dataaccess.file.DocumentContentStream;
import com.clifton.core.util.converter.Converter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;


public class CMISContentStreamToDocumentContentStreamConverter implements Converter<ContentStream, DocumentContentStream> {

	@Override
	public DocumentContentStream convert(ContentStream from) {
		DocumentContentStream stream = new DocumentContentStream();
		stream.setFilename(from.getFileName());
		stream.setLength(from.getLength());
		stream.setMimeType(from.getMimeType());
		stream.setStream(from.getStream());
		return stream;
	}
}
