package com.clifton.document.cmis.cache;

import com.clifton.core.cache.CustomCache;
import org.apache.chemistry.opencmis.client.api.Session;


/**
 * This class caches the CMIS Session objects for users.  It allows reuse of the CMIS session object
 * and will enforce security based on the user used during authentication on connection.
 *
 * @author stevenf
 */
public interface CMISSessionCache extends CustomCache<String, Session> {

	public Session getSession(String userName);


	public void setSession(String userName, Session session);
}
