package com.clifton.document.cmis;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.file.DocumentContentStream;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import com.clifton.core.util.http.SimpleHttpResponse;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.document.DocumentManagementProvider;
import com.clifton.document.DocumentRecord;
import com.clifton.document.cmis.cache.CMISSessionCache;
import com.clifton.document.cmis.converters.CMISContentStreamToDocumentContentStreamConverter;
import com.clifton.document.converters.FileNameToTypeConverter;
import com.clifton.security.user.SecurityUser;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.ObjectIdImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CMISDocumentManagementProvider</code> provides an implementation
 * of the {@link DocumentManagementProvider} using CMIS API.
 */
public class CMISDocumentManagementProvider implements DocumentManagementProvider<Document> {

	private ApplicationContextService applicationContextService;
	private CMISSessionCache cmisSessionCache;
	private FileNameToTypeConverter fileNameToTypeConverter;
	private ContextHandler contextHandler;

	////////////////////////////////////////////////////////////////////////////////

	private static final String DOCUMENT_TYPE = "tcg:contract";
	private static final String CMIS_HEADER = "org.apache.chemistry.opencmis.binding.header";
	/**
	 * IN IMS THIS IS NOW THE DEFINITION NAME SO WE CAN SUPPORT DIFFERENT DOCUMENT TYPES PER TABLE
	 */

	public static final String CMIS_DOCUMENT_PROPERTY_TABLE_NAME = "tcg:tableName";
	public static final String CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY = "tcg:foreignKey";

	private static final String DEFAULT_DOCUMENT_ROOT_FOLDER_PATH = "/IMS";
	private static final String DEFAULT_WEBDAV_URL = "http://docs.paraport.com/alfresco/webdav"; // NOTE IF WE NEED TO USE SSL URL: "https://tcgdocs:8443/alfresco/webdav";
	private static final String DEFAULT_WEBSERVICE_URL = "http://docs.paraport.com/alfresco/services";

	////////////////////////////////////////////////////////////////////////////////

	private String cmisAtomPubUrl;
	private String cmisAuthorizationHeader;
	private String cmisRepositoryId;
	private String documentRootFolderPath; // Folder Path for the top level document root, i.e. IMS or IMS-PROD
	private String documentRootFolderPathId; //ID of root folder - queried for once from root folder path and stored.
	private String documentWebdavUrl; // URL for WebDav (appended with specific document folder path & name)
	private String documentWebserviceUrl;

	private String serviceUserName;
	private String serviceUserPassword;


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Document Content Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DocumentContentStream getDocumentContentStream(String documentId) {
		return getDocumentContentStream(getDocument(documentId));
	}


	@Override
	public DocumentContentStream getDocumentContentStream(Document document) {
		if (document != null && document.getContentStream() != null) {
			return new CMISContentStreamToDocumentContentStreamConverter().convert(document.getContentStream());
		}
		return null;
	}


	@Override
	public void copyDocumentContentStream(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord) {
		Document fromDoc = getDocument(fromDocumentRecord.getDocumentId());
		Document toDoc = getDocument(toDocumentRecord.getDocumentId());
		if (fromDoc == null || toDoc == null) {
			throw new IllegalStateException("Unable to copy document contents because either source or target document doesn't exist");
		}
		toDoc.setContentStream(fromDoc.getContentStream(), true);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////                 Document  Methods                ////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Document getDocument(String documentId) {
		return (Document) getCmisSession().getObject(documentId);
	}


	@Override
	public Document getRendition(String documentId, String type) {
		return getRendition(getDocument(documentId), type);
	}


	@Override
	public Document getRendition(Document document, String type) {
		try {
			HttpPost httpPost = new HttpPost();
			httpPost.setURI(URI.create(getDocumentWebserviceUrl() +
					"/create-pdf-rendition?documentId=" + document.getProperty("alfcmis:nodeRef").getValueAsString()));
			httpPost.setHeader("Authorization","Basic " + Base64.getEncoder().encodeToString((getServiceUserName()+":"+getServiceUserPassword()).getBytes()));

			SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(httpPost);
			String renditionDocumentId = EntityUtils.toString(response.getEntity(), "UTF-8");

			if (!StringUtils.isEmpty(renditionDocumentId)) {
				return (Document) getCmisSession().getObject(renditionDocumentId);
			}
			return null;
		}
		catch (Exception e) {
			throw new RuntimeException("Error creating " + type + " rendition for document '" +
					document.getContentStreamFileName() + "' [" + document.getId() + "]", e);
		}
	}


	@Override
	public Document getDocumentByTableAndFkField(String tableName, long fkFieldId) {
		//in_tree not working with latest version of alfresco and cmis API (5.0.d and 0.13 respectively.)
		//
		String queryString = "SELECT " + PropertyIds.OBJECT_ID + ", " + PropertyIds.VERSION_LABEL +
				" FROM " + DOCUMENT_TYPE + " WHERE in_tree('" + getDocumentRootFolderPathId() + "') and " + CMIS_DOCUMENT_PROPERTY_TABLE_NAME + "='"
				+ tableName + "' and " + CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY + "='" + fkFieldId + "'";
		//System.out.println("QUERY: " + queryString);
		Document returnDoc = null;
		ItemIterable<QueryResult> results = getCmisSession().query(queryString, false);
		// NOTE: PREVIOUS VERSIONS ARE NOT STORED IN THE SAME "TREE", I.E. DOCUMENT ROOT, SO THE ONLY DOCUMENT VERSIONS THAT WOULD BE RETURNED
		// ARE THE LATEST CHECKED IN VERSION, OR THE WORKING COPY IF CURRENTLY CHECKED OUT.
		// WHEN GOING THROUGH THE RESULTS, THIS WILL CHECK VERSION LABEL, IF NOT PWC WILL RETURN IT CONSIDERING IT TO BE THE LATEST NON WORKING COPY.
		// IN PREVIOUS VERSIONS OF CMIS/ALFRESCO YOU USED TO BE ABLE TO GET THE LATEST CHECKED IN VERSION BY THE VERSION SERIES ID, HOWEVER NOW THAT VERSION SERIES ID IS THAT OF THE FIRST VERSION
		int breakCount = 0;
		for (QueryResult qResult : results) {
			//Due to SOLR indexing delay sometimes after a delete requerying for the item before the index is updated can return a null qResult.
			//The iterator has a bug in this case as well, the getTotalNumItems will return a result of 1 which will be null, but the the list
			//will infinitely loop until the request times out so we track the iterations and break manually when they exceed the count.
			if (qResult != null && !"pwc".equalsIgnoreCase(qResult.getPropertyValueById(PropertyIds.VERSION_LABEL))) {
				String objectId = qResult.getPropertyValueById(PropertyIds.OBJECT_ID);
				returnDoc = getDocument(objectId);
				break;
			}
			breakCount++;
			if (breakCount == results.getTotalNumItems()) {
				break;
			}
		}
		return returnDoc;
	}


	@Override
	public Document createDocument(DocumentRecord documentRecord) {
		documentRecord.setFolderName(getDocumentRootFolderPath() + documentRecord.getFolderName());
		ObjectId folderID = getCmisSession().getObjectByPath(documentRecord.getFolderName());
		ContentStream contentStream = getContentStreamForFile(documentRecord.getFile());
		ObjectId objectId = getCmisSession().createDocument(createPropertiesForNewDocument(documentRecord), folderID, contentStream, VersioningState.MAJOR, null, null, null);
		return getDocument(objectId.getId());
	}


	@Override
	public Document updateDocument(DocumentRecord documentRecord, String changedFileExtension) {
		Document document = getDocument(documentRecord.getDocumentId());

		ContentStream contentStream = getContentStreamForFile(documentRecord.getFile());

		// Uploading Changes to a Checked Out Version
		// Only update document name with new extension if file extension changed - i.e.  doc to docx and vice versa
		ObjectId objectId = new ObjectIdImpl(document.getId());
		if (!StringUtils.isEmpty(changedFileExtension)) {
			Map<String, String> properties = new HashMap<>();
			properties.put(PropertyIds.NAME, FileUtils.getFileNameWithoutExtension(document.getName()) + "." + changedFileExtension);
			objectId = document.updateProperties(properties, true);
		}
		document.setContentStream(contentStream, true);
		return getDocument(objectId.getId());
	}


	@Override
	public void updateDocumentProperties(Document document, String newTableName, long newFkFieldId) {
		Map<String, String> properties = new HashMap<>();
		properties.put(CMIS_DOCUMENT_PROPERTY_TABLE_NAME, newTableName);
		properties.put(CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY, ((Long) newFkFieldId).toString());
		document.updateProperties(properties, true);
	}


	@Override
	public Document copyDocument(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord) {
		Document document = getDocument(fromDocumentRecord.getDocumentId());
		DocumentContentStream dcs = getDocumentContentStream(document);

		MultipartFile multipartFile = new MultipartFileImpl(getAvailableDocumentName(toDocumentRecord, 0),
				toDocumentRecord.getName(), dcs.getMimeType(), dcs.getLength(), dcs.getStream());
		toDocumentRecord.setFile(multipartFile);
		return createDocument(toDocumentRecord);
	}


	@Override
	public Document checkoutDocument(Document document) {
		document.checkOut();
		document.refresh();
		return document;
	}


	@Override
	public Document cancelCheckoutDocument(Document document) {
		document.cancelCheckOut(); //at this point the checkedOut document is destroyed in the repository.
		return document.getAllVersions().get(0);
	}


	@Override
	public Document checkinDocument(DocumentRecord record) {
		Document document = getDocument(record.getDocumentId());
		String checkInComment = record.getCheckinComment();
		if (record.isRedLine()) {
			if (checkInComment == null) {
				checkInComment = DocumentRecord.RED_LINE_COMMENT_PREFIX;
			}
			else {
				checkInComment = DocumentRecord.RED_LINE_COMMENT_PREFIX + checkInComment;
			}
		}
		ObjectId objectId = document.checkIn(record.isMajorVersion(), null, document.getContentStream(), checkInComment, null, null, null); //at this point the checkedOut document is destroyed in the repository.
		return getDocument(objectId.getId());
	}


	@Override
	public void deleteDocument(Document document) {
		if (document != null) {
			if (document.isVersionSeriesCheckedOut()) {
				throw new ValidationException("Unable to delete a document that is currently checked out.");
			}
			document.deleteAllVersions();
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                    WebDav Methods                     //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDocumentWebdavUrl(DocumentRecord documentRecord) {
		return getDocumentWebdavUrl() + getDocumentRootFolderPath() + getFullDocumentPath(documentRecord, true).replaceAll("\\+", "%20");
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////               Document Version Methods                //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Document> getDocumentVersionList(Document document) {
		if (document == null) {
			return null;
		}
		return document.getAllVersions();
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                    Helper Methods                     //////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFullDocumentPath(DocumentRecord record, boolean encode) {
		try {
			return record.getFolderName() + "/" + ((encode) ? URLEncoder.encode(record.getName(), "UTF-8") : record.getName());
		}
		catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Cannot encode WebDav url document name = " + record.getName(), e);
		}
	}


	private Map<String, String> createPropertiesForNewDocument(DocumentRecord documentRecord) {
		// If a document already exists with the given name, append -ID to the name
		String docName = getAvailableDocumentName(documentRecord, 0);
		Map<String, String> properties = new HashMap<>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "D:" + DOCUMENT_TYPE);
		properties.put(CMIS_DOCUMENT_PROPERTY_TABLE_NAME, documentRecord.getTableName());
		properties.put(CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY, Long.toString(documentRecord.getFkFieldId()));
		properties.put(PropertyIds.NAME, docName);
		return properties;
	}


	/**
	 * If count = 0, checks if the file name is OK to upload as is
	 * If count = 1, appends "-" + the document record FKFieldID
	 * If count >= 2, appends to the document name + -fkFieldId + "-" + count until an available file name is found
	 */
	private String getAvailableDocumentName(DocumentRecord documentRecord, int count) {
		String documentName = documentRecord.getName();
		try {
			if (count > 0) {
				String extension = FileUtils.getFileExtension(documentName);
				documentName = FileUtils.getFileNameWithoutExtension(documentName) + "-" + documentRecord.getFkFieldId();
				if (count > 1) {
					documentName = documentName + "-" + count;
				}
				documentName = documentName + "." + extension;
			}
			getCmisSession().getObjectByPath(documentRecord.getFolderName() + "/" + documentName);
		}
		catch (Throwable e) {
			// Throws Exception if doesn't exist - Eat the exception
			return documentName;
		}
		// Otherwise - increment counter and try again
		return getAvailableDocumentName(documentRecord, count + 1);
	}


	private ContentStream getContentStreamForFile(MultipartFile file) {
		try {
			return getCmisSession().getObjectFactory().createContentStream(file.getOriginalFilename(), file.getSize(), getFileNameToTypeConverter().convert(file.getOriginalFilename()),
					file.getInputStream());
		}
		catch (Exception e) {
			throw new RuntimeException("Error creating content stream for uploaded file.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Session getCmisSession() {
		long start = System.currentTimeMillis();
		SessionFactory sessionFactory = getApplicationContextService().getContextBean("documentCmisSessionFactory", SessionFactory.class);
		// create session
		SecurityUser currentUser = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		String runAsUser = currentUser.getUserName();

		Session session = getCmisSessionCache().getSession(runAsUser);
		if (session == null) {
			Map<String, String> parameter = new HashMap<>();
			parameter.put(CMIS_HEADER + ".0", getCmisAuthorizationHeader() + ":" + runAsUser);
			parameter.put(SessionParameter.ATOMPUB_URL, getCmisAtomPubUrl());
			parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
			parameter.put(SessionParameter.REPOSITORY_ID, getCmisRepositoryId());
			parameter.put(SessionParameter.USER,getServiceUserName());
			parameter.put(SessionParameter.PASSWORD,getServiceUserPassword());

			session = sessionFactory.createSession(parameter);
			session.getDefaultContext().setCacheEnabled(false);
			getCmisSessionCache().setSession(runAsUser, session);
		}
		LogUtils.debug(getClass(), "TIME TO GET DOCUMENT CMIS SESSION: " + (System.currentTimeMillis() - start));
		return session;
	}


	public String getDocumentRootFolderPathId() {
		if (this.documentRootFolderPathId == null) {
			Folder documentRootFolder = (Folder) this.getCmisSession().getObjectByPath(getDocumentRootFolderPath());
			this.documentRootFolderPathId = documentRootFolder.getId();
		}
		return this.documentRootFolderPathId;
	}


	public String getDocumentRootFolderPath() {
		if (this.documentRootFolderPath == null) {
			this.documentRootFolderPath = DEFAULT_DOCUMENT_ROOT_FOLDER_PATH;
			LogUtils.debug(getClass(), "DOCUMENT ROOT FOLDER PATH: " + this.documentRootFolderPath);
		}
		return this.documentRootFolderPath;
	}


	public String getDocumentWebserviceUrl() {
		if (this.documentWebserviceUrl == null) {
			return DEFAULT_WEBSERVICE_URL;
		}
		return this.documentWebserviceUrl;
	}


	public String getDocumentWebdavUrl() {
		if (this.documentWebdavUrl == null) {
			return DEFAULT_WEBDAV_URL;
		}
		return this.documentWebdavUrl;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCmisRepositoryId() {
		return this.cmisRepositoryId;
	}


	public void setCmisRepositoryId(String cmisRepositoryId) {
		this.cmisRepositoryId = cmisRepositoryId;
	}


	public String getCmisAuthorizationHeader() {
		return this.cmisAuthorizationHeader;
	}


	public void setCmisAuthorizationHeader(String cmisAuthorizationHeader) {
		this.cmisAuthorizationHeader = cmisAuthorizationHeader;
	}


	public String getCmisAtomPubUrl() {
		return this.cmisAtomPubUrl;
	}


	public void setCmisAtomPubUrl(String cmisAtomPubUrl) {
		this.cmisAtomPubUrl = cmisAtomPubUrl;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public CMISSessionCache getCmisSessionCache() {
		return this.cmisSessionCache;
	}


	public void setCmisSessionCache(CMISSessionCache cmisSessionCache) {
		this.cmisSessionCache = cmisSessionCache;
	}


	public FileNameToTypeConverter getFileNameToTypeConverter() {
		return this.fileNameToTypeConverter;
	}


	public void setFileNameToTypeConverter(FileNameToTypeConverter fileNameToTypeConverter) {
		this.fileNameToTypeConverter = fileNameToTypeConverter;
	}


	public void setDocumentRootFolderPath(String documentRootFolderPath) {
		this.documentRootFolderPath = documentRootFolderPath;
	}


	public void setDocumentWebserviceUrl(String documentWebserviceUrl) {
		this.documentWebserviceUrl = documentWebserviceUrl;
	}


	public void setDocumentWebdavUrl(String documentWebdavUrl) {
		this.documentWebdavUrl = documentWebdavUrl;
	}


	public String getServiceUserName() {
		return this.serviceUserName;
	}


	public void setServiceUserName(String serviceUserName) {
		this.serviceUserName = serviceUserName;
	}


	public String getServiceUserPassword() {
		return this.serviceUserPassword;
	}


	public void setServiceUserPassword(String serviceUserPassword) {
		this.serviceUserPassword = serviceUserPassword;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
