package com.clifton.document.cmis.converters;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.document.DocumentRecord;
import com.clifton.document.cmis.CMISDocumentManagementProvider;
import com.clifton.document.converters.DocumentToDocumentRecordConverter;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.springframework.stereotype.Component;


/**
 * The <code>CMISDocumentToDocumentRecordConverter</code> implements the {@link DocumentToDocumentRecordConverter}
 * interface and provides an implementation for converting CMIS {@link Document} to {@link DocumentRecord}
 *
 * @author manderson
 */
@Component("documentToDocumentRecordConverter")
public class CMISDocumentToDocumentRecordConverter implements DocumentToDocumentRecordConverter<Document> {

	@Override
	public DocumentRecord convert(Document from) {
		if (from == null) {
			return null;
		}
		DocumentRecord documentRecord = new DocumentRecord();

		// Document ID
		documentRecord.setDocumentId(from.getId());

		// UX Properties
		documentRecord.setTableName(from.getPropertyValue(CMISDocumentManagementProvider.CMIS_DOCUMENT_PROPERTY_TABLE_NAME));
		documentRecord.setFkFieldId(Integer.parseInt(from.getPropertyValue(CMISDocumentManagementProvider.CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY)));

		// File Info (Folder, Document Name, and Content)
		// NOTE DO NOT GET FOLDERS FOR OLDER VERSIONS
		// The versions are slightly different in how they are stored in alfresco,
		// they don't keep the same structure as the PWC or the latest version
		// To differentiate between them, if there's a ; in the documentID it's an old version
		// We don't use the folders for the older versions anyway
		if (!documentRecord.getDocumentId().contains(";")) {
			Folder f = CollectionUtils.getFirstElement(from.getParents());
			String folderName = (f != null ? f.getPath() : null);
			documentRecord.setFolderName(folderName);
		}
		documentRecord.setName(from.getName());
		//documentRecord.setContentStream(new CMISContentStreamToDocumentContentStreamConverter().convert(from.getContentStream()));

		// Record Stamp Info
		documentRecord.setCreateDate(from.getCreationDate().getTime());
		documentRecord.setCreateUser(from.getCreatedBy());
		documentRecord.setUpdateDate(from.getLastModificationDate().getTime());
		documentRecord.setUpdateUser(from.getLastModifiedBy());

		// Check In Info
		documentRecord.setMajorVersion(from.isMajorVersion());

		String checkInComment = from.getCheckinComment();
		if (!StringUtils.isEmpty(checkInComment) && checkInComment.startsWith(DocumentRecord.RED_LINE_COMMENT_PREFIX)) {
			documentRecord.setRedLine(true);
			documentRecord.setCheckinComment(checkInComment.substring(DocumentRecord.RED_LINE_COMMENT_PREFIX.length()));
		}
		else {
			documentRecord.setRedLine(false);
			documentRecord.setCheckinComment(from.getCheckinComment());
		}

		// Version Info
		documentRecord.setLatestVersion(from.isLatestVersion());
		documentRecord.setVersionLabel(from.getVersionLabel());
		documentRecord.setVersionSeriesID(from.getVersionSeriesId());

		// Checked Out Info
		documentRecord.setVersionSeriesCheckedOut(from.isVersionSeriesCheckedOut());
		documentRecord.setCheckedOutUser(from.getVersionSeriesCheckedOutBy());
		documentRecord.setVersionSeriesCheckedOutId(from.getVersionSeriesCheckedOutId());
		documentRecord.setFileSize(from.getContentStreamLength());

		return documentRecord;
	}
}
