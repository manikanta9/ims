package com.clifton.document.cmis.converters;


import com.clifton.core.util.StringUtils;
import com.clifton.document.DocumentRecord;
import com.clifton.document.cmis.CMISDocumentManagementProvider;
import com.clifton.document.converters.DocumentToDocumentRecordConverter;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.commons.PropertyIds;

import java.util.GregorianCalendar;


/**
 * The <code>CMISQueryResultToDocumentRecordConverter</code> implements the {@link DocumentToDocumentRecordConverter}
 * interface and provides an implementation for converting CMIS {@link QueryResult} to {@link DocumentRecord}
 * <p>
 * Used for list pages, since we don't need all of the details of the document
 *
 * @author manderson
 */
public class CMISQueryResultToDocumentRecordConverter implements DocumentToDocumentRecordConverter<QueryResult> {

	@Override
	public DocumentRecord convert(QueryResult from) {
		if (from == null) {
			return null;
		}
		DocumentRecord documentRecord = new DocumentRecord();

		// Document ID
		documentRecord.setDocumentId(from.getPropertyValueById(PropertyIds.OBJECT_ID));

		// UX Properties for One Document Per Entity types
		documentRecord.setTableName(from.getPropertyValueById(CMISDocumentManagementProvider.CMIS_DOCUMENT_PROPERTY_TABLE_NAME));
		documentRecord.setFkFieldId(Integer.parseInt(from.getPropertyValueById(CMISDocumentManagementProvider.CMIS_DOCUMENT_PROPERTY_FOREIGN_KEY)));
		documentRecord.setName(from.getPropertyValueById(PropertyIds.NAME));

		// Record Stamp Info
		documentRecord.setCreateDate(from.<GregorianCalendar>getPropertyValueById(PropertyIds.CREATION_DATE).getTime());
		documentRecord.setCreateUser(from.getPropertyValueById(PropertyIds.CREATED_BY));
		documentRecord.setUpdateDate(from.<GregorianCalendar>getPropertyValueById(PropertyIds.LAST_MODIFICATION_DATE).getTime());
		documentRecord.setUpdateUser(from.getPropertyValueById(PropertyIds.LAST_MODIFIED_BY));

		// Check In Info
		//documentRecord.setMajorVersion((Boolean) from.getPropertyValueById(PropertyIds.IS_MAJOR_VERSION));

		String checkInComment = from.getPropertyValueById(PropertyIds.CHECKIN_COMMENT);
		if (!StringUtils.isEmpty(checkInComment) && checkInComment.startsWith(DocumentRecord.RED_LINE_COMMENT_PREFIX)) {
			documentRecord.setRedLine(true);
			documentRecord.setCheckinComment(checkInComment.substring(DocumentRecord.RED_LINE_COMMENT_PREFIX.length()));
		}
		else {
			documentRecord.setRedLine(false);
			documentRecord.setCheckinComment(checkInComment);
		}

		// Version Info
		documentRecord.setLatestVersion(from.getPropertyValueById(PropertyIds.IS_LATEST_VERSION));
		documentRecord.setVersionLabel(from.getPropertyValueById(PropertyIds.VERSION_LABEL));
		//documentRecord.setVersionSeriesID((String) from.getPropertyValueById(PropertyIds.VERSION_SERIES_ID));

		// Checked Out Info
		documentRecord.setVersionSeriesCheckedOut(from.getPropertyValueById(PropertyIds.IS_VERSION_SERIES_CHECKED_OUT));
		documentRecord.setCheckedOutUser(from.getPropertyValueById(PropertyIds.VERSION_SERIES_CHECKED_OUT_BY));
		//documentRecord.setVersionSeriesCheckedOutId((String) from.getPropertyValueById(PropertyIds.VERSION_SERIES_CHECKED_OUT_ID));

		return documentRecord;
	}
}
