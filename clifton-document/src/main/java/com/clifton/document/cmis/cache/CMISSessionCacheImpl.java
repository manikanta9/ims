package com.clifton.document.cmis.cache;

import com.clifton.core.cache.CacheHandler;
import org.apache.chemistry.opencmis.client.api.Session;
import org.springframework.stereotype.Component;


/**
 * This class caches the
 *
 * @author stevenf
 */
@Component("cmisSessionCache")
public class CMISSessionCacheImpl implements CMISSessionCache {

	private CacheHandler<String, Session> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Session getSession(String userName) {
		return getCacheHandler().get(getCacheName(), userName);
	}


	@Override
	public void setSession(String userName, Session session) {
		getCacheHandler().put(getCacheName(), userName, session);
	}


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, Session> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, Session> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
