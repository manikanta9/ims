package com.clifton.document.converters;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>FileNameToTypeConverter</code> interface defines
 * the Converter from String filename to String types.
 *
 * @author manderson
 */
public interface FileNameToTypeConverter extends Converter<String, String> {

	// NOTHING HERE
}
