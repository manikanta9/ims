package com.clifton.document.converters;


import com.clifton.core.util.converter.Converter;
import com.clifton.document.DocumentRecord;


/**
 * The <code>DocumentToDocumentRecordConverter</code> converts an {@link Object} document
 * from an external system to a {@link DocumentRecord}
 *
 * @author manderson
 */
public interface DocumentToDocumentRecordConverter<T> extends Converter<T, DocumentRecord> {

	// NOTHING HERE
}
