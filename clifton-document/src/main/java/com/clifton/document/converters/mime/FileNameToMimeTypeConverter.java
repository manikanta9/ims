package com.clifton.document.converters.mime;


import com.clifton.core.util.StringUtils;
import com.clifton.document.converters.FileNameToTypeConverter;

import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>FileNameToMimeTypeConverter</code> implements the {@link FileNameToTypeConverter} and returns the
 * mime type as the type.
 * <p>
 * Returns the the mime type if found, or "" if not found
 *
 * @author manderson
 */
public class FileNameToMimeTypeConverter implements FileNameToTypeConverter {

	private static final ConcurrentHashMap<String, String> FILE_EXTENSION_MIME_TYPE_MAP = new ConcurrentHashMap<>();


	static {
		FILE_EXTENSION_MIME_TYPE_MAP.put("bm", "image/bmp");
		FILE_EXTENSION_MIME_TYPE_MAP.put("bmp", "image/bmp");
		FILE_EXTENSION_MIME_TYPE_MAP.put("bmp", "image/x-windows-bmp");
		FILE_EXTENSION_MIME_TYPE_MAP.put("gif", "image/gif");
		FILE_EXTENSION_MIME_TYPE_MAP.put("jpe", "image/jpeg");
		FILE_EXTENSION_MIME_TYPE_MAP.put("jpeg", "image/jpeg");
		FILE_EXTENSION_MIME_TYPE_MAP.put("jpg", "image/jpeg");
		FILE_EXTENSION_MIME_TYPE_MAP.put("pdf", "application/pdf");
		FILE_EXTENSION_MIME_TYPE_MAP.put("tif", "image/tiff");
		FILE_EXTENSION_MIME_TYPE_MAP.put("tiff", "image/tiff");
		FILE_EXTENSION_MIME_TYPE_MAP.put("vsd", "application/x-visio");
		FILE_EXTENSION_MIME_TYPE_MAP.put("vst", "application/x-visio");
		FILE_EXTENSION_MIME_TYPE_MAP.put("vsw", "application/x-visio");
		FILE_EXTENSION_MIME_TYPE_MAP.put("word", "application/msword");
		FILE_EXTENSION_MIME_TYPE_MAP.put("doc", "application/msword");
		FILE_EXTENSION_MIME_TYPE_MAP.put("dot", "application/msword");
		FILE_EXTENSION_MIME_TYPE_MAP.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		FILE_EXTENSION_MIME_TYPE_MAP.put("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
		FILE_EXTENSION_MIME_TYPE_MAP.put("docm", "application/vnd.ms-word.document.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("dotm", "application/vnd.ms-word.template.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xls", "application/vnd.ms-excel");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xlt", "application/vnd.ms-excel");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xla", "application/vnd.ms-excel");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xltm", "application/vnd.ms-excel.template.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xlam", "application/vnd.ms-excel.addin.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("ppt", "application/vnd.ms-powerpoint");
		FILE_EXTENSION_MIME_TYPE_MAP.put("pot", "application/vnd.ms-powerpoint");
		FILE_EXTENSION_MIME_TYPE_MAP.put("pps", "application/vnd.ms-powerpoint");
		FILE_EXTENSION_MIME_TYPE_MAP.put("ppa", "application/vnd.ms-powerpoint");
		FILE_EXTENSION_MIME_TYPE_MAP.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
		FILE_EXTENSION_MIME_TYPE_MAP.put("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
		FILE_EXTENSION_MIME_TYPE_MAP.put("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
		FILE_EXTENSION_MIME_TYPE_MAP.put("ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("potm", "application/vnd.ms-powerpoint.template.macroEnabled.12");
		FILE_EXTENSION_MIME_TYPE_MAP.put("ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12");
	}


	@Override
	public String convert(String from) {
		String mimeType = FILE_EXTENSION_MIME_TYPE_MAP.get(StringUtils.substringAfterLast(from, "."));
		if (StringUtils.isEmpty(mimeType)) {
			mimeType = "";
		}
		return mimeType;
	}
}
