package com.clifton.document.cache;

import com.clifton.core.cache.CacheHandler;
import org.springframework.stereotype.Component;


@Component
public class DocumentIdCacheImpl implements DocumentIdCache {

	private CacheHandler<String, String> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDocumentId(String tableName, long fkFieldId) {
		return getCacheHandler().get(getCacheName(), tableName + fkFieldId);
	}


	@Override
	public void setDocumentId(String tableName, long fkFieldId, String documentId) {
		getCacheHandler().put(getCacheName(), tableName + fkFieldId, documentId);
	}


	@Override
	public void clearDocumentId(String tableName, long fkFieldId) {
		getCacheHandler().remove(getCacheName(), tableName + fkFieldId);
	}


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, String> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, String> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
