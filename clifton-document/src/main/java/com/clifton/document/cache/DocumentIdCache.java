package com.clifton.document.cache;

import com.clifton.core.cache.CustomCache;


/**
 * This class caches the DocumentRecord objects to help to deal with a 15 second delay between updates to Alfresco
 * and the Solr4 engine pulling in the latest transaction/updating the search index.
 * It allows for the documentId to be cached so that subsequent actions on a document do not need to request Alfresco.
 *
 * @author stevenf
 */
public interface DocumentIdCache extends CustomCache<String, String> {

	public String getDocumentId(String tableName, long fkFieldId);


	public void setDocumentId(String tableName, long fkFieldId, String documentId);


	public void clearDocumentId(String tableName, long fkFieldId);
}
