package com.clifton.document;


import com.clifton.core.dataaccess.file.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


/**
 * The <code>DocumentRecord</code> represents a Document object from another system.
 * <p>
 * Because the name Document is used by other systems (Alfresco), using the name DocumentRecord so
 * the code in the Manager is easier to read/manage.
 */
public class DocumentRecord {

	/**
	 * Used to flag that redLine = true.  Stripped from /added to checkin comment when true.
	 */
	public static final String RED_LINE_COMMENT_PREFIX = "[RED_LINE]";

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * ID of this document with the repository
	 */
	private String documentId;

	/**
	 * References the entity from out database,
	 * i.e. BusinessContract with ID of 5, DocumentFile with ID of 10
	 */
	private String tableName;
	private long fkFieldId;

	private String name;

	/**
	 * Populated on our side - not pulled from document management system
	 */
	private String folderName;

	/**
	 * If the file is currently checked out, this is the user
	 * that has checked it out.
	 */
	private String checkedOutUser;

	/**
	 * Displays information about the user
	 * that created the document and when.
	 */
	private String createUser;
	private Date createDate;

	/**
	 * Displays information about the last user
	 * that updated the document and when.
	 */
	private String updateUser;
	private Date updateDate;

	private boolean latestVersion;
	private boolean majorVersion;
	private String versionLabel;
	private String checkinComment;

	private boolean redLine;

	//ID for the collection of Document objects that were created from an original document.
	private String versionSeriesID;
	//True if there currently exists a Private Working Copy for this version series.
	private boolean versionSeriesCheckedOut;
	//If versionSeriesCheckedOut is true, then this contains the Identifier of the Private Working Copy
	private String versionSeriesCheckedOutId;

	/**
	 * Size of the file in bytes
	 */
	private Long fileSize;

	// Used to upload files to Documents
	private MultipartFile file;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFileExtension() {
		return FileUtils.getFileExtension(getName());
	}


	public String getRedLineLabel() {
		if (isRedLine()) {
			return RED_LINE_COMMENT_PREFIX;
		}
		return "";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isLatestVersion() {
		return this.latestVersion;
	}


	public void setLatestVersion(boolean latestVersion) {
		this.latestVersion = latestVersion;
	}


	public boolean isMajorVersion() {
		return this.majorVersion;
	}


	public void setMajorVersion(boolean majorVersion) {
		this.majorVersion = majorVersion;
	}


	public String getVersionLabel() {
		return this.versionLabel;
	}


	public void setVersionLabel(String versionLabel) {
		this.versionLabel = versionLabel;
	}


	public String getCheckinComment() {
		return this.checkinComment;
	}


	public void setCheckinComment(String checkinComment) {
		this.checkinComment = checkinComment;
	}


	public long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getCheckedOutUser() {
		return this.checkedOutUser;
	}


	public void setCheckedOutUser(String checkedOutUser) {
		this.checkedOutUser = checkedOutUser;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCreateUser() {
		return this.createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getUpdateUser() {
		return this.updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public String getDocumentId() {
		return this.documentId;
	}


	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	public String getVersionSeriesID() {
		return this.versionSeriesID;
	}


	public void setVersionSeriesID(String versionSeriesID) {
		this.versionSeriesID = versionSeriesID;
	}


	public boolean isVersionSeriesCheckedOut() {
		return this.versionSeriesCheckedOut;
	}


	public void setVersionSeriesCheckedOut(boolean versionSeriesCheckedOut) {
		this.versionSeriesCheckedOut = versionSeriesCheckedOut;
	}


	public String getVersionSeriesCheckedOutId() {
		return this.versionSeriesCheckedOutId;
	}


	public void setVersionSeriesCheckedOutId(String versionSeriesCheckedOutId) {
		this.versionSeriesCheckedOutId = versionSeriesCheckedOutId;
	}


	public String getFolderName() {
		return this.folderName;
	}


	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public boolean isRedLine() {
		return this.redLine;
	}


	public void setRedLine(boolean redLine) {
		this.redLine = redLine;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
}
