package com.clifton.document;


import com.clifton.core.dataaccess.file.DocumentContentStream;

import java.util.List;


/**
 * The <code>DocumentManagementProvider</code> defines the methods for retrieving & editing
 * documents from an outside system.
 *
 * @author manderson
 */
public interface DocumentManagementProvider<T> {

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Document Content Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentContentStream getDocumentContentStream(String documentId);


	public DocumentContentStream getDocumentContentStream(T document);


	/**
	 * Copies document content between to documents
	 * Used when restoring a document to a previous version
	 */
	public void copyDocumentContentStream(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord);


	////////////////////////////////////////////////////////////////////////////////
	//////////////                 Document  Methods                ////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the {@link T} document in the document management system with the given documentId
	 */
	public T getDocument(String documentId);


	/**
	 * Returns the {@link T} document in the document management system with the given documentId
	 */
	public T getRendition(String documentId, String type);


	public T getRendition(T document, String type);


	/**
	 * Returns the {@link T} document in the document management system associated with the given table/fkField combination.
	 * <p>
	 * For definitions that don't support one per entity the values passed should be DocumentFile and DocumentFileID
	 * For those that do, the tableName/fkFieldId is of the actual entity, i.e. BusinessContract, BusinessContractID
	 */
	public T getDocumentByTableAndFkField(String tableName, long fkFieldId);


	/**
	 * Creates a new document in the document management system from the given document record.
	 */
	public T createDocument(DocumentRecord documentRecord);


	/**
	 * Updates the content stream of the document based upon the the given upload file.
	 *
	 * @param changedFileExtension - Updates document name with new extension if file extension changed - i.e.  doc to docx and vice versa
	 */
	public T updateDocument(DocumentRecord documentRecord, String changedFileExtension);


	public void updateDocumentProperties(T document, String newTableName, long newFkFieldId);


	/**
	 * Copies an existing document
	 */
	public T copyDocument(DocumentRecord fromDocumentRecord, DocumentRecord toDocumentRecord);


	public T checkoutDocument(T document);


	public T cancelCheckoutDocument(T document);


	public T checkinDocument(DocumentRecord documentRecord);


	public void deleteDocument(T document);


	////////////////////////////////////////////////////////////////////////////////
	///////////                    WebDav Methods                     //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the WebDavUrl for editing the document online
	 * <p>
	 * Alfresco uses two implementations of WebDAV:
	 * RFC-compliant WebDAV: alfresco/webdav
	 * Microsoft-compliant WebDAV: alfresco/aos
	 * Microsoft WebDAV extensions (MS-DAVEXT) are only partially compatible with the WebDAV standard,
	 * therefore it is recommended that you use /alfresco/aos on Windows clients and /alfresco/webdav on Linux-based systems.
	 */
	public String getDocumentWebdavUrl(DocumentRecord documentRecord);

	////////////////////////////////////////////////////////////////////////////////
	///////////               Document Version Methods                //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of all versions of the specified document.
	 * i.e. pwc (current checked out working copy), v1, v2, etc.
	 * from check in history
	 */
	public List<T> getDocumentVersionList(T document);
}
