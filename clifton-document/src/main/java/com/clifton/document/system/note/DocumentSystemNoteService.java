package com.clifton.document.system.note;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.system.note.SystemNote;


/**
 * The <code>DocumentNoteService</code> supports working with SystemNotes and Documents (Attachments) to
 * those notes directly.
 *
 * @author manderson
 */
public interface DocumentSystemNoteService {

	/**
	 * Creates SystemNote and immediately attaches document attachment to it.
	 * Validates SystemNote supports attachments, etc.
	 * <p>
	 * Requires table name, FKFieldID (so we know which entity the note will belong to) and that the selected
	 * note type allows attachments.  Optional note text (unless note type requires additional text)
	 * Requires a file as well.
	 */
	@SecureMethod(dynamicTableNameBeanPath = "noteType.table.name", dtoClass = SystemNote.class, dynamicSecurityResourceTableNameSuffix = "Note", permissionResolverBeanName = "systemNoteSecureMethodPermissionResolver")
	public void uploadDocumentSystemNote(SystemNote systemNote, FileUploadWrapper fileWrapper);


	/**
	 * Called from code where customized system note logic can be held. (i.e. TradeNoteService)
	 * THIS WILL NOT SAVE THE NOTE, assumes it has already been saved, but will create the correct
	 * DocumentRecord or DocumentFile object and attach the file to it appropriately if the note type supports one or multiple attachments
	 */
	@DoNotAddRequestMapping
	public void saveDocumentSystemNoteAttachment(SystemNote systemNote, FileUploadWrapper fileWrapper);
}
