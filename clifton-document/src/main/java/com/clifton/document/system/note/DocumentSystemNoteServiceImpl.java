package com.clifton.document.system.note;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class DocumentSystemNoteServiceImpl implements DocumentSystemNoteService {

	private DocumentSetupService documentSetupService;
	private DocumentManagementService documentManagementService;
	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////////

	private static final String SYSTEM_NOTE_MULTIPLE_ATTACHMENTS_DEFINITION_NAME = "SystemNoteAttachments";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void uploadDocumentSystemNote(SystemNote systemNote, FileUploadWrapper fileWrapper) {
		ValidationUtils.assertNotNull(systemNote.getNoteType(), "Note Type is Required");
		ValidationUtils.assertNotNull(systemNote.getNoteType().getId(), "Note Type is Required");
		ValidationUtils.assertNotNull(systemNote.getFkFieldId(), "Fk Field ID is Required.");
		ValidationUtils.assertNotNull(fileWrapper, "File is Required.");
		ValidationUtils.assertNotNull(fileWrapper.getFile(), "File is Required.");

		systemNote.setNoteType(getSystemNoteService().getSystemNoteType(systemNote.getNoteType().getId()));

		ValidationUtils.assertFalse(DocumentAttachmentSupportedTypes.NONE == systemNote.getNoteType().getAttachmentSupportedType(), "Selected note type [" + systemNote.getNoteType().getName()
				+ "] does not support file attachments.");
		if (!systemNote.getNoteType().isBlankNoteAllowed()) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(systemNote.getText()), "Selected note type [" + systemNote.getNoteType().getName() + "] requires note text to be entered.");
		}

		// Create the Note
		systemNote = getSystemNoteService().saveSystemNote(systemNote);
		saveDocumentSystemNoteAttachment(systemNote, fileWrapper);
	}


	@Override
	public void saveDocumentSystemNoteAttachment(SystemNote systemNote, FileUploadWrapper fileWrapper) {
		// Single Note Attachment
		if (systemNote.isSingleAttachmentAllowed()) {
			DocumentRecord record = new DocumentRecord();
			record.setFile(fileWrapper.getFile());
			record.setTableName(SystemNote.SYSTEM_NOTE_TABLE_NAME);
			record.setFkFieldId(systemNote.getId());
			getDocumentManagementService().uploadDocumentRecord(record);
		}
		// Otherwise a Multiple File Attachment Note
		else {
			DocumentFile df = new DocumentFile();
			df.setFile(fileWrapper.getFile());
			df.setFkFieldId(BeanUtils.getIdentityAsLong(systemNote));
			df.setDefinition(getDocumentSetupService().getDocumentDefinitionByName(SYSTEM_NOTE_MULTIPLE_ATTACHMENTS_DEFINITION_NAME));
			getDocumentManagementService().uploadDocumentFile(df);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}
}
