package com.clifton.document;


import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.document.setup.DocumentSetupService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;


/**
 * The <code>DocumentManagerObserver</code> handles all DAO Validation and Document related events
 * attached to any {@link DocumentAware} bean that utilizes Document Management Functionality.
 * <p>
 * This includes:
 * Deleting Documents related to given bean
 *
 * @author manderson
 */
@Component
public class DocumentManagerObserver<T extends DocumentAware> extends BaseDaoEventObserver<T> {

	private DocumentManagementService documentManagementService;
	private DocumentSetupService documentSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") T bean) {
		// NOTHING FOR NOW
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null && DaoEventTypes.DELETE == event) {
			//If this index in Alfresco is not updated then the getDocumentRecord will fail to return the document
			//i.e. if the user just uploaded, then closed the screen and attempts to delete the row prior to the index updating.
			//TODO - add a sleep/retry here? or?  Should be very rare.

			// Don't delete the document until the transaction is committed successfully
			// If there is a rollback the document is still deleted from Alfresco, but not IMS which causes an object not found error
			getDocumentSetupService().deleteDocumentSecurityRestrictionListForDocument(dao.getConfiguration().getTableName(), (Integer) bean.getIdentity());

			if (TransactionSynchronizationManager.isSynchronizationActive()) {
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {

					@Override
					public void afterCompletion(int status) {
						if (status == TransactionSynchronization.STATUS_COMMITTED) {
							doDeleteDocument(dao, bean);
						}
					}
				});
			}
			else {
				doDeleteDocument(dao, bean);
			}
		}
	}


	private void doDeleteDocument(ReadOnlyDAO<T> dao, T bean) {
		DocumentRecord document = getDocumentManagementService().getDocumentRecord(dao.getConfiguration().getTableName(), (Integer) bean.getIdentity());
		if (document != null && document.getDocumentId() != null) {
			getDocumentManagementService().deleteDocumentRecord(document.getDocumentId(), document.getTableName(), document.getFkFieldId(), true);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}
}
