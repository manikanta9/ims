Clifton.document.system.note.SingleNoteWithDocumentCreateWindow = Ext.extend(TCG.file.DragAndDropPopupWindow, {
	title: 'System Note with Attachment Window',
	iconCls: 'attach',
	height: 400,
	width: 600,

	okButtonTooltip: 'Create Note with Upload File',

	saveUrl: 'documentSystemNoteUpload.json',
	formItems: [
		{fieldLabel: 'FKFieldID', name: 'fkFieldId', xtype: 'hidden'},
		{name: 'columnGroupName', xtype: 'hidden', value: 'System Note Custom Fields'}, // I think because of items dynamically being added this doesn't appear to populate, so manually setting for now

		// Used to limit selections to specific tag
		{name: 'categoryHierarchyId', xtype: 'hidden', submitValue: false},
		{fieldLabel: 'Table', name: 'noteType.table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'noteType.table.id', submitDetailField: false},
		{
			fieldLabel: 'Note Type', name: 'noteType.name', hiddenName: 'noteType.id', xtype: 'combo', detailIdField: 'noteType.id', detailPageClass: 'Clifton.system.note.TypeWindow', url: 'systemNoteTypeListFind.json?excludeAttachmentSupportedType=NONE',
			requiredFields: ['noteType.table.name'],
			beforequery: function(queryEvent) {
				const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
				const params = {};
				params.tableName = TCG.getValue('noteType.table.name', form.formValues);
				if (TCG.getValue('categoryHierarchyId', form.formValues)) {
					params.categoryName = 'System Note Type Tags';
					params.categoryHierarchyId = TCG.getValue('categoryHierarchyId', form.formValues);
				}
				queryEvent.combo.store.baseParams = params;
			},
			getDefaultData: function(f) { // defaults table for "add new" drill down
				return {
					table: TCG.getValue('noteType.table', f.formValues)
				};
			},
			// reset document related fields and toolbar based on selection
			selectHandler: function(combo, record, index) {
				combo.ownerCt.getWindow().resetNoteTypeFields(combo.ownerCt, record.json);
			}
		},
		{fieldLabel: 'Text', name: 'text', xtype: 'textarea'},

		{xtype: 'label', html: '<hr/>'},
		{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', allowNegative: false},
		{name: 'privateNote', xtype: 'checkbox', boxLabel: 'Private Note (Should be viewed by internal employees only)'},
		{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
		{fieldLabel: 'End Date', xtype: 'datefield', name: 'endDate'},

		{xtype: 'label', html: '<hr/>'}
	],

	resetNoteTypeFields: function(fp, record) {
		if (record) {
			const f = fp.getForm();
			const orderField = f.findField('order');
			const orderSupported = record.orderSupported;
			orderField.setDisabled(!orderSupported);
			orderField.setVisible(orderSupported);
			if (!orderSupported) {
				orderField.setValue('');
			}

			const privateField = f.findField('privateNote');
			if (record.internal === true) {
				privateField.setValue(true);
				privateField.setDisabled(true);
			}
			else {
				privateField.setDisabled(false);
			}

			const startDateField = f.findField('startDate');
			startDateField.setDisabled(!record.dateRangeAllowed);
			startDateField.setVisible(record.dateRangeAllowed);

			const endDateField = f.findField('endDate');
			if (record.dateRangeAllowed === true || record.endDateResolutionDate === true) {
				endDateField.setDisabled(false);
				endDateField.setVisible(true);
				let lbl = 'End Date:';
				if (record.endDateResolutionDate === true) {
					lbl = 'Resolution Date: ';
				}
				endDateField.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(lbl);
			}
			else {
				endDateField.setValue('');
				endDateField.setDisabled(true);
				endDateField.setVisible(false);
			}

			const noteTextField = f.findField('text');
			if (record.defaultNoteTextPopulated === true && noteTextField.getValue() === '') {
				noteTextField.setValue(record.defaultNoteText);
			}
			noteTextField.allowBlank = (record.blankNoteAllowed === true);
		}
	}
});
