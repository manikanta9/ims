Ext.ns('Clifton.document', 'Clifton.document.system', 'Clifton.document.system.note', 'Clifton.document.setup', 'Clifton.document.enableDD');


Clifton.document.DocumentToolbar = Ext.extend(TCG.toolbar.Toolbar, {
	tableName: 'REQUIRED-TABLE-NAME',
	docRecord: undefined, // Populated during loadDocument()
	currentUser: undefined, // Populated during loadCurrentUser() & only loaded if docRecord.checkedOutUser is not null
	formatText: undefined, // Toolbar.TextItem - Populated during first load of document that exists (sets format type & file info)

	showPDFButton: false,
	hideRemoveButton: false, // Used for DocumentFile windows where it doesn't make sense to delete just the document, would delete the DocumentFile record
	hideViewButtons: false, // Used for Document Version History grid where we don't need extra view buttons because you can view each version already
	hideFileFormat: false, // Used for Document Version History grid where we don't want to show the format - i.e. Contact Photo
	hideSecurityButton: false, // Used for specific cases where we don't want to show/use custom security restrictions (uses too much space in toolbar) - i.e. Contact Photo
	hideEditOnlineButton: false, // Used for specific cases where Edit online wouldn't be used, so reduce the clutter in the toolbar - i.e. Contact Photo

	getDocumentParams: function() {
		return {
			tableName: this.tableName,
			fkFieldId: this.getEntityId(),
			documentId: this.getDocumentId()
		};
	},

	getDocumentWorkingCopyParams: function() {
		return {
			tableName: this.tableName,
			fkFieldId: this.getEntityId(),
			documentId: this.getDocumentWorkingCopyId()
		};
	},

	getDocumentId: function() {
		if (this.docRecord) {
			return this.docRecord.documentId;
		}
		return undefined;
	},

	getDocumentWorkingCopyId: function() {
		if (this.docRecord) {
			return this.docRecord.versionSeriesCheckedOutId;
		}
		return undefined;
	},

	getEntityId: function() {
		const f = this.getMainFormPanel();
		const w = f.getWindow();
		return w.getMainFormId();
	},


	initComponent: function() {
		const tb = this;

		this.viewMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'View Latest Version',
					tooltip: 'Download latest version for viewing',
					iconCls: 'view',
					handler: function() {
						tb.downloadDocument(false);
					}
				},
				{
					text: 'View Working Copy',
					tooltip: 'Download latest working copy file for viewing',
					iconCls: 'pencil',
					handler: function() {
						tb.downloadDocument(true);
					}
				}
			]
		});

		this.viewPDFSubMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Latest Version',
					tooltip: 'Download latest version for viewing as a PDF document',
					iconCls: 'pdf',
					handler: function() {
						tb.downloadDocument(false, 'pdf');
					}
				},
				{
					text: 'Working Copy',
					tooltip: 'Download working copy for viewing as a PDF document.',
					iconCls: 'pdf',
					handler: function() {
						tb.downloadDocument(true, 'pdf');
					}
				}
			]
		});

		this.viewPDFMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'View',
					tooltip: 'Download latest version of this document in its original format for viewing',
					iconCls: 'view',
					handler: function() {
						tb.downloadDocument(false);
					}
				},
				'-',
				{
					text: 'View PDF',
					tooltip: 'Download latest version of this document (converted to PDF) for viewing',
					iconCls: 'pdf',
					handler: function() {
						tb.downloadDocument(false, 'pdf');
					}
				}]
		});

		this.viewPDFCheckOutMenu = ['-',
			{
				text: 'View PDF',
				tooltip: 'Download latest version of this document (converted to PDF) for viewing',
				iconCls: 'pdf',
				handler: function() {
					tb.downloadDocument(false, 'pdf');
				},
				menu: this.viewPDFSubMenu
			}];


		this.items = [{
			text: 'Loading Document Information...',
			xtype: 'button',
			iconCls: 'loading',
			enabledFor: ['none']
		},
			{xtype: 'tbseparator', hidden: true}];

		if (this.hideViewButtons !== true) {
			this.items.push({
				text: 'View',
				xtype: 'splitbutton',
				tooltip: 'Download latest version of this document for viewing',
				iconCls: 'view',
				hidden: true,
				enabledFor: ['checkout', 'view', 'checkin'],
				handler: function() {
					tb.downloadDocument(false);
				}
			});
			this.items.push({xtype: 'tbseparator', hidden: true});

			this.items.push({
				text: 'View PDF',
				xtype: 'splitbutton',
				tooltip: 'Download latest version of this document (converted to PDF) for viewing',
				iconCls: 'pdf',
				hidden: true,
				enabledFor: ['checkout', 'view', 'checkin'],
				handler: function() {
					tb.downloadDocument(false, 'pdf');
				}
			});
			this.items.push({xtype: 'tbseparator', hidden: true});
		}

		this.items.push({
			// Document is not currently checked out - allow user to check it out
			text: 'Check Out',
			tooltip: 'Lock this document for editing and prevent others from making changes to it',
			iconCls: 'lock',
			hidden: true,
			enabledFor: ['checkout'],
			handler: function() {
				tb.executeDocumentCheckout(true);
			}
		});
		this.items.push({xtype: 'tbseparator', hidden: true});
		this.items.push({
			// Current user has this document checked out.  Allow check in/editing
			text: 'Cancel Check Out',
			tooltip: 'Undo changes made since last check out and unlock the document',
			iconCls: 'lock-delete',
			hidden: true,
			enabledFor: ['checkin'],
			handler: function() {
				tb.executeDocumentCheckout(false);
			}
		});
		this.items.push({xtype: 'tbseparator', hidden: true});
		this.items.push({
			text: 'Check In',
			tooltip: 'Submit your changes to the document and unlock it to allow others make new changes',
			iconCls: 'unlock',
			hidden: true,
			enabledFor: ['checkin'],
			handler: function() {
				tb.executeDocumentCheckin();
			}
		});
		this.items.push({xtype: 'tbseparator', hidden: true});

		if (this.hideEditOnlineButton !== true) {
			this.items.push({
				text: 'Edit',
				tooltip: 'Open this document for online editing (saving the document automatically uploads your changes)',
				iconCls: 'pencil',
				hidden: true,
				enabledFor: ['checkin'],
				handler: function() {
					tb.executeDocumentEdit();
				}
			});
			this.items.push({xtype: 'tbseparator', hidden: true});
		}
		this.items.push({
			text: 'Upload',
			tooltip: 'Upload changes made to the document (replaces last revision but keeps revision history)',
			iconCls: 'upload',
			hidden: true,
			enabledFor: ['new', 'checkin'],
			handler: function() {
				tb.executeDocumentUpload();
			}
		});
		this.items.push({xtype: 'tbseparator', hidden: true});
		if (this.hideRemoveButton !== true) {
			this.items.push({
				text: 'Remove',
				tooltip: 'Remove the associated document (Also deletes all version history for the document)',
				iconCls: 'remove',
				hidden: true,
				enabledFor: ['checkout'],
				handler: function() {
					tb.executeDocumentDelete();
				}
			});
			this.items.push({xtype: 'tbseparator', hidden: true});
		}
		if (this.hideSecurityButton !== true) {
			this.items.push({
				text: 'Security',
				tooltip: 'Override Security and specify users or groups that can view this document',
				iconCls: 'key',
				hidden: true,
				enabledFor: ['checkout', 'view', 'checkin'],
				handler: function() {
					tb.executeDocumentSecurity();
				}
			});
			this.items.push({xtype: 'tbseparator', hidden: true});
		}


		// Can be overridden for additional functionality
		this.addToolbarButtons();

		Clifton.document.DocumentToolbar.superclass.initComponent.call(this);
	},

	downloadDocument: function(workingCopy, conversionType) {
		const params = (workingCopy ? this.getDocumentWorkingCopyParams() : this.getDocumentParams());
		if (TCG.isNull(conversionType) || this.docRecord.fileExtension === conversionType) {
			// Regular Download
			TCG.downloadFile('documentRecordDownload.json', params, this);
		}
		else {
			// Convert, then Download
			TCG.downloadFile('documentRecordConversionDownload.json?type=' + conversionType, params, this);
		}
	},

	loadDocumentWarningMessage: function() {
		let msg = undefined;
		let checkedOutUser = this.docRecord.checkedOutUser;
		if (TCG.isNotBlank(checkedOutUser)) {
			checkedOutUser = checkedOutUser.toLowerCase();
			const updateDate = TCG.parseDate(this.docRecord.updateDate);
			msg = 'This document is currently checked out by <b>' + checkedOutUser + '</b> (checked out date: ' + TCG.renderDate(updateDate) + ')';
		}
		if (this.docWarningMsg !== msg) {
			this.docWarningMsg = msg;
			const docWarnId = this.id + '-docwarn';
			const docWarn = Ext.get(docWarnId);
			if (TCG.isNotNull(docWarn)) {
				this.remove(docWarnId);
				Ext.destroy(docWarn);
				this.doLayout();
			}
			if (TCG.isNotNull(msg)) {
				if (!Ext.isArray(msg)) {
					const html = msg;
					msg = [];
					msg[0] = {xtype: 'label', html: html};
				}
				const f = this.getMainFormPanel();
				f.insert(0, {xtype: 'container', layout: 'hbox', id: docWarnId, autoEl: 'div', cls: 'warning-msg', items: msg});
				f.doLayout();
			}
		}
	},


	loadDocument: function() {
		const tb = this;
		const docLoader = new TCG.data.JsonLoader({
			timeout: 30000, // first call can be slow
			params: this.getDocumentParams(),
			onLoad: function(record, conf) {
				if (record) {
					tb.docRecord = record;
					tb.loadCurrentUser();
				}
				else {
					tb.docRecord = undefined;
					tb.loadDocumentSpecificItems();
				}
			}
		});
		docLoader.load('documentRecord.json', this);
	},

	loadCurrentUser: function() {
		const tb = this;
		const docRecord = this.docRecord;
		// Only need the current user if the document is actually checked out.
		if (docRecord && TCG.isNotBlank(docRecord.checkedOutUser)) {
			// Only pull the current user again if it isn't set
			if (TCG.isBlank(this.currentUser)) {
				this.currentUser = TCG.getCurrentUser().userName;
			}
		}

		// Load Document Specific Items & Enable/Disable the Toolbar Buttons
		tb.loadDocumentSpecificItems();
	},

	loadDocumentSpecificItems: function() {
		if (TCG.isNull(this.docRecord)) {
			// No Document Created Yet - Allow Upload Only, Clear formatText in case document was deleted
			this.createTopToolbar('new');
		}
		else {
			const docRecord = this.docRecord;
			this.loadDocumentWarningMessage();

			if (TCG.isBlank(docRecord.checkedOutUser)) {
				this.createTopToolbar('checkout');
			}
			else if (docRecord.checkedOutUser.toLowerCase() !== this.currentUser.toLowerCase()) {
				this.createTopToolbar('view');
			}
			else {
				this.createTopToolbar('checkin');
			}
		}
	},
	createTopToolbar: function(type) {
		const tb = this;

		// Add 2 as we loop through because of the Spacers
		for (let i = 0; i < tb.items.length; i = i + 2) {
			const button = tb.items.items[i];
			// Last two, if they are there - are the filler and the document format icon
			if (!button.isFill) {
				this.enableDisableButton(button, type);
				const spacer = tb.items.items[i + 1];
				if (spacer) {
					if (button.isVisible() !== spacer.isVisible()) {
						spacer.setVisible(button.isVisible());
					}
				}
			}
		}

		if (this.hideFileFormat !== true) {
			if (type === 'new') {
				if (this.formatText) {
					tb.remove(this.formatText);
				}
				this.formatText = undefined;
			}
			else if (TCG.isNull(this.formatText)) {
				tb.addFill();
				const iconCls = tb.getFileTypeIconCls();
				const formatTooltip = tb.getFileTypeDescription() + ': ' + tb.docRecord.folderName + '/' + tb.docRecord.name;
				if (TCG.isNotNull(iconCls)) {
					tb.formatText = tb.addText('<span title="' + formatTooltip + '">Format:&nbsp;<span class="' + iconCls + '""> &nbsp; &nbsp; &nbsp;</span></span>');
				}
				else {
					tb.formatText = tb.addText('<span title="' + formatTooltip + '">Format:&nbsp;' + tb.docRecord.fileExtension + '</span>');
				}
				tb.doLayout();
			}
		}
	},


	addToolbarButtons: function() {
		// Can be overridden to add additional buttons
		// Should also implement isButtonEnabled method to handle the extra buttons added
	},


	getFileTypeDescription: function() {
		return TCG.getDocumentFileTypeDescription(this.docRecord.fileExtension);
	},

	getFileTypeIconCls: function() {
		return TCG.getDocumentFileTypeIconClass(this.docRecord.fileExtension);
	},


	isConvertToPDF: function() {
		if (this.docRecord) {
			const ext = this.docRecord.fileExtension;
			if (ext) {
				return ext.indexOf('doc') === 0 || ext.indexOf('xls') === 0 || ext.indexOf('ppt') === 0;
			}
		}
		return false;
	},

	isButtonEnabled: function(button, type) {
		// If toolbar is disabled, then everything should be considered "disabled"
		if (this.disabled === true) {
			return false;
		}
		if (button.validateEnabled) {
			return button.validateEnabled(button, type);
		}
		else if (button.enabledFor) {
			return button.enabledFor.indexOf(type) >= 0;
		}
		// If there are no enabledFor or validateEnabled, consider the button to always be displayed
		return true;
	},

	enableDisableButton: function(button, type) {
		if (!button || !button.text) {
			return;
		}
		let enabled = this.isButtonEnabled(button, type);

		if (enabled && button.text === 'View') {
			if (type === 'checkin') {
				button.menu = this.viewMenu;
				if (!this.showPDFButton && this.isConvertToPDF() === true) {
					button.menu.add(this.viewPDFCheckOutMenu);
				}
			}
			else if (!this.showPDFButton && this.isConvertToPDF() === true) {
				button.menu = this.viewPDFMenu;
			}
			else {
				button.menu = undefined;
			}
		}

		if (enabled && button.text === 'View PDF') {
			// added above as a menu item above to View Button
			if (!this.showPDFButton) {
				enabled = false;
			}
			// otherwise verify convert to pdf and show the button & add menus if necessary
			else {
				if (this.isConvertToPDF() === false) {
					enabled = false;
				}
				else {
					if (type === 'checkin') {
						button.menu = this.viewPDFSubMenu;
					}
					else {
						button.menu = undefined;
					}
				}
			}
		}
		else if (button.text === 'Upload') {
			if (enabled) {
				// allow drag and drop files to container panel
				TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), this.getDocumentWorkingCopyParams(), this, 'documentRecordUpload.json');
			}
			else {
				// disable drag and drop
				TCG.file.disableDD(TCG.getParentByClass(button, Ext.Panel));
			}
		}

		else if (enabled && button.text === 'Remove') {
			if (this.hideRemoveButton) {
				enabled = false;
			}
		}

		// Show Hide the Button
		button.setVisible(enabled);
	},

	executeDocumentAction: function(url, waitMsg, params) {
		// Disable all toolbar items while document methods are executed
		this.createTopToolbar('none');
		const f = this.ownerCt.ownerCt.items.get(0);
		const tb = this;
		const loader = new TCG.data.JsonLoader({
			waitTarget: f,
			waitMsg: waitMsg,
			params: params,
			conf: params,
			onLoad: function(record, conf) {
				tb.docRecord = record;
				tb.reloadAfterDocumentAction();
			},
			onFailure: function() {
				// Reload the Toolbar based on existing document record
				tb.reloadAfterDocumentAction();
			}
		});
		loader.load(url);
	},

	reloadAfterDocumentAction: function() {
		this.loadCurrentUser();
		this.refreshWindow();

	},

	refreshWindow: function() {
		const fp = this.getMainFormPanel();
		if (fp && fp.getWindow && fp.getWindow().refreshWindow) {
			fp.getWindow().refreshWindow();
		}
	},

	reload: function() {
		this.refreshWindow();
		this.loadDocument();
	},


	executeDocumentCheckout: function(checkout) {
		let params = this.getDocumentParams();
		let url = 'documentRecordCheckout.json';
		let waitMsg = 'Checking Out...';
		if (!checkout) {
			url = 'documentRecordCancelCheckout.json';
			waitMsg = 'Canceling Check Out...';
			params = this.getDocumentWorkingCopyParams();
		}

		this.executeDocumentAction(url, waitMsg, params);
	},


	executeDocumentDelete: function() {
		const t = this;
		Ext.Msg.confirm('Confirm Document Removal', 'Are you sure you want to delete the associated document?  All version history of the document will also be deleted', function(a) {
			if (a === 'yes') {
				const params = t.getDocumentParams();
				const url = 'documentRecordDelete.json';
				const waitMsg = 'Removing...';
				t.executeDocumentAction(url, waitMsg, params);
			}
		});
	},

	executeDocumentCheckin: function() {
		TCG.createComponent('Clifton.document.DocumentCheckinWindow', {
			defaultData: this.getDocumentWorkingCopyParams(),
			openerCt: this
		});
	},

	executeDocumentUpload: function() {
		TCG.createComponent('Clifton.document.DocumentUploadWindow', {
			defaultData: this.getDocumentWorkingCopyParams(),
			openerCt: this
		});
	},

	executeDocumentEdit: function() {
		const tb = this;
		// Prepend with the office URI scheme for online editing 'ms-word:ofe|u|'
		// see: https://msdn.microsoft.com/en-us/library/office/dn906146.aspx
		const url = 'ms-word:ofe|u|' + TCG.getResponseText('documentRecordWebDavUrl.json?documentId=' + tb.getDocumentWorkingCopyId(), this);
		TCG.enableConfirmBeforeLeavingWindow(true);
		window.setTimeout(TCG.enableConfirmBeforeLeavingWindow, 1000);
		window.open(url, '_self');
	},

	executeDocumentSecurity: function() {
		TCG.createComponent('Clifton.document.DocumentSecurityRestrictionWindow', {
			defaultData: this.getDocumentParams(),
			openerCt: this
		});
	},

	onRender: function() {
		Clifton.document.DocumentToolbar.superclass.onRender.apply(this, arguments);
		const f = this.getMainFormPanel();
		f.on('afterload', this.loadDocument, this);
	},

	getMainFormPanel: function() {
		return this.ownerCt.items.get(0);
	}

});
Ext.reg('document-toolbar', Clifton.document.DocumentToolbar);


Clifton.document.DocumentVersionGrid = Ext.extend(TCG.grid.GridPanel, {
	tableName: 'OVERRIDE-ME',
	name: 'documentRecordVersionList',
	instructions: 'Revision history lists each change that was made for selected document.',
	showEditToolbar: false, // Set to true to include check out/check in, etc. buttons
	// If showEditToolbar is true, can use the following to customize the document toolbar buttons displayed
	hideFileFormat: false,
	hideSecurityButton: false,
	hideEditOnlineButton: false,

	initComponent: function() {
		const gp = this;
		if (this.showEditToolbar === true) {
			this.tbar = {
				xtype: 'document-toolbar',
				tableName: gp.tableName,
				hideViewButtons: true, // Always hidden since we already have view option for each version
				hideFileFormat: gp.hideFileFormat,
				hideSecurityButton: gp.hideSecurityButton,
				hideEditOnlineButton: gp.hideEditOnlineButton,
				onRender: function() {
					Clifton.document.DocumentToolbar.superclass.onRender.apply(this, arguments);
					this.loadDocument();
				},
				getEntityId: function() {
					const w = gp.getWindow();
					return w.getMainFormId();
				},
				reload: function() {
					this.loadDocument();
					gp.reload();
				},
				reloadAfterDocumentAction: function() {
					this.loadCurrentUser();
					gp.reload();
				}
			};
		}
		Clifton.document.DocumentVersionGrid.superclass.initComponent.apply(this, arguments);
	},

	columns: [
		{header: 'Document ID', width: 30, dataIndex: 'documentId', hidden: true},
		{header: 'Version', width: 40, dataIndex: 'versionLabel'},
		{header: 'Red Line', width: 40, dataIndex: 'redLine', type: 'boolean'},
		{header: 'Comments', width: 200, dataIndex: 'checkinComment'},
		{
			header: 'Size', width: 35, dataIndex: 'fileSize',
			renderer: function(v, metaData, r) {
				return TCG.fileSize(v);
			}
		},
		{header: 'Changed By', width: 60, dataIndex: 'updateUser'},
		{header: 'Changed On', width: 80, dataIndex: 'updateDate'}
	],
	getLoadParams: function() {
		return {
			tableName: this.tableName,
			fkFieldId: this.getWindow().getMainFormId()
		};
	},
	editor: {
		addEnabled: false,
		deleteEnabled: false,
		getViewParams: function(selectionModel) {
			return {documentId: selectionModel.getSelected().id};
		},

		addEditButtons: function(t, gridPanel) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
			t.add({
				text: 'Restore',
				tooltip: 'Restore Selected Version',
				iconCls: 'undo',
				enabledFor: ['checkout'], // When used with document toolbar - only enabled when document is available for checkout
				scope: this,
				handler: function() {
					const editor = this;
					const grid = this.grid;
					const sm = grid.getSelectionModel();

					if (sm.getCount() === 0) {
						TCG.showError('Please select a version to restore.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection restorations are not supported.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						Ext.Msg.confirm('Confirm Restore Version', 'Are you sure you want to restore the selected version as the latest version for this document?', function(a) {
							if (a === 'yes') {
								const params = editor.getGridPanel().getLoadParams();
								params['documentId'] = sm.getSelected().get('documentId');
								TCG.createComponent('Clifton.document.DocumentRestoreVersionWindow', {
									defaultData: params,
									openerCt: gridPanel
								});
							}
						});
					}
				}
			});
			t.add('-');

			t.add({
				text: 'View',
				tooltip: 'Download selected file version for viewing',
				iconCls: 'view',
				enabledFor: ['checkout', 'view', 'checkin'], // When used with document toolbar - will only display view button if there is something to view
				scope: this,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();

					if (sm.getCount() === 0) {
						TCG.showError('Please select a version to download.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection downloads are not supported.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						const params = {documentId: sm.getSelected().get('documentId')};
						TCG.downloadFile('documentRecordDownload.json', params, gridPanel);
					}
				}
			});
			t.add('-');

			t.add({
				text: 'View PDF',
				tooltip: 'View file for selected version converted to PDF.  If conversion is not possible, the original document will be returned.',
				iconCls: 'pdf',
				hidden: true,
				scope: this,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a version to download.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection downloads are not supported.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						const params = {documentId: sm.getSelected().get('documentId'), type: 'pdf'};

						const format = TCG.getValue('documentFileType', gridPanel.getWindow().getMainForm().formValues);
						if (!format) {
							TCG.showError('Unable to determine document Format for selected row.', 'MISSING FORMAT');
						}
						if (format.indexOf('doc') === 0 || format.indexOf('xls') === 0 || format.indexOf('ppt') === 0) {
							TCG.downloadFile('documentRecordConversionDownload.json', params, gridPanel);
						}
						else if (format.indexOf('pdf') === 0) {
							// If already a pdf - just download it
							TCG.downloadFile('documentRecordDownload.json', params, gridPanel);
						}
						else {
							TCG.showError('Only Documents of type [doc, xls, ppt, pdf] can be downloaded as a pdf document.', 'NOT SUPPORTED');
						}
					}
				}
			});
			t.add('-');
		},

		startEditing: function(grid, rowIndex, evt) {
			const row = grid.store.data.items[rowIndex];
			const params = {documentId: row.get('documentId')};
			const gridPanel = this.getGridPanel();
			TCG.downloadFile('documentRecordDownload.json', params, gridPanel);
		}
	}
});
Ext.reg('document-version-grid', Clifton.document.DocumentVersionGrid);


Clifton.document.DownloadDocumentsAsZip = function(tableName, grid, fkFieldIds) {
	const params = {'tableName': tableName, 'fkFieldIds': fkFieldIds};
	TCG.downloadFile('documentRecordsAsZipDownload.json', params, grid);
};


Clifton.document.DownloadDocument = function(tableName, grid, fkFieldId) {
	const params = {'tableName': tableName, 'fkFieldId': fkFieldId};
	const docLoader = new TCG.data.JsonLoader({
		timeout: 30000, // can be slow
		params: params,
		onLoad: function(record, conf) {
			if (record) {
				TCG.downloadFile('documentRecordDownload.json', {'documentId': record.documentId}, grid);
			}
			else {
				TCG.showError('There is no document associated with that record.');
			}
		}
	});
	docLoader.load('documentRecord.json', grid);
};


Clifton.document.DownloadConvertedDocument = function(tableName, grid, fkFieldId, format) {
	const params = {'tableName': tableName, 'fkFieldId': fkFieldId};
	const docLoader = new TCG.data.JsonLoader({
		timeout: 30000, // can be slow
		params: params,
		onLoad: function(record, conf) {
			if (record) {
				TCG.downloadFile('documentRecordConversionDownload.json?type=' + format, {'documentId': record.documentId}, grid);
			}
			else {
				TCG.showError('There is no document associated with that record.');
			}
		}
	});
	docLoader.load('documentRecord.json', grid);
};


Clifton.document.DocumentUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Document Upload',
	iconCls: 'upload',
	height: 150,
	modal: true,
	doNotWarnOnCloseModified: true,
	// After upload the screen should be closed
	hideApplyButton: true,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Browse the file and click OK to upload.',
		labelWidth: 50,
		items: [
			{name: 'documentId', xtype: 'hidden'},
			{name: 'tableName', xtype: 'hidden'},
			{name: 'fkFieldId', xtype: 'hidden'},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false}
		],

		getSaveURL: function() {
			return 'documentRecordUpload.json';
		}
	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		form.submit({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				win.openerCt.docRecord = action.result.data;
				win.openerCt.loadCurrentUser();
				win.openerCt.refreshWindow();
				win.openerCt = null;
				win.closeWindow();
			}
		});
	}
});


Clifton.document.DocumentCheckInFormPanel = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	labelWidth: 100,
	instructions: 'Enter a check in comment and select major revision if this is a major revision.  If red line versions are supported then red line selection will be enabled for selection.',
	saveURL: 'documentRecordCheckin.json',

	initComponent: function() {
		const currentItems = [];

		Ext.each(this.checkInItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.document.DocumentCheckInFormPanel.superclass.initComponent.call(this);
	},

	checkInItems: [
		{name: 'documentId', xtype: 'hidden'},
		{name: 'tableName', xtype: 'hidden'},
		{name: 'fkFieldId', xtype: 'hidden'},
		{fieldLabel: 'Comments', name: 'checkinComment', xtype: 'textarea'},
		{
			boxLabel: 'This is a major revision', name: 'majorVersion', xtype: 'checkbox', disabled: true,
			handler: function(checkbox, checked) {
				const fp = TCG.getParentFormPanel(checkbox);
				fp.setCommentRequired(checkbox, checked);
			}
		},
		{boxLabel: 'This is a red line version', name: 'redLine', xtype: 'checkbox', disabled: true}
	],


	getSaveURL: function() {
		return this.saveURL;
	},

	afterRenderPromise: function(fp) {
		const f = fp.getForm();
		const revisionCheckBox = f.findField('majorVersion');
		const redLineCheckBox = f.findField('redLine');
		const commentTextArea = f.findField('checkinComment');
		const tbName = TCG.getValue('tableName', f.formValues);
		const fkIdValue = TCG.getValue('fkFieldId', f.formValues);
		const definitionLoader = new TCG.data.JsonLoader({
			params: {tableName: tbName, fkFieldId: fkIdValue},
			onLoad: function(record, conf) {
				if (record) {
					if (record.defaultComment !== '') {
						const defCom = TCG.getResponseText('documentDefaultComment.json?tableName=' + tbName + '&fkFieldId=' + fkIdValue, fp);
						commentTextArea.setValue(defCom);
					}
					if (record.majorRevisionSupported === true && record.minorRevisionSupported === true) {
						revisionCheckBox.enable();
						fp.minorRequired = record.minorCommentRequired;
						fp.majorRequired = record.majorCommentRequired;
						// Defaults to Minor
						commentTextArea.allowBlank = !record.minorCommentRequired;
					}
					else {
						revisionCheckBox.setValue(record.majorRevisionSupported);
						if (record.majorRevisionSupported === true) {
							commentTextArea.allowBlank = !record.majorCommentRequired;
						}
						else {
							commentTextArea.allowBlank = !record.minorCommentRequired;
						}
					}
					if (record.redLineSupported === true) {
						redLineCheckBox.enable();
					}
					commentTextArea.fireEvent('change', commentTextArea);
				}
			}
		});
		definitionLoader.load('documentDefinitionForEntity.json', fp);
	},

	setCommentRequired: function(revisionCheckBox, checked) {
		// Set allowBlank based upon revisionCheckBox & required
		let required;
		if (checked === true) {
			required = this.majorRequired;
		}
		else {
			required = this.minorRequired;
		}
		const f = this.getForm();
		const commentTextArea = f.findField('checkinComment');
		commentTextArea.allowBlank = !required;
		commentTextArea.fireEvent('change', commentTextArea);
	}
});
Ext.reg('document-checkin-formPanel', Clifton.document.DocumentCheckInFormPanel);


Clifton.document.DocumentCheckinWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Document Check In',
	iconCls: 'unlock',
	height: 250,
	modal: true,
	items: [{
		xtype: 'document-checkin-formPanel'
	}]
});


Clifton.document.DocumentRestoreVersionWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Restore Document Version',
	iconCls: 'undo',
	height: 250,
	modal: true,

	items: [{
		xtype: 'document-checkin-formPanel',
		instructions: 'Enter a check in comment and select major revision if this is a major revision. Click OK to restore document as latest version.',
		saveURL: 'documentRecordRestore.json'
	}]
});

Clifton.document.DocumentSecurityRestrictionWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Document Security',
	iconCls: 'key',
	height: 250,
	modal: true,

	items: [{
		xtype: 'gridpanel',
		instructions: 'The following overrides provide the ability to specify specific security users or groups that have access to view this file.  If none are specified, the view condition will be used to evaluate who has access.  If that is not specified the user will be required to have read access to the associated table linked to the document.  In order to edit security for a document, you must have check out access to the document.',
		name: 'documentSecurityRestrictionListForDocument',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'User or Group', width: 200, dataIndex: 'securityLabel'}
		],
		getLoadParams: function() {
			return {
				tableName: this.getWindow().defaultData.tableName,
				fkFieldId: this.getWindow().defaultData.fkFieldId
			};
		},
		editor: {
			deleteURL: 'documentSecurityRestrictionDelete.json',
			getDetailPageClass: function(grid, row) {
				return row.json.securityUser ? 'Clifton.security.user.UserWindow' : 'Clifton.security.user.GroupWindow';
			},
			getDetailPageId: function(gridPanel, row) {
				return row.json.securityUser ? row.json.securityUser.id : row.json.securityGroup.id;
			},
			addToolbarAddButton: function(toolBar) {
				const gridPanel = this.getGridPanel();
				toolBar.add({
					text: 'Add',
					tooltip: 'Add a new user/group',
					iconCls: 'add',
					menu: new Ext.menu.Menu({
						items: [
							// 	these items will render as dropdown menu items when the arrow is clicked:
							{
								text: 'User', iconCls: 'user',
								menu: new Ext.menu.Menu({
									layout: 'fit',
									style: {overflow: 'visible'},
									width: 200,
									items: [
										{
											xtype: 'combo', name: 'user', url: 'securityUserListFind.json?disabled=false', displayField: 'label',
											getListParent: function() {
												return this.el.up('.x-menu');
											},
											listeners: {
												'select': function(combo) {
													const tableName = gridPanel.getWindow().defaultData.tableName;
													const fkFieldId = gridPanel.getWindow().defaultData.fkFieldId;
													const userId = combo.getValue();
													combo.reset();
													const loader = new TCG.data.JsonLoader({
														waitTarget: gridPanel,
														waitMsg: 'Adding...',
														params: {securityUserId: userId, tableName: tableName, fkFieldId: fkFieldId},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('documentSecurityRestrictionToSecurityUserLink.json');
												}
											}
										}
									]
								})
							},
							{
								text: 'Group', iconCls: 'group',
								menu: new Ext.menu.Menu({
									layout: 'fit',
									style: {overflow: 'visible'},
									width: 200,
									items: [
										{
											xtype: 'combo', name: 'group', url: 'securityGroupListFind.json', displayField: 'label',
											getListParent: function() {
												return this.el.up('.x-menu');
											},
											listeners: {
												'select': function(combo) {
													const tableName = gridPanel.getWindow().defaultData.tableName;
													const fkFieldId = gridPanel.getWindow().defaultData.fkFieldId;
													const groupId = combo.getValue();
													combo.reset();
													const loader = new TCG.data.JsonLoader({
														waitTarget: gridPanel,
														waitMsg: 'Adding...',
														params: {securityGroupId: groupId, tableName: tableName, fkFieldId: fkFieldId},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('documentSecurityRestrictionToSecurityGroupLink.json');
												}
											}
										}
									]
								})
							}
						]
					})
				});
				toolBar.add('-');
			}
		}
	}]
});


Clifton.document.DocumentAwareGridEditor = Ext.extend(TCG.grid.GridEditor, {
	tableName: 'OVERRIDE-ME',
	entityText: 'OVERRIDE-ME',

	addEditButtons: function(t) {

		TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

		const tableName = this.tableName;
		const entityText = this.entityText;

		t.add({
			text: 'View',
			tooltip: 'View file for selected ' + entityText,
			iconCls: 'view',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a ' + entityText + ' to be downloaded.', 'No Row(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					TCG.showError('Multi-selection downloads are not supported.  Please select one row or use the ZIP option to download multiple.', 'NOT SUPPORTED');
				}
				else {
					Clifton.document.DownloadDocument(tableName, grid, sm.getSelected().id);
				}
			}
		});
		t.add('-');
		t.add({
			text: 'View PDF',
			tooltip: 'View file for selected ' + entityText + ' converted to PDF',
			iconCls: 'pdf',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a ' + entityText + ' to be downloaded.', 'No Row(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					TCG.showError('Multi-selection downloads are not supported yet.  Please select one row.', 'NOT SUPPORTED');
				}
				else {
					const format = sm.getSelected().data.documentFileType;
					if (!format) {
						TCG.showError('Unable to determine document Format for selected row.', 'MISSING FORMAT');
					}
					if (format.indexOf('doc') === 0 || format.indexOf('xls') === 0 || format.indexOf('ppt') === 0) {
						Clifton.document.DownloadConvertedDocument(tableName, grid, sm.getSelected().id, 'pdf');
					}
					else if (format.indexOf('pdf') === 0) {
						// If already a pdf - just download it
						Clifton.document.DownloadDocument(tableName, grid, sm.getSelected().id);
					}
					else {
						TCG.showError('Only Documents of type [doc, xls, ppt, pdf] can be downloaded as a pdf document.', 'NOT SUPPORTED');
					}
				}
			}
		});
		t.add('-');
		t.add({
			text: 'Download All (Zip)',
			xtype: 'splitbutton',
			tooltip: 'Download (Zip) all files for selected ' + entityText,
			iconCls: 'zip',
			scope: this,
			handler: function() {
				this.downloadAsZip(tableName, this.grid, true);
			},
			menu: new Ext.menu.Menu({
				items: [{
					text: 'Download All Files as Zip',
					tooltip: 'Download all files in the below grid as one zip file.',
					scope: this,
					handler: function() {
						this.downloadAsZip(tableName, this.grid, true);
					}
				}, {
					text: 'Download Selected Files as Zip',
					tooltip: 'Download selected files only as one zip file.',
					scope: this,
					handler: function() {
						this.downloadAsZip(tableName, this.grid, false);
					}
				}]
			})
		});
		t.add('-');
	},

	downloadAsZip: function(tableName, grid, downloadAll) {
		const fkFieldIds = [];
		if (TCG.isTrue(downloadAll)) {
			let i = 0;
			let row = grid.store.data.items[i];
			while (row) {
				fkFieldIds.push(row.id);
				i = i + 1;
				row = grid.store.data.items[i];
			}
		}
		else {
			if (grid.getSelectionModel().getCount() === 0) {
				TCG.showError('Please select a ' + entityText + ' to be downloaded.', 'No Row(s) Selected');
				return;
			}
			const records = grid.getSelectionModel().getSelections();
			for (let i = 0; i < records.length; i++) {
				fkFieldIds.push(records[i].id);
			}
		}
		Clifton.document.DownloadDocumentsAsZip(tableName, grid, fkFieldIds);
	}

});
Ext.preg('documentAware-grideditor', Clifton.document.DocumentAwareGridEditor);


/////////////////////////////////////////////////////////////////////////////
// Document File Aware
/////////////////////////////////////////////////////////////////////////////

Clifton.document.DocumentFileSimpleGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'documentFileListFind',
	instructions: 'The following files are attached to this record.  To add new files, use the add button or you can drag and drop a file into the grid.  To edit document file details double click an existing file.',

	rowSelectionModel: 'multiple', // Note: Zip option allows selecting multiple files

	definitionName: 'OVERRIDE_ME',
	hideDateFields: false, // If true hide Active/Start/End Date and don't filter on these
	allowMultiple: true,
	doNotEnableDDOnParentPanel: false, // By default when you drag and drop it accepts it on the entire form panel - however in some cases (Billing Invoice - we have two grids so the drag and drop must be on the gridpanel only)

	layout: 'fit',
	frame: true,
	border: true,
	collapsible: true,
	collapsed: true,
	gridConfig: {
		border: true,
		autoHeight: true,
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (eventName === 'ViewFile') {
						Clifton.document.DownloadDocument('DocumentFile', grid, row.json.id);
					}
				}
			}
		}
	},

	MESSES_UP_TOP_TOOLBAR_isPagingEnabled: function() {
		return false;
	},

	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},

	// Used for lookups only to send to the search form and return a list of documents attached directly to this entity, or optionally other related entities
	// Ex: InvestmentEvent with a recurrence.  As a user I want to see all documents specifically for this event occurence, as well as the parent event that defined the recurrence
	getEntityIds: function() {
		return undefined;
	},

	listeners: {
		afterRender: function() {
			const gridPanel = this;
			const fp = this.getWindow().getMainFormPanel();
			fp.on('afterload', function() {
				if (fp.getWindow().savedSinceOpen === true) {
					gridPanel.reload();
				}
			}, this);
		}
	},

	updateCount: function() {
		TCG.grid.GridPanel.prototype.updateCount.call(this, arguments);
		this.resetGridVisibility(this.grid.getStore().getCount());
	},

	resetGridVisibility: function(fileCount) {
		if (this.collapsible) {
			if (!this.getEntityId()) {
				this.setVisible(false);
			}
			else {
				this.setVisible(true);
				if (fileCount && fileCount > 0) {
					this.expand();
				}
				else {
					this.collapse();
				}
			}
		}
	},

	columns: [
		{header: 'ID', width: 35, dataIndex: 'id', hidden: true},
		{
			header: 'Format', width: 25, dataIndex: 'documentFileType', filter: false, align: 'center', tooltip: 'File Format: hover on the icon to see file extension',
			renderer: function(ft, args, r) {
				return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
			}
		},
		{
			header: 'File Name', width: 180, dataIndex: 'name',
			renderer: function(v, metaData, r) {
				const desc = r.data['description'];
				if (TCG.isNotBlank(desc)) {
					const qtip = desc;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{
			header: 'Size', width: 35, dataIndex: 'fileSize', hidden: true,
			tooltip: 'When filtering or sorting, the file size in bytes will be used.',
			renderer: function(v, metaData, r) {
				return TCG.fileSize(v);
			}
		},
		{header: 'Description', hidden: true, width: 75, dataIndex: 'description'},
		{header: 'Start Date', width: 35, dataIndex: 'startDate'},
		{header: 'End Date', width: 35, dataIndex: 'endDate'},

		{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser'},
		{header: 'Doc Updated On', width: 65, dataIndex: 'documentUpdateDate'},
		{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
	],

	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		// Hide Date/Active Columns if option set
		const cm = this.getColumnModel();
		if (this.hideDateFields) {
			cm.setHidden(cm.findColumnIndex('active'), true);
			cm.setHidden(cm.findColumnIndex('startDate'), true);
			cm.setHidden(cm.findColumnIndex('endDate'), true);
		}
	},

	editor: {
		ptype: 'documentAware-grideditor',
		tableName: 'DocumentFile',
		entityText: 'document',
		detailPageClass: 'Clifton.document.setup.FileWindow',
		// Override for the special window for new files
		addToolbarAddButton: function(toolBar) {
			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new file',
				iconCls: 'add',
				scope: this,
				handler: function() {
					this.openDetailPage('Clifton.document.setup.FileUploadWindow', this.getGridPanel());
				}
			});
			toolBar.add('-');
		},
		getDefaultData: function(gridPanel) {
			return {
				definition: TCG.data.getData('documentDefinitionByName.json?name=' + gridPanel.definitionName, gridPanel, 'document.definitionName.' + gridPanel.definitionName),
				fkFieldId: gridPanel.getWindow().getMainFormId()
			};
		},
		getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
			return undefined; // Not needed for Existing
		}
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			if (this.hideDateFields === false) {
				this.setFilterValue('active', true);
			}
		}
		if (!this.getEntityId()) {
			this.resetGridVisibility(0);
			return false;
		}
		// allow drag and drop files (only if getEntityId is populated)

		// ability to NOT allow on the container panel and would only accept on the gridpanel (RARE)
		let ddObject = this;
		// Otherwise accept on the container panel (i.e. form panel) (COMMON)
		if (!TCG.isTrue(this.doNotEnableDDOnParentPanel)) {
			ddObject = TCG.getParentByClass(this, Ext.Panel);
		}
		TCG.file.enableDD(ddObject, this.getUploadData(), this, 'documentFileUpload.json', this.allowMultiple, 5);

		if (this.getEntityIds()) {
			return {definitionNameEquals: this.definitionName, fkFieldIds: this.getEntityIds()};
		}
		return {definitionNameEquals: this.definitionName, fkFieldId: this.getEntityId()};
	},
	getUploadData: function() {
		const def = TCG.data.getData('documentDefinitionByName.json?name=' + this.definitionName, this, 'document.definitionName.' + this.definitionName);
		const fkId = this.getEntityId();
		return {
			'definition.id': def.id,
			'fkFieldId': fkId
		};
	}
});
Ext.reg('documentFileGrid-simple', Clifton.document.DocumentFileSimpleGrid);


Clifton.document.system.note.NoteWithDocumentUploadGridPanel = Ext.extend(Clifton.system.note.NoteGridPanel, {
	showAttachmentInfo: true,

	getTopToolbarFilters: function(toolBar) {
		const gridPanel = this;
		// Doesn't add an extra filter, but added the DD Icon/Message to the toolbar
		// Drag and Drop Support
		toolBar.add({
			xtype: 'drag-drop-container',
			layout: 'fit',
			allowMultiple: false,
			bindToFormLoad: gridPanel.bindDragAndDropToFormLoad,
			cls: undefined,
			popupComponentName: 'Clifton.document.system.note.SingleNoteWithDocumentCreateWindow',
			message: '&nbsp;',
			tooltipMessage: 'Drag and drop a file to this grid to create a new single note associated with this entity and attach the file to it.',
			getParams: function() {
				return gridPanel.getDocumentNoteParams();
			},
			// Object to reload - i.e. gridPanel to reload after attaching new file
			getReloadObject: function() {
				return gridPanel;
			}
		});

		return Clifton.system.note.NoteGridPanel.prototype.getTopToolbarFilters.call(this, toolBar);
	},

	bindDragAndDropToFormLoad: false,

	getDocumentNoteParams: function() {
		const gridPanel = this;
		const tbl = TCG.data.getData('systemTableByName.json?tableName=' + gridPanel.tableName, gridPanel, 'system.table.' + gridPanel.tableName);
		return {
			noteType: {table: tbl},
			fkFieldId: gridPanel.getEntityId()
		};
	}
});
Ext.reg('document-system-note-grid', Clifton.document.system.note.NoteWithDocumentUploadGridPanel);
//  System Note Grids can automatically use documents if documents project is imported
Ext.reg('system-note-grid', Clifton.document.system.note.NoteWithDocumentUploadGridPanel);


Clifton.system.note.SupportAttachments = true; // Disabled by default, enabled here if this file is imported, just used in UI to hide columns

Ext.override(Clifton.system.note.NoteWindowBase, {

	// For Single Attachment Support shows the check out, check in, etc. toolbar
	tabToolbar: {
		xtype: 'document-toolbar',
		hidden: true,
		tableName: 'SystemNote'
	},

	// For Single Attachment Support shows the revision history of that document
	revisionTab: {
		title: 'Revision History',
		items: [{
			xtype: 'document-version-grid',
			tableName: 'SystemNote'
		}]
	},

	attachmentItems: [
		{
			xtype: 'fieldset',
			collapsed: true,
			hidden: true,
			name: 'attachFieldset',
			title: 'Attached Document Info',
			instructions: 'Document information for this note. Document Name is optional and can be used to default the document name when downloading from the system.',
			anchor: '0',
			items: [
				{fieldLabel: 'Document Name', name: 'documentName'},
				{
					fieldLabel: 'Updated By', xtype: 'compositefield',
					items: [
						{name: 'documentUpdateUser', xtype: 'displayfield', flex: 1},
						{value: 'On:', xtype: 'displayfield', width: 50},
						{name: 'documentUpdateDate', xtype: 'displayfield', flex: 2}
					]
				}
			]
		},
		{
			xtype: 'fieldset',
			collapsible: false,
			hidden: true,
			border: false,
			name: 'multipleAttachFieldset',
			anchor: '0',
			items: [{
				title: 'File Attachments',
				xtype: 'documentFileGrid-simple',
				definitionName: 'SystemNoteAttachments',
				hideDateFields: true,
				anchor: '0'
			}]
		}
	],

	resetNoteTypeFieldsForAttachments: function(formpanel, record) {
		const tabs = formpanel.getWindow().items.get(0);

		// Do Nothing, overridden in Document Project
		const attachFieldSet = TCG.getChildByName(formpanel, 'attachFieldset');
		const multipleAttachFieldSet = TCG.getChildByName(formpanel, 'multipleAttachFieldset');
		const tb = formpanel.ownerCt.getTopToolbar();


		const attachAllowed = (record.singleAttachmentAllowed === true);
		if (attachAllowed) {
			attachFieldSet.expand();
		}
		else {
			// Disable DD so it doesn't mess with multiple File Attachments
			TCG.file.disableDD(formpanel);
		}

		// Enable/Disable Document Attachment Fieldset
		attachFieldSet.setDisabled(!attachAllowed);
		attachFieldSet.setVisible(attachAllowed);

		// Enable/Disable Document Toolbar
		if (tb) {
			tb.setDisabled(!attachAllowed);
			tb.setVisible(attachAllowed);
		}

		// Show/Hide Revision History Tab
		const revTab = formpanel.getWindow().getTabPosition('Revision History');
		if (revTab === 0 && attachAllowed) {
			tabs.add(this.revisionTab);
		}
		else if (revTab !== 0 && !attachAllowed) {
			tabs.remove(revTab);
		}

		const multipleAttachAllowed = (record.multipleAttachmentsAllowed === true);
		if (multipleAttachAllowed) {
			multipleAttachFieldSet.expand();
		}
		// Enable/Disable Document Attachment Fieldset
		multipleAttachFieldSet.setDisabled(!multipleAttachAllowed);
		multipleAttachFieldSet.setVisible(multipleAttachAllowed);
	}
});
