Clifton.document.FileImportBulkSelectExistHandlerWindow = Ext.extend(TCG.file.DragAndDropPopupWindow, {
	// override any properties using 'popupComponentConfig'
	title: 'Bulk Upload',
	iconCls: 'upload',
	height: 325,
	width: 500,

	instructions: 'Select an option to process the file upload.',

	okButtonTooltip: 'Upload File(s)',

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const uploadParams = Ext.apply(panel.getFormValuesFormatted(), panel.getForm().formValues);
		const win = this;
		const files = win.ddFiles;
		Clifton.document.enableDD.upload(files, 0, panel.getEl(), uploadParams, win, this.saveUrl);
	},
	saveUrl: 'documentFileUploadUpload.json',
	formItems: [
		{xtype: 'sectionheaderfield', header: 'If same file has already been uploaded and not processed:'},
		{
			xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'fileAction', allowBlank: false, items: [
				{fieldLabel: '', boxLabel: 'Error if file already exists', name: 'fileAction', inputValue: 'ERROR', checked: true},
				{fieldLabel: '', boxLabel: 'Skip files that already exist.', name: 'fileAction', inputValue: 'SKIP'},
				{fieldLabel: '', boxLabel: 'Replace files that already exist', name: 'fileAction', inputValue: 'REPLACE'}
			]
		}
	]
});


Clifton.document.enableDD.upload = function(files, fileCount, el, params, reloadObj, loadUrl) {
	const file = files[fileCount];
	const form = new FormData();
	form.append('file', file);
	for (const p in params) {
		if (params[p] || params[p] === false) {
			form.append(p, params[p]);
		}
	}
	const xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	el.mask('Uploading ' + file.name);
	xhr.addEventListener('load', function(obj) {
		el.unmask();
		const json = obj.currentTarget.responseText;
		const o = Ext.decode(json);
		if (o && o.success === false) {
			if (o.message.indexOf('already exists') > 0) {
				const friendlyMessage = o.message.replace('Unexpected Error Occurred: ', '');
				Ext.Msg.confirm('File Exists', friendlyMessage + '<br><br><b>Would you like to replace the file?</b>&nbsp; Click Yes to replace the file, No to skip it.', function(a) {
					if (a === 'yes') {
						params = Ext.apply(params, {'fileAction': 'REPLACE'});
						Clifton.document.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl);
					}
					else {
						fileCount++;
						if (fileCount === files.length) {
							if (reloadObj && reloadObj.reload) {
								reloadObj.reload.defer(300, reloadObj);
							}
						}
						else {
							Clifton.document.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl);
						}
					}
				});
			}
			else {
				TCG.showError('Upload Error: ' + o.message, 'Upload Error');
			}
		}
		else {
			fileCount++;
			if (fileCount === files.length) {
				if (reloadObj && reloadObj.reload) {
					reloadObj.reload.defer(300, reloadObj);
				}
			}
			else {
				Clifton.document.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl);
			}
		}

	}, false);
	xhr.addEventListener('error', function(obj) {
		el.unmask();
		TCG.showError('Upload Failed for: ' + file.name, 'Upload Error');
	}, false);
	xhr.open('POST', loadUrl);
	xhr.send(form);
};
