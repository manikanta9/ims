Clifton.document.setup.DocumentSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'documentSetupWindow',
	title: 'Document Management',
	iconCls: 'book',
	width: 1200,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Document Definitions',
				items: [{
					name: 'documentDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'Document definitions are used to map our Tables to Documents in the Document Management System. Folder Structures and Document Names are generated based upon this configuration.  Document Version Name is used instead of the Document Name when downloading Document Versions (Default for the Save As... dialogue box).  There can be only one "One Per Entity" per table defined.',
					wikiPage: 'IT/Document+Management',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'label',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Definitions" : "Definition"]})',
//					remoteSort: true,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition', width: 150, dataIndex: 'label', defaultSortColumn: true, hidden: true},
						{header: 'Folder', width: 100, dataIndex: 'folderStructure'},
						{header: 'Table', width: 100, dataIndex: 'table.name', filter: {searchFieldName: 'tableName'}},
						{header: 'One Per Entity', width: 60, dataIndex: 'oneDocumentPerEntity', type: 'boolean'},
						{header: 'Document Name', width: 200, dataIndex: 'documentName', hidden: true},
						{header: 'Document Version Name', width: 200, dataIndex: 'documentVersionName', hidden: true},
						{header: 'View Condition', width: 200, dataIndex: 'viewCondition.name'},
						{header: 'Modify Condition', width: 200, dataIndex: 'modifyCondition.name'},
						{header: 'Default Comment', width: 200, dataIndex: 'defaultComment', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.document.setup.DefinitionWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Files',
				items: [{
					name: 'documentFileListFind',
					xtype: 'gridpanel',
					instructions: 'Document files are documents uploaded and associated with a particular entity based on the document definition.  This page only includes files that have been uploaded to a definition that supports multiple per entity.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'definition.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const eventName = TCG.getActionColumnEventName(evt);
									const row = grid.store.data.items[rowIndex];
									if (eventName === 'ViewFile') {
										Clifton.document.DownloadDocument('DocumentFile', grid, row.json.id);
									}
									if (eventName === 'ShowLink') {
										const clz = row.json.definition.table.detailScreenClass;
										const id = row.json.fkFieldId;
										const gridPanel = grid.ownerGridPanel;
										gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
									}
								}
							}
						}
					},
					additionalPropertiesToRequest: 'definition.table.label',
					columns: [
						{header: 'ID', width: 35, dataIndex: 'id', hidden: true},
						{header: 'FKFieldID', width: 35, dataIndex: 'fkFieldId', hidden: true},
						{header: 'Document Definition', width: 100, dataIndex: 'definition.name', hidden: true},
						{
							header: 'Format', width: 25, dataIndex: 'documentFileType', filter: false, align: 'center',
							renderer: function(ft, args, r) {
								return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
							}
						},
						{
							header: 'File Name', width: 180, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								const desc = r.data['description'];
								if (TCG.isNotBlank(desc)) {
									const qtip = desc;
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(qtip);
								}
								return v;
							}
						},
						{
							header: 'Link', width: 40, dataIndex: 'definition.table.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('definition.table.label', r.json) + ' that this file is linked to.', 'ShowLink');
								}
								return 'N/A';
							}
						},
						{header: 'Description', hidden: true, width: 75, dataIndex: 'description'},
						{header: 'Start Date', width: 35, dataIndex: 'startDate'},
						{header: 'End Date', width: 35, dataIndex: 'endDate'},

						{
							header: 'File Size', width: 40, dataIndex: 'fileSize', type: 'int', useNull: true,
							tooltip: 'When filtering or sorting, the file size in bytes will be used.',
							renderer: function(v, metaData, r) {
								return TCG.fileSize(v);
							}
						},
						{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser'},
						{header: 'Doc Updated On', width: 60, dataIndex: 'documentUpdateDate'},
						{header: 'Active', width: 35, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						ptype: 'documentAware-grideditor',
						tableName: 'DocumentFile',
						entityText: 'document',
						detailPageClass: 'Clifton.document.setup.FileWindow',
						drillDownOnly: true
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default doc update date to last 7 days
							this.setFilterValue('documentUpdateDate', {'after': new Date().add(Date.DAY, -7)});
						}
					}
				}]
			}


			/**,
			 NOT USED - THIS READS THE DOCUMENT RECORDS FROM ALFRESCO WHICH IS EXTREMELY SLOW WHEN TRYING TO SEARCH
			 AGAINST ALL
			 {
				title: 'Documents',
				items: [{
					name: 'documentRecordListFind',
					xtype: 'gridpanel',
					reloadOnRender: false,
					timeout: 3000,
					instructions: 'The following list the latest checked in versions of documents for a selected definition.',
					columns: [
						  {header: 'Table', dataIndex: 'tableName', hidden: true},
						  {header: 'FkFieldID', dataIndex: 'fkFieldId', type: 'int', hidden: true, filter: false},
						  {header: 'Document Name', dataIndex: 'name', width: 250 },
						  {header: 'Format', dataIndex: 'fileExtension', width: 30, filter: false, align: 'center',
							  renderer: function(ft) {
								  return TCG.renderDocumentFileTypeIcon(ft);
							  }
						  },
						  {header: 'Current Version', dataIndex: 'versionLabel', width: 60, filter: false},
						  {header: 'Checked Out By', dataIndex: 'checkedOutUser', width: 65 },
						  {header: 'Updated By', dataIndex: 'updateUser', width: 65 },
						  {header: 'Updated On', dataIndex: 'updateDate', width: 75, filter: false }
					],
					getLoadParams: function(firstLoad) {
						var t = this.getTopToolbar();
						var dd = TCG.getChildByName(t, 'documentDefinition');
						if (dd.getValue() != '') {
							return { documentDefinitionId: dd.getValue() }
						}
						else {
							TCG.showError('Select a document definition to load documents for.');
							return false;
						}
					},
					getTopToolbarFilters: function(toolbar) {
						   var grid = this;
						   return [
							   {fieldLabel: 'Document Definition', xtype: 'combo', name: 'documentDefinition', width: 180, url: 'documentDefinitionListFind.json', displayField: 'table.name'}
						   ];
					},
			 **/
			/**
			 createStore: function(fields) {
					var gridPanel = this;
					var storeConf = {
							fields: fields,
							url: encodeURI(this.getLoadURL()),
							listeners: {
								datachanged: function(store, records, opts){
									gridPanel.updateCount();
								}
							}
					};
					if (!this.dataTable && this.isPagingEnabled()) {
						storeConf.root = 'documentRecordList.rows',
						storeConf.totalProperty =  'documentRecordList.total'
					}
					var storeClass = TCG.data.JsonStore;
					if (this.dataTable) {
						if (this.groupField) {
							storeClass = TCG.data.DataTableGroupingStore;
							storeConf.groupField = this.groupField;
							storeConf.remoteSort = this.remoteSort || false;
						}
						else {
							storeClass = TCG.data.DataTableStore;
						}
					}
					else if (this.groupField) {
						storeClass = TCG.data.GroupingStore;
						storeConf.groupField = this.groupField;
						storeConf.remoteSort = this.remoteSort || false;
					}
					return new storeClass(storeConf);
				},
			 **/
			/**
			 }]
			 }
			 **/
		]
	}]
});


