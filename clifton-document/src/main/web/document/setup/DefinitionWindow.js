Clifton.document.setup.DefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Document Definition',
	iconCls: 'book',
	width: 650,
	height: 600,

	tbar: [
		{
			text: 'Test Name',
			tooltip: 'Test the Document Name syntax against an actual entity.',
			iconCls: 'view',
			handler: function() {

				const f = this.ownerCt.ownerCt.items.get(0);
				const w = f.getWindow();
				const dName = TCG.getValue('documentName', w.getMainForm().formValues);
				const sample = TCG.getResponseText('documentNameSample.json?id=' + w.getMainFormId() + '&documentName=' + dName, this);
				Ext.Msg.alert('Sample Document Name', sample);
			}
		}, '-',
		{
			text: 'Test Version Name',
			tooltip: 'Test the Document Version Name syntax against an actual entity.',
			iconCls: 'view',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				const w = f.getWindow();
				const dName = TCG.getValue('documentVersionName', w.getMainForm().formValues);
				const sample = TCG.getResponseText('documentNameSample.json?id=' + w.getMainFormId() + '&documentName=' + dName, this);
				Ext.Msg.alert('Sample Document Version Name', sample);
			}
		}, '-',
		{
			text: 'Test Default Comment',
			tooltip: 'Test the Default Comment syntax against an actual entity.',
			iconCls: 'view',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				const w = f.getWindow();
				const dName = TCG.getValue('defaultComment', w.getMainForm().formValues);
				const sample = TCG.getResponseText('documentDefaultCommentSample.json?id=' + w.getMainFormId() + '&defaultComment=' + dName, this);
				Ext.Msg.alert('Sample Default Comment', sample);
			}
		}
	],

	items: [{
		xtype: 'formpanel',
		instructions: 'A document definition maps a specific table and document type to a Document Space in the Document Management system.' +
			'<ul class="c-list">' +
			'<li>The folder defines what space in the Document Management system these documents are stored.</li>' +
			'<li>Optional View Conditions are validated during document downloads and can be used to restrict what users can view the actual document. If none specified, the user will be required to have Read access to the security resource assigned to the table in this definition.  For cases where document viewing security is specific, overrides can be made for a specific document.  In these cases, the View condition is ignored and only those users/groups assigned to the document will have view access.</li>' +
			'<li>Optional Modify Conditions are validated during document checkouts and can be used to restrict what users can check out documents, and/or when documents can be checked out.</li>' +
			'<li>Optional File Bulk Upload Locator Bean.  If set, bulk upload of files is supported.  The specified bean determines how to find the component it belongs to.</li>' +
			'<li>There can only be one "One Per Entity" definition per table..</li>' +
			'</ul>',

		url: 'documentDefinition.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Definition Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Definition Label', name: 'label'},
			{fieldLabel: 'Table', name: 'table.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow', detailIdField: 'table.id'},
			{fieldLabel: 'Folder', name: 'folderStructure'},
			{fieldLabel: 'View Condition', name: 'viewCondition.label', hiddenName: 'viewCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
			{fieldLabel: 'Modify Condition', name: 'modifyCondition.label', hiddenName: 'modifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
			{fieldLabel: 'File Bulk Upload Locator Bean', name: 'fileBulkUploadFkFieldIdLocatorBean.label', hiddenName: 'fileBulkUploadFkFieldIdLocatorBean.id', displayField: 'name', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Document File Foreign Key Field ID Locator&typeName=Document File Bulk Upload Account Performance Locator', detailPageClass: 'Clifton.system.bean.BeanWindow'},
			{boxLabel: 'Allow only one document per entity (table row).', name: 'oneDocumentPerEntity', xtype: 'checkbox'},
			{
				xtype: 'fieldset',
				title: 'Document Naming Options',
				labelWidth: 150,
				instructions: 'Document Name is Freemarker Syntax that is used to generate document names for specific entities.  To view sample document names for the given syntax, click <i>Test Name</i> button above.' +
					' Document Version Name is Freemarker Syntax that is used to generate document names during download of specific versions (defaults the Save As... dialogue box).  To view sample document version names for the given syntax, click <i>Test Version Name</i> button above.',
				items: [
					{fieldLabel: 'Document Name', name: 'documentName', xtype: 'textarea', height: 50},
					{fieldLabel: 'Document Version Name', name: 'documentVersionName', xtype: 'textarea', height: 50}
				]
			},
			{
				xtype: 'fieldset',
				title: 'Document Revision Options',
				labelWidth: 150,
				defaultType: 'checkbox',
				items: [
					{boxLabel: 'Allow Red Line Versions', name: 'redLineSupported'},
					{xtype: 'label', html: '<hr/>'},
					{boxLabel: 'Allow Minor Revision', name: 'minorRevisionSupported'},
					{boxLabel: 'Require comment on minor revision checkin', name: 'minorCommentRequired', requiredFields: ['minorRevisionSupported']},
					{xtype: 'label', html: '<hr/>'},
					{boxLabel: 'Allow Major Revision', name: 'majorRevisionSupported'},
					{boxLabel: 'Require comment on major revision checkin', name: 'majorCommentRequired', requiredFields: ['majorRevisionSupported']},
					{xtype: 'label', html: '<hr/>'},
					{xtype: 'label', html: 'Default comment is Freemarker Syntax used to pre-populate the comment field during checkins.  This value can be overwritten or added to by users during checkins. To view a sample default comment for the given syntax, click <i>Test Default Comment</i> button above.'},
					{fieldLabel: 'Default Comment', name: 'defaultComment', xtype: 'textarea'}
				]
			}
		]
	}]
});
