Clifton.document.setup.FileUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'File Upload',
	iconCls: 'upload',
	height: 350,
	modal: true,
	// After upload the screen should be closed
	hideApplyButton: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Browse the file and click OK to upload. Use Additional Settings section to add more details.',
		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{
				xtype: 'fieldset',
				title: 'Additional Settings',
				collapsed: true,
				labelWidth: 130,
				items: [
					{fieldLabel: 'Document Definition', name: 'definition.label', xtype: 'linkfield', detailPageClass: 'Clifton.document.setup.DefinitionWindow', detailIdField: 'definition.id'},
					{fieldLabel: 'File Name', name: 'name'},
					{fieldLabel: 'FKFieldID', name: 'fkFieldId', hidden: true},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{
						fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
						items: [
							{name: 'startDate', xtype: 'datefield', flex: 1},
							{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 100},
							{name: 'endDate', xtype: 'datefield', flex: 1}
						]
					}
				]
			}
		],

		getSaveURL: function() {
			return 'documentFileUpload.json';
		}

	}]
});
