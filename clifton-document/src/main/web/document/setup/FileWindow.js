Clifton.document.setup.FileWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'File',
	iconCls: 'attach',
	width: 750,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'File',
				tbar: {
					xtype: 'document-toolbar',
					tableName: 'DocumentFile',
					hideRemoveButton: true
				},
				items: [{
					xtype: 'formpanel',
					instructions: 'Document Files are meta-data records of actual files stored in the Document Management system.',
					url: 'documentFile.json',
					labelWidth: 130,
					items: [
						{fieldLabel: 'Document Definition', name: 'definition.label', xtype: 'linkfield', detailPageClass: 'Clifton.document.setup.DefinitionWindow', detailIdField: 'definition.id'},
						{fieldLabel: 'File Name', name: 'name'},
						{fieldLabel: 'FKFieldID', name: 'fkFieldId', hidden: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
							items: [
								{name: 'startDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 130},
								{name: 'endDate', xtype: 'datefield', flex: 1}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'File Updated By', xtype: 'compositefield',
							items: [
								{name: 'documentUpdateUser', xtype: 'displayfield', flex: 1},
								{value: 'On:', xtype: 'displayfield', width: 50},
								{name: 'documentUpdateDate', xtype: 'displayfield', flex: 2}
							]
						}
					]
				}]
			},


			{
				title: 'Revision History',
				items: [{
					xtype: 'document-version-grid',
					tableName: 'DocumentFile'
				}]
			}
		]
	}]
});
