Ext.ns('Clifton.billing', 'Clifton.billing.definition', 'Clifton.billing.invoice', 'Clifton.billing.payment', 'Clifton.billing.commission', 'Clifton.billing.commission.schedule', 'Clifton.billing.payment.upload');


//Simple Upload Windows
Clifton.system.schema.SimpleUploadWindows['BillingBasisExternal'] = 'Clifton.billing.invoice.ExternalUploadWindow';

Clifton.billing.payment.upload.BillingPaymentAllocationUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload (Billing Allocation)',
	iconCls: 'import',
	height: 150,
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	width: 500,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 80,
		instructions: 'Browse for the external file, and click OK to upload.',
		items: [
			{fieldLabel: 'External File', name: 'file', xtype: 'fileuploadfield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'billingPaymentAllocationFileUpload.json?billingPaymentId=' + this.getFormValue('billingPaymentId');
		},
		listeners: {
			afterload: function(panel) {
				panel.getWindow().defaultData.openerCt.reload();
			}
		}
	}]
});


// Client Window Additional Tabs
Clifton.business.client.ClientWindowBaseAdditionalTabs.push({
	addAdditionalTabs: function(w) {
		const tabs = w.items.get(0);
		const pos = w.getTabPosition(w, 'Contacts');
		tabs.insert(pos + 1, this.billingTab);
	},

	billingTab: {
		title: 'Billing',
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},

		items: [
			{
				title: 'Billing Definitions',
				xtype: 'billing-definitionsGrid-byClient',
				isIncludeRelatedClientCheckbox: function() {
					return this.getWindow().isIncludeRelatedCheckbox();
				},
				getRelatedClientCheckboxLabel: function() {
					return this.getWindow().getRelatedCheckboxLabel('Billing Definitions');
				},
				border: 1,
				flex: 1
			},
			{flex: 0.02},
			{
				title: 'Invoices',
				xtype: 'billing-invoicesGrid-byClient',
				isIncludeRelatedClientCheckbox: function() {
					return this.getWindow().isIncludeRelatedCheckbox();
				},
				getRelatedClientCheckboxLabel: function() {
					return this.getWindow().getRelatedCheckboxLabel('Invoices');
				},
				border: 1,
				flex: 1.3
			}
		]
	}
});

//Client Relationship Window Additional Tabs
Clifton.business.client.ClientRelationshipWindowAdditionalTabs.push({
	title: 'Billing',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [
		{
			title: 'Billing Definitions',
			xtype: 'gridpanel',
			border: 1,
			flex: 1,
			name: 'billingDefinitionListFind',
			instructions: 'Billing Definition links billing rules defined in a contract (IMA, etc.) to corresponding investment accounts. A separate invoice is generated for each active billing definition.',
			columns: [
				{header: 'ID', width: 35, dataIndex: 'id', hidden: true},
				{header: 'Invoice Type', width: 75, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
				{header: 'Company', width: 200, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'companyName'}, hidden: true},
				{
					header: 'Contract', width: 200, dataIndex: 'billingContract.name', filter: {searchFieldName: 'contractName'},
					renderer: function(v, metaData, r) {
						const note = r.data.note;
						if (TCG.isNotBlank(note)) {
							const qtip = r.data.note;
							metaData.css = 'amountAdjusted';
							metaData.attr = TCG.renderQtip(qtip);
						}
						return v;
					}
				},
				{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
				{header: 'Billing CCY', width: 50, dataIndex: 'billingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'billingCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
				{
					header: 'Frequency', width: 60, dataIndex: 'billingFrequency', filter: {
						type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
						store: new Ext.data.ArrayStore({
							fields: ['name', 'value', 'description'],
							data: Clifton.billing.BillingFrequencies
						})
					}
				},
				{header: 'Payment In Advance', width: 60, dataIndex: 'paymentInAdvance', type: 'boolean'},
				{
					header: 'Workflow State', width: '60', dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
					renderer: function(v, metaData, r) {
						const sts = r.data['workflowStatus.name'];
						if (sts === 'Approved') {
							metaData.css = 'amountPositive';
						}
						else if (sts === 'Draft') {
							metaData.css = 'amountNegative';
						}
						else {
							metaData.css = 'amountAdjusted';
						}
						return v;
					}
				},
				{header: 'Workflow Status', width: '60', dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
				{header: 'Start Date', width: 40, dataIndex: 'startDate'},
				{header: 'Second Year Start Date', width: 60, dataIndex: 'secondYearStartDate', hidden: true},
				{header: 'End Date', width: 40, dataIndex: 'endDate'},
				{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
			],
			getLoadParams: function(firstLoad) {
				const params = {'companyId': TCG.getValue('company.id', this.getWindow().getMainForm().formValues)};
				if (TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeClient').getValue())) {
					params.includeClientOrClientRelationship = true;
				}
				if (TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeAccountsExist').getValue())) {
					params.includeAccountsExistOfClientOrClientRelationship = true;
				}
				return params;
			},
			getTopToolbarFilters: function(toolbar) {
				return [
					{boxLabel: 'Include Accounts Referenced In Billing Definition&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeAccountsExist', checked: true, qtip: 'If checked will include billing definitions where there is an account included on the accounts tab under this client relationship.'},
					{boxLabel: 'Include Client\'s Billing Definitions&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeClient', checked: true}];
			},
			editor: {
				detailPageClass: 'Clifton.billing.definition.DefinitionWindow'
			}
		},
		{flex: 0.02},
		{
			title: 'Invoices',
			border: 1,
			flex: 1.3,
			xtype: 'gridpanel',
			name: 'billingInvoiceListFind',

			instructions: 'Billing Invoices are bills generated and sent to clients for payment. The following invoices were generated for the clients associated with this client relationship.',
			columns: [
				{header: 'Invoice #', width: 35, dataIndex: 'id', numberFormat: '0000'},
				{
					header: '', width: 15, dataIndex: 'locked', type: 'boolean',
					renderer: function(v, metaData, r) {
						return Clifton.system.renderSystemLockIconWithTooltip(v, 'invoice', r.data['lockedByUser.displayName'], r.data['lockNote']);
					}
				},
				{header: 'Locked By', width: 50, hidden: true, dataIndex: 'lockedByUser.displayName', filter: {searchFieldName: 'lockedByUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
				{header: 'Lock Note', width: 200, hidden: true, dataIndex: 'lockNote'},
				{header: 'Invoice Type', width: 50, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}, hidden: true},
				{header: 'Company', width: 150, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'companyName'}, hidden: true},
				{
					header: 'Contract', width: 180, dataIndex: 'billingDefinition.billingContract.name', filter: false,
					renderer: function(v, metaData, r) {
						const note = r.data['billingDefinition.note'];
						if (TCG.isNotBlank(note)) {
							const qtip = note;
							metaData.css = 'amountAdjusted';
							metaData.attr = TCG.renderQtip(qtip);
						}
						else {
							if (TCG.isBlank(v)) {
								return r.data['invoiceType.name'];
							}
						}
						return v;
					}
				},
				{header: 'Billing Definition Note', hidden: true, width: 200, dataIndex: 'billingDefinition.note', filter: false, sortable: false},
				{
					header: 'Group #', width: 30, dataIndex: 'invoiceGroup.id',
					renderer: function(grp, args, r) {
						if (TCG.isNotBlank(grp)) {
							return TCG.renderActionColumn('view', grp, 'View Billing Invoice Group ' + grp);
						}
						return '';
					},
					eventListeners: {
						'click': function(column, grid, rowIndex, event) {
							const groupId = TCG.getValue('invoiceGroup.id', grid.getStore().getAt(rowIndex).json);
							if (TCG.isNotBlank(groupId)) {
								grid.ownerGridPanel.editor.openDetailPage('Clifton.billing.invoice.InvoiceGroupWindow', grid.ownerGridPanel, groupId, rowIndex);
							}
						}
					}
				},
				{header: 'Invoice Date', width: 40, dataIndex: 'invoiceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
				{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
				{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this invoice on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
				{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 50, filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
				{
					header: 'Workflow State', dataIndex: 'workflowState.name', width: 50, filter: {searchFieldName: 'workflowStateName'},
					renderer: function(v, metaData, r) {
						const sts = r.data['workflowStatus.name'];
						if (sts === 'Open') {
							metaData.css = 'amountNegative';
						}
						else if (sts === 'Closed' || sts === 'Cancelled') {
							metaData.css = 'amountPositive';
						}
						else {
							metaData.css = 'amountAdjusted';
						}
						return v;
					}
				},
				{
					header: 'Violation Status', width: 60, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
					renderer: function(v, p, r) {
						return (Clifton.rule.violation.renderViolationStatus(v));
					}
				},
				{
					header: 'Revenue (Gross)', width: 45, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'},
					tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.',
					summaryTotalCondition: function(data) {
						return TCG.isNotEquals('Void', data.workflowState.name);
					}
				},
				{
					header: 'Broker Fee', width: 45, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'},
					tooltip: 'Broker Fee - revenue that we share (pay out) to an external party',
					summaryTotalCondition: function(data) {
						return TCG.isNotEquals('Void', data.workflowState.name);
					}
				},
				{
					header: 'Revenue Net', width: 45, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
					tooltip: 'Gross Revenue and Broker Fees',
					summaryTotalCondition: function(data) {
						return TCG.isNotEquals('Void', data.workflowState.name);
					}
				},
				{
					header: 'Revenue Share', width: 45, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
					tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.',
					summaryTotalCondition: function(data) {
						return TCG.isNotEquals('Void', data.workflowState.name);
					}
				},
				{
					header: 'Revenue (Final)', width: 45, hidden: true, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
					tooltip: 'Gross Revenue and Broker Fees and Revenue Share',
					summaryTotalCondition: function(data) {
						return TCG.isNotEquals('Void', data.workflowState.name);
					}
				},
				{header: 'Other', width: 40, dataIndex: 'otherTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}}
			],
			plugins: {ptype: 'gridsummary'},
			editor: {
				detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					toolBar.add(new TCG.form.ComboBox({name: 'invoiceType', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=false', width: 150, listWidth: 200, emptyText: '< Select Invoice Type >'}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Create a new manual Invoice of selected type',
						iconCls: 'add',
						scope: this,
						handler: function() {
							const invoiceType = TCG.getChildByName(toolBar, 'invoiceType');
							const invoiceTypeId = invoiceType.getValue();
							if (TCG.isBlank(invoiceTypeId)) {
								TCG.showError('You must first select desired invoice type from the list.');
							}
							else {
								const cmpId = TCG.getComponentId(this.getDetailPageClass());
								TCG.createComponent(this.getDetailPageClass(), {
									id: cmpId,
									defaultData: {invoiceType: {'id': invoiceTypeId, 'name': invoiceType.getRawValue()}, businessCompany: TCG.getValue('company', this.getWindow().getMainForm().formValues)},
									openerCt: gridPanel,
									defaultIconCls: gridPanel.getWindow().iconCls
								});
							}
						}
					});
					toolBar.add('-');
				}
			},
			getTopToolbarFilters: function(toolbar) {
				return [
					{boxLabel: 'Include Client\'s Invoices&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeClient', checked: true},
					{boxLabel: 'Include Voided Invoices', xtype: 'checkbox', name: 'includeCanceled'}
				];
			},
			getLoadParams: function(firstLoad) {
				const params = {'companyId': TCG.getValue('company.id', this.getWindow().getMainForm().formValues)};
				if (TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeClient').getValue())) {
					params.includeClientOrClientRelationship = true;
				}
				const includeCanceled = TCG.getChildByName(this.getTopToolbar(), 'includeCanceled').getValue();
				if (TCG.isFalse(includeCanceled)) {
					params.excludeWorkflowStatusName = 'Cancelled';
				}
				return params;
			}
		}
	]
});


Clifton.billing.BillingFrequencies = [
	['Monthly', 'MONTHLY', 'Monthly (Uses 1 Month or 30 Days for Month/Day Counts)'],
	['Quarterly', 'QUARTERLY', 'Quarterly (Uses 3 Months or 91 Days for Month/Day Counts)'],
	['Semi-Annually', 'SEMI_ANNUALLY', 'Semi-Annually (Uses 6 Months or 182 Days for Month/Day Counts)'],
	['Annually', 'ANNUALLY', 'Annually (Uses 12 Month or 365 Days for Month/Day Counts)']
];

Clifton.billing.BillingScheduleAccrualFrequencies = [
	['Daily', 'DAILY', 'Daily - Accrues fees for each day in the period'],
	['Monthly', 'MONTHLY', 'Monthly - Accrues fees for each month in the period'],
	['Quarterly', 'QUARTERLY', 'Quarterly - Accrues fees for each quarter in the period']
	// Note: not including semi annual or annual because can't think of any cases they would be used
];


Clifton.billing.BillingScheduleFrequencies = [
	['Monthly', 'MONTHLY', 'Monthly (Uses Months for Schedule Amount Conversions)'],
	['Quarterly', 'QUARTERLY', 'Quarterly (Uses Months for Schedule Amount Conversions)'],
	['Semi-Annually', 'SEMI_ANNUALLY', 'Semi-Annually (Uses Months for Schedule Amount Conversions)'],
	['Annually', 'ANNUALLY', 'Annually (Uses Months for Schedule Amount Conversions).'],
	['Annually/Daily', 'ANNUALLY_DAILY', 'Annually (Uses Days for Schedule Amount Conversions) - Assumes 365 Days in a Year.'],
	['Actual Annually/Daily', 'ANNUALLY_DAILY_ACTUAL', 'Annually (Uses Days for Schedule Amount Conversions) - Uses Actual Days in the year.'],
	['Once', 'ONCE', 'One time Fee applied to each invoice while schedule is active - No Conversions or Prorating is applied']
];


Clifton.billing.invoice.BillingInvoiceRevenueTypes = [
	['NONE', 'None', 'Not Revenue.  i.e. Reimbursement for Legal Fees'],
	['REVENUE', 'Revenue', 'Gross Revenue'],
	['REVENUE_SHARE_INTERNAL', 'Revenue Share', 'Internal Revenue Share'],
	['REVENUE_SHARE_EXTERNAL', 'Broker Fee', 'Broker Fee - External Revenue Share']
];


Clifton.billing.invoice.getDefaultInvoiceStartDate = function(dt) {
	if (!dt) {
		dt = new Date();
	}
	const month = dt.getMonth();
	if (month < 3) {
		// 10/1
		return new Date(dt.getFullYear() - 1, 9, 1);
	}
	else if (month < 6) {
		// 1/1
		return new Date(dt.getFullYear(), 0, 1);
	}
	else if (month < 9) {
		// 4/1
		return new Date(dt.getFullYear(), 3, 1);
	}
	// 7/1
	return new Date(dt.getFullYear(), 6, 1);
};


Clifton.billing.invoice.getDefaultInvoiceEndDate = function(dt) {
	if (!dt) {
		dt = new Date();
	}
	const month = dt.getMonth();
	if (month < 3) {
		// 12/31
		return new Date(dt.getFullYear() - 1, 11, 31);
	}
	else if (month < 6) {
		// 3/31
		return new Date(dt.getFullYear(), 2, 31);
	}
	else if (month < 9) {
		// 6/30
		return new Date(dt.getFullYear(), 5, 30);
	}
	// 9/30
	return new Date(dt.getFullYear(), 8, 30);
};


Clifton.billing.invoice.InvoiceGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'billingInvoiceListFind',
	pageSize: 500,
	instructions: 'Billing Invoices are bills generated and sent to clients for payment.',
	// Default Filters
	defaultDateFilter: true,
	excludeCanceled: true,

	workflowStateName: undefined,
	workflowStatusName: undefined,

	// If true also includes the non-billing definition type drop down with add button for manual invoice entry
	includeAddButton: false,

	// Default Columns
	showDefinitionInfo: true,
	showWorkflowStateStatus: true,

	additionalPropertiesToRequest: 'invoiceType.billingDefinitionRequired',
	columns: [
		{header: 'Invoice Type', width: 90, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
		{header: 'Invoice #', width: 45, dataIndex: 'id', numberFormat: '0000'},
		{
			header: '<div class="lock" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, the invoice is locked.', exportHeader: 'Locked', dataIndex: 'locked', type: 'boolean',
			renderer: function(v, metaData, r) {
				return Clifton.system.renderSystemLockIconWithTooltip(v, 'invoice', r.data['lockedByUser.displayName'], r.data['lockNote']);
			}
		},
		{header: 'Locked By', width: 50, hidden: true, dataIndex: 'lockedByUser.displayName', filter: {searchFieldName: 'lockedByUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
		{header: 'Lock Note', width: 200, hidden: true, dataIndex: 'lockNote'},
		{header: 'Billing Definition ID', hidden: true, width: 35, dataIndex: 'billingDefinition.id'},
		{header: 'Billing Contract', hidden: true, width: 150, dataIndex: 'billingDefinition.billingContract.label'},
		{
			header: 'Company', width: 180, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'companyName'},
			renderer: function(v, metaData, r) {
				const note = r.data['billingDefinition.note'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['billingDefinition.note'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Billing Definition Note', hidden: true, width: 200, dataIndex: 'billingDefinition.note', filter: false, sortable: false},
		{
			header: 'Group #', width: 40, dataIndex: 'invoiceGroup.id', filter: {searchFieldName: 'invoiceGroupId', type: 'int'},
			renderer: function(grp, args, r) {
				if (TCG.isNotBlank(grp)) {
					return TCG.renderActionColumn('view', grp, 'View Billing Invoice Group ' + grp);
				}
				return '';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const groupId = TCG.getValue('invoiceGroup.id', grid.getStore().getAt(rowIndex).json);
					if (TCG.isNotBlank(groupId)) {
						grid.ownerGridPanel.editor.openDetailPage('Clifton.billing.invoice.InvoiceGroupWindow', grid.ownerGridPanel, groupId, rowIndex);
					}
				}
			}
		},
		{header: 'Payment In Advance', width: 35, dataIndex: 'billingDefinition.paymentInAdvance', type: 'boolean', hidden: true},
		{header: 'Invoice Date', width: 47, dataIndex: 'invoiceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{header: 'Posting Required', width: 40, dataIndex: 'billingDefinition.invoicePostedToPortal', type: 'boolean', tooltip: 'If checked invoice is expected to be posted to the portal (option on the billing definition).', filter: {searchFieldName: 'definitionInvoicePostedToPortal'}},
		{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this invoice on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
		{
			header: 'Created By', width: 40, dataIndex: 'createUserId', idDataIndex: 'createUserId', filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{
			header: 'Updated By', width: 45, dataIndex: 'updateUserId', idDataIndex: 'updateUserId', filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 50, filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{
			header: 'Workflow State', dataIndex: 'workflowState.name', width: 50, filter: {searchFieldName: 'workflowStateName'},
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Open') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'Closed') {
					metaData.css = 'amountPositive';
				}
				else if (sts !== 'Cancelled') {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{
			header: 'Violation Status', width: 60, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.json.invoiceType.billingDefinitionRequired)) {
					return (Clifton.rule.violation.renderViolationStatus(v));
				}
				return 'N/A';
			}
		},
		{
			header: 'Revenue (Gross)', width: 45, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'},
			tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Broker Fee', width: 45, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Broker Fee - revenue that we share (pay out) to an external party',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue Net', width: 45, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue Share', width: 45, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue (Final)', width: 45, hidden: true, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees and Revenue Share',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{header: 'Other', width: 40, dataIndex: 'otherTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}},
		{header: 'Total', width: 40, dataIndex: 'totalAmount', hidden: true, type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}},
		{header: 'Paid Amount', width: 40, dataIndex: 'paidAmount', hidden: true, type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
		{header: 'Unpaid Amount', width: 40, dataIndex: 'unpaidAmount', hidden: true, type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
		{header: 'Void Reason', width: 75, dataIndex: 'voidReason', hidden: true}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
		deleteEnabled: true,
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			if (TCG.isTrue(gridPanel.includeAddButton)) {
				toolBar.add(new TCG.form.ComboBox({name: 'invoiceType', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=false', width: 150, listWidth: 200, emptyText: '< Select Invoice Type >'}));
				toolBar.add({
					text: 'Add',
					tooltip: 'Create a new manual Invoice of selected type',
					iconCls: 'add',
					scope: this,
					handler: function() {
						const invoiceType = TCG.getChildByName(toolBar, 'invoiceType');
						const invoiceTypeId = invoiceType.getValue();
						if (TCG.isBlank(invoiceTypeId)) {
							TCG.showError('You must first select desired invoice type from the list.');
						}
						else {
							const cmpId = TCG.getComponentId(this.getDetailPageClass());
							TCG.createComponent(this.getDetailPageClass(), {
								id: cmpId,
								defaultData: {invoiceType: {'id': invoiceTypeId, 'name': invoiceType.getRawValue()}},
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						}
					}
				});
				toolBar.add('-');
			}
		},
		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected invoice',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const editor = this;
					editor.getGridPanel().deleteInvoice(editor);
				}
			});
			toolBar.add('-');
		}
	},
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		// Default presentation
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('businessCompany.name'), TCG.isFalse(this.showDefinitionInfo));
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 175, xtype: 'toolbar-combo', name: 'billingTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Billing Tags'},
			{fieldLabel: 'Revenue Type', xtype: 'toolbar-combo', name: 'revenueType', width: 125, mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}},
			{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, minListWidth: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true}
		];
	},
	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			if (TCG.isTrue(this.defaultDateFilter)) {
				this.setFilterValue('invoiceDate', {'after': Clifton.billing.invoice.getDefaultInvoiceStartDate()});
			}
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
			}
		}
		const params = {requestedMaxDepth: 5};
		if (this.workflowStateName) {
			params.workflowStateNameEquals = this.workflowStateName;
		}
		else if (this.workflowStatusName) {
			params.workflowStatusNameEquals = this.workflowStatusName;
		}
		else if (TCG.isTrue(this.excludeCanceled)) {
			params.excludeWorkflowStatusName = 'Cancelled';
		}
		let v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
		if (v) {
			params.investmentAccountGroupId = v;
		}
		v = TCG.getChildByName(t, 'revenueType').getValue();
		if (v) {
			params.revenueType = v;
		}
		v = TCG.getChildByName(t, 'teamSecurityGroupId').getValue();
		if (v) {
			params.teamSecurityGroupId = v;
		}

		const tag = TCG.getChildByName(t, 'billingTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Billing Tags';
			params.categoryHierarchyId = tag.getValue();
			params.categoryTableName = 'BillingDefinition';
			params.categoryLinkFieldPath = 'billingDefinition';
		}
		return params;
	},

	deleteInvoice: function(editor) {
		const grid = editor.grid;
		const sm = grid.getSelectionModel();

		if (sm.getCount() === 0) {
			TCG.showError('Please select an invoice to be deleted.', 'No Invoice(s) Selected');
		}
		else if (sm.getCount() !== 1) {
			TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
		}
		else {
			const grpId = sm.getSelected().get('invoiceGroup.id');
			if (TCG.isBlank(grpId)) {
				Ext.Msg.confirm('Delete Selected Invoice', 'Would you like to delete selected invoice?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Deleting...',
							params: editor.getDeleteParams(sm),
							conf: {rowId: sm.getSelected().id},
							onLoad: function(record, conf) {
								grid.getStore().remove(sm.getSelected());
								grid.ownerCt.updateCount();
							}
						});
						loader.load(editor.getDeleteURL());
					}
				});
			}
			else {
				Ext.Msg.confirm('Delete Selected Invoice Group', 'Warning - This invoice is part of an invoice group.  Deleting the invoice will delete the entire invoice group and all invoices in that group.  Would you like to delete selected invoice group?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Deleting...',
							params: {id: grpId},
							conf: {rowId: sm.getSelected().id},
							onLoad: function(record, conf) {
								grid.ownerCt.reload(); // Need to fully reload since it deletes multiple
							}
						});
						loader.load('billingInvoiceGroupDelete.json');
					}
				});
			}
		}
	}
});
Ext.reg('billing-invoiceGrid', Clifton.billing.invoice.InvoiceGrid);


Clifton.billing.payment.BillingPaymentGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'billingPaymentListFind',
	xtype: 'gridpanel',
	pageSize: 500,
	instructions: 'Billing Payments are payments received from clients.  Payments are allocated to invoices that we send clients for our services rendered.',
	showAllocationInfo: false,
	showClientInfo: true,
	showVoidedColumn: false,

	columns: [
		{header: 'Payment #', width: 40, dataIndex: 'id'},
		{header: 'Company', width: 170, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'companyName'}},
		{header: 'Payment Notes', width: 100, dataIndex: 'notes'},
		{
			header: 'Status', width: 45, dataIndex: 'billingPaymentStatusLabel', filter: false, sortable: false,
			renderer: function(v, metaData, r) {
				const sts = r.data['billingPaymentStatus'];
				if (sts === 'NONE') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'FULL') {
					metaData.css = 'amountPositive';
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Payment Type', width: 40, dataIndex: 'paymentType'},
		{header: 'Payment Date', width: 45, dataIndex: 'paymentDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Payment Amount', width: 48, dataIndex: 'paymentAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
		{header: 'Allocated Amount', hidden: true, width: 40, dataIndex: 'allocatedAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
		{header: 'Available Balance', hidden: true, width: 40, dataIndex: 'availableAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', sortable: false, filter: false},
		{header: 'Voided', hidden: true, width: 40, dataIndex: 'voided', type: 'boolean'}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.billing.payment.PaymentWindow',
		deleteButtonName: 'Void',
		deleteButtonTooltip: 'Void selected payment',
		deleteConfirmMessage: 'Payments cannot be removed, but they can be voided.  Would you like to Void selected payment?',
		getDefaultData: function(gridPanel) {
			if (TCG.isNotNull(gridPanel.getDefaultData)) {
				return gridPanel.getDefaultData(gridPanel);
			}
			return {
				allocatedAmount: 0
			};
		}
	},
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		// Default presentation
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('allocatedAmount'), TCG.isFalse(this.showAllocationInfo));
		cm.setHidden(cm.findColumnIndex('availableAmount'), TCG.isFalse(this.showAllocationInfo));
		cm.setHidden(cm.findColumnIndex('businessCompany.name'), TCG.isFalse(this.showClientInfo));
		cm.setHidden(cm.findColumnIndex('voided'), TCG.isFalse(this.showVoidedColumn));
	}
});
Ext.reg('billing-paymentGrid', Clifton.billing.payment.BillingPaymentGrid);


Clifton.billing.payment.BillingPaymentAccountAllocationGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'billingPaymentInvestmentAccountAllocationListFind',
	xtype: 'gridpanel',
	rowSelectionModel: 'checkbox',
	pageSize: 500,
	instructions: 'For invoices that use billing definitions, billing amounts are allocated to client accounts.  Payments are also allocated at the account level.  This lists the account billing amount for each invoice and their payment status.',
	additionalPropertiesToRequest: ['investmentAccount.id|billingInvoice.note'],
	columns: [
		{header: 'Invoice #', width: 35, dataIndex: 'billingInvoice.id', filter: {searchFieldName: 'billingInvoiceId'}, type: 'int', numberFormat: '0000'},
		{header: 'Invoice Date', width: 35, dataIndex: 'billingInvoice.invoiceDate', filter: {searchFieldName: 'invoiceDate'}, type: 'date'},
		{header: 'Invoice Type', width: 65, dataIndex: 'billingInvoice.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
		{
			header: 'Invoice Company', width: 125, dataIndex: 'billingInvoice.businessCompany.name', filter: {searchFieldName: 'invoiceCompanyName'},
			renderer: function(v, metaData, r) {
				const note = r.json['billingInvoice.note'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.json['billingInvoice.note'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Investment Account', width: 175, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'investmentAccountLabel'}},
		{
			header: 'Invoice State', width: 50, dataIndex: 'billingInvoice.workflowState.name', filter: {searchFieldName: 'invoiceWorkflowStateNameContains'},
			renderer: function(v, metaData, r) {
				const sts = r.data['billingInvoice.workflowStatus.name'];
				if (sts === 'Open') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'Closed') {
					metaData.css = 'amountPositive';
				}
				else if (sts !== 'Cancelled') {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Invoice Status', width: 50, hidden: true, dataIndex: 'billingInvoice.workflowStatus.name', filter: {searchFieldName: 'invoiceWorkflowStatusNameContains'}},

		{
			header: 'Revenue (Gross)', width: 45, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'},
			tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.'
		},
		{
			header: 'Broker Fee', width: 45, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Broker Fee - revenue that we share (pay out) to an external party'
		},
		{
			header: 'Revenue Net', width: 45, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees'
		},
		{
			header: 'Revenue Share', width: 45, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.'
		},
		{
			header: 'Revenue (Final)', width: 45, hidden: true, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees and Revenue Share'
		},
		{header: 'Other', width: 40, dataIndex: 'otherTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}},

		{
			header: 'Paid Amount', width: 50, dataIndex: 'paidAmount', type: 'currency', summaryType: 'sum',
			renderer: function(paidAmount, args, r) {
				if (TCG.isNotBlank(paidAmount) && paidAmount !== 0) {
					return TCG.renderActionColumn('view', TCG.renderAmount(paidAmount), 'View Payment Details');
				}
				return TCG.renderAmount(paidAmount);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v);
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const paidAmount = TCG.getValue('paidAmount', grid.getStore().getAt(rowIndex).json);
					if (TCG.isNotBlank(paidAmount) && paidAmount !== 0) {
						const accountId = TCG.getValue('investmentAccount.id', grid.getStore().getAt(rowIndex).json);
						const invoiceId = TCG.getValue('billingInvoice.id', grid.getStore().getAt(rowIndex).json);
						const gridPanel = grid.ownerGridPanel;
						TCG.createComponent('Clifton.billing.payment.PaymentAllocationListWindow', {
							defaultData: {'billingInvoiceId': invoiceId, 'investmentAccountId': accountId},
							openerCt: gridPanel
						});
					}
				}
			}
		},
		{header: 'Unpaid Amount', width: 50, dataIndex: 'unpaidAmount', type: 'currency', summaryType: 'sum'}
	],
	plugins: {ptype: 'gridsummary'},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Platform', xtype: 'toolbar-combo', name: 'businessCompanyPlatformId', width: 180, url: 'businessCompanyPlatformListFind.json'},
			{fieldLabel: 'Tag', width: 180, xtype: 'toolbar-combo', name: 'billingTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Billing Tags'},
			{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'}
		];
	},
	editor: {
		deleteEnabled: false,
		detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.billingInvoice.id;
		},
		addToolbarAddButton: function(toolBar, gridPanel) {
			toolBar.add({
				text: 'Apply Payment',
				tooltip: 'Apply Payment (New or Existing) to selected rows',
				iconCls: 'add',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one row to apply payment to.', 'No Row(s) Selected');
					}
					else {
						const selections = sm.getSelections();
						const defaultData = {};
						defaultData.applyPaymentTo = 'account';
						defaultData.billingPaymentAllocationList = [];

						let counter = 0;
						for (let i = 0; i < selections.length; i++) {
							const s = selections[i].json;
							counter = counter - 10;
							if (s.unpaidAmount !== 0) {
								const invoice = s.billingInvoice;
								const account = s.investmentAccount;

								defaultData.billingPaymentAllocationList.push({
									class: 'com.clifton.billing.payment.BillingPaymentAllocation',
									id: counter,
									investmentAccount: account,
									billingInvoice: invoice,
									allocatedAmount: s.unpaidAmount,
									unpaidAmount: s.unpaidAmount
								});
							}
						}
						TCG.createComponent('Clifton.billing.payment.PaymentAllocationEntryWindow', {
							defaultData: defaultData,
							defaultDataIsReal: true,
							openerCt: gridPanel
						});
					}
				}
			});
			toolBar.add('-');
		}
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			this.setFilterValue('unpaidAmount', {'ne': 0});
		}
		const params = {};
		const tag = TCG.getChildByName(t, 'billingTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Billing Tags';
			params.categoryHierarchyId = tag.getValue();
			params.categoryTableName = 'BillingDefinition';
			params.categoryLinkFieldPath = 'billingInvoice.billingDefinition';
		}

		const acctGroup = TCG.getChildByName(t, 'investmentAccountGroupId');
		if (TCG.isNotBlank(acctGroup.getValue())) {
			params.investmentAccountGroupId = acctGroup.getValue();
		}
		const platform = TCG.getChildByName(t, 'businessCompanyPlatformId');
		if (TCG.isNotBlank(platform.getValue())) {
			params.businessCompanyPlatformId = platform.getValue();
		}
		return params;
	}
});
Ext.reg('billing-payment-account-allocation-grid', Clifton.billing.payment.BillingPaymentAccountAllocationGrid);


Clifton.billing.DefinitionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'billingDefinitionListFind',
	xtype: 'gridpanel',
	instructions: 'Billing Definition links billing rules defined in a contract (IMA, etc.) to corresponding investment accounts. A separate invoice is generated for each active billing definition.',
	wikiPage: 'IT/Billing Requirements',

	// Can be used to filter results to specific workflow state/status
	workflowStateName: undefined,
	workflowStatusName: undefined,
	defaultActiveFilter: true,
	// Acceptable Options are ALWAYS, NEVER, OPTIONAL_TRUE, OPTIONAL_FALSE
	// ALWAYS = excludeWorkflowStateName = Archived in ALL requests
	// NEVER = do nothing
	// OPTIONAL_TRUE, OPTIONAL_FALSE = Adds toolbar check box and defaults to true or false
	excludeArchived: 'ALWAYS',

	// Switch to checkbox if using transitions
	rowSelectionModel: 'single',

	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Invoice Type', width: 75, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
		{
			header: 'Company', width: 200, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'companyName'},
			renderer: function(v, metaData, r) {
				const note = r.data.note;
				if (TCG.isNotBlank(note)) {
					const qtip = r.data.note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Contract', width: 200, dataIndex: 'billingContract.name', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
		{header: 'Broker Fee Company', width: 200, hidden: true, dataIndex: 'revenueShareExternalCompany.name', filter: {searchFieldName: 'revenueShareExternalCompanyName'}, tooltip: 'The external party that we share revenue with. Is included in Net Revenue amounts.'},
		{header: 'Revenue Share Company', width: 200, hidden: true, dataIndex: 'revenueShareInternalCompany.name', filter: {searchFieldName: 'revenueShareInternalCompanyName'}, tooltip: 'The internal party (another PPA office or EV) that we share revenue with. Used for internal reporting only.'},
		{header: 'Billing CCY', width: 50, dataIndex: 'billingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'currencyId', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol'}},
		{
			header: 'Frequency', width: 50, dataIndex: 'billingFrequency', filter: {
				type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.billing.BillingFrequencies
				})
			}
		},
		{header: 'Payment In Advance', width: 50, dataIndex: 'paymentInAdvance', type: 'boolean'},
		{
			header: 'Workflow State', width: 60, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Approved') {
					if (v !== 'Archived') {
						metaData.css = 'amountPositive';
					}
				}
				else if (sts === 'Draft') {
					metaData.css = 'amountNegative';
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Workflow Status', width: 60, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{header: 'Start Date', width: 40, dataIndex: 'startDate'},
		{header: 'Second Year Start Date', width: 60, dataIndex: 'secondYearStartDate', hidden: true},
		{header: 'End Date', width: 40, dataIndex: 'endDate'},
		{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false},
		{header: 'Post to Portal', width: 40, dataIndex: 'invoicePostedToPortal', type: 'boolean', tooltip: 'If checked invoices for this billing definition are posted to the portal.'}
	],
	editor: {
		detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
		ptype: 'workflowAwareEntity-grideditor',
		tableName: 'BillingDefinition',


		// Overridden because the option is on the gridpanel
		isAddEnabled: function() {
			const gp = this.getGridPanel();
			return !(gp && gp.drillDownOnly);

		},

		isDeleteEnabled: function() {
			const gp = this.getGridPanel();
			return !(gp && gp.drillDownOnly);

		}
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad && TCG.isTrue(this.defaultActiveFilter)) {
			this.setFilterValue('active', true);
		}
		const params = {};
		const t = this.getTopToolbar();
		const acct = TCG.getChildByName(t, 'clientAccount');
		if (TCG.isNotBlank(acct.getValue())) {
			params.investmentAccountId = acct.getValue();
		}
		const acctGroup = TCG.getChildByName(t, 'clientAccountGroup');
		if (TCG.isNotBlank(acctGroup.getValue())) {
			params.investmentAccountGroupId = acctGroup.getValue();
		}
		const revType = TCG.getChildByName(t, 'revenueType');
		if (TCG.isNotBlank(revType.getValue())) {
			params.revenueType = revType.getValue();
		}
		const tag = TCG.getChildByName(this.getTopToolbar(), 'billingTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Billing Tags';
			params.categoryHierarchyId = tag.getValue();
		}

		const excludeArchivedFilter = TCG.getChildByName(this.getTopToolbar(), 'excludeArchived');
		// Only applies if we aren't already filtering on workflow state name
		if (TCG.isNotBlank(this.workflowStateName)) {
			params.workflowStateNameEquals = this.workflowStateName;
		}
		else if (this.excludeArchived === 'ALWAYS' || (excludeArchivedFilter && excludeArchivedFilter.checked)) {
			params.excludeWorkflowStateName = 'Archived';
		}
		if (TCG.isNotBlank(this.workflowStatusName)) {
			params.workflowStatusNameEquals = this.workflowStatusName;
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Tag', width: 175, xtype: 'toolbar-combo', name: 'billingTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Billing Tags'});
		filters.push({fieldLabel: 'Revenue Type', xtype: 'toolbar-combo', name: 'revenueType', width: 125, mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}});
		filters.push({fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientAccountGroup', width: 180, url: 'investmentAccountGroupListFind.json', displayField: 'label'});
		filters.push({fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'clientAccount', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'});
		if (this.excludeArchived === 'OPTIONAL_TRUE' || this.excludeArchived === 'OPTIONAL_FALSE') {
			filters.push('-');
			filters.push({fieldLabel: '', boxLabel: 'Exclude Archived', xtype: 'toolbar-checkbox', name: 'excludeArchived', checked: (this.excludeArchived === 'OPTIONAL_TRUE')});
		}

		return filters;
	}
});
Ext.reg('billing-definitionGrid', Clifton.billing.DefinitionGrid);

Clifton.billing.DefinitionsGrid_ByClient = Ext.extend(TCG.grid.GridPanel, {
	name: 'billingDefinitionListFind',
	isIncludeRelatedClientCheckbox: function() {
		return false;
	},
	getRelatedClientCheckboxLabel: function() {
		return 'Include Parent/Child Client\'s Definitions';
	},

	instructions: 'Billing Definition links billing rules defined in a contract (IMA, etc.) to corresponding investment accounts. A separate invoice is generated for each active billing definition.',
	columns: [
		{header: 'ID', width: 35, dataIndex: 'id', hidden: true},
		{header: 'Invoice Type', width: 75, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
		{
			header: 'Contract', width: 200, dataIndex: 'billingContract.name', filter: {searchFieldName: 'contractName'},
			renderer: function(v, metaData, r) {
				const note = r.data.note;
				if (TCG.isNotBlank(note)) {
					const qtip = r.data.note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
		{header: 'Billing CCY', width: 50, dataIndex: 'billingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'billingCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
		{
			header: 'Frequency', width: 60, dataIndex: 'billingFrequency', filter: {
				type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.billing.BillingFrequencies
				})
			}
		},
		{header: 'Payment In Advance', width: 60, dataIndex: 'paymentInAdvance', type: 'boolean'},
		{
			header: 'Workflow State', width: '60', dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Approved') {
					metaData.css = 'amountPositive';
				}
				else if (sts === 'Draft') {
					metaData.css = 'amountNegative';
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Workflow Status', width: '60', dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{header: 'Start Date', width: 40, dataIndex: 'startDate'},
		{header: 'Second Year Start Date', width: 60, dataIndex: 'secondYearStartDate', hidden: true},
		{header: 'End Date', width: 40, dataIndex: 'endDate'},
		{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
	],
	editor: {
		detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
		getDefaultData: function(gridPanel) { // defaults client for the detail page
			return {
				businessCompany: TCG.getValue('company', gridPanel.getWindow().getMainForm().formValues)
			};
		}
	},
	getLoadParams: function(firstLoad) {
		const params = {companyId: TCG.getValue('company.id', this.getWindow().getMainForm().formValues)};
		if (TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeAccountsExist').getValue())) {
			params.includeAccountsExistOfClientOrClientRelationship = true;
		}
		if (TCG.isTrue(this.isIncludeRelatedClientCheckbox())) {
			const inclRelated = TCG.getChildByName(this.getTopToolbar(), 'includeRelatedClients').checked;
			if (inclRelated) {
				delete params.companyId;
				params.companyIdOrRelatedCompany = TCG.getValue('company.id', this.getWindow().getMainForm().formValues);
			}
		}
		const includeClientRelationship = TCG.getChildByName(this.getTopToolbar(), 'includeClientRelationship');
		if (includeClientRelationship && TCG.isTrue(includeClientRelationship.getValue())) {
			params.includeClientOrClientRelationship = true;
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [{boxLabel: 'Include Accounts Referenced In Billing Definition&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeAccountsExist', checked: true, qtip: 'If checked will include billing definitions where there is an account included on the accounts tab under this client.'}];
		if (TCG.isNotBlank(TCG.getValue('clientRelationship.id', this.getWindow().getMainForm().formValues))) {
			filters.push({boxLabel: 'Include Client Relationship\'s Billing Definitions&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeClientRelationship', checked: true});
		}
		if (TCG.isTrue(this.isIncludeRelatedClientCheckbox())) {
			filters.push({boxLabel: this.getRelatedClientCheckboxLabel(), xtype: 'toolbar-checkbox', name: 'includeRelatedClients'});
		}
		return filters;
	}
});
Ext.reg('billing-definitionsGrid-byClient', Clifton.billing.DefinitionsGrid_ByClient);


Clifton.billing.InvoicesGrid_ByClient = Ext.extend(TCG.grid.GridPanel, {
	isIncludeRelatedClientCheckbox: function() {
		return false;
	},
	getRelatedClientCheckboxLabel: function() {
		return 'Include Parent/Child Client\'s Invoices&nbsp;&nbsp;';
	},

	name: 'billingInvoiceListFind',
	xtype: 'gridpanel',
	border: 1,
	flex: 1,

	instructions: 'Billing Invoices are bills generated and sent to clients for payment. The following invoices were generated for the selected client.',
	columns: [
		{header: 'Invoice #', width: 35, dataIndex: 'id', numberFormat: '0000'},
		{
			header: '', width: 15, dataIndex: 'locked', type: 'boolean',
			renderer: function(v, metaData, r) {
				return Clifton.system.renderSystemLockIconWithTooltip(v, 'invoice', r.data['lockedByUser.displayName'], r.data['lockNote']);
			}
		},
		{header: 'Locked By', width: 50, hidden: true, dataIndex: 'lockedByUser.displayName', filter: {searchFieldName: 'lockedByUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
		{header: 'Lock Note', width: 200, hidden: true, dataIndex: 'lockNote'},
		{header: 'Invoice Type', width: 50, dataIndex: 'invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}, hidden: true},
		{
			header: 'Contract/Invoice Type', width: 180, dataIndex: 'billingDefinition.billingContract.name', filter: false, sortable: false,
			renderer: function(v, metaData, r) {
				const note = r.data['billingDefinition.note'];
				if (TCG.isNotBlank(note)) {
					const qtip = note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				else {
					if (TCG.isBlank(v)) {
						return r.data['invoiceType.name'];
					}
				}
				return v;
			}
		},
		{header: 'Billing Definition Note', hidden: true, width: 200, dataIndex: 'billingDefinition.note', filter: false, sortable: false},
		{
			header: 'Group #', width: 30, dataIndex: 'invoiceGroup.id',
			renderer: function(grp, args, r) {
				if (TCG.isNotBlank(grp)) {
					return TCG.renderActionColumn('view', grp, 'View Billing Invoice Group ' + grp);
				}
				return '';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const groupId = TCG.getValue('invoiceGroup.id', grid.getStore().getAt(rowIndex).json);
					if (TCG.isNotBlank(groupId)) {
						grid.ownerGridPanel.editor.openDetailPage('Clifton.billing.invoice.InvoiceGroupWindow', grid.ownerGridPanel, groupId, rowIndex);
					}
				}
			}
		},
		{header: 'Invoice Date', width: 40, dataIndex: 'invoiceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this invoice on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
		{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 50, filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{
			header: 'Workflow State', dataIndex: 'workflowState.name', width: 50, filter: {searchFieldName: 'workflowStateName'},
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Open') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'Closed' || sts === 'Cancelled') {
					metaData.css = 'amountPositive';
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{
			header: 'Violation Status', width: 60, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{
			header: 'Revenue (Gross)', width: 45, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'},
			tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Broker Fee', width: 45, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Broker Fee - revenue that we share (pay out) to an external party',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue Net', width: 45, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue Share', width: 45, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{
			header: 'Revenue (Final)', width: 45, hidden: true, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
			tooltip: 'Gross Revenue and Broker Fees and Revenue Share',
			summaryTotalCondition: function(data) {
				return TCG.isNotEquals('Void', data.workflowState.name);
			}
		},
		{header: 'Other', width: 40, dataIndex: 'otherTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			toolBar.add(new TCG.form.ComboBox({name: 'invoiceType', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=false', width: 150, listWidth: 200, emptyText: '< Select Invoice Type >'}));
			toolBar.add({
				text: 'Add',
				tooltip: 'Create a new manual Invoice of selected type',
				iconCls: 'add',
				scope: this,
				handler: function() {
					const invoiceType = TCG.getChildByName(toolBar, 'invoiceType');
					const invoiceTypeId = invoiceType.getValue();
					if (TCG.isBlank(invoiceTypeId)) {
						TCG.showError('You must first select desired invoice type from the list.');
					}
					else {
						const cmpId = TCG.getComponentId(this.getDetailPageClass());
						TCG.createComponent(this.getDetailPageClass(), {
							id: cmpId,
							defaultData: {invoiceType: {'id': invoiceTypeId, 'name': invoiceType.getRawValue()}, businessCompany: TCG.getValue('company', gridPanel.getWindow().getMainForm().formValues)},
							openerCt: gridPanel,
							defaultIconCls: gridPanel.getWindow().iconCls
						});
					}
				}
			});
			toolBar.add('-');
		},
		getDefaultData: function(gridPanel) { // defaults client for the detail page
			return {
				businessCompany: TCG.getValue('company', gridPanel.getWindow().getMainForm().formValues)
			};
		}
	},

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (TCG.isNotBlank(TCG.getValue('clientRelationship.id', this.getWindow().getMainForm().formValues))) {
			filters.push({boxLabel: 'Include Client Relationship\'s Invoices&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeClientRelationship', checked: true});
		}
		if (TCG.isTrue(this.isIncludeRelatedClientCheckbox())) {
			filters.push({
				boxLabel: this.getRelatedClientCheckboxLabel(), xtype: 'checkbox', name: 'includeRelatedClients',
				listeners: {
					check: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			});
		}
		filters.push({boxLabel: 'Include Voided Invoices', xtype: 'checkbox', name: 'includeCanceled'});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const params = {companyId: TCG.getValue('company.id', this.getWindow().getMainForm().formValues)};
		const includeCanceled = TCG.getChildByName(this.getTopToolbar(), 'includeCanceled').getValue();
		if (TCG.isFalse(includeCanceled)) {
			params.excludeWorkflowStatusName = 'Cancelled';
		}

		if (TCG.isTrue(this.isIncludeRelatedClientCheckbox())) {
			const inclRelated = TCG.getChildByName(this.getTopToolbar(), 'includeRelatedClients').checked;
			if (inclRelated) {
				delete params.companyId;
				params.companyIdOrRelatedCompany = TCG.getValue('company.id', this.getWindow().getMainForm().formValues);
			}
		}
		const includeClientRelationship = TCG.getChildByName(this.getTopToolbar(), 'includeClientRelationship');
		if (includeClientRelationship && TCG.isTrue(includeClientRelationship.getValue())) {
			params.includeClientOrClientRelationship = true;
		}
		return params;
	}
});
Ext.reg('billing-invoicesGrid-byClient', Clifton.billing.InvoicesGrid_ByClient);


Clifton.billing.commission.schedule.ScheduleGrid = Ext.extend(TCG.grid.GridPanel, {
		xtype: 'gridpanel',
		name: 'billingCommissionScheduleListFind',
		instructions: 'Sales Commission Schedules',
		// Can be used to limit results on each tab
		workflowStateName: undefined,
		workflowStatusName: undefined,

		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Schedule Name', width: 100, dataIndex: 'name'},
			{header: 'Description', width: 200, dataIndex: 'description'},
			{
				header: 'Workflow State', width: 80, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
				renderer: function(v, metaData, r) {
					const sts = r.data['workflowStatus.name'];
					if (sts === 'Final') {
						metaData.css = 'amountPositive';
					}
					else if (sts === 'Draft') {
						metaData.css = 'amountNegative';
					}
					else if (v !== 'Canceled') {
						metaData.css = 'amountAdjusted';
					}
					return v;
				}
			},
			{header: 'Workflow Status', width: 40, hidden: true, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}}
		],
		editor: {
			detailPageClass: 'Clifton.billing.commission.schedule.ScheduleWindow'
		},
		getLoadParams: function(firstLoad) {
			const params = {};
			if (TCG.isNotBlank(this.workflowStatusName)) {
				params.workflowStatusNameEquals = this.workflowStatusName;
			}
			if (TCG.isNotBlank(this.workflowStateName)) {
				params.workflowStateNameEquals = this.workflowStateName;
			}
			return params;
		}
	}
);
Ext.reg('billing-commission-schedule-grid', Clifton.billing.commission.schedule.ScheduleGrid);


Clifton.billing.commission.schedule.AssignmentGrid = Ext.extend(TCG.grid.GridPanel, {
		xtype: 'gridpanel',
		name: 'billingCommissionScheduleAssignmentListFind',
		instructions: 'Sales Commission Assignments define the commission schedule to apply to an account',
		importTableName: 'BillingCommissionScheduleAssignment',
		// Can be used to limit results on each tab
		workflowStateName: undefined,
		workflowStatusName: undefined,
		// Screen options
		defaultActiveOnDate: true,
		showBulkAddButton: false,
		// Switch to checkbox if using transitions
		rowSelectionModel: 'single',
		getTopToolbarFilters: function(toolbar) {
			return [
				{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
			];
		},
		additionalPropertiesToRequest: 'endDate|terminateDate|referenceOne.description',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false},
			{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
			{header: 'Client', width: 150, dataIndex: 'referenceTwo.businessClient.label', hidden: true, filter: {searchFieldName: 'clientLabel'}},
			{header: 'Account #', width: 50, dataIndex: 'referenceTwo.number', filter: {searchFieldName: 'investmentAccountLabel'}},
			{
				header: 'Account Name', width: 180, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'investmentAccountLabel', sortFieldName: 'investmentAccountName'},
				renderer: function(v, metaData, r) {
					const note = r.data.notes;
					if (TCG.isNotBlank(note)) {
						const qtip = note;
						metaData.css = 'amountAdjusted';
						metaData.attr = TCG.renderQtip(qtip);
					}
					return v;
				}
			},
			{header: 'Service', width: 120, dataIndex: 'referenceTwo.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
			{header: 'Service Processing Type', width: 80, dataIndex: 'referenceTwo.serviceProcessingType.name', filter: {type: 'combo', searchFieldName: 'businessServiceProcessingTypeId', url: 'businessServiceProcessingTypeListFind.json'}, hidden: true},
			{
				header: 'Schedule', width: 150, dataIndex: 'referenceOne.name', filter: {searchFieldName: 'commissionScheduleName'},
				renderer: function(v, metaData, r) {
					const note = r.json.referenceOne.description;
					if (TCG.isNotBlank(note)) {
						metaData.attr = TCG.renderQtip(note);
					}
					return v;
				}
			},
			{header: 'Notes', width: 200, dataIndex: 'notes', hidden: true},
			{header: 'Recipient', width: 100, dataIndex: 'recipientContact.label', filter: {searchFieldName: 'recipientContactId', type: 'combo', url: 'businessContactListFind.json?ourCompany=true', displayField: 'label'}},
			{header: 'Start Date', width: 50, dataIndex: 'startDate'},
			{header: 'End Date', width: 50, dataIndex: 'coalesceTerminateEndDate'},
			{header: 'Discount', width: 50, useNull: true, dataIndex: 'commissionDiscount', type: 'currency'},
			{
				header: 'Workflow State', width: 80, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
				renderer: function(v, metaData, r) {
					const sts = r.data['workflowStatus.name'];
					if (sts === 'Final') {
						metaData.css = 'amountPositive';
					}
					else if (sts === 'Draft') {
						metaData.css = 'amountNegative';
					}
					else if (sts === 'Pending' || sts === 'Inactive') {
						metaData.css = 'amountAdjusted';
					}
					return v;
				}
			},
			{header: 'Workflow Status', width: 80, hidden: true, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}}
		],
		editor: {
			detailPageClass: 'Clifton.billing.commission.schedule.AssignmentWindow',
			ptype: 'workflowAwareEntity-grideditor',
			tableName: 'BillingCommissionScheduleAssignment',
			addToolbarAddButton: function(toolBar, gridPanel) {
				TCG.grid.GridEditor.prototype.addToolbarAddButton.apply(this, arguments);
				if (TCG.isTrue(gridPanel.showBulkAddButton)) {
					toolBar.add({
						text: 'Bulk Add',
						tooltip: 'Add same schedule assignment to a list of accounts.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							this.openDetailPage('Clifton.billing.commission.schedule.BulkAssignmentAddWindow', gridPanel);
						}
					});
					toolBar.add('-');
				}
			},

			// Overridden because the option is on the gridpanel
			isAddEnabled: function() {
				const gp = this.getGridPanel();
				return !(gp && gp.drillDownOnly);
			},

			isDeleteEnabled: function() {
				const gp = this.getGridPanel();
				return !(gp && gp.drillDownOnly);

			}
		},
		getLoadParams: function(firstLoad) {
			const t = this.getTopToolbar();
			const bd = TCG.getChildByName(t, 'activeOnDate');
			const params = {};

			if (firstLoad && TCG.isTrue(this.defaultActiveOnDate)) {
				// default to today
				if (TCG.isBlank(bd.getValue())) {
					bd.setValue((new Date()).format('m/d/Y'));
				}
			}

			if (TCG.isNotBlank(bd.getValue())) {
				params.activeOnDate = (bd.getValue()).format('m/d/Y');
			}

			if (TCG.isNotBlank(this.workflowStatusName)) {
				params.workflowStatusNameEquals = this.workflowStatusName;
			}
			if (TCG.isNotBlank(this.workflowStateName)) {
				params.workflowStateNameEquals = this.workflowStateName;
			}
			return params;
		}
	}
);
Ext.reg('billing-commission-schedule-assignment-grid', Clifton.billing.commission.schedule.AssignmentGrid);
