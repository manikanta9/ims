Clifton.billing.BillingSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'billingSetupWindow',
	title: 'Billing Setup',
	iconCls: 'billing',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Schedule Templates',
				items: [{
					xtype: 'gridpanel',
					name: 'billingScheduleTemplateListFind',
					instructions: 'The following schedules templates are common schedules that can be used to easily create a new schedule using pre-defined information.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Template Name', width: 150, dataIndex: 'name'},
						{header: 'Default Schedule Name', width: 100, dataIndex: 'description'},
						{
							header: 'Frequency', width: 60, dataIndex: 'scheduleFrequency',
							filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleFrequencies
								})
							}
						},
						{header: 'Revenue Type', width: 65, dataIndex: 'revenueTypeLabel', filter: {type: 'combo', searchFieldName: 'revenueType', mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}}},
						{header: 'Schedule Type', width: 100, dataIndex: 'scheduleType.name', filter: {type: 'combo', url: 'billingScheduleTypeListFind.json', searchFieldName: 'scheduleTypeId'}},
						{header: 'Account(s) Label', width: 80, dataIndex: 'accountLabel', filter: false, sortable: false},
						{header: 'Waive Condition', width: 100, dataIndex: 'waiveSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule', searchFieldName: 'waiveSystemConditionId'}},
						{
							header: 'Accrual', width: 60, dataIndex: 'accrualFrequency',
							filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleAccrualFrequencies
								})
							}
						},

						{header: 'Amount', width: 50, dataIndex: 'scheduleAmount', type: 'float', useNull: true},
						{header: 'Secondary Amount', width: 70, dataIndex: 'secondaryScheduleAmount', type: 'float', useNull: true},
						{header: 'Order', width: 40, dataIndex: 'scheduleType.scheduleOrder', type: 'int', filter: {searchFieldName: 'scheduleTypeOrder'}, defaultSortColumn: true, defaultSortDirection: 'asc'},
						{header: 'Start Date', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 40, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
					],

					editor: {
						detailPageClass: 'Clifton.billing.definition.ScheduleTemplateWindow',
						deleteURL: 'billingScheduleDelete.json'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to Active Schedules
							this.setFilterValue('active', true);
						}
					}
				}]
			},


			{
				title: 'Schedule Types',
				items: [{
					name: 'billingScheduleTypeListFind.json',
					xtype: 'gridpanel',
					instructions: 'Billing schedule types define different calculation types that we can bill under. These calculations are applied to billing basis amounts.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Schedule Type Name', width: 110, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Order', width: 30, dataIndex: 'scheduleOrder', type: 'int'},
						{header: 'Flat Fee', width: 30, dataIndex: 'flatFee', type: 'boolean'},
						{header: 'Flat %', width: 30, dataIndex: 'flatFeePercent', type: 'boolean'},
						{header: 'Minimum', width: 30, dataIndex: 'minimumFee', type: 'boolean'},
						{header: 'Maximum', width: 30, dataIndex: 'maximumFee', type: 'boolean'},
						{header: 'Sharing Allowed', width: 45, dataIndex: 'sharedScheduleAllowed', type: 'boolean'},
						{header: 'Tiered', width: 30, dataIndex: 'tiered', type: 'boolean'},
						{header: 'Annual', tooltip: 'Annual Fee is used to apply the schedule to the annual invoice period, and is used primarily for annual minimums/maximums', width: 30, dataIndex: 'annualFee', type: 'boolean'},
						{header: 'Schedule Amount Label', width: 70, dataIndex: 'scheduleAmountLabel'},
						{header: 'Secondary Amount Label', width: 70, dataIndex: 'secondaryScheduleAmountLabel', hidden: true},
						{header: 'Secondary Amount Required', width: 80, dataIndex: 'secondaryScheduleAmountRequired', type: 'boolean', hidden: true},
						{header: 'Accrual Frequency Allowed', width: 80, dataIndex: 'accrualFrequencyAllowed', type: 'boolean'},
						{header: 'Default Revenue Type', width: 60, dataIndex: 'defaultRevenueType'},
						{header: 'Calculator', width: 110, dataIndex: 'billingAmountCalculatorBean.name'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.billing.definition.ScheduleTypeWindow'
					}
				}]
			},


			{
				title: 'Billing Basis Types',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				frame: true,
				items: [
					{
						title: 'Calculation Types',
						name: 'billingBasisCalculationTypeListFind.json',
						xtype: 'gridpanel',
						instructions: 'Billing basis calculation types define how and which dates billing basis values for an account should be calculated for the billing period.',
						topToolbarSearchParameter: 'searchPattern',
						flex: 1,
						height: 250,
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Calculation Type Name', width: 100, dataIndex: 'name'},
							{header: 'Description', width: 300, dataIndex: 'description'},
							{
								header: 'Date Range Specific', width: 60, dataIndex: 'dateRangeSpecific', type: 'boolean',
								tooltip: 'If billing basis can be split into date specific segments, i.e. from x date to y date use one billing basis and from y to z use another.  These can not be used for in advance billing.'
							},
							{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean'},
							{header: 'Calculator', width: 130, dataIndex: 'calculatorBean.name', filter: {searchFieldName: 'calculatorBeanName'}}
						]
					},

					{flex: 0.02},

					{
						title: 'Valuation Types',
						name: 'billingBasisValuationTypeListFind.json',
						xtype: 'gridpanel',
						instructions: 'Billing basis valuation types define how billing basis values can be retrieved/calculated.',
						topToolbarSearchParameter: 'searchPattern',
						flex: 1,
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Valuation Type Name', width: 100, dataIndex: 'name'},
							{header: 'Reporting Label', width: 50, tooltip: 'Header displayed in Invoice PDF reports', dataIndex: 'label'},
							{header: 'Source Table', width: 80, dataIndex: 'sourceTable.name', filter: {searchFieldName: 'sourceTableName'}},
							{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
							{header: 'Investment Group Allowed', width: 50, dataIndex: 'investmentGroupAllowed', type: 'boolean'},
							{header: 'Investment Manager Required', width: 50, dataIndex: 'investmentManagerRequired', type: 'boolean'},
							{
								header: 'Allow Negative Daily Values', width: 50, dataIndex: 'allowNegativeDailyValues', type: 'boolean',
								tooltip: 'In some instances, like Manager Cash where the Manager Cash is a portion of the overall total we need to be able to determine whether or not we are using absolute value of the daily (period average)/periodic totals.  i.e. 2 days of negative values will bring the monthly average down.  Each month\'s average can also be negative, but the final period average is always a positive value.'
							},
							{header: 'Calculator', width: 150, dataIndex: 'calculatorBean.name', filter: {searchFieldName: 'calculatorBeanName'}}
						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ValuationTypeWindow',
							drillDownOnly: true
						}
					}
				]
			},


			{
				title: 'Billing Basis Aggregation Types',
				items: [{
					name: 'billingBasisAggregationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Billing basis aggregation types define how billing basis values can be aggregated for fee sharing. This is primarily useful for cases where there is a tiered fee and clients receive benefit of shared billing basis. The field paths entered are from the billing definition investment account.  When not selected on a schedule the system will group all applicable billing basis into one value.<br><br><b>Note:&nbsp;</b>Shared Schedules can additionally define if the billing basis is shared (grouped invoice) or should also be valued independently for each billing definition.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Aggregation Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Bean Field Path', width: 125, dataIndex: 'aggregationBeanFieldPath'},
						{header: 'Aggregation Label Template', width: 150, dataIndex: 'aggregationLabelTemplate'}
					],
					editor: {
						detailPageClass: 'Clifton.billing.definition.AggregationTypeWindow'
					}
				}]
			},


			{
				title: 'Invoice Types',
				items: [{
					name: 'billingInvoiceTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Billing invoice types define the different invoice types that we have in the system.  Those types that do not use a billing definition are generally entered manually as it could be a one-off reimbursement type invoice.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description'},
						{
							header: 'Amount Precision', width: 55, dataIndex: 'billingAmountPrecision', type: 'int', useNull: true,
							tooltip: 'Used with billing definitions only. Defines how the system rounds billing amounts for the invoices.  i.e. Nearest Dollar = 0, Nearest Penny = 2.  Changing this value will not change existing invoices unless they are re-processed.  If multiple invoices share a schedule with different precision the least precision will be used for that schedule.'
						},
						{header: 'Revenue', width: 40, dataIndex: 'revenue', type: 'boolean'},
						{header: 'Billing Definition Required', width: 70, dataIndex: 'billingDefinitionRequired', type: 'boolean'},
						{header: 'Invoice Posted Default', width: 60, dataIndex: 'invoicePostedToPortalDefault', type: 'boolean', tooltip: 'When creating new billing definitions, used  to default the invoice post to portal option on the billing definition window.'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.billing.invoice.TypeWindow'
					}
				}]
			}
		]
	}]
});

