Clifton.billing.commission.schedule.AssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Sales Commission Assignment',
	width: 900,
	height: 700,
	iconCls: 'sales-commission',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',


				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingCommissionScheduleAssignment',
					finalState: 'Terminated',
					reloadFormPanelAfterTransition: true
				},

				items: [{
					xtype: 'formpanel',
					url: 'billingCommissionScheduleAssignment.json',
					labelFieldName: 'label',
					instructions: 'Sales Commission Assignments link a Commission Schedule to a specific account for a start date and recipient.  End Date on the assignment is system calculated based on the start date, selected schedule, and any hold periods entered.  Terminate date is entered only when terminating the assignment prior to the end date.<br/>When selecting a client account:  1. If start date is blank it will be defaulted to the Account\'s positions on date. 2. If Recipient is blank it will be defaulted to the Client\'s NBD Client Service Representative.<br><b>Note: The only way to add an assignment to an account that already has a commission schedule is to add from an existing account assignment.</b>',
					labelWidth: 120,
					getDataAfterSave: function(data) {
						return TCG.data.getData(this.url + '?id=' + data.id, this);
					},
					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('billingCommissionScheduleAssignment.json?id=' + w.getMainFormId(), this), true);
						w.setModified(w.isModified());
						// to load the formgrid
						this.fireEvent('afterload', this);
					},

					getDefaultData: function(win) {
						const dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						const ct = TCG.data.getData('businessContactTypeByName.json?name=NBD Client Service Representative', this, 'business.contact.type.NBD Client Service Representative');
						if (ct) {
							dd.contactType = ct;
							dd.contactTypeId = ct.id;
						}
						return dd;
					},

					items: [
						{fieldLabel: 'Schedule', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', url: 'billingCommissionScheduleListFind.json', detailPageClass: 'Clifton.billing.commission.schedule.ScheduleWindow'},
						{
							fieldLabel: 'Client Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', displayField: 'label',
							requestedProps: 'inceptionDate|businessClient.company.id',
							qtip: 'Account search includes ONLY those accounts that do not have any existing commission schedule assignments. To add a supplementary schedule to an existing account, you must add from one of the existing assignments for that account and the account will pre-populate for you.',
							listeners: {
								beforequery: function(queryEvent) {
									queryEvent.combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'EQUALS', value: true, field: 'missingBillingCommissionScheduleAssignment'}]));
								},
								select: function(combo, record, index) {
									// update start date from selected account
									const fp = combo.getParentForm();
									const form = fp.getForm();
									const startDateField = form.findField('startDate');
									const positionsOnDate = record.json.inceptionDate;

									if (TCG.isBlank(startDateField.getValue()) && TCG.isNotBlank(positionsOnDate)) {
										startDateField.setValue(TCG.parseDate(positionsOnDate).format('m/d/Y'));
									}

									const companyId = record.json.businessClient.company.id;
									const recipientField = form.findField('recipientContact.label');
									if (TCG.isBlank(recipientField.getValue()) && TCG.isNotBlank(companyId)) {
										const url = 'businessCompanyContactListFind.json?requestedPropertiesRoot=data&requestedProperties=referenceTwo.id|referenceTwo.label&companyId=' + companyId + '&contactTypeName=NBD Client Service Representative';
										let nbdReps = TCG.data.getData(url + '&includeClientOrClientRelationship=false', combo);
										if (nbdReps.length === 0) {
											nbdReps = TCG.data.getData(url + '&includeClientOrClientRelationship=true', combo);
										}
										if (nbdReps.length === 1) {
											const r = nbdReps[0];
											recipientField.setValue({value: r.referenceTwo.id, text: r.referenceTwo.label});
										}
									}
								}
							}
						},
						{
							xtype: 'formtable',
							columns: 4,
							defaults: {
								width: '95%'
							},
							items: [
								{html: 'Start Date:', cellWidth: '125px'},
								{name: 'startDate', xtype: 'datefield', cellWidth: '125px'},
								{html: 'End Date:'},
								{
									name: 'endDate', xtype: 'datefield', readOnly: true, cellWidth: '125px',
									qtip: 'End Date of the commission schedule assignment.  Auto populated for you based on the selected schedule, assignment start date, and any hold periods entered below'
								},
								{html: '', cellWidth: '125px'},
								{html: ''},
								{html: 'Terminate Date:'},
								{
									name: 'terminateDate', xtype: 'datefield',
									qtip: 'Use only when submitting schedule for termination.  Terminate date must be before calculated End Date of the commission schedule assignment.'
								},

								{html: 'Recipient:'},
								{
									name: 'recipientContact.label', hiddenName: 'recipientContact.id', xtype: 'combo'
									, qtip: 'Recipient of the sales commission.  Selections come from employees of our company and can be further limited by contact type selection.'
									, url: 'businessContactListFind.json?ourCompany=true'
									, detailPageClass: 'Clifton.business.contact.ContactWindow'
									, displayField: 'label'
									, width: 250,
									beforequery: function(queryEvent) {
										const cmb = queryEvent.combo;
										const f = TCG.getParentFormPanel(cmb).getForm();
										cmb.store.baseParams = {
											contactTypeId: f.findField('contactType.name').getValue()
										};
									}
								},
								// Used to filter Recipient list
								{html: 'Contact Type:'},
								{
									name: 'contactType.name', hiddenName: 'contactTypeId', xtype: 'combo', submitValue: false
									, qtip: 'Contact Type filter can be used to filter the list of Recipients to those associated with our company and has at least one assignment using the following contact type.'
									, url: 'businessContactTypeListFind.json'
									, width: 250,
									listeners: {
										select: function(combo, record, index) {
											// clear recipient filter
											const fp = combo.getParentForm();
											const form = fp.getForm();
											const recField = form.findField('recipientContact.label');
											recField.clearAndReset();
										}
									}
								}
							]
						},
						{fieldLabel: 'Discount', qtip: 'Amount to subtract from revenue before applying commission schedule.', name: 'commissionDiscount', xtype: 'currencyfield', minValue: 0},
						{fieldLabel: 'Notes', name: 'notes', xtype: 'textarea', height: 50, grow: true},
						{
							xtype: 'formgrid',
							storeRoot: 'periodList',
							height: 150,
							readOnly: true,
							dtoClass: 'com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignmentPeriod',
							doNotSubmitFields: ['holdPeriod', 'periodPercentage'],
							title: 'Assignment Schedule Breakdown',
							columnsConfig: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
								{header: 'PeriodID', width: 15, dataIndex: 'period.id', hidden: true},
								{header: 'Start Date', width: 100, dataIndex: 'startDate', type: 'date', editor: {xtype: 'datefield'}, defaultSortColumn: true},
								{header: 'End Date', width: 100, dataIndex: 'endDate', type: 'date', editor: {xtype: 'datefield'}},
								{header: 'On Hold', width: 100, dataIndex: 'holdPeriod', type: 'boolean'},
								{header: 'Percentage', width: 100, dataIndex: 'periodPercentage', type: 'currency', numberFormat: '0,000.0000 %'}
							],
							addToolbarButtons: function(toolBar, grid) {
								grid.addToolbarAddButton(toolBar, grid);
								toolBar.add('-');

								toolBar.add({
									text: 'Remove',
									tooltip: 'Remove selected item',
									iconCls: 'remove',
									scope: this,
									handler: function() {
										const index = grid.getSelectionModel().getSelectedCell();
										if (index) {
											const store = grid.getStore();
											const rec = store.getAt(index[0]);
											if (TCG.isFalse(rec.json.holdPeriod)) {
												TCG.showError('You cannot remove periods that are not hold periods.  After editing hold periods, all other periods will be automatically recalculated for you.');
											}
											else {
												store.remove(rec);
												grid.markModified();
											}
										}
									}
								});
								toolBar.add('-');
							},
							// Overridden to support editing HOLD periods ONLY
							startEditing: function(row, col) {
								if (TCG.isFalse(this.getStore().getAt(row).get('holdPeriod'))) {
									return false;
								}

								TCG.grid.FormGridPanel.superclass.startEditing.apply(this, arguments);
								// need a way to get to grid from editable fields: combo, etc. to get form arguments
								const ed = this.colModel.getCellEditor(col, row);
								if (ed) {
									ed.containerGrid = this;
								}
							},
							onAfterUpdateFieldValue: function(editor, field) {
								// Any editable rows should be "on hold"
								editor.record.set('holdPeriod', true);
							}
						},
						{
							title: 'File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'BillingCommissionScheduleAssignmentAttachments',
							collapsed: false, // need this to fix layout bug that skips tbar
							isPagingEnabled: function() {
								return false;
							}
						}
					]
				}]
			},


			{
				title: 'Account Assignments',
				xtype: 'gridpanel',
				name: 'billingCommissionScheduleAssignmentListFind',
				instructions: 'The following are all of the Sales Commission Assignments defined for this account',
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
					];
				},
				additionalPropertiesToRequest: 'endDate|terminateDate|referenceOne.description',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false},
					{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
					{header: 'Client', width: 150, hidden: true, dataIndex: 'referenceTwo.businessClient.label', filter: {searchFieldName: 'clientLabel'}},
					{header: 'Account #', width: 75, hidden: true, dataIndex: 'referenceTwo.number', filter: {searchFieldName: 'investmentAccountLabel'}},
					{header: 'Account Name', width: 150, hidden: true, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'investmentAccountLabel', sortFieldName: 'investmentAccountName'}},
					{
						header: 'Schedule', width: 150, dataIndex: 'referenceOne.name', filter: {searchFieldName: 'commissionScheduleName'},
						renderer: function(v, metaData, r) {
							const note = r.json.referenceOne.description;
							if (TCG.isNotBlank(note)) {
								metaData.attr = TCG.renderQtip(note);
							}
							return v;
						}
					},
					{header: 'Notes', width: 150, dataIndex: 'notes'},
					{header: 'Recipient', width: 100, dataIndex: 'recipientContact.label', filter: {searchFieldName: 'recipientContactId', type: 'combo', url: 'businessContactListFind.json?ourCompany=true', displayField: 'label'}},
					{header: 'Start Date', width: 50, dataIndex: 'startDate'},
					{header: 'End Date', width: 50, dataIndex: 'coalesceTerminateEndDate'},
					{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
					{header: 'Discount', width: 50, useNull: true, dataIndex: 'commissionDiscount', type: 'currency'},
					{
						header: 'Workflow State', width: 80, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
						renderer: function(v, metaData, r) {
							const sts = r.data['workflowStatus.name'];
							if (sts === 'Final') {
								metaData.css = 'amountPositive';
							}
							else if (sts === 'Draft') {
								metaData.css = 'amountNegative';
							}
							else if (sts === 'Pending' || sts === 'Inactive') {
								metaData.css = 'amountAdjusted';
							}
							return v;
						}
					},
					{header: 'Workflow Status', width: 100, hidden: true, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}}
				],
				editor: {
					detailPageClass: 'Clifton.billing.commission.schedule.AssignmentWindow',
					getDefaultData: function(gridPanel) {
						const assignment = gridPanel.getWindow().getMainForm().formValues;
						return {referenceTwo: assignment.referenceTwo};
					}
				},
				getLoadParams: function(firstLoad) {
					const lp = {
						investmentAccountId: TCG.getValue('referenceTwo.id', this.getWindow().getMainForm().formValues)
					};
					const t = this.getTopToolbar();
					const bd = TCG.getChildByName(t, 'activeOnDate');
					if (TCG.isNotBlank(bd.getValue())) {
						lp.activeOnDate = (bd.getValue()).format('m/d/Y');
					}
					return lp;
				}
			}
		]
	}]
});
