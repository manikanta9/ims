Clifton.billing.commission.schedule.BulkAssignmentAddWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Bulk Sales Commission Assignments Add',
	width: 900,
	height: 700,
	iconCls: 'sales-commission',

	items: [{
		xtype: 'formpanel',
		url: 'billingCommissionScheduleAssignment.json',
		instructions: 'Sales Commission Assignments link a Commission Schedule to a specific account for a start date and recipient.  Use the following to add the same schedule assignment to a list of accounts.',
		labelWidth: 120,
		getSaveURL: function() {
			return 'billingCommissionScheduleAssignmentBulkSave.json';
		},
		getDefaultData: function(win) {
			const dd = win.defaultData || {};
			if (win.defaultDataIsReal) {
				return dd;
			}
			const ct = TCG.data.getData('businessContactTypeByName.json?name=NBD Client Service Representative', this, 'business.contact.type.NBD Client Service Representative');
			if (ct) {
				dd.contactType = ct;
				dd.contactTypeId = ct.id;
			}
			return dd;
		},

		items: [
			{fieldLabel: 'Schedule', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', url: 'billingCommissionScheduleListFind.json', detailPageClass: 'Clifton.billing.commission.schedule.ScheduleWindow'},
			{
				fieldLabel: 'Main Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', displayField: 'label',
				requestedProps: 'inceptionDate|businessClient.company.id',
				qtip: 'This is the account to include and use for the default start date and recipient.  Additional account assignments can be added below.',
				listeners: {
					beforequery: function(queryEvent) {
						queryEvent.combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'EQUALS', value: true, field: 'missingBillingCommissionScheduleAssignment'}]));
					},
					select: function(combo, record, index) {
						// update start date from selected account
						const fp = combo.getParentForm();
						const form = fp.getForm();
						const startDateField = form.findField('startDate');
						const positionsOnDate = record.json.inceptionDate;

						if (TCG.isBlank(startDateField.getValue()) && TCG.isNotBlank(positionsOnDate)) {
							startDateField.setValue(TCG.parseDate(positionsOnDate).format('m/d/Y'));
						}

						const companyId = record.json.businessClient.company.id;
						const recipientField = form.findField('recipientContact.label');
						if (TCG.isBlank(recipientField.getValue()) && TCG.isNotBlank(companyId)) {
							const url = 'businessCompanyContactListFind.json?requestedPropertiesRoot=data&requestedProperties=referenceTwo.id|referenceTwo.label&companyId=' + companyId + '&contactTypeName=NBD Client Service Representative';
							let nbdReps = TCG.data.getData(url + '&includeClientOrClientRelationship=false', combo);
							if (nbdReps.length === 0) {
								nbdReps = TCG.data.getData(url + '&includeClientOrClientRelationship=true', combo);
							}
							if (nbdReps.length === 1) {
								const r = nbdReps[0];
								recipientField.setValue({value: r.referenceTwo.id, text: r.referenceTwo.label});
							}
						}
					}
				}
			},
			{
				xtype: 'listfield',
				allowBlank: true,
				name: 'additionalClientAccountIds',
				fieldLabel: 'Additional Accounts',
				controlConfig: {
					fieldLabel: 'Client Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', valueField: 'id', displayField: 'label', xtype: 'combo',
					url: 'investmentAccountListFind.json?ourAccount=true', detailIdField: 'referenceTwo.id',
					beforequery: function(queryEvent) {
						queryEvent.combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'EQUALS', value: true, field: 'missingBillingCommissionScheduleAssignment'}]));
					}
				}
			},
			{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
			{
				fieldLabel: 'Recipient',
				name: 'recipientContact.label', hiddenName: 'recipientContact.id', xtype: 'combo'
				, qtip: 'Recipient of the sales commission.  Selections come from employees of our company and can be further limited by contact type selection.'
				, url: 'businessContactListFind.json?ourCompany=true'
				, detailPageClass: 'Clifton.business.contact.ContactWindow'
				, displayField: 'label'
				, width: 250,
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					cmb.store.baseParams = {
						contactTypeId: f.findField('contactType.name').getValue()
					};
				}
			},
			// Used to filter Recipient list
			{
				fieldLabel: 'Contact Type',
				name: 'contactType.name', hiddenName: 'contactTypeId', xtype: 'combo', submitValue: false
				, qtip: 'Contact Type filter can be used to filter the list of Recipients to those associated with our company and has at least one assignment using the following contact type.'
				, url: 'businessContactTypeListFind.json'
				, width: 250,
				listeners: {
					select: function(combo, record, index) {
						// clear recipient filter
						const fp = combo.getParentForm();
						const form = fp.getForm();
						const recField = form.findField('recipientContact.label');
						recField.clearAndReset();
					}
				}
			},
			{fieldLabel: 'Discount', qtip: 'Amount to subtract from revenue before applying commission schedule.', name: 'commissionDiscount', xtype: 'currencyfield', minValue: 0},
			{fieldLabel: 'Notes', name: 'notes', xtype: 'textarea'}
		]
	}]
});
