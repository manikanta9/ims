Clifton.billing.commission.schedule.ScheduleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Commission Schedule',
	width: 850,
	height: 600,
	iconCls: 'sales-commission',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingCommissionSchedule',
					finalState: 'Canceled'
				},
				items: [{
					xtype: 'formpanel',
					url: 'billingCommissionSchedule.json',
					instructions: 'The following commission schedule follows the below periods.  Periods are applied in order and do not overlap.  For example, for the first year apply x%, then the next 2 years, y%.',

					items: [
						{fieldLabel: 'Schedule Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							xtype: 'formgrid',
							storeRoot: 'periodList',
							height: 150,
							dtoClass: 'com.clifton.billing.commission.schedule.BillingCommissionSchedulePeriod',
							columnsConfig: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
								{header: 'Order', width: 100, dataIndex: 'periodOrder', type: 'int', editor: {xtype: 'spinnerfield', allowBlank: false, minValue: 1}},
								{
									header: 'Period Type', width: 200, dataIndex: 'periodType',
									editor: {
										displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
										store: new Ext.data.ArrayStore({
											fields: ['name', 'value', 'description'],
											data: [['DAYS', 'DAYS', 'Apply commission schedule period for a number of days.'],
												['MONTHS', 'MONTHS', 'Apply commission schedule period for a number of months.'],
												['YEARS', 'YEARS', 'Apply commission schedule period for a number of years.']
											]
										})
									}
								},
								{header: 'Period Length', width: 100, dataIndex: 'periodLength', type: 'int', editor: {xtype: 'spinnerfield', allowBlank: false, minValue: 1}},
								{header: 'Percentage', width: 100, dataIndex: 'periodPercentage', type: 'currency', numberFormat: '0,000.0000 %', editor: {xtype: 'floatfield', allowBlank: false}}
							]
						}
					]
				}]
			},


			{
				title: 'Assignments',
				xtype: 'gridpanel',
				name: 'billingCommissionScheduleAssignmentListFind',
				instructions: 'Sales Commission Assignments define the commission schedule to apply to an account',
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
					];
				},
				additionalPropertiesToRequest: 'endDate|terminateDate|referenceOne.description',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false},
					{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
					{header: 'Client', width: 150, hidden: true, dataIndex: 'referenceTwo.businessClient.label', filter: {searchFieldName: 'clientLabel'}},
					{header: 'Account #', width: 75, dataIndex: 'referenceTwo.number', filter: {searchFieldName: 'investmentAccountLabel'}},
					{
						header: 'Account Name', width: 150, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'investmentAccountLabel', sortFieldName: 'investmentAccountName'},
						renderer: function(v, metaData, r) {
							const note = r.data.notes;
							if (TCG.isNotBlank(note)) {
								const qtip = note;
								metaData.css = 'amountAdjusted';
								metaData.attr = TCG.renderQtip(qtip);
							}
							return v;
						}
					},
					{header: 'Schedule', width: 150, dataIndex: 'referenceOne.name', hidden: true, filter: {searchFieldName: 'commissionScheduleName'}},
					{header: 'Notes', width: 200, dataIndex: 'notes', hidden: true},
					{header: 'Recipient', width: 100, dataIndex: 'recipientContact.label', filter: {searchFieldName: 'recipientContactId', type: 'combo', url: 'businessContactListFind.json?ourCompany=true', displayField: 'label'}},
					{header: 'Start Date', width: 50, dataIndex: 'startDate'},
					{header: 'End Date', width: 50, dataIndex: 'coalesceTerminateEndDate'},
					{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
					{header: 'Discount', width: 50, useNull: true, dataIndex: 'commissionDiscount', type: 'currency'},
					{
						header: 'Workflow State', width: 80, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
						renderer: function(v, metaData, r) {
							const sts = r.data['workflowStatus.name'];
							if (sts === 'Final') {
								metaData.css = 'amountPositive';
							}
							else if (sts === 'Draft') {
								metaData.css = 'amountNegative';
							}
							else if (sts === 'Pending' || sts === 'Inactive') {
								metaData.css = 'amountAdjusted';
							}
							return v;
						}
					},
					{header: 'Workflow Status', width: 100, hidden: true, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}}
				],
				editor: {
					detailPageClass: 'Clifton.billing.commission.schedule.AssignmentWindow',
					getDefaultData: function(gridPanel) {
						const fv = gridPanel.getWindow().getMainForm().formValues;
						return {referenceOne: fv};
					}
				},
				getLoadParams: function(firstLoad) {
					const lp = {
						commissionScheduleId: this.getWindow().getMainFormId()
					};
					const t = this.getTopToolbar();
					const bd = TCG.getChildByName(t, 'activeOnDate');

					if (firstLoad) {
						// default to today
						if (TCG.isBlank(bd.getValue())) {
							bd.setValue((new Date()).format('m/d/Y'));
						}
					}

					if (TCG.isNotBlank(bd.getValue())) {
						lp.activeOnDate = (bd.getValue()).format('m/d/Y');
					}
					return lp;
				}
			}
		]
	}]
});
