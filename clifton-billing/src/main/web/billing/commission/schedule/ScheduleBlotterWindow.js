Clifton.billing.commission.schedule.ScheduleBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'billingCommissionBlotterWindow',
	title: 'Sales Commission Setup',
	iconCls: 'sales-commission',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'All Schedules',
				xtype: 'billing-commission-schedule-grid'
			},
			{
				title: 'Draft Schedules',
				workflowStatusName: 'Draft',
				xtype: 'billing-commission-schedule-grid'
			},
			{
				title: 'Pending Approval Schedules',
				workflowStatusName: 'Pending',
				xtype: 'billing-commission-schedule-grid'
			},
			{
				title: 'Active Schedules',
				workflowStatusName: 'Final',
				xtype: 'billing-commission-schedule-grid'
			},
			{
				title: 'Inactive Schedules',
				workflowStatusName: 'Inactive',
				workflowStateName: 'Inactive',
				xtype: 'billing-commission-schedule-grid'
			},
			{
				title: 'Canceled Schedules',
				workflowStatusName: 'Inactive',
				workflowStateName: 'Canceled',
				xtype: 'billing-commission-schedule-grid'
			}
		]
	}]
});
