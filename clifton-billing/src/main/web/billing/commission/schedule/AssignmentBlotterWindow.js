Clifton.billing.commission.schedule.AssignmentBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'billingCommissionScheduleAssignmentBlotterWindow',
	title: 'Sales Commission Schedule Assignments',
	iconCls: 'sales-commission',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'All Assignments',
				xtype: 'billing-commission-schedule-assignment-grid',
				showBulkAddButton: true

			},

			{
				title: 'Draft Assignments',
				xtype: 'billing-commission-schedule-assignment-grid',
				workflowStatusName: 'Draft',
				defaultActiveOnDate: false,
				showBulkAddButton: true,
				rowSelectionModel: 'checkbox',
				transitionWorkflowStateList: [
					{stateName: 'Pending Approval', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approve Selected Commission Schedule Assignment - Moves to Pending Approval'}
				]
			},

			{
				title: 'Pending Approval Assignments',
				xtype: 'billing-commission-schedule-assignment-grid',
				workflowStatusName: 'Pending',
				defaultActiveOnDate: false,
				drillDownOnly: true,
				rowSelectionModel: 'checkbox',
				transitionWorkflowStateList: [
					{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject for Edits', buttonTooltip: 'Reject Selected Billing Commission Schedule Assignment - Return to Draft State'},
					{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Secondary Approval of Selected Commission Schedule Assignment'}
				]
			},

			{
				title: 'Approved Assignments',
				xtype: 'billing-commission-schedule-assignment-grid',
				workflowStatusName: 'Final',
				rowSelectionModel: 'checkbox',
				drillDownOnly: true,
				transitionWorkflowStateList: [
					{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject for Edits', buttonTooltip: 'Reject Selected Billing Commission Schedule Assignment - Return to Draft State'}
				]
			},

			{
				title: 'Pending Termination Assignments',
				xtype: 'billing-commission-schedule-assignment-grid',
				workflowStatusName: 'Inactive',
				defaultActiveOnDate: false,
				rowSelectionModel: 'checkbox',
				drillDownOnly: true,
				transitionWorkflowStateList: [
					{stateName: 'Terminated', iconCls: 'remove', buttonText: 'Terminate', buttonTooltip: 'Terminate Selected Assignment. If you would like to re-activate the assignment, open the record and clear the termination date.'}
				]

			},
			{
				title: 'Terminated Assignments',
				workflowStatusName: 'Cancelled',
				defaultActiveOnDate: false,
				drillDownOnly: true,
				xtype: 'billing-commission-schedule-assignment-grid'
			}
		]
	}]
});
