Clifton.billing.invoice.InvoiceBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'billingInvoiceBlotterWindow',
	title: 'Invoice Blotter',
	iconCls: 'billing',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,

		items: [
			{
				title: 'All Invoices',
				items: [{
					xtype: 'billing-invoiceGrid',
					includeAddButton: true

				}]
			},


			{
				title: 'Invalid Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Invalid" workflow state.',
					workflowStatusName: 'Open',
					defaultDateFilter: false,
					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'run', buttonText: 'Re-Process Invoice', buttonTooltip: 'Re-Process Selected Invoices'}
					]
				}]
			},


			{
				title: 'Pending Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Valid" workflow state.',
					workflowStateName: 'Valid',
					defaultDateFilter: false,
					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'run', buttonText: 'Re-Process Invoice', buttonTooltip: 'Re-Process Selected Invoices'},
						{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve Invoice', buttonTooltip: 'Approve Selected Invoices'}
					]
				}]
			},


			{
				title: 'Billing Admin Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Approved" workflow state.',
					workflowStateName: 'Approved',
					defaultDateFilter: false,
					transitionWorkflowStateList: [
						{stateName: 'Approved by Admin', iconCls: 'row_reconciled', buttonText: 'Final Approval', buttonTooltip: 'Final Approval of Invoices by Billing Admin'},
						{stateName: 'Valid', iconCls: 'cancel', buttonText: 'Reject Invoice', buttonTooltip: 'Reject Selected Invoices'}
					]
				}]
			},


			{
				title: 'Approved Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					workflowStateName: 'Approved by Admin',
					defaultDateFilter: false,
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Approved by Admin" workflow state.',
					transitionWorkflowStateList: [
						{stateName: 'Sent', iconCls: 'www', buttonText: 'Post/Mark Sent', buttonTooltip: 'Post Invoice (if applies) or Mark Invoice as Sent'},
						{stateName: 'Paid', iconCls: 'cash', buttonText: 'Mark Sent & Paid', buttonTooltip: 'Moves invoice to Paid (Note: Must fulfill the payment check that it is fully paid)'},
						{stateName: 'Valid', iconCls: 'cancel', buttonText: 'Reject Invoice', buttonTooltip: 'Reject Selected Invoices'}
					]
				}]
			},


			{
				title: 'Sent Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Sent" workflow state.',
					workflowStateName: 'Sent',
					defaultDateFilter: false,
					showPostButton: true,
					transitionWorkflowStateList: [
						{stateName: 'Void', iconCls: 'cancel', buttonText: 'Void Invoice', buttonTooltip: 'Void Invoices'},
						{stateName: 'Corrected', iconCls: 'undo', buttonText: 'Void and Create Corrected Invoice', buttonTooltip: 'Void and Create Corrected Invoices'}
					]
				}]
			},


			{
				title: 'Partially Paid Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Partially Paid" workflow state.',
					workflowStateName: 'Partially Paid',
					defaultDateFilter: false,
					showPostButton: true,
					columnOverrides: [{dataIndex: 'totalAmount', hidden: false}, {dataIndex: 'unpaidAmount', hidden: false}, {dataIndex: 'revenueGrossTotalAmount', hidden: true}, {dataIndex: 'revenueShareExternalTotalAmount', hidden: true}, {dataIndex: 'revenueNetTotalAmount', hidden: true}, {dataIndex: 'revenueShareInternalTotalAmount', hidden: true}, {dataIndex: 'revenueFinalTotalAmount', hidden: true}],
					transitionWorkflowStateList: [
						{stateName: 'Void', iconCls: 'cancel', buttonText: 'Void Invoice', buttonTooltip: 'Void Invoices'},
						{stateName: 'Void and Create Corrected', iconCls: 'undo', buttonText: 'Void and Create Corrected Invoice', buttonTooltip: 'Void and Create Corrected Invoices'}
					]
				}]
			},


			{
				title: 'Paid Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Paid" workflow state.',
					workflowStateName: 'Paid',
					showPostButton: true,
					transitionWorkflowStateList: [
						{stateName: 'Void', iconCls: 'cancel', buttonText: 'Void Invoice', buttonTooltip: 'Void Invoices'},
						{stateName: 'Void and Create Corrected', iconCls: 'undo', buttonText: 'Void and Create Corrected Invoice', buttonTooltip: 'Void and Create Corrected Invoices'}
					]
				}]
			},


			{
				title: 'Account Breakdown',
				items: [{
					name: 'billingInvoiceDetailAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following lists the invoice details and amounts broken out by account.',
					groupField: 'investmentAccount.label',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Details" : "Detail"]}',
					columns: [
						{header: 'Client Relationship', width: 100, dataIndex: 'investmentAccount.businessClient.clientRelationship.name', hidden: true, filter: {searchFieldName: 'clientRelationshipName'}},
						{header: 'Client', width: 100, dataIndex: 'investmentAccount.businessClient.name', hidden: true, filter: {searchFieldName: 'clientName'}},
						{header: 'Account', width: 100, dataIndex: 'investmentAccount.label', hidden: true, filter: {searchFieldName: 'investmentAccountLabel'}, defaultSortColumn: true},
						{header: 'Invoice #', width: 30, dataIndex: 'invoiceDetail.invoice.id', filter: {searchFieldName: 'invoiceId'}, numberFormat: '0000'},
						{header: 'Invoice Date', width: 50, dataIndex: 'invoiceDetail.invoice.invoiceDate', filter: {searchFieldName: 'invoiceDate'}},
						{header: 'Workflow Status', dataIndex: 'invoiceDetail.invoice.workflowStatus.name', width: 50, filter: {searchFieldName: 'workflowStatusNameContains'}, hidden: true},
						{
							header: 'Workflow State', dataIndex: 'invoiceDetail.invoice.workflowState.name', width: 50, filter: {searchFieldName: 'workflowStateNameContains'},
							renderer: function(v, metaData, r) {
								const sts = r.data['invoiceDetail.invoice.workflowStatus.name'];
								if (sts === 'Open') {
									metaData.css = 'amountNegative';
								}
								else if (sts === 'Closed') {
									metaData.css = 'amountPositive';
								}
								else {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Valuation', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.valuationLabel', filter: false, sortable: false, renderer: TCG.renderValueWithTooltip},
						{
							header: 'Schedule', width: 150, dataIndex: 'invoiceDetail.billingSchedule.name', filter: {searchFieldName: 'scheduleName'},
							renderer: function(v, p, r) {
								if (TCG.isTrue(r.data['invoiceDetail.manual'])) {
									return 'Adjustment';
								}
								return v;
							}
						},
						{header: 'Manual', width: 30, dataIndex: 'invoiceDetail.manual', type: 'boolean', filter: {searchFieldName: 'manualAdjustment'}},
						{header: 'Billing Basis', width: 50, dataIndex: 'billingBasis', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
						{header: 'Allocation', width: 50, dataIndex: 'allocationPercentage', type: 'currency', numberFormat: '0,000.0000 %'},
						{header: 'Revenue (Gross)', width: 45, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}, tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.'},
						{header: 'Broker Fee', width: 45, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, tooltip: 'Broker Fee - revenue that we share (pay out) to an external party'},
						{header: 'Revenue Net', width: 45, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, tooltip: 'Gross Revenue and Broker Fees'},
						{header: 'Revenue Share', width: 45, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.'},
						{header: 'Revenue (Final)', width: 45, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, tooltip: 'Gross Revenue and Broker Fees and Revenue Share'},
						// Shouldn't apply to account detail so leaving hidden
						{header: 'Other', width: 40, dataIndex: 'otherTotalAmount', hidden: true, type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true, filter: {searchFieldName: 'totalAmount'}}
					],
					plugins: {ptype: 'groupsummary'},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.invoiceDetail.invoice.id;
						}
					},

					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 175, xtype: 'toolbar-combo', name: 'billingTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Billing Tags'},
							{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', displayField: 'label', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('invoiceDetail.invoice.invoiceDate', {'after': Clifton.billing.invoice.getDefaultInvoiceStartDate()});
						}
						const params = {excludeWorkflowStatusName: 'Cancelled'};
						const t = this.getTopToolbar();
						let v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
						if (v) {
							params.investmentAccountGroupId = v;
						}
						v = TCG.getChildByName(t, 'investmentAccountId').getValue();
						if (v) {
							params.investmentAccountId = v;
						}
						const tag = TCG.getChildByName(t, 'billingTagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Billing Tags';
							params.categoryHierarchyId = tag.getValue();
							params.categoryTableName = 'BillingDefinition';
							params.categoryLinkFieldPath = 'invoiceDetail.invoice.billingDefinition';
						}
						return params;
					}
				}]
			},


			{
				title: 'Voided Invoices',
				items: [{
					xtype: 'billing-invoiceWorkflowGrid',
					instructions: 'Billing Invoices are bills generated and sent to clients for payment. NOTE: The following list only includes invoices that are in "Canceled" workflow status.',
					workflowStatusName: 'Cancelled',
					initComponent: function() {
						TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
						// Default presentation
						const cm = this.getColumnModel();
						cm.setHidden(cm.findColumnIndex('voidReason'), false);
					}
				}]
			},


			{
				title: 'Invoice Generation',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 180,
						loadValidation: false, // using the form only to get background color/padding
						height: 230,
						buttonAlign: 'right',
						instructions: 'Use this section to create multiple Invoices at a time. Select criteria below to filter which Billing Definitions to create Invoices for. Invoice Start/End Dates are used to generate invoices for all valid invoice periods for the billing definition.  If an invoice already exists for a billing definition and has not been voided, that billing definition will be skipped.',
						listeners: {
							afterrender: function(fp) {
								const f = this.getForm();
								const sd = f.findField('invoiceStartDate');
								if (TCG.isBlank(sd.getValue())) {
									sd.setValue(Clifton.billing.invoice.getDefaultInvoiceStartDate().format('m/d/Y'));
								}
								const ed = f.findField('invoiceEndDate');
								if (TCG.isBlank(ed.getValue())) {
									ed.setValue(Clifton.billing.invoice.getDefaultInvoiceEndDate().format('m/d/Y'));
								}
							}
						},
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .60,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['companyId', 'billingDefinitionId'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{fieldLabel: 'Company', name: 'companyName', hiddenName: 'companyId', xtype: 'combo', url: 'businessCompanyListFind.json?contractAllowed=true', displayField: 'nameWithType', mutuallyExclusiveFields: ['investmentAccountGroupId', 'billingDefinitionId']},
											{fieldLabel: 'Billing Definition', name: 'definitionLabel', hiddenName: 'billingDefinitionId', xtype: 'combo', url: 'billingDefinitionListFind.json?active=true', displayField: 'label', mutuallyExclusiveFields: ['investmentAccountGroupId', 'companyId']}
										]
									}]
								},
								{columnWidth: .05, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .35,
									items: [{
										xtype: 'formfragment',
										frame: false,
										items: [
											{fieldLabel: 'Start Date', xtype: 'datefield', name: 'invoiceStartDate', allowBlank: false},
											{fieldLabel: 'End Date', xtype: 'datefield', name: 'invoiceEndDate', allowBlank: false}
										]
									}]
								}
							]
						}],

						buttons: [
							{
								text: 'Generate Invoices',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const formPanel = this.findParentByType('formpanel');
									const form = formPanel.getForm();
									const groupId = form.findField('investmentAccountGroupId').getValue();
									const companyId = form.findField('companyId').getValue();
									const definitionId = form.findField('billingDefinitionId').getValue();
									if (TCG.isBlank(groupId) && TCG.isBlank(companyId) && TCG.isBlank(definitionId)) {
										Ext.Msg.confirm('Generate All Invoices for Date Range', 'There is no account group, company, or billing definition selected. Are you sure you would like to generate all missing invoices for the date range?', function(a) {
											if (a === 'yes') {
												formPanel.generateInvoices();
											}
										});
									}
									else {
										formPanel.generateInvoices();
									}
								}
							}
						],

						generateInvoices: function() {
							const formPanel = this;
							formPanel.getForm().submit(Ext.applyIf({
								url: 'billingInvoiceGenerateList.json',
								waitMsg: 'Generating...',
								success: function(form, action) {
									Ext.Msg.alert('Invoice Generation Started', action.result.data, function() {
										const grid = formPanel.ownerCt.items.get(1);
										grid.reloadRepeated(); // uses defaults of every 3s for 30s
									});
								}
							}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
						}
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'BILLING-INVOICE',
						instantRunner: true,
						instructions: 'The following invoices are being generated right now.',
						title: 'Current Invoice Generation',
						flex: 1
					}
				]
			},


			{
				title: 'External Billing Basis',
				items: [{
					name: 'billingBasisExternalListFind',
					xtype: 'gridpanel',
					instructions: 'Some billing definition accounts use external (custodian, etc.) values to determine the billing basis.  Use this screen to view/upload External Billing Basis',
					importTableName: 'BillingBasisExternal',
					importComponentName: 'Clifton.billing.invoice.ExternalUploadWindow',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Invoice Type', width: 75, dataIndex: 'billingDefinitionInvestmentAccount.referenceOne.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
						{header: 'Definition', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.referenceOne.label', filter: {type: 'combo', searchFieldName: 'billingDefinitionId', url: 'billingDefinitionListFind.json', displayField: 'label'}},
						{header: 'Client Account', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Asset Class', width: 100, dataIndex: 'assetClass.name', filter: {type: 'combo', searchFieldName: 'assetClassId', url: 'investmentAssetClassListFind.json'}},
						{header: 'Date', width: 50, dataIndex: 'date'},
						{header: 'Billing Basis', width: 70, dataIndex: 'billingBasisAmount', type: 'currency', numberFormat: '0,000'},
						{header: 'Billing Fx Rate', width: 70, useNull: true, dataIndex: 'billingFxRate', type: 'float', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.billing.invoice.ExternalWindow',
						addEditButtons: function(toolBar, gridPanel) {
							this.addToolbarAddButton(toolBar, gridPanel);
							this.addToolbarDeleteButton(toolBar, gridPanel);
							toolBar.add({
								text: 'Remove Selected',
								tooltip: 'Remove all External Billing Basis Records for selected filter(s)',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									gridPanel.deleteBillingBasisExternalSelected();
								}
							});
							toolBar.add('-');
						}
					},
					deleteBillingBasisExternalSelected: function() {
						const grid = this;
						Ext.Msg.confirm('Remove Selected', 'Are you sure you want to remove ALL external billing basis records for the current filters?  All current data listed will be removed.', function(a) {
							if (a === 'yes') {
								grid.loadURL = 'billingBasisExternalListDelete.json';
								grid.reload();
								// Reset the load URL
								grid.loadURL = 'billingBasisExternalListFind.json';
							}
						});
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of balances
							this.setFilterValue('date', {'after': Clifton.billing.invoice.getDefaultInvoiceStartDate()});
						}
					}
				}]
			}
		]
	}]
});


Clifton.billing.invoice.InvoiceWorkflowGrid = Ext.extend(Clifton.billing.invoice.InvoiceGrid, {
	workflowStateName: undefined,
	workflowStatusName: undefined,

	rowSelectionModel: 'checkbox',
	showWorkflowStateStatus: false,
	showPostButton: false,

	addEditButtons: function(toolbar, gridPanel) {
		if (TCG.isTrue(this.showPostButton)) {
			toolbar.add({
				text: 'Post',
				tooltip: 'Post selected invoice and any Client File Attachments',
				iconCls: 'www',
				scope: gridPanel,
				handler: function() {
					const sm = this.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one invoice to post', 'No Rows Selected');
					}
					else {
						const ids = [];
						const ut = sm.getSelections();

						for (let i = 0; i < ut.length; i++) {
							ids.push(ut[i].json.id);
						}
						gridPanel.postInvoiceList(ids, gridPanel);
					}
				}
			});
			toolbar.add('-');
		}
	},

	postInvoiceList: function(ids, gridPanel) {
		const postCount = 0;
		gridPanel.postInvoice(ids, postCount, gridPanel);

	},

	postInvoice: function(ids, postCount, gridPanel) {
		const loader = new TCG.data.JsonLoader({
			waitTarget: this.grid,
			waitMsg: 'Posting...',
			params: {invoiceId: ids[postCount], updatePortalEntityStatus: true},
			onLoad: function(record, conf) {
				postCount++;
				if (postCount === ids.length) { // refresh after all rows were posted
					gridPanel.reload();
				}
				else {
					gridPanel.postInvoice(ids, postCount, gridPanel);
				}
			}
		});
		loader.load('billingInvoicePost.json');
	},

	editor: {
		ptype: 'workflowAwareEntity-grideditor',
		detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
		addEnabled: false,
		tableName: 'BillingInvoice',

		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected invoice',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const editor = this;
					editor.getGridPanel().deleteInvoice(editor);
				}
			});
			toolBar.add('-');
		},

		getRowIdToTransition: function(tableName, rows, currentIndex) {
			const invoice = rows[currentIndex].json;
			if (invoice['invoiceGroup']) {
				// Skip Grouped Invoices - handled separately
				return undefined;
			}
			return invoice.id;
		},

		// Overrides to support Invoices/Invoice Groups
		transitionList: function(invoices, stateName, gridEditor, gridPanel) {
			Ext.Msg.confirm('Transition Selected Invoice(s)', 'Would you like to transition selected invoice(s)? NOTE: Invoices that are part of a group will transition the entire invoice group, not just the individual invoice in the group.', function(a) {
				if (a === 'yes') {
					const invoiceGroupIds = [];
					let invoiceIdForWorkflow = null;
					let invoiceGroupIdForWorkflow = null;
					for (let i = 0; i < invoices.length; i++) {
						if (invoices[i].json.invoiceGroup) {
							const grpId = invoices[i].json.invoiceGroup.id;
							if (invoiceGroupIds.indexOf(grpId) < 0) {
								invoiceGroupIds.push(grpId);
							}
							if (!invoiceGroupIdForWorkflow) {
								invoiceGroupIdForWorkflow = grpId;
							}
						}
						else {
							if (!invoiceIdForWorkflow) {
								invoiceIdForWorkflow = invoices[i].json.id;
							}
						}
					}

					// get next state(s) to transition to - depends on if it's grouped or not
					let invoiceTransition = null;
					if (TCG.isNotNull(invoiceIdForWorkflow)) {
						invoiceTransition = gridEditor.findWorkflowTransition('BillingInvoice', invoiceIdForWorkflow, stateName);
					}
					let groupTransition = null;
					if (TCG.isNotNull(invoiceGroupIdForWorkflow)) {
						groupTransition = gridEditor.findWorkflowTransition('BillingInvoiceGroup', invoiceGroupIdForWorkflow, stateName);
					}
					if (invoiceGroupIds.length === 0) {
						// No groups - just transition the invoices
						gridEditor.transitionRow(invoices, 0, invoiceTransition, gridPanel, 'BillingInvoice');
					}
					else {
						if (TCG.isNull(groupTransition)) {
							TCG.showError('Cannot find [' + stateName + '] next workflow state for grouped invoices', 'Illegal Workflow State');
						}
						// After groups are processed - calls invoice transitions, then when that is done - reloads
						gridEditor.transitionInvoiceGroup(invoiceGroupIds, 0, groupTransition, stateName, gridPanel, invoices, invoiceTransition);
					}
				}
			});
		},

		transitionInvoiceGroup: function(invoiceGroupIds, transitionCount, transition, stateName, gridPanel, invoices, invoiceTransition) {
			const gridEditor = this;
			const invoiceGroupId = invoiceGroupIds[transitionCount];
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Executing Transition',
				waitTarget: gridPanel,
				params: {
					tableName: 'BillingInvoiceGroup',
					id: invoiceGroupId,
					workflowTransitionId: transition.id
				},
				onLoad: function(record, conf) {
					transitionCount++;
					if (transitionCount === invoiceGroupIds.length) { // move on to individual invoices after all groups are processed
						gridEditor.transitionRow(invoices, 0, invoiceTransition, gridPanel, 'BillingInvoice');
					}
					else {
						gridEditor.transitionInvoiceGroup(invoiceGroupIds, transitionCount, transition, stateName, gridPanel, invoices, invoiceTransition);
					}
				}
			});
			loader.load('workflowTransitionExecuteByTransition.json');
		}
	}
});
Ext.reg('billing-invoiceWorkflowGrid', Clifton.billing.invoice.InvoiceWorkflowGrid);

