Clifton.billing.invoice.BillingInvoiceReportExportWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Export Billing Invoice Report',
	iconCls: 'report',
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 250,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		buttonAlign: 'right',
		labelWidth: 200,
		instructions: 'Export Selected Report for selected Invoice. Available reports are only those that accept Invoice ID and Include Payments as parameters. (Template tag = Billing Invoices)',
		buttons: [
			{
				text: 'Export Report',
				iconCls: 'pdf',
				xtype: 'splitbutton',
				handler: function() {
					TCG.getParentFormPanel(this).executeReport('PDF');
				},
				menu: {
					items: [
						{
							text: 'Excel',
							iconCls: 'excel_open_xml',
							tooltip: 'Generate the excel version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('EXCEL_OPEN_XML');
							}
						},
						{
							text: 'Excel (97-2003)',
							iconCls: 'excel',
							tooltip: 'Generate the excel version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('EXCEL');
							}
						},
						{
							text: 'Word',
							iconCls: 'word',
							tooltip: 'Generate the word version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('WORD');
							}
						}
					]
				}
			}
		],
		items: [
			{xtype: 'hidden', name: 'invoiceId'},
			{fieldLabel: 'Invoice', name: 'invoiceLabel', xtype: 'displayfield'},
			{fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json?categoryName=Report Template Tags&categoryTableName=ReportTemplate&categoryLinkFieldPath=reportTemplate&categoryHierarchyName=Billing Invoices', displayField: 'label', allowBlank: false},
			{fieldLabel: '', boxLabel: 'Include Payments on Invoice (Not Supported by All Reports)', name: 'includePayments', xtype: 'checkbox'}
		],

		executeReport: function(format) {
			const data = this.getForm().getValues();
			data['parameterMap[InvoiceID]'] = data['invoiceId'];
			data['parameterMap[IncludePreviousPayments]'] = this.getForm().findField('includePayments').checked;
			data['exportFormat'] = format;
			data['throwErrorOnBadReport'] = false;
			data['cacheIgnored'] = false; // Custom reports - don't use cache ever
			data['reportFileName'] = this.getForm().findField('invoiceLabel').getValue() + ' - ' + this.getForm().findField('reportId').getRawValue();
			TCG.downloadFile('reportFileDownload.json', data, this);
		}
	}]
});
