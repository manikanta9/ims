Clifton.billing.invoice.ExternalUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'billingBasisExternalUploadWindow',
	title: 'External Billing Basis Import',
	iconCls: 'import',
	height: 600,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for External Billing Basis Upload',
		handler: function() {
			TCG.openFile('billing/invoice/ExternalBillingBasisUploadFileSample.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'External Billing Basis Uploads allow you to import billing basis from files directly into the system.',

		items: [
			{fieldLabel: 'Table', name: 'tableName', xtype: 'hidden', value: 'BillingBasisExternal'},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'Billing Definition Investment Account'},
			{xtype: 'label', html: 'Optional selection of billing definition account.  If not selected, \'BillingDefinitionInvestmentAccount-ReferenceTwo-Number\' and \'BillingDefinitionInvestmentAccount-BillingBasisValuationTypeDescription\' are required in the upload file in order to look up the BillingDefinitionInvestment account for the row.'},
			{
				fieldLabel: '', name: 'billingDefinitionInvestmentAccount.accountLabel', hiddenName: 'billingDefinitionInvestmentAccountId', xtype: 'combo', displayField: 'accountLabel', url: 'billingDefinitionInvestmentAccountListFind.json?externalOnly=true',
				listeners: {
					beforequery: function(queryEvent) {
						if (queryEvent.combo.getParentForm().getForm().findField('includeInactiveDefinitionAccounts').checked === false) {
							queryEvent.combo.store.baseParams = {active: true};
						}
					}
				}
			},
			{
				boxLabel: 'Leave unchecked to search for Active Billing Definition Accounts only.', xtype: 'checkbox', name: 'includeInactiveDefinitionAccounts', submitValue: 'false',
				listeners: {
					check: function(f) {
						// On changes reset definition account drop down
						const p = TCG.getParentFormPanel(f);
						const bc = p.getForm().findField('billingDefinitionInvestmentAccount.accountLabel');
						bc.clearValue();
						bc.store.removeAll();
						bc.lastQuery = null;
					}
				}
			},
			{xtype: 'sectionheaderfield', header: 'If uploaded row already exists in the system'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '',
				items: [
					{fieldLabel: '', boxLabel: 'Skip upload for rows that already exist.', name: 'existingBeans', inputValue: 'SKIP', checked: true},
					{fieldLabel: '', boxLabel: 'Overwrite existing data with uploaded information. (Columns not included in the file will NOT overwrite existing data in the database)', name: 'existingBeans', inputValue: 'UPDATE'},
					{fieldLabel: '', boxLabel: 'Insert all data to improve upload performance (Fails if a constraint is violated).', name: 'existingBeans', inputValue: 'INSERT'}
				]

			},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],
		getSaveURL: function() {
			return 'billingBasisExternalUploadFileUpload.json';
		}

	}]
});
