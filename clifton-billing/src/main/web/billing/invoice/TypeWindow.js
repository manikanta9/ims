Clifton.billing.invoice.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Invoice Type',
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingInvoiceType.json',
		instructions: 'Billing invoice types define the different invoice types that we have in the system.  Those types that do not use a billing definition are generally entered manually as it could be a one-off reimbursement type invoice.',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'System Defined', xtype: 'checkbox', name: 'systemDefined', disabled: true, qtip: 'System Defined invoice types cannot be added or deleted from UI, and their names cannot change.'},
			{boxLabel: 'Revenue', xtype: 'checkbox', name: 'revenue', disabled: true, qtip: 'Invoices under this type are used to generate revenue. Non-revenue type invoices are generally fees we pay on behalf of the client and then look for reimbursement (i.e. legal fees or vendor fees).'},
			{boxLabel: 'Billing Definition Required', xtype: 'checkbox', name: 'billingDefinitionRequired', disabled: true},
			{boxLabel: 'Default "Invoice Post To Portal" on Billing Definitions for this Invoice Type"', xtype: 'checkbox', name: 'invoicePostedToPortalDefault', requiredFields: ['billingDefinitionRequired'], qtip: 'If checked, when creating new billing definitions of this invoice type the screen will by default check the post to portal option.'},
			{
				fieldLabel: 'Amount Precision', name: 'billingAmountPrecision', xtype: 'spinnerfield', minValue: 0, maxValue: 2, requiredFields: ['billingDefinitionRequired'], allowBlank: false,
				qtip: 'Used with billing definitions only. Defines how the system rounds billing amounts for the invoices.  i.e. Nearest Dollar = 0, Nearest Penny = 2.  Changing this value will not change existing invoices unless they are re-processed.  If multiple invoices share a schedule with different precision the least precision will be used for that schedule.'
			}
		]
	}]
});
