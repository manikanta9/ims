Clifton.billing.invoice.BillingDefinitionInvoiceDetailWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'billingInvoiceDetail.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.billing.invoice.ManualInvoiceDetailWindow';
		if (entity && !TCG.isTrue(entity.manual)) {
			className = 'Clifton.billing.invoice.ScheduleInvoiceDetailWindow';
		}
		return className;
	}
});


Clifton.billing.invoice.ScheduleInvoiceDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Invoice Detail',
	width: 860,
	height: 510,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingInvoiceDetail.json',
		readOnly: true,
		items: [
			{xtype: 'hidden', name: 'billingSchedule.sharedSchedule', submitValue: false},
			{
				fieldLabel: 'Invoice', xtype: 'linkfield', name: 'invoice.companyInvoiceNumberAndAmountLabel', detailIdField: 'invoice.id',
				detailPageClass: 'Clifton.billing.invoice.BillingDefinitionInvoiceWindow'
			},
			{
				fieldLabel: 'Revenue Type', name: 'coalesceRevenueType', hiddenName: 'coalesceRevenueType', xtype: 'combo', mode: 'local', readOnly: true,
				store: {
					xtype: 'arraystore',
					data: Clifton.billing.invoice.BillingInvoiceRevenueTypes
				}
			},
			{
				fieldLabel: 'Schedule', xtype: 'linkfield', name: 'billingSchedule.name', detailIdField: 'billingSchedule.id',
				getDetailPageClass: function(fp) {
					if (TCG.isTrue(fp.getForm().findField('billingSchedule.sharedSchedule').getValue())) {
						return 'Clifton.billing.definition.ScheduleSharedWindow';
					}
					return 'Clifton.billing.definition.ScheduleWindow';
				}
			},
			{fieldLabel: 'Note', xtype: 'textarea', name: 'note'},
			{fieldLabel: 'Billing Basis', xtype: 'currencyfield', name: 'billingBasis'},
			{fieldLabel: 'Detail Total', xtype: 'currencyfield', name: 'billingAmount'},
			{
				xtype: 'formgrid',
				storeRoot: 'detailAccountList',
				readOnly: true,
				height: 500,
				dtoClass: 'com.clifton.billing.invoice.BillingInvoiceDetailAccount',
				columnsConfig: [
					{header: 'Account', width: 225, dataIndex: 'investmentAccount.label'},
					{header: 'Valuation', width: 250, dataIndex: 'billingDefinitionInvestmentAccount.valuationLabel'},
					{header: 'Billing Basis', width: 100, dataIndex: 'billingBasis', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
					{header: 'Allocation', width: 75, dataIndex: 'allocationPercentage', type: 'currency', numberFormat: '0,000.00 %', summaryType: 'sum', useNull: true},
					{header: 'Amount', width: 100, dataIndex: 'billingAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}]
});


Clifton.billing.invoice.ManualInvoiceDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manual Invoice Detail',
	width: 860,
	height: 510,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingInvoiceDetail.json',

		listeners: {
			afterload: function(panel) {
				// If the invoice is not still in Open or Pending status it's considered read only
				const wfStatus = this.getFormValue('invoice.workflowStatus.name');
				if (wfStatus !== 'Open' && wfStatus !== 'Pending') {
					this.setReadOnly(true);
				}
				// When creating we need to mark the window as modified - so if no id is available set forceModified = true
				// This is because when using defaulted data we need the buttons to trigger for saving
				this.getWindow().forceModified = !TCG.isNumber(this.getWindow().getMainFormId());
			}
		},

		items: [
			{
				fieldLabel: 'Invoice', xtype: 'linkfield', name: 'invoice.companyInvoiceNumberAndAmountLabel', detailIdField: 'invoice.id',
				detailPageClass: 'Clifton.billing.invoice.BillingDefinitionInvoiceWindow'
			},
			{fieldLabel: 'Note', xtype: 'textarea', name: 'note'},
			{
				fieldLabel: 'Detail Total', xtype: 'currencyfield', name: 'billingAmount', readOnly: true,
				qtip: 'Detail Total will automatically be set for you based on the account details entered below.  Note: Although account details support pennies, the total for the detail line may not based on the invoice type.'
			},
			{
				fieldLabel: 'Revenue Type', name: 'revenueType', hiddenName: 'revenueType', xtype: 'combo', mode: 'local', allowBlank: false,
				store: {
					xtype: 'arraystore',
					data: Clifton.billing.invoice.BillingInvoiceRevenueTypes
				}
			},
			{
				fieldLabel: 'Override Precision', name: 'forceAllowPrecision', type: 'int', xtype: 'spinnerfield', readOnly: true,
				qtip: 'When offsetting account unpaid balances, we support entering penny level detail even if the invoice itself does not support it. This is only set by using the "Add Adjustment to Match Paid Total" option on the invoice window'
			},
			{
				xtype: 'formgrid',
				name: 'detailAccountListGrid',
				storeRoot: 'detailAccountList',
				height: 500,
				alwaysSubmitFields: ['investmentAccount.id', 'billingAmount'],
				dtoClass: 'com.clifton.billing.invoice.BillingInvoiceDetailAccount',
				addToolbarButtons: function(toolBar) {
					toolBar.add('->');
					toolBar.add({xtype: 'checkbox', submitValue: false, name: 'includeAllAccounts', fieldLabel: '', boxLabel: 'Leave unchecked to search invoice client\'s accounts only'});
				},
				listeners: {
					'grandtotalupdated': function(v, cf) {
						// NOTE: USE FORM PANEL setFormCalculatedValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
						if (cf.dataIndex === 'billingAmount') {
							this.setFormCalculatedValue('billingAmount', v);
						}
					}
				},
				setFormCalculatedValue: function(fieldName, v) {
					const f = TCG.getParentFormPanel(this);
					f.setFormValue(fieldName, Ext.util.Format.number(v, '0,000.00'), true);
				},
				columnsConfig: [
					{
						header: 'Account', width: 450, dataIndex: 'investmentAccount.label', idDataIndex: 'investmentAccount.id',
						editor: {
							xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label',
							beforequery: function(queryEvent) {
								const grid = queryEvent.combo.gridEditor.containerGrid;
								const f = TCG.getParentFormPanel(grid).getForm();
								const fld = TCG.getChildByName(grid.getTopToolbar(), 'includeAllAccounts');
								if (fld.checked === false) {
									queryEvent.combo.store.baseParams = {clientOrClientRelationshipCompanyId: TCG.getValue('invoice.businessCompany.id', f.formValues)};
								}
								else {
									queryEvent.combo.store.baseParams = {};
								}
							}
						}
					},
					{header: 'Amount', width: 100, dataIndex: 'billingAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', editor: {xtype: 'currencyfield'}}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}]
});
