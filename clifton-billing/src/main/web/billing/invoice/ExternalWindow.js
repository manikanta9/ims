Clifton.billing.invoice.ExternalWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'External Billing Basis',
	iconCls: 'billing',
	width: 800,

	items: [{
		xtype: 'formpanel',
		url: 'billingBasisExternal.json',
		instructions: 'Some billing definition accounts use external (custodian, etc.) values to determine the billing basis.  Use this screen to view/upload External Billing Basis',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Billing Definition', name: 'billingDefinitionInvestmentAccount.referenceOne.label', hiddenName: 'billingDefinitionInvestmentAccount.referenceOne.id', xtype: 'combo', displayField: 'label', url: 'billingDefinitionListFind.json', submitValue: false, detailPageClass: 'Clifton.billing.definition.DefinitionWindow'},
			{
				fieldLabel: 'Billing Definition Account', name: 'billingDefinitionInvestmentAccount.accountLabel', hiddenName: 'billingDefinitionInvestmentAccount.id', xtype: 'combo', displayField: 'accountLabel', url: 'billingDefinitionInvestmentAccountListFind.json?externalOnly=true',
				requiredFields: ['billingDefinitionInvestmentAccount.referenceOne.id'],
				listeners: {
					beforequery: function(queryEvent) {
						const defId = queryEvent.combo.getParentForm().getForm().findField('billingDefinitionInvestmentAccount.referenceOne.id').value;
						const params = {billingDefinitionId: defId};
						if (queryEvent.combo.getParentForm().getForm().findField('includeInactiveDefinitionAccounts').checked === false) {
							params.active = true;
						}
						queryEvent.combo.store.baseParams = params;
					},
					select: function(combo, record, index) {
						const acctCurr = record.json.referenceTwo.baseCurrency.symbol;
						const billCurr = record.json.referenceOne.billingCurrency.symbol;
						const fp = combo.getParentForm();
						const f = fp.getForm();
						f.findField('billingDefinitionInvestmentAccount.referenceTwo.baseCurrency.symbol').setValue(acctCurr);
						f.findField('billingDefinitionInvestmentAccount.referenceOne.billingCurrency.symbol').setValue(billCurr);
						f.findField('billingFxRateSupported').setValue(acctCurr !== billCurr);
					}
				}
			},
			{
				boxLabel: 'Leave unchecked to search for Active Billing Definition Accounts only.', xtype: 'checkbox', name: 'includeInactiveDefinitionAccounts', submitValue: 'false',
				listeners: {
					check: function(f) {
						// On changes reset definition account drop down
						const p = TCG.getParentFormPanel(f);
						const bc = p.getForm().findField('billingDefinitionInvestmentAccount.accountLabel');
						bc.clearValue();
						bc.store.removeAll();
						bc.lastQuery = null;
					}
				}
			},
			{fieldLabel: 'Asset Class', name: 'assetClass.name', hiddenName: 'assetClass.id', xtype: 'combo', url: 'investmentAssetClassListFind.json'},
			{fieldLabel: 'Date', name: 'date', xtype: 'datefield'},
			{fieldLabel: 'Billing Basis', xtype: 'currencyfield', name: 'billingBasisAmount'},
			// FX Rate Information
			{xtype: 'label', html: '<hr />The billing basis should always be entered in the account\'s base currency.  If the billing definition defines billing in a different currency, the following FX rate can be applied to the billing basis to convert it to the billing currency.  If not set, the system will determine the FX Rate on each day based on the Account or use Goldman\'s rate by default.'},
			{xtype: 'hidden', name: 'billingFxRateSupported'},
			{fieldLabel: 'Account Base Currency', xtype: 'displayfield', name: 'billingDefinitionInvestmentAccount.referenceTwo.baseCurrency.symbol', submitValue: false},
			{fieldLabel: 'Billing Currency', xtype: 'displayfield', name: 'billingDefinitionInvestmentAccount.referenceOne.billingCurrency.symbol', submitValue: false},
			{fieldLabel: 'Billing FX Rate', name: 'billingFxRate', requiredFields: ['billingFxRateSupported']}
		]
	}]
});
