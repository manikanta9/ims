// not the actual window but a window selector:
//   - management fee invoices have a special window
//   - otherwise get the generic editable invoice window

Clifton.billing.invoice.InvoiceWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'billingInvoice.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.billing.invoice.CompanyInvoiceWindow';
		if ((entity && TCG.isTrue(entity.invoiceType.billingDefinitionRequired)) || (!entity && TCG.isTrue(config.defaultData.invoiceType.billingDefinitionRequired))) {
			className = 'Clifton.billing.invoice.BillingDefinitionInvoiceWindow';
		}
		return className;
	}
});
