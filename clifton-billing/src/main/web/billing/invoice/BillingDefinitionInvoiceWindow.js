Clifton.billing.invoice.BillingDefinitionInvoiceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Invoice',
	width: 1100,
	height: 800,
	iconCls: 'billing',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Summary',
				layout: 'border',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingInvoice',
					finalState: 'Void',
					groupedInvoice: false,
					allowLocking: true,
					useLockNotes: true,
					additionalSubmitParameters: {
						requestedMaxDepth: 3,
						requestedPropertiesToExclude: 'detailList' // Loaded separately in below grid
					},
					addAdditionalButtons: function(toolbar) {
						const mainForm = TCG.getParentTabPanel(this).getWindow().getMainForm();
						const emailButton = {
							text: 'Email Invoice',
							tooltip: 'Email recipients for this invoice based on the billing definition.',
							iconCls: 'email',
							disabled: mainForm.formValues.workflowState.name !== 'Approved by Admin',
							handler: function() {
								const companyId = mainForm.formValues.businessCompany.id;
								const definitionId = mainForm.formValues.billingDefinition.id;
								const bodyNoteTypeName = 'Invoice Email Body';
								const subjectNoteTypeName = 'Invoice Email Subject';
								let recipientsList;
								let body;
								let subject;
								TCG.data.getDataPromise('businessCompanyContactEntryListFind.json?companyId=' + companyId + '&contactTypeName=Management&nbspFee&nbspInvoice&active=true', this).then(function(businessCompanyContactEntry) {
									recipientsList = businessCompanyContactEntry.rows.map(function(recip) {
										return recip.referenceTwo.emailAddress + ';';
									});
									return TCG.data.getDataPromise('systemNoteListForEntityFind.json?fkFieldId=' + definitionId + '&entityTableName=BillingDefinition&includeNotesForLinkedEntity=false&includeNotesAsLinkedEntity=false&noteTypeName=' + bodyNoteTypeName + '&activeOnDate=' + new Date(mainForm.formValues.invoiceDate).toLocaleDateString(), this);
								}).then(function(bodyNoteList) {
									if (bodyNoteList !== null && bodyNoteList.length > 0) {
										body = bodyNoteList[0].text;
									}
									return TCG.data.getDataPromise('systemNoteListForEntityFind.json?fkFieldId=' + definitionId + '&entityTableName=BillingDefinition&includeNotesForLinkedEntity=false&includeNotesAsLinkedEntity=false&noteTypeName=' + subjectNoteTypeName + '&activeOnDate=' + new Date(mainForm.formValues.invoiceDate).toLocaleDateString(), this);
								}).then(function(subjectNote) {
									if (subjectNote !== null && subjectNote.length > 0) {
										subject = subjectNote[0].text;
									}
									return TCG.mailto(recipientsList.join(''), null, null, subject, body);
								});
							}
						};
						toolbar.add(emailButton);
					},
					isInFinalState: function(data) {
						const grp = data.invoiceGroup;
						// Don't draw toolbar for grouped invoices
						if (TCG.isNotBlank(grp)) {
							this.groupedInvoice = true;
							return true;
						}
						return (this.finalState === data.workflowState.name);
					},

					getFinalWorkflowMessage: function() {
						if (this.groupedInvoice) {
							return 'NOTE: Workflow transitions available on the Invoice Group Only';
						}
						return 'NOTE: no transitions available from final workflow state';
					},
					reloadFormPanelAfterTransition: true
				},
				items: [{
					xtype: 'formpanel',
					region: 'north',
					height: 325,
					url: 'billingInvoice.json',
					labelFieldName: 'companyInvoiceNumberAndAmountLabel',

					getWarningMessage: function(f) {
						this.loadPostingWarningMessage(f);

						let grp = f.findField('invoiceGroup.id').getValue();
						if (TCG.isNotBlank(grp)) {
							grp = 'Invoice Group ' + grp;
							return [
								{xtype: 'label', html: 'This invoice is part of&nbsp;'},
								{xtype: 'linkfield', style: 'padding: 0', value: grp, detailPageClass: 'Clifton.billing.invoice.InvoiceGroupWindow', detailIdField: 'invoiceGroup.id'},
								{xtype: 'label', html: '.&nbsp;All Workflow and Processing must be done at the group level.'}
							];
						}
						return undefined;
					},

					postWarningMsg: undefined,
					loadPostingWarningMessage: function(f) {
						const fp = this;
						let msg = undefined;
						const wfStatus = fp.getFormValue('workflowStatus.name');
						if (TCG.isEquals('Active', wfStatus) || TCG.isEquals('Closed', wfStatus)) {
							if (TCG.isFalse(fp.getFormValue('billingDefinition.invoicePostedToPortal'))) {
								msg = '<b>Note</b>: The billing definition for this invoice is currently set to <b>NOT</b> post to the portal.';
							}
						}
						if (this.postWarningMsg !== msg) {
							this.postWarningMsg = msg;
							const postWarnId = this.id + '-postWarn';
							const postWarn = Ext.get(postWarnId);
							if (TCG.isNotNull(postWarn)) {
								this.remove(postWarnId);
								Ext.destroy(postWarn);
								this.doLayout();
							}
							if (TCG.isNotNull(msg)) {
								if (!Ext.isArray(msg)) {
									const html = msg;
									msg = [];
									msg[0] = {xtype: 'label', html: html};
								}
								this.insert(0, {xtype: 'container', layout: 'hbox', id: postWarnId, autoEl: 'div', cls: 'warning-msg', items: msg});
								this.doLayout();
							}
						}
					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('billingInvoice.json?id=' + w.getMainFormId(), this), true);
						w.setModified(w.isModified());
						// to load the formgrid
						this.fireEvent('afterload', this);
					},
					executeReport: function(includePayments) {
						const w = this.getWindow();
						const id = w.getMainFormId();
						if (TCG.isNotBlank(id)) {
							let url = 'billingInvoiceReportDownload.json?invoiceId=' + id;
							if (TCG.isTrue(includePayments)) {
								url += '&includePayments=true';
							}
							TCG.downloadFile(url, null, this);
						}
					},

					enableDisableManualDetailButton: function(btn) {
						let enable = false;
						const wfStatus = TCG.getValue('workflowStatus.name', this.getForm().formValues);
						if (wfStatus === 'Open' || wfStatus === 'Pending') {
							enable = true;
						}
						if (TCG.isTrue(enable)) {
							btn.enable();
						}
						else {
							btn.disable();
						}
					},
					items: [
						{fieldLabel: 'Group ID', name: 'invoiceGroup.id', xtype: 'hidden'},
						{fieldLabel: 'Billing Definition', name: 'billingDefinition.label', detailIdField: 'billingDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.billing.definition.DefinitionWindow'},
						{name: 'businessCompany.id', xtype: 'hidden'},
						{name: 'businessCompany.name', xtype: 'hidden'},
						{
							xtype: 'formtable',
							columns: 6,
							items: [
								// Row 1
								{html: 'Correction Of:', cellWidth: '110px'},
								{
									name: 'parent.invoiceNumberAndAmountLabel', hiddenName: 'parent.id', xtype: 'combo', displayField: 'invoiceNumberAndAmountLabel', url: 'billingInvoiceListFind.json', detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
									width: 300,
									beforequery: function(queryEvent) {
										const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
										const defId = form.findField('billingDefinition.id').value;
										const invoiceDate = form.findField('invoiceDate').value;
										queryEvent.combo.store.baseParams = {
											billingDefinitionId: defId,
											invoiceDate: invoiceDate,
											workflowStatusNameEquals: 'Cancelled'
										};
									}
								},
								{html: 'Revenue (Gross):', cellWidth: '100px'},
								{name: 'totalAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true},

								{html: '&nbsp;Revenue (Net):', cellWidth: '100px'},
								{name: 'revenueNetTotalAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true},

								// Row 2
								{html: 'Invoice Date:'},
								{name: 'invoiceDate', xtype: 'datefield', readOnly: true, width: 300},

								{html: 'Broker Fee:'},
								{name: 'revenueShareExternalTotalAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true},

								{html: '&nbsp;Paid Amount:'},
								{name: 'paidAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true},


								// Row 3
								{html: 'Invoice Type:'},
								{name: 'invoiceType.name', xtype: 'linkfield', detailIdField: 'invoiceType.id', detailPageClass: 'Clifton.billing.invoice.TypeWindow'},

								{html: 'Revenue Share:'},
								{name: 'revenueShareInternalTotalAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true},

								{html: '&nbsp;Unpaid Amount:'},
								{name: 'unpaidAmount', xtype: 'currencyfield', decimalPrecision: 2, readOnly: true}
							]
						},

						{
							xtype: 'fieldset',
							collapsed: true,
							title: 'Invoice Period Info',
							instructions: 'Invoice Period is the date range this invoice applies to.  If the billing definition started or ended within the invoice period, the billing period may be different (details are prorated on the number of days). Annual Invoice Period is the year this invoice applies to.',
							items: [
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										flex: 1
									},
									items: [
										{value: 'Start Date'},
										{value: 'End Date'},
										{value: 'Day Count'}
									]
								},
								{
									fieldLabel: 'Invoice Period', xtype: 'compositefield',
									defaults: {
										flex: 1
									},
									items: [
										{name: 'invoicePeriodStartDate', xtype: 'datefield', readOnly: true},
										{name: 'invoicePeriodEndDate', xtype: 'datefield', readOnly: true},
										{name: 'daysInInvoicePeriod', xtype: 'integerfield', readOnly: true}
									]
								},
								{
									fieldLabel: 'Billing Period', xtype: 'compositefield',
									defaults: {
										flex: 1
									},
									items: [
										{name: 'invoiceBillingStartDate', xtype: 'datefield', readOnly: true},
										{name: 'invoiceBillingEndDate', xtype: 'datefield', readOnly: true},
										{name: 'daysInInvoiceBilling', xtype: 'integerfield', readOnly: true}
									]
								},
								{
									fieldLabel: 'Annual Period', xtype: 'compositefield',
									defaults: {
										flex: 1
									},
									items: [
										{name: 'annualInvoicePeriodStartDate', xtype: 'datefield', readOnly: true},
										{name: 'annualInvoicePeriodEndDate', xtype: 'datefield', readOnly: true},
										{xtype: 'label', html: '&nbsp;'}
									]
								}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Corrected Invoices',
							checkboxName: 'corrected',
							name: 'correctedFieldset',
							instructions: 'The following invoices were generated as corrections of this invoice.',

							items: [
								{fieldLabel: 'Void Reason', name: 'voidReason', xtype: 'textarea', height: 50},
								{
									xtype: 'formgrid-scroll',
									storeRoot: 'childrenList',
									readOnly: true,
									visible: false,
									height: 100,
									dtoClass: 'com.clifton.billing.invoice.BillingInvoice',
									columnsConfig: [
										{header: 'Invoice #', width: 75, dataIndex: 'id', numberFormat: '0000'},
										{
											header: 'Violation Status', width: 200, dataIndex: 'violationStatus.name',
											renderer: function(v, p, r) {
												return (Clifton.rule.violation.renderViolationStatus(v));
											}
										},
										{header: 'Workflow State', dataIndex: 'workflowState.name', width: 120},
										{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 120},
										{header: 'Revenue (Gross)', width: 85, dataIndex: 'revenueGrossTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
										{header: 'Broker Fee', width: 85, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
										{header: 'Revenue Share', width: 85, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true}
									],
									listeners: {
										// drill into specific invoice
										'celldblclick': function(grid, rowIndex, cellIndex, evt) {
											const row = grid.store.data.items[rowIndex];
											const sid = row.data['id'];
											const detailPageClass = 'Clifton.billing.invoice.InvoiceWindow';
											TCG.createComponent(detailPageClass, {
												id: TCG.getComponentId(detailPageClass, sid),
												params: {id: sid},
												openerCt: grid
											});
										}
									}
								}]
						},


						{
							title: 'Internal File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'BillingInvoiceAttachments',
							hideDateFields: true,
							instructions: 'The following files are considered to be internal attachments for this invoice.  To add new files, use the add button or you can drag and drop a file into the grid.  To edit document file details double click an existing file.',
							doNotEnableDDOnParentPanel: true
						},
						{
							title: 'Client File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'BillingInvoiceClientAttachments',
							hideDateFields: true,
							instructions: 'The following files are considered to be client ready attachments for this invoice, and <b>WILL BE POSTED TO THE PORTAL</b>.  To add new files, use the add button or you can drag and drop a file into the grid.  To edit document file details double click an existing file.',
							doNotEnableDDOnParentPanel: true,

							addToolbarButtons: function(toolBar, gridPanel) {
								toolBar.add({
									iconCls: 'www',
									text: 'Post',
									tooltip: 'Post selected File to the Portal',
									scope: this,
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a file to post.', 'No Row(s) Selected');
										}
										else if (sm.getCount() !== 1) {
											TCG.showError('Multi-selection is not supported.  Please select one file.', 'NOT SUPPORTED');
										}
										else {
											gridPanel.postDocumentFile(sm.getSelected().id);
										}
									}
								});
								toolBar.add('-');
							},

							postDocumentFile: function(documentFileId) {
								const id = this.getWindow().getMainFormId();
								const loader = new TCG.data.JsonLoader({
									waitTarget: this.getWindow().getMainFormPanel(),
									waitMsg: 'Posting...',
									params: {invoiceId: id, documentFileId: documentFileId}
								});
								loader.load('billingInvoiceAttachmentPost.json');
							}
						}
					],
					buttons: [
						{
							text: 'Export Invoice',
							xtype: 'button',
							iconCls: 'pdf',
							width: 150,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeReport(false);
							}
						},
						{
							text: 'Export Invoice (Incl Payments)',
							xtype: 'button',
							iconCls: 'pdf',
							width: 150,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeReport(true);
							}
						},
						{
							text: 'Export Custom Report',
							xtype: 'button',
							iconCls: 'report',
							width: 150,
							tooltip: 'Export Selected Report for this Invoice. Available reports are only those that accept Invoice ID as parameters. (Template tag = Billing Invoices)',
							handler: function(btn) {
								const f = TCG.getParentFormPanel(this);
								const invoiceId = f.getFormValue('id');
								const invoiceLabel = f.getFormValue('businessCompany.name') + ' Invoice ' + invoiceId;
								const clz = 'Clifton.billing.invoice.BillingInvoiceReportExportWindow';
								const cmpId = TCG.getComponentId(clz, invoiceId);
								TCG.createComponent(clz, {
									id: cmpId,
									defaultData: {
										'invoiceId': invoiceId,
										'invoiceLabel': invoiceLabel
									},
									openerCt: this
								});
							}
						}
					]
				},


					{flex: 0.02},
					{

						name: 'billingInvoiceDetailListFind',
						xtype: 'gridpanel',
						title: 'Invoice Details',
						flex: 0.49,
						region: 'center',
						useBufferView: false,
						getLoadParams: function() {
							return {
								invoiceId: this.getWindow().getMainFormId()
							};
						},

						columns: [
							{header: 'ID', hidden: true, dataIndex: 'id', width: 15},
							{
								header: 'Schedule', width: 125, dataIndex: 'billingScheduleNameWithAccrual', filter: {searchFieldName: 'billingScheduleName'},
								renderer: function(v, p, r) {
									if (TCG.isTrue(r.data.manual)) {
										return 'Adjustment';
									}
									return v;
								}
							},
							{header: 'Accrual Start', width: 60, dataIndex: 'accrualStartDate', hidden: true},
							{header: 'Accrual End', width: 60, dataIndex: 'accrualEndDate', hidden: true},
							{header: 'Manual', width: 50, hidden: true, dataIndex: 'manual', type: 'boolean'},
							{header: 'Shared Schedule', width: 60, hidden: true, dataIndex: 'billingSchedule.sharedSchedule', type: 'boolean', filter: {searchFieldName: 'sharedSchedule'}},
							{header: 'Note', width: 250, dataIndex: 'noteAsHtml', renderer: TCG.renderText, filter: {searchFieldName: 'note'}},
							{header: 'Billing Basis', width: 85, dataIndex: 'billingBasis', type: 'currency', numberFormat: '0,000', useNull: true},
							{header: 'Revenue (Gross)', width: 85, dataIndex: 'revenueGrossTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
							{header: 'Broker Fee', width: 85, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
							{header: 'Revenue Share', width: 85, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true}
						],
						plugins: {ptype: 'gridsummary'},
						editor: {
							detailPageClass: 'Clifton.billing.invoice.BillingDefinitionInvoiceDetailWindow',
							drillDownOnly: true,
							addEditButtons: function(toolBar, grid) {
								const fp = grid.getWindow().getMainFormPanel();
								toolBar.add({
									text: 'Add',
									tooltip: 'Add a manual invoice detail record',
									xtype: 'splitbutton',
									iconCls: 'add',
									scope: grid,
									handler: function() {
										const className = 'Clifton.billing.invoice.BillingDefinitionInvoiceDetailWindow';
										const cmpId = TCG.getComponentId(className);
										TCG.createComponent(className, {
											id: cmpId,
											defaultData: {invoice: fp.getForm().formValues, manual: 'true'},
											defaultDataIsReal: true,
											openerCt: fp,
											defaultIconCls: this.getWindow().iconCls
										});
									},
									menu: {
										items: [{
											text: 'Add Adjustment To Match Paid Total',
											tooltip: 'Opens the manual invoice detail window pre-populated to create an adjustment on the invoice to offset the unpaid balance for each account.',
											scope: grid,
											handler: function() {
												const className = 'Clifton.billing.invoice.BillingDefinitionInvoiceDetailWindow';
												const cmpId = TCG.getComponentId(className);
												const invoice = fp.getForm().formValues;
												TCG.data.getDataPromise('billingPaymentInvestmentAccountAllocationListFind.json?billingInvoiceId=' + invoice.id + '&restrictionList=[{"comparison":"NOT_EQUALS","value":0,"field":"unpaidAmount"}]&requestedPropertiesRoot=data&requestedProperties=investmentAccount.id|investmentAccount.label|unpaidAmount', this)
													.then(function(accountList) {
														if (accountList.length === 0) {
															TCG.showError('There are no unpaid accounts to create offsetting manual entry for.');
														}
														else {
															const detailAccountList = [];
															let counter = -10;
															accountList.forEach(function(account) {
																detailAccountList.push({
																	class: 'com.clifton.billing.invoice.BillingInvoiceDetailAccount',
																	id: counter,
																	investmentAccount: account.investmentAccount,
																	billingAmount: -account.unpaidAmount
																});
																counter = counter - 10;
															});


															TCG.createComponent(className, {
																id: cmpId,
																defaultData: {invoice: invoice, manual: 'true', note: 'Adjustment to Match Payment', forceAllowPrecision: 2, detailAccountList: detailAccountList},
																defaultDataIsReal: true,
																openerCt: fp,
																defaultIconCls: grid.getWindow().iconCls
															});
														}
													});
											}
										}]
									},
									listeners: {
										afterRender: function() {
											const btn = this;
											const fp = grid.getWindow().getMainFormPanel();
											fp.on('afterload', function() {
												fp.enableDisableManualDetailButton(btn);
												grid.reload();
											}, this);
										}
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Remove',
									tooltip: 'Remove selected manual invoice detail record',
									iconCls: 'remove',
									scope: this,
									handler: function() {
										const editor = this;
										const sm = editor.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
										}
										else if (sm.getCount() !== 1) {
											TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
										}
										else {
											if (!TCG.isTrue(sm.getSelected().json.manual)) {
												TCG.showError('Only manual invoice detail records can be manually removed from an invoice.', 'Cannot Remove');
											}
											else {
												Ext.Msg.confirm('Delete Selected Manual Invoice Detail', 'Would you like to delete selected row?', function(a) {
													if (a === 'yes') {
														const loader = new TCG.data.JsonLoader({
															waitTarget: grid.ownerCt,
															waitMsg: 'Deleting...',
															params: {id: sm.getSelected().id},
															onLoad: function(record, conf) {
																fp.reload();
															}
														});
														loader.load('billingInvoiceDetailDelete.json');
													}
												});
											}
										}
									},
									listeners: {
										afterRender: function() {
											const btn = this;
											const fp = grid.getWindow().getMainFormPanel();
											fp.on('afterload', function() {
												fp.enableDisableManualDetailButton(btn);
											}, this);
										}
									}
								});
								toolBar.add('-');
							}
						}
					}
				]
			},
			{
				title: 'Account Details',
				reloadOnTabChange: true,
				items: [{
					name: 'billingInvoiceDetailAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following lists the invoice details and amounts broken out by account.',
					groupField: 'investmentAccount.label',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Details" : "Detail"]}',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Account Search', xtype: 'toolbar-textfield', name: 'investmentAccountLabel', width: 250}
						];
					},
					columns: [
						{header: 'Account', width: 100, dataIndex: 'investmentAccount.label', hidden: true, filter: {searchFieldName: 'investmentAccountLabel'}, defaultSortColumn: true},
						{header: 'Valuation', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.valuationLabel', filter: false, sortable: false, renderer: TCG.renderValueWithTooltip},
						{
							header: 'Schedule', width: 100, dataIndex: 'invoiceDetail.billingScheduleNameWithAccrual', filter: {searchFieldName: 'scheduleName'},
							renderer: function(v, p, r) {
								if (TCG.isTrue(r.data['invoiceDetail.manual'])) {
									return 'Adjustment';
								}
								return v;
							}
						},
						{header: 'Manual', width: 75, hidden: true, dataIndex: 'invoiceDetail.manual', type: 'boolean'},
						{header: 'Billing Basis', width: 50, dataIndex: 'billingBasis', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
						{header: 'Allocation', width: 50, dataIndex: 'allocationPercentage', type: 'currency', numberFormat: '0,000.0000 %'},
						{header: 'Revenue (Gross)', width: 85, dataIndex: 'revenueGrossTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
						{header: 'Broker Fee', width: 85, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
						{header: 'Revenue Share', width: 85, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function() {
						return {
							investmentAccountLabel: TCG.getChildByName(this.getTopToolbar(), 'investmentAccountLabel').getValue(),
							invoiceId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'BillingInvoice'
				}]
			},


			{
				title: 'Recipients',
				items: [{
					xtype: 'business-companyContactEntryGrid-byCompany',
					instructions: 'The following contacts are associated with this company and marked to receive invoices (Contact Type: Management Fee Invoice).',
					getCompany: function() {
						return TCG.getValue('businessCompany', this.getWindow().getMainForm().formValues);
					},
					getLoadParams: function() {
						return {
							companyId: this.getCompany().id,
							contactTypeName: 'Management Fee Invoice'
						};
					}
				}]
			},


			{
				title: 'Payments',
				layout: 'vbox',
				layoutConfig: {
					align: 'stretch'
				},

				items: [
					{
						title: 'Account Payment Status',
						xtype: 'billing-payment-account-allocation-grid',
						border: 1,
						flex: 1,

						instructions: 'The following displays each account billed on this invoice and their current payment status.',

						columnOverrides: [
							{dataIndex: 'billingInvoice.invoiceDate', hidden: true},
							{dataIndex: 'billingInvoice.id', hidden: true},
							{dataIndex: 'billingInvoice.invoiceType.name', hidden: true},
							{dataIndex: 'billingInvoice.businessCompany.name', hidden: true},
							{dataIndex: 'billingInvoice.workflowState.name', hidden: true}
						],

						getLoadParams: function() {
							const w = this.getWindow();
							const invId = w.getMainFormId();
							return {
								billingInvoiceId: invId
							};
						},
						getTopToolbarFilters: function(toolbar) {
							return [];
						}

					},
					{flex: 0.02},

					{
						title: 'Unallocated Payments',
						name: 'billingPaymentListFind',
						xtype: 'billing-paymentGrid',
						instructions: 'The following payments have been received for the client but have not been fully allocated to invoices.',
						showAllocationInfo: true,
						showClientInfo: false,
						getLoadParams: function(firstLoad) {
							return {
								companyId: TCG.getValue('businessCompany.id', this.getWindow().getMainForm().formValues),
								fullyAllocated: false
							};
						},
						border: 1,
						flex: 1,
						getDefaultData: function(gridPanel) {
							return {
								businessCompany: TCG.getValue('businessCompany', gridPanel.getWindow().getMainForm().formValues),
								allocatedAmount: 0,
								paymentAmount: TCG.getValue('totalAmount', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'BillingDefinition',
					showGlobalNoteMenu: true,
					showAttachmentInfo: true,
					showCreateDate: true,
					defaultActiveFilter: false,
					showFilterActiveOnDate: true,
					getDefaultActiveOnDate: function() {
						return TCG.parseDate(TCG.getValue('invoiceDate', this.getWindow().getMainForm().formValues)).format('m/d/Y');
					},
					getEntityId: function() {
						return TCG.getValue('billingDefinition.id', this.getWindow().getMainForm().formValues);
					}
				}]
			},


			{
				title: 'Billing Basis Details',
				items: [{
					name: 'billingBasisSnapshotListFind',
					xtype: 'gridpanel',
					instructions: 'The following are the billing basis details for this invoice.',
					groupField: 'billingDefinitionInvestmentAccount.accountLabel',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Details" : "Detail"]}',
					columns: [
						{header: 'Billing Definition Account', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.accountLabel', hidden: true, filter: false, sortable: false},
						{header: 'Account', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.label', hidden: true, filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Source Table', hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationType.sourceTable.label', filter: {searchFieldName: 'sourceTableId', type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true'}},
						{header: 'FKFieldID', hidden: true, dataIndex: 'sourceFkFieldId'},
						{header: 'Investment Manager', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.investmentManagerAccount.label', hidden: true, filter: {searchFieldName: 'investmentManagerAccountId', type: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label'}},
						{header: 'Investment Group', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.investmentGroup.name', hidden: true, filter: {searchFieldName: 'investmentGroupName'}},
						{header: 'Date', width: 65, dataIndex: 'snapshotDate'},
						{header: 'Valuation', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationType.name', filter: {searchFieldName: 'valuationTypeName'}},
						{header: 'Asset Class', width: 120, dataIndex: 'assetClass.name', filter: {searchFieldName: 'assetClassName'}},
						{header: 'Security', width: 120, dataIndex: 'security.label', useNull: true, filter: {searchFieldName: 'securityLabel'}},
						{header: 'Quantity', width: 65, dataIndex: 'quantity', type: 'currency', useNull: true, numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
						{header: 'Security Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true, useNull: true},
						{header: 'Days To Mature', width: 80, dataIndex: 'daysToMaturity', hidden: true, type: 'int', negativeInRed: true, useNull: true, filter: false, sortable: false},
						{header: 'Price Multiplier', width: 85, dataIndex: 'security.priceMultiplier', type: 'float', useNull: true, filter: false, sortable: false},
						{header: 'Billing Basis (Client Base Currency)', width: 170, dataIndex: 'snapshotValue', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, hidden: true},
						{header: 'FX Rate', width: 100, dataIndex: 'fxRate', type: 'float', hidden: true, useNull: true},
						{header: 'Billing Basis', width: 100, dataIndex: 'snapshotValueInBillingBaseCurrency', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, filter: {searchFieldName: 'snapshotValue'}},
						{
							header: 'Source', width: 60, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationType.sourceTable.detailScreenClass', filter: false, sortable: false,
							renderer: function(clz, args, r) {
								if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.sourceFkFieldId)) {
									return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('billingDefinitionInvestmentAccount.billingBasisValuationType.sourceTable.label', r.json) + ' source record associated with this billing basis detail', 'ShowSource');
								}
								return 'N/A';
							}
						}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function() {
						const lp = {};
						lp.billingInvoiceId = this.getWindow().getMainFormId();

						const panel = this;
						const acct = TCG.getChildByName(panel.getTopToolbar(), 'billingDefinitionInvestmentAccountId').getValue();

						if (TCG.isNotBlank(acct)) {
							lp.billingDefinitionInvestmentAccountId = acct;
						}
						return lp;
					},
					addToolbarButtons: function(t, gridPanel) {
						t.add({
							text: 'Detail Report',
							tooltip: 'Export Details in Report',
							iconCls: 'pdf',
							handler: function() {
								this.findParentByType(Ext.Panel).exportAsReport('PDF');
							}
						});
						t.add('-');
					},
					getTopToolbarFilters: function(t) {
						return [
							{
								fieldLabel: 'Billing Account', xtype: 'combo', name: 'billingDefinitionInvestmentAccountId', width: 200, listWidth: 350, url: 'billingDefinitionInvestmentAccountListFind.json', displayField: 'accountLabel',
								listeners: {
									beforequery: function(queryEvent) {
										const form = TCG.getParentByClass(queryEvent.combo, Ext.Panel).getWindow().getMainForm();
										queryEvent.combo.store.baseParams = {billingDefinitionId: TCG.getValue('billingDefinition.id', form.formValues)};
									},
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							},
							{
								xtype: 'button',
								iconCls: 'excel',
								tooltip: 'Export Selected Account Billing Basis Details into Excel',
								handler: function() {
									this.findParentByType(Ext.Panel).exportAccountDetailToExcel();
								}
							}

						];
					},
					exportAccountDetailToExcel: function() {
						const qName = 'Billing Basis Details: Specific Account and Invoice';

						const panel = this;
						const acct = TCG.getChildByName(panel.getTopToolbar(), 'billingDefinitionInvestmentAccountId').getValue();
						const invoiceDate = TCG.getValue('invoiceDate', panel.getWindow().getMainForm().formValues);
						const params = {
							queryName: qName,
							invoiceDate: TCG.parseDate(invoiceDate).format('m/d/Y'),
							billingDefinitionInvestmentAccountId: acct,
							fileName: qName
						};
						params.outputFormat = 'xlsx';

						TCG.downloadFile('billingInvoiceAccountBillingBasisSystemQueryResult.json', params, panel);
					},
					exportAsReport: function(format) {
						const p = this;
						const invoiceId = p.getWindow().getMainFormId();
						const report = TCG.data.getData('reportByNameAndTemplate.json?reportName=Billing Invoice Snapshot Details&templateName=Billing Report', this);
						if (!report) {
							TCG.showError('Cannot find "Billing Invoice Snapshot Details" report for "Billing Report" template.', 'Report Not Found');
							return;
						}
						let url = TCG.getResponseText('reportUrl.json?id=' + report.id, this);
						if (url !== undefined) {
							url += '&exportFormat=' + format + '&InvoiceID=' + invoiceId;
							TCG.downloadFile(url, null, p);
						}
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const eventName = TCG.getActionColumnEventName(evt);
									const row = grid.store.data.items[rowIndex];

									if (eventName === 'ShowSource') {
										const clz = row.json.billingDefinitionInvestmentAccount.billingBasisValuationType.sourceTable.detailScreenClass;
										const id = row.json.sourceFkFieldId;

										const gridPanel = grid.ownerGridPanel;
										TCG.createComponent(clz, {
											id: TCG.getComponentId(clz, id),
											params: {id: id},
											openerCt: gridPanel
										});

									}
								}
							}
						}
					}
				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'BillingInvoice'
					}

				]
			}
		]
	}
	]
})
;
