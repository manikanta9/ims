Clifton.billing.invoice.InvoiceGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Invoice Group',
	width: 1200,
	height: 600,
	iconCls: 'billing',
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Summary',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingInvoiceGroup',
					finalState: 'Void',
					reloadFormPanelAfterTransition: true,
					timeout: 120000, // Invoice Groups have a lot more processing depending on the number of invoices and therefore allow for a long timeout
					additionalSubmitParameters: {
						requestedProperties: 'id|billingDefinition.businessCompany.id|billingDefinition.businessCompany.name|billingDefinition.note|violationStatus.name|workflowStatus.name|workflowState.name|revenueGrossTotalAmount|revenueShareExternalTotalAmount|revenueNetTotalAmount|revenueShareInternalTotalAmount|revenueFinalTotalAmount|paidAmount|unpaidAmount|voidReason',
						requestedPropertiesRoot: 'data.invoiceList'
					}
				},
				items: [{
					xtype: 'formpanel',
					url: 'billingInvoiceGroup.json',
					labelFieldName: 'id',
					readOnly: true,

					listRequestedProperties: 'id|billingDefinition.businessCompany.id|billingDefinition.businessCompany.name|billingDefinition.note|violationStatus.name|workflowStatus.name|workflowState.name|revenueGrossTotalAmount|revenueShareExternalTotalAmount|revenueNetTotalAmount|revenueShareInternalTotalAmount|revenueFinalTotalAmount|paidAmount|unpaidAmount|voidReason',
					listRequestedPropertiesRoot: 'data.invoiceList',

					items: [
						{
							fieldLabel: 'Invoice Date', xtype: 'compositefield',
							defaults: {
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'invoiceDate', xtype: 'datefield'},
								{xtype: 'label'},
								{xtype: 'label', html: 'Warning Status:'},
								{name: 'violationStatus.name', xtype: 'displayfield'}
							]
						},
						{
							xtype: 'formgrid-scroll',
							storeRoot: 'invoiceList',
							readOnly: true,
							height: 400,
							dtoClass: 'com.clifton.billing.invoice.BillingInvoice',
							columnsConfig: [
								{header: 'Invoice #', width: 75, dataIndex: 'id', numberFormat: '0000'},
								{
									header: '', width: 15, dataIndex: 'locked', type: 'boolean',
									renderer: function(v, metaData, r) {
										return Clifton.system.renderSystemLockIconWithTooltip(v, 'invoice', r.data['lockedByUser.displayName'], r.data['lockNote']);
									}
								},
								{header: 'Locked By', width: 50, hidden: true, dataIndex: 'lockedByUser.displayName', filter: {searchFieldName: 'lockedByUserId', type: 'combo', url: 'securityUserListFind.json', displayField: 'label'}},
								{header: 'Lock Note', width: 200, hidden: true, dataIndex: 'lockNote'},
								{
									header: 'Company', width: 250, dataIndex: 'billingDefinition.businessCompany.name', filter: {type: 'combo', searchFieldName: 'companyId', url: 'businessCompanyListFind.json'},
									renderer: function(v, metaData, r) {
										const note = r.data['billingDefinition.note'];
										if (TCG.isNotBlank(note)) {
											const qtip = r.data['billingDefinition.note'];
											metaData.css = 'amountAdjusted';
											metaData.attr = TCG.renderQtip(qtip);
										}
										return v;
									}
								},
								{header: 'Billing Definition Note', hidden: true, width: 200, dataIndex: 'billingDefinition.note', filter: false, sortable: false},
								{
									header: 'Violation Status', width: 150, dataIndex: 'violationStatus.name',
									renderer: function(v, p, r) {
										return (Clifton.rule.violation.renderViolationStatus(v));
									}
								},
								{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name'},
								{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', hidden: true},
								{
									header: 'Revenue (Gross)', width: 75, dataIndex: 'revenueGrossTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
									tooltip: 'Gross Revenue - excludes all revenue sharing and broker fees.'
								},
								{
									header: 'Broker Fee', width: 75, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
									tooltip: 'Broker Fee - revenue that we share (pay out) to an external party'
								},
								{
									header: 'Revenue Net', width: 75, dataIndex: 'revenueNetTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
									tooltip: 'Gross Revenue and Broker Fees'
								},
								{
									header: 'Revenue Share', width: 75, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
									tooltip: 'Internally shared revenue - revenue that we pay out - within our company and affiliates.'
								},
								{
									header: 'Revenue (Final)', width: 75, hidden: true, dataIndex: 'revenueFinalTotalAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum', useNull: true,
									tooltip: 'Gross Revenue and Broker Fees and Revenue Share'
								},
								{header: 'Paid Amount', width: 75, dataIndex: 'paidAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
								{header: 'Unpaid Amount', width: 75, dataIndex: 'unpaidAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
								{header: 'Void Reason', width: 200, dataIndex: 'voidReason', hidden: true}
							],
							plugins: {ptype: 'gridsummary'},
							listeners: {
								// drill into specific invoice
								'celldblclick': function(grid, rowIndex, cellIndex, evt) {
									const row = grid.store.data.items[rowIndex];
									const sid = row.data['id'];
									const detailPageClass = 'Clifton.billing.invoice.InvoiceWindow';
									TCG.createComponent(detailPageClass, {
										id: TCG.getComponentId(detailPageClass, sid),
										params: {id: sid},
										openerCt: grid
									});
								}
							}
						}
					]
				}]
			},


			{
				title: 'Account Details',
				items: [{
					name: 'billingInvoiceDetailAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following lists the invoice details and amounts broken out by account for the invoice group.',
					groupField: 'invoiceDetail.billingSchedule.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Details" : "Detail"]}',
					columns: [
						{
							header: 'Schedule', width: 100, dataIndex: 'invoiceDetail.billingSchedule.name', filter: {searchFieldName: 'scheduleName'}, defaultSortColumn: true, hidden: true,
							renderer: function(v, p, r) {
								if (TCG.isTrue(r.data['invoiceDetail.manual'])) {
									return 'Adjustment';
								}
								return v;
							}
						},
						{header: 'Manual', width: 75, hidden: true, dataIndex: 'invoiceDetail.manual', type: 'boolean'},
						{header: 'Invoice #', width: 50, dataIndex: 'invoiceDetail.invoice.id', type: 'int', filter: {searchFieldName: 'invoiceId'}, numberFormat: '0000'},
						{header: 'Account', width: 175, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Valuation', width: 125, dataIndex: 'billingDefinitionInvestmentAccount.valuationLabel', filter: false, sortable: false},
						{header: 'Billing Basis', width: 50, dataIndex: 'billingBasis', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
						{
							header: 'Allocation', width: 50, dataIndex: 'billingBasisPercentage', type: 'currency', numberFormat: '0,000.00 %', summaryType: 'sum', useNull: true,
							tooltip: 'Allocation of billing basis this account/valuation was applied for the schedule'
						},
						{header: 'Revenue (Gross)', width: 50, dataIndex: 'revenueGrossTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
						{header: 'Broker Fee', width: 50, dataIndex: 'revenueShareExternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true},
						{header: 'Revenue Share', width: 50, dataIndex: 'revenueShareInternalTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', useNull: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function() {
						return {
							invoiceGroupId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.invoiceDetail.invoice.id;
						}
					}
				}]
			}
		]
	}]
});
