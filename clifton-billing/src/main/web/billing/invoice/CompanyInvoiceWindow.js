Clifton.billing.invoice.CompanyInvoiceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Invoice',
	width: 850,
	height: 600,
	iconCls: 'billing',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Summary',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingInvoice',
					finalState: 'Void',
					reloadFormPanelAfterTransition: true,
					allowLocking: true
				},
				items: [{
					xtype: 'formpanel',
					url: 'billingInvoice.json',
					labelFieldName: 'companyInvoiceNumberAndAmountLabel',
					labelWidth: 150,
					getWarningMessage: function(f) {
						let grp = f.findField('invoiceGroup.id').getValue();
						if (TCG.isNotBlank(grp)) {
							grp = 'Invoice Group ' + grp;
							this.ownerCt.getTopToolbar().setDisabled(true);
							return [
								{xtype: 'label', html: 'This invoice is part of&nbsp;'},
								{xtype: 'linkfield', style: 'padding: 0', value: grp, detailPageClass: 'Clifton.billing.invoice.InvoiceGroupWindow', detailIdField: 'invoiceGroup.id'},
								{xtype: 'label', html: '.&nbsp;All Workflow and Processing must be done at the group level.'}
							];
						}
						return undefined;
					},
					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('billingInvoice.json?id=' + w.getMainFormId(), this), true);
						w.setModified(w.isModified());
						// to load the formgrid
						this.fireEvent('afterload', this);
					},
					executeReport: function(includePayments) {
						const w = this.getWindow();
						const id = w.getMainFormId();
						if (TCG.isNotBlank(id)) {
							let url = 'billingInvoiceReportDownload.json?invoiceId=' + id;
							if (TCG.isTrue(includePayments)) {
								url += '&includePayments=true';
							}
							TCG.downloadFile(url, null, this);
						}
					},
					items: [
						{fieldLabel: 'Group ID', name: 'invoiceGroup.id', xtype: 'hidden'},
						{fieldLabel: 'Invoice Type', name: 'invoiceType.name', hiddenName: 'invoiceType.id', xtype: 'combo', displayField: 'name', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=false', detailPageClass: 'Clifton.billing.invoice.TypeWindow'},
						{
							fieldLabel: 'Correction Of', name: 'parent.label', hiddenName: 'parent.id', xtype: 'combo', displayField: 'label', url: 'billingInvoiceListFind.json', detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
							beforequery: function(queryEvent) {
								const typeId = queryEvent.combo.getParentForm().getForm().findField('invoiceType.id').value;
								const companyId = queryEvent.combo.getParentForm().getForm().findField('businessCompany.id').value;
								const invoiceDate = queryEvent.combo.getParentForm().getForm().findField('invoiceDate').value;
								queryEvent.combo.store.baseParams = {
									invoiceTypeId: typeId,
									companyId: companyId,
									invoiceDate: invoiceDate,
									workflowStatusNameEquals: 'Cancelled'
								};
							}
						},
						{fieldLabel: 'Company', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
						{fieldLabel: 'Vendor', name: 'vendorBusinessCompany.name', hiddenName: 'vendorBusinessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
						{
							fieldLabel: 'Invoice Date', xtype: 'compositefield',
							items: [
								{name: 'invoiceDate', xtype: 'datefield', flex: 1},
								{xtype: 'label', flex: 1},
								{value: 'Total Amount:', xtype: 'displayfield', width: 120},
								{name: 'totalAmount', xtype: 'currencyfield', flex: 1, decimalPrecision: 2, readOnly: true}
							]
						},
						{
							fieldLabel: 'Invoice Period Start', xtype: 'compositefield',
							items: [
								{name: 'invoicePeriodStartDate', xtype: 'datefield', flex: 1},
								{xtype: 'label', flex: 1},
								{value: 'End Date:', xtype: 'displayfield', width: 120},
								{name: 'invoicePeriodEndDate', xtype: 'datefield', flex: 1}
							]
						},
						{fieldLabel: 'Void Reason', name: 'voidReason', xtype: 'textarea', height: 30},
						{
							xtype: 'fieldset-checkbox',
							title: 'Corrected Invoices',
							checkboxName: 'corrected',
							name: 'correctedFieldset',
							instructions: 'The following invoices were generated as corrections of this invoice.',
							items: [{
								xtype: 'formgrid-scroll',
								storeRoot: 'childrenList',
								readOnly: true,
								visible: false,
								height: 100,
								dtoClass: 'com.clifton.billing.invoice.BillingInvoice',
								columnsConfig: [
									{header: 'Invoice #', width: 75, dataIndex: 'id'},
									{header: 'Workflow State', dataIndex: 'workflowState.name', width: 120},
									{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 120},
									{header: 'Total Amount', width: 120, dataIndex: 'totalAmount', type: 'currency', numberFormat: '0,000.00'}
								],
								listeners: {
									// drill into specific invoice
									'celldblclick': function(grid, rowIndex, cellIndex, evt) {
										const row = grid.store.data.items[rowIndex];
										const sid = row.data['id'];
										const detailPageClass = 'Clifton.billing.invoice.InvoiceWindow';
										TCG.createComponent(detailPageClass, {
											id: TCG.getComponentId(detailPageClass, sid),
											params: {id: sid},
											openerCt: grid
										});
									}
								}
							}]
						},
						{
							xtype: 'formgrid-scroll',
							storeRoot: 'detailList',
							height: 150,
							dtoClass: 'com.clifton.billing.invoice.BillingInvoiceDetail',
							listeners: {
								grandtotalupdated: function(v, cf) {
									// NOTE: USE FORM PANEL setFormCalculatedValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
									if (cf.dataIndex === 'billingAmount') {
										this.setFormCalculatedValue('totalAmount', v);
									}
								}
							},
							setFormCalculatedValue: function(fieldName, v) {
								const f = TCG.getParentFormPanel(this);
								f.setFormValue(fieldName, Ext.util.Format.number(v, '0,000.00'), true);
							},
							columnsConfig: [
								{header: 'Note', width: 350, dataIndex: 'note', editor: {xtype: 'textarea', allowBlank: false}},
								{header: 'Billing Amount', width: 100, dataIndex: 'billingAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000.00', editor: {xtype: 'floatfield', allowBlank: false}}
							],
							plugins: {ptype: 'gridsummary'}
						},
						{xtype: 'label', html: '&nbsp;'},
						{
							title: 'File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'BillingInvoiceAttachments',
							hideDateFields: true
						}
					],
					buttons: [
						{
							text: 'Export Invoice',
							xtype: 'button',
							iconCls: 'pdf',
							width: 120,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeReport(false);
							}
						},
						{
							text: 'Export Invoice (Incl Payments)',
							xtype: 'button',
							iconCls: 'pdf',
							width: 120,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeReport(true);
							}
						}
					]
				}]
			},


			{
				title: 'Payments',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Payment Allocations',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,

						name: 'billingPaymentAllocationListFind',
						instructions: 'The following payments have been allocated to this invoice. Use the unallocated payments section at the bottom to add new payments or to allocate open payments to this invoice.',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Payment #', width: 75, dataIndex: 'billingPayment.id'},
							{header: 'Payment Date', width: 75, dataIndex: 'billingPayment.paymentDate'},
							{header: 'Payment Amount', width: 100, dataIndex: 'billingPayment.paymentAmount', type: 'currency', summaryType: 'sum'},
							{header: 'Allocated Amount', width: 100, dataIndex: 'allocatedAmount', type: 'currency', summaryType: 'sum'},
							{header: 'Unallocated Amount', width: 100, dataIndex: 'billingPayment.availableAmount', type: 'currency', summaryType: 'sum'}
						],
						plugins: {ptype: 'gridsummary'},
						getLoadParams: function() {
							const w = this.getWindow();
							const invId = w.getMainFormId();
							return {
								billingInvoiceId: invId
							};
						},
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.billing.payment.PaymentWindow',
							getDetailPageId: function(gridPanel, row) {
								return row.json.billingPayment.id;
							}
						}
					},

					{flex: 0.02},

					{
						title: 'Unallocated Payments',
						name: 'billingPaymentListFind',
						xtype: 'billing-paymentGrid',
						instructions: 'The following payments have been received for the client but have not been fully allocated to invoices.',
						showAllocationInfo: true,
						showClientInfo: false,
						getLoadParams: function(firstLoad) {
							return {
								companyId: TCG.getValue('businessCompany.id', this.getWindow().getMainForm().formValues),
								fullyAllocated: false
							};
						},
						border: 1,
						flex: 1,
						getDefaultData: function(gridPanel) {
							return {
								businessCompany: TCG.getValue('businessCompany', gridPanel.getWindow().getMainForm().formValues),
								allocatedAmount: 0,
								paymentAmount: TCG.getValue('totalAmount', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				]
			}
		]
	}]
});
