Clifton.billing.invoice.ExternalDeleteWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'External Billing Basis Bulk Delete',
	iconCls: 'remove',
	height: 300,
	width: 800,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Select a billing definition and account, date range, and optional asset class to remove ALL external billing basis records for those selections.',
		labelWidth: 140,
		confirmBeforeSaveMsgTitle: 'Remove External Billing Basis Records',
		confirmBeforeSaveMsg: 'Are you sure you want to remove ALL external billing basis records for the selected account, date range and asset class (optional).',

		items: [
			{
				fieldLabel: 'Billing Definition Account', name: 'billingDefinitionInvestmentAccount.accountLabel', hiddenName: 'billingDefinitionInvestmentAccountId', xtype: 'combo', displayField: 'accountLabel', url: 'billingDefinitionInvestmentAccountListFind.json?externalOnly=true',
				allowBlank: false
			},
			{fieldLabel: 'Asset Class', name: 'assetClass.name', hiddenName: 'assetClassId', xtype: 'combo', url: 'investmentAssetClassListFind.json'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'billingBasisExternalListDelete.json';
		}
	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Removing...',
			success: function(form, action) {
				win.savedSinceOpen = true;
				if (win.closeOnSuccess) {
					win.closeWindow();
				}
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
