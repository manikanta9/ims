Clifton.billing.definition.DefinitionBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'billingDefinitionBlotterWindow',
	title: 'Billing Definition Blotter',
	iconCls: 'billing',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Invoices',
				items: [{
					xtype: 'billing-invoiceGrid',
					includeAddButton: true
				}]
			},


			{
				title: 'All Billing Definitions',
				items: [{
					xtype: 'billing-definitionGrid',
					defaultActiveFilter: false, // EXCLUDING ARCHIVED BY DEFAULT SO DON'T NEED TO DEFAULT TO ACTIVE ONLY
					excludeArchived: 'OPTIONAL_TRUE'
				}]
			},


			{
				title: 'Draft Billing Definitions',
				items: [{
					xtype: 'billing-definitionGrid',
					workflowStatusName: 'Draft',
					defaultActiveFilter: false,
					excludeArchived: 'ALWAYS'
					// No workflow transitions here because each definition approval requires a note (pop up window on individual transition)
				}]
			},


			{
				title: 'Pending Approval Billing Definitions',
				items: [{
					xtype: 'billing-definitionGrid',
					workflowStatusName: 'Pending',
					excludeArchived: 'ALWAYS',
					defaultActiveFilter: false,
					drillDownOnly: true,
					rowSelectionModel: 'checkbox',
					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject for Edits', buttonTooltip: 'Reject Selected Billing Definitions - Return to Draft State'},
						{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Perform secondary/final approval of selected Billing Definitions - Moves to Approved'}
					]
				}]
			},


			{
				title: 'Approved Billing Definitions',
				items: [{
					xtype: 'billing-definitionGrid',
					workflowStatusName: 'Approved',
					excludeArchived: 'ALWAYS',
					drillDownOnly: true,
					rowSelectionModel: 'checkbox',
					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Return for Edits', buttonTooltip: 'Return Selected Billing Definitions to draft for edits'}
					]
				}]
			},


			{
				title: 'Archived Billing Definitions',
				items: [{
					xtype: 'billing-definitionGrid',
					workflowStateName: 'Archived',
					defaultActiveFilter: false,
					excludeArchived: 'NEVER',
					drillDownOnly: true
				}]
			},


			{
				title: 'Billing Definition Accounts',
				items: [{
					name: 'billingDefinitionInvestmentAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Billing Definition Investment Accounts define the account valuation and calculation for Billing Basis for invoices.',
					additionalPropertiesToRequest: 'id|billingSchedule.description',
					columns: [
						{header: 'DefinitionID', width: 35, dataIndex: 'referenceOne.id', type: 'int', hidden: true, filter: {searchFieldName: 'billingDefinitionId'}},
						{header: 'Invoice Type', width: 75, dataIndex: 'referenceOne.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
						{header: 'Client Relationship', width: 200, dataIndex: 'referenceTwo.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{
							header: 'Client', width: 150, hidden: true, dataIndex: 'referenceTwo.businessClient.name', filter: {searchFieldName: 'clientName'},
							renderer: function(v, metaData, r) {
								const note = r.data['referenceOne.note'];
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(note);
								}
								return v;
							}
						},
						{header: 'Definition Note', width: 150, hidden: true, dataIndex: 'referenceOne.note', filter: false, sortable: false},
						{header: 'Client Account', width: 150, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Account Positions On', width: 50, hidden: true, dataIndex: 'referenceTwo.inceptionDate', filter: {searchFieldName: 'investmentAccountInceptionDate'}},
						{header: 'Service', width: 100, dataIndex: 'referenceTwo.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Service Processing Type', width: 100, dataIndex: 'referenceTwo.serviceProcessingType.name', filter: {searchFieldName: 'serviceProcessingTypeName'}, hidden: true},
						{
							header: 'Valuation Type', width: 60, dataIndex: 'billingBasisValuationType.name', filter: {searchFieldName: 'billingBasisValuationTypeName', url: 'billingBasisValuationTypeListFind.json', loadAll: true},
							renderer: function(v, metaData, r) {
								const desc = r.data['billingBasisValuationTypeDescription'];
								if (TCG.isNotBlank(desc)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(desc);
								}
								return v;
							}
						},
						{header: 'Valuation Type Description', width: 80, hidden: true, dataIndex: 'billingBasisValuationTypeDescription', filter: {searchFieldName: 'billingBasisValuationTypeDescription'}},

						{header: 'Calculation Type', width: 60, dataIndex: 'billingBasisCalculationType.name', filter: {searchFieldName: 'billingBasisCalculationTypeName', url: 'billingBasisCalculationTypeListFind.json?inactive=false', loadAll: true}},
						{header: 'Instrument Group', width: 70, dataIndex: 'investmentGroup.name', filter: {type: 'combo', searchFieldName: 'investmentGroupId', url: 'investmentGroupListFind.json'}},
						{header: 'Manager Account', width: 70, hidden: true, dataIndex: 'investmentManagerAccount.label', filter: {type: 'combo', searchFieldName: 'investmentManagerAccountId', url: 'investmentManagerAccountListFind.json', displayField: 'label'}},

						{
							header: 'Schedule Name', width: 70, dataIndex: 'billingSchedule.name', filter: {searchFieldName: 'billingScheduleName'}, tooltip: 'If selected, the billing definition investment account applies ONLY to that specific schedule - otherwise to all schedules.',
							renderer: function(v, metaData, r) {
								const note = r.json.billingSchedule ? r.json.billingSchedule.description : '';
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(note);
								}
								return v;
							}
						},
						{header: 'Start Date', width: 40, dataIndex: 'startDate'},
						{header: 'End Date', width: 40, dataIndex: 'endDate'},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false},
						{header: 'Active Definition', width: 40, dataIndex: 'referenceOne.active', type: 'boolean', hidden: true, filter: {searchFieldName: 'activeDefinition'}, sortable: false}
					],
					editor: {
						detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'billingTagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Billing Tags';
							params.categoryTableName = 'BillingDefinition';
							params.categoryLinkFieldPath = 'referenceOne';
							params.categoryHierarchyId = tag.getValue();
						}

						const excludeArchivedFilter = TCG.getChildByName(this.getTopToolbar(), 'excludeArchived');
						if (excludeArchivedFilter && excludeArchivedFilter.checked) {
							params.excludeBillingDefinitionWorkflowStateName = 'Archived';
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 175, xtype: 'toolbar-combo', name: 'billingTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Billing Tags'},
							'-',
							{fieldLabel: '', boxLabel: 'Exclude Archived Definitions', xtype: 'toolbar-checkbox', name: 'excludeArchived', checked: true}
						];
					}
				}]
			},


			{
				title: 'Definition Dependencies',
				items: [
					{
						xtype: 'gridpanel',
						name: 'billingScheduleBillingDefinitionDependencyListFind',
						instructions: 'Billing Schedule Billing Definition Dependencies are used to define for a specific schedule the dependency on invoices from another billing definition - optionally limited to a specific account.  This means that the schedule relies on actual billed amounts from the pre-requisite invoices.  i.e. Minimum Fee with a Credit for fees paid on another invoice.',
						additionalPropertiesToRequest: 'billingSchedule.description',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Prerequisite Invoice Type', width: 100, hidden: true, dataIndex: 'billingDefinition.invoiceType.name', filter: {searchFieldName: 'prerequisiteInvoiceTypeId', type: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Prerequisite Definition', width: 150, dataIndex: 'billingDefinition.label', filter: {searchFieldName: 'prerequisiteBillingDefinitionLabel'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Prerequisite Billing Account', width: 150, dataIndex: 'accountLabel', filter: {searchFieldName: 'prerequisiteAccountLabel'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Prerequisite Definition Account', width: 100, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.label', filter: {searchFieldName: 'prerequisiteBillingDefinitionInvestmentAccountId', type: 'combo', url: 'billingDefinitionInvestmentAccountListFind.json', displayField: 'label'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Prerequisite Account', width: 100, hidden: true, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'prerequisiteInvestmentAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Dependent Invoice Type', width: 75, dataIndex: 'billingSchedule.billingDefinition.invoiceType.name', filter: {searchFieldName: 'dependentInvoiceTypeId', type: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Dependent Definition', width: 150, dataIndex: 'billingSchedule.billingDefinition.label', filter: {searchFieldName: 'dependentBillingDefinitionLabel'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Dependent Schedule Type', width: 75, hidden: true, dataIndex: 'billingSchedule.scheduleType.name', filter: {searchFieldName: 'billingScheduleTypeId', type: 'combo', url: 'billingScheduleTypeListFind.json'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Dependent Schedule', width: 75, dataIndex: 'billingSchedule.label', filter: {searchFieldName: 'billingScheduleLabel'}, renderer: TCG.renderValueWithTooltip},
							{header: 'Note', width: 100, hidden: true, dataIndex: 'note', renderer: TCG.renderValueWithTooltip}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.billing.definition.ScheduleDefinitionDependencyWindow'
						},
						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: '', boxLabel: 'Include Active Schedules Only', width: 130, xtype: 'toolbar-checkbox', name: 'active', checked: true},
								'-',
								{fieldLabel: '', boxLabel: 'Exclude Archived Definitions', xtype: 'toolbar-checkbox', name: 'excludeArchived', checked: true, qtip: 'Excludes where either billing definition (pre-req or dependent is in workflow state Archived'}
							];
						},
						getLoadParams: function(firstLoad) {
							const params = {};
							const active = TCG.getChildByName(this.getTopToolbar(), 'active');
							if (active && TCG.isTrue(active.getValue())) {
								params.active = true;
							}
							const excludeArchivedFilter = TCG.getChildByName(this.getTopToolbar(), 'excludeArchived');
							if (excludeArchivedFilter && excludeArchivedFilter.checked) {
								params.excludeBillingDefinitionWorkflowStateName = 'Archived';
							}
							return params;
						}
					}
				]
			},


			{
				title: 'External Billing Basis',
				items: [{
					name: 'billingBasisExternalListFind',
					xtype: 'gridpanel',
					instructions: 'Some billing definition accounts use external (custodian, etc.) values to determine the billing basis.  Use this screen to view/upload External Billing Basis',
					importTableName: 'BillingBasisExternal',
					importComponentName: 'Clifton.billing.invoice.ExternalUploadWindow',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Invoice Type', width: 75, dataIndex: 'billingDefinitionInvestmentAccount.referenceOne.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
						{header: 'Client Relationship', width: 200, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{
							header: 'Client', width: 200, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.businessClient.name', filter: {searchFieldName: 'clientName'},
							renderer: function(v, metaData, r) {
								const note = r.data['billingDefinitionInvestmentAccount.referenceOne.note'];
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(note);
								}
								return v;
							}
						},
						{header: 'Definition Note', width: 200, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.referenceOne.note', filter: false, sortable: false},
						{header: 'Client Account', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Description', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationTypeDescription', filter: {searchFieldName: 'billingBasisValuationTypeDescription'}},
						{header: 'Asset Class', width: 100, dataIndex: 'assetClass.name', filter: {type: 'combo', searchFieldName: 'assetClassId', url: 'investmentAssetClassListFind.json'}},
						{header: 'Date', width: 50, dataIndex: 'date'},
						{header: 'Billing Basis', width: 70, dataIndex: 'billingBasisAmount', type: 'currency', numberFormat: '0,000'},
						{header: 'Billing Fx Rate', width: 70, useNull: true, dataIndex: 'billingFxRate', type: 'float', hidden: true}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientAccount', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.billing.invoice.ExternalWindow',
						addEditButtons: function(toolBar, gridPanel) {
							this.addToolbarAddButton(toolBar, gridPanel);
							this.addToolbarDeleteButton(toolBar, gridPanel);
							toolBar.add({
								text: 'Bulk Remove',
								tooltip: 'Remove all External Billing Basis Records for a specific account, date range, and optional asset class',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									TCG.createComponent('Clifton.billing.invoice.ExternalDeleteWindow', {
										openerCt: gridPanel
									});
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of balances
							this.setFilterValue('date', {'after': Clifton.billing.invoice.getDefaultInvoiceStartDate()});
						}
					}
				}]
			},


			{
				title: 'Shared Schedules',
				items: [{
					xtype: 'gridpanel',
					name: 'billingScheduleListFind',
					instructions: 'The following schedules are set up for sharing across multiple billing definitions. Billing schedules can be shared across multiple billing definitions in order to get better tiered pricing. When including active definitions, then shared schedules assigned to at least one active definition will be included.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Schedule Name', width: 150, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								const note = r.data['description'];
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(note);
								}
								return v;
							}
						},
						{header: 'Schedule Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Revenue Type', width: 50, dataIndex: 'revenueTypeLabel', filter: {type: 'combo', searchFieldName: 'revenueType', mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}}},
						{header: 'Schedule Type', width: 85, dataIndex: 'scheduleType.name', filter: {type: 'combo', url: 'billingScheduleTypeListFind.json', searchFieldName: 'scheduleTypeId'}},
						{header: 'Aggregation Type', width: 75, dataIndex: 'billingBasisAggregationType.name', filter: {type: 'combo', url: 'billingBasisAggregationTypeListFind.json', searchFieldName: 'billingBasisAggregationTypeId'}},
						{header: 'Do Not Group Invoice', width: 60, dataIndex: 'doNotGroupInvoice', type: 'boolean'},
						{header: 'Waive Condition', width: 100, dataIndex: 'waiveSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule', searchFieldName: 'waiveSystemConditionId'}},
						{
							header: 'Frequency', width: 60, dataIndex: 'scheduleFrequency', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleFrequencies
								})
							}
						},
						{
							header: 'Accrual', width: 60, dataIndex: 'accrualFrequency', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleAccrualFrequencies
								})
							}
						},
						{header: 'Amount', width: 60, dataIndex: 'scheduleAmount', type: 'float', useNull: true},
						{header: 'Secondary Amount', width: 60, dataIndex: 'secondaryScheduleAmount', type: 'float', useNull: true, hidden: true},
						{header: 'Order', width: 60, dataIndex: 'scheduleType.scheduleOrder', type: 'int', filter: {searchFieldName: 'scheduleTypeOrder'}, defaultSortColumn: true, defaultSortDirection: 'asc', hidden: true},
						{header: 'Start Date', width: 40, dataIndex: 'startDate'},
						{header: 'End Date', width: 40, dataIndex: 'endDate'},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
					],
					getTopToolbarFilters: function(toolbar) {
						return [{fieldLabel: '', boxLabel: 'Include Active Definitions Only', width: 130, xtype: 'toolbar-checkbox', name: 'activeDefinition', checked: true}];
					},
					editor: {
						detailPageClass: 'Clifton.billing.definition.ScheduleSharedWindow'
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to Active Schedules
							this.setFilterValue('active', true);
						}
						const params = {sharedSchedule: true};
						const activeDefinition = TCG.getChildByName(this.getTopToolbar(), 'activeDefinition');
						if (activeDefinition && TCG.isTrue(activeDefinition.getValue())) {
							params.activeDefinition = true;
						}
						return params;
					}
				}]
			},


			{
				title: 'Definition Schedules',
				items: [{
					xtype: 'gridpanel',
					name: 'billingScheduleListFind',
					instructions: 'The following schedules are not shared and tied directly to a specific billing definition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Invoice Type', width: 75, dataIndex: 'billingDefinition.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
						{
							header: 'Company', width: 200, dataIndex: 'billingDefinition.businessCompany.name', filter: {searchFieldName: 'companyName'},
							renderer: function(v, metaData, r) {
								const note = r.data['billingDefinition.note'];
								if (TCG.isNotBlank(note)) {
									const qtip = note;
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(qtip);
								}
								return v;
							}
						},
						{header: 'Definition Note', width: 200, hidden: true, dataIndex: 'billingDefinition.note', filter: false, sortable: false},
						{
							header: 'Schedule Name', width: 150, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								const note = r.data['description'];
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(note);
								}
								return v;
							}
						},
						{header: 'Schedule Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Revenue Type', width: 65, dataIndex: 'revenueTypeLabel', filter: {type: 'combo', searchFieldName: 'revenueType', mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}}},
						{header: 'Schedule Type', width: 100, dataIndex: 'scheduleType.name', filter: {type: 'combo', url: 'billingScheduleTypeListFind.json', searchFieldName: 'scheduleTypeId'}},
						{header: 'Aggregation Type', width: 75, dataIndex: 'billingBasisAggregationType.name', filter: {type: 'combo', url: 'billingBasisAggregationTypeListFind.json', searchFieldName: 'billingBasisAggregationTypeId'}},
						{header: 'Account(s)', width: 100, dataIndex: 'accountLabel', filter: false, sortable: false},
						{header: 'Waive Condition', width: 100, dataIndex: 'waiveSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule', searchFieldName: 'waiveSystemConditionId'}},
						{
							header: 'Frequency', width: 60, dataIndex: 'scheduleFrequency', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleFrequencies
								})
							}
						},
						{
							header: 'Accrual', width: 60, dataIndex: 'accrualFrequency', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingScheduleAccrualFrequencies
								})
							}
						},
						{header: 'Amount', width: 60, dataIndex: 'scheduleAmount', type: 'float', useNull: true},
						{header: 'Secondary Amount', width: 60, dataIndex: 'secondaryScheduleAmount', type: 'float', useNull: true, hidden: true},
						{header: 'Order', width: 60, dataIndex: 'scheduleType.scheduleOrder', type: 'int', filter: {searchFieldName: 'scheduleTypeOrder'}, defaultSortColumn: true, defaultSortDirection: 'asc', hidden: true},
						{header: 'Start Date', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 40, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
					],
					getTopToolbarFilters: function(toolbar) {
						return [{fieldLabel: '', boxLabel: 'Include Active Definitions Only', width: 130, xtype: 'toolbar-checkbox', name: 'activeDefinition', checked: true}];
					},

					editor: {
						detailPageClass: 'Clifton.billing.definition.ScheduleWindow',
						drillDownOnly: true
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to Active Schedules
							this.setFilterValue('active', true);
						}
						const params = {sharedSchedule: false};
						const activeDefinition = TCG.getChildByName(this.getTopToolbar(), 'activeDefinition');
						if (activeDefinition && TCG.isTrue(activeDefinition.getValue())) {
							params.activeDefinition = true;
						}
						return params;
					}
				}]
			}
		]
	}]
});

