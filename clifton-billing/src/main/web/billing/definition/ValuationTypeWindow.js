Clifton.billing.definition.ValuationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Basis Valuation Type',
	iconCls: 'billing',
	width: 850,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'formpanel',
					url: 'billingBasisValuationType.json',
					instructions: 'Billing basis valuation types define how billing basis values can be retrieved/calculated.',
					labelWidth: 140,
					readOnly: true,
					items: [
						{fieldLabel: 'Valuation Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Reporting Label', name: 'label'},
						{fieldLabel: 'Source Table', name: 'sourceTable.name', detailIdField: 'sourceTable.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.TableWindow'},
						{fieldLabel: '', boxLabel: 'Investment Group Allowed', name: 'investmentGroupAllowed', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Investment Manager Required', name: 'investmentManagerRequired', xtype: 'checkbox'},
						{fieldLabel: '', boxLabel: 'Allow Negative Daily Values', name: 'allowNegativeDailyValues', xtype: 'checkbox', qtip: 'In some instances, like Manager Cash where the Manager Cash is a portion of the overall total we need to be able to determine whether or not we are using absolute value of the daily (period average)/periodic totals.  i.e. 2 days of negative values will bring the monthly average down.  Each month\'s average can also be negative, but the final period average is always a positive value.'},
						{fieldLabel: 'Calculator', xtype: 'linkfield', name: 'calculatorBean.name', detailIdField: 'calculatorBean.id', detailPageClass: 'Clifton.system.bean.BeanWindow'}
					]
				}]
			},


			{
				title: 'Investment Accounts',
				items: [{
					xtype: 'gridpanel',
					name: 'billingDefinitionInvestmentAccountListFind',
					instructions: 'The following accounts use this valuation type',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
						];
					},
					columns: [
						{header: 'DefinitionID', width: 15, dataIndex: 'referenceOne.id', hidden: true},
						{
							header: 'Client', width: 150, hidden: true, dataIndex: 'referenceTwo.businessClient.name', filter: {searchFieldName: 'clientName'},
							renderer: function(v, metaData, r) {
								const note = r.data['referenceOne.note'];
								if (TCG.isNotBlank(note)) {
									const qtip = note;
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(qtip);
								}
								return v;
							}
						},
						{header: 'Definition Note', width: 150, hidden: true, dataIndex: 'referenceOne.note', filter: false, sortable: false},
						{header: 'Investment Account', width: 150, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Service', width: 100, dataIndex: 'referenceTwo.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Service Processing Type', width: 100, dataIndex: 'referenceTwo.serviceProcessingType.name', filter: {searchFieldName: 'serviceProcessingTypeName'}, hidden: true},
						{header: 'Calculation Type', width: 60, dataIndex: 'billingBasisCalculationType.name', filter: {searchFieldName: 'billingBasisCalculationTypeName', url: 'billingBasisCalculationTypeListFind.json?inactive=false', loadAll: true}},
						{header: 'Instrument Group', width: 70, dataIndex: 'investmentGroup.name', filter: {type: 'combo', searchFieldName: 'investmentGroupId', url: 'investmentGroupListFind.json'}},
						{header: 'Start Date', width: 40, dataIndex: 'startDate'},
						{header: 'End Date', width: 40, dataIndex: 'endDate'},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false},
						{header: 'Active Definition', width: 40, dataIndex: 'referenceOne.active', type: 'boolean', hidden: true, filter: {searchFieldName: 'activeDefinition'}}
					],
					editor: {
						detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						}
					},
					getLoadParams: function(firstLoad) {
						const lp = {
							billingBasisValuationTypeId: this.getWindow().getMainFormId()
						};
						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'activeOnDate');

						if (firstLoad) {
							// default to today
							if (TCG.isBlank(bd.getValue())) {
								bd.setValue((new Date()).format('m/d/Y'));
							}
						}

						if (TCG.isNotBlank(bd.getValue())) {
							lp.activeOnDate = (bd.getValue()).format('m/d/Y');
						}
						return lp;
					}
				}]
			}
		]
	}]
});
