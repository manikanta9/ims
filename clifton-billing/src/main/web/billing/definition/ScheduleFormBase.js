Clifton.billing.definition.ScheduleFormBase = Ext.extend(TCG.form.FormPanel, {

	url: 'billingSchedule.json',
	labelWidth: 140,

	initComponent: function() {
		const currentItems = [];

		Ext.each(this.beforeScheduleItems, function(f) {
			currentItems.push(f);
		});

		Ext.each(this.baseScheduleItems, function(f) {
			currentItems.push(f);
		});

		Ext.each(this.afterScheduleItems, function(f) {
			currentItems.push(f);
		});
		currentItems.push(this.tiersFieldSet);

		this.items = currentItems;
		Clifton.billing.definition.ScheduleFormBase.superclass.initComponent.call(this);
	},

	baseScheduleItems: [
		{
			fieldLabel: 'Schedule Frequency', name: 'scheduleFrequency', hiddenName: 'scheduleFrequency', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
			store: new Ext.data.ArrayStore({
				fields: ['name', 'value', 'description'],
				data: Clifton.billing.BillingScheduleFrequencies
			})
		},
		{
			fieldLabel: 'Schedule Type', name: 'scheduleType.name', hiddenName: 'scheduleType.id', xtype: 'combo', url: 'billingScheduleTypeListFind.json', detailPageClass: 'Clifton.billing.definition.ScheduleTypeWindow',
			limitRequestedProperties: false,
			listeners: {
				// reset schedule amount label/remove field enable/disable tiers based on selection
				select: function(combo, record, index) {
					combo.ownerCt.resetScheduleTypeFields(record.json);
				}
			}
		},
		{
			fieldLabel: 'Revenue Type', name: 'revenueType', hiddenName: 'revenueType', xtype: 'combo', mode: 'local',
			store: {
				xtype: 'arraystore',
				data: Clifton.billing.invoice.BillingInvoiceRevenueTypes
			}
		},
		{fieldLabel: 'Schedule Amount', xtype: 'floatfield', name: 'scheduleAmount', requiredFields: ['scheduleType.name'], doNotClearIfRequiredChanges: true},
		{fieldLabel: 'Secondary Schedule Amount', xtype: 'floatfield', name: 'secondaryScheduleAmount', requiredFields: ['scheduleType.name'], doNotClearIfRequiredChanges: true},
		{
			fieldLabel: 'Accrual Frequency', name: 'accrualFrequency', hiddenName: 'accrualFrequency', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
			qtip: 'How often fees accrue on the invoice.  If blank then for the entire invoice period. If selected, accrual frequencies must be the same or shorter time periods than the billing frequency.  For example you can have monthly accrual on a quarterly invoice, but you can NOT have quarterly accruals on a monthly invoice.',
			store: new Ext.data.ArrayStore({
				fields: ['name', 'value', 'description'],
				data: Clifton.billing.BillingScheduleAccrualFrequencies
			})
		},
		{
			fieldLabel: 'Aggregation Type', xtype: 'combo', name: 'billingBasisAggregationType.name', hiddenName: 'billingBasisAggregationType.id', url: 'billingBasisAggregationTypeListFind.json',
			emptyText: '< Default >',
			qtip: 'Determines how assets/billing basis for applicable billing definition investment accounts are grouped for the benefit of generating one fee and allocating that fee proportionally to accounts.<br><br>'
				+ 'Leave blank for all application billing basis to be aggregated together (default option)'
		}
	],

	tiersFieldSet: {
		xtype: 'fieldset',
		collapsed: true,
		name: 'tiersFieldset',
		title: 'Billing Tiers',
		instructions: 'The following "Fee %" is charged for the Billing Basis between this and previous Threshold.',
		items: [{
			xtype: 'formgrid',
			storeRoot: 'scheduleTierList',
			dtoClass: 'com.clifton.billing.definition.BillingScheduleTier',
			columnsConfig: [
				{header: 'Threshold', width: 200, useNull: true, dataIndex: 'thresholdAmount', editor: {xtype: 'currencyfield', allowDecimals: false}, type: 'currency', numberFormat: '0,000'},
				{header: 'Fee %', width: 200, dataIndex: 'feePercent', editor: {xtype: 'floatfield'}, type: 'float'}
			]
		}]
	},

	listeners: {
		afterload: function(form, isClosing) {
			this.resetScheduleTypeFields();
			// When creating from a template we need to mark the window as modified - so if no id is available set forceModified = true
			this.getWindow().forceModified = !TCG.isNumber(this.getWindow().getMainFormId());
		}
	},

	resetScheduleTypeFields: function(record) {
		if (!record) {
			record = TCG.getValue('scheduleType', this.getForm().formValues);
		}
		if (record) {
			const f = this.getForm();
			const tiersFieldSet = TCG.getChildByName(this.ownerCt, 'tiersFieldset');
			const accrualFrequencyField = f.findField('accrualFrequency');
			const scheduleFrequencyField = f.findField('scheduleFrequency');
			const scheduleAmountField = f.findField('scheduleAmount');
			const secondaryScheduleAmountField = f.findField('secondaryScheduleAmount');
			const revenueTypeField = f.findField('revenueType');
			if (TCG.isTrue(record.tiered)) {
				// Enable Tiers FormGrid
				tiersFieldSet.expand();
				tiersFieldSet.setDisabled(false);
				tiersFieldSet.setVisible(true);
				// Disable Schedule Amount Field
				this.setFieldLabel('scheduleAmount', 'Schedule Amount:');
				scheduleAmountField.setValue('');
				this.hideField('scheduleAmount');
			}
			else {
				// Disable Tiers FormGrid
				tiersFieldSet.collapse();
				tiersFieldSet.setDisabled(true);
				tiersFieldSet.setVisible(false);
				// Enable Schedule Amount Field and Reset Label
				this.setFieldLabel('scheduleAmount', record.scheduleAmountLabel + ':');
				this.showField('scheduleAmount');
			}


			if (TCG.isNotBlank(record.secondaryScheduleAmountLabel)) {
				this.setFieldLabel('secondaryScheduleAmount', record.secondaryScheduleAmountLabel + ':');
				this.showField('secondaryScheduleAmount');
				secondaryScheduleAmountField.allowBlank = !TCG.isTrue(record.secondaryScheduleAmountRequired);
			}
			else {
				this.setFieldLabel('secondaryScheduleAmount', 'Secondary Schedule Amount:');
				this.hideField('secondaryScheduleAmount');
				secondaryScheduleAmountField.allowBlank = true;
			}


			if (TCG.isTrue(record.accrualFrequencyAllowed)) {
				this.showField('accrualFrequency');
			}
			else {
				this.hideField('accrualFrequency');
				accrualFrequencyField.setValue('');
			}

			if (TCG.isTrue(record.flatFeePercent)) {
				this.hideField('scheduleFrequency');
				scheduleFrequencyField.setValue('');
			}
			else {
				this.showField('scheduleFrequency');
			}

			if (TCG.isNotBlank(record.defaultRevenueType) && TCG.isBlank(revenueTypeField.getValue())) {
				this.setFormValue('revenueType', record.defaultRevenueType);
			}
		}
	}
});
Ext.reg('billing-schedule-form', Clifton.billing.definition.ScheduleFormBase);
