Clifton.billing.definition.ScheduleSharedCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Copy Shared Schedule',
	iconCls: 'billing',
	height: 500,
	width: 900,
	modal: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Create a new shared schedule and optionally end existing schedule on Start Date - 1.  Select which billing definitions you would like to remain linked to the new shared schedule.',
		getDefaultData: function(window) {
			const defaultData = window.defaultData;
			if (defaultData && defaultData.copyFromId) {
				return Promise.resolve(TCG.data.getDataPromise('billingScheduleSharingListForSchedule.json?scheduleId=' + defaultData.copyFromId + '&requestedPropertiesRoot=data&requestedProperties=referenceTwo.id|referenceTwo.label', this)
					.then(function(records) {
						if (records && records.length > 0) {
							const definitions = [];
							records.forEach(rec => (definitions.push({id: rec.referenceTwo.id, label: rec.referenceTwo.label})));
							defaultData.billingDefinitionIds = definitions;
						}
						return defaultData;
					}));
			}
			return defaultData;
		},

		afterRenderPromise: function(formPanel) {
			// Default data will define the billing definitions, the grid needs to see that data
			const billingDefinitionRows = this.getFormValue('billingDefinitionIds');
			const billingDefinitionDetailListGrid = TCG.getChildByName(formPanel, 'billingDefinitionListGrid');
			if (Array.isArray(billingDefinitionRows) && billingDefinitionRows.length > 0 && billingDefinitionDetailListGrid.getStore().getTotalCount() !== billingDefinitionRows.length) {
				billingDefinitionDetailListGrid.getStore().loadData(this.getForm().formValues);
				// apply changes for submit
				billingDefinitionDetailListGrid.markModified();
			}
		},

		items: [
			{name: 'copyFromId', xtype: 'hidden'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: '', boxLabel: 'End existing schedule on Start Date - 1', name: 'endExisting', xtype: 'checkbox'},
			{
				xtype: 'formgrid',
				storeRoot: 'billingDefinitionIds',
				collapsible: false,
				name: 'billingDefinitionListGrid',
				dtoClass: 'com.clifton.billing.definition.BillingDefinition',
				doNotSubmitFields: ['includeInCopy', 'label'],
				frame: false,
				readOnly: true,
				columnsConfig: [
					{
						width: 30, dataIndex: 'includeInCopy', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
						menuDisabled: true,
						header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', '-on'),
						renderer: function(v, p, record) {
							p.css += ' x-grid3-check-col-td';
							if (TCG.isBlank(v)) {
								record.data['includeInCopy'] = true;
								v = true;
							}
							return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
						}
					},
					{header: 'Definition ID', dataIndex: 'id', width: 50, hidden: true},
					{header: 'Billing Definition', dataIndex: 'label', width: 575}
				],
				markModified: function(initOnly) {
					const result = [];
					this.store.each(function(record) {
						if (TCG.isTrue(record.data['includeInCopy'])) {
							result.push(record.id);
						}
					}, this);
					const value = Ext.util.JSON.encode(result);
					this.submitField.setValue(value); // changed value
					if (initOnly) {
						this.submitField.originalValue = value; // initial value
					}
					else {
						this.getWindow().setModified(true);
					}
				}
			}
		],
		getSaveURL: function() {
			return 'billingScheduleSharedCopy.json';
		},
		listeners: {
			// After Creating - open the newly created Shared Schedule Window
			aftercreate: function(panel, closeOnSuccess) {
				console.log(panel.getIdFieldValue());
				const config = {
					params: {id: panel.getForm().idFieldValue},
					openerCt: panel.ownerCt.openerCt
				};
				TCG.createComponent('Clifton.billing.definition.ScheduleSharedWindow', config);
			}
		}
	}]
});
