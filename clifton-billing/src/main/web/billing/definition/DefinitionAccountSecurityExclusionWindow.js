Clifton.billing.definition.DefinitionAccountSecurityExclusionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Account Security Exclusion',
	width: 850,
	height: 600,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingDefinitionInvestmentAccountSecurityExclusion.json',
		instructions: 'Billing Account Security Exclusions allow excluding a specific security from billing account valuations on specific date ranges with optional condition. Security Exclusions are supported for any account that uses a billing basis valuation type that supports instrument groups. Security is not required, but if not selected an exclude condition is required.',
		labelFieldName: 'id',


		getWarningMessage: function(f) {
			const wfStatus = TCG.getValue('billingDefinitionInvestmentAccount.referenceOne.workflowStatus.name', f.formValues);
			if (wfStatus === 'Approved') {
				return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Return for Edits</i>.';
			}
			if (wfStatus === 'Pending') {
				return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Reject</i>, or <i>Approve</i> existing changes and then <i>Return for Edits</i>';
			}

			return undefined;
		},

		listeners: {
			afterload: function(panel) {
				if (this.getFormValue('billingDefinitionInvestmentAccount.referenceOne.workflowStatus.name') !== 'Draft') {
					this.setReadOnlyFieldsForApprovedStatus(true);
				}
				else {
					this.setReadOnlyFieldsForApprovedStatus(false);
				}
			}
		},

		// Note: Should stay in sync with "Approved Billing Account Security Exclusion Modify Condition"
		approvedEditableFields: ['exclusionNote'],
		setReadOnlyFieldsForApprovedStatus: function(readOnly) {
			this.setReadOnly(readOnly);
			if (TCG.isTrue(readOnly)) {
				for (let i = 0; i < this.approvedEditableFields.length; i++) {
					const fieldName = this.approvedEditableFields[i];
					this.getForm().findField(fieldName).setReadOnly(false);
				}
			}
		},

		items: [
			{fieldLabel: 'Billing Definition', name: 'billingDefinitionInvestmentAccount.referenceOne.label', detailIdField: 'billingDefinitionInvestmentAccount.referenceOne.id', xtype: 'linkfield', detailPageClass: 'Clifton.billing.definition.DefinitionWindow', submitValue: false},
			{xtype: 'hidden', name: 'billingDefinitionInvestmentAccount.investmentGroup.id', submitValue: false},
			{
				fieldLabel: 'Billing Account', name: 'billingDefinitionInvestmentAccount.accountLabelLong', hiddenName: 'billingDefinitionInvestmentAccount.id', xtype: 'combo', displayField: 'accountLabelLong', url: 'billingDefinitionInvestmentAccountListFind.json', detailPageClass: 'Clifton.billing.definition.DefinitionAccountWindow',
				requestedProps: 'investmentGroup.id',
				listeners: {
					select: function(combo, record, index) {
						const form = combo.getParentForm();
						const investmentGroupId = TCG.getValue('investmentGroup.id', record.json);
						form.setFormValue('billingDefinitionInvestmentAccount.investmentGroup.id', investmentGroupId);
					}
				},
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					const definitionId = TCG.getValue('billingDefinitionInvestmentAccount.referenceOne.id', f.formValues);
					if (TCG.isBlank(definitionId)) {
						TCG.showError('Billing Definition is missing.');
						return;
					}
					queryEvent.combo.store.setBaseParam('billingDefinitionId', definitionId);
					queryEvent.combo.store.setBaseParam('billingBasisValuationTypeInvestmentGroupAllowed', true);
				}
			},
			{
				fieldLabel: 'Excluded Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					const investmentGroupId = f.findField('billingDefinitionInvestmentAccount.investmentGroup.id').getValue();
					queryEvent.combo.store.setBaseParam('investmentGroupId', investmentGroupId);
				}
			},
			{fieldLabel: 'Exclude Condition', name: 'excludeCondition.name', hiddenName: 'excludeCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingBasisSnapshot', qtip: 'Required if security is not selected.  Evaluates the condition on the billing basis snapshot object.  Can be used for example to exclude positions on maturity date.'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
			{fieldLabel: 'Note', name: 'exclusionNote', xtype: 'textarea'}
		]
	}]
});
