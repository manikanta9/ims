Clifton.billing.definition.AggregationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Basis Aggregation Type',
	width: 850,
	height: 500,
	iconCls: 'billing',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'formpanel',
					url: 'billingBasisAggregationType.json',
					instructions: 'Billing basis aggregation types define how billing basis values can be aggregated for fee sharing. This is primarily useful for cases where there is a tiered fee and clients receive benefit of shared billing basis. The field paths entered are from the billing definition investment account. When not selected on a schedule the system will group all applicable billing basis into one value.<br><br><b>Note:&nbsp;</b>Shared Schedules can additionally define if the billing basis is shared (grouped invoice) or should also be valued independently for each billing definition.',
					labelWidth: 170,
					items: [
						{fieldLabel: 'Aggregation Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Aggregation Field Path', name: 'aggregationBeanFieldPath'},
						{
							fieldLabel: 'Aggregation Label Template', name: 'aggregationLabelTemplate',
							qtip: 'Freemarker Template used to optionally generate a label value for each invoice detail line.  Should correspond with the aggregation field. i.e. aggregation on Client, the label should be the Client Name.  The first aggregation value will generate the label only.'
						}
					]
				}]
			}
		]
	}]
});
