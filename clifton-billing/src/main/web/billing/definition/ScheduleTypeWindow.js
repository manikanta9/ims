Clifton.billing.definition.ScheduleTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Schedule Type',
	width: 900,
	height: 550,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingScheduleType.json',
		readOnly: true,
		labelWidth: 145,
		items: [
			{fieldLabel: 'Schedule Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 80, grow: true},
			{fieldLabel: 'Order', name: 'scheduleOrder', xtype: 'integerfield'},
			{
				fieldLabel: 'Default Revenue Type', name: 'defaultRevenueType', hiddenName: 'defaultRevenueType', xtype: 'combo', mode: 'local',
				store: {
					xtype: 'arraystore',
					data: Clifton.billing.invoice.BillingInvoiceRevenueTypes
				}
			},
			{fieldLabel: 'Calculator', name: 'billingAmountCalculatorBean.name', xtype: 'linkfield', detailIdField: 'billingAmountCalculatorBean.id', detailPageClass: 'Clifton.system.bean.BeanWindow'},
			{boxLabel: 'Sharing Allowed (Allows use of schedules of this type to be shared across billing definitions and billing amounts are then calculated proportionally for each.)', name: 'sharedScheduleAllowed', xtype: 'checkbox'},
			{boxLabel: 'Minimum Fee', name: 'minimumFee', xtype: 'checkbox'},
			{boxLabel: 'Maximum Fee', name: 'maximumFee', xtype: 'checkbox'},
			{boxLabel: 'Annual Fee', name: 'annualFee', xtype: 'checkbox'},
			{boxLabel: 'Flat Fee', name: 'flatFee', xtype: 'checkbox'},
			{boxLabel: 'Flat Percentage', name: 'flatFeePercent', xtype: 'checkbox', qtip: 'Flat percentage means that the percentages entered are as they are regardless of billing or schedule frequency.  i.e. a 10% Discount'},
			{boxLabel: 'Tiered', name: 'tiered', xtype: 'checkbox'},
			{fieldLabel: 'Schedule Amount Label', name: 'scheduleAmountLabel'},
			{fieldLabel: 'Secondary Amount Label', name: 'secondaryScheduleAmountLabel'},
			// depends on Secondary Amount Label being populated, but since read only window, not setting that requirement
			{boxLabel: 'Secondary Amount Required (If checked, schedules that use this schedule type require the secondary amount to be populated.)', name: 'secondaryScheduleAmountRequired', xtype: 'checkbox'},
			{
				boxLabel: 'Accrual Frequency Allowed (If checked, schedules that use this schedule type can populate the accrual frequency on the schedule).', name: 'accrualFrequencyAllowed', xtype: 'checkbox',
				qtip: 'Schedules like annual minimums or schedules that depend on other billing amounts cannot use a schedule accrual frequency.'
			}
		]
	}]
});
