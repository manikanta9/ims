Clifton.billing.definition.ScheduleDefinitionDependencyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Schedule Dependency',
	width: 800,
	height: 500,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingScheduleBillingDefinitionDependency.json',
		labelWidth: 140,
		instructions: 'The following defines a billing definition dependency for the schedule.  This means that the schedule processing depends on the selected billing definition (and optional account information from that billing definition) in order to process.  This will require the definition\'s invoice to be generated prior to this schedule\'s definition invoice for the invoice period.  Dependencies can not apply between definitions that share a schedule, and dependencies can only be one level deep (i.e. a Billing Definition can not be a prerequisite and a dependent for the same period).',
		items: [
			{xtype: 'sectionheaderfield', header: 'Dependent'},
			{fieldLabel: 'Invoice Type', name: 'billingSchedule.billingDefinition.invoiceType.name', hiddenName: 'billingSchedule.billingDefinition.invoiceType.id', xtype: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true', doNotSubmitValue: true, allowBlank: false},
			{
				fieldLabel: 'Billing Definition', name: 'billingSchedule.billingDefinition.label', hiddenName: 'billingSchedule.billingDefinition.id', displayField: 'label', xtype: 'combo', url: 'billingDefinitionListFind.json', detailPageClass: 'Clifton.billing.definition.DefinitionWindow', requiredFields: ['billingSchedule.billingDefinition.invoiceType.name'], disableAddNewItem: true, doNotSubmitValue: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					queryEvent.combo.store.setBaseParam('invoiceTypeId', f.getFormValue('billingSchedule.billingDefinition.invoiceType.id', true, true));
				}
			},
			{
				fieldLabel: 'Billing Schedule', name: 'billingSchedule.labelWithDates', hiddenName: 'billingSchedule.id', xtype: 'combo', displayField: 'labelWithDates', loadAll: true, url: 'billingScheduleListForDefinition.json', detailPageClass: 'Clifton.billing.definition.ScheduleWindow', requiredFields: ['billingSchedule.billingDefinition.label'], disableAddNewItem: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					queryEvent.combo.store.setBaseParam('definitionId', f.getFormValue('billingSchedule.billingDefinition.id', true, true));
				}
			},
			{xtype: 'sectionheaderfield', header: 'Prerequisite'},
			{fieldLabel: 'Invoice Type', name: 'billingDefinition.invoiceType.name', hiddenName: 'billingDefinition.invoiceType.id', xtype: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true', doNotSubmitValue: true, allowBlank: false},
			{
				fieldLabel: 'Billing Definition', name: 'billingDefinition.label', hiddenName: 'billingDefinition.id', xtype: 'combo', detailPageClass: 'Clifton.billing.definition.DefinitionWindow', url: 'billingDefinitionListFind.json', displayField: 'label', requiredFields: ['billingDefinition.invoiceType.name'], disableAddNewItem: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					queryEvent.combo.store.setBaseParam('invoiceTypeId', f.getFormValue('billingDefinition.invoiceType.id', true, true));
				}
			},
			{
				fieldLabel: 'Billing Definition Account', name: 'billingDefinitionInvestmentAccount.label', hiddenName: 'billingDefinitionInvestmentAccount.id', xtype: 'combo', url: 'billingDefinitionInvestmentAccountListFind.json', displayField: 'label', requiredFields: ['billingDefinition.label'], mutuallyExclusiveFields: ['investmentAccount.label'],
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm();
					queryEvent.combo.store.setBaseParam('billingDefinitionId', f.getFormValue('billingDefinition.id', true, true));
				}
			},
			{fieldLabel: 'Client Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', requiredFields: ['billingDefinition.label'], mutuallyExclusiveFields: ['billingDefinitionInvestmentAccount.label'], detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 50, grow: true}
		]
	}]
});
