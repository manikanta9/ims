Clifton.billing.definition.DefinitionAccountEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Definition Account Entry',
	width: 1200,
	height: 600,
	iconCls: 'billing',

	items: [
		{
			xtype: 'formpanel',
			url: 'billingDefinitionPopulated.json',
			listRequestedProperties: 'id|referenceTwo.label|referenceTwo.id|billingBasisValuationTypeDescription|billingBasisCalculationType.name|billingBasisCalculationType.id|billingBasisValuationType.name|billingBasisValuationType.id|investmentGroup.name|investmentGroup.id|investmentManagerAccount.label|investmentManagerAccount.id|billingSchedule.name|billingSchedule.id|startDate|endDate',
			listRequestedPropertiesRoot: 'data.investmentAccountList',

			loadValidation: false,
			labelFieldName: 'label',
			labelWidth: 120,
			childTables: 'BillingDefinitionInvestmentAccount', // used by audit trail
			instructions: 'The following client accounts are billed under this definition. Optional Instrument Group (if valuation type supports it) can limit billing basis calculation to a subset of securities. If valuation type uses a manager account, that selection is required, otherwise not allowed.  Optional Billing Schedule can be selected if the billing definition investment account should apply to a specific schedule only.',

			getLoadURL: function() {
				return this.url + '?requestedPropertiesRoot=' + this.listRequestedPropertiesRoot + '&requestedProperties=' + this.listRequestedProperties + '&requestedMaxDepth=4&requestedPropertiesToExclude=billingScheduleList';
			},

			getSaveURL: function() {
				return 'billingDefinitionInvestmentAccountListForDefinitionSave.json';
			},

			getWarningMessage: function(f) {
				const wfStatus = TCG.getValue('workflowStatus.name', f.formValues);
				if (wfStatus === 'Approved') {
					return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Return for Edits</i>.';
				}
				if (wfStatus === 'Pending') {
					return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Reject</i>, or <i>Approve</i> existing changes and then <i>Return for Edits</i>';
				}

				return undefined;
			},

			listeners: {
				afterload: function(panel) {
					if (this.getFormValue('workflowStatus.name') !== 'Draft') {
						this.setReadOnlyFieldsForApprovedStatus(true);
					}
					else {
						this.setReadOnlyFieldsForApprovedStatus(false);
					}

					// If the company for the billing definition is not a system defined company type, then it's not a client or client relationships
					// so just check the box to allow searching across all accounts
					if (TCG.isFalse(this.getFormValue('businessCompany.type.systemDefined'))) {
						const tb = TCG.getChildByName(panel, 'definitionAccountsGrid').getTopToolbar();
						const fld = TCG.getChildByName(tb, 'includeAllAccounts');
						fld.setValue(true);
					}
				}
			},

			// Note: Should stay in sync with "Approved Billing Definition Modify Condition"
			setReadOnlyFieldsForApprovedStatus: function(readOnly) {
				const tb = TCG.getChildByName(this, 'definitionAccountsGrid').getTopToolbar();
				if (tb.items && tb.items.items) {
					const tbItems = tb.items.items;
					for (let i = 0; i < tbItems.length; i++) {
						if (tbItems[i].text === 'Add' || tbItems[i].text === 'Remove') {
							if (readOnly === true) {
								tbItems[i].disable();
							}
							else {
								tbItems[i].enable();
							}
						}
					}
				}
			},

			items: [
				{
					xtype: 'formgrid-scroll',
					name: 'definitionAccountsGrid',
					storeRoot: 'investmentAccountList',
					height: 450,
					heightResized: true,
					dtoClass: 'com.clifton.billing.definition.BillingDefinitionInvestmentAccount',
					addToolbarButtons: function(toolBar) {
						toolBar.add('->');
						toolBar.add({xtype: 'checkbox', submitValue: false, name: 'includeAllAccounts', fieldLabel: '', boxLabel: 'Leave unchecked to search selected company\'s client accounts only'});
					},
					// Overridden to support disabling read only columns on approved or pending approval definitions
					startEditing: function(row, col) {
						// As long as NOT a new definition and workflow status name is not equal to Draft lock down specific columns for edits
						if (TCG.getValue('workflowStatus.name', TCG.getParentFormPanel(this).getForm().formValues) !== 'Draft') {
							const colObj = this.colModel.getColumnAt(col);
							const colField = colObj.dataIndex;
							if (colField !== 'billingBasisValuationTypeDescription') {
								return false;
							}
						}

						TCG.grid.FormGridPanel.superclass.startEditing.apply(this, arguments);
						// need a way to get to grid from editable fields: combo, etc. to get form arguments
						const ed = this.colModel.getCellEditor(col, row);
						if (ed) {
							ed.containerGrid = this;
						}
					},
					columnsConfig: [
						{
							header: 'Account', width: 200, dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id', renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', allowBlank: false, detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true,
								beforequery: function(queryEvent) {
									const grid = queryEvent.combo.gridEditor.containerGrid;
									const f = TCG.getParentFormPanel(grid).getForm();
									const fld = TCG.getChildByName(grid.getTopToolbar(), 'includeAllAccounts');
									if (TCG.isFalse(fld.checked)) {
										queryEvent.combo.store.baseParams = {clientOrClientRelationshipCompanyId: TCG.getValue('businessCompany.id', f.formValues)};
									}
									else {
										queryEvent.combo.store.baseParams = {};
									}
								}
							}
							, summaryType: 'count'
							, summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return 'Total: ' + TCG.renderAmount(v, false, '0,000');
								}
							}
						},
						{
							header: 'Valuation Type Description', width: 150, dataIndex: 'billingBasisValuationTypeDescription', editor: {xtype: 'textfield'}, renderer: TCG.renderValueWithTooltip,
							tooltip: 'Can be set for any Valuation Type, but most useful for External Valuation Types to note where these External values are being imported from.'
						},
						{
							header: 'Valuation Type', width: 150, dataIndex: 'billingBasisValuationType.name', idDataIndex: 'billingBasisValuationType.id', renderer: TCG.renderValueWithTooltip,
							editor: {xtype: 'combo', allowBlank: false, url: 'billingBasisValuationTypeListFind.json'}
						},

						{
							header: 'Calculation Type', width: 100, dataIndex: 'billingBasisCalculationType.name', idDataIndex: 'billingBasisCalculationType.id', renderer: TCG.renderValueWithTooltip,
							editor: {xtype: 'combo', url: 'billingBasisCalculationTypeListFind.json?inactive=false'}
						},
						{
							header: 'Instrument Group', width: 110, dataIndex: 'investmentGroup.name', idDataIndex: 'investmentGroup.id', renderer: TCG.renderValueWithTooltip,
							editor: {xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'}
						},
						{
							header: 'Investment Manager', width: 110, dataIndex: 'investmentManagerAccount.label', idDataIndex: 'investmentManagerAccount.id', renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'investmentManagerAccountListFind.json', detailPageClass: 'Clifton.investment.manager.AccountWindow',
								displayField: 'label',
								beforequery: function(queryEvent) {
									const grid = queryEvent.combo.gridEditor.containerGrid;
									const f = TCG.getParentFormPanel(grid).getForm();
									const fld = TCG.getChildByName(grid.getTopToolbar(), 'includeAllAccounts');
									if (TCG.isFalse(fld.checked)) {
										queryEvent.combo.store.baseParams = {clientOrClientRelationshipCompanyId: TCG.getValue('businessCompany.id', f.formValues)};
									}
									else {
										queryEvent.combo.store.baseParams = {};
									}
								}
							}
						},
						{
							header: 'Schedule', width: 110, dataIndex: 'billingSchedule.name', idDataIndex: 'billingSchedule.id', renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'billingScheduleListForDefinition.json',
								root: 'data',
								beforequery: function(queryEvent) {
									const grid = queryEvent.combo.gridEditor.containerGrid;
									const f = TCG.getParentFormPanel(grid).getForm();
									queryEvent.combo.store.baseParams = {definitionId: TCG.getValue('id', f.formValues)};
								}
							}
						},
						{header: 'Start Date', width: 65, dataIndex: 'startDate', type: 'date', editor: {xtype: 'datefield'}},
						{header: 'End Date', width: 65, dataIndex: 'endDate', type: 'date', editor: {xtype: 'datefield'}}
					],
					plugins: {ptype: 'gridsummary'},
					// When Approved, Client Account is Read Only and Right Click in the combo is not available, so Double Click will open the Client Account always
					listeners: {
						// drill into account
						'celldblclick': function(grid, rowIndex, cellIndex, evt) {
							const row = grid.store.data.items[rowIndex];
							const accountId = row.data['referenceTwo.id'];
							if (accountId) {
								const detailPageClass = 'Clifton.investment.account.AccountWindow';
								TCG.createComponent(detailPageClass, {
									id: TCG.getComponentId(detailPageClass, accountId),
									params: {id: accountId},
									openerCt: grid
								});
							}
						}
					}
				}
			]
		}
	]
});
