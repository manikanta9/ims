Clifton.billing.definition.BillingBasisPostProcessorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Basis Post Processor',
	width: 600,
	height: 500,
	iconCls: 'billing',

	items: [{
		xtype: 'formpanel',
		url: 'billingDefinitionBillingBasisPostProcessor.json',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Billing Definition', name: 'referenceOne.label', detailIdField: 'referenceOne.id', xtype: 'linkfield', detailPageClass: 'Clifton.billing.definition.DefinitionWindow'},
			{
				fieldLabel: 'Post Processor', name: 'referenceTwo.name', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Billing Basis Post Processor', detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Billing Basis Post Processor'}}};
				}
			},
			{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'}
		]
	}]
});
