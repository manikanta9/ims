Clifton.billing.definition.DefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Definition',
	width: 1400,
	height: 800,
	iconCls: 'billing',

	enableRefreshWindow: true,

	gridTabWithCount: ['Billing Accounts', 'Definition Dependencies', 'Recipients'],

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Details',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'BillingDefinition'
				},
				items: [{
					xtype: 'formpanel-custom-fields',
					url: 'billingDefinition.json',
					columnGroupName: 'Billing Definition Custom Fields',
					dynamicFieldFormFragment: 'billingDefinitionCustomFields',
					labelFieldName: 'label',
					labelWidth: 140,
					childTables: 'BillingDefinitionInvestmentAccount', // used by audit trail

					getWarningMessage: function(f) {
						const wfStatus = TCG.getValue('workflowStatus.name', f.formValues);
						const wfState = TCG.getValue('workflowState.name', f.formValues);
						if (wfStatus === 'Approved') {
							if (wfState === 'Archived') {
								return 'This definition is <b>' + wfState + '</b> and only limited fields can be edited and is no longer eligible for generating invoices.';
							}
							return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to <i>Return for Edits</i>.';
						}
						if (wfStatus === 'Pending') {
							return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to <i>Reject</i>, or <i>Approve</i> existing changes and then <i>Return for Edits</i>';
						}

						return undefined;
					},

					listeners: {
						afterload: function(panel) {
							if (this.getFormValue('workflowStatus.name') !== 'Draft') {
								this.setReadOnlyFieldsForApprovedStatus(true);
							}
							else {
								this.setReadOnlyFieldsForApprovedStatus(false);
							}
						}
					},

					// Note: Should stay in sync with "Approved Billing Definition Modify Condition"
					approvedReadOnlyFields: ['businessCompany.nameWithType', 'billingContract.label', 'contractSearchAll', 'invoiceType.name', 'billingFrequency', 'billingCurrency.symbol', 'paymentInAdvance', 'startDate', 'secondYearStartDate', 'endDate'],
					setReadOnlyFieldsForApprovedStatus: function(readOnly) {
						for (let i = 0; i < this.approvedReadOnlyFields.length; i++) {
							const fieldName = this.approvedReadOnlyFields[i];
							this.getForm().findField(fieldName).setReadOnly(readOnly);
						}
					},

					items: [
						{fieldLabel: 'Company', name: 'businessCompany.nameWithType', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?contractAllowed=true', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, displayField: 'nameWithType'},
						{
							fieldLabel: 'Contract', name: 'billingContract.label', hiddenName: 'billingContract.id', xtype: 'combo', url: 'businessContractListFind.json', requiredFields: ['businessCompany.id'], displayField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true,
							beforequery: function(queryEvent) {
								const companyId = queryEvent.combo.getParentForm().getForm().findField('businessCompany.id').value;
								if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('contractSearchAll').checked)) {
									queryEvent.combo.store.baseParams = {
										companyIdOrRelatedCompany: companyId,
										includeClientOrClientRelationship: true
									};
								}
								else {
									queryEvent.combo.store.baseParams = {
										companyId: companyId,
										beginsWithContractTypeName: 'IMA'
									};
								}
							}
						},

						{
							boxLabel: 'Leave unchecked to search for Company specific IMA related contract types.', xtype: 'checkbox', name: 'contractSearchAll', submitValue: 'false',
							qtip: 'If checked, will search against any company contract, or related client contract (if part of a client group) or client relationship contracts.',
							listeners: {
								check: function(f) {
									// On changes reset contract drop down
									const p = TCG.getParentFormPanel(f);
									const bc = p.getForm().findField('billingContract.label');
									bc.clearAndReset();
								}
							}
						},
						{xtype: 'label', html: '<hr />'},
						{
							fieldLabel: 'Broker Fee Company', name: 'revenueShareExternalCompany.nameWithType', hiddenName: 'revenueShareExternalCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, displayField: 'nameWithType',
							qtip: 'Required selection in order to use schedules that calculate a Broker Fee amount.  This is who we share the revenue with externally.  Most commonly used for retail clients where we have a relationship with the broker and they get a portion of the gross revenue.'
						},
						{
							fieldLabel: 'Revenue Share Company', name: 'revenueShareInternalCompany.nameWithType', hiddenName: 'revenueShareInternalCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, displayField: 'nameWithType',
							qtip: 'Required selection in order to use schedules that calculate a Revenue Share amount.  This is who we share the revenue with internally, i.e. EV, PPA Seattle.  Used for internal reporting only.'
						},
						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									config: {columnWidth: 0.33},
									rows: [
										{
											fieldLabel: 'Invoice Type', name: 'invoiceType.name', hiddenName: 'invoiceType.id', xtype: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true', detailPageClass: 'Clifton.billing.invoice.TypeWindow',
											requestedProps: 'invoicePostedToPortalDefault',
											listeners: {
												// set post to portal if a new billing definition and invoice type is selected to do so
												select: function(combo, record, index) {
													const formPanel = combo.getParentForm();
													if (!TCG.isNumber(formPanel.getIdFieldValue())) {
														formPanel.getForm().findField('invoicePostedToPortal').setValue(TCG.isTrue(record.json.invoicePostedToPortalDefault));
													}
												}
											}
										},
										{fieldLabel: 'Billing Currency', name: 'billingCurrency.symbol', hiddenName: 'billingCurrency.id', xtype: 'combo', displayField: 'symbol', url: 'investmentSecurityListFind.json?currency=true'}
									]
								},
								{
									config: {columnWidth: 0.66},
									rows: [
										{
											fieldLabel: 'Billing Frequency', name: 'billingFrequency', hiddenName: 'billingFrequency', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
											store: new Ext.data.ArrayStore({
												fields: ['name', 'value', 'description'],
												data: Clifton.billing.BillingFrequencies
											})
										},
										{boxLabel: 'Payments are made in advance (Leave unchecked to invoice in arrears)', xtype: 'checkbox', name: 'paymentInAdvance'}
									]
								}
							]
						},
						{xtype: 'checkbox', name: 'invoicePostedToPortal', fieldLabel: '', boxLabel: 'Invoices for this billing definition are posted to the portal.'},
						{xtype: 'label', html: '<hr />'},
						{
							fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox', qtip: 'Start Date is the first day of the billing period (date positions on).',
							items: [
								{name: 'startDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;Second Year Start Date:', name: 'SecondYearStartDateDisplay', xtype: 'displayfield', width: 165, qtip: 'Second Year Start Date is used for billing definitions with an Annual Min/Max defined and is used to define the annual billing period.'},
								{name: 'secondYearStartDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', name: 'EndDateDisplay', xtype: 'displayfield', width: 165, qtip: 'End Date is set when the billing period end is known (date when positions are removed).'},
								{name: 'endDate', xtype: 'datefield', flex: 1}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Note', xtype: 'textarea', name: 'note', height: 50},
						{
							xtype: 'fieldset',
							title: 'Advanced',
							collapsed: false,
							items: [{
								xtype: 'formfragment',
								frame: false,
								labelWidth: 135,
								name: 'billingDefinitionCustomFields',
								items: []
							}]
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'BillingDefinition',
							hierarchyCategoryName: 'Billing Tags'
						}
					],
					getDefaultData: function(win) {
						const dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						dd.billingFrequency = 'QUARTERLY';
						dd.billingFrequencyLabel = 'Quarterly';
						dd.billingCurrency = TCG.data.getData('investmentSecurityBySymbol.json?symbol=USD&currency=true', this);

						return dd;
					}
				}]
			},

			{
				title: 'Billing Accounts',
				items: [{
					xtype: 'gridpanel',
					name: 'billingDefinitionInvestmentAccountListForDefinition',
					forceLocalFiltering: true,
					reloadOnChange: true,
					instructions: 'The following client accounts are billed under this definition. Optional Instrument Group (if valuation type supports it) can limit billing basis calculation to a subset of securities. If valuation type uses a manager account, that selection is required, otherwise not allowed.  Optional Billing Schedule can be selected if the billing definition investment account should apply to a specific schedule only.',

					columns: [
						{
							header: '', tooltip: 'Open Client Account Window', width: 35, dataIndex: 'referenceTwo.id', filter: false, sortable: false,
							renderer: function(v, metaData, row) {
								return TCG.renderActionColumn('account', '', 'Open Client Account Window');
							},
							eventListeners: {
								'click': function(column, grid, rowIndex, event) {
									const accountId = TCG.getValue('referenceTwo.id', grid.getStore().getAt(rowIndex).json);
									if (TCG.isNotBlank(accountId)) {
										grid.ownerGridPanel.editor.openDetailPage('Clifton.investment.account.AccountWindow', grid.ownerGridPanel, accountId, rowIndex);
									}
								}
							}
						},
						{header: 'Client Account', width: 200, dataIndex: 'referenceTwo.label'},
						{header: 'Positions On', width: 65, dataIndex: 'referenceTwo.inceptionDate', hidden: true},
						{header: 'Service', width: 125, dataIndex: 'referenceTwo.businessService.name'},
						{header: 'Valuation Type Description', width: 150, dataIndex: 'billingBasisValuationTypeDescription', tooltip: 'Can be set for any Valuation Type, but most useful for External Valuation Types to note where these External values are being imported from.'},
						{header: 'Valuation Type', width: 150, dataIndex: 'billingBasisValuationType.name', renderer: TCG.renderValueWithTooltip},
						{header: 'Calculation Type', width: 100, dataIndex: 'billingBasisCalculationType.name', renderer: TCG.renderValueWithTooltip},
						{header: 'Instrument Group', width: 110, dataIndex: 'investmentGroup.name', renderer: TCG.renderValueWithTooltip},
						{header: 'Investment Manager', width: 110, dataIndex: 'investmentManagerAccount.label', renderer: TCG.renderValueWithTooltip},
						{header: 'Schedule', width: 110, dataIndex: 'billingSchedule.name', renderer: TCG.renderValueWithTooltip},
						{header: 'Start Date', width: 65, dataIndex: 'startDate', type: 'date'},
						{header: 'End Date', width: 65, dataIndex: 'endDate', type: 'date'}
					],
					getLoadParams: function() {
						return {
							definitionId: this.getWindow().getMainFormId()
						};
					},
					addFirstToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Bulk Entry',
							tooltip: 'Bulk Entry across all accounts for this billing definition',
							iconCls: 'pencil',
							name: 'bulkEntry',
							scope: this,
							handler: function() {
								gridPanel.editor.openDetailPage('Clifton.billing.definition.DefinitionAccountEntryWindow', gridPanel, gridPanel.getWindow().getMainFormId());
							}
						});
						toolBar.add('-');
					},
					editor: {
						detailPageClass: 'Clifton.billing.definition.DefinitionAccountWindow',
						deleteURL: 'billingDefinitionInvestmentAccountDelete.json',
						getDefaultData: function(gridPanel, row) {
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								name: 'add',
								scope: this,
								handler: function() {
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								name: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									const sm = editor.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										const deleteConfirmMessage = editor.deleteConfirmMessage ? editor.deleteConfirmMessage : 'Would you like to delete selected row?';
										Ext.Msg.confirm('Delete Selected Row', deleteConfirmMessage, function(a) {
											if (a === 'yes') {
												editor.deleteSelection(sm, gridPanel, editor.getDeleteURL());
											}
										});
									}
								}
							});
							toolBar.add('-');
						}

					},


					listeners: {
						afterRender: function() {
							this.enableDisableButtons();
							const fp = this.getWindow().getMainFormPanel();
							fp.on('afterload', function() {
								this.enableDisableButtons();
							}, this);
						}
					},

					enableDisableButtons: function() {
						const workflowStatus = TCG.getValue('workflowStatus.name', this.getWindow().getMainForm().formValues);
						if (TCG.isEquals('Draft', workflowStatus)) {
							TCG.getChildByName(this.getTopToolbar(), 'bulkEntry', true).enable();
							TCG.getChildByName(this.getTopToolbar(), 'add', true).enable();
							TCG.getChildByName(this.getTopToolbar(), 'remove', true).enable();
						}
						else {
							TCG.getChildByName(this.getTopToolbar(), 'bulkEntry', true).disable();
							TCG.getChildByName(this.getTopToolbar(), 'add', true).disable();
							TCG.getChildByName(this.getTopToolbar(), 'remove', true).disable();
						}
					}
				}]
			},


			{
				title: 'Account Security Exclusions',
				items: [{
					xtype: 'gridpanel',
					name: 'billingDefinitionInvestmentAccountSecurityExclusionListFind',
					reloadOnChange: true,
					columns: [

						{header: 'Client Account', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Valuation Type Description', hidden: true, width: 150, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationTypeDescription', filter: {searchFieldName: 'billingBasisValuationTypeDescription'}},
						{header: 'Valuation Type', width: 75, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationType.name', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'billingBasisValuationTypeName'}},
						{header: 'Calculation Type', width: 75, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisCalculationType.name', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'billingBasisCalculationTypeName'}},
						{header: 'Instrument Group', width: 75, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.investmentGroup.name', renderer: TCG.renderValueWithTooltip},
						{header: 'Exclusion Note', width: 200, dataIndex: 'exclusionNote'},
						{header: 'Excluded Security', width: 125, dataIndex: 'investmentSecurity.label', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'investmentSecurityId', type: 'combo', url: 'investmentSecurityListFind.json'}},
						{header: 'Exclude Condition', width: 125, dataIndex: 'excludeCondition.name', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'excludeConditionName'}},
						{header: 'Start Date', width: 50, dataIndex: 'startDate', type: 'date'},
						{header: 'End Date', width: 50, dataIndex: 'endDate', type: 'date'}
					],
					getLoadParams: function() {
						return {
							definitionId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.billing.definition.DefinitionAccountSecurityExclusionWindow',
						getDefaultData: function(gridPanel, row) {
							return {
								billingDefinitionInvestmentAccount: {
									referenceOne: gridPanel.getWindow().getMainForm().formValues
								}
							};
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								name: 'add',
								scope: this,
								handler: function() {
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								name: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									const sm = editor.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										const deleteConfirmMessage = editor.deleteConfirmMessage ? editor.deleteConfirmMessage : 'Would you like to delete selected row?';
										Ext.Msg.confirm('Delete Selected Row', deleteConfirmMessage, function(a) {
											if (a === 'yes') {
												editor.deleteSelection(sm, gridPanel, editor.getDeleteURL());
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					},


					listeners: {
						afterRender: function() {
							this.enableDisableButtons();
							const fp = this.getWindow().getMainFormPanel();
							fp.on('afterload', function() {
								this.enableDisableButtons();
							}, this);
						}
					},

					enableDisableButtons: function() {
						const workflowStatus = TCG.getValue('workflowStatus.name', this.getWindow().getMainForm().formValues);
						if (TCG.isEquals('Draft', workflowStatus)) {
							TCG.getChildByName(this.getTopToolbar(), 'add', true).enable();
							TCG.getChildByName(this.getTopToolbar(), 'remove', true).enable();
						}
						else {
							TCG.getChildByName(this.getTopToolbar(), 'add', true).disable();
							TCG.getChildByName(this.getTopToolbar(), 'remove', true).disable();
						}
					}
				}]
			},


			{
				title: 'Billing Schedules',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Definition Specific Schedules',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						name: 'billingScheduleListFind',
						instructions: 'The following billing schedules apply to this billing definition.',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{
								header: 'Schedule Name', width: 150, dataIndex: 'name',
								renderer: function(v, metaData, r) {
									const note = r.data['description'];
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							{header: 'Schedule Description', width: 150, dataIndex: 'description', hidden: true},
							{header: 'Revenue Type', width: 65, dataIndex: 'revenueTypeLabel', filter: {type: 'combo', searchFieldName: 'revenueType', mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}}},
							{header: 'Schedule Type', width: 100, dataIndex: 'scheduleType.name'},
							{header: 'Aggregation Type', width: 75, dataIndex: 'billingBasisAggregationType.name', filter: {type: 'combo', url: 'billingBasisAggregationTypeListFind.json', searchFieldName: 'billingBasisAggregationTypeId'}},
							{header: 'Account(s)', width: 100, dataIndex: 'accountLabel', filter: false, sortable: false},
							{header: 'Waive Condition', width: 100, dataIndex: 'waiveSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json', searchFieldName: 'waiveSystemConditionId'}},
							{
								header: 'Frequency', width: 60, dataIndex: 'scheduleFrequency',
								filter: {
									type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'value', 'description'],
										data: Clifton.billing.BillingScheduleFrequencies
									})
								}
							},
							{
								header: 'Accrual', width: 60, dataIndex: 'accrualFrequency', filter: {
									type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'value', 'description'],
										data: Clifton.billing.BillingScheduleAccrualFrequencies
									})
								}
							},
							{header: 'Amount', width: 55, dataIndex: 'scheduleAmount', type: 'float', useNull: true},
							{header: 'Secondary Amount', width: 60, dataIndex: 'secondaryScheduleAmount', type: 'float', useNull: true},
							{header: 'Order', width: 55, dataIndex: 'scheduleType.scheduleOrder', type: 'int', filter: {searchFieldName: 'scheduleTypeOrder'}, defaultSortColumn: true, defaultSortDirection: 'asc', hidden: true},
							{header: 'Start Date', width: 45, dataIndex: 'startDate'},
							{header: 'End Date', width: 40, dataIndex: 'endDate'},
							{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}

						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ScheduleWindow',
							getDefaultData: function(gridPanel) {
								const fv = gridPanel.getWindow().getMainForm().formValues;
								return {billingDefinition: fv};
							},

							addToolbarAddButton: function(toolBar, gridPanel) {
								const editor = this;
								toolBar.add({
									text: 'Add',
									xtype: 'splitbutton',
									tooltip: 'Add a new item',
									iconCls: 'add',
									handler: function() {
										editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
									},
									menu: new Ext.menu.Menu({
										items: [
											{
												text: 'Add',
												tooltip: 'Add a new item',
												iconCls: 'add',
												handler: function() {
													editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
												}
											},
											{
												text: 'Add from Template',
												tooltip: 'Populate new schedule fields from selected template',
												iconCls: 'copy',
												menu: new Ext.menu.Menu({
													layout: 'fit',
													style: {overflow: 'visible'},
													width: 250,
													items: [
														{
															xtype: 'combo', name: 'template', url: 'billingScheduleTemplateListFind.json?active=true',
															getListParent: function() {
																return this.el.up('.x-menu');
															},
															listeners: {
																'select': function(combo) {
																	const loader = new TCG.data.JsonLoader({
																		params: {id: combo.getValue()},
																		onLoad: function(record, conf) {
																			const pageClass = editor.getDetailPageClass();
																			// Ignore Template Name, use description field as the schedule name
																			record.name = record.description;
																			record.description = undefined;
																			TCG.createComponent(pageClass, {
																				id: TCG.getComponentId(pageClass),
																				defaultData: Ext.apply(record, editor.getDefaultData(gridPanel)),
																				defaultDataIsReal: true,
																				openerCt: gridPanel
																			});
																		}
																	});
																	loader.load('billingScheduleForTemplate.json');
																	combo.reset();
																	TCG.getParentByClass(combo, Ext.Button).hideMenu();
																}
															}
														}
													]
												})
											}

										]
									})
								});
								toolBar.add('-');
							}
						},
						getLoadParams: function() {
							return {
								sharedSchedule: false,
								billingDefinitionId: this.getWindow().getMainFormId()
							};
						}
					},

					{flex: 0.02},

					{
						title: 'Shared Schedules',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						name: 'billingScheduleListFind',
						instructions: 'The following billing schedules are used for this billing definition but can also be shared with other billing definitions (better tiered pricing, etc.).',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{
								header: 'Schedule Name', width: 150, dataIndex: 'name',
								renderer: function(v, metaData, r) {
									const note = r.data['description'];
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							{header: 'Schedule Description', width: 150, dataIndex: 'description', hidden: true},
							{header: 'Revenue Type', width: 65, dataIndex: 'revenueTypeLabel', filter: {type: 'combo', searchFieldName: 'revenueType', mode: 'local', store: {xtype: 'arraystore', data: Clifton.billing.invoice.BillingInvoiceRevenueTypes}}},
							{header: 'Schedule Type', width: 100, dataIndex: 'scheduleType.name'},
							{header: 'Aggregation Type', width: 75, dataIndex: 'billingBasisAggregationType.name', filter: {type: 'combo', url: 'billingBasisAggregationTypeListFind.json', searchFieldName: 'billingBasisAggregationTypeId'}},
							{header: 'Do Not Group Invoice', width: 50, dataIndex: 'doNotGroupInvoice', type: 'boolean'},
							{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
							{header: 'Waive Condition', width: 100, dataIndex: 'waiveSystemCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json', searchFieldName: 'waiveSystemConditionId'}},
							{
								header: 'Frequency', width: 60, dataIndex: 'scheduleFrequency', filter: {
									type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'value', 'description'],
										data: Clifton.billing.BillingScheduleFrequencies
									})
								}
							},
							{
								header: 'Accrual', width: 60, dataIndex: 'accrualFrequency', filter: {
									type: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'value', 'description'],
										data: Clifton.billing.BillingScheduleAccrualFrequencies
									})
								}
							},
							{header: 'Amount', width: 60, dataIndex: 'scheduleAmount', type: 'float', useNull: true},
							{header: 'Secondary Amount', width: 60, dataIndex: 'secondaryScheduleAmount', type: 'float', useNull: true, hidden: true},
							{header: 'Order', width: 60, dataIndex: 'scheduleType.scheduleOrder', type: 'int', filter: {searchFieldName: 'scheduleTypeOrder'}, defaultSortColumn: true, defaultSortDirection: 'asc', hidden: true},
							{header: 'Start Date', width: 40, dataIndex: 'startDate'},
							{header: 'End Date', width: 40, dataIndex: 'endDate'},
							{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ScheduleSharedWindow',
							getDefaultData: function(gridPanel, row, className) {
								const fv = gridPanel.getWindow().getMainForm().formValues;
								return {sharedSchedule: true, autoApplySharedScheduleToDefinition: fv};
							},
							deleteURL: 'billingScheduleSharingForScheduleAndDefinitionDelete.json',
							getDeleteParams: function(selectionModel) {
								return {
									definitionId: this.getWindow().getMainFormId(),
									scheduleId: selectionModel.getSelected().get('id')
								};
							},
							addToolbarAddButton: function(toolBar, gridPanel) {
								const editor = this;
								toolBar.add(new TCG.form.ComboBox({name: 'sharedSchedule', url: 'billingScheduleListFind.json?sharedSchedule=true', width: 150, listWidth: 230}));
								toolBar.add({
									text: 'Add Selected Schedule',
									tooltip: 'Add selected shared schedule to this definition',
									iconCls: 'add',
									handler: function() {
										const scheduleId = TCG.getChildByName(toolBar, 'sharedSchedule').getValue();
										if (TCG.isBlank(scheduleId)) {
											TCG.showError('You must first select a shared schedule to link to.');
										}
										else {
											const definitionId = gridPanel.getWindow().getMainFormId();
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Adding...',
												params: {definitionId: definitionId, scheduleId: scheduleId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'sharedSchedule').reset();
												}
											});
											loader.load('billingScheduleSharingLink.json');
										}
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Add',
									xtype: 'splitbutton',
									tooltip: 'Add a new item',
									iconCls: 'add',
									handler: function() {
										editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
									},
									menu: new Ext.menu.Menu({
										items: [
											{
												text: 'Add',
												tooltip: 'Create a new shared schedule',
												iconCls: 'add',
												scope: this,
												handler: function() {
													this.openDetailPage(this.getDetailPageClass(), gridPanel);
												}
											},
											{
												text: 'Add from Template',
												tooltip: 'Populate new shared schedule fields from selected template',
												iconCls: 'copy',
												menu: new Ext.menu.Menu({
													layout: 'fit',
													style: {overflow: 'visible'},
													width: 250,
													items: [
														{
															xtype: 'combo', name: 'template', url: 'billingScheduleTemplateListFind.json?active=true&scheduleTypeSharedScheduleAllowed=true',
															getListParent: function() {
																return this.el.up('.x-menu');
															},
															listeners: {
																'select': function(combo) {
																	const loader = new TCG.data.JsonLoader({
																		params: {id: combo.getValue()},
																		onLoad: function(record, conf) {
																			// Ignore Template Name, use description field as the schedule name
																			record.name = record.description;
																			record.description = undefined;
																			const pageClass = editor.getDetailPageClass();
																			TCG.createComponent(pageClass, {
																				id: TCG.getComponentId(pageClass),
																				defaultData: Ext.apply(record, editor.getDefaultData(gridPanel)),
																				defaultDataIsReal: true,
																				openerCt: gridPanel
																			});
																		}
																	});
																	loader.load('billingScheduleForTemplate.json');
																	combo.reset();
																	TCG.getParentByClass(combo, Ext.Button).hideMenu();
																}
															}
														}
													]
												})
											}

										]
									})
								});
								toolBar.add('-');
							}
						},
						getLoadParams: function() {
							return {
								sharedSchedule: true,
								sharedBillingDefinitionId: this.getWindow().getMainFormId()
							};
						}
					}
				]
			},


			{
				title: 'Definition Dependencies',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				updateTabCount: function() {
					const tab = this;
					const gp1 = TCG.getChildrenByClass(tab, TCG.grid.GridPanel)[0];
					const gp2 = TCG.getChildrenByClass(tab, TCG.grid.GridPanel)[1];

					gp1.reloadOnRender = false;
					gp2.reloadOnRender = false;
					gp1.ds.addListener('load', function(store, records, opts) {
						tab.updateTotalTabCount();
					});
					gp2.ds.addListener('load', function(store, records, opts) {
						tab.updateTotalTabCount();
					});
					gp1.reload();
					gp2.reload();
				},

				updateTotalTabCount: function() {
					const count1 = TCG.getChildrenByClass(this, TCG.grid.GridPanel)[0].grid.getStore().getCount();
					const count2 = TCG.getChildrenByClass(this, TCG.grid.GridPanel)[1].grid.getStore().getCount();
					this.setTitle('Definition Dependencies<b> (' + count1 + '/' + count2 + ')</b>');
				},

				items: [
					{
						title: 'Prerequisite Billing Definitions',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						name: 'billingScheduleBillingDefinitionDependencyListByDefinition',
						instructions: 'This billing definition depends on the following billing definitions.  This means that the following billing definition\'s invoices are required to be processed prior to this billing definition\'s invoice for the period. To add or remove dependencies, please go to the schedule the dependency is for.',
						additionalPropertiesToRequest: 'billingSchedule.description',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{
								header: 'Prerequisite Billing Definition', width: 150, dataIndex: 'billingDefinition.label',
								renderer: function(v, metaData, r) {
									const note = r.data.note;
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							// Using Non-Persistent Field to enable local filtering
							{header: 'Prerequisite Active', dataIndex: 'billingDefinition.active', width: 40, type: 'boolean', tooltip: 'Prerequisite Billing Definition Active', nonPersistentField: true},

							{header: 'Prerequisite Billing Account', width: 150, dataIndex: 'accountLabel'},
							{header: 'Definition Account', width: 100, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{header: 'Investment Account', width: 100, hidden: true, dataIndex: 'investmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{
								header: 'Dependent Billing Schedule', width: 75, dataIndex: 'billingSchedule.label',
								renderer: function(v, metaData, r) {
									const note = r.json.billingSchedule.description;
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							// Using Non-Persistent Field to enable local filtering
							{header: 'Schedule Active', dataIndex: 'billingSchedule.active', width: 35, type: 'boolean', tooltip: 'Active Schedule', nonPersistentField: true},
							{header: 'Note', width: 100, hidden: true, dataIndex: 'note', renderer: TCG.renderValueWithTooltip}
						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ScheduleDefinitionDependencyWindow',
							getDefaultData: function(gridPanel) {
								const fv = gridPanel.getWindow().getMainForm().formValues;
								return {
									billingSchedule: {billingDefinition: fv}
								};
							}
						},
						getLoadParams: function(firstLoad) {
							if (TCG.isTrue(firstLoad)) {
								this.setFilterValue('billingSchedule.active', true);
								this.setFilterValue('billingDefinition.active', true);
							}
							return {
								definitionId: this.getWindow().getMainFormId(),
								prerequisite: false
							};
						}
					},

					{flex: 0.02},

					{
						title: 'Dependent Billing Definitions',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						name: 'billingScheduleBillingDefinitionDependencyListByDefinition',
						instructions: 'The following billing definitions depend on this billing definition, i.e. this billing definition is a prerequisite of the following billing definitions.  This means that this billing definition\'s invoices must be processed prior to its dependents.',
						additionalPropertiesToRequest: 'billingSchedule.description',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{
								header: 'Dependent Billing Definition', width: 150, dataIndex: 'billingSchedule.billingDefinition.label',
								renderer: function(v, metaData, r) {
									const note = r.data.note;
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							{header: 'Prerequisite Billing Account', width: 150, dataIndex: 'accountLabel'},
							{header: 'Definition Account', width: 100, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{header: 'Investment Account', width: 100, hidden: true, dataIndex: 'investmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{
								header: 'Dependent Billing Schedule', width: 75, dataIndex: 'billingSchedule.label',
								renderer: function(v, metaData, r) {
									const note = r.json.billingSchedule.description;
									if (TCG.isNotBlank(note)) {
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(note);
									}
									return v;
								}
							},
							// Using Non-Persistent Field to enable local filtering
							{header: 'Schedule Active', dataIndex: 'billingSchedule.active', width: 35, type: 'boolean', tooltip: 'Active Schedule', nonPersistentField: true},
							{header: 'Note', width: 100, hidden: true, dataIndex: 'note', renderer: TCG.renderValueWithTooltip}
						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ScheduleDefinitionDependencyWindow',
							getDefaultData: function(gridPanel) {
								const fv = gridPanel.getWindow().getMainForm().formValues;
								return {
									billingDefinition: fv
								};
							}
						},
						getLoadParams: function(firstLoad) {
							if (TCG.isTrue(firstLoad)) {
								this.setFilterValue('billingSchedule.active', true);
							}
							return {
								definitionId: this.getWindow().getMainFormId(),
								prerequisite: true
							};
						}
					}
				]
			},


			{
				title: 'Billing Basis Post Processors',
				items: [{
					name: 'billingDefinitionBillingBasisPostProcessorListByDefinition',
					xtype: 'gridpanel',
					instructions: 'Billing Basis post processors can alter billing basis. For example, the Billing Basis Shifter can move billing basis from one account to another.',
					columns: [
						{header: 'Post Processor', width: 250, dataIndex: 'referenceTwo.name'},
						{header: 'Order', width: 60, dataIndex: 'order', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.billing.definition.BillingBasisPostProcessorWindow',
						deleteURL: 'billingDefinitionBillingBasisPostProcessorDelete.json',
						getDefaultData: function() {
							return {referenceOne: this.getWindow().getMainForm().formValues};
						}
					},
					getLoadParams: function() {
						return {
							definitionId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Invoices',
				items: [{
					xtype: 'billing-invoiceGrid',
					instructions: 'Billing Invoices for selected Billing Definitions: bills generated and sent to clients for payment.',
					showDefinitionInfo: false,
					columnOverrides: [
						{dataIndex: 'paidAmount', hidden: false}
					],
					editor: {
						detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							const defaultDate = Clifton.billing.invoice.getDefaultInvoiceEndDate().format('m/d/Y');
							toolBar.add(new Ext.form.DateField({
								name: 'invoiceDate',
								fieldLabel: 'Invoice Date',
								width: 100,
								value: defaultDate
							}));
							toolBar.add({
								text: 'Generate',
								tooltip: 'Generate Invoice',
								iconCls: 'add',
								handler: function() {
									const invoiceDate = TCG.parseDate(TCG.getChildByName(toolBar, 'invoiceDate').getValue(), 'm/d/Y');
									if (TCG.isBlank(invoiceDate)) {
										TCG.showError('You must first enter an invoice date to generate an invoice for.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Generating...',
											timeout: 180000,
											params: {billingDefinitionId: gridPanel.getWindow().getMainFormId(), invoiceDate: invoiceDate.format('m/d/Y')},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'invoiceDate').reset();
											}
										});
										loader.load('billingInvoiceGenerate.json');
									}
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected invoice',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									editor.getGridPanel().deleteInvoice(editor);
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {
							billingDefinitionId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Recipients',
				items: [{
					xtype: 'business-companyContactGrid',
					instructions: 'The following contacts are associated with this company and marked to receive invoices (Contact Type: Management Fee Invoice).',

					// Uses own columns to more closely reflect the order on the client window as well as include the relevant information for invoice recipients
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Company Type', width: 85, dataIndex: 'referenceOne.type.label', filter: {type: 'combo', url: 'businessCompanyTypeListFind.json', searchFieldName: 'companyTypeId'},
							renderer: function(v, metaData, r) {
								if (v === 'Client Relationship') {
									metaData.css = 'amountPositive';
								}
								else if (v === 'Client' || v === 'Client Group' || v === 'Commingled Vehicle' || v === 'Virtual Client') {
									metaData.css = 'amountAdjusted';
								}
								metaData.attr = 'qtip=\'' + r.data['referenceOne.name'] + '\'';
								return v;
							}

						},
						{header: 'Company', width: 200, dataIndex: 'referenceOne.nameExpandedWithType', filter: {searchFieldName: 'companyName'}},
						{header: 'Contact Name', width: 100, dataIndex: 'referenceTwo.nameLabel', filter: {searchFieldName: 'contactName'}},
						{header: 'First Name', width: 75, hidden: true, dataIndex: 'referenceTwo.firstName', filter: {searchFieldName: 'contactName'}},
						{header: 'Last Name', width: 75, hidden: true, dataIndex: 'referenceTwo.lastName', filter: {searchFieldName: 'contactName'}},
						{header: 'Email Address', width: 100, dataIndex: 'referenceTwo.emailAddress', renderer: TCG.renderEmailAddress, filter: {searchFieldName: 'contactEmailAddress'}},
						{header: 'Phone Number', width: 75, dataIndex: 'referenceTwo.phoneNumber', filter: {searchFieldName: 'contactPhoneNumber'}},
						{header: 'Display Order', width: 50, dataIndex: 'displayOrder', type: 'int', useNull: true, hidden: true},
						{header: 'Type Category', width: 65, dataIndex: 'contactType.contactCategory.name', filter: {searchFieldName: 'contactTypeCategoryId', type: 'combo', url: 'businessContactCategoryListFind.json'}, hidden: true},
						{header: 'Contact Type', width: 120, hidden: true, dataIndex: 'contactType.label', filter: {type: 'combo', url: 'businessContactTypeListFind.json', searchFieldName: 'contactTypeId'}},
						{header: 'Functional Role', width: 100, dataIndex: 'functionalRole'},
						{header: 'Title', width: 75, dataIndex: 'referenceTwo.title', filter: {searchFieldName: 'contactTitle'}},
						{header: 'Start Date', width: 65, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 65, dataIndex: 'endDate', hidden: true},
						{header: 'Primary', width: 50, dataIndex: 'primary', type: 'boolean'},
						{
							header: 'Private', width: 50, dataIndex: 'privateRelationship', type: 'boolean',
							tooltip: 'Private relationships are excluded from Client facing reports.'
						},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
					],
					getCompany: function() {
						return TCG.getValue('businessCompany', this.getWindow().getMainForm().formValues);
					},
					getLoadParams: function() {
						return {
							companyId: this.getCompany().id,
							contactTypeName: 'Management Fee Invoice'
						};
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'BillingDefinition',
					showGlobalNoteMenu: true,
					showAttachmentInfo: true,
					showCreateDate: true,
					defaultActiveFilter: false,
					showFilterActiveOnDate: true,

					// Allow the filtering, but don't default it to anything
					getDefaultActiveOnDate: function() {
						return '';
					}
				}]
			},


			{
				title: 'External Billing Basis',
				items: [{
					name: 'billingBasisExternalListFind',
					xtype: 'gridpanel',
					instructions: 'Some billing definition accounts use external (custodian, etc.) values to determine the billing basis.  Use this screen to view/upload External Billing Basis',
					importTableName: 'BillingBasisExternal',
					importComponentName: 'Clifton.billing.invoice.ExternalUploadWindow',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition', width: 150, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.referenceOne.label', filter: {type: 'combo', searchFieldName: 'billingDefinitionId', url: 'billingDefinitionListFind.json', displayField: 'label'}},
						{header: 'Client Account', width: 150, dataIndex: 'billingDefinitionInvestmentAccount.referenceTwo.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Description', width: 100, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisValuationTypeDescription', filter: {searchFieldName: 'billingBasisValuationTypeDescription'}},
						{header: 'Calculation Type', width: 80, dataIndex: 'billingDefinitionInvestmentAccount.billingBasisCalculationTypeLabel', filter: false, sortable: false},
						{header: 'Asset Class', width: 100, dataIndex: 'assetClass.name', filter: {type: 'combo', searchFieldName: 'assetClassId', url: 'investmentAssetClassListFind.json'}},
						{header: 'Date', width: 50, dataIndex: 'date'},
						{header: 'Billing Basis', width: 70, dataIndex: 'billingBasisAmount', type: 'currency', numberFormat: '0,000'},
						{header: 'Billing Fx Rate', width: 70, useNull: true, dataIndex: 'billingFxRate', type: 'float', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.billing.invoice.ExternalWindow',
						getDefaultData: function(gridPanel) {
							return {
								billingDefinitionInvestmentAccount: {referenceOne: gridPanel.getWindow().getMainForm().formValues}
							};
						},
						addEditButtons: function(toolBar, gridPanel) {
							this.addToolbarAddButton(toolBar, gridPanel);
							this.addToolbarDeleteButton(toolBar, gridPanel);

							toolBar.add({
								text: 'Bulk Remove',
								tooltip: 'Remove all External Billing Basis Records for a specific account, date range, and optional asset class',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									TCG.createComponent('Clifton.billing.invoice.ExternalDeleteWindow', {
										openerCt: gridPanel
									});
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of balances
							this.setFilterValue('date', {'after': Clifton.billing.invoice.getDefaultInvoiceStartDate()});
						}
						return {
							billingDefinitionId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Rules',
				items: [{
					xtype: 'rule-assignment-grid-forAdditionalScope',
					scopeTableName: 'BillingDefinition',
					defaultCategoryName: 'Billing Invoice Management Fee Rules'
				}]
			}
		]
	}]
});
