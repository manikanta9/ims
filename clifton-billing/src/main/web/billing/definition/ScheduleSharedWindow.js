TCG.use('Clifton.billing.definition.ScheduleFormBase');

Clifton.billing.definition.ScheduleSharedWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Schedule: Shared',
	width: 1000,
	height: 800,
	iconCls: 'billing',
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				tbar: [
					{
						text: 'Copy Schedule',
						iconCls: 'copy',
						tooltip: 'Create a copy of this schedule and optionally select which billing definitions to include on the new schedule.  Note: All affected billing definitions must be in a Draft State.',
						handler: function() {
							const f = this.ownerCt.ownerCt.items.get(0);
							const w = f.getWindow();
							const id = w.getMainFormId();
							TCG.createComponent('Clifton.billing.definition.ScheduleSharedCopyWindow', {
								defaultData: {copyFromId: id}
							});
						}
					}
				],
				items: [{
					xtype: 'billing-schedule-form',
					beforeScheduleItems: [
						{name: 'template', xtype: 'hidden', value: 'false'},
						{name: 'sharedSchedule', xtype: 'hidden', value: true},
						{name: 'autoApplySharedScheduleToDefinition.id', xtype: 'hidden'},
						{fieldLabel: 'Schedule Name', name: 'name'},
						{fieldLabel: 'Schedule Description', name: 'description', xtype: 'textarea', height: 70}
					],
					afterScheduleItems: [
						{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Do Not Group Invoice', name: 'doNotGroupInvoice', qtip: 'If checked the schedule is evaluated independently for each billing definition as though it is NOT shared.  Leave unchecked to share billing basis across billing definitions and create grouped invoices.'},
						{xtype: 'sectionheaderfield', header: 'Schedule Dates'},
						{xtype: 'label', html: 'Schedule Start/End Dates can be used to apply this schedule to a given time period.  Schedules will be applied to invoices where there is an overlap in the schedule start/end dates with the invoice period start/end dates.'},
						{
							fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
							items: [
								{name: 'startDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 165},
								{name: 'endDate', xtype: 'datefield', flex: 1}
							]
						},
						{xtype: 'sectionheaderfield', header: 'Waive Condition'},
						{xtype: 'label', html: 'Waive condition (if specified) can be used to waive the schedule under specified conditions.'},
						{fieldLabel: 'Waive Condition', name: 'waiveSystemCondition.name', hiddenName: 'waiveSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule'}
					]
				}]
			},


			{
				title: 'Billing Definitions',
				items: [
					{
						xtype: 'gridpanel',
						name: 'billingScheduleSharingListForSchedule',
						instructions: 'This schedule is shared among the following billing definitions. The bulk workflow transition feature can be used to return billing definitions back to draft and final approvals. However, billing definitions that are in Draft can not be bulk moved to Pending Approval because each billing definition requires a note which is not supported in bulk at this time.  Please individually transition each billing definition from Draft state.',
						rowSelectionModel: 'checkbox',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Def ID', tooltip: 'Billing Definition ID', width: 35, dataIndex: 'referenceTwo.id'},
							{header: 'Company', width: 100, dataIndex: 'referenceTwo.businessCompany.name', hidden: true},
							{
								header: 'Contract', width: 200, dataIndex: 'referenceTwo.billingContract.name',
								renderer: function(v, metaData, r) {
									const note = r.data.note;
									if (TCG.isNotBlank(note)) {
										const qtip = r.data.note;
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(qtip);
									}
									return v;
								}
							},
							{
								header: 'Workflow State', width: '60', dataIndex: 'referenceTwo.workflowState.name',
								renderer: function(v, metaData, r) {
									const sts = r.data['workflowStatus.name'];
									if (sts === 'Approved') {
										metaData.css = 'amountPositive';
									}
									else if (sts === 'Draft') {
										metaData.css = 'amountNegative';
									}
									else {
										metaData.css = 'amountAdjusted';
									}
									return v;
								}
							},
							{header: 'Workflow Status', width: '60', dataIndex: 'referenceTwo.workflowStatus.name', hidden: true},
							{header: 'Start Date', width: 40, dataIndex: 'referenceTwo.startDate'},
							{header: 'Second Year Start Date', width: 60, dataIndex: 'referenceTwo.secondYearStartDate', hidden: true},
							{header: 'End Date', width: 40, dataIndex: 'referenceTwo.endDate'},
							{header: 'Active', width: 40, dataIndex: 'referenceTwo.active', type: 'boolean'}
						],
						editor: {
							ptype: 'workflow-transition-grid',
							transitionWindowTitle: 'Transition Billing Definitions',
							transitionWindowInstructions: 'Select Transition for selected billing definition records',
							transitionEntityPath: 'referenceTwo',
							excludeTransitionsWithEntryScreens: true, // Prevent Draft to Pending Approval since a note is required for each billing definition being transitioned
							noTransitionsErrorMessage: 'Could not find any available transitions.<br><br>Note: If you are attempting to transition from Draft to Pending Approval you must transition each billing definition individually as a note is required for each transition.',
							tableName: 'BillingDefinition',
							deleteURL: 'billingScheduleSharingDelete.json',
							detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
							getDetailPageId: function(grid, row) {
								return TCG.getValue('referenceTwo.id', row.json);
							},
							addToolbarAddButton: function(toolBar) {
								const gridPanel = this.getGridPanel();
								toolBar.add(new TCG.form.ComboBox({name: 'billingDefinition', url: 'billingDefinitionListFind.json', displayField: 'label', width: 425, listWidth: 550, detailPageClass: 'Clifton.billing.definition.DefinitionWindow'}));
								toolBar.add({
									text: 'Add',
									tooltip: 'Add Definition to Schedule',
									iconCls: 'add',
									handler: function() {
										const scheduleId = gridPanel.getWindow().getMainFormId();
										const definitionId = TCG.getChildByName(toolBar, 'billingDefinition').getValue();
										if (TCG.isBlank(definitionId)) {
											TCG.showError('You must select a billing definition.');
										}
										else {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Adding...',
												params: {scheduleId: scheduleId, definitionId: definitionId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'billingDefinition').reset();
												}
											});
											loader.load('billingScheduleSharingLink.json');
										}
									}
								});
								toolBar.add('-');
							}
						},
						getLoadParams: function(firstLoad) {
							return {
								scheduleId: this.getWindow().getMainFormId()
							};
						}
					}
				]
			}]
	}]
});
