TCG.use('Clifton.billing.definition.ScheduleFormBase');

Clifton.billing.definition.ScheduleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Schedule',
	width: 1000,
	height: 800,
	iconCls: 'billing',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'billing-schedule-form',
					beforeScheduleItems: [
						{name: 'template', xtype: 'hidden', value: 'false'},
						{name: 'billingDefinition.businessCompany.id', xtype: 'hidden', submitValue: false},
						{fieldLabel: 'Billing Definition', name: 'billingDefinition.label', detailIdField: 'billingDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.billing.definition.DefinitionWindow'},
						{name: 'sharedSchedule', xtype: 'hidden', value: false},
						{fieldLabel: 'Schedule Name', name: 'name'},
						{fieldLabel: 'Schedule Description', name: 'description', xtype: 'textarea', height: 70}
					],

					afterScheduleItems: [
						{xtype: 'sectionheaderfield', header: 'Schedule Dates'},
						{xtype: 'label', html: 'Schedule Start/End Dates can be used to apply this schedule to a given time period.  Schedules will be applied to invoices where there is an overlap in the schedule start/end dates with the invoice period start/end dates.'},
						{
							fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
							items: [
								{name: 'startDate', xtype: 'datefield', flex: 1},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 165},
								{name: 'endDate', xtype: 'datefield', flex: 1}
							]
						},
						{xtype: 'sectionheaderfield', header: 'Account Options'},
						{xtype: 'label', html: 'Account/Account Group selections can be used to have this schedule apply to a specific account(s). For schedules that do not utilize billing basis, the account group selection will process this schedule for <b>each</b> account in the group. For those that do utilize a billing basis, then the schedule will be processed once and will include the billing basis from the accounts that are in the group.  Applying the fee to all accounts for the client will process this schedule for each client account tied to the billing definition.'},
						{
							fieldLabel: 'Client Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', mutuallyExclusiveFields: ['investmentAccountGroup.id', 'applyToEachClientAccount'], detailPageClass: 'Clifton.investment.account.AccountWindow',
							beforequery: function(queryEvent) {
								const clientOrClientRelationshipCompanyId = queryEvent.combo.getParentForm().getForm().findField('billingDefinition.businessCompany.id').value;
								queryEvent.combo.store.baseParams = {
									clientOrClientRelationshipCompanyId: clientOrClientRelationshipCompanyId
								};
							}
						},
						{fieldLabel: 'Client Account Group', name: 'investmentAccountGroup.name', hiddenName: 'investmentAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['investmentAccount.id', 'applyToEachClientAccount'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{
							boxLabel: 'Apply To All Client Accounts', name: 'applyToEachClientAccount', xtype: 'checkbox', mutuallyExclusiveFields: ['investmentAccount.id', 'investmentAccountGroup.id'],
							qtip: 'Applies schedule to each client account tied to the billing definition regardless if the account is explicitly tied to a different schedule.'
						},
						{xtype: 'sectionheaderfield', header: 'Waive Condition'},
						{xtype: 'label', html: 'Waive condition (if specified) can be used to waive the schedule under specified conditions.  If an account group/or client account option is selected, the waive condition will be validated for each account.'},
						{fieldLabel: 'Waive Condition', name: 'waiveSystemCondition.name', hiddenName: 'waiveSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule'}
					]
				}]
			},


			{
				title: 'Definition Dependencies',
				items: [
					{
						xtype: 'gridpanel',
						name: 'billingScheduleBillingDefinitionDependencyListBySchedule',
						instructions: 'This schedule depends on the following Billing Definition(s)/Account(s).',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{
								header: 'Prerequisite Billing Definition', width: 150, dataIndex: 'billingDefinition.label',
								renderer: function(v, metaData, r) {
									const note = r.data.note;
									if (TCG.isNotBlank(note)) {
										const qtip = r.data.note;
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(qtip);
									}
									return v;
								}
							},
							// Using Non-Persistent Field to enable local filtering
							{header: 'Prerequisite Active', dataIndex: 'billingDefinition.active', width: 35, type: 'boolean', tooltip: 'Prerequisite Billing Definition Active', nonPersistentField: true},
							{header: 'Prerequisite Billing Account', width: 150, dataIndex: 'accountLabel'},
							{header: 'Definition Account', width: 100, hidden: true, dataIndex: 'billingDefinitionInvestmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{header: 'Investment Account', width: 100, hidden: true, dataIndex: 'investmentAccount.label', renderer: TCG.renderValueWithTooltip},
							{header: 'Note', width: 100, hidden: true, dataIndex: 'note', renderer: TCG.renderValueWithTooltip}
						],
						editor: {
							detailPageClass: 'Clifton.billing.definition.ScheduleDefinitionDependencyWindow',
							getDefaultData: function(gridPanel) {
								const fv = gridPanel.getWindow().getMainForm().formValues;
								return {
									billingSchedule: fv
								};
							}
						},
						getLoadParams: function(firstLoad) {
							if (TCG.isTrue(firstLoad)) {
								this.setFilterValue('billingDefinition.active', true);
							}
							return {
								scheduleId: this.getWindow().getMainFormId()
							};
						}

					}
				]
			}
		]
	}]
});
