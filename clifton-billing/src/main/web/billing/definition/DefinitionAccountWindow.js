Clifton.billing.definition.DefinitionAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Definition Account',
	width: 850,
	height: 500,
	iconCls: 'billing',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'formpanel',
					url: 'billingDefinitionInvestmentAccount.json',
					labelWidth: 150,
					childTables: 'BillingDefinitionInvestmentAccountSecurityExclusion', // used by audit trail

					getWarningMessage: function(f) {
						const wfStatus = TCG.getValue('referenceOne.workflowStatus.name', f.formValues);
						if (wfStatus === 'Approved') {
							return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Return for Edits</i>.';
						}
						if (wfStatus === 'Pending') {
							return 'This definition is <b>' + wfStatus + '</b> and only limited fields can be edited in this state.  To make additional changes you will need to update the billing definition and <i>Reject</i>, or <i>Approve</i> existing changes and then <i>Return for Edits</i>';
						}

						return undefined;
					},

					listeners: {
						afterload: function(panel) {
							if (this.getFormValue('referenceOne.workflowStatus.name') !== 'Draft') {
								this.setReadOnlyFieldsForApprovedStatus(true);
							}
							else {
								this.setReadOnlyFieldsForApprovedStatus(false);
							}
						}
					},

					// Note: Should stay in sync with "Approved Billing Definition Modify Condition"
					approvedEditableFields: ['billingBasisValuationTypeDescription'],
					setReadOnlyFieldsForApprovedStatus: function(readOnly) {
						this.setReadOnly(readOnly);
						if (TCG.isTrue(readOnly)) {
							for (let i = 0; i < this.approvedEditableFields.length; i++) {
								const fieldName = this.approvedEditableFields[i];
								this.getForm().findField(fieldName).setReadOnly(false);
							}
						}
					},

					items: [
						{fieldLabel: 'Billing Definition', name: 'referenceOne.label', xtype: 'linkfield', detailIdField: 'referenceOne.id', detailPageClass: 'Clifton.billing.definition.DefinitionWindow'},
						{xtype: 'hidden', submitValue: false, name: 'referenceOne.businessCompany.id'},
						{
							fieldLabel: 'Client Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true,
							displayField: 'label',
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								if (TCG.isFalse(f.findField('includeAllAccounts').checked)) {
									queryEvent.combo.store.setBaseParam('clientOrClientRelationshipCompanyId', f.findField('referenceOne.businessCompany.id').getValue());
								}
								else {
									queryEvent.combo.store.setBaseParam('clientOrClientRelationshipCompanyId', null);
								}
							}
						},
						{
							boxLabel: 'Leave unchecked to search selected company\'s client accounts only', xtype: 'checkbox', name: 'includeAllAccounts', submitValue: 'false',
							listeners: {
								check: function(f) {
									// On changes reset contract drop down
									const p = TCG.getParentFormPanel(f);
									const acctField = p.getForm().findField('referenceTwo.label');
									acctField.clearAndReset();

									const mgrField = p.getForm().findField('investmentManagerAccount.label');
									mgrField.clearAndReset();
								}
							}
						},
						{fieldLabel: 'Valuation Type', xtype: 'combo', name: 'billingBasisValuationType.name', hiddenName: 'billingBasisValuationType.id', allowBlank: false, url: 'billingBasisValuationTypeListFind.json'},
						{
							fieldLabel: 'Valuation Type Description', xtype: 'textfield', name: 'billingBasisValuationTypeDescription',
							qtip: 'Can be set for any Valuation Type, but most useful for External Valuation Types to note where these External values are being imported from.'
						},
						{fieldLabel: 'Calculation Type', xtype: 'combo', name: 'billingBasisCalculationType.name', hiddenName: 'billingBasisCalculationType.id', url: 'billingBasisCalculationTypeListFind.json?inactive=false'},
						{fieldLabel: 'Instrument Group', xtype: 'combo', name: 'investmentGroup.name', hiddenName: 'investmentGroup.id', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
						{
							fieldLabel: 'Investment Manager', xtype: 'combo', name: 'investmentManagerAccount.label', hiddenName: 'investmentManagerAccount.id', url: 'investmentManagerAccountListFind.json', detailPageClass: 'Clifton.investment.manager.AccountWindow',
							displayField: 'label',
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								if (TCG.isFalse(f.findField('includeAllAccounts').checked)) {
									queryEvent.combo.store.setBaseParam('clientOrClientRelationshipCompanyId', f.findField('referenceOne.businessCompany.id').getValue());
								}
								else {
									queryEvent.combo.store.setBaseParam('clientOrClientRelationshipCompanyId', null);
								}
							}
						},
						{
							fieldLabel: 'Schedule', xtype: 'combo', name: 'billingSchedule.name', hiddenName: 'billingSchedule.id', url: 'billingScheduleListForDefinition.json', loadAll: true,
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								queryEvent.combo.store.setBaseParam('definitionId', f.findField('referenceOne.id').getValue());
							}
						},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
					]
				}
				]
			},


			{
				title: 'Security Exclusions',
				xtype: 'gridpanel',
				name: 'billingDefinitionInvestmentAccountSecurityExclusionListFind',
				columns: [
					{header: 'Exclusion Note', width: 200, dataIndex: 'exclusionNote'},
					{header: 'Exclude Security', width: 125, dataIndex: 'investmentSecurity.label', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'investmentSecurityId', type: 'combo', url: 'investmentSecurityListFind.json'}},
					{header: 'Exclude Condition', width: 125, dataIndex: 'excludeCondition.name', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'excludeConditionName'}},
					{header: 'Start Date', width: 50, dataIndex: 'startDate', type: 'date'},
					{header: 'End Date', width: 50, dataIndex: 'endDate', type: 'date'}
				],
				getLoadParams: function() {
					return {
						billingDefinitionInvestmentAccountId: this.getWindow().getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.billing.definition.DefinitionAccountSecurityExclusionWindow',
					getDefaultData: function(gridPanel, row) {
						return {
							billingDefinitionInvestmentAccount: gridPanel.getWindow().getMainForm().formValues
						};
					}

				}
			}
		]
	}]
});
