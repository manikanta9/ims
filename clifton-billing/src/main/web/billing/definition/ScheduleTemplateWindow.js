TCG.use('Clifton.billing.definition.ScheduleFormBase');

Clifton.billing.definition.ScheduleTemplateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Billing Schedule Template',
	width: 800,
	height: 600,
	iconCls: 'billing',

	items: [{
		xtype: 'billing-schedule-form',
		beforeScheduleItems: [
			{name: 'template', xtype: 'hidden', value: 'true'},
			// Using Template name to help identify what the template is used for
			{fieldLabel: 'Template Name', name: 'name'},
			// Using Description to default the schedule name
			{fieldLabel: 'Default Schedule Name', name: 'description', maxLength: 50}
		],
		afterScheduleItems: [
			{
				fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
				items: [
					{name: 'startDate', xtype: 'datefield', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 165},
					{name: 'endDate', xtype: 'datefield', flex: 1}
				]
			},
			{
				boxLabel: 'Apply To All Client Accounts', name: 'applyToEachClientAccount', xtype: 'checkbox',
				qtip: 'Applies schedule to each client account tied to the billing definition regardless if the account is explicitly tied to a different schedule. Commonly applies to a standard fee like Account Reporting Fee'
			},
			{xtype: 'sectionheaderfield', header: 'Waive Condition'},
			{xtype: 'label', html: 'Waive condition (if specified) can be used to waive the schedule under specified conditions.'},
			{fieldLabel: 'Waive Condition', name: 'waiveSystemCondition.name', hiddenName: 'waiveSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=BillingSchedule'}
		]
	}]
});
