Clifton.billing.payment.PaymentAllocationEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Billing Payment Allocation Entry',
	width: 900,
	height: 700,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'This screen allows you to apply payments to invoices and specific accounts.',
		getSaveURL: function() {
			return 'billingPaymentAllocationEntrySave.json';
		},
		listeners: {
			afterload: function(formpanel) {
				const form = formpanel.getForm();
				const payment = form.findField('payment');
				if (payment.getValue() && !TCG.isBlank(payment.getValue().inputValue)) {
					payment.fireEvent('change', payment, payment.getValue());
				}

				const applyPaymentTo = form.findField('applyPaymentTo');
				if (applyPaymentTo.getValue() && !TCG.isBlank(applyPaymentTo.getValue().inputValue)) {
					applyPaymentTo.fireEvent('change', applyPaymentTo, applyPaymentTo.getValue());
				}
				// Displayed by default so just hide it if not being triggered by radio group field
				else {
					formpanel.showHideFormFragment('applyPaymentToAccount', false);
				}
			}
		},

		items: [
			{xtype: 'sectionheaderfield', header: 'Payment'},
			{
				xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'payment', allowBlank: false, submitValue: false,
				items: [
					{boxLabel: 'Use Existing Payment', xtype: 'radio', name: 'payment', inputValue: 'existing'},
					{boxLabel: 'Add New Payment', xtype: 'radio', name: 'payment', inputValue: 'new'}
				],
				listeners: {
					change: (radioGroup, checkedItem) => {
						const formPanel = TCG.getParentFormPanel(radioGroup);
						// Show correct fields
						switch (checkedItem && checkedItem.inputValue) {
							case 'existing':
								formPanel.showHideFormFragment('existingPayment', true);
								formPanel.showHideFormFragment('newPayment', false);
								break;
							case 'new':
								// Must clear existing selection
								formPanel.getForm().findField('existingBillingPayment.unallocatedLabel').clearAndReset();
								formPanel.showHideFormFragment('existingPayment', false);
								formPanel.showHideFormFragment('newPayment', true);
								break;
							default:
							// Do nothing
						}
					}
				}
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'existingPayment',
				hidden: true,
				items: [
					{fieldLabel: 'Payment', name: 'existingBillingPayment.unallocatedLabel', hiddenName: 'existingBillingPayment.id', xtype: 'combo', url: 'billingPaymentListFind.json?fullyAllocated=false', detailPageClass: 'Clifton.billing.payment.PaymentWindow', displayField: 'unallocatedLabel'}
				]
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'newPayment',
				hidden: true,
				items: [
					{fieldLabel: 'Company', name: 'newBillingPayment.businessCompany.nameWithType', hiddenName: 'newBillingPayment.businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, displayField: 'nameWithType'},
					{fieldLabel: 'Payment Date', name: 'newBillingPayment.paymentDate', xtype: 'datefield', allowBlank: false, value: TCG.getPreviousWeekday().format('m/d/Y')},
					{fieldLabel: 'Payment Type', name: 'newBillingPayment.paymentType', xtype: 'system-list-combo', listName: 'Billing Invoice Payment Types'},
					{fieldLabel: 'Payment Amount', name: 'newBillingPayment.paymentAmount', xtype: 'currencyfield', allowBlank: false},
					{fieldLabel: 'Notes', name: 'notes', xtype: 'textarea', height: 45}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Invoice Allocation'},
			{
				xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'applyPaymentTo', allowBlank: false, submitValue: false,
				items: [
					{boxLabel: 'Apply Payment To Invoice', xtype: 'radio', name: 'applyPaymentTo', inputValue: 'invoice'},
					{boxLabel: 'Apply Payment Specifically To Accounts', xtype: 'radio', name: 'applyPaymentTo', inputValue: 'account'}
				],
				listeners: {
					change: (radioGroup, checkedItem) => {
						const formPanel = TCG.getParentFormPanel(radioGroup);
						// Show correct fields
						switch (checkedItem && checkedItem.inputValue) {
							case 'invoice':
								formPanel.showHideFormFragment('applyPaymentToInvoice', true);
								formPanel.getForm().findField('applyToBillingInvoice.labelUnpaidBalance').allowBlank = false;
								formPanel.showHideFormFragment('applyPaymentToFormFragment', true);
								formPanel.getForm().findField('autoApplyAmounts').setValue(true);
								formPanel.getForm().findField('autoApplyAmounts').setDisabled(true);
								formPanel.showHideFormFragment('applyPaymentToAccount', false);
								break;
							case 'account':
								// Must clear existing selection
								formPanel.getForm().findField('applyToBillingInvoice.labelUnpaidBalance').clearAndReset();
								formPanel.getForm().findField('applyToBillingInvoice.labelUnpaidBalance').allowBlank = true;
								formPanel.showHideFormFragment('applyPaymentToFormFragment', true);
								formPanel.getForm().findField('autoApplyAmounts').setValue(false);
								formPanel.getForm().findField('autoApplyAmounts').setDisabled(false);
								formPanel.showHideFormFragment('applyPaymentToInvoice', false);
								formPanel.showHideFormFragment('applyPaymentToAccount', true);
								break;
							default:
							// Do nothing
						}
					}
				}
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'applyPaymentToInvoice',
				hidden: true,
				items: [
					{
						fieldLabel: 'Invoice', name: 'applyToBillingInvoice.labelUnpaidBalance', hiddenName: 'applyToBillingInvoice.id', xtype: 'combo', url: 'billingInvoiceListFind.json', detailPageClass: 'Clifton.billing.invoice.InvoiceWindow', displayField: 'labelUnpaidBalance', listWidth: 900,
						beforequery: function(queryEvent) {
							queryEvent.combo.store.setBaseParam('excludeWorkflowStatusNames', ['Cancelled', 'Closed']);

						}
					}
				]
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'applyPaymentToFormFragment',
				hidden: true,
				items: [
					{
						boxLabel: 'System Calculated Allocation Amounts', xtype: 'checkbox', name: 'autoApplyAmounts',
						listeners: {
							'check': function(f, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(f);
								const gp = TCG.getChildByName(fp, 'billingPaymentAllocationGrid');
								const cm = gp.getColumnModel();
								cm.setHidden(cm.findColumnIndex('allocatedAmount'), TCG.isTrue(newValue));
							}
						}
					},
					{fieldLabel: 'Allocation Amount', xtype: 'currencyfield', requiredFields: ['autoApplyAmounts'], name: 'allocationAmount', qtip: 'Total amount to allocate.  Leave blank to allocate maximum available.'}
				]
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'applyPaymentToAccount',
				hidden: false,  // the form grid must be rendered or else the submission doesn't act right
				items: [

					{
						xtype: 'formgrid',
						title: 'Account Allocations',
						name: 'billingPaymentAllocationGrid',
						storeRoot: 'billingPaymentAllocationList',
						dtoClass: 'com.clifton.billing.payment.BillingPaymentAllocation',

						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({
								name: 'paymentAccount', displayField: 'unpaidLabel', url: 'billingPaymentInvestmentAccountAllocationListFind.json', width: 400, listWidth: 700, submitValue: false, doNotSubmitValue: true,
								requestedProps: 'billingInvoice.id|investmentAccount.id|investmentAccount.label|unpaidAmount',
								beforequery: function(queryEvent) {
									queryEvent.combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'NOT_EQUALS', value: 0, field: 'unpaidAmount'}]));
								},
								listeners: {
									// reset invoice and account id
									select: function(combo, record, index) {
										toolBar.addDefaultData = {'billingInvoice.id': record.json.billingInvoice.id, 'investmentAccount.id': record.json.investmentAccount.id, 'investmentAccount.label': record.json.investmentAccount.label, 'unpaidAmount': record.json.unpaidAmount};
									}
								}
							}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								scope: this,
								handler: function() {
									if (TCG.isBlank(toolBar.addDefaultData)) {
										TCG.showError('You must first select desired unpaid invoice / account to allocate to.');
										return;
									}

									const store = this.getStore();
									const RowClass = store.recordType;
									this.stopEditing();
									const record = new RowClass(toolBar.addDefaultData);
									store.insert(0, record);
									this.startEditing(0, 0);
									record.set('allocatedAmount', record.get('unpaidAmount'));
									this.markModified();
								}
							});
						},
						alwaysSubmitFields: ['billingInvoice.id', 'investmentAccount.id', 'allocatedAmount'],
						doNotSubmitFields: ['unpaidAmount'],

						columnsConfig: [
							{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
							{header: 'Invoice #', width: 100, dataIndex: 'billingInvoice.id'},
							{header: 'Invoice #', width: 50, dataIndex: 'investmentAccount.id', hidden: true},
							{header: 'Account', width: 350, dataIndex: 'investmentAccount.label'},
							{header: 'Unpaid Amount', dataIndex: 'unpaidAmount', width: 150, type: 'currency', summaryType: 'sum'},
							{header: 'Allocation Amount', dataIndex: 'allocatedAmount', width: 150, type: 'currency', editor: {xtype: 'currencyfield'}, summaryType: 'sum'}

						],
						plugins: {ptype: 'gridsummary'}
					}
				]
			}
		],

		showHideFormFragment: function(formFragmentName, show) {
			const formPanel = this;
			TCG.getChildByName(formPanel, formFragmentName).setVisible(show);
			TCG.getChildByName(formPanel, formFragmentName).items.items.forEach(function(fld) {
				// Don't auto enable if it uses required fields
				if (!show || TCG.isBlank(fld.requiredFields)) {
					fld.setDisabled(!show);
				}
			});
		}
	}]
});
