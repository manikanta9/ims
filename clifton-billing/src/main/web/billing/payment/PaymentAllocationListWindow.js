Clifton.billing.payment.PaymentAllocationListWindow = Ext.extend(TCG.app.Window, {
	title: 'Payment Allocation Drill Down',
	iconCls: 'billing',
	width: 700,
	height: 300,

	items: [{
		xtype: 'gridpanel',
		name: 'billingPaymentAllocationListFind',
		instructions: 'The following payment allocations make up the invoice account billing payments.',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Payment #', width: 75, dataIndex: 'billingPayment.id'},
			{header: 'Payment Date', width: 75, dataIndex: 'billingPayment.paymentDate'},
			{header: 'Payment Amount', width: 100, dataIndex: 'billingPayment.paymentAmount', type: 'currency'},
			{header: 'Allocated Amount', width: 100, dataIndex: 'allocatedAmount', type: 'currency', summaryType: 'sum'}
		],
		plugins: {ptype: 'gridsummary'},
		getLoadParams: function() {
			const data = this.getWindow().defaultData;
			if (data) {
				return {
					billingInvoiceId: data.billingInvoiceId,
					investmentAccountId: data.investmentAccountId
				};
			}
			return false;
		},
		editor: {
			drillDownOnly: true,
			isDeleteEnabled: function() {
				return true;
			},
			detailPageClass: 'Clifton.billing.payment.PaymentWindow',
			getDetailPageId: function(gridPanel, row) {
				return row.json.billingPayment.id;
			}
		}

	}]
});
