Clifton.billing.payment.PaymentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Payment',
	iconCls: 'billing',
	width: 1000,
	height: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Payment Info',
				layout: 'border',
				items: [
					{
						xtype: 'formpanel',
						region: 'north',
						height: 340,
						instructions: 'A payment is money received from a client or credit that can be applied to a client for services rendered.  Payments are allocated to invoices.',
						url: 'billingPayment.json',
						labelWidth: 120,
						labelFieldName: 'id',
						getWarningMessage: function(form) {
							let msg = undefined;
							if (TCG.isTrue(form.formValues.voided)) {
								msg = 'This payment has been <b>voided</b> and can not be allocated to any invoices.';
							}
							return msg;
						},
						items: [
							{fieldLabel: 'Company', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
							{
								fieldLabel: 'Payment Date', xtype: 'compositefield',
								items: [
									{name: 'paymentDate', xtype: 'datefield', flex: 1, value: TCG.getPreviousWeekday().format('m/d/Y')},
									{xtype: 'displayfield', width: 50},
									{value: 'Payment Amount:', xtype: 'displayfield', width: 120},
									{name: 'paymentAmount', xtype: 'currencyfield', flex: 1}
								]
							},
							{
								fieldLabel: 'Payment Type', xtype: 'compositefield',
								items: [
									{name: 'paymentType', xtype: 'system-list-combo', flex: 1, listName: 'Billing Invoice Payment Types'},
									{xtype: 'displayfield', width: 50},
									{value: 'Allocated Amount:', xtype: 'displayfield', width: 120},
									{name: 'allocatedAmount', xtype: 'currencyfield', flex: 1, readOnly: true, submitValue: false}
								]
							},
							{
								fieldLabel: '', xtype: 'compositefield', labelSeparator: '',
								items: [
									{flex: 1},
									{xtype: 'displayfield', width: 50},
									{value: 'Available Amount:', xtype: 'displayfield', width: 120},
									{name: 'availableAmount', xtype: 'currencyfield', flex: 1, readOnly: true}
								]
							},
							{fieldLabel: 'Notes', name: 'notes', xtype: 'textarea', height: 45},
							{
								title: 'File Attachments',
								xtype: 'documentFileGrid-simple',
								definitionName: 'BillingPaymentAttachments',
								hideDateFields: true
							}
						]
					},

					{
						region: 'center',
						xtype: 'gridpanel',
						title: 'Payment Allocations',
						name: 'billingPaymentAllocationListFind',
						instructions: 'This payment is allocated to the following invoices / accounts for this client.',
						listeners: {
							afterRender: function() {
								const grid = this;
								const fp = this.getWindow().getMainFormPanel();
								fp.on('afterload', function(fp) {
									if (TCG.isTrue(TCG.getValue('voided', fp.getForm().formValues))) {
										this.setVisible(false);
									}
									else {
										grid.reload();
									}
								}, this);
							}
						},
						groupField: 'billingInvoice.labelShort',
						groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Accounts" : "Account"]}',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Invoice Label', width: 200, dataIndex: 'billingInvoice.labelShort', hidden: true},
							{header: 'Invoice Date', width: 50, dataIndex: 'billingInvoice.invoiceDate'},
							{header: 'Invoice #', width: 70, dataIndex: 'billingInvoice.id', numberFormat: '0000', hidden: true},
							{
								header: 'Billing Definition', width: 250, dataIndex: 'billingInvoice.billingDefinition.businessCompany.name', hidden: true,
								renderer: function(v, metaData, r) {
									const note = r.data['billingInvoice.billingDefinition.note'];
									if (TCG.isNotBlank(note)) {
										const qtip = r.data['billingInvoice.billingDefinition.note'];
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(qtip);
									}
									return v;
								}
							},
							{header: 'Billing Definition Note', hidden: true, width: 200, dataIndex: 'billingInvoice.billingDefinition.note'},
							{header: 'Account', width: 200, dataIndex: 'investmentAccount.label', idDataIndex: 'investmentAccount.id'},
							{header: 'Invoice Amount', width: 100, dataIndex: 'billingInvoice.totalAmount', type: 'currency', numberFormat: '0,000.00', hidden: true},
							{header: 'Allocated Amount', width: 100, dataIndex: 'allocatedAmount', type: 'currency', numberFormat: '0,000.00', summaryType: 'sum'},
							{header: 'Invoice Balance', width: 100, dataIndex: 'billingInvoice.unpaidAmount', type: 'currency', numberFormat: '0,000.00', hidden: true}
						],
						plugins: {ptype: 'gridsummary'},
						getLoadParams: function() {
							const w = this.getWindow();
							if (w.isMainFormSaved()) {
								return {billingPaymentId: w.getMainFormId()};
							}
							return false;
						},
						editor: {
							detailPageClass: 'Clifton.billing.invoice.InvoiceWindow',
							getDetailPageId: function(gridPanel, row) {
								return row.json.billingInvoice.id;
							},
							addToolbarAddButton: function(toolBar) {
								const gridPanel = this.getGridPanel();
								toolBar.add({
									text: 'Add Allocation',
									tooltip: 'Add allocation of this payment to an invoice or account(s).',
									iconCls: 'add',
									handler: function() {
										const w = gridPanel.getWindow();
										if (!w.isMainFormSaved()) {
											TCG.showError('Please save the payment before adding allocations.');
											return false;
										}

										const defaultData = {};
										defaultData.payment = 'existing';
										defaultData.existingBillingPayment = gridPanel.getWindow().getMainForm().formValues;

										TCG.createComponent('Clifton.billing.payment.PaymentAllocationEntryWindow', {
											defaultData: defaultData,
											defaultDataIsReal: true,
											openerCt: gridPanel
										});
									}
								});
								toolBar.add('-');
							}
						},
						addToolbarButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Remove All',
								tooltip: 'Remove ALL allocations for this payment',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									Ext.Msg.confirm('Delete All Allocations', 'Would you like to delete all allocations for this payment?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Removing...',
												params: {paymentId: gridPanel.getWindow().getMainFormId()},
												onLoad: function(record, conf) {
													gridPanel.reload();
												}
											});
											loader.load('billingPaymentAllocationListForPaymentDelete.json');
										}
									});
								}
							});
							toolBar.add('-');
							const uploadButton = toolBar.add({
								text: 'Upload',
								iconCls: 'import',
								tooltip: 'Upload payment allocation. Required columns are: allocatedAmount, clientAccountNumber, invoiceDate',
								scope: this,
								handler: function() {

									TCG.createComponent('Clifton.billing.payment.upload.BillingPaymentAllocationUploadWindow', {
										defaultData: {
											openerCt: gridPanel,
											billingPaymentId: gridPanel.getWindow().getMainFormId()
										}
									});
								}
							});
							toolBar.add('-');
							TCG.file.enableDD(TCG.getParentByClass(uploadButton, Ext.Panel), null, gridPanel, 'billingPaymentAllocationFileUpload.json?billingPaymentId=' + gridPanel.getWindow().getMainFormId(), true);
							toolBar.add({
								text: 'Sample File',
								iconCls: 'excel',
								tooltip: 'Download Excel Sample File for Custom Billing Payment Allocation.',
								handler: function() {
									TCG.openFile('billing/payment/BillingPaymentAllocationUploadSample.xls');
								}
							});
						}
					}
				]
			},

			{
				title: 'Company Payment History',
				name: 'billingPaymentListFind',
				xtype: 'billing-paymentGrid',
				instructions: 'The following payments have been received by the selected company from this payment.',
				showAllocationInfo: true,
				showVoidedColumn: true,
				showClientInfo: false,
				getLoadParams: function(firstLoad) {
					return {
						includeVoided: true,
						companyId: TCG.getValue('businessCompany.id', this.getWindow().getMainForm().formValues)
					};
				}
			}
		]
	}]
});
