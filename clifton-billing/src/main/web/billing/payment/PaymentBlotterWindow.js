Clifton.billing.payment.PaymentBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'billingPaymentBlotterWindow',
	title: 'Payment Blotter',
	iconCls: 'billing',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,

		items: [
			{
				title: 'All Payments',
				items: [{
					name: 'billingPaymentListFind',
					xtype: 'billing-paymentGrid',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of payments
							this.setFilterValue('paymentDate', {'after': new Date().add(Date.DAY, -30)});
						}
					}
				}]
			},


			{
				title: 'Unallocated Payments',
				name: 'billingPaymentListFind',
				xtype: 'billing-paymentGrid',
				instructions: 'The following payments have been received but have not been fully allocated to invoices.',
				showAllocationInfo: true,
				getLoadParams: function(firstLoad) {
					return {
						fullyAllocated: false
					};
				}
			},


			{
				title: 'Account Payment Allocations',
				xtype: 'billing-payment-account-allocation-grid'
			},


			{
				title: 'Voided Payments',
				items: [{
					name: 'billingPaymentListFind',
					xtype: 'billing-paymentGrid',
					instructions: 'The following payments were entered and then voided from the system.  Payments cannot be deleted, only voided, and voided payments cannot be allocated to invoices.',
					editor: {
						detailPageClass: 'Clifton.billing.payment.PaymentWindow',
						drillDownOnly: true
					},
					showVoidedColumn: false,
					getLoadParams: function(firstLoad) {
						this.setFilterValue('voided', true);
						return {'includeVoided': true};
					}
				}]
			}
		]
	}]
});





