package com.clifton.billing.billingbasis.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author Mary Anderson
 */
public class BillingBasisSnapshotSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "billingDefinitionInvestmentAccount.id")
	private Integer billingDefinitionInvestmentAccountId;

	@SearchField(searchField = "billingDefinitionInvestmentAccount.id")
	private Integer[] billingDefinitionInvestmentAccountIds;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceTwo", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.billingBasisValuationType", searchField = "name")
	private String valuationTypeName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "billingBasisValuationType.id")
	private Short[] valuationTypeIds;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.billingBasisValuationType", searchField = "sourceTable.id")
	private Short sourceTableId;

	@SearchField
	private Integer sourceFkFieldId;

	@SearchField(searchField = "sourceFkFieldId", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullSourceFkFieldId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.investmentGroup", searchField = "name")
	private String investmentGroupName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "investmentManagerAccount.id", sortField = "investmentManagerAccount.accountNumber")
	private Integer investmentManagerAccountId;

	@SearchField
	private Date snapshotDate;

	@SearchField(searchFieldPath = "assetClass", searchField = "name")
	private String assetClassName;

	@SearchField(searchFieldPath = "security", searchField = "symbol,name", sortField = "symbol")
	private String securityLabel;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal securityPrice;

	@SearchField
	private BigDecimal fxRate;

	@SearchField
	private BigDecimal snapshotValue;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "referenceOne.id")
	private Integer billingDefinitionId;

	@SearchField(searchField = "billingInvoice.id")
	private Integer billingInvoiceId;

	@SearchField(searchFieldPath = "billingInvoice", searchField = "invoiceDate")
	private Date invoiceDate;

	@SearchField(searchFieldPath = "billingInvoice.workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeInvoiceWorkflowStatusName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionInvestmentAccountId() {
		return this.billingDefinitionInvestmentAccountId;
	}


	public void setBillingDefinitionInvestmentAccountId(Integer billingDefinitionInvestmentAccountId) {
		this.billingDefinitionInvestmentAccountId = billingDefinitionInvestmentAccountId;
	}


	public Integer[] getBillingDefinitionInvestmentAccountIds() {
		return this.billingDefinitionInvestmentAccountIds;
	}


	public void setBillingDefinitionInvestmentAccountIds(Integer[] billingDefinitionInvestmentAccountIds) {
		this.billingDefinitionInvestmentAccountIds = billingDefinitionInvestmentAccountIds;
	}


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public Integer getBillingInvoiceId() {
		return this.billingInvoiceId;
	}


	public void setBillingInvoiceId(Integer billingInvoiceId) {
		this.billingInvoiceId = billingInvoiceId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getValuationTypeName() {
		return this.valuationTypeName;
	}


	public void setValuationTypeName(String valuationTypeName) {
		this.valuationTypeName = valuationTypeName;
	}


	public Short getSourceTableId() {
		return this.sourceTableId;
	}


	public void setSourceTableId(Short sourceTableId) {
		this.sourceTableId = sourceTableId;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public void setInvestmentGroupName(String investmentGroupName) {
		this.investmentGroupName = investmentGroupName;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public String getSecurityLabel() {
		return this.securityLabel;
	}


	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getSnapshotValue() {
		return this.snapshotValue;
	}


	public void setSnapshotValue(BigDecimal snapshotValue) {
		this.snapshotValue = snapshotValue;
	}


	public Boolean getNullSourceFkFieldId() {
		return this.nullSourceFkFieldId;
	}


	public void setNullSourceFkFieldId(Boolean nullSourceFkFieldId) {
		this.nullSourceFkFieldId = nullSourceFkFieldId;
	}


	public Integer getInvestmentManagerAccountId() {
		return this.investmentManagerAccountId;
	}


	public void setInvestmentManagerAccountId(Integer investmentManagerAccountId) {
		this.investmentManagerAccountId = investmentManagerAccountId;
	}


	public Short[] getValuationTypeIds() {
		return this.valuationTypeIds;
	}


	public void setValuationTypeIds(Short[] valuationTypeIds) {
		this.valuationTypeIds = valuationTypeIds;
	}


	public String getExcludeInvoiceWorkflowStatusName() {
		return this.excludeInvoiceWorkflowStatusName;
	}


	public void setExcludeInvoiceWorkflowStatusName(String excludeInvoiceWorkflowStatusName) {
		this.excludeInvoiceWorkflowStatusName = excludeInvoiceWorkflowStatusName;
	}
}
