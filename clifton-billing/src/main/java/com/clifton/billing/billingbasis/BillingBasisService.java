package com.clifton.billing.billingbasis;


import com.clifton.billing.billingbasis.search.BillingBasisAggregationTypeSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisCalculationTypeSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisExternalSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisValuationTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.Date;
import java.util.List;


public interface BillingBasisService {

	////////////////////////////////////////////////////////////////////////////
	////////            Billing Basis Calculation Type Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public BillingBasisCalculationType getBillingBasisCalculationType(short id);


	public List<BillingBasisCalculationType> getBillingBasisCalculationTypeList(BillingBasisCalculationTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////            Billing Basis Valuation Type Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public BillingBasisValuationType getBillingBasisValuationType(short id);


	public List<BillingBasisValuationType> getBillingBasisValuationTypeList(BillingBasisValuationTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////            Billing Basis Aggregation Type Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public BillingBasisAggregationType getBillingBasisAggregationType(short id);


	public List<BillingBasisAggregationType> getBillingBasisAggregationTypeList(BillingBasisAggregationTypeSearchForm searchForm);


	public BillingBasisAggregationType saveBillingBasisAggregationType(BillingBasisAggregationType bean);


	public void deleteBillingBasisAggregationType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////////            Billing Basis External Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingBasisExternal getBillingBasisExternal(int id);


	/**
	 * Currently used for uploads only to get existing record for UX properties on the external record
	 */
	@DoNotAddRequestMapping
	public BillingBasisExternal getBillingBasisExternalExisting(BillingBasisExternal bean);


	public List<BillingBasisExternal> getBillingBasisExternalList(BillingBasisExternalSearchForm searchForm);


	public BillingBasisExternal saveBillingBasisExternal(BillingBasisExternal bean);


	public void saveBillingBasisExternalList(List<BillingBasisExternal> list);


	public void deleteBillingBasisExternal(int id);


	/**
	 * Used to delete a range of external values.  Requires a billingDefinitionInvestmentAccount, start date and end date
	 * Helpful if values are accidentally uploaded to the wrong account, they can be deleted in one action rather than deleting each record for each day of the invoice period
	 */
	public void deleteBillingBasisExternalList(BillingBasisExternalSearchForm searchForm, Date startDate, Date endDate);


	////////////////////////////////////////////////////////////////////////////
	/////////             Billing Basis Snapshot Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public List<BillingBasisSnapshot> getBillingBasisSnapshotList(BillingBasisSnapshotSearchForm searchForm);


	public List<BillingBasisSnapshot> getBillingBasisSnapshotListForInvoice(int billingInvoiceId);


	public List<BillingBasisSnapshot> getBillingBasisSnapshotList(int billingInvoiceId, int billingDefinitionInvestmentAccountId);


	public void saveBillingBasisSnapshotList(List<BillingBasisSnapshot> list);


	public void deleteBillingBasisSnapshotListForInvoice(int billingInvoiceId);
}
