package com.clifton.billing.billingbasis;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>BillingBasisCalculationType</code> defines how the billing basis is calculated from the valuation details.
 * Examples: Period Average, Period End, Period Start, Period Trade Dates
 *
 * @author manderson
 */
public class BillingBasisCalculationType extends NamedEntity<Short> {

	private SystemBean calculatorBean;

	/**
	 * If billing basis can be split into date specific ranges, i.e. Period Trade Dates
	 * If true, then can't be used for an in advance billing definition
	 * This breaks the billing invoice detail into multiple lines split across date ranges when trades occurred.
	 */
	private boolean dateRangeSpecific;

	/**
	 * Whether or not the calculation type can be selected.  Currently used to flag Period Average Projected
	 * as inactive so users can't select it on screen, but it's used each month for projected revenue.
	 */
	private boolean inactive;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBean getCalculatorBean() {
		return this.calculatorBean;
	}


	public void setCalculatorBean(SystemBean calculatorBean) {
		this.calculatorBean = calculatorBean;
	}


	public boolean isDateRangeSpecific() {
		return this.dateRangeSpecific;
	}


	public void setDateRangeSpecific(boolean dateRangeSpecific) {
		this.dateRangeSpecific = dateRangeSpecific;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}
}
