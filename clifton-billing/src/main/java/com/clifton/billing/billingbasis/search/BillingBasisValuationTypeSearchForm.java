package com.clifton.billing.billingbasis.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class BillingBasisValuationTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String label;

	@SearchField
	private String description;

	@SearchField(searchFieldPath = "calculatorBean", searchField = "name")
	private String calculatorBeanName;

	@SearchField(searchFieldPath = "sourceTable", searchField = "name")
	private String sourceTableName;

	@SearchField
	private Boolean investmentGroupAllowed;

	@SearchField
	private Boolean investmentManagerRequired;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCalculatorBeanName() {
		return this.calculatorBeanName;
	}


	public void setCalculatorBeanName(String calculatorBeanName) {
		this.calculatorBeanName = calculatorBeanName;
	}


	public String getSourceTableName() {
		return this.sourceTableName;
	}


	public void setSourceTableName(String sourceTableName) {
		this.sourceTableName = sourceTableName;
	}


	public Boolean getInvestmentGroupAllowed() {
		return this.investmentGroupAllowed;
	}


	public void setInvestmentGroupAllowed(Boolean investmentGroupAllowed) {
		this.investmentGroupAllowed = investmentGroupAllowed;
	}


	public Boolean getInvestmentManagerRequired() {
		return this.investmentManagerRequired;
	}


	public void setInvestmentManagerRequired(Boolean investmentManagerRequired) {
		this.investmentManagerRequired = investmentManagerRequired;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
