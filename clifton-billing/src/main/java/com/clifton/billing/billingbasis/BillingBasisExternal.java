package com.clifton.billing.billingbasis;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.setup.InvestmentAssetClass;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingBasisExternal</code> is the billing basis details uploaded or manually inserted into the system by a user.  These are values
 * that cannot be calculated by the system, and most often apply to cases where we bill based on the custodian data, not ours.
 *
 * @author Mary Anderson
 */
public class BillingBasisExternal extends BaseEntity<Integer> {

	public static final String BILLING_BASIS_EXTERNAL_TABLE_NAME = "BillingBasisExternal";

	////////////////////////////////////////////////////////////////////////////////

	private BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount;

	private Date date;

	private BigDecimal billingBasisAmount;

	/**
	 * Optional asset class - used for reporting/breakdown of external values
	 */
	private InvestmentAssetClass assetClass;

	/**
	 * Billing Basis is stored in the Base Currency of the Client Account.
	 * <p>
	 * Sometimes, the bills we generate are in a different currency that the base currency of the account.
	 * Currently, the only Client that uses this feature is Kodak Canada and they happen to use an External Billing Basis.
	 * The agreed upon FX rate to use is pulled by Kelly from Bloomberg and a screen shot of the FX rate is sent as supporting
	 * details to the client.  In this case, Kelly needs to be able to manually enter the Billing FX rate to use when she enters
	 * the value so it matches Bloomberg.  If and when we have cases that we don't use External or External is missing an explicit FX Rate
	 * the system would use current logic to pull FX rate from Market data. (Tries Datasource selected on the client account else Goldman by default)
	 * <p>
	 * IF the billing currency and client account base currency are the same this field does not apply
	 */
	private BigDecimal billingFxRate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isBillingFxRateSupported() {
		if (getBillingDefinitionInvestmentAccount() != null) {
			return !getBillingDefinitionInvestmentAccount().getReferenceOne().getBillingCurrency().equals(getBillingDefinitionInvestmentAccount().getReferenceTwo().getBaseCurrency());
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount() {
		return this.billingDefinitionInvestmentAccount;
	}


	public void setBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		this.billingDefinitionInvestmentAccount = billingDefinitionInvestmentAccount;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public BigDecimal getBillingBasisAmount() {
		return this.billingBasisAmount;
	}


	public void setBillingBasisAmount(BigDecimal billingBasisAmount) {
		this.billingBasisAmount = billingBasisAmount;
	}


	public InvestmentAssetClass getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(InvestmentAssetClass assetClass) {
		this.assetClass = assetClass;
	}


	public BigDecimal getBillingFxRate() {
		return this.billingFxRate;
	}


	public void setBillingFxRate(BigDecimal billingFxRate) {
		this.billingFxRate = billingFxRate;
	}
}
