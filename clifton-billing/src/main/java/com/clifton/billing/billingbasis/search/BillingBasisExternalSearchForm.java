package com.clifton.billing.billingbasis.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class BillingBasisExternalSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "billingDefinitionInvestmentAccount.id")
	private Integer billingDefinitionInvestmentAccountId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "referenceOne.id")
	private Integer billingDefinitionId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceOne.invoiceType", searchField = "name")
	private String invoiceTypeName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceOne", searchField = "invoiceType.id", sortField = "invoiceType.name")
	private Short invoiceTypeId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceTwo.businessClient.clientRelationship", searchField = "name,legalName", sortField = "name")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceTwo.businessClient", searchField = "name,legalName", sortField = "name")
	private String clientName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "referenceTwo.id", sortField = "referenceTwo.number")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceTwo", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "billingBasisValuationTypeDescription")
	private String billingBasisValuationTypeDescription;

	@SearchField
	private Date date;

	@SearchField
	private BigDecimal billingBasisAmount;

	@SearchField
	private BigDecimal billingFxRate;

	@SearchField(searchField = "assetClass.id")
	private Short assetClassId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionInvestmentAccountId() {
		return this.billingDefinitionInvestmentAccountId;
	}


	public void setBillingDefinitionInvestmentAccountId(Integer billingDefinitionInvestmentAccountId) {
		this.billingDefinitionInvestmentAccountId = billingDefinitionInvestmentAccountId;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public BigDecimal getBillingBasisAmount() {
		return this.billingBasisAmount;
	}


	public void setBillingBasisAmount(BigDecimal billingBasisAmount) {
		this.billingBasisAmount = billingBasisAmount;
	}


	public Short getAssetClassId() {
		return this.assetClassId;
	}


	public void setAssetClassId(Short assetClassId) {
		this.assetClassId = assetClassId;
	}


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public BigDecimal getBillingFxRate() {
		return this.billingFxRate;
	}


	public void setBillingFxRate(BigDecimal billingFxRate) {
		this.billingFxRate = billingFxRate;
	}


	public String getBillingBasisValuationTypeDescription() {
		return this.billingBasisValuationTypeDescription;
	}


	public void setBillingBasisValuationTypeDescription(String billingBasisValuationTypeDescription) {
		this.billingBasisValuationTypeDescription = billingBasisValuationTypeDescription;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public String getInvoiceTypeName() {
		return this.invoiceTypeName;
	}


	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}
}
