package com.clifton.billing.billingbasis.upload;

import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>BillingBasisExternalUploadCommand</code> is used to upload external billing basis.
 * <p>
 * For convenience the billing definition investment account can be selected on screen
 *
 * @author manderson
 */
public class BillingBasisExternalUploadCommand extends SystemUploadCommand {


	/**
	 * The {@link com.clifton.billing.definition.BillingDefinitionInvestmentAccount} the external billing basis is being upload for.
	 * The valuation type on that account setup must be External
	 */
	private Integer billingDefinitionInvestmentAccountId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionInvestmentAccountId() {
		return this.billingDefinitionInvestmentAccountId;
	}


	public void setBillingDefinitionInvestmentAccountId(Integer billingDefinitionInvestmentAccountId) {
		this.billingDefinitionInvestmentAccountId = billingDefinitionInvestmentAccountId;
	}
}
