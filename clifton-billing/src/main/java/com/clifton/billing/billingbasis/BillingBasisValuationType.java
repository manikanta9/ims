package com.clifton.billing.billingbasis;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>BillingBasisValuationType</code> defines how the billing basis details are retrieved/calculated.
 * i.e. Cash Market Value, Position Market Value, Notional, External, Synthetic Exposure
 *
 * @author manderson
 */
public class BillingBasisValuationType extends NamedEntity<Short> {

	private SystemBean calculatorBean;

	/**
	 * Source table associated with this valuation.  Not used for calculations that pull values from snapshots as we do not
	 * create a hard link to those tables, however used for other cases, like "Synthetic Exposure" where we can reference back to the record on the run the value came from.
	 */
	private SystemTable sourceTable;

	/**
	 * Anything that is not a position valuation would not allow investment group selections
	 * For example, AUM calculation just pulls the month end AUM value reported for the account on the account's period closing.  Because we don't store these values by position,
	 * you can't select an investment group to filter against.
	 * <p>
	 * Used to be able to apply different valuation types for different types of securities.  For example, Synthetic Exposure for futures, but Market Value for Bonds.
	 */
	private boolean investmentGroupAllowed;


	/**
	 * Anything that uses managers would require manager selection.
	 * Note: If manager selection is not required, then it's also not allowed
	 */
	private boolean investmentManagerRequired;


	/**
	 * In some instances, like Manager Cash where the Manager Cash is a portion of the overall total
	 * we need to be able to determine whether or not we are using absolute value of the daily total
	 */
	private boolean allowNegativeDailyValues;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isNoBillingBasis() {
		return getCalculatorBean() == null;
	}


	public boolean isExternalBillingBasis() {
		if (getSourceTable() != null) {
			return BillingBasisExternal.BILLING_BASIS_EXTERNAL_TABLE_NAME.equals(getSourceTable().getName());
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBean getCalculatorBean() {
		return this.calculatorBean;
	}


	public void setCalculatorBean(SystemBean calculatorBean) {
		this.calculatorBean = calculatorBean;
	}


	public SystemTable getSourceTable() {
		return this.sourceTable;
	}


	public void setSourceTable(SystemTable sourceTable) {
		this.sourceTable = sourceTable;
	}


	public boolean isInvestmentGroupAllowed() {
		return this.investmentGroupAllowed;
	}


	public void setInvestmentGroupAllowed(boolean investmentGroupAllowed) {
		this.investmentGroupAllowed = investmentGroupAllowed;
	}


	public boolean isInvestmentManagerRequired() {
		return this.investmentManagerRequired;
	}


	public void setInvestmentManagerRequired(boolean investmentManagerRequired) {
		this.investmentManagerRequired = investmentManagerRequired;
	}


	public boolean isAllowNegativeDailyValues() {
		return this.allowNegativeDailyValues;
	}


	public void setAllowNegativeDailyValues(boolean allowNegativeDailyValues) {
		this.allowNegativeDailyValues = allowNegativeDailyValues;
	}
}
