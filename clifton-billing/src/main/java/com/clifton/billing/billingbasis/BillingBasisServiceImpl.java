package com.clifton.billing.billingbasis;


import com.clifton.billing.billingbasis.search.BillingBasisAggregationTypeSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisCalculationTypeSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisExternalSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.billingbasis.search.BillingBasisValuationTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Service
public class BillingBasisServiceImpl implements BillingBasisService {

	private AdvancedUpdatableDAO<BillingBasisValuationType, Criteria> billingBasisValuationTypeDAO;
	private AdvancedUpdatableDAO<BillingBasisCalculationType, Criteria> billingBasisCalculationTypeDAO;
	private AdvancedUpdatableDAO<BillingBasisAggregationType, Criteria> billingBasisAggregationTypeDAO;

	private AdvancedUpdatableDAO<BillingBasisExternal, Criteria> billingBasisExternalDAO;
	private AdvancedUpdatableDAO<BillingBasisSnapshot, Criteria> billingBasisSnapshotDAO;


	////////////////////////////////////////////////////////////////////////////////
	//////////            Billing Basis Calculation Type Methods           /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisCalculationType getBillingBasisCalculationType(short id) {
		return getBillingBasisCalculationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingBasisCalculationType> getBillingBasisCalculationTypeList(BillingBasisCalculationTypeSearchForm searchForm) {
		return getBillingBasisCalculationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////            Billing Basis Valuation Type Methods           //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisValuationType getBillingBasisValuationType(short id) {
		return getBillingBasisValuationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingBasisValuationType> getBillingBasisValuationTypeList(BillingBasisValuationTypeSearchForm searchForm) {
		return getBillingBasisValuationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Billing Basis Aggregation Type Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisAggregationType getBillingBasisAggregationType(short id) {
		return getBillingBasisAggregationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingBasisAggregationType> getBillingBasisAggregationTypeList(BillingBasisAggregationTypeSearchForm searchForm) {
		return getBillingBasisAggregationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BillingBasisAggregationType saveBillingBasisAggregationType(BillingBasisAggregationType bean) {
		return getBillingBasisAggregationTypeDAO().save(bean);
	}


	@Override
	public void deleteBillingBasisAggregationType(short id) {
		getBillingBasisAggregationTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////            Billing Basis External Methods           ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisExternal getBillingBasisExternal(int id) {
		return getBillingBasisExternalDAO().findByPrimaryKey(id);
	}


	@Override
	public BillingBasisExternal getBillingBasisExternalExisting(BillingBasisExternal bean) {
		return getBillingBasisExternalDAO().findOneByFields(new String[]{"billingDefinitionInvestmentAccount.id", "assetClass.id", "date"},
				new Object[]{bean.getBillingDefinitionInvestmentAccount().getId(), bean.getAssetClass() == null ? null : bean.getAssetClass().getId(), bean.getDate()});
	}


	@Override
	public List<BillingBasisExternal> getBillingBasisExternalList(BillingBasisExternalSearchForm searchForm) {
		return getBillingBasisExternalDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BillingBasisExternal saveBillingBasisExternal(BillingBasisExternal bean) {
		return getBillingBasisExternalDAO().save(bean);
	}


	@Override
	@Transactional
	public void saveBillingBasisExternalList(List<BillingBasisExternal> list) {
		getBillingBasisExternalDAO().saveList(list);
	}


	@Override
	public void deleteBillingBasisExternal(int id) {
		getBillingBasisExternalDAO().delete(id);
	}


	@Override
	public void deleteBillingBasisExternalList(BillingBasisExternalSearchForm searchForm, Date startDate, Date endDate) {
		ValidationUtils.assertNotNull(searchForm.getBillingDefinitionInvestmentAccountId(), "Please select a specific definition/account to delete records for.");
		ValidationUtils.assertNotNull(startDate, "Please enter a start date.");
		ValidationUtils.assertNotNull(endDate, "Please enter an end date.");

		searchForm.addSearchRestriction(new SearchRestriction("date", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("date", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));

		List<BillingBasisExternal> list = getBillingBasisExternalList(searchForm);
		getBillingBasisExternalDAO().deleteList(list);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////            Billing Basis Snapshot Methods           ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingBasisSnapshot> getBillingBasisSnapshotList(BillingBasisSnapshotSearchForm searchForm) {
		return getBillingBasisSnapshotDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<BillingBasisSnapshot> getBillingBasisSnapshotListForInvoice(int billingInvoiceId) {
		return getBillingBasisSnapshotDAO().findByField("billingInvoice.id", billingInvoiceId);
	}


	@Override
	public List<BillingBasisSnapshot> getBillingBasisSnapshotList(int billingInvoiceId, int billingDefinitionInvestmentAccountId) {
		return getBillingBasisSnapshotDAO().findByFields(new String[]{"billingInvoice.id", "billingDefinitionInvestmentAccount.id"},
				new Object[]{billingInvoiceId, billingDefinitionInvestmentAccountId});
	}


	@Override
	public void saveBillingBasisSnapshotList(List<BillingBasisSnapshot> list) {
		// If every value is 0 do not save the list
		int zeroValueListSize = CollectionUtils.getSize(BeanUtils.filter(list, BillingBasisSnapshot::getSnapshotValue, BigDecimal.ZERO));
		if (zeroValueListSize != CollectionUtils.getSize(list)) {
			getBillingBasisSnapshotDAO().saveList(list);
		}
	}


	@Override
	public void deleteBillingBasisSnapshotListForInvoice(int billingInvoiceId) {
		getBillingBasisSnapshotDAO().deleteList(getBillingBasisSnapshotListForInvoice(billingInvoiceId));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////              Getter and Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BillingBasisExternal, Criteria> getBillingBasisExternalDAO() {
		return this.billingBasisExternalDAO;
	}


	public void setBillingBasisExternalDAO(AdvancedUpdatableDAO<BillingBasisExternal, Criteria> billingBasisExternalDAO) {
		this.billingBasisExternalDAO = billingBasisExternalDAO;
	}


	public AdvancedUpdatableDAO<BillingBasisSnapshot, Criteria> getBillingBasisSnapshotDAO() {
		return this.billingBasisSnapshotDAO;
	}


	public void setBillingBasisSnapshotDAO(AdvancedUpdatableDAO<BillingBasisSnapshot, Criteria> billingBasisSnapshotDAO) {
		this.billingBasisSnapshotDAO = billingBasisSnapshotDAO;
	}


	public AdvancedUpdatableDAO<BillingBasisValuationType, Criteria> getBillingBasisValuationTypeDAO() {
		return this.billingBasisValuationTypeDAO;
	}


	public void setBillingBasisValuationTypeDAO(AdvancedUpdatableDAO<BillingBasisValuationType, Criteria> billingBasisValuationTypeDAO) {
		this.billingBasisValuationTypeDAO = billingBasisValuationTypeDAO;
	}


	public AdvancedUpdatableDAO<BillingBasisCalculationType, Criteria> getBillingBasisCalculationTypeDAO() {
		return this.billingBasisCalculationTypeDAO;
	}


	public void setBillingBasisCalculationTypeDAO(AdvancedUpdatableDAO<BillingBasisCalculationType, Criteria> billingBasisCalculationTypeDAO) {
		this.billingBasisCalculationTypeDAO = billingBasisCalculationTypeDAO;
	}


	public AdvancedUpdatableDAO<BillingBasisAggregationType, Criteria> getBillingBasisAggregationTypeDAO() {
		return this.billingBasisAggregationTypeDAO;
	}


	public void setBillingBasisAggregationTypeDAO(AdvancedUpdatableDAO<BillingBasisAggregationType, Criteria> billingBasisAggregationTypeDAO) {
		this.billingBasisAggregationTypeDAO = billingBasisAggregationTypeDAO;
	}
}
