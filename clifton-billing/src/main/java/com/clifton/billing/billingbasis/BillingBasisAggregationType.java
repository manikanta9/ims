package com.clifton.billing.billingbasis;

import com.clifton.core.beans.NamedEntity;


/**
 * The <code>{@link BillingBasisAggregationType}</code> defines how billing basis values can be aggregated for fee sharing within or across billing definitions.
 * This is primarily useful for cases where there is a tiered fee and clients receive benefit of shared billing basis but it doesn't apply to ALL included on the definition and needs to be broken out
 * When not selected on a schedule the system will group all applicable billing basis into one value.
 * <p>
 * This allows for creating one billing definition (i.e. Merrill Accounts) where the advisor (Merrill) get's the invoices for fee debiting so we do not need separate invoices
 * for each client, but we can still accurately aggregate fees into buckets for calculating the tiered fee.  All accounts in these cases use the same schedule(s) so that repetition of setup is not necessary.
 *
 * @author manderson
 */
public class BillingBasisAggregationType extends NamedEntity<Short> {


	/**
	 * The field path (from the BillingDefinitionInvestmentAccount) that determines the aggregation/grouping level
	 * i.e. investmentAccount.businessClient.clientRelationship.id if we are to aggregate separately for each Client Relationship
	 */
	private String aggregationBeanFieldPath;

	/**
	 * The freemarker template to generate an optional label for each detail line. i.e. if Aggregation is at the account level, each detail line can say Account #: 123
	 * BillingDefinitionInvestmentAccount object is passed to the context as the "bean" property
	 * BillingSchedule object is passed to the context as the "schedule" property
	 */
	private String aggregationLabelTemplate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getAggregationBeanFieldPath() {
		return this.aggregationBeanFieldPath;
	}


	public void setAggregationBeanFieldPath(String aggregationBeanFieldPath) {
		this.aggregationBeanFieldPath = aggregationBeanFieldPath;
	}


	public String getAggregationLabelTemplate() {
		return this.aggregationLabelTemplate;
	}


	public void setAggregationLabelTemplate(String aggregationLabelTemplate) {
		this.aggregationLabelTemplate = aggregationLabelTemplate;
	}
}
