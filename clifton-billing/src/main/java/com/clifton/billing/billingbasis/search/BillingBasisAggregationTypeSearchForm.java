package com.clifton.billing.billingbasis.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class BillingBasisAggregationTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String aggregationBeanFieldPath;

	@SearchField
	private String aggregationLabelTemplate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getAggregationBeanFieldPath() {
		return this.aggregationBeanFieldPath;
	}


	public void setAggregationBeanFieldPath(String aggregationBeanFieldPath) {
		this.aggregationBeanFieldPath = aggregationBeanFieldPath;
	}


	public String getAggregationLabelTemplate() {
		return this.aggregationLabelTemplate;
	}


	public void setAggregationLabelTemplate(String aggregationLabelTemplate) {
		this.aggregationLabelTemplate = aggregationLabelTemplate;
	}
}
