package com.clifton.billing.billingbasis.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class BillingBasisCalculationTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchFieldPath = "calculatorBean", searchField = "name")
	private String calculatorBeanName;

	@SearchField
	private Boolean dateRangeSpecific;

	@SearchField
	private Boolean inactive;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCalculatorBeanName() {
		return this.calculatorBeanName;
	}


	public void setCalculatorBeanName(String calculatorBeanName) {
		this.calculatorBeanName = calculatorBeanName;
	}


	public Boolean getDateRangeSpecific() {
		return this.dateRangeSpecific;
	}


	public void setDateRangeSpecific(Boolean dateRangeSpecific) {
		this.dateRangeSpecific = dateRangeSpecific;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
