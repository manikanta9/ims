package com.clifton.billing.billingbasis.upload;

import com.clifton.billing.billingbasis.BillingBasisExternal;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * @author manderson
 */
public interface BillingBasisUploadService {


	////////////////////////////////////////////////////////////////////////////
	////////           Billing Basis External Upload Methods         ///////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BillingBasisExternal.class)
	public void uploadBillingBasisExternalUploadFile(BillingBasisExternalUploadCommand uploadCommand);
}
