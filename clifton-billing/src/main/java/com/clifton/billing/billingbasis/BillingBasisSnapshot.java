package com.clifton.billing.billingbasis;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * BillingBasisSnapshot contains specific details for BillingBasisValues for a particular position or cash record that is used to calculate
 * the billing basis for the billing definition investment account.  These are the results of the {@link BillingBasisValuationType} calculations
 */
public class BillingBasisSnapshot extends BaseSimpleEntity<Integer> {

	/**
	 * The account and valuation type this billing basis value was calculated for.
	 */
	private BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount;

	/**
	 * The invoice this billing basis was generated for.  We track the invoice for cases where an invoice
	 * is voided, we can always go back and see what the details were on the voided invoice vs. the current invoice
	 */
	private BillingInvoice billingInvoice;

	/**
	 * Date this applies to and billing basis value that applies on this date for this snapshot
	 * Snapshot Value is saved in the Client Account's Base CCY
	 */
	private Date snapshotDate;
	private BigDecimal snapshotValue;

	/**
	 * Details - Different fields are populated depending on the @{link {@link BillingBasisValuationType}} and the source of the billing details
	 */
	private InvestmentAssetClass assetClass;
	private InvestmentSecurity security;
	private BigDecimal quantity;
	private BigDecimal securityPrice;

	/**
	 * Billing Basis Snapshot is stored in the Base Currency of the Client Account, because that is how the data
	 * is stored/pulled.
	 * <p>
	 * Sometimes, the bills we generate are in a different currency that the base currency of the account.
	 * <p>
	 * In these cases where the fxRate is used, i.e. Invoice is billed in a different currency than the client account's base
	 * currency, store the fxRate so it's known what rate was used to convert the billing basis from the client account's base currency
	 * to the base currency of the invoice.  Will be NULL if wasn't used.
	 */
	private BigDecimal fxRate;

	/**
	 * For some billing basis valuations, we keep a link back to the original
	 * table/row that the billing basis came from.  (i.e. Synthetic Exposure points back to the ProductOverlayAssetClassReplication row that it came from)
	 * This is useful for drill downs in the screen to go right back to the source of the data
	 */
	private Integer sourceFkFieldId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisSnapshot() {
		super();
	}


	public BillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionAccount) {
		this.billingInvoice = invoice;
		this.billingDefinitionInvestmentAccount = billingDefinitionAccount;
	}


	public BillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionAccount, Date snapshotDate) {
		this(invoice, billingDefinitionAccount);
		this.snapshotDate = snapshotDate;
		this.snapshotValue = BigDecimal.ZERO;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getValuationType() {
		if (getBillingDefinitionInvestmentAccount() != null) {
			return getBillingDefinitionInvestmentAccount().getBillingBasisValuationType().getName();
		}
		return null;
	}


	public Integer getDaysToMaturity() {
		if (getSecurity() == null || getSecurity().getEndDate() == null) {
			return null;
		}
		return DateUtils.getDaysDifference(getSecurity().getEndDate(), getSnapshotDate());
	}


	public BigDecimal getSnapshotValueInBillingBaseCurrency() {
		if (getFxRate() != null) {
			return MathUtils.multiply(getSnapshotValue(), getFxRate());
		}
		return getSnapshotValue();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAssetClass getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(InvestmentAssetClass assetClass) {
		this.assetClass = assetClass;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount() {
		return this.billingDefinitionInvestmentAccount;
	}


	public void setBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		this.billingDefinitionInvestmentAccount = billingDefinitionInvestmentAccount;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public BigDecimal getSnapshotValue() {
		return this.snapshotValue;
	}


	public void setSnapshotValue(BigDecimal snapshotValue) {
		this.snapshotValue = snapshotValue;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BillingInvoice getBillingInvoice() {
		return this.billingInvoice;
	}


	public void setBillingInvoice(BillingInvoice billingInvoice) {
		this.billingInvoice = billingInvoice;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}
}
