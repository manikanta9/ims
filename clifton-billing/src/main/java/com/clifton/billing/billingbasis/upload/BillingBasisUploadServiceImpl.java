package com.clifton.billing.billingbasis.upload;

import com.clifton.billing.billingbasis.BillingBasisExternal;
import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSearchForm;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InvestmentAssetClassSearchForm;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class BillingBasisUploadServiceImpl implements BillingBasisUploadService {

	private BillingBasisService billingBasisService;
	private BillingDefinitionService billingDefinitionService;

	private InvestmentSetupService investmentSetupService;

	private SystemUploadHandler systemUploadHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadBillingBasisExternalUploadFile(BillingBasisExternalUploadCommand uploadCommand) {
		uploadCommand.setSimple(true);
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, false);

		BillingDefinitionInvestmentAccount defaultBillingAccount = null;
		if (uploadCommand.getBillingDefinitionInvestmentAccountId() != null) {
			defaultBillingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(uploadCommand.getBillingDefinitionInvestmentAccountId());
		}

		// Create a map of natural key (Account #_Valuation Description) to BillingDefinitionInvestmentAccount to avoid duplicate look-ups
		Map<String, BillingDefinitionInvestmentAccount> accountMap = new HashMap<>();
		Map<String, InvestmentAssetClass> assetClassMap = new HashMap<>();

		List<BillingBasisExternal> list = new ArrayList<>();
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			BillingBasisExternal external = (BillingBasisExternal) obj;
			ValidationUtils.assertNotNull(external.getBillingBasisAmount(), "Billing Basis Amount is required for all rows.");
			ValidationUtils.assertNotNull(external.getDate(), "Date is required for all rows.");

			populateBillingDefinitionInvestmentAccount(external, defaultBillingAccount, accountMap);
			populateInvestmentAssetClass(external, assetClassMap);

			if (FileUploadExistingBeanActions.INSERT != uploadCommand.getExistingBeans()) {
				BillingBasisExternal existing = getBillingBasisService().getBillingBasisExternalExisting(external);
				if (existing != null) {
					if (FileUploadExistingBeanActions.SKIP == uploadCommand.getExistingBeans()) {
						continue; // Skip this value
					}
					existing.setBillingBasisAmount(external.getBillingBasisAmount());
					list.add(existing);
					continue;
				}
			}
			list.add(external);
		}
		getBillingBasisService().saveBillingBasisExternalList(list);
		uploadCommand.getUploadResult().addUploadResults(BillingBasisExternal.BILLING_BASIS_EXTERNAL_TABLE_NAME, CollectionUtils.getSize(list), true);
	}


	private void populateBillingDefinitionInvestmentAccount(BillingBasisExternal external, BillingDefinitionInvestmentAccount defaultBillingAccount, Map<String, BillingDefinitionInvestmentAccount> accountMap) {
		BillingDefinitionInvestmentAccount billingAccount = null;
		if (external.getBillingDefinitionInvestmentAccount() != null) {
			String accountNumber = external.getBillingDefinitionInvestmentAccount().getReferenceTwo().getNumber();
			if (!StringUtils.isEmpty(accountNumber)) {
				String valuationDescription = external.getBillingDefinitionInvestmentAccount().getBillingBasisValuationTypeDescription();
				if (StringUtils.isEmpty(valuationDescription)) {
					throw new ValidationException(
							"Billing Basis Valuation Type Description is Required when Account Numbers are present in order to look up the correct Billing Definition Investment Account.");
				}
				String key = accountNumber + "_" + valuationDescription;
				if (accountMap.containsKey(key)) {
					billingAccount = accountMap.get(key);
				}
				else {
					BillingDefinitionInvestmentAccountSearchForm searchForm = new BillingDefinitionInvestmentAccountSearchForm();
					searchForm.setBillingBasisValuationSourceTableName(BillingBasisExternal.BILLING_BASIS_EXTERNAL_TABLE_NAME);
					searchForm.setBillingBasisValuationTypeDescriptionExact(valuationDescription);
					searchForm.setInvestmentAccountNumberExact(accountNumber);
					List<BillingDefinitionInvestmentAccount> billingAccountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountList(searchForm);
					if (CollectionUtils.isEmpty(billingAccountList)) {
						throw new ValidationException("Cannot find an External Billing Definition Investment Account for Account #: [" + accountNumber + "] with valuation type description ["
								+ valuationDescription + "]");
					}
					else if (CollectionUtils.getSize(billingAccountList) > 1) {
						throw new ValidationException("Found [" + CollectionUtils.getSize(billingAccountList) + "] External Billing Definition Investment Account records for Account #: [" + accountNumber
								+ "] with valuation type description [" + valuationDescription + "]");
					}
					else {
						billingAccount = billingAccountList.get(0);
						accountMap.put(key, billingAccount);
					}
				}
			}
		}
		if (billingAccount == null) {
			if (defaultBillingAccount == null) {
				throw new ValidationException(
						"A default billing definition investment account should be selected if each row in the file doesn't have a different account number and valuation type description to map to.");
			}
			billingAccount = defaultBillingAccount;
		}
		external.setBillingDefinitionInvestmentAccount(billingAccount);
	}


	private void populateInvestmentAssetClass(BillingBasisExternal external, Map<String, InvestmentAssetClass> assetClassMap) {
		if (external.getAssetClass() != null && !StringUtils.isEmpty(external.getAssetClass().getName())) {
			String name = external.getAssetClass().getName();
			String parentName = null;
			if (external.getAssetClass().getParent() != null && !StringUtils.isEmpty(external.getAssetClass().getParent().getName())) {
				parentName = external.getAssetClass().getParent().getName();
			}
			String key = (parentName != null ? parentName + "_" : "") + name;
			InvestmentAssetClass assetClass;
			if (assetClassMap.containsKey(key)) {
				assetClass = assetClassMap.get(key);
			}
			else {
				InvestmentAssetClassSearchForm assetClassSearchForm = new InvestmentAssetClassSearchForm();
				assetClassSearchForm.setName(name);
				if (!StringUtils.isEmpty(parentName)) {
					assetClassSearchForm.setParentName(parentName);
				}
				else {
					assetClassSearchForm.setNullParent(true);
				}
				// Note: Not Using Strict method because want to throw customized error message with asset class name
				assetClass = CollectionUtils.getOnlyElement(getInvestmentSetupService().getInvestmentAssetClassList(assetClassSearchForm));
				if (assetClass == null) {
					throw new ValidationException("Asset Class [" + external.getAssetClass().getName()
							+ "] is not a valid selection.  Please either add it as an asset class, or change the asset class in the file to an existing one.");
				}
				assetClassMap.put(key, assetClass);
			}
			external.setAssetClass(assetClass);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
