package com.clifton.billing.invoice.process.schedule.waive;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;

import java.util.Map;


/**
 * The <code>BillingScheduleWaiveConditionConfig</code> is an object that is passed
 * to the comparison class when it implements BillingScheduleWaiveCondition interface
 * that contains additional information during schedule processing, such as start/end dates, billing basis map, etc
 * so that processing specific waive conditions can be applied.
 *
 * @author manderson
 */
public class BillingScheduleWaiveConditionConfig {

	private final BillingInvoice invoice;
	private final Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingScheduleWaiveConditionConfig(BillingInvoice invoice, Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap) {
		this.invoice = invoice;
		this.billingBasisMap = billingBasisMap;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoice getInvoice() {
		return this.invoice;
	}


	public Map<BillingDefinitionInvestmentAccount, BillingBasis> getBillingBasisMap() {
		return this.billingBasisMap;
	}
}
