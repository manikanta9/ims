package com.clifton.billing.invoice.process.schedule.amount.proraters;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingAmountForBillingAccountDatesProrater</code> prorates the amount if the account
 * starts or ends during the quarter.  Uses BillingDefinitionInvestmentAccount start/end date for the account
 * If null, defaults to using the Billing Definition start/end date
 *
 * @author manderson
 */
public class BillingAmountForBillingAccountDatesProrater extends BillingAmountForBillingDatesProrater {

	private BillingDefinitionService billingDefinitionService;

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateBillingAmountDateProraterConfig(BillingAmountDateProraterConfig proraterConfig, BillingScheduleProcessConfig scheduleConfig, BillingBasis.BillingBasisDetail billingBasisDetail) {
		Date startDate;
		Date endDate;

		if (scheduleConfig.getSchedule().getScheduleType().isAnnualFee()) {
			startDate = scheduleConfig.getAnnualInvoicePeriodStartDate();
			endDate = scheduleConfig.getAnnualInvoicePeriodEndDate();
		}
		else {
			startDate = scheduleConfig.getInvoiceBillingStartDate();
			endDate = scheduleConfig.getInvoiceBillingEndDate();
		}

		BillingSchedule billingSchedule = scheduleConfig.getSchedule();

		// Get/Filter the List of BillingDefinitionInvestmentAccount objects to check dates for
		List<BillingDefinitionInvestmentAccount> billingAccountList = new ArrayList<>();
		// This also includes cases where "IsApplyToAllClientAccounts" since it takes all client accounts on the billing definition
		if (billingSchedule.getBillingDefinition() != null) {
			billingAccountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(billingSchedule.getBillingDefinition().getId());
		}
		else {
			for (BillingScheduleSharing bss : CollectionUtils.getIterable(getBillingDefinitionService().getBillingScheduleSharingListForSchedule(billingSchedule.getId()))) {
				billingAccountList.addAll(getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(bss.getReferenceTwo().getId()));
			}
		}

		// If Applying to Specific Account/Accounts - Filter:
		if (billingSchedule.getInvestmentAccount() != null) {
			billingAccountList = BeanUtils.filter(billingAccountList, BillingDefinitionInvestmentAccount::getReferenceTwo, billingSchedule.getInvestmentAccount());
		}
		else if (billingSchedule.getInvestmentAccountGroup() != null) {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setOurAccount(true);
			searchForm.setInvestmentAccountGroupId(billingSchedule.getInvestmentAccountGroup().getId());
			List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

			billingAccountList = BeanUtils.filter(billingAccountList, billingAccount -> CollectionUtils.contains(accountList, billingAccount.getReferenceTwo()));
		}

		Date accountStartDate = null;
		Date accountEndDate = null;
		boolean noEnd = false;
		for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(billingAccountList)) {
			// Find the Earliest Account StartDate
			Date billingAccountStartDate = billingAccount.getStartDate() != null ? billingAccount.getStartDate() : billingAccount.getReferenceOne().getStartDate();
			if (accountStartDate == null) {
				accountStartDate = billingAccountStartDate;
			}
			else {
				if (DateUtils.compare(billingAccountStartDate, accountStartDate, false) < 0) {
					accountStartDate = billingAccountStartDate;
				}
			}

			// Find the Latest Account EndDate
			if (!noEnd) {
				Date billingAccountEndDate = billingAccount.getEndDate() != null ? billingAccount.getEndDate() : billingAccount.getReferenceOne().getEndDate();
				if (billingAccountEndDate == null) {
					noEnd = true;
				}
				else {
					if (accountEndDate == null) {
						accountEndDate = billingAccountEndDate;
					}
					else {
						if (DateUtils.compare(billingAccountEndDate, accountEndDate, false) > 0) {
							accountEndDate = billingAccountEndDate;
						}
					}
				}
			}
		}

		// If Account Start is Before Billing Start - Use Billing Start
		if (DateUtils.compare(accountStartDate, startDate, false) < 0) {
			accountStartDate = startDate;
		}
		// If No End Specified or Account End is After Billing End - Use Billing End
		if (noEnd || DateUtils.compare(accountEndDate, endDate, false) > 0) {
			accountEndDate = endDate;
		}

		proraterConfig.setProrateStartDate(accountStartDate);
		proraterConfig.setProrateEndDate(accountEndDate);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
