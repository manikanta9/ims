package com.clifton.billing.invoice.search;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.workflow.search.BaseWorkflowAwareSystemHierarchyItemSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class BillingInvoiceSearchForm extends BaseWorkflowAwareSystemHierarchyItemSearchForm implements BusinessCompanyAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "id,businessCompany.name", sortField = "businessCompany.name", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER)
	private String searchPattern;

	@SearchField(searchField = "billingDefinition.id")
	private Integer billingDefinitionId;

	@SearchField(searchField = "billingDefinition.id")
	private Integer[] billingDefinitionIds;

	@SearchField(searchField = "invoiceType.id")
	private Short invoiceTypeId;

	@SearchField(searchField = "name", searchFieldPath = "invoiceType")
	private String invoiceTypeName;

	@SearchField(searchField = "revenue", searchFieldPath = "invoiceType")
	private Boolean revenue;

	// Custom Search Field based on if amount for the selected is populated
	private BillingInvoiceRevenueTypes revenueType;

	@SearchField(searchFieldPath = "billingDefinition", searchField = "invoicePostedToPortal")
	private Boolean definitionInvoicePostedToPortal;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyId;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;


	@SearchField(searchField = "investmentAccountList.referenceTwo.groupList.referenceOne.id", searchFieldPath = "billingDefinition", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "investmentAccountList.referenceTwo.teamSecurityGroup.id", searchFieldPath = "billingDefinition", comparisonConditions = ComparisonConditions.EXISTS)
	private Short teamSecurityGroupId;

	@SearchField(searchFieldPath = "businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField(searchField = "invoiceGroup.id")
	private Integer invoiceGroupId;

	@SearchField
	private Date invoiceDate;

	@SearchField(searchField = "invoiceDate", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Date excludeInvoiceDate;

	@SearchField
	private BigDecimal totalAmount;

	@SearchField
	private BigDecimal revenueShareExternalTotalAmount;

	@SearchField
	private BigDecimal revenueShareInternalTotalAmount;

	@SearchField(searchField = "totalAmount,revenueShareExternalTotalAmount", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal revenueNetTotalAmount;

	@SearchField(searchField = "totalAmount,revenueShareExternalTotalAmount,revenueShareInternalTotalAmount", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal revenueFinalTotalAmount;

	@SearchField
	private BigDecimal paidAmount;

	// Custom Search Field (Total Amount + Revenue Share External Total Amount - Paid Amount)
	private BigDecimal unpaidAmount;

	@SearchField(searchField = "invoiceDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date invoiceStartDate;

	@SearchField(searchField = "invoiceDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date invoiceEndDate;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	@SearchField
	private String voidReason;

	// Custom Search Field - Used to filter out invoices already allocated to a payment
	private Integer excludePaymentAllocationPaymentId;

	@SearchField
	private Short documentFileCount;


	@SearchField(searchField = "lockedByUser.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean locked;


	@SearchField(searchField = "lockedByUser.id", sortField = "lockedByUser.displayName")
	private Short lockedByUserId;

	@SearchField
	private String lockNote;

	@SearchField
	private Boolean postedToPortal;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return BillingInvoice.BILLING_INVOICE_TABLE_NAME;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"businessCompany"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		// DO NOTHING
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		// DO NOTHING
	}

	////////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	public Date getInvoiceStartDate() {
		return this.invoiceStartDate;
	}


	public void setInvoiceStartDate(Date invoiceStartDate) {
		this.invoiceStartDate = invoiceStartDate;
	}


	public Date getInvoiceEndDate() {
		return this.invoiceEndDate;
	}


	public void setInvoiceEndDate(Date invoiceEndDate) {
		this.invoiceEndDate = invoiceEndDate;
	}


	public Integer getInvoiceGroupId() {
		return this.invoiceGroupId;
	}


	public void setInvoiceGroupId(Integer invoiceGroupId) {
		this.invoiceGroupId = invoiceGroupId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getVoidReason() {
		return this.voidReason;
	}


	public void setVoidReason(String voidReason) {
		this.voidReason = voidReason;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getExcludePaymentAllocationPaymentId() {
		return this.excludePaymentAllocationPaymentId;
	}


	public void setExcludePaymentAllocationPaymentId(Integer excludePaymentAllocationPaymentId) {
		this.excludePaymentAllocationPaymentId = excludePaymentAllocationPaymentId;
	}


	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}


	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public Date getExcludeInvoiceDate() {
		return this.excludeInvoiceDate;
	}


	public void setExcludeInvoiceDate(Date excludeInvoiceDate) {
		this.excludeInvoiceDate = excludeInvoiceDate;
	}


	public String getInvoiceTypeName() {
		return this.invoiceTypeName;
	}


	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}


	public Short getLockedByUserId() {
		return this.lockedByUserId;
	}


	public void setLockedByUserId(Short lockedByUserId) {
		this.lockedByUserId = lockedByUserId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer[] getBillingDefinitionIds() {
		return this.billingDefinitionIds;
	}


	public void setBillingDefinitionIds(Integer[] billingDefinitionIds) {
		this.billingDefinitionIds = billingDefinitionIds;
	}


	public Boolean getLocked() {
		return this.locked;
	}


	public void setLocked(Boolean locked) {
		this.locked = locked;
	}


	public String getLockNote() {
		return this.lockNote;
	}


	public void setLockNote(String lockNote) {
		this.lockNote = lockNote;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Boolean getPostedToPortal() {
		return this.postedToPortal;
	}


	public void setPostedToPortal(Boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Boolean getRevenue() {
		return this.revenue;
	}


	public void setRevenue(Boolean revenue) {
		this.revenue = revenue;
	}


	public BillingInvoiceRevenueTypes getRevenueType() {
		return this.revenueType;
	}


	public void setRevenueType(BillingInvoiceRevenueTypes revenueType) {
		this.revenueType = revenueType;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public BigDecimal getUnpaidAmount() {
		return this.unpaidAmount;
	}


	public void setUnpaidAmount(BigDecimal unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}


	public Boolean getDefinitionInvoicePostedToPortal() {
		return this.definitionInvoicePostedToPortal;
	}


	public void setDefinitionInvoicePostedToPortal(Boolean definitionInvoicePostedToPortal) {
		this.definitionInvoicePostedToPortal = definitionInvoicePostedToPortal;
	}


	public BigDecimal getRevenueShareExternalTotalAmount() {
		return this.revenueShareExternalTotalAmount;
	}


	public void setRevenueShareExternalTotalAmount(BigDecimal revenueShareExternalTotalAmount) {
		this.revenueShareExternalTotalAmount = revenueShareExternalTotalAmount;
	}


	public BigDecimal getRevenueShareInternalTotalAmount() {
		return this.revenueShareInternalTotalAmount;
	}


	public void setRevenueShareInternalTotalAmount(BigDecimal revenueShareInternalTotalAmount) {
		this.revenueShareInternalTotalAmount = revenueShareInternalTotalAmount;
	}


	public BigDecimal getRevenueNetTotalAmount() {
		return this.revenueNetTotalAmount;
	}


	public void setRevenueNetTotalAmount(BigDecimal revenueNetTotalAmount) {
		this.revenueNetTotalAmount = revenueNetTotalAmount;
	}


	public BigDecimal getRevenueFinalTotalAmount() {
		return this.revenueFinalTotalAmount;
	}


	public void setRevenueFinalTotalAmount(BigDecimal revenueFinalTotalAmount) {
		this.revenueFinalTotalAmount = revenueFinalTotalAmount;
	}
}
