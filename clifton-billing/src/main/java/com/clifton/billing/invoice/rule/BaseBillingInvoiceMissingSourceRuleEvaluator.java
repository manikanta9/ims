package com.clifton.billing.invoice.rule;

import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.history.WorkflowHistory;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseBillingInvoiceMissingSourceRuleEvaluator</code> is an abstract class that can be extended by specific rule evaluators that check for missing source data.
 *
 * @author manderson
 */
public abstract class BaseBillingInvoiceMissingSourceRuleEvaluator extends BaseBillingInvoiceRuleEvaluator {

	private BillingBasisService billingBasisService;

	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Overridden to supply the source table - i.e. ProductOverlayAssetClassReplication, InvestmentManagerAccountBalance
	 */
	public abstract String getSourceTableName();


	/**
	 * Differentiates between false = null SourceFKFieldID and true = snapshot value = 0
	 */
	public abstract boolean isUseZeroValueAsMissingSource();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of BillingBasisSnapshot that utilize billing basis valuation type for the source table where source FKFieldID is null
	 * The list is also filtered based on the account billing as well and inception and termination dates so we don't give violations when we don't require values
	 */
	protected List<BillingBasisSnapshot> getBillingBasisSnapshotListMissingSource(BillingInvoice invoice, BillingInvoiceRuleEvaluatorContext context) {
		BillingBasisSnapshotSearchForm searchForm = new BillingBasisSnapshotSearchForm();
		searchForm.setBillingInvoiceId(invoice.getId());
		searchForm.setSourceTableId(getSystemSchemaService().getSystemTableByName(getSourceTableName()).getId());
		if (isUseZeroValueAsMissingSource()) {
			searchForm.setSnapshotValue(BigDecimal.ZERO);
		}
		else {
			searchForm.setNullSourceFkFieldId(true);
		}
		return BeanUtils.filter(getBillingBasisService().getBillingBasisSnapshotList(searchForm), billingBasisSnapshot -> isBillingBasisBetweenAccountStartAndEnd(billingBasisSnapshot, context));
	}


	protected void addRuleViolationToList(List<RuleViolation> violationList, EntityConfig entityConfig, BillingInvoice invoice, Integer causeEntityId, List<Date> dateMissingList) {
		if (!CollectionUtils.isEmpty(dateMissingList)) {
			Collections.sort(dateMissingList);
			Map<String, Object> templateValues = new HashMap<>();
			templateValues.put("dateMissingList", StringUtils.collectionToCommaDelimitedString(dateMissingList, DateUtils::fromDateShort));
			RuleViolation violation = getRuleViolationService().createRuleViolationWithCause(entityConfig, invoice.getId(), causeEntityId, null, templateValues);
			violationList.add(violation);
		}
	}


	/**
	 * Verify date really expects a value - (i.e. don't apply rule if before Account Inception, after Account Termination)
	 */
	private boolean isBillingBasisBetweenAccountStartAndEnd(BillingBasisSnapshot bbs, BillingInvoiceRuleEvaluatorContext context) {
		// If this BillingDefinitionInvestmentAccount has billing start after default start, then use that date.
		Date startDate = bbs.getBillingDefinitionInvestmentAccount().getStartDate();
		if (startDate == null) {
			startDate = bbs.getBillingDefinitionInvestmentAccount().getReferenceOne().getStartDate();
		}
		Date endDate = bbs.getBillingDefinitionInvestmentAccount().getEndDate();
		if (endDate == null) {
			endDate = bbs.getBillingDefinitionInvestmentAccount().getReferenceOne().getEndDate();
		}
		// Date is between billing dates
		if (DateUtils.isDateBetween(bbs.getSnapshotDate(), startDate, endDate, false)) {
			InvestmentAccount account = bbs.getBillingDefinitionInvestmentAccount().getReferenceTwo();
			// Check Positions On Date is populated and before snapshot date
			if (account.getInceptionDate() != null) {
				if (DateUtils.compare(account.getInceptionDate(), bbs.getSnapshotDate(), false) <= 0) {
					// Finally - Check if account was in Active status on the snapshot date
					List<WorkflowHistory> accountHistoryList = context.getClientAccountWorkflowHistory(account);
					for (WorkflowHistory history : accountHistoryList) {
						if (history.isActiveOnDate(bbs.getSnapshotDate()) && (WorkflowStatus.STATUS_ACTIVE.equals(history.getEndWorkflowState().getStatus().getName()))) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////            Getter and Setter Methods              ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
