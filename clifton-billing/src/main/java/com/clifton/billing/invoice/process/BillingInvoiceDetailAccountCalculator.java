package com.clifton.billing.invoice.process;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceDetailAccountCalculator</code> is responsible for allocating a BillingInvoiceDetail record to the accounts.
 *
 * @author manderson
 */
public interface BillingInvoiceDetailAccountCalculator {

	/**
	 * Creates a list of {@link BillingInvoiceDetailAccount} records that allocates the BillingInvoiceDetail amount to accounts.
	 *
	 * @param invoiceAllocationPercentage  - For shared invoices, this would be the percentage of the total that applies to this invoice.  Need this because the billing basis on the invoice is listed as the total, but the billing amount is prorated at this percentage.
	 * @param proportionalSplitAccountList - When billing basis map isn't used, the billing amount is split evenly across this list of accounts
	 */
	public List<BillingInvoiceDetailAccount> calculateBillingInvoiceDetailAccountList(BillingInvoice invoice, BillingInvoiceDetail invoiceDetail, BigDecimal invoiceAllocationPercentage,
	                                                                                  BillingBasisDetail billingBasisDetail, Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap, List<InvestmentAccount> proportionalSplitAccountList);
}
