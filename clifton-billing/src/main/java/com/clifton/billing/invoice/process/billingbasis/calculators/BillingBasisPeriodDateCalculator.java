package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseBillingBasisPeriodDateCalculator</code> supports calculation types for Period End or Period Start based on configuration
 *
 * @author Mary Anderson
 */
public class BillingBasisPeriodDateCalculator extends BaseBillingBasisCalculator {

	/**
	 * If true: Use values from the last day of the billing period, else first day
	 */
	private boolean periodEnd;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isProjectedRevenueSupported() {
		return true;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		Date date = getBillingValueDate(billingDefinitionInvestmentAccount, defaultStart, defaultEnd);
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);

		if (!calculator.isValidValuationDate(date)) {
			date = calculator.getNextValuationDate(date, isMoveDateForward());
		}
		return calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), date, date);
	}


	private Date getBillingValueDate(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		// Billing Basis End Date
		if (isPeriodEnd()) {
			return getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd);
		}
		return getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart);
	}


	private boolean isMoveDateForward() {
		return !isPeriodEnd();
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		Date date = getBillingValueDate(billingDefinitionInvestmentAccount, startDate, endDate);
		List<BillingBasisSnapshot> valueList = getBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, startDate, endDate);

		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);
		if (!StringUtils.isEmpty(dataSourceName)) {
			BigDecimal exchangeRate = getBillingBasisSnapshotExchangeRate(invoice, billingDefinitionInvestmentAccount, dataSourceName, date);
			for (BillingBasisSnapshot sn : CollectionUtils.getIterable(valueList)) {
				// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
				// If missing only, then use system determined rate
				if (MathUtils.isNullOrZero(sn.getFxRate())) {
					sn.setFxRate(exchangeRate);
				}
			}
		}

		// There is only one date's value in the list
		BigDecimal value = getBillingBasisTotalFromList(valueList);
		if (value == null) {
			value = BigDecimal.ZERO;
		}
		getBillingBasisService().saveBillingBasisSnapshotList(valueList);

		// If account's billing basis was from a partial period, then need to prorate this account's billing basis
		BigDecimal days = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, endDate),
				getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, startDate)));
		BigDecimal totalDays = getBillingTotalDays(startDate, endDate);
		if (!MathUtils.isEqual(totalDays, days)) {
			value = CoreMathUtils.getValueProrated(days, totalDays, value);
		}
		// Ensure Final Billing Basis Total is Positive Value
		return new BillingBasis(startDate, endDate, MathUtils.abs(value));
	}


	private BigDecimal getBillingTotalDays(Date startDate, Date endDate) {
		if (isProjectedRevenue()) {
			Date projectedStartDate = DateUtils.getFirstDayOfMonth(getProjectedRevenueDate());
			if (!DateUtils.isDateAfter(projectedStartDate, startDate)) {
				projectedStartDate = startDate;
			}
			Date projectedEndDate = getProjectedRevenueDate();
			if (DateUtils.isDateBefore(endDate, projectedEndDate, false)) {
				projectedEndDate = endDate;
			}
			return BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(projectedEndDate, projectedStartDate));
		}
		return BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(endDate, startDate));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPeriodEnd() {
		return this.periodEnd;
	}


	public void setPeriodEnd(boolean periodEnd) {
		this.periodEnd = periodEnd;
	}
}
