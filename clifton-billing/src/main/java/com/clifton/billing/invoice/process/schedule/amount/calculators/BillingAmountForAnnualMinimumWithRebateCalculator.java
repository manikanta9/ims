package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingScheduleType;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>BillingAmountForAnnualMinimumWithRebateCalculator</code> works similar to the annual minimum, but if annual minimum is surpassed will rebate client for overages that were billed due to other minimum fees.
 * <p>
 * <p>
 * Example: Annual Minimum of 75,000, Quarterly Minimum of 18,750
 * Quarter 1 the quarterly minimum added 10,000 to the invoice.  By quarter 3 we've charged 80,000.  On that invoice the client would be credited 5,000
 * which brings the account back to 75,000 minimum and applied the 5,000 discount from the 10,000 quarterly minimum applied.  If, by the last quarter of the year
 * we've now charged the client 100,000, then they can get at most another 5,000 discount, since they've only paid 10,000 in minimum fees that year.
 * http://jira/browse/BILLING-65
 *
 * @author manderson
 */
public class BillingAmountForAnnualMinimumWithRebateCalculator extends BillingAmountForAnnualBoundaryCalculator {

	@Override
	public void validateBillingScheduleType(BillingScheduleType scheduleType) {
		super.validateBillingScheduleType(scheduleType);
		ValidationUtils.assertTrue(scheduleType.isMinimumFee(), "Selected calculator supports Minimum Fees only.");
	}


	@Override
	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		ValidationUtils.assertTrue(BillingFrequencies.ANNUALLY == scheduleConfig.getSchedule().getScheduleFrequency().getBillingFrequency(),
				"Cannot calculate Annual Boundaries for a schedule that isn't set up with an annual frequency");

		StringBuilder note = new StringBuilder(16);

		BigDecimal amount = calculateAmount(scheduleConfig, null, note);

		BigDecimal feeBilled = BigDecimal.ZERO;
		BigDecimal billingAmount = BigDecimal.ZERO;
		BigDecimal minimumBilled = BigDecimal.ZERO;
		BigDecimal minimumRebate = BigDecimal.ZERO;

		List<BillingInvoice> annualInvoiceList = getAnnualInvoiceList(scheduleConfig, false);
		for (BillingInvoice invoice : CollectionUtils.getIterable(annualInvoiceList)) {
			for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
				BigDecimal amt = invoiceDetail.getBillingAmount();
				// Check for Billing Schedule Not Null - If it's null then it is a manual invoice adjustment
				if (invoiceDetail.getBillingSchedule() != null && invoiceDetail.getBillingSchedule().getScheduleType().isMinimumFee()) {
					if (!invoiceDetail.getBillingSchedule().equals(scheduleConfig.getSchedule()) || MathUtils.isLessThan(amt, BigDecimal.ZERO)) {
						minimumBilled = MathUtils.add(minimumBilled, amt);
					}
				}
				// Otherwise - add it to the total amount billed
				else {
					feeBilled = MathUtils.add(feeBilled, amt);
				}
			}
		}

		BigDecimal totalBilled = MathUtils.add(feeBilled, minimumBilled);

		// If we didn't hit the annual minimum, then add and done
		if (MathUtils.isLessThan(totalBilled, amount)) {
			// AS LONG AS not a mid-period invoice
			if (!scheduleConfig.isLastInvoiceForAnnualInvoicePeriod()) {
				note.append("Mid-Year Invoice.  Current Annual Invoice Period Ends on ").append(DateUtils.fromDateShort(scheduleConfig.getAnnualInvoicePeriodEndDate()));
				addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
				return;
			}
			billingAmount = MathUtils.subtract(amount, totalBilled);
		}
		// If we hit the annual minimum, or went over, we need to credit other minimums back if they apply
		else if (MathUtils.isGreaterThanOrEqual(totalBilled, amount)) {
			BigDecimal over = MathUtils.subtract(totalBilled, amount);
			if (MathUtils.isLessThan(minimumBilled, over)) {
				minimumRebate = minimumBilled;
			}
			else {
				minimumRebate = over;
			}
			billingAmount = MathUtils.negate(minimumRebate);
		}
		appendBoundaryNote(scheduleConfig, true, note, amount, totalBilled);

		if (!MathUtils.isNullOrZero(minimumRebate)) {
			note.append(StringUtils.NEW_LINE + StringUtils.TAB + "Credit for Previous Minimum Fees: ").append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, minimumRebate, false));
		}
		applyBillingAmountToInvoices(config, scheduleConfig, annualInvoiceList, totalBilled, billingAmount, note);
	}


	@Override
	protected boolean isIncludeInvoiceDetailInTotalBilled(@SuppressWarnings("unused") BillingInvoiceDetail detail, @SuppressWarnings("unused") BillingScheduleProcessConfig scheduleConfig) {
		return true;
	}
}
