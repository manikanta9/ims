package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The BillingBasisPeriodAverageCalculator calculates the billing basis as the average of each month's average.
 * <p>
 */
public class BillingBasisPeriodAverageCalculator extends BaseBillingBasisCalculator {


	/**
	 * This calculator is NOT intended to be used for Projected Revenue.  The Period Average Projected Calculator should be used instead!
	 */
	@Override
	public boolean isProjectedRevenueSupported() {
		return false;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		Date startDate = getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart);
		Date endDate = getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd);

		// Get all values for the date range
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		return calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), startDate, endDate);
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		// Get values
		List<BillingBasisSnapshot> valueList = getBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, startDate, endDate);
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
		// Uses Data Source for the M2M account relationship issuing company, else default.
		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);
		Date date = startDate;

		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		if (!calculator.isValidValuationDate(date)) {
			date = calculator.getNextValuationDate(date, true);
		}

		// Split averages into months and then average the monthly data
		List<BigDecimal> notionalValueList = new ArrayList<>();
		List<BigDecimal> currentMonthValueList = new ArrayList<>();
		int currentMonth = DateUtils.getMonthOfYear(date);

		List<BillingBasisSnapshot> filteredValueList = new ArrayList<>();
		boolean external = billingDefinitionInvestmentAccount.getBillingBasisValuationType().isExternalBillingBasis();

		while (DateUtils.compare(date, endDate, false) <= 0) {
			List<BillingBasisSnapshot> dateFilteredList = filterBillingBasisSnapshotListByDate(valueList, date);
			if (CollectionUtils.isEmpty(dateFilteredList)) {
				// If no data Only add zero row if not external
				// We don't skip holidays anymore as Portfolio runs and Snapshots are still generated
				// for holidays (generated for each weekday) so this will enforce consistency across accounts
				// to have values for each weekday of the period.
				// However, SPECIAL CASE FOR EXTERNAL - Skip Dates not available
				// This way for cases where we have to match a bank statement we have the same days and data that was sent to us
				// If it's missing and not external, the zero will be used and would indicate that a snapshot or run needs to be built for that date
				if (!external) {
					// Otherwise Create a 0 Record Row for that date
					dateFilteredList = CollectionUtils.createList(new BillingBasisSnapshot(invoice, billingDefinitionInvestmentAccount, date));
				}
			}
			if (!CollectionUtils.isEmpty(dateFilteredList)) {
				// DataSourceName will only be populated if we need to lookup exchange rate
				if (!StringUtils.isEmpty(dataSourceName)) {
					BigDecimal exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName,
							billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), date).flexibleLookup());
					for (BillingBasisSnapshot sn : CollectionUtils.getIterable(dateFilteredList)) {
						// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
						// If missing only, then use system determined rate
						if (MathUtils.isNullOrZero(sn.getFxRate())) {
							sn.setFxRate(exchangeRate);
						}
					}
				}
				BigDecimal value = getBillingBasisTotalFromList(dateFilteredList);
				filteredValueList.addAll(dateFilteredList);
				currentMonthValueList.add(value);
			}

			// Move to next valuation date
			date = calculator.getNextValuationDate(date, true);

			// If crossed to next month add monthly average to final list
			// Or have now moved past the end date
			if (DateUtils.getMonthOfYear(date) != currentMonth || DateUtils.compare(date, endDate, false) > 0) {
				notionalValueList.add(CoreMathUtils.average(currentMonthValueList));
				currentMonthValueList = new ArrayList<>();
				currentMonth = DateUtils.getMonthOfYear(date);
			}
		}

		getBillingBasisService().saveBillingBasisSnapshotList(filteredValueList);

		// Pass the number of months we have averages for
		// Ensure Final Billing Basis Total is Positive Value
		BigDecimal average = MathUtils.abs(CoreMathUtils.average(notionalValueList));
		return new BillingBasis(startDate, endDate, average);
	}
}
