package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;

import java.util.Date;


/**
 * The <code>BillingBasisCalculator</code> interface must be implemented by various billing invoice billing basis calculator
 * Examples: Period Average, Period End, Period Start, Period Trade Dates
 * <p>
 * It calls the account's {@link com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator} that generates a list of {@link com.clifton.billing.billingbasis.BillingBasisSnapshot}
 * and then converts filters the list based on what applies and converts those into a single {@link BillingBasis} object
 *
 * @author vgomelsky
 */
public interface BillingBasisCalculator {

	/**
	 * To protect against any possible incorrect calculations when running for projected revenue - this option should be set to confirm the calculator has been adjusted to work  with projected revenue
	 */
	public boolean isProjectedRevenueSupported();


	/**
	 * Calculates and returns one BillingBasis object used to calculate schedule amounts.  Each BillingBasis object contains a list of start/end date, and final billing basis amount to use.
	 */
	public BillingBasis calculate(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate);
}
