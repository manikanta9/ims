package com.clifton.billing.invoice;

import java.math.BigDecimal;


/**
 * The <code>BillingInvoiceRevenueTypeAmountAware</code> is implemented by entities that can report on invoices with amounts in various buckets.
 * Having consistent getters to be implemented by each allows for consistent naming conventions throughout each object
 * <p>
 * Currently used for {@link BillingInvoice}, {@link BillingInvoiceDetail}, {@link BillingInvoiceDetailAccount}
 *
 * @author manderson
 */
public interface BillingInvoiceRevenueTypeAmountAware {


	/**
	 * InvoiceType.IsRevenue = true / RevenueType = REVENUE
	 */
	public BigDecimal getRevenueGrossTotalAmount();


	/**
	 * RevenueType = REVENUE_SHARE_EXTERNAL
	 */
	public BigDecimal getRevenueShareExternalTotalAmount();


	/**
	 * RevenueType = REVENUE_SHARE_INTERNAL
	 */
	public BigDecimal getRevenueShareInternalTotalAmount();


	/**
	 * RevenueGross + RevenueShareExternal
	 */
	public BigDecimal getRevenueNetTotalAmount();


	/**
	 * RevenueGross + RevenueShareExternal + RevenueShareInternal
	 */

	public BigDecimal getRevenueFinalTotalAmount();


	/**
	 * InvoiceType.IsRevenue = false / REVENUE TYPE = NONE
	 */
	public BigDecimal getOtherTotalAmount();
}
