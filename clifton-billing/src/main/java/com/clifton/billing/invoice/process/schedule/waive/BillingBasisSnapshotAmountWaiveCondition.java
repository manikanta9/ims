package com.clifton.billing.invoice.process.schedule.waive;

import com.clifton.billing.billingbasis.BillingBasisCalculationType;
import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * The <code>BillingBasisSnapshotAmountWaiveCondition</code> is a waive condition that evaluates
 * for the selected billing definition investment account(s) billing basis snapshot totals across each date
 * total amount is >, >=, =, <=, < specified amount.
 * <p>
 * Example: Sonoma Discount Schedule is Waived if billing basis for any date is < 60 million
 * <p>
 * This is different than the {@link BillingBasisAmountWaiveCondition} in that this validates against the snapshots, vs. the final result
 * i.e. Sonoma Average might be > 60 million, but on a few days the daily value may fall below 60 million
 *
 * @author manderson
 */
public class BillingBasisSnapshotAmountWaiveCondition extends BaseBillingAmountComparison implements Comparison<BillingSchedule> {

	private BillingBasisService billingBasisService;

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Can include any billing accounts for billing definitions included in the schedule
	 * If others are included, will require the BillingScheduleBillingDefinitionDependency exists
	 * <p>
	 * Also validates for each one selected that the {@link com.clifton.billing.billingbasis.BillingBasisCalculationType} is the same across
	 * all selections.  i.e. Can't compare values for Period Average and Period End
	 */
	private List<Integer> billingDefinitionInvestmentAccountIds;


	/**
	 * If true, then the first date it finds as true will return true
	 * Otherwise, will return true only if all dates evaluate to true
	 * We need this because waive conditions are when NOT to apply the schedule
	 * i.e. we need to be able to say if amount < 60 million on ANY date then return true to Waive the schedule (i.e. don't apply the schedule)
	 */
	private boolean evaluateToTrueIfAnyDateTrue;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(BillingSchedule schedule, ComparisonContext context) {
		validateBillingDefinitionInvestmentAccounts(schedule);
		Map<Date, BigDecimal> billingBasisSnapshotMap = getBillingBasisSnapshotTotalMap(schedule.getWaiveSystemConditionConfig().getInvoice());
		return evaluateForBillingBasisSnapshotMap(billingBasisSnapshotMap, context);
	}


	/**
	 * Given a map of Dates to Total Values for each date - evaluates the condition
	 */
	protected boolean evaluateForBillingBasisSnapshotMap(Map<Date, BigDecimal> billingBasisSnapshotMap, ComparisonContext context) {
		Date firstTrueDate = null;
		Date firstFalseDate = null;
		for (Map.Entry<Date, BigDecimal> dateBigDecimalEntry : billingBasisSnapshotMap.entrySet()) {
			if (!evaluateCondition(dateBigDecimalEntry.getValue())) {
				firstFalseDate = dateBigDecimalEntry.getKey();
			}
			else if (isEvaluateToTrueIfAnyDateTrue()) {
				firstTrueDate = dateBigDecimalEntry.getKey();
			}
			// Stop after finding one true value (if all we are looking for) or one false value (if we need all values to be true)
			if (firstTrueDate != null || (firstFalseDate != null && !isEvaluateToTrueIfAnyDateTrue())) {
				break;
			}
		}


		boolean result = (firstTrueDate != null || firstFalseDate == null);
		if (context != null) {
			if (result) {
				context.recordTrueMessage("Billing Basis Snapshot Total " + (firstTrueDate != null ? ("[" + CoreMathUtils.formatNumberMoney(billingBasisSnapshotMap.get(firstTrueDate)) + "] on " + DateUtils.fromDateShort(firstTrueDate) + " is ") : "on all dates are ") + getComparisonType().getComparisonExpression() + " " + CoreMathUtils.formatNumberMoney(getAmount()));
			}
			else {
				context.recordFalseMessage("Billing Basis Snapshot Total " + (isEvaluateToTrueIfAnyDateTrue() ? "on all dates are " : ("[" + CoreMathUtils.formatNumberMoney(billingBasisSnapshotMap.get(firstFalseDate)) + "] on " + DateUtils.fromDateShort(firstFalseDate) + " is ")) + "not " + getComparisonType().getComparisonExpression() + " ["
						+ CoreMathUtils.formatNumberMoney(getAmount()) + "].");
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Map<Date, BigDecimal> getBillingBasisSnapshotTotalMap(BillingInvoice invoice) {
		// Use a Tree Map here so the dates are in chronological order
		Map<Date, BigDecimal> dateValueMap = new TreeMap<>();

		BillingBasisSnapshotSearchForm searchForm = new BillingBasisSnapshotSearchForm();
		searchForm.setBillingDefinitionInvestmentAccountIds(getBillingDefinitionInvestmentAccountIds().toArray(new Integer[getBillingDefinitionInvestmentAccountIds().size()]));
		searchForm.addSearchRestriction(new SearchRestriction("snapshotDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, invoice.getBillingBasisStartDate()));
		searchForm.addSearchRestriction(new SearchRestriction("snapshotDate", ComparisonConditions.LESS_THAN_OR_EQUALS, invoice.getBillingBasisEndDate()));
		searchForm.setExcludeInvoiceWorkflowStatusName(WorkflowStatus.STATUS_CANCELED); // Always exclude Voided invoices
		List<BillingBasisSnapshot> snapshotList = getBillingBasisService().getBillingBasisSnapshotList(searchForm);

		// Convert it to a map of dates to list of snapshots for that date
		Map<Date, List<BillingBasisSnapshot>> dateListMap = BeanUtils.getBeansMap(snapshotList, BillingBasisSnapshot::getSnapshotDate);
		// Then, for each date - map those snapshots to each billing definition investment account
		for (Map.Entry<Date, List<BillingBasisSnapshot>> dateListEntry : dateListMap.entrySet()) {
			BigDecimal dateValue = BigDecimal.ZERO;
			Map<BillingDefinitionInvestmentAccount, List<BillingBasisSnapshot>> accountDateListMap = BeanUtils.getBeansMap(dateListEntry.getValue(), BillingBasisSnapshot::getBillingDefinitionInvestmentAccount);
			// For each account - add the ABS of the sum
			for (Map.Entry<BillingDefinitionInvestmentAccount, List<BillingBasisSnapshot>> billingDefinitionInvestmentAccountListEntry : accountDateListMap.entrySet()) {
				dateValue = MathUtils.add(dateValue, MathUtils.abs(CoreMathUtils.sumProperty(billingDefinitionInvestmentAccountListEntry.getValue(), BillingBasisSnapshot::getSnapshotValueInBillingBaseCurrency)));
			}
			dateValueMap.put(dateListEntry.getKey(), dateValue);
		}
		return dateValueMap;
	}


	private Set<BillingDefinitionInvestmentAccount> validateBillingDefinitionInvestmentAccounts(BillingSchedule schedule) {
		ValidationUtils.assertNotEmpty(getBillingDefinitionInvestmentAccountIds(), "No Billing Definition Accounts Selected.");
		Set<BillingDefinitionInvestmentAccount> billingAccountSet = new HashSet<>();
		BillingBasisCalculationType calculationType = null;
		for (Integer billingAccountId : getBillingDefinitionInvestmentAccountIds()) {
			BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingAccountId);
			billingAccountSet.add(billingDefinitionInvestmentAccount);
			if (calculationType == null) {
				calculationType = billingDefinitionInvestmentAccount.getBillingBasisCalculationType();
			}
			else if (!calculationType.equals(billingDefinitionInvestmentAccount.getBillingBasisCalculationType())) {
				throw new ValidationException("You must use billing accounts that use the same calculation type.  Found " + calculationType.getLabel() + " and " + billingDefinitionInvestmentAccount.getBillingBasisCalculationType().getLabel());
			}
		}
		validatePrerequisiteBillingDefinitionInvestmentAccounts(schedule, billingAccountSet);
		return billingAccountSet;
	}


	private void validatePrerequisiteBillingDefinitionInvestmentAccounts(BillingSchedule schedule, Set<BillingDefinitionInvestmentAccount> billingAccountSet) {
		List<BillingScheduleBillingDefinitionDependency> scheduleDependencyList = getBillingDefinitionService().getBillingScheduleBillingDefinitionDependencyListBySchedule(schedule.getId());
		for (BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount : CollectionUtils.getIterable(billingAccountSet)) {
			// If it's included on the schedule then don't check for dependency
			if (billingDefinitionInvestmentAccount.getReferenceOne().equals(schedule.getBillingDefinition()) || (!CollectionUtils.isEmpty(BeanUtils.filter(getBillingDefinitionService().getBillingScheduleSharingListForSchedule(schedule.getId()), BillingScheduleSharing::getReferenceTwo, billingDefinitionInvestmentAccount.getReferenceOne())))) {
				continue;
			}
			boolean found = false;
			for (BillingScheduleBillingDefinitionDependency dependency : CollectionUtils.getIterable(scheduleDependencyList)) {
				if (dependency.getBillingDefinitionInvestmentAccount() != null) {
					if (dependency.getBillingDefinitionInvestmentAccount().equals(billingDefinitionInvestmentAccount)) {
						found = true;
						break;
					}
				}
				if (dependency.getBillingDefinition().equals(billingDefinitionInvestmentAccount.getReferenceOne())) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new ValidationException("Billing Schedule [" + schedule.getLabel() + "]'s waive condition is expecting to use billing accounts from a prerequisite invoice for " + billingDefinitionInvestmentAccount.getLabel() + " however the schedule dependency is not defined for that billing definition or account.");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                 Getter and Setter Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public List<Integer> getBillingDefinitionInvestmentAccountIds() {
		return this.billingDefinitionInvestmentAccountIds;
	}


	public void setBillingDefinitionInvestmentAccountIds(List<Integer> billingDefinitionInvestmentAccountIds) {
		this.billingDefinitionInvestmentAccountIds = billingDefinitionInvestmentAccountIds;
	}


	public boolean isEvaluateToTrueIfAnyDateTrue() {
		return this.evaluateToTrueIfAnyDateTrue;
	}


	public void setEvaluateToTrueIfAnyDateTrue(boolean evaluateToTrueIfAnyDateTrue) {
		this.evaluateToTrueIfAnyDateTrue = evaluateToTrueIfAnyDateTrue;
	}
}
