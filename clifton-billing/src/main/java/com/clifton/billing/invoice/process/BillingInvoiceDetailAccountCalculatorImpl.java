package com.clifton.billing.invoice.process;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceDetailAccountCalculatorImpl</code> provides basic functionality for allocating
 * the BillingInvoiceDetail to accounts (i.e. creates a list of BillingInvoiceDetailAccounts).
 * <p>
 * This can be overridden to provide specific functionality - i.e. overridden in IMS project so that proportional split
 * across accounts only applies the fees to accounts that actually have positions on.
 *
 * @author manderson
 */
@Component
public class BillingInvoiceDetailAccountCalculatorImpl implements BillingInvoiceDetailAccountCalculator {

	@Override
	public List<BillingInvoiceDetailAccount> calculateBillingInvoiceDetailAccountList(BillingInvoice invoice, BillingInvoiceDetail invoiceDetail, BigDecimal invoiceAllocationPercentage,
	                                                                                  BillingBasisDetail billingBasisDetail, Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap, List<InvestmentAccount> proportionalSplitAccountList) {

		// For account allocations - round percentages to 4 decimals and billing amounts to the penny
		List<BillingInvoiceDetailAccount> detailAccountList = new ArrayList<>();
		if (billingBasisMap == null || billingBasisMap.isEmpty()) {
			List<InvestmentAccount> filteredList = filterProportionalSplitAccountList(invoice, proportionalSplitAccountList);
			for (InvestmentAccount acct : CollectionUtils.getIterable(filteredList)) {
				BillingInvoiceDetailAccount da = new BillingInvoiceDetailAccount();
				da.setBillingBasis(BigDecimal.ZERO);
				da.setInvestmentAccount(acct);
				da.setInvoiceDetail(invoiceDetail);
				da.setAllocationPercentage(MathUtils.round(CoreMathUtils.getPercentValue(BigDecimal.ONE, BigDecimal.valueOf(CollectionUtils.getSize(filteredList)), true), 4));
				da.setBillingAmount(MathUtils.round(MathUtils.getPercentageOf(da.getAllocationPercentage(), invoiceDetail.getBillingAmount(), true), 2));
				detailAccountList.add(da);
			}
		}
		else {
			BigDecimal invoiceBillingBasis = invoiceDetail.getBillingBasis();
			if (!MathUtils.isNullOrZero(invoiceAllocationPercentage) && !MathUtils.isEqual(invoiceAllocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
				invoiceBillingBasis = MathUtils.getPercentageOf(invoiceAllocationPercentage, invoiceBillingBasis, true);
			}
			for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> billingDefinitionInvestmentAccountBillingBasisEntry : billingBasisMap.entrySet()) {
				if (billingDefinitionInvestmentAccountBillingBasisEntry.getValue().getBillingBasisDetail(billingBasisDetail.getStartDate(), billingBasisDetail.getEndDate()) != null) {
					BillingInvoiceDetailAccount da = new BillingInvoiceDetailAccount();
					da.setBillingBasis(billingDefinitionInvestmentAccountBillingBasisEntry.getValue().getBillingBasisDetail(billingBasisDetail.getStartDate(), billingBasisDetail.getEndDate()).getTotalAmount());
					da.setBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountBillingBasisEntry.getKey());
					da.setInvestmentAccount((billingDefinitionInvestmentAccountBillingBasisEntry.getKey()).getReferenceTwo());
					da.setInvoiceDetail(invoiceDetail);
					da.setAllocationPercentage(MathUtils.round(CoreMathUtils.getPercentValue(da.getBillingBasis(), invoiceBillingBasis, true), 4));
					da.setBillingAmount(MathUtils.round(MathUtils.getPercentageOf(da.getAllocationPercentage(), invoiceDetail.getBillingAmount(), true), 2));
					detailAccountList.add(da);
				}
			}
		}
		return detailAccountList;
	}


	protected List<InvestmentAccount> filterProportionalSplitAccountList(BillingInvoice invoice, List<InvestmentAccount> proportionalSplitAccountList) {
		// Default Functionality - Doesn't filter the list at all - just return it
		// IMS override will filter the list based on which accounts actually had positions on
		return proportionalSplitAccountList;
	}
}
