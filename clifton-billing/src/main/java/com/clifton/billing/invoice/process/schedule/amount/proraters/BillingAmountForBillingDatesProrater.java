package com.clifton.billing.invoice.process.schedule.amount.proraters;


import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.core.util.date.DateUtils;


/**
 * The <code>BillingAmountForBillingDatesProrater</code> prorates amounts based on the billing dates (billing definition start and end dates)
 */
public class BillingAmountForBillingDatesProrater extends BaseBillingAmountProrater {


	@Override
	protected void populateBillingAmountDateProraterConfig(BillingAmountDateProraterConfig proraterConfig, BillingScheduleProcessConfig scheduleConfig, BillingBasis.BillingBasisDetail billingBasisDetail) {
		if (scheduleConfig.getSchedule().getScheduleType().isAnnualFee()) {
			proraterConfig.setProrateStartDate(scheduleConfig.getAnnualInvoicePeriodStartDate());
			proraterConfig.setProrateEndDate(scheduleConfig.getAnnualInvoicePeriodEndDate());
		}
		else {
			proraterConfig.setProrateStartDate(scheduleConfig.getInvoiceBillingStartDate());
			if (scheduleConfig.getAccrualStartDate() != null && DateUtils.isDateAfter(scheduleConfig.getAccrualStartDate(), proraterConfig.getProrateStartDate())) {
				proraterConfig.setProrateStartDate(scheduleConfig.getAccrualStartDate());
			}
			proraterConfig.setProrateEndDate(scheduleConfig.getInvoiceBillingEndDate());
			if (scheduleConfig.getAccrualEndDate() != null && DateUtils.isDateBefore(scheduleConfig.getAccrualEndDate(), proraterConfig.getProrateEndDate(), false)) {
				proraterConfig.setProrateEndDate(scheduleConfig.getAccrualEndDate());
			}
		}
	}
}
