package com.clifton.billing.invoice.process.schedule.amount;


/**
 * The <code>BillingAmountCalculatorTypes</code> defines the various schedule types we have in more generic classifications.
 * i.e. ANNUAL_BOUNDARY can be used by schedule types for Annual Minimum, Annual Maximum, Annual Minimum with Rebates, etc.
 *
 * @author manderson
 */
public enum BillingAmountCalculatorTypes {

	TIERED(true), //
	TIERED_FLAT_PERCENTAGE(true, true, true, false, false), //
	PERCENTAGE(false), //
	FLAT_FEE(true, false, false, false), //
	FLAT_PERCENTAGE(true, true, false, false), //
	BOUNDARY(true, false, true, false), //
	ANNUAL_BOUNDARY(true, false, true, true); //

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	BillingAmountCalculatorTypes(boolean tiered) {
		this(false, false, tiered, false, false);
	}


	BillingAmountCalculatorTypes(boolean flatFee, boolean flatFeePercent, boolean boundary, boolean annual) {
		this(flatFee, flatFeePercent, false, boundary, annual);
	}


	BillingAmountCalculatorTypes(boolean flatFee, boolean flatFeePercent, boolean tiered, boolean boundary, boolean annual) {
		this.flatFee = flatFee;
		this.flatFeePercent = flatFeePercent;
		this.tiered = tiered;
		this.boundary = boundary;
		this.annualFee = annual;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Minimum or Maximum Fee
	 */
	private final boolean boundary;

	/**
	 * Flat fee - 5,000 per month
	 */
	private final boolean flatFee;

	/**
	 * Flat Percentage, i.e. 10% Discount = 10% whether it's Annual, Monthly, Quarterly.
	 * vs. 0.05% Annual = 0.05% / 12 Monthly Rate
	 */
	private final boolean flatFeePercent;

	/**
	 * The fee ends up being a blended fee where each tier is applied at different rate.  The higher the tiers the lower the rate.
	 */
	private final boolean tiered;

	/**
	 * Annual Minimum / Annual Maximum - We track this to confirm we can calculate the annual period for the invoice and also retrieve billing information for previous invoices within the annual period.
	 */
	private final boolean annualFee;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isBoundary() {
		return this.boundary;
	}


	public boolean isFlatFee() {
		return this.flatFee;
	}


	public boolean isFlatFeePercent() {
		return this.flatFeePercent;
	}


	public boolean isTiered() {
		return this.tiered;
	}


	public boolean isAnnualFee() {
		return this.annualFee;
	}
}
