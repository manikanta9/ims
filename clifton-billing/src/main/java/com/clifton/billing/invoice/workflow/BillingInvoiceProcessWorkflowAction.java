package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>BillingInvoiceProcessWorkflowAction</code> handles calling the service method to re-process an invoice
 *
 * @author Mary Anderson
 */
public class BillingInvoiceProcessWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {

	private BillingInvoiceProcessService billingInvoiceProcessService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice invoice, WorkflowTransition transition) {
		getBillingInvoiceProcessService().processBillingInvoice(invoice.getId());
		return invoice;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceProcessService getBillingInvoiceProcessService() {
		return this.billingInvoiceProcessService;
	}


	public void setBillingInvoiceProcessService(BillingInvoiceProcessService billingInvoiceProcessService) {
		this.billingInvoiceProcessService = billingInvoiceProcessService;
	}
}
