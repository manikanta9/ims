package com.clifton.billing.invoice;


import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.validation.BillingDefinitionValidationUtils;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceDetailSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceTypeSearchForm;
import com.clifton.billing.invoice.workflow.BillingInvoiceGenerateRevisionWorkflowAction;
import com.clifton.billing.payment.BillingPaymentAllocation;
import com.clifton.core.converter.string.StringToBigDecimalConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.MathComplexExpression;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class BillingInvoiceServiceImpl implements BillingInvoiceService {

	private AdvancedUpdatableDAO<BillingInvoiceType, Criteria> billingInvoiceTypeDAO;
	private AdvancedUpdatableDAO<BillingInvoice, Criteria> billingInvoiceDAO;
	private AdvancedUpdatableDAO<BillingInvoiceDetail, Criteria> billingInvoiceDetailDAO;
	private AdvancedUpdatableDAO<BillingInvoiceDetailAccount, Criteria> billingInvoiceDetailAccountDAO;
	private AdvancedUpdatableDAO<BillingInvoiceGroup, Criteria> billingInvoiceGroupDAO;

	private DaoNamedEntityCache<BillingInvoiceType> billingInvoiceTypeCache;

	private BillingBasisService billingBasisService;


	private WorkflowDefinitionService workflowDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	////////             Billing Invoice Type Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoiceType getBillingInvoiceType(short id) {
		return getBillingInvoiceTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BillingInvoiceType getBillingInvoiceTypeByName(String name) {
		return getBillingInvoiceTypeCache().getBeanForKeyValue(getBillingInvoiceTypeDAO(), name);
	}


	@Override
	public List<BillingInvoiceType> getBillingInvoiceTypeList(BillingInvoiceTypeSearchForm searchForm) {
		return getBillingInvoiceTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BillingInvoiceType saveBillingInvoiceType(BillingInvoiceType bean) {
		return getBillingInvoiceTypeDAO().save(bean);
	}


	@Override
	public void deleteBillingInvoiceType(short id) {
		getBillingInvoiceTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Billing Invoice Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice getBillingInvoice(int id) {
		BillingInvoice invoice = getBillingInvoiceDAO().findByPrimaryKey(id);
		if (invoice != null) {
			// If invoice was voided, see if there are any revisions entered for it
			if (invoice.isCanceled()) {
				invoice.setChildrenList(getBillingInvoiceDAO().findByField("parent.id", id));
			}
		}
		return invoice;
	}


	@Override
	public BillingInvoice getBillingInvoicePopulated(int id) {
		BillingInvoice invoice = getBillingInvoice(id);
		if (invoice != null) {
			invoice.setDetailList(getBillingInvoiceDetailListByInvoice(id));
		}
		return invoice;
	}


	@Override
	public List<BillingInvoice> getBillingInvoiceList(final BillingInvoiceSearchForm searchForm) {
		// This needs to run BEFORE so we don't lose the search restriction comparison value in the restriction list
		final Map<String, BigDecimal> unpaidAmountRestrictionMap = getUnpaidAmountRestrictionMap(searchForm);

		if (searchForm.getRevenueType() != null) {
			searchForm.setRevenue(searchForm.getRevenueType().isRevenue());
			if (searchForm.getRevenueType().isRevenueShare()) {
				searchForm.addSearchRestriction(searchForm.getRevenueType().isExternal() ? "revenueShareExternalTotalAmount" : "revenueShareInternalTotalAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO);
			}
		}

		HibernateSearchFormConfigurer config = new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				for (Map.Entry<String, BigDecimal> unpaidAmountRestrictionEntry : unpaidAmountRestrictionMap.entrySet()) {
					criteria.add(new MathComplexExpression(new String[]{"+", "-"}, true, unpaidAmountRestrictionEntry.getKey(), new String[]{"totalAmount", "revenueShareExternalTotalAmount", "paidAmount"}, unpaidAmountRestrictionEntry.getValue()));
				}

				if (searchForm.getExcludePaymentAllocationPaymentId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(BillingPaymentAllocation.class, "bpa");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("billingInvoice", "bi");
					sub.createAlias("billingPayment", "bp");
					sub.add(Restrictions.eqProperty("bi.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("bp.id", searchForm.getExcludePaymentAllocationPaymentId()));
					criteria.add(Subqueries.notExists(sub));
				}
			}
		};
		return getBillingInvoiceDAO().findBySearchCriteria(config);
	}


	/**
	 * Returns a map of restrictions comparisons to their amounts.
	 * i.e. Can search for unpaid amount < 5000 and unpaid amount > 0
	 * If NOTHING in the restriction list, will look at the search form field
	 */
	private Map<String, BigDecimal> getUnpaidAmountRestrictionMap(BillingInvoiceSearchForm searchForm) {
		Map<String, BigDecimal> unpaidAmountRestrictionMap = new HashMap<>();
		for (SearchRestriction restriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			if (StringUtils.isEqual("unpaidAmount", restriction.getField())) {
				unpaidAmountRestrictionMap.put(restriction.getComparison().getComparisonExpression(), (new StringToBigDecimalConverter()).convert((String) restriction.getValue()));
			}
		}

		if (searchForm.getUnpaidAmount() != null) {
			unpaidAmountRestrictionMap.put(ComparisonConditions.EQUALS.getComparisonExpression(), searchForm.getUnpaidAmount());
		}
		return unpaidAmountRestrictionMap;
	}


	@Override
	@Transactional
	public BillingInvoice saveBillingInvoice(BillingInvoice bean) {
		if (bean.getBillingDefinition() != null) {
			bean.setBusinessCompany(bean.getBillingDefinition().getBusinessCompany());
		}

		List<BillingInvoiceDetail> detailList = bean.getDetailList();
		List<BillingInvoiceDetail> originalDetailList = null;
		if (!bean.isNewBean()) {
			originalDetailList = getBillingInvoiceDetailListByInvoice(bean.getId());
		}
		bean = getBillingInvoiceDAO().save(bean);
		for (BillingInvoiceDetail detail : CollectionUtils.getIterable(detailList)) {
			detail.setInvoice(bean);
		}
		saveBillingInvoiceDetailList(detailList, originalDetailList);
		bean.setDetailList(detailList);
		return bean;
	}


	/**
	 * Special method called from BillingPaymentAllocationObserver that ONLY updates the paid amount
	 * Cannot use general save method because we aren't touching the detail lines for the invoice at this point.
	 */
	@Override
	public void saveBillingInvoicePaidAmount(BillingInvoice bean, BigDecimal paidAmount) {
		bean.setPaidAmount(paidAmount);
		getBillingInvoiceDAO().save(bean);
	}


	/**
	 * Used for manual invoice detail records that are added/updated/removed to recalculate invoice total
	 */
	private void recalculateAndSaveBillingInvoiceTotalAmount(int invoiceId) {
		// Recalculate Invoice Total
		BillingInvoice invoice = getBillingInvoicePopulated(invoiceId);
		BillingInvoiceUtils.recalculateTotalAmountsForBillingInvoice(invoice, invoice.getDetailList());
		this.billingInvoiceDAO.save(invoice); // Just call dao method so we don't re-save all detail records, etc.
	}


	/**
	 * Special method called from {@link BillingInvoiceGenerateRevisionWorkflowAction} and {@link com.clifton.billing.invoice.workflow.BillingInvoiceGroupGenerateRevisionWorkflowAction}
	 * that ONLY updates the parent field of the invoice
	 * Cannot use general save method because we aren't touching the detail lines for the invoice at this point.
	 */
	@Override
	public void saveBillingInvoiceParent(BillingInvoice bean, BillingInvoice parentBean) {
		bean.setParent(parentBean);
		getBillingInvoiceDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteBillingInvoice(int id) {
		BillingInvoice invoice = getBillingInvoice(id);
		if (invoice != null) {
			// If the Invoice is In a Group - Delete the entire group
			if (invoice.getInvoiceGroup() != null) {
				throw new ValidationException("Invoice # " + id + " is a part of Invoice Group # " + invoice.getInvoiceGroup().getId()
						+ ".  You cannot delete a single invoice in a group, but must delete the entire invoice group.");
			}
			deleteBillingInvoiceImpl(invoice);
		}
	}


	private void deleteBillingInvoiceImpl(BillingInvoice invoice) {
		getBillingBasisService().deleteBillingBasisSnapshotListForInvoice(invoice.getId());
		for (BillingInvoiceDetail detail : CollectionUtils.getIterable(getBillingInvoiceDetailListByInvoice(invoice.getId()))) {
			deleteBillingInvoiceDetailImpl(detail);
		}
		getBillingInvoiceDAO().delete(invoice.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	/////////            Billing Invoice Detail Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoiceDetail getBillingInvoiceDetail(int id) {
		BillingInvoiceDetail bean = getBillingInvoiceDetailDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setDetailAccountList(getBillingInvoiceDetailAccountListByInvoiceDetail(id));
		}
		return bean;
	}


	@Override
	public List<BillingInvoiceDetail> getBillingInvoiceDetailList(BillingInvoiceDetailSearchForm searchForm) {
		return getBillingInvoiceDetailDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private List<BillingInvoiceDetail> getBillingInvoiceDetailListByInvoice(int invoiceId) {
		return getBillingInvoiceDetailDAO().findByField("invoice.id", invoiceId);
	}


	private void saveBillingInvoiceDetailList(List<BillingInvoiceDetail> newList, List<BillingInvoiceDetail> originalList) {
		for (BillingInvoiceDetail old : CollectionUtils.getIterable(originalList)) {
			if (newList == null || !newList.contains(old)) {
				deleteBillingInvoiceDetailImpl(old);
			}
		}

		for (BillingInvoiceDetail newDetail : CollectionUtils.getIterable(newList)) {
			saveBillingInvoiceDetailImpl(newDetail);
		}
	}


	@Override
	@Transactional
	public BillingInvoiceDetail saveBillingInvoiceDetail(BillingInvoiceDetail bean, Integer forceAllowPrecision) {
		ValidationUtils.assertTrue(bean.isManual(), "Only manual billing invoice detail records can be entered individually.");
		ValidationUtils.assertNotNull(bean.getBillingAmount(), "Billing Amount for the invoice detail is required.");
		ValidationUtils.assertNotNull(bean.getRevenueType(), "Revenue type is required for manual invoice details.");
		BillingDefinitionValidationUtils.validateRevenueTypeForBillingDefinition(bean.getInvoice().getBillingDefinition(), bean.getRevenueType());
		bean.setBillingBasis(BigDecimal.ZERO);
		if (bean.getInvoice().getInvoiceType().isBillingDefinitionRequired()) {
			ValidationUtils.assertFalse(CollectionUtils.isEmpty(bean.getDetailAccountList()), "Account breakdown for the detail record is required.");

			BigDecimal detailTotal = MathUtils.round(bean.getBillingAmount(), forceAllowPrecision != null ? forceAllowPrecision : bean.getInvoice().getInvoiceType().getBillingAmountPrecision().intValue());
			bean.setBillingAmount(detailTotal);
			BigDecimal accountTotal = BigDecimal.ZERO;
			for (BillingInvoiceDetailAccount detail : bean.getDetailAccountList()) {
				detail.setInvoiceDetail(bean);
				detail.setBillingBasis(BigDecimal.ZERO);
				ValidationUtils.assertNotNull(detail.getBillingAmount(), "Billing Amount for the account [" + detail.getAccountLabel() + "] is required.");
				detail.setAllocationPercentage(MathUtils.round(CoreMathUtils.getPercentValue(detail.getBillingAmount(), detailTotal, true), 4));
				accountTotal = MathUtils.add(accountTotal, detail.getBillingAmount());
			}
			// We allow account details to be entered with pennies, but not the total - to make it easier, if difference is less than one apply sum property difference to the account detail
			BigDecimal diff = MathUtils.subtract(detailTotal, accountTotal);
			if (!MathUtils.isNullOrZero(diff)) {
				if (MathUtils.isLessThan(MathUtils.abs(diff), BigDecimal.ONE)) {
					CoreMathUtils.applySumPropertyDifference(bean.getDetailAccountList(), BillingInvoiceDetailAccount::getBillingAmount, BillingInvoiceDetailAccount::setBillingAmount, detailTotal, true);
				}
				else {
					ValidationUtils.assertTrue(MathUtils.isEqual(detailTotal, accountTotal), "Invoice Detail Total Entered [" + CoreMathUtils.formatNumberInteger(detailTotal) + ", does not match total allocated to accounts [" + CoreMathUtils.formatNumberInteger(accountTotal) + "].");
				}
			}
			CoreMathUtils.applySumPropertyDifference(bean.getDetailAccountList(), BillingInvoiceDetailAccount::getAllocationPercentage, BillingInvoiceDetailAccount::setAllocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
		}
		bean = saveBillingInvoiceDetailImpl(bean);
		// Recalculate Invoice Total
		recalculateAndSaveBillingInvoiceTotalAmount(bean.getInvoice().getId());
		return bean;
	}


	private BillingInvoiceDetail saveBillingInvoiceDetailImpl(BillingInvoiceDetail bean) {
		List<BillingInvoiceDetailAccount> originalAccountList = null;
		if (!bean.isNewBean()) {
			originalAccountList = getBillingInvoiceDetailAccountListByInvoiceDetail(bean.getId());
		}
		List<BillingInvoiceDetailAccount> newAccountList = bean.getDetailAccountList();
		bean = getBillingInvoiceDetailDAO().save(bean);
		getBillingInvoiceDetailAccountDAO().saveList(newAccountList, originalAccountList);
		bean.setDetailAccountList(newAccountList);
		return bean;
	}


	@Override
	public void deleteBillingInvoiceDetail(int id) {
		BillingInvoiceDetail bean = getBillingInvoiceDetail(id);
		if (bean != null) {
			ValidationUtils.assertTrue(bean.isManual(), "Only manual billing invoice detail records can be deleted individually.");
			deleteBillingInvoiceDetailImpl(bean);
			recalculateAndSaveBillingInvoiceTotalAmount(bean.getInvoice().getId());
		}
	}


	private void deleteBillingInvoiceDetailImpl(BillingInvoiceDetail bean) {
		getBillingInvoiceDetailAccountDAO().deleteList(getBillingInvoiceDetailAccountListByInvoiceDetail(bean.getId()));
		getBillingInvoiceDetailDAO().delete(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////         Billing Invoice Detail Account Methods         ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingInvoiceDetailAccount> getBillingInvoiceDetailAccountListByInvoiceDetail(int invoiceDetailId) {
		return getBillingInvoiceDetailAccountDAO().findByField("invoiceDetail.id", invoiceDetailId);
	}


	@Override
	public List<BillingInvoiceDetailAccount> getBillingInvoiceDetailAccountList(BillingInvoiceDetailAccountSearchForm searchForm) {
		return getBillingInvoiceDetailAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////              Billing Invoice Group Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoiceGroup getBillingInvoiceGroup(int id) {
		BillingInvoiceGroup group = getBillingInvoiceGroupDAO().findByPrimaryKey(id);
		if (group != null) {
			group.setInvoiceList(getBillingInvoiceDAO().findByField("invoiceGroup.id", id));
		}
		return group;
	}


	@Override
	public BillingInvoiceGroup saveBillingInvoiceGroup(BillingInvoiceGroup bean) {
		return getBillingInvoiceGroupDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteBillingInvoiceGroup(int id) {
		// Try to First delete all the invoices
		BillingInvoiceGroup group = getBillingInvoiceGroup(id);
		for (BillingInvoice invoice : CollectionUtils.getIterable(group.getInvoiceList())) {
			deleteBillingInvoiceImpl(invoice);
		}
		getBillingInvoiceGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BillingInvoice, Criteria> getBillingInvoiceDAO() {
		return this.billingInvoiceDAO;
	}


	public void setBillingInvoiceDAO(AdvancedUpdatableDAO<BillingInvoice, Criteria> billingInvoiceDAO) {
		this.billingInvoiceDAO = billingInvoiceDAO;
	}


	public AdvancedUpdatableDAO<BillingInvoiceDetail, Criteria> getBillingInvoiceDetailDAO() {
		return this.billingInvoiceDetailDAO;
	}


	public void setBillingInvoiceDetailDAO(AdvancedUpdatableDAO<BillingInvoiceDetail, Criteria> billingInvoiceDetailDAO) {
		this.billingInvoiceDetailDAO = billingInvoiceDetailDAO;
	}


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public AdvancedUpdatableDAO<BillingInvoiceGroup, Criteria> getBillingInvoiceGroupDAO() {
		return this.billingInvoiceGroupDAO;
	}


	public void setBillingInvoiceGroupDAO(AdvancedUpdatableDAO<BillingInvoiceGroup, Criteria> billingInvoiceGroupDAO) {
		this.billingInvoiceGroupDAO = billingInvoiceGroupDAO;
	}


	public AdvancedUpdatableDAO<BillingInvoiceDetailAccount, Criteria> getBillingInvoiceDetailAccountDAO() {
		return this.billingInvoiceDetailAccountDAO;
	}


	public void setBillingInvoiceDetailAccountDAO(AdvancedUpdatableDAO<BillingInvoiceDetailAccount, Criteria> billingInvoiceDetailAccountDAO) {
		this.billingInvoiceDetailAccountDAO = billingInvoiceDetailAccountDAO;
	}


	public AdvancedUpdatableDAO<BillingInvoiceType, Criteria> getBillingInvoiceTypeDAO() {
		return this.billingInvoiceTypeDAO;
	}


	public void setBillingInvoiceTypeDAO(AdvancedUpdatableDAO<BillingInvoiceType, Criteria> billingInvoiceTypeDAO) {
		this.billingInvoiceTypeDAO = billingInvoiceTypeDAO;
	}


	public DaoNamedEntityCache<BillingInvoiceType> getBillingInvoiceTypeCache() {
		return this.billingInvoiceTypeCache;
	}


	public void setBillingInvoiceTypeCache(DaoNamedEntityCache<BillingInvoiceType> billingInvoiceTypeCache) {
		this.billingInvoiceTypeCache = billingInvoiceTypeCache;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
