package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasis</code> class is used ONLY during processing
 * The calculators take the BillingBasisSnapshot values, calculate a final billing basis for them
 * and it is stored in this class.  As each account is processed we can add these together to get the final
 * billing basis that is used on the invoice.
 * <p/>
 * The containing BillingBasisDetail class is used for the Date specific (i.e. Period Trade Date) calculations where values
 * are segmented and prorated according to a specified number of days.
 *
 * @author Mary Anderson
 */
public class BillingBasis {

	/**
	 * Period Trade Dates, even if one segment for entire period - always display date detail in invoice detail
	 */
	private boolean alwaysDisplayDateDetails = false;

	private final List<BillingBasisDetail> detailList = new ArrayList<>();


	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////


	public BillingBasis() {
		super();
	}


	public BillingBasis(Date startDate, Date endDate, BigDecimal amount) {
		this();
		addBillingBasis(startDate, endDate, amount);
	}


	public void addBillingBasis(BillingBasis billingBasis) {
		if (billingBasis.isAlwaysDisplayDateDetails()) {
			setAlwaysDisplayDateDetails(true);
		}
		for (BillingBasisDetail detail : CollectionUtils.getIterable(billingBasis.getDetailList())) {
			boolean found = false;
			for (BillingBasisDetail thisDetail : CollectionUtils.getIterable(getDetailList())) {
				if (thisDetail.equals(detail)) {
					found = true;
					thisDetail.addAmount(detail.getTotalAmount(), detail.getNote());
					break;
				}
			}
			if (!found) {
				this.detailList.add(new BillingBasisDetail(detail.getStartDate(), detail.getEndDate(), detail.getTotalAmount(), detail.getNote()));
			}
		}
	}


	private void addBillingBasisDetail(BillingBasisDetail detail) {
		// If exists - add amount to total amount
		BillingBasisDetail thisDetail = getBillingBasisDetail(detail.getStartDate(), detail.getEndDate());
		if (thisDetail != null) {
			thisDetail.addAmount(detail.getTotalAmount(), detail.getNote());
		}
		else {
			// Otherwise - create a new detail line
			this.detailList.add(detail);
		}
	}


	public void addBillingBasis(Date startDate, Date endDate, BigDecimal amount) {
		addBillingBasis(startDate, endDate, amount, null);
	}


	public void addBillingBasis(Date startDate, Date endDate, BigDecimal amount, String note) {
		ValidationUtils.assertNotNull(startDate, "Start Date is required to add billing basis - use Invoice Start/End Dates if not a subset of dates.");
		ValidationUtils.assertNotNull(endDate, "End Date is required to add billing basis - use Invoice Start/End Dates if not a subset of dates.");

		BillingBasisDetail d = new BillingBasisDetail(startDate, endDate, amount, note);
		addBillingBasisDetail(d);
	}


	public BillingBasisDetail getBillingBasisDetail(Date startDate, Date endDate) {
		BillingBasisDetail d = new BillingBasisDetail(startDate, endDate, BigDecimal.ZERO);
		for (BillingBasisDetail thisDetail : CollectionUtils.getIterable(getDetailList())) {
			if (thisDetail.equals(d)) {
				return thisDetail;
			}
		}
		return null; //return new BillingBasisDetail(startDate, endDate, BigDecimal.ZERO);
	}


	public BigDecimal getBillingBasisTotal() {
		return CoreMathUtils.sumProperty(this.detailList, BillingBasisDetail::getTotalAmount);
	}


	public Integer getBillingBasisPeriodCount() {
		return CollectionUtils.getSize(this.detailList);
	}


	public boolean isAlwaysDisplayDateDetails() {
		return this.alwaysDisplayDateDetails;
	}

	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////

	/**
	 * The <code>BillingBasisDetail</code> contains the billing basis amount for a specified date range.
	 */
	public class BillingBasisDetail {

		private final Date startDate;
		private final Date endDate;

		private BigDecimal totalAmount;

		private String note = "";


		private BillingBasisDetail(Date startDate, Date endDate, BigDecimal totalAmount) {
			this(startDate, endDate, totalAmount, null);
		}


		private BillingBasisDetail(Date startDate, Date endDate, BigDecimal totalAmount, String note) {
			this.startDate = startDate;
			this.endDate = endDate;
			this.totalAmount = totalAmount;
			if (!StringUtils.isEmpty(note)) {
				this.note = note;
			}
		}


		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof BillingBasisDetail)) {
				return false;
			}
			BillingBasisDetail billObj = (BillingBasisDetail) obj;
			if (DateUtils.compare(billObj.getStartDate(), this.startDate, false) == 0) {
				if (DateUtils.compare(billObj.getEndDate(), this.endDate, false) == 0) {
					return true;
				}
			}
			return false;
		}


		@Override
		public int hashCode() {
			return this.startDate.hashCode();
		}


		public String getDateLabel() {
			return DateUtils.fromDateRange(getStartDate(), getEndDate(), true);
		}


		public void addAmount(BigDecimal amount, String amountNote) {
			this.totalAmount = MathUtils.add(this.totalAmount, amount);
			if (!StringUtils.isEmpty(amountNote)) {
				if (this.note == null) {
					this.note = amountNote;
				}
				else {
					this.note += amountNote;
				}
			}
		}


		public Date getStartDate() {
			return this.startDate;
		}


		public Date getEndDate() {
			return this.endDate;
		}


		public BigDecimal getTotalAmount() {
			return this.totalAmount;
		}


		public String getNote() {
			return this.note;
		}
	}


	public List<BillingBasisDetail> getDetailList() {
		return this.detailList;
	}


	public void setAlwaysDisplayDateDetails(boolean alwaysDisplayDateDetails) {
		this.alwaysDisplayDateDetails = alwaysDisplayDateDetails;
	}
}
