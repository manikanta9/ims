package com.clifton.billing.invoice.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BillingInvoiceTypeSearchForm</code> ...
 *
 * @author manderson
 */
public class BillingInvoiceTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean revenue;

	@SearchField
	private Boolean billingDefinitionRequired;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean invoicePostedToPortalDefault;

	@SearchField
	private Short billingAmountPrecision;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getRevenue() {
		return this.revenue;
	}


	public void setRevenue(Boolean revenue) {
		this.revenue = revenue;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getBillingDefinitionRequired() {
		return this.billingDefinitionRequired;
	}


	public void setBillingDefinitionRequired(Boolean billingDefinitionRequired) {
		this.billingDefinitionRequired = billingDefinitionRequired;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Short getBillingAmountPrecision() {
		return this.billingAmountPrecision;
	}


	public void setBillingAmountPrecision(Short billingAmountPrecision) {
		this.billingAmountPrecision = billingAmountPrecision;
	}


	public Boolean getInvoicePostedToPortalDefault() {
		return this.invoicePostedToPortalDefault;
	}


	public void setInvoicePostedToPortalDefault(Boolean invoicePostedToPortalDefault) {
		this.invoicePostedToPortalDefault = invoicePostedToPortalDefault;
	}
}
