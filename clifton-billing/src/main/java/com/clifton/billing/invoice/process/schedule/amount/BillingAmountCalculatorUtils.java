package com.clifton.billing.invoice.process.schedule.amount;


import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleFrequencies;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>BillingAmountCalculatorUtils</code> contains common utility methods used when calculating schedule amounts.
 *
 * @author Mary Anderson
 */
public class BillingAmountCalculatorUtils {

	/**
	 * Returns the sum of billingAmounts for all existing details of the invoice
	 * Excludes Manual Adjustments
	 * If revenueTypeList is not empty, will limit to just those revenue types
	 */
	public static BigDecimal getBillingInvoiceTotalAmountCalculated(BillingInvoice invoice, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return BillingAmountCalculatorUtils.getBillingInvoiceTotalAmountCalculated(invoice, null, revenueTypeList);
	}


	/**
	 * Returns the sum of billingAmounts for all existing details of the invoice excluding the given schedule (if provided)
	 * Excludes Manual Adjustments
	 * If revenueTypeList is not empty, will limit to just those revenue types
	 */
	public static BigDecimal getBillingInvoiceTotalAmountCalculated(BillingInvoice invoice, BillingSchedule excludeSchedule, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return CollectionUtils.getStream(invoice.getDetailList())
				.filter(billingInvoiceDetail -> billingInvoiceDetail.getBillingSchedule() != null && (excludeSchedule == null || !excludeSchedule.equals(billingInvoiceDetail.getBillingSchedule())))
				.filter(billingInvoiceDetail -> CollectionUtils.isEmpty(revenueTypeList) || CollectionUtils.contains(revenueTypeList, billingInvoiceDetail.getCoalesceRevenueType()))
				.map(BillingInvoiceDetail::getBillingAmount)
				.reduce(MathUtils::add).orElse(BigDecimal.ZERO);
	}


	/**
	 * Converts the Amount defined as the frequency for the schedule to the frequency defined for the Invoice.
	 * <p>
	 * Examples:
	 * Schedule for Account Retainer is set up Monthly, but billed quarterly.  (Monthly Amount / 1) * 3 is returned for the Quarterly Bill.
	 * Schedule for Account Retainer is set up Annually, but billed quarterly.  (Annual Amount / 12) * 3 is returned for the Quarterly Bill.
	 */
	public static BigDecimal getBillingInvoiceAmountConvertedForFrequency(BillingScheduleProcessConfig scheduleConfig, BigDecimal amount) {
		BillingFrequencies invoiceFrequency = scheduleConfig.getBillingFrequency();
		BillingScheduleFrequencies scheduleFrequency = scheduleConfig.getSchedule().getScheduleFrequency();

		// One time fee - never prorate this
		if (BillingScheduleFrequencies.ONCE == scheduleFrequency) {
			return amount;
		}
		// Flat percentage - nothing to convert
		if (scheduleConfig.getSchedule().getScheduleType().isFlatFeePercent()) {
			return amount;
		}

		// If schedule & invoice have the same frequency - no conversion needed
		if (invoiceFrequency != scheduleFrequency.getBillingFrequency()) {
			// If schedule & invoice do not have the same frequency - convert the schedule amount to the invoice frequency
			// Example - Schedule is set up Monthly, but we bill quarterly - need to set amount to 3xs scheduled amount
			// Schedule Frequencies define the conversion rate to use, monthsInPeriod or daysInPeriod
			Integer invoiceRate;
			int scheduleRate;

			if (scheduleFrequency.isUseDailyConversion()) {
				invoiceRate = scheduleConfig.getDaysInInvoicePeriod();
				if (scheduleFrequency.isUseActualDays()) {
					scheduleRate = DateUtils.getDaysInYear(scheduleConfig.getInvoiceDate());
				}
				else {
					scheduleRate = scheduleFrequency.getBillingFrequency().getDaysInPeriod();
				}
			}
			else {
				invoiceRate = invoiceFrequency.getMonthsInPeriod();
				scheduleRate = scheduleFrequency.getBillingFrequency().getMonthsInPeriod();
			}
			amount = CoreMathUtils.getValueProrated(BigDecimal.valueOf(invoiceRate), BigDecimal.valueOf(scheduleRate), amount);
		}
		return amount;
	}


	/**
	 * Rounds all billing values to the nearest dollar or penny (based on the billing amount precision as defined by the invoice type), unless the value is a percentage, then we round 5 places out
	 */
	public static BigDecimal roundBillingAmount(BillingScheduleProcessConfig scheduleConfig, BigDecimal amount, boolean percentage) {
		if (percentage) {
			return MathUtils.round(amount, 5);
		}
		return MathUtils.round(amount, scheduleConfig.getBillingAmountPrecision());
	}


	/**
	 * Returns the string representation of the value for the invoice.  First rounds the amount to the appropriate precision.
	 *
	 * @param percentage - If true, returns the value with % suffix, else will add the currency symbol (prefix) if we have it ($), else the Ticker as a suffix if we don't (USD)
	 */
	public static String formatBillingAmountForInvoice(BillingScheduleProcessConfig scheduleConfig, BigDecimal amount, boolean percentage) {
		// All amounts are rounded to nearest dollar or penny based on the precision as defined by the invoice type, percentages - 5 places out
		amount = roundBillingAmount(scheduleConfig, amount, percentage);

		if (percentage) {
			return CoreMathUtils.formatNumberDecimal(amount) + " %";
		}
		return InvestmentUtils.formatAmountWithCurrency(amount, scheduleConfig.getBillingCurrency().getSymbol(), true, CoreMathUtils::formatNumberDecimal);
	}


	public static void addBillingInvoiceScheduleAmountNote(BillingScheduleProcessConfig scheduleConfig, StringBuilder note, boolean percentage) {
		note.append(formatBillingAmountForInvoice(scheduleConfig, scheduleConfig.getSchedule().getScheduleAmount(), percentage));

		if (BillingScheduleFrequencies.ONCE == scheduleConfig.getSchedule().getScheduleFrequency()) {
			if (MathUtils.isLessThan(scheduleConfig.getSchedule().getScheduleAmount(), 0)) {
				note.append(" Credit");
			}
			else {
				note.append(" Charge");
			}
		}
		else if (scheduleConfig.getSchedule().getScheduleFrequency() != null) {
			note.append(" ").append(scheduleConfig.getSchedule().getScheduleFrequency().getInvoiceDescription());
		}
		note.append(StringUtils.NEW_LINE);
	}


	/**
	 * Appends to the note the prorated details, i.e. Prorated for 1 Month and 5 Days, 76 of 91 Days, etc.
	 */
	public static void addBillingInvoiceProratedNote(StringBuilder note, BigDecimal months, BigDecimal totalMonths, BigDecimal days, BigDecimal totalDays) {
		if (!MathUtils.isEqual(months, totalMonths) || !MathUtils.isEqual(days, totalDays)) {
			note.append(StringUtils.TAB + "Prorated for ");

			if (!MathUtils.isNullOrZero(months)) {
				note.append(CoreMathUtils.formatNumberInteger(months));
				note.append(" month");
				if (MathUtils.isGreaterThan(months, BigDecimal.ONE)) {
					note.append("s");
				}
				if (!MathUtils.isNullOrZero(days)) {
					note.append(" and ").append(CoreMathUtils.formatNumberInteger(days)).append(" day");
					if (MathUtils.isGreaterThan(days, BigDecimal.ONE)) {
						note.append("s");
					}
				}
			}
			else {
				note.append(CoreMathUtils.formatNumberInteger(days)).append(" of ").append(CoreMathUtils.formatNumberInteger(totalDays)).append(" days");
			}
		}
	}
}
