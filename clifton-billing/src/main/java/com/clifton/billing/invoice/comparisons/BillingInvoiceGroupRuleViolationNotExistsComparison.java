package com.clifton.billing.invoice.comparisons;

/**
 * The BillingInvoiceGroupRuleViolationNotExistsComparison class is an extension of the BillingInvoiceGroupRuleViolationExistsComparison, but checks
 * if violations do not exist
 *
 * @author manderson
 */
public class BillingInvoiceGroupRuleViolationNotExistsComparison extends BillingInvoiceGroupRuleViolationExistsComparison {

	@Override
	protected boolean isExistsComparison() {
		return false;
	}
}
