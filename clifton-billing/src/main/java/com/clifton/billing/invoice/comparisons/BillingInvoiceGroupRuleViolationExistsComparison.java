package com.clifton.billing.invoice.comparisons;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.comparison.RuleViolationExistsComparison;

import java.util.ArrayList;
import java.util.List;


/**
 * The BillingInvoiceGroupRuleViolationExistsComparison class is an extension of the RuleViolationExistsComparison, and it
 * checks each invoice in the group for existing violations.
 *
 * @author manderson
 */
public class BillingInvoiceGroupRuleViolationExistsComparison extends RuleViolationExistsComparison {


	private BillingInvoiceService billingInvoiceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// get all violations/warnings that exist for all invoices in the group
		if (!(bean instanceof BillingInvoiceGroup)) {
			throw new ValidationException(getClass().getName() + " can only by used for Billing Invoice Group objects.");
		}

		// Note: Lists are cached by invoice, so append the lists for each invoice
		List<BillingInvoice> invoiceList = getBillingInvoiceService().getBillingInvoiceGroup(((BillingInvoiceGroup) bean).getId()).getInvoiceList();
		// Invoices have already been converted to use Rules only - so not necessary to look up System Warnings like the generic comparison does
		List<RuleViolation> violationList = new ArrayList<>();
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			List<RuleViolation> invoiceViolationList = getRuleViolationService().getRuleViolationListByLinkedEntity(BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId());
			if (CollectionUtils.isEmpty(violationList)) {
				violationList = invoiceViolationList;
			}
			else if (!CollectionUtils.isEmpty(invoiceViolationList)) {
				violationList.addAll(invoiceViolationList);
			}
		}
		return evaluateResults(context, violationList);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}
}
