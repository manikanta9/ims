package com.clifton.billing.invoice;


import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.payment.BillingPaymentStatuses;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.lock.SystemLockWithNoteAware;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.BaseWorkflowAwareEntity;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingInvoice</code> class represents a single invoice sent to a client.
 *
 * @author vgomelsky
 */
public class BillingInvoice extends BaseWorkflowAwareEntity<Integer> implements LabeledObject, RuleViolationAware, HierarchicalObject<BillingInvoice>, DocumentFileCountAware, SystemLockWithNoteAware, PortalFileAware, BillingInvoiceRevenueTypeAmountAware {

	public static final String BILLING_INVOICE_TABLE_NAME = "BillingInvoice";

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Invoice type - determines if it's revenue or not, if it's revenue then it would use {@link BillingDefinition} to generate
	 */
	private BillingInvoiceType invoiceType;

	/**
	 * If set, the parent invoice would be the previous invoice that has been voided and this
	 * invoice is a revision of it.
	 */
	private BillingInvoice parent;

	/**
	 * Used when invoiceType uses billing definitions
	 */
	private BillingDefinition billingDefinition;

	/**
	 * The company for the invoice - usually who should receive the invoice.
	 * For institutional clients this is usually the client or client relationships company
	 * For retail clients where the broker gets the fee information to debit the accounts, it would be the broker company
	 */
	private BusinessCompany businessCompany;

	private Date invoiceDate;

	/**
	 * Invoice Period information - i.e. Quarterly invoice would have 7/1 - 9/30, Monthly invoice would have 7/1 - 7/31
	 */
	private Date invoicePeriodStartDate;
	private Date invoicePeriodEndDate;

	/**
	 * For Revenue Type invoices, this is the total gross REVENUE, for non revenue type invoices, this is just the total amount
	 */
	private BigDecimal totalAmount;

	/**
	 * For Revenue Type invoices only.  This is the total amount that we share with an external party - a.k.a Broker Fee
	 * If used, requires a revenueShareExternalCompany to be selected on the billing definition
	 */
	private BigDecimal revenueShareExternalTotalAmount;

	/**
	 * For Revenue Type invoices only.  This is the total amount that we share with an internal party - a.k.a Revenue Share (i.e. Eaton Vance)
	 * If used, requires a revenueShareInternalCompany to be selected on the billing definition
	 */
	private BigDecimal revenueShareInternalTotalAmount;

	/**
	 * For those invoices that are generated manually - can assign a vendor whom performed services
	 * i.e. Legal Fee invoices would have the attorney selected as the vendor
	 */
	private BusinessCompany vendorBusinessCompany;

	/**
	 * System managed property that's updated by BillingPaymentAllocations for this invoice
	 * Field is required on the database and when invoices are generated it is automatically set to zero
	 * This will enable 0 invoices to automatically be moved to paid when they are done with Final Approval or Marked as Sent
	 * For revenue type invoices, we only track payments for Gross and Broker Fees, Internal Revenue Share amounts is used for internal reporting only and we do not track payments to internal parties
	 */
	private BigDecimal paidAmount = BigDecimal.ZERO;


	private List<BillingInvoiceDetail> detailList;

	private RuleViolationStatus violationStatus;

	/**
	 * Used when an Invoice is marked as Void.
	 */
	private String voidReason;

	/**
	 * Total number of attachments tied to the invoice
	 */
	private Short documentFileCount;

	/**
	 * If true, there is at least one file posted to the portal using this BillingInvoice as it's source
	 */
	private boolean postedToPortal;

	/**
	 * Set by the system when an invoice's billing definition
	 * shares it's schedule with other billing definitions.
	 * All invoices are created as a group and move through the workflow together.
	 */
	private BillingInvoiceGroup invoiceGroup;

	/**
	 * For Canceled invoices, these are a list of invoices that were generated
	 * as a revision/correction of this invoice.  Likely will be one:one but not enforced
	 */
	private List<BillingInvoice> childrenList;


	/**
	 * Invoices can be locked to prevent someone from sending an invoice out while someone else is working on it
	 * This is the user that locked the invoice
	 */
	private SecurityUser lockedByUser;

	/**
	 * If the invoice is locked, this note is required.  When the invoice is unlocked the note is cleared
	 */
	private String lockNote;


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String lbl = "";
		if (getInvoiceType() != null) {
			lbl += getInvoiceType().getName() + ": ";
		}
		if (getBillingDefinition() != null) {
			lbl += getBillingDefinition().getLabel() + " ";
		}
		else if (getBusinessCompany() != null) {
			lbl += getBusinessCompany().getName() + " ";
		}
		lbl += DateUtils.fromDateShort(getInvoiceDate());
		if (getParent() != null) {
			lbl += " CORRECTED";
		}
		return lbl;
	}


	public String getLabelShort() {
		String lbl = "# " + getId() + " ";
		if (getBillingDefinition() != null) {
			lbl += getBillingDefinition().getLabel() + " ";
		}
		else if (getBusinessCompany() != null) {
			lbl += getBusinessCompany().getName() + " ";
		}
		return lbl;
	}


	public String getInvoiceNumberAndAmountLabel() {
		String lbl = "";
		if (getId() != null) {
			lbl = "#" + getId();
		}
		if (getPayableTotalAmount() != null) {
			lbl += " Amount Due: " + InvestmentUtils.formatAmountWithCurrency(getPayableTotalAmount(), (getBillingDefinition() == null ? null : getBillingDefinition().getBillingCurrency().getSymbol()), false, null);
		}
		return lbl;
	}


	public String getCompanyInvoiceNumberAndAmountLabel() {
		String lbl = getInvoiceNumberAndAmountLabel();
		if (getBusinessCompany() != null) {
			lbl += " (" + getBusinessCompany().getName() + ")";
		}
		return lbl;
	}


	@Override
	public BigDecimal getRevenueGrossTotalAmount() {
		if (getInvoiceType().isRevenue()) {
			return getTotalAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueNetTotalAmount() {
		return MathUtils.add(getRevenueGrossTotalAmount(), getRevenueShareExternalTotalAmount());
	}


	@Override
	public BigDecimal getRevenueFinalTotalAmount() {
		return MathUtils.add(getRevenueNetTotalAmount(), getRevenueShareInternalTotalAmount());
	}


	@Override
	public BigDecimal getOtherTotalAmount() {
		if (!getInvoiceType().isRevenue()) {
			return getTotalAmount();
		}
		return null;
	}


	/**
	 * We DO NOT Track payments for internal revenue share - so payable total is revenue net for Revenue type invoices, else total amount
	 */
	public BigDecimal getPayableTotalAmount() {
		if (getInvoiceType() != null && getInvoiceType().isRevenue()) {
			return getRevenueNetTotalAmount();
		}
		return getTotalAmount();
	}


	/**
	 * We DO NOT Track payments for internal revenue share - so unpaid amount is always total + external share
	 */
	public BigDecimal getUnpaidAmount() {
		return MathUtils.subtract(getPayableTotalAmount(), getPaidAmount());
	}


	public String getLabelUnpaidBalance() {
		String lbl = getLabelShort();
		if (!isNewBean()) {
			lbl = getId() + ": ";
		}
		if (getBusinessCompany() != null) {
			lbl += " (" + getBusinessCompany().getName() + ") ";
		}
		lbl += DateUtils.fromDateShort(getInvoiceDate());
		if (getParent() != null) {
			lbl += " CORRECTED";
		}
		if (getWorkflowState() != null) {
			lbl += " " + getWorkflowState().getName();
		}
		lbl += ": " + CoreMathUtils.formatNumberMoney(getPayableTotalAmount());
		lbl += ", Unpaid Balance: " + CoreMathUtils.formatNumberMoney(getUnpaidAmount());
		return lbl;
	}


	public String getLabelLongUnpaidBalance() {
		String lbl = "";
		if (!isNewBean()) {
			lbl = getId() + ": ";
		}
		lbl += DateUtils.fromDateShort(getInvoiceDate());
		if (getParent() != null) {
			lbl += " CORRECTED";
		}
		if (getWorkflowState() != null) {
			lbl += " " + getWorkflowState().getName();
		}
		lbl += ": " + CoreMathUtils.formatNumberMoney(getPayableTotalAmount());
		lbl += ", Unpaid Balance: " + CoreMathUtils.formatNumberMoney(getUnpaidAmount());
		return lbl;
	}


	public BillingPaymentStatuses getBillingPaymentStatus() {
		return BillingPaymentStatuses.validateBillingPaymentStatus(getPayableTotalAmount(), getPaidAmount());
	}


	public boolean isLocked() {
		return getLockedByUser() != null;
	}


	public boolean isCanceled() {
		return (getWorkflowStatus() != null && WorkflowStatus.STATUS_CANCELED.equals(getWorkflowStatus().getName()));
	}


	public boolean isCorrected() {
		return !CollectionUtils.isEmpty(getChildrenList());
	}


	public Integer getDaysInInvoicePeriod() {
		return DateUtils.getDaysDifferenceInclusive(getInvoicePeriodEndDate(), getInvoicePeriodStartDate());
	}


	public Date getBillingBasisStartDate() {
		if (getBillingDefinition() != null) {
			Date start = DateUtils.addMonths(DateUtils.addDays(getInvoiceDate(), 1), -getBillingDefinition().getBillingFrequency().getMonthsInPeriod());
			if (DateUtils.compare(getBillingDefinition().getStartDate(), start, false) > 0) {
				return getBillingDefinition().getStartDate();
			}
			return start;
		}
		return null;
	}


	public Date getBillingBasisEndDate() {
		if (getBillingDefinition() != null) {
			Date end = getInvoiceDate();
			if (getBillingDefinition().getEndDate() != null && (DateUtils.compare(getBillingDefinition().getEndDate(), end, false) < 0)) {
				return getBillingDefinition().getEndDate();
			}
			return end;
		}
		return null;
	}


	public Integer getDaysInBillingBasisPeriod() {
		if (getBillingDefinition() != null) {
			return DateUtils.getDaysDifferenceInclusive(getBillingBasisEndDate(), getBillingBasisStartDate());
		}
		return null;
	}


	public Date getInvoiceBillingStartDate() {
		if (getBillingDefinition() != null) {
			Date startDate = getBillingDefinition().getStartDate();

			if (!DateUtils.isDateBetween(startDate, getInvoicePeriodStartDate(), getInvoicePeriodEndDate(), false)) {
				startDate = getInvoicePeriodStartDate();
			}
			return startDate;
		}
		return null;
	}


	public Date getInvoiceBillingEndDate() {
		if (getBillingDefinition() != null) {
			Date endDate = getBillingDefinition().getEndDate();

			if (!DateUtils.isDateBetween(endDate, getInvoicePeriodStartDate(), getInvoicePeriodEndDate(), false)) {
				endDate = getInvoicePeriodEndDate();
			}

			return endDate;
		}
		return null;
	}


	public Integer getDaysInInvoiceBilling() {
		if (getBillingDefinition() != null) {
			Date startDate = getInvoiceBillingStartDate();
			Date endDate = getInvoiceBillingEndDate();
			return DateUtils.getDaysDifferenceInclusive(endDate, startDate);
		}
		return null;
	}


	public void addBillingInvoiceDetail(BillingInvoiceDetail detail) {
		if (this.detailList == null) {
			this.detailList = new ArrayList<>();
		}
		this.detailList.add(detail);
	}


	public Date getAnnualInvoicePeriodStartDate() {
		if (getBillingDefinition() != null) {
			Date secondYearStart = getBillingDefinition().getSecondYearStartDate();
			if (secondYearStart != null) {
				if (DateUtils.isDateBetween(getInvoicePeriodEndDate(), getBillingDefinition().getStartDate(), secondYearStart, false)) {
					return getBillingDefinition().getStartDate();
				}
				int month = DateUtils.getMonthOfYear(secondYearStart);
				int invoiceMonth = DateUtils.getMonthOfYear(getInvoicePeriodEndDate());
				if (month < invoiceMonth) {
					return DateUtils.toDate(month + "/01/" + DateUtils.getYear(getInvoicePeriodEndDate()));
				}
				return DateUtils.toDate(month + "/01/" + (DateUtils.getYear(getInvoicePeriodEndDate()) - 1));
			}
		}
		return null;
	}


	public Date getAnnualAbsoluteInvoicePeriodEndDate() {
		if (getBillingDefinition() != null) {
			Date secondYearStart = getBillingDefinition().getSecondYearStartDate();
			if (secondYearStart != null) {
				if (DateUtils.isDateBetween(getInvoicePeriodEndDate(), getBillingDefinition().getStartDate(), secondYearStart, false)) {
					return DateUtils.addDays(secondYearStart, -1);
				}
				int month = DateUtils.getMonthOfYear(secondYearStart);
				int invoiceMonth = DateUtils.getMonthOfYear(getInvoicePeriodEndDate());
				Date date;
				if (month < invoiceMonth) {
					date = DateUtils.toDate(month + "/01/" + (DateUtils.getYear(getInvoicePeriodEndDate()) + 1));
				}
				else {
					date = DateUtils.toDate(month + "/01/" + (DateUtils.getYear(getInvoicePeriodEndDate())));
				}
				return DateUtils.addDays(date, -1);
			}
		}
		return null;
	}


	public Date getAnnualInvoicePeriodEndDate() {
		if (getBillingDefinition() != null) {
			Date absEnd = getAnnualAbsoluteInvoicePeriodEndDate();
			// If billing definition ends before the annual invoice period, use the billing definition end date
			if (getBillingDefinition().getEndDate() != null && DateUtils.compare(getBillingDefinition().getEndDate(), absEnd, false) < 0) {
				return getBillingDefinition().getEndDate();
			}
			return absEnd;
		}
		return null;
	}


	public boolean isLastInvoiceForAnnualInvoicePeriod() {
		if (getBillingDefinition() != null) {
			return DateUtils.isDateBetween(getAnnualInvoicePeriodEndDate(), getInvoicePeriodStartDate(), getInvoicePeriodEndDate(), false);
		}
		return false;
	}


	public Integer getDaysInAnnualInvoicePeriod() {
		if (getBillingDefinition() != null) {
			Date startDate = getAnnualInvoicePeriodStartDate();
			if (startDate != null) {
				Date endDate = getAnnualInvoicePeriodEndDate();
				return DateUtils.getDaysDifferenceInclusive(endDate, startDate);
			}
		}
		return null;
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	// HierarchicalEntity methods


	@Override
	public Integer getMaxDepth() {
		return null;
	}


	@Override
	public BillingInvoice getRootParent() {
		return getParentAtLevel(1);
	}


	@Override
	public BillingInvoice getParentAtLevel(int level) {
		int currentLevel = getLevel();

		if (currentLevel <= level) {
			return this;
		}

		BillingInvoice root = getParent();
		currentLevel--;

		while (currentLevel > level && root != null) {
			root = root.getParent();
			currentLevel--;
		}

		return root;
	}


	@Override
	public int getLevel() {
		int level = 1;
		if (getParent() != null) {
			return getParent().getLevel() + 1;
		}
		return level;
	}


	@Override
	public boolean isLeaf() {
		Integer maxDepth = getMaxDepth();
		if (maxDepth == null || maxDepth > getLevel()) {
			return false;
		}
		return true;
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public BillingDefinition getBillingDefinition() {
		return this.billingDefinition;
	}


	public void setBillingDefinition(BillingDefinition billingDefinition) {
		this.billingDefinition = billingDefinition;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	@Override
	public BigDecimal getRevenueShareExternalTotalAmount() {
		return this.revenueShareExternalTotalAmount;
	}


	public void setRevenueShareExternalTotalAmount(BigDecimal revenueShareExternalTotalAmount) {
		this.revenueShareExternalTotalAmount = revenueShareExternalTotalAmount;
	}


	@Override
	public BigDecimal getRevenueShareInternalTotalAmount() {
		return this.revenueShareInternalTotalAmount;
	}


	public void setRevenueShareInternalTotalAmount(BigDecimal revenueShareInternalTotalAmount) {
		this.revenueShareInternalTotalAmount = revenueShareInternalTotalAmount;
	}


	public List<BillingInvoiceDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<BillingInvoiceDetail> detailList) {
		this.detailList = detailList;
	}


	public String getVoidReason() {
		return this.voidReason;
	}


	public void setVoidReason(String voidReason) {
		this.voidReason = voidReason;
	}


	public BillingInvoiceGroup getInvoiceGroup() {
		return this.invoiceGroup;
	}


	public void setInvoiceGroup(BillingInvoiceGroup invoiceGroup) {
		this.invoiceGroup = invoiceGroup;
	}


	@Override
	public BillingInvoice getParent() {
		return this.parent;
	}


	public void setParent(BillingInvoice parent) {
		this.parent = parent;
	}


	public List<BillingInvoice> getChildrenList() {
		return this.childrenList;
	}


	public void setChildrenList(List<BillingInvoice> childrenList) {
		this.childrenList = childrenList;
	}


	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}


	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}


	public BillingInvoiceType getInvoiceType() {
		return this.invoiceType;
	}


	public void setInvoiceType(BillingInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}


	public void setInvoicePeriodStartDate(Date invoicePeriodStartDate) {
		this.invoicePeriodStartDate = invoicePeriodStartDate;
	}


	public void setInvoicePeriodEndDate(Date invoicePeriodEndDate) {
		this.invoicePeriodEndDate = invoicePeriodEndDate;
	}


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public Date getInvoicePeriodStartDate() {
		return this.invoicePeriodStartDate;
	}


	public Date getInvoicePeriodEndDate() {
		return this.invoicePeriodEndDate;
	}


	public BusinessCompany getVendorBusinessCompany() {
		return this.vendorBusinessCompany;
	}


	public void setVendorBusinessCompany(BusinessCompany vendorBusinessCompany) {
		this.vendorBusinessCompany = vendorBusinessCompany;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	@Override
	public SecurityUser getLockedByUser() {
		return this.lockedByUser;
	}


	@Override
	public void setLockedByUser(SecurityUser lockedByUser) {
		this.lockedByUser = lockedByUser;
	}


	@Override
	public String getLockNote() {
		return this.lockNote;
	}


	@Override
	public void setLockNote(String lockNote) {
		this.lockNote = lockNote;
	}


	@Override
	public boolean isPostedToPortal() {
		return this.postedToPortal;
	}


	@Override
	public void setPostedToPortal(boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}
}
