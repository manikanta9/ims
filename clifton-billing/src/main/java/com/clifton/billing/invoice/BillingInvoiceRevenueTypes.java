package com.clifton.billing.invoice;

/**
 * @author manderson
 */
public enum BillingInvoiceRevenueTypes {

	NONE("None", false, false, false), // NOT REVENUE - I.E. REIMBURSEMENT FOR LEGAL FEES
	REVENUE("Revenue", true, false, false), // GROSS REVENUE
	REVENUE_SHARE_EXTERNAL("Broker Fee", true, true, true), // Broker Fee
	REVENUE_SHARE_INTERNAL("Revenue Share", true, true, false); // EV or PPA Shared Revenue

	////////////////////////////////////////////////////////////////////////////////

	private final String label;

	private final boolean revenue;

	/**
	 * Amount is shared with another party (internal or external)
	 */
	private final boolean revenueShare;

	/**
	 * If revenueShare is true, is it an internal share or external share (Broker Fee)
	 */
	private final boolean external;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	BillingInvoiceRevenueTypes(String label, boolean revenue, boolean revenueShare, boolean external) {
		this.label = label;
		this.revenue = revenue;
		this.revenueShare = revenueShare;
		this.external = external;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public boolean isRevenue() {
		return this.revenue;
	}


	public boolean isRevenueShare() {
		return this.revenueShare;
	}


	public boolean isExternal() {
		return this.external;
	}
}
