package com.clifton.billing.invoice.process.billingbasis.postprocessors;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;

import java.util.Map;


/**
 * The <code>BillingBasisPostProcessor</code> defines the interface for performing a post processing action on a BillingBasis.
 *
 * @author Mary Anderson
 */
public interface BillingBasisPostProcessor {

	/**
	 * Updates the billing basis map with results after processing the post-processor
	 */
	public void process(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap);
}
