package com.clifton.billing.invoice.comparisons;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>BillingInvoicePrerequisiteWorkflowStateComparison</code> is used for invoices to find their prerequisite invoices
 * and confirm they are in or not in the given states.  It can be passed an individual invoice or an invoice group (in which case it will check each invoice in the group)
 * <p>
 * Note: Always Returns true if No Prerequisite Invoices apply
 *
 * @author manderson
 */
public class BillingInvoicePrerequisiteWorkflowStateComparison implements Comparison<IdentityObject> {

	private BillingDefinitionService billingDefinitionService;
	private BillingInvoiceService billingInvoiceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private List<String> allowedWorkflowStateNames;

	private List<String> disallowedWorkflowStateNames;

	/**
	 * Optional: This can be used to make the invalid state message more user friendly
	 * i.e. Confirming prerequisite invoice has been approved - instead of saying the prerequisite invoice is in invalid state of Draft, just say the prerequisite invoice hasn't been approved yet.
	 */
	private String invalidStateMessageSuffix;

	/**
	 * If billing definition setup determines there is a prerequisite
	 * but that prerequisite invoice is missing - then will fail if this is true - otherwise will ignore it
	 */
	private boolean falseIfMissingPrerequisiteInvoice;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		if (bean instanceof BillingInvoice) {
			return evaluateForInvoiceList(CollectionUtils.createList((BillingInvoice) bean), context);
		}
		if (bean instanceof BillingInvoiceGroup) {
			return evaluateForInvoiceList(getBillingInvoiceService().getBillingInvoiceGroup(((BillingInvoiceGroup) bean).getId()).getInvoiceList(), context);
		}
		throw new IllegalStateException("BillingInvoicePrerequisiteWorkflowStateComparison can be used by BillingInvoice or BillingInvoiceGroup objects only");
	}


	private boolean evaluateForInvoiceList(List<BillingInvoice> invoiceList, ComparisonContext context) {
		// Even if there are multiple - then they would be part of a group and have the same invoice period
		Date invoicePeriodStartDate = invoiceList.get(0).getInvoicePeriodStartDate();
		Date invoicePeriodEndDate = invoiceList.get(0).getInvoicePeriodEndDate();

		// Determine if there are any pre-requisites
		Set<BillingDefinition> billingDefinitions = new HashSet<>();
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			List<BillingScheduleBillingDefinitionDependency> prerequisiteList = getBillingDefinitionService().getBillingScheduleBillingDefinitionDependencyListByDefinition(invoice.getBillingDefinition().getId(), false);
			if (!CollectionUtils.isEmpty(prerequisiteList)) {
				for (BillingScheduleBillingDefinitionDependency prerequisite : prerequisiteList) {
					// If the schedule is active during the invoice period include it
					if (!billingDefinitions.contains(prerequisite.getBillingDefinition()) && DateUtils.isOverlapInDates(prerequisite.getBillingSchedule().getStartDate(), prerequisite.getBillingSchedule().getEndDate(), invoicePeriodStartDate, invoicePeriodEndDate)) {
						// If the billing definition is active during the invoice period - include it
						if (DateUtils.isOverlapInDates(prerequisite.getBillingDefinition().getStartDate(), prerequisite.getBillingDefinition().getEndDate(), invoicePeriodStartDate, invoicePeriodEndDate)) {
							billingDefinitions.add(prerequisite.getBillingDefinition());
						}
					}
				}
			}
		}

		// If there are no prerequisites necessary - return true
		if (CollectionUtils.isEmpty(billingDefinitions)) {
			if (context != null) {
				context.recordTrueMessage("No prerequisite invoices apply");
			}
			return true;
		}

		for (BillingDefinition billingDefinition : billingDefinitions) {
			BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
			searchForm.setBillingDefinitionId(billingDefinition.getId());
			searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CANCELED); // Always exclude Voided invoices
			searchForm.setInvoiceStartDate(invoicePeriodStartDate);
			searchForm.setInvoiceEndDate(invoicePeriodEndDate);
			List<BillingInvoice> prerequisiteInvoiceList = getBillingInvoiceService().getBillingInvoiceList(searchForm);
			if (CollectionUtils.isEmpty(prerequisiteInvoiceList)) {
				if (isFalseIfMissingPrerequisiteInvoice()) {
					if (context != null) {
						context.recordFalseMessage("Missing Prerequisite Invoice(s) for Billing Definition " + billingDefinition.getLabel() + " for Invoice Period " + DateUtils.fromDateRange(invoicePeriodStartDate, invoicePeriodEndDate, true));
					}
					return false;
				}
			}
			else if (isFalseIfMissingPrerequisiteInvoice()) {
				// Confirm all invoices exist for the period
				Date nextInvoiceExpectedDate = billingDefinition.getBillingFrequency().getLastDayOfBillingCycle(invoicePeriodStartDate, billingDefinition.getSecondYearStartDate());

				while (DateUtils.isDateBetween(nextInvoiceExpectedDate, invoicePeriodStartDate, invoicePeriodEndDate, false)) {
					Date nextInvoicePeriodStartDate = DateUtils.addMonths(DateUtils.addDays(nextInvoiceExpectedDate, 1), -billingDefinition.getBillingFrequency().getMonthsInPeriod());
					Date nextInvoicePeriodEndDate = nextInvoiceExpectedDate;

					// Only confirm if Billing Definition Is Active during invoice period
					if (DateUtils.isOverlapInDates(nextInvoicePeriodStartDate, nextInvoicePeriodEndDate, billingDefinition.getStartDate(), billingDefinition.getEndDate())) {
						BillingInvoice prerequisiteInvoice = CollectionUtils.getOnlyElement(BeanUtils.filter(prerequisiteInvoiceList, BillingInvoice::getInvoiceDate, nextInvoiceExpectedDate));
						if (prerequisiteInvoice == null) {
							if (context != null) {
								context.recordFalseMessage("Missing Prerequisite Invoice for Billing Definition " + billingDefinition.getLabel() + " on " + DateUtils.fromDateShort(nextInvoiceExpectedDate));
							}
							return false;
						}
						nextInvoiceExpectedDate = billingDefinition.getBillingFrequency().getLastDayOfBillingCycle(DateUtils.addDays(nextInvoiceExpectedDate, 1), billingDefinition.getSecondYearStartDate());
					}
					else {
						break; // Break out of loop if definition is no longer active
					}
				}
			}

			for (BillingInvoice invoice : CollectionUtils.getIterable(prerequisiteInvoiceList)) {
				// If invoice is not in allowed workflow state
				if ((!CollectionUtils.isEmpty(getAllowedWorkflowStateNames()) && !getAllowedWorkflowStateNames().contains(invoice.getWorkflowState().getName()))
						// Or Invoice is in disallowed workflow state
						|| (!CollectionUtils.isEmpty(getDisallowedWorkflowStateNames()) && getDisallowedWorkflowStateNames().contains(invoice.getWorkflowState().getName()))) {
					if (context != null) {
						context.recordFalseMessage("Prerequisite Invoice " + invoice.getLabelShort() + " " + StringUtils.coalesce(true, getInvalidStateMessageSuffix(), "is in invalid state " + invoice.getWorkflowState().getName()));
					}
					return false;
				}
			}
		}

		if (context != null) {
			context.recordTrueMessage("All prerequisite invoices validated against allowed and/or disallowed states successfully.");
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public List<String> getAllowedWorkflowStateNames() {
		return this.allowedWorkflowStateNames;
	}


	public void setAllowedWorkflowStateNames(List<String> allowedWorkflowStateNames) {
		this.allowedWorkflowStateNames = allowedWorkflowStateNames;
	}


	public List<String> getDisallowedWorkflowStateNames() {
		return this.disallowedWorkflowStateNames;
	}


	public void setDisallowedWorkflowStateNames(List<String> disallowedWorkflowStateNames) {
		this.disallowedWorkflowStateNames = disallowedWorkflowStateNames;
	}


	public boolean isFalseIfMissingPrerequisiteInvoice() {
		return this.falseIfMissingPrerequisiteInvoice;
	}


	public void setFalseIfMissingPrerequisiteInvoice(boolean falseIfMissingPrerequisiteInvoice) {
		this.falseIfMissingPrerequisiteInvoice = falseIfMissingPrerequisiteInvoice;
	}


	public String getInvalidStateMessageSuffix() {
		return this.invalidStateMessageSuffix;
	}


	public void setInvalidStateMessageSuffix(String invalidStateMessageSuffix) {
		this.invalidStateMessageSuffix = invalidStateMessageSuffix;
	}
}
