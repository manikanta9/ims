package com.clifton.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>{@link BillingBasisSecurityTargetNotionalValueCalculator}</code> is used to calculate BillingBasis for accounts that use security targets.
 * The result is for each active target on billing basis snapshot date is one of 3 values based on how the target is defined:
 * 1. target notional value
 * 2. Manager balance of the target manager
 * 3. Target quantity * Current price i.e. Target of 50,000 shares of AAPL = 50,000 * the Current Price of AAPL (or Target Quantity * Period End (month end) price)
 * <p>
 * All above 3 cases then apply the target multiplier and result is the abs value
 *
 * @author manderson
 */
public class BillingBasisSecurityTargetNotionalValueCalculator extends BaseBillingBasisValueCalculator {


	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private BillingDefinitionService billingDefinitionService;

	private MarketDataRetriever marketDataRetriever;

	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Uses period end price for the security (most commonly used with monthly accruals, so would be month end price)
	 * NOTE: The QUANTITY is adjusted for price adjustments from date to period end date.  The price remains as it is on month end
	 * i.e. if there is a stock split during the month
	 * Only applies if the target is a target quantity
	 */
	private boolean usePeriodEndPrice;


	/**
	 * Security Target valuation at this point is only used for weighted and month end
	 * so any day of the month can be a valid valuation date.  Leaving this as an option in case we need
	 * to turn it back on.
	 */
	private boolean weekdayValuationOnly;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidValuationDate(Date date) {
		if (isWeekdayValuationOnly()) {
			return DateUtils.isWeekday(date);
		}
		return true;
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		if (isWeekdayValuationOnly()) {
			if (moveForward) {
				return DateUtils.getNextWeekday(date);
			}
			return DateUtils.getPreviousWeekday(date);
		}
		return DateUtils.addDays(date, moveForward ? 1 : -1);
	}


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(billingAccount, startDate, endDate);

		List<BillingBasisSnapshot> list = new ArrayList<>();
		Map<Integer, Map<Date, MarketDataValueHolder>> securityDateMarketValueHolderMap = new HashMap<>();

		// Go through each date and calculate Snapshot Details for each target
		Date date = startDate;
		while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
			for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(securityTargetList)) {
				if (DateUtils.isDateBetween(date, securityTarget.getStartDate(), securityTarget.getEndDate(), false)) {
					list.add(createBillingBasisSnapshot(config, invoice, billingAccount, securityTarget, date, endDate, securityDateMarketValueHolderMap));
				}
			}
			date = getNextValuationDate(date, true);
		}
		return list;
	}


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		InvestmentAccountSecurityTargetSearchForm searchForm = new InvestmentAccountSecurityTargetSearchForm();
		searchForm.setClientInvestmentAccountId(billingAccount.getReferenceTwo().getId());
		// Pull All Active during the date range
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, endDate));
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, startDate));
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(searchForm);
	}


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoiceProcessConfig processConfig, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, InvestmentAccountSecurityTarget securityTarget, Date date, Date endDate, Map<Integer, Map<Date, MarketDataValueHolder>> securityDateMarketValueHolderMap) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSecurity(securityTarget.getTargetUnderlyingSecurity());
		bbs.setSourceFkFieldId(securityTarget.getId());
		bbs.setSnapshotDate(date);

		// Not always populated, but if it is, let's save it on the billing basis snapshot
		bbs.setQuantity(securityTarget.getTargetUnderlyingQuantity());

		BigDecimal underlyingSecurityTarget;
		if (securityTarget.getTargetUnderlyingNotional() != null) {
			underlyingSecurityTarget = securityTarget.getTargetUnderlyingNotional();
		}
		else if (securityTarget.getTargetUnderlyingManagerAccount() != null) {
			// Notional is from the manager account balance
			InvestmentManagerAccountBalance balance = null;
			boolean flexible = isProjectedRevenue();
			boolean addViolation = false;
			if (!flexible) {
				balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(securityTarget.getTargetUnderlyingManagerAccount().getId(), date, false);
				addViolation = (balance == null);
			}
			if (balance == null) {
				balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDateFlexible(securityTarget.getTargetUnderlyingManagerAccount().getId(), date, false);
			}
			if (balance == null || addViolation) {
				processConfig.setInvoiceBillingBasisMissingSourceDataViolationMap(invoice.getId(), securityTarget.getTargetUnderlyingManagerAccount(), date);
			}
			underlyingSecurityTarget = balance == null ? BigDecimal.ZERO : balance.getAdjustedTotalValue();
		}
		else {
			ValidationUtils.assertNotNull(securityTarget.getTargetUnderlyingQuantity(), "Security Target [" + securityTarget.getLabelShort() + "] is missing a target notional, manager selection, and underlying quantity.  One of those should be entered.");
			// Need to calculate value as notional value of the quantity

			// Adjust quantity if necessary
			underlyingSecurityTarget = MathUtils.multiply(securityTarget.getTargetUnderlyingQuantity(), getPriceAdjustmentFactorForSecurity(processConfig, invoice, securityTarget.getTargetUnderlyingSecurity(), date, endDate, securityDateMarketValueHolderMap));
			bbs.setQuantity(underlyingSecurityTarget);
			BigDecimal price = getPriceObjectForSecurity(processConfig, invoice, securityTarget.getTargetUnderlyingSecurity(), (isUsePeriodEndPrice() ? endDate : date), securityDateMarketValueHolderMap).getMeasureValue();
			bbs.setSecurityPrice(price);
			underlyingSecurityTarget = MathUtils.multiply(underlyingSecurityTarget, price);
		}
		underlyingSecurityTarget = MathUtils.multiply(underlyingSecurityTarget, securityTarget.getTargetUnderlyingMultiplier());
		bbs.setSnapshotValue(MathUtils.abs(underlyingSecurityTarget));
		return bbs;
	}


	/**
	 * If we are using period end price, then we need to adjust the QUANTITY for the price adjustment factor (if there are splits, etc.)
	 * We adjust the quantity, not the price so that the price is reported as it was on that date
	 */
	private BigDecimal getPriceAdjustmentFactorForSecurity(BillingInvoiceProcessConfig processConfig, BillingInvoice invoice, InvestmentSecurity security, Date date, Date endDate, Map<Integer, Map<Date, MarketDataValueHolder>> securityDateMarketValueHolderMap) {
		if (isUsePeriodEndPrice()) {
			MarketDataValueHolder priceHolder = getPriceObjectForSecurity(processConfig, invoice, security, date, securityDateMarketValueHolderMap);
			MarketDataValueHolder endPriceHolder = getPriceObjectForSecurity(processConfig, invoice, security, endDate, securityDateMarketValueHolderMap);
			return MathUtils.divide(endPriceHolder.getMeasureValueAdjustmentFactor(), priceHolder.getMeasureValueAdjustmentFactor(), BigDecimal.ONE);
		}
		// No adjustment factor if we are using daily pricing
		return BigDecimal.ONE;
	}


	private MarketDataValueHolder getPriceObjectForSecurity(BillingInvoiceProcessConfig processConfig, BillingInvoice invoice, InvestmentSecurity security, Date date, Map<Integer, Map<Date, MarketDataValueHolder>> securityDateMarketValueHolderMap) {
		Map<Date, MarketDataValueHolder> securityPriceMap = securityDateMarketValueHolderMap.get(security.getId());
		if (securityPriceMap == null) {
			securityPriceMap = new HashMap<>();
		}
		boolean flexible = isProjectedRevenue();

		// If we are using end date pricing and we are trying to price a security after it was ended/matured, move date back to the security end date
		if (isUsePeriodEndPrice() && (security.getEndDate() != null && DateUtils.isDateAfter(date, security.getEndDate()))) {
			date = security.getEndDate();
			flexible = true; // We don't always have price on end date, so we'll need to use prior price
		}


		MarketDataValueHolder priceHolder = securityPriceMap.get(date);
		if (priceHolder == null) {
			// Exception if missing only if flexible look up
			priceHolder = getMarketDataRetriever().getPriceMarketDataValueHolder(security, date, flexible, flexible, null);
			// If null, then missing ON expected date - change to do a flexible look up and add violation
			if (priceHolder == null) {
				processConfig.setInvoiceBillingBasisMissingSourceDataViolationMap(invoice.getId(), security, date);
				priceHolder = getMarketDataRetriever().getPriceMarketDataValueHolder(security, date, true, true, null);
			}
		}
		securityPriceMap.put(date, priceHolder);
		securityDateMarketValueHolderMap.put(security.getId(), securityPriceMap);
		return priceHolder;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Not expected to be used, but if we were to use the trade date logic for security targets, we assume this would be anytime the target changes
	 */
	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		// Returns the List of Dates the Targets Change
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(billingDefinitionInvestmentAccount, startDate, endDate);
		for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(securityTargetList)) {
			Date securityTargetEndDate = securityTarget.getEndDate();
			if (DateUtils.isDateBetween(securityTargetEndDate, startDate, endDate, false) && !currentTradeDateList.contains(securityTargetEndDate)) {
				currentTradeDateList.add(securityTargetEndDate);
			}
		}

		// Add billing period end date so we value from last trade to last date of billing period
		// Note: End date is the last day of billing period, not last day of invoice period in case the billing definition is
		// to end before the invoice period ends
		if (!currentTradeDateList.contains(endDate)) {
			currentTradeDateList.add(endDate);
		}
		return currentTradeDateList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public boolean isUsePeriodEndPrice() {
		return this.usePeriodEndPrice;
	}


	public void setUsePeriodEndPrice(boolean usePeriodEndPrice) {
		this.usePeriodEndPrice = usePeriodEndPrice;
	}


	public boolean isWeekdayValuationOnly() {
		return this.weekdayValuationOnly;
	}


	public void setWeekdayValuationOnly(boolean weekdayValuationOnly) {
		this.weekdayValuationOnly = weekdayValuationOnly;
	}
}
