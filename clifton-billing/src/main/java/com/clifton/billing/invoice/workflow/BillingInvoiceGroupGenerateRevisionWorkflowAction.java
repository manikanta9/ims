package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>BillingInvoiceGroupGenerateRevisionWorkflowAction</code> is called after the original invoice group is Voided
 * and will generate a new invoice group and will set each invoice's parent in the group as the original
 * <p>
 * Also copies all attachments from the original to the new invoice(s)
 *
 * @author manderson
 */
public class BillingInvoiceGroupGenerateRevisionWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoiceGroup> {


	private BillingInvoiceService billingInvoiceService;
	private BillingInvoiceProcessService billingInvoiceProcessService;

	private DocumentManagementService documentManagementService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoiceGroup processAction(BillingInvoiceGroup invoiceGroup, WorkflowTransition transition) {

		List<BillingInvoice> invoiceList = getBillingInvoiceService().getBillingInvoiceGroup(invoiceGroup.getId()).getInvoiceList();
		List<BillingInvoice> newList = null;
		if (!CollectionUtils.isEmpty(invoiceList)) {
			// Take One in the Group and Generate it - will automatically create the group
			BillingInvoice inv = invoiceList.get(0);
			ValidationUtils.assertTrue(inv.getInvoiceType().isBillingDefinitionRequired(), "Grouped Invoices are supported for Invoice Types that use Billing Definitions only.");
			BillingInvoice newInvoice = getBillingInvoiceProcessService().generateBillingInvoice(inv.getBillingDefinition().getId(), inv.getInvoiceDate());
			if (newInvoice.getInvoiceGroup() != null) {
				newList = getBillingInvoiceService().getBillingInvoiceGroup(newInvoice.getInvoiceGroup().getId()).getInvoiceList();
			}
		}

		for (BillingInvoice invoice : invoiceList) {
			for (BillingInvoice newInvoice : CollectionUtils.getIterable(newList)) {
				if (MathUtils.isEqual(invoice.getBillingDefinition().getId(), newInvoice.getBillingDefinition().getId())) {
					getBillingInvoiceService().saveBillingInvoiceParent(newInvoice, invoice);
					getDocumentManagementService().copyDocumentFileList(BillingInvoice.BILLING_INVOICE_TABLE_NAME, BeanUtils.getIdentityAsLong(invoice), BeanUtils.getIdentityAsLong(newInvoice));
				}
			}
		}
		return invoiceGroup;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public BillingInvoiceProcessService getBillingInvoiceProcessService() {
		return this.billingInvoiceProcessService;
	}


	public void setBillingInvoiceProcessService(BillingInvoiceProcessService billingInvoiceProcessService) {
		this.billingInvoiceProcessService = billingInvoiceProcessService;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}
}
