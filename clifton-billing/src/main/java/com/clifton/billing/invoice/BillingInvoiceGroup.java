package com.clifton.billing.invoice;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;


/**
 * The <code>BillingInvoiceGroup</code> contains a list of invoices that are related and must be processed together/move through the workflow together.
 * These invoices will share at least one schedule.
 *
 * @author Mary Anderson
 */
public class BillingInvoiceGroup extends BaseSimpleEntity<Integer> implements WorkflowAware {

	public static final String BILLING_INVOICE_GROUP_TABLE_NAME = "BillingInvoiceGroup";


	private Date invoiceDate;

	/**
	 * The Minimum Workflow State/Status for the Invoices in the Group.
	 */
	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	private RuleViolationStatus violationStatus;

	private List<BillingInvoice> invoiceList;

	/**
	 * Not populated automatically by the system, but leaving here
	 * in case a group note is needed by Kelly/Billing
	 */
	private String note;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public List<BillingInvoice> getInvoiceList() {
		return this.invoiceList;
	}


	public void setInvoiceList(List<BillingInvoice> invoiceList) {
		this.invoiceList = invoiceList;
	}


	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}
}
