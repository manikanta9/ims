package com.clifton.billing.invoice.rule;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceMissingManagerBalanceRuleEvaluator</code> returns violations for dates where we are missing a manager balance
 *
 * @author manderson
 */
public class BillingInvoiceMissingManagerBalanceRuleEvaluator extends BaseBillingInvoiceMissingSourceRuleEvaluator {

	@Override
	public String getSourceTableName() {
		return "InvestmentManagerAccountBalance";
	}


	@Override
	public boolean isUseZeroValueAsMissingSource() {
		return false;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		List<BillingBasisSnapshot> snapshotList = getBillingBasisSnapshotListMissingSource(invoice, context);
		if (!CollectionUtils.isEmpty(snapshotList)) {
			// Get Map of Manager Accounts to Dates Missing
			MultiValueHashMap<Integer, Date> managerMissingDateMap = new MultiValueHashMap<>(false);
			for (BillingBasisSnapshot bbs : snapshotList) {
				Integer managerAccountId = bbs.getBillingDefinitionInvestmentAccount().getInvestmentManagerAccount().getId();
				if (managerMissingDateMap.get(managerAccountId) == null || !managerMissingDateMap.get(managerAccountId).contains(bbs.getSnapshotDate())) {
					managerMissingDateMap.put(managerAccountId, bbs.getSnapshotDate());
				}
			}

			// Check for missing balances for each manager on the date
			for (Map.Entry<Integer, Collection<Date>> managerAccountEntry : managerMissingDateMap.entrySet()) {
				addRuleViolationToList(violationList, entityConfig, invoice, managerAccountEntry.getKey(), new ArrayList<>(managerAccountEntry.getValue()));
			}
		}
		return violationList;
	}
}
