package com.clifton.billing.invoice.report;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.Date;


/**
 * Methods for working with Billing Invoices Reports and System Queries
 *
 * @author manderson
 */
public interface BillingInvoiceReportService {

	////////////////////////////////////////////////////////////////////////////////
	/////////         Billing Invoice Report Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Downloads the report file as defined by a custom field value on the billing definition
	 */
	@SecureMethod(dtoClass = BillingInvoice.class)
	public FileWrapper downloadBillingInvoiceReport(int invoiceId, Boolean includePayments, Boolean regenerateCache);


	@DoNotAddRequestMapping
	public void clearBillingInvoiceReportCache(int invoiceId);


	////////////////////////////////////////////////////////////////////////////////
	////// Billing Basis Snapshot Details - Account Export Business Methods   //////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes system query that takes two parameters invoice date and billing definition investment account.
	 * Returns Data Table with billing basis details.  For cases where a source table is linked to it (i.e. Synthetic Exposure)
	 * will also include all of those details (duration, syn adj, actual contracts, etc).
	 */
	@SecureMethod(dtoClass = BillingBasisSnapshot.class)
	public DataTable getBillingInvoiceAccountBillingBasisSystemQueryResult(String queryName, Date invoiceDate, int billingDefinitionInvestmentAccountId);


	////////////////////////////////////////////////////////////////////////////////
	/////////                Billing Invoice Posting Methods               /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Post an invoice.  The invoice must be in Active or Closed status
	 * All attachments for the invoice under "BillingInvoiceClientAttachments" will also be posted
	 *
	 * @param skipIfDefinitionPostOptionOff is used by workflow transitions that will just skip posting if the definition is turned off, however
	 *                                      if a user tries to manually post an invoice where the definition is turned off they will get an error.
	 * @param updatePortalEntityStatus      should only be set when manually posting the invoice, not through workflow.  This finds the workflow action system bean to get the status template to update,
	 *                                      however when executing through workflow transitions, the status is updated by a subsequent workflow action and not necessary
	 */
	public BillingInvoice postBillingInvoice(int invoiceId, boolean skipIfDefinitionPostOptionOff, boolean updatePortalEntityStatus);


	/**
	 * This is used to update the status of the file on the Portal - i.e. when the invoice is marked as Paid in IMS, the Portal status gets updated.
	 */
	@DoNotAddRequestMapping
	public void updateBillingInvoicePortalEntityStatus(BillingInvoice invoice, String portalStatusTemplate);


	/**
	 * Ability to post an individual attachment.  The attachment MUST belong to
	 * Document Definition "BillingInvoiceClientAttachments"
	 */
	@SecureMethod(dtoClass = BillingInvoice.class)
	public void postBillingInvoiceAttachment(int invoiceId, int documentFileId, FileDuplicateActions fileDuplicateAction);


	/**
	 * Ability to post an individual attachment.  The attachment MUST belong to
	 * Document Definition "BillingInvoiceClientAttachments"
	 * Uses replace option so if existing attachment on Portal is not approved, the file can be replaced instead of removed and re-added
	 */
	@SecureMethod(dtoClass = BillingInvoice.class)
	public void postBillingInvoiceAttachmentReplace(int invoiceId, int documentFileId);
}
