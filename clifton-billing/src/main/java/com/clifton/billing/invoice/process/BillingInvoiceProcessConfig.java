package com.clifton.billing.invoice.process;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountCalculator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.bean.SystemBeanService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BillingInvoiceProcessConfig</code> contains information
 * for an invoice, or group of invoices that is needed to process
 * an invoice.
 *
 * @author manderson
 */
public class BillingInvoiceProcessConfig {

	/**
	 * Used for cases where none of the billing definitions we are
	 * currently processing are NOT shared for any of it's schedules;
	 */
	private BillingInvoice invoice;

	/**
	 * Used for cases where at least one of the billing definitions we are
	 * currently processing IS shared for at least one of it's schedules;
	 */
	private BillingInvoiceGroup invoiceGroup;

	private BillingFrequencies defaultAccrualFrequency;


	private List<BillingSchedule> scheduleList;

	/**
	 * Stores the billing amount calculator beans so we don't have to retrieve them multiple times
	 */
	final Map<Integer, BillingAmountCalculator> billingAmountCalculatorMap = new HashMap<>();


	/**
	 * For each id of BillingDefinitionInvestmentAccount - tracks the list of security exclusions we may need to check while building the billing basis and snapshots
	 */
	private Map<Integer, List<BillingDefinitionInvestmentAccountSecurityExclusion>> accountBillingSecurityExclusionMap = new HashMap<>();


	/**
	 * Tracks the calculated billing basis for all BillingDefinitionInvestment accounts
	 * used for the invoice or invoice group
	 */
	private Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap;

	/**
	 * Does one lookup per account, billing valuation type
	 * that list can then be reused for cases where the same underlying data is used for the same account
	 * but for a different instrument group
	 * Key = AccountID_SourceTableName_StartDate_EndDate
	 */
	private final Map<String, List<?>> accountBillingSourceMap = new HashMap<>();

	// Create Maps of Schedules Processed - Waived Violations
	final Map<Integer, Map<Integer, List<String>>> scheduleWaivedViolationsMap = new HashMap<>();

	// Create Maps of Schedules Processed - Processing Violations
	// Used to include additional detail about schedule processing that users should know, but the schedule isn't waived
	final Map<Integer, Map<Integer, List<String>>> scheduleProcessingViolationsMap = new HashMap<>();

	// Create Maps of Source Data missing - i.e. missing price - or missing manager balance but the processing will continue with a flexible lookup
	// Allows the invoice to process using a "preview" value, but not ignorable violations prevent it from getting approved Used for invoices where one account terminates early so we need that specific account's revenue
	// i.e. InvestmentSecurity and list of dates prices are missing, manager account and list of dates balances are missing...
	final Map<Integer, Map<IdentityObject, Set<Date>>> billingBasisMissingSourceDataViolationMap = new HashMap<>();


	/**
	 * Creates maps of {@link BillingDefinitionInvestmentAccountSecurityExclusion} to a map of String exclude messages and list of dates.  Allows limiting the number of violations per security / exclusion / date range
	 */
	final Map<BillingDefinitionInvestmentAccountSecurityExclusion, Map<String, Set<Date>>> billingBasisSecurityExclusionViolationMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceProcessConfig(BillingInvoice invoice) {
		this.invoice = invoice;
		this.defaultAccrualFrequency = invoice.getBillingDefinition().getBillingFrequency();
	}


	public BillingInvoiceProcessConfig(BillingInvoiceGroup invoiceGroup) {
		this.invoiceGroup = invoiceGroup;
		// All invoices in the group bill using the same frequency
		this.defaultAccrualFrequency = this.getInvoiceList().get(0).getBillingDefinition().getBillingFrequency();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<BillingInvoice> getInvoiceList() {
		if (isUseInvoiceGroup()) {
			return getInvoiceGroup().getInvoiceList();
		}
		return CollectionUtils.createList(getInvoice());
	}


	public List<BillingInvoice> getInvoiceListForSchedule(BillingSchedule schedule, List<BillingScheduleSharing> scheduleSharingList) {
		List<BillingInvoice> filteredList = new ArrayList<>();
		// Only need to filter if we are using an invoice group - otherwise - invoice specific
		if (isUseInvoiceGroup()) {
			for (BillingInvoice inv : getInvoiceGroup().getInvoiceList()) {
				// If Invoice/Schedule Dates match up - include it
				if (DateUtils.isOverlapInDates(schedule.getStartDate(), schedule.getEndDate(), inv.getInvoicePeriodStartDate(), inv.getInvoicePeriodEndDate())) {
					if (schedule.isSharedSchedule()) {
						for (BillingScheduleSharing sharing : CollectionUtils.getIterable(scheduleSharingList)) {
							if (sharing.getReferenceTwo().equals(inv.getBillingDefinition())) {
								filteredList.add(inv);
								break;
							}
						}
					}
					else {
						if (inv.getBillingDefinition().equals(schedule.getBillingDefinition())) {
							filteredList.add(inv);
						}
					}
				}
			}
		}
		else {
			// First Verify Overlap In Schedule Dates - If not, then the schedule wasn't active during the invoice period
			if (DateUtils.isOverlapInDates(getInvoice().getInvoicePeriodStartDate(), getInvoice().getInvoicePeriodEndDate(), schedule.getStartDate(), schedule.getEndDate())) {
				filteredList.add(getInvoice());
			}
		}
		return filteredList;
	}


	public void addInvoiceDetail(BillingInvoiceDetail detail) {
		if (isUseInvoiceGroup()) {
			for (BillingInvoice inv : getInvoiceList()) {
				if (inv.equals(detail.getInvoice())) {
					inv.addBillingInvoiceDetail(detail);
					break;
				}
			}
		}
		else {
			getInvoice().addBillingInvoiceDetail(detail);
		}
	}


	@SuppressWarnings("unchecked")
	public <T> List<T> getAccountBillingSourceMapForAccount(Integer accountId, Class<T> clazz, Date startDate, Date endDate) {
		return (List<T>) this.accountBillingSourceMap.get(getKeyForAccountBillingSourceMap(accountId, clazz.getName(), startDate, endDate));
	}


	public <T> void setAccountBillingSourceMapForAccount(Integer accountId, Class<T> clazz, Date startDate, Date endDate, List<T> valueList) {
		this.accountBillingSourceMap.put(getKeyForAccountBillingSourceMap(accountId, clazz.getName(), startDate, endDate), valueList);
	}


	private String getKeyForAccountBillingSourceMap(Integer accountId, String sourceTableName, Date startDate, Date endDate) {
		return accountId + "_" + sourceTableName + "_" + DateUtils.fromDateRange(startDate, endDate, true);
	}


	public BillingAmountCalculator getBillingAmountCalculatorForSchedule(BillingSchedule schedule, SystemBeanService systemBeanService) {
		return this.billingAmountCalculatorMap.computeIfAbsent(schedule.getScheduleType().getBillingAmountCalculatorBean().getId(), beanId -> (BillingAmountCalculator) systemBeanService.getBeanInstance(schedule.getScheduleType().getBillingAmountCalculatorBean()));
	}

	////////////////////////////////////////////////////////////////////////////////


	public boolean isUseInvoiceGroup() {
		return getInvoiceGroup() != null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoice getInvoice() {
		return this.invoice;
	}


	public BillingInvoiceGroup getInvoiceGroup() {
		return this.invoiceGroup;
	}


	public void setBillingBasisMap(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap) {
		this.billingBasisMap = billingBasisMap;
	}


	public Map<BillingDefinitionInvestmentAccount, BillingBasis> getBillingBasisMap() {
		return this.billingBasisMap;
	}


	public Map<Integer, List<String>> getInvoiceScheduleWaivedViolationsMap(Integer invoiceId) {
		return this.scheduleWaivedViolationsMap.get(invoiceId);
	}


	public void setInvoiceScheduleWaivedViolation(Integer invoiceId, BillingSchedule schedule, String waived) {
		Map<Integer, List<String>> invoiceViolationsMap = this.scheduleWaivedViolationsMap.computeIfAbsent(invoiceId, HashMap::new);
		invoiceViolationsMap.computeIfAbsent(schedule.getId(), ArrayList::new).add(StringUtils.isEmpty(waived) ? waived : "Schedule " + schedule.getName() + " " + waived);
	}


	public Map<Integer, List<String>> getInvoiceScheduleProcessingViolationsMap(Integer invoiceId) {
		return this.scheduleProcessingViolationsMap.get(invoiceId);
	}


	public void setInvoiceScheduleProcessingViolation(Integer invoiceId, BillingSchedule schedule, String processingNote) {
		Map<Integer, List<String>> invoiceViolationsMap = this.scheduleProcessingViolationsMap.computeIfAbsent(invoiceId, HashMap::new);
		invoiceViolationsMap.computeIfAbsent(schedule.getId(), ArrayList::new).add(StringUtils.isEmpty(processingNote) ? processingNote : "Schedule " + schedule.getName() + " " + processingNote);
	}


	public Map<IdentityObject, Set<Date>> getInvoiceBillingBasisMissingSourceDataViolationMap(Integer invoiceId) {
		return this.billingBasisMissingSourceDataViolationMap.get(invoiceId);
	}


	public void setInvoiceBillingBasisMissingSourceDataViolationMap(Integer invoiceId, IdentityObject sourceEntity, Date date) {
		Map<IdentityObject, Set<Date>> invoiceSourceDataViolationMap = this.billingBasisMissingSourceDataViolationMap.computeIfAbsent(invoiceId, HashMap::new);
		Set<Date> dateSet = invoiceSourceDataViolationMap.computeIfAbsent(sourceEntity, k -> new HashSet<>());
		dateSet.add(date);
	}


	public void setAccountBillingSecurityExclusionMap(List<BillingDefinitionInvestmentAccountSecurityExclusion> exclusionList) {
		this.accountBillingSecurityExclusionMap = BeanUtils.getBeansMap(exclusionList, billingDefinitionInvestmentAccountSecurityExclusion -> billingDefinitionInvestmentAccountSecurityExclusion.getBillingDefinitionInvestmentAccount().getId());
	}


	public Map<Integer, List<BillingDefinitionInvestmentAccountSecurityExclusion>> getAccountBillingSecurityExclusionMap() {
		return this.accountBillingSecurityExclusionMap;
	}


	public List<BillingSchedule> getScheduleList() {
		return this.scheduleList;
	}


	public void setScheduleList(List<BillingSchedule> scheduleList) {
		this.scheduleList = scheduleList;
	}


	public BillingFrequencies getDefaultAccrualFrequency() {
		return this.defaultAccrualFrequency;
	}


	public Map<BillingDefinitionInvestmentAccountSecurityExclusion, Map<String, Set<Date>>> getBillingBasisSecurityExclusionViolationMap() {
		return this.billingBasisSecurityExclusionViolationMap;
	}
}
