package com.clifton.billing.invoice.process.schedule.amount.proraters;


import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;


/**
 * The <code>BillingAmountForBillingBasisProrater</code> prorates the amount based on the dates from the billing basis detail.  Used most commonly
 * for Period Trade Dates calculations.
 *
 * @author manderson
 */
public class BillingAmountForBillingBasisProrater extends BaseBillingAmountProrater {

	@Override
	protected void populateBillingAmountDateProraterConfig(BillingAmountDateProraterConfig proraterConfig, BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail) {
		// In Advance Prorating - This is because the billing basis itself when not
		// in advance has 0 fillers which already accurately pro-rates the amount because of the lower billing basis. 
		// However, when in advance we could have a billing basis from the entire previous quarter, but the invoice period is ending
		// so these cases we would take the entire billing basis and calculate the amount and then prorate it.
		if (scheduleConfig.getPaymentInAdvance()) {
			proraterConfig.setProrateStartDate(scheduleConfig.getInvoiceBillingStartDate());
			proraterConfig.setProrateEndDate(scheduleConfig.getInvoiceBillingEndDate());
		}
		// If not in advance, the only time we prorate is when the billing basis details is for a specific time frame
		// This currently applies to the PERIOD TRADE DATES calculation.  PERIOD_TRADE_DATES calculations cannot be defined
		// for In Advance definitions
		else {
			// If prorating is necessary i.e. Actual Billing is not for the full period
			proraterConfig.setProrateStartDate(billingBasisDetail.getStartDate());
			proraterConfig.setProrateEndDate(billingBasisDetail.getEndDate());
		}
	}
}
