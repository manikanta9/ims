package com.clifton.billing.invoice.search;


import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingInvoiceDetailAccountSearchForm</code> ...
 *
 * @author manderson
 */
public class BillingInvoiceDetailAccountSearchForm extends BaseSystemHierarchyItemSearchForm {

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "invoice.id")
	private Integer invoiceId;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "invoice.id")
	private Integer[] invoiceIds;

	@SearchField(searchFieldPath = "invoiceDetail.invoice", searchField = "invoiceGroup.id")
	private Integer invoiceGroupId;

	@SearchField(searchFieldPath = "invoiceDetail.invoice", searchField = "invoiceDate")
	private Date invoiceDate;

	@SearchField(searchFieldPath = "invoiceDetail.invoice", searchField = "billingDefinition.id")
	private Integer billingDefinitionId;

	@SearchField(searchFieldPath = "invoiceDetail.invoice", searchField = "billingDefinition.id")
	private Integer[] billingDefinitionIds;

	@SearchField(searchField = "name,legalName", searchFieldPath = "investmentAccount.businessClient", sortField = "name")
	private String clientName;

	@SearchField(searchField = "name,legalName", searchFieldPath = "investmentAccount.businessClient.clientRelationship", sortField = "name")
	private String clientRelationshipName;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "name,number", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchFieldPath = "invoiceDetail.billingSchedule", searchField = "name")
	private String scheduleName;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "billingSchedule.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean manualAdjustment;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "accrualStartDate")
	private Date accrualStartDate;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "accrualEndDate")
	private Date accrualEndDate;

	@SearchField
	private BigDecimal billingBasis;

	@SearchField
	private BigDecimal allocationPercentage;

	@SearchField
	private BigDecimal billingAmount;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "revenueType,billingSchedule.revenueType", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private BillingInvoiceRevenueTypes revenueType;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "revenueType,billingSchedule.revenueType", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, comparisonConditions = ComparisonConditions.NOT_EQUALS, leftJoin = true)
	private BillingInvoiceRevenueTypes excludeRevenueType;

	@SearchField(searchFieldPath = "invoiceDetail", searchField = "revenueType,billingSchedule.revenueType", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private BillingInvoiceRevenueTypes[] revenueTypes;

	@SearchField(searchFieldPath = "invoiceDetail.invoice.workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStatusName;

	@SearchField(searchFieldPath = "invoiceDetail.invoice.workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStatusName;

	@SearchField(searchFieldPath = "invoiceDetail.invoice.workflowStatus", searchField = "name")
	private String workflowStatusNameContains;

	@SearchField(searchFieldPath = "invoiceDetail.invoice.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStateName;

	@SearchField(searchFieldPath = "invoiceDetail.invoice.workflowState", searchField = "name")
	private String workflowStateNameContains;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "BillingInvoiceDetailAccount";
	}


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@Override
	public void validate() {
		// By Default "Voided" invoices are excluded, so need to require a filter besides excludeWorkflowStatusName
		SearchUtils.validateRequiredFilter(this, "excludeWorkflowStatusName");
	}


	////////////////////////////////////////////////////////////////////////////////


	public Integer getInvoiceId() {
		return this.invoiceId;
	}


	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getScheduleName() {
		return this.scheduleName;
	}


	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}


	public BigDecimal getBillingBasis() {
		return this.billingBasis;
	}


	public void setBillingBasis(BigDecimal billingBasis) {
		this.billingBasis = billingBasis;
	}


	public BigDecimal getAllocationPercentage() {
		return this.allocationPercentage;
	}


	public void setAllocationPercentage(BigDecimal allocationPercentage) {
		this.allocationPercentage = allocationPercentage;
	}


	public BigDecimal getBillingAmount() {
		return this.billingAmount;
	}


	public void setBillingAmount(BigDecimal billingAmount) {
		this.billingAmount = billingAmount;
	}


	public String getExcludeWorkflowStatusName() {
		return this.excludeWorkflowStatusName;
	}


	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName) {
		this.excludeWorkflowStatusName = excludeWorkflowStatusName;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	public String getWorkflowStatusNameContains() {
		return this.workflowStatusNameContains;
	}


	public void setWorkflowStatusNameContains(String workflowStatusNameContains) {
		this.workflowStatusNameContains = workflowStatusNameContains;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public String getWorkflowStateNameContains() {
		return this.workflowStateNameContains;
	}


	public void setWorkflowStateNameContains(String workflowStateNameContains) {
		this.workflowStateNameContains = workflowStateNameContains;
	}


	public Integer getInvoiceGroupId() {
		return this.invoiceGroupId;
	}


	public void setInvoiceGroupId(Integer invoiceGroupId) {
		this.invoiceGroupId = invoiceGroupId;
	}


	public Integer[] getInvoiceIds() {
		return this.invoiceIds;
	}


	public void setInvoiceIds(Integer[] invoiceIds) {
		this.invoiceIds = invoiceIds;
	}


	public Boolean getManualAdjustment() {
		return this.manualAdjustment;
	}


	public void setManualAdjustment(Boolean manualAdjustment) {
		this.manualAdjustment = manualAdjustment;
	}


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Integer[] getBillingDefinitionIds() {
		return this.billingDefinitionIds;
	}


	public void setBillingDefinitionIds(Integer[] billingDefinitionIds) {
		this.billingDefinitionIds = billingDefinitionIds;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Date getAccrualStartDate() {
		return this.accrualStartDate;
	}


	public void setAccrualStartDate(Date accrualStartDate) {
		this.accrualStartDate = accrualStartDate;
	}


	public Date getAccrualEndDate() {
		return this.accrualEndDate;
	}


	public void setAccrualEndDate(Date accrualEndDate) {
		this.accrualEndDate = accrualEndDate;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public BillingInvoiceRevenueTypes getRevenueType() {
		return this.revenueType;
	}


	public void setRevenueType(BillingInvoiceRevenueTypes revenueType) {
		this.revenueType = revenueType;
	}


	public BillingInvoiceRevenueTypes[] getRevenueTypes() {
		return this.revenueTypes;
	}


	public void setRevenueTypes(BillingInvoiceRevenueTypes[] revenueTypes) {
		this.revenueTypes = revenueTypes;
	}


	public BillingInvoiceRevenueTypes getExcludeRevenueType() {
		return this.excludeRevenueType;
	}


	public void setExcludeRevenueType(BillingInvoiceRevenueTypes excludeRevenueType) {
		this.excludeRevenueType = excludeRevenueType;
	}
}
