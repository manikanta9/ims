package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>BillingInvoiceGroupProcessWorkflowAction</code> handles calling the service method to re-process all invoices
 * in an invoice group.
 *
 * @author Mary Anderson
 */
public class BillingInvoiceGroupProcessWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoiceGroup> {

	private BillingInvoiceProcessService billingInvoiceProcessService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoiceGroup processAction(BillingInvoiceGroup invoiceGroup, WorkflowTransition transition) {
		getBillingInvoiceProcessService().processBillingInvoiceGroup(invoiceGroup.getId());
		return invoiceGroup;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceProcessService getBillingInvoiceProcessService() {
		return this.billingInvoiceProcessService;
	}


	public void setBillingInvoiceProcessService(BillingInvoiceProcessService billingInvoiceProcessService) {
		this.billingInvoiceProcessService = billingInvoiceProcessService;
	}
}
