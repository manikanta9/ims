package com.clifton.billing.invoice.rule;

import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleTier;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The BillingInvoiceBillingBasisGreaterThanMaxTierRuleEvaluator generates a violation if there is a tiered schedule applied and the billing basis exceeded the max tier
 * Most cases we have an open ended top tier - in some cases we don't have that and anything over the existing top tier may be open to negotiation once we surpass it.
 *
 * @author manderson
 */
public class BillingInvoiceBillingBasisGreaterThanMaxTierRuleEvaluator extends BaseBillingInvoiceRuleEvaluator {


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		List<BillingInvoiceDetail> detailList = context.getBillingInvoiceDetailList(invoice.getId());
		for (BillingInvoiceDetail detail : CollectionUtils.getIterable(detailList)) {
			BillingSchedule schedule = detail.getBillingSchedule();
			if (schedule != null && schedule.getScheduleType().isTiered()) {
				schedule = context.getBillingSchedule(schedule.getId());
				BigDecimal maxTier = BigDecimal.ZERO;
				for (BillingScheduleTier tier : CollectionUtils.getIterable(schedule.getScheduleTierList())) {
					// If there is a tier with no threshold - nothing to compare
					if (tier.getThresholdAmount() == null) {
						maxTier = null;
						break;
					}
					if (MathUtils.isGreaterThan(tier.getThresholdAmount(), maxTier)) {
						maxTier = tier.getThresholdAmount();
					}
				}
				if (maxTier != null && MathUtils.isGreaterThan(detail.getBillingBasis(), maxTier)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("maxTier", maxTier);
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, invoice.getId(), detail.getId(), null, templateValues));
				}
			}
		}
		return violationList;
	}
}
