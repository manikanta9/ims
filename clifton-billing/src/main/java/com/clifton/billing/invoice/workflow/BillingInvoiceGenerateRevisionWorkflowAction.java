package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>BillingInvoiceGenerateRevisionWorkflowAction</code> is called after the original invoice is Voided
 * and will generate a new invoice with the original as the parent.
 * <p>
 * Also copies all attachments from the original to the new invoice
 *
 * @author manderson
 */
public class BillingInvoiceGenerateRevisionWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {


	private BillingInvoiceService billingInvoiceService;
	private BillingInvoiceProcessService billingInvoiceProcessService;

	private DocumentManagementService documentManagementService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice invoice, WorkflowTransition transition) {
		BillingInvoice newInvoice;
		if (invoice.getInvoiceType().isBillingDefinitionRequired()) {
			newInvoice = getBillingInvoiceProcessService().generateBillingInvoice(invoice.getBillingDefinition().getId(), invoice.getInvoiceDate());
			// Call simple set parent method since detail lines are generated via other Workflow actions
			// and we don't want to clear them
			getBillingInvoiceService().saveBillingInvoiceParent(newInvoice, invoice);
		}
		else {
			newInvoice = BeanUtils.cloneBean(invoice, false, false);
			newInvoice.setViolationStatus(null);
			newInvoice.setWorkflowState(null);
			newInvoice.setWorkflowStatus(null);

			for (BillingInvoiceDetail detail : CollectionUtils.getIterable(getBillingInvoiceService().getBillingInvoicePopulated(invoice.getId()).getDetailList())) {
				BillingInvoiceDetail newDetail = BeanUtils.cloneBean(detail, false, false);
				newDetail.setId(null);
				newDetail.setInvoice(null);
				newInvoice.addBillingInvoiceDetail(newDetail);
			}
			// Set the parent and do a full save since we manually copied the detail lines
			newInvoice.setParent(invoice);
			getBillingInvoiceService().saveBillingInvoice(newInvoice);
		}
		// Copy All Attachments
		getDocumentManagementService().copyDocumentFileList(BillingInvoice.BILLING_INVOICE_TABLE_NAME, BeanUtils.getIdentityAsLong(invoice), BeanUtils.getIdentityAsLong(newInvoice));
		return invoice;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public BillingInvoiceProcessService getBillingInvoiceProcessService() {
		return this.billingInvoiceProcessService;
	}


	public void setBillingInvoiceProcessService(BillingInvoiceProcessService billingInvoiceProcessService) {
		this.billingInvoiceProcessService = billingInvoiceProcessService;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}
}
