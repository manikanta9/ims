package com.clifton.billing.invoice.rule;

import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;


/**
 * The <code>BillingInvoiceRuleEvaluatorContext</code> is used by all Billing Management Fee evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 */
public class BillingInvoiceRuleEvaluatorContext extends BaseRuleEvaluatorContext {


	private BillingDefinitionService billingDefinitionService;

	private BillingInvoiceService billingInvoiceService;

	private WorkflowHistoryService workflowHistoryService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<WorkflowHistory> getClientAccountWorkflowHistory(InvestmentAccount clientAccount) {
		String key = "ACCOUNT_WORKFLOW_HISTORY_" + clientAccount.getId();
		return getContext().getOrSupplyBean(key, () -> getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(clientAccount, true), ArrayList::new);
	}


	public List<BillingInvoiceDetail> getBillingInvoiceDetailList(int invoiceId) {
		String key = "INVOICE_DETAIL_LIST";
		return getContext().getOrSupplyBean(key, () -> getBillingInvoiceService().getBillingInvoicePopulated(invoiceId).getDetailList(), ArrayList::new);
	}


	public List<BillingInvoiceDetailAccount> getBillingInvoiceDetailAccountList(int invoiceId) {
		String key = "INVOICE_DETAIL_ACCOUNT_LIST";
		return getContext().getOrSupplyBean(key, () -> {
			BillingInvoiceDetailAccountSearchForm searchForm = new BillingInvoiceDetailAccountSearchForm();
			searchForm.setInvoiceId(invoiceId);
			return getBillingInvoiceService().getBillingInvoiceDetailAccountList(searchForm);
		}, ArrayList::new);
	}


	public BillingSchedule getBillingSchedule(int scheduleId) {
		String key = "SCHEDULE_MAP_" + scheduleId;
		// Schedule Lookup By ID won't be null, but cache it because it has additional info associated with it - like Tiers
		return getContext().getOrSupplyBean(key, () -> getBillingDefinitionService().getBillingSchedule(scheduleId));
	}


	/**
	 * Note - this must pass in the supplier because it uses accounting project data which isn't accessible to Billing
	 */
	public boolean isPositionsOnForAccountAndDate(int investmentAccountId, Date date, Supplier<Boolean> functionReturningPositionsOnOffIfMissing) {
		String key = "ACCOUNT_POSITIONS_ON_" + investmentAccountId + "_" + DateUtils.fromDateShort(date);
		return getContext().getOrSupplyBean(key, functionReturningPositionsOnOffIfMissing);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}
}
