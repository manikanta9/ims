package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingAmountForAnnualBoundaryCalculator</code> class calculates the amount to
 * account for an ANNUAL boundary crossing - either MinimumFee (i.e. add amount to bring invoice up to minimum annual charge)
 * or Maximum Fee (i.e. subtract amount that invoice is over the maximum by) year to date.
 * <p>
 * Minimum Fees are ONLY applied if it's the last billing invoice on the annual invoice period.
 * Maximum Fees are always checked so that we don't overcharge and than have to discount later
 *
 * @author Mary Anderson
 */
public class BillingAmountForAnnualBoundaryCalculator extends BillingAmountForBoundaryCalculator {


	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		return BillingAmountCalculatorTypes.ANNUAL_BOUNDARY;
	}


	@Override
	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		ValidationUtils.assertTrue(BillingFrequencies.ANNUALLY == scheduleConfig.getSchedule().getScheduleFrequency().getBillingFrequency(),
				"Cannot calculate Annual Boundaries for a schedule that isn't set up with an annual frequency");

		StringBuilder note = new StringBuilder(16);

		// Annual Minimum only applies if it is the last invoice of the annual period
		boolean minimum = isMinimum(scheduleConfig.getSchedule());
		if (minimum && !scheduleConfig.isLastInvoiceForAnnualInvoicePeriod()) {
			note.append("Mid-Year Invoice.  Current Annual Invoice Period Ends on [").append(DateUtils.fromDateShort(scheduleConfig.getAnnualAbsoluteInvoicePeriodEndDate())).append("].");
			addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
			return;
		}

		BigDecimal amount = calculateAmount(scheduleConfig, null, note);

		BigDecimal totalBilled = BigDecimal.ZERO;

		List<BillingInvoice> annualInvoiceList = getAnnualInvoiceList(scheduleConfig, minimum);
		for (BillingInvoice invoice : CollectionUtils.getIterable(annualInvoiceList)) {
			BigDecimal invoiceTotal = BillingAmountCalculatorUtils.getBillingInvoiceTotalAmountCalculated(invoice, scheduleConfig.getSchedule(), getRevenueTypeList());
			totalBilled = MathUtils.add(totalBilled, invoiceTotal);
		}

		BigDecimal billingAmount = BigDecimal.ZERO;

		// If less than minimum or more than maximum
		if ((minimum && MathUtils.isLessThan(totalBilled, amount)) || (!minimum && MathUtils.isGreaterThan(totalBilled, amount))) {
			billingAmount = MathUtils.subtract(amount, totalBilled);
		}
		appendBoundaryNote(scheduleConfig, minimum, note, amount, totalBilled);
		applyBillingAmountToInvoices(config, scheduleConfig, annualInvoiceList, totalBilled, billingAmount, note);
	}


	protected boolean isIncludeInvoiceDetailInTotalBilled(BillingInvoiceDetail detail, BillingScheduleProcessConfig scheduleConfig) {
		return !(detail.getBillingSchedule() != null && scheduleConfig.getSchedule().equals(detail.getBillingSchedule()));
	}


	protected void applyBillingAmountToInvoices(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig, List<BillingInvoice> annualInvoiceList,
	                                            BigDecimal totalBilled, BigDecimal billingAmount, StringBuilder note) {
		if (MathUtils.isNullOrZero(billingAmount)) {
			addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
		}
		else {
			List<BillingInvoiceDetail> detailList = new ArrayList<>();

			for (BillingInvoice thisInvoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
				BigDecimal invoiceTotal = BigDecimal.ZERO;
				Map<InvestmentAccount, BigDecimal> accountAllocationMap = new HashMap<>();

				for (BillingInvoice invoice : CollectionUtils.getIterable(annualInvoiceList)) {
					if (invoice.getBillingDefinition().equals(thisInvoice.getBillingDefinition())) {

						for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
							if (isIncludeInvoiceDetailInTotalBilled(invoiceDetail, scheduleConfig)) {
								invoiceTotal = MathUtils.add(invoiceTotal, invoiceDetail.getBillingAmount());
								for (BillingInvoiceDetailAccount invoiceDetailAccount : CollectionUtils.getIterable(invoiceDetail.getDetailAccountList())) {
									InvestmentAccount acct = invoiceDetailAccount.getInvestmentAccount() != null ? invoiceDetailAccount.getInvestmentAccount()
											: invoiceDetailAccount.getBillingDefinitionInvestmentAccount().getReferenceTwo();
									accountAllocationMap.put(acct, MathUtils.add(accountAllocationMap.get(acct), invoiceDetailAccount.getBillingAmount()));
								}
							}
						}
					}
				}

				BigDecimal allocationPercentage = MathUtils.round(CoreMathUtils.getPercentValue(invoiceTotal, totalBilled, true), 2);
				if (MathUtils.isNullOrZero(totalBilled)) {
					allocationPercentage = MathUtils.round(MathUtils.divide(MathUtils.BIG_DECIMAL_ONE_HUNDRED, CollectionUtils.getSize(scheduleConfig.getInvoiceList())), 2);
				}
				BigDecimal allocationAmount = MathUtils.getPercentageOf(allocationPercentage, billingAmount, true);

				String invoiceDetailNote = note.toString();
				if (allocationPercentage != null && !MathUtils.isEqual(allocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
					invoiceDetailNote += StringUtils.NEW_LINE + "Allocated for " + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, allocationPercentage, true)
							+ " of total fees";
				}
				BillingInvoiceDetail detail = createInvoiceDetail(thisInvoice, scheduleConfig, null, allocationAmount, invoiceDetailNote);
				if (CollectionUtils.isEmpty(accountAllocationMap)) {
					detail.setDetailAccountList(getBillingInvoiceDetailAccountCalculator().calculateBillingInvoiceDetailAccountList(thisInvoice, detail, allocationPercentage, null, null,
							scheduleConfig.getProportionalSplitAccountList(thisInvoice.getId())));
				}
				else {
					createInvoiceDetailAccountListFromPreviouslyBilledAllocations(detail, accountAllocationMap);
				}
				detailList.add(detail);
			}
			CoreMathUtils.applySumPropertyDifference(detailList, BillingInvoiceDetail::getBillingAmount, BillingInvoiceDetail::setBillingAmount, billingAmount, true);
			for (BillingInvoiceDetail detail : CollectionUtils.getIterable(detailList)) {
				config.addInvoiceDetail(detail);
			}
		}
	}


	@Override
	public BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, @SuppressWarnings("unused") BillingBasisDetail billingBasisDetail, StringBuilder note) {
		BigDecimal amount = scheduleConfig.getSchedule().getScheduleAmount();
		note.append("Annual ").append(isMinimum(scheduleConfig.getSchedule()) ? "Minimum" : "Maximum").append(" Fee: ");
		note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, amount, false));
		return amount;
	}


	@Override
	protected void appendBoundaryNote(BillingScheduleProcessConfig scheduleConfig, boolean minimum, StringBuilder note, BigDecimal amount, BigDecimal totalBilled) {
		// Show again, only if it was prorated - i.e. not the same as the amount on the schedule
		if (!MathUtils.isEqual(scheduleConfig.getSchedule().getScheduleAmount(), amount)) {
			note.append(StringUtils.TAB).append(minimum ? "Minimum" : "Maximum").append(" Fee: ");
			note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, amount, false));
		}
		note.append(StringUtils.TAB + "Billed YTD: ").append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, totalBilled, false));
	}


	protected List<BillingInvoice> getAnnualInvoiceList(BillingScheduleProcessConfig scheduleConfig, boolean requireAll) {
		List<BillingInvoice> invoiceList = new ArrayList<>();

		Date startDate = scheduleConfig.getAnnualInvoicePeriodStartDate();
		Date endDate = scheduleConfig.getAnnualAbsoluteInvoicePeriodEndDate();

		if (scheduleConfig.getPaymentInAdvance()) {
			startDate = DateUtils.addMonths(startDate, -scheduleConfig.getBillingFrequency().getMonthsInPeriod());
			endDate = DateUtils.addMonths(endDate, -scheduleConfig.getBillingFrequency().getMonthsInPeriod());
		}

		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
			searchForm.setInvoiceStartDate(startDate);
			searchForm.setInvoiceEndDate(endDate);
			searchForm.setBillingDefinitionId(invoice.getBillingDefinition().getId());
			// Exclude this invoice - want to keep what we've generated so far
			searchForm.setExcludeInvoiceDate(invoice.getInvoiceDate());
			List<BillingInvoice> defInvoiceList = getBillingInvoicePopulatedList(searchForm);

			// Note: This may not be the most efficient way, but this way we can validate each billing definition has an invoice that applies on that date
			Date date = scheduleConfig.getBillingFrequency().getLastDayOfBillingCycle(startDate, invoice.getBillingDefinition().getSecondYearStartDate());
			while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
				boolean found = false;
				if (DateUtils.compare(date, invoice.getInvoiceDate(), false) == 0) {
					invoiceList.add(invoice);
					found = true;
				}
				else {
					for (BillingInvoice defInvoice : CollectionUtils.getIterable(defInvoiceList)) {
						if (DateUtils.compare(defInvoice.getInvoiceDate(), date, false) == 0 && !defInvoice.isCanceled()) {
							found = true;
							invoiceList.add(defInvoice);
							break;
						}
					}
				}
				// Only require if definition doesn't have an end date or definition end date is after the invoice date we are looking for...
				if (!found && (invoice.getBillingDefinition().getEndDate() == null || DateUtils.isDateAfterOrEqual(invoice.getBillingDefinition().getEndDate(), date))) {
					// If maximum check, we don't necessarily have the full year of invoices yet, just go up year to date
					// Minimums only check on last invoice of the year, so always required
					if (requireAll) {
						throw new ValidationException("Unable to process schedule [" + scheduleConfig.getSchedule().getName() + "] because the invoice for Billing Definition ["
								+ invoice.getBillingDefinition().getLabel() + "] does not exist on [" + DateUtils.fromDateShort(date) + "]");
					}
				}
				date = DateUtils.addDays(date, 1);
				date = scheduleConfig.getBillingFrequency().getLastDayOfBillingCycle(date, invoice.getBillingDefinition().getSecondYearStartDate());
			}
		}
		return invoiceList;
	}


	private List<BillingInvoice> getBillingInvoicePopulatedList(BillingInvoiceSearchForm searchForm) {
		List<BillingInvoice> invoiceList = getBillingInvoiceService().getBillingInvoiceList(searchForm);
		List<BillingInvoice> popList = new ArrayList<>();
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			invoice = getBillingInvoiceService().getBillingInvoicePopulated(invoice.getId());
			for (BillingInvoiceDetail detail : CollectionUtils.getIterable(invoice.getDetailList())) {
				detail.setDetailAccountList(getBillingInvoiceService().getBillingInvoiceDetailAccountListByInvoiceDetail(detail.getId()));
			}
			popList.add(invoice);
		}
		return popList;
	}
}
