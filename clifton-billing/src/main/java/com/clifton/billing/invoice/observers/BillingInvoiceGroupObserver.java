package com.clifton.billing.invoice.observers;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BillingInvoiceGroupObserver</code> ...
 * <p>
 * When the Invoice Group is transitioned, it pushes the invoice through the "mirrored" workflow
 *
 * @author Mary Anderson
 */
@Component
public class BillingInvoiceGroupObserver extends SelfRegisteringDaoObserver<BillingInvoiceGroup> {

	private BillingInvoiceService billingInvoiceService;
	private WorkflowTransitionService workflowTransitionService;


	public BillingInvoiceGroupObserver() {
		super();
		// Setting order to below default so this Observer runs before those at default.
		setOrder(-100);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<BillingInvoiceGroup> dao, DaoEventTypes event, BillingInvoiceGroup bean) {
		if (event.isUpdate()) {
			// Get original bean so we have it for later
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<BillingInvoiceGroup> dao, DaoEventTypes event, BillingInvoiceGroup bean, Throwable e) {
		if (e == null) {
			BillingInvoiceGroup original = null;
			if (event.isUpdate()) {
				original = getOriginalBean(dao, bean);
			}
			// Only If a change in workflow state
			if (original != null && original.getWorkflowState() != null && !original.getWorkflowState().equals(bean.getWorkflowState())) {
				// Pull From the db so invoice list is populated
				BillingInvoiceGroup group = getBillingInvoiceService().getBillingInvoiceGroup(bean.getId());
				WorkflowState nextState = null;
				for (BillingInvoice invoice : CollectionUtils.getIterable(group.getInvoiceList())) {
					if (nextState == null) {
						nextState = getNextWorkflowState(invoice, bean.getWorkflowState());
					}
					if (nextState != null) {
						getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), nextState.getId());
					}
				}
			}
		}
	}


	private WorkflowState getNextWorkflowState(BillingInvoice invoice, WorkflowState groupState) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(BillingInvoice.BILLING_INVOICE_TABLE_NAME, BeanUtils.getIdentityAsLong(invoice));
		for (WorkflowTransition trans : CollectionUtils.getIterable(transitionList)) {
			if (trans.isSystemDefined()) {
				if (groupState.getName().equalsIgnoreCase(trans.getEndWorkflowState().getName())) {
					return trans.getEndWorkflowState();
				}
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
