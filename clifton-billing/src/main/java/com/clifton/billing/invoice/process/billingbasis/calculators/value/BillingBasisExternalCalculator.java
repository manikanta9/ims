package com.clifton.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.billingbasis.BillingBasisExternal;
import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.billingbasis.search.BillingBasisExternalSearchForm;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisExternalValueCalculator</code> generates the BillingBasisSnapshot list as a list of values as entered into the BillingBasisExternal table
 *
 * @author Mary Anderson
 */
public class BillingBasisExternalCalculator extends BaseBillingBasisValueCalculator {

	private BillingDefinitionService billingDefinitionService;
	private BillingBasisService billingBasisService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * External can have data entered on any date
	 */
	@Override
	public boolean isValidValuationDate(Date date) {
		return true;
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		return DateUtils.addDays(date, (moveForward ? 1 : -1));
	}


	/**
	 * Does not use config object to cache look ups because {@link BillingBasisExternal} values are directly entered for each {@link BillingDefinitionInvestmentAccount} individually
	 */
	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		List<BillingBasisSnapshot> list = new ArrayList<>();
		List<BillingBasisExternal> externalList = getBillingBasisExternalList(billingDefinitionInvestmentAccountId, startDate, endDate);

		for (BillingBasisExternal ext : CollectionUtils.getIterable(externalList)) {
			list.add(createBillingBasisSnapshot(invoice, billingAccount, ext));
		}
		return list;
	}


	private List<BillingBasisExternal> getBillingBasisExternalList(int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		List<BillingBasisExternal> externalList = getBillingBasisExternalListForBillingAccountAndDateRange(billingDefinitionInvestmentAccountId, startDate, endDate);
		if (isProjectedRevenue() && CollectionUtils.isEmpty(externalList)) {
			// Try Previous Month
			Date externalEndDate = DateUtils.getLastDayOfPreviousMonth(startDate);
			Date externalStartDate = DateUtils.getFirstDayOfMonth(externalEndDate);
			externalList = getBillingBasisExternalListForBillingAccountAndDateRange(billingDefinitionInvestmentAccountId, externalStartDate, externalEndDate);

			// If Still Empty - Try last month of previous quarter
			if (CollectionUtils.isEmpty(externalList)) {
				externalEndDate = DateUtils.getLastDayOfPreviousQuarter(startDate);
				externalStartDate = DateUtils.getFirstDayOfMonth(externalEndDate);
				externalList = getBillingBasisExternalListForBillingAccountAndDateRange(billingDefinitionInvestmentAccountId, externalStartDate, externalEndDate);
			}

			// If Still Empty - Go back one more quarter
			if (CollectionUtils.isEmpty(externalList)) {
				externalEndDate = DateUtils.getLastDayOfPreviousQuarter(externalEndDate);
				externalStartDate = DateUtils.getFirstDayOfMonth(externalEndDate);
				externalList = getBillingBasisExternalListForBillingAccountAndDateRange(billingDefinitionInvestmentAccountId, externalStartDate, externalEndDate);
			}
		}
		return externalList;
	}


	private List<BillingBasisExternal> getBillingBasisExternalListForBillingAccountAndDateRange(int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingBasisExternalSearchForm searchForm = new BillingBasisExternalSearchForm();
		searchForm.setBillingDefinitionInvestmentAccountId(billingDefinitionInvestmentAccountId);
		if (startDate != null) {
			searchForm.addSearchRestriction(new SearchRestriction("date", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		}
		if (endDate != null) {
			searchForm.addSearchRestriction(new SearchRestriction("date", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		}
		return getBillingBasisService().getBillingBasisExternalList(searchForm);
	}


	/**
	 * External returns all dates values are entered for - if type is set to dates we expect users to enter the "trade" dates
	 * that the positions are valued on
	 */
	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		// Special Case where Dates to be valued are explicitly set in the External Billing Basis
		List<BillingBasisExternal> externalList = getBillingBasisExternalList(billingDefinitionInvestmentAccount.getId(), startDate, endDate);
		for (BillingBasisExternal external : CollectionUtils.getIterable(externalList)) {
			if (currentTradeDateList.contains(external.getDate())) {
				continue;
			}
			currentTradeDateList.add(external.getDate());
		}
		return currentTradeDateList;
	}


	////////////////////////////////////////////////////////////////////////////////


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, BillingBasisExternal billingBasisExternal) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setAssetClass(billingBasisExternal.getAssetClass());
		bbs.setSnapshotDate(billingBasisExternal.getDate());
		bbs.setSnapshotValue(billingBasisExternal.getBillingBasisAmount());
		if (billingBasisExternal.isBillingFxRateSupported() && !MathUtils.isNullOrZero(billingBasisExternal.getBillingFxRate())) {
			bbs.setFxRate(billingBasisExternal.getBillingFxRate());
		}
		return bbs;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
