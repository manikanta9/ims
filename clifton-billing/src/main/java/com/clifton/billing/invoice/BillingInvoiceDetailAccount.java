package com.clifton.billing.invoice;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>BillingInvoiceDetailAccount</code> allocates the invoice detail line into account specific charges.  This is useful
 * for projected revenue and calculates sales commissions on accounts.
 *
 * @author manderson
 */
public class BillingInvoiceDetailAccount extends BaseSimpleEntity<Integer> implements BillingInvoiceRevenueTypeAmountAware {

	private BillingInvoiceDetail invoiceDetail;

	/**
	 * Which account definition this detail's account proportion is for
	 */
	private BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount;

	/**
	 * When BillingDefinitionInvestmentAccount is set then this is auto populated from there
	 * Otherwise, in cases where billing basis is not used, i.e. flat fees a line will be created for each
	 * actual account and the total billing amount will be divided proportionally across accounts.
	 * Can't rely on splitting proportionally across billingDefinitionInvestmentAccount because in some cases there could
	 * be multiple definitions for one account, but only one for another account and then the split wouldn't be even.
	 */
	private InvestmentAccount investmentAccount;

	/**
	 * The amount that BillingSchedule is applied to in order to calculate billingAmount for this billingDefinitionInvestmentAccount only
	 */
	private BigDecimal billingBasis;

	/**
	 * Percentage of the billing amount on the detail line that this account gets
	 */
	private BigDecimal allocationPercentage;

	/**
	 * allocationPercentage of BillingInvoiceDetail billingAmount
	 * Although it could be calculated based on the allocation percentage, saving it because
	 * the biggest one is slightly adjusted to account for rounding and ensures the total of the account detail billing amount equals
	 * the invoice detail billing amount.
	 */
	private BigDecimal billingAmount;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder lbl = new StringBuilder(getAccountLabel());
		lbl.append("_BillingBasis_").append(CoreMathUtils.formatNumberInteger(getBillingBasis()));
		lbl.append("_Percentage_").append(CoreMathUtils.formatNumberDecimal(getAllocationPercentage()));
		lbl.append("_BillingAmount_").append(CoreMathUtils.formatNumberMoney(getBillingAmount()));
		return lbl.toString();
	}


	public String getAccountLabel() {
		if (getBillingDefinitionInvestmentAccount() != null) {
			return getBillingDefinitionInvestmentAccount().getAccountLabel();
		}
		if (getInvestmentAccount() != null) {
			return getInvestmentAccount().getLabel();
		}
		return null;
	}


	/**
	 * Helpful for Grouped Invoices - Returns the % of billing basis from the shared schedule, not just for that invoice
	 */
	public BigDecimal getBillingBasisPercentage() {
		if (getInvoiceDetail() == null || MathUtils.isNullOrZero(getInvoiceDetail().getBillingBasis())) {
			return null;
		}
		return CoreMathUtils.getPercentValue(getBillingBasis(), getInvoiceDetail().getBillingBasis(), true);
	}


	/**
	 * Could do this via UI, but this will allow the values to show correctly on export to excel
	 */

	private BillingInvoiceRevenueTypes getRevenueType() {
		if (getInvoiceDetail() != null) {
			return getInvoiceDetail().getCoalesceRevenueType();
		}
		return BillingInvoiceRevenueTypes.NONE;
	}


	@Override
	public BigDecimal getRevenueGrossTotalAmount() {
		if (getRevenueType() == BillingInvoiceRevenueTypes.REVENUE) {
			return getBillingAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueShareInternalTotalAmount() {
		if (getRevenueType() == BillingInvoiceRevenueTypes.REVENUE_SHARE_INTERNAL) {
			return getBillingAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueShareExternalTotalAmount() {
		if (getRevenueType() == BillingInvoiceRevenueTypes.REVENUE_SHARE_EXTERNAL) {
			return getBillingAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueNetTotalAmount() {
		if (getRevenueType() == BillingInvoiceRevenueTypes.REVENUE || getRevenueType() == BillingInvoiceRevenueTypes.REVENUE_SHARE_EXTERNAL) {
			return getBillingAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueFinalTotalAmount() {
		if (getRevenueType().isRevenue()) {
			return getBillingAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getOtherTotalAmount() {
		if (!getRevenueType().isRevenue()) {
			return getBillingAmount();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceDetail getInvoiceDetail() {
		return this.invoiceDetail;
	}


	public void setInvoiceDetail(BillingInvoiceDetail invoiceDetail) {
		this.invoiceDetail = invoiceDetail;
	}


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount() {
		return this.billingDefinitionInvestmentAccount;
	}


	public void setBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		this.billingDefinitionInvestmentAccount = billingDefinitionInvestmentAccount;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public BigDecimal getBillingBasis() {
		return this.billingBasis;
	}


	public void setBillingBasis(BigDecimal billingBasis) {
		this.billingBasis = billingBasis;
	}


	public BigDecimal getAllocationPercentage() {
		return this.allocationPercentage;
	}


	public void setAllocationPercentage(BigDecimal allocationPercentage) {
		this.allocationPercentage = allocationPercentage;
	}


	public BigDecimal getBillingAmount() {
		return this.billingAmount;
	}


	public void setBillingAmount(BigDecimal billingAmount) {
		this.billingAmount = billingAmount;
	}
}
