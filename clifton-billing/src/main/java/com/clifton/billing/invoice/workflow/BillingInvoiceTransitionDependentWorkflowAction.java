package com.clifton.billing.invoice.workflow;

import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>BillingInvoiceTransitionDependentWorkflowAction</code> is used by workflow actions to transition any dependent invoices
 * based on the current invoices transition.
 * <p>
 * This can be used by either an Invoice Group or Individual Invoice - The reason why we check at the group level is possible because more than one invoice in the group
 * can be a pre-requisite for the same invoice, but we don't want to transition them twice
 * <p>
 * <p>
 * For example:
 * When a prerequisite invoice is approved, the dependent invoices are re-processed automatically to ensure it has the latest information
 * When a prerequisite invoice is voided and re-generated, the dependent invoices are also voided and re-generated
 *
 * @author manderson
 */
public class BillingInvoiceTransitionDependentWorkflowAction<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {

	private BillingDefinitionService billingDefinitionService;

	private BillingInvoiceService billingInvoiceService;

	private WorkflowTransitionService workflowTransitionService;

	/**
	 * The workflow transition name the invoice should execute.
	 * Note: The transition names are the same for invoice groups and invoices
	 */
	private String workflowTransitionName;

	/**
	 * The workflow transition the invoice should execute if there is no transition for workflowTransitionName
	 * This is useful for cases where the prerequisite invoice is voided, but the dependent invoice isn't in a state yet that can be voided, so we'll need to Reject it instead
	 */
	private String backupWorkflowTransitionName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		if (bean instanceof BillingInvoice) {
			processActionForInvoiceList(CollectionUtils.createList((BillingInvoice) bean));
		}
		else if (bean instanceof BillingInvoiceGroup) {
			processActionForInvoiceList(getBillingInvoiceService().getBillingInvoiceGroup(((BillingInvoiceGroup) bean).getId()).getInvoiceList());
		}
		else {
			throw new IllegalStateException("BillingInvoiceTransitionDependentWorkflowAction can be used by BillingInvoice or BillingInvoiceGroup objects only");
		}
		return bean;
	}


	private void processActionForInvoiceList(List<BillingInvoice> invoiceList) {
		// Even if there are multiple - then they would be part of a group and have the same invoice period
		Date invoicePeriodStartDate = invoiceList.get(0).getInvoicePeriodStartDate();
		Date invoicePeriodEndDate = invoiceList.get(0).getInvoicePeriodEndDate();

		// Determine if there are any dependents
		Set<Integer> billingDefinitionIds = new HashSet<>();
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			List<BillingScheduleBillingDefinitionDependency> dependencyList = getBillingDefinitionService().getBillingScheduleBillingDefinitionDependencyListByDefinition(invoice.getBillingDefinition().getId(), true);
			if (!CollectionUtils.isEmpty(dependencyList)) {
				for (BillingScheduleBillingDefinitionDependency dependency : dependencyList) {
					if (!billingDefinitionIds.contains(dependency.getBillingSchedule().getBillingDefinition().getId()) && DateUtils.isOverlapInDates(dependency.getBillingSchedule().getStartDate(), dependency.getBillingSchedule().getEndDate(), invoicePeriodStartDate, invoicePeriodEndDate)) {
						billingDefinitionIds.add(dependency.getBillingSchedule().getBillingDefinition().getId());
					}
				}
			}
		}

		// If there are - get their invoices if they exist
		if (!CollectionUtils.isEmpty(billingDefinitionIds)) {
			BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
			searchForm.setBillingDefinitionIds(billingDefinitionIds.toArray(new Integer[0]));
			searchForm.setInvoiceStartDate(invoicePeriodStartDate);
			searchForm.setInvoiceEndDate(invoicePeriodEndDate);
			searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CANCELED); // Always Exclude Voided Invoices
			List<BillingInvoice> dependentInvoiceList = getBillingInvoiceService().getBillingInvoiceList(searchForm);
			Set<Integer> invoiceGroupsProcessed = new HashSet<>();
			for (BillingInvoice invoice : CollectionUtils.getIterable(dependentInvoiceList)) {
				if (invoice.getInvoiceGroup() != null) {
					if (!invoiceGroupsProcessed.contains(invoice.getInvoiceGroup().getId())) {
						invoiceGroupsProcessed.add(invoice.getInvoiceGroup().getId());
						transitionToWorkflowState(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, invoice.getInvoiceGroup().getId());
					}
				}
				else {
					transitionToWorkflowState(BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId());
				}
			}
		}
	}


	private void transitionToWorkflowState(String tableName, int entityId) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(tableName, MathUtils.getNumberAsLong(entityId));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
			if (StringUtils.isEqualIgnoreCase(getWorkflowTransitionName(), transition.getName())) {
				getWorkflowTransitionService().executeWorkflowTransitionByTransition(tableName, entityId, transition.getId());
				return;
			}
		}

		// If not found and back up exists - look for that one and execute it
		if (!StringUtils.isEmpty(getBackupWorkflowTransitionName())) {
			for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
				if (StringUtils.isEqualIgnoreCase(getBackupWorkflowTransitionName(), transition.getName())) {
					getWorkflowTransitionService().executeWorkflowTransitionByTransition(tableName, entityId, transition.getId());
					return;
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public String getWorkflowTransitionName() {
		return this.workflowTransitionName;
	}


	public void setWorkflowTransitionName(String workflowTransitionName) {
		this.workflowTransitionName = workflowTransitionName;
	}


	public String getBackupWorkflowTransitionName() {
		return this.backupWorkflowTransitionName;
	}


	public void setBackupWorkflowTransitionName(String backupWorkflowTransitionName) {
		this.backupWorkflowTransitionName = backupWorkflowTransitionName;
	}
}
