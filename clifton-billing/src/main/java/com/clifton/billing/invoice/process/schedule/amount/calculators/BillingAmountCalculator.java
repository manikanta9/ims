package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleType;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;


/**
 * The <code>BillingAmountCalculator</code> interface calculates the actual billing amount to be applied for a schedule on the invoice(s) based on the pre-calculated billing basis.
 * Each {@link BillingScheduleType} schedule type is associated with a specific BillingAmountCalculator bean implementation. Some properties on the schedule type are required to match those of the calculator bean implementations.
 * i.e. Annual Minimum fee schedule type can only be associated with a calculator bean that can calculate an annual minimum - these combinations of what is allowed is defined by the {@link BillingAmountCalculatorTypes}
 *
 * @author vgomelsky
 */
public interface BillingAmountCalculator {

	public BillingAmountCalculatorTypes getBillingAmountCalculatorType();


	/**
	 * Ensures that the schedule type setup matches the calculator support
	 * i.e. Tiered calculator can only be used for Tiered schedule types, etc.
	 */
	public void validateBillingScheduleType(BillingScheduleType scheduleType);


	/**
	 * Used during schedule processing as well as when generating billing basis details to determine for each {@link com.clifton.billing.definition.BillingDefinitionInvestmentAccount} what accrual frequency is used
	 */
	public boolean isBillingBasisUsed(BillingSchedule billingSchedule);


	/**
	 * Calculates Billing Amounts and adds details to affected invoices and applies to affected account level detail for each invoice detail
	 * created
	 */
	public void calculate(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig);
}
