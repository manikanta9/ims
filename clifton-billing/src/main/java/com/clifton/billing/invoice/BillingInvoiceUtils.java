package com.clifton.billing.invoice;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.List;


/**
 * @author manderson
 */
public class BillingInvoiceUtils {


	/**
	 * Recalculates the invoice totals for each bucket based on the amounts in the detail list
	 */
	public static void recalculateTotalAmountsForBillingInvoice(BillingInvoice invoice, List<BillingInvoiceDetail> detailList) {
		ValidationUtils.assertNotNull(invoice, "Invoice is required");
		boolean revenue = invoice.getInvoiceType().isRevenue();

		if (revenue) {
			invoice.setTotalAmount(CoreMathUtils.sumProperty(detailList, BillingInvoiceDetail::getRevenueGrossTotalAmount));
			if (invoice.getBillingDefinition() != null && invoice.getBillingDefinition().getRevenueShareExternalCompany() != null) {
				invoice.setRevenueShareExternalTotalAmount(CoreMathUtils.sumProperty(detailList, BillingInvoiceDetail::getRevenueShareExternalTotalAmount));
			}
			else {
				invoice.setRevenueShareExternalTotalAmount(null);
			}
			if (invoice.getBillingDefinition() != null && invoice.getBillingDefinition().getRevenueShareInternalCompany() != null) {
				invoice.setRevenueShareInternalTotalAmount(CoreMathUtils.sumProperty(detailList, BillingInvoiceDetail::getRevenueShareInternalTotalAmount));
			}
			else {
				invoice.setRevenueShareInternalTotalAmount(null);
			}
		}
		else {
			invoice.setTotalAmount(CoreMathUtils.sumProperty(detailList, BillingInvoiceDetail::getOtherTotalAmount));
			invoice.setRevenueShareExternalTotalAmount(null);
			invoice.setRevenueShareInternalTotalAmount(null);
		}
	}
}
