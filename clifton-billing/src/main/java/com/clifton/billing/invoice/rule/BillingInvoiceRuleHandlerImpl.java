package com.clifton.billing.invoice.rule;

import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
public class BillingInvoiceRuleHandlerImpl implements BillingInvoiceRuleHandler {


	private DaoLocator daoLocator;

	private RuleEvaluatorService ruleEvaluatorService;
	private RuleViolationService ruleViolationService;

	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	///////        Billing Invoice System Warning Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateBillingInvoiceRules(BillingInvoice invoice) {
		// Run Rule Evaluators
		getRuleEvaluatorService().executeRules(RULE_CATEGORY_BILLING_INVOICE_MANAGEMENT_FEE, BeanUtils.getIdentityAsLong(invoice), null);
	}


	@Override
	public RuleViolation createSystemDefinedRuleViolation(Integer linkedFkFieldId, Integer causeFkFieldId, String violationNote, String ruleDefinitionName) {
		return getRuleViolationService().createRuleViolationSystemDefinedWithCause(violationNote, ruleDefinitionName, linkedFkFieldId, causeFkFieldId, null);
	}


	@Override
	public void saveBillingInvoiceRuleViolationListSystemDefined(BillingInvoice invoice, Map<String, List<RuleViolation>> ruleViolationMap) {
		for (Map.Entry<String, List<RuleViolation>> stringListEntry : ruleViolationMap.entrySet()) {
			getRuleViolationService().updateRuleViolationListForEntityAndDefinition(stringListEntry.getValue(), BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), stringListEntry.getKey());
		}
	}


	@Override
	public void saveBillingInvoiceRuleViolationListSystemDefined(BillingInvoice invoice, String ruleDefinitionName, Map<Integer, List<String>> processedEntities) {
		List<RuleViolation> newList = new ArrayList<>();
		// Go through map of processed entities and add warnings where msg exists
		if (processedEntities != null) {
			for (Map.Entry<Integer, List<String>> integerStringEntry : processedEntities.entrySet()) {
				String violations = CollectionUtils.getStream(integerStringEntry.getValue())
						.filter(violationNote -> !StringUtils.isEmpty(violationNote))
						.collect(Collectors.joining(StringUtils.NEW_LINE));
				if (!StringUtils.isEmpty(violations)) {
					newList.add(getRuleViolationService().createRuleViolationSystemDefinedWithCause(violations, ruleDefinitionName, invoice.getId(), integerStringEntry.getKey(), null));
				}
			}
		}
		getRuleViolationService().updateRuleViolationListForEntityAndDefinition(newList, BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), ruleDefinitionName);
	}


	/**
	 * Specific method for the {@see BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_MISSING_SOURCE_DATA} violation so we can pass the cause entity and list of dates
	 */
	@Override
	public void saveBillingInvoiceRuleViolationListForBillingBasisMissingSourceData(BillingInvoice invoice, Map<IdentityObject, Set<Date>> missingSourceDataMap) {
		List<RuleViolation> newList = new ArrayList<>();
		// Go through map of missing source data and add violation where dates are populated
		if (missingSourceDataMap != null) {
			for (Map.Entry<IdentityObject, Set<Date>> causeEntityEntry : missingSourceDataMap.entrySet()) {
				if (!CollectionUtils.isEmpty(causeEntityEntry.getValue())) {
					ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(causeEntityEntry.getKey());
					SystemTable causeTable = getSystemSchemaService().getSystemTableByName(dao.getConfiguration().getTableName());

					Map<String, Object> templateContextMap = new HashMap<>();
					templateContextMap.put("dateList", CollectionUtils.createSorted(causeEntityEntry.getValue()));
					newList.add(getRuleViolationService().createRuleViolationSystemDefinedWithCauseUsingTemplate(BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_MISSING_SOURCE_DATA, invoice.getId(), causeEntityEntry.getKey().getIdentity(), causeTable, templateContextMap));
				}
			}
		}
		getRuleViolationService().updateRuleViolationListForEntityAndDefinition(newList, BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_MISSING_SOURCE_DATA);
	}


	/**
	 * Specific method for the {@see BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_SNAPSHOT_SECURITY_EXCLUDED} violation so we can pass the cause entity and list of dates
	 */
	@Override
	public void saveBillingInvoiceRuleViolationListForBillingBasisSnapshotSecurityExcluded(BillingInvoice invoice, Map<BillingDefinitionInvestmentAccountSecurityExclusion, Map<String, Set<Date>>> securityExclusionSecurityDateListMap) {
		List<RuleViolation> newList = new ArrayList<>();
		// Go through map of missing source data and add violation where dates are populated
		if (!CollectionUtils.isEmpty(securityExclusionSecurityDateListMap)) {
			for (Map.Entry<BillingDefinitionInvestmentAccountSecurityExclusion, Map<String, Set<Date>>> securityExclusionSecurityListMapEntry : securityExclusionSecurityDateListMap.entrySet()) {
				if (securityExclusionSecurityListMapEntry.getKey().getBillingDefinitionInvestmentAccount().getReferenceOne().equals(invoice.getBillingDefinition())) {
					List<String> messageList = new ArrayList<>();
					for (Map.Entry<String, Set<Date>> securityExcludeMessageDateListMapEntry : securityExclusionSecurityListMapEntry.getValue().entrySet()) {
						String dateListString = DateUtils.toDateRangesString(CollectionUtils.createSorted(securityExcludeMessageDateListMapEntry.getValue()), true, DateUtils::fromDateShort);
						messageList.add(securityExcludeMessageDateListMapEntry.getKey() + ": " + dateListString);
					}
					Map<String, Object> templateContextMap = new HashMap<>();
					templateContextMap.put("messageList", messageList);
					newList.add(getRuleViolationService().createRuleViolationSystemDefinedWithCauseUsingTemplate(BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_SNAPSHOT_SECURITY_EXCLUDED, invoice.getId(), securityExclusionSecurityListMapEntry.getKey().getIdentity(), null, templateContextMap));
				}
			}
		}
		getRuleViolationService().updateRuleViolationListForEntityAndDefinition(newList, BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_SNAPSHOT_SECURITY_EXCLUDED);
	}

	///////////////////////////////////////////////////////////////////////
	////////////          Getter & Setter Methods             /////////////
	///////////////////////////////////////////////////////////////////////


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
