package com.clifton.billing.invoice.report;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.billing.invoice.workflow.BillingInvoicePortalUpdateWorkflowAction;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.document.setup.search.DocumentFileSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.PortalFileDuplicateException;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.report.definition.Report;
import com.clifton.report.definition.ReportDefinitionService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class BillingInvoiceReportServiceImpl implements BillingInvoiceReportService {

	private BillingInvoiceService billingInvoiceService;

	private BusinessClientService businessClientService;

	private DocumentManagementService documentManagementService;
	private DocumentSetupService documentSetupService;

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;

	private ReportDefinitionService reportDefinitionService;
	private ReportExportService reportExportService;
	private ReportPostingController<BillingInvoice> reportPostingController;

	private SystemBeanService systemBeanService;

	private SystemQueryExecutionService systemQueryExecutionService;
	private SystemQueryService systemQueryService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private TemplateConverter templateConverter;


	////////////////////////////////////////////////////////////////////////////////

	// Manual Invoice Reports
	public static final String NO_BILLING_DEFINITION_REPORT_NAME = "Standard Billing Invoice - No Supporting Pages";
	public static final String NO_BILLING_DEFINITION_REPORT_TEMPLATE = "Billing Report";

	private static final String PORTAL_SOURCE_SECTION_BILLING_INVOICE = "Management Fee Invoice";

	private static final String PORTAL_FILE_GROUP_BILLING_INVOICE = "Management Fee Invoice";
	private static final String PORTAL_FILE_GROUP_BILLING_INVOICE_SUPPLEMENTAL_FILES = "Supplemental Invoice Documentation";

	private static final String DOCUMENT_DEFINITION_BILLING_INVOICE_CLIENT_ATTACHMENTS = "BillingInvoiceClientAttachments";

	// When manually posting, we need to also manually update the invoice status on the portal, so we get the template to use from the workflow action
	private static final String PORTAL_INVOICE_UPDATE_WORKFLOW_ACTION_BEAN_NAME = "Billing Invoice Portal Update Workflow Action";


	////////////////////////////////////////////////////////////////////////////////
	/////////         Billing Invoice Report Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadBillingInvoiceReport(int invoiceId, Boolean includePayments, Boolean regenerateCache) {
		// Get the invoice
		BillingInvoice invoice = getBillingInvoiceService().getBillingInvoice(invoiceId);
		// Get the report
		Integer reportId = getBillingInvoiceReport(invoice);

		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("InvoiceID", invoice.getId());
		if (includePayments != null && includePayments) {
			parameters.put("IncludePreviousPayments", true);
		}
		String reportFileName = invoice.getBusinessCompany().getName() + " Invoice " + invoiceId;
		ReportExportConfiguration exportConfiguration = new ReportExportConfiguration(reportId, parameters, false, regenerateCache != null && regenerateCache);
		exportConfiguration.setReportFileName(reportFileName);
		return getReportExportService().getReportPDFFile(exportConfiguration);
	}


	@Override
	public void clearBillingInvoiceReportCache(int invoiceId) {
		// Get the invoice
		BillingInvoice invoice = getBillingInvoiceService().getBillingInvoice(invoiceId);
		// Get the report
		Integer reportId = getBillingInvoiceReport(invoice);

		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("InvoiceID", invoice.getId());
		getReportExportService().clearReportPDFFileCache(reportId, parameters);

		parameters.put("IncludePreviousPayments", true);
		getReportExportService().clearReportPDFFileCache(reportId, parameters);
	}


	private Integer getBillingInvoiceReport(BillingInvoice invoice) {
		Integer reportId;
		if (invoice.getBillingDefinition() != null) {
			// Get the report
			reportId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(invoice.getBillingDefinition(), BillingDefinition.BILLING_DEFINITION_CUSTOM_FIELD_GROUP, BillingDefinition.FIELD_BILLING_INVOICE_REPORT, true);
			ValidationUtils.assertNotNull(reportId, "No billing invoice report is defined for the billing definition.");
		}
		else {
			Report report = getReportDefinitionService().getReportByNameAndTemplate(NO_BILLING_DEFINITION_REPORT_NAME, NO_BILLING_DEFINITION_REPORT_TEMPLATE);
			reportId = report.getId();
		}
		ValidationUtils.assertNotNull(reportId, "Unable to determine report for Invoice [" + invoice.getLabel() + "].");
		return reportId;
	}


	////////////////////////////////////////////////////////////////////////////
	//// Billing Basis Snapshot Details - Account Export Business Methods   ////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable getBillingInvoiceAccountBillingBasisSystemQueryResult(String queryName, Date invoiceDate, int billingDefinitionInvestmentAccountId) {
		SystemQuery query = getSystemQueryService().getSystemQueryByName(queryName);
		ValidationUtils.assertNotNull(query, "Cannot find query with name " + queryName);
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		ValidationUtils.assertTrue(CollectionUtils.getSize(parameterList) == 2, "System Query " + queryName + " should have two parameters invoice date and billing account id.");
		List<SystemQueryParameterValue> valueList = new ArrayList<>();
		for (SystemQueryParameter param : parameterList) {
			SystemQueryParameterValue pv = new SystemQueryParameterValue();
			pv.setParameter(param);

			if (StringUtils.isEqual("Invoice Date", param.getName())) {
				pv.setValue(DateUtils.fromDateShort(invoiceDate));
			}
			else {
				pv.setValue(Integer.toString(billingDefinitionInvestmentAccountId));
			}
			valueList.add(pv);
		}
		query.setParameterValueList(valueList);
		return getSystemQueryExecutionService().getSystemQueryResult(query);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////               Billing Invoice Posting Methods                /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice postBillingInvoice(int invoiceId, boolean skipIfDefinitionPostOptionOff, boolean updatePortalEntityStatus) {
		BillingInvoice invoice = getBillingInvoiceService().getBillingInvoice(invoiceId);

		if (isBillingInvoiceAvailableToPost(invoice, !skipIfDefinitionPostOptionOff)) {
			createBillingInvoicePortalEntityIfMissing(invoice);

			FileWrapper report = downloadBillingInvoiceReport(invoice.getId(), true, true);

			ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
			postingConfig.setPortalFileCategoryName(PORTAL_FILE_GROUP_BILLING_INVOICE);
			populateBillingInvoicePostEntity(invoice, postingConfig);
			postingConfig.setSourceEntitySectionName(PORTAL_SOURCE_SECTION_BILLING_INVOICE);
			postingConfig.setSourceEntityFkFieldId(invoice.getId());
			postingConfig.setReportDate(invoice.getInvoiceDate());
			if (BillingFrequencies.MONTHLY == invoice.getBillingDefinition().getBillingFrequency()) {
				postingConfig.setPortalFileFrequency(PortalFileFrequencies.MONTHLY.name());
			}
			else {
				postingConfig.setPortalFileFrequency(PortalFileFrequencies.QUARTERLY.name());
			}
			postingConfig.setFileWrapper(report);
			getReportPostingController().postClientFile(postingConfig, invoice);

			// Get all Client File Attachments and Post them
			DocumentFileSearchForm searchForm = new DocumentFileSearchForm();
			searchForm.setDefinitionNameEquals(DOCUMENT_DEFINITION_BILLING_INVOICE_CLIENT_ATTACHMENTS);
			searchForm.setActive(true);
			searchForm.setFkFieldId(MathUtils.getNumberAsLong(invoiceId));
			List<DocumentFile> documentFileList = getDocumentSetupService().getDocumentFileList(searchForm);
			for (DocumentFile documentFile : CollectionUtils.getIterable(documentFileList)) {
				postBillingInvoiceAttachmentImpl(invoice, documentFile, null);
			}

			// If we need to update the status during manual posting, then we need to get the update workflow action bean to get the status freemarker template in order to populate the status
			// Update the status after the posting is done in case there was an error we don't show something as under revision if the revision didn't post
			if (updatePortalEntityStatus) {
				SystemBean portalUpdateBean = getSystemBeanService().getSystemBeanByName(PORTAL_INVOICE_UPDATE_WORKFLOW_ACTION_BEAN_NAME);
				if (portalUpdateBean == null) {
					throw new ValidationException("Could Not find a System Bean with name [" + PORTAL_INVOICE_UPDATE_WORKFLOW_ACTION_BEAN_NAME + "] which is needed in order to populate the status on the portal");
				}
				String statusTemplate = ((BillingInvoicePortalUpdateWorkflowAction) getSystemBeanService().getBeanInstance(portalUpdateBean)).getPortalStatusTemplate();
				updateBillingInvoicePortalEntityStatus(invoice, statusTemplate);
			}
		}
		return invoice;
	}


	@Override
	public void postBillingInvoiceAttachmentReplace(int invoiceId, int documentFileId) {
		postBillingInvoiceAttachment(invoiceId, documentFileId, FileDuplicateActions.REPLACE);
	}


	@Override
	public void postBillingInvoiceAttachment(int invoiceId, int documentFileId, FileDuplicateActions fileDuplicateAction) {
		try {
			BillingInvoice invoice = getBillingInvoiceService().getBillingInvoice(invoiceId);
			if (isBillingInvoiceAvailableToPost(invoice, true)) {
				DocumentFile documentFile = getDocumentSetupService().getDocumentFile(documentFileId);
				postBillingInvoiceAttachmentImpl(invoice, documentFile, fileDuplicateAction);
			}
		}
		catch (Throwable e) {
			Throwable original = ExceptionUtils.getOriginalException(e);
			if (original instanceof PortalFileDuplicateException) {
				if (((PortalFileDuplicateException) original).isReplaceAllowed()) {
					throw new UserIgnorableValidationException(getClass(), "postBillingInvoiceAttachmentReplace", original.getMessage() + "<br><br>Note: Clicking Yes will post the file using the Replace option.");
				}
			}
			throw e;
		}
	}


	protected void postBillingInvoiceAttachmentImpl(BillingInvoice invoice, DocumentFile documentFile, FileDuplicateActions fileDuplicateAction) {
		ValidationUtils.assertTrue(MathUtils.isEqual(documentFile.getFkFieldId(), invoice.getId()) && StringUtils.isEqual(documentFile.getDefinition().getTable().getName(), BillingInvoice.BILLING_INVOICE_TABLE_NAME), "Selected Document File does not belong to Invoice [" + invoice.getLabelShort() + "]");
		ValidationUtils.assertEquals(DOCUMENT_DEFINITION_BILLING_INVOICE_CLIENT_ATTACHMENTS, documentFile.getDefinition().getName(), "Selected document file does not belong to document definition [" + DOCUMENT_DEFINITION_BILLING_INVOICE_CLIENT_ATTACHMENTS + "].  Only client file attachments can be posted.");

		DocumentRecord record = getDocumentManagementService().getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, documentFile.getId());
		// Might be missing file in doc system.  This would be likely to happen if not running in a Production environment
		if (record != null && !StringUtils.isEmpty(record.getDocumentId())) {
			ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
			postingConfig.setFileDuplicateAction(fileDuplicateAction);
			postingConfig.setPortalFileCategoryName(PORTAL_FILE_GROUP_BILLING_INVOICE_SUPPLEMENTAL_FILES);
			populateBillingInvoicePostEntity(invoice, postingConfig);
			postingConfig.setSourceEntitySectionName(PORTAL_SOURCE_SECTION_BILLING_INVOICE);
			postingConfig.setSourceEntityFkFieldId(invoice.getId());
			postingConfig.setReportDate(invoice.getInvoiceDate());
			if (BillingFrequencies.MONTHLY == invoice.getBillingDefinition().getBillingFrequency()) {
				postingConfig.setPortalFileFrequency(PortalFileFrequencies.MONTHLY.name());
			}
			else {
				postingConfig.setPortalFileFrequency(PortalFileFrequencies.QUARTERLY.name());
			}
			FileWrapper file = getDocumentManagementService().downloadDocumentRecord(record.getDocumentId());
			postingConfig.setDisplayName("Invoice #: " + invoice.getId() + " " + documentFile.getName());
			postingConfig.setFileWrapper(file);
			getReportPostingController().postClientFile(postingConfig, invoice);
		}
		else {
			throw new ValidationException("Cannot find file for document file id " + documentFile.getId());
		}
	}


	private void populateBillingInvoicePostEntity(BillingInvoice invoice, ReportPostingFileConfiguration postingFileConfiguration) {
		ValidationUtils.assertNotNull(invoice.getBusinessCompany(), "Invoice is missing a company defined.");
		if (StringUtils.isEqual(BusinessClientRelationship.COMPANY_TYPE_NAME, invoice.getBusinessCompany().getType().getName())) {
			BusinessClientRelationship clientRelationship = getBusinessClientService().getBusinessClientRelationshipByCompany(invoice.getBusinessCompany().getId());
			ValidationUtils.assertNotNull(clientRelationship, "Cannot find Client Relationship for Company ID " + invoice.getBusinessCompany().getId());
			postingFileConfiguration.setPostEntitySourceTableName(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME);
			postingFileConfiguration.setPostEntitySourceFkFieldId(clientRelationship.getId());
		}
		// Otherwise we expect it to be a client
		else {
			BusinessClient client = getBusinessClientService().getBusinessClientByCompany(invoice.getBusinessCompany().getId());
			ValidationUtils.assertNotNull(client, "Cannot find Client for Company ID " + invoice.getBusinessCompany().getId());
			postingFileConfiguration.setPostEntitySourceTableName(BusinessClient.BUSINESS_CLIENT_TABLE_NAME);
			postingFileConfiguration.setPostEntitySourceFkFieldId(client.getId());
		}
	}


	protected boolean isBillingInvoiceAvailableToPost(BillingInvoice invoice, boolean throwExceptionIfNotPosting) {
		boolean availableToPost = invoice.getBillingDefinition().isInvoicePostedToPortal();
		if (availableToPost) {
			if (StringUtils.isEqual(WorkflowStatus.STATUS_ACTIVE, invoice.getWorkflowStatus().getName()) || StringUtils.isEqual(WorkflowStatus.STATUS_CLOSED, invoice.getWorkflowStatus().getName())) {
				return availableToPost;
			}
			availableToPost = false;
			if (throwExceptionIfNotPosting) {
				throw new ValidationException("You cannot post invoice [" + invoice.getLabelShort() + "] because it is not in Active or Closed Status.");
			}
		}
		else if (throwExceptionIfNotPosting) {
			throw new ValidationException("Billing Definition for Invoice [" + invoice.getLabel() + "] has posting option turned off.");
		}
		return availableToPost;
	}


	protected void createBillingInvoicePortalEntityIfMissing(BillingInvoice invoice) {
		PortalEntity invoiceEntity = getPortalEntityService().getPortalEntityBySourceTable("IMS", BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), true);
		if (invoiceEntity == null) {
			invoiceEntity = new PortalEntity();
			invoiceEntity.setEntitySourceSection(getPortalEntitySetupService().getPortalEntitySourceSectionByName("IMS", PORTAL_SOURCE_SECTION_BILLING_INVOICE));
			invoiceEntity.setSourceFkFieldId(invoice.getId());
			populateBillingInvoicePortalEntityProperties(invoice, invoiceEntity);
			getPortalEntityService().savePortalEntity(invoiceEntity);
		}
	}


	private void populateBillingInvoicePortalEntityProperties(BillingInvoice invoice, PortalEntity invoicePortalEntity) {
		invoicePortalEntity.setName(invoice.getLabelShort());
		invoicePortalEntity.setLabel(invoice.getInvoiceNumberAndAmountLabel());
		invoicePortalEntity.setDescription(null);
		invoicePortalEntity.setStartDate(invoice.getInvoicePeriodStartDate());
		invoicePortalEntity.setEndDate(invoice.getInvoicePeriodEndDate());
	}


	@Override
	public void updateBillingInvoicePortalEntityStatus(BillingInvoice invoice, String portalStatusTemplate) {
		PortalEntity invoicePortalEntity = getPortalEntityService().getPortalEntityBySourceTable("IMS", BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId(), true);
		if (invoicePortalEntity != null) {
			// Update Name, Label, Description, etc.
			populateBillingInvoicePortalEntityProperties(invoice, invoicePortalEntity);
			saveBillingInvoicePortalEntityStatusImpl(invoice, invoicePortalEntity, portalStatusTemplate);
		}

		if (invoice.getParent() != null) {
			updateBillingInvoicePortalEntityStatus(invoice.getParent(), portalStatusTemplate);
		}
	}


	protected void saveBillingInvoicePortalEntityStatusImpl(BillingInvoice invoice, PortalEntity invoicePortalEntity, String portalStatusTemplate) {
		String statusName = getBillingInvoicePortalStatusName(invoice, portalStatusTemplate);

		if (!StringUtils.isEmpty(statusName)) {
			PortalEntityFieldValue statusFieldValue = CollectionUtils.getFirstElement(invoicePortalEntity.getFieldValueList());
			// If there is no change to the status - return and don't perform unnecessary update
			if (statusFieldValue != null && StringUtils.isEqual(statusFieldValue.getValue(), statusName)) {
				return;
			}
			if (statusFieldValue == null) {
				statusFieldValue = new PortalEntityFieldValue();
			}
			statusFieldValue.setPortalEntity(invoicePortalEntity);
			statusFieldValue.setPortalEntityFieldType(getPortalEntityStatusFieldType(invoicePortalEntity.getEntitySourceSection().getEntityType()));
			statusFieldValue.setValue(statusName);
			invoicePortalEntity.setFieldValueList(CollectionUtils.createList(statusFieldValue));
		}
		else {
			invoicePortalEntity.setFieldValueList(null);
		}
		getPortalEntityService().savePortalEntity(invoicePortalEntity);
	}


	protected PortalEntityFieldType getPortalEntityStatusFieldType(PortalEntityType entityType) {
		return CollectionUtils.getFirstElementStrict(BeanUtils.filter(getPortalEntitySetupService().getPortalEntityFieldTypeListByEntityType(entityType.getId()), PortalEntityFieldType::getName, PortalEntityFieldType.FIELD_TYPE_STATUS));
	}


	protected String getBillingInvoicePortalStatusName(BillingInvoice invoice, String portalStatusTemplate) {
		// IF nothing, always return null?  Better to show no status than one clients shouldn't see?
		if (StringUtils.isEmpty(portalStatusTemplate)) {
			return null;
		}

		BillingInvoice revisedInvoice = null;
		if (invoice.isCanceled()) {
			BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
			searchForm.setParentId(invoice.getId());
			revisedInvoice = CollectionUtils.getFirstElement(getBillingInvoiceService().getBillingInvoiceList(searchForm));
		}

		TemplateConfig templateConfig = new TemplateConfig(portalStatusTemplate);
		templateConfig.addBeanToContext("invoice", invoice);
		if (revisedInvoice != null) {
			templateConfig.addBeanToContext("revisedInvoice", revisedInvoice);
		}

		return getTemplateConverter().convert(templateConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}


	public ReportDefinitionService getReportDefinitionService() {
		return this.reportDefinitionService;
	}


	public void setReportDefinitionService(ReportDefinitionService reportDefinitionService) {
		this.reportDefinitionService = reportDefinitionService;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}


	public ReportPostingController<BillingInvoice> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<BillingInvoice> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
