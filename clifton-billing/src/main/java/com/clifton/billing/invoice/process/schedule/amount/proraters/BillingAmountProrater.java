package com.clifton.billing.invoice.process.schedule.amount.proraters;


import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;

import java.math.BigDecimal;


/**
 * The BillingAmountProrater takes the calculated schedule amount and can prorate based on various options defined by each implementation.
 * i.e. Number of days with positions on, dates the account is billing
 * <p>
 * Prorating most commonly comes into play during the period when an account is starting or ending, however in some cases and account may go on Pause or take positions off for a period of time. In those cases we may not charge them the retainer during the times they don't have positions on.
 */
public interface BillingAmountProrater {

	/**
	 * Prorates the calculated amount based on implementation, i.e. billing dates, positions on dates, billing basis dates, etc.
	 */
	public BigDecimal prorateCalculatedAmount(BigDecimal amount, BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail, StringBuilder note);
}
