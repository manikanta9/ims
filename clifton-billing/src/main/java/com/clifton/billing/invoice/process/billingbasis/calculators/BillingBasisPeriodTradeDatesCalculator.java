package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisPeriodTradeDatesCalculator</code> calculates the billing basis for each sub-period based on the dates the accounts traded in.
 *
 * @author Mary Anderson
 */
public class BillingBasisPeriodTradeDatesCalculator extends BaseBillingBasisCalculator {

	@Override
	public boolean isProjectedRevenueSupported() {
		return true;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		Date startDate = getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart);
		Date endDate = getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd);

		// Get all values for the date range
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		List<BillingBasisSnapshot> list = calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), startDate, endDate);

		// Filter List By Date Specifics
		List<BillingBasisSnapshot> filteredList = new ArrayList<>();
		List<Date> dateList = getBillingBasisDatesFromTradeDates(billingDefinitionInvestmentAccount.getReferenceOne().getId(), defaultStart, defaultEnd);
		if (isProjectedRevenue() && !dateList.contains(endDate)) {
			dateList.add(endDate);
		}
		// If missing a value for Date Specific Ranges, then we need to add a 0 Record for calculations to properly split across all accounts
		for (Date date : CollectionUtils.getIterable(dateList)) {
			List<BillingBasisSnapshot> dateFilteredList = filterBillingBasisSnapshotListByDate(list, date);
			if (CollectionUtils.isEmpty(dateFilteredList)) {
				dateFilteredList = CollectionUtils.createList(new BillingBasisSnapshot(invoice, billingDefinitionInvestmentAccount, date));
			}
			filteredList.addAll(dateFilteredList);
		}
		list = filteredList;
		return list;
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {

		List<BillingBasisSnapshot> valueList = getBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, startDate, endDate);
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
		// Uses Datasource for the M2M account relationship issuing company, else default.
		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);

		BillingBasis billingBasis = new BillingBasis();
		billingBasis.setAlwaysDisplayDateDetails(true);
		Date previousDate = startDate;
		Date date = null;
		// Put In Date Order
		valueList = BeanUtils.sortWithFunction(valueList, BillingBasisSnapshot::getSnapshotDate, true);
		List<BillingBasisSnapshot> filteredValueList = new ArrayList<>();
		for (BillingBasisSnapshot v : CollectionUtils.getIterable(valueList)) {
			if (date == null) {
				date = v.getSnapshotDate();
			}
			else if (DateUtils.compare(date, v.getSnapshotDate(), false) == 0) {
				continue;
			}
			else {
				// Move to next day so we don't bill for two billing basis on the same day.
				previousDate = DateUtils.addDays(date, 1);
				date = v.getSnapshotDate();
			}

			List<BillingBasisSnapshot> dateFilteredList = filterBillingBasisSnapshotListByDate(valueList, date);

			// DataSourceName will only be populated if we need to lookup exchange rate
			if (!StringUtils.isEmpty(dataSourceName)) {
				BigDecimal exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName,
						billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), date).flexibleLookup());
				for (BillingBasisSnapshot sn : CollectionUtils.getIterable(dateFilteredList)) {
					// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
					// If missing only, then use system determined rate
					if (MathUtils.isNullOrZero(sn.getFxRate())) {
						sn.setFxRate(exchangeRate);
					}
				}
			}

			BigDecimal value = getBillingBasisTotalFromList(dateFilteredList);
			filteredValueList.addAll(dateFilteredList);
			// Ensure Final Billing Basis Total is Positive Value
			billingBasis.addBillingBasis(previousDate, date, MathUtils.abs(value));

			// Make previous date the current date plus one so we don't double count that day
			previousDate = DateUtils.addDays(date, 1);
		}
		getBillingBasisService().saveBillingBasisSnapshotList(filteredValueList);
		return billingBasis;
	}


	private List<Date> getBillingBasisDatesFromTradeDates(Integer billingDefinitionId, Date startDate, Date endDate) {
		// If shared - also need their trade dates
		List<Integer> definitionIdList = getBillingDefinitionService().getBillingDefinitionIdSharedList(billingDefinitionId, startDate, endDate);

		List<Date> dateList = new ArrayList<>();
		for (Integer definitionId : CollectionUtils.getIterable(definitionIdList)) {
			BillingDefinition billingDefinition = getBillingDefinitionService().getBillingDefinitionPopulated(definitionId);
			for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(billingDefinition.getInvestmentAccountList())) {
				// Only include those with the same (i.e. Date Range Specific) calculation
				if (billingAccount.getBillingBasisCalculationType().isDateRangeSpecific()) {
					BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingAccount);
					dateList = (calculator.getTradeDateList(billingAccount, startDate, endDate, dateList));
				}
			}
		}

		// Always add the last day of the period
		if (!dateList.contains(endDate)) {
			dateList.add(endDate);
		}
		return dateList;
	}
}
