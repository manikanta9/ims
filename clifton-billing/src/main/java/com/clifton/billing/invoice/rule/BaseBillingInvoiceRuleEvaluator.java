package com.clifton.billing.invoice.rule;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseBillingInvoiceRuleEvaluator</code> is a base class that have generic methods for running Invoice rule violations
 *
 * @author manderson
 */
public abstract class BaseBillingInvoiceRuleEvaluator extends BaseRuleEvaluator<BillingInvoice, BillingInvoiceRuleEvaluatorContext> {


	/**
	 * In some cases we will not run a violation if the invoice total is 0
	 */
	private boolean skipForZeroInvoice;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final List<RuleViolation> evaluateRule(BillingInvoice invoice, RuleConfig ruleConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (!isSkipForZeroInvoice() || !MathUtils.isNullOrZero(invoice.getTotalAmount())) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				ruleViolationList.addAll(evaluateRuleImpl(invoice, entityConfig, context));
			}
		}
		return ruleViolationList;
	}


	public abstract List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSkipForZeroInvoice() {
		return this.skipForZeroInvoice;
	}


	public void setSkipForZeroInvoice(boolean skipForZeroInvoice) {
		this.skipForZeroInvoice = skipForZeroInvoice;
	}
}
