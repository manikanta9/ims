package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.report.BillingInvoiceReportService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>BillingInvoiceReportPostWorkflowAction</code> handles posting the report and creating the invoice source entity in the portal
 *
 * @author manderson
 */
public class BillingInvoiceReportPostWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {


	private BillingInvoiceReportService billingInvoiceReportService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice bean, WorkflowTransition transition) {
		return getBillingInvoiceReportService().postBillingInvoice(bean.getId(), true, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceReportService getBillingInvoiceReportService() {
		return this.billingInvoiceReportService;
	}


	public void setBillingInvoiceReportService(BillingInvoiceReportService billingInvoiceReportService) {
		this.billingInvoiceReportService = billingInvoiceReportService;
	}
}
