package com.clifton.billing.invoice.workflow;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.report.BillingInvoiceReportService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>BillingInvoicePortalUpdateWorkflowAction</code>  handles updating the status of the invoice on the portal during specific transitions.
 * Will only update if the invoice exists on the portal and the portal status has changed
 *
 * @author manderson
 */
public class BillingInvoicePortalUpdateWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {


	private BillingInvoiceReportService billingInvoiceReportService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Freemarker template to determine the "portal status" for an invoice
	 * i.e. Sent/Partially Paid  = "Presented for Payment"
	 * Void and pending correction = "Under Revision"
	 * Void = Void
	 * Sets "invoice" and "revisedInvoice" in the config
	 */
	private String portalStatusTemplate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice bean, WorkflowTransition transition) {
		getBillingInvoiceReportService().updateBillingInvoicePortalEntityStatus(bean, getPortalStatusTemplate());
		return bean;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceReportService getBillingInvoiceReportService() {
		return this.billingInvoiceReportService;
	}


	public void setBillingInvoiceReportService(BillingInvoiceReportService billingInvoiceReportService) {
		this.billingInvoiceReportService = billingInvoiceReportService;
	}


	public String getPortalStatusTemplate() {
		return this.portalStatusTemplate;
	}


	public void setPortalStatusTemplate(String portalStatusTemplate) {
		this.portalStatusTemplate = portalStatusTemplate;
	}
}
