package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;

import java.math.BigDecimal;


/**
 * The <code>BillingAmountForFixedFeeCalculator</code> class calculates the fixed amount to
 * apply to the invoice.  i.e. 5,000 per account, etc.
 * <p>
 *
 * @author Mary Anderson
 */
public class BillingAmountForFixedFeeCalculator extends BaseBillingAmountCalculator {

	/**
	 * If true, will apply fee proportionally using billing basis as opposed to splitting evenly across the number of accounts
	 * No change if the invoice is shared as that already allocates proportionally
	 * Currently used for Equal Sector Private Fund to apply management fee proportionally across investors
	 */
	private boolean useBillingBasis;


	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		return BillingAmountCalculatorTypes.FLAT_FEE;
	}


	@Override
	public BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, @SuppressWarnings("unused") BillingBasisDetail billingBasisDetail, StringBuilder note) {
		BigDecimal amount = scheduleConfig.getSchedule().getScheduleAmount();
		BillingAmountCalculatorUtils.addBillingInvoiceScheduleAmountNote(scheduleConfig, note, false);
		// Example: If schedule is set up monthly, but we actually bill quarterly - need to multiply amount by 3.
		// Or schedule is set up annually, but we bill quarterly - need to multiply by .25
		amount = BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, amount);
		return amount;
	}


	/**
	 * Used for shared schedules (or if explicitly selected) in order to allocate fees to invoices
	 */
	@Override
	public boolean isBillingBasisUsed(BillingSchedule schedule) {
		if (isUseBillingBasis()) {
			return true;
		}
		return schedule.isSharedSchedule() && !schedule.isDoNotGroupInvoice();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isUseBillingBasis() {
		return this.useBillingBasis;
	}


	public void setUseBillingBasis(boolean useBillingBasis) {
		this.useBillingBasis = useBillingBasis;
	}
}
