package com.clifton.billing.invoice.process.billingbasis.calculators;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisPeriodWeightedCalculator</code> is used to weight values for the period instead of averaging them.
 * <p>
 * It attempts to "group" values based on specific properties - i.e. same security, then for each date the billing basis value is the same, will exclude the second value from the snapshot, and will weight the original value for that day.
 * This is most commonly used for Security Targets.
 * For example, if a Client Targets 5000 shares of AAPL on 7/1, then changes that amount to 7500 shares on 7/17.  The values stores would be for 7/1 and 7/17, and the 7/1 value has a weight of 16/31, and the 7/17 value has a weight of 15/31
 *
 * @author manderson
 */
public class BillingBasisPeriodWeightedCalculator extends BaseBillingBasisCalculator {


	/**
	 * This is currently set to FALSE because it's only used for monthly invoices.
	 * Need a proper example to confirm logic for other frequencies
	 */
	@Override
	public boolean isProjectedRevenueSupported() {
		return false;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		Date startDate = getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart);
		Date endDate = getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd);

		// Get all values for the date range
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		return calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), startDate, endDate);
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		// Get values - need to start the look up the day before start in case we start on a weekend
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);

		// Force Rebuilding - Because we go back a day if accruing monthly the previously filtered values alter the results for the current month
		List<BillingBasisSnapshot> valueList = rebuildBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, calculator.getNextValuationDate(startDate, false), endDate);
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
		// Uses Data Source for the M2M account relationship issuing company, else default.
		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);

		// Split Billing Basis Values into Specific Lists (i.e. By Security or Manager or Asset Class based on what we have)
		Map<String, List<BillingBasisSnapshot>> snapshotListMap = BeanUtils.getBeansMap(valueList, this::getBillingBasisSnapshotGroupingKey);

		List<BigDecimal> weightedValueList = new ArrayList<>();
		List<BillingBasisSnapshot> filteredValueList = new ArrayList<>();

		for (Map.Entry<String, List<BillingBasisSnapshot>> snapshotListEntry : snapshotListMap.entrySet()) {
			Date date = startDate;
			BigDecimal previousValue = null;
			List<BillingBasisSnapshot> previousValueList = null;
			Date previousValueDate = null;
			while (DateUtils.compare(date, endDate, false) <= 0) {
				Date lookupDate = date;
				// If not a valid valuation date, i.e. weekend look back to the previous day for the values
				if (!calculator.isValidValuationDate(date)) {
					lookupDate = calculator.getNextValuationDate(date, false);
				}
				List<BillingBasisSnapshot> dateFilteredList;
				// If dates are different - clone the list and set the date to the current date we are processing
				if (!DateUtils.isEqualWithoutTime(date, lookupDate)) {
					List<BillingBasisSnapshot> originalList = filterBillingBasisSnapshotListByDate(snapshotListEntry.getValue(), lookupDate);
					dateFilteredList = new ArrayList<>();
					for (BillingBasisSnapshot bbs : CollectionUtils.getIterable(originalList)) {
						BillingBasisSnapshot clone = BeanUtils.cloneBean(bbs, false, false);
						clone.setSnapshotDate(date);
						dateFilteredList.add(clone);
					}
				}
				else {
					dateFilteredList = filterBillingBasisSnapshotListByDate(snapshotListEntry.getValue(), lookupDate);
				}

				// If list is empty - clone the previous list, but set values to zero
				if (CollectionUtils.isEmpty(dateFilteredList)) {
					for (BillingBasisSnapshot previousSnapshot : CollectionUtils.getIterable(previousValueList)) {
						BillingBasisSnapshot snapshot = BeanUtils.cloneBean(previousSnapshot, false, false);
						snapshot.setId(null);
						snapshot.setSnapshotDate(date);
						snapshot.setSnapshotValue(BigDecimal.ZERO);
						snapshot.setQuantity(BigDecimal.ZERO);
						snapshot.setSecurityPrice(BigDecimal.ZERO);
						dateFilteredList.add(snapshot);
					}
				}

				if (!CollectionUtils.isEmpty(dateFilteredList)) {
					// DataSourceName will only be populated if we need to lookup exchange rate
					if (!StringUtils.isEmpty(dataSourceName)) {
						BigDecimal exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName,
								billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), lookupDate).flexibleLookup());
						for (BillingBasisSnapshot sn : CollectionUtils.getIterable(dateFilteredList)) {
							// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
							// If missing only, then use system determined rate
							if (MathUtils.isNullOrZero(sn.getFxRate())) {
								sn.setFxRate(exchangeRate);
							}
						}
					}
				}


				BigDecimal currentValue = getBillingBasisTotalFromList(dateFilteredList);
				if (previousValue == null) {
					previousValue = currentValue;
					previousValueList = dateFilteredList;
					previousValueDate = date;
				}
				if (!MathUtils.isEqual(currentValue, previousValue)) {
					filteredValueList.addAll(previousValueList);
					BigDecimal weight = MathUtils.divide(DateUtils.getDaysDifference(date, previousValueDate), DateUtils.getDaysDifferenceInclusive(endDate, startDate));
					BigDecimal weightedValue = MathUtils.multiply(previousValue, weight);
					weightedValueList.add(weightedValue);

					previousValue = currentValue;
					previousValueList = dateFilteredList;
					previousValueDate = date;
				}

				// Move to next day
				date = DateUtils.addDays(date, 1);
			}

			// Add the last set in:
			if (!CollectionUtils.isEmpty(previousValueList)) {
				filteredValueList.addAll(previousValueList);
				BigDecimal weight = MathUtils.divide((DateUtils.isEqualWithoutTime(endDate, date) ? DateUtils.getDaysDifferenceInclusive(date, previousValueDate) : DateUtils.getDaysDifference(date, previousValueDate)), DateUtils.getDaysDifferenceInclusive(endDate, startDate));
				BigDecimal weightedValue = MathUtils.multiply(previousValue, weight);
				weightedValueList.add(weightedValue);
			}
		}
		getBillingBasisService().saveBillingBasisSnapshotList(filteredValueList);

		// Add together the weighted list
		// Ensure Final Billing Basis Total is Positive Value
		BigDecimal total = MathUtils.abs(CoreMathUtils.sum(weightedValueList));
		return new BillingBasis(startDate, endDate, total);
	}


	private String getBillingBasisSnapshotGroupingKey(BillingBasisSnapshot billingBasisSnapshot) {
		if (billingBasisSnapshot.getSecurity() != null) {
			return "SECURITY_" + billingBasisSnapshot.getSecurity().getId();
		}
		// ARE THERE ANY OTHER POSSIBLE SCENARIOS THAT WOULD MAKE SENSE? NO OTHER USE CASES YET
		throw new ValidationException("Weighted calculation only currently supported for security specific details.");
	}
}
