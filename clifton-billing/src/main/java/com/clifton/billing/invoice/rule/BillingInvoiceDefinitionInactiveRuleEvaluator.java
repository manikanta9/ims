package com.clifton.billing.invoice.rule;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * The BillingInvoiceDefinitionInactiveRuleEvaluator validates that the billing definition is active during the invoice period. Invoice Generation itself wouldn't allow this
 * but extra check in case any modifications applied later (Although now with workflow not likely to ever happen).
 *
 * @author manderson
 */
public class BillingInvoiceDefinitionInactiveRuleEvaluator extends BaseBillingInvoiceRuleEvaluator {


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (invoice.getBillingDefinition() != null) {
			if (!DateUtils.isOverlapInDates(invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate(), invoice.getBillingDefinition().getStartDate(), invoice.getBillingDefinition().getEndDate())) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, invoice.getId(), invoice.getBillingDefinition().getId()));
			}
		}
		return ruleViolationList;
	}
}
