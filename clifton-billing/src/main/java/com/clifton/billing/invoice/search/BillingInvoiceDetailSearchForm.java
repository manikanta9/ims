package com.clifton.billing.invoice.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingInvoiceDetailAccountSearchForm</code> ...
 *
 * @author manderson
 */
public class BillingInvoiceDetailSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "invoice.id")
	private Integer invoiceId;

	@SearchField(searchField = "billingSchedule.id")
	private Integer billingScheduleId;

	@SearchField(searchFieldPath = "billingSchedule", searchField = "name")
	private String billingScheduleName;

	@SearchField(searchFieldPath = "billingSchedule", searchField = "sharedSchedule")
	private Boolean sharedSchedule;

	@SearchField
	private BigDecimal billingBasis;

	@SearchField
	private BigDecimal billingAmount;

	@SearchField
	private Date accrualStartDate;

	@SearchField(searchField = "billingSchedule.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean manual;


	@SearchField
	private Date accrualEndDate;

	@SearchField
	private String note;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvoiceId() {
		return this.invoiceId;
	}


	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}


	public Integer getBillingScheduleId() {
		return this.billingScheduleId;
	}


	public void setBillingScheduleId(Integer billingScheduleId) {
		this.billingScheduleId = billingScheduleId;
	}


	public BigDecimal getBillingBasis() {
		return this.billingBasis;
	}


	public void setBillingBasis(BigDecimal billingBasis) {
		this.billingBasis = billingBasis;
	}


	public BigDecimal getBillingAmount() {
		return this.billingAmount;
	}


	public void setBillingAmount(BigDecimal billingAmount) {
		this.billingAmount = billingAmount;
	}


	public Date getAccrualStartDate() {
		return this.accrualStartDate;
	}


	public void setAccrualStartDate(Date accrualStartDate) {
		this.accrualStartDate = accrualStartDate;
	}


	public Date getAccrualEndDate() {
		return this.accrualEndDate;
	}


	public void setAccrualEndDate(Date accrualEndDate) {
		this.accrualEndDate = accrualEndDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getBillingScheduleName() {
		return this.billingScheduleName;
	}


	public void setBillingScheduleName(String billingScheduleName) {
		this.billingScheduleName = billingScheduleName;
	}


	public Boolean getSharedSchedule() {
		return this.sharedSchedule;
	}


	public void setSharedSchedule(Boolean sharedSchedule) {
		this.sharedSchedule = sharedSchedule;
	}


	public Boolean getManual() {
		return this.manual;
	}


	public void setManual(Boolean manual) {
		this.manual = manual;
	}
}
