package com.clifton.billing.invoice.rule;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceTotalEqualsAccountAllocationTotalRuleEvaluator</code> validates that the invoice total and the amount allocated to accounts match (within specified threshold).
 * This ensures that accounts are properly allocated which is important for projected revenue and sales commission calculations.
 * <p>
 * Checks each amount category individually, i.e. Gross REVENUE, REVENUE_SHARE_EXTERNAL, etc.
 *
 * @author manderson
 */
public class BillingInvoiceTotalEqualsAccountAllocationTotalRuleEvaluator extends BaseBillingInvoiceRuleEvaluator {


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		List<BillingInvoiceDetailAccount> accountDetailList = context.getBillingInvoiceDetailAccountList(invoice.getId());
		validateTotalAmounts(invoice, invoice.getRevenueGrossTotalAmount(), CoreMathUtils.sumProperty(accountDetailList, BillingInvoiceDetailAccount::getRevenueGrossTotalAmount), "Revenue Gross Total Amount", entityConfig, violationList);
		validateTotalAmounts(invoice, invoice.getRevenueShareExternalTotalAmount(), CoreMathUtils.sumProperty(accountDetailList, BillingInvoiceDetailAccount::getRevenueShareExternalTotalAmount), "Broker Fee Total Amount", entityConfig, violationList);
		validateTotalAmounts(invoice, invoice.getRevenueShareInternalTotalAmount(), CoreMathUtils.sumProperty(accountDetailList, BillingInvoiceDetailAccount::getRevenueShareInternalTotalAmount), "Revenue Share Total Amount", entityConfig, violationList);
		validateTotalAmounts(invoice, invoice.getOtherTotalAmount(), CoreMathUtils.sumProperty(accountDetailList, BillingInvoiceDetailAccount::getOtherTotalAmount), "Total Amount", entityConfig, violationList);

		return violationList;
	}


	private void validateTotalAmounts(BillingInvoice invoice, BigDecimal invoiceTotal, BigDecimal accountTotal, String propertyName, EntityConfig entityConfig, List<RuleViolation> violationList) {
		Map<String, Object> templateValues = new HashMap<>();
		invoiceTotal = ObjectUtils.coalesce(invoiceTotal, BigDecimal.ZERO);
		accountTotal = ObjectUtils.coalesce(accountTotal, BigDecimal.ZERO);
		if (RuleEvaluatorUtils.isEntityConfigRangeViolated(MathUtils.subtract(invoiceTotal, accountTotal), entityConfig, templateValues)) {
			templateValues.put("accountTotal", accountTotal);
			templateValues.put("invoiceTotal", invoiceTotal);
			templateValues.put("propertyName", propertyName);
			violationList.add(getRuleViolationService().createRuleViolation(entityConfig, invoice.getId(), templateValues));
		}
	}
}
