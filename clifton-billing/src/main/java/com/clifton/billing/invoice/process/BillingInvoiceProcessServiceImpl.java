package com.clifton.billing.invoice.process;


import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionBillingBasisPostProcessor;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSecurityExclusionSearchForm;
import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.BillingInvoiceUtils;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisCalculator;
import com.clifton.billing.invoice.process.billingbasis.postprocessors.BillingBasisPostProcessor;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountCalculator;
import com.clifton.billing.invoice.process.schedule.waive.BillingScheduleWaiveConditionConfig;
import com.clifton.billing.invoice.report.BillingInvoiceReportService;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleHandler;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleHandlerImpl;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceProcessServiceImpl</code> provides processing for the Management Fee Invoice Types
 * These types are noted as requiring a BillingDefinition and there can only be one type.
 *
 * @author manderson
 */
@Service
public class BillingInvoiceProcessServiceImpl implements BillingInvoiceProcessService {

	private BillingBasisService billingBasisService;
	private BillingDefinitionService billingDefinitionService;
	private BillingInvoiceRuleHandler billingInvoiceRuleHandler;
	private BillingInvoiceService billingInvoiceService;
	private BillingInvoiceReportService billingInvoiceReportService;

	private InvestmentAccountService investmentAccountService;

	private RuleViolationStatusService ruleViolationStatusService;
	private RunnerHandler runnerHandler;

	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////
	////////            Billing Invoice Generating Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String generateBillingInvoiceList(BillingDefinitionSearchForm searchForm, final Date invoiceStartDate, final Date invoiceEndDate) {
		// ALWAYS EXCLUDE ARCHIVED DEFINITIONS
		searchForm.setExcludeWorkflowStateName(BillingDefinition.ARCHIVED_WORKFLOW_STATE_NAME);

		// asynchronous run support
		String runId;
		if (searchForm.getBillingDefinitionId() != null) {
			runId = "DEF-" + searchForm.getBillingDefinitionId();
		}
		else {
			if (searchForm.getCompanyId() != null) {
				runId = "COMPANY-" + searchForm.getCompanyId();
			}
			else if (searchForm.getInvestmentAccountGroupId() != null) {
				runId = "GROUP-" + searchForm.getInvestmentAccountGroupId();
			}
			else {
				runId = "ALL";
			}
		}
		final List<BillingDefinition> definitionList = getBillingDefinitionService().getBillingDefinitionList(searchForm);
		runId += "-" + DateUtils.fromDateShort(invoiceStartDate) + DateUtils.fromDateShort(invoiceEndDate);

		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("BILLING-INVOICE", runId, now) {
			@Override
			public void run() {
				getStatus().setMessage(doGenerateBillingInvoiceList(definitionList, invoiceStartDate, invoiceEndDate, getStatus()));
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started generating requested invoices. Processing will be completed shortly.";
	}


	private String doGenerateBillingInvoiceList(List<BillingDefinition> definitionList, Date startDate, Date endDate, Status status) {
		int success = 0;
		int skipped = 0;
		int failed = 0;
		StringBuilder failedErrors = new StringBuilder();

		for (BillingDefinition def : CollectionUtils.getIterable(definitionList)) {
			Date date = def.getBillingFrequency().getLastDayOfBillingCycle(startDate, def.getSecondYearStartDate());
			Date definitionEndDate = (def.getEndDate() == null ? endDate : def.getBillingFrequency().getLastDayOfBillingCycle(endDate, def.getSecondYearStartDate()));
			BillingInvoice invoice = null;
			while (DateUtils.isDateBetween(date, startDate, definitionEndDate, false)) {
				try {
					invoice = populateNewBillingInvoice(def, date);
					Date periodEnd = invoice.getInvoicePeriodEndDate();
					// If Billing Definition Not Active at any point during invoice period skip it
					if (!DateUtils.isOverlapInDates(invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate(), def.getStartDate(), def.getEndDate())) {
						skipped++;
						// Move date to the next invoice period date
						date = def.getBillingFrequency().getLastDayOfBillingCycle(DateUtils.addDays(periodEnd, 1), def.getSecondYearStartDate());
						continue;
					}
					invoice = generateBillingInvoiceImpl(def.getId(), date, null, null, true, false);
					if (invoice != null) {
						success++;
					}
				}
				catch (Exception e) {
					failed++;
					String msg = "Def: " + def.getLabel() + (invoice != null ? " on: " + DateUtils.fromDateShort(invoice.getInvoiceDate()) : "") + ": " + ExceptionUtils.getOriginalMessage(e);
					if (status != null) {
						status.addError(msg);
					}
					failedErrors.append(StringUtils.NEW_LINE).append(msg);
				}
				if (status != null) {
					status.setMessage("Total Billing Definitions: " + CollectionUtils.getSize(definitionList) + " Inactive Definitions Skipped: " + skipped + ". Invoices Created Successfully: "
							+ success + ", Failed: " + failed + failedErrors.toString());
				}
				// Move to Next Day to see if another invoice should be generated for the start/end dates
				date = def.getBillingFrequency().getLastDayOfBillingCycle(DateUtils.addDays((invoice == null ? endDate : invoice.getInvoiceDate()), 1), def.getSecondYearStartDate());
			}
		}
		return "Processing Complete. Total Billing Definitions Processed: " + CollectionUtils.getSize(definitionList) + " Inactive Definitions Skipped: " + skipped
				+ ". Invoices Created Successfully: " + success + ", Failed: " + failed + failedErrors.toString();
	}


	@Transactional(timeout = 180)
	@Override
	public BillingInvoice generateBillingInvoice(int billingDefinitionId, Date invoiceDate) {
		BillingDefinition definition = getBillingDefinitionService().getBillingDefinitionPopulated(billingDefinitionId);
		// Move date passed to actual invoice date for the period
		invoiceDate = definition.getBillingFrequency().getLastDayOfBillingCycle(invoiceDate, definition.getSecondYearStartDate());

		BillingInvoice invoice = populateNewBillingInvoice(definition, invoiceDate);
		// If Billing Definition Not Active at any point during invoice period skip it
		if (!DateUtils.isOverlapInDates(invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate(), definition.getStartDate(), definition.getEndDate())) {
			throw new ValidationException("Cannot generate invoice for period [" + DateUtils.fromDateShort(invoice.getInvoicePeriodStartDate()) + " - "
					+ DateUtils.fromDateShort(invoice.getInvoicePeriodEndDate()) + "] because the billing definition is not active during this period.");
		}

		return generateBillingInvoiceImpl(billingDefinitionId, invoiceDate, null, invoiceDate, false, false);
	}


	private BillingInvoice generateBillingInvoiceImpl(int billingDefinitionId, Date invoiceDate, Date startDate, Date endDate, boolean returnExistingIfExists,
	                                                  boolean groupedInvoice) {
		BillingDefinition definition = getBillingDefinitionService().getBillingDefinitionPopulated(billingDefinitionId);
		// Additional Check to Prevent Creating new Invoices for Archived Billing Definitions
		if (definition.getWorkflowState() != null) {
			ValidationUtils.assertFalse(StringUtils.isEqualIgnoreCase(BillingDefinition.ARCHIVED_WORKFLOW_STATE_NAME, definition.getWorkflowState().getName()), "Cannot generate invoice for billing definition [" + definition.getLabel() + "] because definition is in " + definition.getWorkflowState().getName() + " workflow state.");
		}

		// Move Invoice Date to the Last Day of the Month, Quarter, Year (depends on frequency)
		invoiceDate = definition.getBillingFrequency().getLastDayOfBillingCycle(invoiceDate, definition.getSecondYearStartDate());

		// Validate Start/End Dates if Necessary (Used for Grouped Invoices)
		if (!DateUtils.isDateBetween(invoiceDate, startDate, endDate, false)) {
			return null;
		}

		// Make sure same invoice doesn't exist for same BillingDefinition & Invoice Date (that isn't voided/cancelled)
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(billingDefinitionId);
		searchForm.setInvoiceDate(invoiceDate);
		searchForm.setInvoiceTypeId(definition.getInvoiceType().getId());

		List<BillingInvoice> existingInvoices = getBillingInvoiceService().getBillingInvoiceList(searchForm);
		for (BillingInvoice inv : CollectionUtils.getIterable(existingInvoices)) {
			if (inv.isCanceled()) {
				continue;
			}
			if (returnExistingIfExists) {
				return inv;
			}
			throw new ValidationException(
					"Cannot create a new Invoice for ["
							+ inv.getLabel()
							+ " because there is already one in the system that is not currently Voided/Cancelled.  Please void the original invoice before creating a new invoice for the same billing definition/invoice period.");
		}

		// Definition / Invoice has be Validated for Creation
		BillingInvoice invoice = populateNewBillingInvoice(definition, invoiceDate);

		// Determine if there are any prerequisite invoices that need to be generated for the invoice
		List<BillingScheduleBillingDefinitionDependency> prerequisiteDefinitions = getBillingDefinitionService().getBillingScheduleBillingDefinitionDependencyListByDefinition(billingDefinitionId, false);
		prerequisiteDefinitions = BeanUtils.filter(prerequisiteDefinitions, prerequisiteDefinition -> DateUtils.isOverlapInDates(prerequisiteDefinition.getBillingSchedule().getStartDate(), prerequisiteDefinition.getBillingSchedule().getEndDate(), invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate()));
		if (!CollectionUtils.isEmpty(prerequisiteDefinitions)) {
			BillingDefinition[] prerequisiteBillingDefinitions = BeanUtils.getPropertyValuesUniqueExcludeNull(prerequisiteDefinitions, BillingScheduleBillingDefinitionDependency::getBillingDefinition, BillingDefinition.class);
			doGenerateBillingInvoiceList(Arrays.asList(prerequisiteBillingDefinitions), invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate(), null);
		}

		// Find out if the invoice should be part of a group - if so, create all in the group
		// If Not already grouped, see if we need to group it
		if (!groupedInvoice) {
			List<Integer> sharedDefinitionIdList = getBillingDefinitionService().getBillingDefinitionIdSharedList(billingDefinitionId, invoice.getInvoicePeriodStartDate(),
					invoice.getInvoicePeriodEndDate());
			// If only one definition in the list, then it's not shared, if there's more than one, then it's shared, so we need the group
			if (CollectionUtils.getSize(sharedDefinitionIdList) > 1) {
				List<BillingInvoice> groupedInvoiceList = new ArrayList<>();
				groupedInvoiceList.add(invoice);
				for (Integer sharedBdId : sharedDefinitionIdList) {
					if (sharedBdId == billingDefinitionId) {
						continue;
					}
					// Verifies dates when adding the invoice to the group - make sure not null
					BillingInvoice thisInvoice = generateBillingInvoiceImpl(sharedBdId, invoiceDate, null, invoiceDate, returnExistingIfExists, true);
					if (thisInvoice != null) {
						groupedInvoiceList.add(thisInvoice);
					}
				}
				if (CollectionUtils.getSize(groupedInvoiceList) > 1) {
					createBillingInvoiceGroupForInvoiceList(groupedInvoiceList, invoiceDate);
				}
				else {
					getBillingInvoiceService().saveBillingInvoice(groupedInvoiceList.get(0));
				}
			}
			// If not grouping it - Save the Invoice
			else {
				getBillingInvoiceService().saveBillingInvoice(invoice);
			}
		}

		// Saving a new invoice will kick off workflow to process the invoice
		return invoice;
	}


	private BillingInvoice populateNewBillingInvoice(BillingDefinition definition, Date invoiceDate) {
		BillingInvoice invoice = new BillingInvoice();
		invoice.setBillingDefinition(definition);
		invoice.setInvoiceType(definition.getInvoiceType());
		invoice.setInvoiceDate(invoiceDate);
		invoice.setTotalAmount(BigDecimal.ZERO);

		if (definition.isPaymentInAdvance()) {
			invoice.setInvoicePeriodStartDate(DateUtils.addDays(invoiceDate, 1));
			invoice.setInvoicePeriodEndDate(DateUtils.addDays(DateUtils.addMonths(invoice.getInvoicePeriodStartDate(), definition.getBillingFrequency().getMonthsInPeriod()), -1));
		}
		else {
			invoice.setInvoicePeriodStartDate(DateUtils.addMonths(DateUtils.addDays(invoiceDate, 1), -definition.getBillingFrequency().getMonthsInPeriod()));
			invoice.setInvoicePeriodEndDate(invoiceDate);
		}
		return invoice;
	}


	private void createBillingInvoiceGroupForInvoiceList(List<BillingInvoice> invoiceList, Date invoiceDate) {
		BillingInvoiceGroup group = createBillingInvoiceGroup(invoiceList, invoiceDate);
		Short stateId = null;
		List<WorkflowState> nextStates = getWorkflowDefinitionService().getWorkflowStateNextAllowedList(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, BeanUtils.getIdentityAsLong(group));
		for (WorkflowState state : CollectionUtils.getIterable(nextStates)) {
			if ("Draft".equals(state.getName())) {
				stateId = state.getId();
				break;
			}
		}
		if (stateId != null) {
			getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, group.getId(), stateId);
		}
	}


	@Transactional
	protected BillingInvoiceGroup createBillingInvoiceGroup(List<BillingInvoice> invoiceList, Date invoiceDate) {
		ValidationUtils.assertTrue(CollectionUtils.getSize(invoiceList) > 1, "Cannot create a group of invoices unless there is more than one invoice in the group");
		BillingInvoiceGroup group = new BillingInvoiceGroup();
		group.setInvoiceDate(invoiceDate);
		getBillingInvoiceService().saveBillingInvoiceGroup(group);

		for (BillingInvoice invoice : invoiceList) {
			invoice.setInvoiceGroup(group);
			getBillingInvoiceService().saveBillingInvoice(invoice);
		}
		return group;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Billing Invoice Processing Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processBillingInvoice(int invoiceId) {
		BillingInvoice invoice = getBillingInvoiceService().getBillingInvoicePopulated(invoiceId);
		if (!invoice.getInvoiceType().isBillingDefinitionRequired()) {
			throw new ValidationException("Unable to Process Invoice [" + invoice.getId() + "] because its type [" + invoice.getInvoiceType().getName()
					+ "] does not use a billing definition which is required for processing.");
		}
		clearBillingInvoiceForReProcessing(invoice);

		BillingInvoiceProcessConfig config = new BillingInvoiceProcessConfig(invoice);
		// Get a list of ALL the schedules - in schedule order, security exclusions, etc.
		populateBillingInvoiceProcessConfigDetails(config);
		Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap = processBillingBasisListForInvoiceImpl(config, invoice, invoice.getBillingBasisStartDate(), invoice.getBillingBasisEndDate());
		config.setBillingBasisMap(billingBasisMap);
		processBillingInvoiceImpl(config);
	}


	@Override
	@Transactional(timeout = 180)
	public void processBillingInvoiceGroup(int invoiceGroupId) {
		BillingInvoiceGroup group = getBillingInvoiceService().getBillingInvoiceGroup(invoiceGroupId);

		// First Delete all Billing Basis across all, and clear report cache
		for (BillingInvoice invoice : CollectionUtils.getIterable(group.getInvoiceList())) {
			ValidationUtils.assertTrue(invoice.getInvoiceType().isBillingDefinitionRequired(), "Grouped Invoices are supported for Invoice Types that use Billing Definitions only.");
			clearBillingInvoiceForReProcessing(invoice);
		}

		// Process Billing Basis
		BillingInvoiceProcessConfig config = new BillingInvoiceProcessConfig(group);
		// Get a list of ALL the schedules - in schedule order, security exclusions, etc.
		populateBillingInvoiceProcessConfigDetails(config);
		Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap = processBillingBasisListForInvoiceGroup(config);
		config.setBillingBasisMap(billingBasisMap);
		processBillingInvoiceImpl(config);
	}


	private void clearBillingInvoiceForReProcessing(BillingInvoice invoice) {
		// Clear Billing Basis
		getBillingBasisService().deleteBillingBasisSnapshotListForInvoice(invoice.getId());

		// Clear the report cache
		getBillingInvoiceReportService().clearBillingInvoiceReportCache(invoice.getId());

		// Set Un-Processed
		invoice.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));

		// Note: When the invoice is saved - by removing them from the invoice here they will be automatically deleted
		List<BillingInvoiceDetail> manualDetailList = BeanUtils.filter(invoice.getDetailList(), BillingInvoiceDetail::isManual);
		for (BillingInvoiceDetail detail : CollectionUtils.getIterable(manualDetailList)) {
			// Pull again with account level details so we don't lose them
			detail.setDetailAccountList(getBillingInvoiceService().getBillingInvoiceDetailAccountListByInvoiceDetail(detail.getId()));
		}
		BillingInvoiceUtils.recalculateTotalAmountsForBillingInvoice(invoice, manualDetailList);
		invoice.setDetailList(manualDetailList);

		// Save the Invoice so the detail list is deleted and amount is reset
		getBillingInvoiceService().saveBillingInvoice(invoice);

		// Re-set Billing Definition With Account List Populated
		invoice.setBillingDefinition(getBillingDefinitionService().getBillingDefinitionPopulated(invoice.getBillingDefinition().getId()));
	}


	/**
	 * Processes the invoice or invoice group (whichever is set in the config object) and saves the resulting
	 * invoice(s).
	 */
	private void processBillingInvoiceImpl(BillingInvoiceProcessConfig config) {
		// Processes all schedules - independent of which billing definition they are associated with in schedule order
		// schedule processing will allocate and apply the schedule to the invoices that use them appropriately
		for (BillingSchedule schedule : config.getScheduleList()) {
			processBillingSchedule(config, schedule);
		}

		List<BillingInvoice> invoiceList = config.getInvoiceList();
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			BillingInvoiceUtils.recalculateTotalAmountsForBillingInvoice(invoice, invoice.getDetailList());
			invoice.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));

			// Merge New/Removed/Updated System Defined Violation Lists
			getBillingInvoiceRuleHandler().saveBillingInvoiceRuleViolationListSystemDefined(invoice, BillingInvoiceRuleHandlerImpl.RULE_DEFINITION_WAIVED_SCHEDULES, config.getInvoiceScheduleWaivedViolationsMap(invoice.getId()));
			getBillingInvoiceRuleHandler().saveBillingInvoiceRuleViolationListSystemDefined(invoice, BillingInvoiceRuleHandlerImpl.RULE_DEFINITION_PROCESSING_SCHEDULES, config.getInvoiceScheduleProcessingViolationsMap(invoice.getId()));
			getBillingInvoiceRuleHandler().saveBillingInvoiceRuleViolationListForBillingBasisMissingSourceData(invoice, config.getInvoiceBillingBasisMissingSourceDataViolationMap(invoice.getId()));
			getBillingInvoiceRuleHandler().saveBillingInvoiceRuleViolationListForBillingBasisSnapshotSecurityExcluded(invoice, config.getBillingBasisSecurityExclusionViolationMap());

			// Save the invoice
			getBillingInvoiceService().saveBillingInvoice(invoice);

			// Run Rule Violations
			getBillingInvoiceRuleHandler().validateBillingInvoiceRules(invoice);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////     Billing Invoice Billing Basis Processing Methods           //////
	////////////////////////////////////////////////////////////////////////////


	private Map<BillingDefinitionInvestmentAccount, BillingBasis> processBillingBasisListForInvoiceGroup(BillingInvoiceProcessConfig config) {
		BillingInvoiceGroup group = config.getInvoiceGroup();
		List<BillingInvoice> invoiceList = group.getInvoiceList();

		// Map of BillingDefinitionInvestmentAccount -> BillingBasis that can be used by schedules
		Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap = new HashMap<>();

		Date startDate = null;
		Date endDate = null;

		// Determine Start/End Dates - Shared Invoices Billing Basis should be the same so schedules can be applied to the same date ranges
		for (BillingInvoice invoice : invoiceList) {
			if (startDate == null) {
				startDate = invoice.getBillingBasisStartDate();
			}
			else {
				// If shared definition starts differently - use the earlier date
				if (DateUtils.compare(invoice.getBillingBasisStartDate(), startDate, false) < 0) {
					startDate = invoice.getBillingBasisStartDate();
				}
			}
			if (endDate == null) {
				endDate = invoice.getBillingBasisEndDate();
			}
			else {
				// If shared definition end differently - use the later date
				if (DateUtils.compare(invoice.getBillingBasisEndDate(), endDate, false) > 0) {
					endDate = invoice.getBillingBasisEndDate();
				}
			}
		}

		// Then calculate billing basis for each invoice in group
		for (BillingInvoice invoice : invoiceList) {
			Map<BillingDefinitionInvestmentAccount, BillingBasis> invoiceBillingBasisMap = processBillingBasisListForInvoiceImpl(config, invoice, startDate, endDate);
			for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> billingDefinitionInvestmentAccountBillingBasisEntry : invoiceBillingBasisMap.entrySet()) {
				billingBasisMap.put(billingDefinitionInvestmentAccountBillingBasisEntry.getKey(), billingDefinitionInvestmentAccountBillingBasisEntry.getValue());
			}
		}
		return billingBasisMap;
	}


	private Map<BillingDefinitionInvestmentAccount, BillingBasis> processBillingBasisListForInvoiceImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, Date startDate, Date endDate) {
		// Map of BillingDefinitionInvestmentAccount -> BillingBasis that can be used by schedules
		Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap = new HashMap<>();

		// Create Map of BillingDefinitionInvestmentAccounts Processed and Missing Billing Basis Violations
		//Add the definitions to the map since the rule should still be evaluated when the violation list is updated
		//in order to 'remove' violations from previous processing that have since been fixed.
		Map<String, List<RuleViolation>> ruleViolationMap = new HashMap<>();
		ruleViolationMap.put(BillingInvoiceRuleHandler.RULE_DEFINITION_NO_BILLING_BASIS, null);
		ruleViolationMap.put(BillingInvoiceRuleHandler.RULE_DEFINITION_ZERO_BILLING_BASIS, null);

		// Go through each Billing Definition, Account and calculate billing basis
		for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(invoice.getBillingDefinition().getId()))) {
			// Returns calculated billing basis and adds warning to warning list of billing basis is null/zero
			BillingBasis billingBasis = calculateBillingBasis(config, invoice, billingAccount, ruleViolationMap, startDate, endDate);

			if (billingBasis != null) {
				billingBasisMap.put(billingAccount, billingBasis);
			}
		}

		// Apply Post Processors
		for (BillingDefinitionBillingBasisPostProcessor postProcessor : CollectionUtils.getIterable(getBillingDefinitionService().getBillingDefinitionBillingBasisPostProcessorListByDefinition(
				invoice.getBillingDefinition().getId()))) {
			BillingBasisPostProcessor processor = (BillingBasisPostProcessor) getSystemBeanService().getBeanInstance(postProcessor.getReferenceTwo());
			processor.process(billingBasisMap);
		}

		// Merge New/Removed/Updated System Defined Warning Lists
		getBillingInvoiceRuleHandler().saveBillingInvoiceRuleViolationListSystemDefined(invoice, ruleViolationMap);
		return billingBasisMap;
	}


	private BillingBasis calculateBillingBasis(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount account, Map<String, List<RuleViolation>> ruleViolationMap, Date startDate, Date endDate) {
		// Verify Billing Definition Investment Account is Active during Billing Basis Period
		if (!DateUtils.isOverlapInDates(startDate, endDate, account.getStartDate(), account.getEndDate())) {
			// Skip It - Don't need a warning, because it's not a current billing basis
			return null;
		}

		// NONE BillingBasisValuationTypes will NOT have a BillingBasisCalculationType defined, thus no billing basis
		// Skip these completely - no warning necessary because it's defined as NONE
		if (account.getBillingBasisValuationType().isNoBillingBasis()) {
			return null;
		}
		else if (account.getBillingBasisCalculationType() == null || account.getBillingBasisCalculationType().getCalculatorBean() == null) {
			throw new ValidationException("Missing billing basis calculation type for account " + account.getAccountLabel());
		}

		BillingBasisCalculator billingBasisCalculator = (BillingBasisCalculator) getSystemBeanService().getBeanInstance(account.getBillingBasisCalculationType().getCalculatorBean());
		BillingBasis billingBasis = billingBasisCalculator.calculate(config, invoice, account, startDate, endDate);

		String violationNote = null;
		String ruleDefinitionName = null;
		if (billingBasis == null) {
			violationNote = "No billing basis found for account " + account.getAccountLabel();
			ruleDefinitionName = BillingInvoiceRuleHandler.RULE_DEFINITION_NO_BILLING_BASIS;
		}
		else if (MathUtils.isEqual(BigDecimal.ZERO, billingBasis.getBillingBasisTotal())) {
			violationNote = "$0 billing basis generated for account " + account.getAccountLabel();
			ruleDefinitionName = BillingInvoiceRuleHandler.RULE_DEFINITION_ZERO_BILLING_BASIS;
		}
		if (violationNote != null) {
			RuleViolation ruleViolation = getBillingInvoiceRuleHandler().createSystemDefinedRuleViolation(invoice.getId(), account.getId(), violationNote, ruleDefinitionName);
			List<RuleViolation> ruleViolationList = ruleViolationMap.get(ruleDefinitionName);
			if (ruleViolationList == null) {
				ruleViolationList = new ArrayList<>();
			}
			ruleViolationList.add(ruleViolation);
			ruleViolationMap.put(ruleDefinitionName, ruleViolationList);
		}
		return billingBasis;
	}


	////////////////////////////////////////////////////////////////////////////
	/////    Billing Invoice Billing Schedule Processing Methods          //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates schedules and security exclusions on the config
	 */
	private void populateBillingInvoiceProcessConfigDetails(BillingInvoiceProcessConfig config) {
		int size = CollectionUtils.getSize(config.getInvoiceList());
		Integer[] bdIds = new Integer[size];
		for (int i = 0; i < size; i++) {
			bdIds[i] = config.getInvoiceList().get(i).getBillingDefinition().getId();
		}
		List<BillingSchedule> scheduleList = getBillingDefinitionService().getBillingScheduleListForDefinitionIds(bdIds);
		// Sort all schedules in order by schedule order
		scheduleList = BeanUtils.sortWithFunction(scheduleList, schedule -> schedule.getScheduleType().getScheduleOrder(), true);
		config.setScheduleList(scheduleList);

		BillingDefinitionInvestmentAccountSecurityExclusionSearchForm securityExclusionSearchForm = new BillingDefinitionInvestmentAccountSecurityExclusionSearchForm();
		securityExclusionSearchForm.setDefinitionIds(bdIds);
		config.setAccountBillingSecurityExclusionMap(getBillingDefinitionService().getBillingDefinitionInvestmentAccountSecurityExclusionList(securityExclusionSearchForm));
	}


	private void processBillingSchedule(BillingInvoiceProcessConfig config, BillingSchedule schedule) {
		// Pull Schedule From DB so all lists are populated
		schedule = getBillingDefinitionService().getBillingSchedule(schedule.getId());

		// Get a List of Invoices Affected By This schedule
		// Note: The list retrieval also verifies the schedule is active during the invoice period - if no invoices are returned, then it's not an active schedule
		List<BillingInvoice> invoiceList = config.getInvoiceListForSchedule(schedule, schedule.isSharedSchedule() ? getBillingDefinitionService().getBillingScheduleSharingListForSchedule(schedule.getId()) : null);
		if (CollectionUtils.isEmpty(invoiceList)) {
			return;
		}

		// Schedule only applies to one invoice - process it
		if (CollectionUtils.getSize(invoiceList) == 1) {
			processBillingScheduleForInvoice(config, invoiceList.get(0), schedule);
		}
		// Shared, but don't group invoice - process each invoice individually
		else if (schedule.isDoNotGroupInvoice()) {
			for (BillingInvoice invoice : invoiceList) {
				processBillingScheduleForInvoice(config, invoice, schedule);
			}
		}
		// Shared - Process together
		else {
			processBillingScheduleForInvoiceList(config, invoiceList, schedule);
		}
	}


	private void processBillingScheduleForInvoice(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingSchedule schedule) {
		boolean processPerAccount = false;
		if (schedule.getScheduleType().isFlatFee()) {
			// If multiple line items for an account group or apply to all client accounts, process each
			// only if not processing billing basis, if processing billing basis, then apply the billing basis only for that account
			if (schedule.getInvestmentAccountGroup() != null || schedule.isApplyToEachClientAccount()) {
				processPerAccount = true;
				List<InvestmentAccount> accountList;
				if (schedule.getInvestmentAccountGroup() != null) {
					InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
					searchForm.setOurAccount(true);
					searchForm.setInvestmentAccountGroupId(schedule.getInvestmentAccountGroup().getId());
					accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
				}
				else {
					accountList = new ArrayList<>();
					for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(invoice.getBillingDefinition().getInvestmentAccountList())) {
						if (accountList.contains(billingAccount.getReferenceTwo())) {
							continue;
						}
						if (DateUtils.isOverlapInDates(invoice.getBillingBasisStartDate(), invoice.getBillingBasisEndDate(), billingAccount.getStartDate(), billingAccount.getEndDate())) {
							accountList.add(billingAccount.getReferenceTwo());
						}
					}
				}

				// Clone the schedule, so we can set account and process it for just that account
				boolean originalApplyToEach = schedule.isApplyToEachClientAccount();
				InvestmentAccountGroup originalAccountGroup = schedule.getInvestmentAccountGroup();

				try {
					schedule.setApplyToEachClientAccount(false);
					schedule.setInvestmentAccountGroup(null);
					StringBuilder waivedMsg = new StringBuilder(16);

					for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
						schedule.setInvestmentAccount(account);
						String acctWaived = evaluateBillingScheduleWaiveCondition(config, invoice, schedule);
						if (!StringUtils.isEmpty(acctWaived)) {
							waivedMsg.append("Account #: ").append(account.getNumber()).append(" ").append(acctWaived).append(StringUtils.NEW_LINE);
						}
						else {
							processBillingScheduleImpl(config, CollectionUtils.createList(invoice), schedule);
						}
					}
					if (!StringUtils.isEmpty(waivedMsg.toString())) {
						config.setInvoiceScheduleWaivedViolation(invoice.getId(), schedule, waivedMsg.toString());
					}
				}
				finally {
					schedule.setInvestmentAccount(null);
					schedule.setApplyToEachClientAccount(originalApplyToEach);
					schedule.setInvestmentAccountGroup(originalAccountGroup);
				}
			}
		}
		if (!processPerAccount) {
			String waived = evaluateBillingScheduleWaiveCondition(config, invoice, schedule);
			if (!StringUtils.isEmpty(waived)) {
				config.setInvoiceScheduleWaivedViolation(invoice.getId(), schedule, waived);
			}
			else {
				processBillingScheduleImpl(config, CollectionUtils.createList(invoice), schedule);
			}
		}
	}


	private void processBillingScheduleForInvoiceList(BillingInvoiceProcessConfig config, List<BillingInvoice> invoiceList, BillingSchedule schedule) {

		List<BillingInvoice> filteredList = invoiceList;

		// If there is a waive condition - filter the list - otherwise don't bother iterating through the list
		if (schedule.getWaiveSystemCondition() != null) {
			filteredList = new ArrayList<>();
			for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
				String waived = evaluateBillingScheduleWaiveCondition(config, invoice, schedule);
				if (waived != null) {
					config.setInvoiceScheduleWaivedViolation(invoice.getId(), schedule, waived);
				}
				else {
					filteredList.add(invoice);
				}
			}
		}

		if (!filteredList.isEmpty()) {
			processBillingScheduleImpl(config, filteredList, schedule);
		}
	}


	private String evaluateBillingScheduleWaiveCondition(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingSchedule schedule) {
		String waivedMsg = null;

		// If a Waive System Condition is specified - evaluate it to see if we need to actually process this schedule
		if (schedule.getWaiveSystemCondition() != null) {

			schedule.setWaiveSystemConditionConfig(new BillingScheduleWaiveConditionConfig(invoice, config.getBillingBasisMap()));

			try {
				waivedMsg = getSystemConditionEvaluationHandler().getConditionTrueMessage(schedule.getWaiveSystemCondition(), schedule);
				if (waivedMsg != null) {
					waivedMsg = " waived because [" + waivedMsg + "]";
				}
			}
			catch (Throwable e) {
				throw new ValidationException("Error processing waive condition [" + schedule.getWaiveSystemCondition().getName() + "] for schedule [" + schedule.getName() + "] "
						+ (schedule.getInvestmentAccount() != null ? " and account [" + schedule.getInvestmentAccount().getLabel() + "]: " : ": ") + ExceptionUtils.getOriginalMessage(e), e);
			}
			finally {
				schedule.setWaiveSystemConditionConfig(null);
			}
		}
		return waivedMsg;
	}


	private void processBillingScheduleImpl(BillingInvoiceProcessConfig config, List<BillingInvoice> invoiceList, BillingSchedule schedule) {
		// lookup billing fee calculator and calculate billing fee
		BillingAmountCalculator billingAmountCalculator = config.getBillingAmountCalculatorForSchedule(schedule, getSystemBeanService());
		// Process for each accrual period if different than invoice frequency
		if (schedule.getAccrualFrequency() != null && config.getDefaultAccrualFrequency() != schedule.getAccrualFrequency()) {
			BillingScheduleProcessConfig scheduleConfig = new BillingScheduleProcessConfig(schedule, invoiceList);

			Date accrualStartDate = scheduleConfig.getInvoicePeriodStartDate();
			Date accrualEndDate = schedule.getAccrualFrequency().getLastDayOfBillingCycle(accrualStartDate, invoiceList.get(0).getBillingDefinition().getSecondYearStartDate());

			// Continue as long as within billing basis date range
			while (DateUtils.isDateBeforeOrEqual(accrualStartDate, scheduleConfig.getBillingBasisEndDate(), false)) {
				// If billing basis applies during accrual period - calculate it (i.e. skip if accrual period is for July, but Invoice Billing starts in September
				if (DateUtils.isOverlapInDates(accrualStartDate, accrualEndDate, scheduleConfig.getBillingBasisStartDate(), scheduleConfig.getBillingBasisEndDate())) {
					scheduleConfig.resetForAccrualPeriod(accrualStartDate, accrualEndDate);
					billingAmountCalculator.calculate(config, scheduleConfig);
				}
				// Move to Next Accrual Period
				accrualStartDate = DateUtils.addDays(accrualEndDate, 1);
				accrualEndDate = schedule.getAccrualFrequency().getLastDayOfBillingCycle(accrualStartDate, invoiceList.get(0).getBillingDefinition().getSecondYearStartDate());
			}
		}
		else {
			BillingScheduleProcessConfig scheduleConfig = new BillingScheduleProcessConfig(schedule, invoiceList);
			billingAmountCalculator.calculate(config, scheduleConfig);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceRuleHandler getBillingInvoiceRuleHandler() {
		return this.billingInvoiceRuleHandler;
	}


	public void setBillingInvoiceRuleHandler(BillingInvoiceRuleHandler billingInvoiceRuleHandler) {
		this.billingInvoiceRuleHandler = billingInvoiceRuleHandler;
	}


	public BillingInvoiceReportService getBillingInvoiceReportService() {
		return this.billingInvoiceReportService;
	}


	public void setBillingInvoiceReportService(BillingInvoiceReportService billingInvoiceReportService) {
		this.billingInvoiceReportService = billingInvoiceReportService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
