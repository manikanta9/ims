package com.clifton.billing.invoice.process.schedule.waive;

import com.clifton.core.util.math.ComparisonTypes;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;


/**
 * The <code>BaseBillingAmountComparison</code> can be extended by Billing comparisons that need to compare a value to a given static value.
 * i.e. BillingBasis >= 100 million
 *
 * @author manderson
 */
public abstract class BaseBillingAmountComparison {


	/**
	 * >, =, <, etc.
	 */
	private ComparisonTypes comparisonType;

	/**
	 * The amount to compare to
	 */
	private BigDecimal amount;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected boolean evaluateCondition(BigDecimal billingAmount) {
		if (getComparisonType() == null) {
			throw new ValidationException("Unable to compare billing amount [" + CoreMathUtils.formatNumberMoney(billingAmount) + "] to [" + CoreMathUtils.formatNumberMoney(getAmount())
					+ "].  Missing comparison type selection.");
		}
		return getComparisonType().compare(billingAmount, getAmount());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                 Getter and Setter Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ComparisonTypes getComparisonType() {
		return this.comparisonType;
	}


	public void setComparisonType(ComparisonTypes comparisonType) {
		this.comparisonType = comparisonType;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
