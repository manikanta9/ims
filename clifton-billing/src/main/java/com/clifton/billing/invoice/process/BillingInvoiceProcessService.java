package com.clifton.billing.invoice.process;


import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * The <code>BillingInvoiceProcessService</code> ...
 *
 * @author manderson
 */
public interface BillingInvoiceProcessService {

	////////////////////////////////////////////////////////////////////////////
	////////            Billing Invoice Generating Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("result")
	@RequestMapping("billingInvoiceGenerateList")
	public String generateBillingInvoiceList(BillingDefinitionSearchForm searchForm, final Date invoiceStartDate, final Date invoiceEndDate);


	public BillingInvoice generateBillingInvoice(int billingDefinitionId, Date invoiceDate);


	/**
	 * 1. Clears/Rebuilds BillingBasisSnapshot for the invoice
	 * 2. Processes all Schedules
	 * 3. Runs all Violations
	 */
	public void processBillingInvoice(int invoiceId);


	////////////////////////////////////////////////////////////////////////////
	///////         Billing Invoice Group Processing Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * 1. Clears/Rebuilds BillingBasisSnapshot for all invoices in the group
	 * 2. Processes all Schedules in order across all invoices
	 * 3. Runs all Violations
	 */
	public void processBillingInvoiceGroup(int invoiceGroupId);
}
