package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>BillingAmountForPercentageFeeCalculator</code> applies one percentage to the billing basis
 * Works similar to Tiered fees, except there is only one tier.
 *
 * @author Mary Anderson
 */
public class BillingAmountForPercentageFeeCalculator extends BillingAmountForTieredFeeCalculator {

	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		if (isApplyToBillingAmounts()) {
			// Flat Percentage - i.e. 10% revenue share is 10%, not 10% annually converted to quarterly or monthly
			return BillingAmountCalculatorTypes.FLAT_PERCENTAGE;
		}
		return BillingAmountCalculatorTypes.PERCENTAGE;
	}


	@Override
	public BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail, StringBuilder note) {
		// Billing Basis
		note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, billingBasisDetail.getTotalAmount(), false)).append(" at ");
		BillingAmountCalculatorUtils.addBillingInvoiceScheduleAmountNote(scheduleConfig, note, true);

		// In case percentage was defined at a different frequency than invoice
		BigDecimal percentage = BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, scheduleConfig.getSchedule().getScheduleAmount());

		return MathUtils.getPercentageOf(percentage, billingBasisDetail.getTotalAmount(), true);
	}
}
