package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleTier;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingAmountForTieredFeeCalculator</code> calculates the fee based on the billing basis and tiers from the schedule.
 *
 * @author Mary Anderson
 */
public class BillingAmountForTieredFeeCalculator extends BaseBillingAmountCalculator {


	/**
	 * Used for Revenue Sharing where billing basis isn't used, but the amounts are calculated off of the previously billed amounts for the same billing period
	 * Note: If applyToBillingAmountsFromPrerequisiteInvoices is set will require definition dependencies.  Otherwise will look ONLY on this invoice (or invoice group)
	 */
	private boolean applyToBillingAmounts;


	/**
	 * The amounts to apply the calculation against reside on another invoice(s) - Requires the use of definition dependencies
	 */
	private boolean applyToBillingAmountsFromPrerequisiteInvoices;


	/**
	 * Can calculate Revenue share off of Gross or Net revenue
	 */
	private List<BillingInvoiceRevenueTypes> applyToBillingAmountsRevenueTypeList;

	/**
	 * Only can be used when applyToBillingAmounts = true
	 * When set, this applies the tiers to the billing basis first, then the billing amount is calculated as a single percentage (blended fee) value
	 * of the tier calculation value as a percentage of the original billing basis.
	 * This is currently only used for revenue sharing for Merrill accounts.
	 */
	private boolean useBillingBasisForTiersToCalculatePercentageFee;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		if (isApplyToBillingAmounts()) {
			// Flat Percentage - i.e. 10% revenue share is 10%, not 10% annually converted to quarterly or monthly
			return BillingAmountCalculatorTypes.TIERED_FLAT_PERCENTAGE;
		}
		return BillingAmountCalculatorTypes.TIERED;
	}


	/**
	 * If using the pre-requisite invoices, then need to calculate without billing basis and apply account details based on previously billed amounts to each account
	 */
	@Override
	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		if (isApplyToBillingAmounts()) {
			// At this time we don't allow sharing so will only be one invoice.  We can still use a shared schedule that doesn't group the invoice
			// At some point we might want to open this up to allow sharing as long as not using pre-req invoices - but will need a specific use case for that.
			ValidationUtils.assertFalse(CollectionUtils.getSize(scheduleConfig.getInvoiceList()) > 1, "Shared schedules are not supported for revenue sharing type schedules where amount applies based on prerequisite invoices.");
			StringBuilder note = new StringBuilder(16);
			Map<InvestmentAccount, BigDecimal> accountBillingMap = isApplyToBillingAmountsFromPrerequisiteInvoices() ? getPrerequisiteInvoiceAccountFees(scheduleConfig, getApplyToBillingAmountsRevenueTypeList()) : getInvoiceAccountFees(scheduleConfig, getApplyToBillingAmountsRevenueTypeList());
			BigDecimal totalBilledAmount = CoreMathUtils.sum(CollectionUtils.getValues(accountBillingMap));
			BigDecimal amount;
			if (isUseBillingBasisForTiersToCalculatePercentageFee()) {
				Map<InvestmentAccount, BigDecimal> accountBillingBasisMap = isApplyToBillingAmountsFromPrerequisiteInvoices() ? getPrerequisiteInvoiceAccountBillingBasis(scheduleConfig, getApplyToBillingAmountsRevenueTypeList()) : getInvoiceAccountBillingBasis(scheduleConfig, getApplyToBillingAmountsRevenueTypeList());
				BigDecimal totalBillingBasis = CoreMathUtils.sum(CollectionUtils.getValues(accountBillingBasisMap));
				// Create it as new BillingBasis for the calculation
				BillingBasis billingBasis = new BillingBasis(scheduleConfig.getInvoicePeriodStartDate(), scheduleConfig.getInvoicePeriodEndDate(), totalBillingBasis);
				note.append("Blended Fee").append(StringUtils.NEW_LINE);
				BigDecimal calculatedBillingBasisTotal = calculateAmountImpl(scheduleConfig, billingBasis.getDetailList().get(0), note);

				BigDecimal percentageFee = CoreMathUtils.getPercentValue(calculatedBillingBasisTotal, totalBillingBasis, true);
				note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, MathUtils.abs(calculatedBillingBasisTotal), false)).append(" of ").append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, totalBillingBasis, false));
				note.append(StringUtils.NEW_LINE);
				note.append(StringUtils.NEW_LINE);

				note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, totalBilledAmount, false)).append(" at ");
				note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, percentageFee, true));
				if (scheduleConfig.getSchedule().getScheduleFrequency() != null) {
					note.append(" ").append(scheduleConfig.getSchedule().getScheduleFrequency().getInvoiceDescription());
				}

				// In case percentage was defined at a different frequency than invoice
				BigDecimal percentage = BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, percentageFee);
				amount = MathUtils.getPercentageOf(percentage, totalBilledAmount, true);
			}
			else {
				// Create it as new BillingBasis for the calculation
				BillingBasis billingBasis = new BillingBasis(scheduleConfig.getInvoicePeriodStartDate(), scheduleConfig.getInvoicePeriodEndDate(), totalBilledAmount);
				amount = calculateAmountImpl(scheduleConfig, billingBasis.getDetailList().get(0), note);
			}

			if (MathUtils.isNullOrZero(amount)) {
				addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
			}
			else {
				// At this time we don't allow sharing so will only be one invoice.  We can still use a shared schedule that doesn't group the invoice
				BillingInvoice invoice = scheduleConfig.getInvoiceList().get(0);
				BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, BigDecimal.ZERO, amount, note.toString());
				createInvoiceDetailAccountListFromPreviouslyBilledAllocations(detail, accountBillingMap);
				config.addInvoiceDetail(detail);
			}
		}
		else {
			super.calculateImpl(config, scheduleConfig);
		}
	}


	@Override
	public BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail, StringBuilder note) {
		BillingSchedule schedule = scheduleConfig.getSchedule();
		ValidationUtils.assertNotEmpty(schedule.getScheduleTierList(), "At least one Schedule Tier is required for Tiered Billing Amount Calculations.");

		return applyTiers(scheduleConfig, billingBasisDetail.getTotalAmount(), note);
	}


	private BigDecimal applyTiers(BillingScheduleProcessConfig scheduleConfig, BigDecimal totalAmount, StringBuilder note) {
		BigDecimal amount = BigDecimal.ZERO;

		// Put the tiers in order from smallest to largest
		BigDecimal previousThreshold = BigDecimal.ZERO;
		BillingScheduleTier finalTier = null;
		BigDecimal finalTierPercentage = null;

		for (BillingScheduleTier tier : BeanUtils.sortWithFunction(scheduleConfig.getSchedule().getScheduleTierList(), BillingScheduleTier::getThresholdAmount, true)) {
			// Final Tier - apply last if necessary
			if (tier.isMaxTier()) {
				finalTier = tier;
				finalTierPercentage = tier.getFeePercent();
				// In case percentage was defined at a different frequency than invoice
				finalTierPercentage = BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, finalTierPercentage);
				continue;
			}
			BigDecimal currentThreshold = tier.getThresholdAmount();
			BigDecimal percentage = tier.getFeePercent();
			// In case percentage was defined at a different frequency than invoice
			percentage = BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, percentage);
			// If Billing Basis is greater than the previous threshold then we need to apply this one
			if (MathUtils.isNullOrZero(previousThreshold) || MathUtils.isGreaterThan(totalAmount, previousThreshold)) {
				// Amount to apply depends on if the billing basis is less than or greater than the current threshold
				if (MathUtils.isGreaterThan(totalAmount, currentThreshold)) {
					addTierNote(scheduleConfig, tier, note, MathUtils.subtract(currentThreshold, previousThreshold));
					amount = MathUtils.add(amount, MathUtils.getPercentageOf(percentage, MathUtils.subtract(currentThreshold, previousThreshold), true));
				}
				else {
					addTierNote(scheduleConfig, tier, note, MathUtils.subtract(totalAmount, previousThreshold));
					amount = MathUtils.add(amount, MathUtils.getPercentageOf(percentage, MathUtils.subtract(totalAmount, previousThreshold), true));
				}
			}
			previousThreshold = currentThreshold;
		}

		// Apply Final Tier if necessary
		if (finalTierPercentage != null && MathUtils.isGreaterThan(totalAmount, previousThreshold)) {
			addTierNote(scheduleConfig, finalTier, note, MathUtils.subtract(totalAmount, previousThreshold));
			amount = MathUtils.add(amount, MathUtils.getPercentageOf(finalTierPercentage, MathUtils.subtract(totalAmount, previousThreshold), true));
		}

		return amount;
	}


	private void addTierNote(BillingScheduleProcessConfig scheduleConfig, BillingScheduleTier tier, StringBuilder note, BigDecimal amount) {
		// Billing Basis
		note.append(StringUtils.TAB).append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, amount, false)).append(" at ");

		// If Not Daily Conversion display the converted rate
		if (scheduleConfig.getSchedule().getScheduleFrequency() != null && scheduleConfig.getSchedule().getScheduleFrequency().isUseDailyConversion()) {
			note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, tier.getFeePercent(), true));
			note.append(" ").append(scheduleConfig.getSchedule().getScheduleFrequency().getInvoiceDescription());
		}
		else {
			note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, BillingAmountCalculatorUtils.getBillingInvoiceAmountConvertedForFrequency(scheduleConfig, tier.getFeePercent()), true));
		}
		note.append(StringUtils.NEW_LINE);
	}


	@Override
	public boolean isBillingBasisUsed(BillingSchedule billingSchedule) {
		return !isApplyToBillingAmountsFromPrerequisiteInvoices();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Map<InvestmentAccount, BigDecimal> getInvoiceAccountFees(BillingScheduleProcessConfig scheduleConfig, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return getInvestmentAccountValueMap(scheduleConfig, false, revenueTypeList);
	}


	private Map<InvestmentAccount, BigDecimal> getInvoiceAccountBillingBasis(BillingScheduleProcessConfig scheduleConfig, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return getInvestmentAccountValueMap(scheduleConfig, true, revenueTypeList);
	}


	private Map<InvestmentAccount, BigDecimal> getInvestmentAccountValueMap(BillingScheduleProcessConfig scheduleConfig, boolean useBillingBasis, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		Map<InvestmentAccount, BigDecimal> accountValueMap = new HashMap<>();
		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
				// As long as it's not the same schedule
				// Note: Not including Manual Invoice Details - May want to or may address that with payments?  It does affect the rev share so might want to support it here.
				if (invoiceDetail.getBillingSchedule() != null && !invoiceDetail.getBillingSchedule().equals(scheduleConfig.getSchedule())) {
					// If Revenue Types are not set, or included
					if (CollectionUtils.isEmpty(revenueTypeList) || revenueTypeList.contains(invoiceDetail.getCoalesceRevenueType())) {
						// If using an accrual frequency confirm all details also use an accrual frequency and details are for shorter or equal time periods
						if (scheduleConfig.getSchedule().getAccrualFrequency() != null) {
							ValidationUtils.assertNotNull(invoiceDetail.getBillingSchedule().getAccrualFrequency(), "Cannot apply an accrual frequency to [" + scheduleConfig.getSchedule().getName() + "] because there is at least one invoice detail for schedule [" + invoiceDetail.getBillingSchedule().getName() + "] that doesn't use accrual frequencies.");
							ValidationUtils.assertTrue(scheduleConfig.getSchedule().getAccrualFrequency().getDaysInPeriod() >= invoiceDetail.getBillingSchedule().getAccrualFrequency().getDaysInPeriod(), "Cannot apply an accrual frequency of [" + scheduleConfig.getSchedule().getAccrualFrequency().name() + "] to schedule [" + scheduleConfig.getSchedule().getName() + "] because there is at least one invoice detail for schedule [" + invoiceDetail.getBillingSchedule().getName() + "] that uses an accrual frequency of a longer period [" + invoiceDetail.getBillingSchedule().getAccrualFrequency().name() + "].  Unable to apply discount at shorter intervals.");
							// If setup passes, then include only if an overlap in dates
							if (!DateUtils.isOverlapInDates(scheduleConfig.getAccrualStartDate(), scheduleConfig.getAccrualEndDate(), invoiceDetail.getAccrualStartDate(), invoiceDetail.getAccrualEndDate())) {
								continue;
							}
						}

						for (BillingInvoiceDetailAccount invoiceDetailAccount : CollectionUtils.getIterable(invoiceDetail.getDetailAccountList())) {
							InvestmentAccount acct = invoiceDetailAccount.getInvestmentAccount() != null ? invoiceDetailAccount.getInvestmentAccount()
									: invoiceDetailAccount.getBillingDefinitionInvestmentAccount().getReferenceTwo();
							accountValueMap.put(acct, MathUtils.add(accountValueMap.get(acct), useBillingBasis ? invoiceDetailAccount.getBillingBasis() : invoiceDetailAccount.getBillingAmount()));
						}
					}
				}
			}
		}
		return accountValueMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isApplyToBillingAmounts() {
		return this.applyToBillingAmounts;
	}


	public void setApplyToBillingAmounts(boolean applyToBillingAmounts) {
		this.applyToBillingAmounts = applyToBillingAmounts;
	}


	public boolean isApplyToBillingAmountsFromPrerequisiteInvoices() {
		return this.applyToBillingAmountsFromPrerequisiteInvoices;
	}


	public void setApplyToBillingAmountsFromPrerequisiteInvoices(boolean applyToBillingAmountsFromPrerequisiteInvoices) {
		this.applyToBillingAmountsFromPrerequisiteInvoices = applyToBillingAmountsFromPrerequisiteInvoices;
	}


	public List<BillingInvoiceRevenueTypes> getApplyToBillingAmountsRevenueTypeList() {
		return this.applyToBillingAmountsRevenueTypeList;
	}


	public void setApplyToBillingAmountsRevenueTypeList(List<BillingInvoiceRevenueTypes> applyToBillingAmountsRevenueTypeList) {
		this.applyToBillingAmountsRevenueTypeList = applyToBillingAmountsRevenueTypeList;
	}


	public boolean isUseBillingBasisForTiersToCalculatePercentageFee() {
		return this.useBillingBasisForTiersToCalculatePercentageFee;
	}


	public void setUseBillingBasisForTiersToCalculatePercentageFee(boolean useBillingBasisForTiersToCalculatePercentageFee) {
		this.useBillingBasisForTiersToCalculatePercentageFee = useBillingBasisForTiersToCalculatePercentageFee;
	}
}
