package com.clifton.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisManagerMarketValueCalculator</code> calculates billing basis for a specific manager
 * as Adjusted Cash Balance, Adjusted Securities Balance, or Total Adjusted Balance
 *
 * @author manderson
 */
public class BillingBasisManagerMarketValueCalculator extends BaseBillingBasisValueCalculator {

	// Options for balance type - Values are set on System List Investment Holding Types (Note: "BOTH" is the option to include Cash and Securities)
	private static final String CASH_ONLY = "CASH";
	private static final String SECURITIES_ONLY = "SECURITIES";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private BillingDefinitionService billingDefinitionService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String balanceType;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidValuationDate(Date date) {
		return DateUtils.isWeekday(date);
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		if (moveForward) {
			return DateUtils.getNextWeekday(date);
		}
		return DateUtils.getPreviousWeekday(date);
	}


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		List<BillingBasisSnapshot> list = new ArrayList<>();
		List<InvestmentManagerAccountBalance> managerAccountBalanceList = getInvestmentManagerAccountBalanceList(billingAccount, startDate, endDate);

		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(managerAccountBalanceList)) {
			list.add(createBillingBasisSnapshot(invoice, billingAccount, balance));
		}
		return list;
	}


	private List<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		ValidationUtils.assertNotNull(billingAccount.getInvestmentManagerAccount(), "Selected manager account is required for valuation type [" + billingAccount.getBillingBasisValuationType().getName() + "]");
		InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
		searchForm.setManagerAccountId(billingAccount.getInvestmentManagerAccount().getId());
		searchForm.addSearchRestriction(new SearchRestriction("balanceDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("balanceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		return getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, InvestmentManagerAccountBalance managerAccountBalance) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSecurity(managerAccountBalance.getManagerAccount().getBaseCurrency());
		bbs.setSourceFkFieldId(managerAccountBalance.getId());
		bbs.setSnapshotDate(managerAccountBalance.getBalanceDate());

		if (StringUtils.isEqualIgnoreCase(CASH_ONLY, getBalanceType())) {
			bbs.setSnapshotValue(managerAccountBalance.getAdjustedCashValue());
		}
		else if (StringUtils.isEqualIgnoreCase(SECURITIES_ONLY, getBalanceType())) {
			bbs.setSnapshotValue(managerAccountBalance.getAdjustedSecuritiesValue());
		}
		else {
			bbs.setSnapshotValue(managerAccountBalance.getAdjustedTotalValue());
		}
		return bbs;
	}


	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		// Does Not Apply to Manager Balances
		return currentTradeDateList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public String getBalanceType() {
		return this.balanceType;
	}


	public void setBalanceType(String balanceType) {
		this.balanceType = balanceType;
	}
}
