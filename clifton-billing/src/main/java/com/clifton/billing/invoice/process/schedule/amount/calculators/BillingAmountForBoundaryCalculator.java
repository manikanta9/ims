package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleType;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingAmountForBoundaryCalculator</code> class calculates the amount to
 * account for a boundary crossing - either MinimumFee (i.e. add amount to bring invoice up to minimum charge)
 * or Maximum Fee (i.e. subtract amount that invoice is over the maximum by)
 * <p>
 * NOTE: THIS ONLY VALIDATES AGAINST THIS INVOICE PERIOD, FOR ANNUAL MINIMUM/MAXIMUMS USE THE AnnualBoundaryCalculator
 *
 * @author Mary Anderson
 */
public class BillingAmountForBoundaryCalculator extends BillingAmountForFixedFeeCalculator {


	/**
	 * Allowed for Minimum Fees Only to add a second detail line (if applicable)
	 * which adds a credit for the minimum fees based on other fees paid on other invoice(s) during the invoice period
	 * Note: Requires BillingScheduleBillingDefinitionDependency entries exist for the schedule
	 */
	private boolean applyCreditFromPrerequisiteInvoices;

	/**
	 * Which revenue types should be included in the minimum, i.e. gross revenue
	 */
	private List<BillingInvoiceRevenueTypes> revenueTypeList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		return BillingAmountCalculatorTypes.BOUNDARY;
	}


	@Override
	public void validateBillingScheduleType(BillingScheduleType scheduleType) {
		super.validateBillingScheduleType(scheduleType);
		if (isApplyCreditFromPrerequisiteInvoices()) {
			ValidationUtils.assertTrue(scheduleType.isMinimumFee(), "Applying Credit from prerequisite invoices is supported by Minimum Fees only.");
		}
	}


	@Override
	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		StringBuilder note = new StringBuilder(16);
		BigDecimal amount = calculateAmount(scheduleConfig, null, note);

		BigDecimal totalBilled = BigDecimal.ZERO;

		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			BigDecimal invoiceTotal = BillingAmountCalculatorUtils.getBillingInvoiceTotalAmountCalculated(invoice, scheduleConfig.getSchedule(), getRevenueTypeList());
			totalBilled = MathUtils.add(totalBilled, invoiceTotal);
		}

		BigDecimal billingAmount = BigDecimal.ZERO;
		boolean min = isMinimum(scheduleConfig.getSchedule());

		if ((min && MathUtils.isLessThan(totalBilled, amount)) || (!min && MathUtils.isGreaterThan(totalBilled, amount))) {
			billingAmount = MathUtils.subtract(amount, totalBilled);
		}

		appendBoundaryNote(scheduleConfig, min, note, amount, totalBilled);

		if (MathUtils.isNullOrZero(billingAmount)) {
			addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
		}
		else {
			applyBillingAmountToInvoices(config, scheduleConfig, billingAmount, totalBilled, note);
			// Only apply credit if billing amount was non-zero
			if (isApplyCreditFromPrerequisiteInvoices()) {
				note = new StringBuilder(16);

				BigDecimal otherPaidFees = CoreMathUtils.sum(CollectionUtils.getValues(getPrerequisiteInvoiceAccountFees(scheduleConfig, getRevenueTypeList())));

				if (MathUtils.isNullOrZero(otherPaidFees)) {
					// Not waiving the schedule, but the credit is 0.  Add it to the processing violations, not waived
					addProcessingBillingScheduleWarning(config, scheduleConfig, "Minimum Fee Credit does not apply. No other fees paid to credit for during invoice period.");
				}
				else {
					BigDecimal creditAmount;
					if (MathUtils.isGreaterThanOrEqual(billingAmount, otherPaidFees)) {
						creditAmount = MathUtils.negate(otherPaidFees);
					}
					else {
						creditAmount = MathUtils.negate(billingAmount);
					}

					// If there is a maximum credit allowed and we go over - give the maximum credit and add a violation for the actual calculated amount
					BigDecimal maxCredit = scheduleConfig.getSchedule().getSecondaryScheduleAmount();
					if (maxCredit != null && MathUtils.isGreaterThan(MathUtils.abs(creditAmount), maxCredit)) {
						addProcessingBillingScheduleWarning(config, scheduleConfig, "Calculated credit was originally [" + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, MathUtils.abs(creditAmount), false) + "].  Credit applied was the maximum allowed of [" + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, maxCredit, false) + "].");
						creditAmount = MathUtils.negate(maxCredit);
					}

					note.append("Credit for Other Management Fees Paid ").append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, MathUtils.abs(creditAmount), false));
					applyBillingAmountToInvoices(config, scheduleConfig, creditAmount, totalBilled, note);
				}
			}
		}
	}


	@Override
	public boolean isBillingBasisUsed(@SuppressWarnings("unused") BillingSchedule schedule) {
		return false;
	}


	protected boolean isMinimum(BillingSchedule schedule) {
		if (schedule.getScheduleType().isMinimumFee()) {
			return true;
		}
		if (!schedule.getScheduleType().isMaximumFee()) {
			throw new ValidationException("Schedule Type [" + schedule.getScheduleType() + "] is missing minimum or maximum fee selection.");
		}
		return false;
	}


	protected void applyBillingAmountToInvoices(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig, BigDecimal billingAmount, BigDecimal totalBilled, StringBuilder note) {
		List<BillingInvoiceDetail> detailList = new ArrayList<>();
		if (MathUtils.isNullOrZero(totalBilled)) {
			for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
				BigDecimal allocationAmount = MathUtils.divide(billingAmount, scheduleConfig.getInvoiceList().size());
				BigDecimal allocationPercentage = CoreMathUtils.getPercentValue(allocationAmount, billingAmount, true);

				String invoiceDetailNote = note.toString();

				if (!MathUtils.isNullOrZero(allocationPercentage) && !MathUtils.isEqual(allocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
					invoiceDetailNote += StringUtils.NEW_LINE + "Allocated for " + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, allocationPercentage, true)
							+ " of total fees";
				}
				BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, null, allocationAmount, invoiceDetailNote);
				detail.setDetailAccountList(getBillingInvoiceDetailAccountCalculator().calculateBillingInvoiceDetailAccountList(invoice, detail, allocationPercentage, null, null,
						scheduleConfig.getProportionalSplitAccountList(invoice.getId())));
				detailList.add(detail);
			}
		}
		else {
			for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
				BigDecimal invoiceTotal = BillingAmountCalculatorUtils.getBillingInvoiceTotalAmountCalculated(invoice, scheduleConfig.getSchedule(), getRevenueTypeList());
				BigDecimal allocationPercentage = MathUtils.round(CoreMathUtils.getPercentValue(invoiceTotal, totalBilled, true), 2);
				BigDecimal allocationAmount = MathUtils.getPercentageOf(allocationPercentage, billingAmount, true);

				Map<InvestmentAccount, BigDecimal> accountAllocationMap = new HashMap<>();
				String invoiceDetailNote = note.toString();

				if (!MathUtils.isNullOrZero(allocationPercentage) && !MathUtils.isEqual(allocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
					invoiceDetailNote += StringUtils.NEW_LINE + "Allocated for " + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, allocationPercentage, true)
							+ " of total fees";
				}
				BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, null, allocationAmount, invoiceDetailNote);
				if (!CollectionUtils.isEmpty(invoice.getDetailList())) {
					for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
						// As long as it's not the same schedule - include the amount
						// Check for Billing Schedule Not Null - If it's null then it is a manual invoice adjustment
						if (invoiceDetail.getBillingSchedule() != null && !invoiceDetail.getBillingSchedule().equals(scheduleConfig.getSchedule())) {
							for (BillingInvoiceDetailAccount invoiceDetailAccount : CollectionUtils.getIterable(invoiceDetail.getDetailAccountList())) {
								InvestmentAccount acct = invoiceDetailAccount.getInvestmentAccount() != null ? invoiceDetailAccount.getInvestmentAccount()
										: invoiceDetailAccount.getBillingDefinitionInvestmentAccount().getReferenceTwo();
								accountAllocationMap.put(acct, MathUtils.add(accountAllocationMap.get(acct), invoiceDetailAccount.getBillingAmount()));
							}
						}
					}
					createInvoiceDetailAccountListFromPreviouslyBilledAllocations(detail, accountAllocationMap);
				}
				else {
					detail.setDetailAccountList(getBillingInvoiceDetailAccountCalculator().calculateBillingInvoiceDetailAccountList(invoice, detail, allocationPercentage, null, null,
							scheduleConfig.getProportionalSplitAccountList(invoice.getId())));
				}
				detailList.add(detail);
			}
		}
		CoreMathUtils.applySumPropertyDifference(detailList, BillingInvoiceDetail::getBillingAmount, BillingInvoiceDetail::setBillingAmount, BillingAmountCalculatorUtils.roundBillingAmount(scheduleConfig, billingAmount, false), true);
		for (BillingInvoiceDetail detail : CollectionUtils.getIterable(detailList)) {
			CoreMathUtils.applySumPropertyDifference(detail.getDetailAccountList(), BillingInvoiceDetailAccount::getBillingAmount, BillingInvoiceDetailAccount::setBillingAmount, detail.getBillingAmount(), true);
			config.addInvoiceDetail(detail);
		}
	}


	protected void appendBoundaryNote(BillingScheduleProcessConfig scheduleConfig, boolean minimum, StringBuilder note, BigDecimal amount, BigDecimal totalBilled) {
		note.append(StringUtils.TAB).append(minimum ? "Minimum" : "Maximum").append(" Fee: ");
		note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, amount, false));
		note.append(StringUtils.TAB + "Billed: ").append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, totalBilled, false));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isApplyCreditFromPrerequisiteInvoices() {
		return this.applyCreditFromPrerequisiteInvoices;
	}


	public void setApplyCreditFromPrerequisiteInvoices(boolean applyCreditFromPrerequisiteInvoices) {
		this.applyCreditFromPrerequisiteInvoices = applyCreditFromPrerequisiteInvoices;
	}


	public List<BillingInvoiceRevenueTypes> getRevenueTypeList() {
		return this.revenueTypeList;
	}


	public void setRevenueTypeList(List<BillingInvoiceRevenueTypes> revenueTypeList) {
		this.revenueTypeList = revenueTypeList;
	}
}
