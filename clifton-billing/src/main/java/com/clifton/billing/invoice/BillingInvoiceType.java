package com.clifton.billing.invoice;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>BillingInvoiceType</code> class defines the different invoice types that we have in the system.
 * For example: Broker Fee, Legal Fee, Management Fee, Revenue Share, etc.
 * Those types that do not use a billing definition are generally entered manually as it could be a one-off reimbursement type invoice.
 *
 * @author manderson
 */
@CacheByName
public class BillingInvoiceType extends NamedEntity<Short> {


	/**
	 * If the invoice type is revenue or not.  Non-revenue type invoices are generally fees we pay on behalf of the client and then look for reimbursement (i.e. legal fees or vendor fees).
	 */
	private boolean revenue;


	/**
	 * If the invoice type can be used by billing definitions
	 */
	private boolean billingDefinitionRequired;

	/**
	 * Drives the default invoice posted option for billing definitions
	 */
	private boolean invoicePostedToPortalDefault;


	/**
	 * System Defined invoice types cannot be added or deleted from UI, and their names cannot change.
	 */
	private boolean systemDefined;


	/**
	 * For those invoice types that use a billing definition
	 * Defines how each billing amount is rounded. Two use cases: 0 (rounded to nearest dollar) - MPLS invoicing, 2 (rounded to nearest penny) - Westport invoicing
	 * If there are any cases where a shared schedule applies to 2 invoice types, the least precision wins. (i.e. rounded to nearest dollar)
	 * Allowed value range is 0 - 2
	 */
	private Short billingAmountPrecision;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRevenue() {
		return this.revenue;
	}


	public void setRevenue(boolean revenue) {
		this.revenue = revenue;
	}


	public boolean isBillingDefinitionRequired() {
		return this.billingDefinitionRequired;
	}


	public void setBillingDefinitionRequired(boolean billingDefinitionRequired) {
		this.billingDefinitionRequired = billingDefinitionRequired;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Short getBillingAmountPrecision() {
		return this.billingAmountPrecision;
	}


	public void setBillingAmountPrecision(Short billingAmountPrecision) {
		this.billingAmountPrecision = billingAmountPrecision;
	}


	public boolean isInvoicePostedToPortalDefault() {
		return this.invoicePostedToPortalDefault;
	}


	public void setInvoicePostedToPortalDefault(boolean invoicePostedToPortalDefault) {
		this.invoicePostedToPortalDefault = invoicePostedToPortalDefault;
	}
}
