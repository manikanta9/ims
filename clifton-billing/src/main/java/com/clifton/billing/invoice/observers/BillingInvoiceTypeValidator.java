package com.clifton.billing.invoice.observers;


import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingInvoiceTypeValidator</code> validates inserts/updates/deletes to invoice types that require billing definitions or are name system defined
 *
 * @author manderson
 */
@Component
public class BillingInvoiceTypeValidator extends SelfRegisteringDaoValidator<BillingInvoiceType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BillingInvoiceType bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Invoice Types is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Invoice Types is not allowed.");
			}
		}
		if (config.isUpdate()) {
			BillingInvoiceType originalBean = getOriginalBean(bean);
			if (originalBean.isBillingDefinitionRequired() != bean.isBillingDefinitionRequired()) {
				throw new FieldValidationException("You cannot edit the Billing Definition Required field for Invoice Types", "billingDefinitionRequired");
			}

			if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
				throw new ValidationException("You cannot change the System Defined field of the invoice type.");
			}
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "System Defined Invoice Type Names cannot be changed.", "name");
			}
		}
		if (config.isInsert() || config.isUpdate()) {
			if (bean.isBillingDefinitionRequired()) {
				ValidationUtils.assertNotNull(bean.getBillingAmountPrecision(), "Billing Amount Precision is required for invoice types that use billing definitions.  Allowed values are 0 - 2.");
				ValidationUtils.assertTrue(MathUtils.isBetween(bean.getBillingAmountPrecision(), 0, 2), "Billing Amount Precision allowed values are 0 - 2.");
			}
			else {
				bean.setBillingAmountPrecision(null);
			}
		}
	}
}
