package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BillingAmountForDiscountCalculator</code> class calculates the amount to
 * discount on the invoice.  Fees are calculated based on what has been previously charged on the invoice.
 * i.e. 10% discount
 * <p>
 *
 * @author Mary Anderson
 */
public class BillingAmountForDiscountCalculator extends BaseBillingAmountCalculator {

	@Override
	public BillingAmountCalculatorTypes getBillingAmountCalculatorType() {
		return BillingAmountCalculatorTypes.FLAT_PERCENTAGE;
	}


	@Override
	public BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, @SuppressWarnings("unused") BillingBasisDetail billingBasisDetail, StringBuilder note) {
		// Convert % Discount to Negative Amount (entered as positive)
		BigDecimal percentage = MathUtils.negate(scheduleConfig.getSchedule().getScheduleAmount());
		note.append(BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, scheduleConfig.getSchedule().getScheduleAmount(), true));
		return percentage;
	}


	@Override
	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		StringBuilder percentageNote = new StringBuilder(16);
		BigDecimal percentage = calculateAmount(scheduleConfig, null, percentageNote);

		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			BigDecimal invoiceTotal = BigDecimal.ZERO;
			Map<InvestmentAccount, BigDecimal> accountAllocationMap = new HashMap<>();
			String totalLabel = scheduleConfig.getSchedule().getAccrualFrequency() != null ? scheduleConfig.getSchedule().getAccrualFrequency().getDateRangeLabel(scheduleConfig.getAccrualStartDate(), scheduleConfig.getAccrualEndDate()) : "total";

			for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
				// As long as it's not the same schedule (and not manual) - include the amount
				if (invoiceDetail.getBillingSchedule() != null && !invoiceDetail.getBillingSchedule().equals(scheduleConfig.getSchedule())) {
					// If using an accrual frequency confirm all details also use an accrual frequency and details are for shorter or equal time periods
					if (scheduleConfig.getSchedule().getAccrualFrequency() != null) {
						ValidationUtils.assertNotNull(invoiceDetail.getBillingSchedule().getAccrualFrequency(), "Cannot apply an accrual frequency to [" + scheduleConfig.getSchedule().getName() + "] because there is at least one invoice detail for schedule [" + invoiceDetail.getBillingSchedule().getName() + "] that doesn't use accrual frequencies.");
						ValidationUtils.assertTrue(scheduleConfig.getSchedule().getAccrualFrequency().getDaysInPeriod() >= invoiceDetail.getBillingSchedule().getAccrualFrequency().getDaysInPeriod(), "Cannot apply an accrual frequency of [" + scheduleConfig.getSchedule().getAccrualFrequency().name() + "] to schedule [" + scheduleConfig.getSchedule().getName() + "] because there is at least one invoice detail for schedule [" + invoiceDetail.getBillingSchedule().getName() + "] that uses an accrual frequency of a longer period [" + invoiceDetail.getBillingSchedule().getAccrualFrequency().name() + "].  Unable to apply discount at shorter intervals.");
						// If setup passes, then include only if an overlap in dates
						if (!DateUtils.isOverlapInDates(scheduleConfig.getAccrualStartDate(), scheduleConfig.getAccrualEndDate(), invoiceDetail.getAccrualStartDate(), invoiceDetail.getAccrualEndDate())) {
							continue;
						}
					}
					invoiceTotal = MathUtils.add(invoiceTotal, invoiceDetail.getBillingAmount());
					for (BillingInvoiceDetailAccount invoiceDetailAccount : CollectionUtils.getIterable(invoiceDetail.getDetailAccountList())) {
						InvestmentAccount acct = invoiceDetailAccount.getInvestmentAccount() != null ? invoiceDetailAccount.getInvestmentAccount()
								: invoiceDetailAccount.getBillingDefinitionInvestmentAccount().getReferenceTwo();
						accountAllocationMap.put(acct, MathUtils.add(accountAllocationMap.get(acct), invoiceDetailAccount.getBillingAmount()));
					}
				}
			}
			BigDecimal amount = MathUtils.getPercentageOf(percentage, invoiceTotal, true);
			amount = BillingAmountCalculatorUtils.roundBillingAmount(scheduleConfig, amount, false);

			String note = percentageNote + " off of " + totalLabel + " invoice amount: " + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, invoiceTotal, false);
			if (MathUtils.isNullOrZero(amount)) {
				config.setInvoiceScheduleWaivedViolation(invoice.getId(), scheduleConfig.getSchedule(), note);
			}
			else {
				BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, null, amount, note);
				createInvoiceDetailAccountListFromPreviouslyBilledAllocations(detail, accountAllocationMap);
				config.addInvoiceDetail(detail);
			}
		}
	}


	@Override
	public boolean isBillingBasisUsed(@SuppressWarnings("unused") BillingSchedule schedule) {
		return false;
	}
}
