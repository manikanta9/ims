package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>BillingInvoiceValidateGroupPaidWorkflowAction</code> is called when invoices that are part of a group
 * are moved to "Paid" state.  This action checks all invoices in the group, if all fully paid, then
 * moves the group to Paid.
 *
 * @author manderson
 */
public class BillingInvoiceValidateGroupPaidWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {

	private BillingInvoiceService billingInvoiceService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice bean, WorkflowTransition transition) {
		if (bean.getInvoiceGroup() != null) {
			BillingInvoiceGroup group = getBillingInvoiceService().getBillingInvoiceGroup(bean.getInvoiceGroup().getId());
			WorkflowState minState = null;
			for (BillingInvoice invoice : group.getInvoiceList()) {
				if (minState == null) {
					minState = invoice.getWorkflowState();
				}
				if (MathUtils.isLessThan(invoice.getWorkflowState().getOrder(), minState.getOrder())) {
					minState = invoice.getWorkflowState();
				}
			}

			if (minState != null && !group.getWorkflowState().getName().equalsIgnoreCase(minState.getName())) {
				WorkflowState nextState = getNextWorkflowState(group, minState);
				if (nextState != null) {
					getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, group.getId(), nextState.getId());
				}
			}
		}
		return bean;
	}


	private WorkflowState getNextWorkflowState(BillingInvoiceGroup group, WorkflowState state) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, BeanUtils.getIdentityAsLong(group));
		for (WorkflowTransition trans : CollectionUtils.getIterable(transitionList)) {
			if (trans.isSystemDefined()) {
				if (state.getName().equalsIgnoreCase(trans.getEndWorkflowState().getName())) {
					return trans.getEndWorkflowState();
				}
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
