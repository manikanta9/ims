package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisPeriodMonthEndAverageCalculator</code> supports calculation types for Period Month End Average
 * This is different from period average in that we only use the ending billing basis values for each month and is useful for cases where we may not have daily values (i.e. final AUM we store only for month end)
 *
 * @author Mary Anderson
 */
public class BillingBasisPeriodMonthEndAverageCalculator extends BaseBillingBasisCalculator {


	/**
	 * Currently only used for AUM (which is converted to External and External billing basis) - so these are switched to use the period average projected calculator
	 */
	@Override
	public boolean isProjectedRevenueSupported() {
		return false;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		// If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
		// Uses Datasource for the M2M account relationship issuing company, else default.
		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);

		// Get all values for the date range
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		List<BillingBasisSnapshot> snapshotList = new ArrayList<>();
		List<Date> accountDateList = getPeriodMonthEndLookupDates(getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart),
				getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd));

		for (Date date : CollectionUtils.getIterable(accountDateList)) {
			Date originalDate = date;
			if (!calculator.isValidValuationDate(date)) {
				date = calculator.getNextValuationDate(date, false);
			}
			List<BillingBasisSnapshot> list = calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), date, date);
			if (!CollectionUtils.isEmpty(list)) {
				// DataSourceName will only be populated if we need to lookup exchange rate
				if (!StringUtils.isEmpty(dataSourceName)) {
					BigDecimal exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName,
							billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), originalDate).flexibleLookup());
					for (BillingBasisSnapshot sn : CollectionUtils.getIterable(list)) {
						// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
						// If missing only, then use system determined rate
						if (MathUtils.isNullOrZero(sn.getFxRate())) {
							sn.setFxRate(exchangeRate);
						}
					}
				}
				snapshotList.addAll(list);
			}
			else {
				snapshotList.add(new BillingBasisSnapshot(invoice, billingDefinitionInvestmentAccount, originalDate));
			}
		}
		return snapshotList;
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		// Get values
		List<BillingBasisSnapshot> valueList = getBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, startDate, endDate);
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		getBillingBasisService().saveBillingBasisSnapshotList(valueList);

		Date accountStart = getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, startDate);
		Date accountEnd = getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, endDate);
		boolean prorateAccount = false;
		if ((DateUtils.compare(accountStart, startDate, false) > 0) || DateUtils.compare(accountEnd, endDate, false) < 0) {
			prorateAccount = true;
		}

		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		List<Date> dateList = getPeriodMonthEndLookupDates(startDate, endDate);
		List<BigDecimal> monthValueList = new ArrayList<>();
		for (Date date : CollectionUtils.getIterable(dateList)) {
			if (!calculator.isValidValuationDate(date)) {
				date = calculator.getNextValuationDate(date, false);
			}
			List<BillingBasisSnapshot> dateFilteredList = filterBillingBasisSnapshotListByDate(valueList, date);
			BigDecimal monthValue = CoreMathUtils.sumProperty(dateFilteredList, BillingBasisSnapshot::getSnapshotValue);

			// Account Billing Basis Started/Ended Mid Month - prorate it
			BigDecimal prorateDays = null;
			BigDecimal totalDays = null;
			if (prorateAccount) {
				Date monthStart = DateUtils.getFirstDayOfMonth(date);
				Date monthEnd = DateUtils.getLastDayOfMonth(date);

				// Don't do inclusive date between - if account start - month start or account end - month end - don't prorate
				if (DateUtils.isDateBetween(accountStart, DateUtils.addDays(monthStart, 1), monthEnd, false)) {
					if (DateUtils.isDateBetween(accountEnd, monthStart, DateUtils.addDays(monthEnd, -1), false)) {
						prorateDays = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(accountEnd, accountStart));
					}
					else {
						prorateDays = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(monthEnd, accountStart));
					}
				}
				else if (DateUtils.isDateBetween(accountEnd, monthStart, DateUtils.addDays(monthEnd, -1), false)) {
					prorateDays = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(accountEnd, monthStart));
				}
				if (prorateDays != null) {
					totalDays = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(monthEnd, monthStart));
				}
			}

			if (prorateDays != null) {
				monthValueList.add(CoreMathUtils.getValueProrated(prorateDays, totalDays, monthValue));
			}
			else {
				monthValueList.add(monthValue);
			}
		}

		// Use the month count we are averaging - amounts will still be prorated based on number of days for start/end date if necessary
		BigDecimal average = MathUtils.divide(CoreMathUtils.sum(monthValueList), CollectionUtils.getSize(monthValueList));
		//System.out.println("Billing Basis calculated for account " + billingDefinitionInvestmentAccount.getReferenceTwo().getNumber() + " as " + CoreMathUtils.formatNumberInteger(average));
		return new BillingBasis(startDate, endDate, average);
	}


	private List<Date> getPeriodMonthEndLookupDates(Date startDate, Date endDate) {
		Date monthEndDate = DateUtils.getLastDayOfMonth(startDate);
		if (DateUtils.compare(startDate, monthEndDate, false) > 0) {
			monthEndDate = DateUtils.getLastDayOfMonth(startDate);
		}
		List<Date> dateList = new ArrayList<>();
		while (DateUtils.isDateBetween(monthEndDate, startDate, endDate, false)) {
			Date monthStartDate = DateUtils.getFirstDayOfMonth(monthEndDate);
			if (DateUtils.isDateBetween(endDate, monthStartDate, monthEndDate, false)) {
				dateList.add(endDate);
				break;
			}
			dateList.add(monthEndDate);
			monthEndDate = DateUtils.getLastDayOfMonth(DateUtils.addDays(monthEndDate, 1));
		}
		return dateList;
	}
}
