package com.clifton.billing.invoice;


import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceDetailSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceTypeSearchForm;
import com.clifton.billing.invoice.workflow.BillingInvoiceGenerateRevisionWorkflowAction;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.math.BigDecimal;
import java.util.List;


public interface BillingInvoiceService {

	////////////////////////////////////////////////////////////////////////////
	////////             Billing Invoice Type Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceType getBillingInvoiceType(short id);


	public BillingInvoiceType getBillingInvoiceTypeByName(String name);


	public List<BillingInvoiceType> getBillingInvoiceTypeList(BillingInvoiceTypeSearchForm searchForm);


	public BillingInvoiceType saveBillingInvoiceType(BillingInvoiceType bean);


	public void deleteBillingInvoiceType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////////              Billing Invoice Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoice getBillingInvoice(int id);


	/**
	 * Also populates the detailList
	 */
	@DoNotAddRequestMapping
	public BillingInvoice getBillingInvoicePopulated(int id);


	public List<BillingInvoice> getBillingInvoiceList(final BillingInvoiceSearchForm searchForm);


	/**
	 * Also saves the detail list - so should be on the invoice bean so it doesn't get deleted
	 */
	public BillingInvoice saveBillingInvoice(BillingInvoice bean);


	/**
	 * Special method called from BillingPaymentAllocationObserver that ONLY updates the paid amount
	 * Cannot use general save method because we aren't touching the detail lines for the invoice at this point.
	 */
	@DoNotAddRequestMapping
	public void saveBillingInvoicePaidAmount(BillingInvoice bean, BigDecimal paidAmount);


	/**
	 * Special method called from {@link BillingInvoiceGenerateRevisionWorkflowAction} and {@link com.clifton.billing.invoice.workflow.BillingInvoiceGroupGenerateRevisionWorkflowAction}
	 * that ONLY updates the parent field of the invoice
	 * Cannot use general save method because we aren't touching the detail lines for the invoice at this point.
	 */
	@DoNotAddRequestMapping
	public void saveBillingInvoiceParent(BillingInvoice bean, BillingInvoice parentBean);


	public void deleteBillingInvoice(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////             Billing Invoice Detail Methods             ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * NOTE: THE FOLLOWING METHODS ARE EXPOSED VIA UI TO ALLOW BILLING ADMINS
	 * TO ENTER "MANUAL" DETAIL LINES
	 */

	public BillingInvoiceDetail getBillingInvoiceDetail(int id);


	public List<BillingInvoiceDetail> getBillingInvoiceDetailList(BillingInvoiceDetailSearchForm searchForm);


	/**
	 * @param forceAllowPrecision - Is used by manual invoice detail lines only when making account adjustments to offset unpaid balance
	 *                            - allows even though the invoice may not use pennies, the payment might and we need to offset that
	 */
	public BillingInvoiceDetail saveBillingInvoiceDetail(BillingInvoiceDetail bean, Integer forceAllowPrecision);


	public void deleteBillingInvoiceDetail(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////         Billing Invoice Detail Account Methods         ///////////
	////////////////////////////////////////////////////////////////////////////


	public List<BillingInvoiceDetailAccount> getBillingInvoiceDetailAccountListByInvoiceDetail(int invoiceDetailId);


	public List<BillingInvoiceDetailAccount> getBillingInvoiceDetailAccountList(BillingInvoiceDetailAccountSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	/////////              Billing Invoice Group Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceGroup getBillingInvoiceGroup(int id);


	public BillingInvoiceGroup saveBillingInvoiceGroup(BillingInvoiceGroup bean);


	public void deleteBillingInvoiceGroup(int id);
}
