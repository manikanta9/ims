package com.clifton.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;

import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisValueCalculator</code> interface defines the methods used to generate the {@list BillingBasisSnapshot} list that contains the results of how the valuation is calculated.
 * For example, if the valuation type is Notional, then the value calculator returns a list of snapshot records for each position and date and their notional value.
 *
 * @author Mary Anderson
 */
public interface BillingBasisValueCalculator {


	/**
	 * Returns the next valid valuation date
	 * Valid dates can depend on where the values come from.  For example, client snapshots (accounting position daily) would be any weekday and month end,
	 * synthetic exposure would just be weekdays, external can be any date.
	 */
	public Date getNextValuationDate(Date date, boolean moveForward);


	/**
	 * Returns true if the given date is a valid date to pull values for.
	 * Valid dates can depend on where the values come from.  For example, snapshots would be any weekday and month end,
	 * synthetic exposure would just be weekdays, external can be any date.
	 */
	public boolean isValidValuationDate(Date date);


	/**
	 * Calculates and returns the BillingBasisSnapshot list of calculated billing basis values for each entity and each date.  These records are saved and associated with the invoice
	 * for research/reporting on the invoice billing details.  These snapshots are then used by the BillingBasisCalculator to calculate the billing basis for each account used on the invoice.
	 */
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate);


	/**
	 * Some Billing Basis calculations utilize trade dates to break out the billing into sub-periods.  Because we need to keep that breakdown consistent across all accounts
	 * on the invoice, we need the list of trade dates to compile so we know the breakdown to use for all.
	 */
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList);
}
