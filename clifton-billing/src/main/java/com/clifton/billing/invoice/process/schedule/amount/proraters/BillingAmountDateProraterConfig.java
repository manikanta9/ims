package com.clifton.billing.invoice.process.schedule.amount.proraters;

import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingAmountDateProraterConfig</code> is used to define dates and calculation types for prorating.
 * This object is populated by the individual prorater calculator to populate dates and details, and then
 * used to calculate the prorated value
 *
 * @author manderson
 */
public class BillingAmountDateProraterConfig {

	/**
	 * If false prorates for the number of days
	 * If true prorates for the number of months
	 * i.e. 1 month could have 31 days - so for a quarter using days the calculation would be 31/number of days in the quarter
	 * and uses months would be 1/3 (# of months in the quarter)
	 */
	private final boolean prorateForMonths;

	/**
	 * When prorating for months and dates are for a partial month - i.e. 1 month + 15 days
	 * can either charge for full 2 months (2/3), or 1 month and 15 days (1/3) + ((1/3) + 15/days in month)
	 */
	private final boolean prorateForPartialMonths;


	private Date prorateStartDate;
	private Date prorateEndDate;

	// In some instances (Prorating for positions on days) we can't calculate one start/end date and use days calculation only
	private BigDecimal prorateDays;

	private Date startDate;
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	BillingAmountDateProraterConfig(BillingScheduleProcessConfig scheduleConfig, boolean prorateForMonths, boolean prorateForPartialMonths) {
		this.prorateForMonths = prorateForMonths;
		this.prorateForPartialMonths = prorateForPartialMonths;

		if (scheduleConfig.getSchedule().getScheduleType().isAnnualFee()) {
			// Go 1 year back from the end date
			this.startDate = DateUtils.addYears(DateUtils.addDays(scheduleConfig.getAnnualAbsoluteInvoicePeriodEndDate(), 1), -1);
			this.endDate = scheduleConfig.getAnnualAbsoluteInvoicePeriodEndDate();
		}
		else {
			this.startDate = ObjectUtils.coalesce(scheduleConfig.getAccrualStartDate(), scheduleConfig.getInvoicePeriodStartDate());
			this.endDate = ObjectUtils.coalesce(scheduleConfig.getAccrualEndDate(), scheduleConfig.getInvoicePeriodEndDate());
		}
	}


	BillingAmountDateProraterConfig(Date startDate, Date endDate, boolean prorateForMonths, boolean prorateForPartialMonths) {
		this.prorateForMonths = prorateForMonths;
		this.prorateForPartialMonths = prorateForPartialMonths;
		this.startDate = startDate;
		this.endDate = endDate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal prorateAmount(BigDecimal amount, StringBuilder note) {
		int billedMonths = 0;
		int totalMonths = 0;

		BigDecimal billedDays = getProrateDays() != null ? getProrateDays() : BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(getProrateEndDate(), getProrateStartDate()));
		BigDecimal totalDays = BigDecimal.valueOf(DateUtils.getDaysDifferenceInclusive(getEndDate(), getStartDate()));

		if (MathUtils.isLessThan(billedDays, totalDays)) {
			// If monthly and billed days is not zero
			if (isProrateForMonths() && !MathUtils.isNullOrZero(billedDays)) {
				BigDecimal billedMonthlyDays = BigDecimal.ZERO;
				BigDecimal totalMonthlyDays = BigDecimal.ZERO;
				int partialMonthCount = 0;
				if (isProrateForPartialMonths()) {
					Date monthStartDate = getStartDate();
					while (DateUtils.isDateBetween(monthStartDate, getStartDate(), getEndDate(), false)) {
						totalMonths++;
						Date monthEndDate = DateUtils.getLastDayOfMonth(monthStartDate);
						// First Determine if Month is included
						if (!(DateUtils.isDateAfter(getProrateStartDate(), monthEndDate) || DateUtils.isDateAfter(monthStartDate, getProrateEndDate()))) {
							Date prorateMonthStartDate = (DateUtils.isDateAfter(getProrateStartDate(), monthStartDate) ? getProrateStartDate() : monthStartDate);
							Date prorateMonthEndDate = (DateUtils.isDateAfter(getProrateEndDate(), monthEndDate) ? monthEndDate : getProrateEndDate());

							int thisMonthTotalDays = DateUtils.getDaysDifferenceInclusive(monthEndDate, monthStartDate);
							int thisMonthBilledDays = DateUtils.getDaysDifferenceInclusive(prorateMonthEndDate, prorateMonthStartDate);
							if (thisMonthBilledDays < thisMonthTotalDays) {
								billedMonthlyDays = MathUtils.add(billedMonthlyDays, thisMonthBilledDays);
								totalMonthlyDays = MathUtils.add(totalMonthlyDays, thisMonthTotalDays);
								partialMonthCount++;
							}
							else {
								billedMonths++;
							}
						}
						monthStartDate = DateUtils.addDays(monthEndDate, 1);
					}
					// More than one partial month - just do standard daily prorating
					if (partialMonthCount > 1) {
						// Clear Months
						billedMonths = 0;
						totalMonths = 0;

						amount = CoreMathUtils.getValueProrated(billedDays, totalDays, amount);
					}
					else {
						billedDays = billedMonthlyDays;
						totalDays = totalMonthlyDays;

						BigDecimal monthlyAmount = CoreMathUtils.getValueProrated(BigDecimal.valueOf(billedMonths), BigDecimal.valueOf(totalMonths), amount);
						BigDecimal dailyAmount = (MathUtils.isNullOrZero(billedDays) ? BigDecimal.ZERO : CoreMathUtils.getValueProrated(billedDays, totalDays, CoreMathUtils.getValueProrated(BigDecimal.ONE, BigDecimal.valueOf(totalMonths), amount)));
						amount = MathUtils.add(monthlyAmount, dailyAmount);
					}
				}
				else {
					billedMonths = DateUtils.getMonthsDifference(getProrateEndDate(), getProrateStartDate()) + 1;
					totalMonths = DateUtils.getMonthsDifference(getEndDate(), getStartDate()) + 1;

					// Clear Days
					billedDays = null;
					totalDays = null;

					amount = CoreMathUtils.getValueProrated(BigDecimal.valueOf(billedMonths), BigDecimal.valueOf(totalMonths), amount);
				}
			}
			// Daily
			else {
				amount = CoreMathUtils.getValueProrated(billedDays, totalDays, amount);
			}
			BillingAmountCalculatorUtils.addBillingInvoiceProratedNote(note, BigDecimal.valueOf(billedMonths), BigDecimal.valueOf(totalMonths), billedDays, totalDays);
		}
		return amount;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isProrateForMonths() {
		return this.prorateForMonths;
	}


	public boolean isProrateForPartialMonths() {
		return this.prorateForPartialMonths;
	}


	public Date getProrateStartDate() {
		return this.prorateStartDate;
	}


	public void setProrateStartDate(Date prorateStartDate) {
		this.prorateStartDate = prorateStartDate;
	}


	public Date getProrateEndDate() {
		return this.prorateEndDate;
	}


	public void setProrateEndDate(Date prorateEndDate) {
		this.prorateEndDate = prorateEndDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getProrateDays() {
		return this.prorateDays;
	}


	public void setProrateDays(BigDecimal prorateDays) {
		this.prorateDays = prorateDays;
	}
}


