package com.clifton.billing.invoice.observers;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingInvoiceObserver</code> ...
 * <p>
 * Validation (INSERT OR UPDATE)
 * - Invoice Type is populated and if Billing Definition is Required that is also populated (used to keep users from manually creating invoices that are associated with billing definitions)
 * - When Parent is populated (i.e. the child invoice is a correction of the original invoice) the parent/child are for the same invoice period/date/client/billing definition, etc.
 * <p>
 * Observer Updates (UPDATES ONLY)
 * - If Invoice is part of a Group - check Group RuleViolationStatus and update if necessary
 *
 * @author Mary Anderson
 */
@Component
public class BillingInvoiceObserver extends SelfRegisteringDaoObserver<BillingInvoice> {

	private BillingInvoiceService billingInvoiceService;
	private UpdatableDAO<BillingInvoiceGroup> billingInvoiceGroupDAO;


	public BillingInvoiceObserver() {
		super();
		// Setting order to below default so this Observer runs before those at default.
		setOrder(-100);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<BillingInvoice> dao, DaoEventTypes event, BillingInvoice bean) {
		if (event.isUpdate() && isViolationStatusUpdateRequired(bean)) {
			// Get original bean so we have it for later
			getOriginalBean(dao, bean);
		}

		validateBillingInvoice(event, bean);
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<BillingInvoice> dao, DaoEventTypes event, BillingInvoice bean, Throwable e) {
		if (e == null) {
			// if is an update and invoice is part of an invoice group checks violation status if it needs to be updated on the group
			if (event.isUpdate() && isViolationStatusUpdateRequired(bean)) {
				BillingInvoice original = getOriginalBean(dao, bean);
				updateBillingInvoiceGroupViolationStatus(original, bean);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                      Helper Methods                      /////////
	////////////////////////////////////////////////////////////////////////////


	private void validateBillingInvoice(DaoEventTypes event, BillingInvoice bean) {
		if (event.isInsert() || event.isUpdate()) {
			ValidationUtils.assertNotNull(bean.getInvoiceType(), "Invoice Type is required");
			if (bean.getInvoiceType().isBillingDefinitionRequired()) {
				ValidationUtils.assertNotNull(bean.getBillingDefinition(), "Invoice Type [" + bean.getInvoiceType().getName() + "] requires a billing definition.");
			}
			else {
				ValidationUtils.assertNull(bean.getBillingDefinition(), "Invoice Type [" + bean.getInvoiceType().getName() + "] does not use a billing definition.");
			}
			if (bean.getParent() != null) {
				ValidationUtils.assertTrue(MathUtils.isEqual(bean.getInvoiceType().getId(), bean.getParent().getInvoiceType().getId()),
						"This invoice cannot be a corrected invoice for a different billing invoice type.", "parent");
				if (bean.getBillingDefinition() != null) {
					ValidationUtils.assertTrue(MathUtils.isEqual(bean.getBillingDefinition().getId(), bean.getParent().getBillingDefinition().getId()),
							"This invoice cannot be a corrected invoice for a different billing definition.", "parent");
				}
				else {
					ValidationUtils.assertTrue(MathUtils.isEqual(bean.getBusinessCompany().getId(), bean.getParent().getBusinessCompany().getId()),
							"This invoice cannot be a corrected invoice for a different company.", "parent");
				}
				ValidationUtils.assertTrue(bean.getParent().isCanceled(), "This invoice cannot be a corrected invoice for an invoice that hasn't been voided.", "parent");
				ValidationUtils.assertTrue(DateUtils.compare(bean.getInvoiceDate(), bean.getParent().getInvoiceDate(), false) == 0,
						"This invoice cannot be a corrected invoice for an invoice with a different invoice date.", "parent");
			}
		}
	}


	private void updateBillingInvoiceGroupViolationStatus(BillingInvoice original, BillingInvoice bean) {
		// If change in RuleViolationStatus
		if (original == null || original.getViolationStatus() == null || !original.getViolationStatus().equals(bean.getViolationStatus())) {
			// If new RuleViolationStatus is not the same as the group's status, reset group to the min
			// Can't use less than, because re-processing may move to different warning statuses with different orders
			if (bean.getViolationStatus() != null && (bean.getInvoiceGroup().getViolationStatus() == null ||
					MathUtils.isNotEqual(bean.getInvoiceGroup().getViolationStatus().getOrder(), bean.getViolationStatus().getOrder()))) {
				BillingInvoiceGroup group = getBillingInvoiceService().getBillingInvoiceGroup(bean.getInvoiceGroup().getId());
				// Get an Invoice with the Min RuleViolationStatus Order, if after the Group RuleViolationStatus, update the Group
				for (BillingInvoice minInvoice : CollectionUtils.getIterable(BeanUtils.sortWithFunction(group.getInvoiceList(), billingInvoice -> (billingInvoice.getViolationStatus() == null ? null : billingInvoice.getViolationStatus().getOrder()), true))) {
					if (minInvoice.getViolationStatus() != null) {
						group.setViolationStatus(minInvoice.getViolationStatus());
						// Call DAO method direct.  Service method only allows Invoice Group Inserts
						getBillingInvoiceGroupDAO().save(group);
						break;
					}
				}
			}
		}
	}


	private boolean isViolationStatusUpdateRequired(BillingInvoice invoice) {
		if (invoice == null) {
			return false;
		}
		return invoice.getInvoiceGroup() != null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public UpdatableDAO<BillingInvoiceGroup> getBillingInvoiceGroupDAO() {
		return this.billingInvoiceGroupDAO;
	}


	public void setBillingInvoiceGroupDAO(UpdatableDAO<BillingInvoiceGroup> billingInvoiceGroupDAO) {
		this.billingInvoiceGroupDAO = billingInvoiceGroupDAO;
	}
}
