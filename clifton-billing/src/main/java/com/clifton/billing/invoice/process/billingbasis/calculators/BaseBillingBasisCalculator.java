package com.clifton.billing.invoice.process.billingbasis.calculators;


import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountCalculator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseBillingBasisCalculator</code> is a base class that contains common methods to use by implementations of {@link BillingBasisCalculator}
 * <p>
 */
public abstract class BaseBillingBasisCalculator implements BillingBasisCalculator {

	private BillingBasisService billingBasisService;
	private BillingDefinitionService billingDefinitionService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private boolean projectedRevenue;

	private Date projectedRevenueDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final BillingBasis calculate(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		BillingFrequencies definitionAccrualFrequency = billingDefinitionInvestmentAccount.getReferenceOne().getBillingFrequency();
		BillingFrequencies scheduleAccrualFrequency = null;

		// If the account is specifically identified for one schedule, then we just use that one and we don't need to check all
		if (billingDefinitionInvestmentAccount.getBillingSchedule() != null) {
			scheduleAccrualFrequency = ObjectUtils.coalesce(billingDefinitionInvestmentAccount.getBillingSchedule().getAccrualFrequency(), definitionAccrualFrequency);
		}
		else {
			// FIND ALL SCHEDULES THAT APPLY
			for (BillingSchedule schedule : config.getScheduleList()) {
				if (DateUtils.isOverlapInDates(schedule.getStartDate(), schedule.getEndDate(), invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate())) {
					BillingAmountCalculator billingAmountCalculator = config.getBillingAmountCalculatorForSchedule(schedule, getSystemBeanService());
					if (billingAmountCalculator.isBillingBasisUsed(schedule)) {
						if (scheduleAccrualFrequency == null) {
							scheduleAccrualFrequency = ObjectUtils.coalesce(schedule.getAccrualFrequency(), definitionAccrualFrequency);
						}
						else if (scheduleAccrualFrequency != ObjectUtils.coalesce(schedule.getAccrualFrequency(), definitionAccrualFrequency)) {
							throw new ValidationException("Cannot generate invoice - billing definition investment account " + billingDefinitionInvestmentAccount.getAccountLabel() + " is associated with multiple schedules that use different accrual frequencies.");
						}
					}
				}
			}
		}

		// If none of the schedules actually use billing basis then we default to using the billing definition frequency
		scheduleAccrualFrequency = ObjectUtils.coalesce(scheduleAccrualFrequency, definitionAccrualFrequency);

		// Validation for Projected Revenue ONLY if not a Monthly Accrual
		if (scheduleAccrualFrequency != BillingFrequencies.MONTHLY && isProjectedRevenue() && !isProjectedRevenueSupported()) {
			throw new ValidationException("Cannot generate projected revenue for billing definition investment account " + billingDefinitionInvestmentAccount.getAccountLabel() + ". Projected revenue calculation is not supported for " + billingDefinitionInvestmentAccount.getBillingBasisCalculationType().getName());
		}


		Date accrualStartDate = startDate;
		BillingBasis billingBasis = new BillingBasis();
		while (DateUtils.isDateBeforeOrEqual(accrualStartDate, endDate, false)) {
			Date accrualEndDate = scheduleAccrualFrequency.getLastDayOfBillingCycle(accrualStartDate, invoice.getBillingDefinition().getSecondYearStartDate());
			if (DateUtils.isDateAfter(accrualEndDate, endDate)) {
				accrualEndDate = endDate;
			}
			BillingBasis accrualBillingBasis = calculateImpl(config, invoice, billingDefinitionInvestmentAccount, accrualStartDate, accrualEndDate);
			if (accrualBillingBasis != null) {
				billingBasis.addBillingBasis(accrualBillingBasis);
			}
			accrualStartDate = DateUtils.addDays(accrualEndDate, 1);
		}
		return billingBasis;
	}


	public abstract BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected BillingBasisValueCalculator getBillingBasisValueCalculator(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		if (billingDefinitionInvestmentAccount.getBillingBasisValuationType() != null && billingDefinitionInvestmentAccount.getBillingBasisValuationType().getCalculatorBean() != null) {
			return (BillingBasisValueCalculator) getSystemBeanService().getBeanInstance(billingDefinitionInvestmentAccount.getBillingBasisValuationType().getCalculatorBean());
		}
		return null;
	}


	public List<BillingBasisSnapshot> getBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		// First Look it up in the database
		List<BillingBasisSnapshot> list = getBillingBasisService().getBillingBasisSnapshotList(invoice.getId(), billingDefinitionInvestmentAccount.getId());
		// filter on start/end dates (for cases where accrual frequency is used)
		list = BeanUtils.filter(list, snapshot -> DateUtils.isDateBetween(snapshot.getSnapshotDate(), defaultStart, defaultEnd, false));
		// If it's not there, let's rebuild it
		if (CollectionUtils.isEmpty(list)) {
			list = rebuildBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, defaultStart, defaultEnd);
		}
		return list;
	}


	public abstract List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart,
	                                                                           Date defaultEnd);


	protected BigDecimal getBillingBasisTotalFromList(List<BillingBasisSnapshot> snapshotList) {
		BigDecimal bb = CoreMathUtils.sumProperty(snapshotList, BillingBasisSnapshot::getSnapshotValueInBillingBaseCurrency);
		if (bb == null || CollectionUtils.isEmpty(snapshotList)) {
			return BigDecimal.ZERO;
		}
		BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = snapshotList.get(0).getBillingDefinitionInvestmentAccount();
		// Since market value (possibly notional too) include negative market value in each position billing basis
		// we need the sum to be the absolute value otherwise negative billing basis would result in
		// negative billing amounts and then we'd have to pay our clients instead of them paying us :)
		// Rare cases (manager cash) where ABS can be explicitly turned off
		return billingDefinitionInvestmentAccount.getBillingBasisValuationType().isAllowNegativeDailyValues() ? bb : MathUtils.abs(bb);
	}


	/**
	 * If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
	 * Uses Datasource for the M2M account relationship issuing company, else default.
	 */
	protected String getBillingBasisSnapshotExchangeRateDataSource(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		String dataSourceName = null;
		if (!invoice.getBillingDefinition().getBillingCurrency().equals(billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency())) {
			dataSourceName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(billingDefinitionInvestmentAccount.getReferenceTwo().toClientAccount());
		}
		return dataSourceName;
	}


	protected BigDecimal getBillingBasisSnapshotExchangeRate(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, String dataSourceName, Date date) {
		return getMarketDataExchangeRatesApiService().getExchangeRate(
				FxRateLookupCommand.forDataSource(dataSourceName, billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), date).flexibleLookup());
	}


	protected List<BillingBasisSnapshot> filterBillingBasisSnapshotListByDate(List<BillingBasisSnapshot> snapshotList, Date date) {
		// Note: Something odd happening with BeanUtils.filter, so filtering the list myself.
		// Seems like some cases Dates are returned as Timestamp from the getPropertyValue.
		// Since billingBasisDate is a calculated getter that returns Date type, tried explicitly creating a property of Date
		// and still returns timestamp.
		List<BillingBasisSnapshot> dateList = new ArrayList<>();
		for (BillingBasisSnapshot value : CollectionUtils.getIterable(snapshotList)) {
			if (DateUtils.compare(date, value.getSnapshotDate(), false) == 0) {
				dateList.add(value);
			}
		}
		return dateList;
	}


	protected Date getBillingBasisStartDateForAccount(BillingDefinitionInvestmentAccount billingAccount, Date defaultStart) {
		Date startDate = defaultStart;
		// If this BillingDefinitionInvestmentAccount has billing start after default start, then use that date.
		if (billingAccount.getStartDate() != null && DateUtils.compare(billingAccount.getStartDate(), defaultStart, false) > 0) {
			startDate = billingAccount.getStartDate();
		}

		// Projected Revenue - we only use the billing basis for the month of the projected revenue
		if (isProjectedRevenue()) {
			if (DateUtils.getFirstDayOfMonth(getProjectedRevenueDate()).after(startDate)) {
				return DateUtils.getFirstDayOfMonth(getProjectedRevenueDate());
			}
		}
		return startDate;
	}


	protected Date getBillingBasisEndDateForAccount(BillingDefinitionInvestmentAccount billingAccount, Date defaultEnd) {
		// If this BillingDefinitionInvestmentAccount has billing end before default end, then use that date
		Date endDate = defaultEnd;
		if (billingAccount.getEndDate() != null && DateUtils.compare(billingAccount.getEndDate(), defaultEnd, false) < 0) {
			endDate = billingAccount.getEndDate();
		}


		// If account ends before month end - we want to use the account end
		if (isProjectedRevenue()) {
			if (getProjectedRevenueDate().before(endDate)) {
				return getProjectedRevenueDate();
			}
		}
		return endDate;
	}


	/////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////
	/////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public boolean isProjectedRevenue() {
		return this.projectedRevenue;
	}


	public void setProjectedRevenue(boolean projectedRevenue) {
		this.projectedRevenue = projectedRevenue;
	}


	public Date getProjectedRevenueDate() {
		return this.projectedRevenueDate;
	}


	public void setProjectedRevenueDate(Date projectedRevenueDate) {
		this.projectedRevenueDate = projectedRevenueDate;
	}
}
