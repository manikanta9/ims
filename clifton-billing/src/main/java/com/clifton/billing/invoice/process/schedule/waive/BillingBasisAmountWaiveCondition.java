package com.clifton.billing.invoice.process.schedule.waive;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisAmountWaiveCondition</code> is a waive condition that evaluates
 * for the selected billing definition investment account(s) billing basis
 * total amount is >, >=, =, <=, < specified amount.
 * <p>
 * Example: Philips Electronics
 * If Treasuries billing  basis >= One Billion then Derivatives Schedule X is waived and Schedule Y applies
 * , otherwise if Treasuries billing basis < one billion, then Schedule Y is waived and Schedule X applies
 *
 * @author manderson
 */
public class BillingBasisAmountWaiveCondition extends BaseBillingAmountComparison implements Comparison<BillingSchedule> {

	/**
	 * Describes information about billing definition investment account(s)
	 * for result messages, i.e. Account 123456 or Account 123456 Treasuries
	 */
	private String billingBasisDescription;


	/**
	 * The Billing Accounts to include in the comparison
	 */
	private List<Integer> billingDefinitionInvestmentAccountIds;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(BillingSchedule schedule, ComparisonContext context) {

		BigDecimal billingBasis = BigDecimal.ZERO;
		BillingScheduleWaiveConditionConfig config = schedule.getWaiveSystemConditionConfig();

		for (Integer aId : CollectionUtils.getIterable(getBillingDefinitionInvestmentAccountIds())) {
			for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> accountBillingBasisEntry : config.getBillingBasisMap().entrySet()) {
				if (aId.equals(accountBillingBasisEntry.getKey().getId())) {
					billingBasis = MathUtils.add(billingBasis, accountBillingBasisEntry.getValue().getBillingBasisTotal());
					break;
				}
			}
		}

		boolean result = evaluateCondition(billingBasis);

		if (context != null) {
			String msg = getBillingBasisDescription() + " Billing Basis Amount [" + CoreMathUtils.formatNumberMoney(billingBasis) + "] is " + (!result ? "not " : "") + getComparisonType().getComparisonExpression() + " ["
					+ CoreMathUtils.formatNumberMoney(getAmount()) + "].";
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Integer> getBillingDefinitionInvestmentAccountIds() {
		return this.billingDefinitionInvestmentAccountIds;
	}


	public void setBillingDefinitionInvestmentAccountIds(List<Integer> billingDefinitionInvestmentAccountIds) {
		this.billingDefinitionInvestmentAccountIds = billingDefinitionInvestmentAccountIds;
	}


	public String getBillingBasisDescription() {
		return this.billingBasisDescription;
	}


	public void setBillingBasisDescription(String billingBasisDescription) {
		this.billingBasisDescription = billingBasisDescription;
	}
}
