package com.clifton.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BaseBillingBasisValueCalculator</code> is the base implementation of the valuation calculators, and should be extended by all billing basis value calculators.
 * It handles itself any changes when running billing for projected revenue, and also handles any security exclusions that may apply.
 *
 * @author manderson
 */
public abstract class BaseBillingBasisValueCalculator implements BillingBasisValueCalculator {


	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to alter processing when running invoices in order to generate projected revenue
	 */
	private boolean projectedRevenue;


	/**
	 * Required if projectedRevenue flag is set.  Used to determine the month we are running projected revenue for (usually previous month)
	 */
	private Date projectedRevenueDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Calls the real calculate method for each valuation type - then removes entries that should be excluded based on security exclusion tables and creates violations for any exclusions that were done
	 */
	@Override
	public final List<BillingBasisSnapshot> calculateBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		if (isProjectedRevenue()) {
			ValidationUtils.assertNotNull(getProjectedRevenueDate(), "Projected Revenue Date is required for running projected revenue");
			Date projectedRevenueStart = DateUtils.getFirstDayOfMonth(getProjectedRevenueDate());
			Date projectedRevenueEnd = DateUtils.getLastDayOfMonth(getProjectedRevenueDate());
			if (!DateUtils.isDateAfter(startDate, projectedRevenueStart)) {
				startDate = projectedRevenueStart;
			}
			if (DateUtils.isDateAfter(endDate, projectedRevenueEnd)) {
				endDate = projectedRevenueEnd;
			}
		}
		List<BillingBasisSnapshot> snapshotList = calculateBillingBasisSnapshotListImpl(config, invoice, billingDefinitionInvestmentAccountId, startDate, endDate);
		return applyBillingDefinitionInvestmentAccountSecurityExclusions(config, billingDefinitionInvestmentAccountId, snapshotList, startDate, endDate);
	}


	public abstract List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Filters the BillingBasisSnapshot list based on any security exclusions that have been defined.
	 */
	private List<BillingBasisSnapshot> applyBillingDefinitionInvestmentAccountSecurityExclusions(BillingInvoiceProcessConfig config, int billingDefinitionInvestmentAccountId, List<BillingBasisSnapshot> snapshotList, Date startDate, Date endDate) {
		List<BillingDefinitionInvestmentAccountSecurityExclusion> exclusionList = BeanUtils.filter(config.getAccountBillingSecurityExclusionMap().get(billingDefinitionInvestmentAccountId), exclusion -> DateUtils.isOverlapInDates(startDate, endDate, exclusion.getStartDate(), exclusion.getEndDate()));
		if (!CollectionUtils.isEmpty(exclusionList)) {
			return BeanUtils.filter(snapshotList, snapshot -> !isBillingBasisSnapshotExcluded(config, snapshot, exclusionList));
		}
		return snapshotList;
	}


	private boolean isBillingBasisSnapshotExcluded(BillingInvoiceProcessConfig config, BillingBasisSnapshot snapshot, List<BillingDefinitionInvestmentAccountSecurityExclusion> exclusionList) {
		// Puts the exclusions in order so we check security specific first, then conditions
		List<BillingDefinitionInvestmentAccountSecurityExclusion> snapshotExclusionList = BeanUtils.sortWithFunction(BeanUtils.filter(exclusionList, exclusion -> {
			if (exclusion.getInvestmentSecurity() != null && !CompareUtils.isEqual(exclusion.getInvestmentSecurity(), snapshot.getSecurity())) {
				return false;
			}
			if (!DateUtils.isDateBetween(snapshot.getSnapshotDate(), exclusion.getStartDate(), exclusion.getEndDate(), false)) {
				return false;
			}
			return true;
		}), exclusion -> {
			if (exclusion.getInvestmentSecurity() != null) {
				if (exclusion.getExcludeCondition() == null) {
					return 1;
				}
				return 2;
			}
			return 3;
		}, true);

		for (BillingDefinitionInvestmentAccountSecurityExclusion securityExclusion : CollectionUtils.getIterable(snapshotExclusionList)) {
			if (securityExclusion.getExcludeCondition() == null) {
				if (CompareUtils.isEqual(snapshot.getSecurity(), securityExclusion.getInvestmentSecurity())) {
					populateSecurityExclusionViolationMap(config, securityExclusion, "Security " + securityExclusion.getInvestmentSecurity().getLabel() + ": explicitly excluded.", snapshot.getSnapshotDate());
					return true;
				}
			}
			else {
				String trueMessage = getSystemConditionEvaluationHandler().getConditionTrueMessage(securityExclusion.getExcludeCondition(), snapshot);
				if (!StringUtils.isEmpty(trueMessage)) {
					populateSecurityExclusionViolationMap(config, securityExclusion, "Security " + snapshot.getSecurity().getLabel() + ": " + trueMessage, snapshot.getSnapshotDate());
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * A map of security exclusions as applied is populated and used in the end when saving the invoice to also generate a violation for the invoice so users are aware of any exclusions that were applied and why.
	 */
	private void populateSecurityExclusionViolationMap(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccountSecurityExclusion securityExclusion, String excludeMessage, Date date) {
		Map<String, Set<Date>> securityExclusionViolationMap = config.getBillingBasisSecurityExclusionViolationMap().computeIfAbsent(securityExclusion, k -> new HashMap<>());
		Set<Date> dateSet = securityExclusionViolationMap.computeIfAbsent(excludeMessage, k -> new HashSet<>());
		dateSet.add(date);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public boolean isProjectedRevenue() {
		return this.projectedRevenue;
	}


	public void setProjectedRevenue(boolean projectedRevenue) {
		this.projectedRevenue = projectedRevenue;
	}


	public Date getProjectedRevenueDate() {
		return this.projectedRevenueDate;
	}


	public void setProjectedRevenueDate(Date projectedRevenueDate) {
		this.projectedRevenueDate = projectedRevenueDate;
	}
}
