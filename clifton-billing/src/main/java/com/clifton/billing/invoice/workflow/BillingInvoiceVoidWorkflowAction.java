package com.clifton.billing.invoice.workflow;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.payment.BillingPaymentService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.tracking.PortalTrackingEventExtended;
import com.clifton.portal.tracking.PortalTrackingEventTypes;
import com.clifton.portal.tracking.PortalTrackingService;
import com.clifton.portal.tracking.search.PortalTrackingEventExtendedSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>BillingInvoiceVoidWorkflowAction</code> handles removing any allocated payments to an invoice after it's been voided.
 * <p>
 * Will also unapprove portal files for the invoice if the invoice was never downloaded by the client
 *
 * @author Mary Anderson
 */
public class BillingInvoiceVoidWorkflowAction implements WorkflowTransitionActionHandler<BillingInvoice> {


	private BillingPaymentService billingPaymentService;

	private PortalFileService portalFileService;

	private PortalTrackingService portalTrackingService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingInvoice processAction(BillingInvoice invoice, WorkflowTransition transition) {
		getBillingPaymentService().deleteBillingPaymentAllocationListForInvoice(invoice.getId());

		// When Voiding the invoice - we will "unapprove" the file so it doesn't display on the portal ONLY if no client ever downloaded the file
		List<PortalFile> fileList = getPortalFileService().getPortalFileListForSourceEntity("IMS", BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId());
		if (!CollectionUtils.isEmpty(fileList)) {
			Integer[] portalFileIds = BeanUtils.getBeanIdentityArray(fileList, Integer.class);
			PortalTrackingEventExtendedSearchForm searchForm = new PortalTrackingEventExtendedSearchForm();
			if (fileList.size() == 1) {
				searchForm.setPortalFileId(fileList.get(0).getId());
			}
			else {
				searchForm.setPortalFileIds(portalFileIds);
			}
			searchForm.setTrackingEventTypeNameEquals(PortalTrackingEventTypes.FILE_DOWNLOAD.getName());
			// Limit to Client Users - Not internal
			searchForm.setPortalSecurityUserRolePortalEntitySpecific(true);
			List<PortalTrackingEventExtended> downloadEventList = getPortalTrackingService().getPortalTrackingEventExtendedList(searchForm, false);
			if (CollectionUtils.isEmpty(downloadEventList)) {
				getPortalFileService().approvePortalFileList(portalFileIds, false);
			}
		}
		return invoice;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPaymentService getBillingPaymentService() {
		return this.billingPaymentService;
	}


	public void setBillingPaymentService(BillingPaymentService billingPaymentService) {
		this.billingPaymentService = billingPaymentService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalTrackingService getPortalTrackingService() {
		return this.portalTrackingService;
	}


	public void setPortalTrackingService(PortalTrackingService portalTrackingService) {
		this.portalTrackingService = portalTrackingService;
	}
}
