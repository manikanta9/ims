package com.clifton.billing.invoice.process.schedule;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingScheduleProcessConfig</code> is used during processing of invoices to pass around with all necessary information stored on the one
 * object for convenience.
 *
 * @author manderson
 */
public class BillingScheduleProcessConfig {

	private final List<BillingInvoice> invoiceList;
	private final BillingSchedule schedule;

	/**
	 * If a shared schedule this will be the least precision across the definitions that share it
	 */
	private Short billingAmountPrecision;

	private Date invoiceDate;

	private BillingFrequencies billingFrequency;
	private InvestmentSecurity billingCurrency;

	private Date invoicePeriodStartDate;
	private Date invoicePeriodEndDate;

	private Boolean paymentInAdvance;

	private Date invoiceBillingStartDate;
	private Date invoiceBillingEndDate;

	private Date billingBasisStartDate;
	private Date billingBasisEndDate;

	private Date annualInvoicePeriodStartDate;
	private Date annualInvoicePeriodEndDate;

	private Date annualAbsoluteInvoicePeriodEndDate;

	/**
	 * When billing schedules for different frequencies than the invoice
	 * we process the schedule once for each accrual period
	 */
	private Date accrualStartDate;
	private Date accrualEndDate;

	///////////////////////////////////////////////////////////////////
	// Process/Allocation Properties used to Generate Invoice Details/Invoice Detail Account Details

	// Total Billing Basis, and Billing Basis used just for this invoice (used for shared where we need total, but allocated based on this)
	// Mapped by aggregation level
	private final Map<String, BillingBasis> totalBillingBasisMapByAggregationLevel = new HashMap<>();

	// Map of Aggregation Level to String Label (i.e. if breaking out fees by client or account there could be multiple lines on the invoice so want optional label for each detail)
	private final Map<String, String> aggregationLevelLabelMap = new HashMap<>();

	// Billing Basis Map for definition accounts. Used to allocate fees to accounts.
	// Mapped by aggregation level
	private final Map<String, Map<BillingDefinitionInvestmentAccount, BillingBasis>> accountBillingBasisMapByAggregationLevel = new HashMap<>();

	// For those schedules that are fixed fees (i.e. don't use a billing basis), the following contains the list of account numbers that should get a proportional allocation of the total fee
	// Note: Any account that didn't have any positions during the period are skipped, or if the account billing dates weren't active during the invoice period
	private final Map<Integer, List<InvestmentAccount>> proportionalSplitAccountMapList = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingScheduleProcessConfig(BillingSchedule schedule, List<BillingInvoice> invoiceList) {
		this.schedule = schedule;
		this.invoiceList = invoiceList;

		// Used for schedules that share an annual fee to ensure they are all on the same annual schedule
		Date absoluteAnnualEnd = null;


		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			if (this.invoiceDate == null) {
				this.invoiceDate = invoice.getInvoiceDate();
			}
			else if (DateUtils.compare(this.invoiceDate, invoice.getInvoiceDate(), false) != 0) {
				throw new ValidationException("Unable to process Schedule [" + schedule.getName() + "] because there are invoices in invoice group [" + invoice.getInvoiceGroup().getId()
						+ "] with at least two different invoice dates [" + DateUtils.fromDateShort(this.invoiceDate) + ", " + DateUtils.fromDateShort(invoice.getInvoiceDate()) + "].");
			}
			// If Billing Amount Precision Isn't Set, or this invoice uses less precision than already set, set it to the current invoice's billing precision
			if (this.billingAmountPrecision == null || MathUtils.isGreaterThan(this.billingAmountPrecision, invoice.getInvoiceType().getBillingAmountPrecision())) {
				this.billingAmountPrecision = invoice.getInvoiceType().getBillingAmountPrecision();
			}
			if (this.billingFrequency == null) {
				this.billingFrequency = ObjectUtils.coalesce(schedule.getAccrualFrequency(), invoice.getBillingDefinition().getBillingFrequency());
			}
			else if (this.billingFrequency != ObjectUtils.coalesce(schedule.getAccrualFrequency(), invoice.getBillingDefinition().getBillingFrequency())) {
				throw new ValidationException("Unable to process Schedule [" + schedule.getName() + "] because there are invoices in invoice group [" + invoice.getInvoiceGroup().getId()
						+ "] with at least two different billing frequencies [" + this.billingFrequency.name() + ", " + ObjectUtils.coalesce(schedule.getAccrualFrequency(), invoice.getBillingDefinition().getBillingFrequency()).name() + "].");
			}
			if (this.billingCurrency == null) {
				this.billingCurrency = invoice.getBillingDefinition().getBillingCurrency();
			}
			else if (!this.billingCurrency.equals(invoice.getBillingDefinition().getBillingCurrency())) {
				throw new ValidationException("Unable to process Schedule [" + schedule.getName() + "] because there are invoices in invoice group [" + invoice.getInvoiceGroup().getId()
						+ "] with at least two different billing currencies [" + this.billingCurrency.getSymbol() + ", " + invoice.getBillingDefinition().getBillingCurrency().getSymbol() + "].");
			}

			if (this.paymentInAdvance == null) {
				this.paymentInAdvance = invoice.getBillingDefinition().isPaymentInAdvance();
			}
			else if (this.paymentInAdvance != invoice.getBillingDefinition().isPaymentInAdvance()) {
				throw new ValidationException("Unable to process Schedule [" + schedule.getName() + "] because there are invoices in invoice group [" + invoice.getInvoiceGroup().getId()
						+ "] with payment in advance and not payment in advance.");
			}

			if (schedule.getScheduleType().isAnnualFee()) {
				if (absoluteAnnualEnd == null) {
					absoluteAnnualEnd = invoice.getAnnualAbsoluteInvoicePeriodEndDate();
				}
				else if (DateUtils.compare(absoluteAnnualEnd, invoice.getAnnualAbsoluteInvoicePeriodEndDate(), false) != 0) {
					throw new ValidationException("Unable to process Schedule [" + schedule.getName() + "] because there are invoices in invoice group [" + invoice.getInvoiceGroup().getId()
							+ "] with different annual schedules, but they share an annual fee. Please verify all Second Year Start Dates are populated correctly and match on the Month/Day.");
				}
			}
			updateInvoicePeriodDatesFromInvoice(invoice);
		}
		this.annualAbsoluteInvoicePeriodEndDate = absoluteAnnualEnd;
		// Shouldn't happen, precision is required on invoice types that use billing definitions
		ValidationUtils.assertNotNull(this.billingAmountPrecision, "Cannot determine billing amount precision to use - check invoice types have precision set.");
	}


	private void updateInvoicePeriodDatesFromInvoice(BillingInvoice invoice) {
		// if billingBasisStartDate is null or invoice.BillingBasisStartDate is earlier - use the biggest date range
		if ((this.billingBasisStartDate == null) || (DateUtils.isDateBefore(invoice.getBillingBasisStartDate(), this.billingBasisStartDate, false))) {
			this.billingBasisStartDate = invoice.getBillingBasisStartDate();
		}

		// if billingBasisEndDate is null or invoice.BillingBasisEndDate() later - use the biggest date range
		if ((this.billingBasisEndDate == null) || (DateUtils.compare(invoice.getBillingBasisEndDate(), this.billingBasisEndDate, false) > 0)) {
			this.billingBasisEndDate = invoice.getBillingBasisEndDate();
		}

		// if invoicePeriodStartDate is null or invoice.InvoicePeriodStartDate is earlier - use the biggest date range
		if (this.invoicePeriodStartDate == null || (DateUtils.isDateBefore(invoice.getInvoicePeriodStartDate(), this.invoicePeriodStartDate, false))) {
			this.invoicePeriodStartDate = invoice.getInvoicePeriodStartDate();
		}

		// if invoicePeriodEndDate is null or invoice.InvoicePeriodEndDate is later - use the biggest date range
		if ((this.invoicePeriodEndDate == null) || (DateUtils.compare(invoice.getInvoicePeriodEndDate(), this.invoicePeriodEndDate, false) > 0)) {
			this.invoicePeriodEndDate = invoice.getInvoicePeriodEndDate();
		}

		// if invoiceBillingStartDate is null or if invoice.InvoiceBillingStartDate is earlier - use the biggest date range
		if ((this.invoiceBillingStartDate == null) || (DateUtils.isDateBefore(invoice.getInvoiceBillingStartDate(), this.invoiceBillingStartDate, false))) {
			this.invoiceBillingStartDate = invoice.getInvoiceBillingStartDate();
		}

		// if invoiceBillingEndDate or invoice.InvoiceBillingEndDate() is later - use the biggest date range
		if ((this.invoiceBillingEndDate == null) || (DateUtils.compare(invoice.getInvoiceBillingEndDate(), this.invoiceBillingEndDate, false) > 0)) {
			this.invoiceBillingEndDate = invoice.getInvoiceBillingEndDate();
		}

		// if annualInvoicePeriodStartDate is null or invoice.AnnualInvoicePeriodStartDate is earlier - use the biggest date range
		if ((this.annualInvoicePeriodStartDate == null) || (DateUtils.isDateBefore(invoice.getAnnualInvoicePeriodStartDate(), this.annualInvoicePeriodStartDate, false))) {
			this.annualInvoicePeriodStartDate = invoice.getAnnualInvoicePeriodStartDate();
		}

		// if annualInvoicePeriodEndDate or  invoice.AnnualInvoicePeriodEndDate is later - use the biggest date range
		if ((this.annualInvoicePeriodEndDate == null) || (DateUtils.compare(invoice.getAnnualInvoicePeriodEndDate(), this.annualInvoicePeriodEndDate, false) > 0)) {
			this.annualInvoicePeriodEndDate = invoice.getAnnualInvoicePeriodEndDate();
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If the account accrues fees monthly, but bills quarterly, we need to reset these dates in order to reprocess the schedule for the next accrual period, however we don't need to create completely new object.
	 */
	public void resetForAccrualPeriod(Date accrualStartDate, Date accrualEndDate) {
		this.accrualStartDate = accrualStartDate;
		this.accrualEndDate = accrualEndDate;

		this.totalBillingBasisMapByAggregationLevel.clear();
		this.aggregationLevelLabelMap.clear();
		this.accountBillingBasisMapByAggregationLevel.clear();
		this.proportionalSplitAccountMapList.clear();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getDaysInInvoicePeriod() {
		return DateUtils.getDaysDifferenceInclusive(ObjectUtils.coalesce(getAccrualEndDate(), getInvoicePeriodEndDate()), ObjectUtils.coalesce(getAccrualStartDate(), getInvoicePeriodStartDate()));
	}


	public Integer getDaysInInvoiceBilling() {
		return DateUtils.getDaysDifferenceInclusive(getInvoiceBillingEndDate(), getInvoiceBillingStartDate());
	}


	public Integer getDaysInAnnualInvoicePeriod() {
		return DateUtils.getDaysDifferenceInclusive(getAnnualInvoicePeriodEndDate(), getAnnualInvoicePeriodStartDate());
	}


	public boolean isLastInvoiceForAnnualInvoicePeriod() {
		return DateUtils.isDateBetween(getAnnualInvoicePeriodEndDate(), getInvoicePeriodStartDate(), getInvoicePeriodEndDate(), false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public void addBillingBasis(String aggregationLevel, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, BillingBasis billingBasis) {
		this.totalBillingBasisMapByAggregationLevel.computeIfAbsent(aggregationLevel, level -> new BillingBasis()).addBillingBasis(billingBasis);
		this.accountBillingBasisMapByAggregationLevel.computeIfAbsent(aggregationLevel, level -> new HashMap<>()).computeIfAbsent(billingDefinitionInvestmentAccount, account -> new BillingBasis()).addBillingBasis(billingBasis);
	}


	public void addAggregationLevelLabel(String aggregationLevel, String aggregationLevelLabel) {
		this.aggregationLevelLabelMap.putIfAbsent(aggregationLevel, aggregationLevelLabel);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<InvestmentAccount> getProportionalSplitAccountList(Integer billingInvoiceId) {
		List<InvestmentAccount> accountList = this.proportionalSplitAccountMapList.get(billingInvoiceId);
		if (getSchedule().getScheduleType().isMinimumFee() && CollectionUtils.isEmpty(accountList)) {
			accountList = new ArrayList<>();
			for (BillingInvoice invoice : getInvoiceList()) {
				if (billingInvoiceId.equals(invoice.getId())) {
					for (BillingDefinitionInvestmentAccount da : invoice.getBillingDefinition().getInvestmentAccountList()) {
						if (!accountList.contains(da.getReferenceTwo())) {
							accountList.add(da.getReferenceTwo());
						}
					}
				}
			}
		}
		return accountList;
	}


	public void addProportionalSplitAccountToList(Integer billingInvoiceId, InvestmentAccount account) {
		List<InvestmentAccount> acctList = this.proportionalSplitAccountMapList.get(billingInvoiceId);
		if (acctList == null) {
			acctList = new ArrayList<>();
		}
		if (!acctList.contains(account)) {
			acctList.add(account);
			this.proportionalSplitAccountMapList.put(billingInvoiceId, acctList);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<BillingInvoice> getInvoiceList() {
		return this.invoiceList;
	}


	public BillingSchedule getSchedule() {
		return this.schedule;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public Short getBillingAmountPrecision() {
		return this.billingAmountPrecision;
	}


	public BillingFrequencies getBillingFrequency() {
		return this.billingFrequency;
	}


	public InvestmentSecurity getBillingCurrency() {
		return this.billingCurrency;
	}


	public Date getInvoicePeriodStartDate() {
		return this.invoicePeriodStartDate;
	}


	public Date getInvoicePeriodEndDate() {
		return this.invoicePeriodEndDate;
	}


	public Boolean getPaymentInAdvance() {
		return this.paymentInAdvance;
	}


	public Date getInvoiceBillingStartDate() {
		return this.invoiceBillingStartDate;
	}


	public Date getInvoiceBillingEndDate() {
		return this.invoiceBillingEndDate;
	}


	public Date getAnnualInvoicePeriodStartDate() {
		return this.annualInvoicePeriodStartDate;
	}


	public Date getAnnualInvoicePeriodEndDate() {
		return this.annualInvoicePeriodEndDate;
	}


	public Map<String, BillingBasis> getTotalBillingBasisMapByAggregationLevel() {
		return this.totalBillingBasisMapByAggregationLevel;
	}


	public Map<String, Map<BillingDefinitionInvestmentAccount, BillingBasis>> getAccountBillingBasisMapByAggregationLevel() {
		return this.accountBillingBasisMapByAggregationLevel;
	}


	public Map<String, String> getAggregationLevelLabelMap() {
		return this.aggregationLevelLabelMap;
	}


	public Date getAnnualAbsoluteInvoicePeriodEndDate() {
		return this.annualAbsoluteInvoicePeriodEndDate;
	}


	public void setAnnualAbsoluteInvoicePeriodEndDate(Date annualAbsoluteInvoicePeriodEndDate) {
		this.annualAbsoluteInvoicePeriodEndDate = annualAbsoluteInvoicePeriodEndDate;
	}


	public Date getBillingBasisStartDate() {
		return this.billingBasisStartDate;
	}


	public Date getBillingBasisEndDate() {
		return this.billingBasisEndDate;
	}


	public Date getAccrualStartDate() {
		return this.accrualStartDate;
	}


	public Date getAccrualEndDate() {
		return this.accrualEndDate;
	}
}
