package com.clifton.billing.invoice.rule;

import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.violation.RuleViolation;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The BillingInvoiceRuleHandler interface is used for working with invoices and rule evaluations
 *
 * @author manderson
 */
public interface BillingInvoiceRuleHandler {

	public static final String RULE_CATEGORY_BILLING_INVOICE_MANAGEMENT_FEE = "Billing Invoice Management Fee Rules";
	public static final String RULE_DEFINITION_NO_BILLING_BASIS = "Billing Definition Account: No Billing Basis";
	public static final String RULE_DEFINITION_ZERO_BILLING_BASIS = "Billing Definition Account: Zero Billing Basis";
	public static final String RULE_DEFINITION_WAIVED_SCHEDULES = "Billing Schedule Waived";
	public static final String RULE_DEFINITION_PROCESSING_SCHEDULES = "Billing Schedule Processing";
	public static final String RULE_DEFINITION_BILLING_BASIS_MISSING_SOURCE_DATA = "Billing Basis Missing Source Data";
	public static final String RULE_DEFINITION_BILLING_BASIS_SNAPSHOT_SECURITY_EXCLUDED = "Billing Basis Snapshot Security Excluded";


	/**
	 * Runs Billing Invoice Rules for the invoice - also moves the invoice to PROCESSED_SUCCESS and saves so the warning status is properly updated
	 */
	public void validateBillingInvoiceRules(BillingInvoice invoice);


	public RuleViolation createSystemDefinedRuleViolation(Integer linkedFkFieldId, Integer causeFkFieldId, String violationNote, String ruleDefinitionName);


	public void saveBillingInvoiceRuleViolationListSystemDefined(BillingInvoice invoice, Map<String, List<RuleViolation>> ruleViolationMap);


	public void saveBillingInvoiceRuleViolationListSystemDefined(BillingInvoice invoice, String ruleDefinitionName, Map<Integer, List<String>> processedEntities);


	/**
	 * Specific method for the {@see BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_MISSING_SOURCE_DATA} violation so we can pass the cause entity and list of dates
	 */
	public void saveBillingInvoiceRuleViolationListForBillingBasisMissingSourceData(BillingInvoice invoice, Map<IdentityObject, Set<Date>> missingSourceDataMap);


	/**
	 * Specific method for the {@see BillingInvoiceRuleHandler.RULE_DEFINITION_BILLING_BASIS_SNAPSHOT_SECURITY_EXCLUDED} violation that filters the violation list to the invoice and groups violations together based on the linked exclusion id
	 */
	public void saveBillingInvoiceRuleViolationListForBillingBasisSnapshotSecurityExcluded(BillingInvoice invoice, Map<BillingDefinitionInvestmentAccountSecurityExclusion, Map<String, Set<Date>>> securityExclusionSecurityDateListMap);
}
