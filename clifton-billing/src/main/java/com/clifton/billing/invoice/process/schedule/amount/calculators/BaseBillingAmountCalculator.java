package com.clifton.billing.invoice.process.schedule.amount.calculators;


import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.definition.BillingScheduleType;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetail;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.process.BillingInvoiceDetailAccountCalculator;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorTypes;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountProrater;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BaseBillingAmountCalculator</code> defines the common functionality used by BillingAmountCalculators
 *
 * @author manderson
 */
public abstract class BaseBillingAmountCalculator implements BillingAmountCalculator {

	private BillingDefinitionService billingDefinitionService;

	private BillingInvoiceService billingInvoiceService;

	private BillingInvoiceDetailAccountCalculator billingInvoiceDetailAccountCalculator;

	private InvestmentAccountService investmentAccountService;

	private SystemBeanService systemBeanService;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private SystemBean billingAmountProraterBean;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateBillingScheduleType(BillingScheduleType scheduleType) {
		BillingAmountCalculatorTypes calculatorType = getBillingAmountCalculatorType();
		ValidationUtils.assertEquals(scheduleType.isTiered(), calculatorType.isTiered(), generateValidationMessage(calculatorType.isTiered(), "tiered schedules"));
		ValidationUtils.assertEquals(scheduleType.isFlatFee(), calculatorType.isFlatFee(), generateValidationMessage(calculatorType.isFlatFee(), "flat fee schedules"));
		ValidationUtils.assertEquals(scheduleType.isFlatFeePercent(), calculatorType.isFlatFeePercent(), generateValidationMessage(calculatorType.isFlatFeePercent(), "flat fee percent schedules"));
		ValidationUtils.assertEquals(scheduleType.isMinimumFee() || scheduleType.isMaximumFee(), calculatorType.isBoundary(),
				generateValidationMessage(calculatorType.isBoundary(), "min/max schedules"));
		ValidationUtils.assertEquals(scheduleType.isAnnualFee(), calculatorType.isAnnualFee(), generateValidationMessage(calculatorType.isAnnualFee(), "annual schedule types"));
	}


	private String generateValidationMessage(boolean required, String description) {
		return "Selected calculator " + (required ? "requires " : "does not support ") + description + ".";
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final void calculate(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		validateBillingScheduleType(scheduleConfig.getSchedule().getScheduleType());
		setupBillingBasisForProcessing(config, scheduleConfig);
		calculateImpl(config, scheduleConfig);
	}


	protected void calculateImpl(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		// Use Billing Basis would be true if it's shared - i.e. invoice list size greater than one
		if (isBillingBasisUsed(scheduleConfig.getSchedule()) || CollectionUtils.getSize(scheduleConfig.getInvoiceList()) > 1) {
			for (Map.Entry<String, BillingBasis> aggregationLevelEntry : scheduleConfig.getTotalBillingBasisMapByAggregationLevel().entrySet()) {
				BillingBasis totalBillingBasis = aggregationLevelEntry.getValue();
				boolean addDateToDetailNote = CollectionUtils.getSize(totalBillingBasis.getDetailList()) > 1 || totalBillingBasis.isAlwaysDisplayDateDetails();
				for (BillingBasisDetail totalDetail : CollectionUtils.getIterable(totalBillingBasis.getDetailList())) {
					if (scheduleConfig.getAccrualStartDate() != null && !DateUtils.isOverlapInDates(totalDetail.getStartDate(), totalDetail.getEndDate(), scheduleConfig.getAccrualStartDate(), scheduleConfig.getAccrualEndDate())) {
						continue;
					}
					// If Billing Basis was adjusted, there would be a note as to why
					StringBuilder note = new StringBuilder(16);
					if (totalDetail.getNote() != null && !StringUtils.isEmpty(totalDetail.getNote())) {
						note.append(totalDetail.getNote());
					}

					if (scheduleConfig.getSchedule().getInvestmentAccount() != null) {
						note.append("Account #: ").append(scheduleConfig.getSchedule().getInvestmentAccount().getNumber()).append(StringUtils.NEW_LINE);
					}
					else if (!StringUtils.isEmpty(scheduleConfig.getAggregationLevelLabelMap().get(aggregationLevelEntry.getKey()))) {
						note.append(scheduleConfig.getAggregationLevelLabelMap().get(aggregationLevelEntry.getKey())).append(StringUtils.NEW_LINE);
					}


					if (addDateToDetailNote) {
						note.append(totalDetail.getDateLabel()).append(":").append(StringUtils.NEW_LINE);
					}

					BigDecimal amount = calculateAmount(scheduleConfig, totalDetail, note);

					if (MathUtils.isNullOrZero(amount)) {
						addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
					}
					else {
						List<BillingInvoiceDetail> detailList = new ArrayList<>();
						Set<Integer> invoiceIncludedSet = new HashSet<>();
						Map<Integer, String> invoiceWaiveMap = new HashMap<>(); // Track waived schedules for each invoice because if aggregation causes multiple details we only want a violation if missing on relevant invoices
						for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
							BigDecimal allocationPercentage = null;
							Map<BillingDefinitionInvestmentAccount, BillingBasis> invoiceBillingBasisMap = new HashMap<>();
							// If there is only one invoice - it gets the total
							if (CollectionUtils.getSize(scheduleConfig.getInvoiceList()) == 1) {
								invoiceBillingBasisMap = scheduleConfig.getAccountBillingBasisMapByAggregationLevel().get(aggregationLevelEntry.getKey());
							}
							else {
								if (!CollectionUtils.isEmpty(scheduleConfig.getAccountBillingBasisMapByAggregationLevel().get(aggregationLevelEntry.getKey()))) {
									BigDecimal invoiceBillingBasisTotal = BigDecimal.ZERO;
									for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> accountBillingBasisEntry : scheduleConfig.getAccountBillingBasisMapByAggregationLevel().get(aggregationLevelEntry.getKey()).entrySet()) {
										if (invoice.getBillingDefinition().equals(accountBillingBasisEntry.getKey().getReferenceOne())) {
											invoiceIncludedSet.add(invoice.getId());
											BillingBasis bb = accountBillingBasisEntry.getValue();
											invoiceBillingBasisMap.put(accountBillingBasisEntry.getKey(), bb);
											BillingBasisDetail accountTotalDetail = bb.getBillingBasisDetail(totalDetail.getStartDate(), totalDetail.getEndDate());
											if (accountTotalDetail != null) {
												invoiceBillingBasisTotal = MathUtils.add(invoiceBillingBasisTotal, accountTotalDetail.getTotalAmount());
											}
										}
									}
									allocationPercentage = CoreMathUtils.getPercentValue(invoiceBillingBasisTotal, totalDetail.getTotalAmount(), true);
								}
							}

							String invoiceDetailNote = note.toString();

							BigDecimal allocationAmount = (allocationPercentage == null ? amount : MathUtils.getPercentageOf(allocationPercentage, amount, true));
							if (allocationPercentage != null && !MathUtils.isEqual(allocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
								invoiceDetailNote += StringUtils.NEW_LINE + "Allocated for " + BillingAmountCalculatorUtils.formatBillingAmountForInvoice(scheduleConfig, MathUtils.round(allocationPercentage, 2), true)
										+ " of total fees";
							}
							if (MathUtils.isEqual(BigDecimal.ZERO, allocationAmount)) {
								invoiceWaiveMap.put(invoice.getId(), invoiceDetailNote);
							}
							else {
								BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, totalDetail.getTotalAmount(), allocationAmount, invoiceDetailNote);
								List<BillingInvoiceDetailAccount> detailAccountList = getBillingInvoiceDetailAccountCalculator().calculateBillingInvoiceDetailAccountList(invoice, detail,
										allocationPercentage, totalDetail, invoiceBillingBasisMap, scheduleConfig.getProportionalSplitAccountList(invoice.getId()));
								// Note: Rounding differences are done later - after invoice details rounding is fixed so they always match
								detail.setDetailAccountList(detailAccountList);
								detailList.add(detail);
							}
						}
						CoreMathUtils.applySumPropertyDifference(detailList, BillingInvoiceDetail::getBillingAmount, BillingInvoiceDetail::setBillingAmount, BillingAmountCalculatorUtils.roundBillingAmount(scheduleConfig, amount, false), true);
						for (BillingInvoiceDetail detail : CollectionUtils.getIterable(detailList)) {
							applyInvoiceDetailAccountListToInvoiceDetail(detail, detail.getDetailAccountList());
							config.addInvoiceDetail(detail);
						}
						for (Map.Entry<Integer, String> invoiceScheduleWaivedEntry : invoiceWaiveMap.entrySet()) {
							if (invoiceIncludedSet.contains(invoiceScheduleWaivedEntry.getKey())) {
								config.setInvoiceScheduleWaivedViolation(invoiceScheduleWaivedEntry.getKey(), scheduleConfig.getSchedule(), invoiceScheduleWaivedEntry.getValue());
							}
						}
					}
				}
			}
		}
		else {
			StringBuilder note = new StringBuilder(16);
			if (scheduleConfig.getSchedule().getInvestmentAccount() != null) {
				note.append("Account #: ").append(scheduleConfig.getSchedule().getInvestmentAccount().getNumber()).append(StringUtils.NEW_LINE);
			}

			BigDecimal amount = calculateAmount(scheduleConfig, null, note);

			if (MathUtils.isNullOrZero(amount)) {
				addWaivedBillingScheduleWarning(config, scheduleConfig, note.toString());
			}
			else {
				// Already checked there is only one invoice
				BillingInvoice invoice = scheduleConfig.getInvoiceList().get(0);
				BillingInvoiceDetail detail = createInvoiceDetail(invoice, scheduleConfig, BigDecimal.ZERO, amount, note.toString());
				List<BillingInvoiceDetailAccount> detailAccountList = getBillingInvoiceDetailAccountCalculator().calculateBillingInvoiceDetailAccountList(invoice, detail, null, null, null,
						scheduleConfig.getProportionalSplitAccountList(invoice.getId()));
				applyInvoiceDetailAccountListToInvoiceDetail(detail, detailAccountList);
				config.addInvoiceDetail(detail);
			}
		}
	}


	////////////////////////////////////////////////////////////
	////////           Calculate Methods               /////////
	////////////////////////////////////////////////////////////


	protected BigDecimal calculateAmount(BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail, StringBuilder note) {
		BigDecimal amount = calculateAmountImpl(scheduleConfig, billingBasisDetail, note);
		if (!MathUtils.isNullOrZero(amount) && getBillingAmountProraterBean() != null) {
			BillingAmountProrater prorater = (BillingAmountProrater) getSystemBeanService().getBeanInstance(getBillingAmountProraterBean());
			if (prorater != null) {
				amount = prorater.prorateCalculatedAmount(amount, scheduleConfig, billingBasisDetail, note);
			}
		}
		return amount;
	}


	public abstract BigDecimal calculateAmountImpl(BillingScheduleProcessConfig scheduleConfig, BillingBasisDetail billingBasisDetail, StringBuilder note);


	private void setupBillingBasisForProcessing(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig) {
		BillingSchedule schedule = scheduleConfig.getSchedule();
		if (isBillingBasisUsed(scheduleConfig.getSchedule())) {
			List<InvestmentAccount> accountList = null;
			if (schedule.getInvestmentAccount() != null) {
				accountList = CollectionUtils.createList(schedule.getInvestmentAccount());
			}
			else if (schedule.getInvestmentAccountGroup() != null) {
				InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
				searchForm.setOurAccount(true);
				searchForm.setInvestmentAccountGroupId(schedule.getInvestmentAccountGroup().getId());
				accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
			}


			for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> accountBillingBasisEntry : config.getBillingBasisMap().entrySet()) {
				if (isBillingDefinitionInvestmentAccountProcessedForSchedule(accountBillingBasisEntry.getKey(), scheduleConfig, accountList)) {
					String aggregationLevel = getAggregationValue(accountBillingBasisEntry.getKey(), schedule);
					scheduleConfig.addBillingBasis(aggregationLevel, accountBillingBasisEntry.getKey(), accountBillingBasisEntry.getValue());
					scheduleConfig.getAggregationLevelLabelMap().putIfAbsent(aggregationLevel, getAggregationLabel(accountBillingBasisEntry.getKey(), schedule));
				}
			}
		}
		else {
			// Apply Fixed Fees evenly at the account level (Not proportional to billing basis as they may not use a basis)
			// Also, for cases without specific account/schedule defined (and not min/max) will allocate to accounts with positions on during the invoice period only
			for (BillingInvoice invoice : config.getInvoiceList()) {
				// Specific account on the schedule - that account gets the full allocation
				if (schedule.getInvestmentAccount() != null) {
					if (schedule.getBillingDefinition().equals(invoice.getBillingDefinition())) {
						scheduleConfig.addProportionalSplitAccountToList(invoice.getId(), schedule.getInvestmentAccount());
					}
				}
				else {
					for (BillingDefinitionInvestmentAccount billingDefinitionAccount : CollectionUtils.getIterable(invoice.getBillingDefinition().getInvestmentAccountList())) {
						// If the billing definition investment account is tied to a specific schedule, but it's not this schedule then skip it
						if (isBillingDefinitionInvestmentAccountProcessedForSchedule(billingDefinitionAccount, scheduleConfig, null)) {
							scheduleConfig.addProportionalSplitAccountToList(invoice.getId(), billingDefinitionAccount.getReferenceTwo());
						}
					}
				}
			}
		}
	}


	private String getAggregationValue(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, BillingSchedule billingSchedule) {
		if (billingSchedule.getBillingBasisAggregationType() == null) {
			return "ALL";
		}
		String prefix = "";
		if (billingSchedule.isSharedSchedule() && billingSchedule.isDoNotGroupInvoice()) {
			prefix = billingDefinitionInvestmentAccount.getReferenceOne().getId() + "_";
		}
		Object value = BeanUtils.getPropertyValue(billingDefinitionInvestmentAccount, billingSchedule.getBillingBasisAggregationType().getAggregationBeanFieldPath());
		return prefix + ObjectUtils.toString(value);
	}


	private String getAggregationLabel(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, BillingSchedule billingSchedule) {
		if (billingSchedule.getBillingBasisAggregationType() == null || StringUtils.isEmpty(billingSchedule.getBillingBasisAggregationType().getAggregationLabelTemplate())) {
			return null;
		}
		TemplateConfig config = new TemplateConfig(billingSchedule.getBillingBasisAggregationType().getAggregationLabelTemplate());
		config.addBeanToContext("bean", billingDefinitionInvestmentAccount);
		config.addBeanToContext("schedule", billingSchedule);
		return getTemplateConverter().convert(config);
	}


	////////////////////////////////////////////////////////////
	//////    Allocation to Invoice(s) Methods           ///////
	////////////////////////////////////////////////////////////


	protected void createInvoiceDetailAccountListFromPreviouslyBilledAllocations(BillingInvoiceDetail detail, Map<InvestmentAccount, BigDecimal> accountAllocationMap) {
		List<BillingInvoiceDetailAccount> accountList = new ArrayList<>();
		BigDecimal total = CoreMathUtils.sum(accountAllocationMap.values());
		for (Map.Entry<InvestmentAccount, BigDecimal> investmentAccountBigDecimalEntry : accountAllocationMap.entrySet()) {
			BigDecimal percentage = CoreMathUtils.getPercentValue(investmentAccountBigDecimalEntry.getValue(), total, true);
			BillingInvoiceDetailAccount da = new BillingInvoiceDetailAccount();
			da.setInvoiceDetail(detail);
			da.setAllocationPercentage(MathUtils.round(percentage, 4));
			da.setBillingAmount(MathUtils.round(MathUtils.getPercentageOf(percentage, detail.getBillingAmount(), true), 2));
			da.setBillingBasis(BigDecimal.ZERO);
			da.setInvestmentAccount(investmentAccountBigDecimalEntry.getKey());
			accountList.add(da);
		}
		applyInvoiceDetailAccountListToInvoiceDetail(detail, accountList);
	}


	protected BillingInvoiceDetail createInvoiceDetail(BillingInvoice invoice, BillingScheduleProcessConfig scheduleConfig, BigDecimal billingBasisAmount, BigDecimal
			amount, String note) {
		BillingInvoiceDetail detail = new BillingInvoiceDetail();
		detail.setAccrualStartDate(scheduleConfig.getAccrualStartDate());
		detail.setAccrualEndDate(scheduleConfig.getAccrualEndDate());
		detail.setBillingSchedule(scheduleConfig.getSchedule());
		detail.setInvoice(invoice);
		detail.setBillingBasis(billingBasisAmount == null ? BigDecimal.ZERO : billingBasisAmount);
		detail.setBillingAmount(BillingAmountCalculatorUtils.roundBillingAmount(scheduleConfig, amount, false));
		detail.setNote(note);
		return detail;
	}


	////////////////////////////////////////////////////////////////////////////////


	private void applyInvoiceDetailAccountListToInvoiceDetail(BillingInvoiceDetail detail, List<BillingInvoiceDetailAccount> detailAccountList) {
		// Ensure % split equals 100% and amount split equals total billed
		// Note: Percentages are rounded to 4 decimal points
		if (CollectionUtils.isEmpty(detailAccountList)) {
			throw new ValidationException("Unable to allocate schedule [" + detail.getBillingSchedule().getName()
					+ "] to accounts.  Please check the billing setup as there are no accounts that currently apply to this schedule.");
		}
		CoreMathUtils.applySumPropertyDifference(detailAccountList, BillingInvoiceDetailAccount::getAllocationPercentage, BillingInvoiceDetailAccount::setAllocationPercentage, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
		CoreMathUtils.applySumPropertyDifference(detailAccountList, BillingInvoiceDetailAccount::getBillingAmount, BillingInvoiceDetailAccount::setBillingAmount, detail.getBillingAmount(), true);

		detail.setDetailAccountList(detailAccountList);
	}


	////////////////////////////////////////////////////////////
	//////////           Helper Methods               //////////
	////////////////////////////////////////////////////////////


	/**
	 * Returns true if the account should be processed for the schedule.
	 */
	private boolean isBillingDefinitionInvestmentAccountProcessedForSchedule(BillingDefinitionInvestmentAccount billingDefinitionAccount, BillingScheduleProcessConfig scheduleConfig, List<InvestmentAccount> accountList) {
		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			// Explicit Selection - Include It If Equal
			if (billingDefinitionAccount.getBillingSchedule() != null) {
				return scheduleConfig.getSchedule().equals(billingDefinitionAccount.getBillingSchedule());
			}

			// If the billing definition account is NOT active during the invoice period - skip it
			if (!DateUtils.isOverlapInDates(billingDefinitionAccount.getStartDate(), billingDefinitionAccount.getEndDate(), invoice.getInvoicePeriodStartDate(), invoice.getInvoicePeriodEndDate())) {
				return false;
			}

			// If Account List is not empty then if account is in the list and billing definition is included in the invoice list
			if (!CollectionUtils.isEmpty(accountList)) {
				for (InvestmentAccount clientAccount : accountList) {
					if (billingDefinitionAccount.getReferenceTwo().equals(clientAccount) && billingDefinitionAccount.getReferenceOne().equals(invoice.getBillingDefinition())) {
						return true;
					}
				}
			}

			// If it's billing definition matches the invoice include it
			else if (billingDefinitionAccount.getReferenceOne().equals(invoice.getBillingDefinition())) {
				return true;
			}
		}
		return false;
	}


	protected void addWaivedBillingScheduleWarning(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig, String msg) {
		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			config.setInvoiceScheduleWaivedViolation(invoice.getId(), scheduleConfig.getSchedule(), " does not apply because [" + msg + "]");
		}
	}


	protected void addProcessingBillingScheduleWarning(BillingInvoiceProcessConfig config, BillingScheduleProcessConfig scheduleConfig, String msg) {
		for (BillingInvoice invoice : CollectionUtils.getIterable(scheduleConfig.getInvoiceList())) {
			config.setInvoiceScheduleProcessingViolation(invoice.getId(), scheduleConfig.getSchedule(), msg);
		}
	}


	/**
	 * Note: This will get the pre-requisite invoices, but it won't fail if any are missing.  This allows the invoice to be processed without them, but they
	 * are required prior to Approval.  And the prerequisite invoices will re-process this invoice once they are approved.
	 * We don't require it in the interim because could be generating an invoice early for testing (or projected revenue)
	 * Returns account level breakdown OF BILLING AMOUNTS in case the calling method needs to apply the amount to the original accounts that were billed
	 * if revenueTypeList is populated will limit results to those invoice details
	 */
	protected Map<InvestmentAccount, BigDecimal> getPrerequisiteInvoiceAccountFees(BillingScheduleProcessConfig scheduleConfig, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return getPrerequisiteInvoiceAccountAmountsImpl(scheduleConfig, false, revenueTypeList);
	}


	/**
	 * Note: This will get the pre-requisite invoices, but it won't fail if any are missing.  This allows the invoice to be processed without them, but they
	 * are required prior to Approval.  And the prerequisite invoices will re-process this invoice once they are approved.
	 * We don't require it in the interim because could be generating an invoice early for testing (or projected revenue)
	 * Returns account level breakdown of BILLING BASIS in case the calling method needs to apply the amount to the original accounts that were billed
	 */
	protected Map<InvestmentAccount, BigDecimal> getPrerequisiteInvoiceAccountBillingBasis(BillingScheduleProcessConfig scheduleConfig, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		return getPrerequisiteInvoiceAccountAmountsImpl(scheduleConfig, true, revenueTypeList);
	}


	private Map<InvestmentAccount, BigDecimal> getPrerequisiteInvoiceAccountAmountsImpl(BillingScheduleProcessConfig scheduleConfig, boolean useBillingBasis, List<BillingInvoiceRevenueTypes> revenueTypeList) {
		List<BillingScheduleBillingDefinitionDependency> dependencyList = getBillingDefinitionService().getBillingScheduleBillingDefinitionDependencyListBySchedule(scheduleConfig.getSchedule().getId());
		ValidationUtils.assertNotEmpty(dependencyList, "Schedule " + scheduleConfig.getSchedule().getName() + " cannot apply a credit for other management fees paid because there are no definition dependencies set up for the schedule.");

		BillingInvoiceDetailAccountSearchForm detailAccountSearchForm = new BillingInvoiceDetailAccountSearchForm();
		detailAccountSearchForm.setBillingDefinitionIds(BeanUtils.getPropertyValuesUniqueExcludeNull(dependencyList, dependency -> dependency.getBillingDefinition().getId(), Integer.class));
		detailAccountSearchForm.addSearchRestriction(new SearchRestriction("invoiceDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, scheduleConfig.getInvoicePeriodStartDate()));
		detailAccountSearchForm.addSearchRestriction(new SearchRestriction("invoiceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, scheduleConfig.getInvoicePeriodEndDate()));
		detailAccountSearchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CANCELED); // Always exclude Voided invoices
		if (!CollectionUtils.isEmpty(revenueTypeList)) {
			if (revenueTypeList.size() == 1) {
				detailAccountSearchForm.setRevenueType(revenueTypeList.get(0));
			}
			else {
				detailAccountSearchForm.setRevenueTypes(CollectionUtils.toArray(revenueTypeList, BillingInvoiceRevenueTypes.class));
			}
		}

		List<BillingInvoiceDetailAccount> fullDetailAccountList = getBillingInvoiceService().getBillingInvoiceDetailAccountList(detailAccountSearchForm);
		List<BillingInvoiceDetailAccount> detailAccountList = new ArrayList<>();

		for (BillingInvoiceDetailAccount detailAccount : CollectionUtils.getIterable(fullDetailAccountList)) {
			// If using an accrual frequency confirm all details also use an accrual frequency and details are for shorter or equal time periods
			if (scheduleConfig.getSchedule().getAccrualFrequency() != null) {
				ValidationUtils.assertNotNull(detailAccount.getInvoiceDetail().getBillingSchedule().getAccrualFrequency(), "Cannot apply an accrual frequency to [" + scheduleConfig.getSchedule().getName() + "] because there is at least one dependent invoice detail  [" + detailAccount.getInvoiceDetail().getLabel() + "] that doesn't use accrual frequencies.");
				ValidationUtils.assertTrue(scheduleConfig.getSchedule().getAccrualFrequency().getDaysInPeriod() >= detailAccount.getInvoiceDetail().getBillingSchedule().getAccrualFrequency().getDaysInPeriod(), "Cannot apply an accrual frequency of [" + scheduleConfig.getSchedule().getAccrualFrequency().name() + "] to schedule [" + scheduleConfig.getSchedule().getName() + "] because there is at least one invoice detail  [" + detailAccount.getInvoiceDetail().getLabel() + "] that uses an accrual frequency of a longer period.  Unable to apply dependencies at shorter intervals.");
				// If setup passes, then include only if an overlap in dates
				if (!DateUtils.isOverlapInDates(scheduleConfig.getAccrualStartDate(), scheduleConfig.getAccrualEndDate(), detailAccount.getInvoiceDetail().getAccrualStartDate(), detailAccount.getInvoiceDetail().getAccrualEndDate())) {
					continue;
				}
			}
			detailAccountList.add(detailAccount);
		}


		Map<InvestmentAccount, BigDecimal> accountBillingMap = new HashMap<>();
		for (BillingInvoiceDetailAccount detailAccount : CollectionUtils.getIterable(detailAccountList)) {
			if (isIncludeBillingInvoiceDetailAccount(detailAccount, dependencyList)) {
				if (!accountBillingMap.containsKey(detailAccount.getInvestmentAccount())) {
					accountBillingMap.put(detailAccount.getInvestmentAccount(), BigDecimal.ZERO);
				}
				accountBillingMap.put(detailAccount.getInvestmentAccount(), MathUtils.add(accountBillingMap.get(detailAccount.getInvestmentAccount()), (useBillingBasis ? detailAccount.getBillingBasis() : detailAccount.getBillingAmount())));
			}
		}
		return accountBillingMap;
	}


	private boolean isIncludeBillingInvoiceDetailAccount(BillingInvoiceDetailAccount detailAccount, List<BillingScheduleBillingDefinitionDependency> dependencyList) {
		BillingDefinition detailAccountDefinition = detailAccount.getInvoiceDetail().getInvoice().getBillingDefinition();
		for (BillingScheduleBillingDefinitionDependency dependency : dependencyList) {
			if (dependency.getBillingDefinition().equals(detailAccountDefinition)) {
				if (dependency.getBillingDefinitionInvestmentAccount() == null && dependency.getInvestmentAccount() == null) {
					return true;
				}
				if (dependency.getBillingDefinitionInvestmentAccount() != null && CompareUtils.isEqual(dependency.getBillingDefinitionInvestmentAccount(), detailAccount.getBillingDefinitionInvestmentAccount())) {
					return true;
				}
				if (dependency.getInvestmentAccount() != null && CompareUtils.isEqual(dependency.getInvestmentAccount(), detailAccount.getInvestmentAccount())) {
					return true;
				}
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemBean getBillingAmountProraterBean() {
		return this.billingAmountProraterBean;
	}


	public void setBillingAmountProraterBean(SystemBean billingAmountProraterBean) {
		this.billingAmountProraterBean = billingAmountProraterBean;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public BillingInvoiceDetailAccountCalculator getBillingInvoiceDetailAccountCalculator() {
		return this.billingInvoiceDetailAccountCalculator;
	}


	public void setBillingInvoiceDetailAccountCalculator(BillingInvoiceDetailAccountCalculator billingInvoiceDetailAccountCalculator) {
		this.billingInvoiceDetailAccountCalculator = billingInvoiceDetailAccountCalculator;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
