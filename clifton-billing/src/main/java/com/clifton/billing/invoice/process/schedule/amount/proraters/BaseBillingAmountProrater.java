package com.clifton.billing.invoice.process.schedule.amount.proraters;

import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public abstract class BaseBillingAmountProrater implements BillingAmountProrater {

	/**
	 * If false prorates for the number of days
	 * If true prorates for the number of months
	 * i.e. 1 month could have 31 days - so for a quarter using days the calculation would be 31/number of days in the quarter
	 * and uses months would be 1/3 (# of months in the quarter)
	 */
	private boolean prorateForMonths;

	/**
	 * When prorating for months and dates are for a partial month - i.e. 1 month + 15 days
	 * can either charge for full 2 months (2/3), or 1 month and 15 days (1/3) + ((1/3) + 15/days in month)
	 */
	private boolean prorateForPartialMonths;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal prorateCalculatedAmount(BigDecimal amount, BillingScheduleProcessConfig scheduleConfig, BillingBasis.BillingBasisDetail billingBasisDetail, StringBuilder note) {
		BillingAmountDateProraterConfig proraterConfig = new BillingAmountDateProraterConfig(scheduleConfig, isProrateForMonths(), isProrateForPartialMonths());
		populateBillingAmountDateProraterConfig(proraterConfig, scheduleConfig, billingBasisDetail);
		return BillingAmountCalculatorUtils.roundBillingAmount(scheduleConfig, proraterConfig.prorateAmount(amount, note), false);
	}


	protected abstract void populateBillingAmountDateProraterConfig(BillingAmountDateProraterConfig proraterConfig, BillingScheduleProcessConfig scheduleConfig, BillingBasis.BillingBasisDetail billingBasisDetail);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isProrateForMonths() {
		return this.prorateForMonths;
	}


	public void setProrateForMonths(boolean prorateForMonths) {
		this.prorateForMonths = prorateForMonths;
	}


	public boolean isProrateForPartialMonths() {
		return this.prorateForPartialMonths;
	}


	public void setProrateForPartialMonths(boolean prorateForPartialMonths) {
		this.prorateForPartialMonths = prorateForPartialMonths;
	}
}
