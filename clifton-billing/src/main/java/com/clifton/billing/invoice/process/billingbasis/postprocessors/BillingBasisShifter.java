package com.clifton.billing.invoice.process.billingbasis.postprocessors;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis.BillingBasisDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisShifter</code> can move billing basis from a specific billing definition investment account, or investment account, or account group
 * to the billing basis for a different billing definition investment account, investment account, or account group
 * Will move as much as is available if the billing basis in the "to" hasn't reached the minimum and only until the "to" reaches the minimum
 *
 * @author Mary Anderson
 */
public class BillingBasisShifter implements BillingBasisPostProcessor {

	private BillingDefinitionService billingDefinitionService;
	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private Integer fromBillingDefinitionInvestmentAccountId;
	private Integer fromInvestmentAccountId;
	private Integer fromInvestmentAccountGroupId;

	private Integer toBillingDefinitionInvestmentAccountId;
	private Integer toInvestmentAccountId;
	private Integer toInvestmentAccountGroupId;

	private BigDecimal minimumAmountInToAccount;

	private String note;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap) {
		// Since there are multiple options for From and To we can't validate them as required in the system bean, so verify first something is selected for From and To
		validate();
		List<BillingBasis> fromBillingBasisList = getBillingBasisFrom(billingBasisMap);
		List<BillingBasis> toBillingBasisList = getBillingBasisTo(billingBasisMap);

		// Merge Billing Basis to properly sum/compare values
		BillingBasis fromBillingBasis = new BillingBasis();
		for (BillingBasis bb : CollectionUtils.getIterable(fromBillingBasisList)) {
			fromBillingBasis.addBillingBasis(bb);
		}

		BillingBasis toBillingBasis = new BillingBasis();
		for (BillingBasis bb : CollectionUtils.getIterable(toBillingBasisList)) {
			toBillingBasis.addBillingBasis(bb);
		}

		for (BillingBasisDetail bbd : CollectionUtils.getIterable(toBillingBasis.getDetailList())) {
			if (MathUtils.isGreaterThan(bbd.getTotalAmount(), getMinimumAmountInToAccount())) {
				continue;
			}
			BigDecimal shift = BigDecimal.ZERO;
			BillingBasisDetail fromBbd = fromBillingBasis.getBillingBasisDetail(bbd.getStartDate(), bbd.getEndDate());
			if (fromBbd != null) {
				BigDecimal maxDifference = MathUtils.subtract(getMinimumAmountInToAccount(), bbd.getTotalAmount());
				if (MathUtils.isLessThan(fromBbd.getTotalAmount(), maxDifference)) {
					shift = fromBbd.getTotalAmount();
				}
				else {
					shift = maxDifference;
				}
			}
			if (!MathUtils.isNullOrZero(shift)) {
				adjustBillingBasis(bbd.getStartDate(), bbd.getEndDate(), toBillingBasisList, shift);
				adjustBillingBasis(bbd.getStartDate(), bbd.getEndDate(), fromBillingBasisList, MathUtils.negate(shift));
			}
		}
	}


	private void validate() {
		if (getFromBillingDefinitionInvestmentAccountId() == null) {
			if (getFromInvestmentAccountId() == null) {
				if (getFromInvestmentAccountGroupId() == null) {
					throw new ValidationException("Cannot process Billing Basis Shifter because there is no FROM selection.");
				}
			}
		}
		if (getToBillingDefinitionInvestmentAccountId() == null) {
			if (getToInvestmentAccountId() == null) {
				if (getToInvestmentAccountGroupId() == null) {
					throw new ValidationException("Cannot process Billing Basis Shifter because there is no TO selection.");
				}
			}
		}
	}


	private List<BillingBasis> getBillingBasisFrom(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap) {
		return getBillingBasis(billingBasisMap, getFromBillingDefinitionInvestmentAccountId(), getFromInvestmentAccountId(), getFromInvestmentAccountGroupId());
	}


	private List<BillingBasis> getBillingBasisTo(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap) {
		return getBillingBasis(billingBasisMap, getToBillingDefinitionInvestmentAccountId(), getToInvestmentAccountId(), getToInvestmentAccountGroupId());
	}


	private List<BillingBasis> getBillingBasis(Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap, Integer billingDefinitionInvestmentAccountId, Integer investmentAccountId,
	                                           Integer investmentAccountGroupId) {

		List<InvestmentAccount> accountList = null;
		if (investmentAccountGroupId != null) {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setInvestmentAccountGroupId(investmentAccountGroupId);
			accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		}
		List<BillingBasis> billingBasisList = new ArrayList<>();
		for (Map.Entry<BillingDefinitionInvestmentAccount, BillingBasis> billingDefinitionInvestmentAccountBillingBasisEntry : billingBasisMap.entrySet()) {

			if (billingDefinitionInvestmentAccountId != null && CompareUtils.isEqual((billingDefinitionInvestmentAccountBillingBasisEntry.getKey()).getId(), billingDefinitionInvestmentAccountId)) {
				billingBasisList.add(billingDefinitionInvestmentAccountBillingBasisEntry.getValue());
			}
			else if (investmentAccountId != null && CompareUtils.isEqual((billingDefinitionInvestmentAccountBillingBasisEntry.getKey()).getReferenceTwo().getId(), investmentAccountId)) {
				billingBasisList.add(billingDefinitionInvestmentAccountBillingBasisEntry.getValue());
			}
			else {
				for (InvestmentAccount acct : CollectionUtils.getIterable(accountList)) {
					if (CompareUtils.isEqual(acct.getId(), (billingDefinitionInvestmentAccountBillingBasisEntry.getKey()).getReferenceTwo().getId())) {
						billingBasisList.add(billingDefinitionInvestmentAccountBillingBasisEntry.getValue());
						break;
					}
				}
			}
		}
		return billingBasisList;
	}


	private void adjustBillingBasis(Date start, Date end, List<BillingBasis> bbList, BigDecimal amount) {
		BillingBasis first = CollectionUtils.getFirstElement(bbList);
		// Based on calling method, should never be null
		if (first != null) {
			String adjustNote = "Applied Billing Basis Adjustment of [" + CoreMathUtils.formatNumberInteger(amount) + "]";
			if (!StringUtils.isEmpty(getNote())) {
				adjustNote += " because [" + getNote() + "]";
			}
			first.addBillingBasis(start, end, amount, adjustNote + StringUtils.NEW_LINE);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Integer getFromBillingDefinitionInvestmentAccountId() {
		return this.fromBillingDefinitionInvestmentAccountId;
	}


	public void setFromBillingDefinitionInvestmentAccountId(Integer fromBillingDefinitionInvestmentAccountId) {
		this.fromBillingDefinitionInvestmentAccountId = fromBillingDefinitionInvestmentAccountId;
	}


	public Integer getToBillingDefinitionInvestmentAccountId() {
		return this.toBillingDefinitionInvestmentAccountId;
	}


	public void setToBillingDefinitionInvestmentAccountId(Integer toBillingDefinitionInvestmentAccountId) {
		this.toBillingDefinitionInvestmentAccountId = toBillingDefinitionInvestmentAccountId;
	}


	public BigDecimal getMinimumAmountInToAccount() {
		return this.minimumAmountInToAccount;
	}


	public void setMinimumAmountInToAccount(BigDecimal minimumAmountInToAccount) {
		this.minimumAmountInToAccount = minimumAmountInToAccount;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public Integer getFromInvestmentAccountId() {
		return this.fromInvestmentAccountId;
	}


	public void setFromInvestmentAccountId(Integer fromInvestmentAccountId) {
		this.fromInvestmentAccountId = fromInvestmentAccountId;
	}


	public Integer getFromInvestmentAccountGroupId() {
		return this.fromInvestmentAccountGroupId;
	}


	public void setFromInvestmentAccountGroupId(Integer fromInvestmentAccountGroupId) {
		this.fromInvestmentAccountGroupId = fromInvestmentAccountGroupId;
	}


	public Integer getToInvestmentAccountId() {
		return this.toInvestmentAccountId;
	}


	public void setToInvestmentAccountId(Integer toInvestmentAccountId) {
		this.toInvestmentAccountId = toInvestmentAccountId;
	}


	public Integer getToInvestmentAccountGroupId() {
		return this.toInvestmentAccountGroupId;
	}


	public void setToInvestmentAccountGroupId(Integer toInvestmentAccountGroupId) {
		this.toInvestmentAccountGroupId = toInvestmentAccountGroupId;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
