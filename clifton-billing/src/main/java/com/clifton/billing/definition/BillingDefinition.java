package com.clifton.billing.definition;


import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.BaseWorkflowAwareEntity;

import java.util.Date;
import java.util.List;


/**
 * The <code>BillingDefinition</code> class defines a single billing definition.
 * A separate invoice is generated for each billing definition.  One or more client
 * accounts maybe included in a single billing definition.
 * <p>
 *
 * @author vgomelsky
 */
public class BillingDefinition extends BaseWorkflowAwareEntity<Integer> implements SystemColumnCustomValueAware {

	public static final String BILLING_DEFINITION_TABLE_NAME = "BillingDefinition";
	public static final String BILLING_DEFINITION_CUSTOM_FIELD_GROUP = "Billing Definition Custom Fields";

	public static final String FIELD_BILLING_INVOICE_REPORT = "Billing Invoice Report";

	public static final String ARCHIVED_WORKFLOW_STATE_NAME = "Archived";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The company this billing definition is for.  This is usually a Client (for Institutional Clients) or a Broker (for Retail Clients)
	 * Who gets the invoice
	 */
	private BusinessCompany businessCompany;

	/**
	 * This is the contract that outlines the fee schedules and various legal documentation regarding the billing setup
	 * For clients, this is usually the IMS or Guidelines
	 */
	private BusinessContract billingContract;


	/**
	 * The currency the invoice is billed in.  Usually USD
	 */
	private InvestmentSecurity billingCurrency;

	/**
	 * The frequency of the invoices.  Most invoices are generated quarterly with the ability to accrue fees in shorter intervals (i.e. monthly accruals - see the Billing Schedule for that option)
	 */
	private BillingFrequencies billingFrequency;

	/**
	 * The type of invoice this billing definition generates, i.e. Management Fee Invoice  where the invoiceType.IsBillingDefinitionRequired = true
	 */
	private BillingInvoiceType invoiceType;

	/**
	 * Required if any schedules on the definition apply to Revenue Share External bucket (i.e. Broker Fee)
	 * "Who" gets the external revenue share amount
	 */
	private BusinessCompany revenueShareExternalCompany;

	/**
	 * Required if any schedules on the definition apply to Revenue Share Internal bucket (i.e. Revenue Share)
	 * "Who" gets the amount - should be PPA or EV company
	 */
	private BusinessCompany revenueShareInternalCompany;

	/**
	 * Used by workflow to determine if the the invoice is posted to the portal.
	 * There is a default option defined based on the invoice type, and can only be true if the billing definition company is a client or client relationship (i.e. somewhere to post it to)
	 */
	private boolean invoicePostedToPortal;

	/**
	 * arrears vs. advance
	 * Most cases payments are made in arrears (after services rendered), but some cases billing is charged in advance (before services rendered)
	 * In cases where charged in advance, the valuation is done for the last day of the previous quarter.  i.e. 7/1 - 9/30 quarter billing is generated from 6/30 values
	 */
	private boolean paymentInAdvance;

	/**
	 * The first day of the first billing period.
	 */
	private Date startDate;

	/**
	 * Second billing year start date could be exactly one year after start date, January 1st a year after start date or any day.
	 * If billing definition allows maximum annual billing amount and startDate is close to year end, we may count remainder of the
	 * year as well as full next year as the first billing year.
	 * This is used to determine the annual cycles for annual minimum and maximum fees
	 */
	private Date secondYearStartDate;

	/**
	 * The date when we stop billing client account(s).  Initially it's null and populated only when:
	 * Billing terms change drastically that a new billing definition will be setup
	 * Client terminates.
	 */
	private Date endDate;


	private String note;

	/**
	 * A list of client accounts that this billing definition is billed for and how they are valued
	 */
	private List<BillingDefinitionInvestmentAccount> investmentAccountList;

	/**
	 * Non-shared Schedule's associated with this billing definition. They are specific to this billing definition
	 * and apply in addition to the shared schedule.
	 */
	private List<BillingSchedule> billingScheduleList;

	/**
	 * Shared schedule(s) associated with this billing definition. These schedules are usually associated with other
	 * billing definitions in order to get better pricing using economies of scale.
	 * Usually none or one shared schedule per definition.
	 */
	private List<BillingSchedule> billingScheduleSharedList;

	/**
	 * A List of custom column values for the definition
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * By Default - Because Billing Definition names (contracts are long and very similar)
	 * we include the Billing Definition ID in the label so it's easy to differentiate between them.
	 */
	public String getLabel() {
		return getLabelImpl(true);
	}


	public String getLabelWithoutId() {
		return getLabelImpl(false);
	}


	private String getLabelImpl(boolean prependId) {
		StringBuilder sb = new StringBuilder();
		if (prependId && !isNewBean()) {
			sb.append(getId()).append(": ");
		}
		if (getBillingContract() != null) {
			sb.append(getBillingContract().getName());
		}
		sb.append(" ").append(DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false));
		return sb.toString();
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	/**
	 * Used for global search return notes for the tooltip field
	 */
	public String getDescription() {
		return getNote();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public BusinessContract getBillingContract() {
		return this.billingContract;
	}


	public void setBillingContract(BusinessContract billingContract) {
		this.billingContract = billingContract;
	}


	public BillingFrequencies getBillingFrequency() {
		return this.billingFrequency;
	}


	public void setBillingFrequency(BillingFrequencies billingFrequency) {
		this.billingFrequency = billingFrequency;
	}


	public BillingInvoiceType getInvoiceType() {
		return this.invoiceType;
	}


	public void setInvoiceType(BillingInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}


	public BusinessCompany getRevenueShareExternalCompany() {
		return this.revenueShareExternalCompany;
	}


	public void setRevenueShareExternalCompany(BusinessCompany revenueShareExternalCompany) {
		this.revenueShareExternalCompany = revenueShareExternalCompany;
	}


	public BusinessCompany getRevenueShareInternalCompany() {
		return this.revenueShareInternalCompany;
	}


	public void setRevenueShareInternalCompany(BusinessCompany revenueShareInternalCompany) {
		this.revenueShareInternalCompany = revenueShareInternalCompany;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getSecondYearStartDate() {
		return this.secondYearStartDate;
	}


	public void setSecondYearStartDate(Date secondYearStartDate) {
		this.secondYearStartDate = secondYearStartDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentSecurity getBillingCurrency() {
		return this.billingCurrency;
	}


	public void setBillingCurrency(InvestmentSecurity billingCurrency) {
		this.billingCurrency = billingCurrency;
	}


	public List<BillingDefinitionInvestmentAccount> getInvestmentAccountList() {
		return this.investmentAccountList;
	}


	public void setInvestmentAccountList(List<BillingDefinitionInvestmentAccount> investmentAccountList) {
		this.investmentAccountList = investmentAccountList;
	}


	public List<BillingSchedule> getBillingScheduleList() {
		return this.billingScheduleList;
	}


	public void setBillingScheduleList(List<BillingSchedule> billingScheduleList) {
		this.billingScheduleList = billingScheduleList;
	}


	public List<BillingSchedule> getBillingScheduleSharedList() {
		return this.billingScheduleSharedList;
	}


	public void setBillingScheduleSharedList(List<BillingSchedule> billingScheduleSharedList) {
		this.billingScheduleSharedList = billingScheduleSharedList;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public boolean isPaymentInAdvance() {
		return this.paymentInAdvance;
	}


	public void setPaymentInAdvance(boolean paymentInAdvance) {
		this.paymentInAdvance = paymentInAdvance;
	}


	public boolean isInvoicePostedToPortal() {
		return this.invoicePostedToPortal;
	}


	public void setInvoicePostedToPortal(boolean invoicePostedToPortal) {
		this.invoicePostedToPortal = invoicePostedToPortal;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
