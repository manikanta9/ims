package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleBillingDefinitionDependencyListByScheduleCache</code> caches the list of dependencies for the given schedule
 *
 * @author manderson
 */
@Component
public class BillingScheduleBillingDefinitionDependencyListByScheduleCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleBillingDefinitionDependency, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "billingSchedule.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleBillingDefinitionDependency bean) {
		if (bean.getBillingSchedule() != null) {
			return bean.getBillingSchedule().getId();
		}
		return null;
	}
}
