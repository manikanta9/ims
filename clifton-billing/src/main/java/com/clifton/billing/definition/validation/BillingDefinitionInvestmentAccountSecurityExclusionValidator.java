package com.clifton.billing.definition.validation;

import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSecurityExclusionSearchForm;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class BillingDefinitionInvestmentAccountSecurityExclusionValidator extends SelfRegisteringDaoValidator<BillingDefinitionInvestmentAccountSecurityExclusion> {

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BillingDefinitionInvestmentAccountSecurityExclusion bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO method
	}


	@Override
	public void validate(BillingDefinitionInvestmentAccountSecurityExclusion bean, DaoEventTypes config, ReadOnlyDAO<BillingDefinitionInvestmentAccountSecurityExclusion> dao) throws ValidationException {
		// Validate Security and/or Condition is Populated - both cannot be null
		ValidationUtils.assertFalse(bean.getExcludeCondition() == null && bean.getInvestmentSecurity() == null, "Security and Exclude Condition cannot be blank.  At least one option must be selected.");

		// Validate account valuation supports security level details
		ValidationUtils.assertTrue(bean.getBillingDefinitionInvestmentAccount().getBillingBasisValuationType().isInvestmentGroupAllowed(), "Selected billing account " + bean.getBillingDefinitionInvestmentAccount().getAccountLabel() + " doesn't support security level valuation.  Security exclusions are not allowed for valuation type " + bean.getBillingDefinitionInvestmentAccount().getBillingBasisValuationType().getName() + ".");

		// Validate start and end dates
		ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");

		// Find existing for Billing Definition Investment Account, Security, Condition, and overlapping date range
		BillingDefinitionInvestmentAccountSecurityExclusionSearchForm securityExclusionSearchForm = new BillingDefinitionInvestmentAccountSecurityExclusionSearchForm();
		securityExclusionSearchForm.setBillingDefinitionInvestmentAccountId(bean.getBillingDefinitionInvestmentAccount().getId());
		if (bean.getInvestmentSecurity() != null) {
			securityExclusionSearchForm.setInvestmentSecurityId(bean.getInvestmentSecurity().getId());
		}
		if (bean.getExcludeCondition() != null) {
			securityExclusionSearchForm.setExcludeConditionId(bean.getExcludeCondition().getId());
		}

		List<BillingDefinitionInvestmentAccountSecurityExclusion> existingList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountSecurityExclusionList(securityExclusionSearchForm);
		for (BillingDefinitionInvestmentAccountSecurityExclusion existing : CollectionUtils.getIterable(existingList)) {
			if (!existing.equals(bean)) {
				if (DateUtils.isOverlapInDates(existing.getStartDate(), existing.getEndDate(), bean.getStartDate(), bean.getEndDate())) {
					throw new ValidationException("There already existing a security exclusion for this billing account for the same security and/or exclude condition and overlapping date range: " + existing.getExcludeLabel());
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
