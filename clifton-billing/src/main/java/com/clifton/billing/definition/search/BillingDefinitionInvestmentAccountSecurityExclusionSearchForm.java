package com.clifton.billing.definition.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author manderson
 */
public class BillingDefinitionInvestmentAccountSecurityExclusionSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {


	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "referenceOne.id")
	private Integer definitionId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "referenceOne.id")
	private Integer[] definitionIds;

	@SearchField(searchField = "billingDefinitionInvestmentAccount.id")
	private Integer billingDefinitionInvestmentAccountId;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.referenceTwo", searchField = "number,name")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount", searchField = "billingBasisValuationTypeDescription")
	private String billingBasisValuationTypeDescription;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.billingBasisValuationType", searchField = "name")
	private String billingBasisValuationTypeName;

	@SearchField(searchFieldPath = "billingDefinitionInvestmentAccount.billingBasisCalculationType", searchField = "name")
	private String billingBasisCalculationTypeName;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "excludeCondition.id")
	private Integer excludeConditionId;

	@SearchField(searchFieldPath = "excludeCondition", searchField = "name")
	private String excludeConditionName;

	@SearchField
	private String exclusionNote;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public Integer getBillingDefinitionInvestmentAccountId() {
		return this.billingDefinitionInvestmentAccountId;
	}


	public void setBillingDefinitionInvestmentAccountId(Integer billingDefinitionInvestmentAccountId) {
		this.billingDefinitionInvestmentAccountId = billingDefinitionInvestmentAccountId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getBillingBasisValuationTypeDescription() {
		return this.billingBasisValuationTypeDescription;
	}


	public void setBillingBasisValuationTypeDescription(String billingBasisValuationTypeDescription) {
		this.billingBasisValuationTypeDescription = billingBasisValuationTypeDescription;
	}


	public String getBillingBasisValuationTypeName() {
		return this.billingBasisValuationTypeName;
	}


	public void setBillingBasisValuationTypeName(String billingBasisValuationTypeName) {
		this.billingBasisValuationTypeName = billingBasisValuationTypeName;
	}


	public String getBillingBasisCalculationTypeName() {
		return this.billingBasisCalculationTypeName;
	}


	public void setBillingBasisCalculationTypeName(String billingBasisCalculationTypeName) {
		this.billingBasisCalculationTypeName = billingBasisCalculationTypeName;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getExcludeConditionId() {
		return this.excludeConditionId;
	}


	public void setExcludeConditionId(Integer excludeConditionId) {
		this.excludeConditionId = excludeConditionId;
	}


	public String getExcludeConditionName() {
		return this.excludeConditionName;
	}


	public void setExcludeConditionName(String excludeConditionName) {
		this.excludeConditionName = excludeConditionName;
	}


	public String getExclusionNote() {
		return this.exclusionNote;
	}


	public void setExclusionNote(String exclusionNote) {
		this.exclusionNote = exclusionNote;
	}
}
