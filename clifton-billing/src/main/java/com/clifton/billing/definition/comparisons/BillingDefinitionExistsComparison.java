package com.clifton.billing.definition.comparisons;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccountSecurityExclusion;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.definition.BillingScheduleTier;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.List;


/**
 * Can be used for any of the following entities
 * BillingDefinition
 * BillingDefinitionInvestmentAccount
 * BillingDefinitionInvestmentAccountSecurityExclusion
 * BillingSchedule
 * BillingScheduleSharing
 * BillingScheduleTier
 * <p>
 * Finds the Billing Definition(s) associated with the edit in given workflow status
 * <p>
 * Note: Although supported, not used directly for Billing Definitions - since each Billing Definition can have Entity Modify Condition entered on each Workflow State
 *
 * @author manderson
 */
public class BillingDefinitionExistsComparison implements Comparison<IdentityObject> {

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks for billing definitions with a specified workflow status name(s)
	 */
	private List<String> workflowStatusNames;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		if (CollectionUtils.isEmpty(getWorkflowStatusNames())) {
			throw new ValidationException("At least one Workflow Status name is required for Billing Definition Exists/Not Exists condition.");
		}
		// When editing schedule templates there are not any definitions that apply
		boolean allowEmptyDefinitionList = false;
		List<BillingDefinition> definitionCheckList = new ArrayList<>();
		if (bean instanceof BillingDefinition) {
			definitionCheckList.add((BillingDefinition) bean);
		}
		else if (bean instanceof BillingDefinitionInvestmentAccount) {
			definitionCheckList.add(((BillingDefinitionInvestmentAccount) bean).getReferenceOne());
		}
		else if (bean instanceof BillingDefinitionInvestmentAccountSecurityExclusion) {
			definitionCheckList.add(((BillingDefinitionInvestmentAccountSecurityExclusion) bean).getBillingDefinitionInvestmentAccount().getReferenceOne());
		}
		else if (bean instanceof BillingSchedule || bean instanceof BillingScheduleTier) {
			BillingSchedule schedule = (bean instanceof BillingSchedule ? (BillingSchedule) bean : ((BillingScheduleTier) bean).getBillingSchedule());
			// Shared schedules can be set up prior to being linked to any definitions, so we allow no definitions linked to it in order to edit
			allowEmptyDefinitionList = schedule.isTemplate() || schedule.isSharedSchedule();
			if (!schedule.isTemplate()) {
				if (schedule.isSharedSchedule()) {
					if (!schedule.isNewBean()) {
						// If nothing actually changes on the schedule - then this isn't validated
						// Only the definition assignments in BillingScheduleSharing need to be validated (i.e. add new, delete definitions from the schedule)
						// But if there is an actual change to the schedule, then every billing definition associated with that schedule must be checked if it can be edited
						List<BillingScheduleSharing> sharedDefinitionList = getBillingDefinitionService().getBillingScheduleSharingListForSchedule(schedule.getId());
						for (BillingScheduleSharing bss : CollectionUtils.getIterable(sharedDefinitionList)) {
							definitionCheckList.add(bss.getReferenceTwo());
						}
					}
				}
				else {
					definitionCheckList.add(schedule.getBillingDefinition());
				}
			}
		}
		else if (bean instanceof BillingScheduleSharing) {
			definitionCheckList.add(((BillingScheduleSharing) bean).getReferenceTwo());
		}
		else if (bean instanceof BillingScheduleBillingDefinitionDependency) {
			// These do not allow sharing - and we only need to confirm status on the definition for the schedule - the one it depends on isn't changing, but a pre-requisite
			definitionCheckList.add(((BillingScheduleBillingDefinitionDependency) bean).getBillingSchedule().getBillingDefinition());
		}

		List<String> existsList = new ArrayList<>(); // Billing Definitions Found In Status
		if (definitionCheckList.isEmpty()) {
			if (!allowEmptyDefinitionList) {
				throw new ValidationException("Unable to determine the billing definition(s) that apply to " + bean);
			}
		}
		else {
			for (BillingDefinition definition : definitionCheckList) {
				if (getWorkflowStatusNames().contains(definition.getWorkflowStatus().getName())) {
					existsList.add(definition.getLabel());
				}
			}
		}
		boolean result = !CollectionUtils.isEmpty(existsList);
		result = isReverse() ? !result : result;
		if (context != null) {
			String msg = "The following Billing Definitions are in " + StringUtils.collectionToCommaDelimitedString(getWorkflowStatusNames()) + " status: " + StringUtils.collectionToCommaDelimitedString(existsList);
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public List<String> getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	public void setWorkflowStatusNames(List<String> workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}
}
