package com.clifton.billing.definition.search;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableDateRangeSystemHierarchyItemSearchForm;

import java.util.Date;


/**
 * @author Mary Anderson
 */
public class BillingDefinitionInvestmentAccountSearchForm extends BaseAuditableDateRangeSystemHierarchyItemSearchForm {

	@SearchField(searchFieldPath = "referenceTwo", searchField = "number,name", sortField = "number")
	private String searchPattern;

	@SearchField(searchField = "referenceOne.id")
	private Integer billingDefinitionId;

	@SearchField(searchField = "referenceOne.id")
	private Integer[] billingDefinitionIds;

	@SearchField(searchFieldPath = "referenceOne.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeBillingDefinitionWorkflowStateName;

	@SearchField(searchFieldPath = "referenceOne", searchField = "invoiceType.id", sortField = "invoiceType.name")
	private Short invoiceTypeId;

	@SearchField(searchFieldPath = "referenceOne.invoiceType", searchField = "name")
	private String invoiceTypeName;

	@SearchField(searchFieldPath = "referenceTwo.businessClient.clientRelationship", searchField = "name,legalName", sortField = "name")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "referenceTwo.businessClient", searchField = "name,legalName", sortField = "name")
	private String clientName;

	@SearchField(searchFieldPath = "referenceOne.businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField(searchField = "referenceTwo.id", sortField = "referenceTwo.number")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "number", comparisonConditions = ComparisonConditions.EQUALS)
	private String investmentAccountNumberExact;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "inceptionDate")
	private Date investmentAccountInceptionDate; // a.k.a positions on date

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "referenceTwo.businessService.id,referenceTwo.businessService.parent.id,referenceTwo.businessService.parent.parent.id,referenceTwo.businessService.parent.parent.parent.id", leftJoin = true, sortField = "referenceTwo.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchFieldPath = "referenceTwo.serviceProcessingType", searchField = "name")
	private String serviceProcessingTypeName;

	@SearchField(searchFieldPath = "referenceTwo.serviceProcessingType", searchField = "processingType")
	private ServiceProcessingTypes serviceProcessingType;

	@SearchField(searchField = "billingBasisValuationType.id")
	private Short billingBasisValuationTypeId;

	@SearchField(searchFieldPath = "billingBasisValuationType", searchField = "name")
	private String billingBasisValuationTypeName;

	@SearchField(searchFieldPath = "billingBasisValuationType.sourceTable", searchField = "name")
	private String billingBasisValuationSourceTableName;

	@SearchField(searchFieldPath = "billingBasisValuationType", searchField = "investmentGroupAllowed")
	private Boolean billingBasisValuationTypeInvestmentGroupAllowed;

	@SearchField(searchFieldPath = "billingBasisValuationType", searchField = "calculatorBean", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean billingBasisUsed;

	@SearchField
	private String billingBasisValuationTypeDescription;

	@SearchField(searchField = "billingBasisValuationTypeDescription", comparisonConditions = ComparisonConditions.EQUALS)
	private String billingBasisValuationTypeDescriptionExact;

	@SearchField(searchFieldPath = "billingBasisCalculationType", searchField = "name")
	private String billingBasisCalculationTypeName;

	@SearchField(searchField = "investmentGroup.id", sortField = "investmentGroup.name")
	private Short investmentGroupId;

	@SearchField(searchField = "investmentManagerAccount.id", sortField = "investmentManagerAccount.accountNumber")
	private Integer investmentManagerAccountId;

	// Custom Search Field
	private Boolean externalOnly;

	// Custom SearchField - Filters on active for the definition
	private Boolean activeDefinition;

	@SearchField(searchFieldPath = "billingSchedule", searchField = "name")
	private String billingScheduleName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "BillingDefinitionInvestmentAccount";
	}

	////////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getBillingBasisValuationTypeDescription() {
		return this.billingBasisValuationTypeDescription;
	}


	public void setBillingBasisValuationTypeDescription(String billingBasisValuationTypeDescription) {
		this.billingBasisValuationTypeDescription = billingBasisValuationTypeDescription;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getInvestmentAccountNumberExact() {
		return this.investmentAccountNumberExact;
	}


	public void setInvestmentAccountNumberExact(String investmentAccountNumberExact) {
		this.investmentAccountNumberExact = investmentAccountNumberExact;
	}


	public String getBillingBasisValuationTypeDescriptionExact() {
		return this.billingBasisValuationTypeDescriptionExact;
	}


	public void setBillingBasisValuationTypeDescriptionExact(String billingBasisValuationTypeDescriptionExact) {
		this.billingBasisValuationTypeDescriptionExact = billingBasisValuationTypeDescriptionExact;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public String getServiceProcessingTypeName() {
		return this.serviceProcessingTypeName;
	}


	public void setServiceProcessingTypeName(String serviceProcessingTypeName) {
		this.serviceProcessingTypeName = serviceProcessingTypeName;
	}


	public ServiceProcessingTypes getServiceProcessingType() {
		return this.serviceProcessingType;
	}


	public void setServiceProcessingType(ServiceProcessingTypes serviceProcessingType) {
		this.serviceProcessingType = serviceProcessingType;
	}


	public Boolean getActiveDefinition() {
		return this.activeDefinition;
	}


	public void setActiveDefinition(Boolean activeDefinition) {
		this.activeDefinition = activeDefinition;
	}


	public Short getBillingBasisValuationTypeId() {
		return this.billingBasisValuationTypeId;
	}


	public void setBillingBasisValuationTypeId(Short billingBasisValuationTypeId) {
		this.billingBasisValuationTypeId = billingBasisValuationTypeId;
	}


	public String getBillingBasisValuationTypeName() {
		return this.billingBasisValuationTypeName;
	}


	public void setBillingBasisValuationTypeName(String billingBasisValuationTypeName) {
		this.billingBasisValuationTypeName = billingBasisValuationTypeName;
	}


	public String getBillingBasisValuationSourceTableName() {
		return this.billingBasisValuationSourceTableName;
	}


	public void setBillingBasisValuationSourceTableName(String billingBasisValuationSourceTableName) {
		this.billingBasisValuationSourceTableName = billingBasisValuationSourceTableName;
	}


	public Integer[] getBillingDefinitionIds() {
		return this.billingDefinitionIds;
	}


	public void setBillingDefinitionIds(Integer[] billingDefinitionIds) {
		this.billingDefinitionIds = billingDefinitionIds;
	}


	public Boolean getBillingBasisUsed() {
		return this.billingBasisUsed;
	}


	public void setBillingBasisUsed(Boolean billingBasisUsed) {
		this.billingBasisUsed = billingBasisUsed;
	}


	public String getBillingBasisCalculationTypeName() {
		return this.billingBasisCalculationTypeName;
	}


	public void setBillingBasisCalculationTypeName(String billingBasisCalculationTypeName) {
		this.billingBasisCalculationTypeName = billingBasisCalculationTypeName;
	}


	public Boolean getExternalOnly() {
		return this.externalOnly;
	}


	public void setExternalOnly(Boolean externalOnly) {
		this.externalOnly = externalOnly;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public Integer getInvestmentManagerAccountId() {
		return this.investmentManagerAccountId;
	}


	public void setInvestmentManagerAccountId(Integer investmentManagerAccountId) {
		this.investmentManagerAccountId = investmentManagerAccountId;
	}


	public String getBillingScheduleName() {
		return this.billingScheduleName;
	}


	public void setBillingScheduleName(String billingScheduleName) {
		this.billingScheduleName = billingScheduleName;
	}


	public Date getInvestmentAccountInceptionDate() {
		return this.investmentAccountInceptionDate;
	}


	public void setInvestmentAccountInceptionDate(Date investmentAccountInceptionDate) {
		this.investmentAccountInceptionDate = investmentAccountInceptionDate;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public String getInvoiceTypeName() {
		return this.invoiceTypeName;
	}


	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}


	public String getExcludeBillingDefinitionWorkflowStateName() {
		return this.excludeBillingDefinitionWorkflowStateName;
	}


	public void setExcludeBillingDefinitionWorkflowStateName(String excludeBillingDefinitionWorkflowStateName) {
		this.excludeBillingDefinitionWorkflowStateName = excludeBillingDefinitionWorkflowStateName;
	}


	public Boolean getBillingBasisValuationTypeInvestmentGroupAllowed() {
		return this.billingBasisValuationTypeInvestmentGroupAllowed;
	}


	public void setBillingBasisValuationTypeInvestmentGroupAllowed(Boolean billingBasisValuationTypeInvestmentGroupAllowed) {
		this.billingBasisValuationTypeInvestmentGroupAllowed = billingBasisValuationTypeInvestmentGroupAllowed;
	}
}
