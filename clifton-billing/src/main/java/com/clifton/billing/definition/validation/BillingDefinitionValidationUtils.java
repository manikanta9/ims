package com.clifton.billing.definition.validation;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public class BillingDefinitionValidationUtils {


	/**
	 * Method is validated on definition saves, schedule saves, and schedule sharing saves
	 * Specifically useful for shared schedules where one change to any of those three would require validation of all shared definitions
	 * Used to validate items that need to be compatible between schedules and definitions:
	 * Accrual Frequency
	 * Revenue Type
	 *
	 * @param billingSchedule     - schedule (could be unsaved updated schedule)
	 * @param billingDefinition   - the billing definition to validate
	 * @param scheduleSharingList - the list of billing definitions that the schedule is shared with
	 *                            Note: billingDefinition if passed but also exists in the scheduleSharingList means it's being updated so validation would use the separately passed definition in it's place
	 */
	public static void validateBillingScheduleForDefinitions(BillingSchedule billingSchedule, BillingDefinition billingDefinition, List<BillingScheduleSharing> scheduleSharingList) {
		validateBillingScheduleAccrualFrequency(billingSchedule, billingDefinition, scheduleSharingList);
		validateBillingScheduleRevenueType(billingSchedule, billingDefinition, scheduleSharingList);
	}


	private static void validateBillingScheduleAccrualFrequency(BillingSchedule billingSchedule, BillingDefinition billingDefinition, List<BillingScheduleSharing> scheduleSharingList) {
		boolean validateSameFrequency = billingSchedule.isSharedSchedule() && !billingSchedule.isDoNotGroupInvoice();
		Set<BillingFrequencies> billingFrequenciesSet = new HashSet<>();

		if (billingDefinition != null) {
			BillingFrequencies.validateAccrualAndBillingFrequency(billingSchedule.getAccrualFrequency(), billingDefinition.getBillingFrequency());
			billingFrequenciesSet.add(billingDefinition.getBillingFrequency());
		}

		for (BillingScheduleSharing scheduleSharing : CollectionUtils.getIterable(scheduleSharingList)) {
			if (billingDefinition == null || !billingDefinition.equals(scheduleSharing.getReferenceTwo())) {
				BillingFrequencies.validateAccrualAndBillingFrequency(billingSchedule.getAccrualFrequency(), scheduleSharing.getReferenceTwo().getBillingFrequency());
				billingFrequenciesSet.add(scheduleSharing.getReferenceTwo().getBillingFrequency());
			}
		}

		if (validateSameFrequency && billingFrequenciesSet.size() > 1) {
			throw new ValidationException("Found multiple definition billing frequencies used for shared schedule " + billingSchedule.getName() + ".  Frequencies found " + CollectionUtils.toString(CollectionUtils.createSorted(billingFrequenciesSet), 5));
		}
	}


	/**
	 * Validates selected revenue type is supported for the billing definitions. i.e. You cannot have a schedule with REVENUE_SHARE_EXTERNAL if the definition doesn't have an external company selected.
	 * And if it's shared the external company should be the same across all!
	 */
	private static void validateBillingScheduleRevenueType(BillingSchedule billingSchedule, BillingDefinition billingDefinition, List<BillingScheduleSharing> scheduleSharingList) {
		ValidationUtils.assertNotNull(billingSchedule.getRevenueType(), "Revenue Type selection is required for billing schedules.");

		boolean validateSameShareCompanyAcrossAll = billingSchedule.isSharedSchedule() && !billingSchedule.isDoNotGroupInvoice() && billingSchedule.getRevenueType().isRevenueShare();
		BusinessCompany revenueShareCompany = null;

		if (billingDefinition != null) {
			revenueShareCompany = validateRevenueTypeForBillingDefinition(billingDefinition, billingSchedule.getRevenueType());
		}


		for (BillingScheduleSharing scheduleSharing : CollectionUtils.getIterable(scheduleSharingList)) {
			if (billingDefinition == null || !billingDefinition.equals(scheduleSharing.getReferenceTwo())) {
				BusinessCompany sharedRevenueShareCompany = validateRevenueTypeForBillingDefinition(scheduleSharing.getReferenceTwo(), billingSchedule.getRevenueType());
				revenueShareCompany = ObjectUtils.coalesce(revenueShareCompany, sharedRevenueShareCompany);
				if (revenueShareCompany != null && sharedRevenueShareCompany != null && validateSameShareCompanyAcrossAll && !CompareUtils.isEqual(revenueShareCompany, sharedRevenueShareCompany)) {
					throw new ValidationException("Found multiple " + billingSchedule.getRevenueType().getLabel() + " companies used for shared schedule " + billingSchedule.getName() + ". Companies found " + revenueShareCompany.getNameWithType() + ", " + sharedRevenueShareCompany.getNameWithType() + ".");
				}
			}
		}
	}


	/**
	 * Validates the selected revenue type is applicable to the billing definition.  If the revenue type is a revenue share, will return the company that gets that share (used for cross definition validation)
	 * Available publicly as it is also used when adding manual invoice details that don't use a schedule
	 */
	public static BusinessCompany validateRevenueTypeForBillingDefinition(BillingDefinition billingDefinition, BillingInvoiceRevenueTypes revenueType) {
		ValidationUtils.assertEquals(billingDefinition.getInvoiceType().isRevenue(), revenueType.isRevenue(), "Revenue type " + revenueType.getLabel() + " is not supported for billing definition " + billingDefinition.getLabel());
		if (revenueType.isRevenueShare()) {
			BusinessCompany revenueShareCompany = revenueType.isExternal() ? billingDefinition.getRevenueShareExternalCompany() : billingDefinition.getRevenueShareInternalCompany();
			ValidationUtils.assertNotNull(revenueShareCompany, "Revenue type " + revenueType.getLabel() + " is not supported for billing definition " + billingDefinition.getLabel() + ".  Billing Definition is missing a company selection to allocate that revenue to.");
			return revenueShareCompany;
		}
		return null;
	}
}
