package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleSharingListByScheduleCache</code> caches the list of BillingScheduleSharing for the given schedule
 *
 * @author manderson
 */
@Component
public class BillingScheduleSharingListByScheduleCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleSharing, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleSharing bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
