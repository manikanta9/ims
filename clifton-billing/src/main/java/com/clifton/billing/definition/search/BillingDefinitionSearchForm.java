package com.clifton.billing.definition.search;


import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.search.BaseWorkflowAwareDateRangeSystemHierarchyItemSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.Date;
import java.util.List;


/**
 * @author Mary Anderson
 */
public class BillingDefinitionSearchForm extends BaseWorkflowAwareDateRangeSystemHierarchyItemSearchForm implements BusinessCompanyAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id,billingContract.name", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER)
	private String searchPattern;

	@SearchField(searchField = "invoiceType.id", sortField = "invoiceType.name")
	private Short invoiceTypeId;

	@SearchField(searchFieldPath = "invoiceType", searchField = "name")
	private String invoiceTypeName;

	@SearchField(searchFieldPath = "invoiceType", searchField = "revenue")
	private Boolean revenue;

	// Custom Search field
	private BillingInvoiceRevenueTypes revenueType;


	@SearchField(searchField = "revenueShareExternalCompany.id")
	private Integer revenueShareExternalCompanyId;

	@SearchField(searchField = "revenueShareExternalCompany.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean revenueShareExternalCompanyIdNull;

	@SearchField(searchFieldPath = "revenueShareExternalCompany", searchField = "name")
	private String revenueShareExternalCompanyName;

	@SearchField(searchField = "revenueShareInternalCompany.id")
	private Integer revenueShareInternalCompanyId;

	@SearchField(searchField = "revenueShareInternalCompany.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean revenueShareInternalCompanyIdNull;

	@SearchField(searchFieldPath = "revenueShareInternalCompany", searchField = "name")
	private String revenueShareInternalCompanyName;

	@SearchField
	private Boolean invoicePostedToPortal;

	@SearchField(searchField = "billingContract.id", sortField = "billingContract.name")
	private Integer contractId;

	@SearchField(searchFieldPath = "billingContract", searchField = "name")
	private String contractName;


	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyId;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;

	/**
	 * Appends additional OR filters to the company filters but looks at {@link com.clifton.billing.definition.BillingDefinitionInvestmentAccount}
	 * table where Account Client Company ID IN (X) or Account Client Client Relationship Company IN (X)
	 */
	private Boolean includeAccountsExistOfClientOrClientRelationship;


	@SearchField(searchFieldPath = "businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField(searchField = "billingCurrency.id", sortField = "billingCurrency.symbol")
	private Integer currencyId;

	@SearchField(searchField = "id")
	private Integer billingDefinitionId;

	@SearchField
	private BillingFrequencies billingFrequency;

	@SearchField(searchField = "billingFrequency")
	private BillingFrequencies[] billingFrequencies;

	@SearchField
	private Boolean paymentInAdvance;

	@SearchField
	private Date secondYearStartDate;

	@SearchField(searchField = "investmentAccountList.referenceTwo.groupList.referenceOne.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "investmentAccountList.referenceTwo.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"businessCompany"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		if (!CollectionUtils.isEmpty(companyIds) && BooleanUtils.isTrue(getIncludeAccountsExistOfClientOrClientRelationship())) {
			DetachedCriteria accountExists = DetachedCriteria.forClass(BillingDefinitionInvestmentAccount.class, "da");
			accountExists.setProjection(Projections.property("id"));
			accountExists.createAlias("referenceTwo", "daa");
			accountExists.createAlias("daa.businessClient", "daa_cl");
			accountExists.createAlias("daa_cl.clientRelationship", "daa_clr", JoinType.LEFT_OUTER_JOIN);
			accountExists.add(Restrictions.eqProperty("da.referenceOne.id", criteria.getAlias() + ".id"));
			if (companyIds.size() == 1) {
				accountExists.add(Restrictions.or(Restrictions.eq("daa_cl.company.id", companyIds.get(0)), Restrictions.eq("daa_clr.company.id", companyIds.get(0))));
			}
			else {
				accountExists.add(Restrictions.or(Restrictions.in("daa_cl.company.id", companyIds), Restrictions.in("daa_clr.company.id", companyIds)));
			}
			orCompanyFilters.add(Subqueries.exists(accountExists));
		}
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		// DO NOTHING
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return BillingDefinition.BILLING_DEFINITION_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public String getInvoiceTypeName() {
		return this.invoiceTypeName;
	}


	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}


	public Boolean getRevenue() {
		return this.revenue;
	}


	public void setRevenue(Boolean revenue) {
		this.revenue = revenue;
	}


	public BillingInvoiceRevenueTypes getRevenueType() {
		return this.revenueType;
	}


	public void setRevenueType(BillingInvoiceRevenueTypes revenueType) {
		this.revenueType = revenueType;
	}


	public Integer getCurrencyId() {
		return this.currencyId;
	}


	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}


	public BillingFrequencies getBillingFrequency() {
		return this.billingFrequency;
	}


	public void setBillingFrequency(BillingFrequencies billingFrequency) {
		this.billingFrequency = billingFrequency;
	}


	public Boolean getPaymentInAdvance() {
		return this.paymentInAdvance;
	}


	public void setPaymentInAdvance(Boolean paymentInAdvance) {
		this.paymentInAdvance = paymentInAdvance;
	}


	public Date getSecondYearStartDate() {
		return this.secondYearStartDate;
	}


	public void setSecondYearStartDate(Date secondYearStartDate) {
		this.secondYearStartDate = secondYearStartDate;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getContractId() {
		return this.contractId;
	}


	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getContractName() {
		return this.contractName;
	}


	public void setContractName(String contractName) {
		this.contractName = contractName;
	}


	public BillingFrequencies[] getBillingFrequencies() {
		return this.billingFrequencies;
	}


	public void setBillingFrequencies(BillingFrequencies[] billingFrequencies) {
		this.billingFrequencies = billingFrequencies;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}


	public Boolean getInvoicePostedToPortal() {
		return this.invoicePostedToPortal;
	}


	public void setInvoicePostedToPortal(Boolean invoicePostedToPortal) {
		this.invoicePostedToPortal = invoicePostedToPortal;
	}


	public Boolean getIncludeAccountsExistOfClientOrClientRelationship() {
		return this.includeAccountsExistOfClientOrClientRelationship;
	}


	public void setIncludeAccountsExistOfClientOrClientRelationship(Boolean includeAccountsExistOfClientOrClientRelationship) {
		this.includeAccountsExistOfClientOrClientRelationship = includeAccountsExistOfClientOrClientRelationship;
	}


	public Integer getRevenueShareExternalCompanyId() {
		return this.revenueShareExternalCompanyId;
	}


	public void setRevenueShareExternalCompanyId(Integer revenueShareExternalCompanyId) {
		this.revenueShareExternalCompanyId = revenueShareExternalCompanyId;
	}


	public Boolean getRevenueShareExternalCompanyIdNull() {
		return this.revenueShareExternalCompanyIdNull;
	}


	public void setRevenueShareExternalCompanyIdNull(Boolean revenueShareExternalCompanyIdNull) {
		this.revenueShareExternalCompanyIdNull = revenueShareExternalCompanyIdNull;
	}


	public String getRevenueShareExternalCompanyName() {
		return this.revenueShareExternalCompanyName;
	}


	public void setRevenueShareExternalCompanyName(String revenueShareExternalCompanyName) {
		this.revenueShareExternalCompanyName = revenueShareExternalCompanyName;
	}


	public Integer getRevenueShareInternalCompanyId() {
		return this.revenueShareInternalCompanyId;
	}


	public void setRevenueShareInternalCompanyId(Integer revenueShareInternalCompanyId) {
		this.revenueShareInternalCompanyId = revenueShareInternalCompanyId;
	}


	public Boolean getRevenueShareInternalCompanyIdNull() {
		return this.revenueShareInternalCompanyIdNull;
	}


	public void setRevenueShareInternalCompanyIdNull(Boolean revenueShareInternalCompanyIdNull) {
		this.revenueShareInternalCompanyIdNull = revenueShareInternalCompanyIdNull;
	}


	public String getRevenueShareInternalCompanyName() {
		return this.revenueShareInternalCompanyName;
	}


	public void setRevenueShareInternalCompanyName(String revenueShareInternalCompanyName) {
		this.revenueShareInternalCompanyName = revenueShareInternalCompanyName;
	}
}
