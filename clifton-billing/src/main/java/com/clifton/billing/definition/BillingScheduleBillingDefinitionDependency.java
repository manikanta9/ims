package com.clifton.billing.definition;

import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The <code>BillingScheduleBillingDefinitionDependency</code> is a way to define a dependency for a billing schedule on another BillingDefinition that isn't shared
 * For example - A client invoice that gets credit towards quarterly minimum for their fund fees, Or a client invoice that gets a discount on their PIOS invoice based on their DE invoice
 * <p>
 * Dependencies can only go one level deep and the dependency cannot be defined between billing definitions that in any way are shared
 *
 * @author manderson
 */
public class BillingScheduleBillingDefinitionDependency extends BaseEntity<Integer> {

	/**
	 * The schedule that depends on another billing definition
	 * Can only be used for definition schedules (not shared schedules)
	 */
	private BillingSchedule billingSchedule;

	/**
	 * This is the billing definition that the schedule depends on
	 * It cannot be a billing definition that is already shared through another schedule with the billing schedule's definition
	 */
	private BillingDefinition billingDefinition;


	/**
	 * Optionally limit the dependency to a specific billing definition investment account
	 * Cannot select an account if this is selected (more specific)
	 */
	private BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount;


	/**
	 * Optionally limit the dependency to a specific account
	 * Cannot select a billing account if this is selected
	 */
	private InvestmentAccount investmentAccount;


	/**
	 * Optional note about this dependency
	 */
	private String note;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder label = new StringBuilder(50);
		if (getBillingSchedule() != null) {
			label.append(getBillingSchedule().getLabel());
		}
		label.append(" - ");
		if (getBillingDefinition() != null) {
			label.append(getBillingDefinition().getLabel());

			String accountLabel = getAccountLabel();
			if (accountLabel != null) {
				label.append(": ");
				label.append(accountLabel);
			}
		}
		return label.toString();
	}


	public String getAccountLabel() {
		if (getBillingDefinitionInvestmentAccount() != null) {
			return getBillingDefinitionInvestmentAccount().getAccountLabel();
		}
		if (getInvestmentAccount() != null) {
			return getInvestmentAccount().getLabel();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingSchedule getBillingSchedule() {
		return this.billingSchedule;
	}


	public void setBillingSchedule(BillingSchedule billingSchedule) {
		this.billingSchedule = billingSchedule;
	}


	public BillingDefinition getBillingDefinition() {
		return this.billingDefinition;
	}


	public void setBillingDefinition(BillingDefinition billingDefinition) {
		this.billingDefinition = billingDefinition;
	}


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount() {
		return this.billingDefinitionInvestmentAccount;
	}


	public void setBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		this.billingDefinitionInvestmentAccount = billingDefinitionInvestmentAccount;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
