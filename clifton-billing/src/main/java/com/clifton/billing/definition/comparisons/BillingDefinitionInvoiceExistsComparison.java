package com.clifton.billing.definition.comparisons;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.List;


/**
 * For given Billing Definition - Returns true if at least one invoice for that billing definition is found with the given filters
 *
 * @author manderson
 */
public class BillingDefinitionInvoiceExistsComparison implements Comparison<BillingDefinition> {

	private BillingInvoiceService billingInvoiceService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks for invoices with a specified workflow state name
	 */
	private String workflowStateName;

	/**
	 * Checks for invoices with a specified workflow status name
	 */
	private String workflowStatusName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(BillingDefinition billingDefinition, ComparisonContext context) {
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(billingDefinition.getId());
		if (!StringUtils.isEmpty(getWorkflowStateName())) {
			searchForm.setWorkflowStateName(getWorkflowStateName());
		}
		if (!StringUtils.isEmpty(getWorkflowStatusName())) {
			searchForm.setWorkflowStatusName(getWorkflowStatusName());
		}

		List<BillingInvoice> invoiceList = getBillingInvoiceService().getBillingInvoiceList(searchForm);
		boolean result = !CollectionUtils.isEmpty(invoiceList);
		result = isReverse() ? !result : result;
		StringBuilder message = new StringBuilder("(" + CollectionUtils.getSize(invoiceList) + " " + getWorkflowStateStatusDescription() + " Invoice(s) Found)");
		if (context != null) {
			if (result) {
				context.recordTrueMessage(message.toString());
			}
			else {
				context.recordFalseMessage(message.toString());
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}


	private String getWorkflowStateStatusDescription() {
		if (!StringUtils.isEmpty(getWorkflowStateName())) {
			if (!StringUtils.isEmpty(getWorkflowStatusName())) {
				return getWorkflowStateName() + "/" + getWorkflowStatusName();
			}
			return getWorkflowStateName();
		}
		if (!StringUtils.isEmpty(getWorkflowStatusName())) {
			return getWorkflowStatusName();
		}
		return "";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}
}
