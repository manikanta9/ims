package com.clifton.billing.definition;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;


/**
 * The <code>BillingFrequencies</code> enum defines how frequently the bill is generated and/or fees are accrued
 *
 * @author vgomelsky
 */
public enum BillingFrequencies {

	DAILY(0, 1, true), // Not used on Definitions, but can be used for Schedule Accrual Frequencies
	MONTHLY(1, 30), //
	QUARTERLY(3, 91), //
	SEMI_ANNUALLY(6, 182), //
	ANNUALLY(12, 365);

	////////////////////////////////////////////////////////////////////////////////


	BillingFrequencies(int monthsInPeriod, int daysInPeriod) {
		this(monthsInPeriod, daysInPeriod, false);
	}


	BillingFrequencies(int monthsInPeriod, int daysInPeriod, boolean accrualFrequencyOnly) {
		this.monthsInPeriod = monthsInPeriod;
		this.daysInPeriod = daysInPeriod;
		this.accrualFrequencyOnly = accrualFrequencyOnly;
	}

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Cannot be selected on Billing Definitions, but can be selected on
	 * Billing Schedules for the accrual frequency. For example - Daily - we wouldn't invoice a client for each day, but the invoice may break out the fee for each day
	 */
	private final boolean accrualFrequencyOnly;

	private final int monthsInPeriod;
	private final int daysInPeriod;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getLastDayOfBillingCycle(Date date, Date annualStartDate) {
		if (this == DAILY) {
			return date;
		}
		if (this == MONTHLY) {
			return DateUtils.getLastDayOfMonth(date);
		}
		if (this == QUARTERLY) {
			return DateUtils.getLastDayOfQuarter(date);
		}

		if (annualStartDate == null) {
			annualStartDate = DateUtils.getFirstDayOfYear(date);
		}
		// Get the Next Annual Start
		Date nextAnnual = DateUtils.toDate(DateUtils.fromDate(annualStartDate, "MM/dd/") + DateUtils.fromDate(date, "yyyy"));
		// Go Back a day to get last day
		nextAnnual = DateUtils.addDays(nextAnnual, -1);
		// Make sure we are in the correct year
		if (DateUtils.compare(nextAnnual, date, false) < 0) {
			nextAnnual = DateUtils.addYears(nextAnnual, 1);
		}

		// If Semi-Annually - Subtract 6 months and see if that is still in the future, if it is, it's the next billing date
		if (this == SEMI_ANNUALLY) {
			// Move to the First Day of the month to subtract the 6 months, then move back to last day of month
			// this should fix any 30 vs. 31 vs. 28 days in the month issue.
			Date semiDate = DateUtils.getLastDayOfMonth(DateUtils.addMonths(DateUtils.getFirstDayOfMonth(nextAnnual), -6));
			if (DateUtils.compare(date, semiDate, false) < 0) {
				// Go Back one day to get the last day
				return semiDate;
			}
		}
		return nextAnnual;
	}


	public int calculateActualDaysInPeriod(Date invoiceDate) {
		Date startDate;
		if (this == DAILY) {
			return 1;
		}
		else if (this == MONTHLY) {
			startDate = DateUtils.getFirstDayOfMonth(invoiceDate);
		}
		else if (this == QUARTERLY) {
			startDate = DateUtils.getFirstDayOfQuarter(invoiceDate);
		}
		else if (this == SEMI_ANNUALLY) {
			startDate = DateUtils.addMonths(DateUtils.addDays(invoiceDate, 1), -6);
		}
		else if (this == ANNUALLY) {
			startDate = DateUtils.addYears(DateUtils.addDays(invoiceDate, 1), -1);
		}
		else {
			throw new RuntimeException("No calculation defined for BillingFrequency " + this);
		}
		return DateUtils.getDaysDifferenceInclusive(invoiceDate, startDate);
	}


	public String getDateRangeLabel(Date startDate, Date endDate) {
		if (this == DAILY) {
			return DateUtils.fromDateShort(startDate);
		}
		else if (this == MONTHLY) {
			return DateUtils.fromDate(startDate, "MMMM yyyy");
		}
		else if (this == QUARTERLY) {
			return DateUtils.getQuarter(startDate) + "Q " + DateUtils.fromDate(startDate, "yyyy");
		}
		return DateUtils.fromDateRange(startDate, endDate, true);
	}
	////////////////////////////////////////////////////////////////////////////////


	public static void validateAccrualAndBillingFrequency(BillingFrequencies accrualFrequency, BillingFrequencies billingFrequency) {
		if (accrualFrequency != null && billingFrequency != null) {
			ValidationUtils.assertTrue(accrualFrequency.getDaysInPeriod() <= billingFrequency.getDaysInPeriod(), "Billing frequency of " + billingFrequency.name() + " and accrual frequency of " + accrualFrequency.name() + " is invalid.  Accrual frequencies must be the same or shorter time periods than the billing frequency.");
		}
	}


	////////////////////////////////////////////////////////////////////////////////


	public int getMonthsInPeriod() {
		return this.monthsInPeriod;
	}


	public int getDaysInPeriod() {
		return this.daysInPeriod;
	}


	public boolean isAccrualFrequencyOnly() {
		return this.accrualFrequencyOnly;
	}
}
