package com.clifton.billing.definition.workflow;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.validation.SystemTableValidationValidator;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Date;
import java.util.List;


/**
 * The <code>BillingDefinitionArchiveWorkflowTransitionAction</code> class is used when moving billing definitions to archived to end all of the accounts and schedules associated with the billing definition.
 * Shared schedules are only ended once the last shared definition is archived.
 * <p>
 * NOTE: All actions are completed with {@link SystemTableValidationValidator} observer disabled to prevent validation that prevents edits to Billing Definitions and their respective set up tables while definition is Approved
 *
 * @author manderson
 */
public class BillingDefinitionArchiveWorkflowTransitionAction implements WorkflowTransitionActionHandler<BillingDefinition> {

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, will set end date to billing definition end date on all billing accounts with no end date
	 */
	private boolean endBillingDefinitionInvestmentAccounts;


	/**
	 * If true, will set end date on all DEFINITION SPECIFIC billing schedules (i.e. not shared) to billing definition end date for all schedules with no end date
	 */
	private boolean endBillingDefinitionBillingSchedules;


	/**
	 * If true, will set end date on all shared schedules (as only as this is the last definition being archived)
	 */
	private boolean endSharedBillingSchedules;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingDefinition processAction(BillingDefinition billingDefinition, WorkflowTransition transition) {
		ValidationUtils.assertNotNull(billingDefinition.getEndDate(), "Archiving Transition action only applies when archiving a billing definition and requires definition end date to be set.");

		// Set End Date on all BillingDefinitionInvestmentAccounts
		if (isEndBillingDefinitionInvestmentAccounts()) {
			List<BillingDefinitionInvestmentAccount> billingDefinitionInvestmentAccountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(billingDefinition.getId());
			CollectionUtils.getStream(billingDefinitionInvestmentAccountList).forEach(billingDefinitionInvestmentAccount -> {
				if (billingDefinitionInvestmentAccount.getEndDate() == null) {
					billingDefinitionInvestmentAccount.setEndDate(billingDefinition.getEndDate());
					DaoUtils.executeWithSpecificObserversDisabled(() -> getBillingDefinitionService().saveBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccount), SystemTableValidationValidator.class);
				}
			});
		}

		// Set End Date on all Definition Specific Schedules
		if (isEndBillingDefinitionBillingSchedules()) {
			List<BillingSchedule> definitionScheduleList = getBillingDefinitionService().getBillingScheduleListForDefinition(billingDefinition.getId(), false);
			CollectionUtils.getStream(definitionScheduleList).forEach(billingSchedule -> {
				if (billingSchedule.getEndDate() == null) {
					billingSchedule.setEndDate(billingDefinition.getEndDate());
					DaoUtils.executeWithSpecificObserversDisabled(() -> getBillingDefinitionService().saveBillingScheduleSimple(billingSchedule), SystemTableValidationValidator.class);
				}
			});
		}

		// Set End Date on all Shared Shared Schedules (If Last One)
		if (isEndSharedBillingSchedules()) {
			List<BillingSchedule> sharedScheduleList = getBillingDefinitionService().getBillingScheduleListForDefinition(billingDefinition.getId(), true);
			CollectionUtils.getStream(sharedScheduleList).forEach(billingSchedule -> {
				if (billingSchedule.getEndDate() == null) {
					Date maxEndDate = getMaxEndDateForSharedSchedule(billingSchedule, billingDefinition, transition);
					if (maxEndDate != null) {
						billingSchedule.setEndDate(maxEndDate);
						DaoUtils.executeWithSpecificObserversDisabled(() -> getBillingDefinitionService().saveBillingScheduleSimple(billingSchedule), SystemTableValidationValidator.class);
					}
				}
			});
		}
		return billingDefinition;
	}


	private Date getMaxEndDateForSharedSchedule(BillingSchedule billingSchedule, BillingDefinition billingDefinition, WorkflowTransition workflowTransition) {
		Date maxEndDate = billingDefinition.getEndDate();
		List<BillingScheduleSharing> scheduleSharingList = getBillingDefinitionService().getBillingScheduleSharingListForSchedule(billingSchedule.getId());
		for (BillingScheduleSharing scheduleSharing : CollectionUtils.getIterable(scheduleSharingList)) {
			// If NOT This Billing Definition
			if (!scheduleSharing.getReferenceTwo().equals(billingDefinition)) {
				// If End Date is Null - return null - not ending the schedule
				if (scheduleSharing.getReferenceTwo().getEndDate() == null) {
					return null;
				}
				// If Shared Definition is not Archived (i.e. end state on the current transition we are performing)
				if (!workflowTransition.getEndWorkflowState().equals(scheduleSharing.getReferenceTwo().getWorkflowState())) {
					return null;
				}
				// Otherwise - update (if necessary) the max end date
				if (DateUtils.isDateAfter(scheduleSharing.getReferenceTwo().getEndDate(), maxEndDate)) {
					maxEndDate = scheduleSharing.getReferenceTwo().getEndDate();
				}
			}
		}
		return maxEndDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public boolean isEndBillingDefinitionInvestmentAccounts() {
		return this.endBillingDefinitionInvestmentAccounts;
	}


	public void setEndBillingDefinitionInvestmentAccounts(boolean endBillingDefinitionInvestmentAccounts) {
		this.endBillingDefinitionInvestmentAccounts = endBillingDefinitionInvestmentAccounts;
	}


	public boolean isEndBillingDefinitionBillingSchedules() {
		return this.endBillingDefinitionBillingSchedules;
	}


	public void setEndBillingDefinitionBillingSchedules(boolean endBillingDefinitionBillingSchedules) {
		this.endBillingDefinitionBillingSchedules = endBillingDefinitionBillingSchedules;
	}


	public boolean isEndSharedBillingSchedules() {
		return this.endSharedBillingSchedules;
	}


	public void setEndSharedBillingSchedules(boolean endSharedBillingSchedules) {
		this.endSharedBillingSchedules = endSharedBillingSchedules;
	}
}
