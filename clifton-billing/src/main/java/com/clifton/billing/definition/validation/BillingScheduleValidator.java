package com.clifton.billing.definition.validation;


import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BillingScheduleValidator</code> Validates schedule properties on Inserts/Updates including if second year start date is required (for annual fees)
 *
 * @author Mary Anderson
 */
@Component
public class BillingScheduleValidator extends SelfRegisteringDaoValidator<BillingSchedule> {

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BillingSchedule bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO method
	}


	@Override
	public void validate(BillingSchedule bean, DaoEventTypes config, ReadOnlyDAO<BillingSchedule> dao) throws ValidationException {
		if (bean.getStartDate() != null) {
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		}

		// Simple Validation for required or not allowed fields
		if (!bean.getScheduleType().isAccrualFrequencyAllowed()) {
			ValidationUtils.assertNull(bean.getAccrualFrequency(), "Accrual Frequency is not an allowed selection for schedule type " + bean.getScheduleType().getName());
		}
		if (!bean.getScheduleType().isTiered()) {
			ValidationUtils.assertNotNull(bean.getScheduleAmount(), bean.getScheduleType().getScheduleAmountLabel() + " is required.", "scheduleAmount");
		}
		if (bean.getScheduleType().isSecondaryScheduleAmountRequired()) {
			ValidationUtils.assertNotNull(bean.getSecondaryScheduleAmount(), bean.getScheduleType().getSecondaryScheduleAmountLabel() + " is required.", "secondaryScheduleAmount");
		}
		if (bean.getScheduleType().isFlatFeePercent()) {
			ValidationUtils.assertNull(bean.getScheduleFrequency(), "Schedule frequencies do not apply to flat percentages.  i.e. A discount is that percentage regardless of the frequency.");
		}
		else {
			ValidationUtils.assertNotNull(bean.getScheduleFrequency(), "Schedule frequency is required.");
		}


		// Require Unique Names for Templates and Overlapping Date Ranges
		if (bean.isTemplate()) {
			BillingSchedule originalBean = (config.isInsert() ? null : getOriginalBean(bean));
			if (originalBean == null || !CompareUtils.isEqual(bean.getName(), originalBean.getName())) {
				List<BillingSchedule> scheduleList = dao.findByFields(new String[]{"name", "template"}, new Object[]{bean.getName(), true});
				for (BillingSchedule schedule : CollectionUtils.getIterable(scheduleList)) {
					if (!schedule.equals(bean) && DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), schedule.getStartDate(), schedule.getEndDate())) {
						throw new ValidationException("There already exists a template schedule with name [" + bean.getName() + "] and overlapping date range.  Please enter a unique name.");
					}
				}
			}
		}


		// If not a template, need to confirm billing definition or definitions (if shared) that use the schedule are valid
		if (!bean.isTemplate()) {
			if (bean.getBillingDefinition() != null) {
				if (bean.getScheduleType().isAnnualFee()) {
					ValidationUtils.assertNotNull(
							bean.getBillingDefinition().getSecondYearStartDate(),
							"Cannot add billing schedule of type ["
									+ bean.getScheduleType().getName()
									+ "] because it is an annual fee and second year start date is required for these types and is missing on the definition.  Please add second year start date to the definition to add this schedule.");
				}
				BillingDefinitionValidationUtils.validateBillingScheduleForDefinitions(bean, bean.getBillingDefinition(), null);
			}
			// If it's a new shared schedule, nothing linked to it...
			else if (!bean.isNewBean()) {
				String secondYear = null;
				List<BillingScheduleSharing> sharingList = getBillingDefinitionService().getBillingScheduleSharingListForSchedule(bean.getId());
				BillingDefinitionValidationUtils.validateBillingScheduleForDefinitions(bean, null, sharingList);
				for (BillingScheduleSharing shared : CollectionUtils.getIterable(sharingList)) {
					if (bean.getScheduleType().isAnnualFee()) {
						ValidationUtils.assertNotNull(
								shared.getReferenceTwo().getSecondYearStartDate(),
								"Cannot add billing schedule of type ["
										+ bean.getScheduleType().getName()
										+ "] to billing definition ["
										+ shared.getReferenceTwo().getLabel()
										+ "] because second year start date is required for these types and is missing on the definition.  Please add second year start date to the definition to add this schedule.");

						if (secondYear == null) {
							secondYear = DateUtils.fromDate(shared.getReferenceTwo().getSecondYearStartDate(), "MM/dd");
						}
						else {
							String sharedSecondYear = DateUtils.fromDate(shared.getReferenceTwo().getSecondYearStartDate(), "MM/dd");
							if (!secondYear.equals(sharedSecondYear)) {
								throw new ValidationException(
										"Billing Definitions that share an annual fee schedule must use the same month/day for the second year start date.  Found at least two different dates ["
												+ sharedSecondYear + ", " + secondYear + "].");
							}
						}
					}
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
