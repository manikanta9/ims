package com.clifton.billing.definition.workflow;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>{@link BillingDefinitionTransitionInvoiceWorkflowAction}</code> class is used on secondary approval of billing definitions to automatically reprocess their pending invoices
 * This ensures all pending invoices are processed using the latest definition setup.
 * <p>
 * If the invoices are part of a group, will only re-process if ALL of the shared definitions are all approved (Otherwise picks it up on last one being approved)
 * Note: Already a condition that doesn't let you approve invoices if the definitions aren't approved
 *
 * @author manderson
 */
public class BillingDefinitionTransitionInvoiceWorkflowAction implements WorkflowTransitionActionHandler<BillingDefinition>, ValidationAware {

	private BillingInvoiceService billingInvoiceService;

	private WorkflowTransitionService workflowTransitionService;


	/**
	 * When finding invoices for transition, the list of workflow statuses to INCLUDE (Open and Pending)
	 */
	private List<String> invoiceFilterIncludeWorkflowStatusNameList;

	/**
	 * When finding invoices for transition, the list of workflow states to EXCLUDE (Approved)w
	 */
	private List<String> invoiceFilterExcludeWorkflowStateNameList;


	/**
	 * The workflow transition name the invoice should execute.
	 * Note: The transition names are the same for invoice groups and invoices
	 */
	private String workflowTransitionName;


	/**
	 * In order to transition an invoice that is part of a group - all billing definitions must be in the specified status
	 * Ignores check on definition that is being transitioned
	 */
	private String groupedInvoiceBillingDefinitionRequiredStatus;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getWorkflowTransitionName(), "Workflow Transition Name is Required.");
		ValidationUtils.assertNotNull(getGroupedInvoiceBillingDefinitionRequiredStatus(), "Grouped Invoice Billing Definition Required Status is Required.");
	}


	@Override
	public BillingDefinition processAction(BillingDefinition billingDefinition, WorkflowTransition transition) {
		List<BillingInvoice> invoiceList = getBillingInvoiceListForDefinition(billingDefinition);
		for (BillingInvoice invoice : CollectionUtils.getIterable(invoiceList)) {
			if (isBillingInvoiceEligibleForTransition(invoice)) {
				if (invoice.getInvoiceGroup() != null) {
					transitionToWorkflowState(BillingInvoiceGroup.BILLING_INVOICE_GROUP_TABLE_NAME, invoice.getInvoiceGroup().getId());
				}
				else {
					transitionToWorkflowState(BillingInvoice.BILLING_INVOICE_TABLE_NAME, invoice.getId());
				}
			}
		}
		return billingDefinition;
	}


	private List<BillingInvoice> getBillingInvoiceListForDefinition(BillingDefinition billingDefinition) {
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(billingDefinition.getId());
		if (!CollectionUtils.isEmpty(getInvoiceFilterIncludeWorkflowStatusNameList())) {
			if (getInvoiceFilterIncludeWorkflowStatusNameList().size() == 1) {
				searchForm.setWorkflowStatusNameEquals(getInvoiceFilterIncludeWorkflowStatusNameList().get(0));
			}
			else {
				searchForm.setWorkflowStatusNames(CollectionUtils.toArray(getInvoiceFilterIncludeWorkflowStatusNameList(), String.class));
			}
		}
		if (!CollectionUtils.isEmpty(getInvoiceFilterExcludeWorkflowStateNameList())) {
			if (getInvoiceFilterExcludeWorkflowStateNameList().size() == 1) {
				searchForm.setExcludeWorkflowStateName(getInvoiceFilterExcludeWorkflowStateNameList().get(0));
			}
			else {
				searchForm.setExcludeWorkflowStateNames(CollectionUtils.toArray(getInvoiceFilterIncludeWorkflowStatusNameList(), String.class));
			}
		}
		return getBillingInvoiceService().getBillingInvoiceList(searchForm);
	}


	private boolean isBillingInvoiceEligibleForTransition(BillingInvoice invoice) {
		if (invoice.getInvoiceGroup() != null) {
			BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
			searchForm.setInvoiceGroupId(invoice.getInvoiceGroup().getId());
			List<BillingInvoice> groupedInvoices = getBillingInvoiceService().getBillingInvoiceList(searchForm);
			for (BillingInvoice groupedInvoice : groupedInvoices) {
				if (!CompareUtils.isEqual(invoice.getBillingDefinition(), groupedInvoice.getBillingDefinition())) {
					if (!getGroupedInvoiceBillingDefinitionRequiredStatus().equals(groupedInvoice.getBillingDefinition().getWorkflowStatus().getName())) {
						return false;
					}
				}
			}
		}
		return true;
	}


	private void transitionToWorkflowState(String tableName, int entityId) {
		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(tableName, MathUtils.getNumberAsLong(entityId));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
			if (StringUtils.isEqualIgnoreCase(getWorkflowTransitionName(), transition.getName())) {
				getWorkflowTransitionService().executeWorkflowTransitionByTransition(tableName, entityId, transition.getId());
				return;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Getter and Setter Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public List<String> getInvoiceFilterIncludeWorkflowStatusNameList() {
		return this.invoiceFilterIncludeWorkflowStatusNameList;
	}


	public void setInvoiceFilterIncludeWorkflowStatusNameList(List<String> invoiceFilterIncludeWorkflowStatusNameList) {
		this.invoiceFilterIncludeWorkflowStatusNameList = invoiceFilterIncludeWorkflowStatusNameList;
	}


	public List<String> getInvoiceFilterExcludeWorkflowStateNameList() {
		return this.invoiceFilterExcludeWorkflowStateNameList;
	}


	public void setInvoiceFilterExcludeWorkflowStateNameList(List<String> invoiceFilterExcludeWorkflowStateNameList) {
		this.invoiceFilterExcludeWorkflowStateNameList = invoiceFilterExcludeWorkflowStateNameList;
	}


	public String getWorkflowTransitionName() {
		return this.workflowTransitionName;
	}


	public void setWorkflowTransitionName(String workflowTransitionName) {
		this.workflowTransitionName = workflowTransitionName;
	}


	public String getGroupedInvoiceBillingDefinitionRequiredStatus() {
		return this.groupedInvoiceBillingDefinitionRequiredStatus;
	}


	public void setGroupedInvoiceBillingDefinitionRequiredStatus(String groupedInvoiceBillingDefinitionRequiredStatus) {
		this.groupedInvoiceBillingDefinitionRequiredStatus = groupedInvoiceBillingDefinitionRequiredStatus;
	}
}
