package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingDefinitionBillingBasisPostProcessor;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingDefinitionBillingBasisPostProcessorListByBillingDefinitionCache</code> caches the list of post processors for the given definition
 * Most of the time there are no post processors
 *
 * @author manderson
 */
@Component
public class BillingDefinitionBillingBasisPostProcessorListByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<BillingDefinitionBillingBasisPostProcessor, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingDefinitionBillingBasisPostProcessor bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
