package com.clifton.billing.definition.comparisons;

/**
 * Can be used for any of the following entities
 * BillingDefinition
 * BillingDefinitionInvestmentAccount
 * BillingDefinitionInvestmentAccountSecurityExclusion
 * BillingSchedule
 * BillingScheduleSharing
 * BillingScheduleTier
 * <p/>
 * Finds the Billing Definition(s) associated with the edit in given workflow status and returns True if NONE exist
 * <p/>
 * This condition is combined via an OR condition with which fields are allowed to be edited in any state.
 * Examples:
 * I can add start/end dates on the accounts on an approved billing definition.
 * I can add a note to a schedule on an approved billing definition
 * I can add a new billing definition or remove a billing definition from a shared schedule - and the only definition that needs to be in Draft status is the one I'm adding or removing
 * I can't change a tier on a schedule unless the billing definition (or all billing definitions associated with the schedule if it's shared) are in Draft status.
 *
 * @author manderson
 */
public class BillingDefinitionNotExistsComparison extends BillingDefinitionExistsComparison {


	@Override
	protected boolean isReverse() {
		return true;
	}
}
