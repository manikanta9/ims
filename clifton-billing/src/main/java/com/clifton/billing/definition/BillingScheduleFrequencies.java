package com.clifton.billing.definition;


/**
 * The <code>BillingScheduleFrequency</code> defines frequencies with the calculation type that they use.
 * <p>
 * By Default MonthsInPeriod is used for conversions, but DaysInPeriod can be used if set.
 * <p>
 * For example, Annual Percentages can be calculated in two ways, one is Annually which uses the Annual Rate / 4 - works the same using months (Annual Rate / 12) * 3 for quarterly rates.
 * The other is Annually/Daily which uses Annually / 365 and then calculates the rate for the number of days.
 * There is also Annually/Daily/Actual which wouldn't use 365 days but the actual number of days in the year.
 *
 * @author Mary Anderson
 */
public enum BillingScheduleFrequencies {

	ONCE(null, ""), //
	MONTHLY(BillingFrequencies.MONTHLY, "per Month"), //
	QUARTERLY(BillingFrequencies.QUARTERLY, "per Quarter"), //
	ANNUALLY(BillingFrequencies.ANNUALLY, "per Year"), //
	ANNUALLY_DAILY(BillingFrequencies.ANNUALLY, true, false, "per Year"), //
	ANNUALLY_DAILY_ACTUAL(BillingFrequencies.ANNUALLY, true, true, "per Year"), //
	SEMI_ANNUALLY(BillingFrequencies.SEMI_ANNUALLY, "per 6 months");

	////////////////////////////////////////////////////////////////////////////////


	BillingScheduleFrequencies(BillingFrequencies billingFrequency, String invoiceDescription) {
		this.billingFrequency = billingFrequency;
		this.invoiceDescription = invoiceDescription;
	}


	BillingScheduleFrequencies(BillingFrequencies billingFrequency, boolean useDailyConversion, boolean useActualDays, String invoiceDescription) {
		this.billingFrequency = billingFrequency;
		this.useDailyConversion = useDailyConversion;
		this.useActualDays = useActualDays;
		this.invoiceDescription = invoiceDescription;
	}

	////////////////////////////////////////////////////////////////////////////////

	private final BillingFrequencies billingFrequency;
	private boolean useDailyConversion;
	private boolean useActualDays;
	private final String invoiceDescription;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingFrequencies getBillingFrequency() {
		return this.billingFrequency;
	}


	public boolean isUseDailyConversion() {
		return this.useDailyConversion;
	}


	public boolean isUseActualDays() {
		return this.useActualDays;
	}


	public String getInvoiceDescription() {
		return this.invoiceDescription;
	}
}
