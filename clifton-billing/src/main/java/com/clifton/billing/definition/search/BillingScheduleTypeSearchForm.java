package com.clifton.billing.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class BillingScheduleTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Boolean sharedScheduleAllowed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getSharedScheduleAllowed() {
		return this.sharedScheduleAllowed;
	}


	public void setSharedScheduleAllowed(Boolean sharedScheduleAllowed) {
		this.sharedScheduleAllowed = sharedScheduleAllowed;
	}
}
