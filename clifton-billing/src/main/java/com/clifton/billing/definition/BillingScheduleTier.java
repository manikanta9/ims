package com.clifton.billing.definition;


import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;


/**
 * The <code>BillingScheduleTier</code> class defines a single tier for a multi-tier schedule:
 * <p>
 * 50,000,000 => 20bps
 * 200,000,000 => 15bps
 * 500,000,000 => 10bps
 * 1,000,000,000 => 5bps
 * => 4bps
 *
 * @author vgomelsky
 */
public class BillingScheduleTier extends BaseEntity<Integer> {

	private BillingSchedule billingSchedule;

	/**
	 * We define "up to" specified threshold, so when null then it's the max tier
	 */
	private BigDecimal thresholdAmount;

	/**
	 * The rate as a percentage for the tier
	 */
	private BigDecimal feePercent;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isMaxTier() {
		return getThresholdAmount() == null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingSchedule getBillingSchedule() {
		return this.billingSchedule;
	}


	public void setBillingSchedule(BillingSchedule billingSchedule) {
		this.billingSchedule = billingSchedule;
	}


	public BigDecimal getThresholdAmount() {
		return this.thresholdAmount;
	}


	public void setThresholdAmount(BigDecimal thresholdAmount) {
		this.thresholdAmount = thresholdAmount;
	}


	public BigDecimal getFeePercent() {
		return this.feePercent;
	}


	public void setFeePercent(BigDecimal feePercent) {
		this.feePercent = feePercent;
	}
}
