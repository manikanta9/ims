package com.clifton.billing.definition.validation;


import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleSharingValidator</code> validates on linking a new definition to a schedule that the frequencies match across all billing definitions
 * and billing frequency is valid for the schedules accrual frequency
 *
 * @author Mary Anderson
 */
@Component
public class BillingScheduleSharingValidator extends SelfRegisteringDaoValidator<BillingScheduleSharing> {

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT;
	}


	@Override
	public void validate(BillingScheduleSharing bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO method
	}


	@Override
	public void validate(BillingScheduleSharing bean, DaoEventTypes config, ReadOnlyDAO<BillingScheduleSharing> dao) throws ValidationException {
		BillingDefinitionValidationUtils.validateBillingScheduleForDefinitions(bean.getReferenceOne(), bean.getReferenceTwo(), getBillingDefinitionService().getBillingScheduleSharingListForSchedule(bean.getReferenceOne().getId()));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
