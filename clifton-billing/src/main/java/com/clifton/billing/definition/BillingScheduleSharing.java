package com.clifton.billing.definition;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>BillingScheduleSharing</code> class maps a billing schedule to corresponding
 * billing definition(s). A billing schedule can be shared across multiple billing definitions
 * (accounts) in order to get better pricing (tiered schedule type).
 * <p/>
 * Only BillingSchedule of type sharedSchedule = true can be shared.
 *
 * @author vgomelsky
 */
public class BillingScheduleSharing extends ManyToManyEntity<BillingSchedule, BillingDefinition, Integer> {

	// nothing here for now
}
