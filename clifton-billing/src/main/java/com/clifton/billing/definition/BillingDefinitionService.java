package com.clifton.billing.definition;


import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSearchForm;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSecurityExclusionSearchForm;
import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.billing.definition.search.BillingScheduleBillingDefinitionDependencySearchForm;
import com.clifton.billing.definition.search.BillingScheduleSearchForm;
import com.clifton.billing.definition.search.BillingScheduleTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.Date;
import java.util.List;


/**
 * @author vgomelsky
 */
public interface BillingDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	///////         Billing Schedule Type Business Methods 		       ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public BillingScheduleType getBillingScheduleType(short id);


	public List<BillingScheduleType> getBillingScheduleTypeList(BillingScheduleTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////         Billing Definition Business Methods 		       ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public BillingDefinition getBillingDefinition(int id);


	/**
	 * Also populates accounts and schedules (shared and non-shared)
	 */
	public BillingDefinition getBillingDefinitionPopulated(int id);


	public List<BillingDefinition> getBillingDefinitionList(BillingDefinitionSearchForm searchForm);


	/**
	 * Returns a list of all BillingDefinitionIds (including this one) that share schedules
	 * Also, if this definition shares with another definition that shares with another (linked by association)
	 * then they are considered to be shared
	 * i.e. a shares with b, and b shares with c, thus a shares with c
	 * <p>
	 * Start & End Dates are used to validate the definition and schedule is active for the invoice period so we don't
	 * link invoices together whose shared schedules no longer apply
	 * <p>
	 * Assumes the original definition passed has already been validated as active for the period
	 * Excludes cases where the schedule is set to "do not group invoice" which act as not shared
	 */
	@DoNotAddRequestMapping
	public List<Integer> getBillingDefinitionIdSharedList(int definitionId, Date startDate, Date endDate);


	public BillingDefinition saveBillingDefinition(BillingDefinition bean);


	public void deleteBillingDefinition(int id);


	////////////////////////////////////////////////////////////////////////////
	//////    Billing Definition Investment Account Business Methods 	  ////// 
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount(int id);


	public List<BillingDefinitionInvestmentAccount> getBillingDefinitionInvestmentAccountList(BillingDefinitionInvestmentAccountSearchForm searchForm);


	public List<BillingDefinitionInvestmentAccount> getBillingDefinitionInvestmentAccountListForDefinition(int definitionId);


	/**
	 * ONLY saves the account list off of the definition
	 */
	public BillingDefinition saveBillingDefinitionInvestmentAccountListForDefinition(BillingDefinition billingDefinition);


	public BillingDefinitionInvestmentAccount saveBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount bean);


	public void deleteBillingDefinitionInvestmentAccount(int id);


	////////////////////////////////////////////////////////////////////////////////
	// Billing Definition Investment Account Security Exclusion Business Methods ///
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionInvestmentAccountSecurityExclusion getBillingDefinitionInvestmentAccountSecurityExclusion(int id);


	public List<BillingDefinitionInvestmentAccountSecurityExclusion> getBillingDefinitionInvestmentAccountSecurityExclusionList(BillingDefinitionInvestmentAccountSecurityExclusionSearchForm searchForm);


	public BillingDefinitionInvestmentAccountSecurityExclusion saveBillingDefinitionInvestmentAccountSecurityExclusion(BillingDefinitionInvestmentAccountSecurityExclusion bean);


	public void deleteBillingDefinitionInvestmentAccountSecurityExclusion(int id);


	////////////////////////////////////////////////////////////////////////////
	///////           Billing Schedule Business Methods 		       ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public BillingSchedule getBillingSchedule(int id);


	/**
	 * Returns the schedule information to be loaded into the schedule window based on the given schedule id
	 * Validates that the selected schedule is a template
	 */
	public BillingSchedule getBillingScheduleForTemplate(int id);


	/**
	 * Returns full list of schedules for a definition - shared & not shared
	 *
	 * @param shared - if null returns shared & not-shared as one list
	 */
	public List<BillingSchedule> getBillingScheduleListForDefinition(int definitionId, Boolean shared);


	/**
	 * Returns all schedules used by the definitions passed - shared and specific
	 */
	public List<BillingSchedule> getBillingScheduleListForDefinitionIds(Integer[] definitionIds);


	/**
	 * Returns all NON template schedules filtered by search form properties
	 */
	public List<BillingSchedule> getBillingScheduleList(BillingScheduleSearchForm searchForm);


	/**
	 * Returns all TEMPLATE schedules, filtered by search form properties
	 */
	public List<BillingSchedule> getBillingScheduleTemplateList(BillingScheduleSearchForm searchForm);


	/**
	 * Used when archiving billing definitions only.  We are only changing the end date, so not necessary to retrieve and then save tiers or go through additional validation
	 */
	@DoNotAddRequestMapping
	public BillingSchedule saveBillingScheduleSimple(BillingSchedule bean);


	public BillingSchedule saveBillingSchedule(BillingSchedule bean);


	/**
	 * Allows copying a shared schedule and optionally selecting which billing definition ids to copy to the new schedule. Returns the new schedule
	 *
	 * @param copyFromId           - Shared Schedule that is being copied
	 * @param startDate            - Start date for the new schedule (end date will be set to blank)
	 * @param endExisting          - If true will set the end date on the existing schedule to above Start Date - 1 Day
	 * @param billingDefinitionIds - The billing definitions to include on the new shared schedule
	 * @return the newly created shared schedule
	 */
	public BillingSchedule copyBillingScheduleShared(int copyFromId, Date startDate, boolean endExisting, Integer[] billingDefinitionIds);


	public void deleteBillingSchedule(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////           Billing Schedule Sharing Methods 		        ////////
	////////////////////////////////////////////////////////////////////////////


	public List<BillingScheduleSharing> getBillingScheduleSharingListForSchedule(int scheduleId);


	public BillingScheduleSharing linkBillingScheduleSharing(int scheduleId, int definitionId);


	public void deleteBillingScheduleSharing(int id);


	public void deleteBillingScheduleSharingForScheduleAndDefinition(int scheduleId, int definitionId);


	////////////////////////////////////////////////////////////////////////////////
	///////  Billing Schedule Billing Definition Dependency Business Methods  //////
	////////////////////////////////////////////////////////////////////////////////


	public BillingScheduleBillingDefinitionDependency getBillingScheduleBillingDefinitionDependency(int id);


	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyListBySchedule(int scheduleId);


	/**
	 * Returns the list of BillingScheduleBillingDefinitionDependency objects for the given billing definition
	 *
	 * @param prerequisite - if true, returns the list of dependencies that depend on the given billing definition (BillingDefinitionID) else returns the list of dependencies the given billing definition depends on (BillingSchedule.BillingDefinitionID)
	 */
	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyListByDefinition(int definitionId, boolean prerequisite);


	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyList(BillingScheduleBillingDefinitionDependencySearchForm searchForm);


	public BillingScheduleBillingDefinitionDependency saveBillingScheduleBillingDefinitionDependency(BillingScheduleBillingDefinitionDependency bean);


	public void deleteBillingScheduleBillingDefinitionDependency(int id);


	////////////////////////////////////////////////////////////////////////////
	///////    Billing Definition Billing Basis Post Processor Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionBillingBasisPostProcessor getBillingDefinitionBillingBasisPostProcessor(int id);


	public List<BillingDefinitionBillingBasisPostProcessor> getBillingDefinitionBillingBasisPostProcessorListByDefinition(int definitionId);


	public BillingDefinitionBillingBasisPostProcessor saveBillingDefinitionBillingBasisPostProcessor(BillingDefinitionBillingBasisPostProcessor bean);


	public void deleteBillingDefinitionBillingBasisPostProcessor(int id);
}
