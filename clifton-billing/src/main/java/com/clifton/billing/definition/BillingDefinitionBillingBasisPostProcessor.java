package com.clifton.billing.definition;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>BillingDefinitionBillingBasisPostProcessor</code> class can be used to define for a particular BillingDefinition
 * how billing basis should be adjusted prior to calculating schedule amounts.
 * <p>
 * Example: When Duke Energy PIOS accounts billing basis is less than 100 million will shift billing basis from the CCY account to the PIOS accounts to fill it up to 100 million
 */
public class BillingDefinitionBillingBasisPostProcessor extends ManyToManyEntity<BillingDefinition, SystemBean, Integer> {

	/**
	 * If there were multiple, then this would allow applying the post processors in specific order
	 */
	private Integer order;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
