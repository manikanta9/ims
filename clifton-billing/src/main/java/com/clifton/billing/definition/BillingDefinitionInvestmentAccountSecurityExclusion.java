package com.clifton.billing.definition;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.condition.SystemCondition;

import java.util.Date;


/**
 * The <code>BillingDefinitionInvestmentAccountSecurityExclusion</code> is used to exclude a specific security or securities from billing valuations for a given date range and optionally based on a condition.
 * <p>
 * Example: Account that holds Swaps, but one particular Swap is waived from billing (explicitly exclude that security)
 * Account that holds TIPS and during roll period there is a time when we hold both the old and new securities -  exclude based on condition where security is a TIPS and Valuation Date = Maturity Date.
 * <p>
 * Conditions make it flexible enough that should be able to come up with logic for any situations.
 *
 * @author manderson
 */
public class BillingDefinitionInvestmentAccountSecurityExclusion extends BaseEntity<Integer> {

	/**
	 * The billing account this exclusion belongs to.  Supported ONLY if the billing account's valuation type supports instrument groups - that means the valuation is available to check security level information
	 */
	private BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount;

	/**
	 * If exclusion applies to a specific security
	 */
	private InvestmentSecurity investmentSecurity;


	/**
	 * If the exclusion should apply based on a specific condition.  Example, exclude TIPS positions on maturity date (usually hold the old and the new and don't want to "double" count on the bill to the client.
	 * IF both security and security group are left blank, this condition is required.
	 */
	private SystemCondition excludeCondition;


	/**
	 * Start date for this exclusion - compares to the billing basis snapshot date
	 */
	private Date startDate;

	/**
	 * End date for this exclusion - compares to the billing basis snapshot date
	 */
	private Date endDate;


	/**
	 * Internal note about this exclusion
	 */
	private String exclusionNote;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getExcludeLabel() {
		String label = "";
		if (getInvestmentSecurity() != null) {
			label += getInvestmentSecurity().getLabel() + " ";
		}
		if (getExcludeCondition() != null) {
			label += getExcludeCondition().getName() + " ";
		}
		label += DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
		return label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount() {
		return this.billingDefinitionInvestmentAccount;
	}


	public void setBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount) {
		this.billingDefinitionInvestmentAccount = billingDefinitionInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public SystemCondition getExcludeCondition() {
		return this.excludeCondition;
	}


	public void setExcludeCondition(SystemCondition excludeCondition) {
		this.excludeCondition = excludeCondition;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getExclusionNote() {
		return this.exclusionNote;
	}


	public void setExclusionNote(String exclusionNote) {
		this.exclusionNote = exclusionNote;
	}
}
