package com.clifton.billing.definition.jobs;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.transition.jobs.BaseWorkflowTransitionExecuteJob;

import java.util.Date;
import java.util.List;


/**
 * The <code>BillingDefinitionArchiveTransitionJob</code> is used to attempt to move billing definitions that have ended a specified number of days back to Archived Workflow State.
 * There is a condition on the transition, so if that doesn't pass the transition is skipped (for example all invoices must also be paid in order to archive the definition)
 *
 * @author manderson
 */
public class BillingDefinitionArchiveTransitionJob extends BaseWorkflowTransitionExecuteJob<BillingDefinition, BillingDefinitionSearchForm> {


	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This is used to limit the billing definitions to find that might be eligible for transitioning to archived.
	 */
	private Integer endDateMinDaysBack;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		super.validate();
		ValidationUtils.assertNotNull(getEndDateMinDaysBack(), "End Date Minimum Days Back Is Required.");
	}


	@Override
	protected BillingDefinitionSearchForm configureWorkflowAwareSearchForm() {
		BillingDefinitionSearchForm searchForm = new BillingDefinitionSearchForm();
		Date minEndDate = DateUtils.clearTime(new Date());
		minEndDate = DateUtils.addDays(minEndDate, -getEndDateMinDaysBack());
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN_OR_EQUALS, minEndDate));
		return searchForm;
	}


	@Override
	protected List<BillingDefinition> getWorkflowAwareEntityList(BillingDefinitionSearchForm searchForm) {
		return getBillingDefinitionService().getBillingDefinitionList(searchForm);
	}


	@Override
	protected String getWorkflowAwareEntityTableName() {
		return BillingDefinition.BILLING_DEFINITION_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public Integer getEndDateMinDaysBack() {
		return this.endDateMinDaysBack;
	}


	public void setEndDateMinDaysBack(Integer endDateMinDaysBack) {
		this.endDateMinDaysBack = endDateMinDaysBack;
	}
}
