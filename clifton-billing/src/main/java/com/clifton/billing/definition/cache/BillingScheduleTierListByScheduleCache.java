package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleTier;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleTierListByScheduleCache</code> caches the list of tiered for a schedule
 *
 * @author manderson
 */
@Component
public class BillingScheduleTierListByScheduleCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleTier, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "billingSchedule.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleTier bean) {
		if (bean.getBillingSchedule() != null) {
			return bean.getBillingSchedule().getId();
		}
		return null;
	}
}
