package com.clifton.billing.definition;


import com.clifton.billing.billingbasis.BillingBasisExternal;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSearchForm;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSecurityExclusionSearchForm;
import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.billing.definition.search.BillingScheduleBillingDefinitionDependencySearchForm;
import com.clifton.billing.definition.search.BillingScheduleSearchForm;
import com.clifton.billing.definition.search.BillingScheduleTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class BillingDefinitionServiceImpl implements BillingDefinitionService {

	private AdvancedUpdatableDAO<BillingScheduleType, Criteria> billingScheduleTypeDAO;
	private AdvancedUpdatableDAO<BillingDefinition, Criteria> billingDefinitionDAO;
	private AdvancedUpdatableDAO<BillingDefinitionInvestmentAccount, Criteria> billingDefinitionInvestmentAccountDAO;
	private AdvancedUpdatableDAO<BillingDefinitionInvestmentAccountSecurityExclusion, Criteria> billingDefinitionInvestmentAccountSecurityExclusionDAO;
	private AdvancedUpdatableDAO<BillingScheduleBillingDefinitionDependency, Criteria> billingScheduleBillingDefinitionDependencyDAO;
	private AdvancedUpdatableDAO<BillingSchedule, Criteria> billingScheduleDAO;
	private AdvancedUpdatableDAO<BillingScheduleSharing, Criteria> billingScheduleSharingDAO;
	private AdvancedUpdatableDAO<BillingScheduleTier, Criteria> billingScheduleTierDAO;
	private AdvancedUpdatableDAO<BillingDefinitionBillingBasisPostProcessor, Criteria> billingDefinitionBillingBasisPostProcessorDAO;

	private DaoSingleKeyListCache<BillingDefinitionInvestmentAccount, Integer> billingDefinitionInvestmentAccountListByDefinitionCache;
	private DaoSingleKeyListCache<BillingScheduleTier, Integer> billingScheduleTierListByScheduleCache;
	private DaoSingleKeyListCache<BillingScheduleSharing, Integer> billingScheduleSharingListByDefinitionCache;
	private DaoSingleKeyListCache<BillingScheduleSharing, Integer> billingScheduleSharingListByScheduleCache;

	// These are rare, so each list is cached for quicker retrieval
	private DaoSingleKeyListCache<BillingDefinitionBillingBasisPostProcessor, Integer> billingDefinitionBillingBasisPostProcessorListByDefinitionCache;
	private DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByScheduleCache;
	private DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByDefinitionCache;
	private DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache;

	private WorkflowDefinitionService workflowDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	///////         Billing Schedule Type Business Methods 		       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingScheduleType getBillingScheduleType(short id) {
		return getBillingScheduleTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingScheduleType> getBillingScheduleTypeList(BillingScheduleTypeSearchForm searchForm) {
		return getBillingScheduleTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////         Billing Definition Business Methods 		       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingDefinition getBillingDefinition(int id) {
		return getBillingDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public BillingDefinition getBillingDefinitionPopulated(int id) {
		BillingDefinition definition = getBillingDefinition(id);
		// Set List Information
		if (definition != null) {
			definition.setInvestmentAccountList(getBillingDefinitionInvestmentAccountListForDefinition(id));
			// Specific Schedules
			definition.setBillingScheduleList(getBillingScheduleDAO().findByField("billingDefinition.id", id));
			// Shared Schedules
			List<BillingScheduleSharing> sharedList = getBillingScheduleSharingListByDefinition(id);
			if (!CollectionUtils.isEmpty(sharedList)) {
				definition.setBillingScheduleSharedList(getBillingScheduleDAO().findByPrimaryKeys(BeanUtils.getPropertyValues(sharedList, "referenceOne.id", Integer.class)));
			}
		}
		return definition;
	}


	@Override
	public List<BillingDefinition> getBillingDefinitionList(final BillingDefinitionSearchForm searchForm) {
		if (searchForm.getRevenueType() != null) {
			searchForm.setRevenue(searchForm.getRevenueType().isRevenue());
			if (searchForm.getRevenueType().isRevenueShare()) {
				if (searchForm.getRevenueType().isExternal()) {
					searchForm.setRevenueShareExternalCompanyIdNull(false);
				}
				else {
					searchForm.setRevenueShareInternalCompanyIdNull(false);
				}
			}
		}

		return getBillingDefinitionDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	public List<Integer> getBillingDefinitionIdSharedList(int definitionId, Date startDate, Date endDate) {
		List<Integer> definitionList = new ArrayList<>();
		return getBillingDefinitionIdSharedListImpl(definitionId, definitionList, startDate, endDate);
	}


	private List<Integer> getBillingDefinitionIdSharedListImpl(int definitionId, List<Integer> list, Date startDate, Date endDate) {
		// If the list already contains this definition, then don't check it again
		if (!list.contains(definitionId)) {
			list.add(definitionId);

			List<BillingScheduleSharing> ssList = getBillingScheduleSharingListByDefinition(definitionId);
			for (BillingScheduleSharing ss : CollectionUtils.getIterable(ssList)) {
				if (!ss.getReferenceOne().isDoNotGroupInvoice()) {
					List<BillingScheduleSharing> scheduleSharedList = getBillingScheduleSharingListForSchedule(ss.getReferenceOne().getId());
					for (BillingScheduleSharing shared : CollectionUtils.getIterable(scheduleSharedList)) {
						if (!list.contains(shared.getReferenceTwo().getId())) {
							// Make sure the definition is active during the period
							if (DateUtils.isOverlapInDates(startDate, endDate, shared.getReferenceTwo().getStartDate(), shared.getReferenceTwo().getEndDate())) {
								// Make sure the schedule is active during the period
								if (DateUtils.isOverlapInDates(startDate, endDate, shared.getReferenceOne().getStartDate(), shared.getReferenceOne().getEndDate())) {
									list = getBillingDefinitionIdSharedListImpl(shared.getReferenceTwo().getId(), list, startDate, endDate);
								}
							}
						}
					}
				}
			}
		}
		return list;
	}


	@Override
	@Transactional
	public BillingDefinition saveBillingDefinition(BillingDefinition bean) {
		return getBillingDefinitionDAO().save(bean);
	}


	@Override
	public void deleteBillingDefinition(int id) {
		getBillingDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////     Billing Definition Investment Account Business Methods   ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingDefinitionInvestmentAccount getBillingDefinitionInvestmentAccount(int id) {
		return getBillingDefinitionInvestmentAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingDefinitionInvestmentAccount> getBillingDefinitionInvestmentAccountListForDefinition(int definitionId) {
		return getBillingDefinitionInvestmentAccountListByDefinitionCache().getBeanListForKeyValue(getBillingDefinitionInvestmentAccountDAO(), definitionId);
	}


	@Override
	public List<BillingDefinitionInvestmentAccount> getBillingDefinitionInvestmentAccountList(BillingDefinitionInvestmentAccountSearchForm searchForm) {
		if (Boolean.TRUE.equals(searchForm.getExternalOnly())) {
			searchForm.setBillingBasisValuationSourceTableName(BillingBasisExternal.BILLING_BASIS_EXTERNAL_TABLE_NAME);
		}
		return getBillingDefinitionInvestmentAccountListImpl(searchForm);
	}


	private List<BillingDefinitionInvestmentAccount> getBillingDefinitionInvestmentAccountListImpl(final BillingDefinitionInvestmentAccountSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveDefinition() != null) {
					String alias = getPathAlias("referenceOne", criteria);
					criteria.add(ActiveExpressionForDates.forActiveWithAlias(searchForm.getActiveDefinition(), alias));
				}
			}
		};
		return getBillingDefinitionInvestmentAccountDAO().findBySearchCriteria(config);
	}


	@Override
	public BillingDefinition saveBillingDefinitionInvestmentAccountListForDefinition(BillingDefinition billingDefinition) {
		ValidationUtils.assertNotNull(billingDefinition, "Billing Definition is required");

		List<BillingDefinitionInvestmentAccount> originalAccountList = getBillingDefinitionInvestmentAccountListForDefinition(billingDefinition.getId());

		for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(billingDefinition.getInvestmentAccountList())) {
			billingAccount.setReferenceOne(billingDefinition);
			validateBillingDefinitionInvestmentAccount(billingAccount);
		}
		getBillingDefinitionInvestmentAccountDAO().saveList(billingDefinition.getInvestmentAccountList(), originalAccountList);
		return billingDefinition;
	}


	@Override
	public BillingDefinitionInvestmentAccount saveBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount bean) {
		validateBillingDefinitionInvestmentAccount(bean);
		return getBillingDefinitionInvestmentAccountDAO().save(bean);
	}


	@Override
	public void deleteBillingDefinitionInvestmentAccount(int id) {
		getBillingDefinitionInvestmentAccountDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateBillingDefinitionInvestmentAccount(BillingDefinitionInvestmentAccount billingAccount) {
		ValidationUtils.assertNotNull(billingAccount.getBillingBasisValuationType(), "Billing Basis Valuation Type is required.");
		if (billingAccount.getBillingBasisValuationType().isNoBillingBasis()) {
			ValidationUtils.assertNull(billingAccount.getBillingBasisCalculationType(), "Billing Basis Valuation Type [" + billingAccount.getBillingBasisValuationType().getName()
					+ "] does not have a billing basis and cannot have a calculation type bean selected.");
		}
		else {
			ValidationUtils.assertNotNull(billingAccount.getBillingBasisCalculationType(), "Billing Basis Valuation Type [" + billingAccount.getBillingBasisValuationType().getName()
					+ "] must have a billing basis calculator bean selected.");

			if (billingAccount.getBillingBasisCalculationType().isDateRangeSpecific()) {
				ValidationUtils.assertFalse(billingAccount.getReferenceOne().isPaymentInAdvance(), "Cannot use billing basis calculation type [" + billingAccount.getBillingBasisCalculationType().getName()
						+ "] with an payment in advance billing definition.");
			}
		}

		if (billingAccount.getInvestmentGroup() != null && !billingAccount.getBillingBasisValuationType().isInvestmentGroupAllowed()) {
			throw new ValidationException("Billing Basis Valuation Type [" + billingAccount.getBillingBasisValuationType().getName()
					+ "] cannot apply to a specific investment group.  Please clear the investment group selection for this valuation type.");
		}


		// Any valuation type that uses managers would require a selection
		if (billingAccount.getBillingBasisValuationType().isInvestmentManagerRequired()) {
			ValidationUtils.assertNotNull(billingAccount.getInvestmentManagerAccount(), "Billing Basis Valuation Type [" + billingAccount.getBillingBasisValuationType().getName()
					+ "] must be applied to a specific investment manager account.  Please select a manager account for this valuation type.");
		}
		// If not required, then not supported
		else {
			ValidationUtils.assertNull(billingAccount.getInvestmentManagerAccount(), "Billing Basis Valuation Type [" + billingAccount.getBillingBasisValuationType().getName()
					+ "] cannot apply to a specific investment manager account.  Please clear the investment manager account selection for this valuation type.");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	// Billing Definition Investment Account Security Exclusion Business Methods ///
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingDefinitionInvestmentAccountSecurityExclusion getBillingDefinitionInvestmentAccountSecurityExclusion(int id) {
		return getBillingDefinitionInvestmentAccountSecurityExclusionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingDefinitionInvestmentAccountSecurityExclusion> getBillingDefinitionInvestmentAccountSecurityExclusionList(BillingDefinitionInvestmentAccountSecurityExclusionSearchForm searchForm) {
		return getBillingDefinitionInvestmentAccountSecurityExclusionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * See {@link com.clifton.billing.definition.validation.BillingDefinitionInvestmentAccountSecurityExclusionValidator} for validation
	 */
	@Override
	public BillingDefinitionInvestmentAccountSecurityExclusion saveBillingDefinitionInvestmentAccountSecurityExclusion(BillingDefinitionInvestmentAccountSecurityExclusion bean) {
		return getBillingDefinitionInvestmentAccountSecurityExclusionDAO().save(bean);
	}


	@Override
	public void deleteBillingDefinitionInvestmentAccountSecurityExclusion(int id) {
		getBillingDefinitionInvestmentAccountSecurityExclusionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	///////           Billing Schedule Business Methods 		       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingSchedule getBillingSchedule(int id) {
		BillingSchedule bean = getBillingScheduleDAO().findByPrimaryKey(id);
		if (bean != null) {
			// If Tiered Fee - Get Tiers
			// Sorted in proper order
			bean.setScheduleTierList(BeanUtils.sortWithFunctions(getBillingScheduleTierListBySchedule(id), CollectionUtils.createList(BillingScheduleTier::isMaxTier, BillingScheduleTier::getThresholdAmount), CollectionUtils.createList(true, true)));
		}
		return bean;
	}


	@Override
	public BillingSchedule getBillingScheduleForTemplate(int id) {
		BillingSchedule scheduleTemplate = getBillingSchedule(id);
		ValidationUtils.assertTrue(scheduleTemplate.isTemplate(), "Selected schedule is not a template");

		BillingSchedule schedule = BeanUtils.cloneBean(scheduleTemplate, false, false);
		schedule.setId(null);
		schedule.setTemplate(false);
		if (scheduleTemplate.isTiersExist()) {
			List<BillingScheduleTier> tierList = new ArrayList<>();
			for (BillingScheduleTier tier : scheduleTemplate.getScheduleTierList()) {
				BillingScheduleTier newTier = BeanUtils.cloneBean(tier, false, false);
				newTier.setBillingSchedule(schedule);
				tierList.add(newTier);
			}
			schedule.setScheduleTierList(tierList);
		}
		return schedule;
	}


	@Override
	public List<BillingSchedule> getBillingScheduleListForDefinition(int definitionId, Boolean shared) {
		List<BillingSchedule> list = null;
		if (shared == null || shared) {
			// Shared Schedules
			List<BillingScheduleSharing> sharedList = getBillingScheduleSharingListByDefinition(definitionId);
			if (!CollectionUtils.isEmpty(sharedList)) {
				list = getBillingScheduleDAO().findByPrimaryKeys(BeanUtils.getPropertyValues(sharedList, billingScheduleSharing -> billingScheduleSharing.getReferenceOne().getId(), Integer.class));
			}
		}
		if (shared == null || !shared) {
			if (list == null) {
				list = new ArrayList<>();
			}
			list.addAll(getBillingScheduleDAO().findByField("billingDefinition.id", definitionId));
		}
		return list;
	}


	@Override
	public List<BillingSchedule> getBillingScheduleListForDefinitionIds(Integer[] definitionIds) {
		List<BillingSchedule> list = getBillingScheduleDAO().findByField("billingDefinition.id", definitionIds);
		// Shared Schedules
		List<BillingScheduleSharing> sharedList = getBillingScheduleSharingDAO().findByField("referenceTwo.id", definitionIds);
		if (!CollectionUtils.isEmpty(sharedList)) {
			if (list == null) {
				list = new ArrayList<>();
			}
			for (BillingScheduleSharing bss : sharedList) {
				if (!list.contains(bss.getReferenceOne())) {
					list.add(bss.getReferenceOne());
				}
			}
		}
		return list;
	}


	@Override
	public List<BillingSchedule> getBillingScheduleList(final BillingScheduleSearchForm searchForm) {
		if (searchForm.getActiveDefinition() != null) {
			ValidationUtils.assertNotNull(searchForm.getSharedSchedule(), "Active Definition Filter can only be used if shared schedule filter is set.");
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// Real Schedules Only - Exclude Templates
				criteria.add(Restrictions.eq("template", false));

				// NOTE: TEMPLATE LIST DOESN'T USE THIS ADDITIONAL CRITERIA - IF ANY CUSTOM CRITERIA IS ADDED HERE THAT SHOULD APPLY TO TEMPLATES WILL NEED TO ADD THERE OR MAKE THE CONFIGURER RE-USABLE
				if (searchForm.getActiveDefinition() != null) {
					if (BooleanUtils.isTrue(searchForm.getSharedSchedule())) {
						// Shared Schedules must check the BillingDefinitions on BillingScheduleSharing table for the schedule
						DetachedCriteria sub = DetachedCriteria.forClass(BillingScheduleSharing.class, "sharing");
						sub.setProjection(Projections.property("id"));
						sub.add(Restrictions.eqProperty("referenceOne.id", criteria.getAlias() + ".id"));
						sub.createAlias("referenceTwo", "definition");
						sub.add(ActiveExpressionForDates.forActiveWithAlias(true, "definition"));
						// Active Definition - then where exists at least one active
						if (BooleanUtils.isTrue(searchForm.getActiveDefinition())) {
							criteria.add(Subqueries.exists(sub));
						}
						// Inactive Definition - then where not exists at least one active
						else {
							criteria.add(Subqueries.notExists(sub));
						}
					}
					else {
						// Definition Schedules can do a direct join to check the definition
						String alias = getPathAlias("billingDefinition", criteria);
						criteria.add(ActiveExpressionForDates.forActiveWithAlias(searchForm.getActiveDefinition(), alias));
					}
				}
			}
		};
		return getBillingScheduleDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<BillingSchedule> getBillingScheduleTemplateList(BillingScheduleSearchForm searchForm) {
		// NOTE: WE DON'T NEED ADDITIONAL CRITERIA FOR BILLING DEFINITIONS BECAUSE TEMPLATES AREN'T ASSOCIATED WITH BILLING DEFINITIONS
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// Template Schedules Only - Exclude Real Schedules
				criteria.add(Restrictions.eq("template", true));
			}
		};
		return getBillingScheduleDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	public BillingSchedule saveBillingScheduleSimple(BillingSchedule bean) {
		return getBillingScheduleDAO().save(bean);
	}


	@Override
	@Transactional
	public BillingSchedule saveBillingSchedule(BillingSchedule bean) {
		List<BillingScheduleTier> originalTierList = null;
		BillingDefinition autoApplySharedScheduleToDefinition = null;
		if (!bean.isNewBean()) {
			originalTierList = getBillingScheduleTierListBySchedule(bean.getId());
		}
		if (bean.getScheduleType().isTiered()) {
			ValidationUtils.assertNotEmpty(bean.getScheduleTierList(), "At least one tier is required for tiered schedules.");
			for (BillingScheduleTier tier : CollectionUtils.getIterable(bean.getScheduleTierList())) {
				tier.setBillingSchedule(bean);
			}
		}
		else {
			bean.setScheduleTierList(null);
		}
		if (bean.isTemplate()) {
			bean.setSharedSchedule(false);
			ValidationUtils.assertNull(bean.getBillingDefinition(), "Billing Definitions are not allowed to be selected for schedule templates.");
			ValidationUtils.assertNull(bean.getInvestmentAccount(), "Specific account selection is not allowed for schedule templates");
			ValidationUtils.assertNull(bean.getInvestmentAccountGroup(), "Specific account group selection is not allowed for schedule templates");
		}
		else {
			if (bean.isSharedSchedule()) {
				// Shouldn't be possible from UI
				ValidationUtils.assertNull(bean.getBillingDefinition(), "Billing Definition cannot be set directly on a shared schedule.");
				// But can for the auto apply
				autoApplySharedScheduleToDefinition = bean.getAutoApplySharedScheduleToDefinition();
			}
			else {
				ValidationUtils.assertNotNull(bean.getBillingDefinition(), "Billing Definition is required for a non-shared schedule.");
				bean.setDoNotGroupInvoice(false); // Only applies to Shared Schedules
			}
		}
		List<BillingScheduleTier> newTierList = bean.getScheduleTierList();
		bean = getBillingScheduleDAO().save(bean);
		getBillingScheduleTierDAO().saveList(newTierList, originalTierList);
		bean.setScheduleTierList(newTierList);
		if (autoApplySharedScheduleToDefinition != null) {
			linkBillingScheduleSharing(bean.getId(), autoApplySharedScheduleToDefinition.getId());
		}
		return bean;
	}


	@Override
	@Transactional
	public BillingSchedule copyBillingScheduleShared(int copyFromId, Date startDate, boolean endExisting, Integer[] billingDefinitionIds) {
		BillingSchedule copyFrom = getBillingSchedule(copyFromId);
		ValidationUtils.assertNotNull(copyFrom, "Cannot find existing billing schedule with ID " + copyFromId);
		ValidationUtils.assertTrue(copyFrom.isSharedSchedule(), "Selected schedule " + copyFrom.getLabel() + " is not a shared schedule.  This copy feature is available for shared schedules only.");

		BillingSchedule newSchedule = BeanUtils.cloneBean(copyFrom, false, false);
		newSchedule.setStartDate(startDate);
		newSchedule.setEndDate(null);
		if (copyFrom.isTiersExist()) {
			newSchedule.setScheduleTierList(new ArrayList<>());
			for (BillingScheduleTier tier : copyFrom.getScheduleTierList()) {
				BillingScheduleTier newTier = BeanUtils.cloneBean(tier, false, false);
				newTier.setBillingSchedule(newSchedule);
				newSchedule.getScheduleTierList().add(newTier);
			}
		}
		saveBillingSchedule(newSchedule);

		if (endExisting) {
			copyFrom.setEndDate(DateUtils.addDays(newSchedule.getStartDate(), -1));
			saveBillingSchedule(copyFrom);
		}

		if (billingDefinitionIds != null && billingDefinitionIds.length > 0) {
			for (Integer billingDefinitionId : billingDefinitionIds) {
				linkBillingScheduleSharing(newSchedule.getId(), billingDefinitionId);
			}
		}

		return newSchedule;
	}


	@Override
	@Transactional
	public void deleteBillingSchedule(int id) {
		getBillingScheduleTierDAO().deleteList(getBillingScheduleTierListBySchedule(id));
		getBillingScheduleSharingDAO().deleteList(getBillingScheduleSharingDAO().findByField("referenceOne.id", id));
		getBillingScheduleDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Billing Schedule Tier Methods               //////////
	////////////////////////////////////////////////////////////////////////////////


	private List<BillingScheduleTier> getBillingScheduleTierListBySchedule(int scheduleId) {
		return getBillingScheduleTierListByScheduleCache().getBeanListForKeyValue(getBillingScheduleTierDAO(), scheduleId);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////             Billing Schedule Sharing Methods              //////////
	////////////////////////////////////////////////////////////////////////////////


	private BillingScheduleSharing getBillingScheduleSharingForScheduleAndDefinition(int scheduleId, int definitionId) {
		return getBillingScheduleSharingDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{scheduleId, definitionId});
	}


	@Override
	public List<BillingScheduleSharing> getBillingScheduleSharingListForSchedule(int scheduleId) {
		return getBillingScheduleSharingListByScheduleCache().getBeanListForKeyValue(getBillingScheduleSharingDAO(), scheduleId);
	}


	private List<BillingScheduleSharing> getBillingScheduleSharingListByDefinition(int definitionId) {
		return getBillingScheduleSharingListByDefinitionCache().getBeanListForKeyValue(getBillingScheduleSharingDAO(), definitionId);
	}


	@Override
	public BillingScheduleSharing linkBillingScheduleSharing(int scheduleId, int definitionId) {
		// Validate Link Doesn't Exist First
		ValidationUtils.assertNull(getBillingScheduleSharingForScheduleAndDefinition(scheduleId, definitionId), "The link between the selected schedule and definition already exists.");

		BillingScheduleSharing bean = new BillingScheduleSharing();
		bean.setReferenceOne(getBillingSchedule(scheduleId));
		bean.setReferenceTwo(getBillingDefinition(definitionId));


		return getBillingScheduleSharingDAO().save(bean);
	}


	@Override
	public void deleteBillingScheduleSharing(int id) {
		getBillingScheduleSharingDAO().delete(id);
	}


	@Override
	public void deleteBillingScheduleSharingForScheduleAndDefinition(int scheduleId, int definitionId) {
		BillingScheduleSharing bean = getBillingScheduleSharingForScheduleAndDefinition(scheduleId, definitionId);
		if (bean != null) {
			getBillingScheduleSharingDAO().delete(bean.getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////   Billing Schedule Billing Definition Dependency Business Methods  /////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingScheduleBillingDefinitionDependency getBillingScheduleBillingDefinitionDependency(int id) {
		return getBillingScheduleBillingDefinitionDependencyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyListBySchedule(int scheduleId) {
		return getBillingScheduleBillingDefinitionDependencyListByScheduleCache().getBeanListForKeyValue(getBillingScheduleBillingDefinitionDependencyDAO(), scheduleId);
	}


	@Override
	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyListByDefinition(int definitionId, boolean prerequisite) {
		if (prerequisite) {
			// List of dependencies that depend on selected definition (i.e. selected definition is a prerequisite of)
			return getBillingScheduleBillingDefinitionDependencyListByDefinitionCache().getBeanListForKeyValue(getBillingScheduleBillingDefinitionDependencyDAO(), definitionId);
		}
		// List of dependencies that selected definition depend on
		return getBillingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache().getBeanListForKeyValue(getBillingScheduleBillingDefinitionDependencyDAO(), definitionId);
	}


	@Override
	public List<BillingScheduleBillingDefinitionDependency> getBillingScheduleBillingDefinitionDependencyList(BillingScheduleBillingDefinitionDependencySearchForm searchForm) {
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActive() != null) {
					String alias = getPathAlias("billingSchedule", criteria);
					criteria.add(ActiveExpressionForDates.forActiveWithAlias(searchForm.getActive(), alias));
				}
			}
		};

		return getBillingScheduleBillingDefinitionDependencyDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	public BillingScheduleBillingDefinitionDependency saveBillingScheduleBillingDefinitionDependency(BillingScheduleBillingDefinitionDependency bean) {
		validateBillingScheduleBillingDefinitionDependency(bean);

		return getBillingScheduleBillingDefinitionDependencyDAO().save(bean);
	}


	@Override
	public void deleteBillingScheduleBillingDefinitionDependency(int id) {
		getBillingScheduleBillingDefinitionDependencyDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////          Billing Definition Link Helper Methods              /////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateBillingScheduleBillingDefinitionDependency(BillingScheduleBillingDefinitionDependency scheduleDefinitionDependency) {
		ValidationUtils.assertNotNull(scheduleDefinitionDependency.getBillingSchedule(), "Billing Schedule is Required.");
		ValidationUtils.assertNotNull(scheduleDefinitionDependency.getBillingSchedule().getBillingDefinition(), "Billing Schedule is missing a billing definition.  Schedule dependencies cannot apply to shared schedules.");
		ValidationUtils.assertNotNull(scheduleDefinitionDependency.getBillingDefinition(), "Prerequisite Billing Definition is Required.");

		BillingDefinition dependentDefinition = scheduleDefinitionDependency.getBillingSchedule().getBillingDefinition();
		BillingDefinition prerequisiteDefinition = scheduleDefinitionDependency.getBillingDefinition();

		ValidationUtils.assertNotEquals(dependentDefinition, prerequisiteDefinition, "Prerequisite Billing Definition cannot be the same as the dependent billing definition.");
		ValidationUtils.assertEquals(dependentDefinition.getBillingCurrency(), prerequisiteDefinition.getBillingCurrency(), "Prerequisite and Dependent Billing Definitions must use the same Billing Currency. Found " + prerequisiteDefinition.getBillingCurrency().getSymbol() + " and " + dependentDefinition.getBillingCurrency().getSymbol());
		if (scheduleDefinitionDependency.getBillingDefinitionInvestmentAccount() != null) {
			ValidationUtils.assertEquals(prerequisiteDefinition, scheduleDefinitionDependency.getBillingDefinitionInvestmentAccount().getReferenceOne(), "Selected Billing Definition Account doesn't belong to the selected prerequisite billing definition.");
			ValidationUtils.assertNull(scheduleDefinitionDependency.getInvestmentAccount(), "Investment Account selection doesn't apply if a billing definition account selection is present.");
		}
		else if (scheduleDefinitionDependency.getInvestmentAccount() != null) {
			// Confirm the account is associated with the billing definition
			List<BillingDefinitionInvestmentAccount> definitionInvestmentAccountList = getBillingDefinitionInvestmentAccountListForDefinition(prerequisiteDefinition.getId());
			ValidationUtils.assertNotEmpty(BeanUtils.filter(definitionInvestmentAccountList, BillingDefinitionInvestmentAccount::getReferenceTwo, scheduleDefinitionDependency.getInvestmentAccount()), "Selected account " + scheduleDefinitionDependency.getInvestmentAccount().getLabel() + " is not available on the selected prerequisite billing definition.");
		}
		// Validate Frequencies - It's OK for a Quarterly Invoice to Depend on a Monthly Invoice, but a Monthly Invoice Can't Depend on a Quarterly Invoice
		if (prerequisiteDefinition.getBillingFrequency().getMonthsInPeriod() > dependentDefinition.getBillingFrequency().getMonthsInPeriod()) {
			throw new ValidationException("The Prerequisite Billing Definition can not be less frequent than the dependent billing definition. Prerequisite Billing Definition [" + scheduleDefinitionDependency.getBillingDefinition().getLabel() + "] is billed [" + scheduleDefinitionDependency.getBillingDefinition().getBillingFrequency().name() + "], and the dependent billing definition [" + scheduleDefinitionDependency.getBillingSchedule().getBillingDefinition().getLabel() + "] is billed [" + scheduleDefinitionDependency.getBillingSchedule().getBillingDefinition().getBillingFrequency().name() + "]");
		}

		// Get Shared Schedule Definitions for the Billing Definition to Ensure not part of a group
		List<Integer> sharedDefinitionIdList = getBillingDefinitionIdSharedList(prerequisiteDefinition.getId(), scheduleDefinitionDependency.getBillingSchedule().getStartDate(), scheduleDefinitionDependency.getBillingSchedule().getEndDate());
		if (sharedDefinitionIdList.contains(scheduleDefinitionDependency.getBillingSchedule().getBillingDefinition().getId())) {
			throw new ValidationException("Dependency [" + scheduleDefinitionDependency.getLabel() + "] is invalid because the two billing definitions share a schedule for the link date range.");
		}

		// For now, we'll only allow one level deep, that is all we need for current use cases and to determine more than one level
		// Too much complexity because of shared schedules and not needed at this time

		// See if Parent is also a child (Used by Billing Definition and Billing Schedule Billing Definition)
		for (Integer sharedDefinitionId : CollectionUtils.getIterable(sharedDefinitionIdList)) {
			List<BillingScheduleBillingDefinitionDependency> parentDependencyList = getBillingScheduleBillingDefinitionDependencyListByDefinition(sharedDefinitionId, false);
			for (BillingScheduleBillingDefinitionDependency parentDependency : CollectionUtils.getIterable(parentDependencyList)) {
				if (DateUtils.isOverlapInDates(scheduleDefinitionDependency.getBillingSchedule().getStartDate(), scheduleDefinitionDependency.getBillingSchedule().getEndDate(), parentDependency.getBillingSchedule().getStartDate(), parentDependency.getBillingSchedule().getEndDate())) {
					throw new ValidationException("Dependency [" + scheduleDefinitionDependency.getLabel() + "] is invalid because it goes more than one level deep.  Prerequisite " + (!MathUtils.isEqual(sharedDefinitionId, prerequisiteDefinition.getId()) ? "Shared " : "") + "Definition [" + prerequisiteDefinition.getLabel() + "] is also dependent on [" + parentDependency.getBillingDefinition().getLabel() + "] for overlapping date range.");
				}
			}
		}

		// See if Child is also a parent
		sharedDefinitionIdList = getBillingDefinitionIdSharedList(dependentDefinition.getId(), scheduleDefinitionDependency.getBillingSchedule().getStartDate(), scheduleDefinitionDependency.getBillingSchedule().getEndDate());
		for (Integer sharedDefinitionId : CollectionUtils.getIterable(sharedDefinitionIdList)) {
			List<BillingScheduleBillingDefinitionDependency> childDependencyList = getBillingScheduleBillingDefinitionDependencyListByDefinition(sharedDefinitionId, true);
			for (BillingScheduleBillingDefinitionDependency childDependency : CollectionUtils.getIterable(childDependencyList)) {
				if (DateUtils.isOverlapInDates(scheduleDefinitionDependency.getBillingSchedule().getStartDate(), scheduleDefinitionDependency.getBillingSchedule().getEndDate(), childDependency.getBillingSchedule().getStartDate(), childDependency.getBillingSchedule().getEndDate())) {
					throw new ValidationException("Dependency [" + scheduleDefinitionDependency.getLabel() + "] is invalid because it goes more than one level deep.  Dependent " + (!MathUtils.isEqual(sharedDefinitionId, dependentDefinition.getId()) ? "Shared " : "") + "Definition [" + dependentDefinition.getLabel() + "] is a prerequisite for [" + childDependency.getBillingSchedule().getBillingDefinition().getLabel() + "] for overlapping date range.");
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////    Billing Definition Billing Basis Post Processor Methods   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingDefinitionBillingBasisPostProcessor getBillingDefinitionBillingBasisPostProcessor(int id) {
		return getBillingDefinitionBillingBasisPostProcessorDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingDefinitionBillingBasisPostProcessor> getBillingDefinitionBillingBasisPostProcessorListByDefinition(int definitionId) {
		return getBillingDefinitionBillingBasisPostProcessorListByDefinitionCache().getBeanListForKeyValue(getBillingDefinitionBillingBasisPostProcessorDAO(), definitionId);
	}


	@Override
	public BillingDefinitionBillingBasisPostProcessor saveBillingDefinitionBillingBasisPostProcessor(BillingDefinitionBillingBasisPostProcessor bean) {
		return getBillingDefinitionBillingBasisPostProcessorDAO().save(bean);
	}


	@Override
	public void deleteBillingDefinitionBillingBasisPostProcessor(int id) {
		getBillingDefinitionBillingBasisPostProcessorDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////////             Getter & Setter Methods 		          //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BillingScheduleType, Criteria> getBillingScheduleTypeDAO() {
		return this.billingScheduleTypeDAO;
	}


	public void setBillingScheduleTypeDAO(AdvancedUpdatableDAO<BillingScheduleType, Criteria> billingScheduleTypeDAO) {
		this.billingScheduleTypeDAO = billingScheduleTypeDAO;
	}


	public AdvancedUpdatableDAO<BillingDefinition, Criteria> getBillingDefinitionDAO() {
		return this.billingDefinitionDAO;
	}


	public void setBillingDefinitionDAO(AdvancedUpdatableDAO<BillingDefinition, Criteria> billingDefinitionDAO) {
		this.billingDefinitionDAO = billingDefinitionDAO;
	}


	public AdvancedUpdatableDAO<BillingDefinitionInvestmentAccount, Criteria> getBillingDefinitionInvestmentAccountDAO() {
		return this.billingDefinitionInvestmentAccountDAO;
	}


	public void setBillingDefinitionInvestmentAccountDAO(AdvancedUpdatableDAO<BillingDefinitionInvestmentAccount, Criteria> billingDefinitionInvestmentAccountDAO) {
		this.billingDefinitionInvestmentAccountDAO = billingDefinitionInvestmentAccountDAO;
	}


	public AdvancedUpdatableDAO<BillingDefinitionInvestmentAccountSecurityExclusion, Criteria> getBillingDefinitionInvestmentAccountSecurityExclusionDAO() {
		return this.billingDefinitionInvestmentAccountSecurityExclusionDAO;
	}


	public void setBillingDefinitionInvestmentAccountSecurityExclusionDAO(AdvancedUpdatableDAO<BillingDefinitionInvestmentAccountSecurityExclusion, Criteria> billingDefinitionInvestmentAccountSecurityExclusionDAO) {
		this.billingDefinitionInvestmentAccountSecurityExclusionDAO = billingDefinitionInvestmentAccountSecurityExclusionDAO;
	}


	public AdvancedUpdatableDAO<BillingSchedule, Criteria> getBillingScheduleDAO() {
		return this.billingScheduleDAO;
	}


	public void setBillingScheduleDAO(AdvancedUpdatableDAO<BillingSchedule, Criteria> billingScheduleDAO) {
		this.billingScheduleDAO = billingScheduleDAO;
	}


	public AdvancedUpdatableDAO<BillingScheduleSharing, Criteria> getBillingScheduleSharingDAO() {
		return this.billingScheduleSharingDAO;
	}


	public void setBillingScheduleSharingDAO(AdvancedUpdatableDAO<BillingScheduleSharing, Criteria> billingScheduleSharingDAO) {
		this.billingScheduleSharingDAO = billingScheduleSharingDAO;
	}


	public AdvancedUpdatableDAO<BillingScheduleTier, Criteria> getBillingScheduleTierDAO() {
		return this.billingScheduleTierDAO;
	}


	public void setBillingScheduleTierDAO(AdvancedUpdatableDAO<BillingScheduleTier, Criteria> billingScheduleTierDAO) {
		this.billingScheduleTierDAO = billingScheduleTierDAO;
	}


	public AdvancedUpdatableDAO<BillingDefinitionBillingBasisPostProcessor, Criteria> getBillingDefinitionBillingBasisPostProcessorDAO() {
		return this.billingDefinitionBillingBasisPostProcessorDAO;
	}


	public void setBillingDefinitionBillingBasisPostProcessorDAO(AdvancedUpdatableDAO<BillingDefinitionBillingBasisPostProcessor, Criteria> billingDefinitionBillingBasisPostProcessorDAO) {
		this.billingDefinitionBillingBasisPostProcessorDAO = billingDefinitionBillingBasisPostProcessorDAO;
	}


	public AdvancedUpdatableDAO<BillingScheduleBillingDefinitionDependency, Criteria> getBillingScheduleBillingDefinitionDependencyDAO() {
		return this.billingScheduleBillingDefinitionDependencyDAO;
	}


	public void setBillingScheduleBillingDefinitionDependencyDAO(AdvancedUpdatableDAO<BillingScheduleBillingDefinitionDependency, Criteria> billingScheduleBillingDefinitionDependencyDAO) {
		this.billingScheduleBillingDefinitionDependencyDAO = billingScheduleBillingDefinitionDependencyDAO;
	}


	public DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> getBillingScheduleBillingDefinitionDependencyListByScheduleCache() {
		return this.billingScheduleBillingDefinitionDependencyListByScheduleCache;
	}


	public void setBillingScheduleBillingDefinitionDependencyListByScheduleCache(DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByScheduleCache) {
		this.billingScheduleBillingDefinitionDependencyListByScheduleCache = billingScheduleBillingDefinitionDependencyListByScheduleCache;
	}


	public DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> getBillingScheduleBillingDefinitionDependencyListByDefinitionCache() {
		return this.billingScheduleBillingDefinitionDependencyListByDefinitionCache;
	}


	public void setBillingScheduleBillingDefinitionDependencyListByDefinitionCache(DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByDefinitionCache) {
		this.billingScheduleBillingDefinitionDependencyListByDefinitionCache = billingScheduleBillingDefinitionDependencyListByDefinitionCache;
	}


	public DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> getBillingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache() {
		return this.billingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache;
	}


	public void setBillingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache(DaoSingleKeyListCache<BillingScheduleBillingDefinitionDependency, Integer> billingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache) {
		this.billingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache = billingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache;
	}


	public DaoSingleKeyListCache<BillingDefinitionBillingBasisPostProcessor, Integer> getBillingDefinitionBillingBasisPostProcessorListByDefinitionCache() {
		return this.billingDefinitionBillingBasisPostProcessorListByDefinitionCache;
	}


	public void setBillingDefinitionBillingBasisPostProcessorListByDefinitionCache(DaoSingleKeyListCache<BillingDefinitionBillingBasisPostProcessor, Integer> billingDefinitionBillingBasisPostProcessorListByDefinitionCache) {
		this.billingDefinitionBillingBasisPostProcessorListByDefinitionCache = billingDefinitionBillingBasisPostProcessorListByDefinitionCache;
	}


	public DaoSingleKeyListCache<BillingDefinitionInvestmentAccount, Integer> getBillingDefinitionInvestmentAccountListByDefinitionCache() {
		return this.billingDefinitionInvestmentAccountListByDefinitionCache;
	}


	public void setBillingDefinitionInvestmentAccountListByDefinitionCache(DaoSingleKeyListCache<BillingDefinitionInvestmentAccount, Integer> billingDefinitionInvestmentAccountListByDefinitionCache) {
		this.billingDefinitionInvestmentAccountListByDefinitionCache = billingDefinitionInvestmentAccountListByDefinitionCache;
	}


	public DaoSingleKeyListCache<BillingScheduleTier, Integer> getBillingScheduleTierListByScheduleCache() {
		return this.billingScheduleTierListByScheduleCache;
	}


	public void setBillingScheduleTierListByScheduleCache(DaoSingleKeyListCache<BillingScheduleTier, Integer> billingScheduleTierListByScheduleCache) {
		this.billingScheduleTierListByScheduleCache = billingScheduleTierListByScheduleCache;
	}


	public DaoSingleKeyListCache<BillingScheduleSharing, Integer> getBillingScheduleSharingListByDefinitionCache() {
		return this.billingScheduleSharingListByDefinitionCache;
	}


	public void setBillingScheduleSharingListByDefinitionCache(DaoSingleKeyListCache<BillingScheduleSharing, Integer> billingScheduleSharingListByDefinitionCache) {
		this.billingScheduleSharingListByDefinitionCache = billingScheduleSharingListByDefinitionCache;
	}


	public DaoSingleKeyListCache<BillingScheduleSharing, Integer> getBillingScheduleSharingListByScheduleCache() {
		return this.billingScheduleSharingListByScheduleCache;
	}


	public void setBillingScheduleSharingListByScheduleCache(DaoSingleKeyListCache<BillingScheduleSharing, Integer> billingScheduleSharingListByScheduleCache) {
		this.billingScheduleSharingListByScheduleCache = billingScheduleSharingListByScheduleCache;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
