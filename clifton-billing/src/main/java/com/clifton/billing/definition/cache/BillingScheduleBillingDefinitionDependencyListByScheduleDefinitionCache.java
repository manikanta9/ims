package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache</code> caches the list of dependencies the given definition depends on
 * i.e. the list of dependencies that referenced by the schedule definition
 *
 * @author manderson
 */
@Component
public class BillingScheduleBillingDefinitionDependencyListByScheduleDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleBillingDefinitionDependency, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "billingSchedule.billingDefinition.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleBillingDefinitionDependency bean) {
		if (bean.getBillingSchedule() != null && bean.getBillingSchedule().getBillingDefinition() != null) {
			return bean.getBillingSchedule().getBillingDefinition().getId();
		}
		return null;
	}
}
