package com.clifton.billing.definition;


import com.clifton.billing.billingbasis.BillingBasisAggregationType;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.process.schedule.waive.BillingScheduleWaiveConditionConfig;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.system.condition.SystemCondition;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingSchedule</code> class defines a billing rule applied to a billing definition.
 * Depending on BillingScheduleType, different rules will be applied to calculate the bill.
 * <p>
 * sharedSchedule = true means that the schedule can be shared across one or more BillingDefinition(s).
 *
 * @author vgomelsky
 */
public class BillingSchedule extends NamedEntity<Integer> {

	/**
	 * Templates are not real schedules, but contain basic information, like tiers that make it easier to create a new schedule
	 * The new schedule is created by copying the properties from the template to the schedule, but there is never a link back to the template is was created from
	 * For templates, The template name = name field, and the description field is used as the schedule name when creating from the template.
	 */
	private boolean template;


	/**
	 * The schedule type - defines what the fee is. i.e. Tiered Fee, Annual Minimum, Monthly Retainer, etc.
	 */
	private BillingScheduleType scheduleType;

	/**
	 * Used for revenue invoices so we know which bucket this schedule applies to
	 * We can bucket fees on a single invoice into Revenue, Revenue Share External (Broker Fee), Revenue Share Internal (sharing with another PPA or EV office)
	 * In order to use either revenue share revenue types, the billing definitions associated with the schedule must have a company populated for that bucket.
	 */
	private BillingInvoiceRevenueTypes revenueType;

	/**
	 * Specifies whether this is a shared schedule: can be shared across multiple investment
	 * accounts in order to get better tier discount, etc.  A shared schedule does not reference
	 * a BillingDefinition but is instead referenced by one or more BillingDefinition(s).
	 * <p>
	 * Non-shared schedules don't require a name and must be linked to corresponding BillingDefinition.
	 * ONLY allowed to be set if the scheduleType.sharedAllowed - true
	 */
	private boolean sharedSchedule;

	/**
	 * Must be not null for non sharedSchedule and null for sharedSchedule = true
	 */
	private BillingDefinition billingDefinition;

	/**
	 * For sharedSchedule = true schedule type, when creating a new schedule we can auto apply the definition the shared schedule was created from.
	 * This is NOT saved and used only on the initial creation of a shared schedule
	 */
	@NonPersistentField
	private BillingDefinition autoApplySharedScheduleToDefinition;

	/**
	 * For schedules that use billing basis how the billing basis is aggregated (all, client relationship, client, account, etc)
	 * This is rare for Institutional clients as all accounts included on the billing definition(s) should be aggregated, however for Retail clients
	 * where the billing definition is associated with the Broker, we need to additionally break out fees for aggregation based on the client relations for each account.
	 * This really only has an effect when Tiered fees are being used as the higher the amounts to apply to the threshold, the smaller the calculated blended fee
	 */
	private BillingBasisAggregationType billingBasisAggregationType;

	/**
	 * Used for Shared schedules ONLY - in additional to billing basis aggregation type
	 * if "do not group invoice" is selected the schedule will be evaluated individually for each billing definition
	 * This allows using a shared schedule for the purposes of the same fee schedule being applied, but not sharing the assets for the fee.
	 * Using the schedule with do not group invoice would not put the invoices into a group - however if there are other schedules that would group them they are still grouped.
	 * Example: We have a group of clients whose tiered fee is recalculated each quarter - we want to enter the schedule once, and associate it with all of the client's billing definitions, but they do not actually share or get invoiced together.
	 */
	private boolean doNotGroupInvoice;

	/**
	 * Fixed fee or percent (bps) applied to this schedule.
	 * Used for Non-Tier Schedule Types
	 */
	private BigDecimal scheduleAmount;

	/**
	 * Optional secondary schedule amount that may apply to a schedule. i.e. Minimum Fee with Credit, the secondary amount is the max credit that can be applied (optional)
	 */
	private BigDecimal secondaryScheduleAmount;

	/**
	 * This can be different than billing frequency: bill quarterly but use $2000 monthly retainer.
	 * Or, 5% Annual Fee - and we bill quarterly, then the Fee is actually 5% / 4 = 1.25% on each quarterly invoice
	 */
	private BillingScheduleFrequencies scheduleFrequency;

	/**
	 * If not populated - uses the billing definition frequency
	 * This can be used to have the fees accrue on the invoice for smaller periods than the billing frequency
	 * i.e. Quarterly invoice, but separate detail lines that apply to each month
	 * In that case - Billing Definition Frequency = Quarterly, Schedule Frequency = Annually (i.e. 5% per year), Accrual Frequency = Monthly
	 */
	private BillingFrequencies accrualFrequency;

	/**
	 * Optional investment account that the schedule applies to.  Usually it's account(s) linked to the corresponding
	 * BillingDefinition. However, one can use a totally different account.
	 */
	private InvestmentAccount investmentAccount;

	/**
	 * Used where the schedule should be duplicated for each account in the group
	 * Waive conditions are applied for each account
	 * <p>
	 * Ex. Additional Account Fee - applies to each client account in the group
	 * So we don't have to create a schedule for each additional account
	 * Can Use Waive Conditions to filter out accounts in the group that shouldn't get the fee
	 */
	private InvestmentAccountGroup investmentAccountGroup;

	/**
	 * Instead of selecting a specific account or account group,
	 * can optionally apply it to all client accounts tied to the billing definition
	 */
	private boolean applyToEachClientAccount;

	/**
	 * If defined, and the condition returns true, the schedule will be waived.
	 * When schedules are waived, a violation is added to the invoice so users are aware
	 */
	private SystemCondition waiveSystemCondition;

	/**
	 * Not stored in the db, but set on the schedule for processing waive condition
	 * when information during processing is needed in order to properly evaluate the
	 * condition
	 */
	@NonPersistentField
	private BillingScheduleWaiveConditionConfig waiveSystemConditionConfig;

	/**
	 * If this schedule should only apply during a specified time period
	 * i.e. a One Time Fee
	 * NOTE: THIS IS NOT USED TO PRORATE THE SCHEDULE AMOUNT, JUST TO FIND IF IT'S ACTIVE DURING THE INVOICE PERIOD
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * A list of schedule tiers if the type is "Tiered"
	 */
	private List<BillingScheduleTier> scheduleTierList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isTiersExist() {
		return !CollectionUtils.isEmpty(getScheduleTierList());
	}


	public String getLabelWithDates() {
		return getLabel() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
	}


	public String getAccountLabel() {
		if (isApplyToEachClientAccount()) {
			return "All Client Accounts";
		}
		if (getInvestmentAccount() != null) {
			return "Account #: " + getInvestmentAccount().getNumber();
		}
		if (getInvestmentAccountGroup() != null) {
			return "Account Group: " + getInvestmentAccountGroup().getName();
		}
		return "";
	}


	public String getRevenueTypeLabel() {
		if (getRevenueType() == null) {
			return null;
		}
		return getRevenueType().getLabel();
	}


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getScheduleAmount() {
		return this.scheduleAmount;
	}


	public void setScheduleAmount(BigDecimal scheduleAmount) {
		this.scheduleAmount = scheduleAmount;
	}


	public BigDecimal getSecondaryScheduleAmount() {
		return this.secondaryScheduleAmount;
	}


	public void setSecondaryScheduleAmount(BigDecimal secondaryScheduleAmount) {
		this.secondaryScheduleAmount = secondaryScheduleAmount;
	}


	public BillingScheduleFrequencies getScheduleFrequency() {
		return this.scheduleFrequency;
	}


	public void setScheduleFrequency(BillingScheduleFrequencies scheduleFrequency) {
		this.scheduleFrequency = scheduleFrequency;
	}


	public BillingFrequencies getAccrualFrequency() {
		return this.accrualFrequency;
	}


	public void setAccrualFrequency(BillingFrequencies accrualFrequency) {
		this.accrualFrequency = accrualFrequency;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public BillingScheduleType getScheduleType() {
		return this.scheduleType;
	}


	public void setScheduleType(BillingScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}


	public BillingInvoiceRevenueTypes getRevenueType() {
		return this.revenueType;
	}


	public void setRevenueType(BillingInvoiceRevenueTypes revenueType) {
		this.revenueType = revenueType;
	}


	public BillingDefinition getBillingDefinition() {
		return this.billingDefinition;
	}


	public void setBillingDefinition(BillingDefinition billingDefinition) {
		this.billingDefinition = billingDefinition;
	}


	public List<BillingScheduleTier> getScheduleTierList() {
		return this.scheduleTierList;
	}


	public void setScheduleTierList(List<BillingScheduleTier> scheduleTierList) {
		this.scheduleTierList = scheduleTierList;
	}


	public boolean isSharedSchedule() {
		return this.sharedSchedule;
	}


	public void setSharedSchedule(boolean sharedSchedule) {
		this.sharedSchedule = sharedSchedule;
	}


	public SystemCondition getWaiveSystemCondition() {
		return this.waiveSystemCondition;
	}


	public void setWaiveSystemCondition(SystemCondition waiveSystemCondition) {
		this.waiveSystemCondition = waiveSystemCondition;
	}


	public InvestmentAccountGroup getInvestmentAccountGroup() {
		return this.investmentAccountGroup;
	}


	public void setInvestmentAccountGroup(InvestmentAccountGroup investmentAccountGroup) {
		this.investmentAccountGroup = investmentAccountGroup;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isApplyToEachClientAccount() {
		return this.applyToEachClientAccount;
	}


	public void setApplyToEachClientAccount(boolean applyToEachClientAccount) {
		this.applyToEachClientAccount = applyToEachClientAccount;
	}


	public BillingScheduleWaiveConditionConfig getWaiveSystemConditionConfig() {
		return this.waiveSystemConditionConfig;
	}


	public void setWaiveSystemConditionConfig(BillingScheduleWaiveConditionConfig waiveSystemConditionConfig) {
		this.waiveSystemConditionConfig = waiveSystemConditionConfig;
	}


	public boolean isTemplate() {
		return this.template;
	}


	public void setTemplate(boolean template) {
		this.template = template;
	}


	public BillingBasisAggregationType getBillingBasisAggregationType() {
		return this.billingBasisAggregationType;
	}


	public void setBillingBasisAggregationType(BillingBasisAggregationType billingBasisAggregationType) {
		this.billingBasisAggregationType = billingBasisAggregationType;
	}


	public BillingDefinition getAutoApplySharedScheduleToDefinition() {
		return this.autoApplySharedScheduleToDefinition;
	}


	public void setAutoApplySharedScheduleToDefinition(BillingDefinition autoApplySharedScheduleToDefinition) {
		this.autoApplySharedScheduleToDefinition = autoApplySharedScheduleToDefinition;
	}


	public boolean isDoNotGroupInvoice() {
		return this.doNotGroupInvoice;
	}


	public void setDoNotGroupInvoice(boolean doNotGroupInvoice) {
		this.doNotGroupInvoice = doNotGroupInvoice;
	}
}
