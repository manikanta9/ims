package com.clifton.billing.definition.search;


import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingScheduleFrequencies;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public class BillingScheduleSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "billingDefinition.id")
	private Integer billingDefinitionId;

	// Custom Search that returns any schedule associated with any of the list billing definitions - shared or not
	private Integer[] billingDefinitionIds;

	@SearchField(searchFieldPath = "billingDefinition.invoiceType", searchField = "name")
	private String invoiceTypeName;

	@SearchField(searchFieldPath = "billingDefinition", searchField = "invoiceType.id", sortField = "invoiceType.name")
	private Short invoiceTypeId;

	@SearchField(searchField = "billingBasisAggregationType.id", sortField = "billingBasisAggregationType.name")
	private Short billingBasisAggregationTypeId;

	@SearchField
	private Boolean doNotGroupInvoice;

	@SearchField(searchFieldPath = "billingDefinition.businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField
	private String name;

	@SearchField(searchField = "sharedDefinitionList.referenceTwo.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer sharedBillingDefinitionId;

	@SearchField(searchField = "scheduleType.id", sortField = "scheduleType.name")
	private Short scheduleTypeId;

	@SearchField
	private BillingInvoiceRevenueTypes revenueType;

	@SearchField(searchField = "waiveSystemCondition.id", sortField = "waiveSystemCondition.name")
	private Integer waiveSystemConditionId;

	@SearchField
	private Boolean sharedSchedule;

	@SearchField
	private BillingScheduleFrequencies scheduleFrequency;

	@SearchField
	private BillingFrequencies accrualFrequency;

	@SearchField(searchFieldPath = "scheduleType", searchField = "scheduleOrder")
	private Integer scheduleTypeOrder;

	@SearchField(searchFieldPath = "scheduleType", searchField = "sharedScheduleAllowed")
	private Boolean scheduleTypeSharedScheduleAllowed;

	@SearchField
	private BigDecimal scheduleAmount;

	@SearchField
	private BigDecimal secondaryScheduleAmount;

	// Custom SearchField - Filters on active for the definition
	private Boolean activeDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getBillingDefinitionId() {
		return this.billingDefinitionId;
	}


	public void setBillingDefinitionId(Integer billingDefinitionId) {
		this.billingDefinitionId = billingDefinitionId;
	}


	public Integer getSharedBillingDefinitionId() {
		return this.sharedBillingDefinitionId;
	}


	public void setSharedBillingDefinitionId(Integer sharedBillingDefinitionId) {
		this.sharedBillingDefinitionId = sharedBillingDefinitionId;
	}


	public Boolean getSharedSchedule() {
		return this.sharedSchedule;
	}


	public void setSharedSchedule(Boolean sharedSchedule) {
		this.sharedSchedule = sharedSchedule;
	}


	public Integer getScheduleTypeOrder() {
		return this.scheduleTypeOrder;
	}


	public void setScheduleTypeOrder(Integer scheduleTypeOrder) {
		this.scheduleTypeOrder = scheduleTypeOrder;
	}


	public Short getScheduleTypeId() {
		return this.scheduleTypeId;
	}


	public void setScheduleTypeId(Short scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}


	public BillingScheduleFrequencies getScheduleFrequency() {
		return this.scheduleFrequency;
	}


	public void setScheduleFrequency(BillingScheduleFrequencies scheduleFrequency) {
		this.scheduleFrequency = scheduleFrequency;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getWaiveSystemConditionId() {
		return this.waiveSystemConditionId;
	}


	public void setWaiveSystemConditionId(Integer waiveSystemConditionId) {
		this.waiveSystemConditionId = waiveSystemConditionId;
	}


	public BigDecimal getScheduleAmount() {
		return this.scheduleAmount;
	}


	public void setScheduleAmount(BigDecimal scheduleAmount) {
		this.scheduleAmount = scheduleAmount;
	}


	public BigDecimal getSecondaryScheduleAmount() {
		return this.secondaryScheduleAmount;
	}


	public void setSecondaryScheduleAmount(BigDecimal secondaryScheduleAmount) {
		this.secondaryScheduleAmount = secondaryScheduleAmount;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer[] getBillingDefinitionIds() {
		return this.billingDefinitionIds;
	}


	public void setBillingDefinitionIds(Integer[] billingDefinitionIds) {
		this.billingDefinitionIds = billingDefinitionIds;
	}


	public Boolean getActiveDefinition() {
		return this.activeDefinition;
	}


	public void setActiveDefinition(Boolean activeDefinition) {
		this.activeDefinition = activeDefinition;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Boolean getScheduleTypeSharedScheduleAllowed() {
		return this.scheduleTypeSharedScheduleAllowed;
	}


	public void setScheduleTypeSharedScheduleAllowed(Boolean scheduleTypeSharedScheduleAllowed) {
		this.scheduleTypeSharedScheduleAllowed = scheduleTypeSharedScheduleAllowed;
	}


	public String getInvoiceTypeName() {
		return this.invoiceTypeName;
	}


	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public Short getBillingBasisAggregationTypeId() {
		return this.billingBasisAggregationTypeId;
	}


	public void setBillingBasisAggregationTypeId(Short billingBasisAggregationTypeId) {
		this.billingBasisAggregationTypeId = billingBasisAggregationTypeId;
	}


	public Boolean getDoNotGroupInvoice() {
		return this.doNotGroupInvoice;
	}


	public void setDoNotGroupInvoice(Boolean doNotGroupInvoice) {
		this.doNotGroupInvoice = doNotGroupInvoice;
	}


	public BillingFrequencies getAccrualFrequency() {
		return this.accrualFrequency;
	}


	public void setAccrualFrequency(BillingFrequencies accrualFrequency) {
		this.accrualFrequency = accrualFrequency;
	}


	public BillingInvoiceRevenueTypes getRevenueType() {
		return this.revenueType;
	}


	public void setRevenueType(BillingInvoiceRevenueTypes revenueType) {
		this.revenueType = revenueType;
	}
}
