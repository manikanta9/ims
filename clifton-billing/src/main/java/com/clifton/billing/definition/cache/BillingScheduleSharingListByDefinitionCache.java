package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleSharingListByDefinitionCache</code> caches the list of BillingScheduleSharing for the given definition
 *
 * @author manderson
 */
@Component
public class BillingScheduleSharingListByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleSharing, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceTwo.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleSharing bean) {
		if (bean.getReferenceTwo() != null) {
			return bean.getReferenceTwo().getId();
		}
		return null;
	}
}
