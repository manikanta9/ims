package com.clifton.billing.definition.comparisons;

/**
 * For given Billing Definition - Returns true if there are no invoices for that billing definition found with the given filters
 *
 * @author manderson
 */
public class BillingDefinitionInvoiceNotExistsComparison extends BillingDefinitionInvoiceExistsComparison {


	@Override
	protected boolean isReverse() {
		return true;
	}
}
