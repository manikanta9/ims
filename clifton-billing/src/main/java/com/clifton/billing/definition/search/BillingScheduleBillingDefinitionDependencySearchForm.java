package com.clifton.billing.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class BillingScheduleBillingDefinitionDependencySearchForm extends BaseAuditableEntitySearchForm {


	/**
	 * Pre-Req Billing Definition/Accounts
	 */

	@SearchField(searchFieldPath = "billingDefinition", searchField = "invoiceType.id")
	private Short prerequisiteInvoiceTypeId;

	@SearchField(searchField = "billingDefinition.id")
	private Integer prerequisiteBillingDefinitionId;

	@SearchField(searchFieldPath = "billingDefinition", searchField = "id,billingContract.name", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER)
	private String prerequisiteBillingDefinitionLabel;

	@SearchField(searchField = "billingDefinitionInvestmentAccount.referenceTwo.name,billingDefinitionInvestmentAccount.referenceTwo.number,investmentAccount.name,investmentAccount.number", leftJoin = true)
	private String prerequisiteAccountLabel;

	@SearchField(searchField = "billingDefinitionInvestmentAccount.id")
	private Integer prerequisiteBillingDefinitionInvestmentAccountId;

	@SearchField(searchField = "investmentAccount.id")
	private Integer prerequisiteInvestmentAccountId;


	/**
	 * Dependent Billing Schedule and Definition
	 */

	@SearchField(searchFieldPath = "billingSchedule.billingDefinition", searchField = "invoiceType.id")
	private Short dependentInvoiceTypeId;

	@SearchField(searchFieldPath = "billingSchedule.billingDefinition", searchField = "id,billingContract.name", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER)
	private String dependentBillingDefinitionLabel;

	@SearchField(searchFieldPath = "billingSchedule", searchField = "scheduleType.id")
	private Short billingScheduleTypeId;

	@SearchField(searchFieldPath = "billingSchedule", searchField = "name,description")
	private String billingScheduleLabel;

	// Custom Search Field
	private Boolean active;

	@SearchField(searchField = "billingSchedule.startDate")
	private Date startDate;

	@SearchField(searchField = "billingSchedule.endDate")
	private Date endDate;

	@SearchField
	private String note;


	@SearchField(searchField = "billingSchedule.billingDefinition.workflowState.name,billingDefinition.workflowState.name", searchFieldCustomType = SearchFieldCustomTypes.OR, comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeBillingDefinitionWorkflowStateName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPrerequisiteInvoiceTypeId() {
		return this.prerequisiteInvoiceTypeId;
	}


	public void setPrerequisiteInvoiceTypeId(Short prerequisiteInvoiceTypeId) {
		this.prerequisiteInvoiceTypeId = prerequisiteInvoiceTypeId;
	}


	public Integer getPrerequisiteBillingDefinitionId() {
		return this.prerequisiteBillingDefinitionId;
	}


	public void setPrerequisiteBillingDefinitionId(Integer prerequisiteBillingDefinitionId) {
		this.prerequisiteBillingDefinitionId = prerequisiteBillingDefinitionId;
	}


	public String getPrerequisiteBillingDefinitionLabel() {
		return this.prerequisiteBillingDefinitionLabel;
	}


	public void setPrerequisiteBillingDefinitionLabel(String prerequisiteBillingDefinitionLabel) {
		this.prerequisiteBillingDefinitionLabel = prerequisiteBillingDefinitionLabel;
	}


	public String getPrerequisiteAccountLabel() {
		return this.prerequisiteAccountLabel;
	}


	public void setPrerequisiteAccountLabel(String prerequisiteAccountLabel) {
		this.prerequisiteAccountLabel = prerequisiteAccountLabel;
	}


	public Integer getPrerequisiteBillingDefinitionInvestmentAccountId() {
		return this.prerequisiteBillingDefinitionInvestmentAccountId;
	}


	public void setPrerequisiteBillingDefinitionInvestmentAccountId(Integer prerequisiteBillingDefinitionInvestmentAccountId) {
		this.prerequisiteBillingDefinitionInvestmentAccountId = prerequisiteBillingDefinitionInvestmentAccountId;
	}


	public Integer getPrerequisiteInvestmentAccountId() {
		return this.prerequisiteInvestmentAccountId;
	}


	public void setPrerequisiteInvestmentAccountId(Integer prerequisiteInvestmentAccountId) {
		this.prerequisiteInvestmentAccountId = prerequisiteInvestmentAccountId;
	}


	public Short getDependentInvoiceTypeId() {
		return this.dependentInvoiceTypeId;
	}


	public void setDependentInvoiceTypeId(Short dependentInvoiceTypeId) {
		this.dependentInvoiceTypeId = dependentInvoiceTypeId;
	}


	public String getDependentBillingDefinitionLabel() {
		return this.dependentBillingDefinitionLabel;
	}


	public void setDependentBillingDefinitionLabel(String dependentBillingDefinitionLabel) {
		this.dependentBillingDefinitionLabel = dependentBillingDefinitionLabel;
	}


	public Short getBillingScheduleTypeId() {
		return this.billingScheduleTypeId;
	}


	public void setBillingScheduleTypeId(Short billingScheduleTypeId) {
		this.billingScheduleTypeId = billingScheduleTypeId;
	}


	public String getBillingScheduleLabel() {
		return this.billingScheduleLabel;
	}


	public void setBillingScheduleLabel(String billingScheduleLabel) {
		this.billingScheduleLabel = billingScheduleLabel;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getExcludeBillingDefinitionWorkflowStateName() {
		return this.excludeBillingDefinitionWorkflowStateName;
	}


	public void setExcludeBillingDefinitionWorkflowStateName(String excludeBillingDefinitionWorkflowStateName) {
		this.excludeBillingDefinitionWorkflowStateName = excludeBillingDefinitionWorkflowStateName;
	}
}
