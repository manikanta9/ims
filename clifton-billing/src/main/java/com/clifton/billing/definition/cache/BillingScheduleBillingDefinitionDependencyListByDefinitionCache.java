package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingScheduleBillingDefinitionDependencyListByDefinitionCache</code> caches the list of dependencies for the given definition
 * i.e. the list of dependencies that depend on the given definition
 *
 * @author manderson
 */
@Component
public class BillingScheduleBillingDefinitionDependencyListByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<BillingScheduleBillingDefinitionDependency, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "billingDefinition.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingScheduleBillingDefinitionDependency bean) {
		if (bean.getBillingDefinition() != null) {
			return bean.getBillingDefinition().getId();
		}
		return null;
	}
}
