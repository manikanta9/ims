package com.clifton.billing.definition;


import com.clifton.billing.billingbasis.BillingBasisCalculationType;
import com.clifton.billing.billingbasis.BillingBasisValuationType;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.setup.group.InvestmentGroup;

import java.util.Date;


/**
 * The <code>BillingDefinitionInvestmentAccount</code> class links client InvestmentAccount
 * to corresponding BillingDefinition.  One BillingDefinition maybe used for multiple
 * investment accounts.
 *
 * @author vgomelsky
 */
public class BillingDefinitionInvestmentAccount extends ManyToManyEntity<BillingDefinition, InvestmentAccount, Integer> {

	/**
	 * Where BillingBasis Values are calculated from, i.e. Synthetic Exposure, Market Value, Cash, etc.
	 */
	private BillingBasisValuationType billingBasisValuationType;

	/**
	 * How billing basis values are calculated, i.e. Period Average, Period End
	 * Required for all except when BillingBasisValuationType = NONE;
	 */
	private BillingBasisCalculationType billingBasisCalculationType;

	/**
	 * Can be set for anything, but especially useful for "External" valuation types so
	 * it's known where that "External" value is actually coming from
	 */
	private String billingBasisValuationTypeDescription;

	/**
	 * Used for valuation types that use managers (i.e. Manager Balances)
	 */
	private InvestmentManagerAccount investmentManagerAccount;

	/**
	 * Optionally apply the rule to a sub-set of positions.
	 * Only allowed if the valuation type investmentGroupAllowed = true
	 */
	private InvestmentGroup investmentGroup;

	/**
	 * Applies to a specific schedule
	 * If Null applies to all
	 */
	private BillingSchedule billingSchedule;

	/**
	 * When null, uses the Billing Definition Start/End Dates
	 */
	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public String getLabelShort() {
		String lbl = "";
		if (getReferenceTwo() != null) {
			lbl += getReferenceTwo().getNumber() + ": ";
		}
		if (getReferenceOne() != null) {
			lbl += getReferenceOne().getLabel();
		}
		if (!StringUtils.isEmpty(getBillingBasisValuationTypeDescription())) {
			lbl += " [" + getBillingBasisValuationTypeDescription() + "]";
		}
		if (getInvestmentGroup() != null) {
			lbl += " [" + getInvestmentGroup().getName() + "]";
		}
		if (getInvestmentManagerAccount() != null) {
			lbl += " [" + getInvestmentManagerAccount().getLabelShort() + "]";
		}
		return lbl;
	}


	@Override
	public String getLabel() {
		String lbl = "";
		if (getReferenceOne() != null) {
			lbl += getReferenceOne().getLabel();
		}
		if (getReferenceTwo() != null) {
			lbl += " - " + getReferenceTwo().getLabel();
		}
		if (getInvestmentGroup() != null) {
			lbl += " [" + getInvestmentGroup().getName() + "]";
		}
		if (getInvestmentManagerAccount() != null) {
			lbl += " [" + getInvestmentManagerAccount().getLabelShort() + "]";
		}
		return lbl;
	}


	public String getAccountLabel() {
		String lbl = "";
		if (getReferenceTwo() != null) {
			lbl += getReferenceTwo().getNumber() + ": ";
			lbl += getReferenceTwo().getName() + " ";
		}
		lbl += getValuationLabel();
		return lbl;
	}


	public String getAccountLabelLong() {
		String lbl = getAccountLabel();
		lbl += " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
		return lbl;
	}


	public String getValuationLabel() {
		String lbl = "";
		if (getBillingBasisValuationType() != null) {
			lbl += StringUtils.coalesce(true, getBillingBasisValuationTypeDescription(), getBillingBasisValuationType().getName());

			if (getBillingBasisCalculationType() != null) {
				lbl += " - " + getBillingBasisCalculationType().getName();
			}
		}
		if (getInvestmentGroup() != null) {
			lbl += " [" + getInvestmentGroup().getName() + "]";
		}
		if (getInvestmentManagerAccount() != null) {
			lbl += " [" + getInvestmentManagerAccount().getLabelShort() + "]";
		}
		if (getBillingSchedule() != null) {
			lbl += " (" + getBillingSchedule().getName() + ")";
		}
		return lbl;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public BillingSchedule getBillingSchedule() {
		return this.billingSchedule;
	}


	public void setBillingSchedule(BillingSchedule billingSchedule) {
		this.billingSchedule = billingSchedule;
	}


	public String getBillingBasisValuationTypeDescription() {
		return this.billingBasisValuationTypeDescription;
	}


	public void setBillingBasisValuationTypeDescription(String billingBasisValuationTypeDescription) {
		this.billingBasisValuationTypeDescription = billingBasisValuationTypeDescription;
	}


	public void setBillingBasisValuationType(BillingBasisValuationType billingBasisValuationType) {
		this.billingBasisValuationType = billingBasisValuationType;
	}


	public BillingBasisValuationType getBillingBasisValuationType() {
		return this.billingBasisValuationType;
	}


	public BillingBasisCalculationType getBillingBasisCalculationType() {
		return this.billingBasisCalculationType;
	}


	public void setBillingBasisCalculationType(BillingBasisCalculationType billingBasisCalculationType) {
		this.billingBasisCalculationType = billingBasisCalculationType;
	}


	public InvestmentManagerAccount getInvestmentManagerAccount() {
		return this.investmentManagerAccount;
	}


	public void setInvestmentManagerAccount(InvestmentManagerAccount investmentManagerAccount) {
		this.investmentManagerAccount = investmentManagerAccount;
	}
}
