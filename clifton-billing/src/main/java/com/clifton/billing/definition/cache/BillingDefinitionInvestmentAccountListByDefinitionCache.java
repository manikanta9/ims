package com.clifton.billing.definition.cache;

import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingDefinitionInvestmentAccountListByDefinitionCache</code> caches the list of BillingDefinitionInvestmentAccounts for the given definition
 *
 * @author manderson
 */
@Component
public class BillingDefinitionInvestmentAccountListByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<BillingDefinitionInvestmentAccount, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(BillingDefinitionInvestmentAccount bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
