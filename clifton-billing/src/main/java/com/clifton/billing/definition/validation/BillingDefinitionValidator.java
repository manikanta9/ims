package com.clifton.billing.definition.validation;


import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BillingDefinitionValidator</code> validates various facets of the billing definitions
 * <p>
 * Examples:
 * Validates on Updates if an "annual" billing schedule is defined, Second Year Start Date is required
 * Also validates if the annual schedule is shared, that all billing definitions follow the same annual schedule.
 * Validates if posting is turned on, the company the billing definition is associated with would be accepting of posts (i.e. a Client, but not a Broker company)
 *
 * @author Mary Anderson
 */
@Component
public class BillingDefinitionValidator extends SelfRegisteringDaoValidator<BillingDefinition> {

	private BillingDefinitionService billingDefinitionService;

	private BusinessClientService businessClientService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BillingDefinition bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertNotNull(bean.getBillingFrequency(), "A Billing Frequency must be specified.");
		ValidationUtils.assertFalse(bean.getBillingFrequency().isAccrualFrequencyOnly(), "Frequency " + bean.getBillingFrequency().name() + " is invalid selection.  That frequency can be used for schedule accruals only.");
		ValidationUtils.assertNotNull(bean.getInvoiceType(), "An Invoice Type must be specified.");

		validateBillingDefinitionInvoicePostOption(bean);

		if (config.isUpdate()) {
			getOriginalBean(bean);

			// OK if it's set and not used, but if it's not set - check the billing definition billing frequency (required if annual billing and check schedules if any that require a second year start date)
			if (BillingFrequencies.ANNUALLY == bean.getBillingFrequency() || BillingFrequencies.SEMI_ANNUALLY == bean.getBillingFrequency()) {
				ValidationUtils.assertNotNull(bean.getSecondYearStartDate(),
						"Second Year Start Date is required because this definition is billed annually or semi-annually which is dependant on the definition's annual start date.",
						"secondYearStartDate");
			}

			// Find all annual fee schedules - shared or not
			List<BillingSchedule> scheduleList = getBillingDefinitionService().getBillingScheduleListForDefinition(bean.getId(), null);
			for (BillingSchedule schedule : CollectionUtils.getIterable(scheduleList)) {
				// If it is a shared schedule - ensure all billing definitions that share use the same billing frequency
				List<BillingScheduleSharing> scheduleSharingList = (schedule.isSharedSchedule() && !schedule.isDoNotGroupInvoice() ? getBillingDefinitionService().getBillingScheduleSharingListForSchedule(schedule.getId()) : null);
				BillingDefinitionValidationUtils.validateBillingScheduleForDefinitions(schedule, bean, scheduleSharingList);

				// If annual fee schedule type is used:
				// Second year start is required
				// and if it is shared - month/day needs to match across all
				if (schedule.getScheduleType().isAnnualFee()) {
					ValidationUtils.assertNotNull(bean.getSecondYearStartDate(),
							"Second Year Start Date is required because this definition has the following annual fee schedule associated with it: " + schedule.getName(), "secondYearStartDate");

					if (!CollectionUtils.isEmpty(scheduleSharingList)) {
						// Pull Schedule with shared list populated
						String secondYear = DateUtils.fromDate(bean.getSecondYearStartDate(), "MM/dd");
						for (BillingScheduleSharing sharedDef : scheduleSharingList) {
							if (!bean.equals(sharedDef.getReferenceTwo())) {
								String sharedSecondYear = DateUtils.fromDate(sharedDef.getReferenceTwo().getSecondYearStartDate(), "MM/dd");
								if (!secondYear.equals(sharedSecondYear)) {
									throw new ValidationException("Billing Definitions that share an annual fee schedule must use the same month/day for the second year start date.  Schedule ["
											+ schedule.getName() + "] is shared with definition [" + sharedDef.getReferenceTwo().getLabel() + "] with a second year start of [" + sharedSecondYear
											+ "] and this billing definition has a second year start of [" + secondYear + "].");
								}
							}
						}
					}
				}
			}
		}
	}


	private void validateBillingDefinitionInvoicePostOption(BillingDefinition billingDefinition) {
		if (billingDefinition.isInvoicePostedToPortal()) {
			boolean validPostEntity = false;
			// Client Relationship
			if (StringUtils.isEqual(BusinessClientRelationship.COMPANY_TYPE_NAME, billingDefinition.getBusinessCompany().getType().getName())) {
				validPostEntity = true;
			}
			// Otherwise we expect it to be a client (which can have multiple types)
			else {
				try {
					BusinessClient client = getBusinessClientService().getBusinessClientByCompany(billingDefinition.getBusinessCompany().getId());
					validPostEntity = (client != null);
				}
				catch (ValidationException e) {
					// Throws validation exception if it can't find the client for the company id - ignore this as we just leave valid post entity as false and return correct error message
				}
			}
			ValidationUtils.assertTrue(validPostEntity, "Cannot use the invoice posted to portal option for billing company [" + billingDefinition.getBusinessCompany().getNameWithType() + "] because it is not a client relationship or a client and therefore unknown where to  post it to.");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}
}
