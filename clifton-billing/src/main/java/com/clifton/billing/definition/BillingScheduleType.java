package com.clifton.billing.definition;


import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>BillingScheduleType</code> class defines the type of a billing schedule:
 * Tiered, Fixed Fee, Retainer, Annual Minimum, etc.
 *
 * @author vgomelsky
 */
public class BillingScheduleType extends NamedEntity<Short> {

	/**
	 * Specifies if billing schedules of this type can be shared.
	 */
	private boolean sharedScheduleAllowed;

	/**
	 * If a billing definition has more than one billing schedule, they must be applied in the following order.  This allows discounts to be applied
	 * after the regular fees, but before any min/max fees.
	 */
	private int scheduleOrder;

	private boolean minimumFee;

	private boolean maximumFee;

	/**
	 * Specifies that this is a simple flat fee schedule: can create multiple with different names via UI: Retainer, Fixed Fee, etc.
	 */
	private boolean flatFee;

	/**
	 * Specifies whether scheduleAmount is absolute value: 10 bps.
	 * Flat Percentages are NOT converted based on billing vs. schedule frequency.  i.e. 10% Discount would never be converted based on frequency
	 */
	private boolean flatFeePercent;

	/**
	 * BillingSchedule.scheduleAmount field label: "Fee Amount", "Percentage", "Minimum Amount" etc.
	 */
	private String scheduleAmountLabel;

	/**
	 * BillingSchedule.secondaryScheduleAmount field label
	 * Optional if the schedule supports it. i.e. for Minimum Fee with Credit - the Minimum Fee is the schedule amount, and the Max Credit is the optional secondary amount
	 */
	private String secondaryScheduleAmountLabel;

	/**
	 * If secondaryScheduleAmountLabel is populated, then the secondary amount is supported.
	 * This flag determines if the secondary amount on the schedule is required or not
	 */
	private boolean secondaryScheduleAmountRequired;

	/**
	 * Uses tiers instead of a schedule amount
	 */
	private boolean tiered;

	/**
	 * Used for Annual Min/Max where an annual billing period must be known, so we need the second year
	 * start date set on the billing definition where these schedule types apply.
	 */
	private boolean annualFee;

	/**
	 * Determines if accrual frequency can be selected on the schedule.
	 * i.e. Does not apply to annual minimum / maximum fees
	 */
	private boolean accrualFrequencyAllowed;

	/**
	 * Default revenue type - so when adding schedules, users will not likely need to change this
	 * only set when an obvious default - some revenue share type schedules could be used for internal or external
	 */
	private BillingInvoiceRevenueTypes defaultRevenueType;

	/**
	 * System Bean that performs specific calculation
	 */
	private SystemBean billingAmountCalculatorBean;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isFlatFee() {
		return this.flatFee;
	}


	public void setFlatFee(boolean flatFee) {
		this.flatFee = flatFee;
	}


	public String getScheduleAmountLabel() {
		return this.scheduleAmountLabel;
	}


	public void setScheduleAmountLabel(String scheduleAmountLabel) {
		this.scheduleAmountLabel = scheduleAmountLabel;
	}


	public int getScheduleOrder() {
		return this.scheduleOrder;
	}


	public void setScheduleOrder(int scheduleOrder) {
		this.scheduleOrder = scheduleOrder;
	}


	public boolean isSharedScheduleAllowed() {
		return this.sharedScheduleAllowed;
	}


	public void setSharedScheduleAllowed(boolean sharedScheduleAllowed) {
		this.sharedScheduleAllowed = sharedScheduleAllowed;
	}


	public boolean isFlatFeePercent() {
		return this.flatFeePercent;
	}


	public void setFlatFeePercent(boolean flatFeePercent) {
		this.flatFeePercent = flatFeePercent;
	}


	public boolean isTiered() {
		return this.tiered;
	}


	public void setTiered(boolean tiered) {
		this.tiered = tiered;
	}


	public SystemBean getBillingAmountCalculatorBean() {
		return this.billingAmountCalculatorBean;
	}


	public void setBillingAmountCalculatorBean(SystemBean billingAmountCalculatorBean) {
		this.billingAmountCalculatorBean = billingAmountCalculatorBean;
	}


	public boolean isMinimumFee() {
		return this.minimumFee;
	}


	public void setMinimumFee(boolean minimumFee) {
		this.minimumFee = minimumFee;
	}


	public boolean isMaximumFee() {
		return this.maximumFee;
	}


	public void setMaximumFee(boolean maximumFee) {
		this.maximumFee = maximumFee;
	}


	public boolean isAnnualFee() {
		return this.annualFee;
	}


	public void setAnnualFee(boolean annualFee) {
		this.annualFee = annualFee;
	}


	public String getSecondaryScheduleAmountLabel() {
		return this.secondaryScheduleAmountLabel;
	}


	public void setSecondaryScheduleAmountLabel(String secondaryScheduleAmountLabel) {
		this.secondaryScheduleAmountLabel = secondaryScheduleAmountLabel;
	}


	public boolean isSecondaryScheduleAmountRequired() {
		return this.secondaryScheduleAmountRequired;
	}


	public void setSecondaryScheduleAmountRequired(boolean secondaryScheduleAmountRequired) {
		this.secondaryScheduleAmountRequired = secondaryScheduleAmountRequired;
	}


	public boolean isAccrualFrequencyAllowed() {
		return this.accrualFrequencyAllowed;
	}


	public void setAccrualFrequencyAllowed(boolean accrualFrequencyAllowed) {
		this.accrualFrequencyAllowed = accrualFrequencyAllowed;
	}


	public BillingInvoiceRevenueTypes getDefaultRevenueType() {
		return this.defaultRevenueType;
	}


	public void setDefaultRevenueType(BillingInvoiceRevenueTypes defaultRevenueType) {
		this.defaultRevenueType = defaultRevenueType;
	}
}
