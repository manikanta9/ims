package com.clifton.billing.payment.account;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class BillingPaymentInvestmentAccountServiceImpl implements BillingPaymentInvestmentAccountService {


	private AdvancedReadOnlyDAO<BillingPaymentInvestmentAccountAllocation, Criteria> billingPaymentInvestmentAccountAllocationDAO;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingPaymentInvestmentAccountAllocation getBillingPaymentInvestmentAccountAllocationForInvoiceAndAccount(int billingInvoiceId, int investmentAccountId) {
		return getBillingPaymentInvestmentAccountAllocationDAO().findOneByFields(new String[]{"billingInvoice.id", "investmentAccount.id"}, new Object[]{billingInvoiceId, investmentAccountId});
	}


	@Override
	public List<BillingPaymentInvestmentAccountAllocation> getBillingPaymentInvestmentAccountAllocationList(BillingPaymentInvestmentAccountAllocationSearchForm searchForm) {
		return getBillingPaymentInvestmentAccountAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getBusinessCompanyPlatformId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "iar");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceTwo", "ra");
					sub.add(Restrictions.eq("ra.companyPlatform.id", searchForm.getBusinessCompanyPlatformId()));
					sub.add(Restrictions.eqProperty("referenceOne.id", getPathAlias("investmentAccount", criteria) + ".id"));

					LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull("startDate"), Restrictions.leProperty("startDate", getPathAlias("billingInvoice", criteria) + ".invoicePeriodEndDate"));
					LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull("endDate"), Restrictions.geProperty("endDate", getPathAlias("billingInvoice", criteria) + ".invoicePeriodStartDate"));
					sub.add(Restrictions.and(startDateFilter, endDateFilter));
					criteria.add(Subqueries.exists(sub));
				}
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<BillingPaymentInvestmentAccountAllocation, Criteria> getBillingPaymentInvestmentAccountAllocationDAO() {
		return this.billingPaymentInvestmentAccountAllocationDAO;
	}


	public void setBillingPaymentInvestmentAccountAllocationDAO(AdvancedReadOnlyDAO<BillingPaymentInvestmentAccountAllocation, Criteria> billingPaymentInvestmentAccountAllocationDAO) {
		this.billingPaymentInvestmentAccountAllocationDAO = billingPaymentInvestmentAccountAllocationDAO;
	}
}
