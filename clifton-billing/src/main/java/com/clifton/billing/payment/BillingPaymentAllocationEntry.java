package com.clifton.billing.payment;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>BillingPaymentAllocationEntry</code> class is used to apply payment allocations to a new or existing payment.  Allocations can be made at the invoice level or account level.  If at the invoice level
 * the system will auto apply the allocation amounts to the accounts for the invoice based on what is left unpaid (and if the invoice bills at the account level).  If at the account level users can specify for each account
 * the payment amount or still have the system calculated the amounts for the specific invoices and accounts based on what is left unpaid.
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class BillingPaymentAllocationEntry {

	/**
	 * Can be a new or existing payment
	 */
	private BillingPayment existingBillingPayment;

	private BillingPayment newBillingPayment;


	/**
	 * If true, the system will allocate the amounts, if false, will use the allocated amounts as specified on each row
	 * Can ONLY be false when accounts are specifically selected
	 */
	private boolean autoApplyAmounts;

	/**
	 * If above is true, and this is null, the system will apply the max possible amount
	 * Otherwise will allocate specified total across all rows.
	 */
	private BigDecimal allocationAmount;


	/**
	 * The invoice to apply payments to (use is mutually exclusive from the account allocations)
	 */
	private BillingInvoice applyToBillingInvoice;


	/**
	 * The list of BillingPaymentAllocations (account specific) to save for this payment, use is mutually exclusive from the invoice selection
	 */
	private List<BillingPaymentAllocation> billingPaymentAllocationList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingPayment getExistingBillingPayment() {
		return this.existingBillingPayment;
	}


	public void setExistingBillingPayment(BillingPayment existingBillingPayment) {
		this.existingBillingPayment = existingBillingPayment;
	}


	public BillingPayment getNewBillingPayment() {
		return this.newBillingPayment;
	}


	public void setNewBillingPayment(BillingPayment newBillingPayment) {
		this.newBillingPayment = newBillingPayment;
	}


	public boolean isAutoApplyAmounts() {
		return this.autoApplyAmounts;
	}


	public void setAutoApplyAmounts(boolean autoApplyAmounts) {
		this.autoApplyAmounts = autoApplyAmounts;
	}


	public BigDecimal getAllocationAmount() {
		return this.allocationAmount;
	}


	public void setAllocationAmount(BigDecimal allocationAmount) {
		this.allocationAmount = allocationAmount;
	}


	public BillingInvoice getApplyToBillingInvoice() {
		return this.applyToBillingInvoice;
	}


	public void setApplyToBillingInvoice(BillingInvoice applyToBillingInvoice) {
		this.applyToBillingInvoice = applyToBillingInvoice;
	}


	public List<BillingPaymentAllocation> getBillingPaymentAllocationList() {
		return this.billingPaymentAllocationList;
	}


	public void setBillingPaymentAllocationList(List<BillingPaymentAllocation> billingPaymentAllocationList) {
		this.billingPaymentAllocationList = billingPaymentAllocationList;
	}
}
