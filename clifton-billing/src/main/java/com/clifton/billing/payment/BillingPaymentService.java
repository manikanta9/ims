package com.clifton.billing.payment;


import com.clifton.billing.payment.search.BillingPaymentAllocationSearchForm;
import com.clifton.billing.payment.search.BillingPaymentSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;

import java.util.List;


/**
 * The <code>BillingPaymentService</code> ...
 *
 * @author manderson
 */
public interface BillingPaymentService {

	////////////////////////////////////////////////////////////////////////////
	//////////              Billing Payment Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPayment getBillingPayment(int id);


	public List<BillingPayment> getBillingPaymentList(BillingPaymentSearchForm searchForm);


	public BillingPayment saveBillingPayment(BillingPayment bean);


	/**
	 * VOIDs the specified payment
	 * NOTE: DELETE IS NOT SUPPORTED, WHEN USERS DELETE, IT ACTUALLY MARKS IT AS VOID
	 *
	 * @param ignoreValidation used to warn users if the payment has allocations and if they continue those allocations will be deleted
	 */
	@UserIgnorableValidation
	public void deleteBillingPayment(int id, boolean ignoreValidation);


	////////////////////////////////////////////////////////////////////////////
	/////////           Billing Payment Allocation Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPaymentAllocation getBillingPaymentAllocation(int id);


	public List<BillingPaymentAllocation> getBillingPaymentAllocationList(BillingPaymentAllocationSearchForm searchForm);


	public List<BillingPaymentAllocation> getBillingPaymentAllocationListForPayment(int paymentId);


	public List<BillingPaymentAllocation> getBillingPaymentAllocationListForInvoice(int invoiceId);


	public void deleteBillingPaymentAllocation(int id);


	public void deleteBillingPaymentAllocationListForInvoice(int invoiceId);


	public void deleteBillingPaymentAllocationListForPayment(int paymentId);


	////////////////////////////////////////////////////////////////////////////
	////////         Billing Payment Allocation Entry Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BillingPayment.class)
	public void saveBillingPaymentAllocationEntry(BillingPaymentAllocationEntry billingPaymentAllocationEntry);
}
