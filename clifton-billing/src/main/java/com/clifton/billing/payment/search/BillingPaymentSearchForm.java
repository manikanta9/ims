package com.clifton.billing.payment.search;


import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingPaymentSearchForm</code> ...
 *
 * @author manderson
 */
public class BillingPaymentSearchForm extends BaseAuditableEntitySearchForm implements BusinessCompanyAwareSearchForm {

	@SearchField
	private Integer id;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyId;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;

	@SearchField(searchFieldPath = "businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField
	private Date paymentDate;

	@SearchField
	private String paymentType;

	@SearchField
	private BigDecimal paymentAmount;

	@SearchField
	private BigDecimal allocatedAmount;

	// Custom Search Field = if True, checks paymentAmount = allocatedAmount
	private Boolean fullyAllocated;

	@SearchField
	private String notes;

	@SearchField
	private Boolean voided;

	/**
	 * In order to use voided search form field, this option must be set to true,
	 * otherwise voided payments are always excluded
	 */
	private Boolean includeVoided;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"businessCompany"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		// DO NOTHING
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		// DO NOTHING
	}

	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getPaymentDate() {
		return this.paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}


	public BigDecimal getPaymentAmount() {
		return this.paymentAmount;
	}


	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getNotes() {
		return this.notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public BigDecimal getAllocatedAmount() {
		return this.allocatedAmount;
	}


	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}


	public Boolean getFullyAllocated() {
		return this.fullyAllocated;
	}


	public void setFullyAllocated(Boolean fullyAllocated) {
		this.fullyAllocated = fullyAllocated;
	}


	public String getPaymentType() {
		return this.paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}


	public Boolean getVoided() {
		return this.voided;
	}


	public void setVoided(Boolean voided) {
		this.voided = voided;
	}


	public Boolean getIncludeVoided() {
		return this.includeVoided;
	}


	public void setIncludeVoided(Boolean includeVoided) {
		this.includeVoided = includeVoided;
	}
}
