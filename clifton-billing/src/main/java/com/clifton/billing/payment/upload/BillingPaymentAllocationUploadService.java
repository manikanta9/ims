package com.clifton.billing.payment.upload;

import com.clifton.billing.payment.BillingPaymentAllocation;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * <code>BillingPaymentAllocationUploadService</code> Interface level definition of custom BillingPaymentAllocation upload functionality
 *
 * @author jonathanr
 */

public interface BillingPaymentAllocationUploadService {

	@SecureMethod(dtoClass = BillingPaymentAllocation.class)
	public void uploadBillingPaymentAllocationFile(BillingPaymentAllocationUploadCustomCommand uploadCommand);
}
