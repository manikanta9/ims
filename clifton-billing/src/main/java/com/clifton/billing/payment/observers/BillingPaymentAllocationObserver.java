package com.clifton.billing.payment.observers;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.billing.payment.BillingPayment;
import com.clifton.billing.payment.BillingPaymentAllocation;
import com.clifton.billing.payment.BillingPaymentService;
import com.clifton.billing.payment.BillingPaymentStatuses;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>BillingPaymentAllocationObserver</code>
 * 1. Validates allocated amount is not greater than payment amount available for allocation
 * 2. Validates allocated amount is not greater than invoice amount available for payment allocation
 * 3. Updates Payment allocated amount and Invoice paid amount based on the changes to the allocation
 *
 * @author manderson
 */
@Component
public class BillingPaymentAllocationObserver extends SelfRegisteringDaoObserver<BillingPaymentAllocation> {

	private BillingInvoiceService billingInvoiceService;
	private BillingPaymentService billingPaymentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<BillingPaymentAllocation> dao, DaoEventTypes event, BillingPaymentAllocation bean) {
		if (event.isInsert() || event.isUpdate()) {
			if (bean.getBillingInvoice().getBillingDefinition() != null) {
				ValidationUtils.assertNotNull(bean.getInvestmentAccount(), "Account selections are required for invoices that use billing definitions.  All payments must be applied to the accounts.");
			}
			ValidationUtils.assertFalse(bean.getBillingPayment().isVoided(), "Voided payments cannot be allocated to invoices.");
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<BillingPaymentAllocation> dao, DaoEventTypes event, BillingPaymentAllocation bean, Throwable e) {
		if (e == null) {
			// Needs to be validated/run after because the underlying invoice/payment is updated and then there are issues saving the allocation
			validateAndUpdatePayment(dao, event, bean);
			validateInvoiceAccountDetail(dao, event, bean);
			validateAndUpdateInvoice(dao, event, bean);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateAndUpdatePayment(ReadOnlyDAO<BillingPaymentAllocation> dao, DaoEventTypes event, BillingPaymentAllocation bean) {
		// Get Existing Payment Allocations
		BillingPayment payment = bean.getBillingPayment();
		List<BillingPaymentAllocation> paymentAllocationList = dao.findByField("billingPayment.id", bean.getBillingPayment().getId());
		BigDecimal total = payment.getPaymentAmount();
		BigDecimal allocatedTotal = BigDecimal.ZERO;

		for (BillingPaymentAllocation alloc : CollectionUtils.getIterable(paymentAllocationList)) {
			if (!bean.isNewBean() && !bean.equals(alloc)) {
				allocatedTotal = MathUtils.add(allocatedTotal, alloc.getAllocatedAmount());
			}
		}

		// Additional validation is done for inserts/updates and this bean's allocated amount is added to the total
		if (event.isInsert() || event.isUpdate()) {
			// VALIDATE PAYMENT IS NOT OVER ALLOCATED
			allocatedTotal = MathUtils.add(allocatedTotal, bean.getAllocatedAmount());
			BillingPaymentStatuses status = BillingPaymentStatuses.validateBillingPaymentStatus(total, allocatedTotal);
			ValidationUtils.assertFalse(BillingPaymentStatuses.OVER == status, "Cannot over allocate a payment.  This payment has a total of [" + CoreMathUtils.formatNumberMoney(total) + "], ["
					+ CoreMathUtils.formatNumberMoney(MathUtils.subtract(allocatedTotal, bean.getAllocatedAmount())) + "] of which has already been allocated.  This payment allocation of [" + CoreMathUtils.formatNumberMoney(bean.getAllocatedAmount()) + "] cannot exceed ["
					+ CoreMathUtils.formatNumberMoney(MathUtils.subtract(total, MathUtils.subtract(allocatedTotal, bean.getAllocatedAmount()))) + "].");
		}

		// For inserts, updates, and deletes - UPDATE PAYMENT ALLOCATED AMOUNT BASED ON THIS NEW VALUE
		payment.setAllocatedAmount(allocatedTotal);
		getBillingPaymentService().saveBillingPayment(payment);
	}


	private void validateInvoiceAccountDetail(ReadOnlyDAO<BillingPaymentAllocation> dao, DaoEventTypes event, BillingPaymentAllocation bean) {
		if (bean.getInvestmentAccount() != null) {
			// Get Existing Invoice Account Allocations
			BillingInvoiceDetailAccountSearchForm searchForm = new BillingInvoiceDetailAccountSearchForm();
			searchForm.setInvoiceId(bean.getBillingInvoice().getId());
			searchForm.setInvestmentAccountId(bean.getInvestmentAccount().getId());
			// Exclude Internal Revenue Share
			searchForm.setExcludeRevenueType(BillingInvoiceRevenueTypes.REVENUE_SHARE_INTERNAL);
			List<BillingInvoiceDetailAccount> detailAccountList = getBillingInvoiceService().getBillingInvoiceDetailAccountList(searchForm);
			BigDecimal accountTotalBilled = CoreMathUtils.sumProperty(detailAccountList, BillingInvoiceDetailAccount::getBillingAmount);

			// Get Payment Account Allocations
			List<BillingPaymentAllocation> accountInvoiceAllocationList = dao.findByFields(new String[]{"billingInvoice.id", "investmentAccount.id"}, new Object[]{bean.getBillingInvoice().getId(), bean.getInvestmentAccount().getId()});
			BigDecimal allocatedTotal = BigDecimal.ZERO;

			for (BillingPaymentAllocation alloc : CollectionUtils.getIterable(accountInvoiceAllocationList)) {
				if (!bean.isNewBean() && !bean.equals(alloc)) {
					allocatedTotal = MathUtils.add(allocatedTotal, alloc.getAllocatedAmount());
				}
			}

			// Additional validation is done for inserts/updates and this bean's allocated amount is added to the total
			if (event.isInsert() || event.isUpdate()) {
				// VALIDATE ACCOUNT IS NOT OVER PAID
				allocatedTotal = MathUtils.add(allocatedTotal, bean.getAllocatedAmount());
			}
			BillingPaymentStatuses status = BillingPaymentStatuses.validateBillingPaymentStatus(accountTotalBilled, allocatedTotal);
			ValidationUtils.assertFalse(BillingPaymentStatuses.OVER == status, "Account " + bean.getInvestmentAccount().getLabel() + " for Invoice # " + bean.getBillingInvoice().getId() + " payment status is over-allocated.  Total Billed: " + CoreMathUtils.formatNumberMoney(accountTotalBilled) + " Total Paid: " + CoreMathUtils.formatNumberMoney(allocatedTotal));
		}
	}


	private void validateAndUpdateInvoice(ReadOnlyDAO<BillingPaymentAllocation> dao, DaoEventTypes event, BillingPaymentAllocation bean) {
		// Get Existing Invoice Allocations
		BillingInvoice invoice = bean.getBillingInvoice();
		List<BillingPaymentAllocation> invoiceAllocationList = dao.findByField("billingInvoice.id", bean.getBillingInvoice().getId());
		BigDecimal total = invoice.getPayableTotalAmount();
		BigDecimal allocatedTotal = BigDecimal.ZERO;

		for (BillingPaymentAllocation alloc : CollectionUtils.getIterable(invoiceAllocationList)) {
			if (!bean.isNewBean() && !bean.equals(alloc)) {
				allocatedTotal = MathUtils.add(allocatedTotal, alloc.getAllocatedAmount());
			}
		}

		// Additional validation is done for inserts/updates and this bean's allocated amount is added to the total
		if (event.isInsert() || event.isUpdate()) {
			// VALIDATE INVOICE IS NOT OVER PAID
			allocatedTotal = MathUtils.add(allocatedTotal, bean.getAllocatedAmount());
			BillingPaymentStatuses status = BillingPaymentStatuses.validateBillingPaymentStatus(total, allocatedTotal);
			ValidationUtils.assertFalse(
					BillingPaymentStatuses.OVER == status,
					"Cannot over allocate payments for an invoice.  This invoice has a total of [" + CoreMathUtils.formatNumberMoney(total) + "], ["
							+ CoreMathUtils.formatNumberMoney(MathUtils.subtract(allocatedTotal, bean.getAllocatedAmount()))
							+ "] of which has already been allocated.  This payment allocation cannot exceed ["
							+ CoreMathUtils.formatNumberMoney(MathUtils.subtract(total, MathUtils.subtract(allocatedTotal, bean.getAllocatedAmount()))) + "].");
		}

		// For inserts, updates, and deletes - UPDATE PAYMENT ALLOCATED AMOUNT BASED ON THIS NEW VALUE
		getBillingInvoiceService().saveBillingInvoicePaidAmount(invoice, allocatedTotal);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPaymentService getBillingPaymentService() {
		return this.billingPaymentService;
	}


	public void setBillingPaymentService(BillingPaymentService billingPaymentService) {
		this.billingPaymentService = billingPaymentService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}
}
