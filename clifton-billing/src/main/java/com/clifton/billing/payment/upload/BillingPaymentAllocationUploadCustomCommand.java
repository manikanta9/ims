package com.clifton.billing.payment.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;


/**
 * @author jonathanr
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class BillingPaymentAllocationUploadCustomCommand extends FileUploadCommand<BillingPaymentAllocationUploadCommand> {

	private Integer billingPaymentId;
	protected static final String[] REQUIRED_PROPERTIES = {"allocatedAmount", "clientAccountNumber", "invoiceDate"};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<BillingPaymentAllocationUploadCommand> getUploadBeanClass() {
		return BillingPaymentAllocationUploadCommand.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	public Integer getBillingPaymentId() {
		return this.billingPaymentId;
	}


	public void setBillingPaymentId(Integer billingPaymentId) {
		this.billingPaymentId = billingPaymentId;
	}
}
