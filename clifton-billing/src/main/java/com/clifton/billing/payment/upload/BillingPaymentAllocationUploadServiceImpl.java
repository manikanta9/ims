package com.clifton.billing.payment.upload;

import com.clifton.billing.payment.BillingPaymentAllocation;
import com.clifton.billing.payment.BillingPaymentAllocationEntry;
import com.clifton.billing.payment.BillingPaymentService;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountAllocation;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountAllocationSearchForm;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountService;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <code>BillingPaymentAllocationUploadServiceImpl</code> implements custom upload functionality for billing allocations.
 *
 * @author jonathanr
 */
@Service
public class BillingPaymentAllocationUploadServiceImpl implements BillingPaymentAllocationUploadService {

	private BillingPaymentInvestmentAccountService billingPaymentInvestmentAccountService;
	private BillingPaymentService billingPaymentService;
	private FileUploadHandler fileUploadHandler;


	@Override
	public void uploadBillingPaymentAllocationFile(BillingPaymentAllocationUploadCustomCommand uploadCommand) {
		List<BillingPaymentAllocationUploadCommand> uploadCommandList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
		BillingPaymentAllocationEntry billingPaymentAllocationEntry = new BillingPaymentAllocationEntry();
		billingPaymentAllocationEntry.setExistingBillingPayment(getBillingPaymentService().getBillingPayment(uploadCommand.getBillingPaymentId()));
		List<BillingPaymentAllocation> billingPaymentAllocationList = new ArrayList<>();
		for (BillingPaymentAllocationUploadCommand command : uploadCommandList) {
			BillingPaymentInvestmentAccountAllocationSearchForm searchForm = new BillingPaymentInvestmentAccountAllocationSearchForm();
			if (command.getInvoiceNumber() != null) {
				searchForm.setBillingInvoiceId(command.getInvoiceNumber());
			}
			else {
				searchForm.setInvestmentAccountNumber(command.getClientAccountNumber());
				searchForm.setInvoiceDate(command.getInvoiceDate());
				searchForm.addSearchRestriction("unpaidAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO);
			}
			BillingPaymentInvestmentAccountAllocation billingPaymentInvestmentAccountAllocation = CollectionUtils.getFirstElementStrict(getBillingPaymentInvestmentAccountService().getBillingPaymentInvestmentAccountAllocationList(searchForm), "Could Not find invoice for " + DateUtils.fromDate(command.getInvoiceDate()) + " and client account " + command.getClientAccountNumber(), "More than one invoice found for " + DateUtils.fromDate(command.getInvoiceDate()) + " and client account " + command.getClientAccountNumber() + "; You can use the optional column 'invoiceNumber' to specify a specific invoice.");
			BillingPaymentAllocation billingPaymentAllocation = new BillingPaymentAllocation();
			billingPaymentAllocation.setAllocatedAmount(command.getAllocatedAmount());
			billingPaymentAllocation.setBillingPayment(billingPaymentAllocationEntry.getExistingBillingPayment());
			billingPaymentAllocation.setBillingInvoice(billingPaymentInvestmentAccountAllocation.getBillingInvoice());
			billingPaymentAllocation.setInvestmentAccount(billingPaymentInvestmentAccountAllocation.getInvestmentAccount());
			billingPaymentAllocationList.add(billingPaymentAllocation);
		}
		billingPaymentAllocationEntry.setBillingPaymentAllocationList(billingPaymentAllocationList);
		getBillingPaymentService().saveBillingPaymentAllocationEntry(billingPaymentAllocationEntry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPaymentInvestmentAccountService getBillingPaymentInvestmentAccountService() {
		return this.billingPaymentInvestmentAccountService;
	}


	public void setBillingPaymentInvestmentAccountService(BillingPaymentInvestmentAccountService billingPaymentInvestmentAccountService) {
		this.billingPaymentInvestmentAccountService = billingPaymentInvestmentAccountService;
	}


	public BillingPaymentService getBillingPaymentService() {
		return this.billingPaymentService;
	}


	public void setBillingPaymentService(BillingPaymentService billingPaymentService) {
		this.billingPaymentService = billingPaymentService;
	}


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}
}
