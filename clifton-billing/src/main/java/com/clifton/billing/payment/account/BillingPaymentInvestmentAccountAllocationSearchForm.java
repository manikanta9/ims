package com.clifton.billing.payment.account;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
public class BillingPaymentInvestmentAccountAllocationSearchForm extends BaseSystemHierarchyItemSearchForm {

	@SearchField
	private String id;

	@SearchField(searchField = "billingInvoice.id,investmentAccount.number,investmentAccount.name,billingInvoice.businessCompany.name", sortField = "investmentAccount.number", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER)
	private String searchPattern;

	@SearchField(searchField = "billingInvoice.id")
	private Integer billingInvoiceId;

	@SearchField(searchFieldPath = "billingInvoice", searchField = "invoiceDate")
	private Date invoiceDate;

	@SearchField(searchFieldPath = "billingInvoice", searchField = "invoiceType.id", sortField = "invoiceType.name")
	private Short invoiceTypeId;

	@SearchField(searchFieldPath = "billingInvoice.businessCompany", searchField = "name,companyLegalName", sortField = "name")
	private String invoiceCompanyName;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "number,shortName,name")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "number")
	private String investmentAccountNumber;

	// Custom Search field for Client account where existing a relationship to a holding account using the selected platform
	// The is active during the invoice period
	private Integer businessCompanyPlatformId;

	@SearchField
	private BigDecimal totalAmount;

	@SearchField
	private BigDecimal revenueShareExternalTotalAmount;

	@SearchField
	private BigDecimal revenueShareInternalTotalAmount;

	@SearchField(searchField = "totalAmount,revenueShareExternalTotalAmount", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal revenueNetTotalAmount;

	@SearchField(searchField = "totalAmount,revenueShareExternalTotalAmount,revenueShareInternalTotalAmount", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal revenueFinalTotalAmount;

	@SearchField
	private BigDecimal paidAmount;

	@SearchField
	private BigDecimal unpaidAmount;

	@SearchField(searchFieldPath = "billingInvoice.workflowStatus", searchField = "name")
	private String invoiceWorkflowStatusNameContains;

	@SearchField(searchFieldPath = "billingInvoice.workflowState", searchField = "name")
	private String invoiceWorkflowStateNameContains;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "BillingPaymentInvestmentAccount";
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getBillingInvoiceId() {
		return this.billingInvoiceId;
	}


	public void setBillingInvoiceId(Integer billingInvoiceId) {
		this.billingInvoiceId = billingInvoiceId;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public Short getInvoiceTypeId() {
		return this.invoiceTypeId;
	}


	public void setInvoiceTypeId(Short invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}


	public String getInvoiceCompanyName() {
		return this.invoiceCompanyName;
	}


	public void setInvoiceCompanyName(String invoiceCompanyName) {
		this.invoiceCompanyName = invoiceCompanyName;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public String getInvestmentAccountNumber() {
		return this.investmentAccountNumber;
	}


	public void setInvestmentAccountNumber(String investmentAccountNumber) {
		this.investmentAccountNumber = investmentAccountNumber;
	}


	public Integer getBusinessCompanyPlatformId() {
		return this.businessCompanyPlatformId;
	}


	public void setBusinessCompanyPlatformId(Integer businessCompanyPlatformId) {
		this.businessCompanyPlatformId = businessCompanyPlatformId;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	public BigDecimal getRevenueShareExternalTotalAmount() {
		return this.revenueShareExternalTotalAmount;
	}


	public void setRevenueShareExternalTotalAmount(BigDecimal revenueShareExternalTotalAmount) {
		this.revenueShareExternalTotalAmount = revenueShareExternalTotalAmount;
	}


	public BigDecimal getRevenueShareInternalTotalAmount() {
		return this.revenueShareInternalTotalAmount;
	}


	public void setRevenueShareInternalTotalAmount(BigDecimal revenueShareInternalTotalAmount) {
		this.revenueShareInternalTotalAmount = revenueShareInternalTotalAmount;
	}


	public BigDecimal getRevenueNetTotalAmount() {
		return this.revenueNetTotalAmount;
	}


	public void setRevenueNetTotalAmount(BigDecimal revenueNetTotalAmount) {
		this.revenueNetTotalAmount = revenueNetTotalAmount;
	}


	public BigDecimal getRevenueFinalTotalAmount() {
		return this.revenueFinalTotalAmount;
	}


	public void setRevenueFinalTotalAmount(BigDecimal revenueFinalTotalAmount) {
		this.revenueFinalTotalAmount = revenueFinalTotalAmount;
	}


	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}


	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}


	public BigDecimal getUnpaidAmount() {
		return this.unpaidAmount;
	}


	public void setUnpaidAmount(BigDecimal unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}


	public String getInvoiceWorkflowStatusNameContains() {
		return this.invoiceWorkflowStatusNameContains;
	}


	public void setInvoiceWorkflowStatusNameContains(String invoiceWorkflowStatusNameContains) {
		this.invoiceWorkflowStatusNameContains = invoiceWorkflowStatusNameContains;
	}


	public String getInvoiceWorkflowStateNameContains() {
		return this.invoiceWorkflowStateNameContains;
	}


	public void setInvoiceWorkflowStateNameContains(String invoiceWorkflowStateNameContains) {
		this.invoiceWorkflowStateNameContains = invoiceWorkflowStateNameContains;
	}
}
