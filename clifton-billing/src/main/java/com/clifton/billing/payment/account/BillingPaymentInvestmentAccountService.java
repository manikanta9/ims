package com.clifton.billing.payment.account;

import com.clifton.billing.payment.BillingPayment;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface BillingPaymentInvestmentAccountService {


	@DoNotAddRequestMapping
	public BillingPaymentInvestmentAccountAllocation getBillingPaymentInvestmentAccountAllocationForInvoiceAndAccount(int billingInvoiceId, int investmentAccountId);


	@SecureMethod(dtoClass = BillingPayment.class)
	public List<BillingPaymentInvestmentAccountAllocation> getBillingPaymentInvestmentAccountAllocationList(BillingPaymentInvestmentAccountAllocationSearchForm searchForm);
}
