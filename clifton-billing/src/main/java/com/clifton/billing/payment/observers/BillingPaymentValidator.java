package com.clifton.billing.payment.observers;


import com.clifton.billing.payment.BillingPayment;
import com.clifton.billing.payment.BillingPaymentStatuses;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingPaymentValidator</code> ...
 *
 * @author manderson
 */
@Component
public class BillingPaymentValidator extends SelfRegisteringDaoValidator<BillingPayment> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BillingPayment bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertFalse(config.isDelete(), "Payments cannot be deleted");
		BillingPaymentStatuses status = BillingPaymentStatuses.validateBillingPaymentStatus(bean.getPaymentAmount(), bean.getAllocatedAmount());
		if (bean.isVoided()) {
			ValidationUtils.assertTrue(status == BillingPaymentStatuses.NONE, "Voided payments cannot be allocated.");
		}
		ValidationUtils.assertFalse(status == BillingPaymentStatuses.OVER, "Payments cannot be allocated to more than the actual payment amount.");
	}
}
