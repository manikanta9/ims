package com.clifton.billing.payment;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountAllocation;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountAllocationSearchForm;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountService;
import com.clifton.billing.payment.search.BillingPaymentAllocationSearchForm;
import com.clifton.billing.payment.search.BillingPaymentSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingPaymentServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class BillingPaymentServiceImpl implements BillingPaymentService {

	private AdvancedUpdatableDAO<BillingPayment, Criteria> billingPaymentDAO;
	private AdvancedUpdatableDAO<BillingPaymentAllocation, Criteria> billingPaymentAllocationDAO;

	private BillingPaymentInvestmentAccountService billingPaymentInvestmentAccountService;


	////////////////////////////////////////////////////////////////////////////
	//////////              Billing Payment Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingPayment getBillingPayment(int id) {
		return getBillingPaymentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingPayment> getBillingPaymentList(final BillingPaymentSearchForm searchForm) {
		if (!BooleanUtils.isTrue(searchForm.getIncludeVoided())) {
			searchForm.setVoided(false);
		}
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getFullyAllocated() != null) {
					if (searchForm.getFullyAllocated()) {
						criteria.add(Restrictions.eqProperty("paymentAmount", "allocatedAmount"));
					}
					else {
						criteria.add(Restrictions.neProperty("paymentAmount", "allocatedAmount"));
					}
				}
			}
		};
		return getBillingPaymentDAO().findBySearchCriteria(config);
	}


	@Override
	@Transactional
	public BillingPayment saveBillingPayment(BillingPayment bean) {
		if (bean.getAllocatedAmount() == null) {
			bean.setAllocatedAmount(BigDecimal.ZERO); // New payments set allocated amount = 0 - observer will reset it
		}
		return getBillingPaymentDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteBillingPayment(int id, boolean ignoreValidation) {
		BillingPayment payment = getBillingPayment(id);
		if (payment != null) {
			ValidationUtils.assertFalse(payment.isVoided(), "Payment has already been voided");

			List<BillingPaymentAllocation> paymentAllocationList = getBillingPaymentAllocationListForPayment(id);
			if (!CollectionUtils.isEmpty(paymentAllocationList)) {
				if (ignoreValidation) {
					deleteBillingPaymentAllocationListForPayment(id);
					payment = getBillingPayment(id); // The above will update the allocation list
				}
				else {
					throw new UserIgnorableValidationException("Selected payment [" + payment.getLabel() + "] has payment allocations.  When the payment is voided those allocations will be removed.");
				}
			}
			payment.setVoided(true);
			getBillingPaymentDAO().save(payment);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////           Billing Payment Allocation Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingPaymentAllocation getBillingPaymentAllocation(int id) {
		return getBillingPaymentAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BillingPaymentAllocation> getBillingPaymentAllocationList(BillingPaymentAllocationSearchForm searchForm) {
		return getBillingPaymentAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<BillingPaymentAllocation> getBillingPaymentAllocationListForPayment(int paymentId) {
		BillingPaymentAllocationSearchForm searchForm = new BillingPaymentAllocationSearchForm();
		searchForm.setBillingPaymentId(paymentId);
		return getBillingPaymentAllocationList(searchForm);
	}


	@Override
	public List<BillingPaymentAllocation> getBillingPaymentAllocationListForInvoice(int invoiceId) {
		BillingPaymentAllocationSearchForm searchForm = new BillingPaymentAllocationSearchForm();
		searchForm.setBillingInvoiceId(invoiceId);
		return getBillingPaymentAllocationList(searchForm);
	}


	protected List<BillingPaymentAllocation> populateBillingPaymentAllocationListForInvoice(BillingPayment billingPayment, BillingInvoice billingInvoice, BigDecimal allocationAmount) {
		// Determines if payments are allocated to accounts or not
		if (!billingInvoice.getInvoiceType().isBillingDefinitionRequired()) {
			BigDecimal maxInvoice = billingInvoice.getUnpaidAmount();
			BigDecimal maxPayment = billingPayment.getAvailableAmount();
			allocationAmount = getMaxAllocationAmount(maxInvoice, maxPayment, allocationAmount);
			BillingPaymentAllocation billingPaymentAllocation = new BillingPaymentAllocation();
			billingPaymentAllocation.setBillingPayment(billingPayment);
			billingPaymentAllocation.setBillingInvoice(billingInvoice);
			billingPaymentAllocation.setAllocatedAmount(allocationAmount);
			return CollectionUtils.createList(billingPaymentAllocation);
		}

		BillingPaymentInvestmentAccountAllocationSearchForm searchForm = new BillingPaymentInvestmentAccountAllocationSearchForm();
		searchForm.setBillingInvoiceId(billingInvoice.getId());
		searchForm.addSearchRestriction(new SearchRestriction("unpaidAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		List<BillingPaymentInvestmentAccountAllocation> existingAccountAllocationList = getBillingPaymentInvestmentAccountService().getBillingPaymentInvestmentAccountAllocationList(searchForm);

		if (CollectionUtils.isEmpty(existingAccountAllocationList)) {
			throw new ValidationException("There are no unpaid accounts on invoice " + billingInvoice.getId() + " to allocate payments to");
		}

		return populateBillingPaymentAllocationListForAccountAllocationList(billingPayment, allocationAmount, existingAccountAllocationList);
	}


	protected List<BillingPaymentAllocation> populateBillingPaymentAllocationListForAccountAllocationList(BillingPayment billingPayment, BigDecimal allocationAmount, List<BillingPaymentInvestmentAccountAllocation> accountAllocationList) {
		BigDecimal maxInvoice = CoreMathUtils.sumProperty(accountAllocationList, BillingPaymentInvestmentAccountAllocation::getUnpaidAmount);
		BigDecimal maxPayment = billingPayment.getAvailableAmount();

		allocationAmount = getMaxAllocationAmount(maxInvoice, maxPayment, allocationAmount);

		boolean applyFullAmount = MathUtils.isEqual(maxInvoice, allocationAmount);
		BigDecimal applyPercentage = CoreMathUtils.getPercentValue(allocationAmount, maxInvoice);
		List<BillingPaymentAllocation> paymentAllocationList = new ArrayList<>();
		BillingPaymentAllocation maxUnpaidAmountAllocation = null; // used to track to apply penny differences from rounding to
		BigDecimal maxUnpaidAmount = null;
		for (BillingPaymentInvestmentAccountAllocation accountAllocation : CollectionUtils.getIterable(accountAllocationList)) {
			BillingPaymentAllocation billingPaymentAllocation = new BillingPaymentAllocation();
			billingPaymentAllocation.setBillingInvoice(accountAllocation.getBillingInvoice());
			billingPaymentAllocation.setBillingPayment(billingPayment);
			billingPaymentAllocation.setInvestmentAccount(accountAllocation.getInvestmentAccount());
			BigDecimal allocatedAmount = applyFullAmount ? accountAllocation.getUnpaidAmount() : MathUtils.round(MathUtils.getPercentageOf(applyPercentage, accountAllocation.getUnpaidAmount()), 2);
			if (MathUtils.isGreaterThan(MathUtils.abs(allocatedAmount), MathUtils.abs(accountAllocation.getUnpaidAmount()))) {
				allocatedAmount = accountAllocation.getUnpaidAmount();
			}
			billingPaymentAllocation.setAllocatedAmount(allocatedAmount);
			paymentAllocationList.add(billingPaymentAllocation);
			BigDecimal accountUnpaidBalance = MathUtils.subtract(accountAllocation.getUnpaidAmount(), allocatedAmount);
			if (!MathUtils.isNullOrZero(accountUnpaidBalance) && (maxUnpaidAmountAllocation == null || MathUtils.isGreaterThan(MathUtils.abs(accountUnpaidBalance), MathUtils.abs(maxUnpaidAmount)))) {
				maxUnpaidAmountAllocation = billingPaymentAllocation;
			}
		}

		BigDecimal totalAllocated = CoreMathUtils.sumProperty(paymentAllocationList, BillingPaymentAllocation::getAllocatedAmount);
		if (!MathUtils.isNullOrZero(MathUtils.subtract(allocationAmount, totalAllocated))) {
			AssertUtils.assertNotNull(maxUnpaidAmountAllocation, "maxUnpaidAmountAllocation is null");
			maxUnpaidAmountAllocation.setAllocatedAmount(MathUtils.add(maxUnpaidAmountAllocation.getAllocatedAmount(), MathUtils.subtract(allocationAmount, totalAllocated)));
		}
		return paymentAllocationList;
	}


	private BigDecimal getMaxAllocationAmount(BigDecimal invoiceBalance, BigDecimal paymentBalance, BigDecimal allocationAmount) {
		if (allocationAmount == null) {
			allocationAmount = paymentBalance;
		}
		if (!MathUtils.isSameSignum(allocationAmount, invoiceBalance)) {
			throw new ValidationException("The total payment allocation and invoice balance must be the same sign. Allocation Amount: " + CoreMathUtils.formatNumberMoney(allocationAmount) + ", Invoice Balance: " + CoreMathUtils.formatNumberMoney(invoiceBalance));
		}
		return (MathUtils.isLessThan(MathUtils.abs(allocationAmount), MathUtils.abs(invoiceBalance))) ? allocationAmount : invoiceBalance;
	}


	@Override
	public void deleteBillingPaymentAllocation(int id) {
		getBillingPaymentAllocationDAO().delete(id);
	}


	@Override
	public void deleteBillingPaymentAllocationListForInvoice(int invoiceId) {
		getBillingPaymentAllocationDAO().deleteList(getBillingPaymentAllocationListForInvoice(invoiceId));
	}


	@Override
	@Transactional
	public void deleteBillingPaymentAllocationListForPayment(int paymentId) {
		getBillingPaymentAllocationDAO().deleteList(getBillingPaymentAllocationListInSaveOrder(getBillingPaymentAllocationListForPayment(paymentId), true));
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Billing Payment Allocation Entry Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void saveBillingPaymentAllocationEntry(BillingPaymentAllocationEntry billingPaymentAllocationEntry) {
		BillingPayment payment = billingPaymentAllocationEntry.getNewBillingPayment();
		if (billingPaymentAllocationEntry.getExistingBillingPayment() != null) {
			payment = billingPaymentAllocationEntry.getExistingBillingPayment();
		}
		ValidationUtils.assertNotNull(payment, "Either existing payment must be selected or new payment information is required.");
		boolean newPayment = payment.isNewBean();
		if (newPayment) {
			payment = saveBillingPayment(payment);
		}

		List<BillingPaymentAllocation> newPaymentAllocationList;

		if (billingPaymentAllocationEntry.getApplyToBillingInvoice() != null) {
			newPaymentAllocationList = populateBillingPaymentAllocationListForInvoice(payment, billingPaymentAllocationEntry.getApplyToBillingInvoice(), billingPaymentAllocationEntry.getAllocationAmount());
		}
		else {
			newPaymentAllocationList = billingPaymentAllocationEntry.getBillingPaymentAllocationList();
			if (CollectionUtils.isEmpty(newPaymentAllocationList)) {
				throw new ValidationException("An invoice or at least one account allocation must be selected.");
			}
			if (billingPaymentAllocationEntry.isAutoApplyAmounts()) {
				List<BillingPaymentInvestmentAccountAllocation> accountAllocationList = new ArrayList<>();
				for (BillingPaymentAllocation paymentAllocation : CollectionUtils.getIterable(billingPaymentAllocationEntry.getBillingPaymentAllocationList())) {
					accountAllocationList.add(getBillingPaymentInvestmentAccountService().getBillingPaymentInvestmentAccountAllocationForInvoiceAndAccount(paymentAllocation.getBillingInvoice().getId(), paymentAllocation.getInvestmentAccount().getId()));
				}
				newPaymentAllocationList = populateBillingPaymentAllocationListForAccountAllocationList(payment, billingPaymentAllocationEntry.getAllocationAmount(), accountAllocationList);
			}
		}

		List<BillingPaymentAllocation> saveList = new ArrayList<>();
		// We need the existing list if we are adding to an existing account allocation so we update the original
		List<BillingPaymentAllocation> existingPaymentAllocationList = (newPayment ? null : getBillingPaymentAllocationListForPayment(payment.getId()));
		Map<String, BillingPaymentAllocation> existingAccountPaymentAllocationMap = BeanUtils.getBeanMap(existingPaymentAllocationList, this::getUniqueKeyForBillingPaymentAllocation);

		// VALIDATION, ETC.
		for (BillingPaymentAllocation paymentAllocation : newPaymentAllocationList) {
			String key = getUniqueKeyForBillingPaymentAllocation(paymentAllocation);
			if (existingAccountPaymentAllocationMap.containsKey(key)) {
				BillingPaymentAllocation existingPaymentAllocation = existingAccountPaymentAllocationMap.get(key);
				existingPaymentAllocation.setAllocatedAmount(MathUtils.add(existingPaymentAllocation.getAllocatedAmount(), paymentAllocation.getAllocatedAmount()));
				saveList.add(existingPaymentAllocation);
			}
			else {
				paymentAllocation.setBillingPayment(payment);
				saveList.add(paymentAllocation);
			}
		}
		getBillingPaymentAllocationDAO().saveList(getBillingPaymentAllocationListInSaveOrder(saveList, false));
	}


	private String getUniqueKeyForBillingPaymentAllocation(BillingPaymentAllocation billingPaymentAllocation) {
		return BeanUtils.getBeanIdentity(billingPaymentAllocation.getBillingInvoice()) + "_" + BeanUtils.getBeanIdentity(billingPaymentAllocation.getInvestmentAccount());
	}


	/**
	 * Returns the given list of payment allocations in "save" order for saves and deletes.
	 * For Saves:  Must add payment allocations to those the opposite sign of the payment first (So we increase the available amount first)
	 * For Deletes: Must remove payment allocations to those the SAME sign of the payment first
	 */
	private List<BillingPaymentAllocation> getBillingPaymentAllocationListInSaveOrder(List<BillingPaymentAllocation> paymentAllocationList, boolean sortForDelete) {
		return BeanUtils.sortWithFunction(paymentAllocationList, paymentAllocation -> MathUtils.isSameSignum(paymentAllocation.getAllocatedAmount(), paymentAllocation.getBillingPayment().getPaymentAmount()) ? 1 : 0, !sortForDelete);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////              Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BillingPayment, Criteria> getBillingPaymentDAO() {
		return this.billingPaymentDAO;
	}


	public void setBillingPaymentDAO(AdvancedUpdatableDAO<BillingPayment, Criteria> billingPaymentDAO) {
		this.billingPaymentDAO = billingPaymentDAO;
	}


	public AdvancedUpdatableDAO<BillingPaymentAllocation, Criteria> getBillingPaymentAllocationDAO() {
		return this.billingPaymentAllocationDAO;
	}


	public void setBillingPaymentAllocationDAO(AdvancedUpdatableDAO<BillingPaymentAllocation, Criteria> billingPaymentAllocationDAO) {
		this.billingPaymentAllocationDAO = billingPaymentAllocationDAO;
	}


	public BillingPaymentInvestmentAccountService getBillingPaymentInvestmentAccountService() {
		return this.billingPaymentInvestmentAccountService;
	}


	public void setBillingPaymentInvestmentAccountService(BillingPaymentInvestmentAccountService billingPaymentInvestmentAccountService) {
		this.billingPaymentInvestmentAccountService = billingPaymentInvestmentAccountService;
	}
}
