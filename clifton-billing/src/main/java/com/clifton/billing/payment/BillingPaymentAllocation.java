package com.clifton.billing.payment;


import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;


/**
 * The <code>BillingPaymentAllocation</code> is an allocation of a BillingPayment applied to an invoice (an account)
 * Account level payment allocations only applies to revenue type invoices where we track the fees at the account level.
 * Non-revenue type invoices we do not track at the account level, therefore we do not allocate payments at this level of granularity
 *
 * @author manderson
 */
public class BillingPaymentAllocation extends BaseEntity<Integer> {

	private BillingPayment billingPayment;

	private BillingInvoice billingInvoice;

	/**
	 * Any invoice that allocates fees to the accounts we'll also allocate the payments to the accounts
	 * This only applies to invoices that use billing definitions - manual invoices don't track account level invoicing
	 */
	private InvestmentAccount investmentAccount;

	private BigDecimal allocatedAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCompany() {
		if (getBillingPayment() != null) {
			return getBillingPayment().getBusinessCompany();
		}
		if (getBillingInvoice() != null) {
			return getBillingInvoice().getBusinessCompany();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingPayment getBillingPayment() {
		return this.billingPayment;
	}


	public void setBillingPayment(BillingPayment billingPayment) {
		this.billingPayment = billingPayment;
	}


	public BillingInvoice getBillingInvoice() {
		return this.billingInvoice;
	}


	public void setBillingInvoice(BillingInvoice billingInvoice) {
		this.billingInvoice = billingInvoice;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public BigDecimal getAllocatedAmount() {
		return this.allocatedAmount;
	}


	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}
}
