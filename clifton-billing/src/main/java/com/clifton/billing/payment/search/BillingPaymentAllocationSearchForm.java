package com.clifton.billing.payment.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BillingPaymentAllocationSearchForm</code> ...
 *
 * @author manderson
 */
public class BillingPaymentAllocationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "billingInvoice.id")
	private Integer billingInvoiceId;

	@SearchField(searchField = "billingPayment.id")
	private Integer billingPaymentId;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getBillingInvoiceId() {
		return this.billingInvoiceId;
	}


	public void setBillingInvoiceId(Integer billingInvoiceId) {
		this.billingInvoiceId = billingInvoiceId;
	}


	public Integer getBillingPaymentId() {
		return this.billingPaymentId;
	}


	public void setBillingPaymentId(Integer billingPaymentId) {
		this.billingPaymentId = billingPaymentId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}
}
