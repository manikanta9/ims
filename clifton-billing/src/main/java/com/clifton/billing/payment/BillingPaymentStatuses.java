package com.clifton.billing.payment;


import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>BillingPaymentStatus</code> defines the status of the payment as it relates to being allocated to invoices.
 *
 * @author manderson
 */
public enum BillingPaymentStatuses {

	NONE("None", 1), //

	PARTIAL("Partially Allocated", 2), //

	FULL("Fully Allocated", 3), //

	OVER("Error - Over Allocated", 4);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BillingPaymentStatuses validateBillingPaymentStatus(BigDecimal total, BigDecimal totalAllocated) {
		// Nothing to Pay Consider to be fully paid is allocated amount is also zero (otherwise could be over paid)
		if (MathUtils.isNullOrZero(total) && MathUtils.isNullOrZero(totalAllocated)) {
			return FULL;
		}
		if (MathUtils.isNullOrZero(totalAllocated)) {
			return NONE;
		}

		BigDecimal difference = MathUtils.subtract(total, totalAllocated);
		if (MathUtils.isNullOrZero(difference)) {
			return FULL;
		}

		// If Difference is same sign as total, then we are partial allocated (i.e. 5 - 4 = 1; 1 is same sign as 5 so we are partially allocated)
		if (MathUtils.isSameSignum(total, difference)) {
			return PARTIAL;
		}
		// Over allocated (5 - 6 = -1; -1 is different sign than 5, so we are over allocated
		return OVER;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	BillingPaymentStatuses(String label, int order) {
		this.label = label;
		this.order = order;
	}


	private final String label;
	private final int order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public int getOrder() {
		return this.order;
	}
}
