package com.clifton.billing.payment.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Row level access object for custom billingPaymentAllocation upload functionality.
 *
 * @author jonathanr
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class BillingPaymentAllocationUploadCommand {

	private Integer invoiceNumber;
	private String clientAccountNumber;
	private Date invoiceDate;
	private BigDecimal allocatedAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvoiceNumber() {
		return this.invoiceNumber;
	}


	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Date getInvoiceDate() {
		return this.invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public BigDecimal getAllocatedAmount() {
		return this.allocatedAmount;
	}


	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}
}
