package com.clifton.billing.payment.account;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypeAmountAware;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>{@link BillingPaymentInvestmentAccountAllocation}</code> class is an extended object that queries for invoice accounts details, grouped by invoice and account and
 * their payment allocation status.
 *
 * @author manderson
 */
@NonPersistentObject
public class BillingPaymentInvestmentAccountAllocation extends BaseSimpleEntity<String> implements BillingInvoiceRevenueTypeAmountAware {

	private BillingInvoice billingInvoice;

	private InvestmentAccount investmentAccount;

	/**
	 * Gross revenue
	 */
	private BigDecimal totalAmount;

	/**
	 * Broker Fee Amount
	 */
	private BigDecimal revenueShareExternalTotalAmount;

	/**
	 * Internal Revenue Share amount - here for information only, not tracked in payments
	 */
	private BigDecimal revenueShareInternalTotalAmount;

	/**
	 * Total amount allocated to payments for this account / invoice
	 */
	private BigDecimal paidAmount;

	/**
	 * Balance outstanding for the account / invoice
	 * Revenue Share Internal is NOT included here
	 */
	private BigDecimal unpaidAmount;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getRevenueGrossTotalAmount() {
		if (getBillingInvoice() != null && getBillingInvoice().getInvoiceType().isRevenue()) {
			return getTotalAmount();
		}
		return null;
	}


	@Override
	public BigDecimal getRevenueNetTotalAmount() {
		return MathUtils.add(getRevenueGrossTotalAmount(), getRevenueShareExternalTotalAmount());
	}


	@Override
	public BigDecimal getRevenueFinalTotalAmount() {
		return MathUtils.add(getRevenueNetTotalAmount(), getRevenueShareInternalTotalAmount());
	}


	@Override
	public BigDecimal getOtherTotalAmount() {
		if (getBillingInvoice() == null || !getBillingInvoice().getInvoiceType().isRevenue()) {
			return getTotalAmount();
		}
		return null;
	}


	public String getUnpaidLabel() {
		StringBuilder lbl = new StringBuilder();
		if (getBillingInvoice() != null) {
			lbl.append("#").append(getBillingInvoice().getId()).append(" ");
		}
		if (getInvestmentAccount() != null) {
			lbl.append(getInvestmentAccount().getLabel()).append(" ");
		}
		lbl.append("Unpaid Amount: ").append(CoreMathUtils.formatNumberMoney(getUnpaidAmount()));
		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BillingInvoice getBillingInvoice() {
		return this.billingInvoice;
	}


	public void setBillingInvoice(BillingInvoice billingInvoice) {
		this.billingInvoice = billingInvoice;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	@Override
	public BigDecimal getRevenueShareExternalTotalAmount() {
		return this.revenueShareExternalTotalAmount;
	}


	public void setRevenueShareExternalTotalAmount(BigDecimal revenueShareExternalTotalAmount) {
		this.revenueShareExternalTotalAmount = revenueShareExternalTotalAmount;
	}


	@Override
	public BigDecimal getRevenueShareInternalTotalAmount() {
		return this.revenueShareInternalTotalAmount;
	}


	public void setRevenueShareInternalTotalAmount(BigDecimal revenueShareInternalTotalAmount) {
		this.revenueShareInternalTotalAmount = revenueShareInternalTotalAmount;
	}


	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}


	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}


	public BigDecimal getUnpaidAmount() {
		return this.unpaidAmount;
	}


	public void setUnpaidAmount(BigDecimal unpaidAmount) {
		this.unpaidAmount = unpaidAmount;
	}
}
