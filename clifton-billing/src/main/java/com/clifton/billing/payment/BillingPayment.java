package com.clifton.billing.payment;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingPayment</code> tracks payments received by Clients.
 * These payments can then be allocated to invoices.
 *
 * @author manderson
 */
public class BillingPayment extends BaseEntity<Integer> implements LabeledObject {

	private BusinessCompany businessCompany;


	private Date paymentDate;

	/**
	 * Uses system list to define the payment types
	 * i.e. Check or Wire
	 */
	private String paymentType;

	/**
	 * The total amount of the payment
	 */
	private BigDecimal paymentAmount;

	/**
	 * System updated field that tracks how much of this payment has been allocated to Invoices
	 */
	private BigDecimal allocatedAmount;

	private String notes;

	/**
	 * We prevent deleting payments, they must be voided so we keep history of them
	 */
	private boolean voided;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String lbl = "# " + getId() + " ";
		if (getBusinessCompany() != null) {
			lbl += getBusinessCompany().getName() + " ";
		}
		lbl += DateUtils.fromDateShort(getPaymentDate());
		return lbl;
	}


	/**
	 * If the payment allocated to invoices - Fully Allocated, Partially Allocated, etc.
	 */
	public BillingPaymentStatuses getBillingPaymentStatus() {
		return BillingPaymentStatuses.validateBillingPaymentStatus(getPaymentAmount(), getAllocatedAmount());
	}


	public String getBillingPaymentStatusLabel() {
		if (getBillingPaymentStatus() != null) {
			return getBillingPaymentStatus().getLabel();
		}
		return null;
	}


	public BigDecimal getAvailableAmount() {
		if (isVoided()) {
			return BigDecimal.ZERO;
		}
		return MathUtils.subtract(getPaymentAmount(), getAllocatedAmount());
	}


	public String getUnallocatedLabel() {
		StringBuilder lbl = new StringBuilder();
		lbl.append(DateUtils.fromDateShort(getPaymentDate()));
		lbl.append(": ");
		if (getBusinessCompany() != null) {
			lbl.append(getBusinessCompany().getName());
			lbl.append(" ");
		}
		lbl.append(CoreMathUtils.formatNumberMoney(getPaymentAmount()));
		lbl.append(" Available: ").append(CoreMathUtils.formatNumberMoney(getAvailableAmount()));
		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public Date getPaymentDate() {
		return this.paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}


	public BigDecimal getPaymentAmount() {
		return this.paymentAmount;
	}


	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getNotes() {
		return this.notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public BigDecimal getAllocatedAmount() {
		return this.allocatedAmount;
	}


	public void setAllocatedAmount(BigDecimal allocatedAmount) {
		this.allocatedAmount = allocatedAmount;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public String getPaymentType() {
		return this.paymentType;
	}


	public boolean isVoided() {
		return this.voided;
	}


	public void setVoided(boolean voided) {
		this.voided = voided;
	}
}
