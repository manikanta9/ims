package com.clifton.billing.commission.schedule;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingCommissionScheduleAssignmentPeriod</code>
 * <p>
 * With the exception of "hold" periods, these are auto-generated based on the commission schedule periods and the assignments start date, as well as any hold periods.
 * When hold periods are modified for the assignment, then the entire period list is re-calculated
 *
 * @author manderson
 * @see com.clifton.billing.commission.schedule.populator.BillingCommissionScheduleAssignmentPeriodPopulatorImpl
 */
public class BillingCommissionScheduleAssignmentPeriod extends BaseEntity<Integer> {

	private BillingCommissionScheduleAssignment assignment;

	/**
	 * If null, then considered a "hold" period.
	 */
	private BillingCommissionSchedulePeriod period;

	private Date startDate;

	private Date endDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getDateLabel() {
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true);
	}


	public boolean isHoldPeriod() {
		return getPeriod() == null;
	}


	public BigDecimal getPeriodPercentage() {
		if (isHoldPeriod()) {
			return BigDecimal.ZERO;
		}
		return getPeriod().getPeriodPercentage();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingCommissionScheduleAssignment getAssignment() {
		return this.assignment;
	}


	public void setAssignment(BillingCommissionScheduleAssignment assignment) {
		this.assignment = assignment;
	}


	public BillingCommissionSchedulePeriod getPeriod() {
		return this.period;
	}


	public void setPeriod(BillingCommissionSchedulePeriod period) {
		this.period = period;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
