package com.clifton.billing.commission.schedule;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingCommissionScheduleAssignment</code> assigns a {@link BillingCommissionSchedule} to an {@link InvestmentAccount} and {@link BusinessContact} recipient of those commissions
 *
 * @author manderson
 */
public class BillingCommissionScheduleAssignment extends ManyToManyEntity<BillingCommissionSchedule, InvestmentAccount, Integer> implements WorkflowAware, DocumentFileCountAware {

	private WorkflowState workflowState;

	private WorkflowStatus workflowStatus;

	private Date startDate;

	/**
	 * Auto-calculated based on the given start date and the periods associated with the schedule
	 *
	 * @see com.clifton.billing.commission.schedule.populator.BillingCommissionScheduleAssignmentPeriodPopulatorImpl
	 */
	private Date endDate;

	/**
	 * If an account terminates PRIOR to calculated end date, they are moved to separate Terminated state
	 * and this date is required.
	 */
	private Date terminateDate;

	/**
	 * Amount subtracted from Invoice Amount before applying commission schedule
	 */
	private BigDecimal commissionDiscount;

	/**
	 * Person assigned to receive the commission
	 */
	private BusinessContact recipientContact;

	/**
	 * Optional information about the assignment
	 */
	private String notes;

	/**
	 * Total number of attachments tied to the assignment
	 */
	private Short documentFileCount;


	private List<BillingCommissionScheduleAssignmentPeriod> periodList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getCoalesceTerminateEndDate(), false);
	}


	public Date getCoalesceTerminateEndDate() {
		return ObjectUtils.coalesce(getTerminateDate(), getEndDate());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}


	public BigDecimal getCommissionDiscount() {
		return this.commissionDiscount;
	}


	public void setCommissionDiscount(BigDecimal commissionDiscount) {
		this.commissionDiscount = commissionDiscount;
	}


	public BusinessContact getRecipientContact() {
		return this.recipientContact;
	}


	public void setRecipientContact(BusinessContact recipientContact) {
		this.recipientContact = recipientContact;
	}


	public String getNotes() {
		return this.notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public List<BillingCommissionScheduleAssignmentPeriod> getPeriodList() {
		return this.periodList;
	}


	public void setPeriodList(List<BillingCommissionScheduleAssignmentPeriod> periodList) {
		this.periodList = periodList;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}
}
