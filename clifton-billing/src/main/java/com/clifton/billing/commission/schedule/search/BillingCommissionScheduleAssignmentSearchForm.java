package com.clifton.billing.commission.schedule.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.dataaccess.search.hibernate.expression.CoalesceExpression;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.search.BaseWorkflowAwareDateRangeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class BillingCommissionScheduleAssignmentSearchForm extends BaseWorkflowAwareDateRangeSearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer commissionScheduleId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "name")
	private String commissionScheduleName;

	@SearchField(searchField = "referenceTwo.id")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "businessClient.id")
	private Integer clientId;

	@SearchField(searchFieldPath = "referenceTwo.businessClient", searchField = "name,legalName")
	private String clientLabel;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String investmentAccountName;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account
	@SearchField(searchField = "referenceTwo.businessService.id,referenceTwo.businessService.parent.id,referenceTwo.businessService.parent.parent.id,referenceTwo.businessService.parent.parent.parent.id", leftJoin = true, sortField = "referenceTwo.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "serviceProcessingType.id", sortField = "serviceProcessingType.name")
	private Short businessServiceProcessingTypeId;

	@SearchField
	private BigDecimal commissionDiscount;

	@SearchField(searchField = "recipientContact.id")
	private Integer recipientContactId;

	@SearchField
	private String notes;

	@SearchField
	private Date terminateDate;

	@SearchField(searchField = "terminateDate,endDate", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Date coalesceTerminateEndDate;

	@SearchField
	private Short documentFileCount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Customized DateRateSearchForm implementation to include coalesce(terminateDate,endDate) logic
	 */
	@Override
	public void configureDateRangeSearchForm(Criteria criteria) {
		SearchFormUtils.convertDateRangeSearchFormRestrictionToFields(this);

		String[] endDateCoalesce = new String[]{"terminateDate", "endDate"};

		if (BooleanUtils.isTrue(getInactive())) {
			SimpleExpression startDateFilter = Restrictions.ge("startDate", new Date());
			CoalesceExpression endDateFilter = new CoalesceExpression("<", endDateCoalesce, new Date());
			criteria.add(Restrictions.or(startDateFilter, endDateFilter));
		}

		// apply custom filters, if set
		if (getActiveOnDate() != null) {
			// NOTE: START/END DATES SHOULD NEVER BE NULL - SO NOT INCLUDED IN LOGIC HERE TOO AVOID ADDITIONAL COMPLICATIONS IN FILTER SETUP
			SimpleExpression startDateFilter = Restrictions.le("startDate", getActiveOnDate());
			CoalesceExpression endDateFilter = new CoalesceExpression(">", endDateCoalesce, getActiveOnDate());
			criteria.add(Restrictions.and(startDateFilter, endDateFilter));
		}
		if (getNotActiveOnDate() != null) {
			SimpleExpression startDateFilter = Restrictions.ge("startDate", getNotActiveOnDate());
			CoalesceExpression endDateFilter = new CoalesceExpression("<", endDateCoalesce, getNotActiveOnDate());
			criteria.add(Restrictions.or(startDateFilter, endDateFilter));
		}
		if (getActiveOnDateRangeStartDate() != null) {
			ValidationUtils.assertNotNull(getActiveOnDateRangeEndDate(), "To use active on date range, both start and end date are required.");
			SimpleExpression startDateFilter = Restrictions.le("startDate", getActiveOnDateRangeEndDate());
			CoalesceExpression endDateFilter = new CoalesceExpression(">", endDateCoalesce, getActiveOnDateRangeStartDate());
			criteria.add(Restrictions.and(startDateFilter, endDateFilter));
		}
		if (getNotActiveOnDateRangeStartDate() != null) {
			ValidationUtils.assertNotNull(getNotActiveOnDateRangeEndDate(), "To use not active on date range, both start and end date are required.");
			criteria.add(ActiveExpressionForDates.forActiveOnDateRange(false, getNotActiveOnDateRangeStartDate(), getNotActiveOnDateRangeEndDate()));
			SimpleExpression startDateFilter = Restrictions.ge("startDate", getNotActiveOnDateRangeEndDate());
			CoalesceExpression endDateFilter = new CoalesceExpression("<", endDateCoalesce, getNotActiveOnDateRangeStartDate());
			criteria.add(Restrictions.or(startDateFilter, endDateFilter));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getCommissionScheduleId() {
		return this.commissionScheduleId;
	}


	public void setCommissionScheduleId(Integer commissionScheduleId) {
		this.commissionScheduleId = commissionScheduleId;
	}


	public String getCommissionScheduleName() {
		return this.commissionScheduleName;
	}


	public void setCommissionScheduleName(String commissionScheduleName) {
		this.commissionScheduleName = commissionScheduleName;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public String getClientLabel() {
		return this.clientLabel;
	}


	public void setClientLabel(String clientLabel) {
		this.clientLabel = clientLabel;
	}


	public String getInvestmentAccountName() {
		return this.investmentAccountName;
	}


	public void setInvestmentAccountName(String investmentAccountName) {
		this.investmentAccountName = investmentAccountName;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public BigDecimal getCommissionDiscount() {
		return this.commissionDiscount;
	}


	public void setCommissionDiscount(BigDecimal commissionDiscount) {
		this.commissionDiscount = commissionDiscount;
	}


	public Integer getRecipientContactId() {
		return this.recipientContactId;
	}


	public void setRecipientContactId(Integer recipientContactId) {
		this.recipientContactId = recipientContactId;
	}


	public String getNotes() {
		return this.notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}


	public Date getCoalesceTerminateEndDate() {
		return this.coalesceTerminateEndDate;
	}


	public void setCoalesceTerminateEndDate(Date coalesceTerminateEndDate) {
		this.coalesceTerminateEndDate = coalesceTerminateEndDate;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Short getBusinessServiceProcessingTypeId() {
		return this.businessServiceProcessingTypeId;
	}


	public void setBusinessServiceProcessingTypeId(Short businessServiceProcessingTypeId) {
		this.businessServiceProcessingTypeId = businessServiceProcessingTypeId;
	}
}
