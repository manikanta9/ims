package com.clifton.billing.commission.schedule;


import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;


/**
 * The <code>BillingCommissionSchedulePeriod</code> defines the commission rate for a given period of time for a schedule
 * i.e. Year 1 = 1%, Year 2 = 0.5%, etc.
 *
 * @author manderson
 */
public class BillingCommissionSchedulePeriod extends BaseEntity<Integer> {

	private BillingCommissionSchedule schedule;

	/**
	 * The unit of measure for the period - i.e. Years, Days, Months
	 */
	private BillingCommissionSchedulePeriodTypes periodType;

	/**
	 * The number of days, months, years for this period as defined by the period type
	 */
	private Short periodLength;

	private Short periodOrder;

	/**
	 * The rate for this period, i.e. Year 1 = 1%, Year 2 = 0.5%
	 */
	private BigDecimal periodPercentage;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingCommissionSchedule getSchedule() {
		return this.schedule;
	}


	public void setSchedule(BillingCommissionSchedule schedule) {
		this.schedule = schedule;
	}


	public BillingCommissionSchedulePeriodTypes getPeriodType() {
		return this.periodType;
	}


	public void setPeriodType(BillingCommissionSchedulePeriodTypes periodType) {
		this.periodType = periodType;
	}


	public Short getPeriodLength() {
		return this.periodLength;
	}


	public void setPeriodLength(Short periodLength) {
		this.periodLength = periodLength;
	}


	public Short getPeriodOrder() {
		return this.periodOrder;
	}


	public void setPeriodOrder(Short periodOrder) {
		this.periodOrder = periodOrder;
	}


	public BigDecimal getPeriodPercentage() {
		return this.periodPercentage;
	}


	public void setPeriodPercentage(BigDecimal periodPercentage) {
		this.periodPercentage = periodPercentage;
	}
}
