package com.clifton.billing.commission.schedule;


import com.clifton.billing.commission.schedule.populator.BillingCommissionScheduleAssignmentPeriodPopulator;
import com.clifton.billing.commission.schedule.search.BillingCommissionScheduleAssignmentSearchForm;
import com.clifton.billing.commission.schedule.search.BillingCommissionScheduleSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>BillingCommissionScheduleServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class BillingCommissionScheduleServiceImpl implements BillingCommissionScheduleService {

	private AdvancedUpdatableDAO<BillingCommissionSchedule, Criteria> billingCommissionScheduleDAO;
	private UpdatableDAO<BillingCommissionSchedulePeriod> billingCommissionSchedulePeriodDAO;

	private AdvancedUpdatableDAO<BillingCommissionScheduleAssignment, Criteria> billingCommissionScheduleAssignmentDAO;
	private UpdatableDAO<BillingCommissionScheduleAssignmentPeriod> billingCommissionScheduleAssignmentPeriodDAO;

	private BillingCommissionScheduleAssignmentPeriodPopulator billingCommissionScheduleAssignmentPeriodPopulator;

	private InvestmentAccountService investmentAccountService;
	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	///////////           Billing Commission Schedule Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingCommissionSchedule getBillingCommissionSchedule(int id) {
		BillingCommissionSchedule bean = getBillingCommissionScheduleDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setPeriodList(getBillingCommissionSchedulePeriodListForSchedule(id));
		}
		return bean;
	}


	@Override
	public List<BillingCommissionSchedule> getBillingCommissionScheduleList(BillingCommissionScheduleSearchForm searchForm) {
		return getBillingCommissionScheduleDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	@Transactional
	public BillingCommissionSchedule saveBillingCommissionSchedule(BillingCommissionSchedule bean) {
		List<BillingCommissionSchedulePeriod> originalList = (bean.isNewBean() ? null : getBillingCommissionSchedulePeriodListForSchedule(bean.getId()));
		List<BillingCommissionSchedulePeriod> periodList = bean.getPeriodList();
		ValidationUtils.assertNotEmpty(periodList, "At least one commission schedule period is required.");
		for (BillingCommissionSchedulePeriod period : periodList) {
			period.setSchedule(bean);
		}
		bean = getBillingCommissionScheduleDAO().save(bean);
		getBillingCommissionSchedulePeriodDAO().saveList(periodList, originalList);
		bean.setPeriodList(periodList);
		return bean;
	}


	@Override
	public void deleteBillingCommissionSchedule(int id) {
		getBillingCommissionSchedulePeriodDAO().deleteList(getBillingCommissionSchedulePeriodListForSchedule(id));
		getBillingCommissionScheduleDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////           Billing Commission Schedule Period Methods          ////////
	////////////////////////////////////////////////////////////////////////////////


	private List<BillingCommissionSchedulePeriod> getBillingCommissionSchedulePeriodListForSchedule(int scheduleId) {
		return getBillingCommissionSchedulePeriodDAO().findByField("schedule.id", scheduleId);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////          Billing Commission Schedule Assignment Methods         ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingCommissionScheduleAssignment getBillingCommissionScheduleAssignment(int id) {
		BillingCommissionScheduleAssignment bean = getBillingCommissionScheduleAssignmentDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setPeriodList(getBillingCommissionScheduleAssignmentPeriodListForAssignment(id));
		}
		return bean;
	}


	@Override
	public List<BillingCommissionScheduleAssignment> getBillingCommissionScheduleAssignmentList(BillingCommissionScheduleAssignmentSearchForm searchForm) {
		return getBillingCommissionScheduleAssignmentDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	@Transactional
	public BillingCommissionScheduleAssignment saveBillingCommissionScheduleAssignment(BillingCommissionScheduleAssignment bean) {
		ValidationUtils.assertNotNull(bean.getReferenceOne(), "Commission Schedule selection is required.");
		ValidationUtils.assertNotNull(bean.getStartDate(), "Start Date is required.");

		getBillingCommissionScheduleAssignmentPeriodPopulator().populateBillingCommissionScheduleAssignmentPeriodList(bean, getBillingCommissionSchedule(bean.getReferenceOne().getId()));

		List<BillingCommissionScheduleAssignmentPeriod> originalList = (bean.isNewBean() ? null : getBillingCommissionScheduleAssignmentPeriodListForAssignment(bean.getId()));
		List<BillingCommissionScheduleAssignmentPeriod> periodList = bean.getPeriodList();
		ValidationUtils.assertNotEmpty(periodList, "At least one commission schedule period is required.");
		for (BillingCommissionScheduleAssignmentPeriod period : periodList) {
			period.setAssignment(bean);
		}

		// Note: Whether or not this field can be entered is defined via Workflow State/Transition Conditions.
		// This just validates if it is entered that it is before calculated end date.  Otherwise it's not considered a termination
		if (bean.getTerminateDate() != null && bean.getEndDate() != null) {
			ValidationUtils.assertFalse(bean.getEndDate().before(bean.getTerminateDate()), "Terminate date must be before calculated end date " + DateUtils.fromDateShort(bean.getEndDate()) + ". Otherwise, it is not considered a termination of the commission schedule assignment.", "terminateDate");
		}

		bean = getBillingCommissionScheduleAssignmentDAO().save(bean);
		getBillingCommissionScheduleAssignmentPeriodDAO().saveList(periodList, originalList);
		bean.setPeriodList(periodList);
		return bean;
	}


	@Override
	@Transactional
	public BillingCommissionScheduleAssignment saveBillingCommissionScheduleAssignmentBulk(BillingCommissionScheduleAssignment assignment, Integer[] additionalClientAccountIds) {
		// Save the assignment for each additional account in the list
		if (additionalClientAccountIds != null && additionalClientAccountIds.length > 0) {
			for (Integer clientAccountId : additionalClientAccountIds) {
				BillingCommissionScheduleAssignment copyAssignment = BeanUtils.cloneBean(assignment, false, false);
				copyAssignment.setReferenceTwo(getInvestmentAccountService().getInvestmentAccount(clientAccountId));
				saveBillingCommissionScheduleAssignment(copyAssignment);
			}
		}
		// Save the main assignment
		return saveBillingCommissionScheduleAssignment(assignment);
	}


	@Override
	public void deleteBillingCommissionScheduleAssignment(int id) {
		getBillingCommissionScheduleAssignmentPeriodDAO().deleteList(getBillingCommissionScheduleAssignmentPeriodListForAssignment(id));
		getBillingCommissionScheduleAssignmentDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////   Billing Commission Schedule Assignment Period Methods    ////////
	////////////////////////////////////////////////////////////////////////////


	private List<BillingCommissionScheduleAssignmentPeriod> getBillingCommissionScheduleAssignmentPeriodListForAssignment(int assignmentId) {
		return getBillingCommissionScheduleAssignmentPeriodDAO().findByField("assignment.id", assignmentId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Getter & Setter Methods                     ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BillingCommissionSchedule, Criteria> getBillingCommissionScheduleDAO() {
		return this.billingCommissionScheduleDAO;
	}


	public void setBillingCommissionScheduleDAO(AdvancedUpdatableDAO<BillingCommissionSchedule, Criteria> billingCommissionScheduleDAO) {
		this.billingCommissionScheduleDAO = billingCommissionScheduleDAO;
	}


	public UpdatableDAO<BillingCommissionSchedulePeriod> getBillingCommissionSchedulePeriodDAO() {
		return this.billingCommissionSchedulePeriodDAO;
	}


	public void setBillingCommissionSchedulePeriodDAO(UpdatableDAO<BillingCommissionSchedulePeriod> billingCommissionSchedulePeriodDAO) {
		this.billingCommissionSchedulePeriodDAO = billingCommissionSchedulePeriodDAO;
	}


	public AdvancedUpdatableDAO<BillingCommissionScheduleAssignment, Criteria> getBillingCommissionScheduleAssignmentDAO() {
		return this.billingCommissionScheduleAssignmentDAO;
	}


	public void setBillingCommissionScheduleAssignmentDAO(AdvancedUpdatableDAO<BillingCommissionScheduleAssignment, Criteria> billingCommissionScheduleAssignmentDAO) {
		this.billingCommissionScheduleAssignmentDAO = billingCommissionScheduleAssignmentDAO;
	}


	public BillingCommissionScheduleAssignmentPeriodPopulator getBillingCommissionScheduleAssignmentPeriodPopulator() {
		return this.billingCommissionScheduleAssignmentPeriodPopulator;
	}


	public void setBillingCommissionScheduleAssignmentPeriodPopulator(BillingCommissionScheduleAssignmentPeriodPopulator billingCommissionScheduleAssignmentPeriodPopulator) {
		this.billingCommissionScheduleAssignmentPeriodPopulator = billingCommissionScheduleAssignmentPeriodPopulator;
	}


	public UpdatableDAO<BillingCommissionScheduleAssignmentPeriod> getBillingCommissionScheduleAssignmentPeriodDAO() {
		return this.billingCommissionScheduleAssignmentPeriodDAO;
	}


	public void setBillingCommissionScheduleAssignmentPeriodDAO(UpdatableDAO<BillingCommissionScheduleAssignmentPeriod> billingCommissionScheduleAssignmentPeriodDAO) {
		this.billingCommissionScheduleAssignmentPeriodDAO = billingCommissionScheduleAssignmentPeriodDAO;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
