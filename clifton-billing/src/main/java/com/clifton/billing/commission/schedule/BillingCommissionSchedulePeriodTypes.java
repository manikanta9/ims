package com.clifton.billing.commission.schedule;


/**
 * The <code>BillingCommissionSchedulePeriodTypes</code> defines the unit of measure for the schedule period.
 *
 * @author manderson
 */
public enum BillingCommissionSchedulePeriodTypes {

	DAYS, //
	MONTHS, //
	YEARS //
}
