package com.clifton.billing.commission.schedule.populator;


import com.clifton.billing.commission.schedule.BillingCommissionSchedule;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignment;


/**
 * The <code>BillingCommissionScheduleAssignmentPeriodPopulator</code> takes a
 * {@link BillingCommissionScheduleAssignment} and populates the periods on the assignment
 * taking into account any hold periods entered by the user
 *
 * @author manderson
 */
public interface BillingCommissionScheduleAssignmentPeriodPopulator {

	/**
	 * Populates the periods on the assignment taking into account any hold periods entered by the user.  Also updates the end date on the assignment to reflect period changes
	 *
	 * @param assignment - Assignment to recalculate/populate periods for
	 * @param schedule   - Fully populated schedule to use to populate periods on the assignment
	 */
	public void populateBillingCommissionScheduleAssignmentPeriodList(BillingCommissionScheduleAssignment assignment, BillingCommissionSchedule schedule);
}
