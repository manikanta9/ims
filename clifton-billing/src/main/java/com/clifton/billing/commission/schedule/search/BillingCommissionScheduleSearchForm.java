package com.clifton.billing.commission.schedule.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;


/**
 * @author manderson
 */
public class BillingCommissionScheduleSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
