package com.clifton.billing.commission.schedule.search;

import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignment;
import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * The <code>BillingCommissionInvestmentAccountSearchFormRestrictionConfigurer</code> class allows to define cross-project form restriction
 * for accounts missing a commission schedule assignment
 * <p/>
 *
 * @author manderson
 */
@Component
public class BillingCommissionInvestmentAccountSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<?> getSearchFormClass() {
		return InvestmentAccountSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "missingBillingCommissionScheduleAssignment";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			//The list of accounts WHERE NOT EXIST a billing commission assignment
			DetachedCriteria ne = DetachedCriteria.forClass(BillingCommissionScheduleAssignment.class, "billingCommissionScheduleAssignment");
			ne.setProjection(Projections.property("id"));
			ne.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.notExists(ne));
		}
	}
}
