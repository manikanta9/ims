package com.clifton.billing.commission.schedule.populator;


import com.clifton.billing.commission.schedule.BillingCommissionSchedule;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignment;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignmentPeriod;
import com.clifton.billing.commission.schedule.BillingCommissionSchedulePeriod;
import com.clifton.billing.commission.schedule.BillingCommissionSchedulePeriodTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class BillingCommissionScheduleAssignmentPeriodPopulatorImpl implements BillingCommissionScheduleAssignmentPeriodPopulator {

	@Override
	public void populateBillingCommissionScheduleAssignmentPeriodList(BillingCommissionScheduleAssignment assignment, BillingCommissionSchedule schedule) {
		ValidationUtils.assertNotNull(assignment.getStartDate(), "Assignment Start Date is missing.");
		ValidationUtils.assertNotNull(schedule, "Schedule is missing");
		ValidationUtils.assertNotEmpty(schedule.getPeriodList(), "Selected Schedule [" + schedule.getName() + "] is missing periods.");

		List<BillingCommissionSchedulePeriod> periodList = BeanUtils.sortWithFunction(schedule.getPeriodList(), BillingCommissionSchedulePeriod::getPeriodOrder, true);
		List<BillingCommissionScheduleAssignmentPeriod> existingPeriodList = BeanUtils.sortWithFunction(BeanUtils.filter(assignment.getPeriodList(), BillingCommissionScheduleAssignmentPeriod::isHoldPeriod, false), BillingCommissionScheduleAssignmentPeriod::getStartDate, true);
		List<BillingCommissionScheduleAssignmentPeriod> holdPeriodList = BeanUtils.sortWithFunction(BeanUtils.filter(assignment.getPeriodList(), BillingCommissionScheduleAssignmentPeriod::isHoldPeriod, true), BillingCommissionScheduleAssignmentPeriod::getStartDate, true);
		assignment.setEndDate(null);
		// This is actually validated twice - once before calculating and once after to make sure hold period dates are OK with fully calculated dates
		validateHoldPeriodList(assignment, holdPeriodList);
		List<BillingCommissionScheduleAssignmentPeriod> assignmentPeriodList = new ArrayList<>();

		Date startDate = assignment.getStartDate();
		for (BillingCommissionSchedulePeriod period : periodList) {
			Date endDate = getCalculatedEndDateForPeriod(period, startDate);

			// See if We have any hold periods during this time:
			for (BillingCommissionScheduleAssignmentPeriod holdPeriod : CollectionUtils.getIterable(holdPeriodList)) {
				if (DateUtils.isOverlapInDates(holdPeriod.getStartDate(), holdPeriod.getEndDate(), startDate, endDate)) {
					if (DateUtils.compare(holdPeriod.getStartDate(), startDate, false) != 0) {
						addBillingCommissionScheduleAssignmentPeriod(assignmentPeriodList, period, startDate, DateUtils.addDays(holdPeriod.getStartDate(), -1));
					}
					endDate = DateUtils.addDays(endDate, DateUtils.getDaysDifferenceInclusive(holdPeriod.getEndDate(), holdPeriod.getStartDate()));
					startDate = DateUtils.addDays(holdPeriod.getEndDate(), 1);
				}
			}

			addBillingCommissionScheduleAssignmentPeriod(assignmentPeriodList, period, startDate, endDate);
			startDate = DateUtils.addDays(endDate, 1);
		}
		mergePeriodLists(assignmentPeriodList, existingPeriodList);
		assignment.setEndDate(DateUtils.addDays(startDate, -1));
		if (holdPeriodList != null) {
			validateHoldPeriodList(assignment, holdPeriodList);
			assignmentPeriodList.addAll(holdPeriodList);
		}
		assignment.setPeriodList(assignmentPeriodList);
	}


	private void validateHoldPeriodList(BillingCommissionScheduleAssignment assignment, List<BillingCommissionScheduleAssignmentPeriod> holdPeriodList) {
		int size = CollectionUtils.getSize(holdPeriodList);
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				BillingCommissionScheduleAssignmentPeriod period = holdPeriodList.get(i);
				if (DateUtils.compare(assignment.getStartDate(), period.getStartDate(), false) > 0) {
					throw new ValidationException("Holding Periods cannot start before account assignment start date of (" + DateUtils.fromDateShort(assignment.getStartDate()) + ").");
				}
				if (assignment.getEndDate() != null && DateUtils.compare(assignment.getEndDate(), period.getStartDate(), false) < 0) {
					throw new ValidationException("Holding Periods cannot start after account assignment end date (currently calculated as: " + DateUtils.fromDateShort(assignment.getEndDate()) + ").");
				}

				for (int j = 0; j < size; j++) {
					if (i != j) {
						BillingCommissionScheduleAssignmentPeriod secondPeriod = holdPeriodList.get(j);
						if (DateUtils.isOverlapInDates(period.getStartDate(), period.getEndDate(), secondPeriod.getStartDate(), secondPeriod.getEndDate())) {
							throw new ValidationException("Hold Periods cannot overlap: " + period.getDateLabel() + ", and " + secondPeriod.getDateLabel());
						}
					}
				}
			}
		}
	}


	private void addBillingCommissionScheduleAssignmentPeriod(List<BillingCommissionScheduleAssignmentPeriod> assignmentPeriodList, BillingCommissionSchedulePeriod period, Date startDate, Date endDate) {
		BillingCommissionScheduleAssignmentPeriod assignmentPeriod = new BillingCommissionScheduleAssignmentPeriod();
		assignmentPeriod.setPeriod(period);
		assignmentPeriod.setStartDate(startDate);
		assignmentPeriod.setEndDate(endDate);
		assignmentPeriodList.add(assignmentPeriod);
	}


	private void mergePeriodLists(List<BillingCommissionScheduleAssignmentPeriod> newList, List<BillingCommissionScheduleAssignmentPeriod> existingList) {
		// We merge the list to keep IDs from existing list so that we don't constantly delete and create new entries
		int existingListSize = CollectionUtils.getSize(existingList);
		for (int i = 0; i < CollectionUtils.getSize(newList); i++) {
			if (i < existingListSize) {
				BillingCommissionScheduleAssignmentPeriod period = newList.get(i);
				BillingCommissionScheduleAssignmentPeriod existingPeriod = existingList.get(i);
				period.setId(existingPeriod.getId());
			}
			else {
				// No more existing records to copy ids from
				return;
			}
		}
	}


	private Date getCalculatedEndDateForPeriod(BillingCommissionSchedulePeriod period, Date startDate) {
		Date endDate = startDate;
		if (BillingCommissionSchedulePeriodTypes.YEARS == period.getPeriodType()) {
			endDate = DateUtils.addYears(endDate, period.getPeriodLength());
		}
		else if (BillingCommissionSchedulePeriodTypes.MONTHS == period.getPeriodType()) {
			endDate = DateUtils.addMonths(endDate, period.getPeriodLength());
		}
		else if (BillingCommissionSchedulePeriodTypes.DAYS == period.getPeriodType()) {
			endDate = DateUtils.addDays(endDate, period.getPeriodLength());
		}
		return DateUtils.addDays(endDate, -1);
	}
}
