package com.clifton.billing.commission.schedule;


import com.clifton.workflow.BasedNamedWorkflowAwareEntity;

import java.util.List;


/**
 * The <code>BillingCommissionSchedule</code> defines a particular commission schedule with a list of periods with rates to apply
 *
 * @author manderson
 */
public class BillingCommissionSchedule extends BasedNamedWorkflowAwareEntity {

	private List<BillingCommissionSchedulePeriod> periodList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<BillingCommissionSchedulePeriod> getPeriodList() {
		return this.periodList;
	}


	public void setPeriodList(List<BillingCommissionSchedulePeriod> periodList) {
		this.periodList = periodList;
	}
}
