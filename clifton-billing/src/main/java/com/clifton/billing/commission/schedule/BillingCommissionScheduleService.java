package com.clifton.billing.commission.schedule;


import com.clifton.billing.commission.schedule.search.BillingCommissionScheduleAssignmentSearchForm;
import com.clifton.billing.commission.schedule.search.BillingCommissionScheduleSearchForm;

import java.util.List;


/**
 * The <code>BillingCommissionScheduleService</code> ...
 *
 * @author manderson
 */
public interface BillingCommissionScheduleService {

	////////////////////////////////////////////////////////////////////////////////
	///////////           Billing Commission Schedule Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Also returns the list of periods on the schedule
	 */
	public BillingCommissionSchedule getBillingCommissionSchedule(int id);


	public List<BillingCommissionSchedule> getBillingCommissionScheduleList(BillingCommissionScheduleSearchForm searchForm);


	/**
	 * Also saves the list of periods on the schedule
	 */
	public BillingCommissionSchedule saveBillingCommissionSchedule(BillingCommissionSchedule bean);


	public void deleteBillingCommissionSchedule(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////          Billing Commission Schedule Assignment Methods         ///////
	////////////////////////////////////////////////////////////////////////////////


	public BillingCommissionScheduleAssignment getBillingCommissionScheduleAssignment(int id);


	public List<BillingCommissionScheduleAssignment> getBillingCommissionScheduleAssignmentList(BillingCommissionScheduleAssignmentSearchForm searchForm);


	public BillingCommissionScheduleAssignment saveBillingCommissionScheduleAssignment(BillingCommissionScheduleAssignment bean);


	/**
	 * Creates the assignment for the assignment's main account, and creates copies of that assignment for each account in the additionalClientAccountIds
	 */
	public BillingCommissionScheduleAssignment saveBillingCommissionScheduleAssignmentBulk(BillingCommissionScheduleAssignment assignment, Integer[] additionalClientAccountIds);


	public void deleteBillingCommissionScheduleAssignment(int id);
}
