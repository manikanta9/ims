package com.clifton.billing.definition;

import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>BillingDefinitionServiceTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BillingDefinitionServiceImplTests {

	public static final Integer BILLING_DEFINITION_MCKNIGHT = 1;
	public static final Integer BILLING_DEFINITION_ACCOUNT_MCKNIGHT = 1;
	public static final Integer BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE = 1;

	public static final Integer BILLING_DEFINITION_GLOBAL_ENDOWMENT = 2;

	public static final Integer BILLING_DEFINITION_FAIRFAX_000 = 10;
	public static final Integer BILLING_DEFINITION_ACCOUNT_FAIRFAX_000 = 10;
	public static final Integer BILLING_SCHEDULE_FAIRFAX_000_RETAINER = 11;
	public static final Integer BILLING_DEFINITION_FAIRFAX_500 = 11;
	public static final Integer BILLING_DEFINITION_FAIRFAX_400 = 12;
	public static final Integer BILLING_SCHEDULE_FAIRFAX_SHARED = 10;

	public static final Integer BILLING_DEFINITION_ACUMENT = 20;
	public static final Integer BILLING_SCHEDULE_ACUMENT_TIERED = 20;

	public static final Integer BILLING_DEFINITION_KIMBERLY_CLARK_359100 = 30;
	public static final Integer BILLING_SCHEDULE_KIMBERLY_CLARK_PERCENTAGE_FEE = 30;

	public static final Integer BILLING_DEFINITION_NEW_ORLEANS_FF_454000 = 40;

	public static final Integer BILLING_DEFINITION_CONTINENTAL_125300 = 50;
	public static final Integer BILLING_SCHEDULE_CONTINENTAL_DISCOUNT = 52;

	public static final Integer BILLING_DEFINITION_WILDER_713450 = 60;

	public static final Integer BILLING_DEFINITION_NC_BAPTIST = 70;
	public static final Integer BILLING_SCHEDULE_NC_BAPTIST_ANNUAL_MINIMUM = 72;

	public static final Integer BILLING_DEFINITION_MCGOWAN = 80;

	public static final Integer BILLING_DEFINITION_DELTA_MASTER_159900 = 90;
	public static final Integer BILLING_DEFINITION_DELTA_PILOTS_159700 = 91;
	public static final Integer BILLING_DEFINITION_NORTHWEST_159800 = 92;

	public static final Integer BILLING_DEFINITION_DORIS_DUKE = 100;

	public static final Integer BILLING_DEFINITION_DUKE_ENERGY = 110;
	public static final Integer BILLING_SCHEDULE_DUKE_ENERGY_PIOS_TIERED = 110;

	public static final Integer BILLING_DEFINITION_TARGET = 120;

	public static final Integer BILLING_DEFINITION_SMITH_FAMILY = 130;
	public static final Integer BILLING_DEFINITION_SMITH_SUSAN = 131;

	private static final Short SCHEDULE_TYPE_DISCOUNT = 30;

	public static final Integer BILLING_DEFINITION_MERRILL = 150;


	////////////////////////////////////////////////////////////////////////////////

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetBillingDefinition() {
		// BILLING_DEFINITION_MCKNIGHT - One Account Assignment, One NOT Shared Schedule
		BillingDefinition def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_MCKNIGHT);
		Assertions.assertNotNull(def, "McKnight Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "McKnight Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "McKnight Billing Definition should have one Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "McKnight Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_GLOBAL_ENDOWMENT - One Account Assignment, One NOT Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_GLOBAL_ENDOWMENT);
		Assertions.assertNotNull(def, "Global Endowment Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Global Endowment Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "Global Endowment Billing Definition should have one Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Global Endowment Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_FAIRFAX_000 - One Account Assignment, One Shared & One Not Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_FAIRFAX_000);
		Assertions.assertNotNull(def, "Fairfax 000 Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Fairfax 000 Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "Fairfax 000 Billing Definition should have one Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Fairfax 000 Billing Definition should one Shared schedule assigned to it.");

		// BILLING_DEFINITION_FAIRFAX_500 - Two Account Assignments, One Shared & One Not Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_FAIRFAX_500);
		Assertions.assertNotNull(def, "Fairfax 500 Billing Definition should be defined.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getInvestmentAccountList()), "Fairfax 500 Billing Definition should have two BillingDefinitionInvestmentAccounts assigned to it.");
		Assertions.assertEquals(3, CollectionUtils.getSize(def.getBillingScheduleList()), "Fairfax 500 Billing Definition should have 3 Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Fairfax 500 Billing Definition should one Shared schedule assigned to it.");

		// BILLING_DEFINITION_FAIRFAX_400 - One Account Assignment, One Shared & One Not Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_FAIRFAX_400);
		Assertions.assertNotNull(def, "Fairfax 400 Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Fairfax 400 Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "Fairfax 400 Billing Definition should have one Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Fairfax 400 Billing Definition should one Shared schedule assigned to it.");

		// BILLING_DEFINITION_ACUMENT - One Account Assignment, Two NOT Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_ACUMENT);
		Assertions.assertNotNull(def, "Acument Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Acument Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getBillingScheduleList()), "Acument Billing Definition should have 2 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Acument Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_KIMBERLY_CLARK_359100 - One Account Assignment, Two Not Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_KIMBERLY_CLARK_359100);
		Assertions.assertNotNull(def, "KC 359100 Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "KC 359100 Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getBillingScheduleList()), "KC 359100 Billing Definition should have 2 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "KC 359100 Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_NEW_ORLEANS_FF_454000 - One Account Assignment, Two Not Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_NEW_ORLEANS_FF_454000);
		Assertions.assertNotNull(def, "NO FF 454000 Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "NO FF 454000 Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getBillingScheduleList()), "NO FF 454000 Billing Definition should have 2 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "NO FF 454000 Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_CONTINENTAL_125300 - One Account Assignment, Four Not Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_CONTINENTAL_125300);
		Assertions.assertNotNull(def, "Continental 125300 Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Continental 125300 Billing Definition should have one BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(4, CollectionUtils.getSize(def.getBillingScheduleList()), "Continental 125300 Billing Definition should have 4 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Continental 125300 Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_WILDER_713450 - One Account Assignment, One Not Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_WILDER_713450);
		Assertions.assertNotNull(def, "Wilder 713450 Billing Definition should be defined.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getInvestmentAccountList()), "Wilder 713450 Billing Definition should have 2 BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "Wilder 713450 Billing Definition should have 1 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Wilder 713450 Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_NC_BAPTIST - TWO Account Assignments, Two Not Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_NC_BAPTIST);
		Assertions.assertNotNull(def, "North Carolina Baptist Billing Definition should be defined.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getInvestmentAccountList()), "North Carolina Baptist Billing Definition should have Two BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(3, CollectionUtils.getSize(def.getBillingScheduleList()), "North Carolina Baptist Billing Definition should have 3 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "North Carolina Baptist Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_MCGOWAN - ONE Account Assignments, Two Not Shared Schedules
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_MCGOWAN);
		Assertions.assertNotNull(def, "McGowan Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "McGowan Billing Definition should have 1 BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getBillingScheduleList()), "McGowan Billing Definition should have 2 Non Shared schedule assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "McGowan Billing Definition should Not have any Shared schedules assigned to it.");

		// BILLING_DEFINITION_DELTA_MASTER_159900 - ONE Account Assignments, One Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_DELTA_MASTER_159900);
		Assertions.assertNotNull(def, "Delta Master Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Delta Master Billing Definition should have 1 BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleList()), "Delta Master Billing Definition should not have any Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Delta Master Billing Definition should have 1 Shared schedules assigned to it.");

		// BILLING_DEFINITION_DELTA_PILOTS_159700 - ONE Account Assignments, One Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_DELTA_PILOTS_159700);
		Assertions.assertNotNull(def, "Delta Pilots Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Delta Pilots Billing Definition should have 1 BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleList()), "Delta Pilots Billing Definition should not have any Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Delta Pilots Billing Definition should have 1 Shared schedules assigned to it.");

		// BILLING_DEFINITION_NORTHWEST_159800 - ONE Account Assignments, One Shared Schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_NORTHWEST_159800);
		Assertions.assertNotNull(def, "Northwest Airlines Billing Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Northwest Airlines Billing Definition should have 1 BillingDefinitionInvestmentAccount assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleList()), "Northwest Airlines Billing Definition should not have any Non Shared schedule assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Northwest Airlines Billing Definition should have 1 Shared schedules assigned to it.");

		// BILLING_DEFINITION_DORIS_DUKE - Two Account Assignments, Each has it's own tiered schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_DORIS_DUKE);
		Assertions.assertNotNull(def, "Doris Duke Billing Definition should be defined.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getInvestmentAccountList()), "Doris Duke Billing Definition should have 2 BillingDefinitionInvestmentAccounts assigned to it.");
		Assertions.assertEquals(4, CollectionUtils.getSize(def.getBillingScheduleList()), "Doris Duke Billing Definition should not have 4 non shared schedules assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Doris Duke Billing Definition should have 0 Shared schedules assigned to it.");

		// BILLING_DEFINITION_DUKE_ENERGY - Two Account Assignments, One has a tiered schedule, the other fixed percentage
		// with a billing basis post processor to move one account billing basis to the other to fill up threshold
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_DUKE_ENERGY);
		Assertions.assertNotNull(def, "Duke EnergyBilling Definition should be defined.");
		Assertions.assertEquals(3, CollectionUtils.getSize(def.getInvestmentAccountList()), "Duke EnergyBilling Definition should have 3 BillingDefinitionInvestmentAccounts assigned to it.");
		Assertions.assertEquals(2, CollectionUtils.getSize(def.getBillingScheduleList()), "Duke EnergyBilling Definition should have 2 non shared schedules assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Duke EnergyBilling Definition should have 0 Shared schedules assigned to it.");

		// BILLING_DEFINITION_TARGET - One Account Assignment, fixed percentage schedule
		def = this.billingDefinitionService.getBillingDefinitionPopulated(BILLING_DEFINITION_TARGET);
		Assertions.assertNotNull(def, "Target Definition should be defined.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getInvestmentAccountList()), "Target Billing Definition should have 1 BillingDefinitionInvestmentAccounts assigned to it.");
		Assertions.assertEquals(1, CollectionUtils.getSize(def.getBillingScheduleList()), "Target Billing Definition should have 1 non shared schedules assigned to it.");
		Assertions.assertEquals(0, CollectionUtils.getSize(def.getBillingScheduleSharedList()), "Target Billing Definition should have 0 Shared schedules assigned to it.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBillingDefinitionInvestmentAccountSecurityExclusion() {
		BillingDefinitionInvestmentAccountSecurityExclusion securityExclusion = new BillingDefinitionInvestmentAccountSecurityExclusion();
		securityExclusion.setBillingDefinitionInvestmentAccount(this.billingDefinitionService.getBillingDefinitionInvestmentAccount(BILLING_DEFINITION_ACCOUNT_MCKNIGHT));
		securityExclusion.setStartDate(DateUtils.toDate("01/01/2019"));
		// Missing Security and Condition
		saveBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion, "Security and Exclude Condition cannot be blank.  At least one option must be selected.");


		securityExclusion.setInvestmentSecurity(this.investmentInstrumentService.getInvestmentSecurity(1));
		// Has a Security Now, but selected billing account doesn't support security level details
		saveBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion, "Selected billing account 392000: McKnight Foundation None doesn't support security level valuation.  Security exclusions are not allowed for valuation type None.");

		// Change to an account that does support security level details
		securityExclusion.setBillingDefinitionInvestmentAccount(this.billingDefinitionService.getBillingDefinitionInvestmentAccount(BILLING_DEFINITION_ACCOUNT_FAIRFAX_000));
		securityExclusion = saveBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion, null);

		// Add a new one for same security, overlapping date range
		BillingDefinitionInvestmentAccountSecurityExclusion dupeSecurityExclusion = new BillingDefinitionInvestmentAccountSecurityExclusion();
		dupeSecurityExclusion.setBillingDefinitionInvestmentAccount(securityExclusion.getBillingDefinitionInvestmentAccount());
		dupeSecurityExclusion.setInvestmentSecurity(securityExclusion.getInvestmentSecurity());
		dupeSecurityExclusion.setStartDate(DateUtils.toDate("04/01/2019"));
		saveBillingDefinitionInvestmentAccountSecurityExclusion(dupeSecurityExclusion, "There already existing a security exclusion for this billing account for the same security and/or exclude condition and overlapping date range: USD (01/01/2019 - )");

		// Set the end date on the original (bad end date - before start date)
		securityExclusion.setEndDate(DateUtils.toDate("12/01/2018"));
		saveBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion, "End date must be after start date 2019-01-01");

		// Fix the end date
		securityExclusion.setEndDate(DateUtils.toDate("03/31/2019"));
		saveBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion, null);

		// Now save the "dupe" - not a dupe anymore
		dupeSecurityExclusion = saveBillingDefinitionInvestmentAccountSecurityExclusion(dupeSecurityExclusion, null);

		// Delete both security exclusions
		this.billingDefinitionService.deleteBillingDefinitionInvestmentAccountSecurityExclusion(securityExclusion.getId());
		this.billingDefinitionService.deleteBillingDefinitionInvestmentAccountSecurityExclusion(dupeSecurityExclusion.getId());
	}


	private BillingDefinitionInvestmentAccountSecurityExclusion saveBillingDefinitionInvestmentAccountSecurityExclusion(BillingDefinitionInvestmentAccountSecurityExclusion exclusion, String expectedErrorMessage) {
		boolean error = false;
		try {
			exclusion = this.billingDefinitionService.saveBillingDefinitionInvestmentAccountSecurityExclusion(exclusion);
		}
		catch (Exception e) {
			error = true;
			if (!StringUtils.isEmpty(expectedErrorMessage)) {
				Assertions.assertEquals(expectedErrorMessage, ExceptionUtils.getOriginalMessage(e));
			}
			else {
				Assertions.fail("Did not expect error on save of security exclusion, but got: " + ExceptionUtils.getOriginalMessage(e));
			}
		}
		if (!StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.assertTrue(error, "Expected error message " + expectedErrorMessage + " but no error thrown on save.");
		}
		return exclusion;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidBillingFrequency_saveSchedule() {
		// Shared Schedule - Quarterly Invoice
		TestUtils.expectException(ValidationException.class, () -> {
			BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED);
			schedule.setAccrualFrequency(BillingFrequencies.ANNUALLY);
			this.billingDefinitionService.saveBillingSchedule(schedule);
		}, "Billing frequency of QUARTERLY and accrual frequency of ANNUALLY is invalid.  Accrual frequencies must be the same or shorter time periods than the billing frequency.");
	}


	@Test
	public void testInvalidBillingFrequency_saveDefinition() {
		// Shared Schedule - Quarterly Invoice
		TestUtils.expectException(ValidationException.class, () -> {
			BillingDefinition billingDefinition = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_FAIRFAX_000);
			billingDefinition.setBillingFrequency(BillingFrequencies.MONTHLY);
			this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		}, "Found multiple definition billing frequencies used for shared schedule Fairfax Shared.  Frequencies found [MONTHLY, QUARTERLY]");
	}


	@Test
	public void testInvalidBillingFrequency_saveScheduleSharing() {
		// Shared Schedule - Quarterly Invoice
		TestUtils.expectException(ValidationException.class, () -> {
			// Change McKnight to Monthly
			BillingDefinition mcknightDefinition = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCKNIGHT);
			mcknightDefinition.setBillingFrequency(BillingFrequencies.MONTHLY);
			this.billingDefinitionService.saveBillingDefinition(mcknightDefinition);

			// Now try to add mcknight (now monthly) to shared schedule that is already billing quarterly
			BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED);
			this.billingDefinitionService.linkBillingScheduleSharing(schedule.getId(), mcknightDefinition.getId());
		}, "Found multiple definition billing frequencies used for shared schedule Fairfax Shared.  Frequencies found [MONTHLY, QUARTERLY]");
	}


	@Test
	public void testAccrualFrequencyNotAllowed_saveSchedule() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_NC_BAPTIST_ANNUAL_MINIMUM);
			schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
			this.billingDefinitionService.saveBillingSchedule(schedule);
		}, "Accrual Frequency is not an allowed selection for schedule type Annual Minimum Fee");
	}


	@Test
	public void testRevenueShareNotAllowed_saveSchedule() {
		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_KIMBERLY_CLARK_PERCENTAGE_FEE);

		TestUtils.expectException(ValidationException.class, () -> {
			schedule.setRevenueType(BillingInvoiceRevenueTypes.REVENUE_SHARE_INTERNAL);
			this.billingDefinitionService.saveBillingSchedule(schedule);
		}, "Revenue type Revenue Share is not supported for billing definition " + schedule.getBillingDefinition().getLabel() + ".  Billing Definition is missing a company selection to allocate that revenue to.");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetBillingDefinitionSharedIdList() {
		List<Integer> sharedDefIdList = this.billingDefinitionService.getBillingDefinitionIdSharedList(BILLING_DEFINITION_FAIRFAX_000, DateUtils.toDate("07/01/2011"), DateUtils.toDate("09/30/2011"));
		Assertions.assertEquals(3, CollectionUtils.getSize(sharedDefIdList));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_FAIRFAX_000));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_FAIRFAX_400));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_FAIRFAX_500));

		// If we were to go back to a previous period 500 definition wouldn't be active
		sharedDefIdList = this.billingDefinitionService.getBillingDefinitionIdSharedList(BILLING_DEFINITION_FAIRFAX_000, DateUtils.toDate("03/01/2011"), DateUtils.toDate("06/30/2011"));
		Assertions.assertEquals(2, CollectionUtils.getSize(sharedDefIdList));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_FAIRFAX_000));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_FAIRFAX_500));

		// No Shared Schedules, should just contain the same id passed in
		sharedDefIdList = this.billingDefinitionService.getBillingDefinitionIdSharedList(BILLING_DEFINITION_DUKE_ENERGY, DateUtils.toDate("07/01/2011"), DateUtils.toDate("09/30/2011"));
		Assertions.assertEquals(1, CollectionUtils.getSize(sharedDefIdList));
		Assertions.assertTrue(sharedDefIdList.contains(BILLING_DEFINITION_DUKE_ENERGY));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBillingSchedule_UniqueNameValidation_Invalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingSchedule templateSchedule1 = setupTestSchedule("10% Discount");
			templateSchedule1.setTemplate(true);

			this.billingDefinitionService.saveBillingSchedule(templateSchedule1);

			BillingSchedule templateSchedule2 = setupTestSchedule("10% Discount");
			templateSchedule2.setTemplate(true);

			this.billingDefinitionService.saveBillingSchedule(templateSchedule2);
		}, "There already exists a template schedule with name [10% Discount] and overlapping date range.  Please enter a unique name.");
	}


	@Test
	public void testBillingSchedule_UniqueNameValidation_Valid() {
		BillingSchedule templateSchedule1 = setupTestSchedule("10% Discount");
		templateSchedule1.setTemplate(true);
		templateSchedule1.setEndDate(DateUtils.toDate("12/31/2017"));

		this.billingDefinitionService.saveBillingSchedule(templateSchedule1);

		BillingSchedule templateSchedule2 = setupTestSchedule("10% Discount");
		templateSchedule2.setTemplate(true);
		templateSchedule2.setStartDate(DateUtils.toDate("01/01/2018"));

		this.billingDefinitionService.saveBillingSchedule(templateSchedule2);
	}


	private BillingSchedule setupTestSchedule(String name) {
		BillingSchedule schedule = new BillingSchedule();
		schedule.setName(name);
		schedule.setScheduleType(this.billingDefinitionService.getBillingScheduleType(SCHEDULE_TYPE_DISCOUNT));
		schedule.setScheduleAmount(BigDecimal.TEN);
		return schedule;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBillingScheduleSharedCopy() {
		BillingSchedule copyFromSchedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED);
		BillingSchedule newSchedule = this.billingDefinitionService.copyBillingScheduleShared(BILLING_SCHEDULE_FAIRFAX_SHARED, DateUtils.toDate("01/01/2019"), false, BeanUtils.getPropertyValues(this.billingDefinitionService.getBillingScheduleSharingListForSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED), sharing -> sharing.getReferenceTwo().getId(), Integer.class));

		// Pull Again to Ensure everything was saved
		newSchedule = this.billingDefinitionService.getBillingSchedule(newSchedule.getId());

		// Validate the New Schedule has All 3 Tiers
		Assertions.assertEquals(copyFromSchedule.getScheduleTierList().size(), newSchedule.getScheduleTierList().size());
		// Validate the New Schedule has all of the billing definitions
		Assertions.assertEquals(this.billingDefinitionService.getBillingScheduleSharingListForSchedule(copyFromSchedule.getId()).size(), this.billingDefinitionService.getBillingScheduleSharingListForSchedule(newSchedule.getId()).size());

		// Copy Again, this time change dates
		BillingSchedule newSchedule2 = this.billingDefinitionService.copyBillingScheduleShared(newSchedule.getId(), DateUtils.toDate("10/01/2019"), true, BeanUtils.getPropertyValues(this.billingDefinitionService.getBillingScheduleSharingListForSchedule(newSchedule.getId()), sharing -> sharing.getReferenceTwo().getId(), Integer.class));
		Assertions.assertEquals(newSchedule.getScheduleTierList().size(), newSchedule2.getScheduleTierList().size());
		// Validate the New Schedule has all of the billing definitions
		Assertions.assertEquals(this.billingDefinitionService.getBillingScheduleSharingListForSchedule(newSchedule.getId()).size(), this.billingDefinitionService.getBillingScheduleSharingListForSchedule(newSchedule2.getId()).size());

		newSchedule = this.billingDefinitionService.getBillingSchedule(newSchedule.getId());
		Assertions.assertEquals("01/01/2019", DateUtils.fromDateShort(newSchedule.getStartDate()));
		Assertions.assertEquals("09/30/2019", DateUtils.fromDateShort(newSchedule.getEndDate()));

		newSchedule2 = this.billingDefinitionService.getBillingSchedule(newSchedule2.getId());
		Assertions.assertEquals("10/01/2019", DateUtils.fromDateShort(newSchedule2.getStartDate()));
		Assertions.assertNull(newSchedule2.getEndDate());

		this.billingDefinitionService.deleteBillingSchedule(newSchedule.getId());
		this.billingDefinitionService.deleteBillingSchedule(newSchedule2.getId());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Valid() {
		// Add a dependency between Duke Energy and Acument
		setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_DUKE_ENERGY, true);

		// Add another dependency between Duke Energy and Fairfax 000
		// Fairfax 000 is shared, but not shared with Duke, and Duke is the parent of both, so it's still only one level deep
		setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_DUKE_ENERGY, true);

		// Add another dependency between McKnight and Fairfax 000
		// Fairfax 000 is shared, but not shared with McKnight, and Fairfax is the child of both, so it's still only one level deep
		setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_MCKNIGHT, true);
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_DifferentBillingCurrency() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingDefinition acument = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT);
			acument.setBillingCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol("CAD", true));
			this.billingDefinitionService.saveBillingDefinition(acument);

			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_MCKNIGHT, true);
		}, "Prerequisite and Dependent Billing Definitions must use the same Billing Currency. Found USD and CAD");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_SharedSchedule() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Billing Schedule is Shared which is not currently allowed
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_SHARED, BILLING_DEFINITION_FAIRFAX_500, true);
		}, "Billing Schedule is missing a billing definition.  Schedule dependencies cannot apply to shared schedules.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_BadFrequency() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingDefinition acument = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT);
			acument.setBillingFrequency(BillingFrequencies.MONTHLY);
			this.billingDefinitionService.saveBillingDefinition(acument);

			// Monthly Invoice Can not Depend on a Quarterly Invoice
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_MCKNIGHT, true);
		}, "The Prerequisite Billing Definition can not be less frequent than the dependent billing definition. Prerequisite Billing Definition [1:  (05/05/1998 - )] is billed [QUARTERLY], and the dependent billing definition [20:  (01/14/2011 - )] is billed [MONTHLY]");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Valid_Frequency() {
		BillingDefinition acument = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT);
		acument.setBillingFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingDefinition(acument);

		// Quarterly Invoice Can Depend on a Monthly Invoice
		setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE, BILLING_DEFINITION_ACUMENT, true);
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_SameDefinition() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Billing Schedule is Shared which is not currently allowed
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_FAIRFAX_000, true);
		}, "Prerequisite Billing Definition cannot be the same as the dependent billing definition.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_Shared() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Billing Definitions Share a Schedule, so should fail
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_FAIRFAX_500, true);
		}, "Dependency [Quarterly Account Retainer - 11:  (10/01/2009 - )] is invalid because the two billing definitions share a schedule for the link date range.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_ChildIsAParent() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_MCKNIGHT, true);
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE, BILLING_DEFINITION_ACUMENT, true);
		}, "Dependency [Monthly Retainer Fee - 20:  (01/14/2011 - )] is invalid because it goes more than one level deep.  Dependent Definition [1:  (05/05/1998 - )] is a prerequisite for [10:  (03/22/2000 - )] for overlapping date range.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_ChildIsAParent_ThroughSharedSchedule() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_FAIRFAX_500, true);
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_MCKNIGHT, true);
		}, "Dependency [Quarterly Account Retainer - 1:  (05/05/1998 - )] is invalid because it goes more than one level deep.  Dependent Shared Definition [10:  (03/22/2000 - )] is a prerequisite for [20:  (01/14/2011 - )] for overlapping date range.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_ParentIsAChild() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_MCKNIGHT, true);
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_FAIRFAX_000, true);
		}, "Dependency [Tiered Fee - 10:  (03/22/2000 - )] is invalid because it goes more than one level deep.  Prerequisite Definition [10:  (03/22/2000 - )] is also dependent on [1:  (05/05/1998 - )] for overlapping date range.");
	}


	@Test
	public void testBillingScheduleBillingDefinitionDependency_Invalid_ParentIsAChild_ThroughSharedSchedule() {
		TestUtils.expectException(ValidationException.class, () -> {
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_FAIRFAX_000_RETAINER, BILLING_DEFINITION_MCKNIGHT, true);
			setupBillingScheduleBillingDefinitionDependency(BILLING_SCHEDULE_ACUMENT_TIERED, BILLING_DEFINITION_FAIRFAX_500, true);
		}, "Dependency [Tiered Fee - 11:  (10/01/2009 - )] is invalid because it goes more than one level deep.  Prerequisite Shared Definition [11:  (10/01/2009 - )] is also dependent on [1:  (05/05/1998 - )] for overlapping date range.");
	}


	private BillingScheduleBillingDefinitionDependency setupBillingScheduleBillingDefinitionDependency(int billingScheduleId, int billingDefinitionId, boolean save) {
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(billingScheduleId));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(billingDefinitionId));
		if (save) {
			return this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);
		}
		return dependency;
	}
}
