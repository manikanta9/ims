package com.clifton.billing.definition;


import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * The <code>BillingFrequencyTests</code> ...
 *
 * @author Mary Anderson
 */
public class BillingFrequencyTests {

	private static final Date[] TEST_DATES = new Date[]{ //
			DateUtils.toDate("01/01/2011"), // 0
			DateUtils.toDate("2/13/2011"), // 1
			DateUtils.toDate("03/31/2011"), // 2
			DateUtils.toDate("04/01/2011"), // 3
			DateUtils.toDate("6/29/2011"), // 4
			DateUtils.toDate("8/31/2011"), // 5
			DateUtils.toDate("9/1/2011"), // 6
			DateUtils.toDate("12/31/2011") // 7
	};


	public void testDailyBillingFrequency() {
		BillingFrequencies freq = BillingFrequencies.DAILY;
		validateBillingInvoiceDate(freq, TEST_DATES[0], TEST_DATES[0], null);
		validateBillingInvoiceDate(freq, TEST_DATES[1], TEST_DATES[1], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, TEST_DATES[2], TEST_DATES[2], null);
		validateBillingInvoiceDate(freq, TEST_DATES[3], TEST_DATES[3], null);
		validateBillingInvoiceDate(freq, TEST_DATES[4], TEST_DATES[4], null);
		validateBillingInvoiceDate(freq, TEST_DATES[5], TEST_DATES[5], null);
		validateBillingInvoiceDate(freq, TEST_DATES[6], TEST_DATES[6], null);
		validateBillingInvoiceDate(freq, TEST_DATES[7], TEST_DATES[7], null);

		validateDateRangeLabel(freq, TEST_DATES[0], TEST_DATES[0], DateUtils.fromDateShort(TEST_DATES[0]));
		validateDateRangeLabel(freq, TEST_DATES[4], TEST_DATES[4], DateUtils.fromDateShort(TEST_DATES[4]));
		validateDateRangeLabel(freq, TEST_DATES[7], TEST_DATES[7], DateUtils.fromDateShort(TEST_DATES[7]));
	}


	@Test
	public void testMonthlyBillingFrequency() {
		BillingFrequencies freq = BillingFrequencies.MONTHLY;
		validateBillingInvoiceDate(freq, DateUtils.toDate("01/31/2011"), TEST_DATES[0], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("02/28/2011"), TEST_DATES[1], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[2], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("04/30/2011"), TEST_DATES[3], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[4], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("08/31/2011"), TEST_DATES[5], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("09/30/2011"), TEST_DATES[6], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[7], null);

		validateDateRangeLabel(freq, TEST_DATES[0], TEST_DATES[0], "January 2011");
		validateDateRangeLabel(freq, TEST_DATES[4], TEST_DATES[4], "June 2011");
		validateDateRangeLabel(freq, TEST_DATES[7], TEST_DATES[7], "December 2011");
	}


	@Test
	public void testQuarterlyBillingFrequency() {
		BillingFrequencies freq = BillingFrequencies.QUARTERLY;
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[0], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[1], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[2], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[3], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[4], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("09/30/2011"), TEST_DATES[5], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("09/30/2011"), TEST_DATES[6], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[7], null);

		validateDateRangeLabel(freq, TEST_DATES[0], TEST_DATES[0], "1Q 2011");
		validateDateRangeLabel(freq, TEST_DATES[4], TEST_DATES[4], "2Q 2011");
		validateDateRangeLabel(freq, TEST_DATES[7], TEST_DATES[7], "4Q 2011");
	}


	@Test
	public void testSemiAnnuallyBillingFrequency() {
		BillingFrequencies freq = BillingFrequencies.SEMI_ANNUALLY;
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[0], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[1], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[2], DateUtils.toDate("04/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[3], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[4], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("10/31/2011"), TEST_DATES[5], DateUtils.toDate("05/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[6], DateUtils.toDate("01/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("01/31/2012"), TEST_DATES[7], DateUtils.toDate("02/01/2010"));

		validateDateRangeLabel(freq, TEST_DATES[0], DateUtils.toDate("06/30/2011"), "01/01/2011 - 06/30/2011");
		validateDateRangeLabel(freq, TEST_DATES[3], DateUtils.toDate("10/31/2011"), "04/01/2011 - 10/31/2011");
		validateDateRangeLabel(freq, TEST_DATES[6], DateUtils.toDate("03/31/2012"), "09/01/2011 - 03/31/2012");
	}


	@Test
	public void testAnnuallyFrequency() {
		BillingFrequencies freq = BillingFrequencies.ANNUALLY;
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[0], null);
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[1], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("03/31/2011"), TEST_DATES[2], DateUtils.toDate("04/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[3], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("06/30/2011"), TEST_DATES[4], DateUtils.toDate("7/1/2009"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[5], DateUtils.toDate("01/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("12/31/2011"), TEST_DATES[6], DateUtils.toDate("01/01/2010"));
		validateBillingInvoiceDate(freq, DateUtils.toDate("01/31/2012"), TEST_DATES[7], DateUtils.toDate("02/01/2010"));

		validateDateRangeLabel(freq, TEST_DATES[0], DateUtils.toDate("12/31/2011"), "01/01/2011 - 12/31/2011");
		validateDateRangeLabel(freq, TEST_DATES[3], DateUtils.toDate("03/31/2012"), "04/01/2011 - 03/31/2012");
		validateDateRangeLabel(freq, TEST_DATES[6], DateUtils.toDate("08/31/2012"), "09/01/2011 - 08/31/2012");
	}


	private void validateBillingInvoiceDate(BillingFrequencies frequency, Date expectedDate, Date date, Date annualStartDate) {
		Date invoiceDate = frequency.getLastDayOfBillingCycle(date, annualStartDate);
		if (DateUtils.compare(expectedDate, invoiceDate, false) != 0) {
			throw new RuntimeException("Expected Invoice Date of [" + DateUtils.fromDate(expectedDate) + "] for date [" + DateUtils.fromDate(date) + "] "
					+ (annualStartDate == null ? "" : " with an annual start date of [" + DateUtils.fromDate(annualStartDate) + "]") + " but was [" + DateUtils.fromDate(invoiceDate) + "]");
		}
	}


	private void validateDateRangeLabel(BillingFrequencies frequency, Date accrualStartDate, Date accrualEndDate, String expectedResult) {
		String result = frequency.getDateRangeLabel(accrualStartDate, accrualEndDate);
		Assertions.assertEquals(expectedResult, result);
	}
}
