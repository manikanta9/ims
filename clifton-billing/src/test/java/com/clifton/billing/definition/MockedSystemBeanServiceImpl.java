package com.clifton.billing.definition;


import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.MockedBillingBasisValueCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisPeriodAverageCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisPeriodDateCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisPeriodMonthEndAverageCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasisPeriodTradeDatesCalculator;
import com.clifton.billing.invoice.process.billingbasis.postprocessors.BillingBasisPostProcessor;
import com.clifton.billing.invoice.process.billingbasis.postprocessors.BillingBasisShifter;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BaseBillingAmountCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForAnnualBoundaryCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForAnnualMinimumWithRebateCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForBoundaryCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForDiscountCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForFixedFeeCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForPercentageFeeCalculator;
import com.clifton.billing.invoice.process.schedule.amount.calculators.BillingAmountForTieredFeeCalculator;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountForBillingBasisProrater;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountForBillingDatesProrater;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountProrater;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleEvaluatorContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanServiceImpl;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * The <code>MockedSystemBeanServiceImpl</code> ...
 *
 * @author manderson
 */
public class MockedSystemBeanServiceImpl extends SystemBeanServiceImpl {

	private static final int PRORATE_BILLING_BASIS_DATES = 500;
	private static final int PRORATE_BILLING_DATES = 510;

	@Resource
	private MockedBillingBasisValueCalculator billingBasisCalculatorMock;


	@Override
	public Object getBeanInstance(SystemBean bean) {
		Object obj = null;
		if ("Duke Energy Shifter Bean".equals(bean.getName())) {
			obj = getPostProcessorObject(bean.getName());
		}
		else if ("Discount".equals(bean.getName()) || bean.getName().endsWith("Fee")) {
			obj = getScheduleAmountCalculator(bean.getName());
		}
		else if (bean.getName().endsWith("Prorater")) {
			obj = getScheduleAmountProrater(bean.getName());
		}
		else if (bean.getName().equals("Billing Basis Valuation Calculator")) {
			return this.billingBasisCalculatorMock;
		}
		else if (bean.getName().startsWith("Period")) {
			obj = getBillingBasisCalculator(bean.getName());
		}
		else if (bean.getName().endsWith("Context")) {
			obj = new BillingInvoiceRuleEvaluatorContext();
		}

		Assertions.assertNotNull(obj, "Test implementation missing for bean [" + bean.getName() + "].");
		getApplicationContextService().autowireBean(obj);
		return obj;
	}


	private BillingBasisPostProcessor getPostProcessorObject(String name) {
		if ("Duke Energy Shifter Bean".equals(name)) {
			BillingBasisShifter obj = new BillingBasisShifter();
			obj.setFromBillingDefinitionInvestmentAccountId(111);
			obj.setToInvestmentAccountGroupId(10);
			obj.setMinimumAmountInToAccount(BigDecimal.valueOf(100000000));
			obj.setNote("Fill PIOS account to 100 Million");
			return obj;
		}
		return null;
	}


	private BillingAmountCalculator getScheduleAmountCalculator(String name) {
		BaseBillingAmountCalculator obj = null;
		Integer proratedBeanId = null;
		if ("Tiered Fee".equals(name)) {
			obj = new BillingAmountForTieredFeeCalculator();
			proratedBeanId = PRORATE_BILLING_BASIS_DATES;
		}
		else if ("Discount".equals(name)) {
			obj = new BillingAmountForDiscountCalculator();
		}
		else if ("Percentage Fee".equals(name)) {
			obj = new BillingAmountForPercentageFeeCalculator();
			proratedBeanId = PRORATE_BILLING_BASIS_DATES;
		}
		else if ("Fixed Fee".equals(name)) {
			obj = new BillingAmountForFixedFeeCalculator();
			proratedBeanId = PRORATE_BILLING_DATES;
		}
		else if ("Minimum Fee".equals(name) || "Maximum Fee".equals(name)) {
			obj = new BillingAmountForBoundaryCalculator();
			proratedBeanId = PRORATE_BILLING_DATES;
		}
		else if ("Annual Minimum Fee".equals(name) || "Annual Maximum Fee".equals(name)) {
			obj = new BillingAmountForAnnualBoundaryCalculator();
			proratedBeanId = PRORATE_BILLING_DATES;
		}
		else if ("Annual Minimum (With Rebates) Fee".equals(name)) {
			obj = new BillingAmountForAnnualMinimumWithRebateCalculator();
			proratedBeanId = PRORATE_BILLING_DATES;
		}
		else if ("Revenue Share - Percentage Fee".equals(name)) {
			proratedBeanId = PRORATE_BILLING_DATES;
			BillingAmountForPercentageFeeCalculator calc = new BillingAmountForPercentageFeeCalculator();
			calc.setApplyToBillingAmounts(true);
			calc.setApplyToBillingAmountsRevenueTypeList(CollectionUtils.createList(BillingInvoiceRevenueTypes.REVENUE));
			obj = calc;
		}

		if (obj != null && proratedBeanId != null) {
			obj.setBillingAmountProraterBean(getSystemBean(proratedBeanId));
		}
		return obj;
	}


	private BillingAmountProrater getScheduleAmountProrater(String name) {
		if ("Billing Dates Prorater".equals(name)) {
			return new BillingAmountForBillingDatesProrater();
		}
		else if ("Billing Basis Dates Prorater".equals(name)) {
			return new BillingAmountForBillingBasisProrater();
		}
		return null;
	}


	private BillingBasisCalculator getBillingBasisCalculator(String name) {

		if ("Period Start".equals(name) || "Period End".equals(name)) {
			BillingBasisPeriodDateCalculator obj = new BillingBasisPeriodDateCalculator();
			obj.setPeriodEnd("Period End".equals(name));
			return obj;
		}
		if ("Period Average".equals(name)) {
			return new BillingBasisPeriodAverageCalculator();
		}
		if ("Period Month End Average".equals(name)) {
			return new BillingBasisPeriodMonthEndAverageCalculator();
		}
		if ("Period Trade Dates".equals(name)) {
			return new BillingBasisPeriodTradeDatesCalculator();
		}
		return null;
	}
}
