package com.clifton.billing.payment;

import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountAllocation;
import com.clifton.billing.payment.account.BillingPaymentInvestmentAccountService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.account.InvestmentAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BillingPaymentServiceImplTests {


	@Resource
	private BillingPaymentServiceImpl billingPaymentService;

	@Resource
	private BillingPaymentInvestmentAccountService billingPaymentInvestmentAccountService;

	@Resource
	private UpdatableDAO<BillingPayment> billingPaymentDAO;

	private static final int PAYMENT_MCGOWAN_INVOICE_2_AND_3 = 2;
	private static final int PAYMENT_UNALLOCATED = 3;


	////////////////////////////////////////////////////////////////////////////////
	////////////                  TEST VOIDED PAYMENTS                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Tests voiding the payment with allocations and validation of the allocations and removal of them
	 */
	@Test
	public void testPayment_Voided() {
		BillingPayment payment = this.billingPaymentService.getBillingPayment(PAYMENT_MCGOWAN_INVOICE_2_AND_3);
		Assertions.assertEquals(BillingPaymentStatuses.FULL, payment.getBillingPaymentStatus());
		Assertions.assertFalse(payment.isVoided());
		List<BillingPaymentAllocation> allocationList = this.billingPaymentService.getBillingPaymentAllocationListForPayment(PAYMENT_MCGOWAN_INVOICE_2_AND_3);
		Assertions.assertEquals(2, CollectionUtils.getSize(allocationList));
		BillingPaymentAllocation paymentAllocation = BeanUtils.cloneBean(allocationList.get(0), false, false);

		// Try voiding the payment, should get exception because there are allocations
		boolean validationExceptionFound = false;
		try {
			this.billingPaymentService.deleteBillingPayment(PAYMENT_MCGOWAN_INVOICE_2_AND_3, false);
		}
		catch (UserIgnorableValidationException e) {
			validationExceptionFound = true;
			Assertions.assertEquals("Selected payment [# 2 12/31/2010] has payment allocations.  When the payment is voided those allocations will be removed.", e.getMessage());
		}
		Assertions.assertTrue(validationExceptionFound, "Expected user ignorable validation exception for voiding payment with allocations.");

		// Try voiding the payment again, this time deleting the allocations
		this.billingPaymentService.deleteBillingPayment(PAYMENT_MCGOWAN_INVOICE_2_AND_3, true);
		payment = this.billingPaymentService.getBillingPayment(payment.getId());
		Assertions.assertTrue(payment.isVoided());
		Assertions.assertEquals(BillingPaymentStatuses.NONE, payment.getBillingPaymentStatus());
		Assertions.assertEquals(BigDecimal.ZERO, payment.getAllocatedAmount());
		Assertions.assertEquals(BigDecimal.ZERO, payment.getAvailableAmount());
		allocationList = this.billingPaymentService.getBillingPaymentAllocationListForPayment(PAYMENT_MCGOWAN_INVOICE_2_AND_3);
		Assertions.assertEquals(0, CollectionUtils.getSize(allocationList));

		// Try to Re-Allocate Voided Payment to Invoice
		BillingPayment finalPayment = payment;
		TestUtils.expectException(ValidationException.class, () -> {
			BillingPaymentAllocationEntry allocationEntry = new BillingPaymentAllocationEntry();
			allocationEntry.setExistingBillingPayment(finalPayment);
			paymentAllocation.setId(null);
			allocationEntry.setBillingPaymentAllocationList(CollectionUtils.createList(paymentAllocation));
			this.billingPaymentService.saveBillingPaymentAllocationEntry(allocationEntry);
		}, "Voided payments cannot be allocated to invoices.");
	}


	/**
	 * Attempts to bypass the void logic and call the delete method directly
	 */
	@Test
	public void testPayment_Delete() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.billingPaymentDAO.delete(PAYMENT_UNALLOCATED);
		}, "Payments cannot be deleted");
	}


	@Test
	public void testPayment_Void_Twice() {
		this.billingPaymentService.deleteBillingPayment(PAYMENT_UNALLOCATED, false);
		TestUtils.expectException(ValidationException.class, () -> {
			this.billingPaymentService.deleteBillingPayment(PAYMENT_UNALLOCATED, false);
		}, "Payment has already been voided");
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////         TEST PAYMENTS THAT ARE ALLOCATED TO ACCOUNTS         /////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPaymentAllocation_OneAccount_Positive() {
		// Full Invoice Payment
		BillingInvoice invoice = populateInvoice(15000.0d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		Map<BillingPaymentInvestmentAccountAllocation, Double> invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 15000.0d, 0.0d), 15000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Re-do explicitly allocating full amount
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 15000.0d);

		// Full Invoice Payment - but only allocating partial amount
		invoice = populateInvoice(15000.0d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 15000.0d, 0.0d), 12000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 12000.0d);


		// Invoice already partially paid
		invoice = populateInvoice(15000.0d);
		payment = populatePaymentForInvoice(invoice, 7000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 15000.0d, 8000.0d), 7000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment larger than invoice total
		invoice = populateInvoice(15000.0d);
		payment = populatePaymentForInvoice(invoice, 25000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 15000.0d, 0.0d), 15000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment less than Invoice total
		invoice = populateInvoice(15000.0d);
		payment = populatePaymentForInvoice(invoice, 10000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 15000.0d, 0.0d), 10000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);
	}


	@Test
	public void testPaymentAllocation_OneAccount_Negative() {
		// Full Invoice Payment
		BillingInvoice invoice = populateInvoice(-15000.0d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		Map<BillingPaymentInvestmentAccountAllocation, Double> invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -15000.0d, 0.0d), -15000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Re-do explicitly allocating full amount
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, -15000.0d);

		// Full Invoice Payment - but only allocating partial amount
		invoice = populateInvoice(-15000.0d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -15000.0d, 0.0d), -12000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, -12000.0d);

		// Invoice already partially paid
		invoice = populateInvoice(-15000.0d);
		payment = populatePaymentForInvoice(invoice, -7000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -15000.0d, -8000.0d), -7000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment larger than invoice total
		invoice = populateInvoice(-15000.0d);
		payment = populatePaymentForInvoice(invoice, -25000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -15000.0d, 0.0d), -15000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment less than Invoice total
		invoice = populateInvoice(-15000.0d);
		payment = populatePaymentForInvoice(invoice, -10000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -15000.0d, 0.0d), -10000.0d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);
	}


	@Test
	public void testPaymentAllocation_TwoAccounts_Positive() {
		// Full Invoice Payment
		BillingInvoice invoice = populateInvoice(37245.00d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		Map<BillingPaymentInvestmentAccountAllocation, Double> invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 21968.72d, 0.0d), 21968.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, 15276.28d, 0.0d), 15276.28d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Re-do explicitly allocating full amount
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 37245.00d);

		// Full Invoice Payment - but only allocating partial amount
		invoice = populateInvoice(37245.00d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 21968.72d, 0.0d), 7078.12);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, 15276.28d, 0.0d), 4921.88);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 12000.0d);


		// Invoice - one account partially paid
		invoice = populateInvoice(37245.00d);
		payment = populatePaymentForInvoice(invoice, 17245.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 21968.72d, 20000.0d), 1968.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, 15276.28d, 0.0d), 15276.28d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);


		// Payment larger than invoice total
		invoice = populateInvoice(37245.00d);
		payment = populatePaymentForInvoice(invoice, 50000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 21968.72d, 0.0d), 21968.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, 15276.28d, 0.0d), 15276.28d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment less than Invoice total
		invoice = populateInvoice(37245.00d);
		payment = populatePaymentForInvoice(invoice, 12000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 21968.72d, 0.0d), 7078.12);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, 15276.28d, 0.0d), 4921.88);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);
	}


	@Test
	public void testPaymentAllocation_FiveAccounts_Negative() {
		// Full Invoice Payment
		BillingInvoice invoice = populateInvoice(-1536.86d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		Map<BillingPaymentInvestmentAccountAllocation, Double> invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -124.17d, 0.0d), -124.17d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -425.18d, 0.0d), -425.18d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(3, invoice, -322.72d, 0.0d), -322.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(4, invoice, -105.85d, 0.0d), -105.85d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(5, invoice, -558.94d, 0.0d), -558.94d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Re-do explicitly allocating full amount
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, -1536.86d);

		// Full Invoice Payment - but only allocating partial amount
		invoice = populateInvoice(-1536.86d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -124.17d, 0.0d), -80.79d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -425.18d, 0.0d), -276.65d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(3, invoice, -322.72d, 0.0d), -209.99d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(4, invoice, -105.85d, 0.0d), -68.87d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(5, invoice, -558.94d, 0.0d), -363.70d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, -1000.0d);


		// Invoice - one account partially paid
		invoice = populateInvoice(-1536.86d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -124.17d, 0.0d), -124.17d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -425.18d, 0.0d), -425.18d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(3, invoice, -322.72d, 0.0d), -322.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(4, invoice, -105.85d, 0.0d), -105.85d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(5, invoice, -558.94d, -250.00d), -308.94d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, -1286.86d);


		// Payment larger than invoice total
		invoice = populateInvoice(-1536.86d);
		payment = populatePaymentForInvoice(invoice, -2000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -124.17d, 0.0d), -124.17d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -425.18d, 0.0d), -425.18d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(3, invoice, -322.72d, 0.0d), -322.72d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(4, invoice, -105.85d, 0.0d), -105.85d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(5, invoice, -558.94d, 0.0d), -558.94d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment less than Invoice total
		invoice = populateInvoice(-1536.86d);
		payment = populatePaymentForInvoice(invoice, -1000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, -124.17d, 0.0d), -80.79d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -425.18d, 0.0d), -276.65d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(3, invoice, -322.72d, 0.0d), -209.99d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(4, invoice, -105.85d, 0.0d), -68.87d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(5, invoice, -558.94d, 0.0d), -363.70d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);
	}


	@Test
	public void testPaymentAllocation_TwoAccounts_OnePositive_OneNegative() {
		// Full Invoice Payment
		BillingInvoice invoice = populateInvoice(12500.00d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		Map<BillingPaymentInvestmentAccountAllocation, Double> invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 14798.30d, 0.0d), 14798.30d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -2298.30, 0.0d), -2298.30);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Re-do explicitly allocating full amount
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 12500.0d);

		// Full Invoice Payment - but only allocating partial amount
		invoice = populateInvoice(12500.0d);
		payment = populatePaymentForInvoice(invoice, null);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 14798.30d, 0.0d), 11838.64d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -2298.30, 0.0d), -1838.64);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, 10000.0d);


		// Invoice - one account partially paid
		invoice = populateInvoice(12500.00);
		payment = populatePaymentForInvoice(invoice, 10000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 14798.30d, 2500.0d), 12298.30d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -2298.30, 0.0d), -2298.30d);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);


		// Payment larger than invoice total
		invoice = populateInvoice(12500.00);
		payment = populatePaymentForInvoice(invoice, 25000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 14798.30d, 0.0d), 14798.30d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -2298.30, 0.0d), -2298.30);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);

		// Payment less than Invoice total
		invoice = populateInvoice(12500.00d);
		payment = populatePaymentForInvoice(invoice, 10000.0d);

		invoicePaymentAllocationExpectedAllocationMap = new HashMap<>();
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(1, invoice, 14798.30d, 0.0d), 11838.64d);
		invoicePaymentAllocationExpectedAllocationMap.put(populateBillingPaymentInvestmentAccountAllocation(2, invoice, -2298.30, 0.0d), -1838.64);
		validatePaymentAllocations(invoice, payment, invoicePaymentAllocationExpectedAllocationMap, null);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////         TEST PAYMENTS THAT ARE ALLOCATED TO ACCOUNTS         /////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testManualInvoicePayment() {
		BillingInvoice invoice = populateManualInvoice(5000.0d);
		BillingPayment payment = populatePaymentForInvoice(invoice, null);

		validatePaymentAllocationForManualInvoice(invoice, payment, null, 5000.0d);

		// Mark Invoice as Partially Paid
		invoice = populateManualInvoice(5000.0d);
		invoice.setPaidAmount(BigDecimal.valueOf(2000.0d));
		payment = populatePaymentForInvoice(invoice, null);

		validatePaymentAllocationForManualInvoice(invoice, payment, null, 3000.0d);

		// Lower available amount on Payment
		invoice = populateManualInvoice(5000.0d);
		payment = populatePaymentForInvoice(invoice, 2000.0d);

		validatePaymentAllocationForManualInvoice(invoice, payment, null, 2000.0d);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                        HELPER METHODS                        /////////
	////////////////////////////////////////////////////////////////////////////////


	private void validatePaymentAllocations(BillingInvoice invoice, BillingPayment payment, Map<BillingPaymentInvestmentAccountAllocation, Double> expectedAccountAllocationMap, Double allocationAmount) {
		List<BillingPaymentInvestmentAccountAllocation> invoicePaymentAllocationList = CollectionUtils.toArrayList(expectedAccountAllocationMap.keySet());

		Mockito.doReturn(invoicePaymentAllocationList).when(this.billingPaymentInvestmentAccountService).getBillingPaymentInvestmentAccountAllocationList(ArgumentMatchers.any());

		List<BillingPaymentAllocation> allocationList = this.billingPaymentService.populateBillingPaymentAllocationListForInvoice(payment, invoice, allocationAmount == null ? null : BigDecimal.valueOf(allocationAmount));
		Map<String, BillingPaymentAllocation> accountAllocationMap = BeanUtils.getBeanMap(allocationList, billingPaymentAllocation -> billingPaymentAllocation.getBillingInvoice().getId() + "_" + billingPaymentAllocation.getInvestmentAccount().getId());
		for (Map.Entry<BillingPaymentInvestmentAccountAllocation, Double> accountAllocationEntry : expectedAccountAllocationMap.entrySet()) {
			BillingPaymentAllocation paymentAllocation = accountAllocationMap.get(accountAllocationEntry.getKey().getId());
			if (MathUtils.isNullOrZero(accountAllocationEntry.getValue())) {
				AssertUtils.assertNull(paymentAllocation, "Did not expect a payment allocation to be created for account " + paymentAllocation.getId() + " but it was for an allocated value of " + CoreMathUtils.formatNumberMoney(payment.getAllocatedAmount()));
			}
			else if (paymentAllocation == null) {
				AssertUtils.fail("Expected a payment allocation to be created for account " + accountAllocationEntry.getKey().getId() + " with an allocated value of " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(accountAllocationEntry.getValue())) + " but it wasn't allocated anything");
			}
			else if (!MathUtils.isEqual(BigDecimal.valueOf(accountAllocationEntry.getValue()), paymentAllocation.getAllocatedAmount())) {
				AssertUtils.fail("Expected a payment allocation to be created for account " + accountAllocationEntry.getKey().getId() + " with an allocated value of " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(accountAllocationEntry.getValue())) + " but it was allocated " + CoreMathUtils.formatNumberMoney(paymentAllocation.getAllocatedAmount()));
			}
		}
	}


	private void validatePaymentAllocationForManualInvoice(BillingInvoice invoice, BillingPayment payment, Double allocationAmount, Double expectedPaymentAllocationAmount) {
		List<BillingPaymentAllocation> allocationList = this.billingPaymentService.populateBillingPaymentAllocationListForInvoice(payment, invoice, allocationAmount == null ? null : BigDecimal.valueOf(allocationAmount));
		AssertUtils.assertEquals(1, CollectionUtils.getSize(allocationList), "Manual Invoices should only have one payment allocation for a payment and invoice");
		BillingPaymentAllocation paymentAllocation = allocationList.get(0);
		AssertUtils.assertNull(paymentAllocation.getInvestmentAccount(), "Manual Invoices should not be allocated to an account");
		if (!MathUtils.isEqual(BigDecimal.valueOf(expectedPaymentAllocationAmount), paymentAllocation.getAllocatedAmount())) {
			AssertUtils.fail("Expected a payment allocation to be created with an allocated value of " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(expectedPaymentAllocationAmount)) + " but it was allocated " + CoreMathUtils.formatNumberMoney(paymentAllocation.getAllocatedAmount()));
		}
	}

	////////////////////////////////////////////////////////////////////////


	private BillingInvoice populateInvoice(double invoiceTotal) {
		return populateInvoiceImpl(invoiceTotal, true);
	}


	private BillingInvoice populateManualInvoice(double invoiceTotal) {
		return populateInvoiceImpl(invoiceTotal, false);
	}


	private BillingInvoice populateInvoiceImpl(double invoiceTotal, boolean supportAccounts) {
		BillingInvoiceType invoiceType = new BillingInvoiceType();
		invoiceType.setName("Test");
		invoiceType.setBillingDefinitionRequired(supportAccounts);
		BillingInvoice billingInvoice = new BillingInvoice();
		billingInvoice.setId(1);
		billingInvoice.setTotalAmount(BigDecimal.valueOf(invoiceTotal));
		billingInvoice.setInvoiceType(invoiceType);
		return billingInvoice;
	}


	private BillingPayment populatePaymentForInvoice(BillingInvoice invoice, Double paymentAmountOverride) {
		BillingPayment payment = new BillingPayment();
		payment.setBusinessCompany(invoice.getBusinessCompany());
		payment.setPaymentAmount(paymentAmountOverride != null ? BigDecimal.valueOf(paymentAmountOverride) : invoice.getPayableTotalAmount());
		payment.setPaymentDate(invoice.getInvoiceDate());
		return payment;
	}


	private BillingPaymentInvestmentAccountAllocation populateBillingPaymentInvestmentAccountAllocation(int accountId, BillingInvoice invoice, double accountBillingAmount, double accountPaidAmount) {
		BillingPaymentInvestmentAccountAllocation investmentAccountAllocation = new BillingPaymentInvestmentAccountAllocation();
		investmentAccountAllocation.setId(invoice.getId() + "_" + accountId);
		investmentAccountAllocation.setBillingInvoice(invoice);
		InvestmentAccount account = new InvestmentAccount();
		account.setId(accountId);
		account.setNumber("0000-" + accountId);
		account.setName("Test Account " + accountId);
		investmentAccountAllocation.setInvestmentAccount(account);
		investmentAccountAllocation.setTotalAmount(BigDecimal.valueOf(accountBillingAmount));
		investmentAccountAllocation.setPaidAmount(BigDecimal.valueOf(accountPaidAmount));
		investmentAccountAllocation.setUnpaidAmount(MathUtils.subtract(investmentAccountAllocation.getTotalAmount(), investmentAccountAllocation.getPaidAmount()));
		return investmentAccountAllocation;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
}
