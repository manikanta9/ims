package com.clifton.billing.payment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public class BillingPaymentStatusesTests {


	@Test
	public void testBillingInvoicePaymentStatus_ZeroInvoice() {
		Integer invoiceAmount = null;
		Integer paidAmount = null;

		validateBillingPaymentStatus(BillingPaymentStatuses.FULL, invoiceAmount, paidAmount);
		invoiceAmount = 0;
		validateBillingPaymentStatus(BillingPaymentStatuses.FULL, invoiceAmount, paidAmount);
		paidAmount = 0;
		validateBillingPaymentStatus(BillingPaymentStatuses.FULL, invoiceAmount, paidAmount);
		paidAmount = 10;
		validateBillingPaymentStatus(BillingPaymentStatuses.OVER, invoiceAmount, paidAmount);
	}


	@Test
	public void testBillingInvoicePaymentStatus_NegativeInvoice() {
		Integer invoiceAmount = -10;
		Integer paidAmount = null;

		// Negative invoices we used to allow to just mark paid with no payments allocated to them, now payment allocations MUST be entered for all non-zero invoices
		validateBillingPaymentStatus(BillingPaymentStatuses.NONE, invoiceAmount, paidAmount);
		paidAmount = 0;
		validateBillingPaymentStatus(BillingPaymentStatuses.NONE, invoiceAmount, paidAmount);
		paidAmount = -5;
		validateBillingPaymentStatus(BillingPaymentStatuses.PARTIAL, invoiceAmount, paidAmount);
		paidAmount = -10;
		validateBillingPaymentStatus(BillingPaymentStatuses.FULL, invoiceAmount, paidAmount);
		paidAmount = -20;
		validateBillingPaymentStatus(BillingPaymentStatuses.OVER, invoiceAmount, paidAmount);
		paidAmount = 10;
		validateBillingPaymentStatus(BillingPaymentStatuses.PARTIAL, invoiceAmount, paidAmount);
	}


	@Test
	public void testBillingInvoicePaymentStatus_PositiveInvoice() {
		Integer invoiceAmount = 10;
		Integer paidAmount = null;

		validateBillingPaymentStatus(BillingPaymentStatuses.NONE, invoiceAmount, paidAmount);
		paidAmount = 0;
		validateBillingPaymentStatus(BillingPaymentStatuses.NONE, invoiceAmount, paidAmount);
		paidAmount = 5;
		validateBillingPaymentStatus(BillingPaymentStatuses.PARTIAL, invoiceAmount, paidAmount);
		paidAmount = 10;
		validateBillingPaymentStatus(BillingPaymentStatuses.FULL, invoiceAmount, paidAmount);
		paidAmount = 21;
		validateBillingPaymentStatus(BillingPaymentStatuses.OVER, invoiceAmount, paidAmount);
		paidAmount = -10;
		validateBillingPaymentStatus(BillingPaymentStatuses.PARTIAL, invoiceAmount, paidAmount);
	}

	////////////////////////////////////////////////////////////////////////////////


	private void validateBillingPaymentStatus(BillingPaymentStatuses expectedStatus, Integer totalAmount, Integer allocatedAmount) {
		Assertions.assertEquals(expectedStatus, BillingPaymentStatuses.validateBillingPaymentStatus(totalAmount == null ? null : new BigDecimal(totalAmount), allocatedAmount == null ? null : new BigDecimal(allocatedAmount)));
	}
}
