package com.clifton.billing.invoice.process.schedule.amount.proraters;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class BillingAmountDateProraterConfigTests {

	private static final Date MONTHLY_INVOICE_START_DATE = DateUtils.toDate("01/01/2016");
	private static final Date MONTHLY_INVOICE_END_DATE = DateUtils.toDate("01/31/2016");

	private static final Date QUARTERLY_INVOICE_START_DATE = DateUtils.toDate("01/01/2016");
	private static final Date QUARTERLY_INVOICE_END_DATE = DateUtils.toDate("03/31/2016");

	private static final Date ANNUAL_INVOICE_START_DATE = DateUtils.toDate("07/01/2015");
	private static final Date ANNUAL_INVOICE_END_DATE = DateUtils.toDate("06/30/2016");


	@Test
	public void testDailyProrating() {
		// Monthly Invoice:
		BillingAmountDateProraterConfig proraterConfig = new BillingAmountDateProraterConfig(MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, false, false);
		validateProratedAmount(proraterConfig, MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, 1500, 1500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), MONTHLY_INVOICE_END_DATE, 1500, 823, "Prorated for 17 of 31 days");

		// Quarterly Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, false, false);
		validateProratedAmount(proraterConfig, QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, 4500, 4500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 3808, "Prorated for 77 of 91 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 841, "Prorated for 17 of 91 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("02/01/2016"), DateUtils.toDate("03/15/2016"), 4500, 2176, "Prorated for 44 of 91 days");

		// Annual Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, false, false);
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, 15000, 15000, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), ANNUAL_INVOICE_END_DATE, 4500, 2066, "Prorated for 168 of 366 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), ANNUAL_INVOICE_END_DATE, 4500, 1328, "Prorated for 108 of 366 days");
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, DateUtils.toDate("03/31/2016"), 4500, 3381, "Prorated for 275 of 366 days");
	}


	@Test
	public void testMonthlyProrating_FullMonths() {
		// Monthly Invoice:
		BillingAmountDateProraterConfig proraterConfig = new BillingAmountDateProraterConfig(MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, true, false);
		validateProratedAmount(proraterConfig, MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, 1500, 1500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), MONTHLY_INVOICE_END_DATE, 1500, 1500, null);


		// Quarterly Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, true, false);
		validateProratedAmount(proraterConfig, QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, 4500, 4500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 4500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 1500, "Prorated for 1 month");
		validateProratedAmount(proraterConfig, DateUtils.toDate("02/01/2016"), DateUtils.toDate("03/15/2016"), 4500, 3000, "Prorated for 2 months");

		// Annual Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, true, false);
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, 15000, 15000, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), ANNUAL_INVOICE_END_DATE, 15000, 7500, "Prorated for 6 months");
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), ANNUAL_INVOICE_END_DATE, 15000, 5000, "Prorated for 4 months");
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, DateUtils.toDate("03/31/2016"), 15000, 11250, "Prorated for 9 months");
	}


	@Test
	public void testMonthlyProrating_PartialMonths() {
		// Monthly Invoice:
		BillingAmountDateProraterConfig proraterConfig = new BillingAmountDateProraterConfig(MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, true, true);
		validateProratedAmount(proraterConfig, MONTHLY_INVOICE_START_DATE, MONTHLY_INVOICE_END_DATE, 1500, 1500, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), MONTHLY_INVOICE_END_DATE, 1500, 823, "Prorated for 17 of 31 days");

		// Quarterly Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, true, true);
		validateProratedAmount(proraterConfig, QUARTERLY_INVOICE_START_DATE, QUARTERLY_INVOICE_END_DATE, 4500, 4500, null);

		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 3823, "Prorated for 2 months and 17 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), QUARTERLY_INVOICE_END_DATE, 4500, 823, "Prorated for 17 of 31 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("02/01/2016"), DateUtils.toDate("03/15/2016"), 4500, 2226, "Prorated for 1 month and 15 days");

		// Not Likely to Happen - But if spans across multiple partial months - would just do regular daily calculation:
		validateProratedAmount(proraterConfig, DateUtils.toDate("02/07/2016"), DateUtils.toDate("03/15/2016"), 4500, 1879, "Prorated for 38 of 91 days");


		// Annual Invoice:
		proraterConfig = new BillingAmountDateProraterConfig(ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, true, true);
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, ANNUAL_INVOICE_END_DATE, 15000, 15000, null);
		validateProratedAmount(proraterConfig, DateUtils.toDate("01/15/2016"), ANNUAL_INVOICE_END_DATE, 15000, 6935, "Prorated for 5 months and 17 days");
		validateProratedAmount(proraterConfig, DateUtils.toDate("03/15/2016"), ANNUAL_INVOICE_END_DATE, 15000, 4435, "Prorated for 3 months and 17 days");
		validateProratedAmount(proraterConfig, ANNUAL_INVOICE_START_DATE, DateUtils.toDate("03/31/2016"), 15000, 11250, "Prorated for 9 months");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateProratedAmount(BillingAmountDateProraterConfig proraterConfig, Date prorateStartDate, Date prorateEndDate, double originalAmount, double expectedProratedAmount, String expectedProratedNote) {
		proraterConfig.setProrateStartDate(prorateStartDate);
		proraterConfig.setProrateEndDate(prorateEndDate);

		StringBuilder note = new StringBuilder(16);
		BigDecimal calculatedAmount = proraterConfig.prorateAmount(BigDecimal.valueOf(originalAmount), note);

		Assertions.assertEquals(expectedProratedAmount, MathUtils.round(calculatedAmount, 0).doubleValue(), 0d);
		if (expectedProratedNote == null) {
			Assertions.assertTrue(StringUtils.isEmpty(note.toString()));
		}
		else {
			Assertions.assertEquals(expectedProratedNote, note.toString().trim());
		}
	}
}
