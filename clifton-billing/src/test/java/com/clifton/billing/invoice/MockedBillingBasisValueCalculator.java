package com.clifton.billing.invoice;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BaseBillingBasisValueCalculator;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>MockedBillingBasisCalculator</code> is used for invoice generation tests to make sure
 * invoice amounts and details are properly calculated off of a given billing basis
 *
 * @author Mary Anderson
 */
public class MockedBillingBasisValueCalculator extends BaseBillingBasisValueCalculator {

	private Map<String, Map<Date, BigDecimal>> valueMap;

	private BillingDefinitionService billingDefinitionService;


	@Override
	public boolean isValidValuationDate(Date date) {
		return true;
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		return DateUtils.addDays(date, (moveForward ? 1 : -1));
	}


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {

		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);
		List<BillingBasisSnapshot> valueList = new ArrayList<>();
		Map<String, Map<Date, BigDecimal>> map = getValueMap();
		Date date = startDate;
		while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
			BigDecimal value = getValue(map, billingAccount, date);
			if (value != null) {
				valueList.add(createBillingBasisSnapshot(invoice, billingAccount, date, getValue(map, billingAccount, date)));
			}
			date = DateUtils.addDays(date, 1);
		}
		return valueList;
	}


	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		// For testing purposes - "trade date" specifics are actual dates entered for test data
		Map<String, Map<Date, BigDecimal>> map = getValueMap();
		String accountNumber = billingDefinitionInvestmentAccount.getReferenceTwo().getNumber();
		if (map != null) {
			Map<Date, BigDecimal> accountValues = map.get(billingDefinitionInvestmentAccount.getBillingBasisValuationType().getName() + "_" + accountNumber);
			if (accountValues != null) {
				for (Map.Entry<Date, BigDecimal> accountDateValueMapEntry : accountValues.entrySet()) {
					if (DateUtils.isDateBetween(accountDateValueMapEntry.getKey(), startDate, endDate, false)) {
						if (!currentTradeDateList.contains(accountDateValueMapEntry.getKey())) {
							currentTradeDateList.add(accountDateValueMapEntry.getKey());
						}
					}
				}
			}
		}
		return currentTradeDateList;
	}


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, Date snapshotDate, BigDecimal value) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSnapshotDate(snapshotDate);
		bbs.setSnapshotValue(value);
		return bbs;
	}


	private BigDecimal getValue(Map<String, Map<Date, BigDecimal>> map, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date date) {
		String accountNumber = billingDefinitionInvestmentAccount.getReferenceTwo().getNumber();
		BigDecimal value = BigDecimal.ZERO;
		if (map != null) {
			Map<Date, BigDecimal> accountValues = map.get(billingDefinitionInvestmentAccount.getBillingBasisValuationType().getName() + "_" + accountNumber);
			if (accountValues != null) {
				// Try the exact date
				value = accountValues.get(date);
				if (value == null) {
					if (billingDefinitionInvestmentAccount.getBillingBasisCalculationType().isDateRangeSpecific()) {
						// Specifically for testing when changing accrual frequency to monthly
						if ("359100".equals(billingDefinitionInvestmentAccount.getReferenceTwo().getNumber()) && "07/31/2011".equals(DateUtils.fromDateShort(date))) {
							value = BigDecimal.valueOf(775000000);
						}
						if ("359100".equals(billingDefinitionInvestmentAccount.getReferenceTwo().getNumber()) && "08/31/2011".equals(DateUtils.fromDateShort(date))) {
							return BigDecimal.valueOf(850000000);
						}
					}
					else {
						// Try the First Day of the month - for testing purposes we don't want to set test values for every day
						value = accountValues.get(DateUtils.getFirstDayOfMonth(date));
					}
				}
			}
		}
		return value;
	}


	public Map<String, Map<Date, BigDecimal>> getValueMap() {
		return this.valueMap;
	}


	public void setValueMap(Map<String, Map<Date, BigDecimal>> valueMap) {
		this.valueMap = valueMap;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
