package com.clifton.billing.invoice;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>BillingInvoicePortalStatusTemplateTests</code> are used to test the freemarker template
 * used by the {@link com.clifton.billing.invoice.workflow.BillingInvoicePortalUpdateWorkflowAction} to confirm results
 * are as expected.  Can be useful if we ever want to change the template to ensure it will work in all expected cases.
 *
 * @author manderson
 */
public class BillingInvoicePortalStatusTemplateTests {


	private static final String PORTAL_STATUS_TEMPLATE = "<#if (revisedInvoice.workflowState.name)! != ''>" + //
			"<#if revisedInvoice.workflowStatus.name == 'Open' || revisedInvoice.workflowStatus.name == 'Pending' || revisedInvoice.workflowState.name == 'Approved by Admin'>Under Revision<#else>Void</#if>" + //
			"<#else>" + //
			"<#if invoice.workflowState.name == 'Sent' || invoice.workflowState.name == 'Partially Paid'>Presented for Payment<#elseif invoice.workflowState.name == 'Paid'>Paid<#elseif invoice.workflowState.name == 'Void'>Void</#if>" + //
			"</#if>";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvoicePortalStatus() {
		BillingInvoice invoice = new BillingInvoice();

		// Start with Invoice In Draft - Should never be posted - if it is show nothing?
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Draft", "Open");
		validatePortalStatusTemplate(invoice, null, "");

		// Invalid - Should never be posted - if it is show nothing?
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Invalid", "Open");
		validatePortalStatusTemplate(invoice, null, "");

		// Valid - Should never be posted - if it is show nothing?
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Valid", "Pending");
		validatePortalStatusTemplate(invoice, null, "");

		// Approved - Should never be posted - if it is show nothing?
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Approved", "Pending");
		validatePortalStatusTemplate(invoice, null, "");

		// Approved By Admin - Should never be posted - if it is show nothing?
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Approved by Admin", "Active");
		validatePortalStatusTemplate(invoice, null, "");

		// Sent
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Sent", "Active");
		validatePortalStatusTemplate(invoice, null, "Presented for Payment");

		// Partially Paid
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Partially Paid", "Active");
		validatePortalStatusTemplate(invoice, null, "Presented for Payment");

		// Paid
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Paid", "Active");
		validatePortalStatusTemplate(invoice, null, "Paid");

		// Void with No Revision
		updateBillingInvoiceInWorkflowStateAndStatus(invoice, "Void", "Cancelled");
		validatePortalStatusTemplate(invoice, null, "Void");

		// Void with a Revision
		BillingInvoice correctedInvoice = new BillingInvoice();
		invoice.setParent(correctedInvoice);

		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Draft", "Open");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Under Revision");

		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Invalid", "Open");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Under Revision");

		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Valid", "Pending");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Under Revision");

		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Approved", "Pending");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Under Revision");

		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Approved by Admin", "Active");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Under Revision");

		// Revision is Now Posted
		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Sent", "Active");
		validatePortalStatusTemplate(correctedInvoice, null, "Presented for Payment");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Void");

		// Partially Paid
		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Partially Paid", "Active");
		validatePortalStatusTemplate(correctedInvoice, null, "Presented for Payment");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Void");

		// Paid
		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Paid", "Active");
		validatePortalStatusTemplate(correctedInvoice, null, "Paid");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Void");

		// Void with No Revision
		updateBillingInvoiceInWorkflowStateAndStatus(correctedInvoice, "Void", "Cancelled");
		validatePortalStatusTemplate(correctedInvoice, null, "Void");
		validatePortalStatusTemplate(invoice, correctedInvoice, "Void");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void updateBillingInvoiceInWorkflowStateAndStatus(BillingInvoice invoice, String stateName, String statusName) {
		WorkflowState state = new WorkflowState();
		state.setName(stateName);
		WorkflowStatus status = new WorkflowStatus();
		status.setName(statusName);
		state.setStatus(status);
		invoice.setWorkflowState(state);
		invoice.setWorkflowStatus(status);
	}


	private void validatePortalStatusTemplate(BillingInvoice invoice, BillingInvoice revisedInvoice, String expectedStatusName) {
		TemplateConfig templateConfig = new TemplateConfig(PORTAL_STATUS_TEMPLATE);
		templateConfig.addBeanToContext("invoice", invoice);
		if (revisedInvoice != null) {
			templateConfig.addBeanToContext("revisedInvoice", revisedInvoice);
		}

		FreemarkerTemplateConverter templateConverter = new FreemarkerTemplateConverter();

		String result = templateConverter.convert(templateConfig);
		Assertions.assertEquals(expectedStatusName, result);
	}
}
