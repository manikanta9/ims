package com.clifton.billing.invoice;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.payment.BillingPaymentStatuses;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class BillingInvoiceTests {

	@Test
	public void testGetBillingBasisStartDate() {
		BillingInvoice invoice = new BillingInvoice();
		invoice.setBillingDefinition(new BillingDefinition());
		invoice.getBillingDefinition().setPaymentInAdvance(false);
		invoice.getBillingDefinition().setBillingFrequency(BillingFrequencies.MONTHLY);

		invoice.setInvoiceDate(DateUtils.toDate("11/15/2011"));
		Assertions.assertEquals(DateUtils.toDate("10/16/2011"), invoice.getBillingBasisStartDate());

		invoice.setInvoiceDate(DateUtils.toDate("11/30/2011"));
		Assertions.assertEquals(DateUtils.toDate("11/01/2011"), invoice.getBillingBasisStartDate());

		invoice.setInvoiceDate(DateUtils.toDate("10/31/2011"));
		Assertions.assertEquals(DateUtils.toDate("10/01/2011"), invoice.getBillingBasisStartDate());

		invoice.setInvoiceDate(DateUtils.toDate("02/28/2011"));
		Assertions.assertEquals(DateUtils.toDate("02/01/2011"), invoice.getBillingBasisStartDate());
	}


	@Test
	public void testBillingInvoice_RevenueTypeAmounts() {
		BillingInvoice invoice = new BillingInvoice();
		BillingInvoiceType invoiceType = new BillingInvoiceType();
		invoiceType.setName("Test Revenue");
		invoiceType.setRevenue(true);

		invoice.setInvoiceType(invoiceType);
		invoice.setTotalAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		invoice.setRevenueShareExternalTotalAmount(MathUtils.negate(BigDecimal.TEN));
		invoice.setRevenueShareInternalTotalAmount(MathUtils.negate(BigDecimal.ONE));

		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, invoice.getRevenueGrossTotalAmount());
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, invoice.getTotalAmount());
		Assertions.assertNull(invoice.getOtherTotalAmount());
		Assertions.assertEquals(BigDecimal.valueOf(90), invoice.getRevenueNetTotalAmount());
		Assertions.assertEquals(BigDecimal.valueOf(89), invoice.getRevenueFinalTotalAmount());
		Assertions.assertEquals(BigDecimal.valueOf(90), invoice.getUnpaidAmount());
	}


	@Test
	public void testBillingInvoice_BillingPaymentStatus_RevenueInvoice() {
		BillingInvoice invoice = new BillingInvoice();
		BillingInvoiceType invoiceType = new BillingInvoiceType();
		invoiceType.setName("Test Revenue");
		invoiceType.setRevenue(true);

		invoice.setInvoiceType(invoiceType);
		invoice.setTotalAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		invoice.setRevenueShareExternalTotalAmount(MathUtils.negate(BigDecimal.TEN));
		invoice.setRevenueShareInternalTotalAmount(MathUtils.negate(BigDecimal.ONE));

		Assertions.assertEquals(BillingPaymentStatuses.NONE, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		Assertions.assertEquals(BillingPaymentStatuses.OVER, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(BigDecimal.TEN);
		Assertions.assertEquals(BillingPaymentStatuses.PARTIAL, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(BigDecimal.valueOf(90));
		Assertions.assertEquals(BillingPaymentStatuses.FULL, invoice.getBillingPaymentStatus());
	}


	@Test
	public void testBillingInvoice_BillingPaymentStatus_OtherInvoice() {
		BillingInvoice invoice = new BillingInvoice();
		BillingInvoiceType invoiceType = new BillingInvoiceType();
		invoiceType.setName("Test Non-Revenue");
		invoiceType.setRevenue(false);

		invoice.setInvoiceType(invoiceType);
		invoice.setTotalAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);

		Assertions.assertEquals(BillingPaymentStatuses.NONE, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(new BigDecimal(200));
		Assertions.assertEquals(BillingPaymentStatuses.OVER, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(BigDecimal.TEN);
		Assertions.assertEquals(BillingPaymentStatuses.PARTIAL, invoice.getBillingPaymentStatus());

		invoice.setPaidAmount(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		Assertions.assertEquals(BillingPaymentStatuses.FULL, invoice.getBillingPaymentStatus());
	}
}
