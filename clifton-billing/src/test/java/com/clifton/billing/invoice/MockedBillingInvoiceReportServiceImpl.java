package com.clifton.billing.invoice;


import com.clifton.billing.invoice.report.BillingInvoiceReportServiceImpl;


/**
 * The <code>MockedBillingInvoiceReportServiceImpl</code> overrides the report caching
 * since processing testing doesn't need the actual report setup
 *
 * @author manderson
 */
public class MockedBillingInvoiceReportServiceImpl extends BillingInvoiceReportServiceImpl {

	@Override
	public void clearBillingInvoiceReportCache(@SuppressWarnings("unused") int invoiceId) {
		// DO NOTHING
	}
}
