package com.clifton.billing.invoice.comparison;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingDefinitionServiceImplTests;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.comparisons.BillingInvoicePrerequisiteWorkflowStateComparison;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BillingInvoicePrerequisiteWorkflowStateComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private BillingInvoiceService billingInvoiceService;


	@Resource
	private XmlUpdatableDAO<BillingInvoice> billingInvoiceDAO;

	@Resource
	private XmlUpdatableDAO<BillingInvoiceGroup> billingInvoiceGroupDAO;

	@Resource
	private XmlUpdatableDAO<WorkflowState> workflowStateDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvoice_PrerequisiteInvoiceMissing() {
		BillingDefinition mcknightDefinition = this.billingDefinitionService.getBillingDefinition(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT);
		// Change Mcknight to Monthly
		mcknightDefinition.setBillingFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingDefinition(mcknightDefinition);

		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BillingDefinitionServiceImplTests.BILLING_SCHEDULE_ACUMENT_TIERED));
		dependency.setBillingDefinition(mcknightDefinition);
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		// Create Acument Invoice for 9/30/2012
		BillingInvoice acumentInvoice = createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_ACUMENT, "09/30/2012", "Draft");

		// McKnight invoices which do not exist yet, but comparison not set to false if missing
		BillingInvoicePrerequisiteWorkflowStateComparison comparison = getComparisonBean();
		evaluateResult(comparison, acumentInvoice, true, null);

		// Set to False if Missing
		comparison.setFalseIfMissingPrerequisiteInvoice(true);
		evaluateResult(comparison, acumentInvoice, false, "Missing Prerequisite Invoice(s) for Billing Definition 1:  (05/05/1998 - ) for Invoice Period 07/01/2012 - 09/30/2012");

		// Create One invoice for one of the months
		BillingInvoice mcknightJulyInvoice = createTestInvoice(mcknightDefinition.getId(), "07/31/2012", "Approved");

		evaluateResult(comparison, acumentInvoice, false, "Missing Prerequisite Invoice for Billing Definition 1:  (05/05/1998 - ) on 08/31/2012");

		// Create August and September
		createTestInvoice(mcknightDefinition.getId(), "08/31/2012", "Approved");
		createTestInvoice(mcknightDefinition.getId(), "09/30/2012", "Approved");

		evaluateResult(comparison, acumentInvoice, true, null);
	}


	@Test
	public void testInvoice_PrerequisiteInvoiceMissing_DefinitionInactive() {
		BillingDefinition mcknightDefinition = this.billingDefinitionService.getBillingDefinition(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT);
		// Change Mcknight to Monthly
		mcknightDefinition.setBillingFrequency(BillingFrequencies.MONTHLY);
		// End McKnight in August
		mcknightDefinition.setEndDate(DateUtils.toDate("08/15/2012"));
		this.billingDefinitionService.saveBillingDefinition(mcknightDefinition);

		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BillingDefinitionServiceImplTests.BILLING_SCHEDULE_ACUMENT_TIERED));
		dependency.setBillingDefinition(mcknightDefinition);
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		// Create Acument Invoice for 9/30/2012
		BillingInvoice acumentInvoice = createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_ACUMENT, "09/30/2012", "Draft");

		// McKnight invoices do not exist yet - false if missing
		BillingInvoicePrerequisiteWorkflowStateComparison comparison = getComparisonBean();
		comparison.setFalseIfMissingPrerequisiteInvoice(true);
		evaluateResult(comparison, acumentInvoice, false, "Missing Prerequisite Invoice(s) for Billing Definition 1:  (05/05/1998 - 08/15/2012) for Invoice Period 07/01/2012 - 09/30/2012");

		// Create One invoice for one of the months
		BillingInvoice mcknightJulyInvoice = createTestInvoice(mcknightDefinition.getId(), "07/31/2012", "Approved");
		evaluateResult(comparison, acumentInvoice, false, "Missing Prerequisite Invoice for Billing Definition 1:  (05/05/1998 - 08/15/2012) on 08/31/2012");

		// Create August
		createTestInvoice(mcknightDefinition.getId(), "08/31/2012", "Approved");

		// September is OK to be missing because the definition is not active
		evaluateResult(comparison, acumentInvoice, true, null);

		// Create Next Quarter Acument - No dependency because McKnight is inactive for entire quarter
		// Create Acument Invoice for 12/31/2012
		acumentInvoice = createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_ACUMENT, "12/31/2012", "Draft");
		evaluateResult(comparison, acumentInvoice, true, null);
	}


	@Test
	public void testInvoiceGroup_PrerequisiteInvoiceMissing() {
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BillingDefinitionServiceImplTests.BILLING_SCHEDULE_FAIRFAX_000_RETAINER));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		// Create Fairfax Invoices for 9/30/2012
		int[] fairfaxDefinitionIds = new int[]{BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_000, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_400, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_500};
		BillingInvoiceGroup group = createTestInvoicesInGroup(fairfaxDefinitionIds, "09/30/2012", "Draft");

		// Fairfax Group depends on McKnight which doesn't exist yet, but comparison not set to false if missing
		BillingInvoicePrerequisiteWorkflowStateComparison comparison = getComparisonBean();
		evaluateResult(comparison, group, true, null);

		// Set to False if Missing
		comparison.setFalseIfMissingPrerequisiteInvoice(true);
		evaluateResult(comparison, group, false, "Missing Prerequisite Invoice(s) for Billing Definition 1:  (05/05/1998 - ) for Invoice Period 07/01/2012 - 09/30/2012");

		// Create Missing Invoice - but make it Void so it should be ignored
		createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT, "09/30/2012", "Void");

		evaluateResult(comparison, group, false, "Missing Prerequisite Invoice(s) for Billing Definition 1:  (05/05/1998 - ) for Invoice Period 07/01/2012 - 09/30/2012");
	}


	@Test
	public void testInvoiceGroup_PrerequisiteInvoiceMissingInAllowedWorkflowState() {
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BillingDefinitionServiceImplTests.BILLING_SCHEDULE_FAIRFAX_000_RETAINER));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		// Create Fairfax Invoices for 9/30/2012
		int[] fairfaxDefinitionIds = new int[]{BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_000, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_400, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_500};
		BillingInvoiceGroup group = createTestInvoicesInGroup(fairfaxDefinitionIds, "09/30/2012", "Draft");

		// Create McKnight Invoice in Draft State
		BillingInvoice mcknightInvoice = createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT, "09/30/2012", "Draft");

		BillingInvoicePrerequisiteWorkflowStateComparison comparison = getComparisonBean();
		comparison.setAllowedWorkflowStateNames(CollectionUtils.createList("Approved", "Approved by Admin"));
		comparison.setInvalidStateMessageSuffix(" has not been approved yet");
		evaluateResult(comparison, group, false, "Prerequisite Invoice # 4 1:  (05/05/1998 - )   has not been approved yet");

		// Change Mcknight Invoice to Approved
		mcknightInvoice.setWorkflowState(this.workflowStateDAO.findOneByField("name", "Approved"));
		mcknightInvoice.setWorkflowStatus(mcknightInvoice.getWorkflowState().getStatus());
		this.billingInvoiceDAO.save(mcknightInvoice);

		evaluateResult(comparison, group, true, "All prerequisite invoices validated against allowed and/or disallowed states successfully.");
	}


	@Test
	public void testInvoiceGroup_PrerequisiteInvoiceMissingInDisallowedWorkflowState() {
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BillingDefinitionServiceImplTests.BILLING_SCHEDULE_FAIRFAX_000_RETAINER));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		// Create Fairfax Invoices for 9/30/2012
		int[] fairfaxDefinitionIds = new int[]{BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_000, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_400, BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_500};
		BillingInvoiceGroup group = createTestInvoicesInGroup(fairfaxDefinitionIds, "09/30/2012", "Draft");

		// Create McKnight Invoice in Draft State
		BillingInvoice mcknightInvoice = createTestInvoice(BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT, "09/30/2012", "Draft");

		BillingInvoicePrerequisiteWorkflowStateComparison comparison = getComparisonBean();
		comparison.setDisallowedWorkflowStateNames(CollectionUtils.createList("Approved", "Approved by Admin"));
		evaluateResult(comparison, group, true, "All prerequisite invoices validated against allowed and/or disallowed states successfully.");

		// Change Mcknight Invoice to Approved
		mcknightInvoice.setWorkflowState(this.workflowStateDAO.findOneByField("name", "Approved"));
		mcknightInvoice.setWorkflowStatus(mcknightInvoice.getWorkflowState().getStatus());
		this.billingInvoiceDAO.save(mcknightInvoice);

		evaluateResult(comparison, group, false, "Prerequisite Invoice # " + mcknightInvoice.getId() + " 1:  (05/05/1998 - )  is in invalid state Approved");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BillingInvoicePrerequisiteWorkflowStateComparison getComparisonBean() {
		BillingInvoicePrerequisiteWorkflowStateComparison comparison = new BillingInvoicePrerequisiteWorkflowStateComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	private void evaluateResult(BillingInvoicePrerequisiteWorkflowStateComparison comparisonBean, IdentityObject bean, boolean expectedResult, String expectedMessage) {
		SimpleComparisonContext context = (expectedMessage == null ? null : new SimpleComparisonContext());
		boolean result = comparisonBean.evaluate(bean, context);
		Assertions.assertEquals(expectedResult, result);
		if (expectedMessage != null) {
			if (expectedResult) {
				Assertions.assertEquals(expectedMessage, context.getTrueMessage());
			}
			else {
				Assertions.assertEquals(expectedMessage, context.getFalseMessage());
			}
		}
	}


	private BillingInvoice createTestInvoice(int billingDefinitionId, String date, String workflowState) {
		BillingDefinition billingDefinition = this.billingDefinitionService.getBillingDefinition(billingDefinitionId);

		BillingInvoice invoice = new BillingInvoice();
		invoice.setBillingDefinition(billingDefinition);
		invoice.setInvoiceDate(DateUtils.toDate(date));
		invoice.setInvoicePeriodEndDate(invoice.getInvoiceDate());
		invoice.setInvoicePeriodStartDate(DateUtils.addMonths(DateUtils.addDays(invoice.getInvoiceDate(), 1), -billingDefinition.getBillingFrequency().getMonthsInPeriod()));
		invoice.setWorkflowState(this.workflowStateDAO.findOneByField("name", workflowState));
		invoice.setWorkflowStatus(invoice.getWorkflowState().getStatus());
		invoice.setTotalAmount(BigDecimal.valueOf(10000 * billingDefinitionId));
		invoice.setInvoiceType(this.billingInvoiceService.getBillingInvoiceType((short) 1));
		return this.billingInvoiceDAO.save(invoice);
	}


	private BillingInvoiceGroup createTestInvoicesInGroup(int[] billingDefinitionIds, String date, String workflowState) {
		BillingInvoiceGroup group = new BillingInvoiceGroup();
		group.setInvoiceDate(DateUtils.toDate(date));
		group.setWorkflowState(this.workflowStateDAO.findOneByField("name", workflowState));
		group.setWorkflowStatus(group.getWorkflowState().getStatus());
		group = this.billingInvoiceGroupDAO.save(group);

		for (int billingDefinitionId : billingDefinitionIds) {
			BillingDefinition billingDefinition = this.billingDefinitionService.getBillingDefinition(billingDefinitionId);
			BillingInvoice invoice = new BillingInvoice();
			invoice.setBillingDefinition(billingDefinition);
			invoice.setInvoiceDate(group.getInvoiceDate());
			invoice.setInvoicePeriodEndDate(invoice.getInvoiceDate());
			invoice.setInvoicePeriodStartDate(DateUtils.addMonths(DateUtils.addDays(invoice.getInvoiceDate(), 1), -billingDefinition.getBillingFrequency().getMonthsInPeriod()));
			invoice.setWorkflowState(group.getWorkflowState());
			invoice.setWorkflowStatus(group.getWorkflowStatus());
			invoice.setTotalAmount(BigDecimal.valueOf(10000 * billingDefinitionId));
			invoice.setInvoiceType(this.billingInvoiceService.getBillingInvoiceType((short) 1));
			invoice.setInvoiceGroup(group);
			this.billingInvoiceDAO.save(invoice);
		}
		return group;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
