package com.clifton.billing.invoice.process.schedule.waive;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.ComparisonTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;


/**
 * The <code>BillingBasisAmountWaiveConditionTests</code> tests the logic of the waive condition given a previously determined map of dates to billing basis values
 * For fuller testing - there are integration tests that will be setup for invoices that use the condition
 *
 * @author manderson
 */
public class BillingBasisSnapshotAmountWaiveConditionTests {


	@Test
	public void testEvaluateForBillingBasisSnapshotMap() {
		BillingBasisSnapshotAmountWaiveCondition condition = new BillingBasisSnapshotAmountWaiveCondition();
		// We aren't using any of the data retrievals so we don't need to wire it or add the accounts we are using

		Map<Date, BigDecimal> allZeroMap = getTestDateValueMap(DateUtils.toDate("10/01/2017"), "0", "0", "0", "0");
		Map<Date, BigDecimal> allNonZeroMap = getTestDateValueMap(DateUtils.toDate("10/01/2017"), "10000", "11000", "120000", "130000");
		Map<Date, BigDecimal> zeroAndNonZeroMap = getTestDateValueMap(DateUtils.toDate("10/01/2017"), "10000", "11000", "0", "130000");

		// Test 1: Return True if All Values are Zero
		condition.setComparisonType(ComparisonTypes.EQUAL_TO);
		condition.setAmount(BigDecimal.ZERO);
		validateResult(true, "Billing Basis Snapshot Total on all dates are = 0.00", condition, allZeroMap);
		validateResult(false, "Billing Basis Snapshot Total [10,000.00] on 10/01/2017 is not = [0.00].", condition, allNonZeroMap);
		validateResult(false, "Billing Basis Snapshot Total [10,000.00] on 10/01/2017 is not = [0.00].", condition, zeroAndNonZeroMap);

		// Test 2: Return True if Any Value Is Zero
		condition.setEvaluateToTrueIfAnyDateTrue(true);
		validateResult(true, "Billing Basis Snapshot Total [0.00] on 10/01/2017 is = 0.00", condition, allZeroMap);
		validateResult(false, "Billing Basis Snapshot Total on all dates are not = [0.00].", condition, allNonZeroMap);
		validateResult(true, "Billing Basis Snapshot Total [0.00] on 10/03/2017 is = 0.00", condition, zeroAndNonZeroMap);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Map<Date, BigDecimal> getTestDateValueMap(Date startDate, String... values) {
		Map<Date, BigDecimal> dateValueMap = new TreeMap<>();
		Date date = startDate;
		for (String value : values) {
			dateValueMap.put(date, new BigDecimal(value));
			date = DateUtils.addDays(date, 1);
		}
		return dateValueMap;
	}


	private void validateResult(boolean expected, String expectedMsg, BillingBasisSnapshotAmountWaiveCondition condition, Map<Date, BigDecimal> dateValueMap) {
		SimpleComparisonContext context = (expectedMsg == null ? null : new SimpleComparisonContext());
		boolean actual = condition.evaluateForBillingBasisSnapshotMap(dateValueMap, context);
		Assertions.assertEquals(expected, actual);
		if (expectedMsg != null) {
			String actualMsg = expected ? context.getTrueMessage() : context.getFalseMessage();
			Assertions.assertEquals(expectedMsg, actualMsg);
		}
	}
}
