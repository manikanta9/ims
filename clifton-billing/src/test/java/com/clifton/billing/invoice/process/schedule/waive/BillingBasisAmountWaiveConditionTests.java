package com.clifton.billing.invoice.process.schedule.waive;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.ComparisonTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BillingBasisAmountWaiveConditionTests</code> ...
 *
 * @author manderson
 */
public class BillingBasisAmountWaiveConditionTests {

	@Test
	public void testEvaluateGreaterThan() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, ComparisonTypes.GREATER_THAN, 25000.0);
		validateConditionResult(true, "Test Account 1 Billing Basis Amount [50,000.00] is > [25,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.GREATER_THAN, 25000.0);
		validateConditionResult(false, "Test Account 2 Billing Basis Amount [10,000.00] is not > [25,000.00].", condition, schedule);

		condition = setupCondition("Test Accounts 1 & 2", new Integer[]{1, 2}, ComparisonTypes.GREATER_THAN, 50000.0);
		validateConditionResult(true, "Test Accounts 1 & 2 Billing Basis Amount [60,000.00] is > [50,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.GREATER_THAN, 50000.0);
		validateConditionResult(false, "Test Account 3 Billing Basis Amount [0.00] is not > [50,000.00].", condition, schedule);
	}


	@Test
	public void testEvaluateGreaterThanOrEqual() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, ComparisonTypes.GREATER_THAN_OR_EQUAL_TO, 50000.0);
		validateConditionResult(true, "Test Account 1 Billing Basis Amount [50,000.00] is >= [50,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.GREATER_THAN_OR_EQUAL_TO, 5000.0);
		validateConditionResult(true, "Test Account 2 Billing Basis Amount [10,000.00] is >= [5,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.GREATER_THAN_OR_EQUAL_TO, 25000.0);
		validateConditionResult(false, "Test Account 2 Billing Basis Amount [10,000.00] is not >= [25,000.00].", condition, schedule);

		condition = setupCondition("Test Accounts 1 & 2", new Integer[]{1, 2}, ComparisonTypes.GREATER_THAN_OR_EQUAL_TO, 50000.0);
		validateConditionResult(true, "Test Accounts 1 & 2 Billing Basis Amount [60,000.00] is >= [50,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.GREATER_THAN_OR_EQUAL_TO, 50000.0);
		validateConditionResult(false, "Test Account 3 Billing Basis Amount [0.00] is not >= [50,000.00].", condition, schedule);
	}


	@Test
	public void testEvaluateLessThan() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, ComparisonTypes.LESS_THAN, 25000.0);
		validateConditionResult(false, "Test Account 1 Billing Basis Amount [50,000.00] is not < [25,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.LESS_THAN, 25000.0);
		validateConditionResult(true, "Test Account 2 Billing Basis Amount [10,000.00] is < [25,000.00].", condition, schedule);

		condition = setupCondition("Test Accounts 1 & 2", new Integer[]{1, 2}, ComparisonTypes.LESS_THAN, 75000.0);
		validateConditionResult(true, "Test Accounts 1 & 2 Billing Basis Amount [60,000.00] is < [75,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.LESS_THAN, 50000.0);
		validateConditionResult(true, "Test Account 3 Billing Basis Amount [0.00] is < [50,000.00].", condition, schedule);
	}


	@Test
	public void testEvaluateLessThanOrEqual() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, ComparisonTypes.LESS_THAN_OR_EQUAL_TO, 25000.0);
		validateConditionResult(false, "Test Account 1 Billing Basis Amount [50,000.00] is not <= [25,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.LESS_THAN_OR_EQUAL_TO, 25000.0);
		validateConditionResult(true, "Test Account 2 Billing Basis Amount [10,000.00] is <= [25,000.00].", condition, schedule);

		condition = setupCondition("Test Accounts 1 & 2", new Integer[]{1, 2}, ComparisonTypes.LESS_THAN_OR_EQUAL_TO, 60000.0);
		validateConditionResult(true, "Test Accounts 1 & 2 Billing Basis Amount [60,000.00] is <= [60,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.LESS_THAN_OR_EQUAL_TO, 50000.0);
		validateConditionResult(true, "Test Account 3 Billing Basis Amount [0.00] is <= [50,000.00].", condition, schedule);
	}


	@Test
	public void testEvaluateEqual() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, ComparisonTypes.EQUAL_TO, 25000.0);
		validateConditionResult(false, "Test Account 1 Billing Basis Amount [50,000.00] is not = [25,000.00].", condition, schedule);

		condition = setupCondition("Test Account 2", new Integer[]{2}, ComparisonTypes.EQUAL_TO, 10000.0);
		validateConditionResult(true, "Test Account 2 Billing Basis Amount [10,000.00] is = [10,000.00].", condition, schedule);

		condition = setupCondition("Test Accounts 1 & 2", new Integer[]{1, 2}, ComparisonTypes.EQUAL_TO, 75000.0);
		validateConditionResult(false, "Test Accounts 1 & 2 Billing Basis Amount [60,000.00] is not = [75,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.EQUAL_TO, 50000.0);
		validateConditionResult(false, "Test Account 3 Billing Basis Amount [0.00] is not = [50,000.00].", condition, schedule);

		condition = setupCondition("Test Account 3", new Integer[]{3}, ComparisonTypes.EQUAL_TO, 0.0);
		validateConditionResult(true, "Test Account 3 Billing Basis Amount [0.00] is = [0.00].", condition, schedule);
	}


	@Test
	public void testEvaluateMissingComparisonType() {
		BillingSchedule schedule = setupSchedule();
		addAccountAmountToBillingBasis(schedule, 1, 50000.00);
		addAccountAmountToBillingBasis(schedule, 2, 10000.00);

		BillingBasisAmountWaiveCondition condition = setupCondition("Test Account 1", new Integer[]{1}, null, 25000.0);
		try {
			condition.evaluate(schedule, null);
		}
		catch (Exception e) {
			Assertions.assertEquals("Unable to compare billing amount [50,000.00] to [25,000.00].  Missing comparison type selection.", e.getMessage());
			return;
		}
		Assertions.fail("Expected invalid condition string to throw an exception.");
	}


	//////////////////////////////////////////
	//////////////////////////////////////////


	private BillingBasisAmountWaiveCondition setupCondition(String accountDesc, Integer[] accountIds, ComparisonTypes comparisonType, double amount) {
		BillingBasisAmountWaiveCondition condition = new BillingBasisAmountWaiveCondition();
		condition.setBillingBasisDescription(accountDesc);
		condition.setBillingDefinitionInvestmentAccountIds(Arrays.asList(accountIds));
		condition.setComparisonType(comparisonType);
		condition.setAmount(BigDecimal.valueOf(amount));
		return condition;
	}


	private BillingSchedule setupSchedule() {
		BillingSchedule schedule = new BillingSchedule();
		BillingInvoice invoice = new BillingInvoice();
		Map<BillingDefinitionInvestmentAccount, BillingBasis> billingBasisMap = new HashMap<>();
		BillingScheduleWaiveConditionConfig config = new BillingScheduleWaiveConditionConfig(invoice, billingBasisMap);
		schedule.setWaiveSystemConditionConfig(config);
		return schedule;
	}


	private void addAccountAmountToBillingBasis(BillingSchedule schedule, int billingDefinitionInvestmentAccountId, double amount) {
		BillingBasis basis = new BillingBasis();
		basis.addBillingBasis(DateUtils.toDate("01/01/2014"), DateUtils.toDate("03/31/2014"), BigDecimal.valueOf(amount));
		BillingDefinitionInvestmentAccount billingAccount = new BillingDefinitionInvestmentAccount();
		billingAccount.setId(billingDefinitionInvestmentAccountId);
		schedule.getWaiveSystemConditionConfig().getBillingBasisMap().put(billingAccount, basis);
	}


	private void validateConditionResult(boolean expected, String expectedMsg, BillingBasisAmountWaiveCondition condition, BillingSchedule schedule) {
		SimpleComparisonContext context = new SimpleComparisonContext();
		boolean actual = condition.evaluate(schedule, context);
		Assertions.assertEquals(expected, actual);
		String actualMsg = expected ? context.getTrueMessage() : context.getFalseMessage();
		Assertions.assertEquals(expectedMsg, actualMsg);
	}
}
