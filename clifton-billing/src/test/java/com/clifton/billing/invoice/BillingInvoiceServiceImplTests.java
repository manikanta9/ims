package com.clifton.billing.invoice;

import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingDefinitionServiceImplTests;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingInvoiceServiceTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BillingInvoiceServiceImplTests {

	@Resource
	private BillingBasisService billingBasisService;

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private BillingInvoiceService billingInvoiceService;

	@Resource
	private BillingInvoiceProcessService billingInvoiceProcessService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private ReadOnlyDAO<InvestmentAccount> investmentAccountDAO;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	@Resource
	private RuleViolationService ruleViolationService;

	private static final Short BILLING_INVOICE_TYPE_PENNIES = 3;

	private static final Short BILLING_AGGREGATION_TYPE_CLIENT_ACCOUNT = 4;

	private static final Integer BILLING_DEFINITION_MCKNIGHT = BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCKNIGHT;
	private static final Integer BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE;

	private static final Integer BILLING_DEFINITION_GLOBAL_ENDOWMENT = BillingDefinitionServiceImplTests.BILLING_DEFINITION_GLOBAL_ENDOWMENT;

	private static final Integer BILLING_DEFINITION_FAIRFAX_000 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_000;
	private static final Integer BILLING_SCHEDULE_FAIRFAX_000_RETAINER = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_FAIRFAX_000_RETAINER;
	private static final Integer BILLING_DEFINITION_FAIRFAX_500 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_500;
	private static final Integer BILLING_DEFINITION_FAIRFAX_400 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_FAIRFAX_400;
	private static final Integer BILLING_SCHEDULE_FAIRFAX_SHARED = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_FAIRFAX_SHARED;

	private static final Integer BILLING_DEFINITION_ACUMENT = BillingDefinitionServiceImplTests.BILLING_DEFINITION_ACUMENT;
	private static final Integer BILLING_SCHEDULE_ACUMENT_TIERED = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_ACUMENT_TIERED;

	private static final Integer BILLING_DEFINITION_KIMBERLY_CLARK_359100 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_KIMBERLY_CLARK_359100;
	private static final Integer BILLING_SCHEDULE_KIMBERLY_CLARK_PERCENTAGE_FEE = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_KIMBERLY_CLARK_PERCENTAGE_FEE;

	private static final Integer BILLING_DEFINITION_NEW_ORLEANS_FF_454000 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_NEW_ORLEANS_FF_454000;
	private static final Integer BILLING_DEFINITION_CONTINENTAL_125300 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_CONTINENTAL_125300;
	private static final Integer BILLING_SCHEDULE_CONTINENTAL_DISCOUNT = BillingDefinitionServiceImplTests.BILLING_SCHEDULE_CONTINENTAL_DISCOUNT;
	private static final Integer BILLING_DEFINITION_WILDER_713450 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_WILDER_713450;
	private static final Integer BILLING_DEFINITION_NC_BAPTIST = BillingDefinitionServiceImplTests.BILLING_DEFINITION_NC_BAPTIST;
	private static final Integer BILLING_DEFINITION_MCGOWAN = BillingDefinitionServiceImplTests.BILLING_DEFINITION_MCGOWAN;

	private static final Integer BILLING_DEFINITION_DELTA_MASTER_159900 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_DELTA_MASTER_159900;
	private static final Integer BILLING_DEFINITION_DELTA_PILOTS_159700 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_DELTA_PILOTS_159700;
	private static final Integer BILLING_DEFINITION_NORTHWEST_159800 = BillingDefinitionServiceImplTests.BILLING_DEFINITION_NORTHWEST_159800;

	private static final Integer BILLING_DEFINITION_DORIS_DUKE = BillingDefinitionServiceImplTests.BILLING_DEFINITION_DORIS_DUKE;
	private static final Integer BILLING_DEFINITION_DUKE_ENERGY = BillingDefinitionServiceImplTests.BILLING_DEFINITION_DUKE_ENERGY;

	private static final Integer BILLING_DEFINITION_TARGET = BillingDefinitionServiceImplTests.BILLING_DEFINITION_TARGET;

	private static final Integer BILLING_DEFINITION_SMITH_FAMILY = BillingDefinitionServiceImplTests.BILLING_DEFINITION_SMITH_FAMILY;
	private static final Integer BILLING_DEFINITION_SMITH_SUSAN = BillingDefinitionServiceImplTests.BILLING_DEFINITION_SMITH_SUSAN;

	private static final Integer BILLING_DEFINITION_BLUE_ROCK = 140;

	private static final Integer BILLING_DEFINITION_MERRILL = 150;

	private static final Date INVOICE_PERIOD_START_DATE = DateUtils.toDate("07/01/2011");
	private static final Date INVOICE_PERIOD_END_DATE = DateUtils.toDate("09/30/2011");
	private static final Date ADVANCE_INVOICE_PERIOD_START_DATE = DateUtils.toDate("10/01/2011");
	private static final Date ADVANCE_INVOICE_PERIOD_END_DATE = DateUtils.toDate("12/31/2011");
	private static final Date INVOICE_DATE = DateUtils.toDate("09/30/2011");

	private static final Integer ANNUAL_DAYS = BillingFrequencies.ANNUALLY.getDaysInPeriod();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupCalendar() {
		// Setup Once
		short yr = 2011;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		yr = 2012;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		yr = 2013;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
	}


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	//               INVOICE TYPE VALIDATION TESTS                  //
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	@Test
	public void testInvoiceTypeInsertUpdateDelete() {
		BillingInvoiceType type = new BillingInvoiceType();
		type.setName("Test New Invoice Type");
		type.setDescription("Test New Invoice Type");

		// Insert it
		this.billingInvoiceService.saveBillingInvoiceType(type);

		// Update it
		type.setName("Test New Invoice Type - Name Updated");
		type.setRevenue(true);
		this.billingInvoiceService.saveBillingInvoiceType(type);

		// Delete it
		this.billingInvoiceService.deleteBillingInvoiceType(type.getId());
	}


	@Test
	public void testInsertSystemDefined() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BillingInvoiceType type = new BillingInvoiceType();
			type.setName("Test New Invoice Type");
			type.setDescription("Test New Invoice Type");
			type.setBillingDefinitionRequired(true);
			type.setSystemDefined(true);
			// Insert it - Should Fail
			this.billingInvoiceService.saveBillingInvoiceType(type);
		});
	}


	@Test
	public void testUpdateBillingDefinitionRequired() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BillingInvoiceType type = this.billingInvoiceService.getBillingInvoiceType(MathUtils.SHORT_TWO);
			AssertUtils.assertFalse(type.isBillingDefinitionRequired(), "Legal Fee Invoice Type should NOT be billing definition required.");
			type.setBillingDefinitionRequired(true);
			// Update it - Should Fail
			this.billingInvoiceService.saveBillingInvoiceType(type);
		});
	}


	@Test
	public void testUpdateInvoiceTypeSystemDefined() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BillingInvoiceType type = this.billingInvoiceService.getBillingInvoiceTypeByName("Management Fee");
			type.setName("Management Fee 2");
			this.billingInvoiceService.saveBillingInvoiceType(type);
		});
	}


	@Test
	public void testDeleteInvoiceTypeSystemDefined() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BillingInvoiceType type = this.billingInvoiceService.getBillingInvoiceTypeByName("Management Fee");
			// Delete it - should fail
			this.billingInvoiceService.deleteBillingInvoiceType(type.getId());
		});
	}


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	//   MANAGEMENT FEE INVOICE GENERATION AND PROCESSING TESTS     //
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	@Test
	public void testFlatFeeInvoices() {

		// Standard Flat Fee - defined monthly, billed quarterly
		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15000.00), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		BillingInvoiceDetail detail = invoice_mcknight.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(15000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
				CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_15,000.00"));

		// Re-testing in advance, so delete it
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());

		// Standard Flat Fee - defined quarterly, billed quarterly
		BillingInvoice invoice_global = generateBillingInvoice(BILLING_DEFINITION_GLOBAL_ENDOWMENT, INVOICE_DATE);
		validateInvoiceSummary(invoice_global, BILLING_DEFINITION_GLOBAL_ENDOWMENT, INVOICE_DATE, BigDecimal.valueOf(12500.00), null);
		validateInvoiceDates(invoice_global, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_global, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_global, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), DateUtils.toDate("12/31/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice_global, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_global.getDetailList()));
		detail = invoice_global.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(12500.00), "$ 12,500 per quarter" + StringUtils.NEW_LINE,
				CollectionUtils.createList("123456: Global Endowment_BillingBasis_0_Percentage_100_BillingAmount_12,500.00"));

		// DUPLICATE INVOICE - SHOULD FAIL BECAUSE ORIGINAL WASN'T CANCELLED
		try {
			generateBillingInvoice(BILLING_DEFINITION_GLOBAL_ENDOWMENT, INVOICE_DATE);
		}
		catch (ValidationException e) {
			Assertions.assertEquals(
					"Cannot create a new Invoice for [Management Fee: 2:  (11/10/2008 - ) 09/30/2011 because there is already one in the system that is not currently Voided/Cancelled.  Please void the original invoice before creating a new invoice for the same billing definition/invoice period.",
					e.getMessage());
			return;
		}
		Assertions.fail("Duplicate Invoice Validation Exception Expected");
	}


	@Test
	public void testFlatFeeInvoices_MonthlyAccrual() {
		// Standard Flat Fee - defined monthly, billed quarterly, accrued monthly
		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE);
		schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15000.00), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(3, CollectionUtils.getSize(invoice_mcknight.getDetailList()));

		for (BillingInvoiceDetail detail : invoice_mcknight.getDetailList()) {
			if (detail.getAccrualStartDate() != null) {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(5000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
							CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_5,000.00"));
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(5000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
							CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_5,000.00"));
				}
				else {
					validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.ZERO, BigDecimal.valueOf(5000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
							CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_5,000.00"));
				}
			}
			else {
				Assertions.fail("Expected all invoice details to have accrual dates set.");
			}
		}

		// Re-testing in advance, so delete it
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());
	}


	@Test
	public void testFlatFeeInvoices_Prorated() {
		// Standard Flat Fee - defined monthly, billed quarterly
		// Set End Date For Prorating
		Date endDate = DateUtils.toDate("08/29/2011");
		setBillingDefinitionEndDate(BILLING_DEFINITION_MCKNIGHT, endDate, false);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(9783.00), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", INVOICE_PERIOD_START_DATE, endDate, 60);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, endDate, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		BillingInvoiceDetail detail = invoice_mcknight.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(9783.00), "$ 5,000 per month" + StringUtils.NEW_LINE + "Prorated for 60 of 92 days" + StringUtils.NEW_LINE,
				CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_9,783.00"));

		// Remove Invoice and Reset End Date for other tests
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());
		setBillingDefinitionEndDate(BILLING_DEFINITION_MCKNIGHT, null, false);
	}


	@Test
	public void testFlatFeeInvoices_Prorated_MonthlyAccrual() {
		// Standard Flat Fee - defined monthly, billed quarterly, accrue monthly
		// Set End Date For Prorating
		Date endDate = DateUtils.toDate("08/29/2011");
		setBillingDefinitionEndDate(BILLING_DEFINITION_MCKNIGHT, endDate, false);

		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE);
		schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(9677), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", INVOICE_PERIOD_START_DATE, endDate, 60);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, endDate, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_mcknight.getDetailList()));

		for (BillingInvoiceDetail detail : invoice_mcknight.getDetailList()) {
			if (detail.getAccrualStartDate() != null) {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(5000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
							CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_5,000.00"));
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(4677), "$ 5,000 per month" + StringUtils.NEW_LINE + "Prorated for 29 of 31 days" + StringUtils.NEW_LINE,
							CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_4,677.00"));
				}
			}
			else {
				Assertions.fail("Expected all invoice details to have accrual dates set.");
			}
		}

		// Remove Invoice and Reset End Date for other tests
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());
		setBillingDefinitionEndDate(BILLING_DEFINITION_MCKNIGHT, null, false);
	}


	@Test
	public void testFlatFeeInvoices_InAdvance() {
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCKNIGHT);
		def.setPaymentInAdvance(true);
		this.billingDefinitionService.saveBillingDefinition(def);

		// Standard Flat Fee - defined monthly, billed quarterly
		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15000.00), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		BillingInvoiceDetail detail = invoice_mcknight.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(15000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
				CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_15,000.00"));

		// Remove Invoice for other tests
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());
	}


	@Test
	public void testFlatFeeInvoices_InAdvance_Prorated() {
		Date endDate = DateUtils.toDate("11/15/2011");
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCKNIGHT);
		def.setPaymentInAdvance(true);
		// Set End Date In "Future" because In Advance
		def.setEndDate(endDate);
		this.billingDefinitionService.saveBillingDefinition(def);

		// Standard Flat Fee - defined monthly, billed quarterly
		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(7500.00), null);
		validateInvoiceDates(invoice_mcknight, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_mcknight, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, endDate, 46);
		validateInvoiceDates(invoice_mcknight, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice_mcknight, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		BillingInvoiceDetail detail = invoice_mcknight.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(7500.00), "$ 5,000 per month" + StringUtils.NEW_LINE + "Prorated for 46 of 92 days" + StringUtils.NEW_LINE,
				CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_7,500.00"));

		// Remove Invoice & Reset Definition for other tests
		this.billingInvoiceService.deleteBillingInvoice(invoice_mcknight.getId());

		def.setPaymentInAdvance(false);
		def.setEndDate(null);
		this.billingDefinitionService.saveBillingDefinition(def);
	}


	@Test
	public void testSharedScheduleTieredInvoices() {
		// Shared Schedule with additional quarterly retainer flat fee
		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());

		// Generating ONE should have generated them all
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(77936), group);
		validateInvoiceDates(invoice000, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice000, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {

			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(74936), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 76.71 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_74,936.00"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(18453), group, 2);
		validateInvoiceDates(invoice500, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "annualInvoicePeriod", DateUtils.toDate("10/01/2010"), DateUtils.toDate("09/30/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice500, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_12,913.54");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_4,289.46");
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(17203), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 17.61 % of total fees", accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee (prorated for late start)
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(6644), group);
		validateInvoiceDates(invoice400, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "invoiceBilling", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, 81);
		validateInvoiceDates(invoice400, "annualInvoicePeriod", DateUtils.toDate("07/12/2011"), DateUtils.toDate("09/30/2012"), 447);
		validateInvoiceDates(invoice400, "billingBasis", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1101), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,101.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(5543), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 5.68 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_5,543.00"));
			}
		}

		// Delete One - Should fail because you have to explicitly delete at the group level
		boolean exceptionFound = false;
		try {
			this.billingInvoiceService.deleteBillingInvoice(invoice400.getId());
		}
		catch (ValidationException e) {
			exceptionFound = true;
			Assertions.assertEquals("Invoice # " + invoice400.getId() + " is a part of Invoice Group # " + invoice400.getInvoiceGroup().getId()
					+ ".  You cannot delete a single invoice in a group, but must delete the entire invoice group.", e.getMessage());

			this.billingInvoiceService.deleteBillingInvoiceGroup(invoice400.getInvoiceGroup().getId());

			Assertions.assertNull(this.billingInvoiceService.getBillingInvoice(invoice000.getId()), "Invoice 000 should have been deleted automatically");
			Assertions.assertNull(this.billingInvoiceService.getBillingInvoice(invoice500.getId()), "Invoice 500 should have been deleted automatically");
			Assertions.assertNull(this.billingInvoiceService.getBillingInvoice(invoice400.getId()), "Invoice 400 should have been deleted automatically");
			Assertions.assertNull(this.billingInvoiceService.getBillingInvoiceGroup(group.getId()), "Invoice Group should have been deleted automatically");
		}
		Assertions.assertTrue(exceptionFound, "Validation Exception should have been thrown when trying to delete a single invoice that is part of a group");
	}


	@Test
	public void testSharedScheduleTieredInvoices_Pennies() {
		// Change Invoice Type on ALL Billing Definitions to use the Pennies One
		updateBillingDefinitionInvoiceType(BILLING_DEFINITION_FAIRFAX_000, BILLING_INVOICE_TYPE_PENNIES);
		updateBillingDefinitionInvoiceType(BILLING_DEFINITION_FAIRFAX_500, BILLING_INVOICE_TYPE_PENNIES);
		updateBillingDefinitionInvoiceType(BILLING_DEFINITION_FAIRFAX_400, BILLING_INVOICE_TYPE_PENNIES);

		// Shared Schedule with additional quarterly retainer flat fee
		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());

		// Generating ONE should have generated them all
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(77935.74), group);
		validateInvoiceDates(invoice000, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice000, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {

			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(74935.74), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,936.67 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 76.71 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_74,935.74"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(18452.53), group, 2);
		validateInvoiceDates(invoice500, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "annualInvoicePeriod", DateUtils.toDate("10/01/2010"), DateUtils.toDate("09/30/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice500, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_12,913.18");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_4,289.35");
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(17202.53), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 531,453,936.67 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 17.61 % of total fees", accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee (prorated for late start)
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(6644.01), group);
		validateInvoiceDates(invoice400, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "invoiceBilling", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, 81);
		validateInvoiceDates(invoice400, "annualInvoicePeriod", DateUtils.toDate("07/12/2011"), DateUtils.toDate("09/30/2012"), 447);
		validateInvoiceDates(invoice400, "billingBasis", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1100.54), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,100.54"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(5543.47), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,936.67 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 5.68 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_5,543.47"));
			}
		}
	}


	@Test
	public void testSharedScheduleTieredInvoices_PenniesAndDollars() {
		// Use Pennies Only for 400 account
		updateBillingDefinitionInvoiceType(BILLING_DEFINITION_FAIRFAX_400, BILLING_INVOICE_TYPE_PENNIES);

		// Shared Schedule with additional quarterly retainer flat fee
		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());

		// Generating ONE should have generated them all
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(77936), group);
		validateInvoiceDates(invoice000, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice000, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {

			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(74936), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 76.71 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_74,936.00"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(18453), group, 2);
		validateInvoiceDates(invoice500, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "annualInvoicePeriod", DateUtils.toDate("10/01/2010"), DateUtils.toDate("09/30/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice500, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_12,913.54");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_4,289.46");
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(17203), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 17.61 % of total fees", accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee (prorated for late start)
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(6643.54), group);
		validateInvoiceDates(invoice400, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "invoiceBilling", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, 81);
		validateInvoiceDates(invoice400, "annualInvoicePeriod", DateUtils.toDate("07/12/2011"), DateUtils.toDate("09/30/2012"), 447);
		validateInvoiceDates(invoice400, "billingBasis", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1100.54), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,100.54"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(5543), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 5.68 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_5,543.00"));
			}
		}
	}


	@Test
	public void testSharedScheduleTieredInvoices_MonthlyAccrual() {
		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED);
		schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		// Shared Schedule with additional quarterly retainer flat fee
		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());

		// Generating ONE should have generated them all
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(78382), group);
		validateInvoiceDates(invoice000, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice000, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(4, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {

			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(588110936), BigDecimal.valueOf(24957), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 488,110,936 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 81.15 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_477,233,383_Percentage_100_BillingAmount_24,957.00"));
			}
			else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(612201887), BigDecimal.valueOf(25346), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 512,201,887 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 79.81 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_488,601,627_Percentage_100_BillingAmount_25,346.00"));
			}
			else {
				validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(638592761), BigDecimal.valueOf(25079), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 538,592,761 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 76.33 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_487,408,971_Percentage_100_BillingAmount_25,079.00"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(15667), group, 2);
		validateInvoiceDates(invoice500, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "annualInvoicePeriod", DateUtils.toDate("10/01/2010"), DateUtils.toDate("09/30/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice500, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(4, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(588110936), BigDecimal.valueOf(4337), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 488,110,936 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 14.1 % of total fees",
						CollectionUtils.createList("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_82,930,899_Percentage_100_BillingAmount_4,337.00"));
			}
			else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(612201887), BigDecimal.valueOf(4338), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 512,201,887 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 13.66 % of total fees",
						CollectionUtils.createList("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,623,346_Percentage_100_BillingAmount_4,338.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,873,585_Percentage_75.1544_BillingAmount_4,315.37");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.8456_BillingAmount_1,426.63");

				validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(638592761), BigDecimal.valueOf(5742), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 538,592,761 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 17.48 % of total fees",
						accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee (prorated for late start)
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(6673), group);
		validateInvoiceDates(invoice400, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "invoiceBilling", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, 81);
		validateInvoiceDates(invoice400, "annualInvoicePeriod", DateUtils.toDate("07/12/2011"), DateUtils.toDate("09/30/2012"), 447);
		validateInvoiceDates(invoice400, "billingBasis", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(4, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1101), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,101.00"));
			}
			else if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(588110936), BigDecimal.valueOf(1461), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 488,110,936 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 4.75 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_27,946,654_Percentage_100_BillingAmount_1,461.00"));
			}
			else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(612201887), BigDecimal.valueOf(2074), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 512,201,887 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 6.53 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_39,976,914_Percentage_100_BillingAmount_2,074.00"));
			}
			else {
				validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(638592761), BigDecimal.valueOf(2037), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.00833 %"
								+ StringUtils.NEW_LINE + "$ 538,592,761 at 0.00417 %" + StringUtils.NEW_LINE + "Allocated for 6.2 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_39,582,092_Percentage_100_BillingAmount_2,037.00"));
			}
		}
	}


	@Test
	public void testSharedScheduleTieredInvoices_InAdvance() {
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_FAIRFAX_000);
		def.setPaymentInAdvance(true);
		this.billingDefinitionService.saveBillingDefinition(def);

		def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_FAIRFAX_500);
		def.setPaymentInAdvance(true);
		this.billingDefinitionService.saveBillingDefinition(def);

		def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_FAIRFAX_400);
		def.setPaymentInAdvance(true);
		this.billingDefinitionService.saveBillingDefinition(def);

		// Shared Schedule with additional quarterly retainer flat fee
		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);
		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(77936), group);
		validateInvoiceDates(invoice000, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice000, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice000, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {
			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(74936), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 76.71 % of total fees",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_74,936.00"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(18453), group, 2);
		validateInvoiceDates(invoice500, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice500, "annualInvoicePeriod", DateUtils.toDate("10/01/2011"), DateUtils.toDate("09/30/2012"), 366);
		validateInvoiceDates(invoice500, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_12,913.54");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_4,289.46");

				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(17203), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 17.61 % of total fees", accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());

		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(6793), group);
		validateInvoiceDates(invoice400, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice400, "annualInvoicePeriod", DateUtils.toDate("07/12/2011"), DateUtils.toDate("09/30/2012"), 447);
		validateInvoiceDates(invoice400, "billingBasis", DateUtils.toDate("07/12/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(631453937), BigDecimal.valueOf(5543), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 531,453,937 at 0.0125 %" + StringUtils.NEW_LINE + "Allocated for 5.68 % of total fees",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_5,543.00"));
			}
		}
	}


	@Test
	public void testSharedScheduleTieredInvoices_AggregationTypes() {
		BillingSchedule sharedSchedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_SHARED); // Fairfax Shared Tiered
		// Leave it as a Shared schedule, but evaluate independently for each account
		sharedSchedule.setBillingBasisAggregationType(this.billingBasisService.getBillingBasisAggregationType(BILLING_AGGREGATION_TYPE_CLIENT_ACCOUNT));
		this.billingDefinitionService.saveBillingSchedule(sharedSchedule);

		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice000.getInvoiceGroup().getId());

		// Generating ONE should have generated them all because STILL grouped schedule
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(82302.00), group);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {

			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(484414660), BigDecimal.valueOf(79302), "Account #: 227000" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 384,414,660 at 0.0125 %",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_79,302.00"));
			}
		}

		// Shared Schedule with 2 definitions applying to the shared schedule additional quarterly additional account fee
		// Invoice is part of a group, should have been already created BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		BillingInvoice invoice500 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_500));
		// Pull the invoice again so detail list is populated
		invoice500 = this.billingInvoiceService.getBillingInvoice(invoice500.getId());

		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(24865.00), group, 1);
		Assertions.assertEquals(3, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			// Because we are not aggregating assets we hit the annual maximum
			else if ("Annual Maximum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-9036), "Annual Maximum Fee: $ 75,000\tBilled YTD: $ 84,036",
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_-9,036.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_24,509.67");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_8,141.33");
				validateInvoiceDetail(detail, BigDecimal.valueOf(111204056), BigDecimal.valueOf(32651.00), "Account #: 227500" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 11,204,056 at 0.0125 %", accountDetailList);
			}
		}

		// Shared Schedule with additional quarterly additional account fee (prorated for late start)
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice400 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_FAIRFAX_400));
		// Pull the invoice again so detail list is populated
		invoice400 = this.billingInvoiceService.getBillingInvoice(invoice400.getId());
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(16310.00), group);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1101.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,101.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(35835220), BigDecimal.valueOf(15209.00), "Account #: 227400" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 10,835,220 at 0.025 %",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_15,209.00"));
			}
		}
	}


	@Test
	public void testSharedScheduleTieredInvoices_DoNotGroupInvoice() {
		BillingSchedule sharedSchedule = this.billingDefinitionService.getBillingSchedule(10); // Fairfax Shared Tiered
		// Leave it as a Shared schedule, but evaluate independently for each billing definition
		sharedSchedule.setDoNotGroupInvoice(true);
		this.billingDefinitionService.saveBillingSchedule(sharedSchedule);

		BillingInvoice invoice000 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE);
		// There is only one shared schedule and it's set to NOT group, so group should not be created
		validateInvoiceSummary(invoice000, BILLING_DEFINITION_FAIRFAX_000, INVOICE_DATE, BigDecimal.valueOf(82302.00), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice000.getDetailList()));
		for (BillingInvoiceDetail detail : invoice000.getDetailList()) {
			if ("Quarterly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000.00), "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227000: Fairfax 227000_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(484414660), BigDecimal.valueOf(79302), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
								+ StringUtils.NEW_LINE + "$ 384,414,660 at 0.0125 %",
						CollectionUtils.createList("227000: Fairfax 227000 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_484,414,660_Percentage_100_BillingAmount_79,302.00"));
			}
		}

		BillingInvoice invoice500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);
		validateInvoiceSummary(invoice500, BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE, BigDecimal.valueOf(24865.00), null, 1);
		Assertions.assertEquals(3, CollectionUtils.getSize(invoice500.getDetailList()));
		for (BillingInvoiceDetail detail : invoice500.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1250.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_1,250.00"));
			}
			// Because we are not aggregating assets we hit the annual maximum
			else if ("Annual Maximum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-9036), "Annual Maximum Fee: $ 75,000\tBilled YTD: $ 84,036",
						CollectionUtils.createList("227500: Fairfax 227500_BillingBasis_0_Percentage_100_BillingAmount_-9,036.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("227500: Fairfax 227500 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_83,475,943_Percentage_75.0656_BillingAmount_24,509.67");
				accountDetailList.add("227500: Fairfax 227500 Market Value - Period End [TIPS]_BillingBasis_27,728,113_Percentage_24.9344_BillingAmount_8,141.33");
				validateInvoiceDetail(detail, BigDecimal.valueOf(111204056), BigDecimal.valueOf(32651.00), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %"
						+ StringUtils.NEW_LINE + "$ 11,204,056 at 0.0125 %", accountDetailList);
			}
		}

		BillingInvoice invoice400 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE);
		validateInvoiceSummary(invoice400, BILLING_DEFINITION_FAIRFAX_400, INVOICE_DATE, BigDecimal.valueOf(14491.00), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice400.getDetailList()));
		for (BillingInvoiceDetail detail : invoice400.getDetailList()) {
			if ("Additional Account Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1101.00), "$ 1,250 per quarter" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days" + StringUtils.NEW_LINE,
						CollectionUtils.createList("227400: Fairfax 227400_BillingBasis_0_Percentage_100_BillingAmount_1,101.00"));
			}
			else {
				// This is prorated and the above tests aren't because the above tests apply to the full quarter and assume the billing basis was already pro-rated.  This just doesn't happen above because of the mocking of the billing basis value retrieval
				validateInvoiceDetail(detail, BigDecimal.valueOf(35835220), BigDecimal.valueOf(13390.00), "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 10,835,220 at 0.025 %" + StringUtils.NEW_LINE + "Prorated for 81 of 92 days",
						CollectionUtils.createList("227400: Fairfax 227400 Synthetic Exposure - Period Average [FUTURES]_BillingBasis_35,835,220_Percentage_100_BillingAmount_13,390.00"));
			}
		}
	}


	@Test
	public void testTierWithMinimumQuarterlyFee() {
		// Setting Schedule Accrual Frequency to Quarterly should have NO effect on the results since billing is quarterly
		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_ACUMENT_TIERED);
		schedule.setAccrualFrequency(BillingFrequencies.QUARTERLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		// Tiered schedule whose fee is less than the quarterly minimum
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(12500), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), DateUtils.toDate("12/31/2011"), 352);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(894), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Minimum Fee: $ 12,500" + StringUtils.NEW_LINE
						+ "Billed: $ 11,606", CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_894.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(23211646), BigDecimal.valueOf(11606), "$ 23,211,646 at 0.05 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_23,211,646_Percentage_100_BillingAmount_11,606.00"));
			}
		}
		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
	}


	@Test
	public void testTierWithMinimumQuarterlyFee_AccrualFrequencyMonthly() {
		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_ACUMENT_TIERED);
		schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		// Tiered schedule whose fee is less than the quarterly minimum and tier fee is accrued monthly
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(12500), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), DateUtils.toDate("12/31/2011"), 352);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(4, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(951), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Minimum Fee: $ 12,500" + StringUtils.NEW_LINE
						+ "Billed: $ 11,549", CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_951.00"));
			}
			else if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(25684633), BigDecimal.valueOf(4224), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 684,633 at 0.00833 %",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_25,684,633_Percentage_100_BillingAmount_4,224.00"));
			}
			else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(22408775), BigDecimal.valueOf(3735), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 22,408,775 at 0.01667 %",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_22,408,775_Percentage_100_BillingAmount_3,735.00"));
			}
			else {
				validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(21541531), BigDecimal.valueOf(3590), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 21,541,531 at 0.01667 %",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_21,541,531_Percentage_100_BillingAmount_3,590.00"));
			}
		}
		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
	}


	@Test
	public void testTierWithMinimumQuarterlyFee_Pennies() {
		updateBillingDefinitionInvoiceType(BILLING_DEFINITION_ACUMENT, BILLING_INVOICE_TYPE_PENNIES);

		// Tiered schedule whose fee is less than the quarterly minimum
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(12500), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), DateUtils.toDate("12/31/2011"), 352);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(894.18), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Minimum Fee: $ 12,500" + StringUtils.NEW_LINE
						+ "Billed: $ 11,605.82", CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_894.18"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(23211646), BigDecimal.valueOf(11605.82), "$ 23,211,646.33 at 0.05 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_23,211,646_Percentage_100_BillingAmount_11,605.82"));
			}
		}
		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
	}


	@Test
	public void testTierWithMinimumQuarterlyFee_Prorated() {
		Date endDate = DateUtils.toDate("09/15/2011");
		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, endDate, true);

		// Tiered schedule whose fee is less than the quarterly minimum - However because of prorating the quarterly minimum should be less and not apply
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(10462), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", INVOICE_PERIOD_START_DATE, endDate, 77);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), endDate, 245);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, endDate, null);

		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(748), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Prorated for 77 of 92 days" + StringUtils.NEW_LINE
								+ "Minimum fee: $ 10,462" + StringUtils.NEW_LINE + "Billed: $ 9,714",
						CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_748.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(23211646), BigDecimal.valueOf(9714), "$ 23,211,646 at 0.05 %" + StringUtils.NEW_LINE + "Prorated for 77 of 92 days",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_23,211,646_Percentage_100_BillingAmount_9,714.00"));
			}
		}

		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, endDate, true);
	}


	@Test
	public void testTierWithMinimumQuarterlyFee_Prorated_MonthlyAccrual() {
		Date endDate = DateUtils.toDate("09/15/2011");
		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, endDate, true);

		BillingSchedule acumentSchedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_ACUMENT_TIERED);
		acumentSchedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(acumentSchedule);

		// Tiered schedule whose fee is less than the quarterly minimum - However because of prorating the quarterly minimum should be less and not apply
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(10462), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", INVOICE_PERIOD_START_DATE, endDate, 77);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), endDate, 245);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, endDate, null);

		Assertions.assertEquals(4, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(708), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Prorated for 77 of 92 days" + StringUtils.NEW_LINE
								+ "Minimum fee: $ 10,462" + StringUtils.NEW_LINE + "Billed: $ 9,754",
						CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_708.00"));
			}
			else if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(25684633), BigDecimal.valueOf(4224), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 25,000,000 at 0.01667 %" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 684,633 at 0.00833 %",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_25,684,633_Percentage_100_BillingAmount_4,224.00"));
			}
			else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
				validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(22408775), BigDecimal.valueOf(3735), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 22,408,775 at 0.01667 %",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_22,408,775_Percentage_100_BillingAmount_3,735.00"));
			}
			else {
				validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(21541531), BigDecimal.valueOf(1795), "09/01/2011 - 09/15/2011:" + StringUtils.NEW_LINE + StringUtils.TAB + "$ 21,541,531 at 0.01667 %" + StringUtils.NEW_LINE + "Prorated for 15 of 30 days",
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_21,541,531_Percentage_100_BillingAmount_1,795.00"));
			}
		}

		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, endDate, true);
	}


	@Test
	public void testTierWithMinimumQuarterlyFee_InAdvance_Prorated() {
		Date endDate = DateUtils.toDate("11/18/2011");
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT);
		def.setPaymentInAdvance(true);
		this.billingDefinitionService.saveBillingDefinition(def);

		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, endDate, true);

		// Tiered schedule whose fee is less than the quarterly minimum - However because of prorating the quarterly minimum should be less and not apply
		BillingInvoice invoice_acument = generateBillingInvoice(BILLING_DEFINITION_ACUMENT, INVOICE_DATE);

		validateInvoiceSummary(invoice_acument, BILLING_DEFINITION_ACUMENT, INVOICE_DATE, BigDecimal.valueOf(6658), null);
		validateInvoiceDates(invoice_acument, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_acument, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, endDate, 49);
		validateInvoiceDates(invoice_acument, "annualInvoicePeriod", DateUtils.toDate("01/14/2011"), endDate, 309);
		validateInvoiceDates(invoice_acument, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_acument.getDetailList()));
		for (BillingInvoiceDetail detail : invoice_acument.getDetailList()) {
			if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(477), "$ 12,500 per quarter" + StringUtils.NEW_LINE + "Prorated for 49 of 92 days" + StringUtils.NEW_LINE
								+ "Minimum fee: $ 6,658" + StringUtils.NEW_LINE + "Billed: $ 6,181",
						CollectionUtils.createList("50920: Acument Global Technologies_BillingBasis_0_Percentage_100_BillingAmount_477.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.valueOf(23211646), BigDecimal.valueOf(6181), "$ 23,211,646 at 0.05 %" + StringUtils.NEW_LINE + "Prorated for 49 of 92 days"
								+ StringUtils.NEW_LINE,
						CollectionUtils.createList("50920: Acument Global Technologies Synthetic Exposure - Period Average_BillingBasis_23,211,646_Percentage_100_BillingAmount_6,181.00"));
			}
		}

		this.billingInvoiceService.deleteBillingInvoice(invoice_acument.getId());
		def.setPaymentInAdvance(false);
		this.billingDefinitionService.saveBillingDefinition(def);
		setBillingDefinitionEndDate(BILLING_DEFINITION_ACUMENT, null, true);
	}


	@Test
	public void testTierWithAnnualDailyPercentageWithOptionsBillingBasis() {
		Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any(InvestmentAccountSearchForm.class))).thenReturn(getKCAccounts());
		Mockito.when(this.systemConditionEvaluationHandler.getConditionTrueMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BillingSchedule.class))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			BillingSchedule schedule = (BillingSchedule) args[1];
			if (!"359200".equals(schedule.getInvestmentAccount().getNumber()) && !"359300".equals(schedule.getInvestmentAccount().getNumber())) {
				return "Condition evaluated true";
			}
			return null;
		});

		// Tiered Annual Percentage for Options Billing Basis which is also split based on on dates it was traded
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_KIMBERLY_CLARK_359100, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_KIMBERLY_CLARK_359100, INVOICE_DATE, BigDecimal.valueOf(63639), null, 1);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), DateUtils.toDate("12/31/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(4, CollectionUtils.getSize(invoice.getDetailList()));
		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Protection Program Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				if (detail.getNote().contains("07/01/2011")) {
					validateInvoiceDetail(detail, BigDecimal.valueOf(844000000), BigDecimal.valueOf(8324), "07/01/2011 - 07/12/2011:" + StringUtils.NEW_LINE + "$ 844,000,000 at 0.03 % per year"
									+ StringUtils.NEW_LINE + "Prorated for 12 of 92 days",
							CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_844,000,000_Percentage_100_BillingAmount_8,324.00"));
				}
				else {
					validateInvoiceDetail(detail, BigDecimal.valueOf(750000000), BigDecimal.valueOf(49315), "07/13/2011 - 09/30/2011:" + StringUtils.NEW_LINE + "$ 750,000,000 at 0.03 % per year"
									+ StringUtils.NEW_LINE + "Prorated for 80 of 92 days",
							CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_750,000,000_Percentage_100_BillingAmount_49,315.00"));
				}
			}
			else if (detail.getNote().contains("359200")) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000), "Account #: 359200" + StringUtils.NEW_LINE + "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("359200: Kimberly Clark 200_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000), "Account #: 359300" + StringUtils.NEW_LINE + "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("359300: Kimberly Clark 300_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
		}
	}


	@Test
	public void testTierWithAnnualDailyPercentageWithOptionsBillingBasis_MonthlyAccrual() {
		Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any(InvestmentAccountSearchForm.class))).thenReturn(getKCAccounts());
		Mockito.when(this.systemConditionEvaluationHandler.getConditionTrueMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BillingSchedule.class))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			BillingSchedule schedule = (BillingSchedule) args[1];
			if (!"359200".equals(schedule.getInvestmentAccount().getNumber()) && !"359300".equals(schedule.getInvestmentAccount().getNumber())) {
				return "Condition evaluated true";
			}
			return null;
		});

		BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_KIMBERLY_CLARK_PERCENTAGE_FEE);
		schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingSchedule(schedule);

		// Annual Percentage for Options Billing Basis which is also split based on on dates it was traded
		// And accrual frequency of monthly so should be at least split into months
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_KIMBERLY_CLARK_359100, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_KIMBERLY_CLARK_359100, INVOICE_DATE, BigDecimal.valueOf(66578), null, 1);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), DateUtils.toDate("12/31/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		Assertions.assertEquals(6, CollectionUtils.getSize(invoice.getDetailList()));
		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Protection Program Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					if (detail.getNote().contains("07/01/2011")) {
						validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(844000000), BigDecimal.valueOf(8324), "07/01/2011 - 07/12/2011:" + StringUtils.NEW_LINE + "$ 844,000,000 at 0.03 % per year"
										+ StringUtils.NEW_LINE + "Prorated for 12 of 31 days",
								CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_844,000,000_Percentage_100_BillingAmount_8,324.00"));
					}
					else {
						validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(775000000), BigDecimal.valueOf(12103), "07/13/2011 - 07/31/2011:" + StringUtils.NEW_LINE + "$ 775,000,000 at 0.03 % per year"
										+ StringUtils.NEW_LINE + "Prorated for 19 of 31 days",
								CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_775,000,000_Percentage_100_BillingAmount_12,103.00"));
					}
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(850000000), BigDecimal.valueOf(21658), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + "$ 850,000,000 at 0.03 % per year",
							CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_850,000,000_Percentage_100_BillingAmount_21,658.00"));
				}
				else {
					validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(750000000), BigDecimal.valueOf(18493), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + "$ 750,000,000 at 0.03 % per year",
							CollectionUtils.createList("359100: Kimberly Clark 100 Notional - Period Trade Dates_BillingBasis_750,000,000_Percentage_100_BillingAmount_18,493.00"));
				}
			}
			else if (detail.getNote().contains("359200")) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000), "Account #: 359200" + StringUtils.NEW_LINE + "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("359200: Kimberly Clark 200_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000), "Account #: 359300" + StringUtils.NEW_LINE + "$ 3,000 per quarter" + StringUtils.NEW_LINE,
						CollectionUtils.createList("359300: Kimberly Clark 300_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
		}
	}


	private void setBillingDefinitionEndDate(Integer defId, Date endDate, boolean applyToAccounts) {
		BillingDefinition def = this.billingDefinitionService.getBillingDefinitionPopulated(defId);
		// Set End Date For Prorating
		def.setEndDate(endDate);

		// Set End Date on all Investment Accounts
		if (applyToAccounts) {
			for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(def.getInvestmentAccountList())) {
				billingAccount.setEndDate(endDate);
			}
		}
		this.billingDefinitionService.saveBillingDefinitionInvestmentAccountListForDefinition(def);
		this.billingDefinitionService.saveBillingDefinition(def);
	}


	private List<InvestmentAccount> getKCAccounts() {
		List<InvestmentAccount> list = new ArrayList<>();
		list.add(this.investmentAccountDAO.findByPrimaryKey(30));
		list.add(this.investmentAccountDAO.findByPrimaryKey(31));
		list.add(this.investmentAccountDAO.findByPrimaryKey(32));
		return list;
	}


	@Test
	public void testTierInAdvanceWithRetainer() {

		// Tiered Percentage for PIOS Billing Basis in Advance
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_NEW_ORLEANS_FF_454000, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_NEW_ORLEANS_FF_454000, INVOICE_DATE, BigDecimal.valueOf(6708), null);
		validateInvoiceDates(invoice, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("04/01/2011"), DateUtils.toDate("03/31/2012"), 366);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(7416660), BigDecimal.valueOf(3708), "$ 7,416,660 at 0.05 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("454000: New Orleans Firefighters Synthetic Exposure - Period End_BillingBasis_7,416,660_Percentage_100_BillingAmount_3,708.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3000), "$ 1,000 per month" + StringUtils.NEW_LINE,
						CollectionUtils.createList("454000: New Orleans Firefighters_BillingBasis_0_Percentage_100_BillingAmount_3,000.00"));
			}
		}

		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());
	}


	@Test
	public void testTierWithRetainerAndDiscount_RetainerProratedForBillingDates() {
		Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any(InvestmentAccountSearchForm.class))).thenReturn(getContinentalAccounts());

		// Tier Percentage for PIOS Billing Basis With Monthly Retainer and Discount
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_CONTINENTAL_125300, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_CONTINENTAL_125300, INVOICE_DATE, BigDecimal.valueOf(12401), null, 1);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", DateUtils.toDate("09/02/2011"), INVOICE_PERIOD_END_DATE, 29);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("09/02/2011"), DateUtils.toDate("9/30/2012"), 395);
		validateInvoiceDates(invoice, "billingBasis", DateUtils.toDate("09/02/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(4, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(69434586), BigDecimal.valueOf(10943), "$ 69,434,586 at 0.05 %" + StringUtils.NEW_LINE + "Prorated for 29 of 92 days",
						CollectionUtils.createList("125300: Continental Airlines Synthetic Exposure - Period Average_BillingBasis_69,434,586_Percentage_100_BillingAmount_10,943.00"));
			}
			else if ("Monthly Account Retainer".equals(detail.getBillingSchedule().getName())) {
				if (detail.getNote().contains("Account #: 125300")) {
					validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1418), "Account #: 125300" + StringUtils.NEW_LINE + "$ 1,500 per month" + StringUtils.NEW_LINE
							+ "Prorated for 29 of 92 days" + StringUtils.NEW_LINE, CollectionUtils.createList("125300: Continental Airlines_BillingBasis_0_Percentage_100_BillingAmount_1,418.00"));
				}
				else {
					validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(1418), "Account #: 125310" + StringUtils.NEW_LINE + "$ 1,500 per month" + StringUtils.NEW_LINE
									+ "Prorated for 29 of 92 days" + StringUtils.NEW_LINE,
							CollectionUtils.createList("125310: Continental Airlines - Extra Acct_BillingBasis_0_Percentage_100_BillingAmount_1,418.00"));
				}
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("125300: Continental Airlines_BillingBasis_0_Percentage_89.709_BillingAmount_-1,236.19");
				accountDetailList.add("125310: Continental Airlines - Extra Acct_BillingBasis_0_Percentage_10.291_BillingAmount_-141.81");
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-1378), "10 % off of total invoice amount: $ 13,779", accountDetailList);
			}
		}
	}


	@Test
	public void testDiscount_InvalidAccrualFrequency() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingSchedule schedule = this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_CONTINENTAL_DISCOUNT);
			schedule.setAccrualFrequency(BillingFrequencies.MONTHLY);
			this.billingDefinitionService.saveBillingSchedule(schedule);
			Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any(InvestmentAccountSearchForm.class))).thenReturn(getContinentalAccounts());
			generateBillingInvoice(BILLING_DEFINITION_CONTINENTAL_125300, INVOICE_DATE);
		}, "Cannot apply an accrual frequency to [Discount at 10%] because there is at least one invoice detail for schedule [Tiered Fee] that doesn't use accrual frequencies.");
	}


	private List<InvestmentAccount> getContinentalAccounts() {
		List<InvestmentAccount> list = new ArrayList<>();
		list.add(this.investmentAccountDAO.findByPrimaryKey(50));
		list.add(this.investmentAccountDAO.findByPrimaryKey(51));
		return list;
	}


	@Test
	public void testTierWithProrating() {
		// Tier Percentage for Protection Plus Billing Basis With Prorated Quarter
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_WILDER_713450, INVOICE_DATE);
		//validateInvoiceSummary(invoice, BILLING_DEFINITION_WILDER_713450, INVOICE_DATE, BigDecimal.valueOf(8990), null);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", DateUtils.toDate("07/06/2011"), INVOICE_PERIOD_END_DATE, 87);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("07/06/2011"), DateUtils.toDate("9/30/2012"), 453);
		validateInvoiceDates(invoice, "billingBasis", DateUtils.toDate("07/06/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice.getDetailList()));
		BillingInvoiceDetail detail = invoice.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.valueOf(33037485), BigDecimal.valueOf(8990), "07/06/2011 - 08/31/2011:" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE
						+ "$ 8,037,485 at 0.025 %" + StringUtils.NEW_LINE + "Prorated for 57 of 92 days",
				CollectionUtils.createList("713450: Amherst H. Wilder Foundation - Protection Plus Notional - Period Trade Dates_BillingBasis_33,037,485_Percentage_100_BillingAmount_8,990.00"));
	}


	@Test
	public void testTierWithProratingAndAdditionalFeeBillingQuarterlyDefinedAnnually() {

		// Tier Percentage for Protection Plus Billing Basis With Prorated Quarter defined with annual percentages/fees
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_NC_BAPTIST, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_NC_BAPTIST, INVOICE_DATE, BigDecimal.valueOf(16076), null, 1);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", DateUtils.toDate("08/08/2011"), INVOICE_PERIOD_END_DATE, 54);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("08/08/2011"), DateUtils.toDate("9/30/2012"), 420);
		validateInvoiceDates(invoice, "billingBasis", DateUtils.toDate("08/08/2011"), INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(3, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Protection Program Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				if (detail.getNote().contains("08/08/2011")) {
					List<String> accountDetailList = new ArrayList<>();
					accountDetailList.add("454600: North Carolina Baptist - S&P 500 Notional - Period Trade Dates_BillingBasis_30,018,100_Percentage_75.0117_BillingAmount_267.04");
					accountDetailList.add("454700: North Carolina Baptist - EFA Notional - Period Trade Dates_BillingBasis_9,999,792_Percentage_24.9883_BillingAmount_88.96");
					validateInvoiceDetail(detail, BigDecimal.valueOf(40017892), BigDecimal.valueOf(356), "08/08/2011 - 08/09/2011:" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.2 % per year"
							+ StringUtils.NEW_LINE + "$ 15,017,892 at 0.1 % per year" + StringUtils.NEW_LINE + "Prorated for 2 of 92 days", accountDetailList);
				}
				else {
					List<String> accountDetailList = new ArrayList<>();
					accountDetailList.add("454600: North Carolina Baptist - S&P 500 Notional - Period Trade Dates_BillingBasis_60,127,300_Percentage_75.0203_BillingAmount_11,238.04");
					accountDetailList.add("454700: North Carolina Baptist - EFA Notional - Period Trade Dates_BillingBasis_20,020,784_Percentage_24.9797_BillingAmount_3,741.96");
					validateInvoiceDetail(detail, BigDecimal.valueOf(80148084), BigDecimal.valueOf(14980), "08/10/2011 - 09/30/2011:" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.2 % per year"
							+ StringUtils.NEW_LINE + "$ 55,148,084 at 0.1 % per year" + StringUtils.NEW_LINE + "Prorated for 52 of 92 days", accountDetailList);
				}
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("454600: North Carolina Baptist - S&P 500_BillingBasis_0_Percentage_50_BillingAmount_370.00");
				accountDetailList.add("454700: North Carolina Baptist - EFA_BillingBasis_0_Percentage_50_BillingAmount_370.00");
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(740), "$ 5,000 per year" + StringUtils.NEW_LINE + "Prorated for 54 of 92 days" + StringUtils.NEW_LINE,
						accountDetailList);
			}
		}
	}


	@Test
	public void testTierAdvancedPeriodEndMarketValueAndAnnualMinimumNotReached() {

		// Tier Percentage for Period End Market Value Billing Basis With Annual Minimum Not Reached
		// NOTE: ACTUAL ANNUAL MINIMUM FOR THIS ACCOUNT IS 4000, CHANGED TO 10000 FOR TESTING ONLY and ANNUAL PERIOD IS 10/1 - 9/30 BUT FOR ADVANCED NEEDED ANNUAL TO END ON 12/31 SO CHANGED DEFINITION DATES TO SUPPORT TEST CASE
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_MCGOWAN, INVOICE_DATE);

		validateInvoiceSummary(invoice, BILLING_DEFINITION_MCGOWAN, INVOICE_DATE, BigDecimal.valueOf(5861), null);
		validateInvoiceDates(invoice, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), DateUtils.toDate("12/31/2011"), ANNUAL_DAYS);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Quarterly Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(523157), BigDecimal.valueOf(1654), "$ 250,000 at 0.4 %" + StringUtils.NEW_LINE + "$ 250,000 at 0.25 %" + StringUtils.NEW_LINE
								+ "$ 23,157 at 0.125 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("391000: William & Sarah McGowan Trust Market Value - Period Start_BillingBasis_523,157_Percentage_100_BillingAmount_1,654.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(4207), "Annual Minimum Fee: $ 10,000" + StringUtils.NEW_LINE + "Billed YTD: $ 5,793",
						CollectionUtils.createList("391000: William & Sarah McGowan Trust_BillingBasis_0_Percentage_100_BillingAmount_4,207.00"));
			}
		}

		// Delete for future tests
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());
	}


	@Test
	public void testTierAdvancedPeriodEndMarketValueAndAnnualMinimumNotReached_ProratedDefinitionEndDate() {
		// Prorated Annual Minimum when ending in
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCGOWAN);
		def.setEndDate(DateUtils.addDays(ADVANCE_INVOICE_PERIOD_END_DATE, -14));
		this.billingDefinitionService.saveBillingDefinition(def);

		// Tier Percentage for Period End Market Value Billing Basis With Annual Minimum Not Reached
		// NOTE: ACTUAL ANNUAL MINIMUM FOR THIS ACCOUNT IS 4000, CHANGED TO 10000 FOR TESTING ONLY and ANNUAL PERIOD IS 10/1 - 9/30 BUT FOR ADVANCED NEEDED ANNUAL TO END ON 12/31 SO CHANGED DEFINITION DATES TO SUPPORT TEST CASE
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_MCGOWAN, INVOICE_DATE);

		validateInvoiceSummary(invoice, BILLING_DEFINITION_MCGOWAN, INVOICE_DATE, BigDecimal.valueOf(5477), null);
		validateInvoiceDates(invoice, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, DateUtils.toDate("12/17/2011"), 78);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), DateUtils.toDate("12/17/2011"), ANNUAL_DAYS - 14);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Quarterly Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(523157), BigDecimal.valueOf(1402), "$ 250,000 at 0.4 %" + StringUtils.NEW_LINE + "$ 250,000 at 0.25 %" + StringUtils.NEW_LINE
								+ "$ 23,157 at 0.125 %" + StringUtils.NEW_LINE + "prorated for 78 of 92 days",
						CollectionUtils.createList("391000: William & Sarah McGowan Trust Market Value - Period Start_BillingBasis_523,157_Percentage_100_BillingAmount_1,402.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(4075), "Annual Minimum Fee: $ 10,000" + StringUtils.NEW_LINE + "Prorated for 351 of 365 days" + StringUtils.NEW_LINE
								+ "Minimum Fee: $ 9,616" + StringUtils.NEW_LINE + "Billed YTD: $ 5,541",
						CollectionUtils.createList("391000: William & Sarah McGowan Trust_BillingBasis_0_Percentage_100_BillingAmount_4,075.00"));
			}
		}

		// Delete for future tests
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());
		def.setEndDate(null);
		this.billingDefinitionService.saveBillingDefinition(def);
	}


	@Test
	public void testTierAdvancedPeriodEndMarketValueAndAnnualMinimumNotReached_Prorated() {
		// Set end date so annual min is prorated
		BillingDefinition def = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCGOWAN);
		def.setEndDate(DateUtils.toDate("11/15/2011"));
		this.billingDefinitionService.saveBillingDefinition(def);

		// Tier Percentage for Period End Market Value Billing Basis With Annual Minimum Not Reached
		// NOTE: ACTUAL ANNUAL MINIMUM FOR THIS ACCOUNT IS 4000, CHANGED TO 10000 FOR TESTING ONLY and ANNUAL PERIOD IS 10/1 - 9/30 BUT FOR ADVANCED NEEDED ANNUAL TO END ON 12/31 SO CHANGED DEFINITION DATES TO SUPPORT TEST CASE
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_MCGOWAN, INVOICE_DATE);

		validateInvoiceSummary(invoice, BILLING_DEFINITION_MCGOWAN, INVOICE_DATE, BigDecimal.valueOf(4601), null);
		validateInvoiceDates(invoice, "invoicePeriod", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", ADVANCE_INVOICE_PERIOD_START_DATE, def.getEndDate(), 46);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2011"), def.getEndDate(), 319);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Quarterly Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(523157), BigDecimal.valueOf(827), "$ 250,000 at 0.4 %" + StringUtils.NEW_LINE + "$ 250,000 at 0.25 %" + StringUtils.NEW_LINE
								+ "$ 23,157 at 0.125 %" + StringUtils.NEW_LINE + "Prorated for 46 of 92 days",
						CollectionUtils.createList("391000: William & Sarah McGowan Trust Market Value - Period Start_BillingBasis_523,157_Percentage_100_BillingAmount_827.00"));
			}
			else {

				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3774), "Annual Minimum Fee: $ 10,000" + StringUtils.NEW_LINE + "Prorated for 319 of 365 days" + StringUtils.NEW_LINE
								+ "Minimum Fee: $ 8,740" + StringUtils.NEW_LINE + "Billed YTD: $ 4,966",
						CollectionUtils.createList("391000: William & Sarah McGowan Trust_BillingBasis_0_Percentage_100_BillingAmount_3,774.00"));
			}
		}

		// Reset
		def.setEndDate(null);
		this.billingDefinitionService.saveBillingDefinition(def);

		// Delete for future tests
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());
	}


	@Test
	public void testTierAdvancedPeriodEndMarketValue() {
		// Tier Percentage for Period End Market Value Billing Basis In Advance - Testing when invoice date is not a business day
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_MCGOWAN, DateUtils.toDate("12/31/2011"));
		validateInvoiceSummary(invoice, BILLING_DEFINITION_MCGOWAN, DateUtils.toDate("12/31/2011"), BigDecimal.valueOf(1688), null, 1);
		validateInvoiceDates(invoice, "invoicePeriod", DateUtils.toDate("01/01/2012"), DateUtils.toDate("03/31/2012"), 91);
		validateInvoiceDates(invoice, "invoiceBilling", DateUtils.toDate("01/01/2012"), DateUtils.toDate("03/31/2012"), 91);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("01/01/2012"), DateUtils.toDate("12/31/2012"), 366);
		validateInvoiceDates(invoice, "billingBasis", ADVANCE_INVOICE_PERIOD_START_DATE, ADVANCE_INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice.getDetailList()));
		validateInvoiceDetail(invoice.getDetailList().get(0), BigDecimal.valueOf(550000), BigDecimal.valueOf(1688), "$ 250,000 at 0.4 %" + StringUtils.NEW_LINE + "$ 250,000 at 0.25 %"
						+ StringUtils.NEW_LINE + "$ 50,000 at 0.125 %" + StringUtils.NEW_LINE,
				CollectionUtils.createList("391000: William & Sarah McGowan Trust Market Value - Period Start_BillingBasis_550,000_Percentage_100_BillingAmount_1,688.00"));
	}


	@Test
	public void testMonthlySharedFixed() {
		// Shared Fixed Fee across 3 Billing Definitions allocated proportionally based on Average Synthetic Exposure
		BillingInvoice invoice_900 = generateBillingInvoice(BILLING_DEFINITION_DELTA_MASTER_159900, INVOICE_DATE);

		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice_900.getInvoiceGroup().getId());

		validateInvoiceSummary(invoice_900, BILLING_DEFINITION_DELTA_MASTER_159900, INVOICE_DATE, BigDecimal.valueOf(16337), group);
		validateInvoiceDates(invoice_900, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_900, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_900, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_900.getDetailList()));
		BillingInvoiceDetail detail = invoice_900.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.valueOf(191190188), BigDecimal.valueOf(16337), "$ 15,000 per month" + StringUtils.NEW_LINE + "Allocated for 36.31 % of total fees",
				CollectionUtils.createList("159900: Delta Master Trust Synthetic Exposure - Period Average_BillingBasis_69,411,838_Percentage_100_BillingAmount_16,337.00"));

		//BillingInvoice invoice_700 = generateBillingInvoice(BILLING_DEFINITION_DELTA_PILOTS_159700, INVOICE_DATE);
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice_700 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_DELTA_PILOTS_159700));
		// Pull the invoice again so detail list is populated
		invoice_700 = this.billingInvoiceService.getBillingInvoice(invoice_700.getId());

		validateInvoiceSummary(invoice_700, BILLING_DEFINITION_DELTA_PILOTS_159700, INVOICE_DATE, BigDecimal.valueOf(9451), group);
		validateInvoiceDates(invoice_700, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_700, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_700, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_700.getDetailList()));
		detail = invoice_700.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.valueOf(191190188), BigDecimal.valueOf(9451), "$ 15,000 per month" + StringUtils.NEW_LINE + "Allocated for 21 % of total fees",
				CollectionUtils.createList("159700: Delta Pilots Synthetic Exposure - Period Average_BillingBasis_40,155,073_Percentage_100_BillingAmount_9,451.00"));

		//BillingInvoice invoice_800 = generateBillingInvoice(BILLING_DEFINITION_NORTHWEST_159800, INVOICE_DATE);
		// Invoice is part of a group, should have been already created
		BillingInvoice invoice_800 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_NORTHWEST_159800));
		// Pull the invoice again so detail list is populated
		invoice_800 = this.billingInvoiceService.getBillingInvoice(invoice_800.getId());

		validateInvoiceSummary(invoice_800, BILLING_DEFINITION_NORTHWEST_159800, INVOICE_DATE, BigDecimal.valueOf(19212), group);
		validateInvoiceDates(invoice_800, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_800, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice_800, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_800.getDetailList()));
		detail = invoice_800.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.valueOf(191190188), BigDecimal.valueOf(19212), "$ 15,000 per month" + StringUtils.NEW_LINE + "Allocated for 42.69 % of total fees",
				CollectionUtils.createList("159800: Northwest Airlines Synthetic Exposure - Period Average_BillingBasis_81,623,277_Percentage_100_BillingAmount_19,212.00"));

		// Sum Across Should Equal the 45000 total
		BigDecimal sum = MathUtils.add(MathUtils.add(invoice_700.getTotalAmount(), invoice_800.getTotalAmount()), invoice_900.getTotalAmount());
		Assertions.assertEquals(BigDecimal.valueOf(45000), MathUtils.round(sum, 0));
	}


	@Test
	public void testSharedQuarterlyMinimum_Prorated() {
		// Each has it's own schedule plus shared quarterly minimum
		Date invoiceDate = DateUtils.toDate("12/31/2012");
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_SMITH_FAMILY, invoiceDate);
		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice.getInvoiceGroup().getId());

		validateInvoiceSummary(invoice, BILLING_DEFINITION_SMITH_FAMILY, invoiceDate, BigDecimal.valueOf(5434), group);
		validateInvoiceDates(invoice, "invoicePeriod", DateUtils.toDate("10/01/2012"), invoiceDate, 92);
		validateInvoiceDates(invoice, "invoiceBilling", DateUtils.toDate("10/01/2012"), DateUtils.toDate("11/29/2012"), 60);
		validateInvoiceDates(invoice, "billingBasis", DateUtils.toDate("10/01/2012"), DateUtils.toDate("11/29/2012"), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Smith Family Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(8840454), BigDecimal.valueOf(2162), "$ 8,840,454 at 0.15 % per Year" + StringUtils.NEW_LINE + "Prorated for 60 of 92 days",
						CollectionUtils.createList("576900: Smith Family Market Value - Period End_BillingBasis_8,840,454_Percentage_100_BillingAmount_2,162.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(3272), "$ 18,750 per Quarter" + StringUtils.NEW_LINE + "Prorated for 60 of 92 days" + StringUtils.NEW_LINE
								+ "Minimum Fee: $ 12,228" + StringUtils.NEW_LINE + "Billed: $ 4,865" + StringUtils.NEW_LINE + "allocated for 44.44 % of total fees",
						CollectionUtils.createList("576900: Smith Family_BillingBasis_0_Percentage_100_BillingAmount_3,272.00"));
			}
		}

		// Invoice is part of a group, should have been already created
		BillingInvoice invoice_2 = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(group.getInvoiceList(), billingInvoice -> billingInvoice.getBillingDefinition().getId(), BILLING_DEFINITION_SMITH_SUSAN));
		// Pull the invoice again so detail list is populated
		invoice_2 = this.billingInvoiceService.getBillingInvoice(invoice_2.getId());

		validateInvoiceSummary(invoice_2, BILLING_DEFINITION_SMITH_SUSAN, invoiceDate, BigDecimal.valueOf(6794), group);
		validateInvoiceDates(invoice_2, "invoicePeriod", DateUtils.toDate("10/01/2012"), invoiceDate, 92);
		validateInvoiceDates(invoice_2, "invoiceBilling", DateUtils.toDate("10/01/2012"), DateUtils.toDate("11/29/2012"), 60);
		validateInvoiceDates(invoice_2, "billingBasis", DateUtils.toDate("10/01/2012"), DateUtils.toDate("11/29/2012"), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_2.getDetailList()));

		for (BillingInvoiceDetail detail : invoice_2.getDetailList()) {
			if ("Susan Smith Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(11050567), BigDecimal.valueOf(2703), "$ 11,050,567 at 0.15 % per Year" + StringUtils.NEW_LINE + "Prorated for 60 of 92 days",
						CollectionUtils.createList("576910: Susan Smith Market Value - Period End_BillingBasis_11,050,567_Percentage_100_BillingAmount_2,703.00"));
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(4091), "$ 18,750 per Quarter" + StringUtils.NEW_LINE + "Prorated for 60 of 92 days" + StringUtils.NEW_LINE
								+ "Minimum Fee: $ 12,228" + StringUtils.NEW_LINE + "Billed: $ 4,865" + StringUtils.NEW_LINE + "allocated for 55.56 % of total fees",
						CollectionUtils.createList("576910: Susan Smith_BillingBasis_0_Percentage_100_BillingAmount_4,091.00"));
			}
		}

		// Sum Across Should Equal the 12228 total (Prorated minimum fee of 18750 for 60 days)
		BigDecimal sum = MathUtils.add(invoice.getTotalAmount(), invoice_2.getTotalAmount());
		Assertions.assertEquals(BigDecimal.valueOf(12228), MathUtils.round(sum, 0));
	}


	@Test
	public void testTwoAccountSpecificTieredSchedules() {

		// Two tiered schedules each applies to the billing basis of a specific account
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_DORIS_DUKE, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_DORIS_DUKE, INVOICE_DATE, BigDecimal.valueOf(17925), null);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", DateUtils.toDate("07/01/2011"), DateUtils.toDate("06/30/2012"), 366);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("PP Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(50000000), BigDecimal.valueOf(15625), "Account #: 183050" + StringUtils.NEW_LINE + "$ 25,000,000 at 0.0375 %" + StringUtils.NEW_LINE
								+ "$ 25,000,000 at 0.025 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("183050: Doris Duke Protection Plus Options Hedging - Period Average_BillingBasis_50,000,000_Percentage_100_BillingAmount_15,625.00"));
			}
			else if ("PIOS Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(10000000), BigDecimal.valueOf(2500), "Account #: 183000" + StringUtils.NEW_LINE + "$ 10,000,000 at 0.025 %" + StringUtils.NEW_LINE,
						CollectionUtils.createList("183000: Doris Duke PIOS Synthetic Exposure - Period Average_BillingBasis_10,000,000_Percentage_100_BillingAmount_2,500.00"));
			}
			else {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("183000: Doris Duke PIOS_BillingBasis_0_Percentage_50_BillingAmount_-100.00");
				accountDetailList.add("183050: Doris Duke Protection Plus_BillingBasis_0_Percentage_50_BillingAmount_-100.00");
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-200), "$ -200 Credit", accountDetailList);
			}
		}
	}


	@Test
	public void testBillingBasisShifterProcessingSchedules() {
		Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any(InvestmentAccountSearchForm.class))).thenReturn(getDukeEnergyPIOSAccounts());

		// One tiered for PIOS, the other fixed % for currency account, but billing basis shifter post processor moves some of the billing basis from the currency account to the pios account
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_DUKE_ENERGY, INVOICE_DATE);
		//validateInvoiceSummary(invoice, BILLING_DEFINITION_DUKE_ENERGY, INVOICE_DATE, BigDecimal.valueOf(56624), null);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("PIOS Tiered Fee".equals(detail.getBillingSchedule().getName())) {
				List<String> accountDetailList = new ArrayList<>();
				accountDetailList.add("123400: Duke Energy PIOS - 1234 Synthetic Exposure - Period Average_BillingBasis_42,863,000_Percentage_42.863_BillingAmount_13,394.69");
				accountDetailList.add("186000: Duke Energy PIOS Synthetic Exposure - Period Average_BillingBasis_57,137,000_Percentage_57.137_BillingAmount_17,855.31");

				validateInvoiceDetail(detail, BigDecimal.valueOf(100000000), BigDecimal.valueOf(31250), "Applied billing basis adjustment of [42,863,000] because [Fill PIOS account to 100 million]"
						+ StringUtils.NEW_LINE + "$ 25,000,000 at 0.05 %" + StringUtils.NEW_LINE + "$ 75,000,000 at 0.025 %" + StringUtils.NEW_LINE, accountDetailList);
			}
			else if ("Currency Fixed Percentage".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(338317669), BigDecimal.valueOf(25374), "Applied billing basis adjustment of [-42,863,000] because [Fill PIOS account to 100 million]"
								+ StringUtils.NEW_LINE + "Account #: 186100" + StringUtils.NEW_LINE + "$ 338,317,669 at 0.03 % Per Year" + StringUtils.NEW_LINE,
						CollectionUtils.createList("186100: Duke Energy Currency Synthetic Exposure - Period Average_BillingBasis_338,317,669_Percentage_100_BillingAmount_25,374.00"));
			}
		}
	}


	private List<InvestmentAccount> getDukeEnergyPIOSAccounts() {
		List<InvestmentAccount> list = new ArrayList<>();
		list.add(this.investmentAccountDAO.findByPrimaryKey(110));
		list.add(this.investmentAccountDAO.findByPrimaryKey(112));
		return list;
	}


	@Test
	public void testPeriodMonthEndAverage() {
		// Fixed Period - defined annually, billed quarterly
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_TARGET, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_TARGET, INVOICE_DATE, BigDecimal.valueOf(3858.00), null);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice.getDetailList()));
		BillingInvoiceDetail detail = invoice.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.valueOf(8573495), BigDecimal.valueOf(3858.00), "$ 8,573,495 at 0.18 % per year" + StringUtils.NEW_LINE,
				CollectionUtils.createList("604900: Target Corp Master Trust Market Value - Period Month End Average_BillingBasis_8,573,495_Percentage_100_BillingAmount_3,858.00"));
	}


	@Test
	public void testQuarterlyAndAnnualMinWithRebate() {
		// Create/Generate Each Invoice For the Year
		Date invoiceDate = DateUtils.toDate("03/31/2011");
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(20000.00), null, 2);

		invoiceDate = DateUtils.toDate("06/30/2011");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(20000.00), null, 2);

		invoiceDate = DateUtils.toDate("09/30/2011");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(18750.00), null, 1);

		invoiceDate = DateUtils.toDate("12/31/2011");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(20750.00), null, 1);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(30000000), BigDecimal.valueOf(30000), "$ 30,000,000 at 0.1 % per quarter", null);
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-9250), "Annual Minimum Fee: $ 75,000	Billed YTD: $ 88,750	Credit for Previous Minimum Fees: $ 9,250", null);
			}
		}
	}


	@Test
	public void testQuarterlyAndAnnualMinWithRebate_2() {
		// Create/Generate Each Invoice For the Year
		Date invoiceDate = DateUtils.toDate("03/31/2012");
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(20000.00), null, 2);

		invoiceDate = DateUtils.toDate("06/30/2012");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(20000.00), null, 2);

		invoiceDate = DateUtils.toDate("09/30/2012");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(18750.00), null, 1);

		invoiceDate = DateUtils.toDate("12/31/2012");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(16250.00), null, 1);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(25000000), BigDecimal.valueOf(25000), "$ 25,000,000 at 0.1 % per quarter", null);
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-8750), "Annual Minimum Fee: $ 75,000	Billed YTD: $ 83,750	Credit for Previous Minimum Fees: $ 8,750", null);
			}
		}
	}


	@Test
	public void testQuarterlyAndAnnualMinWithRebate_AnnualMinHitEarly() {
		// Create/Generate Each Invoice For the Year
		Date invoiceDate = DateUtils.toDate("03/31/2013");
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(50000.00), null, 2);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(50000000), BigDecimal.valueOf(50000), "$ 50,000,000 at 0.1 % per quarter", null);
			}
		}

		invoiceDate = DateUtils.toDate("06/30/2013");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(50000.00), null, 2);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(50000000), BigDecimal.valueOf(50000), "$ 50,000,000 at 0.1 % per quarter", null);
			}
		}

		invoiceDate = DateUtils.toDate("09/30/2013");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(10750.00), null, 0);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(10750000), BigDecimal.valueOf(10750), "$ 10,750,000 at 0.1 % per quarter", null);
			}
			else if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(8000), "$ 18,750 per Quarter	Minimum Fee: $ 18,750	Billed: $ 10,750", null);
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-8000), "Annual Minimum Fee: $ 75,000	Billed YTD: $ 118,750	Credit for Previous Minimum Fees: $ 8,000", null);
			}
		}

		invoiceDate = DateUtils.toDate("12/31/2013");
		invoice = generateBillingInvoice(BILLING_DEFINITION_BLUE_ROCK, invoiceDate);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_BLUE_ROCK, invoiceDate, BigDecimal.valueOf(10250.00), null, 0);

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if ("Fee Schedule".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.valueOf(10250000), BigDecimal.valueOf(10250), "$ 10,250,000 at 0.1 % per quarter", null);
			}
			else if ("Quarterly Minimum".equals(detail.getBillingSchedule().getName())) {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(8500), "$ 18,750 per Quarter	Minimum Fee: $ 18,750	Billed: $ 10,250", null);
			}
			else {
				validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(-8500), "Annual Minimum Fee: $ 75,000	Billed YTD: $ 129,500	Credit for Previous Minimum Fees: $ 8,500", null);
			}
		}
	}


	@Test
	public void testQuarterlyInvoice_MonthlyAccrual_RevenueShare_External_Internal() {
		BillingInvoice invoice = generateBillingInvoice(BILLING_DEFINITION_MERRILL, INVOICE_DATE);
		validateInvoiceSummary(invoice, BILLING_DEFINITION_MERRILL, INVOICE_DATE, BigDecimal.valueOf(24827.30), BigDecimal.valueOf(-6206.82), BigDecimal.valueOf(-4655.12), null, 0);
		validateInvoiceDates(invoice, "invoicePeriod", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "invoiceBilling", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, 92);
		validateInvoiceDates(invoice, "annualInvoicePeriod", null, null, null);
		validateInvoiceDates(invoice, "billingBasis", INVOICE_PERIOD_START_DATE, INVOICE_PERIOD_END_DATE, null);
		Assertions.assertEquals(9, CollectionUtils.getSize(invoice.getDetailList()));

		for (BillingInvoiceDetail detail : invoice.getDetailList()) {
			if (BillingInvoiceRevenueTypes.REVENUE == detail.getCoalesceRevenueType()) {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.valueOf(16574740), BigDecimal.valueOf(8287.37), "07/01/2011 - 07/31/2011:" + StringUtils.NEW_LINE + "$ 16,574,740 at 0.6 % per Year" + StringUtils.NEW_LINE,
							CollectionUtils.createList(
									"W-000608: Lucia GST - INTC Market Value - Period End_BillingBasis_1,214,262_Percentage_7.326_BillingAmount_607.13",
									"W-000243: Martha Parsons - INTU Market Value - Period End_BillingBasis_15,360,478_Percentage_92.674_BillingAmount_7,680.24"
							));
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.valueOf(17111385.00), BigDecimal.valueOf(8555.69), "08/01/2011 - 08/31/2011:" + StringUtils.NEW_LINE + "$ 17,111,385 at 0.6 % per Year" + StringUtils.NEW_LINE,
							CollectionUtils.createList(
									"W-000608: Lucia GST - INTC Market Value - Period End_BillingBasis_1,138,836_Percentage_6.6554_BillingAmount_569.42",
									"W-000243: Martha Parsons - INTU Market Value - Period End_BillingBasis_15,972,549_Percentage_93.3446_BillingAmount_7,986.27"
							));
				}
				else {
					validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.valueOf(15968485), BigDecimal.valueOf(7984.24), "09/01/2011 - 09/30/2011:" + StringUtils.NEW_LINE + "$ 15,968,485 at 0.6 % per Year" + StringUtils.NEW_LINE,
							CollectionUtils.createList(
									"W-000608: Lucia GST - INTC Market Value - Period End_BillingBasis_1,237,802_Percentage_7.7515_BillingAmount_618.90",
									"W-000243: Martha Parsons - INTU Market Value - Period End_BillingBasis_14,730,683_Percentage_92.2485_BillingAmount_7,365.34"
							));
				}
			}
			else if (BillingInvoiceRevenueTypes.REVENUE_SHARE_EXTERNAL == detail.getCoalesceRevenueType()) {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(-2071.84), "$ 8,287.37 at -25 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_7.326_BillingAmount_-151.78", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_92.674_BillingAmount_-1,920.06"));
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(-2138.92), "$ 8,555.69 at -25 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_6.6555_BillingAmount_-142.35", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_93.3445_BillingAmount_-1,996.57"));
				}
				else {
					validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.ZERO, BigDecimal.valueOf(-1996.06), "$ 7,984.24 at -25 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_7.7515_BillingAmount_-154.72", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_92.2485_BillingAmount_-1,841.34"));
				}
			}
			else {
				if ("07/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "07/01/2011", "07/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(-1553.88), "$ 8,287.37 at -18.75 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_7.326_BillingAmount_-113.84", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_92.674_BillingAmount_-1,440.04"));
				}
				else if ("08/01/2011".equals(DateUtils.fromDateShort(detail.getAccrualStartDate()))) {
					validateInvoiceDetail(detail, "08/01/2011", "08/31/2011", BigDecimal.ZERO, BigDecimal.valueOf(-1604.19), "$ 8,555.69 at -18.75 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_6.6555_BillingAmount_-106.77", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_93.3445_BillingAmount_-1,497.42"));
				}
				else {
					validateInvoiceDetail(detail, "09/01/2011", "09/30/2011", BigDecimal.ZERO, BigDecimal.valueOf(-1497.05), "$ 7,984.24 at -18.75 %" + StringUtils.NEW_LINE,
							CollectionUtils.createList("W-000608: Lucia GST - INTC_BillingBasis_0_Percentage_7.7515_BillingAmount_-116.04", "W-000243: Martha Parsons - INTU_BillingBasis_0_Percentage_92.2485_BillingAmount_-1,381.01"));
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testManualInvoiceDetail() {
		// Standard Flat Fee - defined monthly, billed quarterly
		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15000.00), null);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		BillingInvoiceDetail detail = invoice_mcknight.getDetailList().get(0);
		validateInvoiceDetail(detail, BigDecimal.ZERO, BigDecimal.valueOf(15000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
				CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_15,000.00"));

		BillingInvoiceDetail manualDetail = new BillingInvoiceDetail();
		manualDetail.setInvoice(invoice_mcknight);
		manualDetail.setBillingAmount(BigDecimal.valueOf(500.00));
		manualDetail.setNote("Test Manual Entry");

		BillingInvoiceDetailAccount manualDetailAccount = new BillingInvoiceDetailAccount();
		manualDetailAccount.setBillingAmount(manualDetail.getBillingAmount());
		manualDetailAccount.setInvestmentAccount(this.investmentAccountDAO.findOneByField("number", "392000"));
		manualDetail.setDetailAccountList(CollectionUtils.createList(manualDetailAccount));
		manualDetail.setRevenueType(BillingInvoiceRevenueTypes.REVENUE);
		this.billingInvoiceService.saveBillingInvoiceDetail(manualDetail, null);

		invoice_mcknight = this.billingInvoiceService.getBillingInvoice(invoice_mcknight.getId());
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15500.00), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
		for (BillingInvoiceDetail bd : invoice_mcknight.getDetailList()) {
			// Original Detail Record
			if (detail.equals(bd)) {
				Assertions.assertFalse(bd.isManual());
				validateInvoiceDetail(bd, BigDecimal.ZERO, BigDecimal.valueOf(15000.00), "$ 5,000 per month" + StringUtils.NEW_LINE,
						CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_15,000.00"));
			}
			else {
				Assertions.assertTrue(bd.isManual());
				validateInvoiceDetail(bd, BigDecimal.ZERO, BigDecimal.valueOf(500.00), "Test Manual Entry",
						CollectionUtils.createList("392000: McKnight Foundation_BillingBasis_0_Percentage_100_BillingAmount_500.00"));
			}
		}

		// Now, Reprocess the invoice and ensure total remains
		this.billingInvoiceProcessService.processBillingInvoice(invoice_mcknight.getId());
		validateInvoiceSummary(invoice_mcknight, BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE, BigDecimal.valueOf(15500.00), null);
		Assertions.assertEquals(2, CollectionUtils.getSize(invoice_mcknight.getDetailList()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvoiceDependencies_QuarterlyInvoiceDependsOnQuarterlyInvoice() {
		// The links don't actually do anything as far as processing, however we are testing that creating the dependent will automatically create the pre-requisite

		// Quarterly Invoice that Depends On Another Quarterly Invoice
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		// Acument Invoice Should Also Exist
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(BILLING_DEFINITION_ACUMENT);
		searchForm.setInvoiceStartDate(invoice_mcknight.getInvoicePeriodStartDate());
		searchForm.setInvoiceEndDate(invoice_mcknight.getInvoicePeriodEndDate());
		Assertions.assertEquals(1, CollectionUtils.getSize(this.billingInvoiceService.getBillingInvoiceList(searchForm)));
	}


	@Test
	public void testInvoiceDependencies_QuarterlyInvoiceDependsOnMonthlyInvoice() {
		// The links don't actually do anything as far as processing, however we are testing that creating the dependent will automatically create the pre-requisite

		// Change Acument to be Monthly
		BillingDefinition billingDefinition = this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_ACUMENT);
		billingDefinition.setBillingFrequency(BillingFrequencies.MONTHLY);
		this.billingDefinitionService.saveBillingDefinition(billingDefinition);

		// Quarterly Invoice that Depends On A Monthly Invoice
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE));
		dependency.setBillingDefinition(billingDefinition);
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		// 3 Acument Invoices Should Also Exist
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(BILLING_DEFINITION_ACUMENT);
		searchForm.setInvoiceStartDate(invoice_mcknight.getInvoicePeriodStartDate());
		searchForm.setInvoiceEndDate(invoice_mcknight.getInvoicePeriodEndDate());
		Assertions.assertEquals(3, CollectionUtils.getSize(this.billingInvoiceService.getBillingInvoiceList(searchForm)));
	}


	@Test
	public void testInvoiceDependencies_QuarterlyInvoiceDependsOnSharedQuarterlyInvoice() {
		// The links don't actually do anything as far as processing, however we are testing that creating the dependent will automatically create the pre-requisite

		// Quarterly Invoice that Depends On Another Quarterly Invoice
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_MCKNIGHT_FLAT_FEE));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_FAIRFAX_000));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		BillingInvoice invoice_mcknight = generateBillingInvoice(BILLING_DEFINITION_MCKNIGHT, INVOICE_DATE);
		// Fairfax Invoice Should Also Exist
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(BILLING_DEFINITION_FAIRFAX_000);
		searchForm.setInvoiceStartDate(invoice_mcknight.getInvoicePeriodStartDate());
		searchForm.setInvoiceEndDate(invoice_mcknight.getInvoicePeriodEndDate());
		List<BillingInvoice> invoiceList = this.billingInvoiceService.getBillingInvoiceList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoiceList));

		BillingInvoice invoice_fairfax_000 = invoiceList.get(0);
		// Make sure invoice is still part of a group
		Assertions.assertNotNull(invoice_fairfax_000.getInvoiceGroup());

		// Get the invoices in the group - should be 3 of them
		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice_fairfax_000.getInvoiceGroup().getId());
		Assertions.assertEquals(3, group.getInvoiceList().size());
	}


	@Test
	public void testInvoiceDependencies_SharedQuarterlyInvoiceDependsOnQuarterlyInvoice() {
		// The links don't actually do anything as far as processing, however we are testing that creating the dependent will automatically create the pre-requisite

		// Shared Quarterly Invoice that Depends On Another Quarterly Invoice - Triggered by the definition in the group that doesn't have the dependency defined
		BillingScheduleBillingDefinitionDependency dependency = new BillingScheduleBillingDefinitionDependency();
		dependency.setBillingSchedule(this.billingDefinitionService.getBillingSchedule(BILLING_SCHEDULE_FAIRFAX_000_RETAINER));
		dependency.setBillingDefinition(this.billingDefinitionService.getBillingDefinition(BILLING_DEFINITION_MCKNIGHT));
		this.billingDefinitionService.saveBillingScheduleBillingDefinitionDependency(dependency);

		BillingInvoice invoice_fairfax_500 = generateBillingInvoice(BILLING_DEFINITION_FAIRFAX_500, INVOICE_DATE);

		// McKnight Invoice should also exist
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setBillingDefinitionId(BILLING_DEFINITION_MCKNIGHT);
		searchForm.setInvoiceStartDate(invoice_fairfax_500.getInvoicePeriodStartDate());
		searchForm.setInvoiceEndDate(invoice_fairfax_500.getInvoicePeriodEndDate());
		List<BillingInvoice> invoiceList = this.billingInvoiceService.getBillingInvoiceList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(invoiceList));

		// Fairfax Invoice Should still be part of a group
		Assertions.assertNotNull(invoice_fairfax_500.getInvoiceGroup());

		// Get the invoices in the group - should be 3 of them
		BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice_fairfax_500.getInvoiceGroup().getId());
		Assertions.assertEquals(3, group.getInvoiceList().size());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void updateBillingDefinitionInvoiceType(int billingDefinitionId, short invoiceTypeId) {
		BillingDefinition definition = this.billingDefinitionService.getBillingDefinition(billingDefinitionId);
		definition.setInvoiceType(this.billingInvoiceService.getBillingInvoiceType(invoiceTypeId));
		this.billingDefinitionService.saveBillingDefinition(definition);
	}


	private BillingInvoice generateBillingInvoice(Integer billingDefinitionId, Date invoiceDate) {
		BillingInvoice invoice = this.billingInvoiceProcessService.generateBillingInvoice(billingDefinitionId, invoiceDate);
		if (invoice.getInvoiceGroup() == null) {
			this.billingInvoiceProcessService.processBillingInvoice(invoice.getId());
		}
		// If it's in a group - process the whole group now
		else {
			this.billingInvoiceProcessService.processBillingInvoiceGroup(invoice.getInvoiceGroup().getId());
		}

		return this.billingInvoiceService.getBillingInvoice(invoice.getId());
	}


	private void validateInvoiceSummary(BillingInvoice invoice, Integer definitionId, Date invoiceDate, BigDecimal totalAmount, BillingInvoiceGroup group) {
		validateInvoiceSummary(invoice, definitionId, invoiceDate, totalAmount, null, null, group, 0);
	}


	private void validateInvoiceSummary(BillingInvoice invoice, Integer definitionId, Date invoiceDate, BigDecimal totalAmount, BillingInvoiceGroup group, int warningCount) {
		validateInvoiceSummary(invoice, definitionId, invoiceDate, totalAmount, null, null, group, warningCount);
	}


	private void printInvoice(BillingInvoice invoice) {
		System.out.println("************************************");
		System.out.println("Invoice: " + DateUtils.fromDateShort(invoice.getInvoiceDate()) + " " + invoice.getInvoiceNumberAndAmountLabel());
		System.out.println("Gross Revenue: " + CoreMathUtils.formatNumberMoney(invoice.getRevenueGrossTotalAmount()));
		System.out.println("Broker Fee: " + CoreMathUtils.formatNumberMoney(invoice.getRevenueShareExternalTotalAmount()));
		System.out.println("Revenue Share: " + CoreMathUtils.formatNumberMoney(invoice.getRevenueShareInternalTotalAmount()));

		int count = 1;
		for (BillingInvoiceDetail invoiceDetail : CollectionUtils.getIterable(invoice.getDetailList())) {
			System.out.println(count + ": " + (invoiceDetail.getBillingSchedule() == null ? "Manual Invoice Detail" : invoiceDetail.getBillingSchedule().getName()) + StringUtils.TAB + CoreMathUtils.formatNumberMoney(invoiceDetail.getBillingAmount()));
			if (invoiceDetail.getAccrualStartDate() != null) {
				System.out.println("Accrual Period: " + DateUtils.fromDateRange(invoiceDetail.getAccrualStartDate(), invoiceDetail.getAccrualEndDate(), true));
			}
			System.out.println("   Billing Basis: " + CoreMathUtils.formatNumberMoney(invoiceDetail.getBillingBasis()));
			System.out.println("   Revenue Type: " + invoiceDetail.getCoalesceRevenueType());
			System.out.println(invoiceDetail.getNote());
			for (BillingInvoiceDetailAccount detailAccount : invoiceDetail.getDetailAccountList()) {
				System.out.println(detailAccount.getAccountLabel() + "(" + CoreMathUtils.formatNumberMoney(detailAccount.getAllocationPercentage()) + " %)" + StringUtils.TAB + " Billing Basis: "
						+ CoreMathUtils.formatNumberMoney(detailAccount.getBillingBasis()) + ", Amount: " + CoreMathUtils.formatNumberMoney(detailAccount.getBillingAmount()));
			}
			count++;
		}
		List<RuleViolation> violationList = this.ruleViolationService.getRuleViolationListByLinkedEntity("BillingInvoice", invoice.getId());
		System.out.println("Violations: " + (CollectionUtils.getSize(violationList) == 0 ? "NONE" : ""));
		count = 1;
		for (RuleViolation w : CollectionUtils.getIterable(violationList)) {
			System.out.println(count + ": " + w.getViolationNote());
			count++;
		}
		System.out.println("************************************");
	}


	private void validateInvoiceSummary(BillingInvoice invoice, Integer definitionId, Date invoiceDate, BigDecimal grossRevenueAmount, BigDecimal externalRevenueShareAmount, BigDecimal internalRevenueShareAmount, BillingInvoiceGroup group, int warningCount) {
		ValidationUtils.assertNotNull(invoice, "Invoice should have been created.");
		//printInvoice(invoice);
		Assertions.assertEquals(definitionId, invoice.getBillingDefinition().getId());
		Assertions.assertEquals(invoice.getBillingDefinition().getInvoiceType(), invoice.getInvoiceType());
		Assertions.assertEquals(invoiceDate, invoice.getInvoiceDate());
		Assertions.assertEquals(MathUtils.round(grossRevenueAmount, 2), MathUtils.round(invoice.getRevenueGrossTotalAmount(), 2));
		if (externalRevenueShareAmount != null) {
			Assertions.assertEquals(MathUtils.round(externalRevenueShareAmount, 2), MathUtils.round(invoice.getRevenueShareExternalTotalAmount(), 2));
		}
		else {
			Assertions.assertNull(invoice.getRevenueShareExternalTotalAmount(), "Did not expect any external revenue share amounts.");
		}
		if (internalRevenueShareAmount != null) {
			Assertions.assertEquals(MathUtils.round(internalRevenueShareAmount, 2), MathUtils.round(invoice.getRevenueShareInternalTotalAmount(), 2));
		}
		else {
			Assertions.assertNull(invoice.getRevenueShareExternalTotalAmount(), "Did not expect any internal revenue share amounts.");
		}

		if (group == null) {
			Assertions.assertNull(invoice.getInvoiceGroup());
		}
		else {
			Assertions.assertEquals(group.getId(), invoice.getInvoiceGroup().getId());
			Assertions.assertEquals(invoiceDate, invoice.getInvoiceGroup().getInvoiceDate());
		}

		List<RuleViolation> violationList = this.ruleViolationService.getRuleViolationListByLinkedEntity("BillingInvoice", invoice.getId());
		Assertions.assertEquals(warningCount, CollectionUtils.getSize(violationList));
	}


	private void validateInvoiceDates(BillingInvoice invoice, String dateField, Date startDate, Date endDate, Integer daysInPeriod) {
		ValidationUtils.assertNotNull(invoice, "Invoice should have been created.");
		Assertions.assertEquals(startDate, BeanUtils.getPropertyValue(invoice, dateField + "StartDate"));
		Assertions.assertEquals(endDate, BeanUtils.getPropertyValue(invoice, dateField + "EndDate"));
		if (daysInPeriod != null) {
			Assertions.assertEquals(daysInPeriod, BeanUtils.getPropertyValue(invoice, "daysIn" + StringUtils.capitalize(dateField)));
		}
	}


	private void validateInvoiceDetail(BillingInvoiceDetail detail, BigDecimal billingBasis, BigDecimal billingAmount, String note, List<String> accountDetailStrings) {
		validateInvoiceDetail(detail, null, null, billingBasis, billingAmount, note, accountDetailStrings);
	}


	private void validateInvoiceDetail(BillingInvoiceDetail detail, String accrualStartDate, String accrualEndDate, BigDecimal billingBasis, BigDecimal billingAmount, String note, List<String> accountDetailStrings) {
		ValidationUtils.assertNotNull(detail, "Invoice Detail should have been created.");

		Assertions.assertEquals(accrualStartDate == null ? null : DateUtils.toDate(accrualStartDate), detail.getAccrualStartDate());
		Assertions.assertEquals(accrualEndDate == null ? null : DateUtils.toDate(accrualEndDate), detail.getAccrualEndDate());

		Assertions.assertEquals(MathUtils.round(billingBasis, 0), MathUtils.round(detail.getBillingBasis(), 0));
		Assertions.assertEquals(MathUtils.round(billingAmount, 2), MathUtils.round(detail.getBillingAmount(), 2));
		Assertions.assertEquals((note.replaceAll(StringUtils.NEW_LINE, "").replaceAll(StringUtils.TAB, "")).toLowerCase(),
				(detail.getNote().replaceAll(StringUtils.NEW_LINE, "").replaceAll(StringUtils.TAB, "")).toLowerCase());

		List<BillingInvoiceDetailAccount> detailAccountList = this.billingInvoiceService.getBillingInvoiceDetailAccountListByInvoiceDetail(detail.getId());
		Assertions.assertEquals(MathUtils.round(billingAmount, 2), MathUtils.round(CoreMathUtils.sumProperty(detailAccountList, BillingInvoiceDetailAccount::getBillingAmount), 2));
		Assertions.assertEquals(MathUtils.round(MathUtils.BIG_DECIMAL_ONE_HUNDRED, 0), MathUtils.round(CoreMathUtils.sumProperty(detailAccountList, BillingInvoiceDetailAccount::getAllocationPercentage), 0));

		if (accountDetailStrings != null) {
			for (String expectedAccountDetail : accountDetailStrings) {
				boolean found = false;
				for (BillingInvoiceDetailAccount da : detailAccountList) {
					if (expectedAccountDetail.equalsIgnoreCase(da.toString())) {
						found = true;
						break;
					}
				}
				ValidationUtils.assertTrue(found, "Did not find invoice detail account that matches: " + expectedAccountDetail);
			}
			Assertions.assertEquals(CollectionUtils.getSize(accountDetailStrings), CollectionUtils.getSize(detailAccountList));
		}
	}
}
