package com.clifton.billing;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BillingProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "billing";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("amount");
		approvedList.add("proraters");
		approvedList.add("postprocessors");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();

		skipMethods.add("saveBillingDefinitionInvestmentAccount");

		skipMethods.add("saveBillingScheduleBillingDefinitionDependency");

		skipMethods.add("deleteBillingInvoiceDetail");

		skipMethods.add("saveBillingCommissionSchedule");
		skipMethods.add("saveBillingCommissionScheduleAssignment");

		skipMethods.add("getBillingPaymentInvestmentAccountAllocationList");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveBillingPaymentAllocationEntry");
		return ignoredVoidSaveMethodSet;
	}
}
