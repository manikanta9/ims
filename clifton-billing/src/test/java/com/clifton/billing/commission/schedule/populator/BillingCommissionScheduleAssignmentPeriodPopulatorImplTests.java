package com.clifton.billing.commission.schedule.populator;


import com.clifton.billing.commission.schedule.BillingCommissionSchedule;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignment;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleAssignmentPeriod;
import com.clifton.billing.commission.schedule.BillingCommissionScheduleService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>BillingCommissionScheduleAssignmentPeriodPopulatorImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BillingCommissionScheduleAssignmentPeriodPopulatorImplTests {

	@Resource
	private BillingCommissionScheduleService billingCommissionScheduleService;

	@Resource
	private BillingCommissionScheduleAssignmentPeriodPopulator billingCommissionScheduleAssignmentPeriodPopulator;


	///////////////////////////////////////////////

	private static final Integer SCHEDULE_4_YEARS = 10;
	private static final Integer SCHEDULE_2_YEARS_6_MONTHS = 20;
	private static final Integer SCHEDULE_3_YEARS_3_MONTHS_45_DAYS = 30;


	///////////////////////////////////////////////
	/////        Test Invalid Data            /////
	///////////////////////////////////////////////


	@Test
	public void testScheduleAssignmentPeriodPopulator_OverlapInHoldPeriods() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_4_YEARS);

			BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
			assignment.setReferenceOne(schedule);
			assignment.setStartDate(DateUtils.toDate("04/01/2013"));
			addHoldingPeriod(assignment, "04/01/2014", "03/31/2015");
			addHoldingPeriod(assignment, "01/01/2015", "04/30/2015");
			this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		}, "Hold Periods cannot overlap: 04/01/2014 - 03/31/2015, and 01/01/2015 - 04/30/2015");
	}


	@Test
	public void testScheduleAssignmentPeriodPopulator_HoldPeriodBeforeStartDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_4_YEARS);

			BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
			assignment.setReferenceOne(schedule);
			assignment.setStartDate(DateUtils.toDate("04/01/2013"));
			addHoldingPeriod(assignment, "03/31/2013", "03/31/2015");
			this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		}, "Holding Periods cannot start before account assignment start date of (04/01/2013).");
	}


	@Test
	public void testScheduleAssignmentPeriodPopulator_HoldPeriodAfterEndDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_4_YEARS);

			BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
			assignment.setReferenceOne(schedule);
			assignment.setStartDate(DateUtils.toDate("04/01/2013"));
			addHoldingPeriod(assignment, "04/01/2018", "03/31/2019");
			this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		}, "Holding Periods cannot start after account assignment end date (currently calculated as: 03/31/2017).");
	}


	///////////////////////////////////////////////
	/////         Test Valid Data             /////
	///////////////////////////////////////////////


	@Test
	public void testScheduleAssignmentPeriodPopulator_4_Year_Schedule() {
		// SCHEDULE_4_YEARS
		BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_4_YEARS);

		BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
		assignment.setReferenceOne(schedule);
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("03/31/2017", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 04/01/2013 - 03/31/2015 10%|2: 04/01/2015 - 03/31/2016 5%|3: 04/01/2016 - 03/31/2017 2.5%|", getPeriodListAsString(assignment.getPeriodList()));

		// Change Assignment Start Date
		assignment.setStartDate(DateUtils.toDate("12/15/2013"));
		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("12/14/2017", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 12/15/2013 - 12/14/2015 10%|2: 12/15/2015 - 12/14/2016 5%|3: 12/15/2016 - 12/14/2017 2.5%|", getPeriodListAsString(assignment.getPeriodList()));

		// Change start date and add a Hold Period
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));
		// Add One Quarter Hold Period
		addHoldingPeriod(assignment, "04/01/2014", "06/30/2014");

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("06/30/2017", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 04/01/2013 - 03/31/2014 10%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 10%|4: 07/01/2015 - 06/30/2016 5%|5: 07/01/2016 - 06/30/2017 2.5%|",
				getPeriodListAsString(assignment.getPeriodList()));

		// Add a Second 6 Month Hold Period
		addHoldingPeriod(assignment, "08/15/2015", "02/14/2016");
		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 10%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 10%|4: 07/01/2015 - 08/14/2015 5%|5: 08/15/2015 - 02/14/2016 (HOLD PERIOD) 0%|6: 02/15/2016 - 12/31/2016 5%|7: 01/01/2017 - 12/31/2017 2.5%|",
				getPeriodListAsString(assignment.getPeriodList()));
		Assertions.assertEquals("12/31/2017", DateUtils.fromDateShort(assignment.getEndDate()));
	}


	@Test
	public void testScheduleAssignmentPeriodPopulator_2_Year_6_Month_Schedule() {
		BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_2_YEARS_6_MONTHS);

		BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
		assignment.setReferenceOne(schedule);
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("09/30/2015", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 04/01/2013 - 03/31/2014 12%|2: 04/01/2014 - 03/31/2015 7.5%|3: 04/01/2015 - 09/30/2015 5%|", getPeriodListAsString(assignment.getPeriodList()));

		// Change Assignment Start Date
		assignment.setStartDate(DateUtils.toDate("12/15/2013"));
		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("06/14/2016", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 12/15/2013 - 12/14/2014 12%|2: 12/15/2014 - 12/14/2015 7.5%|3: 12/15/2015 - 06/14/2016 5%|", getPeriodListAsString(assignment.getPeriodList()));

		// Change start date and add a Hold Period
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));
		// Add One Quarter Hold Period
		addHoldingPeriod(assignment, "04/01/2014", "06/30/2014");

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("12/31/2015", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 04/01/2013 - 03/31/2014 12%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 12/31/2015 5%|",
				getPeriodListAsString(assignment.getPeriodList()));

		// Add a Second 6 Month Hold Period
		addHoldingPeriod(assignment, "08/15/2015", "02/14/2016");

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 12%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 08/14/2015 5%|5: 08/15/2015 - 02/14/2016 (HOLD PERIOD) 0%|6: 02/15/2016 - 07/02/2016 5%|",
				getPeriodListAsString(assignment.getPeriodList()));
		Assertions.assertEquals("07/02/2016", DateUtils.fromDateShort(assignment.getEndDate()));
	}


	@Test
	public void testScheduleAssignmentPeriodPopulator_3_Year_3_Month_45_Day_Schedule() {
		BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_3_YEARS_3_MONTHS_45_DAYS);

		BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
		assignment.setReferenceOne(schedule);
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("08/14/2016", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 04/01/2013 - 03/31/2015 7.5%|2: 04/01/2015 - 03/31/2016 3.5%|3: 04/01/2016 - 06/30/2016 2.5%|4: 07/01/2016 - 08/14/2016 1%|",
				getPeriodListAsString(assignment.getPeriodList()));

		// Change Assignment Start Date
		assignment.setStartDate(DateUtils.toDate("12/15/2013"));
		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("04/28/2017", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals("1: 12/15/2013 - 12/14/2015 7.5%|2: 12/15/2015 - 12/14/2016 3.5%|3: 12/15/2016 - 03/14/2017 2.5%|4: 03/15/2017 - 04/28/2017 1%|",
				getPeriodListAsString(assignment.getPeriodList()));

		// Change start date and add a Hold Period
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));
		// Add One Quarter Hold Period
		addHoldingPeriod(assignment, "04/01/2014", "06/30/2014");

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals("11/14/2016", DateUtils.fromDateShort(assignment.getEndDate()));
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 7.5%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 06/30/2016 3.5%|5: 07/01/2016 - 09/30/2016 2.5%|6: 10/01/2016 - 11/14/2016 1%|",
				getPeriodListAsString(assignment.getPeriodList()));

		// Add a Second 6 Month Hold Period
		addHoldingPeriod(assignment, "08/15/2015", "02/14/2016");
		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 7.5%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 08/14/2015 3.5%|5: 08/15/2015 - 02/14/2016 (HOLD PERIOD) 0%|6: 02/15/2016 - 12/31/2016 3.5%|7: 01/01/2017 - 03/31/2017 2.5%|8: 04/01/2017 - 05/15/2017 1%|",
				getPeriodListAsString(assignment.getPeriodList()));
		Assertions.assertEquals("05/15/2017", DateUtils.fromDateShort(assignment.getEndDate()));
	}


	@Test
	public void testScheduleAssignmentPeriodPopulator_MultipleBreaks() {
		// SCHEDULE_4_YEARS
		BillingCommissionSchedule schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_4_YEARS);

		BillingCommissionScheduleAssignment assignment = new BillingCommissionScheduleAssignment();
		assignment.setReferenceOne(schedule);
		assignment.setStartDate(DateUtils.toDate("04/01/2013"));
		// Add One Quarter Hold Period
		addHoldingPeriod(assignment, "04/01/2014", "06/30/2014");
		// Add a Second 6 Month Hold Period
		addHoldingPeriod(assignment, "08/15/2015", "02/14/2016");

		// SCHEDULE_2_YEARS_6_MONTHS
		schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_2_YEARS_6_MONTHS);
		assignment.setReferenceOne(schedule);

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 12%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 08/14/2015 5%|5: 08/15/2015 - 02/14/2016 (HOLD PERIOD) 0%|6: 02/15/2016 - 07/02/2016 5%|",
				getPeriodListAsString(assignment.getPeriodList()));
		Assertions.assertEquals("07/02/2016", DateUtils.fromDateShort(assignment.getEndDate()));

		// SCHEDULE_3_YEARS_3_MONTHS_45_DAYS
		schedule = this.billingCommissionScheduleService.getBillingCommissionSchedule(SCHEDULE_3_YEARS_3_MONTHS_45_DAYS);
		assignment.setReferenceOne(schedule);

		this.billingCommissionScheduleAssignmentPeriodPopulator.populateBillingCommissionScheduleAssignmentPeriodList(assignment, schedule);
		Assertions.assertEquals(
				"1: 04/01/2013 - 03/31/2014 7.5%|2: 04/01/2014 - 06/30/2014 (HOLD PERIOD) 0%|3: 07/01/2014 - 06/30/2015 7.5%|4: 07/01/2015 - 08/14/2015 3.5%|5: 08/15/2015 - 02/14/2016 (HOLD PERIOD) 0%|6: 02/15/2016 - 12/31/2016 3.5%|7: 01/01/2017 - 03/31/2017 2.5%|8: 04/01/2017 - 05/15/2017 1%|",
				getPeriodListAsString(assignment.getPeriodList()));
		Assertions.assertEquals("05/15/2017", DateUtils.fromDateShort(assignment.getEndDate()));
	}


	///////////////////////////////////////////////


	private void addHoldingPeriod(BillingCommissionScheduleAssignment assignment, String startDate, String endDate) {
		BillingCommissionScheduleAssignmentPeriod period = new BillingCommissionScheduleAssignmentPeriod();
		period.setStartDate(DateUtils.toDate(startDate));
		period.setEndDate(DateUtils.toDate(endDate));
		if (assignment.getPeriodList() == null) {
			assignment.setPeriodList(CollectionUtils.createList(period));
		}
		else {
			assignment.getPeriodList().add(period);
		}
	}


	private String getPeriodListAsString(List<BillingCommissionScheduleAssignmentPeriod> periodList) {
		if (CollectionUtils.isEmpty(periodList)) {
			return "NO PERIODS";
		}
		StringBuilder sb = new StringBuilder(16);
		periodList = BeanUtils.sortWithFunction(periodList, BillingCommissionScheduleAssignmentPeriod::getStartDate, true);
		int counter = 1;
		for (BillingCommissionScheduleAssignmentPeriod period : periodList) {
			sb.append(counter).append(": ");
			sb.append(DateUtils.fromDateShort(period.getStartDate()));
			sb.append(" - ");
			sb.append(DateUtils.fromDateShort(period.getEndDate()));
			if (period.isHoldPeriod()) {
				sb.append(" (HOLD PERIOD)");
			}
			sb.append(" ").append(CoreMathUtils.formatNumberDecimal(period.getPeriodPercentage())).append("%");
			sb.append("|");
			counter++;
		}
		return sb.toString();
	}
}
