Clifton.compliance.ComplianceSetupOldWindow = Ext.extend(TCG.app.Window, {
	id: 'complianceSetupOldWindow',
	title: 'Trading Rules',
	iconCls: 'verify',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Short/Long',
				items: [{
					name: 'complianceRuleOldListFind',
					xtype: 'gridpanel',
					instructions: 'Global or account specific rules for investments that are allowed long or short positions.',
					columns: [
						{header: 'Rule Type', width: 70, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
						{header: 'Client Account', width: 90, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 40, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', width: 60, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},

						{header: 'Investment Type', width: 40, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Hierarchy', width: 90, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 60, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},

						{header: 'Short Allowed', width: 40, dataIndex: 'shortPositionAllowed', type: 'boolean', align: 'center'},
						{header: 'Long Allowed', width: 40, dataIndex: 'longPositionAllowed', type: 'boolean', align: 'center'},
						{header: 'Ignorable', width: 30, dataIndex: 'ignorable', type: 'boolean'}

					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 180, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientInvestmentAccount.teamSecurityGroup.name'},
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientInvestmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'}
						];
					},
					getLoadParams: function(firstLoad) {
						return {
							typeName: this.editor.complianceRuleTypeName
						};
					},
					editor: {
						detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
						complianceRuleTypeName: 'Short/Long',
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								typeName: this.complianceRuleTypeName
							};
						}
					}
				}]
			},


			{
				title: 'Fat Finger Trader Limits',
				items: [{
					name: 'complianceRuleOldListFind',
					xtype: 'gridpanel',
					instructions: 'The following rules define trader specific limits. No trader is allowed to submit a trade that exceeds limits defined here.',
					columns: [
						{header: 'Rule Type', width: 50, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
						{header: 'Trader', width: 30, dataIndex: 'trader.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},

						{header: 'Investment Type', width: 40, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Hierarchy', width: 60, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 60, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},

						{header: 'Long Transaction Quantity', width: 40, dataIndex: 'maxBuyQuantity', type: 'float', useNull: true, hidden: true},
						{header: 'Short Transaction Quantity', width: 40, dataIndex: 'maxSellQuantity', type: 'float', useNull: true, hidden: true},

						{header: 'Long Transaction Notional', width: 40, dataIndex: 'maxBuyAccountingNotional', type: 'float', useNull: true, hidden: true},
						{header: 'Short Transaction Notional', width: 40, dataIndex: 'maxSellAccountingNotional', type: 'float', useNull: true, hidden: true},

						{header: 'Long Position Quantity', width: 40, dataIndex: 'maxLongPositionQuantity', type: 'float', useNull: true, hidden: true},
						{header: 'Short Position Quantity', width: 40, dataIndex: 'maxShortPositionQuantity', type: 'float', useNull: true, hidden: true},

						{header: 'Long Position Notional', width: 40, dataIndex: 'maxLongPositionAccountingNotional', type: 'float', useNull: true},
						{header: 'Short Position Notional', width: 40, dataIndex: 'maxShortPositionAccountingNotional', type: 'float', useNull: true},
						{header: 'Ignorable', width: 40, dataIndex: 'ignorable', type: 'boolean', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						return {
							typeName: this.editor.complianceRuleTypeName,
							requestedMaxDepth: 3
						};
					},
					editor: {
						detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
						complianceRuleTypeName: 'Fat Finger Trader Limit',
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								typeName: this.complianceRuleTypeName
							};
						}
					}
				}]
			},


			{
				title: 'Notional Limits',
				items: [{
					name: 'complianceRuleOldListFind',
					xtype: 'gridpanel',
					instructions: 'The following rules define client specific or global limits for trades and position.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', name: 'clientInvestmentAccountId', width: 200, displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label'}
						];
					},
					columns: [
						{header: 'Rule Type', width: 60, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
						{header: 'Client Account', width: 65, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 40, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', width: 65, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},

						{header: 'Investment Type', width: 40, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Hierarchy', width: 80, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 80, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},

						{header: 'Long Transaction Quantity', width: 40, dataIndex: 'maxBuyQuantity', type: 'float', useNull: true},
						{header: 'Short Transaction Quantity', width: 40, dataIndex: 'maxSellQuantity', type: 'float', useNull: true},

						{header: 'Long Transaction Notional', width: 40, dataIndex: 'maxBuyAccountingNotional', type: 'float', useNull: true},
						{header: 'Short Transaction Notional', width: 40, dataIndex: 'maxSellAccountingNotional', type: 'float', useNull: true},

						{header: 'Long Position Quantity', width: 40, dataIndex: 'maxLongPositionQuantity', type: 'float', useNull: true},
						{header: 'Short Position Quantity', width: 40, dataIndex: 'maxShortPositionQuantity', type: 'float', useNull: true},

						{header: 'Long Position Notional', width: 40, dataIndex: 'maxLongPositionAccountingNotional', type: 'float', useNull: true},
						{header: 'Short Position Notional', width: 40, dataIndex: 'maxShortPositionAccountingNotional', type: 'float', useNull: true},
						{header: 'Ignorable', width: 40, dataIndex: 'ignorable', type: 'boolean', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						return {
							typeName: this.editor.complianceRuleTypeName
						};
					},
					editor: {
						detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
						complianceRuleTypeName: 'Notional Limit',
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								typeName: this.complianceRuleTypeName
							};
						}
					}
				}]
			},


			{
				title: 'Client Approved Contracts',
				items: [{
					name: 'complianceRuleOldListFind',
					xtype: 'gridpanel',
					instructions: 'The following rules define investments that are allowed for client/holding investment accounts.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', name: 'clientInvestmentAccountId', width: 200, displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label'}
						];
					},
					columns: [
						{header: 'Type', width: 100, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
						{header: 'Client Account', width: 100, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 40, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Holding Account', width: 60, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Investment Type', width: 50, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Hierarchy', width: 100, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 150, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},
						{header: 'Ignorable', width: 40, dataIndex: 'ignorable', type: 'boolean', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						return {
							typeName: this.editor.complianceRuleTypeName
						};
					},
					editor: {
						detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
						complianceRuleTypeName: 'Client Approved Contracts',
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								typeName: this.complianceRuleTypeName
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							const typeId = TCG.data.getData('complianceRuleTypeOldByName.json?typeName=' + this.complianceRuleTypeName, gridPanel).id;
							toolBar.add(new TCG.form.ComboBox({name: 'investmentInstrument', displayField: 'labelShort', url: 'investmentInstrumentListFind.json', width: 150, listWidth: 230}));
							toolBar.add(new TCG.form.ComboBox({name: 'clientInvestmentAccount', url: 'investmentAccountListFind.json?ourAccount=true', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Quick Add',
								tooltip: 'Add instrument to selected clients approved contracts.',
								iconCls: 'add',
								handler: function() {
									const investmentInstrumentId = TCG.getChildByName(toolBar, 'investmentInstrument').getValue();
									const clientInvestmentAccountId = TCG.getChildByName(toolBar, 'clientInvestmentAccount').getValue();
									if (investmentInstrumentId === '') {
										TCG.showError('You must first select desired investment instrument.');
									}
									else if (clientInvestmentAccountId === '') {
										TCG.showError('You must first select desired client account.');
									}
									else {
										const params = {'ruleType.id': typeId, 'investmentInstrument.id': investmentInstrumentId, 'clientInvestmentAccount.id': clientInvestmentAccountId};
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Adding...',
											params: params,
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'investmentInstrument').reset();
												TCG.getChildByName(toolBar, 'clientInvestmentAccount').reset();
											}
										});
										loader.load('complianceRuleOldSave.json');
									}
								}
							});
							toolBar.add('-');

							TCG.grid.GridEditor.prototype.addToolbarAddButton.apply(this, arguments);
						}
					}
				}]
			},

			{
				title: 'Rule Types',
				items: [{
					name: 'complianceRuleTypeOldListFind',
					xtype: 'gridpanel',
					instructions: 'The following types of compliance rules are defined in the system.',
					columns: [
						{header: 'Rule Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Account Allowed', width: 50, dataIndex: 'investmentAccountAllowed', type: 'boolean'},
						{header: 'Account Required', width: 50, dataIndex: 'investmentAccountRequired', type: 'boolean'},
						{header: 'Investment Specific', width: 50, dataIndex: 'investmentSpecific', type: 'boolean'},
						{header: 'Trader Specific', width: 50, dataIndex: 'traderSpecific', type: 'boolean'},
						{header: 'Limit Rule', width: 50, dataIndex: 'limitRule', type: 'boolean'},
						{header: 'Long/Short Rule', width: 50, dataIndex: 'shortLongRule', type: 'boolean'},
						{header: 'Ignorable Allowed', width: 40, dataIndex: 'ignorableAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.ComplianceRuleTypeOldWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
