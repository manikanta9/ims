Clifton.compliance.creditrating.CreditRatingOutlookWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Credit Rating Outlook',
	iconCls: 'thumb-down',

	items: [{
		xtype: 'formpanel',
		instructions: 'Credit rating outlook represents a future outlook from a specific credit rating Agency.',
		url: 'complianceCreditRatingOutlook.json',
		items: [
			{fieldLabel: 'Agency', name: 'ratingAgency.label', hiddenName: 'ratingAgency.id', xtype: 'combo', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Credit Rating Agency', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'Outlook', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Weight', name: 'outlookWeight', xtype: 'spinnerfield'}
		]
	}]
});

