/**
 * The detail window for displaying credit rating type information.
 */
Clifton.compliance.creditrating.CreditRatingTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Credit Rating Type',
	iconCls: 'thumb-down',

	items: [{
		xtype: 'formpanel',
		instructions: 'The credit rating type specifies the type of the credit rating assigned to its recipient.',
		url: 'complianceCreditRatingType.json',
		readOnly: true,
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true},
			{boxLabel: 'Apply credit ratings of this type to Securities (as opposed to Issuers)', name: 'securityRating', xtype: 'checkbox'}
		]
	}]
});

