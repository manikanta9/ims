Clifton.compliance.creditrating.CreditRatingValueWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Credit Rating Value',
	iconCls: 'thumb-down',

	items: [{
		xtype: 'formpanel',
		instructions: 'Different credit rating agencies use different rating schemes and also have separate sets of ratings for short-term and long-term assets.',
		url: 'complianceCreditRatingValue.json?requestedPropertiesToExclude=data.assignee',
		items: [
			{
				fieldLabel: 'Rating Type',
				xtype: 'linkfield',
				name: 'type.label',
				detailIdField: 'type.id',
				detailPageClass: 'Clifton.compliance.creditrating.CreditRatingTypeWindow',
				submitValue: false,
				readOnly: true
			},
			{
				fieldLabel: 'Security',
				xtype: 'combo',
				name: 'security.label',
				hiddenName: 'security.id',
				displayField: 'label',
				url: 'investmentSecurityListFind.json',
				detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				hidden: true
			},
			{
				fieldLabel: 'Issuer',
				xtype: 'combo',
				name: 'issuerCompany.label',
				hiddenName: 'issuerCompany.id',
				displayField: 'label',
				url: 'businessCompanyListFind.json?issuer=true',
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				hidden: true
			},
			{
				fieldLabel: 'Agency',
				xtype: 'combo',
				name: 'creditRating.ratingAgency.label',
				hiddenName: 'creditRating.ratingAgency.id',
				displayField: 'label',
				url: 'businessCompanyListFind.json?companyType=Credit Rating Agency',
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				allowBlank: false
			},
			{
				fieldLabel: 'Credit Rating',
				xtype: 'combo',
				name: 'creditRating.name',
				hiddenName: 'creditRating.id',
				url: 'complianceCreditRatingListFind.json',
				detailPageClass: 'Clifton.compliance.creditrating.CreditRatingWindow',
				requiredFields: ['creditRating.ratingAgency.label'],
				allowBlank: false,
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = {
						ratingAgencyId: queryEvent.combo.getParentForm().getForm().findField('creditRating.ratingAgency.id').value
					};
				}
			},
			{
				fieldLabel: 'Outlook',
				xtype: 'combo',
				name: 'outlook.name',
				hiddenName: 'outlook.id',
				url: 'complianceCreditRatingOutlookListFind.json',
				detailPageClass: 'Clifton.compliance.creditrating.CreditRatingOutlookWindow',
				requiredFields: ['creditRating.name'],
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = {
						ratingAgencyId: queryEvent.combo.getParentForm().getForm().findField('creditRating.ratingAgency.id').value
					};
				}
			},
			{
				fieldLabel: 'Start Date',
				name: 'startDate',
				xtype: 'datefield'
			},
			{
				fieldLabel: 'End Date',
				name: 'endDate',
				xtype: 'datefield'
			}
		],

		listeners: {
			afterrender: function() {
				const fieldEntity = this.getWindow().defaultData;
				if (fieldEntity) {
					this.applyFieldOverrides(fieldEntity);
				}
			},

			afterload: function(panel, closeOnSuccess) {
				// Skip UI modifications if window is about to be closed
				if (!closeOnSuccess) {
					this.applyFieldOverrides();
				}
			}
		},


		/**
		 * Gets the save URL for this form, depending on the object type which should be saved.
		 */
		getSaveURL: function() {
			const urlTypeComponent = !!TCG.getValue('type.securityRating', this.getForm().formValues) ? 'ForSecurity' : 'ForIssuer';
			return this.url.replace('.json', urlTypeComponent + 'Save.json');
		},


		/**
		 * Applies UI overrides to the dialog so that the proper fields are made visible.
		 *
		 * @param fieldEntity the field entity to use, if one does not exist within the form; otherwise, the form will
		 * be searched for a field entity
		 */
		applyFieldOverrides: function(fieldEntity) {
			const form = this.getForm();
			const isSecurityCreditRatingValue = !!TCG.getValue('type.securityRating', fieldEntity || form.formValues);
			const isIssuerCreditRatingValue = !isSecurityCreditRatingValue;

			applyFieldOverride('security.label', isSecurityCreditRatingValue);
			applyFieldOverride('issuerCompany.label', isIssuerCreditRatingValue);

			function applyFieldOverride(fieldName, isActive) {
				const field = form.findField(fieldName);
				field.setVisible(isActive);
				field.allowBlank = !isActive;
				const securityFieldElement = field.hiddenField || TCG.getValue('el.dom', field);
				if (securityFieldElement) {
					securityFieldElement.doNotSubmit = !isActive;
				}
				else {
					field.doNotSubmitValue = !isActive;
				}
			}
		}
	}]
});
