Clifton.compliance.creditrating.CreditRatingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Credit Rating',
	iconCls: 'thumb-down',

	items: [{
		xtype: 'formpanel',
		instructions: 'Different credit rating agencies use different rating schemes and also have separate sets of ratings for short-term and long-term assets.',
		url: 'complianceCreditRating.json',
		items: [
			{fieldLabel: 'Agency', name: 'ratingAgency.label', hiddenName: 'ratingAgency.id', xtype: 'combo', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Credit Rating Agency', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'Short-Term', name: 'shortTerm', xtype: 'checkbox', mutuallyExclusiveFields: ['equivalentShortTermRating.name']},
			{fieldLabel: 'Rating', name: 'name'},
			{
				fieldLabel: 'Equivalent Short Term Rating', name: 'equivalentShortTermRating.name', hiddenName: 'equivalentShortTermRating.id', xtype: 'combo', displayField: 'label', detailPageClass: 'Clifton.compliance.creditrating.CreditRatingWindow', mutuallyExclusiveFields: ['shortTerm'],
				url: 'complianceCreditRatingListFind.json',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {shortTerm: true, ratingAgencyId: TCG.getValue('ratingAgency.id', f.formValues)};
				}
			},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Weight', name: 'ratingWeight', xtype: 'spinnerfield'}
		]
	}]
});
