Clifton.compliance.creditrating.CreditRatingSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'complianceCreditRatingSetupWindow',
	title: 'Credit Ratings',
	iconCls: 'thumb-down',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Credit Rating Values',
				items: [{
					xtype: 'compliance-credit-rating-value-grid',
					importTableName: 'ComplianceCreditRatingValue',
					getImportComponentDefaultData: function() {
						return {existingBeans: 'UPDATE'};
					}
				}]
			},


			{
				title: 'Credit Ratings',
				items: [{
					name: 'complianceCreditRatingListFind',
					xtype: 'gridpanel',
					instructions: 'Different credit rating agencies use different rating schemes and also have separate sets of ratings for short-term and long-term assets.',
					wikiPage: 'IT/Compliance+-+Credit+Ratings',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Agency', width: 40, dataIndex: 'ratingAgency.name'},
						{header: 'Short-Term', width: 20, dataIndex: 'shortTerm', type: 'boolean'},
						{header: 'Rating', width: 20, dataIndex: 'name'},
						{header: 'Short-Term Rating', width: 25, dataIndex: 'equivalentShortTermRating.name'},
						{header: 'Rating Description', width: 100, dataIndex: 'description'},
						{header: 'Weight', width: 15, dataIndex: 'ratingWeight', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.creditrating.CreditRatingWindow'
					}
				}]
			},


			{
				title: 'Credit Rating Outlooks',
				items: [{
					name: 'complianceCreditRatingOutlookListFind',
					xtype: 'gridpanel',
					instructions: 'Credit rating outlook represents a future outlook from a specific credit rating Agency.',
					wikiPage: 'IT/Compliance+-+Credit+Ratings',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Agency', width: 100, dataIndex: 'ratingAgency.name'},
						{header: 'Outlook', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{header: 'Weight', width: 30, dataIndex: 'outlookWeight', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.creditrating.CreditRatingOutlookWindow'
					}
				}]
			},


			{
				title: 'Credit Rating Types',
				items: [{
					name: 'complianceCreditRatingTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all Credit Rating Types available in the system.  The type defines whether the credit rating applies to the Security or to the Issuer.',
					wikiPage: 'IT/Compliance+-+Credit+Ratings',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{header: 'Security Rating', width: 20, dataIndex: 'securityRating', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.creditrating.CreditRatingTypeWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
