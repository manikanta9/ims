Clifton.compliance.exchangelimit.ExchangeLimitSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'limitSetupWindow',
	title: 'Exchange Position Limits',
	iconCls: 'warning-red',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Exchange Limits',
				items: [{
					name: 'compliancePositionExchangeLimitListFind',
					xtype: 'gridpanel',
					instructions: 'The following are the Exchange Position limits currently defined in the system that are active for the specified date. Exchange Position Limits must be unique per type, instrument, accountability, and date range.',
					wikiPage: 'IT/Compliance+-+Position+Limits',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Primary Exchange', width: 150, dataIndex: 'instrument.exchange.name', filter: {type: 'combo', searchFieldName: 'instrumentExchangeId', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'Investment Instrument', width: 150, dataIndex: 'instrument.labelShort', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},

						{header: 'Big Instrument', width: 100, hidden: true, dataIndex: 'instrument.bigInstrument.labelShort', filter: {type: 'combo', searchFieldName: 'bigInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{
							header: 'Limit Type', width: 80, dataIndex: 'limitTypeLabel', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', searchFieldName: 'limitType',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes
								})
							},
							renderer: function(v, metaData, r) {
								const desc = r.data.description;
								if (TCG.isNotBlank(desc)) {
									const qtip = desc;
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(qtip);
								}
								return v;
							}
						},
						{header: 'Description', width: 80, hidden: true, dataIndex: 'description', filter: false, sortable: false},
						{header: 'Spot Month Calculator', width: 100, hidden: true, dataIndex: 'instrument.spotMonthCalculatorBean.name', filter: {type: 'combo', searchFieldName: 'spotMonthCalculatorBeanId', url: 'systemBeanListFind.json?groupName=Investment Security Spot Month Calculator'}},
						{header: 'Accountability', width: 65, dataIndex: 'accountabilityLimit', type: 'boolean'},
						{header: 'Start Date', width: 65, dataIndex: 'startDate'},
						{header: 'End Date', width: 65, dataIndex: 'endDate'},
						{header: 'Active', width: 65, dataIndex: 'active', type: 'boolean', filter: false, sortable: false, hidden: true},
						{header: 'Exchange Limit', width: 75, dataIndex: 'exchangeLimit', type: 'int', useNull: true},
						{header: 'Our Limit', width: 75, dataIndex: 'ourLimit', type: 'int', useNull: true},
						{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'}

					],
					editor: {
						detailPageClass: 'Clifton.compliance.exchangelimit.ExchangeLimitWindow'
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'activeOnDate');

						if (firstLoad) {
							// default to today
							if (TCG.isBlank(bd.getValue())) {
								bd.setValue((new Date()).format('m/d/Y'));
							}
						}

						if (TCG.isNotBlank(bd.getValue())) {
							const dateValue = (bd.getValue()).format('m/d/Y');
							return {activeOnDate: dateValue};
						}
					}
				}]
			},


			{

				title: 'Positions vs. Limits',
				items: [{
					name: 'compliancePositionExchangeLimitPositionListFind',
					xtype: 'gridpanel',
					instructions: 'The following lists active limits on the selected date and current positions held for those instruments against those limits.',
					wikiPage: 'IT/Compliance+-+Position+Limits',
					getTopToolbarFilters: function(toolbar) {
						return [
							{boxLabel: 'Exclude Pending Trades &nbsp;', name: 'excludePendingTrades', xtype: 'toolbar-checkbox'}, '-',
							{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
						];
					},
					timeout: 3000,
					columns: [
						{header: 'LimitID', hidden: true, dataIndex: 'limit.id'},
						{header: 'Investment Hierarchy', width: 150, hidden: true, dataIndex: 'limit.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Primary Exchange', width: 150, dataIndex: 'limit.instrument.exchange.name', filter: {type: 'combo', searchFieldName: 'instrumentExchangeId', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'Investment Instrument', width: 150, dataIndex: 'limit.instrument.labelShort', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},

						{header: 'Big Instrument', width: 100, hidden: true, dataIndex: 'limit.instrument.bigInstrument.labelShort', filter: {type: 'combo', searchFieldName: 'bigInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},

						{
							header: 'Limit Type', width: 75, dataIndex: 'limit.limitTypeLabel', filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', searchFieldName: 'limitType',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes
								})
							}
						},
						{header: 'Accountability', width: 70, dataIndex: 'limit.accountabilityLimit', type: 'boolean', filter: {searchFieldName: 'accountabilityLimit'}},
						{header: 'Description', width: 70, dataIndex: 'positionLimitLabel', filter: false},

						{header: 'Start Date', width: 65, dataIndex: 'limit.startDate', filter: {searchFieldName: 'startDate'}},
						{header: 'End Date', width: 65, dataIndex: 'limit.endDate', filter: {searchFieldName: 'endDate'}},


						{header: 'Exchange Limit', width: 70, dataIndex: 'limit.exchangeLimit', hidden: true, type: 'int', useNull: true, filter: {searchFieldName: 'exchangeLimit'}},
						{header: 'Clifton Limit', width: 70, dataIndex: 'limit.ourLimit', hidden: true, type: 'int', useNull: true, filter: {searchFieldName: 'ourLimit'}},
						{
							header: 'Limit', width: 70, dataIndex: 'limit.limit', type: 'int', useNull: true, filter: {searchFieldName: 'coalesceOurExchangeLimit'},
							renderer: function(v, metaData, r) {
								const exc = r.data.exchangeLimit;
								const our = r.data.ourLimit;
								if (exc !== our) {
									let qtip = '<table><tr><td>Exchange Limit:</td><td align="right">' + (!Ext.isNumber(exc) ? 'N/A' : Ext.util.Format.number(exc, '0,000')) + '</td><tr><td>Clifton Limit:</td><td align="right">' + (!Ext.isNumber(our) ? 'N/A' : Ext.util.Format.number(our, '0,000')) + '</td></tr>';
									qtip += '</table>';
									metaData.css = 'amountAdjusted';
									metaData.attr = 'qtip=\'' + qtip + '\'';
								}
								return Ext.util.Format.number(v, '0,000');
							}
						},

						{header: 'Long Instrument Count', width: 70, dataIndex: 'longInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Instrument Count', width: 70, dataIndex: 'shortInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Instrument Count', width: 70, dataIndex: 'totalInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Long Pending Instrument Count', width: 70, dataIndex: 'longPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Pending Instrument Count', width: 70, dataIndex: 'shortPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Pending Instrument Count', width: 70, dataIndex: 'totalPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Long Big Instrument Count', width: 70, dataIndex: 'longBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Big Instrument Count', width: 70, dataIndex: 'shortBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Big Instrument Count', width: 70, dataIndex: 'totalBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Long Pending Big Instrument Count', width: 70, dataIndex: 'longPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Pending Big Instrument Count', width: 70, dataIndex: 'shortPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Pending Big Instrument Count', width: 70, dataIndex: 'totalPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Big Factor', width: 60, dataIndex: 'bigFactor', hidden: true, type: 'currency', useNull: true, numberFormat: '0,000.00', filter: false},

						{
							header: 'Total Positions', width: 80, dataIndex: 'totalCount', type: 'int', useNull: true, filter: false,
							renderer: function(v, metaData, r) {
								Clifton.compliance.exchangelimit.renderInstrumentPositionCountTooltip(r, metaData);
								return Ext.util.Format.number(v, '0,000');
							}
						},

						{
							header: '% Of Limit', width: 70, dataIndex: 'percentOfLimit', type: 'currency', defaultSortColumn: true, defaultSortDirection: 'DESC',
							renderer: function(value, metaData, r) {
								if (TCG.isNotNull(value) && value !== 0) {
									if (value > 100) {
										metaData.css = 'ruleViolationBig';
										metaData.attr = 'qtip="Over the limit."';
									}
									else if (value > 80) {
										metaData.css = 'ruleViolation';
										metaData.attr = 'qtip="Over 80% of defined limit."';
									}
								}
								else {
									return '';
								}
								return TCG.numberFormat(value, '0,000.00 %');
							}
						}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.compliance.exchangelimit.ExchangeLimitWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.limit.id;
						}

					},
					getLoadParams: function(firstLoad) {
						let dateValue;
						const t = this.getTopToolbar();
						const activeOnDate = TCG.getChildByName(t, 'activeOnDate');

						// do not allow to clear - if blank - always use today
						if (firstLoad) {
							const dd = this.getWindow().defaultData;
							if (dd && dd.activeOnDate) { // activeOnDate can be defaulted by the caller of window open
								activeOnDate.setValue(dd.activeOnDate.format('m/d/Y'));
							}
						}
						if (TCG.isBlank(activeOnDate.getValue())) {
							dateValue = (new Date()).format('m/d/Y');
							activeOnDate.setValue(dateValue);
						}
						else {
							dateValue = (activeOnDate.getValue()).format('m/d/Y');
						}
						return {
							excludePendingTrades: TCG.getChildByName(t, 'excludePendingTrades').getValue(),
							activeOnDate: dateValue
						};
					}
				}]
			}
			]
	}]
});
