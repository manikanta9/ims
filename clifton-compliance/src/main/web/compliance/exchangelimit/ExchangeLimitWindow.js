Clifton.compliance.exchangelimit.ExchangeLimitWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Exchange Limit',
	iconCls: 'warning-red',
	height: 600,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Exchange Limit',
				items: [{
					xtype: 'formpanel',
					labelWidth: 140,
					instructions: 'An exchange limit defines the maximum allowed positions we can hold for a specified instrument. The contracts that are held are compared to limits based on types. Accountability limits are not hard limits that prohibit trading, but warnings to contact the exchange for further instruction. Limits are unique per type, instrument, accountability, and per date range.',
					url: 'compliancePositionExchangeLimit.json',
					items: [
						{fieldLabel: 'Primary Exchange', name: 'instrument.exchange.name', hiddenName: 'instrument.exchange.id', submitValue: false, xtype: 'combo', url: 'investmentExchangeListFind.json?compositeExchange=false', detailPageClass: 'Clifton.investment.setup.ExchangeWindow'},
						{
							fieldLabel: 'Investment Instrument', name: 'instrument.labelShort', hiddenName: 'instrument.id', displayField: 'labelShort', xtype: 'combo', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
							requiredFields: ['instrument.exchange.id'],
							disableAddNewItem: true,
							requestedProps: 'spotMonthCalculatorBean.name',
							listeners: {
								beforequery: function(queryEvent) {
									const exchangeId = queryEvent.combo.getParentForm().getForm().findField('instrument.exchange.id').getValue();
									if (TCG.isNotBlank(exchangeId)) {
										queryEvent.combo.store.setBaseParam('exchangeId', exchangeId);
									}
								},
								// reset Spot Month Calculator
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									const spotMonthCalculatorField = fp.getForm().findField('instrument.spotMonthCalculatorBean.name');
									spotMonthCalculatorField.setValue(TCG.getValue('spotMonthCalculatorBean.name', record.json));
								}
							}
						},
						{fieldLabel: 'Spot Month Calculator', name: 'instrument.spotMonthCalculatorBean.name', xtype: 'displayfield', submitValue: false},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Limit Type', name: 'limitTypeLabel', hiddenName: 'limitType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes
							}),
							listeners: {
								// reset spot month scale down days field
								select: function(combo, record, index) {
									combo.ownerCt.resetLimitTypeFields(record.json[1]);
								}
							}
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{name: 'spotMonthInstructions', submitValue: false, xtype: 'displayfield', value: 'Enter the number of business days before the end of the spot month to start including the scale down limit.  i.e. Last 10 business days of the spot month, where the spot month end date is generally the last delivery date.'},
						{fieldLabel: 'Business Days Back', name: 'spotMonthStartBusinessDaysBack', xtype: 'integerfield'},
						{boxLabel: 'Accountability Limit (Soft Warning)', xtype: 'checkbox', name: 'accountabilityLimit'},
						{boxLabel: 'Apply Limit to longs and shorts separately (if unchecked compares limit once against net positions)', name: 'applyToLongsAndShortsSeparately', xtype: 'checkbox'},
						{xtype: 'label', html: '<hr/>'},

						{
							fieldLabel: 'Start Date', xtype: 'compositefield',
							items: [
								{name: 'startDate', xtype: 'datefield', flex: 1},
								{value: 'Exchange Limit:', xtype: 'displayfield', width: 120},
								{name: 'exchangeLimit', xtype: 'integerfield', flex: 1}
							]
						},
						{
							fieldLabel: 'End Date', xtype: 'compositefield',
							items: [
								{name: 'endDate', xtype: 'datefield', flex: 1},
								{value: 'Our Limit:', xtype: 'displayfield', width: 120},
								{name: 'ourLimit', xtype: 'integerfield', flex: 1}
							]
						},
						{
							title: 'File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'ComplianceExchangeLimitAttachments'
						}
					],
					listeners: {
						afterload: function(form, isClosing) {
							this.resetLimitTypeFields();
						}
					},
					resetLimitTypeFields: function(record) {
						if (!record) {
							record = TCG.getValue('limitType', this.getForm().formValues);
						}
						if (record) {
							const f = this.getForm();
							const spotMonthInstructionsField = f.findField('spotMonthInstructions');
							const spotMonthDaysBackField = f.findField('spotMonthStartBusinessDaysBack');

							if (record === 'SCALE_DOWN_SPOT_MONTH') {
								spotMonthInstructionsField.setVisible(true);
								spotMonthDaysBackField.setVisible(true);
								spotMonthDaysBackField.allowBlank = false;

							}
							else {
								spotMonthInstructionsField.setVisible(false);
								spotMonthDaysBackField.allowBlank = true;
								spotMonthDaysBackField.setValue('');
								spotMonthDaysBackField.setVisible(false);
							}
						}
					}
				}]
			},

			{
				title: 'Instrument Limits',
				items: [{
					xtype: 'compliance-exchangelimit-instrument-grid'
				}]
			},

			{
				title: 'Spot Months',
				name: 'investmentSecuritySpotMonthListFind.json',
				xtype: 'gridpanel',
				instructions: 'Preview Spot Month Calculations for each security.',
				columns: [
					{header: 'SecurityID', width: 20, dataIndex: 'investmentSecurity.id', hidden: true},
					{header: 'Symbol', width: 50, dataIndex: 'investmentSecurity.symbol', filter: {searchFieldName: 'symbol'}},
					{header: 'Name', width: 100, dataIndex: 'investmentSecurity.name', filter: {searchFieldName: 'name'}},
					{header: 'Description', width: 200, dataIndex: 'investmentSecurity.description', hidden: true, filter: {searchFieldName: 'description'}},
					{header: 'Spot Month Start Date', width: 50, dataIndex: 'startDate', filter: false, sortable: false},
					{header: 'Spot Month End Date', width: 50, dataIndex: 'endDate', filter: false, sortable: false},

					{header: 'First Trade', width: 50, dataIndex: 'investmentSecurity.startDate', filter: {searchFieldName: 'startDate'}},
					{header: 'Last Trade', width: 50, dataIndex: 'investmentSecurity.endDate', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {searchFieldName: 'endDate'}},
					{header: 'First Notice Date', width: 50, dataIndex: 'investmentSecurity.firstNoticeDate', hidden: true, filter: {searchFieldName: 'firstNoticeDate'}},
					{header: 'First Delivery Date', width: 50, dataIndex: 'investmentSecurity.firstDeliveryDate', hidden: true, filter: {searchFieldName: 'firstDeliveryDate'}},
					{header: 'Last Delivery Date', width: 50, dataIndex: 'investmentSecurity.lastDeliveryDate', hidden: true, filter: {searchFieldName: 'lastDeliveryDate'}},
					{header: 'Active', width: 30, dataIndex: 'investmentSecurity.active', type: 'boolean', filter: {searchFieldName: 'active'}}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
					drillDownOnly: true,
					getDetailPageId: function(gridPanel, row) {
						return row.json.investmentSecurity.id;
					}
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						this.setFilterValue('investmentSecurity.active', true);
					}
					return {
						instrumentId: TCG.getValue('instrument.id', this.getWindow().getMainForm().formValues)
					};
				}
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'CompliancePositionExchangeLimit',
					showGlobalNoteMenu: false,
					showAttachmentInfo: true,
					defaultActiveFilter: false,
					showFilterActiveOnDate: true,
					// Allow the filtering, but don't default it to anything
					getDefaultActiveOnDate: function() {
						return '';
					}
				}]
			},

			{
				title: 'Instrument Positions vs. Limits',
				items: [{
					name: 'compliancePositionExchangeLimitPositionListFind',
					xtype: 'gridpanel',
					instructions: 'The following lists active limits on the selected date for the limit\'s instrument and current positions held for that instrument against those limits.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{boxLabel: 'Exclude Pending Trades', name: 'excludePendingTrades', xtype: 'checkbox'},
							{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
						];
					},
					columns: [
						{header: 'Hierarchy', hidden: true, width: 150, dataIndex: 'limit.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Primary Exchange', hidden: true, width: 150, dataIndex: 'limit.instrument.exchange.name', filter: {type: 'combo', searchFieldName: 'instrumentExchangeId', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'Instrument', hidden: true, width: 150, dataIndex: 'limit.instrument.labelShort', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Big Instrument', hidden: true, width: 100, dataIndex: 'limit.instrument.bigInstrument.labelShort', filter: {type: 'combo', searchFieldName: 'bigInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{
							header: 'Limit Type', width: 80, dataIndex: 'limit.limitTypeLabel',
							filter: {
								type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', searchFieldName: 'limitType',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes
								})
							}
						},
						{header: 'Accountability', width: 70, dataIndex: 'limit.accountabilityLimit', type: 'boolean', filter: {searchFieldName: 'accountabilityLimit'}},
						{header: 'Description', width: 70, dataIndex: 'positionLimitLabel', filter: false},
						{header: 'Start Date', width: 70, dataIndex: 'limit.startDate', filter: {searchFieldName: 'startDate'}},
						{header: 'End Date', width: 70, dataIndex: 'limit.endDate', filter: {searchFieldName: 'endDate'}},
						{header: 'Exchange Limit', width: 70, dataIndex: 'limit.exchangeLimit', hidden: true, type: 'int', useNull: true, filter: {searchFieldName: 'exchangeLimit'}},
						{header: 'Clifton Limit', width: 70, dataIndex: 'limit.ourLimit', hidden: true, type: 'int', useNull: true, filter: {searchFieldName: 'ourLimit'}},
						{
							header: 'Limit', width: 70, dataIndex: 'limit.limit', type: 'int', useNull: true, filter: {searchFieldName: 'coalesceOurExchangeLimit'},
							renderer: function(v, metaData, r) {
								const exc = r.data.exchangeLimit;
								const our = r.data.ourLimit;
								if (exc !== our) {
									let qtip = '<table><tr><td>Exchange Limit:</td><td align="right">' + (!Ext.isNumber(exc) ? 'N/A' : Ext.util.Format.number(exc, '0,000')) + '</td><tr><td>Clifton Limit:</td><td align="right">' + (!Ext.isNumber(our) ? 'N/A' : Ext.util.Format.number(our, '0,000')) + '</td></tr>';
									qtip += '</table>';
									metaData.css = 'amountAdjusted';
									metaData.attr = 'qtip=\'' + qtip + '\'';
								}
								return Ext.util.Format.number(v, '0,000');
							}
						},
						{header: 'Long Instrument Count', width: 70, dataIndex: 'longInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Instrument Count', width: 70, dataIndex: 'shortInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Instrument Count', width: 70, dataIndex: 'totalInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Long Pending Instrument Count', width: 70, dataIndex: 'longPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Pending Instrument Count', width: 70, dataIndex: 'shortPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false}, {header: 'Total Pending Instrument Count', width: 70, dataIndex: 'totalPendingInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Long Big Instrument Count', width: 70, dataIndex: 'longBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Big Instrument Count', width: 70, dataIndex: 'shortBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Big Instrument Count', width: 70, dataIndex: 'totalBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Long Pending Big Instrument Count', width: 70, dataIndex: 'longPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Short Pending Big Instrument Count', width: 70, dataIndex: 'shortPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},
						{header: 'Total Pending Big Instrument Count', width: 70, dataIndex: 'totalPendingBigInstrumentCount', hidden: true, type: 'int', useNull: true, filter: false},

						{header: 'Big Factor', width: 60, dataIndex: 'bigFactor', hidden: true, type: 'currency', useNull: true, numberFormat: '0,000.00', filter: false},
						{
							header: 'Total Positions', width: 70, dataIndex: 'totalCount', type: 'int', useNull: true, filter: false,
							renderer: function(v, metaData, r) {
								Clifton.compliance.exchangelimit.renderInstrumentPositionCountTooltip(r, metaData);
								return Ext.util.Format.number(v, '0,000');
							}
						},
						{
							header: '% Of Limit', width: 70, dataIndex: 'percentOfLimit', filter: false, sortable: false, type: 'currency', summaryType: 'percent', denominatorValueField: 'limit', numeratorValueField: 'totalPositionCount',
							renderer: function(value, metaData, r) {
								if (TCG.isNotNull(value) && value !== 0) {
									if (value > 100) {
										metaData.css = 'ruleViolationBig';
										metaData.attr = 'qtip="Over the limit."';
									}
									else if (value > 80) {
										metaData.css = 'ruleViolation';
										metaData.attr = 'qtip="Over 80% of defined limit."';
									}
								}
								else {
									return '';
								}
								return TCG.numberFormat(value, '0,000.00 %');
							}
						}
					],
					getLoadParams: function(firstLoad) {
						let dateValue;
						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'activeOnDate');

						// do not allow to clear - if blank - always use today
						if (TCG.isBlank(bd.getValue())) {
							dateValue = (new Date()).format('m/d/Y');
							bd.setValue(dateValue);
						}
						else {
							dateValue = (bd.getValue()).format('m/d/Y');
						}
						return {
							instrumentId: TCG.getValue('instrument.id', this.getWindow().getMainForm().formValues),
							excludePendingTrades: TCG.getChildByName(t, 'excludePendingTrades').getValue(),
							activeOnDate: dateValue
						};
					}
				}]
			},

			{
				title: 'Open Positions',
				layout: 'border',
				items: [
					{
						xtype: 'gridpanel',
						region: 'north',
						name: 'compliancePositionExchangeLimitAccountingPositionList',
						instructions: 'The following positions are currently open in the system that apply to the instrument or big instrument tied to this limit.',
						height: 250,
						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
							];
						},
						groupField: 'holdingInvestmentAccount.issuingCompany.name',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Lots" : "Lot"]})',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Holding Company', width: 125, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', hidden: true},
							{header: 'Client Account', width: 170, dataIndex: 'clientInvestmentAccount.label'},
							{header: 'Holding Account', width: 70, dataIndex: 'holdingInvestmentAccount.number'},
							{header: 'Security', width: 70, dataIndex: 'investmentSecurity.symbol'},
							{header: 'Open Date', width: 50, dataIndex: 'originalTransactionDate'},
							{header: 'Open Price', width: 70, dataIndex: 'price', type: 'float', negativeInRed: true, hidden: true},
							{header: 'FX Rate', width: 50, dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true, hidden: true},
							{header: 'Remaining Qty', width: 60, dataIndex: 'remainingQuantity', type: 'float', negativeInRed: true, summaryType: 'sum'},
							{header: 'Remaining Cost Basis', width: 80, dataIndex: 'remainingCostBasis', type: 'currency', negativeInRed: true, summaryType: 'sum', hidden: true}
						],
						plugins: {ptype: 'groupsummary'},
						editor: {
							detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
							drillDownOnly: true
						},
						getLoadParams: function() {
							let dateValue;
							const t = this.getTopToolbar();
							const bd = TCG.getChildByName(t, 'activeOnDate');
							// do not allow to clear - if blank - always use today
							if (TCG.isBlank(bd.getValue())) {
								dateValue = (new Date()).format('m/d/Y');
								bd.setValue(dateValue);
							}
							else {
								dateValue = (bd.getValue()).format('m/d/Y');
							}
							const f = this.getWindow().getMainForm();
							return {
								limitId: f.formValues.id,
								date: dateValue
							};
						}
					},

					{
						region: 'center',
						title: 'Pending Trades',
						xtype: 'gridpanel',
						name: 'compliancePositionExchangeLimitPendingTradeList',
						instructions: 'The following trades are currently pending in the system that apply to the instrument or big instrument tied to this limit.',
						getLoadParams: function() {
							const f = this.getWindow().getMainForm();
							return {
								limitId: f.formValues.id
							};
						},
						remoteSort: true,
						groupField: 'holdingInvestmentAccount.issuingCompany.name',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Trades" : "Trade"]})',
						columns: [
							{header: 'Holding Company', width: 125, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', hidden: true},
							{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
							{header: 'Workflow Status', width: 25, dataIndex: 'workflowStatus.name'},
							{header: 'Workflow State', width: 25, dataIndex: 'workflowState.name'},
							{header: 'Trade Type', width: 11, dataIndex: 'tradeType.name', hidden: true},
							{header: 'Client Account', width: 65, dataIndex: 'clientInvestmentAccount.label'},
							{header: 'Holding Account', width: 40, dataIndex: 'holdingInvestmentAccount.number'},
							{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
							{
								header: 'Buy/Sell', width: 25, dataIndex: 'buy', type: 'boolean',
								renderer: function(v, metaData) {
									metaData.css = v ? 'buy-light' : 'sell-light';
									return v ? 'BUY' : 'SELL';
								}
							},
							{
								header: 'Quantity', width: 25, dataIndex: 'quantityIntended', type: 'float', useNull: true, summaryType: 'sum',
								renderer: function(v, metaData, r) {
									if (!TCG.isTrue(r.data.buy)) {
										v = -v;
									}
									return TCG.renderAmount(v, false, '0,000.00', true);
								},
								summaryCalculation: function(v, r, field, data, col) {
									if (r && r.json && r.json.quantityIntended) {
										return v + (!TCG.isTrue(r.json.buy) ? -(r.json.quantityIntended) : r.json.quantityIntended);
									}
									return v;
								},
								summaryRenderer: function(v, args, r) {
									if (TCG.isNotBlank(v)) {
										return TCG.renderAmount(v, false, '0,000.00', true);
									}
								}
							},
							{header: 'Security', width: 25, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
							{header: 'Exchange Rate', width: 25, dataIndex: 'exchangeRate', type: 'float', useNull: true, hidden: true},
							{header: 'Average Price', width: 25, dataIndex: 'averageUnitPrice', type: 'float', useNull: true, hidden: true},
							{header: 'Commission Per Unit', width: 25, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
							{header: 'Fee', width: 25, dataIndex: 'feeAmount', type: 'float', hidden: true},
							{header: 'Accounting Notional', width: 25, dataIndex: 'accountingNotional', type: 'currency', useNull: true, hidden: true},
							{header: 'Trader', width: 25, dataIndex: 'traderUser.label'},
							{header: 'Team', width: 25, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
							{header: 'Traded On', width: 25, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'}
						],
						plugins: {ptype: 'groupsummary'},
						editor: {
							detailPageClass: 'Clifton.trade.TradeWindow',
							drillDownOnly: true
						}
					}
				]
			}
		]
	}
	]
})
;
