Clifton.compliance.ComplianceRuleTypeOldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Type',
	iconCls: 'verify',

	items: [{
		xtype: 'formpanel',
		url: 'complianceRuleTypeOld.json',
		readOnly: true,
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Is Investment Account Allowed?', name: 'investmentAccountAllowed', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Investment Account Required?', name: 'investmentAccountRequired', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Investment Specific Rule?', name: 'investmentSpecific', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Trader Specific Rule?', name: 'traderSpecific', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Limit Rule?', name: 'limitRule', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Short/Long Rule?', name: 'shortLongRule', xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Is Ignorable Allowed?', name: 'ignorableAllowed', xtype: 'checkbox', labelSeparator: ''}
		]
	}]
});
