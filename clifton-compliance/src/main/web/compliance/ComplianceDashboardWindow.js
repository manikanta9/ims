Clifton.compliance.ComplianceDashboardWindow = Ext.extend(TCG.app.Window, {
	id: 'complianceDashboardWindow',
	title: 'Compliance Dashboard',
	iconCls: 'chart-bar',
	enableRefreshWindow: true,
	width: 1500,
	height: 810,

	items: [{
		xtype: 'panel',
		layout: 'table',
		layoutConfig: {
			columns: 4,
			tableAttrs: {
				style: {'table-layout': 'fixed', width: '100%'},
				cellspacing: 3
			}
		},
		defaults: {height: 255},
		items: [
			{
				xtype: 'system-query-doughnut-chart',
				title: 'Outstanding Compliance Workflow Tasks by User',
				queryName: 'Workflow Tasks - Outstanding Counts by User',
				queryParams: () => TCG.data.getDataPromiseUsingCaching('workflowTaskCategoryByName.json?name=Compliance Tasks', this, 'workflow.task.category.Compliance Tasks')
					.then(function(category) {
						return {WorkflowTaskCategoryID: category.id};
					}),
				onClickEvent: function(data, label, datasetLabel, fullData) {
					// fullData: optional SecurityUserID is 2nd field and optional SecurityGroupID is 3rd field (can be either assignee or approver user or group)
					TCG.createComponent('Clifton.compliance.ComplianceSetupWindow', {defaultActiveTabName: 'Compliance Tasks', defaultData: {forceReload: true, displayFilter: 'ALL_OPEN'}});
				}
			},
			{
				xtype: 'system-query-doughnut-chart',
				title: 'Aging Report for Compliance Workflow Tasks',
				queryName: 'Workflow Tasks - Overdue Aging Counts',
				queryParams: () => TCG.data.getDataPromiseUsingCaching('workflowTaskCategoryByName.json?name=Compliance Tasks', this, 'workflow.task.category.Compliance Tasks')
					.then(function(category) {
						return {WorkflowTaskCategoryID: category.id};
					}),
				onClickEvent: function(data, label, datasetLabel) {
					TCG.createComponent('Clifton.compliance.ComplianceSetupWindow', {defaultActiveTabName: 'Compliance Tasks', defaultData: {forceReload: true, displayFilter: 'ALL_OPEN'}});
				}
			},
			{
				xtype: 'system-query-bar-chart',
				title: 'Completed Compliance Workflow Tasks',
				queryName: 'Workflow Tasks - Daily Completed Counts',
				queryParams: () => TCG.data.getDataPromiseUsingCaching('workflowTaskCategoryByName.json?name=Compliance Tasks', this, 'workflow.task.category.Compliance Tasks')
					.then(function(category) {
						return {
							WorkflowTaskCategoryID: category.id,
							DaysBack: 13
						};
					}),
				colspan: 2,
				onClickEvent: function(data, label, datasetLabel) {
					TCG.createComponent('Clifton.compliance.ComplianceSetupWindow', {defaultActiveTabName: 'Compliance Tasks', defaultData: {forceReload: true, displayFilter: 'ALL_COMPLETED', updateDate: TCG.parseDate(label, 'm/d/Y')}});
				}
			},


			{
				xtype: 'system-query-stackedbar-chart',
				title: 'Compliance Real Time Rule Violations History (excludes Cancelled Trades)',
				queryName: 'Rule Engine - Violations History',
				queryParams: () => TCG.data.getDataPromiseUsingCaching('ruleDefinitionByName.json?definitionName=Compliance Real Time Rules', this, 'rule.definition.Compliance Real Time Rules')
					.then(function(d) {
						return {
							RuleDefinitionID: d.id,
							DaysBack: 25,
							ExcludeCancelledTrades: true
						};
					}),
				colorScheme: ['#b8c228', '#db5d39'],
				colspan: 4,
				onClickEvent: function(data, label, datasetLabel) {
					TCG.createComponent('Clifton.compliance.ComplianceSetupWindow', {defaultActiveTabName: 'Rule Violations', defaultData: {forceReload: true, displayFilter: 'NOT_CANCELLED', createDate: TCG.parseDate(label, 'm/d/Y')}});
				}
			},


			{
				xtype: 'system-query-stackedbar-chart',
				title: 'Compliance Batch Rule Run Violations History',
				queryName: 'Compliance Rule Run - Violations History',
				queryParams: {DaysBack: 25},
				colspan: 4,
				onClickEvent: function(data, label, datasetLabel) {
					TCG.createComponent('Clifton.compliance.ComplianceSetupWindow', {defaultActiveTabName: 'Rule Runs', defaultData: {forceReload: true, runDate: TCG.parseDate(label, 'm/d/Y')}});
				}
			}
		]
	}]
});
