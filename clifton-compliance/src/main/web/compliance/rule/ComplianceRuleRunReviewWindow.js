Clifton.compliance.rule.ComplianceRuleRunReviewWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Failed Rule Run(s) Review',
	iconCls: 'checked',
	height: 250,
	width: 800,
	modal: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 130,
		loadValidation: false,
		items: [
			{name: 'ruleRunIds', xtype: 'hidden'},
			{fieldLabel: 'Review Note', name: 'reviewNote', xtype: 'textarea', allowBlank: false},
			{
				fieldLabel: 'Recent Review Notes', name: 'recentReviewNote', xtype: 'combo', url: 'complianceRuleRunReviewNotes.json', loadAll: true, allowBlank: true, forceSelection: false, doNotAddContextMenu: true,
				qtip: 'Used to copy an existing Review Note for selected Client Account(s) and Rule(s) from last 7 calendar days. The note can be updated before saving.',
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.baseParams = {
							reviewedOnDateAfter: new Date().add(Date.DAY, -7).format('m/d/Y'),
							ruleRunIds: TCG.getValue('ruleRunIds', f.formValues)
						};
					},
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.setFormValue('reviewNote', record.json.name);
					}
				}
			}
		],
		isUndoReview: function() {
			return (this.getWindow().undoReviewWindow === true);
		},
		getInstructions: function() {
			return this.isUndoReview() ? 'Enter a note explaining why you are undoing this review. Clicking OK will update the status from "Fail (Reviewed)" to "Fail".' : 'Enter a note explaining this "Fail" and any actions that were taken because of it. Clicking OK will update the status from "Fail" to "Fail (Reviewed)".';
		},
		getSaveURL: function() {
			return this.isUndoReview() ? 'complianceRuleRunReviewUndo.json' : 'complianceRuleRunReview.json';
		},
		listeners: {
			afterload: function(panel) {
				panel.getWindow().openerCt.reload();
				panel.getWindow().close();
			}
		}
	}]
});
