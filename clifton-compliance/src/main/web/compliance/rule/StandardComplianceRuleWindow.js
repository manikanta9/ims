Clifton.compliance.rule.StandardComplianceRuleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule',
	iconCls: 'verify',
	width: 1000,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formwithdynamicfields',
					validateDisabled: false,
					url: 'complianceRule.json',
					labelWidth: 200,

					dynamicTriggerFieldName: 'ruleType.label',
					dynamicTriggerValueFieldName: 'ruleType.ruleEvaluatorType.id',
					dynamicFieldTypePropertyName: 'type',
					dynamicFieldListPropertyName: 'ruleEvaluatorBean.propertyList',
					dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
					dynamicFieldsUrlParameterName: 'typeId',

					listeners: {
						afterload: function(form, isClosing) {
							const isSecuritySpecific = TCG.getChildByName(form, 'ruleType.investmentSecuritySpecific').getValue();
							form.showOrHideSpecificity(isSecuritySpecific);

							const limitTypeField = TCG.getChildByName(form, 'complianceRuleLimitType.name');
							const useLimitFields = !!limitTypeField.getValue();
							form[useLimitFields ? 'enableField' : 'disableField']('complianceRuleLimitType.name', true);
							form[useLimitFields ? 'enableField' : 'disableField']('limitValue', true);

							const limitValueRangeField = TCG.getChildByName(form, 'limitValueRange');
							const useLimitValueRangeField = !!limitValueRangeField.getValue();
							form[useLimitValueRangeField ? 'enableField' : 'disableField']('limitValueRange', true);
						}
					},
					items: [
						{name: 'ruleType.ruleEvaluatorType.id', xtype: 'hidden', submitValue: false},
						{name: 'ruleEvaluatorBean.type.id', xtype: 'hidden'},
						{name: 'ruleType.investmentSecuritySpecific', xtype: 'checkbox', hidden: true, submitValue: false},
						{name: 'ruleEvaluatorBean.name', xtype: 'hidden'},
						{name: 'ruleEvaluatorBean.description', xtype: 'hidden'},

						{
							fieldLabel: 'Rule Type', name: 'ruleType.label', hiddenName: 'ruleType.id', displayField: 'label', xtype: 'combo', limitRequestedProperties: false,
							url: 'complianceRuleTypeListFind.json?rollupRule=false',
							detailPageClass: 'Clifton.compliance.rule.ComplianceRuleTypeWindow',
							disableAddNewItem: true,
							listeners: {
								// reset object bean property fields on changes to bean type (triggerField)
								select: function(combo, record, index) {
									const form = combo.getParentForm();
									form.setFormValue('ruleType.ruleEvaluatorType.id', record.json.ruleEvaluatorType.id);
									form.setFormValue('ruleEvaluatorBean.type.id', record.json.ruleEvaluatorType.id);
									form.setFormValue('ruleEvaluatorBean.name', record.json.name);
									form.setFormValue('ruleEvaluatorBean.description', record.json.description);

									form.showOrHideSpecificity(record.json.investmentSecuritySpecific);
									form.showOrHideLimitType(record.json.id);

									combo.ownerCt.resetDynamicFields(combo, false);
								}
							}
						},
						{
							fieldLabel: 'Limit Type',
							name: 'complianceRuleLimitType.name',
							hiddenName: 'complianceRuleLimitType.id',
							displayField: 'name',
							xtype: 'combo',
							url: 'complianceRuleLimitTypeListFind.json',
							loadAll: true,
							allowBlank: false,
							hidden: true
						},
						{fieldLabel: 'Limit Value', name: 'limitValue', xtype: 'floatfield', allowBlank: false, hidden: true},
						{fieldLabel: 'Limit Value Range', name: 'limitValueRange', xtype: 'floatfield', hidden: true},
						{fieldLabel: 'Rule Name', name: 'name', xtype: 'textfield'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'},

						{xtype: 'sectionheaderfield', header: 'Specificity Scope', name: 'specificityHeader', hidden: true, fieldLabel: ''},
						{
							fieldLabel: 'Investment Type',
							name: 'investmentType.name',
							hiddenName: 'investmentType.id',
							xtype: 'combo',
							url: 'investmentTypeList.json',
							loadAll: true,
							hidden: true,
							mutuallyExclusiveFields: ['investmentHierarchy.labelExpanded', 'investmentInstrument.label']
						},
						{
							fieldLabel: 'Investment Sub Type',
							name: 'investmentTypeSubType.name',
							hiddenName: 'investmentTypeSubType.id',
							xtype: 'combo',
							url: 'investmentTypeSubTypeListByType.json',
							loadAll: true,
							hidden: true,
							requiredFields: ['investmentType.name'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
								}
							}
						},
						{
							fieldLabel: 'Investment Sub Type 2',
							name: 'investmentTypeSubType2.name',
							hiddenName: 'investmentTypeSubType2.id',
							xtype: 'combo',
							url: 'investmentTypeSubType2ListByType.json',
							loadAll: true,
							hidden: true,
							requiredFields: ['investmentType.name'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
								}
							}
						},
						{
							fieldLabel: 'Investment Hierarchy',
							name: 'investmentHierarchy.labelExpanded',
							displayField: 'labelExpanded',
							hiddenName: 'investmentHierarchy.id',
							xtype: 'combo',
							queryParam: 'labelExpanded', listWidth: 600,
							detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
							url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true',
							assignmentAllowed: true,
							hidden: true,
							mutuallyExclusiveFields: ['investmentType.name', 'investmentInstrument.label'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									const form = fp.getForm();

									combo.store.baseParams = {
										investmentTypeId: form.findField('investmentType.id').getValue()
									};
								}
							}
						},
						{
							fieldLabel: 'Investment Instrument',
							name: 'investmentInstrument.label',
							displayField: 'label',
							hiddenName: 'investmentInstrument.id',
							xtype: 'combo',
							detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
							url: 'investmentInstrumentListFind.json',
							hidden: true,
							mutuallyExclusiveFields: ['investmentType.name', 'investmentHierarchy.labelExpanded', 'underlyingInstrument.label']
						},
						{
							fieldLabel: 'Underlying Instrument',
							name: 'underlyingInstrument.label',
							displayField: 'label',
							hiddenName: 'underlyingInstrument.id',
							xtype: 'combo',
							detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
							url: 'investmentInstrumentListFind.json',
							hidden: true,
							mutuallyExclusiveFields: ['investmentInstrument.label']
						},
						{
							fieldLabel: 'CCY Type', name: 'currencyType.label', hiddenName: 'currencyType', hidden: true, mode: 'local', xtype: 'combo',
							emptyText: '',
							forceSelection: true,
							store: {xtype: 'arraystore', data: Clifton.investment.instrument.CurrencyTypes}
						},
						{xtype: 'sectionheaderfield', header: 'Execution Time', fieldLabel: ''},
						{boxLabel: 'Execute this rule REAL-TIME (transaction point of entry)', name: 'realTime', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Execute this rule as a BATCH (nightly check)', name: 'batch', xtype: 'checkbox', labelSeparator: ''},
						{xtype: 'sectionheaderfield', header: 'Additional Configuration', fieldLabel: ''}
					],

					prepareDefaultData: function(dd) {
						this.setFormValue('ruleType.ruleEvaluatorType.id', dd.ruleType.ruleEvaluatorType.id);
						this.setFormValue('ruleEvaluatorBean.type.id', dd.ruleType.ruleEvaluatorType.id);
						this.setFormValue('ruleEvaluatorBean.name', dd.ruleType.name);
						this.setFormValue('ruleEvaluatorBean.description', dd.ruleType.description);
						this.showOrHideSpecificity(dd.ruleType.investmentSecuritySpecific);
						this.showOrHideLimitType(dd.ruleType.id);
						this.resetDynamicFields(TCG.getChildByName(this, 'ruleType.label'), false);
						return dd;
					},
					showOrHideSpecificity: function(isSecuritySpecific) {
						const specificityHeaderField = TCG.getChildByName(this, 'specificityHeader');
						specificityHeaderField.setVisible(isSecuritySpecific);
						const configureField = isSecuritySpecific
							? (...args) => this.enableField(...args)
							: (...args) => this.disableField(...args);
						configureField('investmentType.name', true);
						configureField('investmentTypeSubType.name', true);
						configureField('investmentTypeSubType2.name', true);
						configureField('investmentHierarchy.labelExpanded', true);
						configureField('investmentInstrument.label', true);
						configureField('underlyingInstrument.label', true);
						configureField('currencyType.label', true);
						const currencyType = TCG.getChildByName(this, 'currencyType.label');
						currencyType.allowBlank = !isSecuritySpecific;
						this.updateMutualExclusionFields();
					},

					showOrHideLimitType: async function(complianceRuleTypeId) {
						const limitTypeField = TCG.getChildByName(this, 'complianceRuleLimitType.name');
						limitTypeField.store.baseParams = {complianceRuleTypeId: complianceRuleTypeId};
						limitTypeField.store.load({
							callback: (records, options, success) => {
								if (!success) {
									return;
								}

								if (records.length > 0) {
									this.enableField('complianceRuleLimitType.name', true);
									this.enableField('limitValue', true);
									this[records.some(r => r.data.label === 'RANGE') ? 'enableField' : 'disableField']('limitValueRange', true);
									if (records.length === 1) {
										limitTypeField.setValue({value: records[0].get('id'), text: records[0].get('label')});
									}
								}
								else {
									this.disableField('complianceRuleLimitType.name', true);
									this.disableField('limitValue', true);
									this.disableField('limitValueRange', true);
								}
							}
						});
					},

					updateMutualExclusionFields: function() {
						// Refresh fields in order to re-apply mutual exclusion rules
						const mutexFields = [
							TCG.getChildByName(this, 'investmentType.name'),
							TCG.getChildByName(this, 'investmentTypeSubType.name'),
							TCG.getChildByName(this, 'investmentTypeSubType2.name'),
							TCG.getChildByName(this, 'investmentHierarchy.labelExpanded'),
							TCG.getChildByName(this, 'investmentInstrument.label'),
							TCG.getChildByName(this, 'underlyingInstrument.label'),
							TCG.getChildByName(this, 'currencyType.label')
						];
						mutexFields.forEach(field => field.setValue(field.getValue()));
					}
				}]
			},


			{
				title: 'Rule Assignments',
				items: [{
					xtype: 'compliance-assignment-grid',
					columnOverrides: [
						{dataIndex: 'businessServiceAssignment', hidden: true},
						{dataIndex: 'global', hidden: true}
					],
					getLoadParams: function() {
						return {
							ruleId: this.getWindow().getMainFormId(),
							...this.getTopToolbarFilterLoadParams()
						};
					}
				}]
			},


			{
				title: 'Run History',
				items: [{
					xtype: 'compliance-run-grid',
					includeClientAccountFilter: false,
					includeRunDateFilter: false,
					instructions: 'Results for batch compliance rules run for selected Rule on the specified date. Runs are executed only for Active client accounts and active rule assignments.',
					groupField: undefined,
					columnOverrides: [
						{dataIndex: 'complianceRun.runDate', hidden: false, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{width: 80, dataIndex: 'complianceRun.clientInvestmentAccount.label', hidden: false},
						{dataIndex: 'complianceRule.name', hidden: true, viewNames: undefined},
						{width: 150, dataIndex: 'description'}
					],
					getAdditionalLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('complianceRun.runDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {complianceRuleId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
