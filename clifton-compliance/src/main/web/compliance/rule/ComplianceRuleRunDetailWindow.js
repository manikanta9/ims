Clifton.compliance.rule.ComplianceRuleRunDetailWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Rule Run Detail',
	iconCls: 'verify',
	width: 1200,
	height: 600,
	closeWindowTrail: true,
	enableRefreshWindow: true,
	enableShowInfo: true,

	// methods used by system-note-grid
	getMainFormPanel: function() {
		return this.items.get(0).items.get(0).items.get(0);
	},
	getMainForm: function() {
		return this.getMainFormPanel().getForm();
	},
	getMainFormId: function() {
		return this.getMainForm().idFieldValue;
	},

	gridTabWithCount: 'Notes', // show notes count in  the tab

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Details',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						xtype: 'formpanel',
						url: 'complianceRuleRun.json',
						labelFieldName: 'complianceRule.name',
						loadValidation: false,
						height: 115,
						defaults: {
							anchor: '0'
						},

						listeners: {
							afterload: function(form, isClosing) {
								// Show more specific of either rule or rule type field
								const ruleField = TCG.getChildByName(form, 'complianceRule.name');
								this.showField(ruleField.getValue() ? 'complianceRule.name' : 'ruleType.name');
								this.hideFieldIfBlank('reviewedByUser.label');
								this.hideFieldIfBlank('reviewedOnDate');
								this.hideFieldIfBlank('reviewNote');
							}
						},

						items: [
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										columnWidth: .79,
										layout: 'form',
										items: [
											{fieldLabel: 'Rule Type', name: 'ruleType.name', xtype: 'linkfield', detailIdField: 'ruleType.id', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleTypeWindow', hidden: true},
											{fieldLabel: 'Compliance Rule', name: 'complianceRule.name', xtype: 'linkfield', detailIdField: 'complianceRule.id', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow', hidden: true},
											{fieldLabel: 'Client Account', name: 'complianceRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'complianceRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
											{fieldLabel: 'Run Details', name: 'description', xtype: 'textfield', anchor: '0', readOnly: true},
											{fieldLabel: 'Review Note', name: 'reviewNote', xtype: 'textfield', anchor: '0', readOnly: true}
										]
									},
									{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
									{
										columnWidth: .2,
										layout: 'form',
										labelWidth: 100,
										items: [
											{fieldLabel: 'Run Date', name: 'complianceRun.runDate', xtype: 'datefield', width: 100, readOnly: true},
											{
												fieldLabel: 'Rule Status', name: 'status', xtype: 'displayfield', width: 96,
												setRawValue: function(v) {
													if (v === 1 || v === 2) {
														this.el.addClass('success');
														TCG.form.DisplayField.superclass.setRawValue.call(this, v === 1 ? 'PASS' : 'IGNORED');
													}
													else {
														this.el.addClass('fail');
														TCG.form.DisplayField.superclass.setRawValue.call(this, v === 3 ? 'REVIEWED' : 'FAIL');
													}
												}
											},
											{fieldLabel: 'Reviewed By', name: 'reviewedByUser.label', xtype: 'linkfield', detailIdField: 'reviewedByUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
											{fieldLabel: 'Review Date', name: 'reviewedOnDate', xtype: 'displayfield'},
										]
									}
								]
							}
						]
					},

					{
						xtype: 'compliance-rule-run-detail-grid',
						instructions: 'Details information for selected client account rule run.',
						flex: 1,

						addTopToolbarItems: function(toolbar) {
							// Add "Generate Details" button to simulate reprocessed results
							toolbar.add('->', {
								text: 'Generate Details',
								tooltip: 'Preview all details for selected rule run. Rule run is not saved.',
								iconCls: 'preview',
								handler: () => {
									const fp = this.getWindow().getMainFormPanel();
									const processDate = TCG.getChildByName(fp, 'complianceRun.runDate').getValue().format('m/d/Y');
									const clientAccountId = fp.getFormValue('complianceRun.clientInvestmentAccount.id');
									const complianceRuleId = fp.getFormValue('complianceRule.id');
									const complianceRuleTypeId = fp.getFormValue('ruleType.id') || null;
									const ruleName = fp.getFormValue('complianceRule.name');
									const clientInvestmentLabel = fp.getFormValue('complianceRun.clientInvestmentAccount.label');
									const defaultData = {
										complianceRuleId: complianceRuleId,
										complianceRuleTypeId: complianceRuleTypeId,
										clientAccountId: clientAccountId,
										processDate: processDate,
										ruleName: ruleName,
										clientInvestmentLabel: clientInvestmentLabel
									};
									TCG.createComponent('Clifton.compliance.rule.ComplianceRuleRunPreviewWindow', {
										defaultData: defaultData,
										openerCt: this
									});
								}
							});
						},

						getLoadParams: function(firstLoad) {
							const loadParams = TCG.callParent(this, 'getLoadParams', arguments);
							if (loadParams === false) {
								return false;
							}

							return {
								...loadParams,
								complianceRuleRunId: this.getWindow().params.id
							};
						}
					}
				]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'ComplianceRuleRun'
				}]
			},


			{
				title: 'Run History',
				items: [{
					xtype: 'compliance-run-grid',
					includeClientAccountFilter: false,
					includeRunDateFilter: false,
					defaultStatusFilterValue: 'ALL',
					instructions: 'Historical runs for selected client account and rule.',
					groupField: undefined,
					columnOverrides: [
						{dataIndex: 'complianceRun.runDate', hidden: false, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{dataIndex: 'complianceRule.name', hidden: true, viewNames: undefined}
					],
					getAdditionalLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('complianceRun.runDate', {'after': new Date().add(Date.DAY, -90)});
						}
						const f = this.getWindow().getMainForm().formValues;
						return {
							complianceRuleTypeId: (f.ruleType) ? f.ruleType.id : null,
							complianceRuleId: (f.complianceRule) ? f.complianceRule.id : null,
							clientInvestmentAccountId: f.complianceRun.clientInvestmentAccount.id
						};
					}
				}]
			}
		]
	}]
});
