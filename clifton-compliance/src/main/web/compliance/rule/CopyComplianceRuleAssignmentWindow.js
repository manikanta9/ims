Clifton.compliance.rule.CopyComplianceRuleAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Assignment Copy',
	iconCls: 'verify',
	width: 700,
	height: 300,
	items: [{
		xtype: 'formpanel',
		labelWidth: 130,
		url: 'complianceRuleAssignment.json',
		loadValidation: false,

		labelFieldName: 'id',
		instructions: 'Copy all assignments on the from account to the to accounts.  Start date will be set as specified along with an empty end date.  All other assignment properties will be copied. ',
		getSaveURL: function() {
			let url = this.url;
			if (url) {
				const i = url.indexOf('.');
				url = url.substring(0, i) + 'Copy' + url.substring(i);
			}
			return url;
		},
		items: [
			{fieldLabel: 'From Client Account', name: 'fromClientInvestmentAccount.label', hiddenName: 'fromClientAccountId', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStatusName=Closed', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
			{
				xtype: 'listfield',
				allowBlank: true,
				name: 'toClientAccountIds',
				fieldLabel: 'To Client Accounts',
				controlConfig: {fieldLabel: 'To Client Account', name: 'toClientInvestmentAccount.label', hiddenName: 'toClientInvestmentAccount.id', valueField: 'id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStatusName=Closed', detailIdField: 'clientInvestmentAccount.id'}
			},

			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', width: 125, value: Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y')}
		]
	}]
});
