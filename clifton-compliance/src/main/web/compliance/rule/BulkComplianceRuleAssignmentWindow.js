Clifton.compliance.rule.BulkComplianceRuleAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Assignment',
	iconCls: 'verify',
	width: 700,
	height: 600,
	hideApplyButton: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 130,
		url: 'complianceRuleAssignmentBulk.json',
		loadValidation: false,
		labelFieldName: 'id',
		instructions: 'Each compliance rule can either be assigned to a list of client accounts or be a <i>Global</i> rule: applies to all accounts.',

		items: [
			{fieldLabel: 'Compliance Rule', name: 'rule.name', hiddenName: 'rule.id', displayField: 'label', xtype: 'combo', url: 'complianceRuleListFind.json', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow', detailIdField: 'rule.id', allowBlank: false},
			{
				fieldLabel: 'Client Accounts', name: 'clientAccountIds', xtype: 'listfield',
				allowBlank: false, allowDuplicates: false,
				controlConfig: {fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', valueField: 'id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStatusName=Closed', detailIdField: 'clientInvestmentAccount.id'}
			},
			{name: 'exclusionAssignment', xtype: 'checkbox', boxLabel: 'Check to make this an exclusion assignment.', qtip: 'This will prevent the selected rule from being applied to the selected accounts, as long as this is the most specific assignment that applies to each account'},
			{xtype: 'sectionheaderfield', header: 'Ignore Conditions'},
			{fieldLabel: 'Sub Account Purpose', name: 'subAccountPurpose.name', hiddenName: 'subAccountPurpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json', qtip: 'Will also include positions from accounts related by this purpose type'},
			{fieldLabel: 'Subsidiary Purpose', name: 'subsidiaryPurpose.name', hiddenName: 'subsidiaryPurpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json', qtip: 'Will treat related accounts as wholly owned subsidiaries: one security as opposed to individual positions'},

			{xtype: 'sectionheaderfield', header: 'Ignore Conditions'},
			{fieldLabel: 'Pass Ignore Condition', name: 'ignorePassSystemCondition.name', hiddenName: 'ignorePassSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=ComplianceRuleAssignment', qtip: 'Passing rule is set to ignored for this assignment if the condition is met'},
			{fieldLabel: 'Fail Ignore Condition', name: 'ignoreFailSystemCondition.name', hiddenName: 'ignoreFailSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=ComplianceRuleAssignment', qtip: 'Failing rule is set to ignored for this assignment if the condition is met'},

			{xtype: 'sectionheaderfield', header: 'Active Date Range'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', width: 125, value: Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y')},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', width: 125},

			{xtype: 'sectionheaderfield', header: 'Note'},
			{xtype: 'textarea', name: 'note', height: 50}
		]
	}]
});
