Clifton.compliance.rule.ComplianceRuleAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Assignment',
	iconCls: 'verify',
	width: 700,
	height: 520,

	tbar: [
		{
			xtype: 'tbfill'
		},
		{
			text: 'Preview positions on ',
			tooltip: 'Preview positions for this assignment.',
			iconCls: 'preview',
			handler: function() {
				const toolbar = TCG.getParentByClass(this, Ext.Toolbar);
				const dateField = TCG.getChildByName(toolbar, 'processDate');
				const processDate = dateField.getValue().format('m/d/Y');

				const win = TCG.getParentByClass(toolbar, Ext.Window);
				const formPanel = win.getMainFormPanel();

				const businessServiceId = formPanel.getFormValue('businessService.id', true, true);
				const clientAccountId = formPanel.getFormValue('clientInvestmentAccount.id', true, true);
				const complianceRuleId = formPanel.getFormValue('rule.id', true, true);
				const ruleName = formPanel.getFormValue('rule.name');
				const clientInvestmentLabel = formPanel.getFormValue('clientInvestmentAccount.label');

				if (businessServiceId) {
					TCG.showError('Previews for business service assignments are not supported.');
					return;
				}
				if (!clientAccountId && !businessServiceId) {
					TCG.showError('Previews for global assignments are not supported.');
					return;
				}

				const defaultData = {
					complianceRuleId: complianceRuleId,
					businessServiceId: businessServiceId,
					clientAccountId: clientAccountId,
					processDate: processDate,
					ruleName: ruleName,
					clientInvestmentLabel: clientInvestmentLabel
				};

				const className = 'Clifton.compliance.rule.ComplianceRuleRunPreviewWindow';
				TCG.createComponent(className, {
					defaultData: defaultData,
					positionPreview: true,
					openerCt: this
				});
			}
		},
		{
			xtype: 'datefield', name: 'processDate', width: 90, value: Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y')
		}
	],

	items: [{
		xtype: 'formpanel',
		labelWidth: 130,
		url: 'complianceRuleAssignment.json',
		labelFieldName: 'scopeLabel',
		instructions: 'Each compliance rule can be assigned to a specific client account, to a business service, or to all accounts globally.',
		getWarningMessage: function(form) {
			let msg;
			if (form.formValues.global) {
				msg = '<b>CAUTION: </b>THIS RULE IS GLOBAL. IT APPLIES TO ALL CLIENT ACCOUNTS.';
				this.toggleGlobalAssignment(false);
			}
			return msg;
		},
		toggleGlobalAssignment: function(show) {
			if (show) {
				this.showField('clientInvestmentAccount.label');
				this.showField('businessService.label');
				this.showField('subAccountPurpose.name');
				this.showField('subsidiaryPurpose.name');
			}
			else {
				this.hideField('clientInvestmentAccount.label');
				this.hideField('businessService.label');
				this.hideField('subAccountPurpose.name');
				this.hideField('subsidiaryPurpose.name');
			}
		},
		getFirstInValidFieldOverride: function() {
			// Verify mutual exclusion for properties
			const clientAccountField = this.getForm().findField('clientInvestmentAccount.label');
			const businessServiceField = this.getForm().findField('businessService.label');
			const globalField = this.getForm().findField('global');
			const numProvidedProperties = !!clientAccountField.getValue() | 0 + !!businessServiceField.getValue() | 0 + !!globalField.getValue() | 0;
			const mutuallyExclusiveProperties = numProvidedProperties === 1;


			// Find invalid field
			let invalidField = null;
			if (!mutuallyExclusiveProperties) {
				TCG.showError('Rule assignments must assigned either globally, to a single client account, or to a single business service. Combined assignment types are not allowed.', 'Validation Error');
				if (!!clientAccountField.getValue()) {
					invalidField = clientAccountField;
				}
				else if (!!businessServiceField.getValue()) {
					invalidField = businessServiceField;
				}
				else {
					invalidField = globalField;
				}
			}

			// Return invalid field if present
			return invalidField;
		},

		items: [
			{fieldLabel: 'Compliance Rule', name: 'rule.name', hiddenName: 'rule.id', displayField: 'label', xtype: 'combo', url: 'complianceRuleListFind.json', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow'},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStatusName=Closed', detailPageClass: 'Clifton.investment.account.AccountWindow', mutuallyExclusiveFields: ['global', 'businessService.label']},
			{fieldLabel: 'Business Service', name: 'businessService.label', hiddenName: 'businessService.id', displayField: 'label', xtype: 'combo', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', detailPageClass: 'Clifton.business.service.ServiceWindow', mutuallyExclusiveFields: ['global', 'clientInvestmentAccount.label']},
			{
				name: 'global', xtype: 'checkbox', boxLabel: 'Check to allow Global Rule Assignment: applies to ALL Client Accounts',
				listeners: {
					check: function(o, checked) {
						const fp = TCG.getParentFormPanel(o);
						fp.toggleGlobalAssignment(!checked);
					}
				}
			},
			{name: 'exclusionAssignment', xtype: 'checkbox', boxLabel: 'Check to make this an exclusion assignment.', qtip: 'This will prevent the selected rule from being applied to the selected account or business service, as long as this is the most specific assignment that applies to this account'},
			{fieldLabel: 'Sub Account Purpose', name: 'subAccountPurpose.name', hiddenName: 'subAccountPurpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json', qtip: 'Will also include positions from accounts related by this purpose type'},
			{fieldLabel: 'Subsidiary Purpose', name: 'subsidiaryPurpose.name', hiddenName: 'subsidiaryPurpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json', qtip: 'Will treat related accounts as wholly owned subsidiaries: one security as opposed to individual positions'},

			{xtype: 'sectionheaderfield', header: 'Ignore Conditions'},
			{fieldLabel: 'Pass Ignore Condition', name: 'ignorePassSystemCondition.name', hiddenName: 'ignorePassSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=ComplianceRuleAssignment', qtip: 'If defined, and the condition returns true, any rule results for this assignment will be marked as ignored if that rule passes'},
			{fieldLabel: 'Fail Ignore Condition', name: 'ignoreFailSystemCondition.name', hiddenName: 'ignoreFailSystemCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=ComplianceRuleAssignment', qtip: 'If defined, and the condition returns true, any rule results for this assignment will be marked as ignored if that rule fails'},

			{xtype: 'sectionheaderfield', header: 'Active Date Range'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', width: 125, value: Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y')},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', width: 125},

			{xtype: 'sectionheaderfield', header: 'Note'},
			{xtype: 'textarea', name: 'note', height: 50}
		]
	}]
});
