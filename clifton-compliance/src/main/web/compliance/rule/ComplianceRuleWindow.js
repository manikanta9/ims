Clifton.compliance.rule.ComplianceRuleWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'complianceRule.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.compliance.rule.StandardComplianceRuleWindow';
		if (entity && entity.ruleType.rollupRule) {
			className = 'Clifton.compliance.rule.RollupComplianceRuleWindow';
		}
		return className;
	}
});
