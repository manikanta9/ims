Clifton.compliance.rule.ComplianceRuleLimitTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Limit Type',
	iconCls: 'verify',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Limit Type',
				items: [{
					xtype: 'formpanel',
					url: 'complianceRuleLimitType.json',
					readOnly: true,
					items: [
						{fieldLabel: 'Limit Type', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Rule Types',
				items: [{
					name: 'complianceRuleTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following types of compliance rules are defined in the system.',
					columns: [
						{header: 'Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Rollup', width: 20, dataIndex: 'rollupRule', type: 'boolean'},
						{header: 'Multiple Assignment', width: 50, dataIndex: 'multipleAssignmentAllowed', type: 'boolean'},
						{header: 'Security Specific', width: 35, dataIndex: 'investmentSecuritySpecific', type: 'boolean'},
						{header: 'Generate Details', width: 35, dataIndex: 'detailAllowed', type: 'boolean'}
					],
					getLoadParams: function() {
						return {complianceRuleLimitTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.compliance.rule.ComplianceRuleTypeWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
