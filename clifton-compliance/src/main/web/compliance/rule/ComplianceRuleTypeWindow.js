Clifton.compliance.rule.ComplianceRuleTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule Type',
	iconCls: 'verify',
	width: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Type',
				items: [{
					xtype: 'formpanel',
					url: 'complianceRuleType.json',
					labelWidth: 120,
					readOnly: true,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Evaluator Bean Type', name: 'ruleEvaluatorType.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.TypeWindow', detailIdField: 'ruleEvaluatorType.id'},
						{boxLabel: 'Rules of this type are a rollup of other rules', name: 'rollupRule', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Allow assigning instances of this rule to multiple accounts', name: 'multipleAssignmentAllowed', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Display "Specificity Scope" fields on rule detail screen and apply them', name: 'investmentSecuritySpecific', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Generate details for batch runs of rules of this type', name: 'detailAllowed', xtype: 'checkbox', labelSeparator: ''}
					]
				}]
			},


			{
				title: 'Limit Types',
				items: [{
					name: 'complianceRuleTypeLimitListFind',
					xtype: 'gridpanel',
					instructions: 'The following limit types are allowed for this rule type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Limit Type', width: 100, dataIndex: 'complianceRuleLimitType.name'},
						{header: 'Description', width: 250, dataIndex: 'complianceRuleLimitType.description'}
					],
					getLoadParams: function() {
						return {complianceRuleTypeId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Rules',
				items: [{
					name: 'complianceRuleListFind',
					xtype: 'gridpanel',
					instructions: 'The following rules have been created for this rule type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Rule Name', width: 180, dataIndex: 'name'},
						{header: 'Rule Type', dataIndex: 'ruleType.label', width: 80, filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'label', url: 'complianceRuleTypeListFind.json'}, hidden: true},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Batch', width: 35, dataIndex: 'batch', type: 'boolean'},
						{header: 'Real-Time', width: 35, dataIndex: 'realTime', type: 'boolean'},
						{header: 'Rollup', width: 35, dataIndex: 'ruleType.rollupRule', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('ruleType.rollupRule');
								return (t) ? 'Rollup Compliance Rule' : 'Standard Rule';
							},
							items: [
								{text: 'Standard Rule', iconCls: 'verify', className: 'Clifton.compliance.rule.StandardComplianceRuleWindow'},
								{text: 'Rollup Compliance Rule', iconCls: 'hierarchy', className: 'Clifton.compliance.rule.RollupComplianceRuleWindow'}
							]
						},
						getDefaultData: function(grid) {
							return {
								ruleType: grid.getWindow().getMainForm().formValues
							};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {typeId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
