Clifton.compliance.rule.RollupComplianceRuleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule (Rollup)',
	iconCls: 'verify',
	width: 800,
	height: 500,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Info',
			items: [{
				xtype: 'formpanel',
				url: 'complianceRule.json',
				instructions: 'A rollup rule is a collection of other rules. Assigning a rollup rule to an account is equivalent to assigning every single rule to that account. Rollup rules are used to simplify management of similar rule sets.',
				listeners: {
					afterrender: async formPanel => formPanel.setFormValue('ruleType.id', (await TCG.data.getDataPromise('complianceRuleTypeByName.json?name=Rollup Rule', formPanel)).id)
				},
				items: [
					{name: 'rollupRule', xtype: 'hidden', value: 'true'},
					{fieldLabel: 'Rule Name', name: 'name', xtype: 'textfield'},
					{fieldLabel: 'Rule Type Id', name: 'ruleType.id', xtype: 'hidden'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'},

					{
						xtype: 'formgrid-scroll',
						title: 'Child Compliance Rules',
						storeRoot: 'childComplianceRuleList',
						dtoClass: 'com.clifton.compliance.rule.ComplianceRule',
						collapsible: false,
						height: 190,
						heightResized: true,
						viewConfig: {forceFit: true, emptyText: 'This Rollup Rule currently has no child rules', deferEmptyText: false},
						columnsConfig: [
							{header: 'Rule', dataIndex: 'label', idDataIndex: 'id', editor: {xtype: 'combo', url: 'complianceRuleListFind.json', displayField: 'label', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow'}}
						]
					}
				]
			}]
		}, {
			title: 'Rule Assignments',
			items: [{
				xtype: 'compliance-assignment-grid',
				getLoadParams: function() {
					const ruleId = this.getWindow().params.id;
					return {ruleId: ruleId};
				}
			}]
		}]
	}]
});
