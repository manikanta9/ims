Clifton.compliance.rule.ComplianceRuleRunPreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Compliance Rule Preview',
	iconCls: 'verify',
	width: 1200,
	height: 600,
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	enableShowInfo: false,
	/**
	 * If set to <code>true</code>, this window will be used to generate a preview list of positions for an assignment. Otherwise, this window will be used to generate preview results
	 * an evaluation of the assignment.
	 */
	positionPreview: false,

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			height: 60,
			getDefaultData: function(win) {
				this.setFormValue('processDate', win.defaultData.processDate);
				this.setFormValue('complianceRuleRun.complianceRun.clientInvestmentAccount.label', win.defaultData.clientInvestmentLabel);
				this.setFormValue('complianceRuleRun.complianceRule.name', win.defaultData.ruleName || 'All');
				win.setTitle(win.defaultData.ruleName || 'All');
				return win.defaultData;
			},
			items: [{
				xtype: 'panel',
				layout: 'column',
				items: [{
					columnWidth: .75,
					layout: 'form',
					items: [
						{fieldLabel: 'Compliance Rule', name: 'complianceRuleRun.complianceRule.name', xtype: 'linkfield', detailIdField: 'complianceRuleId', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow'},
						{fieldLabel: 'Client Account', name: 'complianceRuleRun.complianceRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientAccountId', detailPageClass: 'Clifton.investment.account.AccountWindow'}
					]
				}, {
					columnWidth: .25,
					layout: 'form',
					items: [{
						fieldLabel: 'Run Date', name: 'processDate', xtype: 'datefield', listeners: {
							select: function(field) {
								// Reload grid with new run date
								const formPanel = TCG.getParentFormPanel(field);
								const win = formPanel.getWindow();
								const grid = win.items.find(item => item.xtype === 'compliance-rule-run-detail-grid');
								grid.reload();
							},
							specialkey: function(field, e) {
								if (e.getKey() === e.ENTER) {
									// Reload grid with new run date
									const formPanel = TCG.getParentFormPanel(field);
									const win = formPanel.getWindow();
									const grid = win.items.find(item => item.xtype === 'compliance-rule-run-detail-grid');
									grid.reload();
								}
							}
						}
					}]
				}]
			}]
		},

		{
			xtype: 'compliance-rule-run-detail-grid',
			instructions: 'This grid displays the rule run preview results for the given client account and compliance rule on the selected date.',
			flex: 1,

			initComponent: function() {
				if (this.getWindow().positionPreview) {
					// Remove result columns in assignment preview mode (display the list of positions to be analyzed only)
					this.columns = this.columns.filter(col => col.dataIndex === 'label');
				}
				else {
					// Setup columns for preview mode
					this.columns = this.columns.map(col => ({
						...col,
						dataIndex: col.dataIndex.replace(/^complianceRuleRun\./g, '')
					}));
				}
				TCG.callParent(this, 'initComponent', arguments);
			},

			getLoadURL: function() {
				return this.getWindow().positionPreview
					? 'complianceAssignmentPreviewList.json'
					: 'complianceRunPreviewList.json';
			},

			getLoadParams: function(firstLoad) {
				const loadParams = TCG.callParent(this, 'getLoadParams', arguments);
				if (loadParams === false) {
					return false;
				}

				const win = this.getWindow();
				const formPanel = win.items.find(item => item.xtype === 'formpanel');
				const processDate = formPanel.getFormValuesFormatted()['processDate'];

				return {
					...loadParams,
					// The response root entity (ComplianceRuleRun) is different than in the base class (ComplianceRuleRunDetail)
					requestedProperties: loadParams.requestedProperties.replace(/(^|\|)complianceRuleRun\./g, '$1'),
					categoryName: win.defaultData.categoryName || 'Trade Rules',
					categoryTableEntityId: win.defaultData.categoryTableEntityId,
					businessServiceId: win.defaultData.businessServiceId,
					clientAccountId: win.defaultData.clientAccountId,
					complianceRuleId: win.defaultData.complianceRuleId,
					complianceRuleTypeId: win.defaultData.complianceRuleTypeId,
					excludeParentSecuritySpecificRules: !!win.defaultData.excludeParentSecuritySpecificRules || win.defaultData.excludeParentSecuritySpecificRules == null,
					previewOnly: true,
					processDate: processDate,
					realTime: !!win.defaultData.realTime
				};
			}
		}
	]
});

