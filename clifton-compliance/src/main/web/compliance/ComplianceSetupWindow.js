Clifton.compliance.ComplianceSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'complianceSetupWindow',
	title: 'Compliance Rules',
	iconCls: 'verify',
	width: 1600,
	height: 800,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Rule Runs',
				items: [{
					xtype: 'compliance-run-grid'
				}]
			},


			{
				title: 'Rule Violations',
				reloadOnTabChange: true,
				items: [{
					xtype: 'base-rule-violation-grid',
					instructions: 'The following rule violations have been recorded for "Compliance Real Time Rules" rule definition. Defaults to today.',
					includeRuleCategoryFilter: false,
					includeRuleDefinitionFilter: false,
					includeDisplayFilter: true,
					groupField: undefined,
					hiddenColumns: [],
					columnOverrides: [
						{dataIndex: 'cancelledLinkedEntity', hidden: false},
						{dataIndex: 'createDate', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					],
					staticLoadParams: {ruleDefinitionNameEquals: 'Compliance Real Time Rules'}
				}]
			},


			{
				title: 'Compliance Tasks',
				reloadOnTabChange: true,
				items: [{
					xtype: 'workflow-task-advanced-grid',
					defaultCategoryName: 'Compliance Tasks'
				}]
			},


			{
				title: 'Rule Assignments',
				items: [{
					name: 'complianceRuleAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all compliance rule assignments defined in the system. Each rule can either be assigned to a specific Client Account, all accounts for a specific Business Service or be a <i>Global</i> rule: applies to all accounts.',
					wikiPage: 'IT/Compliance+Rule+Execution',
					groupField: 'scopeLabel',
					groupTextTpl: '{values.group} - {[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]}',
					remoteSort: true,
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Scope', width: 200, dataIndex: 'scopeLabel', defaultSortColumn: true, hidden: true},
						{header: 'Business Service', width: 150, dataIndex: 'businessService.label', hidden: true, filter: {type: 'combo', searchFieldName: 'businessServiceId', displayField: 'label', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT'}},
						{header: 'Client Account', width: 200, dataIndex: 'clientInvestmentAccount.label', hidden: true, filter: {type: 'combo', searchFieldName: 'accountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Workflow State', width: 60, dataIndex: 'clientInvestmentAccount.workflowState.name', hidden: true},
						{header: 'Rule Type', width: 100, dataIndex: 'rule.ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
						{
							header: 'Compliance Rule', dataIndex: 'rule.label', width: 200, filter: {type: 'combo', searchFieldName: 'ruleId', displayField: 'label', url: 'complianceRuleListFind.json'},
							renderer: function(v, metaData, r) {
								const description = r.data['rule.description'];
								if (TCG.isNotBlank(description)) {
									metaData.attr = TCG.renderQtip(description);
								}

								const note = r.data.note;
								if (TCG.isNotBlank(note)) {
									metaData.css = 'amountAdjusted';
								}

								return v;
							}
						},
						{header: 'Rule Description', dataIndex: 'rule.description', width: 200, hidden: true},
						{header: 'Sub Account Purpose', dataIndex: 'subAccountPurpose.name', width: 50, filter: {type: 'combo', searchFieldName: 'subAccountPurposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
						{header: 'Subsidiary Purpose', dataIndex: 'subsidiaryPurpose.name', width: 50, filter: {type: 'combo', searchFieldName: 'subsidiaryPurposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
						{header: 'Service Assignment', dataIndex: 'businessServiceAssignment', width: 50, type: 'boolean', filter: {searchFieldName: 'businessServiceNotNull'}},
						{header: 'Global Assignment', dataIndex: 'global', width: 50, type: 'boolean'},
						{header: 'Exclusion Assignment', dataIndex: 'exclusionAssignment', width: 50, type: 'boolean'},
						{header: 'Pass Ignore Condition', width: 100, dataIndex: 'ignorePassSystemCondition.name', filter: {type: 'combo', searchFieldName: 'ignorePassSystemConditionId', url: 'systemConditionListFind.json'}, hidden: true},
						{header: 'Fail Ignore Condition', width: 100, dataIndex: 'ignoreFailSystemCondition.name', filter: {type: 'combo', searchFieldName: 'ignoreFailSystemConditionId', url: 'systemConditionListFind.json'}, hidden: true},
						{header: 'Start Date', width: 40, dataIndex: 'startDate'},
						{header: 'End Date', width: 40, dataIndex: 'endDate'},
						{header: 'Note', width: 180, dataIndex: 'note', hidden: true}
					],
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								return 'Standard Assignment';
							},
							items: [
								{text: 'Standard Assignment', iconCls: 'verify', className: 'Clifton.compliance.rule.ComplianceRuleAssignmentWindow'},
								{text: 'Bulk Assignment', iconCls: 'hierarchy', className: 'Clifton.compliance.rule.BulkComplianceRuleAssignmentWindow'},
								{text: 'Copy Assignments', iconCls: 'copy', className: 'Clifton.compliance.rule.CopyComplianceRuleAssignmentWindow'}
							]
						}
					},
					addFirstToolbarButtons: function(toolBar) {
						const gp = this;
						toolBar.add({
							iconCls: 'expand-all',
							tooltip: 'Expand or Collapse Rule Assignments for all Client Accounts',
							scope: this,
							handler: function() {
								gp.grid.collapsed = !gp.grid.collapsed;
								gp.grid.view.toggleAllGroups(!gp.grid.collapsed);
							}
						});
						toolBar.add('-');
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const combo = TCG.getChildByName(this.getTopToolbar(), 'selectAssignmentType');
							combo.setValue({value: null, text: 'All Assignments'});
						}

						return {};
					},
					getTopToolbarFilters: function(toolbar) {
						return [{
							fieldLabel: 'Assignment Type', xtype: 'toolbar-combo', name: 'selectAssignmentType', width: 140, minListWidth: 120, forceSelection: false, mode: 'local', displayField: 'name', valueField: 'value',
							linkedFilter: 'exclusionAssignment',
							listeners: {
								select: function(combo) {
									if (TCG.isNull(combo.value)) {
										TCG.getParentByClass(combo, Ext.Panel).clearFilter(combo.linkedFilter, true);
									}
									else {
										TCG.getParentByClass(combo, Ext.Panel).setFilterValue(combo.linkedFilter, combo.value);
									}
								}
							},
							store: {
								xtype: 'arraystore',
								fields: ['value', 'name', 'description'],
								data: [
									[null, 'All Assignments', 'Display all assignments'],
									[false, 'Inclusion Assignments', 'Display only inclusion assignments '],
									[true, 'Exclusion Assignments', 'Display only exclusion assignments']
								]
							}
						}];
					},
					getGridCellComboLabelFilterValue: function(column, value) {
						return value;
					}
				}]
			},


			{
				title: 'Rule Library',
				items: [{
					name: 'complianceRuleListFind',
					xtype: 'gridpanel',
					instructions: 'A library of all available compliance rules. A rule must be assigned first before it will be used.',
					wikiPage: 'IT/Compliance+Specificity+Guide',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Rule Name', width: 180, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Rule Type', dataIndex: 'ruleType.label', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'label', url: 'complianceRuleTypeListFind.json'}},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Limit Type', width: 35, dataIndex: 'complianceRuleLimitType.name', filter: {searchFieldName: 'limitTypeName'}},
						{header: 'Min Limit', width: 35, dataIndex: 'limitValueRange', hidden: true, type: 'float', useNull: true},
						{header: 'Limit', width: 30, dataIndex: 'limitValue', type: 'float', useNull: true},
						{header: 'Investment Type', width: 50, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Sub Type', width: 50, dataIndex: 'investmentTypeSubType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeSubTypeId', displayField: 'name', url: 'investmentTypeSubTypeListFind.json', loadAll: true}, hidden: true},
						{header: 'Investment Sub Type 2', width: 50, dataIndex: 'investmentTypeSubType2.label', filter: {type: 'combo', searchFieldName: 'investmentTypeSubType2Id', displayField: 'name', url: 'investmentTypeSubType2ListFind.json', loadAll: true}, hidden: true},
						{header: 'Investment Hierarchy', width: 190, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 70, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},
						{header: 'Underlying Instrument', width: 70, dataIndex: 'underlyingInstrument.label', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', url: 'investmentInstrumentListFind.json'}, hidden: true},
						{header: 'CCY Type', width: 50, dataIndex: 'currencyType', storeData: Clifton.investment.instrument.CurrencyTypes, filter: {type: 'list', options: Clifton.investment.instrument.CurrencyTypes}, hidden: true},
						{header: 'Batch', width: 30, dataIndex: 'batch', type: 'boolean', filter: {searchFieldName: 'batchRule'}},
						{header: 'Real-Time', width: 35, dataIndex: 'realTime', type: 'boolean', filter: {searchFieldName: 'realTimeRule'}},
						{header: 'Rollup', width: 35, dataIndex: 'ruleType.rollupRule', type: 'boolean', hidden: true},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', hidden: true}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Rule Type', width: 200, xtype: 'combo', name: 'ruleType', url: 'complianceRuleTypeListFind.json', linkedFilter: 'ruleType.label'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function() {
						return {readUncommittedRequested: true};
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('ruleType.rollupRule');
								return (t) ? 'Rollup Compliance Rule' : 'Standard Rule';
							},
							items: [
								{text: 'Standard Rule', iconCls: 'verify', className: 'Clifton.compliance.rule.StandardComplianceRuleWindow'},
								{text: 'Rollup Compliance Rule', iconCls: 'hierarchy', className: 'Clifton.compliance.rule.RollupComplianceRuleWindow'}
							]
						}
					}
				}]
			},


			{
				title: 'Rule Types',
				items: [{
					name: 'complianceRuleTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following types of compliance rules are defined in the system.',
					wikiPage: 'IT/Compliance+Rule+Execution',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Evaluator Bean Type', dataIndex: 'ruleEvaluatorType.name'},
						{header: 'Rollup', width: 20, dataIndex: 'rollupRule', type: 'boolean'},
						{header: 'Multiple Assignment', width: 30, dataIndex: 'multipleAssignmentAllowed', type: 'boolean'},
						{header: 'Security Specific', width: 30, dataIndex: 'investmentSecuritySpecific', type: 'boolean'},
						{header: 'Generate Details', width: 30, dataIndex: 'detailAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.rule.ComplianceRuleTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Limit Types',
				items: [{
					name: 'complianceRuleLimitTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Compliance Rule Limit Types that represent ways the result of a rule execution should be executed. Ex. MAXIMUM: Percentage rule cannot exceed a certain maximum.',
					wikiPage: 'IT/Compliance+Rule+Execution',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Limit Type', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.compliance.rule.ComplianceRuleLimitTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Administration',
				items: [{
					xtype: 'compliance-runs-rebuild-form'
				}]
			}
		]
	}]
});
