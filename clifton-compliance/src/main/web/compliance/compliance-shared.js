Ext.ns('Clifton.compliance', 'Clifton.compliance.exchangelimit', 'Clifton.compliance.creditrating', 'Clifton.compliance.rule',
	'Clifton.compliance.rule.evaluators.comparison.transformation.CompliancePositionEndDateFilteringTransformation');

/*
 * Constants
 */

Clifton.compliance.rule.evaluators.comparison.transformation.CompliancePositionEndDateFilteringTransformation.DATE_FIELD_ENUM = [
	['END_DATE', 'EndDate', 'Investment Security end date.'],
	['LAST_DELIVERY_DATE', 'Last Delivery Date', 'Investment Security last delivery date.']
];

Clifton.compliance.rule.evaluators.comparison.transformation.FIELD_PREFIX_DELIMITER_CUSTOM_SYNTAX_DESCRIPTION = TCG.trimWhitespace(`
	<br/><br/>
	Custom syntaxes:
	<ul>
		<li><b><code>#</code></b> (as prefix): Market data field (non-flexible lookup). E.g., <code>#Delta</code>. </li>
		<li><b><code>##</code></b> (as prefix): Market data field (flexible lookup). E.g., <code>##Delta</code>. </li>
		<li><b><code>%</code></b> (as delimiter): Custom column. E.g., <code>investmentSecurity%Put or Call</code>.</li>
		<li><b><code>!</code></b> (as prefix): Opening trade field. E.g., <code>!expectedUnitPrice</code>.</li>
		<li><b><code>!#</code></b> (as prefix): Trade market data field. E.g., <code>!#Delta - Trade Data Field</code>.</li>
	</ul>
	</br/>
	The following custom column groups are used when selecting custom columns:
	<ul>
		<li>Securities: <b><code>"Security Custom Fields"</code></b></li>
		<li>Instruments: <b><code>"Instrument Custom Fields"</code></b></li>
		<li>Business Companies: <b><code>"Company Custom Fields"</code></b></li>
		<li>Investment Accounts: <b><code>"Portfolio Setup"</code></b></li>
	</ul>
`);

// Add "Compliance" tab to business service window
Clifton.business.service.ServiceWindowAdditionalTabs.push({
	title: 'Compliance',
	items: [{
		xtype: 'compliance-assignment-grid',
		stripeRows: true,
		defaultView: 'Scoped',
		selectionRequired: false,
		previewEnabled: false,
		getLoadParams: function(firstLoad) {
			const win = this.getWindow();
			const params = {appliesToBusinessServiceId: win.params.id};
			// Apply default convenience sorting
			if (firstLoad) {
				this.ds.sortInfo = null;
			}
			if (!this.ds.sortInfo) {
				params.orderBy = 'ruleTypeId#scopeLabel#ruleName';
			}
			return {
				...params,
				...this.getTopToolbarFilterLoadParams()
			};
		},
		getEditorDefaultData: (gridPanel, row) => ({
			businessService: {
				id: gridPanel.getWindow().getMainFormId(),
				label: TCG.getValue('formValues.label', gridPanel.getWindow().getMainForm()) || gridPanel.getWindow().getMainForm().getFieldValues().name
			}
		})
	}]
});

Clifton.investment.instrument.InstrumentCustomAdditionalTabsByType['Future'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.InstrumentCustomAdditionalTabsByType['Future'], {
	index: 3,
	item: {
		title: 'Instrument Limits',
		items: [{
			xtype: 'compliance-exchangelimit-instrument-grid',
			getInstrumentId: function() {
				return this.getWindow().getMainFormId();
			}
		}]
	}
});

Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'], {
	title: 'Credit Ratings',
	index: 1,
	items: [{
		xtype: 'compliance-credit-rating-value-grid',
		forSecurity: true,
		getLoadParams: function(firstLoad) {
			const params = this.getTopToolbarInitialLoadParams(firstLoad);
			params.securityId = this.getWindow().getMainFormId();
			return params;
		},
		getToolbarAddButtonElements: function() {
			return [{
				text: 'Import Latest Ratings',
				tooltip: 'Import latest credit ratings from Bloomberg',
				iconCls: 'run',
				scope: this,
				handler: function() {
					this.importCreditRatings(false);
				}
			}, '-'
			].concat(this.constructor.prototype.getToolbarAddButtonElements.apply(this, arguments));
		},
		getNewItemDefaultData: function(editor, row, detailPageClass, itemText) {
			return Ext.apply({},
				this.constructor.prototype.getNewItemDefaultData.apply(this, arguments),
				{security: this.getWindow().getMainForm().formValues});
		},
		importCreditRatings: function(historic) {
			const grid = this;

			const params = {'securityId': this.getWindow().getMainFormId(), 'providerName': 'Bloomberg'};
			const loader = new TCG.data.JsonLoader({
				waitTarget: grid,
				waitMsg: 'Processing...',
				params: params,
				conf: params,
				onLoad: function(record, conf) {
					grid.reload();
				},
				onFailure: function() {
					// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
					grid.reload();
				}
			});
			loader.load(historic ? 'complianceCreditRatingHistoryLoad.json' : 'complianceCreditRatingLoad.json');
		}
	}]
});

/*
 * Classes
 */

Clifton.compliance.ComplianceRuleForm = Ext.extend(Ext.Panel, {
	layout: 'anchor',
	autoScroll: true,
	border: false,
	frame: true,
	bodyStyle: 'padding: 5px 5px 0',

	initComponent: function() {
		// Attach "items" property manually instead of on prototype; ExtJS does not properly handle prototype-scoped "items" values during panel children
		this.items = [...this.complianceItems];
		Clifton.compliance.ComplianceRuleForm.superclass.initComponent.call(this);
	},

	complianceItems: [{
		xtype: 'fieldset',
		title: 'Rule Assignments',
		collapsed: false,
		items: [{
			frame: true,
			height: 300,
			anchor: '0',
			stripeRows: true,
			layout: 'fit',
			xtype: 'compliance-assignment-grid',
			defaultView: 'Scoped',
			selectionRequired: false,
			getLoadParams: function(firstLoad) {
				const win = this.getWindow();
				const params = {appliesToAccountId: win.params.id};
				// Apply default convenience sorting
				if (firstLoad) {
					this.ds.sortInfo = null;
				}
				if (!this.ds.sortInfo) {
					params.orderBy = 'ruleTypeId#scopeLabel#ruleName';
				}
				return {
					...params,
					...this.getTopToolbarFilterLoadParams()
				};
			}
		}]
	}, {
		xtype: 'fieldset',
		title: 'Client Approved Contracts',
		collapsed: true,
		items: [{
			xtype: 'gridpanel',
			frame: true,
			height: 300,
			anchor: '0',
			layout: 'fit',
			name: 'complianceRuleOldListFind',
			columns: [
				{header: 'Type', width: 100, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
				{header: 'Client Account', width: 100, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 40, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
				{header: 'Holding Account', width: 60, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
				{header: 'Investment Type', width: 50, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
				{header: 'Investment Hierarchy', width: 100, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
				{header: 'Investment Instrument', width: 100, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}, defaultSortColumn: true},
				{header: 'Ignorable', width: 40, dataIndex: 'ignorable', type: 'boolean', hidden: true}
			],
			getLoadParams: function(firstLoad) {
				const win = this.getWindow();
				return {
					typeName: this.editor.complianceRuleTypeName,
					clientInvestmentAccountId: win.params.id
				};
			},
			editor: {
				detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
				complianceRuleTypeName: 'Client Approved Contracts',
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					const typeId = TCG.data.getData('complianceRuleTypeOldByName.json?typeName=' + this.complianceRuleTypeName, gridPanel).id;
					const win = this.getWindow();
					toolBar.add(new TCG.form.ComboBox({name: 'investmentInstrument', displayField: 'labelShort', url: 'investmentInstrumentListFind.json', width: 150, listWidth: 230}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add instrument to selected clients approved contracts.',
						iconCls: 'add',
						handler: function() {
							const investmentInstrumentId = TCG.getChildByName(toolBar, 'investmentInstrument').getValue();
							const clientInvestmentAccountId = win.params.id;
							if (investmentInstrumentId === '') {
								TCG.showError('You must first select desired investment instrument.');
							}
							else {
								const params = {'ruleType.id': typeId, 'investmentInstrument.id': investmentInstrumentId, 'clientInvestmentAccount.id': clientInvestmentAccountId};
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Adding...',
									params: params,
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.getChildByName(toolBar, 'investmentInstrument').reset();
									}
								});
								loader.load('complianceRuleOldSave.json');
							}
						}
					});
					toolBar.add('-');
				}
			}
		}]
	}, {
		xtype: 'fieldset',
		title: 'Notional Limits',
		collapsed: true,
		items: [{
			xtype: 'gridpanel',
			frame: true,
			anchor: '0',
			height: 300,
			stripeRows: true,
			layout: 'fit',
			name: 'complianceRuleOldListFind',
			columns: [
				{header: 'Type', width: 100, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
				{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},

				{header: 'Investment Hierarchy', width: 100, dataIndex: 'investmentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
				{header: 'Investment Instrument', width: 100, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},
				{header: 'Investment Type', width: 100, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', url: 'investmentTypeListFind.json'}},

				{header: 'Long Position Qty', width: 40, dataIndex: 'maxLongPositionQuantity', type: 'float', useNull: true, hidden: true},
				{header: 'Short Position Qty', width: 40, dataIndex: 'maxShortPositionQuantity', type: 'float', useNull: true, hidden: true},

				{header: 'Long Position Notional', width: 40, dataIndex: 'maxLongPositionAccountingNotional', type: 'float', useNull: true, hidden: true},
				{header: 'Short Position Notional', width: 40, dataIndex: 'maxShortPositionAccountingNotional', type: 'float', useNull: true, hidden: true}

			],
			getLoadParams: function(firstLoad) {
				const win = this.getWindow();
				return {
					typeName: this.editor.complianceRuleTypeName,
					clientInvestmentAccountId: win.params.id,
					requestedMaxDepth: 3
				};
			},
			editor: {
				detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
				complianceRuleTypeName: 'Notional Limit',
				getDefaultData: function(gridPanel, row) {
					// use selected row parameters for drill down filtering
					return {
						typeName: this.complianceRuleTypeName
					};
				}
			}
		}]
	}, {
		xtype: 'fieldset',
		title: 'Short/Long',
		collapsed: true,
		items: [{
			xtype: 'gridpanel',
			frame: true,
			anchor: '0',
			height: 300,
			stripeRows: true,
			layout: 'fit',
			name: 'complianceRuleOldListFind',
			columns: [
				{header: 'Type', width: 100, dataIndex: 'ruleType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}},
				{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},

				{header: 'Investment Type', width: 50, dataIndex: 'investmentType.label', filter: {type: 'combo', searchFieldName: 'investmentTypeId', url: 'investmentTypeListFind.json'}},
				{header: 'Investment Hierarchy', width: 100, dataIndex: 'investmentHierarchy.name', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
				{header: 'Investment Instrument', width: 100, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', url: 'investmentInstrumentListFind.json'}},

				{header: 'Short Allowed', width: 40, dataIndex: 'shortPositionAllowed', type: 'boolean', align: 'center'},
				{header: 'Long Allowed', width: 40, dataIndex: 'longPositionAllowed', type: 'boolean', align: 'center'}
			],
			getLoadParams: function(firstLoad) {
				const win = this.getWindow();
				return {
					typeName: this.editor.complianceRuleTypeName,
					clientInvestmentAccountId: win.params.id,
					requestedMaxDepth: 3
				};
			},
			editor: {
				detailPageClass: 'Clifton.compliance.ComplianceRuleOldWindow',
				complianceRuleTypeName: 'Short/Long',
				getDefaultData: function(gridPanel, row) {
					// use selected row parameters for drill down filtering
					return {
						typeName: this.complianceRuleTypeName
					};
				}
			}
		}]
	}]
});
Ext.reg('compliance-rule-form', Clifton.compliance.ComplianceRuleForm);

Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes = [
	['All Months', 'ALL_MONTHS', 'Applies to all positions'],
	['Single Month', 'SINGLE_MONTH', 'Applies to all positions excluding those currently in Spot Month grouped by month'],
	['Spot Month', 'SPOT_MONTH', 'Applies to positions in Spot Month only'],
	['Scale Down Spot Month', 'SCALE_DOWN_SPOT_MONTH', 'Applies to positions in Spot Month only during last x days of trading']
];

Clifton.compliance.exchangelimit.renderInstrumentPositionCountTooltip = function(row, metaData) {
	const getCount = type => row.data[`${type}InstrumentCount`] || 0;
	const formatCount = type => Ext.util.Format.number(getCount(type), '0,000');
	const instrumentCountRowHtml = `
			<tr><td colspan="3"><b>Instrument Counts:</b></td></tr>
			<tr><td>&nbsp;</td><td>Long:</td><td align="right">${formatCount('long')}</td></tr>
			<tr><td>&nbsp;</td><td>Short:</td><td align="right">${formatCount('long')}</td></tr>
			<tr><td>&nbsp;</td><td colspan="2"><hr/></td></tr>
			<tr><td>&nbsp;</td><td><b>Total:</b></td><td align="right"><b>${formatCount('total')}</b></td></tr>
	`;
	const pendingInstrumentRowHtml = (getCount('longPending') !== 0 || getCount('shortPending') !== 0)
		? `
			<tr><td>&nbsp;</td><td>Pending Long:</td><td align="right">${formatCount('longPending')}</td></tr>
			<tr><td>&nbsp;</td><td>Pending Short:</td><td align="right">${formatCount('shortPending')}</td></tr>
			<tr><td>&nbsp;</td><td colspan="2"><hr/></td></tr>
			<tr><td>&nbsp;</td><td><b>Pending Total:</b></td><td align="right"><b>${formatCount('totalPending')}</b></td></tr>
		`
		: '';
	const bigInstrumentRowHtml = (getCount('longBig') !== 0 || getCount('shortBig') !== 0 || getCount('longPendingBig') !== 0 || getCount('shortPendingBig') !== 0)
		? `
			<tr><td><b>Big Instrument Counts:</b></td></tr>
			<tr><td>&nbsp;</td><td>Long:</td><td align="right">${formatCount('longBig')}</td></tr>
			<tr><td>&nbsp;</td><td>Short:</td><td align="right">${formatCount('shortBig')}</td></tr>
			<tr><td>&nbsp;</td><td colspan="2"><hr/></td></tr>
			<tr><td>&nbsp;</td><td><b>Total:</b></td><td align="right"><b>${formatCount('totalBig')}</b></td></tr>
		`
		: '';
	const pendingBigInstrumentRowHtml = (getCount('longPendingBig') !== 0 || getCount('shortPendingBig') !== 0)
		? `
			<tr><td>&nbsp;</td><td>Pending Long:</td><td align="right">${formatCount('longPendingBig')}</td></tr>
			<tr><td>&nbsp;</td><td>Pending Short:</td><td align="right">${formatCount('shortPendingBig')}</td></tr>
			<tr><td>&nbsp;</td><td colspan="2"><hr/></td></tr>
			<tr><td>&nbsp;</td><td><b>Pending Total:</b></td><td align="right"><b>${formatCount('totalPendingBig')}</b></td></tr>
		`
		: '';
	const qtipHtml = `
		<table>
			${instrumentCountRowHtml}
			${pendingInstrumentRowHtml}
			${bigInstrumentRowHtml}
			${pendingBigInstrumentRowHtml}
		</table>
	`;
	metaData.attr = `qtip='${TCG.trimWhitespace(qtipHtml)}'`;
	metaData.css = 'amountAdjusted';
};

Clifton.compliance.exchangelimit.InstrumentExchangeLimitGrid = Ext.extend(TCG.grid.GridPanel, {
	getInstrumentId: function() {
		// Default -
		return TCG.getValue('instrument.id', this.getWindow().getMainForm().formValues);
	},
	name: 'compliancePositionExchangeLimitListFind',
	instructions: 'The following are the Exchange Position limits currently in the system active for the given date for the instrument. Exchange Position Limits must be unique per type, instrument, accountability, and date range.',
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
		];
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Hierarchy', hidden: true, width: 150, dataIndex: 'instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
		{header: 'Primary Exchange', hidden: true, width: 150, dataIndex: 'instrument.exchange.name', filter: {type: 'combo', searchFieldName: 'instrumentExchangeId', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
		{header: 'Instrument', hidden: true, width: 150, dataIndex: 'instrument.labelShort', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},

		{header: 'Big Instrument', width: 100, hidden: true, dataIndex: 'instrument.bigInstrument.labelShort', filter: {type: 'combo', searchFieldName: 'bigInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
		{
			header: 'Limit Type', width: 80, dataIndex: 'limitTypeLabel', filter: {
				type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', searchFieldName: 'limitType',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.compliance.exchangelimit.CompliancePositionExchangeLimitTypes
				})
			}
		},
		{header: 'Accountability', width: 70, dataIndex: 'accountabilityLimit', type: 'boolean'},
		{header: 'Start Date', width: 65, dataIndex: 'startDate'},
		{header: 'End Date', width: 65, dataIndex: 'endDate'},
		{header: 'Active', width: 70, dataIndex: 'active', type: 'boolean', filter: false, sortable: false, hidden: true},
		{header: 'Exchange Limit', width: 75, dataIndex: 'exchangeLimit', type: 'int', useNull: true},
		{header: 'Our Limit', width: 70, dataIndex: 'ourLimit', type: 'int', useNull: true}
	],
	editor: {
		detailPageClass: 'Clifton.compliance.exchangelimit.ExchangeLimitWindow',
		getDefaultData: function(gridPanel) { // defaults instrument for the detail page
			return {
				instrument: gridPanel.getWindow().getMainForm().formValues
			};
		}
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		const bd = TCG.getChildByName(t, 'activeOnDate');
		if (firstLoad) {
			// default to today
			if (!bd.getValue()) {
				bd.setValue((new Date()).format('m/d/Y'));
			}
		}
		if (bd.getValue()) {
			const dateValue = (bd.getValue()).format('m/d/Y');
			return {
				instrumentId: this.getInstrumentId(),
				activeOnDate: dateValue
			};
		}
		return {
			instrumentId: this.getInstrumentId()
		};
	}
});
Ext.reg('compliance-exchangelimit-instrument-grid', Clifton.compliance.exchangelimit.InstrumentExchangeLimitGrid);

/**
 * The tree-grid component for compliance rule run details.
 */
Clifton.compliance.rule.ComplianceRuleRunDetailGrid = Ext.extend(TCG.tree.TreeGrid, {
	name: 'complianceRuleRunDetailListFind',
	defaults: {anchor: '-35 -35'}, // leave room for error icon
	appendStandardColumns: false,
	readOnly: true,

	columns: [
		{header: 'Details', width: 800, dataIndex: 'label', renderer: (value, entity) => `<span ext:qtip="${value}">${value}</span>`}, // Provide tooltips for long messages
		{header: 'Limit Type', width: 80, dataIndex: 'complianceRuleRun.complianceRule.complianceRuleLimitType.name'},
		{header: 'Limit Value', width: 70, dataIndex: 'complianceRuleRun.limitValue', type: 'float'},
		{header: 'Result (%)', width: 65, name: 'percentOfAssets', dataIndex: 'percentOfAssets', type: 'percent'},
		{header: 'Shares/Value', width: 100, name: 'resultValue', dataIndex: 'resultValue', type: 'currency', qtip: 'Represents the most meaningful value from the set of all possible run details. For instance, if the industry concentration rule has several industries that failed, we will pull out the one that failed the most to be displayed in this overall Result Value.'},
		{header: 'Pass', dataIndex: 'pass', width: 50, name: 'pass', type: 'boolean', renderer: (value, entity) => TCG.renderEvalResultIcon(value != null ? value : entity.status)}
	],

	getLoadParams: function(firstLoad) {
		return {
			requestedPropertiesRoot: 'data',
			requestedProperties: 'id|label|resultValue|percentOfAssets|limitValue|pass|status|error|children|leaf|complianceRule.id|complianceRuleRun.complianceRule.complianceRuleLimitType.name|complianceRuleRun.limitValue',
			requestedPropertiesToExcludeGlobally: 'causeEntity'
		};
	},

	getContextMenuItems: function(node, eventObj) {
		const entity = node.attributes;
		const desc = entity.label;
		const transactionId = desc && Number.parseInt(desc.substring(0, desc.indexOf(':')));
		const menuItems = [];

		// Rule menu items
		if (node.getDepth() === 1) {
			menuItems.push({
				text: 'Open Rule',
				iconCls: 'verify',
				disabled: !entity.complianceRule,
				handler: () => {
					const complianceRuleId = entity.complianceRule.id;
					const classname = 'Clifton.compliance.rule.StandardComplianceRuleWindow';
					TCG.createComponent(classname, {
						id: TCG.getComponentId(classname, complianceRuleId),
						params: {id: complianceRuleId},
						openerCt: this
					});
				}
			});
		}
		// Transaction menu items
		else {
			menuItems.push({
				text: 'Open Source Screen',
				iconCls: 'book-open',
				disabled: isNaN(transactionId),
				handler: async () => {
					const result = await this.getDrillDownWindow(transactionId, 'SOURCE');
					TCG.createComponent(result.classname, {
						id: TCG.getComponentId(result.classname, result.id),
						params: {id: result.id},
						openerCt: this
					});
				}
			}, {
				text: 'Open Journal',
				iconCls: 'book-open-blue',
				disabled: isNaN(transactionId),
				handler: async () => {
					const result = await this.getDrillDownWindow(transactionId, 'JOURNAL');
					TCG.createComponent(result.classname, {
						id: TCG.getComponentId(result.classname, result.id),
						params: {id: result.id},
						openerCt: this
					});
				}
			}, {
				text: 'Open Transaction',
				iconCls: 'book-red',
				disabled: isNaN(transactionId),
				handler: async () => {
					const result = await this.getDrillDownWindow(transactionId, 'TRANSACTION');
					TCG.createComponent(result.classname, {
						id: TCG.getComponentId(result.classname, result.id),
						params: {id: result.id},
						openerCt: this
					});
				}
			});
		}

		if (menuItems.length > 0) {
			const parentItemList = TCG.tree.TreeGrid.prototype.getContextMenuItems.call(this, node, eventObj);
			if (parentItemList.length > 0) {
				menuItems.push('-', parentItemList);
			}
		}
		return menuItems;
	},

	getDrillDownWindow: async function(transactionId, screenSelection) {
		// Guard-clause: No transaction present
		if (!transactionId) {
			throw new Error('An attempt was made to open a drill-down window, but no transaction ID for the target window was specified.');
		}

		// Determine ID and detail page class from screen selection
		let id;
		let detailPage;
		switch (screenSelection) {
			case 'TRANSACTION':
				detailPage = 'Clifton.accounting.gl.TransactionWindow';
				id = transactionId;
				break;
			case 'JOURNAL':
				detailPage = 'Clifton.accounting.journal.JournalWindow';
				id = TCG.getValue('journal.id', await this.getAccountingTransaction(transactionId));
				break;
			case 'SOURCE': {
				// Find source
				const transaction = await this.getAccountingTransaction(transactionId);
				const fkFieldId = TCG.getValue('journal.fkFieldId', transaction);
				if (fkFieldId) {
					// Sub-system-specific screen
					detailPage = TCG.getValue('journal.journalType.systemTable.detailScreenClass', transaction);
					id = fkFieldId;
					if (!detailPage) {
						const tableName = TCG.getValue('journal.journalType.systemTable.name', transaction);
						TCG.showError('Screen class is not defined for table: ' + tableName, 'Undefined Sub-System Screen');
						return;
					}
				}
				else {
					detailPage = 'Clifton.accounting.journal.JournalWindow';
					id = TCG.getValue('journal.id', transaction);
				}
				break;
			}
			default:
				TCG.showError('Unrecognized screen selection: ' + screenSelection, 'Error During Screen Selection');
				return;
		}

		// Error-checking
		if (!id || !detailPage) {
			TCG.showError('Unable to process screen selection. An ID or detail page was not defined.', 'Error During Screen Selection');
			throw new Error('Unable to process screen selection. An ID or detail page was not defined.');
		}
		return {classname: detailPage, id: id};
	},

	getAccountingTransaction: async function(transactionId) {
		return await TCG.data.getDataPromiseUsingCaching('accountingTransaction.json', this, 'ComplianceRuleRunPreviewWindow-' + transactionId, {
			params: {
				id: transactionId,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'journal.id|journal.fkFieldId|journal.journalType.systemTable.detailScreenClass|journal.journalType.systemTable.name'
			}
		});
	}
});
Ext.reg('compliance-rule-run-detail-grid', Clifton.compliance.rule.ComplianceRuleRunDetailGrid);

Clifton.compliance.RunGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'complianceRuleRunListFind',
	instructions: 'Results for batch compliance rules run on the specified date. Runs are executed only for Active client accounts and active rule assignments. Use context menu (right click on a row) in order to Review Failed executions.',
	viewNames: ['Default', 'Failed Review'],
	wikiPage: 'IT/Compliance+Rule+Execution',
	groupField: 'complianceRun.clientInvestmentAccount.label',
	groupTextTpl: '{values.group}',
	remoteSort: true,
	appendStandardColumns: false,
	includeClientAccountFilter: true,
	includeRunDateFilter: true,
	defaultStatusFilterValue: 'NOT_PASSED',
	rowSelectionModel: 'multiple',
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
				text: 'Preview',
				tooltip: 'Preview all details for selected rule run. Rule run is not saved.',
				iconCls: 'preview',
				scope: this,
				handler: function() {
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();

					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be previewed.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection previews are not supported yet.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						const data = sm.getSelected().json;
						const clientAccountId = data.complianceRun.clientInvestmentAccount.id;
						const complianceRuleId = TCG.isNull(data.complianceRule) ? undefined : data.complianceRule.id;
						const complianceRuleTypeId = TCG.isNull(complianceRuleId) ? data.ruleType.id : undefined;
						const ruleName = TCG.isNull(complianceRuleId) ? data.ruleType.name : data.complianceRule.name;
						const clientInvestmentLabel = sm.getSelected().get('complianceRun.clientInvestmentAccount.label');

						const defaultData = {
							complianceRuleId: complianceRuleId,
							clientAccountId: clientAccountId,
							complianceRuleTypeId: complianceRuleTypeId,
							processDate: TCG.parseDateSmart(data.complianceRun.runDate).format('m/d/Y'),
							ruleName: ruleName,
							clientInvestmentLabel: clientInvestmentLabel
						};

						const className = 'Clifton.compliance.rule.ComplianceRuleRunPreviewWindow';
						TCG.createComponent(className, {
							defaultData: defaultData,
							openerCt: gridPanel
						});
					}
				}
			}, '-',
			{
				text: 'Dashboard',
				iconCls: 'chart-bar',
				handler: function() {
					TCG.createComponent('Clifton.compliance.ComplianceDashboardWindow', {openerCt: gridPanel});
				}
			}, '-'
		);
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (this.includeClientAccountFilter) {
			filters.push({fieldLabel: 'Client Account', name: 'clientAccount', valueField: 'id', displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.InvestmentAccountWindow'});
		}
		filters.push({
			fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'status', width: 110, minListWidth: 150, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['ALL', 'All', 'Display all runs'],
					['FAILED', 'Failed', 'Display only Failed runs that have not been reviewed yet'],
					['FAILED_REVIEWED', 'Failed (Reviewed)', 'Display only Failed runs that have been reviewed'],
					['NOT_PASSED', 'Not Passed', 'Display all runs that have not passed: Failed, Failed (Reviewed), Ignored'],
					['IGNORED', 'Ignored', 'Display only Ignored runs']
				]
			})
		});
		if (this.includeRunDateFilter) {
			filters.push({fieldLabel: 'Run Date', xtype: 'toolbar-datefield', name: 'runDate'});
		}
		return filters;
	},

	columns: [
		{header: 'ID', dataIndex: 'id', width: 40, hidden: true},
		{header: 'Run Date', width: 50, dataIndex: 'complianceRun.runDate', filter: {searchFieldName: 'runDate'}, hidden: true},
		{header: 'Client Account', width: 150, dataIndex: 'complianceRun.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
		{header: 'Client Account ID', width: 150, dataIndex: 'complianceRun.clientInvestmentAccount.id', hidden: true, filter: {searchFieldName: 'clientInvestmentAccountId'}},
		{header: 'Rule Type', dataIndex: 'ruleType.name', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'complianceRuleTypeId', url: 'complianceRuleTypeListFind.json'}},
		{header: 'Rule ID', dataIndex: 'complianceRule.id', width: 100, hidden: true, filter: {searchFieldName: 'complianceRuleId'}},
		{
			header: 'Compliance Rule', dataIndex: 'complianceRule.name', width: 200, filter: {searchFieldName: 'complianceRuleName'},
			renderer: (v, metaData, r) => v || r.data['ruleType.name'], viewNames: ['Default', 'Failed Review']
		},
		{header: 'Run Details', width: 200, dataIndex: 'description', viewNames: ['Default', 'Failed Review']},
		{header: 'Limit Type', width: 50, dataIndex: 'complianceRule.complianceRuleLimitType.name', filter: {searchFieldName: 'limitType'}, viewNames: ['Default']},
		{header: 'Limit Value', width: 50, dataIndex: 'limitValue', type: 'float', useNull: true, viewNames: ['Default']},
		{header: 'Result (%)', dataIndex: 'percentOfAssets', width: 50, type: 'percent', useNull: true, viewNames: ['Default']},
		{header: 'Shares/Value', dataIndex: 'resultValue', width: 50, type: 'currency', useNull: true, viewNames: ['Default'], qtip: 'Represents the most meaningful value from the set of all possible run details. For instance, if the industry concentration rule has several industries that failed, we will pull out the one that failed the most to be displayed in this overall Result Value.'},

		{header: 'Reviewed By', dataIndex: 'reviewedByUser.label', width: 40, hidden: true, filter: {searchFieldName: 'reviewedByUserId', url: 'securityUserListFind.json', displayField: 'label'}, viewNames: ['Failed Review']},
		{header: 'Review Date', dataIndex: 'reviewedOnDate', width: 55, hidden: true, viewNames: ['Failed Review']},
		{header: 'Review Note', dataIndex: 'reviewNote', width: 150, hidden: true, viewNames: ['Failed Review'], renderer: TCG.renderValueWithTooltip},

		{xtype: 'compliancerunstatuscolumn', width: 25, viewNames: ['Default', 'Failed Review']},
		{
			header: 'Created By', width: 30, dataIndex: 'complianceRun.createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'complianceRun.createDate', hidden: true},
		{
			header: 'Updated By', width: 30, dataIndex: 'complianceRun.updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 40, dataIndex: 'complianceRun.updateDate', hidden: true}
	],
	listeners: {
		afterrender: function(gridPanel) {
			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				const g = gridPanel.grid;
				g.contextRowIndex = g.view.findRowIndex(target);
				e.preventDefault();
				if (!g.drillDownMenu) {
					g.drillDownMenu = new Ext.menu.Menu({
						items: [
							{
								text: 'Generate Details',
								iconCls: 'verify',
								handler: function() {
									const data = g.store.data.items[g.contextRowIndex].data;
									const clientAccountId = data['complianceRun.clientInvestmentAccount.id'];
									const complianceRuleId = data['complianceRule.id'];
									const ruleName = data['complianceRule.name'];
									const clientInvestmentLabel = data['complianceRun.clientInvestmentAccount.label'];
									const processDate = data['complianceRun.runDate'].format('m/d/Y');

									const defaultData = {
										complianceRuleId: complianceRuleId,
										clientAccountId: clientAccountId,
										processDate: processDate,
										ruleName: ruleName,
										clientInvestmentLabel: clientInvestmentLabel
									};

									const className = 'Clifton.compliance.rule.ComplianceRuleRunPreviewWindow';
									TCG.createComponent(className, {
										defaultData: defaultData,
										openerCt: this
									});
								}
							}, '-',
							{
								text: 'Review Failed', iconCls: 'checked',
								handler: function() {
									gridPanel.openComplianceRuleRunReviewWindow(gridPanel,false);
								}
							},
							{
								text: 'Undo Review', iconCls: 'undo',
								handler: function() {
									gridPanel.openComplianceRuleRunReviewWindow(gridPanel,true);
								}
							}
						]
					});
				}
				g.drillDownMenu.showAt(e.getXY());
			}, gridPanel);
		}
	},

	openComplianceRuleRunReviewWindow: function(gridPanel, undo) {
		const rows = gridPanel.grid.getSelectionModel().getSelections();
		if (rows.length < 1) {
			TCG.showError('At least one row must be selected.', 'No Row(s) Selected');
			return;
		}
		const values = [];
		for (let i = 0; i < rows.length; i++) {
			values.push(rows[i].data.id);
		}
		TCG.createComponent('Clifton.compliance.rule.ComplianceRuleRunReviewWindow', {
			iconCls: undo ? 'undo' : 'checked',
			undoReviewWindow: undo,
			defaultData: {
				ruleRunIds: JSON.stringify(values)
			},
			openerCt: gridPanel
		});
	},

	editor: {
		detailPageClass: 'Clifton.compliance.rule.ComplianceRuleRunDetailWindow',
		drillDownOnly: true
	},
	getAdditionalLoadParams: function(firstLoad) {
		return {};
	},
	getLoadParams: function(firstLoad) {
		let dateValue;
		const t = this.getTopToolbar();
		const params = this.getAdditionalLoadParams(firstLoad);
		if (this.includeRunDateFilter) {
			const rd = TCG.getChildByName(t, 'runDate');
			const dd = this.getWindow().defaultData;
			if (firstLoad && dd && dd.viewName) {
				this.switchToView(dd.viewName, true);
			}
			if (firstLoad && dd && dd.runDate) { // runDate can be defaulted by the caller of window open
				dateValue = dd.runDate.format ? dd.runDate.format('m/d/Y') : dd.runDate;
				rd.setValue(dateValue);
			}
			else if (!rd.getValue()) {
				dateValue = Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y');
				rd.setValue(dateValue);
			}
			else {
				dateValue = rd.getValue().format('m/d/Y');
			}
			params.runDate = dateValue;
		}

		const o = TCG.getChildByName(t, 'clientAccount');
		if (o) {
			params.clientInvestmentAccountId = o.getValue();
		}

		const status = TCG.getChildByName(t, 'status');
		let statusFilter = null;
		let v = status.getValue();
		if (v === 'FAILED') {
			statusFilter = 0;
		}
		else if (v === 'IGNORED') {
			statusFilter = 2;
		}
		else if (v === 'FAILED_REVIEWED') {
			statusFilter = 3;
		}
		else {
			if (firstLoad) {
				v = this.defaultStatusFilterValue;
			}
			if (v === 'NOT_PASSED') {
				statusFilter = [0, 2, 3];
			}
			else {
				v = 'ALL';
			}
			status.setValue(v);
		}
		if (TCG.isNull(statusFilter)) {
			this.clearFilter('status', true);
		}
		else {
			this.setFilterValue('status', statusFilter, false, true);
		}

		return params;
	}
});
Ext.reg('compliance-run-grid', Clifton.compliance.RunGrid);

Clifton.compliance.RuleRunStatusColumn = Ext.extend(Ext.grid.Column, {
		header: 'Pass', dataIndex: 'status', width: 25, align: 'center',
		filter: {type: 'list', matchTextInLocalMode: false, options: [[1, 'Pass'], [2, 'Ignored (Failed)'], [0, 'Fail'], [3, 'Fail (Reviewed)']]},
		renderer: function(v) {
			let success = 'Failure (Pending Review)';
			let imageIcon = 'cancel.png';

			if (v === 1) {
				success = 'Success';
				imageIcon = 'accept.png';
			}
			else if (v === 2) {
				success = 'Ignored';
				imageIcon = 'ignored.png';
			}
			else if (v === 3) {
				success = 'Failure (Reviewed)';
				imageIcon = 'checked.png';
			}

			let r = '<div style="text-align:center;height:13px;overflow:visible;"><img style="vertical-align:-3px;" src="core/images/icons/';
			r += imageIcon + '" title="' + success + '" /></div>';

			return r;
		}
	}
);
Ext.grid.Column.types.compliancerunstatuscolumn = Clifton.compliance.RuleRunStatusColumn;

Clifton.compliance.rule.AssignmentGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'complianceRuleAssignmentListFind',
	additionalPropertiesToRequest: 'rule.name|rule.id|clientInvestmentAccount.id|id|businessService.id',
	defaultView: undefined,
	selectionRequired: true,
	previewEnabled: true,
	viewNames: ['Default', 'Scoped'],
	instructions: 'A list of compliance rule assignments defined in the system. Rules can be assigned to client accounts, to business services, or globally.',
	columns: [
		{header: 'ID', dataIndex: 'id', hidden: true},
		{header: 'Scope', dataIndex: 'scopeLabel', width: 150, viewNames: ['Default'], defaultSortColumn: true},
		{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 150, hidden: true, filter: {type: 'combo', searchFieldName: 'accountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Business Service', dataIndex: 'businessService.label', width: 150, hidden: true, filter: {type: 'combo', searchFieldName: 'businessServiceId', displayField: 'label', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT'}},
		{header: 'Workflow State', dataIndex: 'clientInvestmentAccount.workflowState.name', width: 50, viewNames: ['Default'], qtip: 'Workflow Stats of the Client Account'},
		{header: 'Rule Type', dataIndex: 'rule.ruleType.name', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'ruleTypeId', url: 'complianceRuleTypeListFind.json'}, viewNames: ['Scoped']},
		{
			header: 'Compliance Rule', dataIndex: 'rule.label', width: 230, filter: {type: 'combo', searchFieldName: 'ruleId', displayField: 'label', url: 'ruleListFind.json'}, hidden: true, viewNames: ['Scoped'],
			renderer: function(v, metaData, r) {
				const description = r.data['rule.description'];
				if (TCG.isNotBlank(description)) {
					metaData.attr = TCG.renderQtip(description);
				}

				const note = r.data.note;
				if (TCG.isNotBlank(note)) {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Rule Description', dataIndex: 'rule.description', width: 200, hidden: true},
		{header: 'Sub Account Purpose', dataIndex: 'subAccountPurpose.name', width: 120, hidden: true, filter: {type: 'combo', searchFieldName: 'subAccountPurposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
		{header: 'Subsidiary Purpose', dataIndex: 'subsidiaryPurpose.name', width: 120, hidden: true, filter: {type: 'combo', searchFieldName: 'subsidiaryPurposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
		{header: 'Service Assignment', dataIndex: 'businessServiceAssignment', width: 50, type: 'boolean', filter: {searchFieldName: 'businessServiceNotNull'}},
		{header: 'Global Assignment', dataIndex: 'global', width: 50, type: 'boolean'},
		{header: 'Exclusion Assignment', dataIndex: 'exclusionAssignment', width: 55, type: 'boolean', qtip: 'Specifies whether this rule is excluded (should NOT be evaluated) from rule evaluation for the corresponding scope.'},
		{header: 'Start Date', dataIndex: 'startDate', width: 50, viewNames: ['Default']},
		{header: 'End Date', dataIndex: 'endDate', width: 50, viewNames: ['Default']},
		{header: 'Note', dataIndex: 'note', width: 180, hidden: true}
	],
	listeners: {
		afterrender: function(fp, isClosing) {
			if (this.defaultView) {
				this.setDefaultView(this.defaultView);
			}
		}
	},
	getLoadParams: function(firstLoad) {
		const dd = this.getWindow().defaultData;
		return {
			ruleId: dd.ruleId,
			clientInvestmentAccountId: dd.clientInvestmentAccountId,
			...this.getTopToolbarFilterLoadParams()
		};
	},
	getTopToolbarFilterLoadParams: function() {
		const processDateField = TCG.getChildByName(this.getTopToolbar(), 'processDate');
		return {activeOnDate: (processDateField && processDateField.getValue() && processDateField.getValue().format('m/d/Y')) || null};
	},
	getEditorDefaultData: function(gridPanel, row) {
		const data = this.getWindow().defaultData;
		const defaults = {};
		if (data && data.businessClient) {
			// Investment account window
			defaults.clientInvestmentAccount = {};
			defaults.clientInvestmentAccount.id = data.id;
			defaults.clientInvestmentAccount.label = data.label;
		}
		else if (gridPanel.getWindow().getMainForm().formValues !== undefined) {
			const values = gridPanel.getWindow().getMainForm().formValues;
			defaults.rule = {};
			defaults.rule.id = values.id;
			defaults.rule.name = values.name;
			defaults.rule.label = values.label;
		}
		return defaults;
	},
	editor: {
		detailPageClass: {
			getItemText: function(rowItem) {
				return 'Standard Assignment';
			},
			items: [
				{text: 'Standard Assignment', iconCls: 'verify', className: 'Clifton.compliance.rule.ComplianceRuleAssignmentWindow'},
				{text: 'Bulk Assignments', iconCls: 'hierarchy', className: 'Clifton.compliance.rule.BulkComplianceRuleAssignmentWindow'},
				{text: 'Copy Assignments', iconCls: 'copy', className: 'Clifton.compliance.rule.CopyComplianceRuleAssignmentWindow'}
			]
		},
		getDefaultData: (gridPanel, row) => gridPanel.getEditorDefaultData(gridPanel, row)
	},
	getTopToolbarFilters: function(tb) {
		const filters = [];
		// Preview elements only if preview is enabled
		if (this.previewEnabled) {
			let buttonText;
			let subButtonText;
			let buttonToolTip;
			if (this.selectionRequired) {
				buttonText = 'Preview Selected';
				subButtonText = 'Preview All';
				buttonToolTip = 'Preview rule run for selected account. Rule run is not saved.';
			}
			else {
				buttonText = 'Preview All';
				subButtonText = 'Preview Selected';
				buttonToolTip = 'Preview rule run ALL rules for this account. Rule run is not saved.';
			}
			filters.push({
				text: buttonText,
				xtype: 'splitbutton',
				tooltip: buttonToolTip,
				iconCls: 'preview',
				labelSeparator: '',
				selectionRequired: this.selectionRequired,
				handler: btn => {
					const sm = this.grid.getSelectionModel();
					const previewAll = btn.getText() === 'Preview All';
					// Validation
					if (sm.getCount() === 0 && (this.selectionRequired || !previewAll)) {
						TCG.showError('Please select a row to be previewed.', 'No Row(s) Selected');
						return;
					}

					if (!previewAll) {
						if (sm.getCount() > 1) {
							TCG.showError('Multi-selection previews are not supported. Please select a row.', 'NOT SUPPORTED');
							return;
						}
						if (sm.hasSelection() && sm.getSelected().json.businessService) {
							TCG.showError('Previews are not supported for business service assignments.', 'NOT SUPPORTED');
							return;
						}
					}

					// Get data for preview
					const previewData = {};
					previewData.processDate = (TCG.getChildByName(this.getTopToolbar(), 'processDate').getValue() || Clifton.calendar.getBusinessDayFrom(0)).format('m/d/Y');

					if (previewAll) {
						// Global preview
						previewData.ruleName = 'Global';
						if (this.selectionRequired) {
							const selectedAssignment = sm.getSelected().json;
							previewData.businessServiceId = selectedAssignment.businessService && selectedAssignment.businessService.id;
							previewData.clientAccountId = selectedAssignment.clientInvestmentAccount && selectedAssignment.clientInvestmentAccount.id;
							previewData.clientInvestmentLabel = sm.getSelected().get('clientInvestmentAccount.label');
						}
						else {
							previewData.clientInvestmentLabel = this.getWindow().getMainForm().formValues.label;
							previewData.clientAccountId = this.getWindow().params.id;
						}
					}
					else {
						// Single assignment preview
						const selectedAssignment = sm.getSelected().json;
						previewData.ruleName = selectedAssignment.rule.name;
						previewData.businessServiceId = selectedAssignment.businessService && selectedAssignment.businessService.id;
						previewData.clientAccountId = selectedAssignment.clientInvestmentAccount && selectedAssignment.clientInvestmentAccount.id;
						previewData.clientInvestmentLabel = sm.getSelected().get('clientInvestmentAccount.label');
						previewData.complianceRuleId = selectedAssignment.rule.id;
					}

					TCG.createComponent('Clifton.compliance.rule.ComplianceRuleRunPreviewWindow', {
						defaultData: previewData,
						openerCt: this
					});
				},
				menu: {
					items: [{
						text: subButtonText,
						tooltip: 'Toggles the preview button between Preview All functionality and Preview Selected functionality',
						iconCls: 'preview',
						handler: btn => {
							const splitButton = btn.findParentByType('splitbutton');
							if (splitButton.getText() === 'Preview All') {
								btn.setText('Preview All');
								splitButton.setText('Preview Selected');
								if (splitButton.selectionRequired) {
									splitButton.setTooltip('Preview rule run for selected account on the selected date. Rule run is not saved.');
								}
								else {
									splitButton.setTooltip('Preview rule run selected rule for this account on the selected date. Rule run is not saved.');
								}
							}
							else {
								btn.setText('Preview Selected');
								splitButton.setText('Preview All');
								if (splitButton.selectionRequired) {
									splitButton.setTooltip('Preview rule run ALL rules for the selected account on the selected date. Rule run is not saved.');
								}
								else {
									splitButton.setTooltip('Preview rule run ALL rules for this account on the selected date. Rule run is not saved.');
								}
							}
						}
					}]
				}
			});
		}
		filters.push({xtype: 'toolbar-datefield', fieldLabel: 'Active On Date', name: 'processDate', width: 90});
		return filters;
	}
});
Ext.reg('compliance-assignment-grid', Clifton.compliance.rule.AssignmentGrid);

Clifton.compliance.RunsRebuildForm = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	validatedOnLoad: false,
	listeners: {
		afterrender: function() {
			this.setFormValue('processDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
		}
	},
	initComponent: function() {
		this.items = [...this.rebuildItems];
		Clifton.compliance.RunsRebuildForm.superclass.initComponent.apply(this, arguments);
	},
	rebuildItems: [{
		xtype: 'fieldset',
		title: 'Run Configuration',
		buttonAlign: 'right',
		items: [{
			xtype: 'panel',
			layout: 'column',
			items: [{
				columnWidth: .69,
				items: [{
					xtype: 'formfragment',
					frame: false,
					labelWidth: 200,
					items: [{
						fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', mutuallyExclusiveFields: ['businessServiceLabel', 'groupName', 'excludeGroupName']
					}, {
						fieldLabel: 'Business Service', name: 'businessServiceLabel', hiddenName: 'businessServiceId', xtype: 'combo', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', detailPageClass: 'Clifton.business.service.ServiceWindow', mutuallyExclusiveFields: ['accountLabel']
					}, {
						fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'
					}, {
						fieldLabel: 'Exclude Client Account Group', name: 'excludeGroupName', hiddenName: 'exclusionInvestmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'
					}, {
						fieldLabel: 'Compliance Rule', name: 'rule.name', hiddenName: 'complianceRuleId', displayField: 'label', xtype: 'combo', url: 'complianceRuleListFind.json', detailPageClass: 'Clifton.compliance.rule.ComplianceRuleWindow'
					}]
				}]
			}, {
				columnWidth: .01,
				items: [{xtype: 'label', html: '&nbsp;'}]
			}, {
				columnWidth: .29,
				items: [{
					xtype: 'formfragment',
					labelWidth: 80,
					frame: false,
					items: [
						{fieldLabel: 'Run Date', name: 'processDate', xtype: 'datefield', allowBlank: false},
						{boxLabel: 'Generate Details for Success', name: 'generateDetailsForSuccess', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Generate Sub Details', name: 'generateSubDetails', xtype: 'checkbox', labelSeparator: ''},
						{boxLabel: 'Preview Only', name: 'previewOnly', xtype: 'checkbox', labelSeparator: ''}
					]
				}]
			}]
		}],
		buttons: [{
			text: 'Run Rules',
			iconCls: 'run',
			width: 150,
			handler: function() {
				const owner = this.findParentByType('formpanel');
				const form = owner.getForm();
				Ext.Msg.confirm('Generate Rule Runs', 'WARNING: This functionality should be used very rarely. Make sure to review selected filters and options carefully. Avoid generation of details and sub-details for success (you can always see them using Preview). Would you like to start Rule Run generation?', function(a) {
					if (a === 'yes') {
						form.submit(Ext.applyIf({
							url: 'complianceRunRebuild.json',
							waitMsg: 'Rebuilding...',
							success: function(form, action) {
								Ext.Msg.alert('Processing Started', action.result.data.message, function() {
									const grid = owner.getComponent('complianceRunnerGrid');
									grid.reload.defer(300, grid);
								});
							}
						}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
					}
				});
			}
		}]
	}, {
		xtype: 'core-scheduled-runner-grid',
		itemId: 'complianceRunnerGrid',
		typeName: 'COMPLIANCE-RUN',
		instantRunner: true,
		title: 'Current Processing',
		instructions: 'A list of compliance rule runs that are currently being executed.',
		height: 250
	}]
});
Ext.reg('compliance-runs-rebuild-form', Clifton.compliance.RunsRebuildForm);

Clifton.compliance.creditrating.CreditRatingValueGridPanel = Ext.extend(TCG.grid.GridPanel, {
	// Configuration parameters
	forSecurity: false,
	forIssuer: false,

	name: 'complianceCreditRatingValueListFind',
	instructions: 'Different credit rating agencies use different rating schemes and also have separate sets of ratings for short-term and long-term assets.  Credit Ratings can be either Issue (Security) or Issuer specific. Compliance credit rating values may change over time.',
	wikiPage: 'IT/Compliance+-+Credit+Ratings',
	unfilteredColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Security or Issuer Company', width: 100, dataIndex: 'assigneeLabel', filter: {searchFieldName: 'assignee'}},
		{header: 'Security', width: 100, dataIndex: 'security.label', filter: {searchFieldName: 'securityName'}, hidden: true},
		{header: 'Investment Hierarchy', width: 200, dataIndex: 'security.instrument.hierarchy.labelExpanded', hidden: true, filter: false, sortable: false},
		{header: 'Issuer Company', width: 100, dataIndex: 'issuerCompany.label', filter: {searchFieldName: 'issuerCompanyName'}, hidden: true},
		{header: 'Agency', width: 100, dataIndex: 'creditRating.ratingAgency.name', filter: {searchFieldName: 'agencyName'}},
		{header: 'Rating Type', width: 40, dataIndex: 'type.label', filter: {type: 'combo', searchFieldName: 'typeId', sortFieldName: 'typeName', displayField: 'label', url: 'complianceCreditRatingTypeListFind.json'}},
		{header: 'Short-Term', width: 50, dataIndex: 'creditRating.shortTerm', type: 'boolean', filter: {searchFieldName: 'shortTerm'}},
		{header: 'Rating', width: 50, dataIndex: 'creditRating.name', filter: {searchFieldName: 'creditRatingName'}},
		{header: 'Outlook', width: 50, dataIndex: 'outlook.name', filter: {searchFieldName: 'creditRatingName'}},
		{header: 'Start', width: 40, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'End', width: 40, dataIndex: 'endDate'}
	],

	getTopToolbarFilters: function(toolbar) {
		const toolbarFilters = [{fieldLabel: 'Active On', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
		if (!this.forSecurity && !this.forIssuer) {
			this.topToolbarSearchParameter = 'searchPattern';
			toolbarFilters.push({
				fieldLabel: 'Search',
				width: 150,
				xtype: 'toolbar-textfield',
				name: 'searchPattern'
			});
		}
		return toolbarFilters;
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const dateField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (firstLoad) {
			// default to today
			if (TCG.isBlank(dateField.getValue())) {
				dateField.setValue((new Date()).format('m/d/Y'));
			}
		}
		const params = {};
		if (TCG.isNotBlank(dateField.getValue())) {
			params.activeOnDate = (dateField.getValue()).format('m/d/Y');
		}
		return params;
	},

	editor: {
		detailPageClass: 'Clifton.compliance.creditrating.CreditRatingValueWindow',
		allowToDeleteMultiple: true,
		addToolbarAddButton: function(toolbar, gridPanel) {
			toolbar.add.apply(toolbar, gridPanel.getToolbarAddButtonElements(toolbar, gridPanel));
		},

		getDefaultData: function(gridPanel, row, detailPageClass, itemText) {
			return gridPanel.getNewItemDefaultData(this, row, detailPageClass, itemText);
		}
	},

	initComponent: function() {
		// Apply column customizations
		const columns = [];
		const gridPanel = this;
		gridPanel.unfilteredColumns.forEach(function(col) {
			// Exclude specified columns when grid is security- or issuer-specific
			if (!(gridPanel.forSecurity && ['assigneeLabel', 'issuerCompany.label'].indexOf(col.dataIndex) > -1)
				&& !(gridPanel.forIssuer && ['assigneeLabel', 'security.label'].indexOf(col.dataIndex) > -1)) {
				columns.push(col);
			}
		});
		gridPanel.columns = columns;
		this.constructor.superclass.initComponent.apply(this, arguments);
	},

	/**
	 * Gets the list of button elements to use in place of the "Add" button.
	 */
	getToolbarAddButtonElements: function(toolbar, gridPanel) {
		// Override the add-button function to place a definition dropdown to the left of the "Add" button
		return [
			{
				name: 'type', xtype: 'combo', tooltip: 'Credit Rating Type',
				url: 'complianceCreditRatingTypeListFind.json', limitRequestedProperties: false,
				detailPageClass: 'Clifton.compliance.creditrating.CreditRatingTypeWindow', disableAddNewItem: true,
				width: 120, listWidth: 120, maxHeight: 550, hidden: true, readOnly: true, doNotSubmitValue: true,
				// Include data, such as label, in loaded entities
				loadAll: true,
				listeners: {
					afterrender: function() {
						if (!gridPanel.forSecurity) {
							this.setVisible(true);
							this.setReadOnly(false);
						}
						else {
							// Automatically set type when looking at security credit ratings
							const issueType = TCG.data.getData('complianceCreditRatingTypeByName.json?name=Issue', gridPanel, 'compliance.creditrating.type.name.Issue');
							this.getStore().loadData({data: issueType});
							this.setValue(issueType.id);
						}
					}
				},
				initComponent: function() {
					this.constructor.prototype.initComponent.apply(this, arguments);
					// Set filter for security or non-security type
					if (gridPanel.forSecurity) {
						this.getStore().setBaseParam('securityRating', true);
					}
					else if (gridPanel.forIssuer) {
						this.getStore().setBaseParam('securityRating', false);
					}
					else if (TCG.isNotNull(this.getStore().baseParams)) {
						delete this.getStore().baseParams['securityRating'];
					}
				}
			}, ' ', {
				text: 'Add', tooltip: 'Create a new Credit Rating Value for selected Credit Rating Type',
				iconCls: 'add',
				handler: function() {
					const typeId = TCG.getChildByName(toolbar, 'type').getValue();
					if (typeId === '') {
						if (this.forSecurity) {
							// For security credit ratings, the type should already have been selected
							TCG.showError('An error occurred while attempting to determine the Credit Rating Type. Please close the dialog and try again.');
						}
						else {
							TCG.showError('You must first select a Credit Rating Type from the list.');
						}
					}
					else {
						const editor = gridPanel.editor;
						editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
					}
				}
			}, '-'
		];
	},

	/**
	 * Gets the data object containing defaults with which to populate the new entity dialog, or <tt>null</tt> if no such defaults exist.
	 */
	getNewItemDefaultData: function(editor, row, detailPageClass, itemText) {
		let defaultData = null;
		if (TCG.isNull(row)) {
			// Apply type
			const type = TCG.getChildByName(this.getTopToolbar(), 'type').getValueObject();
			defaultData = {'type': type};
		}
		return defaultData;
	}
});

/**
 * The grid panel for compliance credit ratings.
 */
Ext.reg('compliance-credit-rating-value-grid', Clifton.compliance.creditrating.CreditRatingValueGridPanel);

