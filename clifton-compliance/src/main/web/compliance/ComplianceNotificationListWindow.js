Clifton.compliance.ComplianceNotificationListWindow = Ext.extend(TCG.app.Window, {
	id: 'complianceNotificationWindow',
	title: 'Compliance Notifications',
	iconCls: 'flag-red',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'notification-grid',
		instructions: 'The following compliance notifications exist in the system. View as allows you to view notifications that others see as long as you have Full Control access to notifications.',
		getLoadParams: function(firstLoad) {
			if (firstLoad) {
				// default to not acknowledged
				this.setFilterValue('acknowledged', false);
			}

			const t = this.getTopToolbar();
			const u = TCG.getChildByName(t, 'userId');
			if (TCG.isNotBlank(u.getValue())) {
				return {userId: u.getValue(), typeName: 'Compliance'};
			}
			return {typeName: 'Compliance'};
		}
	}]
});
