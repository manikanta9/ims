Clifton.compliance.ComplianceRuleOldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Compliance Rule',
	iconCls: 'verify',
	width: 735,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'complianceRuleOld.json',
		labelFieldName: 'id',
		labelWidth: 200,
		listeners: {
			afterrender: function() {
				const win = this.getWindow();
				if (win.defaultData && win.defaultData.typeName) {
					const type = TCG.data.getData('complianceRuleTypeOldByName.json?typeName=' + win.defaultData.typeName, this);
					if (type) {
						const f = this.getForm();
						if (!type.limitRule) {
							this.remove(f.findField('maxBuyQuantity'), true);
							this.remove(f.findField('maxLongPositionQuantity'), true);
							this.remove(f.findField('maxSellQuantity'), true);
							this.remove(f.findField('maxShortPositionQuantity'), true);

							this.remove(f.findField('maxBuyAccountingNotional'), true);
							this.remove(f.findField('maxLongPositionAccountingNotional'), true);
							this.remove(f.findField('maxSellAccountingNotional'), true);
							this.remove(f.findField('maxShortPositionAccountingNotional'), true);
						}

						if (!type.shortLongRule) {
							this.remove(f.findField('shortPositionAllowed'), true);
							this.remove(f.findField('longPositionAllowed'), true);
						}

						if (!type.investmentAccountAllowed) {
							this.remove(f.findField('clientInvestmentAccount.label'), true);
							this.remove(f.findField('holdingInvestmentAccount.label'), true);
						}
						if (type.investmentAccountRequired) {
							f.findField('clientInvestmentAccount.label').allowBlank = false;
						}

						if (!type.investmentSpecific) {
							this.remove(f.findField('investmentHierarchy.labelExpanded'), true);
							this.remove(f.findField('investmentInstrument.name'), true);
							this.remove(f.findField('investmentType.name'), true);
							this.remove(f.findField('useSettlementDate'), true);
						}

						if (!type.traderSpecific) {
							this.remove(f.findField('trader.label'), true);
						}
						else {
							f.findField('trader.label').allowBlank = false;
						}

						if (!type.ignorableAllowed) {
							this.remove(f.findField('ignorable'), true);
						}

						if (!type.useSettlementDateAllowed) {
							this.remove(f.findField('useSettlementDate'), true);
						}

						this.doLayout();
					}
				}
			}
		},
		getDefaultData: function(win) {
			let dd = win.defaultData || {};
			if (win.defaultData && win.defaultData.typeName) {
				const type = TCG.data.getData('complianceRuleTypeOldByName.json?typeName=' + win.defaultData.typeName, this);
				dd = Ext.apply({
					ruleType: type,
					shortPositionAllowed: true,
					longPositionAllowed: true
				}, dd);
			}
			return dd;
		},
		items: [
			{fieldLabel: 'Rule Type', name: 'ruleType.name', detailIdField: 'ruleType.id', xtype: 'linkfield', detailPageClass: 'Clifton.compliance.ComplianceRuleTypeOldWindow'},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Trader', name: 'trader.label', hiddenName: 'trader.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json'},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', mutuallyExclusiveFields: ['holdingInvestmentAccount.id'], detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', mutuallyExclusiveFields: ['clientInvestmentAccount.id'], detailPageClass: 'Clifton.investment.account.AccountWindow'},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', xtype: 'combo', url: 'investmentTypeList.json', loadAll: true, mutuallyExclusiveFields: ['investmentHierarchy.id', 'investmentInstrument.id']},
			{
				fieldLabel: 'Investment Hierarchy',
				name: 'investmentHierarchy.labelExpanded',
				displayField: 'labelExpanded',
				hiddenName: 'investmentHierarchy.id',
				queryParam: 'labelExpanded', listWidth: 600,
				xtype: 'combo',
				detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
				url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true',
				mutuallyExclusiveFields: ['investmentType.id', 'investmentInstrument.id']
			}, {
				fieldLabel: 'Investment Instrument',
				name: 'investmentInstrument.label',
				displayField: 'label',
				hiddenName: 'investmentInstrument.id',
				xtype: 'combo',
				detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
				url: 'investmentInstrumentListFind.json',
				mutuallyExclusiveFields: ['investmentHierarchy.id', 'investmentType.id']
			},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: '', name: 'useSettlementDate', boxLabel: 'Use settlement date for position lookup', xtype: 'checkbox'},

			{boxLabel: 'Allow Short Positions', name: 'shortPositionAllowed', xtype: 'checkbox', value: true, labelSeparator: ''},
			{boxLabel: 'Allow Long Positions', name: 'longPositionAllowed', xtype: 'checkbox', value: true, labelSeparator: ''},
			{boxLabel: 'Allow warnings for this rule to be ignorable', name: 'ignorable', xtype: 'checkbox', labelSeparator: ''},

			{fieldLabel: 'Max Buy Quantity', name: 'maxBuyQuantity', xtype: 'floatfield'},
			{fieldLabel: 'Max Long Position Quantity', name: 'maxLongPositionQuantity', xtype: 'floatfield'},
			{fieldLabel: 'Max Sell Quantity', name: 'maxSellQuantity', xtype: 'floatfield'},
			{fieldLabel: 'Max Short Position Quantity', name: 'maxShortPositionQuantity', xtype: 'floatfield'},

			{fieldLabel: 'Max Buy Notional', name: 'maxBuyAccountingNotional', xtype: 'floatfield'},
			{fieldLabel: 'Max Long Position Notional', name: 'maxLongPositionAccountingNotional', xtype: 'floatfield'},
			{fieldLabel: 'Max Sell Notional', name: 'maxSellAccountingNotional', xtype: 'floatfield'},
			{fieldLabel: 'Max Short Position Notional', name: 'maxShortPositionAccountingNotional', xtype: 'floatfield'}
		]
	}]
});
