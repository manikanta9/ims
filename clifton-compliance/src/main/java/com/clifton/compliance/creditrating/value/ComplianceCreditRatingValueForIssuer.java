package com.clifton.compliance.creditrating.value;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntityWithoutLabel;

import javax.persistence.Table;
import java.io.Serializable;


/**
 * The {@code ComplianceCreditRatingValueForIssuer} class represents credit ratings for issuer companies.
 *
 * @see ComplianceCreditRatingValue
 */
@Table(name = "ComplianceCreditRatingValue")
public class ComplianceCreditRatingValueForIssuer extends ComplianceCreditRatingValue {

	private BusinessCompany issuerCompany;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NamedEntityWithoutLabel<? extends Serializable> getAssignee() {
		return getIssuerCompany();
	}


	@Override
	public String getAssigneeSymbol() {
		return getIssuerCompany() != null ? getIssuerCompany().getName() : null;
	}


	public BusinessCompany getIssuerCompany() {
		return this.issuerCompany;
	}


	public void setIssuerCompany(BusinessCompany issuerCompany) {
		this.issuerCompany = issuerCompany;
	}
}
