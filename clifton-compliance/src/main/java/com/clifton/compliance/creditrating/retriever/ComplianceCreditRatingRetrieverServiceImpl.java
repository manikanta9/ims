package com.clifton.compliance.creditrating.retriever;


import com.clifton.business.company.BusinessCompanyService;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.ComplianceCreditRatingSearchForm;
import com.clifton.compliance.creditrating.ComplianceCreditRatingService;
import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlook;
import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlookSearchForm;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingType;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingTypes;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.system.schema.SystemDataType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * The <code>ComplianceCreditRatingRetrieverServiceImpl</code> retrieves latest credit ratings from bloomberg
 *
 * @author apopp
 */
@Service
public class ComplianceCreditRatingRetrieverServiceImpl implements ComplianceCreditRatingRetrieverService {

	private static final String MOODY_NAME = "Moody's";
	private static final String MOODY_RATING_FIELD = "RTG_MOODY";
	private static final String MOODY_EFFECTIVE_DATE_FIELD = "MOODY_EFF_DT";

	private static final String SP_NAME = "Standard & Poor's";
	private static final String SP_RATING_FIELD = "RTG_SP";
	private static final String SP_EFFECTIVE_DATE_FIELD = "SP_EFF_DT";

	private static final String FITCH_NAME = "Fitch";
	private static final String FITCH_RATING_FIELD = "RTG_FITCH";
	private static final String FITCH_EFFECTIVE_DATE_FIELD = "FITCH_EFF_DT";

	private static final String OUTLOOK_DELIMITER = "/*";
	private static final String OUTLOOK_POSITIVE = "+";
	private static final String OUTLOOK_NEGATIVE = "-";

	private static final String COMPANY_TYPE = "Credit Rating Agency";

	////////////////////////////////////////////////////////////////////////////

	private InvestmentInstrumentService investmentInstrumentService;
	private ComplianceCreditRatingService complianceCreditRatingService;
	private BusinessCompanyService businessCompanyService;

	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer loadComplianceCreditRating(int securityId, String providerName) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "Cannot find investment security for id = " + securityId);

		return doLoadComplianceCreditRatingForSecurity(security, providerName);
	}


	private Integer doLoadComplianceCreditRatingForSecurity(InvestmentSecurity security, String providerName) {

		List<ComplianceCreditRatingValueForSecurity> currentCreditRatingList = getCurrentCreditRatingList(security);
		List<ComplianceCreditRatingValueForSecurity> latestCreditRatingList = getLatestCreditRatingList(security, providerName);

		//for each currently active credit rating, under this security, update it if it has expired (set end date to now)
		Iterator<ComplianceCreditRatingValueForSecurity> currentCreditRatingListIterator = currentCreditRatingList.iterator();
		while (currentCreditRatingListIterator.hasNext()) {
			ComplianceCreditRatingValueForSecurity currentRating = currentCreditRatingListIterator.next();

			ComplianceCreditRatingValueForSecurity latestIfOutDated = getLatestCreditRatingIfCurrentOutdated(currentRating, latestCreditRatingList);
			//if the current rating is not outdated then remove it from the current list
			// as it does not need to be updated
			if (latestIfOutDated == null) {
				currentCreditRatingListIterator.remove();
			}
			else {
				//Current rating is outdated therefore prepare it for update by setting it to inactive
				currentRating.setEndDate(DateUtils.clearTime(DateUtils.addDays(latestIfOutDated.getStartDate(), -1)));
			}
		}

		//At this point we know that currentCreditRatingList and latestCreditRatingList are prepared to be saved
		saveComplianceCreditRatingValueForSecurityList(currentCreditRatingList, latestCreditRatingList);

		return CollectionUtils.getSize(latestCreditRatingList);
	}


	/**
	 * Compares a currently active rating to a list of potentially new ratings.
	 * If the new list contains a replacement to the current rating then the current rating is outdated
	 * <p>
	 * If there is a replacement to the current but it is equal to the latest we will remove that
	 * latest value from the underlying latest list supplied as an argument, since they are equal.
	 */
	private ComplianceCreditRatingValueForSecurity getLatestCreditRatingIfCurrentOutdated(ComplianceCreditRatingValueForSecurity currentRating, List<ComplianceCreditRatingValueForSecurity> latestCreditRatingList) {
		Iterator<ComplianceCreditRatingValueForSecurity> latestCreditRatingListIterator = latestCreditRatingList.iterator();
		while (latestCreditRatingListIterator.hasNext()) {
			ComplianceCreditRatingValueForSecurity latestRating = latestCreditRatingListIterator.next();

			//Check if these two credit ratings are from the same agency
			if (currentRating.getCreditRating().getRatingAgency().equals(latestRating.getCreditRating().getRatingAgency())) {
				//if they are equal then current rating is not outdated, get rid of latest as it is the same as current
				if (isRatingValueEquivalent(currentRating, latestRating)) {
					latestCreditRatingListIterator.remove();
					return null;
				}

				return latestRating; //current is outdated, the latest has changed
			}
		}

		//if we get here that means we did not have any latest rating to compare to our current
		// therefore the current is not outdated
		return null;
	}


	/**
	 * Determines whether the given credit rating values are equivalent. Credit rating values are determined to be equivalent if they have the same credit
	 * rating and are of the same type.
	 *
	 * @param activeRating the active rating to test for equality
	 * @param latestRating the latest rating to which the active rating should be compared
	 * @return {@code true} if the given credit rating values are equivalent, or {@code false} otherwise.
	 */
	private boolean isRatingValueEquivalent(ComplianceCreditRatingValueForSecurity activeRating, ComplianceCreditRatingValueForSecurity latestRating) {
		return CompareUtils.isEqual(activeRating.getCreditRating(), latestRating.getCreditRating())
				&& CompareUtils.isEqual(activeRating.getType(), latestRating.getType());
	}


	private List<ComplianceCreditRatingValueForSecurity> getCurrentCreditRatingList(InvestmentSecurity security) {
		ComplianceCreditRatingValueSearchForm searchForm = new ComplianceCreditRatingValueSearchForm();
		searchForm.setActive(true);
		searchForm.setSecurityId(security.getId());
		return getComplianceCreditRatingService().getComplianceCreditRatingValueForSecurityList(searchForm);
	}


	private List<ComplianceCreditRatingValueForSecurity> getLatestCreditRatingList(InvestmentSecurity security, String providerName) {
		Map<String, MarketDataValueGeneric<Object>> dataValueList = queryProvider(security, createMarketDataFieldArray(), providerName);
		List<ComplianceCreditRatingValueForSecurity> latestRatingList = new ArrayList<>();
		latestRatingList.add(createCreditRatingForSecurity(security, dataValueList, MOODY_RATING_FIELD, MOODY_NAME, MOODY_EFFECTIVE_DATE_FIELD));
		latestRatingList.add(createCreditRatingForSecurity(security, dataValueList, FITCH_RATING_FIELD, FITCH_NAME, FITCH_EFFECTIVE_DATE_FIELD));
		latestRatingList.add(createCreditRatingForSecurity(security, dataValueList, SP_RATING_FIELD, SP_NAME, SP_EFFECTIVE_DATE_FIELD));
		latestRatingList.removeAll(Collections.singleton((ComplianceCreditRatingValueForSecurity) null)); //remove nulls for sanity check
		return latestRatingList;
	}


	private Map<String, MarketDataValueGeneric<Object>> queryProvider(InvestmentSecurity security, MarketDataField[] fieldArray, String providerName) {
		MarketDataSource dataSource = (providerName != null) ? getMarketDataSourceService().getMarketDataSourceByName(providerName) : null;
		MarketDataProvider<MarketDataValueGeneric<Object>> marketDataProvider = getMarketDataProviderLocator().locate(dataSource);

		MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(),
				getMarketDataSourceService().getMarketDataSourceByName(marketDataProvider.getMarketDataSourceName()));

		String pricingSource = mapping.getPricingSource() != null ? mapping.getPricingSource().getSymbol() : null;

		Date today = new Date();
		List<String> errors = new ArrayList<>();

		DataFieldValueCommand dataFieldValueCommand = DataFieldValueCommand.newBuilder()
				.addSecurity(security, new ArrayList<>(Arrays.asList(fieldArray)))
				.setStartDate(today)
				.setEndDate(today)
				.setMarketSector(mapping.getMarketSector().getName())
				.setExchangeCodeType(mapping.getExchangeCodeType())
				.setPricingSource(pricingSource)
				.setErrors(errors)
				.build();
		DataFieldValueResponse<MarketDataValueGeneric<Object>> response = marketDataProvider.getMarketDataValueLatest(dataFieldValueCommand);

		Map<String, MarketDataValueGeneric<Object>> values = new HashMap<>();

		Map<MarketDataField, MarketDataValueGeneric<Object>> fieldMap = response.getMarketDataValueMapForSecurity(security);
		fieldMap.forEach((field, value) -> values.put(field.getExternalFieldName(), value));

		return values;
	}


	private ComplianceCreditRatingValueForSecurity createCreditRatingForSecurity(InvestmentSecurity security, Map<String, MarketDataValueGeneric<Object>> dataValueList, String ratingField, String companyName, String effectiveDateField) {
		//not all securities support ratings
		if (!dataValueList.containsKey(ratingField)) {
			return null;
		}

		ValidationUtils.assertTrue(dataValueList.containsKey(effectiveDateField), "No effective date found for credit rating on security [" + security.getSymbol() + "]");

		ComplianceCreditRatingValueForSecurity creditRatingForSecurity = new ComplianceCreditRatingValueForSecurity();
		creditRatingForSecurity.setSecurity(security);
		ComplianceCreditRatingType creditRatingIssuerType = getComplianceCreditRatingService().getComplianceCreditRatingTypeByName(ComplianceCreditRatingTypes.ISSUE.getName());
		creditRatingForSecurity.setType(creditRatingIssuerType);

		ObjectToStringConverter dateConverter = new ObjectToStringConverter();
		String effectiveDate = dateConverter.convert(dataValueList.get(effectiveDateField).getMeasureValue());
		creditRatingForSecurity.setStartDate(DateUtils.toDate(effectiveDate));

		Integer agencyId = getBusinessCompanyService().getBusinessCompanyByTypeAndName(COMPANY_TYPE, companyName).getId();

		String ratingName = dataValueList.get(ratingField).getMeasureValue().toString();
		String outlook = (ratingName.contains(OUTLOOK_DELIMITER)) ? ratingName.split(Pattern.quote(OUTLOOK_DELIMITER))[1].trim() : null;
		ratingName = (outlook != null) ? ratingName.split(Pattern.quote(OUTLOOK_DELIMITER))[0].trim() : ratingName;

		// skip the following "invalid" ratings (is there a better way to handle this?)
		Set<String> skipRatings = new HashSet<>();
		skipRatings.add("NR");
		skipRatings.add("WR");
		skipRatings.add("WD");
		if (skipRatings.contains(ratingName)) {
			return null;
		}

		ComplianceCreditRatingSearchForm searchForm = new ComplianceCreditRatingSearchForm();
		searchForm.setRatingNameEqual(ratingName);
		searchForm.setRatingAgencyId(agencyId);
		searchForm.setShortTerm(false);

		List<ComplianceCreditRating> complianceCreditRatingList = getComplianceCreditRatingService().getComplianceCreditRatingList(searchForm);

		ValidationUtils.assertTrue(complianceCreditRatingList != null && complianceCreditRatingList.size() == 1, "Cannot find rating [" + ratingName + "] for Agency [" + companyName + "]. Security: "
				+ security.getSymbol());

		ComplianceCreditRating complianceCreditRating = CollectionUtils.getFirstElement(complianceCreditRatingList);

		if (outlook != null) {
			ComplianceCreditRatingOutlookSearchForm outlookSearchForm = new ComplianceCreditRatingOutlookSearchForm();
			outlookSearchForm.setRatingAgencyId(agencyId);

			if (outlook.contains(OUTLOOK_POSITIVE)) {
				outlookSearchForm.setSearchPattern("POSITIVE");
			}
			else if (outlook.contains(OUTLOOK_NEGATIVE)) {
				outlookSearchForm.setSearchPattern("NEGATIVE");
			}

			ValidationUtils.assertNotNull(outlookSearchForm.getSearchPattern(), " Mapping does not exist for outlook [" + outlook + "] with rating [" + ratingName + "], Agency [" + companyName
					+ "], while processing security symbol [" + security.getSymbol() + "]");

			ComplianceCreditRatingOutlook complianceCreditRatingOutlook = CollectionUtils.getOnlyElement(getComplianceCreditRatingService().getComplianceCreditRatingOutlookList(outlookSearchForm));
			creditRatingForSecurity.setOutlook(complianceCreditRatingOutlook);
		}
		creditRatingForSecurity.setCreditRating(complianceCreditRating);

		return creditRatingForSecurity;
	}


	private MarketDataField[] createMarketDataFieldArray() {
		return new MarketDataField[]{createDataField(MOODY_RATING_FIELD, "STRING"), createDataField(MOODY_EFFECTIVE_DATE_FIELD, "DATE"), createDataField(SP_RATING_FIELD, "STRING"),
				createDataField(SP_EFFECTIVE_DATE_FIELD, "DATE"), createDataField(FITCH_RATING_FIELD, "STRING"), createDataField(FITCH_EFFECTIVE_DATE_FIELD, "DATE")};
	}


	private MarketDataField createDataField(String field, String type) {
		MarketDataField dataField = new MarketDataField(field);
		SystemDataType stringDataType = new SystemDataType();
		stringDataType.setName(type);
		dataField.setDataType(stringDataType);
		dataField.setLatestValueOnly(true);
		return dataField;
	}


	@Transactional
	protected void saveComplianceCreditRatingValueForSecurityList(List<ComplianceCreditRatingValueForSecurity> currentCreditRatingList, List<ComplianceCreditRatingValueForSecurity> latestCreditRatingList) {
		getComplianceCreditRatingService().saveComplianceCreditRatingValueForSecurityList(currentCreditRatingList);
		getComplianceCreditRatingService().saveComplianceCreditRatingValueForSecurityList(latestCreditRatingList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public ComplianceCreditRatingService getComplianceCreditRatingService() {
		return this.complianceCreditRatingService;
	}


	public void setComplianceCreditRatingService(ComplianceCreditRatingService complianceCreditRatingService) {
		this.complianceCreditRatingService = complianceCreditRatingService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
