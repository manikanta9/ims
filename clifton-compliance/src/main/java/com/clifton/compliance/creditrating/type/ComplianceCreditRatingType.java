package com.clifton.compliance.creditrating.type;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The type of credit rating. One of each type of credit rating may be attributed to a security.
 */
@CacheByName
public class ComplianceCreditRatingType extends NamedEntityWithoutLabel<Short> {

	private boolean securityRating;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSecurityRating() {
		return this.securityRating;
	}


	public void setSecurityRating(boolean securityRating) {
		this.securityRating = securityRating;
	}
}
