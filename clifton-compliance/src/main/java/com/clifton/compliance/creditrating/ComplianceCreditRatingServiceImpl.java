package com.clifton.compliance.creditrating;


import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlook;
import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlookSearchForm;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingType;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingTypeSearchForm;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ComplianceCreditRatingServiceImpl</code> class provides basic implementation of the ComplianceCreditRatingService interface.
 *
 * @author vgomelsky
 */
@Service
public class ComplianceCreditRatingServiceImpl implements ComplianceCreditRatingService {

	private AdvancedUpdatableDAO<ComplianceCreditRating, Criteria> complianceCreditRatingDAO;
	private AdvancedUpdatableDAO<ComplianceCreditRatingOutlook, Criteria> complianceCreditRatingOutlookDAO;
	private AdvancedUpdatableDAO<ComplianceCreditRatingType, Criteria> complianceCreditRatingTypeDAO;
	private AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO;

	private DaoNamedEntityCache<ComplianceCreditRatingType> complianceCreditRatingTypeCache;
	private DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> complianceCreditRatingValueForSecurityCache;
	private DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> complianceCreditRatingValueForIssuerCache;


	////////////////////////////////////////////////////////////////////////////
	//////////          Compliance Credit Rating Methods            ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceCreditRating getComplianceCreditRating(int id) {
		return getComplianceCreditRatingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceCreditRating> getComplianceCreditRatingList(ComplianceCreditRatingSearchForm searchForm) {
		return getComplianceCreditRatingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ComplianceCreditRating saveComplianceCreditRating(ComplianceCreditRating bean) {
		if (bean.isShortTerm()) {
			AssertUtils.assertNull(bean.getEquivalentShortTermRating(), "Equivalent ratings may only be assigned to long term ratings.");
		}
		else {
			ComplianceCreditRating equivalentShortTermRating = bean.getEquivalentShortTermRating();
			if (equivalentShortTermRating != null) {
				AssertUtils.assertTrue(equivalentShortTermRating.isShortTerm(), "The equivalent rating for a long term rating, must be a short term rating.");
			}
		}
		return getComplianceCreditRatingDAO().save(bean);
	}


	@Override
	public void deleteComplianceCreditRating(int id) {
		getComplianceCreditRatingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////       Compliance Credit Rating Outlook Methods       ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceCreditRatingOutlook getComplianceCreditRatingOutlook(short id) {
		return getComplianceCreditRatingOutlookDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceCreditRatingOutlook> getComplianceCreditRatingOutlookList(ComplianceCreditRatingOutlookSearchForm searchForm) {
		return getComplianceCreditRatingOutlookDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ComplianceCreditRatingOutlook saveComplianceCreditRatingOutlook(ComplianceCreditRatingOutlook bean) {
		return getComplianceCreditRatingOutlookDAO().save(bean);
	}


	@Override
	public void deleteComplianceCreditRatingOutlook(short id) {
		getComplianceCreditRatingOutlookDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Compliance Credit Rating Type Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceCreditRatingType getComplianceCreditRatingType(short id) {
		return getComplianceCreditRatingTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ComplianceCreditRatingType getComplianceCreditRatingTypeByName(String name) {
		return getComplianceCreditRatingTypeCache().getBeanForKeyValueStrict(getComplianceCreditRatingTypeDAO(), name);
	}


	@Override
	public List<ComplianceCreditRatingType> getComplianceCreditRatingTypeList(ComplianceCreditRatingTypeSearchForm searchForm) {
		return getComplianceCreditRatingTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            General Credit Rating Value Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceCreditRatingValue getComplianceCreditRatingValue(int id) {
		return getComplianceCreditRatingValueDAO().findByPrimaryKey(id);
	}


	@Override
	public List<? extends ComplianceCreditRatingValue> getComplianceCreditRatingValueList(ComplianceCreditRatingValueSearchForm searchForm) {
		return getComplianceCreditRatingValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void deleteComplianceCreditRatingValue(int id) {
		getComplianceCreditRatingValueDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Credit Rating Value For Security Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceCreditRatingValueForSecurity> getComplianceCreditRatingValueForSecurityList(ComplianceCreditRatingValueSearchForm searchForm) {
		searchForm.setSecuritiesOnly(true);
		return getComplianceCreditRatingValueForSecurityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ComplianceCreditRatingValueForSecurity> getComplianceCreditRatingValueForSecuritiesForSecurity(int securityId) {
		@SuppressWarnings("unchecked")
		List<ComplianceCreditRatingValueForSecurity> creditRatingValueList = (List<ComplianceCreditRatingValueForSecurity>) (List<? extends ComplianceCreditRatingValue>)
				getComplianceCreditRatingValueForSecurityCache().getBeanListForKeyValue(getComplianceCreditRatingValueDAO(), securityId);
		return creditRatingValueList;
	}


	@Override
	@Transactional
	public void saveComplianceCreditRatingValueForSecurityList(List<ComplianceCreditRatingValueForSecurity> beanList) {
		for (ComplianceCreditRatingValueForSecurity complianceCreditRatingValueForSecurity : CollectionUtils.getIterable(beanList)) {
			saveComplianceCreditRatingValueForSecurity(complianceCreditRatingValueForSecurity);
		}
	}


	@Override
	@Transactional
	public void saveComplianceCreditRatingValueForSecurity(ComplianceCreditRatingValueForSecurity bean) {
		ComplianceCreditRatingValueSearchForm searchForm = new ComplianceCreditRatingValueSearchForm();
		searchForm.setSecurityId(bean.getSecurity().getId());
		searchForm.setAgencyId(bean.getCreditRating().getRatingAgency().getId());
		searchForm.setTypeId(bean.getType().getId());
		List<ComplianceCreditRatingValueForSecurity> existingRatings = getComplianceCreditRatingValueForSecurityList(searchForm);
		validateAndSaveCreditRatingValue(bean, existingRatings);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Credit Rating Value For Issuer Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceCreditRatingValueForIssuer> getComplianceCreditRatingValueForIssuerList(ComplianceCreditRatingValueSearchForm searchForm) {
		searchForm.setIssuersOnly(true);
		return getComplianceCreditRatingValueForIssuerDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ComplianceCreditRatingValueForIssuer> getComplianceCreditRatingValueForIssuersForIssuer(int companyId) {
		@SuppressWarnings("unchecked")
		List<ComplianceCreditRatingValueForIssuer> creditRatingValueList = (List<ComplianceCreditRatingValueForIssuer>) (List<? extends ComplianceCreditRatingValue>)
				getComplianceCreditRatingValueForIssuerCache().getBeanListForKeyValue(getComplianceCreditRatingValueDAO(), companyId);
		return creditRatingValueList;
	}


	@Override
	@Transactional
	public void saveComplianceCreditRatingValueForIssuerList(List<ComplianceCreditRatingValueForIssuer> beanList) {
		for (ComplianceCreditRatingValueForIssuer complianceCreditRatingValueForIssuer : CollectionUtils.getIterable(beanList)) {
			saveComplianceCreditRatingValueForIssuer(complianceCreditRatingValueForIssuer);
		}
	}


	@Override
	@Transactional
	public void saveComplianceCreditRatingValueForIssuer(ComplianceCreditRatingValueForIssuer bean) {
		ComplianceCreditRatingValueSearchForm searchForm = new ComplianceCreditRatingValueSearchForm();
		searchForm.setIssuerCompanyId(bean.getIssuerCompany().getId());
		searchForm.setAgencyId(bean.getCreditRating().getRatingAgency().getId());
		searchForm.setTypeId(bean.getType().getId());
		List<ComplianceCreditRatingValueForIssuer> existingRatings = getComplianceCreditRatingValueForIssuerList(searchForm);
		validateAndSaveCreditRatingValue(bean, existingRatings);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates and saves the given bean, updating any existing beans.
	 * <p>
	 * This method validates that the given bean to save conforms to data integrity rules when compared to the existing matching ratings. Any rating in the
	 * given {@code existingRatings} list which does not have an End Date is updated with an appropriate End Date before the Start Date of bean to be saved.
	 *
	 * @param bean            the bean to be saved
	 * @param existingRatings the list of existing ratings which match the type and agency of the bean to be saved
	 */
	private void validateAndSaveCreditRatingValue(ComplianceCreditRatingValue bean, Collection<? extends ComplianceCreditRatingValue> existingRatings) {
		// Validate no overlapping dates
		CollectionUtils.getStream(existingRatings)
				.filter(rating -> !rating.equals(bean))
				.filter(rating -> rating.getEndDate() != null)
				.filter(rating -> DateUtils.isOverlapInDates(rating.getStartDate(), rating.getEndDate(), bean.getStartDate(), bean.getEndDate()))
				.findAny().ifPresent(rating -> {
			throw new ValidationException(String.format("Start and end dates cannot overlap with those of the given existing rating: [%s].", rating));
		});

		// Find at most one matching existing rating with no End Date
		List<ComplianceCreditRatingValue> ratingsToEnd = CollectionUtils.getStream(existingRatings)
				// Get existing non-finished rating (only one should exist)
				.filter(rating -> !rating.equals(bean))
				.filter(rating -> rating.getEndDate() == null)
				.collect(Collectors.toList());
		if (CollectionUtils.getSize(ratingsToEnd) > 1) {
			throw new ValidationException(String.format("Data integrity error: Multiple conflicting ratings with no End Date were found: %s.", CollectionUtils.toString(ratingsToEnd, 5)));
		}

		// End the matching existing rating with no End Date if one was found
		ComplianceCreditRatingValue beanToSave = bean;
		ComplianceCreditRatingValue ratingToEnd = CollectionUtils.getOnlyElement(ratingsToEnd);
		Date newEndDate = DateUtils.addDays(bean.getStartDate(), -1);
		if (ratingToEnd != null) {
			// Validate that the calculated End Date is later than the Start Date of the existing rating
			if (DateUtils.compare(ratingToEnd.getStartDate(), newEndDate, false) > 0) {
				throw new ValidationException(String.format("Unable to automatically end existing rating [%s]: Invalid End Date [%s]. The Start Date of the rating to be saved must be later than the Start Date of the existing rating ([%s]).",
						ratingToEnd, DateUtils.fromDateShort(newEndDate), DateUtils.fromDateShort(ratingToEnd.getStartDate())));
			}

			if (CoreCompareUtils.isEqual(ratingToEnd, bean, new String[]{"creditRating", "outlook"})) {
				// Equivalent rating found; functionally a duplicate of the rating to be saved, so no action necessary (touch row update-date only)
				beanToSave = ratingToEnd;
				ratingToEnd = null;
			}
			else {
				ratingToEnd.setEndDate(newEndDate);
			}
		}

		// Save changes
		if (ratingToEnd != null) {
			getComplianceCreditRatingValueDAO().save(ratingToEnd);
		}
		getComplianceCreditRatingValueDAO().save(beanToSave);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Computed Getter Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	private AdvancedUpdatableDAO<ComplianceCreditRatingValueForSecurity, Criteria> getComplianceCreditRatingValueForSecurityDAO() {
		@SuppressWarnings("unchecked")
		AdvancedUpdatableDAO<ComplianceCreditRatingValueForSecurity, Criteria> dao =
				(AdvancedUpdatableDAO<ComplianceCreditRatingValueForSecurity, Criteria>)
						(AdvancedUpdatableDAO<? extends ComplianceCreditRatingValue, Criteria>)
								getComplianceCreditRatingValueDAO();
		return dao;
	}


	private AdvancedUpdatableDAO<ComplianceCreditRatingValueForIssuer, Criteria> getComplianceCreditRatingValueForIssuerDAO() {
		@SuppressWarnings("unchecked")
		AdvancedUpdatableDAO<ComplianceCreditRatingValueForIssuer, Criteria> dao =
				(AdvancedUpdatableDAO<ComplianceCreditRatingValueForIssuer, Criteria>)
						(AdvancedUpdatableDAO<? extends ComplianceCreditRatingValue, Criteria>)
								getComplianceCreditRatingValueDAO();
		return dao;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ComplianceCreditRating, Criteria> getComplianceCreditRatingDAO() {
		return this.complianceCreditRatingDAO;
	}


	public void setComplianceCreditRatingDAO(AdvancedUpdatableDAO<ComplianceCreditRating, Criteria> complianceCreditRatingDAO) {
		this.complianceCreditRatingDAO = complianceCreditRatingDAO;
	}


	public AdvancedUpdatableDAO<ComplianceCreditRatingOutlook, Criteria> getComplianceCreditRatingOutlookDAO() {
		return this.complianceCreditRatingOutlookDAO;
	}


	public void setComplianceCreditRatingOutlookDAO(AdvancedUpdatableDAO<ComplianceCreditRatingOutlook, Criteria> complianceCreditRatingOutlookDAO) {
		this.complianceCreditRatingOutlookDAO = complianceCreditRatingOutlookDAO;
	}


	public AdvancedUpdatableDAO<ComplianceCreditRatingType, Criteria> getComplianceCreditRatingTypeDAO() {
		return this.complianceCreditRatingTypeDAO;
	}


	public void setComplianceCreditRatingTypeDAO(AdvancedUpdatableDAO<ComplianceCreditRatingType, Criteria> complianceCreditRatingTypeDAO) {
		this.complianceCreditRatingTypeDAO = complianceCreditRatingTypeDAO;
	}


	public AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> getComplianceCreditRatingValueDAO() {
		return this.complianceCreditRatingValueDAO;
	}


	public void setComplianceCreditRatingValueDAO(AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO) {
		this.complianceCreditRatingValueDAO = complianceCreditRatingValueDAO;
	}


	public DaoNamedEntityCache<ComplianceCreditRatingType> getComplianceCreditRatingTypeCache() {
		return this.complianceCreditRatingTypeCache;
	}


	public void setComplianceCreditRatingTypeCache(DaoNamedEntityCache<ComplianceCreditRatingType> complianceCreditRatingTypeCache) {
		this.complianceCreditRatingTypeCache = complianceCreditRatingTypeCache;
	}


	public DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> getComplianceCreditRatingValueForSecurityCache() {
		return this.complianceCreditRatingValueForSecurityCache;
	}


	public void setComplianceCreditRatingValueForSecurityCache(DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> complianceCreditRatingValueForSecurityCache) {
		this.complianceCreditRatingValueForSecurityCache = complianceCreditRatingValueForSecurityCache;
	}


	public DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> getComplianceCreditRatingValueForIssuerCache() {
		return this.complianceCreditRatingValueForIssuerCache;
	}


	public void setComplianceCreditRatingValueForIssuerCache(DaoSingleKeyListCache<ComplianceCreditRatingValue, Integer> complianceCreditRatingValueForIssuerCache) {
		this.complianceCreditRatingValueForIssuerCache = complianceCreditRatingValueForIssuerCache;
	}
}
