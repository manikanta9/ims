package com.clifton.compliance.creditrating;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>ComplianceCreditRatingSearchForm</code> class is a search definition for the ComplianceCreditRating objects.
 *
 * @author vgomelsky
 */
@SearchForm(ormDtoClass = ComplianceCreditRating.class)
public class ComplianceCreditRatingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField(searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String ratingNameEqual;

	@SearchField(searchField = "ratingAgency.id")
	private Integer ratingAgencyId;

	@SearchField(searchField = "name", searchFieldPath = "ratingAgency")
	private String ratingAgencyName;

	@SearchField
	private Integer ratingWeight;

	@SearchField(searchField = "name", searchFieldPath = "equivalentShortTermRating", comparisonConditions = {ComparisonConditions.EQUALS})
	private String equivalentShortTermRatingNameEqual;

	@SearchField
	private Boolean shortTerm;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getRatingAgencyId() {
		return this.ratingAgencyId;
	}


	public void setRatingAgencyId(Integer ratingAgencyId) {
		this.ratingAgencyId = ratingAgencyId;
	}


	public String getRatingAgencyName() {
		return this.ratingAgencyName;
	}


	public void setRatingAgencyName(String ratingAgencyName) {
		this.ratingAgencyName = ratingAgencyName;
	}


	public Integer getRatingWeight() {
		return this.ratingWeight;
	}


	public void setRatingWeight(Integer ratingWeight) {
		this.ratingWeight = ratingWeight;
	}


	public String getEquivalentShortTermRatingNameEqual() {
		return this.equivalentShortTermRatingNameEqual;
	}


	public void setEquivalentShortTermRatingNameEqual(String equivalentShortTermRatingNameEqual) {
		this.equivalentShortTermRatingNameEqual = equivalentShortTermRatingNameEqual;
	}


	public Boolean getShortTerm() {
		return this.shortTerm;
	}


	public void setShortTerm(Boolean shortTerm) {
		this.shortTerm = shortTerm;
	}


	public String getRatingNameEqual() {
		return this.ratingNameEqual;
	}


	public void setRatingNameEqual(String ratingNameEqual) {
		this.ratingNameEqual = ratingNameEqual;
	}
}
