package com.clifton.compliance.creditrating.value;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>ComplianceCreditRatingValueSearchForm</code> class is a search definition for {@link ComplianceCreditRatingValue} objects.
 */
public class ComplianceCreditRatingValueSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {


	@SearchField(searchField = "security.symbol,security.name,security.cusip,security.isin,security.sedol,security.occSymbol,issuerCompany.name,issuerCompany.alias", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "security.name,security.symbol,issuerCompany.name", leftJoin = true)
	private String assignee;

	@SearchField(searchField = "security.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean securitiesOnly;

	@SearchField(searchField = "issuerCompany.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean issuersOnly;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchField = "name", searchFieldPath = "security")
	private String securityName;

	@SearchField(searchField = "issuerCompany.id")
	private Integer issuerCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "issuerCompany")
	private String issuerCompanyName;

	@SearchField(searchField = "creditRating.id")
	private Integer creditRatingId;

	@SearchField(searchField = "name", searchFieldPath = "creditRating")
	private String creditRatingName;

	@SearchField(searchField = "shortTerm", searchFieldPath = "creditRating")
	private Boolean shortTerm;

	@SearchField(searchField = "name", searchFieldPath = "creditRating.ratingAgency")
	private String agencyName;

	@SearchField(searchField = "ratingAgency.id", searchFieldPath = "creditRating")
	private Integer agencyId;

	@SearchField(searchField = "outlook.id")
	private Short outlookId;

	@SearchField(searchField = "name", searchFieldPath = "outlook")
	private String outlookName;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getAssignee() {
		return this.assignee;
	}


	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}


	public Boolean getSecuritiesOnly() {
		return this.securitiesOnly;
	}


	public void setSecuritiesOnly(Boolean securitiesOnly) {
		this.securitiesOnly = securitiesOnly;
	}


	public Boolean getIssuersOnly() {
		return this.issuersOnly;
	}


	public void setIssuersOnly(Boolean issuersOnly) {
		this.issuersOnly = issuersOnly;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public Integer getIssuerCompanyId() {
		return this.issuerCompanyId;
	}


	public void setIssuerCompanyId(Integer issuerCompanyId) {
		this.issuerCompanyId = issuerCompanyId;
	}


	public String getIssuerCompanyName() {
		return this.issuerCompanyName;
	}


	public void setIssuerCompanyName(String issuerCompanyName) {
		this.issuerCompanyName = issuerCompanyName;
	}


	public Integer getCreditRatingId() {
		return this.creditRatingId;
	}


	public void setCreditRatingId(Integer creditRatingId) {
		this.creditRatingId = creditRatingId;
	}


	public String getCreditRatingName() {
		return this.creditRatingName;
	}


	public void setCreditRatingName(String creditRatingName) {
		this.creditRatingName = creditRatingName;
	}


	public Boolean getShortTerm() {
		return this.shortTerm;
	}


	public void setShortTerm(Boolean shortTerm) {
		this.shortTerm = shortTerm;
	}


	public String getAgencyName() {
		return this.agencyName;
	}


	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}


	public Integer getAgencyId() {
		return this.agencyId;
	}


	public void setAgencyId(Integer agencyId) {
		this.agencyId = agencyId;
	}


	public Short getOutlookId() {
		return this.outlookId;
	}


	public void setOutlookId(Short outlookId) {
		this.outlookId = outlookId;
	}


	public String getOutlookName() {
		return this.outlookName;
	}


	public void setOutlookName(String outlookName) {
		this.outlookName = outlookName;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
