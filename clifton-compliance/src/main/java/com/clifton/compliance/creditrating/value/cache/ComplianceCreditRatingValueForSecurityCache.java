package com.clifton.compliance.creditrating.value.cache;

import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * Cache for {@link ComplianceCreditRatingValueForSecurity} lists by security ID.
 *
 * @author MikeH
 */
@Component
public class ComplianceCreditRatingValueForSecurityCache extends SelfRegisteringSingleKeyDaoListCache<ComplianceCreditRatingValue, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "security.id";
	}


	@Override
	protected Integer getBeanKeyValue(ComplianceCreditRatingValue bean) {
		Integer securityId = null;
		if (bean instanceof ComplianceCreditRatingValueForSecurity) {
			securityId = BeanUtils.getBeanIdentity(((ComplianceCreditRatingValueForSecurity) bean).getSecurity());
		}
		return securityId;
	}
}
