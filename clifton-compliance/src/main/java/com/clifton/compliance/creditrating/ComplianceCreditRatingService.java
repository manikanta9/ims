package com.clifton.compliance.creditrating;


import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlook;
import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlookSearchForm;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingType;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingTypeSearchForm;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueSearchForm;

import java.util.List;


/**
 * The <code>ComplianceCreditRatingService</code> interface defines methods for working with compliance credit ratings and related objects.
 *
 * @author vgomelsky
 */
public interface ComplianceCreditRatingService {

	////////////////////////////////////////////////////////////////////////////
	//////////          Compliance Credit Rating Methods            ////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRating getComplianceCreditRating(int id);


	public List<ComplianceCreditRating> getComplianceCreditRatingList(ComplianceCreditRatingSearchForm searchForm);


	public ComplianceCreditRating saveComplianceCreditRating(ComplianceCreditRating bean);


	public void deleteComplianceCreditRating(int id);


	////////////////////////////////////////////////////////////////////////////
	//////////       Compliance Credit Rating Outlook Methods       ////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRatingOutlook getComplianceCreditRatingOutlook(short id);


	public List<ComplianceCreditRatingOutlook> getComplianceCreditRatingOutlookList(ComplianceCreditRatingOutlookSearchForm searchForm);


	public ComplianceCreditRatingOutlook saveComplianceCreditRatingOutlook(ComplianceCreditRatingOutlook bean);


	public void deleteComplianceCreditRatingOutlook(short id);


	////////////////////////////////////////////////////////////////////////////
	////////            Compliance Credit Rating Type Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRatingType getComplianceCreditRatingType(short id);


	public ComplianceCreditRatingType getComplianceCreditRatingTypeByName(String name);


	public List<ComplianceCreditRatingType> getComplianceCreditRatingTypeList(ComplianceCreditRatingTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////            General Credit Rating Value Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRatingValue getComplianceCreditRatingValue(int id);


	public List<? extends ComplianceCreditRatingValue> getComplianceCreditRatingValueList(ComplianceCreditRatingValueSearchForm searchForm);


	public void deleteComplianceCreditRatingValue(int id);


	////////////////////////////////////////////////////////////////////////////
	////////            Credit Rating Value For Security Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	public List<ComplianceCreditRatingValueForSecurity> getComplianceCreditRatingValueForSecurityList(ComplianceCreditRatingValueSearchForm searchForm);


	public List<ComplianceCreditRatingValueForSecurity> getComplianceCreditRatingValueForSecuritiesForSecurity(int securityId);


	public void saveComplianceCreditRatingValueForSecurity(ComplianceCreditRatingValueForSecurity bean);


	public void saveComplianceCreditRatingValueForSecurityList(List<ComplianceCreditRatingValueForSecurity> beanList);


	////////////////////////////////////////////////////////////////////////////
	////////            Credit Rating Value For Issuer Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	public List<ComplianceCreditRatingValueForIssuer> getComplianceCreditRatingValueForIssuerList(ComplianceCreditRatingValueSearchForm searchForm);


	public List<ComplianceCreditRatingValueForIssuer> getComplianceCreditRatingValueForIssuersForIssuer(int companyId);


	public void saveComplianceCreditRatingValueForIssuer(ComplianceCreditRatingValueForIssuer bean);


	public void saveComplianceCreditRatingValueForIssuerList(List<ComplianceCreditRatingValueForIssuer> beanList);
}
