package com.clifton.compliance.creditrating.value;


import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.investment.instrument.InvestmentSecurity;

import javax.persistence.Table;
import java.io.Serializable;


/**
 * The {@code ComplianceCreditRatingValueForSecurity} class represents credit ratings for securities.
 *
 * @see ComplianceCreditRatingValue
 */
@Table(name = "ComplianceCreditRatingValue")
public class ComplianceCreditRatingValueForSecurity extends ComplianceCreditRatingValue {

	private InvestmentSecurity security;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NamedEntityWithoutLabel<? extends Serializable> getAssignee() {
		return getSecurity();
	}


	@Override
	public String getAssigneeSymbol() {
		return getSecurity() != null ? getSecurity().getSymbol() : null;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}
}
