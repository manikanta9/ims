package com.clifton.compliance.creditrating.type;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The {@code ComplianceCreditRatingTypeSearchForm} class is a search definition for the {@link ComplianceCreditRatingType} objects.
 */
public class ComplianceCreditRatingTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean securityRating;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getSecurityRating() {
		return this.securityRating;
	}


	public void setSecurityRating(Boolean securityRating) {
		this.securityRating = securityRating;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
