package com.clifton.compliance.creditrating.retriever;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>ComplianceBondCreditRatingUpdaterJob</code> updates credit ratings for the specified set of securities.
 *
 * @author apopp
 */
public class ComplianceBondCreditRatingUpdaterJob implements Task, StatusHolderObjectAware<Status> {

	/**
	 * Investment group to run update over
	 */
	private Short investmentGroupId;

	/**
	 * Security group to exclude from this update run
	 */
	private Short excludeInvestmentSecurityGroupId;

	/**
	 * The market data source id of the provider to query data from (ex. Bloomberg)
	 */
	private Short marketDataSourceId;

	/**
	 * Include/exclude inactive securities from update
	 */
	private boolean includeInactiveSecurities;

	/**
	 * Optionally limit securities to those with active positions on the specified date. If true, skips securities that we don't currently hold across all accounts.
	 */
	private boolean activePositionsOnly;

	private AccountingPositionService accountingPositionService;
	private ComplianceCreditRatingRetrieverService complianceCreditRatingRetrieverService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private MarketDataSourceService marketDataSourceService;

	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();
		int called = 0, updated = 0, skipped = 0, upToDate = 0, addedEvents = 0, errors = 0;

		// get active securities under hierarchies that allow the specified security event
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInvestmentGroupId(getInvestmentGroupId());
		if (!isIncludeInactiveSecurities()) {
			securitySearchForm.setInstrumentIsInactive(false);
		}
		securitySearchForm.setOrderBy("symbol");

		MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSource(getMarketDataSourceId());
		ValidationUtils.assertNotNull(dataSource, "Cannot find Market Data Source with id = " + getMarketDataSourceId());

		Date lookupDate = DateUtils.clearTime(new Date());
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			try {
				status.setMessage("Securities: " + securityList.size() + "; Called: " + called + ". Skipped: " + skipped + ". Up to date: " + upToDate + ". Updated: " + updated +
						". Errors " + errors + ". Last: " + security.getSymbol());

				if (isSecuritySkipped(security, lookupDate)) {
					skipped++;
					status.addSkipped("Skipping " + security + " on " + DateUtils.fromDateShort(lookupDate));
					continue;
				}

				// update all credit ratings for this security
				Integer updateCnt = getComplianceCreditRatingRetrieverService().loadComplianceCreditRating(security.getId(), dataSource.getName());
				called++;

				if (updateCnt > 0) {
					addedEvents += updateCnt;
					updated++;
				}
				else {
					upToDate++;
				}
			}
			catch (ValidationException e) {
				// already user friendly message
				status.addError(e.getMessage());
				LogUtils.error(getClass(), e.getMessage(), e);
			}
			catch (Throwable e) {
				String msg = security.getSymbol() + ": " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(e));
				status.addError(msg);
				LogUtils.error(getClass(), msg, e);
			}
		}

		status.setMessage("Called " + dataSource.getName() + ' ' + called + " times. Updated = " + updated + ". Ratings Added = " + addedEvents +
				". Skipped = " + skipped + ". Up to date = " + upToDate + ". Error count = " + errors);
		return status;
	}


	/**
	 * Returns true if look up should be limited to Active Positions Only and the specified security does not have active positions on the specified transaction date.
	 * <p/>
	 * Also, return true if security is part of "Exclude" group.
	 *
	 * @param security
	 * @param transactionDate
	 */
	public boolean isSecuritySkipped(InvestmentSecurity security, Date transactionDate) {
		// Exclude any security that falls in the security group we wish to exclude from
		if (getExcludeInvestmentSecurityGroupId() != null && getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), getExcludeInvestmentSecurityGroupId())) {
			return true;
		}

		if (isActivePositionsOnly()) {
			AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
			command.setInvestmentSecurityId(security.getId());
			List<AccountingPosition> positions = getAccountingPositionService().getAccountingPositionListUsingCommand(command);

			return CollectionUtils.isEmpty(positions);
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getExcludeInvestmentSecurityGroupId() {
		return this.excludeInvestmentSecurityGroupId;
	}


	public void setExcludeInvestmentSecurityGroupId(Short excludeInvestmentSecurityGroupId) {
		this.excludeInvestmentSecurityGroupId = excludeInvestmentSecurityGroupId;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public boolean isIncludeInactiveSecurities() {
		return this.includeInactiveSecurities;
	}


	public void setIncludeInactiveSecurities(boolean includeInactiveSecurities) {
		this.includeInactiveSecurities = includeInactiveSecurities;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public ComplianceCreditRatingRetrieverService getComplianceCreditRatingRetrieverService() {
		return this.complianceCreditRatingRetrieverService;
	}


	public void setComplianceCreditRatingRetrieverService(ComplianceCreditRatingRetrieverService complianceCreditRatingRetrieverService) {
		this.complianceCreditRatingRetrieverService = complianceCreditRatingRetrieverService;
	}


	public boolean isActivePositionsOnly() {
		return this.activePositionsOnly;
	}


	public void setActivePositionsOnly(boolean activePositionsOnly) {
		this.activePositionsOnly = activePositionsOnly;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}
}
