package com.clifton.compliance.creditrating.type;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;


/**
 * The pre-determined type name identifier for {@link ComplianceCreditRatingType} objects.
 */
public enum ComplianceCreditRatingTypes {
	ISSUE("Issue"),
	ISSUER("Issuer"),
	SENIOR_UNSECURED("Senior Unsecured"),
	SENIOR_SUBORDINATE("Senior Subordinate"),
	UNKNOWN("Unknown");

	private final String name;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ComplianceCreditRatingTypes(String name) {
		this.name = name;
	}


	/**
	 * Gets the {@link ComplianceCreditRatingTypes type} enum value corresponding to the given object.
	 *
	 * @param complianceCreditRatingType the type object
	 * @return the {@link ComplianceCreditRatingTypes type} enum value
	 */
	public static ComplianceCreditRatingTypes getCreditRatingType(ComplianceCreditRatingType complianceCreditRatingType) {
		return ArrayUtils.getStream(values())
				.filter(type -> StringUtils.isEqual(complianceCreditRatingType.getName(), type.getName()))
				.findFirst().orElse(UNKNOWN);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}
}
