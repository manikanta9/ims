package com.clifton.compliance.creditrating.outlook;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntity;


/**
 * The <code>ComplianceCreditRatingOutlook</code> class represents future outlooks for credit ratings
 * from specific agencies.
 * <p/>
 * For example, Fitch uses following outlooks: Positive, Negative, Evolving.
 * Moody's outlooks are: Rating Under Review, No Outlook, Positive, Negative, Stable, Developing.
 *
 * @author vgomelsky
 */
public class ComplianceCreditRatingOutlook extends NamedEntity<Short> {

	/**
	 * A company of type = 'Credit Rating Agency'
	 */
	private BusinessCompany ratingAgency;

	/**
	 * Used for ordering and bench-marking across different credit rating agencies.
	 * Higher number means a better outlook.
	 */
	private int outlookWeight;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getRatingAgency() {
		return this.ratingAgency;
	}


	public void setRatingAgency(BusinessCompany ratingAgency) {
		this.ratingAgency = ratingAgency;
	}


	public int getOutlookWeight() {
		return this.outlookWeight;
	}


	public void setOutlookWeight(int outlookWeight) {
		this.outlookWeight = outlookWeight;
	}
}
