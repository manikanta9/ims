package com.clifton.compliance.creditrating.value.cache;

import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * Cache for {@link ComplianceCreditRatingValueForIssuer} lists by issuer ID.
 *
 * @author MikeH
 */
@Component
public class ComplianceCreditRatingValueForIssuerCache extends SelfRegisteringSingleKeyDaoListCache<ComplianceCreditRatingValue, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "issuerCompany.id";
	}


	@Override
	protected Integer getBeanKeyValue(ComplianceCreditRatingValue bean) {
		Integer companyId = null;
		if (bean instanceof ComplianceCreditRatingValueForIssuer) {
			companyId = BeanUtils.getBeanIdentity(((ComplianceCreditRatingValueForIssuer) bean).getIssuerCompany());
		}
		return companyId;
	}
}
