package com.clifton.compliance.creditrating;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntity;


/**
 * The <code>ComplianceCreditRating</code> class represents a single credit rating definition for
 * a given credit rating agency.
 * <p/>
 * Different credit rating agencies use different rating schemes and also have separate sets of
 * ratings for short-term and long-term assets.
 *
 * @author vgomelsky
 */
public class ComplianceCreditRating extends NamedEntity<Integer> {

	private ComplianceCreditRating equivalentShortTermRating;
	/**
	 * A company of type = 'Credit Rating Agency'
	 */
	private BusinessCompany ratingAgency;

	/**
	 * Used for ordering and bench-marking across different credit rating agencies.
	 * Higher number means a better credit rating.
	 */
	private int ratingWeight;
	/**
	 * Specifies whether this rating is for short-term or long-term securities.
	 */
	private boolean shortTerm;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getName() + " (" + this.ratingAgency.getName() + ": " + (this.shortTerm ? "short-term" : "long-term") + ")";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRating getEquivalentShortTermRating() {
		return this.equivalentShortTermRating;
	}


	public void setEquivalentShortTermRating(ComplianceCreditRating equivalentShortTermRating) {
		this.equivalentShortTermRating = equivalentShortTermRating;
	}


	public BusinessCompany getRatingAgency() {
		return this.ratingAgency;
	}


	public void setRatingAgency(BusinessCompany ratingAgency) {
		this.ratingAgency = ratingAgency;
	}


	public int getRatingWeight() {
		return this.ratingWeight;
	}


	public void setRatingWeight(int ratingWeight) {
		this.ratingWeight = ratingWeight;
	}


	public boolean isShortTerm() {
		return this.shortTerm;
	}


	public void setShortTerm(boolean shortTerm) {
		this.shortTerm = shortTerm;
	}
}
