package com.clifton.compliance.creditrating.outlook;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>ComplianceCreditRatingOutlookSearchForm</code> class defines search configuration for ComplianceCreditRatingOutlook objects.
 *
 * @author vgomelsky
 */
public class ComplianceCreditRatingOutlookSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "ratingAgency.id")
	private Integer ratingAgencyId;

	@SearchField(searchField = "name", searchFieldPath = "ratingAgency")
	private String ratingAgencyName;

	@SearchField
	private Integer outlookWeight;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getRatingAgencyId() {
		return this.ratingAgencyId;
	}


	public void setRatingAgencyId(Integer ratingAgencyId) {
		this.ratingAgencyId = ratingAgencyId;
	}


	public String getRatingAgencyName() {
		return this.ratingAgencyName;
	}


	public void setRatingAgencyName(String ratingAgencyName) {
		this.ratingAgencyName = ratingAgencyName;
	}


	public Integer getOutlookWeight() {
		return this.outlookWeight;
	}


	public void setOutlookWeight(Integer outlookWeight) {
		this.outlookWeight = outlookWeight;
	}
}
