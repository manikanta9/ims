package com.clifton.compliance.creditrating.retriever;


import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>ComplianceCreditRatingRetrieverService</code> retrieves latest credit ratings from specified provider
 *
 * @author apopp
 */
public interface ComplianceCreditRatingRetrieverService {

	/**
	 * Load latest credit ratings from provider for specified security
	 */
	@ModelAttribute
	@RequestMapping("complianceCreditRatingLoad")
	@SecureMethod(dtoClass = ComplianceCreditRatingValueForSecurity.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public Integer loadComplianceCreditRating(int securityId, String providerName);
}
