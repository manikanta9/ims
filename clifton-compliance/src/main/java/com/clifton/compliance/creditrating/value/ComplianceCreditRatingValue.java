package com.clifton.compliance.creditrating.value;


import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.outlook.ComplianceCreditRatingOutlook;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingType;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;

import java.io.Serializable;
import java.util.Date;


/**
 * The {@code ComplianceCreditRatingValue} class represents credit rating assignments. Because credit ratings can change over time, this class captures start
 * and end dates. For current credit ratings, no end date shall exist.
 */
public abstract class ComplianceCreditRatingValue extends BaseEntity<Integer> implements LabeledObject {

	private ComplianceCreditRating creditRating;
	private ComplianceCreditRatingOutlook outlook;
	private ComplianceCreditRatingType type;

	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract NamedEntityWithoutLabel<? extends Serializable> getAssignee();


	public abstract String getAssigneeSymbol();


	public String getAssigneeLabel() {
		return getAssignee().getLabel();
	}


	@Override
	public String getLabel() {
		final String nullString = "[null]";
		return String.format("%s rated %s as %s from %s%s",
				getAssigneeSymbol() != null ? getAssigneeSymbol() : nullString,
				getCreditRating() != null ? getCreditRating().getName() : nullString,
				getType() != null ? getType().getName() : nullString,
				getStartDate() != null ? DateUtils.fromDate(getStartDate(), DateUtils.DATE_FORMAT_INPUT) : nullString,
				getEndDate() != null ? " to " + DateUtils.fromDate(getEndDate(), DateUtils.DATE_FORMAT_INPUT) : StringUtils.EMPTY_STRING);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRating getCreditRating() {
		return this.creditRating;
	}


	public void setCreditRating(ComplianceCreditRating creditRating) {
		this.creditRating = creditRating;
	}


	public ComplianceCreditRatingOutlook getOutlook() {
		return this.outlook;
	}


	public void setOutlook(ComplianceCreditRatingOutlook outlook) {
		this.outlook = outlook;
	}


	public ComplianceCreditRatingType getType() {
		return this.type;
	}


	public void setType(ComplianceCreditRatingType type) {
		this.type = type;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
