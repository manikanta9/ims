package com.clifton.compliance.creditrating.value;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;


/**
 * The {@code ComplianceCreditRatingValueValidator} class validates credit rating values upon save.
 */
@Component
public class ComplianceCreditRatingValueValidator extends SelfRegisteringDaoValidator<ComplianceCreditRatingValue> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(ComplianceCreditRatingValue bean, DaoEventTypes config) throws ValidationException {
		// Validate that the end date is not earlier than the start date
		if (bean.getEndDate() != null && DateUtils.compare(bean.getStartDate(), bean.getEndDate(), false) > 0) {
			throw new FieldValidationException("The End Date must be later than the Start Date.", "endDate");
		}

		// Validate that an allowed type is used
		if (bean.getType().isSecurityRating() && !(bean instanceof ComplianceCreditRatingValueForSecurity)) {
			throw new ValidationException(String.format("Credit rating values of type [%s] must correspond to securities.", bean.getType().getName()));
		}
		else if (!bean.getType().isSecurityRating() && !(bean instanceof ComplianceCreditRatingValueForIssuer)) {
			throw new ValidationException(String.format("Credit rating values of type [%s] must correspond to issuer companies.", bean.getType().getName()));
		}
	}
}

