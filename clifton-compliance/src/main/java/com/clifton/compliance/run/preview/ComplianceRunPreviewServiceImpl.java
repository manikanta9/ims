package com.clifton.compliance.run.preview;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.compliance.rule.filter.ComplianceRuleFilterHandler;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.execution.ComplianceRunExecutionHandler;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


@Service
public class ComplianceRunPreviewServiceImpl implements ComplianceRunPreviewService {

	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private ComplianceRuleFilterHandler complianceRuleFilterHandler;
	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private ComplianceRunExecutionHandler complianceRunExecutionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public List<ComplianceRuleRun> getComplianceRunPreviewList(ComplianceRunExecutionCommand command) {
		ValidationUtils.assertNotNull(command.getClientAccountId(), "Previews are only enabled for account-specific rule assignments.");
		command.setGenerateDetailsForSuccess(true);
		command.setGenerateSubDetails(true);
		command.setPreviewOnly(true);

		return getComplianceRunExecutionHandler().doRebuildComplianceRunForCommand(command);
	}


	@Override
	public List<ComplianceRuleRun> getComplianceAssignmentPreviewList(ComplianceRunExecutionCommand command) {
		ValidationUtils.assertNotNull(command.getClientAccountId(), "Previews are only enabled for account-specific rule assignments.");
		ValidationUtils.assertNotNull(command.getProcessDate(), "The process date is required", "processDate");
		command.setGenerateDetailsForSuccess(true);

		Context context = new ContextImpl();
		ComplianceRuleAssignment assignment = getComplianceRuleAssignmentHandler().getAssignmentForAccountOnDate(command);
		List<ComplianceRulePosition> positionList = getComplianceRulePositionHandler().getPositionListForAccountByRule(context, assignment, command.getClientAccountId(),
				command.getProcessDate());

		Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerMapping = ComplianceUtils.mapIssuerBySecurityPositionList(
				ComplianceUtils.mapPositionListByUltimateIssuer(positionList));

		if (issuerMapping.containsKey(null)) {
			BusinessCompany naBucket = new BusinessCompany();
			naBucket.setName("Issuer Missing");
			issuerMapping.put(naBucket, issuerMapping.get(null));
			issuerMapping.remove(null);
		}

		ComplianceRuleRunDetail detailByIssuer = getComplianceRuleReportHandler().createFullRunDetail(context, issuerMapping, null, ComplianceRulePositionValues.NO_VALUE);
		detailByIssuer.setDescription("By Ultimate Issuer");

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(positionList);
		ComplianceRuleRunDetail detailBySecurity = getComplianceRuleReportHandler().createFullRunDetail(context, securityPositionMap, null, ComplianceRulePositionValues.NO_VALUE);
		detailBySecurity.setDescription("By Security");

		Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> industryMapping = getComplianceRuleFilterHandler().mapSecurityPositionListToIndustry(securityPositionMap, 3);

		if (industryMapping.containsKey(null)) {
			SystemHierarchy naHierarchy = new SystemHierarchy();
			naHierarchy.setName("Industry Missing");
			naHierarchy.setId((short) -1);
			industryMapping.put(naHierarchy, industryMapping.get(null));
			industryMapping.remove(null);
		}

		ComplianceRuleRunDetail detailByIndustry = getComplianceRuleReportHandler().createFullRunDetail(context, industryMapping, null, ComplianceRulePositionValues.NO_VALUE);
		detailByIndustry.setDescription("By Industry");

		ComplianceRuleRun run = getComplianceRuleReportHandler().createBasicRuleRun("Account Position Preview", BigDecimal.ZERO, true,
				CollectionUtils.createList(detailBySecurity, detailByIssuer, detailByIndustry));
		run.setDescription("Assignment Position Preview");

		return CollectionUtils.createList(run);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public ComplianceRuleFilterHandler getComplianceRuleFilterHandler() {
		return this.complianceRuleFilterHandler;
	}


	public void setComplianceRuleFilterHandler(ComplianceRuleFilterHandler complianceRuleFilterHandler) {
		this.complianceRuleFilterHandler = complianceRuleFilterHandler;
	}


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public ComplianceRunExecutionHandler getComplianceRunExecutionHandler() {
		return this.complianceRunExecutionHandler;
	}


	public void setComplianceRunExecutionHandler(ComplianceRunExecutionHandler complianceRunExecutionHandler) {
		this.complianceRunExecutionHandler = complianceRunExecutionHandler;
	}


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}
}
