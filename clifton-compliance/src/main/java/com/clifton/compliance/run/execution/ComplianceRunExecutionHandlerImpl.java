package com.clifton.compliance.run.execution;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.run.ComplianceRun;
import com.clifton.compliance.run.ComplianceRunService;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.ComplianceRuleRunSearchForm;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.NumberAwareStringComparator;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorContext;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;


@Component
public class ComplianceRunExecutionHandlerImpl implements ComplianceRunExecutionHandler {

	/*
	 * Desired order:
	 * - Security-specific rules first by rule type name
	 * - Non-security-specific rules by rule name
	 * - Rule label (in most cases this will not be reached)
	 */
	private static final Comparator<ComplianceRuleRun> RULE_RUN_COMPARATOR =
			Comparator.comparing(ComplianceRuleRun::getComplianceRule, Comparator.nullsFirst(Comparator.comparing(ComplianceRule::getName)))
					.thenComparing(rule -> rule.getRuleType().getName())
					.thenComparing(ComplianceRuleRun::getLabel);

	private ComplianceRuleAccountHandler complianceRuleAccountHandler;
	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private ComplianceRuleService complianceRuleService;
	private ComplianceRunService complianceRunService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private RuleDefinitionService ruleDefinitionService;
	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRuleRun> doRebuildComplianceRunForCommand(ComplianceRunExecutionCommand command) {
		// Process rules and aggregate results
		ComplianceRuleEvaluationConfig baseRuleEvaluationConfig = command.toRuleEvaluationConfig(getComplianceRuleAccountHandler(), getComplianceRulePositionHandler(), getInvestmentAccountService(), getInvestmentInstrumentService());
		baseRuleEvaluationConfig.setBusinessServiceId(null);
		List<ComplianceRuleRun> ruleResultList = new ArrayList<>();
		for (Integer accountId : getConfigAccountIdList(command)) {
			ComplianceRuleEvaluationConfig accountConfig = new ComplianceRuleEvaluationConfig(baseRuleEvaluationConfig);
			accountConfig.setClientAccountId(accountId);
			ruleResultList.addAll(processRulesForClient(accountConfig));
		}

		//We know this account is active but we shouldn't save anything if there were no rules to execute
		if (!CollectionUtils.isEmpty(ruleResultList) && !command.isPreviewOnly() && !command.isRealTime()) {
			getComplianceRunService().saveRunExecutionResults(ruleResultList, command);
		}
		return CollectionUtils.createSorted(ruleResultList, RULE_RUN_COMPARATOR);
	}


	@Override
	public ComplianceRuleRun executeRule(ComplianceRuleAssignment assignment, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		//It is possible that the flow of a rules execution may change the config as it processes so we create a clone of our config
		//so the rule can execute against it and change it how it needs without affecting other rule executions
		ComplianceRuleEvaluationConfig clonedRuleEvaluationConfig = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig);
		//Set the assignment on the config object to be available for the rules.


		clonedRuleEvaluationConfig.setRuleAssignment(assignment);

		ComplianceRuleRun run;
		//Get an instance of this rule bean
		ComplianceRuleEvaluator ruleEvaluator = (ComplianceRuleEvaluator) getSystemBeanService().getBeanInstance(assignment.getRule().getRuleEvaluatorBean());
		try {
			run = ruleEvaluator.evaluateRule(clonedRuleEvaluationConfig);
		}
		catch (Exception e) {
			LogUtils.error(getClass(), String.format("Failed to execute rule [%s] for %s", assignment.getRule().getName(), ruleEvaluationConfig.retrieveClientAccount().getLabel()), e);
			return generateErrorDetail(clonedRuleEvaluationConfig, assignment, e);
		}

		run.setComplianceRule(assignment.getRule());
		run.setRuleType(assignment.getRule().getRuleType());
		run.setLimitValue(assignment.getRule().getLimitValue());

		if (clonedRuleEvaluationConfig.isRealTime() || clonedRuleEvaluationConfig.isPreviewOnly()) {
			ComplianceRun complianceRun = new ComplianceRun();
			complianceRun.setClientInvestmentAccount(assignment.getClientInvestmentAccount());
			complianceRun.setRunDate(clonedRuleEvaluationConfig.getProcessDate());
			run.setComplianceRun(complianceRun);
		}

		getComplianceRuleReportHandler().setRunSuccess(run, assignment.getRule().getLimitValue(), assignment.getRule().getLimitValueRange(), assignment.getRule().getComplianceRuleLimitType());

		executeIgnorableConditionRuleRunCheck(run, assignment.getIgnoreFailSystemCondition(), assignment.getIgnorePassSystemCondition());
		return run;
	}


	@Override
	public void executeIgnorableConditionRuleRunCheck(ComplianceRuleRun ruleRun, SystemCondition ignorableFailCondition, SystemCondition ignorablePassCondition) {
		if (ignorableFailCondition != null && ruleRun != null && MathUtils.isNullOrZero(ruleRun.getStatus())) {
			boolean isIgnored = getSystemConditionEvaluationHandler().isConditionTrue(ignorableFailCondition, ruleRun);
			if (isIgnored) {
				ruleRun.setStatus(ComplianceRuleRun.STATUS_IGNORED);
			}
		}

		if (ignorablePassCondition != null && ruleRun != null && MathUtils.isGreaterThan(ruleRun.getStatus(), 0)) {
			boolean isIgnored = getSystemConditionEvaluationHandler().isConditionTrue(ignorablePassCondition, ruleRun);
			if (isIgnored) {
				ruleRun.setStatus(ComplianceRuleRun.STATUS_IGNORED);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Given a configuration, which contains an investment account, execute all batch rules for it.
	 *
	 * @param ruleEvaluationConfig - Various parameters set including a required investment account
	 * @return a mapping of all rules executed for this account and their generated results
	 */
	private List<ComplianceRuleRun> processRulesForClient(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		//Look up the category and get an instance of the rule evaluation context for the config if not already populated
		if (ruleEvaluationConfig.getTradeRuleEvaluatorContext() == null) {
			RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategoryByName(ruleEvaluationConfig.getCategoryName());
			RuleEvaluatorContext context = (RuleEvaluatorContext) getSystemBeanService().getBeanInstance(ruleCategory.getRuleEvaluatorContextBean());
			if (context instanceof TradeRuleEvaluatorContext) {
				ruleEvaluationConfig.setTradeRuleEvaluatorContext((TradeRuleEvaluatorContext) context);
			}
			else {
				throw new RuntimeException("Unsupported Rule Evaluator Context for Category with Name: " + ruleEvaluationConfig.getCategoryName());
			}
		}

		// Run rules
		List<ComplianceRuleAssignment> assignmentList = getAssignmentList(ruleEvaluationConfig);
		final List<ComplianceRuleRun> runList;
		if (ruleEvaluationConfig.isRealTime()) {
			// Execute rule directly, regardless of existing positions in system
			runList = CollectionUtils.getConverted(assignmentList, assignment -> executeRule(assignment, ruleEvaluationConfig));
		}
		else {
			// Match with existing positions in system and execute
			List<ComplianceRuleRun> standardRunList = evaluateStandardAssignmentList(ruleEvaluationConfig, assignmentList);
			List<ComplianceRuleRun> securitySpecificRunList = evaluateSecuritySpecificAssignmentList(ruleEvaluationConfig, assignmentList);
			runList = CollectionUtils.combineCollections(standardRunList, securitySpecificRunList);
		}
		return runList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<ComplianceRuleAssignment> getAssignmentList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Retrieve assignment list
		List<ComplianceRuleAssignment> assignmentList;
		if (ruleEvaluationConfig.isRealTime()) {
			//This will only be NULL when performing a real time preview
			if (ruleEvaluationConfig.getBean() == null && ruleEvaluationConfig.getCategoryTableEntityId() != null) {
				Trade trade = getTradeService().getTrade(ruleEvaluationConfig.getCategoryTableEntityId());
				ruleEvaluationConfig.setBean(trade);
				ruleEvaluationConfig.setInvestmentSecurityId(trade.getInvestmentSecurity().getId());
			}
			assignmentList = getComplianceRuleAssignmentHandler().getAssignmentRealTimeList(ruleEvaluationConfig.getBean().getClientInvestmentAccount(), ruleEvaluationConfig.getBean().getInvestmentSecurity(), ruleEvaluationConfig.getProcessDate(), ruleEvaluationConfig.isIncludeSecuritySpecificRules(), ruleEvaluationConfig.isIncludeParentRules());
		}
		else {
			assignmentList = getComplianceRuleAssignmentHandler().getAssignmentBatchList(ruleEvaluationConfig.retrieveClientAccount(), ruleEvaluationConfig.getProcessDate());
		}

		// Filter assignments
		if (ruleEvaluationConfig.getComplianceRuleId() != null) {
			ComplianceRule rule = getComplianceRuleService().getComplianceRule(ruleEvaluationConfig.getComplianceRuleId());
			List<ComplianceRule> ruleList = rule.getRuleType().isRollupRule()
					? getComplianceRuleService().getComplianceRuleFullChildrenList(rule)
					: Collections.singletonList(rule);
			assignmentList = CollectionUtils.getFiltered(assignmentList, assignment -> ruleList.contains(assignment.getRule()));
		}
		if (ruleEvaluationConfig.getComplianceRuleTypeId() != null) {
			Short ruleTypeId = ruleEvaluationConfig.getComplianceRuleTypeId();
			assignmentList = CollectionUtils.getFiltered(assignmentList, assignment -> ruleTypeId.equals(assignment.getRule().getRuleType().getId()));
		}
		assignmentList = CollectionUtils.getFiltered(assignmentList, assignment -> !assignment.isExclusionAssignment());
		return assignmentList;
	}


	private List<ComplianceRuleRun> evaluateStandardAssignmentList(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleAssignment> ruleAssignmentList) {
		List<ComplianceRuleAssignment> standardAssignmentList = CollectionUtils.getFiltered(ruleAssignmentList, assignment -> !assignment.getRule().getRuleType().isInvestmentSecuritySpecific());
		return standardAssignmentList.stream()
				.filter(assignment -> ruleEvaluationConfig.isPreviewOnly() || !ruleExecutedTodayForAccount(ruleEvaluationConfig))
				.map(assignment -> executeRule(assignment, ruleEvaluationConfig))
				.collect(Collectors.toList());
	}


	private List<ComplianceRuleRun> evaluateSecuritySpecificAssignmentList(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleAssignment> ruleAssignmentList) {
		List<ComplianceRuleAssignment> securitySpecificAssignmentList = CollectionUtils.getFiltered(ruleAssignmentList, rule -> rule.getRule().getRuleType().isInvestmentSecuritySpecific());
		// Guard-clause: No assignments provided for evaluation
		if (securitySpecificAssignmentList.isEmpty()) {
			return Collections.emptyList();
		}

		// Retrieve all positions under this security that need to be analyzed
		// Subsidiary and sub-account assignments are not supported for security-specific rules
		InvestmentAccount clientAccount = ruleEvaluationConfig.retrieveClientAccount();
		List<ComplianceRulePosition> positionList = getComplianceRulePositionHandler().getAccountingPositionListFullPopulatedForClientAccount(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), clientAccount.getId(), ruleEvaluationConfig.getProcessDate(), null, null);

		// Process rules for each position
		List<ComplianceRuleRun> rawSecuritySpecificRunList = new ArrayList<>();
		Map<InvestmentSecurity, List<ComplianceRulePosition>> positionListBySecurity = BeanUtils.getBeansMap(positionList, ComplianceRulePosition::getRealSecurityOrPseudo);
		for (Map.Entry<InvestmentSecurity, List<ComplianceRulePosition>> entry : positionListBySecurity.entrySet()) {
			// Find all rules from the provided list of rules that match the applicable (i.e. most-specific) rules for the security
			InvestmentSecurity security = entry.getKey();
			List<ComplianceRulePosition> securityPositionList = entry.getValue();
			List<ComplianceRuleAssignment> realTimeAssignmentListForSecurity = ruleEvaluationConfig.isRealTime()
					? getComplianceRuleAssignmentHandler().getAssignmentRealTimeSecuritySpecificList(clientAccount, security, ruleEvaluationConfig.getProcessDate())
					: getComplianceRuleAssignmentHandler().getAssignmentBatchSecuritySpecificList(clientAccount, security, ruleEvaluationConfig.getProcessDate());

			// The order of parameters is significant here; since equality is loose (duplicate IDs will exist in security-specific list), real-time list must take precedence
			List<ComplianceRuleAssignment> matchingSecurityRuleList = CollectionUtils.getIntersection(realTimeAssignmentListForSecurity, securitySpecificAssignmentList);

			// Run all matching rules for each position on this security
			for (ComplianceRulePosition position : securityPositionList) {
				ComplianceRuleEvaluationConfig securityRuleConfig = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig);
				AccountingBean bean = ObjectUtils.coalesce(position.getPosition(), position.getAccountingBalanceValue(), position.getPendingTrade());
				securityRuleConfig.setBean(bean);
				securityRuleConfig.setInvestmentSecurityId(security.getId());
				List<ComplianceRuleRun> positionRunList = matchingSecurityRuleList.stream()
						.filter(assignment -> securityRuleConfig.isPreviewOnly() || !ruleExecutedTodayForAccount(securityRuleConfig))
						.map(assignment -> executeRule(assignment, securityRuleConfig))
						.collect(Collectors.toList());
				rawSecuritySpecificRunList.addAll(positionRunList);
			}
		}

		// Process security-specific run results
		return processSecuritySpecificRuleRunList(rawSecuritySpecificRunList);
	}


	private List<ComplianceRuleRun> processSecuritySpecificRuleRunList(List<ComplianceRuleRun> runList) {
		Map<ComplianceRuleType, List<ComplianceRuleRun>> runListByRuleType = BeanUtils.getBeansMap(runList, ruleRun -> ruleRun.getComplianceRule().getRuleType());
		List<ComplianceRuleRun> processedRunList = runListByRuleType.entrySet().stream()
				.map(complianceRuleTypeListEntry -> generateSecuritySpecificRuleRunByRuleType(complianceRuleTypeListEntry.getKey(), complianceRuleTypeListEntry.getValue()))
				.collect(Collectors.toList());
		return processedRunList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a {@link ComplianceRuleRun} entity which groups all of the {@link ComplianceRuleRunDetail} elements for each unique {@link ComplianceRule} in the given <code>
	 * ruleRunList</code> together.
	 */
	private ComplianceRuleRun generateSecuritySpecificRuleRunByRuleType(ComplianceRuleType ruleType, List<ComplianceRuleRun> ruleRunList) {
		// Generate over-arching rule run
		ComplianceRuleRun worstRun = ruleRunList.stream()
				.min(Comparator.comparing(ComplianceRuleRun::getStatus))
				.orElseThrow(() -> new IllegalStateException(String.format("No minimum status value could be found while evaluating rule result of type [%s]. Run list: %s.", BeanUtils.getLabel(ruleType), CollectionUtils.toString(ruleRunList, 5))));
		boolean pass = worstRun.getStatus() != ComplianceRuleRun.STATUS_FAILED;
		ComplianceRuleRun ruleTypeRun = new ComplianceRuleRun();
		ruleTypeRun.setRuleType(ruleType);
		ruleTypeRun.setStatus(worstRun.getStatus());

		// Configure description for failed runs
		if (!pass) {
			int failedRunCount = CollectionUtils.getFiltered(ruleRunList, run -> run.getStatus() == ComplianceRuleRun.STATUS_FAILED).size();
			ruleTypeRun.setDescription(failedRunCount > 1
					? worstRun.getDescription() + " (" + (failedRunCount - 1) + " more)"
					: worstRun.getDescription());
		}

		// Group all details together for each rule for the given rule type sorted by rule name and then detail description
		Map<ComplianceRule, List<ComplianceRuleRunDetail>> runDetailListByRule = ruleRunList.stream().collect(Collectors.groupingBy(
				ComplianceRuleRun::getComplianceRule,
				() -> new TreeMap<>(Comparator.comparing(ComplianceRule::getName)),
				Collectors.collectingAndThen(Collectors.toList(), runList -> runList.stream()
						.flatMap(run -> CollectionUtils.getStream(run.getChildren()))
						.sorted(Comparator.comparing(ComplianceRuleRunDetail::getDescription, new NumberAwareStringComparator()))
						.collect(Collectors.toList()))));

		// Generate details
		for (Map.Entry<ComplianceRule, List<ComplianceRuleRunDetail>> ruleToRuleRunListEntry : runDetailListByRule.entrySet()) {
			// Generate second-level detail to encompass all results for this rule
			ComplianceRuleRunDetail ruleRunDetail = new ComplianceRuleRunDetail();
			ruleRunDetail.setDescription(ruleToRuleRunListEntry.getKey().getName());
			ruleRunDetail.setComplianceRuleRun(ruleTypeRun);
			ruleRunDetail.setPass(pass);
			ruleTypeRun.addDetail(ruleRunDetail);

			// Add each detail to the results
			for (ComplianceRuleRunDetail detail : ruleToRuleRunListEntry.getValue()) {
				ComplianceRuleRunSubDetail ruleRunSubDetail = new ComplianceRuleRunSubDetail();
				ruleRunSubDetail.setDescription(detail.getDescription());
				ruleRunDetail.addSubDetail(ruleRunSubDetail);
			}
		}
		return ruleTypeRun;
	}


	private ComplianceRuleRun generateErrorDetail(ComplianceRuleEvaluationConfig ruleEvaluationConfig, ComplianceRuleAssignment ruleAssignment, Exception e) {
		ComplianceRuleRun run = new ComplianceRuleRun();
		ComplianceRuleRunDetail detail = getComplianceRuleReportHandler().createBasicRuleRunDetail(e.getLocalizedMessage(), BigDecimal.ZERO, false, null);

		detail.setComplianceRuleRun(run);
		detail.setDescription("Error [" + e.getLocalizedMessage() + "]");
		detail.setPass(false);
		run.setError(e.getLocalizedMessage());
		run.setDescription(e.getLocalizedMessage());
		run.setPass(false);
		run.addDetail(detail);

		run.setComplianceRule(ruleAssignment.getRule());
		run.setRuleType(ruleAssignment.getRule().getRuleType());
		run.setLimitValue(ruleAssignment.getRule().getLimitValue());

		// Attach run entry to runs which generate details
		if (ruleEvaluationConfig.isRealTime() || ruleEvaluationConfig.isPreviewOnly()) {
			ComplianceRun complianceRun = new ComplianceRun();
			complianceRun.setClientInvestmentAccount(ruleAssignment.getClientInvestmentAccount());
			complianceRun.setRunDate(ruleEvaluationConfig.getProcessDate());
			run.setComplianceRun(complianceRun);
		}

		return run;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Integer> getConfigAccountIdList(ComplianceRunExecutionCommand command) {
		// Process all rules under an account
		List<Integer> accountIdList = new ArrayList<>();
		ObjectUtils.doIfPresent(command.getClientAccountId(), accountIdList::add);
		if (command.getBusinessServiceId() != null) {
			InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
			accountSearchForm.setOurAccount(true);
			accountSearchForm.setBusinessServiceOrParentId(command.getBusinessServiceId());
			accountSearchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CLOSED);
			List<InvestmentAccount> businessServiceClientAccountList = getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
			accountIdList.addAll(CollectionUtils.getConverted(businessServiceClientAccountList, InvestmentAccount::getId));
		}
		return accountIdList;
	}


	/**
	 * Given a rule and a configuration containing an account and process date,
	 * return whether this rule has been executed for the specified day
	 * (We do not execute rules more than once on a given day)
	 *
	 * @return has rule been executed for configured date already
	 */
	private boolean ruleExecutedTodayForAccount(ComplianceRunExecutionCommand command) {
		if (command.getClientAccountId() == null) {
			return false;
		}

		ComplianceRuleRunSearchForm searchForm = new ComplianceRuleRunSearchForm();
		searchForm.setComplianceRuleId(command.getComplianceRuleId());
		searchForm.setClientInvestmentAccountId(command.getClientAccountId());
		searchForm.setRunDate(command.getProcessDate());
		searchForm.setLimit(1);

		List<ComplianceRuleRun> ruleRunList = getComplianceRunService().getComplianceRuleRunList(searchForm);
		return !ruleRunList.isEmpty();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public void setComplianceRuleAccountHandler(ComplianceRuleAccountHandler complianceRuleAccountHandler) {
		this.complianceRuleAccountHandler = complianceRuleAccountHandler;
	}


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}


	public ComplianceRunService getComplianceRunService() {
		return this.complianceRunService;
	}


	public void setComplianceRunService(ComplianceRunService complianceRunService) {
		this.complianceRunService = complianceRunService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
