package com.clifton.compliance.run.rule.report;


import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.context.Context;
import com.clifton.trade.options.combination.TradeOptionCombination;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The handler for shared compliance rule run reporting utilities.
 * <p>
 * Methods declared by this handler provide means of creating rule reporting entities, such as {@link ComplianceRuleRun}, {@link ComplianceRuleRunDetail}, and {@link
 * ComplianceRuleRunSubDetail} objects.
 */
public interface ComplianceRuleReportHandler {

	public <T extends NamedObject, K> List<ComplianceRuleRunSubDetail> generateFullRunSubDetailList(Context context, Map<T, K> mapping, Date processDate, ComplianceRulePositionValues complianceRulePositionValues, ComplianceRuleRunDetail complianceRuleRunDetail, ComplianceRuleRunSubDetail complianceRuleRunSubDetailParent);


	public ComplianceRuleRun createRuleRun(String description, BigDecimal resultValue, Boolean pass, BigDecimal limitValue, BigDecimal percentOfAssets, String error, List<ComplianceRuleRunDetail> runDetailList);


	public ComplianceRuleRun createBasicRuleRun(String description, BigDecimal resultValue, Boolean pass, List<ComplianceRuleRunDetail> runDetailList);


	public ComplianceRuleRunDetail createRuleRunDetail(String description, BigDecimal resultValue, boolean pass, BigDecimal percentOfAssets, List<ComplianceRuleRunSubDetail> children);


	public ComplianceRuleRunDetail createBasicRuleRunDetail(String description, BigDecimal resultValue, boolean pass, List<ComplianceRuleRunSubDetail> children);


	public <T extends NamedObject> ComplianceRuleRunDetail createRuleRunDetail(Map<T, List<ComplianceRulePosition>> positionListMap);


	public <T extends NamedObject> ComplianceRuleRunDetail createRuleRunDetail(Map<T, List<ComplianceRulePosition>> positionListMap, String detailDescription, BigDecimal detailResultValue, boolean detailPass);


	public void propagateChildValues(ComplianceRuleRunDetail parentDetail, BigDecimal assets);


	public void setRunSuccess(ComplianceRuleRun run, BigDecimal limitValue, BigDecimal limitValueExtended, ComplianceRuleLimitType limitType);


	public <T extends NamedObject, K> ComplianceRuleRunDetail createFullRunDetail(Context context, Map<T, K> mapping, Date processDate, ComplianceRulePositionValues complianceRulePositionValues);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a simple {@link ComplianceRuleRunSubDetail} for the given {@link AccountingPositionInfo} object.
	 */
	public ComplianceRuleRunSubDetail createSubDetailForPosition(AccountingPositionInfo position);


	/**
	 * Generates a {@link ComplianceRuleRunSubDetail} for the given {@link TradeOptionCombination}. This provides a wrapper element that is useful for grouping option combinations
	 * together while still retaining details about their contents.
	 *
	 * @see #createSubDetailForOptionCombination(TradeOptionCombination, BigDecimal)
	 */
	public ComplianceRuleRunSubDetail createSubDetailForOptionCombination(TradeOptionCombination combination);


	/**
	 * Generates a {@link ComplianceRuleRunSubDetail} for the given {@link TradeOptionCombination} and <code>value</code>. This provides a wrapper element that is useful for
	 * grouping option combinations together while still retaining details about their contents.
	 */
	public ComplianceRuleRunSubDetail createSubDetailForOptionCombination(TradeOptionCombination combination, BigDecimal value);


	/**
	 * Generates a single "Position Group" {@link ComplianceRuleRunSubDetail} for the given {@link CompliancePositionValueHolder}.
	 */
	public <T extends AccountingPositionInfo> ComplianceRuleRunSubDetail createSubDetailForValueHolder(CompliancePositionValueHolder<T> positionValueHolder);


	/**
	 * Generates a list of "Position Group" {@link ComplianceRuleRunSubDetail} objects for the given list of {@link CompliancePositionValueHolder} objects.
	 */
	public <T extends AccountingPositionInfo> List<ComplianceRuleRunSubDetail> createSubDetailForValueHolderList(Collection<CompliancePositionValueHolder<T>> valueHolderList);
}
