package com.clifton.compliance.run.rule;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.run.ComplianceRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.security.user.SecurityUser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The <code>ComplianceRuleRun</code> represents the result of executing a single rule for the specified Client Account on a given date.
 *
 * @author apopp
 */
public class ComplianceRuleRun extends BaseSimpleEntity<Integer> implements LabeledObject {

	/**
	 * Run status options
	 */
	public static final short STATUS_FAILED = 0;
	public static final short STATUS_PASSED = 1;
	public static final short STATUS_IGNORED = 2;
	public static final short STATUS_FAILED_REVIEWED = 3;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Maintains information such as which account did we run this rule for
	 */
	private ComplianceRun complianceRun;


	/**
	 * Rule runs for specificity rules will only have a rule type defined and no specific rule executed for the run.
	 * The details will be the rule runs.
	 */
	private ComplianceRuleType ruleType;

	/**
	 * Represents which rule this run was performed for.  Null for specificity rules (see comment above).
	 */
	private ComplianceRule complianceRule;


	/**
	 * In this cause resultValue represents the most meaningful resultValue from the set of all possible run details.
	 * For instance, if the industry concentration rule has several industries that failed, we will pull out the one
	 * that failed the most to be displayed in this overall resultValue.
	 * <p/>
	 * If none of the details failed we will pull out the one that was closest to failing and assign this result value
	 * to it.
	 * <p/>
	 * These details are the first level of details shown to the user as a summary of rule execution
	 */
	private BigDecimal resultValue;
	private BigDecimal percentOfAssets;

	/**
	 * Represents the point at which this rule will fail.  Numerator/Denominator cannot be greater than limitValue
	 */
	private BigDecimal limitValue;


	/**
	 * Result status, Failure = 0, Pass = 1, Ignored (Failure) = 2, Failure (Reviewed) = 3
	 */
	private Short status;

	private String description; // details for this rule execution (usually from corresponding detail)


	/**
	 * Rule runs in STATUS_FAILED status must be manually reviewed in order to move to STATUS_FAILED_REVIEWED status.
	 */
	private SecurityUser reviewedByUser;
	private Date reviewedOnDate;
	private String reviewNote;



	@NonPersistentField
	private String error;


	/**
	 * List of all details describing this rule run
	 */
	private List<ComplianceRuleRunDetail> children;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = getComplianceRule() != null ? getComplianceRule().getName() : getRuleType() != null ? getRuleType().getName() : null;
		if (label != null) {
			return StringUtils.isEmpty(getDescription()) ? label : label + " : " + getDescription();
		}
		return getDescription();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addDetail(ComplianceRuleRunDetail detail) {
		if (getChildren() == null) {
			setChildren(new ArrayList<>());
		}
		getChildren().add(detail);
	}


	/**
	 * Helper setter method to set the status short
	 */
	public void setPass(Boolean pass) {
		if (pass == null) {
			setStatus(ComplianceRuleRun.STATUS_IGNORED);
		}
		else if (pass) {
			setStatus(ComplianceRuleRun.STATUS_PASSED);
		}
		else {
			setStatus(ComplianceRuleRun.STATUS_FAILED);
		}
	}


	public boolean isLeaf() {
		return CollectionUtils.isEmpty(getChildren());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		ComplianceRuleRun run = (ComplianceRuleRun) o;
		return Objects.equals(this.complianceRun, run.complianceRun) &&
				Objects.equals(this.complianceRule, run.complianceRule) &&
				Objects.equals(this.resultValue, run.resultValue) &&
				Objects.equals(this.percentOfAssets, run.percentOfAssets) &&
				Objects.equals(this.limitValue, run.limitValue) &&
				Objects.equals(this.status, run.status) &&
				Objects.equals(this.description, run.description);
	}


	@Override
	public int hashCode() {
		return Objects.hash(
				super.hashCode(),
				this.complianceRun,
				this.complianceRule,
				this.resultValue,
				this.percentOfAssets,
				this.limitValue,
				this.status,
				this.description
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRun getComplianceRun() {
		return this.complianceRun;
	}


	public void setComplianceRun(ComplianceRun complianceRun) {
		this.complianceRun = complianceRun;
	}


	public ComplianceRule getComplianceRule() {
		return this.complianceRule;
	}


	public void setComplianceRule(ComplianceRule complianceRule) {
		this.complianceRule = complianceRule;
	}


	public BigDecimal getResultValue() {
		return this.resultValue;
	}


	public void setResultValue(BigDecimal resultValue) {
		this.resultValue = resultValue;
	}


	public BigDecimal getLimitValue() {
		return this.limitValue;
	}


	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}


	public BigDecimal getPercentOfAssets() {
		return this.percentOfAssets;
	}


	public void setPercentOfAssets(BigDecimal percentOfAssets) {
		this.percentOfAssets = percentOfAssets;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<ComplianceRuleRunDetail> getChildren() {
		return this.children;
	}


	public void setChildren(List<ComplianceRuleRunDetail> children) {
		this.children = children;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public Short getStatus() {
		return this.status;
	}


	public void setStatus(Short status) {
		this.status = status;
	}


	public ComplianceRuleType getRuleType() {
		return this.ruleType;
	}


	public void setRuleType(ComplianceRuleType ruleType) {
		this.ruleType = ruleType;
	}


	public SecurityUser getReviewedByUser() {
		return this.reviewedByUser;
	}


	public void setReviewedByUser(SecurityUser reviewedByUser) {
		this.reviewedByUser = reviewedByUser;
	}


	public Date getReviewedOnDate() {
		return this.reviewedOnDate;
	}


	public void setReviewedOnDate(Date reviewedOnDate) {
		this.reviewedOnDate = reviewedOnDate;
	}


	public String getReviewNote() {
		return this.reviewNote;
	}


	public void setReviewNote(String reviewNote) {
		this.reviewNote = reviewNote;
	}
}
