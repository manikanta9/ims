package com.clifton.compliance.run.execution;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.Date;
import java.util.StringJoiner;


/**
 * The <code>ComplianceRunExecutionCommand</code> controls the execution of batch rules.
 *
 * @author apopp
 */
@NonPersistentObject
public class ComplianceRunExecutionCommand {

	// Specific date or date range
	protected Date processDate;
	protected TradeRuleEvaluatorContext tradeRuleEvaluatorContext;

	/**
	 * Preview functionality needs to preview for a specific account bean
	 * <p/>
	 * Initially this will be used to retrieve that trade on preview execution
	 */
	protected AccountingBean bean;

	// Specifies the rule category to use in order to properly obtain and instance of the EvaluatorContext
	protected String categoryName;
	protected Integer categoryTableEntityId;

	protected Integer clientAccountId;
	protected Short businessServiceId;
	protected Integer exclusionInvestmentAccountGroupId;
	protected Integer investmentAccountGroupId;
	protected Integer investmentSecurityId;

	protected Integer complianceRuleId;
	protected Short complianceRuleTypeId;

	protected boolean includeParentRules;
	/**
	 * Include the execution of security specific rules
	 */
	protected boolean includeSecuritySpecificRules = true;
	//Should details for success be generated
	protected boolean generateDetailsForSuccess = false;
	//Should sub details for success be generated
	protected boolean generateSubDetails = false;
	/**
	 * Execute rule runs without saving results
	 */
	protected boolean previewOnly = false;
	/**
	 * Should we execute realTime rules as well as batch
	 */
	protected boolean realTime = false;
	// Synchronous support
	protected boolean synchronous = false;

	// allows to send "live" updates of execution status back to the caller (assume the caller sets this field)
	protected Status status;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRunExecutionCommand() {
	}


	public ComplianceRunExecutionCommand(ComplianceRunExecutionCommand other) {
		// Copy constructor
		this.processDate = other.processDate;
		this.tradeRuleEvaluatorContext = other.tradeRuleEvaluatorContext;
		this.bean = other.bean;
		this.categoryName = other.categoryName;
		this.categoryTableEntityId = other.categoryTableEntityId;
		this.businessServiceId = other.businessServiceId;
		this.clientAccountId = other.clientAccountId;
		this.exclusionInvestmentAccountGroupId = other.exclusionInvestmentAccountGroupId;
		this.investmentAccountGroupId = other.investmentAccountGroupId;
		this.investmentSecurityId = other.investmentSecurityId;
		this.complianceRuleId = other.complianceRuleId;
		this.complianceRuleTypeId = other.complianceRuleTypeId;
		this.includeParentRules = other.includeParentRules;
		this.includeSecuritySpecificRules = other.includeSecuritySpecificRules;
		this.generateDetailsForSuccess = other.generateDetailsForSuccess;
		this.generateSubDetails = other.generateSubDetails;
		this.previewOnly = other.previewOnly;
		this.realTime = other.realTime;
		this.synchronous = other.synchronous;
		this.status = other.status;
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public ComplianceRuleEvaluationConfig toRuleEvaluationConfig(ComplianceRuleAccountHandler complianceRuleAccountHandler, ComplianceRulePositionHandler complianceRulePositionHandler, InvestmentAccountService investmentAccountService, InvestmentInstrumentService investmentInstrumentService) {
		ComplianceRuleEvaluationConfig ruleEvaluationConfig = new ComplianceRuleEvaluationConfig(complianceRuleAccountHandler, complianceRulePositionHandler, investmentAccountService, investmentInstrumentService);
		ruleEvaluationConfig.setTradeRuleEvaluatorContext(getTradeRuleEvaluatorContext());
		ruleEvaluationConfig.setProcessDate(getProcessDate());
		ruleEvaluationConfig.setBean(getBean());
		ruleEvaluationConfig.setCategoryName(getCategoryName());
		ruleEvaluationConfig.setCategoryTableEntityId(getCategoryTableEntityId());
		ruleEvaluationConfig.setClientAccountId(getClientAccountId());
		ruleEvaluationConfig.setBusinessServiceId(getBusinessServiceId());
		ruleEvaluationConfig.setExclusionInvestmentAccountGroupId(getExclusionInvestmentAccountGroupId());
		ruleEvaluationConfig.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		ruleEvaluationConfig.setInvestmentSecurityId(getInvestmentSecurityId());
		ruleEvaluationConfig.setComplianceRuleId(getComplianceRuleId());
		ruleEvaluationConfig.setComplianceRuleTypeId(getComplianceRuleTypeId());
		ruleEvaluationConfig.setIncludeParentRules(isIncludeParentRules());
		ruleEvaluationConfig.setIncludeSecuritySpecificRules(isIncludeSecuritySpecificRules());
		ruleEvaluationConfig.setGenerateDetailsForSuccess(isGenerateDetailsForSuccess());
		ruleEvaluationConfig.setGenerateSubDetails(isGenerateSubDetails());
		ruleEvaluationConfig.setPreviewOnly(isPreviewOnly());
		ruleEvaluationConfig.setRealTime(isRealTime());
		ruleEvaluationConfig.setSynchronous(isSynchronous());
		return ruleEvaluationConfig;
	}


	public String getRunId() {
		StringJoiner runIdJoiner = new StringJoiner("_");
		ObjectUtils.doIfPresent(getClientAccountId(), id -> runIdJoiner.add("Acct").add(String.valueOf(id)));
		ObjectUtils.doIfPresent(getBusinessServiceId(), id -> runIdJoiner.add("BusSvc").add(String.valueOf(id)));
		ObjectUtils.doIfPresent(getInvestmentAccountGroupId(), id -> runIdJoiner.add("AcctGroup").add(String.valueOf(id)));
		ObjectUtils.doIfPresent(getComplianceRuleId(), id -> runIdJoiner.add("ComRule").add(String.valueOf(id)));
		if (runIdJoiner.length() == 0) {
			runIdJoiner.add("ALL");
		}
		runIdJoiner.add(DateUtils.fromDateShort(getProcessDate()));
		return runIdJoiner.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "ComplianceRunExecutionCommand{" +
				"processDate=" + this.processDate +
				", tradeRuleEvaluatorContext=" + this.tradeRuleEvaluatorContext +
				", bean=" + this.bean +
				", categoryName='" + this.categoryName + '\'' +
				", categoryTableEntityId=" + this.categoryTableEntityId +
				", clientAccountId=" + this.clientAccountId +
				", businessServiceId=" + this.businessServiceId +
				", exclusionInvestmentAccountGroupId=" + this.exclusionInvestmentAccountGroupId +
				", investmentAccountGroupId=" + this.investmentAccountGroupId +
				", investmentSecurityId=" + this.investmentSecurityId +
				", complianceRuleId=" + this.complianceRuleId +
				", complianceRuleTypeId=" + this.complianceRuleTypeId +
				", includeParentRules=" + this.includeParentRules +
				", includeSecuritySpecificRules=" + this.includeSecuritySpecificRules +
				", generateDetailsForSuccess=" + this.generateDetailsForSuccess +
				", generateSubDetails=" + this.generateSubDetails +
				", previewOnly=" + this.previewOnly +
				", realTime=" + this.realTime +
				", synchronous=" + this.synchronous +
				", status=" + this.status +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getProcessDate() {
		return this.processDate;
	}


	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}


	public TradeRuleEvaluatorContext getTradeRuleEvaluatorContext() {
		return this.tradeRuleEvaluatorContext;
	}


	public void setTradeRuleEvaluatorContext(TradeRuleEvaluatorContext tradeRuleEvaluatorContext) {
		this.tradeRuleEvaluatorContext = tradeRuleEvaluatorContext;
	}


	public AccountingBean getBean() {
		return this.bean;
	}


	public void setBean(AccountingBean bean) {
		this.bean = bean;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Integer getCategoryTableEntityId() {
		return this.categoryTableEntityId;
	}


	public void setCategoryTableEntityId(Integer categoryTableEntityId) {
		this.categoryTableEntityId = categoryTableEntityId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Integer getExclusionInvestmentAccountGroupId() {
		return this.exclusionInvestmentAccountGroupId;
	}


	public void setExclusionInvestmentAccountGroupId(Integer exclusionInvestmentAccountGroupId) {
		this.exclusionInvestmentAccountGroupId = exclusionInvestmentAccountGroupId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getComplianceRuleId() {
		return this.complianceRuleId;
	}


	public void setComplianceRuleId(Integer complianceRuleId) {
		this.complianceRuleId = complianceRuleId;
	}


	public Short getComplianceRuleTypeId() {
		return this.complianceRuleTypeId;
	}


	public void setComplianceRuleTypeId(Short complianceRuleTypeId) {
		this.complianceRuleTypeId = complianceRuleTypeId;
	}


	public boolean isIncludeParentRules() {
		return this.includeParentRules;
	}


	public void setIncludeParentRules(boolean includeParentRules) {
		this.includeParentRules = includeParentRules;
	}


	public boolean isIncludeSecuritySpecificRules() {
		return this.includeSecuritySpecificRules;
	}


	public void setIncludeSecuritySpecificRules(boolean executeSecuritySpecificRules) {
		this.includeSecuritySpecificRules = executeSecuritySpecificRules;
	}


	public boolean isGenerateDetailsForSuccess() {
		return this.generateDetailsForSuccess;
	}


	public void setGenerateDetailsForSuccess(boolean generateDetailsForSuccess) {
		this.generateDetailsForSuccess = generateDetailsForSuccess;
	}


	public boolean isGenerateSubDetails() {
		return this.generateSubDetails;
	}


	public void setGenerateSubDetails(boolean generateSubDetails) {
		this.generateSubDetails = generateSubDetails;
	}


	public boolean isPreviewOnly() {
		return this.previewOnly;
	}


	public void setPreviewOnly(boolean previewOnly) {
		this.previewOnly = previewOnly;
	}


	public boolean isRealTime() {
		return this.realTime;
	}


	public void setRealTime(boolean realTime) {
		this.realTime = realTime;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
