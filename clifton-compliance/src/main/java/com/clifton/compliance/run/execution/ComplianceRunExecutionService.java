package com.clifton.compliance.run.execution;


import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;


public interface ComplianceRunExecutionService {

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Run Rebuild Business Methods   	   ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuilds a portion of ComplianceRun based on the specified filters.
	 */
	@ModelAttribute("data")
	public Status rebuildComplianceRun(ComplianceRunExecutionCommand command);
}
