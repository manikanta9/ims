package com.clifton.compliance.run.preview;


import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.rule.ComplianceRuleRun;

import java.util.List;


public interface ComplianceRunPreviewService {

	/**
	 * Generates a rule run preview for a specific rule for a given account.  These results are not saved in the database.
	 */
	public List<ComplianceRuleRun> getComplianceRunPreviewList(ComplianceRunExecutionCommand command);


	/**
	 * Generates a position list preview for a given rule assignment
	 */
	public List<ComplianceRuleRun> getComplianceAssignmentPreviewList(ComplianceRunExecutionCommand command);
}
