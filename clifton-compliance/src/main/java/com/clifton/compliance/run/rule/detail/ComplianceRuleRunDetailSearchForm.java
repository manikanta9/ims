package com.clifton.compliance.run.rule.detail;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;


public class ComplianceRuleRunDetailSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "complianceRuleRun.id")
	private Integer complianceRuleRunId;

	@SearchField(searchField = "complianceRuleRun.complianceRule.id")
	private Integer ruleId;

	@SearchField(searchField = "complianceRuleRun.complianceRun.clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField
	private String description;

	@SearchField
	private BigDecimal resultValue;

	@SearchField
	private Boolean pass;


	public Integer getComplianceRuleRunId() {
		return this.complianceRuleRunId;
	}


	public void setComplianceRuleRunId(Integer complianceRuleRunId) {
		this.complianceRuleRunId = complianceRuleRunId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getResultValue() {
		return this.resultValue;
	}


	public void setResultValue(BigDecimal resultValue) {
		this.resultValue = resultValue;
	}


	public Boolean getPass() {
		return this.pass;
	}


	public void setPass(Boolean pass) {
		this.pass = pass;
	}


	public Integer getRuleId() {
		return this.ruleId;
	}


	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}
}
