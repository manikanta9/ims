package com.clifton.compliance.run;

import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.ComplianceRuleRunSearchForm;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetailSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.security.user.SecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class ComplianceRunServiceImpl implements ComplianceRunService {

	private AdvancedUpdatableDAO<ComplianceRun, Criteria> complianceRunDAO;
	private AdvancedUpdatableDAO<ComplianceRuleRun, Criteria> complianceRuleRunDAO;
	private AdvancedUpdatableDAO<ComplianceRuleRunDetail, Criteria> complianceRuleRunDetailDAO;

	private InvestmentAccountService investmentAccountService;
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void saveRunExecutionResults(List<ComplianceRuleRun> ruleResultList, ComplianceRunExecutionCommand command) {
		BeanUtils.sortWithFunction(ruleResultList, ComplianceRuleRun::getPercentOfAssets, false);

		// Failures are sorted to the top, then ignores, and then success
		ruleResultList.sort((o1, o2) -> {
			if ((o1.getStatus() == ComplianceRuleRun.STATUS_IGNORED && o2.getStatus() == ComplianceRuleRun.STATUS_PASSED) || (o1.getStatus() < o2.getStatus() && o1.getStatus() != ComplianceRuleRun.STATUS_PASSED)) {
				return -1;
			}
			else if (o1.getStatus() > o2.getStatus()) {
				return 1;
			}
			return 0;
		});

		InvestmentAccount investmentAccount = getInvestmentAccountService().getInvestmentAccount(command.getClientAccountId());
		ComplianceRun complianceRun = new ComplianceRun();
		complianceRun.setClientInvestmentAccount(investmentAccount);
		complianceRun.setRunDate(command.getProcessDate());
		complianceRun.setStatus(calculateRunStatus(ruleResultList));

		//Save Run
		saveComplianceRun(complianceRun);

		for (ComplianceRuleRun ruleRun : CollectionUtils.getIterable(ruleResultList)) {
			ruleRun.setComplianceRun(complianceRun);
			//Save Rule Run
			saveComplianceRuleRun(ruleRun);

			for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(ruleRun.getChildren())) {
				if (!command.isGenerateDetailsForSuccess() && runDetail.isPass()) {
					continue;
				}
				runDetail.setComplianceRuleRun(ruleRun);
				//Save Detail
				saveComplianceRuleRunDetail(runDetail);

				// Clear children before save when sub-details are not needed
				if (!command.isGenerateSubDetails() && runDetail.isPass()) {
					runDetail.setChildren(null);
				}
			}
		}
	}


	private short calculateRunStatus(Collection<ComplianceRuleRun> ruleRunList) {
		int ignoredCount = 0;
		int reviewedCount = 0;
		for (ComplianceRuleRun ruleRun : ruleRunList) {
			if (ruleRun.getStatus() == ComplianceRuleRun.STATUS_FAILED) {
				return ComplianceRuleRun.STATUS_FAILED;
			}
			else if (ruleRun.getStatus() == ComplianceRuleRun.STATUS_IGNORED) {
				ignoredCount++;
			}
			else if (ruleRun.getStatus() == ComplianceRuleRun.STATUS_FAILED_REVIEWED) {
				reviewedCount++;
			}
		}
		if (reviewedCount > 0) {
			// if there are reviewed and ignored entries, mark as reviewed?
			return ComplianceRuleRun.STATUS_FAILED_REVIEWED;
		}
		if (ignoredCount > 0) {
			return ComplianceRuleRun.STATUS_IGNORED;
		}
		return ComplianceRuleRun.STATUS_PASSED;
	}


	@Override
	@Transactional
	public void reviewComplianceRuleRun(int[] ruleRunIds, String reviewNote) {
		updateComplianceRuleRunStatus(ruleRunIds, reviewNote, ComplianceRuleRun.STATUS_FAILED_REVIEWED);
	}


	@Override
	@Transactional
	public void undoReviewComplianceRuleRun(int[] ruleRunIds, String reviewNote) {
		updateComplianceRuleRunStatus(ruleRunIds, reviewNote, ComplianceRuleRun.STATUS_FAILED);
	}


	private void updateComplianceRuleRunStatus(int[] ruleRunIds, String reviewNote, short newStatus) {
		ValidationUtils.assertNotEmpty(reviewNote, "Review Note is required", "reviewNote");

		List<ComplianceRuleRun> ruleRunList = new ArrayList<>();
		for (int ruleRunId : ruleRunIds) {
			ComplianceRuleRun ruleRun = getComplianceRuleRun(ruleRunId);
			ValidationUtils.assertNotNull(ruleRun, "Cannot find Compliance Rule Run with id = " + ruleRunId);
			if (newStatus == ComplianceRuleRun.STATUS_FAILED_REVIEWED) {
				ValidationUtils.assertEquals(ruleRun.getStatus(), ComplianceRuleRun.STATUS_FAILED, "Compliance Rule Run must be in Failed status: " + ruleRunId);
			}
			else if (newStatus == ComplianceRuleRun.STATUS_FAILED) {
				ValidationUtils.assertEquals(ruleRun.getStatus(), ComplianceRuleRun.STATUS_FAILED_REVIEWED, "Compliance Rule Run must be in Failed (Reviewed) status: " + ruleRunId);
			}
			else {
				throw new ValidationException("Invalid new status [" + newStatus + "] for Compliance Rule Run with id = " + ruleRunId);
			}
			ruleRunList.add(ruleRun);
		}

		Date reviewDate = new Date();
		for (ComplianceRuleRun ruleRun : ruleRunList) {
			ruleRun.setStatus(newStatus);
			if (newStatus == ComplianceRuleRun.STATUS_FAILED_REVIEWED) {
				ruleRun.setReviewedByUser(getSecurityUserService().getSecurityUserCurrent());
				ruleRun.setReviewedOnDate(reviewDate);
			}
			else {
				ruleRun.setReviewedByUser(null);
				ruleRun.setReviewedOnDate(null);
			}
			ruleRun.setReviewNote(reviewNote);
			saveComplianceRuleRun(ruleRun);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Run Business Methods   			   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRun getComplianceRun(int id) {
		return getComplianceRunDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRun> getComplianceRunList(ComplianceRunSearchForm searchForm) {
		return getComplianceRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private ComplianceRun saveComplianceRun(ComplianceRun bean) {
		return getComplianceRunDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Run Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun getComplianceRuleRun(int id) {
		return getComplianceRuleRunDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRuleRun> getComplianceRuleRunList(ComplianceRuleRunSearchForm searchForm) {
		return getComplianceRuleRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private ComplianceRuleRun saveComplianceRuleRun(ComplianceRuleRun bean) {
		return getComplianceRuleRunDAO().save(bean);
	}


	@Override
	public List<NamedEntityWithoutLabel<Integer>> getComplianceRuleRunReviewNotes(int[] ruleRunIds, Date reviewedOnDateAfter) {
		if (ArrayUtils.isEmpty(ruleRunIds)) {
			throw new ValidationException("Required parameter missing: ruleRunIds");
		}
		ValidationUtils.assertNotNull(reviewedOnDateAfter, "Required parameter missing: reviewedOnDateAfter");

		// first retrieve selected Rule Runs
		ComplianceRuleRunSearchForm searchForm = new ComplianceRuleRunSearchForm();
		searchForm.setIds(Arrays.stream(ruleRunIds).boxed().toArray(Integer[]::new));
		List<ComplianceRuleRun> ruleRunList = getComplianceRuleRunList(searchForm);
		ValidationUtils.assertNotEmpty(ruleRunList, "Cannot find Compliance Rule Runs for ids: " + ArrayUtils.toString(ruleRunIds));

		// then get the notes for the rules and client accounts (from the runs)
		searchForm = new ComplianceRuleRunSearchForm();
		// rules are optional: can also be linked to rule type for Long/Short
		Integer[] ruleIds = CollectionUtils.getStream(ruleRunList).filter((run -> run.getComplianceRule() != null)).map(run -> run.getComplianceRule().getId()).distinct().toArray(Integer[]::new);
		if (!ArrayUtils.isEmpty(ruleIds)) {
			searchForm.setComplianceRuleIdsOrNull(ruleIds);
		}
		searchForm.setClientInvestmentAccountIds(CollectionUtils.getStream(ruleRunList).map(run -> run.getComplianceRun().getClientInvestmentAccount().getId()).distinct().toArray(Integer[]::new));
		searchForm.setReviewedOnDateAfter(reviewedOnDateAfter);
		searchForm.setReviewNoteNotNull(true);
		ruleRunList = getComplianceRuleRunList(searchForm);

		// create a list of distinct notes with friendly descriptions
		List<NamedEntityWithoutLabel<Integer>> result = new ArrayList<>();
		Set<String> notes = new HashSet<>();
		for (ComplianceRuleRun ruleRun : CollectionUtils.getIterable(ruleRunList)) {
			if (notes.add(ruleRun.getReviewNote())) {
				NamedEntityWithoutLabel<Integer> item = new NamedEntityWithoutLabel<>();
				item.setId(ruleRun.getId());
				item.setName(ruleRun.getReviewNote());
				item.setDescription("Note from " + DateUtils.fromDateShort(ruleRun.getComplianceRun().getRunDate()) + " Rule Run for " + ruleRun.getComplianceRun().getClientInvestmentAccount().getLabel() + ":\n " + ruleRun.getReviewNote());
				result.add(item);
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	///////           Compliance Rule Run Detail Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRunDetail getComplianceRuleRunDetail(int id) {
		return getComplianceRuleRunDetailDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRuleRunDetail> getComplianceRuleRunDetailList(ComplianceRuleRunDetailSearchForm searchForm) {
		return getComplianceRuleRunDetailDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private ComplianceRuleRunDetail saveComplianceRuleRunDetail(ComplianceRuleRunDetail bean) {
		return getComplianceRuleRunDetailDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ComplianceRun, Criteria> getComplianceRunDAO() {
		return this.complianceRunDAO;
	}


	public void setComplianceRunDAO(AdvancedUpdatableDAO<ComplianceRun, Criteria> complianceRunDAO) {
		this.complianceRunDAO = complianceRunDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleRun, Criteria> getComplianceRuleRunDAO() {
		return this.complianceRuleRunDAO;
	}


	public void setComplianceRuleRunDAO(AdvancedUpdatableDAO<ComplianceRuleRun, Criteria> complianceRuleRunDAO) {
		this.complianceRuleRunDAO = complianceRuleRunDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleRunDetail, Criteria> getComplianceRuleRunDetailDAO() {
		return this.complianceRuleRunDetailDAO;
	}


	public void setComplianceRuleRunDetailDAO(AdvancedUpdatableDAO<ComplianceRuleRunDetail, Criteria> complianceRuleRunDetailDAO) {
		this.complianceRuleRunDetailDAO = complianceRuleRunDetailDAO;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
