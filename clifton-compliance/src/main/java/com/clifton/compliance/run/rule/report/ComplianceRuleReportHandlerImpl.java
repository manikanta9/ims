package com.clifton.compliance.run.rule.report;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitTypes;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.trade.options.combination.TradeOptionCombination;
import com.clifton.trade.options.combination.TradeOptionLeg;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class ComplianceRuleReportHandlerImpl implements ComplianceRuleReportHandler {

	private ComplianceRulePositionHandler complianceRulePositionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun createRuleRun(String description, BigDecimal resultValue, Boolean pass, BigDecimal limitValue, BigDecimal percentOfAssets, String error, List<ComplianceRuleRunDetail> runDetailList) {
		ComplianceRuleRun ruleRun = new ComplianceRuleRun();
		ruleRun.setChildren(runDetailList);
		ruleRun.setDescription(description);
		ruleRun.setLimitValue(limitValue);
		ruleRun.setPass(pass);
		ruleRun.setPercentOfAssets(percentOfAssets);
		ruleRun.setResultValue(resultValue);
		ruleRun.setError(error);
		return ruleRun;
	}


	@Override
	public ComplianceRuleRun createBasicRuleRun(String description, BigDecimal resultValue, Boolean pass, List<ComplianceRuleRunDetail> runDetailList) {
		return createRuleRun(description, resultValue, pass, null, null, null, runDetailList);
	}


	@Override
	public ComplianceRuleRunDetail createRuleRunDetail(String description, BigDecimal resultValue, boolean pass, BigDecimal percentOfAssets, List<ComplianceRuleRunSubDetail> children) {
		ComplianceRuleRunDetail ruleRunDetail = new ComplianceRuleRunDetail();
		ruleRunDetail.setChildren(children);
		ruleRunDetail.setDescription(description);
		ruleRunDetail.setPass(pass);
		ruleRunDetail.setPercentOfAssets(percentOfAssets);
		ruleRunDetail.setResultValue(resultValue);
		return ruleRunDetail;
	}


	@Override
	public <T extends NamedObject, K> ComplianceRuleRunDetail createFullRunDetail(Context context, Map<T, K> mapping, Date processDate, ComplianceRulePositionValues complianceRulePositionValues) {
		ComplianceRuleRunDetail detail = createRuleRunDetail(null, BigDecimal.ZERO, false, BigDecimal.ZERO, null);
		detail.setChildren(generateFullRunSubDetailList(context, mapping, processDate, complianceRulePositionValues, detail, null));
		return detail;
	}


	@Override
	public void setRunSuccess(ComplianceRuleRun run, BigDecimal limitValue, BigDecimal limitValueExtended, ComplianceRuleLimitType limitType) {
		if (limitType == null) {
			return;
		}
		sortDetailListByResult(run.getChildren(), limitType);
		determineDetailPass(run.getChildren(), limitValue, limitValueExtended, limitType);
		ComplianceRuleRunDetail topDetail = CollectionUtils.getFirstElement(run.getChildren());
		if (topDetail != null) {
			run.setResultValue(topDetail.getResultValue());
			run.setPercentOfAssets(topDetail.getPercentOfAssets());
			run.setPass(topDetail.isPass());
		}
	}


	@Override
	public void propagateChildValues(ComplianceRuleRunDetail parentDetail, BigDecimal assets) {
		BigDecimal totalSubValue = propagateChildValues(assets, parentDetail.getChildren());
		parentDetail.setResultValue(totalSubValue);
		parentDetail.setPercentOfAssets(totalSubValue.divide(assets, 15, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
	}


	@Override
	public <T extends NamedObject, K> List<ComplianceRuleRunSubDetail> generateFullRunSubDetailList(Context context, Map<T, K> mapping, Date processDate, ComplianceRulePositionValues complianceRulePositionValues, ComplianceRuleRunDetail complianceRuleRunDetail, ComplianceRuleRunSubDetail complianceRuleRunSubDetailParent) {
		List<ComplianceRuleRunSubDetail> subDetailList = new ArrayList<>();
		//For each entity create a sub detail with that details description being the label on the entity
		// for each key, if it is a deeper map, recursively call this method until
		//  we reach a list of positions. For each position create a leaf node sub detail
		for (Map.Entry<T, K> tkEntry : mapping.entrySet()) {
			K value = tkEntry.getValue();
			ComplianceRuleRunSubDetail subDetail = ComplianceRuleRunSubDetail.create((tkEntry.getKey()).getLabel(), null, null, null);
			//if map, recursively call until we reach list
			if (value instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<T, K> mapValue = (Map<T, K>) value;
				subDetail.setChildren(generateFullRunSubDetailList(context, mapValue, processDate, complianceRulePositionValues, complianceRuleRunDetail, subDetail));
			}
			else if (value instanceof List) {
				@SuppressWarnings("unchecked")
				List<ComplianceRulePosition> listValue = (List<ComplianceRulePosition>) value;
				subDetail.setChildren(createRuleRunSubDetailListFromPositionList(context, listValue, processDate, complianceRulePositionValues));
			}
			subDetailList.add(subDetail);
		}
		return subDetailList;
	}


	@Override
	public ComplianceRuleRunDetail createBasicRuleRunDetail(String description, BigDecimal resultValue, boolean pass, List<ComplianceRuleRunSubDetail> children) {
		return createRuleRunDetail(description, resultValue, pass, null, children);
	}


	@Override
	public <T extends NamedObject> ComplianceRuleRunDetail createRuleRunDetail(Map<T, List<ComplianceRulePosition>> positionListMap, String detailDescription, BigDecimal detailResultValue, boolean detailPass) {
		ComplianceRuleRunDetail runDetail = createBasicRuleRunDetail(detailDescription, detailResultValue, detailPass, null);
		List<ComplianceRuleRunSubDetail> subDetailList = new ArrayList<>();
		for (NamedObject entity : positionListMap.keySet()) {
			subDetailList.add(ComplianceRuleRunSubDetail.create(entity.getLabel(), null, null, null));
		}
		runDetail.setChildren(subDetailList);
		return runDetail;
	}


	@Override
	public <T extends NamedObject> ComplianceRuleRunDetail createRuleRunDetail(Map<T, List<ComplianceRulePosition>> positionListMap) {
		return createRuleRunDetail(positionListMap, null, null, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRunSubDetail createSubDetailForPosition(AccountingPositionInfo position) {
		boolean pendingPosition = (position instanceof AccountingPosition && ((AccountingPosition) position).isPendingActivity());
		String positionIdentifier = pendingPosition ? "(Pending)" : String.valueOf(position.getAccountingTransactionId());
		return ComplianceRuleRunSubDetail.create(String.format("%s: %s", positionIdentifier, position.getInvestmentSecurity().getLabel()));
	}


	@Override
	public ComplianceRuleRunSubDetail createSubDetailForOptionCombination(TradeOptionCombination combination) {
		return createSubDetailForOptionCombination(combination, null);
	}


	@Override
	public ComplianceRuleRunSubDetail createSubDetailForOptionCombination(TradeOptionCombination combination, BigDecimal value) {
		// Get position sub-details
		List<ComplianceRuleRunSubDetail> positionSubDetailList = combination.getOptionLegList().stream()
				.map(TradeOptionLeg::getPositionList)
				.flatMap(Collection::stream)
				.map(this::createSubDetailForPosition)
				.collect(Collectors.toList());

		// Generate option combination sub-detail
		return ComplianceRuleRunSubDetail.create(combination.getLabel(), value, null, positionSubDetailList);
	}


	@Override
	public <T extends AccountingPositionInfo> ComplianceRuleRunSubDetail createSubDetailForValueHolder(CompliancePositionValueHolder<T> positionValueHolder) {
		return ComplianceRuleRunSubDetail.create(String.format("Position Group (Size: %d)", positionValueHolder.getPositionList().size()),
				positionValueHolder.getValue(),
				null,
				CollectionUtils.getConverted(positionValueHolder.getPositionList(), this::createSubDetailForPosition));
	}


	@Override
	public <T extends AccountingPositionInfo> List<ComplianceRuleRunSubDetail> createSubDetailForValueHolderList(Collection<CompliancePositionValueHolder<T>> valueHolderList) {
		return CollectionUtils.getConverted(valueHolderList, this::createSubDetailForValueHolder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void sortDetailListByResult(List<ComplianceRuleRunDetail> runDetailList, ComplianceRuleLimitType limitType) {
		ComplianceRuleLimitTypes type = ComplianceRuleLimitTypes.valueOf(limitType.getName());
		BeanUtils.sortWithFunction(runDetailList, ComplianceRuleRunDetail::getResultValue, type == ComplianceRuleLimitTypes.MINIMUM);
	}


	private void determineDetailPass(List<ComplianceRuleRunDetail> runDetailList, BigDecimal limitValue, BigDecimal limitValueExtended, ComplianceRuleLimitType limitType) {
		ComplianceRuleLimitTypes type = ComplianceRuleLimitTypes.valueOf(limitType.getName());
		for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(runDetailList)) {
			if (runDetail.getComplianceRuleRun() != null && runDetail.getComplianceRuleRun().getError() == null) {
				runDetail.setPass(type.isPass(runDetail.getPercentOfAssets(), limitValue, limitValueExtended));
			}
		}
		BeanUtils.sortWithFunction(runDetailList, ComplianceRuleRunDetail::isPass, true);
	}


	private BigDecimal propagateChildValues(BigDecimal assets, List<ComplianceRuleRunSubDetail> subDetailList) {
		BigDecimal totalVal = BigDecimal.ZERO;
		for (ComplianceRuleRunSubDetail subDetail : CollectionUtils.getIterable(subDetailList)) {
			if (subDetail.getChildren() != null) {
				BigDecimal resultValue = propagateChildValues(assets, subDetail.getChildren());
				subDetail.setResultValue(resultValue);
				subDetail.setPercentOfAssets(resultValue.divide(assets, 15, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
			}
			else {
				subDetail.setPercentOfAssets(subDetail.getResultValue().divide(assets, 15, RoundingMode.HALF_UP).multiply(new BigDecimal(100)));
			}
			totalVal = totalVal.add(subDetail.getResultValue());
		}
		return totalVal;
	}


	private List<ComplianceRuleRunSubDetail> createRuleRunSubDetailListFromPositionList(Context context, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues) {
		List<ComplianceRuleRunSubDetail> subDetailList = new ArrayList<>();
		for (ComplianceRulePosition position : CollectionUtils.getIterable(positionList)) {
			BigDecimal positionValue = getComplianceRulePositionHandler().getValueForPosition(context, position, complianceRulePositionValues, processDate);
			String id = "";
			if (position.getPosition() != null) {
				id = position.getPosition().getId() + ": ";
			}
			if (position.getPendingTrade() != null) {
				id += " (Pending) ";
			}
			subDetailList.add(ComplianceRuleRunSubDetail.create(id + position.getRealSecurityOrPseudo().getLabel() + " open on " + DateUtils.fromDateShort(position.getTransactionDate()), positionValue, null,
					null));
		}
		return subDetailList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}
}
