package com.clifton.compliance.run.execution;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class ComplianceRunExecutionServiceImpl implements ComplianceRunExecutionService {

	private RunnerHandler runnerHandler;
	private InvestmentAccountService investmentAccountService;

	private ComplianceRunExecutionHandler complianceRunExecutionHandler;
	private ComplianceRuleService complianceRuleService;
	private ComplianceRuleAccountHandler complianceRuleAccountHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method is the starting point for compliance rebuild executed by ComplianceRuleRunnerJob.
	 */
	@Override
	public Status rebuildComplianceRun(final ComplianceRunExecutionCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage("Synchronously running for " + command);
			doRebuildComplianceRun(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("COMPLIANCE-RUN", runId, now, statusHolder) {
			@Override
			public void run() {
				try {
					doRebuildComplianceRun(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding compliance run for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return statusHolder.getStatus();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void doRebuildComplianceRun(ComplianceRunExecutionCommand command, Status status) {
		ValidationUtils.assertNotNull(command.getProcessDate(), "Process date is required", "processDate");

		// If a specific rule was set, ensure it is a batch rule
		if (command.getComplianceRuleId() != null) {
			ComplianceRule complianceRule = getComplianceRuleService().getComplianceRule(command.getComplianceRuleId());
			ValidationUtils.assertTrue(complianceRule.isBatch(), "Only batch rules can be executed: " + complianceRule);
		}

		// Get all the applicable accounts based on configuration
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountsToRebuild(command);

		// Execute rules for each account
		int errorCount = 0;
		int warningCount = 0;
		int processedCount = 0;
		int totalAccounts = CollectionUtils.getSize(investmentAccountList);
		String processDateStr = DateUtils.fromDateShort(command.getProcessDate());
		status.setMessage("Potential Accounts: %d; Processed: %d; Errors: %d; Warnings: %d; Last: %s on %s", totalAccounts, processedCount, errorCount, warningCount, "[Not yet started]", processDateStr);
		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(investmentAccountList)) {
			status.setMessageArgs(totalAccounts, processedCount, errorCount, warningCount, clientAccount.getNumber(), processDateStr);
			try {
				ComplianceRunExecutionCommand accountCommand = new ComplianceRunExecutionCommand(command);
				accountCommand.setBusinessServiceId(null);
				accountCommand.setClientAccountId(clientAccount.getId());
				accountCommand.setTradeRuleEvaluatorContext(new TradeRuleEvaluatorContext());
				List<ComplianceRuleRun> ruleRunList = getComplianceRunExecutionHandler().doRebuildComplianceRunForCommand(accountCommand);

				List<ComplianceRuleRun> failedRunList = CollectionUtils.getFiltered(ruleRunList, ruleRun -> ruleRun.getStatus() == ComplianceRuleRun.STATUS_FAILED);
				for (ComplianceRuleRun run : failedRunList) {
					status.addWarning(clientAccount.getLabel() + " - " + run.getLabel());
				}
				processedCount++;
				warningCount += failedRunList.size();
			}
			catch (Throwable e) {
				errorCount++;
				status.addError(clientAccount.getLabel() + ": " + ExceptionUtils.getDetailedMessage(e));
				LogUtils.error(getClass(), "Failed to rebuild compliance rules for " + clientAccount.getLabel(), e);
			}
		}

		if (errorCount > 0) {
			status.setMessage("Completed processing of compliance rules for %d accounts. %d errors. %d warnings.", totalAccounts, errorCount, warningCount);
		}
		else {
			status.setMessage("Successfully completed processing of compliance rules for %d accounts.%s", totalAccounts, warningCount == 0 ? "" : " " + warningCount + " warnings.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentAccount> getInvestmentAccountsToRebuild(ComplianceRunExecutionCommand command) {
		// Only one of either client account or account group is allowed at a given time
		ValidationUtils.assertFalse(command.getClientAccountId() != null && command.getInvestmentAccountGroupId() != null, "The client account and the investment account group cannot be provided simultaneously for compliance runs. Please specify at most one of the options.", "clientAccountId");

		// Find applicable accounts for the command
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setOrderBy("number");
		searchForm.setId(command.getClientAccountId());
		searchForm.setBusinessServiceOrParentId(command.getBusinessServiceId());
		searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
		searchForm.setExcludeInvestmentAccountGroupId(command.getExclusionInvestmentAccountGroupId());
		if (command.getClientAccountId() == null && command.getBusinessServiceId() == null && command.getComplianceRuleId() != null) {
			// Apply rule assignment constraints if no explicit client account or business service is specified
			List<InvestmentAccount> assignedAccountList = getComplianceRuleAssignedAccountList(command.getComplianceRuleId(), command.getProcessDate());
			Integer[] assignedAccountIds = assignedAccountList == null ? null : assignedAccountList.stream()
					.map(BaseSimpleEntity::getId)
					.distinct()
					.toArray(Integer[]::new);
			searchForm.setIds(assignedAccountIds);
		}
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

		return getComplianceRuleAccountHandler().filterAccountList(investmentAccountList, command.getProcessDate());
	}


	/**
	 * Retrieves the list of {@link InvestmentAccount} entities to which the given rule may apply. This does not incorporate other applicable filtering which may override the rule,
	 * such as specificity overrides from other rules of the same type.
	 * <p>
	 * The special return value of {@code null} is used to indicate that the rule applies globally.
	 *
	 * @param ruleId      the ID of the rule for which potentially applicable accounts should be found
	 * @param processDate the date for which accounts should be found
	 * @return the list of {@link InvestmentAccount} entities to which the rule may apply, or {@code null} if the rule is applied globally
	 */
	private List<InvestmentAccount> getComplianceRuleAssignedAccountList(int ruleId, Date processDate) {
		List<ComplianceRuleAssignment> assignmentList = getRuleAssignmentList(ruleId, processDate);
		// Guard-clause: If any global assignments exist then no filtering is necessary
		if (assignmentList.stream().anyMatch(ComplianceRuleAssignment::isGlobal)) {
			return null;
		}

		return getAssignmentAccountList(assignmentList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves all {@link ComplianceRuleAssignment} entities for the given rule ID on the given process date.
	 */
	private List<ComplianceRuleAssignment> getRuleAssignmentList(int ruleId, Date processDate) {
		ComplianceRuleAssignmentSearchForm assignmentSearchForm = new ComplianceRuleAssignmentSearchForm();
		assignmentSearchForm.setRuleId(ruleId);
		assignmentSearchForm.setActiveOnDate(processDate);
		return getComplianceRuleService().getComplianceRuleAssignmentList(assignmentSearchForm);
	}


	/**
	 * Retrieves the list of {@link InvestmentAccount} entities to which the given list of {@link ComplianceRuleAssignment} entities applies via non-global assignments.
	 * <p>
	 * This does not include accounts to which the assignments will apply via global assignments.
	 */
	private List<InvestmentAccount> getAssignmentAccountList(List<ComplianceRuleAssignment> assignmentList) {
		// Get assigned accounts via direct assignments
		List<InvestmentAccount> directAccountList = assignmentList.stream()
				.map(ComplianceRuleAssignment::getClientInvestmentAccount)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		// Get assigned accounts via business service assignments
		Short[] assignmentBusinessServiceIds = assignmentList.stream()
				.map(ComplianceRuleAssignment::getBusinessService)
				.filter(Objects::nonNull)
				.map(BaseSimpleEntity::getId)
				.toArray(Short[]::new);
		final List<InvestmentAccount> businessServiceAccountList;
		if (assignmentBusinessServiceIds.length <= 0) {
			businessServiceAccountList = Collections.emptyList();
		}
		else {
			InvestmentAccountSearchForm businessServiceAccountSearchForm = new InvestmentAccountSearchForm();
			businessServiceAccountSearchForm.setOurAccount(true);
			businessServiceAccountSearchForm.setBusinessServiceOrParentIds(assignmentBusinessServiceIds);
			businessServiceAccountList = getInvestmentAccountService().getInvestmentAccountList(businessServiceAccountSearchForm);
		}

		// Get resulting list
		return CollectionUtils.combineCollections(directAccountList, businessServiceAccountList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public ComplianceRunExecutionHandler getComplianceRunExecutionHandler() {
		return this.complianceRunExecutionHandler;
	}


	public void setComplianceRunExecutionHandler(ComplianceRunExecutionHandler complianceRunExecutionHandler) {
		this.complianceRunExecutionHandler = complianceRunExecutionHandler;
	}


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public void setComplianceRuleAccountHandler(ComplianceRuleAccountHandler complianceRuleAccountHandler) {
		this.complianceRuleAccountHandler = complianceRuleAccountHandler;
	}
}
