package com.clifton.compliance.run;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The <code>ComplianceRun</code> represents the overall run of one or more rules for a given
 * account on a given run date.  It will maintain the overall state as to whether or not we had any failures for any one rule.
 *
 * @author apopp
 */
public class ComplianceRun extends BaseEntity<Integer> {

	private InvestmentAccount clientInvestmentAccount;
	private Date runDate;

	/**
	 * Result status, Failure = 0, Pass = 1, Ignored (Failure) = 2, Failure (Reviewed) = 3
	 */
	private Short status;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public Short getStatus() {
		return this.status;
	}


	public void setStatus(Short status) {
		this.status = status;
	}
}
