package com.clifton.compliance.run.job;


import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.execution.ComplianceRunExecutionService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>ComplianceRuleRunnerJob</code> is used to execute batch rules.
 *
 * @author apopp
 */
public class ComplianceRuleRunnerJob implements Task, StatusHolderObjectAware<Status> {

	private ComplianceRunExecutionService complianceRunExecutionService;

	// Only one of these applies at a time, if all null - rebuilds all client accounts
	private Integer clientAccountId;
	private Short businessServiceId;
	private Integer investmentAccountGroupId;
	private Integer exclusionInvestmentAccountGroupId;
	private Integer complianceRuleId;

	//Should sub details for success be generated
	private boolean generateSubDetails = false;
	//Should details for success be generated
	private boolean generateDetailsForSuccess = false;

	private boolean previousDayExecution;

	private StatusHolderObject<Status> statusHolderObject;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Called by 'BatchRunner' when executing 'Compliance Rule Runner for Previous Day' batch job
	 */
	@Override
	public Status run(Map<String, Object> context) {
		Date today = new Date();
		Date processDate = isPreviousDayExecution() ? DateUtils.getPreviousWeekday(today) : today;

		ComplianceRunExecutionCommand command = new ComplianceRunExecutionCommand();
		command.setSynchronous(true);
		command.setStatus(getStatusHolderObject().getStatus());
		command.setProcessDate(DateUtils.clearTime(processDate));
		command.setBusinessServiceId(getBusinessServiceId());
		command.setClientAccountId(getClientAccountId());
		command.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		command.setExclusionInvestmentAccountGroupId(getExclusionInvestmentAccountGroupId());
		command.setComplianceRuleId(getComplianceRuleId());
		command.setGenerateSubDetails(isGenerateSubDetails());
		command.setGenerateDetailsForSuccess(isGenerateDetailsForSuccess());

		getComplianceRunExecutionService().rebuildComplianceRun(command);

		return command.getStatus();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRunExecutionService getComplianceRunExecutionService() {
		return this.complianceRunExecutionService;
	}


	public void setComplianceRunExecutionService(ComplianceRunExecutionService complianceRunExecutionService) {
		this.complianceRunExecutionService = complianceRunExecutionService;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getExclusionInvestmentAccountGroupId() {
		return this.exclusionInvestmentAccountGroupId;
	}


	public void setExclusionInvestmentAccountGroupId(Integer exclusionInvestmentAccountGroupId) {
		this.exclusionInvestmentAccountGroupId = exclusionInvestmentAccountGroupId;
	}


	public Integer getComplianceRuleId() {
		return this.complianceRuleId;
	}


	public void setComplianceRuleId(Integer complianceRuleId) {
		this.complianceRuleId = complianceRuleId;
	}


	public boolean isGenerateSubDetails() {
		return this.generateSubDetails;
	}


	public void setGenerateSubDetails(boolean generateSubDetails) {
		this.generateSubDetails = generateSubDetails;
	}


	public boolean isGenerateDetailsForSuccess() {
		return this.generateDetailsForSuccess;
	}


	public void setGenerateDetailsForSuccess(boolean generateDetailsForSuccess) {
		this.generateDetailsForSuccess = generateDetailsForSuccess;
	}


	public boolean isPreviousDayExecution() {
		return this.previousDayExecution;
	}


	public void setPreviousDayExecution(boolean previousDayExecution) {
		this.previousDayExecution = previousDayExecution;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}
}
