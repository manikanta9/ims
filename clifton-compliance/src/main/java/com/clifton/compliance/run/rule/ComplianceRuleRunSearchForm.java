package com.clifton.compliance.run.rule;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class ComplianceRuleRunSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "complianceRule.id")
	private Integer complianceRuleId;

	@SearchField(searchField = "complianceRule.id")
	private Integer[] complianceRuleIds;

	@SearchField(searchField = "complianceRule.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Integer[] complianceRuleIdsOrNull;

	@SearchField(searchFieldPath = "complianceRule", searchField = "name")
	private String complianceRuleName;

	@SearchField(searchField = "ruleType.id")
	private Short complianceRuleTypeId;

	@SearchField(searchFieldPath = "complianceRun", searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchFieldPath = "complianceRun", searchField = "clientInvestmentAccount.id")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchFieldPath = "complianceRun", searchField = "runDate")
	private Date runDate;

	@SearchField
	private BigDecimal resultValue;

	@SearchField(searchFieldPath = "complianceRule", searchField = "complianceRuleLimitType.name")
	private String limitType;

	@SearchField
	private BigDecimal limitValue;

	@SearchField
	private Short[] status;


	@SearchField(searchField = "reviewedByUser.id")
	private Short reviewedByUserId;

	@SearchField
	private Date reviewedOnDate;

	@SearchField(searchField = "reviewedOnDate", comparisonConditions = ComparisonConditions.GREATER_THAN)
	private Date reviewedOnDateAfter;

	@SearchField
	private String reviewNote;

	@SearchField(searchField = "reviewNote", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean reviewNoteNotNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public void validate() {
		SearchUtils.validateRequiredFilter(this, "resultValue", "limitType", "limitValue");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer getComplianceRuleId() {
		return this.complianceRuleId;
	}


	public void setComplianceRuleId(Integer complianceRuleId) {
		this.complianceRuleId = complianceRuleId;
	}


	public Integer[] getComplianceRuleIds() {
		return this.complianceRuleIds;
	}


	public void setComplianceRuleIds(Integer[] complianceRuleIds) {
		this.complianceRuleIds = complianceRuleIds;
	}


	public Integer[] getComplianceRuleIdsOrNull() {
		return this.complianceRuleIdsOrNull;
	}


	public void setComplianceRuleIdsOrNull(Integer[] complianceRuleIdsOrNull) {
		this.complianceRuleIdsOrNull = complianceRuleIdsOrNull;
	}


	public Short getComplianceRuleTypeId() {
		return this.complianceRuleTypeId;
	}


	public void setComplianceRuleTypeId(Short complianceRuleTypeId) {
		this.complianceRuleTypeId = complianceRuleTypeId;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public BigDecimal getResultValue() {
		return this.resultValue;
	}


	public void setResultValue(BigDecimal resultValue) {
		this.resultValue = resultValue;
	}


	public String getLimitType() {
		return this.limitType;
	}


	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}


	public BigDecimal getLimitValue() {
		return this.limitValue;
	}


	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public String getComplianceRuleName() {
		return this.complianceRuleName;
	}


	public void setComplianceRuleName(String complianceRuleName) {
		this.complianceRuleName = complianceRuleName;
	}


	public Short[] getStatus() {
		return this.status;
	}


	public void setStatus(Short[] status) {
		this.status = status;
	}


	public Short getReviewedByUserId() {
		return this.reviewedByUserId;
	}


	public void setReviewedByUserId(Short reviewedByUserId) {
		this.reviewedByUserId = reviewedByUserId;
	}


	public Date getReviewedOnDate() {
		return this.reviewedOnDate;
	}


	public void setReviewedOnDate(Date reviewedOnDate) {
		this.reviewedOnDate = reviewedOnDate;
	}


	public Date getReviewedOnDateAfter() {
		return this.reviewedOnDateAfter;
	}


	public void setReviewedOnDateAfter(Date reviewedOnDateAfter) {
		this.reviewedOnDateAfter = reviewedOnDateAfter;
	}


	public String getReviewNote() {
		return this.reviewNote;
	}


	public void setReviewNote(String reviewNote) {
		this.reviewNote = reviewNote;
	}


	public Boolean getReviewNoteNotNull() {
		return this.reviewNoteNotNull;
	}


	public void setReviewNoteNotNull(Boolean reviewNoteNotNull) {
		this.reviewNoteNotNull = reviewNoteNotNull;
	}
}
