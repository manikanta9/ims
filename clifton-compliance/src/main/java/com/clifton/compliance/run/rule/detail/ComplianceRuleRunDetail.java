package com.clifton.compliance.run.rule.detail;


import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ComplianceRuleRunDetail</code> is similar to the ComplianceRuleRunSubDetail in the fact that it maintains the
 * same structure for result value, percent of assets, and description.
 * <p>
 * The difference is that run details can have pass/fail status that influence the overall execution of the rule.
 * <p>
 * For example, industry concentration rule would generate a single run detail object per industry level evaluated.  If any single
 * industry level evaluated violates the parameters of the rule, the whole rule fails.
 * <p>
 * Whereas the aggregate RIC rule states that we cannot have more than a certain percentage in the total of all RICs.
 * This rule would generate a single run detail which represents the bucketed total of all RICs which would be evaluated
 * for a pass/fail status.
 *
 * @author apopp
 */
public class ComplianceRuleRunDetail extends BaseSimpleEntity<Integer> implements LabeledObject {

	private ComplianceRuleRun complianceRuleRun;

	/*
	 * resultValue typically represents the numerator value of a given rule execution. At the detail level, this would
	 * typically be an aggregation of several share or market values aggregated together into a bucket
	 */
	private BigDecimal resultValue;

	//percentOfAssets represents resultValue divided by the denominator which is usually total fund market value
	// or aggregate of shares outstanding
	private BigDecimal percentOfAssets;

	private boolean pass;

	/*
	 * Description represents as basic description to further clarify the meaning of the detail. This typically
	 * represents a security label, issuer name, bucket of several values, industry level name, or ultimate issuer name
	 */
	private String description;

	/**
	 * The list of sub-detail entities which further describe this entity.
	 *
	 * @implNote This entity is stored as a compressed text field.
	 */
	private List<ComplianceRuleRunSubDetail> children;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getDescription();
	}


	public void addSubDetail(ComplianceRuleRunSubDetail subDetail) {
		if (getChildren() == null) {
			setChildren(new ArrayList<>());
		}
		getChildren().add(subDetail);
	}


	public boolean isLeaf() {
		return CollectionUtils.isEmpty(getChildren());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleRun getComplianceRuleRun() {
		return this.complianceRuleRun;
	}


	public void setComplianceRuleRun(ComplianceRuleRun complianceRuleRun) {
		this.complianceRuleRun = complianceRuleRun;
	}


	public BigDecimal getResultValue() {
		return this.resultValue;
	}


	public void setResultValue(BigDecimal resultValue) {
		this.resultValue = resultValue;
	}


	public BigDecimal getPercentOfAssets() {
		return this.percentOfAssets;
	}


	public void setPercentOfAssets(BigDecimal percentOfAssets) {
		this.percentOfAssets = percentOfAssets;
	}


	public boolean isPass() {
		return this.pass;
	}


	public void setPass(boolean pass) {
		this.pass = pass;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<ComplianceRuleRunSubDetail> getChildren() {
		return this.children;
	}


	public void setChildren(List<ComplianceRuleRunSubDetail> children) {
		this.children = children;
	}
}
