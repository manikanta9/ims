package com.clifton.compliance.run.execution;

import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.system.condition.SystemCondition;

import java.util.List;


public interface ComplianceRunExecutionHandler {

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Run Rebuild Business Methods   	   /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Execute rules for account
	 */
	public List<ComplianceRuleRun> doRebuildComplianceRunForCommand(ComplianceRunExecutionCommand command);


	/**
	 * Execute a rule against a specified account with a given configuration.
	 */
	public ComplianceRuleRun executeRule(ComplianceRuleAssignment assignment, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Determines if the run should be set to ignored based on a possible ignorable conditions
	 */
	public void executeIgnorableConditionRuleRunCheck(ComplianceRuleRun ruleRun, SystemCondition ignorableFailCondition, SystemCondition ignorablePassCondition);
}
