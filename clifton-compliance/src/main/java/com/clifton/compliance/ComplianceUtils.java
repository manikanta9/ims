package com.clifton.compliance;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * The <code>ComplianceUtils</code> class provides utility methods for working with rules.
 *
 * @author apopp
 */
public class ComplianceUtils {

	/**
	 * Given a system hierarchy this method will traverse up the parent until the desired level is reached
	 * <p/>
	 * If the level of the supplied start hierarchy is equal to or less than the desired level or it's parent is null,
	 * it will return the start hierarchy
	 *
	 * @return return the closest matching hierarchy to the requested level
	 */
	public static SystemHierarchy traverseHierarchy(SystemHierarchy startHierarchy, Integer level) {
		if (startHierarchy == null) {
			return null;
		}

		if (startHierarchy.getLevel() == level || startHierarchy.getParent() == null || startHierarchy.getLevel() < level) {
			return startHierarchy;
		}

		return traverseHierarchy(startHierarchy.getParent(), level);
	}


	/**
	 * Combine a collection of position lists into a single master list
	 */
	public static List<ComplianceRulePosition> combinePositionCollectionList(Collection<List<ComplianceRulePosition>> positionCollectionList) {
		List<ComplianceRulePosition> positionList = new ArrayList<>();

		for (List<ComplianceRulePosition> positionSubList : CollectionUtils.getIterable(positionCollectionList)) {
			positionList.addAll(positionSubList);
		}

		return positionList;
	}


	/**
	 * Given a position list create a map between the unique issuer and their associated positions.
	 */
	public static Map<BusinessCompany, List<ComplianceRulePosition>> mapPositionListByIssuer(List<ComplianceRulePosition> positionList) {
		return BeanUtils.getBeansMap(positionList, "realSecurityOrPseudo.businessCompany");
	}


	/**
	 * Given a position list create a map between the unique ultimate issuer and their associated positions.
	 */
	public static Map<BusinessCompany, List<ComplianceRulePosition>> mapPositionListByUltimateIssuer(List<ComplianceRulePosition> positionList) {
		return BeanUtils.getBeansMap(positionList, "realSecurityOrPseudo.businessCompany.rootParent");
	}


	/**
	 * Remap a mapping between security to position list to a mapping of issuer to security to position list
	 */
	public static Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> mapUltimateIssuerBySecurityPositionMap(
			Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionsMap) {
		Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap = new HashMap<>();

		if (securityPositionsMap == null) {
			return issuerSecurityPositionMap;
		}

		for (Map.Entry<InvestmentSecurity, List<ComplianceRulePosition>> investmentSecurityListEntry : securityPositionsMap.entrySet()) {
			BusinessCompany issuer = (investmentSecurityListEntry.getKey()).getBusinessCompany();

			if (issuer != null) {
				issuer = issuer.getRootParent();
			}

			Map<InvestmentSecurity, List<ComplianceRulePosition>> positionSecurityMap = issuerSecurityPositionMap.computeIfAbsent(issuer, k -> new HashMap<>());

			positionSecurityMap.put(investmentSecurityListEntry.getKey(), investmentSecurityListEntry.getValue());
		}
		return issuerSecurityPositionMap;
	}


	/**
	 * Remap a mapping between issue to position list
	 * to a mapping of issuer to security to position list
	 */
	public static Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> mapIssuerBySecurityPositionList(Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap) {
		Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap = new HashMap<>();

		for (Map.Entry<BusinessCompany, List<ComplianceRulePosition>> businessCompanyListEntry : issuerPositionMap.entrySet()) {
			issuerSecurityPositionMap.put(businessCompanyListEntry.getKey(), ComplianceUtils.mapPositionListBySecurity(businessCompanyListEntry.getValue()));
		}

		return issuerSecurityPositionMap;
	}


	/**
	 * Given a position list create a map between the unique securities and their associated positions.
	 */
	public static Map<InvestmentSecurity, List<ComplianceRulePosition>> mapPositionListBySecurity(List<ComplianceRulePosition> positionList) {
		return BeanUtils.getBeansMap(positionList, ComplianceRulePosition::getRealSecurityOrPseudo);
	}


	/**
	 * Build mapping between security and position list only including positions from
	 * securities falling under the specified list of security ids
	 */
	public static Map<InvestmentSecurity, List<ComplianceRulePosition>> mapPositionListBySecurity(List<Integer> securityIdKeyList, List<ComplianceRulePosition> positionList) {
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = mapPositionListBySecurity(positionList);

		securityPositionMap.keySet().removeIf(investmentSecurity -> investmentSecurity == null || !securityIdKeyList.contains(investmentSecurity.getId()));

		return securityPositionMap;
	}


	/**
	 * Given a list of positions return back a list of all unique securities involved in those positions
	 */
	public static List<Integer> getUniqueSecurityListFromPositionList(List<ComplianceRulePosition> positionList) {
		Object[] ids = BeanUtils.getPropertyValues(positionList, "realSecurityOrPseudo.id");
		if (ids.length == 0) {
			return new ArrayList<>();
		}
		return CollectionUtils.removeDuplicates(CollectionUtils.createList(ArrayUtils.toIntegerArray(ids)));
	}


	/**
	 * Filter a business company position map by business company
	 */
	public static void filterByCompany(Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap, Integer excludeBusinessCompanyId, boolean inclusion) {
		Iterator<BusinessCompany> companyIterator = issuerPositionMap.keySet().iterator();

		while (companyIterator.hasNext()) {
			BusinessCompany businessCompany = companyIterator.next();
			if (businessCompany == null) {
				continue;
			}

			boolean isInGroup = false;
			if (excludeBusinessCompanyId != null && excludeBusinessCompanyId.equals(businessCompany.getId())) {
				isInGroup = true;
			}

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				companyIterator.remove();
			}
		}
	}


	/**
	 * Filter position list by business company type
	 */
	public static List<ComplianceRulePosition> filterByCompanyType(List<ComplianceRulePosition> positionList, Short excludeBusinessCompanyTypeId, boolean inclusion) {
		if (excludeBusinessCompanyTypeId == null) {
			return positionList;
		}

		return BeanUtils.filter(positionList, complianceRulePosition -> inclusion == CompareUtils.isEqual(excludeBusinessCompanyTypeId,
				Optional.ofNullable(complianceRulePosition.getRealSecurityOrPseudo().getBusinessCompany())
						.map(BusinessCompany::getType) // Since pseudo-businesses are used in compliance, some businesses may not have types
						.map(IdentityObject::getIdentity)
						.orElse(null)));
	}


	/**
	 * Given a position list create a map between the unique securities or underlying instrument and their associated positions.
	 */
	public static Map<? extends NamedObject, List<ComplianceRulePosition>> mapPositionListBySecurityOrUnderlying(List<ComplianceRulePosition> positionList, boolean mapByUnderlying) {
		final Map<? extends NamedObject, List<ComplianceRulePosition>> positionListBySecurityOrUnderlying;
		if (mapByUnderlying) {
			positionListBySecurityOrUnderlying = BeanUtils.getBeansMap(positionList, position -> position.getRealSecurityOrPseudo().getInstrument().getUnderlyingInstrument());
		}
		else {
			positionListBySecurityOrUnderlying = mapPositionListBySecurity(positionList);
		}

		// Validate that no non-matching positions exist
		AssertUtils.assertFalse(positionListBySecurityOrUnderlying.containsKey(null), () -> String.format("Rule positions were found that did not match the specified security or underlying instrument: %s", CollectionUtils.toString(positionListBySecurityOrUnderlying.get(null), 1)));

		return positionListBySecurityOrUnderlying;
	}


	/**
	 * Given a list of positions return back the sum of their quantity normalized.
	 */
	public static BigDecimal sumPositionListQuantityNormalized(List<ComplianceRulePosition> positionList) {
		return positionList.stream()
				.map(complianceRulePosition -> {
					final BigDecimal positionQuantity;
					if (complianceRulePosition.getPosition() != null) {
						positionQuantity = complianceRulePosition.getPosition().getQuantityNormalized();
					}
					else if (complianceRulePosition.getAccountingBalanceValue() != null) {
						positionQuantity = complianceRulePosition.getAccountingBalanceValue().getBaseMarketValue();
					}
					else {
						positionQuantity = BigDecimal.ZERO;
					}
					return positionQuantity;
				})
				.reduce(BigDecimal.ZERO, MathUtils::add);
	}


	/**
	 * Given a mapping of position lists, remove entities from the map where their total exposure is zero.
	 */
	public static Map<? extends NamedObject, List<ComplianceRulePosition>> filterPositionMapByExposure(Map<? extends NamedObject, List<ComplianceRulePosition>> positionListMap) {
		return positionListMap.entrySet().stream()
				.filter(entry -> !MathUtils.isEqual(0, ComplianceUtils.sumPositionListQuantityNormalized(entry.getValue())))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
}
