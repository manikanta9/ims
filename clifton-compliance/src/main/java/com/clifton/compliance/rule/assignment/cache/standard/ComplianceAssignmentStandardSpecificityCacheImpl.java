package com.clifton.compliance.rule.assignment.cache.standard;

import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentSpecificityCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The specificity cache implementation for {@link ComplianceAssignmentStandardSpecificityCache}.
 *
 * @author MikeH
 */
@Component
public class ComplianceAssignmentStandardSpecificityCacheImpl extends BaseComplianceAssignmentSpecificityCache<ComplianceAssignmentStandardSource, ComplianceAssignmentStandardTargetHolder> implements ComplianceAssignmentStandardSpecificityCache {

	public ComplianceAssignmentStandardSpecificityCacheImpl() {
		super(false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRuleAssignment> getMostSpecificNonSecuritySpecificResultList(InvestmentAccount clientAccount, Date processDate, boolean realTime) {
		List<ComplianceRuleAssignment> assignmentList = getMostSpecificResultList(clientAccount, processDate, realTime);
		return CollectionUtils.getFiltered(assignmentList, assignment -> !assignment.getRule().getRuleType().isInvestmentSecuritySpecific());
	}


	@Override
	public List<ComplianceRuleAssignment> getMostSpecificResultList(InvestmentAccount clientAccount, Date processDate, boolean realTime) {
		return getMostSpecificResultList(new ComplianceAssignmentStandardSource(clientAccount, processDate, realTime));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected ComplianceAssignmentStandardTargetHolder generateTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment) {
		ComplianceAssignmentStandardTargetHolder targetHolder = new ComplianceAssignmentStandardTargetHolder(rule, assignment, getKeyGenerator());
		getCacheUpdater().registerCacheUpdateEvent(assignment, targetHolder);
		return targetHolder;
	}
}
