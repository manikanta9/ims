package com.clifton.compliance.rule.limit;


import com.clifton.compliance.rule.limit.typelimit.ComplianceRuleTypeLimit;
import com.clifton.compliance.rule.limit.typelimit.ComplianceRuleTypeLimitSearchForm;

import java.util.List;


public interface ComplianceRuleLimitService {

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Limit Type Business Methods   ///////// 
	////////////////////////////////////////////////////////////////////////////
	public ComplianceRuleLimitType getComplianceRuleLimitType(short id);


	public List<ComplianceRuleLimitType> getComplianceRuleLimitTypeList(final ComplianceRuleLimitTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Type Limit Business Methods   ///////// 
	////////////////////////////////////////////////////////////////////////////
	public ComplianceRuleTypeLimit getComplianceRuleTypeLimit(int id);


	public List<ComplianceRuleTypeLimit> getComplianceRuleTypeLimitList(ComplianceRuleTypeLimitSearchForm searchForm);


	public ComplianceRuleTypeLimit saveComplianceRuleTypeLimit(ComplianceRuleTypeLimit bean);


	public void deleteComplianceRuleTypeLimit(int id);
}
