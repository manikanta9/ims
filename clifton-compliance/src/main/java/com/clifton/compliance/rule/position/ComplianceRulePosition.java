package com.clifton.compliance.rule.position;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ComplianceRulePosition</code> encapsulates special logic for an AccountingPosition
 *
 * @author apopp
 */
public class ComplianceRulePosition {

	/**
	 * The position
	 */
	private AccountingPosition position;

	/**
	 * Currency
	 */
	private AccountingBalanceValue accountingBalanceValue;

	/**
	 * Pending Trade
	 */
	private Trade pendingTrade;

	/**
	 * If set, this implies that this position was retrieved from a subsidiary account
	 * which means special logic must be performed while executing rules for this position
	 * <p/>
	 * This typically means that rule processing will pseudo securitize this position across this account
	 */
	private InvestmentAccount pseudoPositionAccount;

	/**
	 * If this is a subsidiary pseudo position a fake security will be created
	 * for reporting and rule execution purposes
	 */
	private InvestmentSecurity pseudoSecurity;

	/**
	 * If pseudo security position then this defines it as a cash position
	 */
	private boolean cash;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePosition(AccountingPosition position) {
		setPosition(position);
	}


	public ComplianceRulePosition(AccountingBalanceValue accountingBalanceValue) {
		setAccountingBalanceValue(accountingBalanceValue);
	}


	public ComplianceRulePosition(Trade pendingTrade) {
		setPendingTrade(pendingTrade);
	}


	public ComplianceRulePosition(AccountingPosition position, InvestmentAccount account, InvestmentSecurity pseudoSecurity) {
		setPosition(position);
		setPseudoPositionAccount(account);
		setPseudoSecurity(pseudoSecurity);
	}


	public static ComplianceRulePosition newClonedInstance(ComplianceRulePosition complianceRulePosition) {
		ComplianceRulePosition clonedPosition = new ComplianceRulePosition(complianceRulePosition.getPosition(), complianceRulePosition.getPseudoPositionAccount(), complianceRulePosition.getPseudoSecurity());
		clonedPosition.setAccountingBalanceValue(complianceRulePosition.getAccountingBalanceValue());
		clonedPosition.setCash(complianceRulePosition.isCash());
		clonedPosition.setPendingTrade(complianceRulePosition.getPendingTrade());
		return clonedPosition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBean getAccountingBean() {
		if (getPendingTrade() != null) {
			return getPendingTrade();
		}
		if (getPosition() != null) {
			return getPosition();
		}
		if (getAccountingBalanceValue() != null) {
			return getAccountingBalanceValue();
		}
		return null;
	}


	public InvestmentSecurity getRealSecurityOrPseudo() {
		if (getPseudoSecurity() != null) {
			return getPseudoSecurity();
		}

		AccountingBean accountingBean = getAccountingBean();
		return (accountingBean == null) ? null : accountingBean.getInvestmentSecurity();
	}


	public Date getTransactionDate() {
		if (getPosition() != null) {
			return getPosition().getTransactionDate();
		}

		if (getPendingTrade() != null) {
			return getPendingTrade().getTradeDate();
		}

		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////                    Helper Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMarketValueBase(AccountingPositionHandler accountingPositionHandler) {
		BigDecimal result = null;
		if (getPendingTrade() != null) {
			// TODO: this logic should be reviewed, enhanced and moved
			if (InvestmentUtils.isNoPaymentOnOpen(getPendingTrade().getInvestmentSecurity())) {
				result = BigDecimal.ZERO;
			}
			else {
				result = getPendingTrade().getAccountingNotional();
			}
			result = MathUtils.add(result, getPendingTrade().getAccrualAmount());
			if (!getPendingTrade().getTradeType().isAmountsSignAccountsForBuy() && !getPendingTrade().isBuy()) {
				result = result.negate();
			}
			result = MathUtils.multiply(result, getPendingTrade().getExchangeRateToBase());
		}
		else if (getPosition() != null) {
			result = accountingPositionHandler.getAccountingPositionMarketValue(getPosition(), new Date());
		}
		else if (getAccountingBalanceValue() != null) {
			result = getAccountingBalanceValue().getBaseMarketValue();
		}
		return result;
	}


	public BigDecimal getNotionalValueBase(AccountingPositionHandler accountingPositionHandler) {
		BigDecimal result = null;
		if (getPendingTrade() != null) {
			result = MathUtils.multiply(getPendingTrade().getAccountingNotional(), getPendingTrade().getExchangeRateToBase());
			if (!getPendingTrade().getTradeType().isAmountsSignAccountsForBuy() && !getPendingTrade().isBuy()) {
				result = result.negate();
			}
		}
		else if (getPosition() != null) {
			result = accountingPositionHandler.getAccountingPositionNotional(getPosition(), new Date());
		}
		else if (getAccountingBalanceValue() != null) {
			result = getAccountingBalanceValue().getBaseNotional();
		}
		return result;
	}


	public BigDecimal getQuantity() {
		AccountingBean accountingBean = getAccountingBean();
		return (accountingBean == null) ? null : accountingBean.getQuantity();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPosition getPosition() {
		return this.position;
	}


	public void setPosition(AccountingPosition position) {
		this.position = position;
	}


	public InvestmentAccount getPseudoPositionAccount() {
		return this.pseudoPositionAccount;
	}


	public void setPseudoPositionAccount(InvestmentAccount pseudoPositionAccount) {
		this.pseudoPositionAccount = pseudoPositionAccount;
	}


	public InvestmentSecurity getPseudoSecurity() {
		return this.pseudoSecurity;
	}


	public void setPseudoSecurity(InvestmentSecurity pseudoSecurity) {
		this.pseudoSecurity = pseudoSecurity;
	}


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public AccountingBalanceValue getAccountingBalanceValue() {
		return this.accountingBalanceValue;
	}


	public void setAccountingBalanceValue(AccountingBalanceValue accountingBalanceValue) {
		this.accountingBalanceValue = accountingBalanceValue;
	}


	public Trade getPendingTrade() {
		return this.pendingTrade;
	}


	public void setPendingTrade(Trade pendingTrade) {
		this.pendingTrade = pendingTrade;
	}


	@Override
	public String toString() {
		return "ComplianceRulePosition{" +
				"position=" + ((this.position != null) ? this.position.getId() : null) +
				", accountingBalanceValue=" + ((this.accountingBalanceValue != null) ? this.accountingBalanceValue.getBaseMarketValue() + ":" + this.accountingBalanceValue.getBaseNotional() + ":" + this.accountingBalanceValue.getQuantityNormalized() : null) +
				", pendingTrade=" + ((this.pendingTrade != null) ? this.pendingTrade.getId() : null) +
				", pseudoPositionAccount=" + this.pseudoPositionAccount +
				", pseudoSecurity=" + this.pseudoSecurity +
				", cash=" + this.cash +
				'}';
	}
}
