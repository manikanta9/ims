package com.clifton.compliance.rule.assignment.cache.securityspecific;

import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentSpecificityCache;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The specificity cache implementation for {@link ComplianceAssignmentSecuritySpecificSpecificityCache}.
 *
 * @author MikeH
 */
@Component
public class ComplianceAssignmentSecuritySpecificSpecificityCacheImpl extends BaseComplianceAssignmentSpecificityCache<ComplianceAssignmentSecuritySpecificSource, ComplianceAssignmentSecuritySpecificTargetHolder> implements ComplianceAssignmentSecuritySpecificSpecificityCache {

	public ComplianceAssignmentSecuritySpecificSpecificityCacheImpl() {
		super(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRuleAssignment> getMostSpecificResultList(InvestmentAccount clientAccount, InvestmentSecurity security, Date processDate, boolean realTime) {
		return getMostSpecificResultList(new ComplianceAssignmentSecuritySpecificSource(clientAccount, security, processDate, realTime));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected ComplianceAssignmentSecuritySpecificTargetHolder generateTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment) {
		ComplianceAssignmentSecuritySpecificTargetHolder targetHolder = new ComplianceAssignmentSecuritySpecificTargetHolder(rule, assignment, getKeyGenerator());
		getCacheUpdater().registerCacheUpdateEvent(assignment, targetHolder);

		return targetHolder;
	}
}
