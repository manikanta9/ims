package com.clifton.compliance.rule.assignment;

import com.clifton.business.service.BusinessService;
import com.clifton.compliance.rule.assignment.cache.securityspecific.ComplianceAssignmentSecuritySpecificSpecificityCache;
import com.clifton.compliance.rule.assignment.cache.securityspecific.ComplianceAssignmentSecuritySpecificSpecificityCacheImpl;
import com.clifton.compliance.rule.assignment.cache.standard.ComplianceAssignmentStandardSpecificityCache;
import com.clifton.compliance.rule.assignment.cache.standard.ComplianceAssignmentStandardSpecificityCacheImpl;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;
import java.util.List;


/**
 * The handler for specificity-based compliance rule assignments. This handler houses methods for querying {@link ComplianceRuleAssignment} entities based on a number of
 * specificity-related parameters.
 * <p>
 * The assignment specificity structure considers several aspects during evaluation, including the following:
 * <ul>
 * <li>The assignment target (client account; business service, including sub-services; or global)
 * <li>Descendants of rollup rules
 * <li>{@link ComplianceAssignmentSecuritySpecificSpecificityCache Security-specific rules} and {@link ComplianceAssignmentStandardSpecificityCache standard rules}
 * <li>Real-time and batch state for rules
 * <li>The date of evaluation
 * </ul>
 * <p>
 * As a result of the complexity within the assignment specificity structure, the parameters required for assignment retrieval when evaluating an account require more control over
 * specificity querying than is granted by the standard {@link SpecificityCache} structure. To address this, multiple caches have been created. Associated cache components include
 * the following:
 * <ul>
 * <li>{@link ComplianceAssignmentStandardSpecificityCacheImpl}
 * <li>{@link ComplianceAssignmentSecuritySpecificSpecificityCacheImpl}
 * </ul>
 *
 * @author MikeH
 */
public interface ComplianceRuleAssignmentHandler {

	/**
	 * Populates all specificity-based caches for compliance rule assignments.
	 */
	public void buildComplianceCaches();


	////////////////////////////////////////////////////////////////////////////
	////////            Rule Assignment Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of all rules which may be executed for the given account on the given date.
	 */
	public List<ComplianceRuleAssignment> getAssignmentListForAccount(InvestmentAccount clientAccount, Date processDate);


	/**
	 * Returns the list of all batch rules for the given account on the given date.
	 */
	public List<ComplianceRuleAssignment> getAssignmentBatchList(InvestmentAccount clientAccount, Date processDate);


	/**
	 * Returns the most specific list of batch compliance rules for the given account and security on the given date. No more than one compliance rule is returned for every
	 * compliance rule type.
	 */
	public List<ComplianceRuleAssignment> getAssignmentBatchSecuritySpecificList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date);


	/**
	 * Returns the list of all real-time rules for the given account on the given date.
	 */
	public List<ComplianceRuleAssignment> getAssignmentRealTimeList(InvestmentAccount clientAccount, Date processDate);


	/**
	 * Retrieves two lists:
	 * <ul>
	 * <li>The most specific compliance rules for the given trade information (no more than one rule per compliance rule type)
	 * <li>All non-security specific compliance rules
	 * </ul>
	 * <p>
	 * This method returns the union of these two lists.
	 */
	public List<ComplianceRuleAssignment> getAssignmentRealTimeList(InvestmentAccount clientAccount, InvestmentSecurity security, Date processDate, boolean includeSecuritySpecificRules, boolean includeParentRules);


	/**
	 * Returns the most specific list of real-time compliance rules for the given account and security on the given date. No more than one compliance rule is returned for every
	 * compliance rule type.
	 */
	public List<ComplianceRuleAssignment> getAssignmentRealTimeSecuritySpecificList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date);


	/**
	 * Find the related rule assignment for the given rule, account, and date in the command, if one exists. If no such assignment exists, a pseudo-assignment will be generated
	 * with minimal attributes. For example, no {@link ComplianceRuleAssignment#subAccountPurpose sub-account purpose} (nor any other such fields) will be set.
	 * <p>
	 * This will return a <code>null</code> result if no rule or account is provided in the command.
	 */
	public ComplianceRuleAssignment getAssignmentForAccountOnDate(ComplianceRunExecutionCommand command);


	/**
	 * Returns all assignments.
	 *
	 * @param filterToSecuritySpecific if <code>true</code>, the results will be filtered to rules which are {@link ComplianceRuleType#investmentSecuritySpecific
	 *                                 security-specific}; otherwise, all results will be returned
	 */
	public List<ComplianceRuleAssignment> getFullAssignmentList(boolean filterToSecuritySpecific);


	////////////////////////////////////////////////////////////////////////////
	////////            Pseudo-Assignment Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of pseudo-assignments via business services which apply for the given assignment. This method recursively generates pseudo-assignments for all
	 * sub-business-services for the given assignment.
	 * <p>
	 * This method is used the populate the specificity cache with information contained in another separate hierarchy: the {@link BusinessService} hierarchy. Since the specificity
	 * cache does not provide a suitable utility for unbounded hierarchies, such as those implementing {@link HierarchicalObject}, it is necessary to manually populate the cache
	 * with generated pseudo-entities to match hierarchical descendants.
	 * <p>
	 * For example, this method allows the system to function as expected in the following hypothetical circumstance:
	 * <pre><code>
	 * Assignment 1: {"ruleId": 1, "businessService": "Franchise / Private Services"}
	 * Assignment 2: {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors"}
	 * Account for evaluation: {"businessService": "Franchise / Private Services / US"}
	 * </code></pre>
	 * In the above example, the specificity cache will produce pseudo-assignments for all sub-business-services of <code>"Franchise / Private Services"</code> and <code>"Franchise
	 * / Private Services / US / Condors"</code>.
	 * <pre><code>
	 * Assignment 1.1 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US"}
	 * Assignment 1.2 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors"}
	 * Assignment 1.3 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors / Iron"}
	 * Assignment 1.4 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors / Short"}
	 * Assignment 2.1 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors / Iron"}
	 * Assignment 2.2 (pseudo): {"ruleId": 1, "businessService": "Franchise / Private Services / US / Condors / Short"}
	 * ...
	 * </code></pre>
	 * With the pseudo-assignments created, assignment 1.1 is applied as expected when searching for assignments that apply to the account.
	 * <p>
	 * Note that this produces duplicate assignments in cases such as the example above (similar assignments are 2 and 1.2, 1.3 and 2.1, and 1.4 and 2.2). In these cases, redundant
	 * assignments will exist. Since we are unable to cleanly incorporate dates while creating pseudo-assignments for descendants, these redundant assignments are necessary. This
	 * will result in redundant evaluations for accounts in some cases.
	 *
	 * @param businessServiceAssignment the business service assignment for which pseudo-assignments should be retrieved
	 * @return the pseudo-assignment list for business service descendants for the given assignment
	 */
	public List<ComplianceRuleAssignment> getPseudoBusinessServiceAssignmentList(ComplianceRuleAssignment businessServiceAssignment, List<BusinessService> businessServiceList);


	/**
	 * Gets the list of pseudo-assignments that apply via to the given rollup rule assignment.
	 * <p>
	 * This method generates pseudo-assignments for each rule that is contained within the rollup rule for the given assignment. A single pseudo-assignment will be created for
	 * every non-rollup rule within the rule hierarchy. This effectively <i>flattens</i> the rollup rule assignment into its applicable sub-assignments.
	 * <p>
	 * As with {@link #getPseudoBusinessServiceAssignmentList(ComplianceRuleAssignment, List<BusinessService>)}, this method will create duplicate assignments when both a rules is assigned to an account
	 * both directly and via a rollup rule assignment. While this may result in redundant evaluations, it is akin to assigning the same rule to an account twice and should be
	 * treated as such.
	 *
	 * @param rollupRuleAssignment the rollup rule assignment for which to generate pseudo-assignments
	 * @return the pseudo-assignment list of rollup rule children for the given rollup assignment
	 */
	public List<ComplianceRuleAssignment> getPseudoRollupAssignmentList(ComplianceRuleAssignment rollupRuleAssignment);
}
