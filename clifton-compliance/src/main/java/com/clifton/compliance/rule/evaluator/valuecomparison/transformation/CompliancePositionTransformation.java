package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleAccountingPositionValueCalculator;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.system.bean.SystemBeanGroup;

import java.util.Date;
import java.util.List;


/**
 * The {@link SystemBeanGroup} interface for compliance position transformations to be used in {@link ComplianceRuleAccountingPositionValueCalculator} calculations.
 * <p>
 * Compliance position transformations are operations which are performed on sequences of position objects when evaluating Compliance Value Calculators. These operations transform
 * a list of positions or position groups with corresponding values into a manipulated set of positions and values. The resulting positions, groupings, or values may be changed by
 * the transformation. Available transformations may include {@link CompliancePositionGroupingAggregationTransformation grouping and aggregation operations}, {@link
 * CompliancePositionCalculatedValueFilteringTransformation filtering operations}, value retrievals, or other operations.
 * <p>
 * Transformations wrap positions and operate on them in the form of {@link CompliancePositionValueHolder} objects. These objects include information about the underlying positions
 * and the current associated value for the collection of underlying positions as a whole. Transformations may analyze the underlying positions or the produced value for the
 * positions to produce a new resulting collection of {@link CompliancePositionValueHolder}, which will then be handled by subsequent transformations or by an {@link
 * AggregationOperations aggregation operation} configured on the calculator.
 *
 * @author MikeH
 */
public interface CompliancePositionTransformation {

	/**
	 * Applies the given transformation operation to the provided list of position value holders. A new list of position value holders will be produced and returned.
	 *
	 * @param positionValueList the list of position value holders to transform
	 * @param processDate       the process date of the transformation which may be used when determining date-dependant values, such as market data
	 * @param context           the transformation context, for such things as caching
	 * @param runSubDetailList  the list of rule run details to which evaluation details should be added
	 * @return the transformed list of position value holders
	 */
	List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList);
}
