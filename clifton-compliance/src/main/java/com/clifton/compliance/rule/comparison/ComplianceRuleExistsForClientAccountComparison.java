package com.clifton.compliance.rule.comparison;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The <code>ComplianceRuleExistsForClientAccountComparison</code> class is a {@link Comparison} that evaluates whether or not
 * short long account specific rules have been assigned to an account
 *
 * @author apopp
 */
public class ComplianceRuleExistsForClientAccountComparison implements Comparison<InvestmentAccount> {

	private ComplianceRuleService complianceRuleService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Ensure a rule of this type is assigned to the account
	 */
	private Short ruleTypeId;

	/**
	 * Defines whether to check all rules, or all non-global rules (i.e. account or service specific)
	 */
	private boolean excludeGlobalRules;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(InvestmentAccount investmentAccount, ComparisonContext context) {

		boolean result = !CollectionUtils.isEmpty(getComplianceRuleService().getComplianceRuleAssignmentList(buildBaseComplianceRuleAssignmentSearchForm(investmentAccount, getRuleTypeId(), false)));

		if (!result) {
			//If no rules of this type are assigned to this account we must check roll-up rules
			//For each rollup rule check to see if there are any rules of the specified type indirectly
			// assigned to this account through a rollup
			for (ComplianceRuleAssignment complianceRuleAssignment : CollectionUtils.getIterable(getComplianceRuleService().getComplianceRuleAssignmentList(buildBaseComplianceRuleAssignmentSearchForm(investmentAccount, null, true)))) {
				result = !CollectionUtils.isEmpty(BeanUtils.filter(getComplianceRuleService().getComplianceRuleChildrenList(complianceRuleAssignment.getRule()), (ComplianceRule rule) -> rule.getRuleType().getId(), getRuleTypeId()));
				if (result) {
					break;
				}
			}
		}

		// record comparison result message
		if (context != null) {
			if (result) {
				context.recordTrueMessage("Account has required compliance rules assigned");
			}
			else {
				context.recordFalseMessage("Compliance rules are required for account to become active. Please speak with Compliance.");
			}
		}

		return result;
	}


	private ComplianceRuleAssignmentSearchForm buildBaseComplianceRuleAssignmentSearchForm(InvestmentAccount investmentAccount, Short ruleTypeId, Boolean rollupRule) {
		ComplianceRuleAssignmentSearchForm searchForm = new ComplianceRuleAssignmentSearchForm();
		searchForm.setAppliesToAccountId(investmentAccount.getId());
		if (isExcludeGlobalRules()) {
			searchForm.setGlobal(false);
		}
		searchForm.setRollupRule(rollupRule);
		searchForm.setRuleTypeId(ruleTypeId);

		return searchForm;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}


	public Short getRuleTypeId() {
		return this.ruleTypeId;
	}


	public void setRuleTypeId(Short ruleTypeId) {
		this.ruleTypeId = ruleTypeId;
	}


	public boolean isExcludeGlobalRules() {
		return this.excludeGlobalRules;
	}


	public void setExcludeGlobalRules(boolean excludeGlobalRules) {
		this.excludeGlobalRules = excludeGlobalRules;
	}
}
