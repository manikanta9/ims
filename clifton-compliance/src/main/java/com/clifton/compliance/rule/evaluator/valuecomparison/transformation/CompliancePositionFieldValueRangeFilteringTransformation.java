package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionFieldValueRangeFilteringTransformation} bean type for filtering positions based on a numeric property value range.
 * <p>
 * This transformation compares a numeric property value (specified by a field path} against an inclusive numeric range.
 * Entities matching the criteria are returned (in a list of {@link CompliancePositionValueHolder}).
 * If excludeWithinRange is set to true, any entities matching the criteria are excluded from the returned list.
 * The MinValue and MaxValue properties define the comparison range, and minimally, one range endpoint must be defined.
 * Note that a null MinValue is treated as an infinite negative numeric value and a null MaxValue is treated as an infinite positive numeric value.
 *
 * @author DavidI
 */
public class CompliancePositionFieldValueRangeFilteringTransformation implements CompliancePositionTransformation, ValidationAware {

	private String numericField;

	private BigDecimal minValue;

	private BigDecimal maxValue;

	private boolean excludeWithinRange;

	private CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return CollectionUtils.getStream(positionValueList).filter(valueHolder -> this.applyFilterCondition(valueHolder, processDate, runSubDetailList)).collect(Collectors.toList());
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getNumericField(), "A path to the numeric field used for filtering is required.");
		ValidationUtils.assertFalse(getMinValue() == null && getMaxValue() == null, "At least one field value must be set (Minimum or maximum value).");

		if (getMinValue() != null && getMaxValue() != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(getMaxValue(), getMinValue()), "The maximum value must be greater than or equal to the minimum value.");
		}

		getCompliancePositionPropertyPathHandler().validateField(getNumericField());

		ValidationUtils.assertTrue(getCompliancePositionPropertyPathHandler().fieldDataTypeIsNumeric(getNumericField()), "The data type for the selected field must be numeric.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean applyFilterCondition(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		validatePositionValueHolder(positionValueHolder, processDate);
		BigDecimal propertyValue = null;
		Object valueHolder = getCompliancePositionPropertyPathHandler().getPositionPropertyValue(positionValueHolder.getPositionList().get(0), getNumericField(), processDate);
		if (valueHolder != null) {
			propertyValue = new BigDecimal(valueHolder.toString());
		}

		// Inclusive range check.  The isBetween function will consider a null property value as out of range.
		boolean valueInRange = MathUtils.isBetween(propertyValue, getMinValue(), getMaxValue());

		String displayValue = propertyValue == null ? "missing" : CoreMathUtils.formatNumberDecimal(propertyValue);
		String tickerSymbol = positionValueHolder.getPositionList().get(0).getInvestmentSecurity().getSymbol();
		String displayMessage;

		// Create a display message indicating the value and the range.
		if (valueInRange) {
			displayMessage = String.format("[%s] value [%s] for [%s] is in range [%s] to [%s].", getNumericField(), displayValue, tickerSymbol, CoreMathUtils.formatNumberDecimal(getMinValue()), CoreMathUtils.formatNumberDecimal(getMaxValue()));
		}
		else {
			displayMessage = String.format("[%s] value [%s] for [%s] is out of range [%s] to [%s].", getNumericField(), displayValue, tickerSymbol, CoreMathUtils.formatNumberDecimal(getMinValue()), CoreMathUtils.formatNumberDecimal(getMaxValue()));
		}
		runSubDetailList.add(ComplianceRuleRunSubDetail.create(displayMessage));

		return isExcludeWithinRange() ? !valueInRange : valueInRange;
	}


	private void validatePositionValueHolder(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, Date processDate) {

		// Convert position to a distinct list of LastDeliveryDate or EndDate (depending on the DateField property).  Ensure there is one and only one date value.
		List<Object> propertyValueList = CollectionUtils.getStream(positionValueHolder.getPositionList())

				// Obtain the property map for the position
				.map(position -> getCompliancePositionPropertyPathHandler().getPositionPropertyValue(position, getNumericField(), processDate))
				// Find all different property maps
				.distinct()
				.collect(Collectors.toList());

		ValidationUtils.assertEquals(1, propertyValueList.size(), "The specified property used for filtering must have the same value for all positions.");
		// if the value is null, process it as a null value, but if it is not null, ensure it is numeric. In the case of MarketData, we already know that the target property
		// MarketDataValueHolder.measureValue is always numeric.  Should the MarketDataValueHolder or measureValue be null, we will process this as a null value.
		if (propertyValueList.get(0) != null && !(propertyValueList.get(0) instanceof MarketDataValueHolder)) {
			ValidationUtils.assertTrue(propertyValueList.get(0) instanceof Number, "The specified property's value must be numeric.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getNumericField() {
		return this.numericField;
	}


	public void setNumericField(String numericField) {
		this.numericField = numericField;
	}


	public BigDecimal getMinValue() {
		return this.minValue;
	}


	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}


	public BigDecimal getMaxValue() {
		return this.maxValue;
	}


	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}


	public boolean isExcludeWithinRange() {
		return this.excludeWithinRange;
	}


	public void setExcludeWithinRange(boolean excludeWithinRange) {
		this.excludeWithinRange = excludeWithinRange;
	}


	public CompliancePositionPropertyPathHandler getCompliancePositionPropertyPathHandler() {
		return this.compliancePositionPropertyPathHandler;
	}


	public void setCompliancePositionPropertyPathHandler(CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler) {
		this.compliancePositionPropertyPathHandler = compliancePositionPropertyPathHandler;
	}
}

