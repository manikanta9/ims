package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * The <code>SharesOutstandingRule</code> is a generic shares outstanding evaluation rule.
 * <p>
 * It provides the user many ways to determine which securities are evaluated by this rule.  It will
 * ensure we do not own too much percentage of the total outstanding voting shares of a given security.
 * <p>
 * The flexibility of this generic shares outstanding rule will be used for SRB, RIC, and various
 * investment/security group evaluations. (Can't own more than 3% shares outstanding of any ETF issued by PIMCO, etc)
 *
 * @author apopp
 */
public class SharesOutstandingRule extends BaseComplianceRuleRatioEvaluator {

	private InvestmentGroupService investmentGroupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	/**
	 * If set, further restrict securities to ones falling within this investment group for processing
	 */
	private Short investmentGroupId;

	/**
	 * If set, further restrict securities to this security group
	 */
	private Short securityGroupId;

	/**
	 * If set, further restrict securities to ones remaining whose ultimate issuer is this business company
	 */
	private Integer businessCompanyId;

	/**
	 * If not null, only allow securities whose business company type is equal to this
	 */
	private Short businessCompanyTypeId;

	/**
	 * If true, further restrict securities to ones that are classified as SRB (Security Related Business) securities
	 */
	private boolean srbOnly;

	/**
	 * If true group securities by ultimate issuer. The securities grouped will be the total list of securities
	 * filtered by any defined filters.
	 */
	private boolean groupByUltimateIssuer;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String description = "Missing";
		if (ruleEvaluationConfig.getInvestmentSecurityId() != null) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId());
			description = numerator + " of " + security.getLabel() + " out of " + denominator;
		}
		else if (ruleEvaluationConfig.getIssuer() != null) {
			description = numerator + " of " + ruleEvaluationConfig.getIssuer().getLabel() + " out of " + denominator;
		}

		return description;
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionsMap = ComplianceUtils.mapPositionListBySecurity(ruleEvaluationConfig.getPositionList());

		InvestmentGroup investmentGroup = (getInvestmentGroupId() != null) ? getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupId()) : null;
		BusinessCompany businessCompany = (getBusinessCompanyId() != null) ? getBusinessCompanyService().getBusinessCompany(getBusinessCompanyId()) : null;
		BusinessCompanyType businessCompanyType = (getBusinessCompanyTypeId() != null) ? getBusinessCompanyService().getBusinessCompanyType(getBusinessCompanyTypeId()) : null;

		Iterator<InvestmentSecurity> itr = securityPositionsMap.keySet().iterator();

		while (itr.hasNext()) {
			InvestmentSecurity security = itr.next();
			boolean isAllowed = true;

			if (investmentGroup != null && !getInvestmentGroupService().isInvestmentSecurityInGroup(investmentGroup.getName(), security.getId())) {
				isAllowed = false;
			}

			if (getSecurityGroupId() != null && !getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), getSecurityGroupId())) {
				isAllowed = false;
			}

			if (businessCompany != null && !businessCompany.equals(security.getBusinessCompany().getRootParent())) {
				isAllowed = false;
			}

			if (isSrbOnly()) {
				if (!getComplianceRuleFilterHandler().isSecuritySRB(security)) {
					isAllowed = false;
				}
			}

			if (businessCompanyType != null) {
				BusinessCompanyType securityBusinessType = (security.getBusinessCompany() != null) ? security.getBusinessCompany().getType() : null;

				if (!businessCompanyType.equals(securityBusinessType)) {
					isAllowed = false;
				}
			}

			if (!isAllowed) {
				itr.remove();
			}
		}

		if (isGroupByUltimateIssuer()) {

			Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap = ComplianceUtils.mapUltimateIssuerBySecurityPositionMap(securityPositionsMap);

			for (Map.Entry<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> businessCompanyMapEntry : issuerSecurityPositionMap.entrySet()) {
				ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
				config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
				config.setProcessDate(ruleEvaluationConfig.getProcessDate());
				config.setIssuer(businessCompanyMapEntry.getKey());
				config.setSharePosition(true);
				config.setCleanContext(true);
				config.setPositionList(ComplianceUtils.combinePositionCollectionList(businessCompanyMapEntry.getValue().values()));
				configList.add(config);
			}
		}
		else {
			for (InvestmentSecurity security : securityPositionsMap.keySet()) {
				ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
				config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
				config.setProcessDate(ruleEvaluationConfig.getProcessDate());
				config.setInvestmentSecurityId(security.getId());
				config.setSharePosition(true);
				configList.add(config);
			}
		}

		return configList;
	}


	private BigDecimal totalOutstandingStockByIssuer(List<InvestmentSecurity> investmentSecurityList, Date processDate) {
		BigDecimal value = BigDecimal.ONE;
		for (InvestmentSecurity security : CollectionUtils.getIterable(investmentSecurityList)) {
			value = value.add(getComplianceBaseHandler().calculateSharesOutstanding(security, processDate));
		}

		return value;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap;

		if (ruleEvaluationConfig.getInvestmentSecurityId() != null) {
			List<Integer> securityIdList = CollectionUtils.createList(ruleEvaluationConfig.getInvestmentSecurityId());
			securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());
		}
		else {

			securityPositionMap = ComplianceUtils.mapPositionListBySecurity(ruleEvaluationConfig.getPositionList());
		}

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityPositionMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.SHARES_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) throws ValidationException {
		if (ruleEvaluationConfig.getInvestmentSecurityId() == null) {
			List<InvestmentSecurity> uniqueSecurityList = new ArrayList<>(ComplianceUtils.mapPositionListBySecurity(ruleEvaluationConfig.getPositionList()).keySet());
			return totalOutstandingStockByIssuer(uniqueSecurityList, ruleEvaluationConfig.getProcessDate());
		}
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId());
		return getComplianceBaseHandler().calculateSharesOutstanding(security, ruleEvaluationConfig.getProcessDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Short getBusinessCompanyTypeId() {
		return this.businessCompanyTypeId;
	}


	public void setBusinessCompanyTypeId(Short businessCompanyTypeId) {
		this.businessCompanyTypeId = businessCompanyTypeId;
	}


	public boolean isSrbOnly() {
		return this.srbOnly;
	}


	public void setSrbOnly(boolean srbOnly) {
		this.srbOnly = srbOnly;
	}


	public boolean isGroupByUltimateIssuer() {
		return this.groupByUltimateIssuer;
	}


	public void setGroupByUltimateIssuer(boolean groupByUltimateIssuer) {
		this.groupByUltimateIssuer = groupByUltimateIssuer;
	}
}
