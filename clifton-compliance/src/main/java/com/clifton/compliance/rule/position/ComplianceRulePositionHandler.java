package com.clifton.compliance.rule.position;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.context.Context;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>ComplianceRulePositionHandler</code> handles special manipulations of positions for compliance rules.
 *
 * @author apopp
 */
public interface ComplianceRulePositionHandler {


	/**
	 * Retrieves the list of positions for the given client account on the provided transaction date.
	 * <p>
	 * This method considers the subaccount and subsidiary account relationship purposes from the {@link ComplianceRuleAssignment}. Positions from accounts tied by these purposes
	 * are included in the resulting list.
	 *
	 * @param context         the cache context with which to search for positions
	 * @param ruleAssignment
	 * @param clientAccountId the client account ID
	 * @param transactionDate the applicable date for the positions
	 * @return the list of positions
	 */
	public List<AccountingPosition> getAccountingPositionList(Context context, ComplianceRuleAssignment ruleAssignment, int clientAccountId, Date transactionDate);


	/**
	 * Assist method for retrieving the entire position list for a specific account on a given date. It will
	 * attempt to retrieve the position list from the context first.
	 *
	 * @param context
	 * @param ruleAssignment
	 * @param clientAccountId
	 * @param date
	 */
	public List<ComplianceRulePosition> getPositionListForAccountByRule(Context context, ComplianceRuleAssignment ruleAssignment, Integer clientAccountId, Date date);


	/**
	 * Given a list of accounts return back a single list of all positions under all listed accounts on a given date
	 */
	public List<ComplianceRulePosition> getAccountingPositionListPopulatedByAccountList(Context context, List<Integer> accountIdList, Date transactionDate);


	/**
	 * Returns a list of all positions falling under the root account
	 * along with positions from direct sub accounts. This will also
	 * take positions from subsidiary accounts and pseudo securitize them
	 *
	 * @param clientAccountId
	 * @param transactionDate
	 * @param subsidiaryPurposeId
	 * @param subaccountPurposeId
	 */
	public List<ComplianceRulePosition> getAccountingPositionListFullPopulatedForClientAccount(Context context, int clientAccountId, Date transactionDate, Short subsidiaryPurposeId, Short subaccountPurposeId);


	/**
	 * Retrieve cash balance for a given account id
	 */
	public BigDecimal getAccountCashBalanceNetOrTotal(Context context, Integer clientAccountId, Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral);


	/**
	 * Retrieve total cash balance for a given list of account ids
	 */
	public BigDecimal getAccountCashBalanceNetOrTotalByAccountList(Context context, List<Integer> clientAccountIdList, Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral);


	/**
	 * Return back total value of a list of positions
	 * given the positions, process date, and a way to calculate position value.
	 */
	public BigDecimal getPositionListValueTotal(Context context, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues);


	/**
	 * Calculate the value of a given position
	 */
	public BigDecimal getValueForPosition(Context context, ComplianceRulePosition position, ComplianceRulePositionValues complianceRulePositionValues, Date processDate);


	/**
	 * Returns total account value used by most denominator calculations
	 *
	 * @param clientAccountId
	 * @param positionList
	 * @param processDate
	 * @param complianceRulePositionValues
	 * @param subsidiaryPurposeId
	 * @param subaccountPurposeId
	 */
	public BigDecimal getAccountTotalValue(Context context, Integer clientAccountId, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues,
	                                       Short subsidiaryPurposeId, Short subaccountPurposeId, boolean useTotalAssets, boolean excludeCashCollateral);


	/**
	 * Returns total account value used by most denominator calculations via an account list
	 *
	 * @param positionList
	 * @param processDate
	 * @param complianceRulePositionValues
	 */
	public BigDecimal getAccountTotalValue(Context context, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues, List<Integer> accountIdList,
	                                       boolean useTotalAssets, boolean excludeCashCollateral);


	/**
	 * Return total value of account (Cash + Value of positions)
	 * if specified, also rolling up subsidiary/subaccount values
	 *
	 * @param clientAccountId
	 * @param complianceRulePositionValues
	 * @param transactionDate
	 * @param isNetOnly
	 * @param excludeCashCollateral
	 * @param subsidiaryPurposeId
	 * @param subaccountPurposeId
	 */
	public BigDecimal getAccountValueTotalValue(Context context, Integer clientAccountId, ComplianceRulePositionValues complianceRulePositionValues, Date transactionDate, boolean isNetOnly,
	                                            boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId);


	/**
	 * Return total value of account (Cash + Value of positions specified)
	 * if specified, also rolling up subsidiary/subaccount cash values
	 */
	public BigDecimal getAccountValueTotalValueByPositionList(Context context, Integer clientAccountId, List<ComplianceRulePosition> positionList, ComplianceRulePositionValues complianceRulePositionValues,
	                                                          Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId);


	/**
	 * Return percent the supplied value is of the total account assets
	 */
	public BigDecimal calculatePercentOfAssets(Context context, BigDecimal value, Integer clientAccountId, List<ComplianceRulePosition> positionList, ComplianceRulePositionValues complianceRulePositionValues,
	                                           Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId);
}
