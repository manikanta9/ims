package com.clifton.compliance.rule.assignment;


import com.clifton.business.service.BusinessService;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.util.Date;


/**
 * The <code>ComplianceRuleAssignment</code> class defines a relationship between a rule and which account
 * it is assigned to.
 *
 * @author apopp
 */
public class ComplianceRuleAssignment extends BaseEntity<Integer> implements SystemEntityModifyConditionAware {

	/**
	 * Rule to be assigned
	 */
	private ComplianceRule rule;

	/**
	 * The {@link BusinessService} for the assignment. The assigned rule will be triggered for all accounts tied to this service on execution.
	 */
	private BusinessService businessService;

	/**
	 * Client Account the rule is assigned to, global if null
	 */
	private InvestmentAccount clientInvestmentAccount;

	/**
	 * If specified, will also include positions from accounts related to account being verified on valuation date.
	 */
	private InvestmentAccountRelationshipPurpose subAccountPurpose;

	/**
	 * If specified, will treat related accounts as wholly owned subsidiaries: one 'security' as opposed to individual positions.
	 */
	private InvestmentAccountRelationshipPurpose subsidiaryPurpose;

	/**
	 * If defined, and the condition returns true, any rule results for this assignment will be marked as ignored if that rule fails
	 */
	private SystemCondition ignoreFailSystemCondition;

	/**
	 * If defined, and the condition returns true, any rule results for this assignment will be marked as ignored if that rule passes
	 */
	private SystemCondition ignorePassSystemCondition;

	/**
	 * Start and End Date range this rule applies over. Use null to no start or end date.
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * Optional note that describes any special information for this assignment
	 */
	private String note;

	/**
	 * Flag indicating if this assignment is an exclusion (i.e. the rule will not be executed for the specified targets)
	 */
	private boolean exclusionAssignment;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getRule().getEntityModifyCondition();
	}


	public String getScopeLabel() {
		LabeledObject object = ObjectUtils.coalesce(getClientInvestmentAccount(), getBusinessService());
		return (object == null) ? "Global Assignment" : object.getLabel();
	}


	public boolean isBusinessServiceAssignment() {
		return getBusinessService() != null;
	}


	public boolean isGlobal() {
		return getClientInvestmentAccount() == null && getBusinessService() == null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRule getRule() {
		return this.rule;
	}


	public void setRule(ComplianceRule rule) {
		this.rule = rule;
	}


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccountRelationshipPurpose getSubAccountPurpose() {
		return this.subAccountPurpose;
	}


	public void setSubAccountPurpose(InvestmentAccountRelationshipPurpose subAccountPurpose) {
		this.subAccountPurpose = subAccountPurpose;
	}


	public InvestmentAccountRelationshipPurpose getSubsidiaryPurpose() {
		return this.subsidiaryPurpose;
	}


	public void setSubsidiaryPurpose(InvestmentAccountRelationshipPurpose subsidiaryPurpose) {
		this.subsidiaryPurpose = subsidiaryPurpose;
	}


	public SystemCondition getIgnoreFailSystemCondition() {
		return this.ignoreFailSystemCondition;
	}


	public void setIgnoreFailSystemCondition(SystemCondition ignoreFailSystemCondition) {
		this.ignoreFailSystemCondition = ignoreFailSystemCondition;
	}


	public SystemCondition getIgnorePassSystemCondition() {
		return this.ignorePassSystemCondition;
	}


	public void setIgnorePassSystemCondition(SystemCondition ignorePassSystemCondition) {
		this.ignorePassSystemCondition = ignorePassSystemCondition;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public boolean isExclusionAssignment() {
		return this.exclusionAssignment;
	}


	public void setExclusionAssignment(boolean exclusionAssignment) {
		this.exclusionAssignment = exclusionAssignment;
	}
}
