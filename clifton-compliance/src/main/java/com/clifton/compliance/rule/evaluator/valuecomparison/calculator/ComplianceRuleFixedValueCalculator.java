package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.valuecomparison.ComplianceRuleValueComparisonEvaluator;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;

import java.math.BigDecimal;
import java.util.List;


/**
 * The {@link ComplianceRuleFixedValueCalculator} is an implementor of {@link ComplianceRuleValueCalculator}. This type allows for supplying fixed values to aggregate with
 * calculations.
 * <p>
 * The value given in this calculator will be appended to the aggregated sum generated by {@link ComplianceRuleValueComparisonEvaluator}.
 *
 * @author MikeH
 */
public class ComplianceRuleFixedValueCalculator implements ComplianceRuleValueCalculator {

	private BigDecimal value;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return getValue();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}
}
