package com.clifton.compliance.rule.evaluator.valuecomparison;

/**
 * The compliance implementation of {@link BaseRuleValueComparisonEvaluator} for value comparison rule evaluations.
 *
 * @author MikeH
 */
public class ComplianceRuleValueComparisonEvaluator extends BaseRuleValueComparisonEvaluator {

}
