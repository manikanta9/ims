package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.calculator.AccountingPositionValueCalculator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.evaluator.valuecomparison.transformation.CompliancePositionGroupingAggregationTransformation;
import com.clifton.compliance.rule.evaluator.valuecomparison.transformation.CompliancePositionTransformation;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The implementation of {@link BaseComplianceRuleAccountingPositionValueCalculator} which produces calculated values for {@link AccountingPosition} entities.
 * <p>
 * This calculator uses a configured {@link AccountingPositionValueCalculator} to derive a value for each position. The position values are then passed through a configurable set
 * of {@link CompliancePositionTransformation} beans to transform their values before aggregation.
 * <p>
 * Using {@link AccountingPosition} entities provides a means to retrieve live data for positions, while transformations allow the calculator to perform complex operations, such as
 * grouping and filtering to separate and aggregate sub-values before producing a final result.
 *
 * @author MikeH
 */
public class ComplianceRuleAccountingPositionValueCalculator extends BaseComplianceRuleAccountingPositionValueCalculator<AccountingPosition> implements ValidationAware {

	private ComplianceRuleReportHandler complianceRuleReportHandler;

	/**
	 * The position calculator type to use to retrieve the position value. This calculator determines the evaluation logic to derive a value from each position.
	 *
	 * @see #getPositionValue(AccountingPosition, Date, Context)
	 */
	private SystemBean positionValueCalculatorBean;
	/**
	 * The list of transformations to apply to the calculated values for each retrieved position after individual value modifications are performed. These transformations will be
	 * evaluated in the order provided.
	 *
	 * @see BaseComplianceRuleAccountingPositionValueCalculator#applyPositionTransformations(List, Date, Context, List)
	 */
	private List<SystemBean> transformationList;

	/*
	 * These fields are not used and are present for display purposes only. These fields are added to this class only for system bean validation purposes.
	 */
	private Boolean filterOptionsHeader;
	private Boolean calculatorOptionsHeader;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<AccountingPosition> getPositionList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return ruleEvaluationConfig.getAccountingPositionList();
	}


	@Override
	protected BigDecimal getPositionValue(AccountingPosition position, Date transactionDate, Context context) {
		AccountingPositionValueCalculator positionValueCalculator = generatePositionValueCalculator(getPositionValueCalculatorBean());
		return positionValueCalculator.calculate(position, transactionDate);
	}


	@Override
	protected AccountingTransactionInfo getAccountingTransactionInfo(AccountingPosition position) {
		return position;
	}


	@Override
	protected List<CompliancePositionValueHolder<AccountingPosition>> applyPositionTransformations(List<CompliancePositionValueHolder<AccountingPosition>> positionValueHolderList, Date transactionDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		List<CompliancePositionValueHolder<AccountingPosition>> transformedPositionValueHolderList = positionValueHolderList;
		for (SystemBean transformationBean : CollectionUtils.getIterable(getTransformationList())) {
			CompliancePositionTransformation transformation = generateTransformation(transformationBean);
			List<ComplianceRuleRunSubDetail> transformationMessageList = new ArrayList<>();
			transformedPositionValueHolderList = transformation.apply(transformedPositionValueHolderList, transactionDate, context, transformationMessageList);
			ComplianceRuleRunSubDetail transformationSubDetail = ComplianceRuleRunSubDetail.create(String.format("%s (Size: %d)", transformationBean.getLabelShort(), transformedPositionValueHolderList.size()));
			transformationSubDetail.setCauseEntity(transformationBean);
			transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Messages (Size: %d)", transformationMessageList.size()), transformationMessageList));
			transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Results (Size: %d)", transformedPositionValueHolderList.size()), getComplianceRuleReportHandler().createSubDetailForValueHolderList(transformedPositionValueHolderList)));
			runSubDetailList.add(transformationSubDetail);
		}
		return transformedPositionValueHolderList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		// Verify that all groupings are subsets of all parent groupings
		List<String> parentGroupings = null;
		for (SystemBean transformationBean : CollectionUtils.getIterable(getTransformationList())) {
			CompliancePositionTransformation transformation = generateTransformation(transformationBean);
			if (transformation instanceof CompliancePositionGroupingAggregationTransformation) {
				CompliancePositionGroupingAggregationTransformation groupingTransformation = (CompliancePositionGroupingAggregationTransformation) transformation;
				List<String> stageGroupings = Arrays.asList(groupingTransformation.getGroupingFields());
				if (parentGroupings != null && !parentGroupings.containsAll(stageGroupings)) {
					ValidationUtils.fail("The fields for the grouping transformation must be a subset of all previous grouping fields. Transformation: [%s]. Found grouping fields: [%s]. Available grouping fields: [%s]", transformationBean.getName(), StringUtils.join(stageGroupings, ", "), StringUtils.join(parentGroupings, ", "));
				}
				parentGroupings = stageGroupings;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the {@link AccountingPositionValueCalculator} from the given bean.
	 *
	 * @param calculatorBean the bean defining the calculator configuration
	 * @return the generated calculator instance
	 */
	private AccountingPositionValueCalculator generatePositionValueCalculator(SystemBean calculatorBean) {
		return (AccountingPositionValueCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
	}


	/**
	 * Generates the {@link CompliancePositionTransformation} entity from the given bean. This bean must correspond to the correct entity type.
	 *
	 * @param transformationBean the {@link SystemBean} entity
	 * @return the {@link CompliancePositionTransformation} entity
	 */
	private CompliancePositionTransformation generateTransformation(SystemBean transformationBean) {
		return (CompliancePositionTransformation) getSystemBeanService().getBeanInstance(transformationBean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public SystemBean getPositionValueCalculatorBean() {
		return this.positionValueCalculatorBean;
	}


	public void setPositionValueCalculatorBean(SystemBean positionValueCalculatorBean) {
		this.positionValueCalculatorBean = positionValueCalculatorBean;
	}


	public List<SystemBean> getTransformationList() {
		return this.transformationList;
	}


	public void setTransformationList(List<SystemBean> transformationList) {
		this.transformationList = transformationList;
	}


	public Boolean getFilterOptionsHeader() {
		return this.filterOptionsHeader;
	}


	public void setFilterOptionsHeader(Boolean filterOptionsHeader) {
		this.filterOptionsHeader = filterOptionsHeader;
	}


	public Boolean getCalculatorOptionsHeader() {
		return this.calculatorOptionsHeader;
	}


	public void setCalculatorOptionsHeader(Boolean calculatorOptionsHeader) {
		this.calculatorOptionsHeader = calculatorOptionsHeader;
	}
}
