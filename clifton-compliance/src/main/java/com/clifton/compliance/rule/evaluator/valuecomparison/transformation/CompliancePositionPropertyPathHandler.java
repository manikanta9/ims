package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.trade.Trade;

import java.util.Date;


/**
 * The interface for a class that manages the lookup of a property value from an {@link AccountingPosition}, given a delimited property path to identify the
 * desired property.
 * <p>
 * Supported property types include {@link AccountingPosition} fields, {@link SystemColumnCustomValueAware custom column values} for navigable entities, {@link MarketDataValue} values,
 * and opening {@link Trade} values.
 * <p>
 * See {@link #getPositionPropertyValue(AccountingPosition, String, Date)} for more details.
 *
 * @author Davidi
 */
public interface CompliancePositionPropertyPathHandler {

	// Non-flexible market data prefix, only looks up data for processing date
	public static final String MARKET_DATA_FIELD_NAME_PREFIX = "#";

	// Flexible market data prefix - looks up market data for current date, or if not available, uses market data from previous business day's close.
	public static final String FLEXIBLE_MARKET_DATA_FIELD_NAME_PREFIX = "##";

	public static final String CUSTOM_COLUMN_NAME_DELIMITER = "%";
	public static final String OPENING_TRADE_PREFIX = "!";
	public static final String TRADE_MARKET_DATA_FIELD_NAME_PREFIX = "!#";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets an individual property value for the given position.
	 * <p>
	 * This field shall be evaluated as a property path for the given positions. For example, <tt>investmentSecurity.id</tt> may be used to group the fields by the security ID.
	 * Special field syntaxes may be used to indicate special field types:
	 * <ul>
	 * <li><b>{@value #MARKET_DATA_FIELD_NAME_PREFIX}</b> (used as prefix): Market data field names for non-flexible queries. For example, <tt>#Delta</tt>.
	 * <li><b>{@value #FLEXIBLE_MARKET_DATA_FIELD_NAME_PREFIX}</b> (used as prefix): Market data field names for flexible queries. For example, <tt>##Delta</tt>.
	 * <li><b>{@value #CUSTOM_COLUMN_NAME_DELIMITER}</b> (used as delimiter): Custom column names. For example, <tt>investmentSecurity%Put Or Call</tt>.
	 * <li><b>{@value #OPENING_TRADE_PREFIX}</b> (used as prefix): A prefix for opening trade property names. For example, <tt>!executingSponsorCompany</tt>.
	 * <li><b>{@value #TRADE_MARKET_DATA_FIELD_NAME_PREFIX}</b> (used as prefix): A prefix for trade additional data property names. For example, <tt>!#Delta - On Executed</tt>.
	 * </ul>
	 *
	 * @param position        the position for which to retrieve the value
	 * @param field           the field name or path whose value shall be retrieved
	 * @param transactionDate the date for which the value shall be retrieved (relevant for market data only)
	 * @return the value of the given field
	 */
	public Object getPositionPropertyValue(AccountingPosition position, String field, Date transactionDate);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates an individual grouping field.
	 *
	 * @param field the field to validate
	 */
	public void validateField(String field);


	/**
	 * A method that returns true if the property designated in the field path is of type Number or a subclass of Number.  Primitive numeric types such as
	 * byte, int, long, decimal, float, and double also evaluate to true. Any Non-numeric data will evaluate as false.
	 * Useful in instances where additional type validation of the field is needed in the absence of field data.
	 *
	 * @param field the field path
	 */
	public boolean fieldDataTypeIsNumeric(String field);


	/**
	 * A method that returns true if the property designated in the field path is of type String. Otherwise returns false.
	 * Useful in instances where additional type validation of the field is needed in the absence of field data.
	 *
	 * @param field the field path
	 */
	public boolean fieldDataTypeIsString(String field);


	/**
	 * A method that returns true if the property designated in the field path is of type Date. Otherwise returns false.
	 * Useful in instances where additional type validation of the field is needed in the absence of field data.
	 *
	 * @param field the field path
	 */
	public boolean fieldDataTypeIsDate(String field);


	/**
	 * A method that returns true if the property designated in the field path is of type Boolean or primitive type boolean. Otherwise returns false.
	 * Useful in instances where additional type validation of the field is needed in the absence of field data.
	 *
	 * @param field the field path
	 */
	public boolean fieldDataTypeIsBoolean(String field);
}
