package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleCreditRatingEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * Implementor for {@link BaseComplianceRuleCreditRatingEvaluator}. Compliance rule evaluator type for evaluating credit ratings for counterparties
 * (companies).
 * <p>
 * This evaluator validates credit ratings against specified requirements for counterparties of holding accounts corresponding to the client account being
 * processed.
 * <p>
 * The target of this evaluator is the holding account. The credit rating value assignee is the counterparty or list of counterparties for the holding account.
 *
 * @author MikeH
 */
public class ComplianceRuleCompanyCreditRatingEvaluator extends BaseComplianceRuleCreditRatingEvaluator<InvestmentAccount, BusinessCompany> {

	/**
	 * The context key for the cache indicating whether a holding account has holdings under the given specifications.
	 *
	 * @see #getAccountPopulatedCache(ComplianceRuleEvaluationConfig)
	 */
	private static final String CONTEXT_HOLDING_ACCOUNT_POPULATED_CACHE_KEY = "CONTEXT_HOLDING_ACCOUNT_POPULATED_CACHE";


	private InvestmentAccountService investmentAccountService;
	private BusinessContractService businessContractService;
	private AccountingBalanceService accountingBalanceService;


	/**
	 * The holding account types to include when gathering counterparties to validate.
	 */
	private Short[] accountTypeIds;

	/**
	 * If {@code true}, this rule will validate all companies. Otherwise, this rule will only validate companies given by {@link #companyIds}.
	 *
	 * @see #companyIds
	 */
	private boolean validateAllCompanies;

	/**
	 * The company filter for this rule. This rule will only check credit ratings of the given companies.
	 *
	 * @see #validateAllCompanies
	 */
	private Integer[] companyIds;

	/**
	 * If {@code true}, counterparties for active holding accounts with no holdings will be included in the list of companies to validate. Otherwise, only
	 * counterparties for holding accounts with current holdings will be validated.
	 */
	private boolean includeEmptyAccounts;

	/**
	 * If {@code true}, parties labeled as credit support providers for holding accounts associated with the client will be included for validation. By default,
	 * only parties labeled as counterparties will be included for validation.
	 */
	private boolean includeCreditSupportProviders;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected InvestmentAccount getEvaluationTarget(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getInvestmentAccountService().getInvestmentAccount(ruleEvaluationConfig.getClientAccountId());
	}


	@Override
	protected List<BusinessCompany> getTargetAssigneeList(InvestmentAccount target, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		final List<InvestmentAccount> holdingAccountList;
		if (ruleEvaluationConfig.isRealTime()) {
			// Real-time run: evaluate counterparty for trade holding account only
			holdingAccountList = Collections.singletonList(ruleEvaluationConfig.getBean().getHoldingInvestmentAccount());
		}
		else {
			// Batch run: get holding accounts corresponding to the current client account
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setMainAccountId(target.getId());
			searchForm.setAccountTypeIds(getAccountTypeIds());
			holdingAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		}

		// Get counterparties for evaluation
		Map<AccountBalanceKey, Boolean> accountPopulatedCache = getAccountPopulatedCache(ruleEvaluationConfig);
		return CollectionUtils.getStream(holdingAccountList)
				// Get included holding accounts
				.filter(holdingAccount -> isIncludedAccount(holdingAccount, ruleEvaluationConfig.getProcessDate(), accountPopulatedCache))
				// Get included counterparties
				.map(this::getAccountCompanyList)
				.flatMap(Collection::stream).distinct()
				.filter(this::isIncludedCompany)
				.collect(Collectors.toList());
	}


	@Override
	protected List<ComplianceCreditRating> getAssigneeCreditRatingList(BusinessCompany assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Get credit rating values for assignee
		List<ComplianceCreditRatingValueForIssuer> creditRatingValueList = getComplianceCreditRatingService().getComplianceCreditRatingValueForIssuersForIssuer(assignee.getId());

		// Get active credit ratings
		return CollectionUtils.getStream(creditRatingValueList)
				.filter(creditRatingValue -> DateUtils.isDateBetween(ruleEvaluationConfig.getProcessDate(),
						creditRatingValue.getStartDate(), creditRatingValue.getEndDate(), false))
				.map(ComplianceCreditRatingValue::getCreditRating)
				.collect(Collectors.toList());
	}


	@Override
	protected String getTargetType() {
		return "counterparty";
	}


	@Override
	protected String getAssigneeType() {
		return "counterparty";
	}


	@Override
	protected boolean isEvaluationRequired(BusinessCompany assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return true;
	}


	@Override
	protected String getDetailDescriptionPrefix(InvestmentAccount target, BusinessCompany assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return "Counterparty";
	}


	@Override
	protected String getAssigneeLabel(BusinessCompany assignee) {
		StringBuilder labelSb = new StringBuilder();
		labelSb.append(assignee.getName());
		if (assignee.getAbbreviation() != null) {
			labelSb.append(" (").append(assignee.getAbbreviation()).append(')');
		}
		return labelSb.toString();
	}


	@Override
	public void validate() throws ValidationException {
		super.validate();
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(getAccountTypeIds()), "At least one account type must be selected for validation.", "accountTypeIds");
		ValidationUtils.assertFalse(!isValidateAllCompanies() && ArrayUtils.isEmpty(getCompanyIds()), "At least one company must be selected for validation.", "companyIds");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines if the given account should be included for counterparty validation on the given process date.
	 * <p>
	 * The following criteria must all be qualified for an account to be included for validation:
	 * <ul>
	 * <li>The account is of one of the configured types ({@link #accountTypeIds})
	 * <li>The account is active <i>or</i> the account is non-empty and {@link #includeEmptyAccounts} is {@code true}
	 * </ul>
	 *
	 * @param holdingAccount        the holding account to evaluate
	 * @param processDate           the date on which to check if the account is included for processing
	 * @param accountPopulatedCache the cache with which to store or retrieve the flag for the presence of account holdings
	 * @return {@code true} if the account counterparties should be validated against credit rating criteria, or {@code false} otherwise
	 */
	private boolean isIncludedAccount(InvestmentAccount holdingAccount, Date processDate, Map<AccountBalanceKey, Boolean> accountPopulatedCache) {
		// Guard-clause: Include accounts of selected types only
		if (!ArrayUtils.contains(getAccountTypeIds(), BeanUtils.getBeanIdentity(holdingAccount.getType()))) {
			return false;
		}

		return (isIncludeEmptyAccounts() && holdingAccount.isActive())
				|| isAccountPopulated(holdingAccount, processDate, accountPopulatedCache);
	}


	/**
	 * Determines whether the given account has holdings on the specified day.
	 * <p>
	 * A somewhat expensive query is required to determine whether an account is empty. Caching is used to reduce this impact via the specified {@code
	 * accountPopulatedCache} cache.
	 *
	 * @param account               the account to evaluate for the presence of holdings
	 * @param processDate           the day on which the account holdings shall be analyzed
	 * @param accountPopulatedCache the cache with which to store or retrieve results
	 * @return {@code true} if the account has holdings on the given day, or {@code false} otherwise
	 */
	private boolean isAccountPopulated(InvestmentAccount account, Date processDate, Map<AccountBalanceKey, Boolean> accountPopulatedCache) {
		AccountBalanceKey accountBalanceKey = new AccountBalanceKey(account.getId(), processDate);
		Boolean accountPopulated = accountPopulatedCache.computeIfAbsent(accountBalanceKey, key -> {
			// Get any active holdings for the account
			AccountingBalanceSearchForm balanceSearchForm = AccountingBalanceSearchForm.onTransactionDate(processDate);
			balanceSearchForm.setHoldingInvestmentAccountId(account.getId());
			balanceSearchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
			List<AccountingBalance> balanceList = getAccountingBalanceService().getAccountingBalanceList(balanceSearchForm);
			return !CollectionUtils.isEmpty(balanceList);
		});
		return BooleanUtils.isTrue(accountPopulated);
	}


	/**
	 * Retrieves the list of counterparties which correspond to the given holding account.
	 */
	private List<BusinessCompany> getAccountCompanyList(InvestmentAccount holdingAccount) {
		List<BusinessCompany> holdingAccountCompanyList = new ArrayList<>();

		// Attempt to retrieve relevant parties via contract
		if (holdingAccount.getBusinessContract() != null) {
			List<String> partyRoleNameList = CollectionUtils.createList(BusinessContractPartyRole.PARTY_ROLE_COUNTERPARTY_NAME);
			if (isIncludeCreditSupportProviders()) {
				partyRoleNameList.add(BusinessContractPartyRole.PARTY_ROLE_CREDIT_SUPPORT_PROVIDER_NAME);
			}
			List<BusinessContractParty> contractPartyList = getBusinessContractService().getBusinessContractPartyListByContractAndRole(
					BeanUtils.getBeanIdentity(holdingAccount.getBusinessContract()), partyRoleNameList.toArray(new String[0]));
			holdingAccountCompanyList.addAll(CollectionUtils.getConverted(contractPartyList, BusinessContractParty::getCompany));
		}

		// Fallback to holding account issuing company
		if (holdingAccountCompanyList.isEmpty() && holdingAccount.getIssuingCompany() != null) {
			holdingAccountCompanyList.add(holdingAccount.getIssuingCompany());
		}

		return holdingAccountCompanyList;
	}


	/**
	 * Determines if the given company is included for validation via the current rule configuration.
	 *
	 * @param businessCompany the company to evaluate
	 * @return {@code true} if the company is included  for validation, or {@code false} otherwise
	 */
	private boolean isIncludedCompany(BusinessCompany businessCompany) {
		return isValidateAllCompanies() || ArrayUtils.contains(getCompanyIds(), BeanUtils.getBeanIdentity(businessCompany));
	}


	/**
	 * Gets the cache indicating whether a given account has holdings under a given list of search specifications. This cache is stored in the {@link
	 * TradeRuleEvaluatorContext} object and is unique to this instance of rule processing.
	 * <p>
	 * The company list cache stores the counterparty list for each account which has been previously processed in the current context.
	 *
	 * @param ruleEvaluationConfig configuration rule configuration object
	 * @return the cache
	 */
	private Map<AccountBalanceKey, Boolean> getAccountPopulatedCache(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext().getOrSupplyBean(CONTEXT_HOLDING_ACCOUNT_POPULATED_CACHE_KEY, ConcurrentHashMap::new);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Cache key for determining whether accounts have balance provided the given parameters.
	 *
	 * @see #getAccountPopulatedCache(ComplianceRuleEvaluationConfig)
	 */
	private static class AccountBalanceKey {

		private final Integer accountId;
		private final Date transactionDate;


		public AccountBalanceKey(Integer accountId, Date transactionDate) {
			AssertUtils.assertNotNull(accountId, "The account ID must not be null.");
			AssertUtils.assertNotNull(transactionDate, "The transaction date must not be null.");
			this.accountId = accountId;
			this.transactionDate = transactionDate;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			AccountBalanceKey that = (AccountBalanceKey) o;
			return this.accountId.equals(that.accountId)
					&& this.transactionDate.equals(that.transactionDate);
		}


		@Override
		public int hashCode() {
			return 31 * this.accountId.hashCode() + this.transactionDate.hashCode();
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public Integer getAccountId() {
			return this.accountId;
		}


		public Date getTransactionDate() {
			return this.transactionDate;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public Short[] getAccountTypeIds() {
		return this.accountTypeIds;
	}


	public void setAccountTypeIds(Short[] accountTypeIds) {
		this.accountTypeIds = accountTypeIds;
	}


	public boolean isValidateAllCompanies() {
		return this.validateAllCompanies;
	}


	public void setValidateAllCompanies(boolean validateAllCompanies) {
		this.validateAllCompanies = validateAllCompanies;
	}


	public Integer[] getCompanyIds() {
		return this.companyIds;
	}


	public void setCompanyIds(Integer[] companyIds) {
		this.companyIds = companyIds;
	}


	public boolean isIncludeEmptyAccounts() {
		return this.includeEmptyAccounts;
	}


	public void setIncludeEmptyAccounts(boolean includeEmptyAccounts) {
		this.includeEmptyAccounts = includeEmptyAccounts;
	}


	public boolean isIncludeCreditSupportProviders() {
		return this.includeCreditSupportProviders;
	}


	public void setIncludeCreditSupportProviders(boolean includeCreditSupportProviders) {
		this.includeCreditSupportProviders = includeCreditSupportProviders;
	}
}
