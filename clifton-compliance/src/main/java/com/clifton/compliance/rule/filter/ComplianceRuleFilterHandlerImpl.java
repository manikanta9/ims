package com.clifton.compliance.rule.filter;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.ComplianceBaseHandler;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitTypes;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.context.Context;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


//TODO - REMOVE this class and replace with static util function(s) and/or inline lambdas where needed?
@Component
public class ComplianceRuleFilterHandlerImpl implements ComplianceRuleFilterHandler {

	private static final String RIC = "Registered Investment Company";

	private InvestmentGroupService InvestmentGroupService;
	private InvestmentSetupService InvestmentSetupService;
	private InvestmentInstrumentService InvestmentInstrumentService;

	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private ComplianceBaseHandler complianceBaseHandler;


	@Override
	public void filterByInvestmentGroup(Map<? extends NamedObject, List<ComplianceRulePosition>> positionListMap, Short investmentGroupId, boolean instrument) {
		if (investmentGroupId == null || getInvestmentGroupService().getInvestmentGroup(investmentGroupId) == null
				|| getInvestmentGroupService().getInvestmentGroup(investmentGroupId).getName() == null) {
			return;
		}

		Iterator<? extends NamedObject> iterator = positionListMap.keySet().iterator();

		while (iterator.hasNext()) {
			NamedObject entity = iterator.next();

			if (entity == null) {
				iterator.remove();
				continue;
			}

			Integer id = (Integer) entity.getIdentity();

			if ((instrument && !getInvestmentGroupService().isInvestmentInstrumentInGroup(getInvestmentGroupService().getInvestmentGroup(investmentGroupId).getName(), id))
					|| (!instrument && !getInvestmentGroupService().isInvestmentSecurityInGroup(getInvestmentGroupService().getInvestmentGroup(investmentGroupId).getName(), id))) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByInvestmentGroup(List<Integer> securityIdList, Short investmentGroupId, boolean inclusion) {
		if (investmentGroupId == null || getInvestmentGroupService().getInvestmentGroup(investmentGroupId) == null
				|| getInvestmentGroupService().getInvestmentGroup(investmentGroupId).getName() == null) {
			return;
		}

		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			if (id == null) {
				continue;
			}

			boolean isInGroup = getInvestmentGroupService().isInvestmentSecurityInGroup(getInvestmentGroupService().getInvestmentGroup(investmentGroupId).getName(), id);

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByInvestmentType(List<Integer> securityIdList, Short investmentTypeId, boolean inclusion) {
		if (investmentTypeId == null || getInvestmentSetupService().getInvestmentType(investmentTypeId) == null || getInvestmentSetupService().getInvestmentType(investmentTypeId).getName() == null) {
			return;
		}

		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			if (id == null) {
				continue;
			}

			boolean isInGroup = InvestmentUtils.isSecurityOfType(getInvestmentInstrumentService().getInvestmentSecurity(id), getInvestmentSetupService().getInvestmentType(investmentTypeId).getName());

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByInvestmentTypeSubType(List<Integer> securityIdList, Short investmentTypeSubTypeId, boolean inclusion) {
		if (investmentTypeSubTypeId == null || getInvestmentSetupService().getInvestmentTypeSubType(investmentTypeSubTypeId) == null
				|| getInvestmentSetupService().getInvestmentTypeSubType(investmentTypeSubTypeId).getName() == null) {
			return;
		}

		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			if (id == null) {
				continue;
			}

			boolean isInGroup = InvestmentUtils.isSecurityOfTypeSubType(getInvestmentInstrumentService().getInvestmentSecurity(id),
					getInvestmentSetupService().getInvestmentTypeSubType(investmentTypeSubTypeId).getName());

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByInvestmentHierarchy(List<Integer> securityIdList, Short investmentHierarchyId, boolean inclusion) {
		if (investmentHierarchyId == null || getInvestmentSetupService().getInvestmentInstrumentHierarchy(investmentHierarchyId) == null
				|| getInvestmentSetupService().getInvestmentInstrumentHierarchy(investmentHierarchyId).getName() == null) {
			return;
		}

		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			if (id == null) {
				continue;
			}

			boolean isInGroup = InvestmentUtils.isSecurityInHierarchy(getInvestmentInstrumentService().getInvestmentSecurity(id),
					getInvestmentSetupService().getInvestmentInstrumentHierarchy(investmentHierarchyId).getName());

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByBusinessCompany(List<Integer> securityIdList, int businessCompanyId, boolean inclusion) {
		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(id);

			if (security == null || security.getBusinessCompany() == null) {
				continue;
			}

			boolean isInGroup = security.getBusinessCompany().getRootParent().getId().equals(businessCompanyId);

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByIlliquid(List<Integer> securityIdList, boolean inclusion) {
		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(id);

			if (security == null) {
				continue;
			}

			boolean isInGroup = security.isIlliquid();

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByRIC(List<Integer> securityIdList, boolean inclusion) {
		Iterator<Integer> iterator = securityIdList.iterator();
		while (iterator.hasNext()) {
			Integer id = iterator.next();
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(id);
			//If the id is invalid, or the security is a currency, then remove.
			if (security == null || security.isCurrency()) {
				iterator.remove();
				continue;
			}
			if (security.getBusinessCompany() == null) {
				if (inclusion) {
					iterator.remove();
				}
				continue;
			}
			boolean isInGroup = security.getBusinessCompany().getType().getName().equals(RIC);
			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterBySRB(List<Integer> securityIdList, boolean inclusion) {
		Iterator<Integer> iterator = securityIdList.iterator();

		while (iterator.hasNext()) {
			Integer id = iterator.next();

			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(id);

			if (security == null) {
				iterator.remove();
				continue;
			}

			boolean isInGroup = isSecuritySRB(security);

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public boolean isSecuritySRB(InvestmentSecurity security) {
		boolean isInGroup;

		SystemHierarchy issuerIndustryHierarchy = getComplianceBaseHandler().getIndustryFromIssuer(security);

		if (issuerIndustryHierarchy != null) {
			isInGroup = isHierarchySRB(issuerIndustryHierarchy);
		}
		else {
			SystemHierarchy securityIndustryHierarchy = getComplianceBaseHandler().getIndustryFromSecurity(security);
			isInGroup = isHierarchySRB(securityIndustryHierarchy);
		}

		return isInGroup;
	}


	private boolean isHierarchySRB(SystemHierarchy hierarchy) {
		if (hierarchy != null) {
			SystemHierarchy rootHierarchy = ComplianceUtils.traverseHierarchy(hierarchy, 2);

			if (rootHierarchy != null && ("Banking".equals(rootHierarchy.getName()) || "Specialty Finance".equals(rootHierarchy.getName()) || "Insurance".equals(rootHierarchy.getName()))) {
				return true;
			}
		}

		return false;
	}


	@Override
	public void filterByIndustryExclusionList(Set<SystemHierarchy> hierarchies, List<Short> exclusionIdList, boolean inclusion) {
		if (exclusionIdList == null) {
			return;
		}

		Iterator<SystemHierarchy> iterator = hierarchies.iterator();

		while (iterator.hasNext()) {
			SystemHierarchy hierarchy = iterator.next();

			if (hierarchy == null) {
				iterator.remove();
				continue;
			}

			boolean isInGroup = exclusionIdList.contains(hierarchy.getId());

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				iterator.remove();
			}
		}
	}


	@Override
	public void filterByUltimateIssuerValue(Context context, Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionList, boolean inclusion, BigDecimal percentLimit, ComplianceRuleLimitTypes limitType,
	                                        Date processDate, BigDecimal denominator) {

		Iterator<Map.Entry<BusinessCompany, List<ComplianceRulePosition>>> entryIterator = issuerPositionList.entrySet().iterator();

		while (entryIterator.hasNext()) {
			Map.Entry<BusinessCompany, List<ComplianceRulePosition>> issuerEntry = entryIterator.next();

			if (issuerEntry.getKey() == null) {
				continue;
			}
			BigDecimal numerator = getComplianceRulePositionHandler().getPositionListValueTotal(context, issuerEntry.getValue(), processDate, ComplianceRulePositionValues.MARKET_VALUE);
			BigDecimal resultValue = numerator.divide(denominator, 5, RoundingMode.HALF_UP).multiply(new BigDecimal(100));

			boolean isInGroup = !limitType.isPass(resultValue, percentLimit, null);

			if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
				entryIterator.remove();
			}
		}
	}


	@Override
	public void filterByVotingShares(Context context, Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap, boolean inclusion, BigDecimal percentLimit, ComplianceRuleLimitTypes limitType,
	                                 Date processDate) {
		Iterator<Map.Entry<InvestmentSecurity, List<ComplianceRulePosition>>> entryIterator = securityPositionMap.entrySet().iterator();

		while (entryIterator.hasNext()) {
			Map.Entry<InvestmentSecurity, List<ComplianceRulePosition>> securityEntry = entryIterator.next();

			if (securityEntry.getKey() == null) {
				continue;
			}

			try {
				BigDecimal denominator = getComplianceBaseHandler().calculateSharesOutstanding(securityEntry.getKey(), processDate);
				BigDecimal numerator = getComplianceRulePositionHandler().getPositionListValueTotal(context, securityEntry.getValue(), processDate, ComplianceRulePositionValues.SHARES_VALUE);

				BigDecimal resultValue = numerator.divide(denominator, 5, RoundingMode.HALF_UP).multiply(new BigDecimal(100));

				boolean isInGroup = !limitType.isPass(resultValue, percentLimit, null);

				if ((!isInGroup && inclusion) || (isInGroup && !inclusion)) {
					entryIterator.remove();
				}
			}
			catch (ValidationException e) {
				entryIterator.remove(); //Security does not support voting shares
			}
		}
	}


	@Override
	public Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> mapSecurityPositionListToIndustry(Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap,
	                                                                                                                     Integer level) {
		Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> industryMapping = new HashMap<>();

		for (Map.Entry<InvestmentSecurity, List<ComplianceRulePosition>> investmentSecurityListEntry : securityPositionMap.entrySet()) {
			SystemHierarchy industry = getComplianceBaseHandler().getIndustryFromSecurity(investmentSecurityListEntry.getKey(), level);

			Map<InvestmentSecurity, List<ComplianceRulePosition>> securityToPositionList = industryMapping.computeIfAbsent(industry, k -> new HashMap<>());

			securityToPositionList.put(investmentSecurityListEntry.getKey(), investmentSecurityListEntry.getValue());
		}

		return industryMapping;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.InvestmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.InvestmentGroupService = investmentGroupService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.InvestmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.InvestmentSetupService = investmentSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.InvestmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.InvestmentInstrumentService = investmentInstrumentService;
	}


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public ComplianceBaseHandler getComplianceBaseHandler() {
		return this.complianceBaseHandler;
	}


	public void setComplianceBaseHandler(ComplianceBaseHandler complianceBaseHandler) {
		this.complianceBaseHandler = complianceBaseHandler;
	}
}
