package com.clifton.compliance.rule.assignment.cache.standard;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentTargetHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.key.AbstractSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;


/**
 * The <i>target type</i> for the {@link ComplianceAssignmentStandardSpecificityCache} specificity cache.
 * <p>
 * This target type contains relevant information for specificity evaluations and for retrieval for rule assignment entities.
 *
 * @author MikeH
 */
public class ComplianceAssignmentStandardTargetHolder extends BaseComplianceAssignmentTargetHolder {

	public ComplianceAssignmentStandardTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment, AbstractSpecificityKeyGenerator generator) {
		// The rule ID is used as the scope ID since only a single applicable assignment may precipitate per rule
		super(rule, assignment, generateKey(assignment, generator), rule.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static String generateKey(ComplianceRuleAssignment assignment, AbstractSpecificityKeyGenerator generator) {
		Object[] accountSpecificity = {
				BeanUtils.getBeanIdentity(assignment.getClientInvestmentAccount()),
				BeanUtils.getBeanIdentity(assignment.getBusinessService()),
				null
		};
		SpecificityKeySource keySource = SpecificityKeySource
				.builder(accountSpecificity)
				.build();
		return generator.generateKeyForTarget(keySource);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "ComplianceAssignmentStandardTargetHolder{} " + super.toString();
	}
}
