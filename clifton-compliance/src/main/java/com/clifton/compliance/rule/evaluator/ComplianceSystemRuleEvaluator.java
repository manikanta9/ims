package com.clifton.compliance.rule.evaluator;

import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.execution.ComplianceRunExecutionHandler;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.workflow.RuleEvaluatorWorkflowAction;
import com.clifton.trade.Trade;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ComplianceSystemRuleEvaluator</code> class is the system {@link RuleEvaluator rule evaluator} which triggers the evaluation of all compliance rules pertaining to an
 * entity.
 * <p>
 * This evaluator is typically triggered for transaction-related entities, such as {@link Trade} and {@link AccountingPositionTransfer} entities. Evaluation is generally triggered
 * either manually by the saving method or through workflow transition actions (see {@link RuleEvaluatorWorkflowAction}, for example).
 *
 * @author stevenf
 */
public class ComplianceSystemRuleEvaluator<T extends IdentityObject & AccountingBean> implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private ComplianceRunExecutionHandler complianceRunExecutionHandler;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private RuleViolationService ruleViolationService;

	/**
	 * Exclude security specific rules from real-time execution
	 */
	private boolean excludeSecuritySpecificRules;

	/**
	 * Execute parent rules as well
	 */
	private boolean executeParentRules;

	/**
	 * Exclude specific accounts from real time rule execution
	 */
	private Integer excludeInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T accountingBean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			//Exclude execution of rules for accounts in exclusion group
			if (getExcludeInvestmentAccountGroupId() != null) {
				InvestmentAccountGroup accountGroup = getInvestmentAccountGroupService().getInvestmentAccountGroup(getExcludeInvestmentAccountGroupId());
				if (accountGroup != null && getInvestmentAccountGroupService().isInvestmentAccountInGroup(accountingBean.getClientInvestmentAccount().getId(), accountGroup.getName())) {
					return ruleViolationList;
				}
			}

			ComplianceRunExecutionCommand command = new ComplianceRunExecutionCommand();
			command.setBean(accountingBean);
			command.setClientAccountId(accountingBean.getClientInvestmentAccount().getId());
			command.setInvestmentSecurityId(accountingBean.getInvestmentSecurity().getId());
			command.setProcessDate(accountingBean.getTransactionDate());
			command.setRealTime(true);
			command.setIncludeSecuritySpecificRules(!isExcludeSecuritySpecificRules());
			command.setIncludeParentRules(isExecuteParentRules());
			command.setTradeRuleEvaluatorContext(context);

			List<ComplianceRuleRun> ruleRunList = getComplianceRunExecutionHandler().doRebuildComplianceRunForCommand(command);
			for (ComplianceRuleRun run : CollectionUtils.getIterable(ruleRunList)) {
				if (run.getStatus() != null && run.getStatus() == ComplianceRuleRun.STATUS_FAILED) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put(VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, getParentDescription(accountingBean.getClientInvestmentAccount(), run) +
							run.getComplianceRule().getName() + " : " + run.getDescription() + " : Failed" + generateDetailLevelViolations(run));
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, accountingBean.getSourceEntityId(), run.getComplianceRule().getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getParentDescription(InvestmentAccount clientAccount, ComplianceRuleRun run) {
		String parentDescription = "";
		if (run.getComplianceRun().getClientInvestmentAccount() != null && !CompareUtils.isEqual(run.getComplianceRun().getClientInvestmentAccount(), clientAccount)) {
			parentDescription = "Parent [" + run.getComplianceRun().getClientInvestmentAccount().getNumber() + "]: ";
		}
		return parentDescription;
	}


	private String generateDetailLevelViolations(ComplianceRuleRun run) {
		StringBuilder detailViolations = new StringBuilder();
		for (ComplianceRuleRunDetail detail : CollectionUtils.getIterable(run.getChildren())) {
			if (run.getLimitValue() != null && detail.getPercentOfAssets() != null) {
				detailViolations.append(" [")
						.append(detail.getDescription())
						.append(", Limit Value: ")
						.append(run.getLimitValue())
						.append(", Result: ")
						.append(detail.getPercentOfAssets())
						.append("]");
			}
		}
		return detailViolations.toString();
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ComplianceRunExecutionHandler getComplianceRunExecutionHandler() {
		return this.complianceRunExecutionHandler;
	}


	public void setComplianceRunExecutionHandler(ComplianceRunExecutionHandler complianceRunExecutionHandler) {
		this.complianceRunExecutionHandler = complianceRunExecutionHandler;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public boolean isExcludeSecuritySpecificRules() {
		return this.excludeSecuritySpecificRules;
	}


	public void setExcludeSecuritySpecificRules(boolean excludeSecuritySpecificRules) {
		this.excludeSecuritySpecificRules = excludeSecuritySpecificRules;
	}


	public boolean isExecuteParentRules() {
		return this.executeParentRules;
	}


	public void setExecuteParentRules(boolean executeParentRules) {
		this.executeParentRules = executeParentRules;
	}


	public Integer getExcludeInvestmentAccountGroupId() {
		return this.excludeInvestmentAccountGroupId;
	}


	public void setExcludeInvestmentAccountGroupId(Integer excludeInvestmentAccountGroupId) {
		this.excludeInvestmentAccountGroupId = excludeInvestmentAccountGroupId;
	}
}
