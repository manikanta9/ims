package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IndustrySharesOutstanding</code> rule filters securities by a specified industry hierarchy. We cannot own more than X% of the total
 * outstanding voting shares of any security falling under this industry hierarchy.
 *
 * @author apopp
 */
public class IndustrySharesOutstanding extends BaseComplianceRuleRatioEvaluator {

	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	private Short industryId;
	private Short systemHierarchyCategoryId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String msg = "";

		SystemHierarchy industry = getSystemHierarchyDefinitionService().getSystemHierarchy(getIndustryId());

		if (industry != null) {
			msg = industry.getName() + " : ";
		}

		if (ruleEvaluationConfig.getInvestmentSecurityId() != null) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId());
			msg = msg + numerator + " of " + security.getLabel() + " out of " + denominator;
		}

		return msg;
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();

		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());
		Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> industryMapping = getComplianceRuleFilterHandler().mapSecurityPositionListToIndustry(securityPositionMap, 3);

		getComplianceRuleFilterHandler().filterByIndustryExclusionList(industryMapping.keySet(), CollectionUtils.createList(getIndustryId()), true);

		if (!industryMapping.isEmpty()) {

			for (InvestmentSecurity security : industryMapping.get(CollectionUtils.getFirstElement(industryMapping.keySet())).keySet()) {
				ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
				config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
				config.setProcessDate(ruleEvaluationConfig.getProcessDate());
				config.setInvestmentSecurityId(security.getId());
				config.setSharePosition(true);
				configList.add(config);
			}
		}
		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<Integer> securityIdList = CollectionUtils.createList(ruleEvaluationConfig.getInvestmentSecurityId());
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityPositionMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.SHARES_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) throws ValidationException {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId());
		return getComplianceBaseHandler().calculateSharesOutstanding(security, ruleEvaluationConfig.getProcessDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public Short getIndustryId() {
		return this.industryId;
	}


	public void setIndustryId(Short industryId) {
		this.industryId = industryId;
	}


	public Short getSystemHierarchyCategoryId() {
		return this.systemHierarchyCategoryId;
	}


	public void setSystemHierarchyCategoryId(Short systemHierarchyCategoryId) {
		this.systemHierarchyCategoryId = systemHierarchyCategoryId;
	}
}
