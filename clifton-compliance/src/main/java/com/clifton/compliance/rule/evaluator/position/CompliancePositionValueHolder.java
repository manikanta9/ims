package com.clifton.compliance.rule.evaluator.position;

import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleValueCalculator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;


/**
 * The value holder cartridge for {@link AccountingPositionInfo} objects for in-progress compliance position value evaluations via {@link ComplianceRuleValueCalculator}
 * calculators.
 * <p>
 * This object holds a list of {@link #positionList positions} along with a corresponding {@link #value value} for the current calculated value of the positions at some point in a
 * procedure.
 *
 * @author MikeH
 */
public class CompliancePositionValueHolder<T extends AccountingPositionInfo> {

	/**
	 * The current calculated value of the positions. The meaning of this value is determined by its generator.
	 */
	private final BigDecimal value;
	/**
	 * The position list for this value holder. This is the list of positions that were taken into account when generating this value holder.
	 */
	private final List<T> positionList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CompliancePositionValueHolder(BigDecimal value, List<T> positionList) {
		Objects.requireNonNull(value);
		Objects.requireNonNull(positionList);
		this.value = value;
		this.positionList = positionList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "CompliancePositionValueHolder{" +
				"value=" + this.value +
				", positionList=" + this.positionList +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getValue() {
		return this.value;
	}


	public List<T> getPositionList() {
		return this.positionList;
	}
}
