package com.clifton.compliance.rule.position;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class ComplianceRulePositionHandlerImpl implements ComplianceRulePositionHandler {

	private AccountingBalanceService accountingBalanceService;
	private AccountingPositionHandler accountingPositionHandler;
	private AccountingPositionService accountingPositionService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService InvestmentAccountService;
	private ComplianceRuleAccountHandler complianceRuleAccountHandler;
	private AccountingValuationService accountingValuationService;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////            Accounting Position Retrieval Methods           ////////
	////////////////////////////////////////////////////////////////////////////

	/*
	 * These methods are a refactoring of the historic compliance methods. While we want to retain this service for now, we should try to clean up and remove the old methods as
	 * possible.
	 */


	@Override
	public List<AccountingPosition> getAccountingPositionList(Context context, ComplianceRuleAssignment ruleAssignment, int clientAccountId, Date transactionDate) {
		// Get relationship purpose names
		String subaccountPurposeName = (ruleAssignment.getSubAccountPurpose() != null) ? ruleAssignment.getSubAccountPurpose().getName() : null;
		String subsidiaryPurposeName = (ruleAssignment.getSubsidiaryPurpose() != null) ? ruleAssignment.getSubsidiaryPurpose().getName() : null;

		// Aggregate unique account IDs
		Set<Integer> accountIdList = new HashSet<>();
		accountIdList.add(clientAccountId);
		accountIdList.addAll(getAccountIdListForRelationship(context, clientAccountId, subaccountPurposeName, transactionDate));
		accountIdList.addAll(getAccountIdListForRelationship(context, clientAccountId, subsidiaryPurposeName, transactionDate));

		// Get all positions
		return CollectionUtils.getConvertedFlattened(accountIdList, accountId -> getAccountingPositionListForClientAccount(context, accountId, transactionDate));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of account IDs for accounts related to the provided account via the given relationship purpose. If an <tt>activeDate</tt> is provided, then results will be
	 * filtered to those active on the given date. Otherwise, all historic related accounts will be returned.
	 *
	 * @param context                 the rule evaluation context for cache storage and retrieval
	 * @param accountId               the ID of the source account for which to find related accounts
	 * @param relationshipPurposeName the name of the {@link InvestmentAccountRelationshipPurpose} for which related accounts should be found
	 * @param activeDate              if provided, the date for which the relationships of returned account IDs must be active; if {@code null}, then all historic related accounts
	 *                                will be returned
	 * @return the list of IDs for the related accounts
	 */
	private List<Integer> getAccountIdListForRelationship(Context context, int accountId, String relationshipPurposeName, Date activeDate) {
		// Guard-clause: No relationship purpose was provided
		if (relationshipPurposeName == null) {
			return Collections.emptyList();
		}

		String cacheKey = getAccountIdListForRelationshipCacheKey(accountId, relationshipPurposeName, activeDate);
		return context.getOrSupplyBean(cacheKey, () -> {
			// Get unique list of IDs for related accounts
			List<InvestmentAccountRelationship> accountRelationshipList = getInvestmentAccountRelationshipService()
					.getInvestmentAccountRelationshipListForPurpose(accountId, relationshipPurposeName, activeDate, true);
			List<Integer> converted = CollectionUtils.getConverted(accountRelationshipList, relationship -> relationship.getReferenceTwo().getId());
			return CollectionUtils.removeDuplicates(converted);
		});
	}


	/**
	 * Retrieves the list of {@link AccountingPosition} entities for the given account ID on the given <tt>transactionDate</tt>.
	 * <p>
	 * This applies the standard list of filters used for position retrieval within compliance rules.
	 *
	 * @param context         the rule evaluation context for cache storage and retrieval
	 * @param clientAccountId the account ID for which to retrieve positions
	 * @param transactionDate the date for which positions should be retrieved
	 * @return the resulting list of positions
	 */
	private List<AccountingPosition> getAccountingPositionListForClientAccount(Context context, int clientAccountId, Date transactionDate) {
		String cacheKey = getAccountingPositionListCacheKey(clientAccountId, transactionDate);
		return context.getOrSupplyBean(cacheKey, () -> {
			AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
			command.setClientInvestmentAccountId(clientAccountId);
			command.setExcludeNotOurGLAccounts(true);
			command.setExcludeReceivableGLAccounts(true);
			command.setPendingActivityRequest(PendingActivityRequests.POSITIONS_STRICT_WITH_MERGE_AND_PREVIEW);
			return getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getAccountingPositionListCacheKey(Integer clientAccountId, Date transactionDate) {
		return String.format("getAccountingPositionList-%d-%tF", clientAccountId, transactionDate);
	}


	private String getAccountIdListForRelationshipCacheKey(Integer clientAccountId, String purposeName, Date activeDate) {
		return String.format("getAccountIdListForRelationship-%d-%s-%tF", clientAccountId, purposeName, activeDate);
	}


	/*
	 * Do not add new methods below this line.
	 */


	//////////////////////////////////////////////////////////////////////////////////
	///////         Compliance Rule Account Position Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRulePosition> getPositionListForAccountByRule(Context context, ComplianceRuleAssignment ruleAssignment, Integer clientAccountId, Date date) {
		Short subaccountPurposeId = (ruleAssignment.getSubAccountPurpose() != null) ? ruleAssignment.getSubAccountPurpose().getId() : null;
		Short subsidiaryPurposeId = (ruleAssignment.getSubsidiaryPurpose() != null) ? ruleAssignment.getSubsidiaryPurpose().getId() : null;

		//Ensure no one can manipulate the state of the cache list.
		return new ArrayList<>(getAccountingPositionListFullPopulatedForClientAccount(context, clientAccountId, date, subsidiaryPurposeId, subaccountPurposeId));
	}


	public List<ComplianceRulePosition> getAccountPositionListPopulatedForSingleSecurity(Context context, InvestmentSecurity security, Integer clientAccountId, Date processDate, Short exclusionPurposeId,
	                                                                                     Short positionPurposeId) {
		List<ComplianceRulePosition> positionList = getAccountPositionListPopulatedCompletely(context, clientAccountId, processDate, exclusionPurposeId, positionPurposeId);
		return positionList.stream()
				.filter(position -> Objects.equals(security, position.getRealSecurityOrPseudo()))
				.collect(Collectors.toList());
	}


	@Override
	public List<ComplianceRulePosition> getAccountingPositionListFullPopulatedForClientAccount(Context context, int clientAccountId, Date transactionDate, Short subsidiaryPurposeId, Short subaccountPurposeId) {
		//Get list of all unique subAccounts with subsidiary accounts removed
		List<Integer> subaccountIdList = getComplianceRuleAccountHandler().getSubAccountListUniqueWithoutSubsidiary(context, clientAccountId, transactionDate, subsidiaryPurposeId, subaccountPurposeId);
		subaccountIdList.add(clientAccountId);

		//Aggregate all positions from subAccounts and provided account
		List<ComplianceRulePosition> positionList = getAccountingPositionListPopulatedByAccountList(context, subaccountIdList, transactionDate);

		//Get list of all subsidiary accounts
		List<Integer> subsidiaryAccountIdList = getComplianceRuleAccountHandler().getAccountRelatedAccountListByPurpose(context, clientAccountId, transactionDate, subsidiaryPurposeId);

		//Pseudo securitize positions from subsidiary accounts and add them to total position list
		positionList.addAll(pseudoSecuritizeAccountListPositions(context, subsidiaryAccountIdList, transactionDate));

		return new ArrayList<>(positionList);
	}


	private List<ComplianceRulePosition> pseudoSecuritizeAccountListPositions(Context context, List<Integer> subsidiaryAccountIdList, Date transactionDate) {
		List<ComplianceRulePosition> positionList = new ArrayList<>();

		for (Integer accountId : CollectionUtils.getIterable(subsidiaryAccountIdList)) {
			positionList.addAll(pseudoSecuritizeAccountPositions(context, accountId, transactionDate));
		}

		return positionList;
	}


	private List<ComplianceRulePosition> pseudoSecuritizeAccountPositions(Context context, Integer clientAccountId, Date transactionDate) {
		List<ComplianceRulePosition> positionList = getAccountingPositionListPopulatedForClientAccount(context, clientAccountId, transactionDate);
		InvestmentAccount subsidiaryAccount = getInvestmentAccountService().getInvestmentAccount(clientAccountId);

		for (ComplianceRulePosition position : CollectionUtils.getIterable(positionList)) {
			position.setPseudoPositionAccount(subsidiaryAccount);
			position.setPseudoSecurity(createPseudoSecurity(subsidiaryAccount));
		}

		positionList.add(getAccountCashAsPosition(context, BeanUtils.cloneBean(subsidiaryAccount, true, true), transactionDate, createPseudoSecurity(subsidiaryAccount)));

		return positionList;
	}


	private ComplianceRulePosition getAccountCashAsPosition(Context context, InvestmentAccount subsidiaryAccount, Date transactionDate, InvestmentSecurity security) {
		BigDecimal cash = getAccountCashBalanceNetOrTotal(context, subsidiaryAccount.getId(), transactionDate, false, false);

		AccountingTransaction transaction = new AccountingTransaction();
		AccountingPosition accountingPosition = AccountingPosition.forOpeningTransaction(transaction);
		ComplianceRulePosition compliancePosition = new ComplianceRulePosition(accountingPosition);

		InvestmentInstrument instrument = new InvestmentInstrument();
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		AccountingJournal journal = new AccountingJournal();

		transaction.setJournal(journal);
		transaction.setTransactionDate(transactionDate);
		compliancePosition.setPseudoPositionAccount(subsidiaryAccount);
		compliancePosition.setPseudoSecurity(security);
		transaction.setClientInvestmentAccount(subsidiaryAccount);
		transaction.setInvestmentSecurity(security);
		instrument.setHierarchy(hierarchy);
		security.setInstrument(instrument);
		transaction.setHoldingInvestmentAccount(new InvestmentAccount());

		accountingPosition.setRemainingQuantity(cash);
		compliancePosition.setCash(true);

		security.setCurrency(true);
		security.setName("Cash: " + security.getName());
		security.setSymbol("");

		instrument.setTradingCurrency(security);
		hierarchy.setIndexRatioAdjusted(false);
		subsidiaryAccount.setBaseCurrency(security);
		transaction.setSettlementDate(transactionDate);

		return compliancePosition;
	}


	private InvestmentSecurity createPseudoSecurity(InvestmentAccount account) {
		InvestmentSecurity pseudoSecurity = new InvestmentSecurity();
		pseudoSecurity.setId(Integer.valueOf("-1" + account.getId()));
		pseudoSecurity.setName(account.getLabel());
		pseudoSecurity.setSymbol(account.getLabel());

		//Need to set business company so aggregation at ultimate issuer works with pseudo securities
		BusinessCompany c = new BusinessCompany();
		c.setId(Integer.valueOf("-1" + account.getId()));
		c.setName(account.getName());
		pseudoSecurity.setBusinessCompany(c);

		return pseudoSecurity;
	}


	public List<ComplianceRulePosition> getAccountPositionListPopulatedCompletely(Context context, Integer clientAccountId, Date processDate, Short exclusionPurposeId, Short positionPurposeId) {
		List<Integer> accountIdList = getComplianceRuleAccountHandler().getSubAccountListUniqueWithoutSubsidiary(context, clientAccountId, processDate, exclusionPurposeId, positionPurposeId);
		return getAccountingPositionListPopulatedByAccountList(context, accountIdList, processDate);
	}


	private List<ComplianceRulePosition> getAccountingPositionListPopulatedForClientAccount(Context context, int clientInvestmentAccountId, Date transactionDate) {
		String contextKey = "getAccountingPositionListPopulatedForClientAccount-" + clientInvestmentAccountId + "-" + transactionDate;
		@SuppressWarnings("unchecked")
		List<ComplianceRulePosition> compliancePositionList = (List<ComplianceRulePosition>) context.getBean(contextKey);

		if (compliancePositionList != null) {
			//Ensure no one can manipulate the state of the cache list.
			return cloneComplianceRulePositionList(compliancePositionList);
		}

		compliancePositionList = new ArrayList<>();
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setClientInvestmentAccountId(clientInvestmentAccountId);
		command.setExcludeNotOurGLAccounts(true);
		command.setExcludeReceivableGLAccounts(true);
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);

		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(transactionDate);
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_ACCOUNTS);
		List<AccountingBalanceValue> ccyBalanceList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);

		//positions
		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			compliancePositionList.add(new ComplianceRulePosition(position));
		}

		//currencies (excluding cash)
		for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(ccyBalanceList)) {
			compliancePositionList.add(new ComplianceRulePosition(accountingBalanceValue));
		}

		// Only include pending trades if the transaction date is today
		if (DateUtils.compare(transactionDate, new Date(), false) == 0) {
			for (Trade pendingTrade : CollectionUtils.getIterable(getTradeService().getTradePendingList(clientInvestmentAccountId, null, null))) {
				compliancePositionList.add(new ComplianceRulePosition(pendingTrade));
			}
		}

		context.setBean(contextKey, compliancePositionList);

		//Ensure no one can manipulate the state of the cache list.
		return cloneComplianceRulePositionList(compliancePositionList);
	}


	private List<ComplianceRulePosition> cloneComplianceRulePositionList(List<ComplianceRulePosition> compliancePositionList) {
		List<ComplianceRulePosition> cleanPositionList = new ArrayList<>();

		for (ComplianceRulePosition position : CollectionUtils.getIterable(compliancePositionList)) {
			cleanPositionList.add(ComplianceRulePosition.newClonedInstance(position));
		}
		//Ensure no one can manipulate the state of the cache list.
		return cleanPositionList;
	}


	@Override
	public List<ComplianceRulePosition> getAccountingPositionListPopulatedByAccountList(Context context, List<Integer> accountIdList, Date transactionDate) {
		List<ComplianceRulePosition> positionList = new ArrayList<>();

		for (Integer id : CollectionUtils.getIterable(accountIdList)) {
			positionList.addAll(getAccountingPositionListPopulatedForClientAccount(context, id, transactionDate));
		}

		return positionList;
	}


	//////////////////////////////////////////////////////////////////////////////////
	///////         Compliance Rule Account Cash Balance Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCompleteAccountCashBalanceNetOrTotal(Context context, Integer clientAccountId, Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral, Short subsidiaryPurposeId,
	                                                          Short subaccountPurposeId) {

		List<Integer> subAccountIdList = getComplianceRuleAccountHandler().getAccountRelatedAccountListByPurpose(context, clientAccountId, transactionDate, subaccountPurposeId);
		List<Integer> subsidiaryAccountIdList = getComplianceRuleAccountHandler().getAccountRelatedAccountListByPurpose(context, clientAccountId, transactionDate, subsidiaryPurposeId);

		List<Integer> allAccountIdList = CoreCollectionUtils.removeAll(subAccountIdList, subsidiaryAccountIdList);

		return getAccountCashBalanceNetOrTotalByAccountList(context, allAccountIdList, transactionDate, isNetOnly, excludeCashCollateral);
	}


	@Override
	public BigDecimal getAccountValueTotalValue(Context context, Integer clientAccountId, ComplianceRulePositionValues complianceRulePositionValues, Date transactionDate, boolean isNetOnly,
	                                            boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId) {
		List<Integer> accountIdList = getComplianceRuleAccountHandler().getSubAccountListUniqueWithoutSubsidiary(context, clientAccountId, transactionDate, subsidiaryPurposeId, subaccountPurposeId);

		if (!accountIdList.contains(clientAccountId)) {
			accountIdList.add(clientAccountId);
		}

		List<ComplianceRulePosition> totalPositionList = getAccountingPositionListPopulatedByAccountList(context, accountIdList, transactionDate);

		return getAccountValueTotalValueByPositionList(context, clientAccountId, totalPositionList, complianceRulePositionValues, transactionDate, isNetOnly, excludeCashCollateral, subsidiaryPurposeId,
				subaccountPurposeId);
	}


	@Override
	public BigDecimal getAccountTotalValue(Context context, Integer clientAccountId, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues,
	                                       Short subsidiaryPurposeId, Short subaccountPurposeId, boolean useTotalAssets, boolean excludeCashCollateral) {

		BigDecimal positionListValue = getPositionListValueTotal(context, positionList, processDate, complianceRulePositionValues);
		List<Integer> accountIdList = getComplianceRuleAccountHandler().getAccountRelatedAccountListByPurpose(context, clientAccountId, processDate, subaccountPurposeId, subsidiaryPurposeId);
		positionListValue = positionListValue.add(getAccountCashBalanceNetOrTotalByAccountList(context, accountIdList, processDate, !useTotalAssets, excludeCashCollateral));

		return positionListValue;
	}


	@Override
	public BigDecimal getAccountTotalValue(Context context, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues, List<Integer> accountIdList,
	                                       boolean useTotalAssets, boolean excludeCashCollateral) {
		List<ComplianceRulePosition> positionListNoCash = BeanUtils.filter(positionList, complianceRulePosition -> !complianceRulePosition.isCash());
		BigDecimal positionListValue = getPositionListValueTotal(context, positionListNoCash, processDate, complianceRulePositionValues);
		positionListValue = positionListValue.add(getAccountCashBalanceNetOrTotalByAccountList(context, accountIdList, processDate, !useTotalAssets, excludeCashCollateral));

		return positionListValue;
	}


	@Override
	public BigDecimal getAccountValueTotalValueByPositionList(Context context, Integer clientAccountId, List<ComplianceRulePosition> positionList, ComplianceRulePositionValues complianceRulePositionValues,
	                                                          Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId) {

		BigDecimal totalCashValue = getCompleteAccountCashBalanceNetOrTotal(context, clientAccountId, transactionDate, isNetOnly, excludeCashCollateral, subsidiaryPurposeId, subaccountPurposeId);
		BigDecimal totalPositionValue = getPositionListValueTotal(context, positionList, transactionDate, complianceRulePositionValues);

		return totalCashValue.add(totalPositionValue);
	}


	@Override
	public BigDecimal calculatePercentOfAssets(Context context, BigDecimal value, Integer clientAccountId, List<ComplianceRulePosition> positionList, ComplianceRulePositionValues complianceRulePositionValues,
	                                           Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral, Short subsidiaryPurposeId, Short subaccountPurposeId) {

		BigDecimal accountValue = getAccountValueTotalValueByPositionList(context, clientAccountId, positionList, complianceRulePositionValues, transactionDate, isNetOnly, excludeCashCollateral,
				subsidiaryPurposeId, subaccountPurposeId);

		return value.divide(accountValue, 15, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
	}


	@Override
	public BigDecimal getAccountCashBalanceNetOrTotal(Context context, Integer clientAccountId, Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral) {
		BigDecimal value = (BigDecimal) context.getBean(clientAccountId + "-" + transactionDate + "-" + isNetOnly + "-" + excludeCashCollateral);
		if (value != null) {
			return value;
		}

		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountOnTransactionDate(clientAccountId, transactionDate)
				.withNetOnly(isNetOnly)
				.withExcludeCashCollateral(excludeCashCollateral);

		value = getAccountingBalanceService().getAccountingBalance(command);
		context.setBean(clientAccountId + "-" + transactionDate + "-" + isNetOnly + "-" + excludeCashCollateral, value);

		return value;
	}


	@Override
	public BigDecimal getAccountCashBalanceNetOrTotalByAccountList(Context context, List<Integer> clientAccountIdList, Date transactionDate, boolean isNetOnly, boolean excludeCashCollateral) {
		BigDecimal totalCash = BigDecimal.ZERO;

		for (Integer id : CollectionUtils.getIterable(clientAccountIdList)) {
			totalCash = totalCash.add(getAccountCashBalanceNetOrTotal(context, id, transactionDate, isNetOnly, excludeCashCollateral));
		}

		return totalCash;
	}


	@Override
	public BigDecimal getPositionListValueTotal(Context context, List<ComplianceRulePosition> positionList, Date processDate, ComplianceRulePositionValues complianceRulePositionValues) {
		BigDecimal valueSum = BigDecimal.ZERO;

		for (ComplianceRulePosition position : CollectionUtils.getIterable(positionList)) {
			valueSum = valueSum.add(getValueForPosition(context, position, complianceRulePositionValues, processDate));
		}

		return valueSum;
	}


	@Override
	public BigDecimal getValueForPosition(Context context, ComplianceRulePosition position, ComplianceRulePositionValues complianceRulePositionValues, Date processDate) {
		BigDecimal value = (BigDecimal) context.getBean(position.toString() + "-" + complianceRulePositionValues + "-" + processDate);

		if (value != null) {
			return value;
		}

		value = complianceRulePositionValues.calculateValue(getAccountingPositionHandler(), position, processDate);
		context.setBean(position.toString() + "-" + complianceRulePositionValues + "-" + processDate, value);

		return value;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public void setComplianceRuleAccountHandler(ComplianceRuleAccountHandler complianceRuleAccountHandler) {
		this.complianceRuleAccountHandler = complianceRuleAccountHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.InvestmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.InvestmentAccountService = investmentAccountService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
