package com.clifton.compliance.rule.evaluator;


import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;

import java.math.BigDecimal;
import java.util.List;


public interface ComplianceRuleEquationEvaluator extends ComplianceRuleEvaluator {

	/**
	 * A rule implementation is given several parameters under which it can generate the description of the details of an execution
	 *
	 * @param numerator
	 * @param denominator
	 * @param ruleEvaluationConfig
	 */
	String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Given a rule configuration, a rule will return back the result of evaluating it's numerator value
	 *
	 * @param ruleEvaluationConfig
	 */
	ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Given a rule configuration, a rule will return back the result of evaluating it's denominator value
	 *
	 * @param ruleEvaluationConfig
	 */
	BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Each rule implementation will generate a list of configurations used to evaluate each possible detail.
	 * This configuration is typically generated from the parameters of a parent configuration.
	 * <p>
	 * For instance, the industry concentration rule may receive a parent configuration telling it to evaluate itself
	 * against as specific account with certain parameters. It will then go and determine all of the industries under which
	 * the rule must execute against. For each industry it will then create a configuration specific to that industry.
	 * The abstract parent will then evaluate this config list by evaluating each of them separately at the detail level.
	 *
	 * @param ruleEvaluationConfig
	 */
	List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig);
}
