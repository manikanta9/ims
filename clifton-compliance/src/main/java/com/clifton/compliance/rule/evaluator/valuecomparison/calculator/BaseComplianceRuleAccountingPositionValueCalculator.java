package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.accounting.gl.position.AccountingPositionTypes;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.math.MathTransformationTypes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * The {@link BaseComplianceRuleAccountingPositionValueCalculator} is an implementor of {@link ComplianceRuleValueCalculator}. This is an abstract super-type which allows for
 * calculations based on positions.
 * <p>
 * Implementors of this class provide ways of calculating values from positions. For example, an implementor may retrieve the {@link AccountingPosition} entities associated with
 * the current operation and then aggregate their notional values.
 * <p>
 * This class provides configurable transformations for position values, such as multiplying the calculated value by a market data field or applying a {@link
 * MathTransformationTypes} operator to the value derived for a position. Position values are aggregated using the selected {@link #aggregationOperation aggregation operation} and
 * may then be further transformed by the selected {@link #multiplier}.
 *
 * @author MikeH
 */
public abstract class BaseComplianceRuleAccountingPositionValueCalculator<T extends AccountingPositionInfo> extends BaseComplianceRuleDateAwareValueCalculator {

	private SystemBeanService systemBeanService;
	private InvestmentGroupService investmentGroupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private MarketDataFieldService marketDataFieldService;

	// Transformations in order of application
	/**
	 * The ID of the market data field which shall be used as a position value multiplier. The value for this market data field shall be retrieved for each position and then
	 * multiplied by the calculated value for the position. The resulting value shall be the intermediate calculator value before any transformations are applied.
	 */
	private Short multiplierMarketDataFieldId;
	/**
	 * If {@code true}, the value for the {@link #multiplierMarketDataFieldId} shall be obtained by a flexible lookup. Otherwise, the market data field value shall be retrieved
	 * using a strict lookup.
	 */
	private boolean multiplierMarketDataFieldFlexibleLookup;
	/**
	 * The unary operation to apply to each individually calculated position value.
	 */
	private MathTransformationTypes transformationType;
	/**
	 * The aggregation operation to apply to the resulting {@link CompliancePositionValueHolder} values after all transformations have been applied. This aggregation operation is
	 * used to aggregate the list of values into a single value.
	 */
	private AggregationOperations aggregationOperation;
	/**
	 * The multiplier to apply to the final value before it is returned to the caller.
	 */
	private BigDecimal multiplier;

	// Position filters
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short instrumentHierarchyId;
	private Short investmentGroupId;
	private Short investmentSecurityGroupId;
	private AccountingPositionTypes positionType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected BigDecimal calculateImplWithCalculateDate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		List<T> positionList = BeanUtils.filter(getPositionList(ruleEvaluationConfig), this::isPositionApplicable);
		Date transactionDate = ruleEvaluationConfig.getCalculationDate();
		Context context = ruleEvaluationConfig.getContext();
		return getCalculatedValue(positionList, transactionDate, context, runSubDetailList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of position entities which will be evaluated by this calculator.
	 *
	 * @param ruleEvaluationConfig the rule evaluation context in which the positions shall be retrieved
	 * @return the list of applicable positions
	 */
	protected abstract List<T> getPositionList(ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Derives a single value from the given position entity. This is the initial value which will be used for calculations from the position and may be further transformed by
	 * configured modifiers.
	 *
	 * @param position        the position whose value shall be retrieved
	 * @param transactionDate the transaction date on which to retrieve the value
	 * @param context         the context in which the position value shall be calculated; this context may be used for caching
	 * @return the position value
	 */
	protected abstract BigDecimal getPositionValue(T position, Date transactionDate, Context context);


	/**
	 * Gets the transaction info associated with the given position entity. The transaction info is used to validate filter applicability for the given position.
	 *
	 * @param position the position entity
	 * @return the transaction info for the position entity
	 */
	protected abstract AccountingTransactionInfo getAccountingTransactionInfo(T position);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Applies a set of transformations to the given collection of position value holders.
	 * <p>
	 * By default, this class does nothing. This class may be overridden by implementors in order to perform transformations. This operation is executed after position values are
	 * retrieved and modified, but just before position values are aggregated according to the selected {@link #aggregationOperation aggregation operation}.
	 *
	 * @param positionValueHolderList the list of position value holders to be transformed
	 * @param transactionDate         the transaction date on which the operation is being performed
	 * @param context                 the context in which the transformations shall be performed; this context may be used for caching
	 * @param runSubDetailList        the list of rule run details to which evaluation details should be added
	 * @return the list of transformed position value holders
	 */
	protected List<CompliancePositionValueHolder<T>> applyPositionTransformations(List<CompliancePositionValueHolder<T>> positionValueHolderList, Date transactionDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return positionValueHolderList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the total calculated value for the given positions.
	 * <p>
	 * The resulting position values are determined via the following procedure:
	 * <ol>
	 * <li>Determine the value for each position via {@link #getPositionValue(AccountingPositionInfo, Date, Context)}
	 * <li>Multiply each value by the {@link #multiplierMarketDataFieldId market data multiplier}, if present
	 * <li>Transform the value for each position using the {@link #transformationType specified transformation}
	 * <li>Transform the resulting collection of position values using {@link #applyPositionTransformations(List, Date, Context, List)}, if implemented
	 * <li>Aggregate the resulting position values using the {@link #aggregationOperation selected aggregation operation}
	 * <li>Apply the {@link #multiplier specified multiplier}
	 * </ol>
	 * <p>
	 * Of note in this procedure is that transformations, if applicable, are applied to each individual position value, while the multiplier, if applicable, is applied to the total
	 * accumulated value.
	 *
	 * @param positionList     the list of positions for which calculated values should be evaluated
	 * @param transactionDate  the date for which the calculator should be applied
	 * @param context          the cache context for the current run
	 * @param runSubDetailList the list of rule run details to which evaluation details should be added
	 * @return the sum of the calculated values
	 */
	private BigDecimal getCalculatedValue(List<T> positionList, Date transactionDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Package individual positions with calculated values into position value holders
		List<CompliancePositionValueHolder<T>> positionValueHolderList = getModifiedPositionValues(positionList, transactionDate, context);
		ComplianceRuleRunSubDetail initialPositionListSubDetail = new ComplianceRuleRunSubDetail();
		initialPositionListSubDetail.setDescription("Initial Position List (Size: " + positionValueHolderList.size() + ")");
		for (CompliancePositionValueHolder<T> positionGroup : positionValueHolderList) {
			ComplianceRuleRunSubDetail positionGroupSubDetail = new ComplianceRuleRunSubDetail();
			// Flatten since all collections should be single-element collections
			T position = CollectionUtils.getOnlyElementStrict(positionGroup.getPositionList());
			positionGroupSubDetail.setDescription(position.getAccountingTransactionId() + ": " + position.getInvestmentSecurity().getLabel());
			positionGroupSubDetail.setResultValue(positionGroup.getValue());
			initialPositionListSubDetail.addSubDetail(positionGroupSubDetail);
		}
		runSubDetailList.add(initialPositionListSubDetail);

		List<CompliancePositionValueHolder<T>> transformedPositionValueHolderList = applyPositionTransformations(positionValueHolderList, transactionDate, context, runSubDetailList);

		BigDecimal aggregatedValue = getAggregatedPositionValue(transformedPositionValueHolderList);
		BigDecimal aggregatedMultipliedValue = MathUtils.multiply(aggregatedValue, getMultiplier());
		ComplianceRuleRunSubDetail aggregatedValueSubDetail = new ComplianceRuleRunSubDetail();
		aggregatedValueSubDetail.setDescription("Final Value (" + getAggregationOperation().getLabel() + ")");
		aggregatedValueSubDetail.setResultValue(aggregatedMultipliedValue);
		runSubDetailList.add(aggregatedValueSubDetail);
		return aggregatedMultipliedValue;
	}


	/**
	 * Gets the position values after modifiers (market data multipliers and transformations) are applied.
	 *
	 * @param positionList    the list of positions whose values should be retrieved
	 * @param transactionDate the date for which the values should be retrieved
	 * @param context         the cache context for the current run
	 * @return the list of position value holders with values attached
	 */
	private List<CompliancePositionValueHolder<T>> getModifiedPositionValues(List<T> positionList, Date transactionDate, Context context) {
		return CollectionUtils.getConverted(positionList, position -> {
			BigDecimal value = getPositionValue(position, transactionDate, context);
			value = applyMarketDataMultiplier(value, position, transactionDate);
			value = getTransformationType().transform(value);
			return new CompliancePositionValueHolder<>(value, Collections.singletonList(position));
		});
	}


	/**
	 * Applies the configured {@link #multiplierMarketDataFieldId market data multiplier} to the provided value for the given position and transaction date.
	 *
	 * @param positionValue   the derived position value
	 * @param position        the position associated with the given value
	 * @param transactionDate the date for which the market data field value shall be retrieved
	 * @return the position value multiplied by the market data field multiplier
	 */
	private BigDecimal applyMarketDataMultiplier(BigDecimal positionValue, T position, Date transactionDate) {
		AccountingTransactionInfo transactionInfo = getAccountingTransactionInfo(position);
		BigDecimal marketDataFieldMultiplier = getMarketDataFieldMultiplier(transactionInfo, transactionDate);
		return MathUtils.multiply(positionValue, marketDataFieldMultiplier);
	}


	/**
	 * Calculates the aggregated value for the given collection of position value holders using the configured {@link #aggregationOperation aggregation operation}.
	 * <p>
	 * Behavior for aggregation operations is normalized to return <tt>null</tt> for empty collections of position value holders. This is relevant for aggregation operations which
	 * do not have a logical value for empty sets.
	 *
	 * @param positionValueHolderList the collection of position value holders
	 * @return the aggregated value using the selected aggregation operation
	 */
	private BigDecimal getAggregatedPositionValue(List<CompliancePositionValueHolder<T>> positionValueHolderList) {
		// Early exit clause: Return 0 for aggregation operations which do not allow empty lists
		if (CollectionUtils.isEmpty(positionValueHolderList)) {
			return BigDecimal.ZERO;
		}

		// Aggregate value
		List<BigDecimal> positionValueList = CollectionUtils.getConverted(positionValueHolderList, CompliancePositionValueHolder::getValue);
		return getAggregationOperation().aggregate(positionValueList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the market data field multiplier to use for the given position. This multiplier is the value of the specified market data field for the given position on the given
	 * date.
	 * <p>
	 * This is only applicable if {@link #multiplierMarketDataFieldId} is set. If no market data field is given, this will return {@link BigDecimal#ONE}.
	 *
	 * @param position        the position for which to retrieve the market data field multiplier value
	 * @param transactionDate the date for which the market data field multiplier value shall be retrieved
	 * @return the market data field value, or {@link BigDecimal#ONE} if no market data field is specified
	 */
	private BigDecimal getMarketDataFieldMultiplier(AccountingTransactionInfo position, Date transactionDate) {
		// Guard-clause: No market data field was supplied
		if (getMultiplierMarketDataFieldId() == null) {
			return BigDecimal.ONE;
		}

		// Retrieve the market data value
		String marketDataFieldName = getMarketDataFieldService().getMarketDataField(getMultiplierMarketDataFieldId()).getName();
		MarketDataValueHolder marketDataValue = getMarketDataFieldService().getMarketDataValueForDate(position.getInvestmentSecurity(), transactionDate, marketDataFieldName, null, isMultiplierMarketDataFieldFlexibleLookup(), false);
		AssertUtils.assertNotNull(marketDataValue, "Unable to find market data for security [%s] and field [%s] on date [%tF]", position.getInvestmentSecurity().getSymbol(), marketDataFieldName, transactionDate);
		return marketDataValue.getMeasureValue();
	}


	/**
	 * Determines if the given position satisfies the configured position filters.
	 *
	 * @param position the position to check
	 * @return {@code true} if the position satisfies the configured filters, or {@code false} otherwise
	 */
	private boolean isPositionApplicable(T position) {
		// Pre-populate validation data
		InvestmentSecurity security = position.getInvestmentSecurity();
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentType type = hierarchy.getInvestmentType();
		InvestmentTypeSubType subType = hierarchy.getInvestmentTypeSubType();
		InvestmentTypeSubType2 subType2 = hierarchy.getInvestmentTypeSubType2();

		// Validate position, short-circuiting in order of (generally) increasing resource cost and decreasing specificity
		boolean include = (getPositionType() == null || getPositionType().isMatchingPosition(getAccountingTransactionInfo(position)));
		include = include && CompareUtils.isFilterMatched(getInstrumentHierarchyId(), BeanUtils.getBeanIdentity(hierarchy));
		include = include && CompareUtils.isFilterMatched(getInvestmentTypeSubType2Id(), BeanUtils.getBeanIdentity(subType2));
		include = include && CompareUtils.isFilterMatched(getInvestmentTypeSubTypeId(), BeanUtils.getBeanIdentity(subType));
		include = include && CompareUtils.isFilterMatched(getInvestmentTypeId(), BeanUtils.getBeanIdentity(type));
		include = include && (getInvestmentGroupId() == null || getInvestmentGroupService().isInvestmentSecurityInGroup(getInvestmentGroupId(), security.getId()));
		include = include && (getInvestmentSecurityGroupId() == null || getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), getInvestmentSecurityGroupId()));
		return include;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public AccountingPositionTypes getPositionType() {
		return this.positionType;
	}


	public void setPositionType(AccountingPositionTypes positionType) {
		this.positionType = positionType;
	}


	public Short getMultiplierMarketDataFieldId() {
		return this.multiplierMarketDataFieldId;
	}


	public void setMultiplierMarketDataFieldId(Short multiplierMarketDataFieldId) {
		this.multiplierMarketDataFieldId = multiplierMarketDataFieldId;
	}


	public boolean isMultiplierMarketDataFieldFlexibleLookup() {
		return this.multiplierMarketDataFieldFlexibleLookup;
	}


	public void setMultiplierMarketDataFieldFlexibleLookup(boolean multiplierMarketDataFieldFlexibleLookup) {
		this.multiplierMarketDataFieldFlexibleLookup = multiplierMarketDataFieldFlexibleLookup;
	}


	public MathTransformationTypes getTransformationType() {
		return this.transformationType;
	}


	public void setTransformationType(MathTransformationTypes transformationType) {
		this.transformationType = transformationType;
	}


	public AggregationOperations getAggregationOperation() {
		return this.aggregationOperation;
	}


	public void setAggregationOperation(AggregationOperations aggregationOperation) {
		this.aggregationOperation = aggregationOperation;
	}


	public BigDecimal getMultiplier() {
		return this.multiplier;
	}


	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}
}
