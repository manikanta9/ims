package com.clifton.compliance.rule.type;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBeanType;


/**
 * The <code>ComplianceRuleType</code> class defines the types of compliance rules:
 * Client Approved Contracts
 * Fat Finger Trader Limits
 * Notional Limits
 * Cash Exposure Violation
 * <p/>
 * Other rules: Exchange Contract Limits, ... various 40 Act Rules
 *
 * @author apopp
 */
public class ComplianceRuleType extends NamedEntity<Short> {

	public static final String EXECUTING_BROKER_RULE_TYPE_NAME = "Executing Broker Rule";

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Defines the implementation logic for rules of this type. Each rule will be linked to
	 * corresponding SystemBean of this type and can be further customized via available bean properties.
	 */
	private SystemBeanType ruleEvaluatorType;

	/**
	 * Defines if this rule is a rollup of other rules
	 */
	private boolean rollupRule;

	/**
	 * Specifies whether instances of this rule can be assigned to multiple client accounts.
	 */
	private boolean multipleAssignmentAllowed;

	/**
	 * If set, will display Specificity Scope fields on the rule. Otherwise, they will not be applicable (hidden).
	 * For example, set for Long/Short rule and not set for Industry Concentration.
	 */
	private boolean investmentSecuritySpecific;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanType getRuleEvaluatorType() {
		return this.ruleEvaluatorType;
	}


	public void setRuleEvaluatorType(SystemBeanType ruleEvaluatorType) {
		this.ruleEvaluatorType = ruleEvaluatorType;
	}


	public boolean isMultipleAssignmentAllowed() {
		return this.multipleAssignmentAllowed;
	}


	public void setMultipleAssignmentAllowed(boolean multipleAssignmentAllowed) {
		this.multipleAssignmentAllowed = multipleAssignmentAllowed;
	}


	public boolean isRollupRule() {
		return this.rollupRule;
	}


	public void setRollupRule(boolean rollupRule) {
		this.rollupRule = rollupRule;
	}


	public boolean isInvestmentSecuritySpecific() {
		return this.investmentSecuritySpecific;
	}


	public void setInvestmentSecuritySpecific(boolean investmentSecuritySpecific) {
		this.investmentSecuritySpecific = investmentSecuritySpecific;
	}
}
