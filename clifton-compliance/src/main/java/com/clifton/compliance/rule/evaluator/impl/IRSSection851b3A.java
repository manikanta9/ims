package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitTypes;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * The <code>IRSSection851b3A</code>
 * <p>
 * <p>
 * IRS Section 851(b)(3)A (To qualify as a RIC for tax treatment):  At least 5% / 50% of the Fund's total assets must be represented by:
 * 1) Cash and Cash items (including receivables);
 * 2) Government securities;
 * 3) Securities of other investment companies;
 * 4) Other securities which meet the following limitations:
 * a) securities of any one issuer not exceeding 5% of the
 * Fund's total assets and; b) securities that do not
 * represent more than 10% of the outstanding voting
 * securities of any one issuer. (These two restrictions are
 * broken out separately below)
 * <p>
 * Rule tested as follows:  Any Ultimate issuer (excluding: Government securities) that exceeds 5% of the Funds Total Assets gets put into a bucket and the total of the over 5% Ultimate issuers must be less than 50% of the Funds Total Assets.
 * <p>
 * Also note if the Fund owns more than 10% of a securities outstanding voting shares then it automatically goes into the greater than 5% bucket regardless of the percentage of the Funds Total Assets.
 *
 * @author apopp
 */
public class IRSSection851b3A extends BaseComplianceRuleRatioEvaluator {

	private ComplianceRulePositionHandler complianceRulePositionHandler;

	private boolean excludeCashCollateral;
	private BigDecimal maxSharesOutstanding;
	private BigDecimal maxAssetPercentageAllowed;

	/*
	 * This rule needs to be able to exclude U.S. Gov issued securities. To be more flexible we allow the rule to
	 * specify a business company issuer to exclude. All securities whose ultimate issuer falls under this business
	 * company will be excluded from the processing of the rule
	 */
	private Integer excludeBusinessCompanyId;

	//This rule needs to be able to allow exclusion of RIC securities, defined by TYPE = "Registered Investment Company"
	private Short excludeBusinessCompanyTypeId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return "Bucket - Ultimate Issuer and Share Violators";
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();
		ruleEvaluationConfig.setIncludeCashCollateral(!isExcludeCashCollateral());
		configList.add(ruleEvaluationConfig);
		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap = ComplianceUtils.mapPositionListByUltimateIssuer(ComplianceUtils.filterByCompanyType(
				ruleEvaluationConfig.getPositionList(), getExcludeBusinessCompanyTypeId(), false));

		ComplianceUtils.filterByCompany(issuerPositionMap, getExcludeBusinessCompanyId(), false);
		List<ComplianceRulePosition> positionListFiltered = aggregatePositionsByCompanyMap(issuerPositionMap);

		Context context = ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext();
		//Ultimate issuer section
		getComplianceRuleFilterHandler().filterByUltimateIssuerValue(context, issuerPositionMap, true, getMaxAssetPercentageAllowed(), ComplianceRuleLimitTypes.MAXIMUM, ruleEvaluationConfig.getProcessDate(),
				evaluateDenominator(ruleEvaluationConfig));

		Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap = ComplianceUtils.mapIssuerBySecurityPositionList(issuerPositionMap);
		issuerSecurityPositionMap.remove(null);

		ComplianceRuleRunDetail assetDetail = getComplianceRuleReportHandler().createFullRunDetail(context, issuerSecurityPositionMap, ruleEvaluationConfig.getProcessDate(),
				ComplianceRulePositionValues.MARKET_VALUE);

		//Voting shares section
		Map<InvestmentSecurity, List<ComplianceRulePosition>> votingSecurityMapping = ComplianceUtils.mapPositionListBySecurity(positionListFiltered);

		filterAccountedForSecurities(votingSecurityMapping, issuerSecurityPositionMap);
		getComplianceRuleFilterHandler().filterByVotingShares(context, votingSecurityMapping, true, getMaxSharesOutstanding(), ComplianceRuleLimitTypes.MAXIMUM, ruleEvaluationConfig.getProcessDate());

		ComplianceRuleRunDetail sharesDetail = getComplianceRuleReportHandler().createFullRunDetail(context, votingSecurityMapping, ruleEvaluationConfig.getProcessDate(),
				ComplianceRulePositionValues.MARKET_VALUE);

		//Build report section
		ComplianceRuleRunSubDetail assetBucket = ComplianceRuleRunSubDetail.create("Ultimate Issuers Exceeding " + getMaxAssetPercentageAllowed() + "% of Funds Total Assets",
				null, null, assetDetail.getChildren());
		ComplianceRuleRunSubDetail sharesBucket = ComplianceRuleRunSubDetail.create("Securities exceeding " + getMaxSharesOutstanding() + "% of Outstanding Voting Shares", null,
				null, sharesDetail.getChildren());

		return getComplianceRuleReportHandler().createRuleRunDetail(null, null, false, null, CollectionUtils.createList(assetBucket, sharesBucket));
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getComplianceRulePositionHandler().getAccountTotalValue(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ruleEvaluationConfig.getPositionList(), ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE,
				ruleEvaluationConfig.retrieveAccountIdList(), isUseTotalAssets(), isExcludeCashCollateral());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Filter out securities we have already included in another bucket
	 *
	 * @param votingSecurityMapping
	 * @param issuerSecurityPositionMap
	 */
	private void filterAccountedForSecurities(Map<InvestmentSecurity, List<ComplianceRulePosition>> votingSecurityMapping,
	                                          Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap) {
		Iterator<InvestmentSecurity> iterator = votingSecurityMapping.keySet().iterator();

		while (iterator.hasNext()) {
			InvestmentSecurity security = iterator.next();

			if (security == null) {
				continue;
			}

			for (BusinessCompany company : issuerSecurityPositionMap.keySet()) {
				if (security.getBusinessCompany() != null && CompareUtils.isEqual(security.getBusinessCompany().getRootParent().getId(), company.getId())) {
					iterator.remove();
				}
			}
		}
	}


	private List<ComplianceRulePosition> aggregatePositionsByCompanyMap(Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap) {
		List<ComplianceRulePosition> positionList = new ArrayList<>();

		for (Map.Entry<BusinessCompany, List<ComplianceRulePosition>> businessCompanyListEntry : issuerPositionMap.entrySet()) {
			positionList.addAll(businessCompanyListEntry.getValue());
		}

		return positionList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public boolean isExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}


	public BigDecimal getMaxSharesOutstanding() {
		return this.maxSharesOutstanding;
	}


	public void setMaxSharesOutstanding(BigDecimal maxSharesOutstanding) {
		this.maxSharesOutstanding = maxSharesOutstanding;
	}


	public BigDecimal getMaxAssetPercentageAllowed() {
		return this.maxAssetPercentageAllowed;
	}


	public void setMaxAssetPercentageAllowed(BigDecimal maxAssetPercentageAllowed) {
		this.maxAssetPercentageAllowed = maxAssetPercentageAllowed;
	}


	public Integer getExcludeBusinessCompanyId() {
		return this.excludeBusinessCompanyId;
	}


	public void setExcludeBusinessCompanyId(Integer excludeBusinessCompanyId) {
		this.excludeBusinessCompanyId = excludeBusinessCompanyId;
	}


	public Short getExcludeBusinessCompanyTypeId() {
		return this.excludeBusinessCompanyTypeId;
	}


	public void setExcludeBusinessCompanyTypeId(Short excludeBusinessCompanyTypeId) {
		this.excludeBusinessCompanyTypeId = excludeBusinessCompanyTypeId;
	}
}
