package com.clifton.compliance.rule.limit;


import com.clifton.compliance.rule.limit.typelimit.ComplianceRuleTypeLimit;
import com.clifton.compliance.rule.limit.typelimit.ComplianceRuleTypeLimitSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ComplianceRuleLimitServiceImpl implements ComplianceRuleLimitService {

	private AdvancedUpdatableDAO<ComplianceRuleTypeLimit, Criteria> complianceRuleTypeLimitDAO;
	private AdvancedUpdatableDAO<ComplianceRuleLimitType, Criteria> complianceRuleLimitTypeDAO;


	@Override
	public ComplianceRuleLimitType getComplianceRuleLimitType(short id) {
		return getComplianceRuleLimitTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRuleLimitType> getComplianceRuleLimitTypeList(final ComplianceRuleLimitTypeSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getComplianceRuleTypeId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(ComplianceRuleTypeLimit.class, "limit");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("limit.complianceRuleLimitType.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("complianceRuleType.id", searchForm.getComplianceRuleTypeId()));

					criteria.add(Subqueries.exists(sub));
				}
			}
		};

		return getComplianceRuleLimitTypeDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public ComplianceRuleTypeLimit getComplianceRuleTypeLimit(int id) {
		return getComplianceRuleTypeLimitDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRuleTypeLimit> getComplianceRuleTypeLimitList(ComplianceRuleTypeLimitSearchForm searchForm) {
		return getComplianceRuleTypeLimitDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ComplianceRuleTypeLimit saveComplianceRuleTypeLimit(ComplianceRuleTypeLimit bean) {
		return getComplianceRuleTypeLimitDAO().save(bean);
	}


	@Override
	public void deleteComplianceRuleTypeLimit(int id) {
		getComplianceRuleTypeLimitDAO().delete(id);
	}


	public AdvancedUpdatableDAO<ComplianceRuleTypeLimit, Criteria> getComplianceRuleTypeLimitDAO() {
		return this.complianceRuleTypeLimitDAO;
	}


	public void setComplianceRuleTypeLimitDAO(AdvancedUpdatableDAO<ComplianceRuleTypeLimit, Criteria> complianceRuleTypeLimitDAO) {
		this.complianceRuleTypeLimitDAO = complianceRuleTypeLimitDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleLimitType, Criteria> getComplianceRuleLimitTypeDAO() {
		return this.complianceRuleLimitTypeDAO;
	}


	public void setComplianceRuleLimitTypeDAO(AdvancedUpdatableDAO<ComplianceRuleLimitType, Criteria> complianceRuleLimitTypeDAO) {
		this.complianceRuleLimitTypeDAO = complianceRuleLimitTypeDAO;
	}
}
