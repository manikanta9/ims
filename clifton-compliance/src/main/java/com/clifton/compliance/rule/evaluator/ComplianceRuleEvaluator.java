package com.clifton.compliance.rule.evaluator;


import com.clifton.compliance.run.rule.ComplianceRuleRun;


/**
 * The <code>ComplianceRuleEvaluator</code> is the base class entry point for real time rules.
 * <p>
 * A rule evaluator will use this which will evaluate all relevant real-time rules.
 * The results of the executions will be properly translated into a list of rule violations
 *
 * @author stevenf
 */
public interface ComplianceRuleEvaluator {

	/**
	 * Evaluates the rule using the provided rule evaluation configuration, returning the {@link ComplianceRuleRun} with the run results.
	 *
	 * @param ruleEvaluationConfig the config with which to evaluate the compliance rule
	 * @return the results object for the rule run
	 */
	ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig);
}
