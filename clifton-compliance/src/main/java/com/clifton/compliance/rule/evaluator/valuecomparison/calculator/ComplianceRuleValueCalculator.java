package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.valuecomparison.ComplianceRuleValueComparisonEvaluator;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;

import java.math.BigDecimal;
import java.util.List;


/**
 * The {@link ComplianceRuleValueCalculator} interface provides methods for calculating values during a compliance run.
 * <p>
 * Implementors may be used by compliance rules (e.g., {@link ComplianceRuleValueComparisonEvaluator}) to calculate a wide variety of values, such as asset
 * totals, position totals, or manager balances.
 *
 * @author MikeH
 */
public interface ComplianceRuleValueCalculator {

	/**
	 * Calculates the value using properties provided by the given configuration object.
	 *
	 * @param ruleEvaluationConfig the configuration object
	 * @param runSubDetailList     the list of rule run details to which evaluation details should be added
	 * @return the calculated value
	 */
	BigDecimal calculate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList);
}
