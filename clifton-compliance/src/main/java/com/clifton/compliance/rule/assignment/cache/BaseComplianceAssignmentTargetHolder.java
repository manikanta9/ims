package com.clifton.compliance.rule.assignment.cache;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.cache.specificity.SimpleSpecificityScope;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;

import java.util.Date;
import java.util.Objects;


/**
 * The {@link SpecificityCache} <i>target type</i> for {@link ComplianceRuleAssignment} specificity entries.
 * <p>
 * This target type contains relevant information for specificity evaluations and for retrieval for rule assignment entities.
 *
 * @author MikeH
 */
public abstract class BaseComplianceAssignmentTargetHolder implements SpecificityTargetHolder {

	// Rule fields
	private final int ruleId;
	private final int ruleTypeId;
	private final boolean batch;
	private final boolean realTime;

	// Assignment fields
	private final int ruleAssignmentId;
	private final Date startDate;
	private final Date endDate;

	// Specificity target holder fields
	private final String key;
	private final SpecificityScope scope;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseComplianceAssignmentTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment, String key, int scopeId) {
		this.ruleId = rule.getId();
		this.ruleTypeId = rule.getRuleType().getId();
		this.batch = rule.isBatch();
		this.realTime = rule.isRealTime();

		this.ruleAssignmentId = assignment.getId();
		this.startDate = assignment.getStartDate();
		this.endDate = assignment.getEndDate();

		this.key = key;
		/*
		 * Assignment target holder scopes include all assignments at the most-specific applicable level of specificity when queried. In other words, if multiple results are found
		 * with the same (most-specific) level of specificity, then all of those results will be returned.
		 *
		 * This is intended to address instances where clear specificity rules do not apply. For example, when both a rule is applied to an account via both a roll-up rule
		 * assignment and a direct rule assignment simultaneously, it is not clear whether the roll-up rule assignment should be overridden by the direct assignment or vice versa.
		 * Additionally, this addresses issues where attempting to incorporate specificity would require excessively complex logic, such as the aforementioned example.
		 *
		 * This can lead to redundant evaluations when such instances arise.
		 */
		this.scope = new SimpleSpecificityScope(scopeId, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int hashCode() {
		return Objects.hash(
				this.ruleId,
				this.ruleAssignmentId,
				this.key
		);
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BaseComplianceAssignmentTargetHolder that = (BaseComplianceAssignmentTargetHolder) o;
		return this.ruleId == that.ruleId &&
				this.ruleAssignmentId == that.ruleAssignmentId &&
				this.key.equals(that.key);
	}


	@Override
	public String toString() {
		return "BaseComplianceAssignmentTargetHolder{" +
				"ruleId=" + this.ruleId +
				", ruleTypeId=" + this.ruleTypeId +
				", batch=" + this.batch +
				", realTime=" + this.realTime +
				", ruleAssignmentId=" + this.ruleAssignmentId +
				", startDate=" + this.startDate +
				", endDate=" + this.endDate +
				", key='" + this.key + '\'' +
				", scope=" + this.scope +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public SpecificityScope getScope() {
		return this.scope;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getRuleId() {
		return this.ruleId;
	}


	public int getRuleTypeId() {
		return this.ruleTypeId;
	}


	public boolean isBatch() {
		return this.batch;
	}


	public boolean isRealTime() {
		return this.realTime;
	}


	public int getRuleAssignmentId() {
		return this.ruleAssignmentId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}
}
