package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.math.MathTransformationTypes;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link ComplianceRuleAccountBalanceValueCalculator} is an implementor of {@link ComplianceRuleValueCalculator}. This type allows for calculations based
 * on account balances.
 * <p>
 * This class allows for filtering based on {@link AccountingAccountType} and {@link AccountingAccount}. The calculated result shall be the total market value
 * evaluated for all accounts of the given filtered types.
 *
 * @author MikeH
 */
public class ComplianceRuleAccountBalanceValueCalculator extends BaseComplianceRuleDateAwareValueCalculator implements ValidationAware {

	private AccountingBalanceService accountingBalanceService;
	private AccountingValuationService accountingValuationService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	// GL account filters
	private List<Short> accountingAccountTypeIdList;
	private List<Short> accountingAccountIdList;

	// Transformations, in order of application
	private MathTransformationTypes transformationType;
	private BigDecimal multiplier;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected BigDecimal calculateImplWithCalculateDate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		Date calculationDate = ruleEvaluationConfig.getCalculationDate();
		List<AccountingBalanceValue> accountBalanceValueList = getAccountBalanceValueList(ruleEvaluationConfig.retrieveAccountIdList(), calculationDate);
		BigDecimal balanceTotal = calculateBalanceTotal(accountBalanceValueList, ruleEvaluationConfig.retrieveClientAccount(), calculationDate, runSubDetailList);
		return getTransformedValue(balanceTotal, runSubDetailList);
	}


	@Override
	public void validate() throws ValidationException {
		super.validate();
		ValidationUtils.assertMutuallyExclusive("One and only one of the \"GL Account Types\" or \"GL Accounts\" fields must be specified.",
				getAccountingAccountTypeIdList(), getAccountingAccountIdList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of balance values for the given client accounts on the given date.
	 * <p>
	 * Evaluated accounting accounts are filtered according to the configuration for this calculator.
	 *
	 * @param clientAccountIdList the list of client accounts for which to retrieve balance values
	 * @param processDate         the date for which the balance values should be retrieved
	 * @return the list of retrieved balance values
	 */
	private List<AccountingBalanceValue> getAccountBalanceValueList(List<Integer> clientAccountIdList, Date processDate) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(processDate);
		searchForm.setClientInvestmentAccountIds(CollectionUtils.toArray(clientAccountIdList, Integer.class));
		searchForm.setPendingActivityRequest(PendingActivityRequests.ALL_WITH_MERGE_AND_PREVIEW);

		// Apply user-configured filters
		searchForm.setAccountingAccountTypeIds(CollectionUtils.toArrayOrNull(getAccountingAccountTypeIdList(), Short.class));
		searchForm.setAccountingAccountIds(CollectionUtils.toArrayOrNull(getAccountingAccountIdList(), Short.class));

		// Retrieve balance values
		return getAccountingValuationService().getAccountingBalanceValueList(searchForm);
	}


	/**
	 * Calculates the total value from the given list of {@link AccountingBalanceValue} entities.
	 *
	 * @param accountBalanceValueList the list of balance values
	 * @param clientAccount           the client account determining the base currency to which balance values should be converted
	 * @param processDate             the process date for which aggregated values are desired
	 * @param runSubDetailList        the list of rule run details to which evaluation details should be added
	 * @return the total aggregated balance value
	 */
	private BigDecimal calculateBalanceTotal(List<AccountingBalanceValue> accountBalanceValueList, InvestmentAccount clientAccount, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Get balance total
		Map<AccountingBalanceValue, BigDecimal> balanceToValueMap = CollectionUtils.getMapped(accountBalanceValueList, balance -> calculateAccountBalanceForClient(balance, clientAccount, processDate));
		BigDecimal balanceTotal = CoreMathUtils.sum(balanceToValueMap.values());

		// Add details
		ComplianceRuleRunSubDetail balanceListSubDetail = ComplianceRuleRunSubDetail.create("Account Balance Values", balanceTotal);
		CollectionUtils.getStream(accountBalanceValueList).collect(Collectors.groupingBy(AccountingBalanceValue::getHoldingInvestmentAccount))
				.forEach((holdingAccount, balanceValueList) -> {
					BigDecimal holdingAccountValue = CoreMathUtils.sum(CollectionUtils.getConverted(balanceValueList, balanceToValueMap::get));
					List<ComplianceRuleRunSubDetail> balanceValueSubDetailList = CollectionUtils.getConverted(balanceValueList, value ->
							ComplianceRuleRunSubDetail.create(
									String.format("Client Account: [%s]. Security: [%s].",
											value.getClientInvestmentAccount().getLabel(),
											value.getInvestmentSecurity().getLabel()),
									balanceToValueMap.get(value)));
					ComplianceRuleRunSubDetail holdingAccountSubDetail = ComplianceRuleRunSubDetail.create(
							String.format("Holding Account: [%s]", holdingAccount.getLabel()),
							holdingAccountValue);
					balanceValueSubDetailList.sort((detail1, detail2) -> StringUtils.compare(detail1.getLabel(), detail2.getLabel()));
					holdingAccountSubDetail.setChildren(balanceValueSubDetailList);
					balanceListSubDetail.addSubDetail(holdingAccountSubDetail);
				});
		CollectionUtils.sort(balanceListSubDetail.getChildren(), (detail1, detail2) -> StringUtils.compare(detail1.getLabel(), detail2.getLabel()));
		runSubDetailList.add(balanceListSubDetail);

		return balanceTotal;
	}


	/**
	 * Calculates the given account balance value when attributed to the given client account.
	 * <p>
	 * This converts the balance value from its provided base currency, which is the base currency of its associated client account, to the base currency of provided client
	 * account.
	 *
	 * @param balanceValue           the balance value to convert to the given client account
	 * @param clientAccount          the target account to which the resulting value may be attributed
	 * @param currencyConversionDate the date for which the FX rate shall be retrieved
	 * @return the value of the account balance in the base currency of the target client account
	 */
	private BigDecimal calculateAccountBalanceForClient(AccountingBalanceValue balanceValue, InvestmentAccount clientAccount, Date currencyConversionDate) {
		BigDecimal accountBalance = balanceValue.getBaseMarketValue();

		// Get exchange rate
		String localCurrency = balanceValue.getClientInvestmentAccount().getBaseCurrency().getSymbol();
		String baseCurrency = clientAccount.getBaseCurrency().getSymbol();
		BigDecimal fxRate = BigDecimal.ONE;
		if (!CompareUtils.isEqual(localCurrency, baseCurrency)) {
			fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(clientAccount.toClientAccount(), localCurrency, baseCurrency, currencyConversionDate).flexibleLookup());
		}

		// Apply exchange rate
		return MathUtils.multiply(accountBalance, fxRate);
	}


	/**
	 * Transforms the given calculated value according to the configured transformation flags.
	 *
	 * @param calculatedValue  the calculated value
	 * @param runSubDetailList the list of rule run details to which evaluation details should be added
	 * @return the transformed value
	 */
	private BigDecimal getTransformedValue(BigDecimal calculatedValue, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Transformation order is relevant and should be documented for client
		BigDecimal transformedValue = getTransformationType().transform(calculatedValue);
		BigDecimal multipliedValue = getMultiplier().multiply(transformedValue);

		// Add details
		ComplianceRuleRunSubDetail transformationSubDetail = ComplianceRuleRunSubDetail.create("Final transformed value", multipliedValue);
		transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Transformation: [%s]", StringUtils.capitalize(getTransformationType().toString())), transformedValue));
		transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Multiplier: [%s]", getMultiplier()), multipliedValue));
		runSubDetailList.add(transformationSubDetail);

		return multipliedValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public List<Short> getAccountingAccountTypeIdList() {
		return this.accountingAccountTypeIdList;
	}


	public void setAccountingAccountTypeIdList(List<Short> accountingAccountTypeIdList) {
		this.accountingAccountTypeIdList = accountingAccountTypeIdList;
	}


	public List<Short> getAccountingAccountIdList() {
		return this.accountingAccountIdList;
	}


	public void setAccountingAccountIdList(List<Short> accountingAccountIdList) {
		this.accountingAccountIdList = accountingAccountIdList;
	}


	public MathTransformationTypes getTransformationType() {
		return this.transformationType;
	}


	public void setTransformationType(MathTransformationTypes transformationType) {
		this.transformationType = transformationType;
	}


	public BigDecimal getMultiplier() {
		return this.multiplier;
	}


	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}
}
