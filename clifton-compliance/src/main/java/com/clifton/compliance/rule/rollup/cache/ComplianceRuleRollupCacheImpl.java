package com.clifton.compliance.rule.rollup.cache;


import com.clifton.compliance.rule.rollup.ComplianceRuleRollup;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>ComplianceRuleRollupCacheImpl</code> caches
 * rollup relationships for a compliance rule as either the parent or the child.
 * <p>
 * Cache stores ID values of the {@link ComplianceRuleRollup} and when retrieved looks up by id values
 * <p>
 * Calling methods can then filter out on parent or child = rule id to get the list of parents or the list of children
 * <p>
 * Cache is cleared for parent and child when rollup is updated
 * <p>
 * NOTE: This cache is pretty big, because stores empty lists for managers that are neither a parent or a child.
 * so that we don't attempt to do look ups again each time
 *
 * @author Mary Anderson
 */
@Component
public class ComplianceRuleRollupCacheImpl extends SelfRegisteringSimpleDaoCache<ComplianceRuleRollup, Integer, Integer[]> implements ComplianceRuleRollupCache {

	@Override
	public Integer[] getComplianceRuleRollupList(int complianceRuleId) {
		return getCacheHandler().get(getCacheName(), complianceRuleId);
	}


	@Override
	public void storeComplianceRuleRollupList(int complianceRuleId, List<ComplianceRuleRollup> list) {
		Integer[] ids;
		// Create a 0 size array so not null and we know not to look it up
		if (CollectionUtils.isEmpty(list)) {
			ids = new Integer[0];
		}
		else {
			ids = BeanUtils.getBeanIdentityArray(list, Integer.class);
		}
		getCacheHandler().put(getCacheName(), complianceRuleId, ids);
	}


	private void clearComplianceRuleRollupCache(int complianceRuleId) {
		getCacheHandler().remove(getCacheName(), complianceRuleId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<ComplianceRuleRollup> dao, DaoEventTypes event, ComplianceRuleRollup bean, Throwable e) {
		if (e == null) {
			boolean clear = false;
			if (event.isUpdate()) {
				ComplianceRuleRollup originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(originalBean, bean, false))) {
					clear = true;
					// Clear the original reference's cache, not just the new one
					clearComplianceRuleRollupCache(originalBean.getReferenceOne().getId());
					clearComplianceRuleRollupCache(originalBean.getReferenceTwo().getId());
				}
			}
			if (clear || event.isInsert() || event.isDelete()) {
				clearComplianceRuleRollupCache(bean.getReferenceOne().getId());
				clearComplianceRuleRollupCache(bean.getReferenceTwo().getId());
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<ComplianceRuleRollup> dao, DaoEventTypes event, ComplianceRuleRollup bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}
}
