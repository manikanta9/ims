package com.clifton.compliance.rule.limit;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ComplianceRuleLimitTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	//custom
	private Short complianceRuleTypeId;

	@SearchField
	private String name;


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getComplianceRuleTypeId() {
		return this.complianceRuleTypeId;
	}


	public void setComplianceRuleTypeId(Short complianceRuleTypeId) {
		this.complianceRuleTypeId = complianceRuleTypeId;
	}
}
