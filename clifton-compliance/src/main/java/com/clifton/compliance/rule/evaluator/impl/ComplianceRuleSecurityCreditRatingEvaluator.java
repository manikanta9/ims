package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleCreditRatingEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Implementor for {@link BaseComplianceRuleCreditRatingEvaluator}. Compliance rule evaluator type for evaluating credit ratings for securities.
 * <p>
 * This is a security-specific evaluator. The target and the credit rating value assignee for this evaluator are the security.
 *
 * @author MikeH
 */
public class ComplianceRuleSecurityCreditRatingEvaluator extends BaseComplianceRuleCreditRatingEvaluator<InvestmentSecurity, InvestmentSecurity> {

	/**
	 * The list of coupon types for which cash equivalence is determined using the maturity date of the security. For all other coupon types, the next coupon
	 * payment date will be used to determine cash equivalence.
	 */
	private static final String[] CASH_EQUIVALENT_MATURITY_DATE_COUPON_TYPES = new String[]{"None", "Zero", "Fixed", "Fixed OID"};

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private SystemColumnValueHandler systemColumnValueHandler;

	/**
	 * If {@code true}, process cash equivalent securities only.
	 *
	 * @see #isCashEquivalentThresholdSatisfied(InvestmentSecurity, Date)
	 */
	private boolean cashEquivalent;
	/**
	 * The number of days before maturity at which a security becomes considered cash equivalent.
	 */
	private Integer cashEquivalentThreshold;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected InvestmentSecurity getEvaluationTarget(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return ruleEvaluationConfig.getInvestmentSecurityId() != null
				? getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId())
				: null;
	}


	@Override
	protected List<InvestmentSecurity> getTargetAssigneeList(InvestmentSecurity target, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return Collections.singletonList(target);
	}


	@Override
	protected List<ComplianceCreditRating> getAssigneeCreditRatingList(InvestmentSecurity assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Get credit rating values for assignee
		List<? extends ComplianceCreditRatingValue> creditRatingValueList = getComplianceCreditRatingService().getComplianceCreditRatingValueForSecuritiesForSecurity(assignee.getId());

		// Traverse up issuer hierarchy to find first occurrence of credit ratings, if necessary
		BusinessCompany issuerCompany = assignee.getBusinessCompany();
		while (issuerCompany != null && CollectionUtils.isEmpty(creditRatingValueList)) {
			creditRatingValueList = getComplianceCreditRatingService().getComplianceCreditRatingValueForIssuersForIssuer(issuerCompany.getId());
			issuerCompany = issuerCompany.getParent();
		}

		// Get active credit ratings
		return CollectionUtils.getStream(creditRatingValueList)
				.filter(creditRatingValue -> DateUtils.isDateBetween(ruleEvaluationConfig.getProcessDate(), creditRatingValue.getStartDate(), creditRatingValue.getEndDate(), false))
				.map(ComplianceCreditRatingValue::getCreditRating)
				.collect(Collectors.toList());
	}


	@Override
	protected String getTargetType() {
		return "security";
	}


	@Override
	protected String getAssigneeType() {
		return "security";
	}


	@Override
	protected boolean isEvaluationRequired(InvestmentSecurity assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Guard-clause: Do nothing if filter is not active
		if (!isCashEquivalent()) {
			return true;
		}

		// Only apply to securities meeting maturity/coupon-payment threshold for cash equivalence
		return isCashEquivalentThresholdSatisfied(assignee, ruleEvaluationConfig.getProcessDate());
	}


	@Override
	protected String getDetailDescriptionPrefix(InvestmentSecurity target, InvestmentSecurity assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String prefix = null;
		if (ruleEvaluationConfig.getBean() instanceof AccountingPosition) {
			// Use accounting position ID
			prefix = String.valueOf(((AccountingPosition) ruleEvaluationConfig.getBean()).getId());
		}
		return prefix;
	}


	@Override
	protected String getAssigneeLabel(InvestmentSecurity assignee) {
		return assignee.getLabel();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines whether or not the cash equivalent threshold is satisfied by the given assignee on the given process date.
	 *
	 * @param assignee    the security to be evaluated
	 * @param processDate the date on which cash equivalence is being evaluated
	 * @return {@code true} if the cash equivalent threshold is satisfied, or {@code false} otherwise
	 * @see #CASH_EQUIVALENT_MATURITY_DATE_COUPON_TYPES
	 */
	private boolean isCashEquivalentThresholdSatisfied(InvestmentSecurity assignee, Date processDate) {
		// Get the number of days to use to compare cash equivalence
		// Determine whether maturity date or next coupon payment date should be used to determine cash equivalence
		final int daysForCashEquivalence;
		String couponType = (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(assignee, "Security Custom Fields", "Coupon Type", false);
		boolean useMaturityDate = couponType == null || StringUtils.equalsAnyIgnoreCase(couponType, CASH_EQUIVALENT_MATURITY_DATE_COUPON_TYPES);
		if (useMaturityDate) {
			// Use maturity date to determine cash equivalent status
			daysForCashEquivalence = DateUtils.getDaysDifference(assignee.getEndDate(), processDate);
		}
		else {
			// Use next coupon payment date to determine cash equivalent status
			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(assignee.getId());
			searchForm.setTypeName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
			searchForm.setOrderBy("eventDate:asc");
			List<InvestmentSecurityEvent> investmentSecurityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
			Optional<InvestmentSecurityEvent> nextEventOpt = CollectionUtils.getStream(investmentSecurityEventList)
					.filter(event -> DateUtils.isDateAfterOrEqual(event.getEventDate(), processDate))
					.findFirst();
			if (nextEventOpt.isPresent()) {
				daysForCashEquivalence = DateUtils.getDaysDifference(nextEventOpt.get().getEventDate(), processDate);
			}
			else {
				throw new ValidationException(String.format("Unable to determine cash equivalence for the security [%s]. The next coupon payment date could not be found.", assignee.getLabel()));
			}
		}

		return MathUtils.isLessThanOrEqual(daysForCashEquivalence, getCashEquivalentThresholdOrDefault());
	}


	private Integer getCashEquivalentThresholdOrDefault() {
		// Default threshold for cash equivalence to one standard year
		return this.getCashEquivalentThreshold() != null ? this.getCashEquivalentThreshold() : 365;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public boolean isCashEquivalent() {
		return this.cashEquivalent;
	}


	public void setCashEquivalent(boolean cashEquivalent) {
		this.cashEquivalent = cashEquivalent;
	}


	public Integer getCashEquivalentThreshold() {
		return this.cashEquivalentThreshold;
	}


	public void setCashEquivalentThreshold(Integer cashEquivalentThreshold) {
		this.cashEquivalentThreshold = cashEquivalentThreshold;
	}
}
