package com.clifton.compliance.rule.assignment;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class ComplianceRuleAssignmentSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "clientInvestmentAccount.number,clientInvestmentAccount.name,businessService.name,rule.name,rule.description", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "clientInvestmentAccount.number,clientInvestmentAccount.name,businessService.name", sortField = "clientInvestmentAccount.number,businessService.name,rule.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER_AND_SPACE_PADDING)
	private String scopeLabel;

	@SearchField(searchField = "rule.name")
	private String ruleName;

	@SearchField(searchField = "rule.id")
	private Integer ruleId;

	@SearchField(searchField = "subAccountPurpose.id")
	private Short subAccountPurposeId;

	@SearchField(searchField = "subsidiaryPurpose.id")
	private Short subsidiaryPurposeId;

	@SearchField(searchField = "rule.ruleType.id")
	private Short ruleTypeId;

	@SearchField(searchField = "clientInvestmentAccount.name")
	private String accountName;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer accountId;

	@SearchField(searchField = "businessService.id")
	private Short businessServiceId;

	@SearchField(searchField = "businessService.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean businessServiceNotNull;

	@SearchField(searchField = "ignorePassSystemCondition.name")
	private String ignorePassSystemCondition;

	@SearchField(searchField = "ignoreFailSystemCondition.name")
	private String ignoreFailSystemCondition;

	@SearchField(searchField = "ignorePassSystemCondition.id")
	private Integer ignorePassSystemConditionId;

	@SearchField(searchField = "ignoreFailSystemCondition.id")
	private Integer ignoreFailSystemConditionId;

	@SearchField(searchFieldPath = "rule", searchField = "batch")
	private Boolean batchRule;

	@SearchField(searchFieldPath = "rule", searchField = "realTime")
	private Boolean realTimeRule;

	@SearchField(searchFieldPath = "rule.ruleType", searchField = "investmentSecuritySpecific")
	private Boolean investmentSecuritySpecific;

	@SearchField(searchFieldPath = "rule.ruleType", searchField = "rollupRule")
	private Boolean rollupRule;

	@SearchField(searchField = "exclusionAssignment")
	private Boolean exclusionAssignment;

	// Custom search fields
	/**
	 * When present, a condition will be applied such that all returned assignments apply to the given account ID.
	 */
	private Integer appliesToAccountId;
	/**
	 * When present, a condition will be applied such that all returned assignments apply to the given business service ID.
	 */
	private Short appliesToBusinessServiceId;
	/**
	 * If true - both account and service id are null, if false, then either is not null
	 */
	@SearchField(searchField = "businessService.id,clientInvestmentAccount.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE_SORT_ON_ALL_NULL)
	private Boolean global;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getScopeLabel() {
		return this.scopeLabel;
	}


	public void setScopeLabel(String scopeLabel) {
		this.scopeLabel = scopeLabel;
	}


	public String getRuleName() {
		return this.ruleName;
	}


	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}


	public Integer getRuleId() {
		return this.ruleId;
	}


	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}


	public Short getSubAccountPurposeId() {
		return this.subAccountPurposeId;
	}


	public void setSubAccountPurposeId(Short subAccountPurposeId) {
		this.subAccountPurposeId = subAccountPurposeId;
	}


	public Short getSubsidiaryPurposeId() {
		return this.subsidiaryPurposeId;
	}


	public void setSubsidiaryPurposeId(Short subsidiaryPurposeId) {
		this.subsidiaryPurposeId = subsidiaryPurposeId;
	}


	public Short getRuleTypeId() {
		return this.ruleTypeId;
	}


	public void setRuleTypeId(Short ruleTypeId) {
		this.ruleTypeId = ruleTypeId;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Boolean getBusinessServiceNotNull() {
		return this.businessServiceNotNull;
	}


	public void setBusinessServiceNotNull(Boolean businessServiceNotNull) {
		this.businessServiceNotNull = businessServiceNotNull;
	}


	public String getIgnorePassSystemCondition() {
		return this.ignorePassSystemCondition;
	}


	public void setIgnorePassSystemCondition(String ignorePassSystemCondition) {
		this.ignorePassSystemCondition = ignorePassSystemCondition;
	}


	public String getIgnoreFailSystemCondition() {
		return this.ignoreFailSystemCondition;
	}


	public void setIgnoreFailSystemCondition(String ignoreFailSystemCondition) {
		this.ignoreFailSystemCondition = ignoreFailSystemCondition;
	}


	public Integer getIgnorePassSystemConditionId() {
		return this.ignorePassSystemConditionId;
	}


	public void setIgnorePassSystemConditionId(Integer ignorePassSystemConditionId) {
		this.ignorePassSystemConditionId = ignorePassSystemConditionId;
	}


	public Integer getIgnoreFailSystemConditionId() {
		return this.ignoreFailSystemConditionId;
	}


	public void setIgnoreFailSystemConditionId(Integer ignoreFailSystemConditionId) {
		this.ignoreFailSystemConditionId = ignoreFailSystemConditionId;
	}


	public Boolean getBatchRule() {
		return this.batchRule;
	}


	public void setBatchRule(Boolean batchRule) {
		this.batchRule = batchRule;
	}


	public Boolean getRealTimeRule() {
		return this.realTimeRule;
	}


	public void setRealTimeRule(Boolean realTimeRule) {
		this.realTimeRule = realTimeRule;
	}


	public Boolean getInvestmentSecuritySpecific() {
		return this.investmentSecuritySpecific;
	}


	public void setInvestmentSecuritySpecific(Boolean investmentSecuritySpecific) {
		this.investmentSecuritySpecific = investmentSecuritySpecific;
	}


	public Boolean getRollupRule() {
		return this.rollupRule;
	}


	public void setRollupRule(Boolean rollupRule) {
		this.rollupRule = rollupRule;
	}


	public Boolean getExclusionAssignment() {
		return this.exclusionAssignment;
	}


	public void setExclusionAssignment(Boolean exclusionAssignment) {
		this.exclusionAssignment = exclusionAssignment;
	}


	public Integer getAppliesToAccountId() {
		return this.appliesToAccountId;
	}


	public void setAppliesToAccountId(Integer appliesToAccountId) {
		this.appliesToAccountId = appliesToAccountId;
	}


	public Short getAppliesToBusinessServiceId() {
		return this.appliesToBusinessServiceId;
	}


	public void setAppliesToBusinessServiceId(Short appliesToBusinessServiceId) {
		this.appliesToBusinessServiceId = appliesToBusinessServiceId;
	}


	public Boolean getGlobal() {
		return this.global;
	}


	public void setGlobal(Boolean global) {
		this.global = global;
	}
}
