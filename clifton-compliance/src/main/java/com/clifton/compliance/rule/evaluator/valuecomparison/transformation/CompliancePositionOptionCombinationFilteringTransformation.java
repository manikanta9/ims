package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.trade.TradeService;
import com.clifton.trade.marketdata.TradeMarketDataField;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import com.clifton.trade.options.combination.TradeOptionCombination;
import com.clifton.trade.options.combination.TradeOptionCombinationHandler;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionOptionCombinationFilteringTransformation} is a {@link CompliancePositionTransformation} type for filtering {@link CompliancePositionValueHolder
 * position value holders} based on {@link TradeOptionCombination option combination} constraints.
 * <p>
 * This transformation includes configurable constraint filters based on allowed {@link TradeOptionCombinationTypes combination types} and on {@link
 * TradeOptionCombinationHandler#getOptionCombinationDownsidePotential(TradeOptionCombination) downside potential} limits.
 *
 * @author MikeH
 */
public class CompliancePositionOptionCombinationFilteringTransformation implements CompliancePositionTransformation, ValidationAware {

	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private MarketDataFieldService marketDataFieldService;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private TradeService tradeService;
	private TradeOptionCombinationHandler tradeOptionCombinationHandler;

	/**
	 * The allowed combination types, in order of categorization precedence.
	 */
	private TradeOptionCombinationTypes[] combinationTypes;
	/**
	 * The {@link MarketDataField} to use for downside potential limit evaluation, or <code>null</code> if no downside potential constraint applies. This field must be marked as
	 * {@link MarketDataField#isRealTimeReferenceData() real-time reference data}. This will be used in tandem with {@link #downsidePotentialLowerBoundMultiplier} and {@link
	 * #downsidePotentialUpperBoundMultiplier} to determine the downside potential bounds.
	 */
	private Short downsidePotentialMarketDataFieldId;
	private BigDecimal downsidePotentialLowerBoundMultiplier;
	private BigDecimal downsidePotentialUpperBoundMultiplier;
	/**
	 * If <code>true</code>, the downside will be compared per-unit-of-underlying. This will use the option {@link InvestmentSecurity#getPriceMultiplier() contract size} to
	 * determine the amount of the downside potential for each contract that can be attributed to each unit of underling. This can be used to verify, for example, that the downside
	 * potential for each unit of underlying is between 4% and 5% of the current underlying price.
	 * <p>
	 * If this flag is enabled, then the configured combination types must also have the {@link TradeOptionCombinationTypes#isSameUnderlyingQuantity()} flag enabled.
	 */
	private boolean useDownsidePerUnitOfUnderlying;
	/**
	 * If <code>true</code>, the results will include {@link CompliancePositionValueHolder position value holders} which match the constraints. Otherwise, the results will include
	 * all value holders which do not match the constraints.
	 */
	private boolean includeMatches;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return CollectionUtils.getFiltered(positionValueList, valueHolder -> isMatchedValueHolder(valueHolder, runSubDetailList) == isIncludeMatches());
	}


	@Override
	public void validate() throws ValidationException {
		// Validate downside potential configuration
		if (getDownsidePotentialMarketDataFieldId() != null) {
			ValidationUtils.assertTrue(getDownsidePotentialLowerBoundMultiplier() != null || getDownsidePotentialUpperBoundMultiplier() != null, "If a downside potential limit field is provided then a lower bound multiplier or an upper bound multiplier must also be provided.");
			// Validate that the market data field has the correct tag assigned
			MarketDataField downsidePotentialMarketDataField = getMarketDataFieldService().getMarketDataField(getDownsidePotentialMarketDataFieldId());
			List<SystemHierarchyLink> marketDataFieldHierarchyLinkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkList("MarketDataField", downsidePotentialMarketDataField.getId(), MarketDataField.MARKET_DATA_TAG_CATEGORY_NAME);
			boolean tradePricesMarketDataField = marketDataFieldHierarchyLinkList.stream()
					.map(SystemHierarchyLink::getHierarchy)
					.map(NamedEntityWithoutLabel::getName)
					.anyMatch(TradeMarketDataField.TRADE_FIELD_TAG_NAME::equals);
			ValidationUtils.assertTrue(tradePricesMarketDataField, () -> String.format("The downside potential limit market data field must have the \"%s\" tag attached.", TradeMarketDataField.TRADE_FIELD_TAG_NAME), "downsidePotentialMarketDataField");
		}
		// Validate "same quantity" flag when downside-per-unit-of-underlying is used
		if (isUseDownsidePerUnitOfUnderlying()) {
			List<TradeOptionCombinationTypes> nonSameQuantityCombinationTypeList = CollectionUtils.getFiltered(Arrays.asList(getCombinationTypes()), combinationType -> !combinationType.isSameUnderlyingQuantity());
			ValidationUtils.assertEmpty(nonSameQuantityCombinationTypeList, () -> String.format("All configured combination types must have have the \"Same Underlying Quantity\" flag enabled. Violating combination types: %s", CollectionUtils.toString(CollectionUtils.getConverted(nonSameQuantityCombinationTypeList, BeanUtils::getLabel), 5)));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines if the given value holder matches all configured constraints for the transformation.
	 */
	private boolean isMatchedValueHolder(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		boolean matched = true;
		// Validate combination types
		Collection<TradeOptionCombination> combinationList = getTradeOptionCombinationHandler().getTradeOptionCombinationList(positionValueHolder.getPositionList(), getCombinationTypes());
		runSubDetailList.add(ComplianceRuleRunSubDetail.create(String.format("Combination List (Size: %d)", combinationList.size()), combinationList.stream()
				.map(getComplianceRuleReportHandler()::createSubDetailForOptionCombination)
				.sorted(Comparator.comparing(ComplianceRuleRunSubDetail::getLabel))
				.collect(Collectors.toList())));
		Map<Boolean, List<TradeOptionCombination>> combinationListByIsUnknown = CollectionUtils.getPartitioned(combinationList, TradeOptionCombination::isUnknownType);
		List<TradeOptionCombination> unmatchedCombinationList = combinationListByIsUnknown.get(true);
		List<TradeOptionCombination> matchedCombinationList = combinationListByIsUnknown.get(false);
		if (!unmatchedCombinationList.isEmpty()) {
			matched = false;
			runSubDetailList.addAll(getUnmatchedCombinationSubDetailList(unmatchedCombinationList));
		}

		// Validate downside potential values
		if (getDownsidePotentialMarketDataFieldId() != null) {
			List<ComplianceRuleRunSubDetail> downsidePotentialViolationSubDetailList = getCombinationDownsidePotentialViolationSubDetailList(matchedCombinationList);
			runSubDetailList.addAll(downsidePotentialViolationSubDetailList);
			boolean downsidePotentialSatisfied = downsidePotentialViolationSubDetailList.isEmpty();
			matched = matched && downsidePotentialSatisfied;
		}
		return matched;
	}


	/**
	 * Determines if the given value holder satisfies the configured downside potential constraints.
	 */
	private List<ComplianceRuleRunSubDetail> getCombinationDownsidePotentialViolationSubDetailList(List<TradeOptionCombination> combinationList) {
		// Get details for combinations which violate downside potential constraints
		return combinationList.stream()
				.map(this::getCombinationDownsideViolation)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}


	/**
	 * Generates the violation {@link ComplianceRuleRunSubDetail sub-detail} for the combination and the configured constraints, or <code>null</code> if there is no violation.
	 */
	private ComplianceRuleRunSubDetail getCombinationDownsideViolation(TradeOptionCombination combination) {
		// Guard-clause: Combinations must have same underlying quantity when comparing per-unit-of-underlying
		if (isUseDownsidePerUnitOfUnderlying() && !combination.getCombinationType().isSameUnderlyingQuantity()) {
			return ComplianceRuleRunSubDetail.create(String.format("Violation: [%s]: Downside potential per unit of underlying must cannot be evaluated for combinations of type [%s].", combination.getLabel(), BeanUtils.getLabel(combination.getCombinationType())), CollectionUtils.createList(getComplianceRuleReportHandler().createSubDetailForOptionCombination(combination, null)));
		}

		// Evaluate potential constraints
		BigDecimal downsidePotential = isUseDownsidePerUnitOfUnderlying()
				? MathUtils.divide(getTradeOptionCombinationHandler().getOptionCombinationDownsidePotential(combination), combination.getUnderlyingRemainingQuantity())
				: getTradeOptionCombinationHandler().getOptionCombinationDownsidePotential(combination);
		String downsidePotentialStr = StringUtils.coalesce(false, CoreMathUtils.formatNumberMoney(downsidePotential), "Infinity");
		BigDecimal downsidePotentialLimitValue = getCombinationDownsidePotentialLimitValue(combination);
		final String violationMessage;
		if (downsidePotentialLimitValue == null) {
			// Missing potential downside value
			MarketDataField marketDataField = getMarketDataFieldService().getMarketDataField(getDownsidePotentialMarketDataFieldId());
			violationMessage = String.format("Violation: [%s]: Downside potential [%s] cannot be compared against missing value for field [%s].", combination.getLabel(), downsidePotentialStr, marketDataField.getName());
		}
		else {
			// Compare potential downside values
			BigDecimal minimumDownsidePotential = MathUtils.multiply(downsidePotentialLimitValue, getDownsidePotentialLowerBoundMultiplier());
			BigDecimal maximumDownsidePotential = MathUtils.multiply(downsidePotentialLimitValue, getDownsidePotentialUpperBoundMultiplier());
			boolean minimumSatisfied = minimumDownsidePotential == null || downsidePotential == null || MathUtils.isGreaterThanOrEqual(downsidePotential, minimumDownsidePotential);
			boolean maximumSatisfied = maximumDownsidePotential == null || (downsidePotential != null && MathUtils.isLessThanOrEqual(downsidePotential, maximumDownsidePotential));
			violationMessage = !(minimumSatisfied && maximumSatisfied)
					? String.format("Violation: [%s]: Downside potential [%s] is outside of allowed range [%s] to [%s].", combination.getLabel(), downsidePotentialStr, StringUtils.coalesce(false, CoreMathUtils.formatNumberMoney(minimumDownsidePotential), "0"), StringUtils.coalesce(false, CoreMathUtils.formatNumberMoney(maximumDownsidePotential), "Infinity"))
					: null;
		}

		// Report comparison results
		return violationMessage != null
				? ComplianceRuleRunSubDetail.create(violationMessage, downsidePotential, null, CollectionUtils.createList(getComplianceRuleReportHandler().createSubDetailForOptionCombination(combination, downsidePotential)))
				: null;
	}


	/**
	 * Gets the base constraint value for downside potential limits, or <code>null</code> if no constraint value can be found.
	 */
	private BigDecimal getCombinationDownsidePotentialLimitValue(TradeOptionCombination combination) {
		// Get relevant trade IDs
		Integer[] tradeIds = getCombinationPositionList(combination).stream()
				.map(getTradeService()::getOpeningTradeForAccountingPosition)
				.map(BaseSimpleEntity::getId)
				.distinct()
				.toArray(Integer[]::new);

		// Get saved trade market data values
		TradeMarketDataValueSearchForm searchForm = new TradeMarketDataValueSearchForm();
		searchForm.setTradeIds(tradeIds);
		searchForm.setMarketDataFieldId(getDownsidePotentialMarketDataFieldId());
		List<TradeMarketDataValue> marketDataValueList = getTradeMarketDataFieldService().getTradeMarketDataValueListUnfiltered(searchForm);

		// Return latest value
		return marketDataValueList.stream()
				.map(ManyToManyEntity::getReferenceTwo)
				.max(Comparator.comparing(MarketDataValue::getMeasureDateWithTime))
				.map(MarketDataValueGeneric::getMeasureValue)
				.orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a {@link ComplianceRuleRunSubDetail} for the given list of {@link TradeOptionCombination combinations} which did not match the configured list of allowed
	 * {@link TradeOptionCombinationTypes}.
	 */
	private List<ComplianceRuleRunSubDetail> getUnmatchedCombinationSubDetailList(List<TradeOptionCombination> unmatchedCombinationList) {
		String combinationTypesLabel = StringUtils.join(getCombinationTypes(), BeanUtils::getLabel, ", ");
		List<ComplianceRuleRunSubDetail> unmatchedPositionSubDetailList = new ArrayList<>();
		for (TradeOptionCombination combination : unmatchedCombinationList) {
			List<ComplianceRuleRunSubDetail> combinationLegSubDetailList = unmatchedCombinationList.stream()
					.map(this::getCombinationPositionList)
					.flatMap(Collection::stream)
					.map(getComplianceRuleReportHandler()::createSubDetailForPosition)
					.collect(Collectors.toList());
			unmatchedPositionSubDetailList.add(ComplianceRuleRunSubDetail.create(String.format("Violation: [%s]: Combination does not match type(s) [%s].", combination.getLabel(), combinationTypesLabel), combinationLegSubDetailList));
		}
		return unmatchedPositionSubDetailList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingPosition> getCombinationPositionList(TradeOptionCombination combination) {
		return combination.getOptionLegList().stream()
				.flatMap(leg -> leg.getPositionList().stream())
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeOptionCombinationHandler getTradeOptionCombinationHandler() {
		return this.tradeOptionCombinationHandler;
	}


	public void setTradeOptionCombinationHandler(TradeOptionCombinationHandler tradeOptionCombinationHandler) {
		this.tradeOptionCombinationHandler = tradeOptionCombinationHandler;
	}


	public TradeOptionCombinationTypes[] getCombinationTypes() {
		return this.combinationTypes;
	}


	public void setCombinationTypes(TradeOptionCombinationTypes[] combinationTypes) {
		this.combinationTypes = combinationTypes;
	}


	public Short getDownsidePotentialMarketDataFieldId() {
		return this.downsidePotentialMarketDataFieldId;
	}


	public void setDownsidePotentialMarketDataFieldId(Short downsidePotentialMarketDataFieldId) {
		this.downsidePotentialMarketDataFieldId = downsidePotentialMarketDataFieldId;
	}


	public BigDecimal getDownsidePotentialLowerBoundMultiplier() {
		return this.downsidePotentialLowerBoundMultiplier;
	}


	public void setDownsidePotentialLowerBoundMultiplier(BigDecimal downsidePotentialLowerBoundMultiplier) {
		this.downsidePotentialLowerBoundMultiplier = downsidePotentialLowerBoundMultiplier;
	}


	public BigDecimal getDownsidePotentialUpperBoundMultiplier() {
		return this.downsidePotentialUpperBoundMultiplier;
	}


	public void setDownsidePotentialUpperBoundMultiplier(BigDecimal downsidePotentialUpperBoundMultiplier) {
		this.downsidePotentialUpperBoundMultiplier = downsidePotentialUpperBoundMultiplier;
	}


	public boolean isUseDownsidePerUnitOfUnderlying() {
		return this.useDownsidePerUnitOfUnderlying;
	}


	public void setUseDownsidePerUnitOfUnderlying(boolean useDownsidePerUnitOfUnderlying) {
		this.useDownsidePerUnitOfUnderlying = useDownsidePerUnitOfUnderlying;
	}


	public boolean isIncludeMatches() {
		return this.includeMatches;
	}


	public void setIncludeMatches(boolean includeMatches) {
		this.includeMatches = includeMatches;
	}
}
