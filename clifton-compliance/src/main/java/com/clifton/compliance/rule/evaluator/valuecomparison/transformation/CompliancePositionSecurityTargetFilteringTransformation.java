package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.options.combination.TradeOptionCombination;
import com.clifton.trade.options.combination.TradeOptionCombinationHandler;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.securitytarget.group.util.TradeGroupTradeCycleUtilHandler;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionTransformation} which filters {@link CompliancePositionValueHolder} objects based on their validity in accordance with account {@link
 * InvestmentAccountSecurityTarget} constraints.
 * <p>
 * This filtering transformation produces value holder objects which do not satisfy all of the following structural conditions:
 * <ul>
 * <li>All positions within the value holder are for securities which have an underlying security.
 * <li>All positions within the value holder have a security target.
 * <li>All positions within the value holder use the same security target.
 * <li>The security target used for the value holder is unique (it is not used by any other value holder).
 * </ul>
 * <p>
 * In addition, this filtering transformation evaluates security target constraints for value holder objects which do satisfy the above conditions. This filtering transformation
 * will also produce value holder objects which do not satisfy all of the following security target conditions:
 * <ul>
 * <li><i>(If the account uses {@link TradeGroupTradeCycleUtilHandler#isUsesValueAtRisk(BusinessServiceProcessingType) value at risk})</i>: The aggregate downside potential for all
 * {@link TradeOptionCombination option combinations} does not exceed the targeted notional multiplied by the configured value at risk.
 * <li><i>(Otherwise)</i>: The aggregate notional or quantity (in shares) does not exceed the targeted notional or quantity limit.
 * </ul>
 *
 * @author MikeH
 */
public class CompliancePositionSecurityTargetFilteringTransformation implements CompliancePositionTransformation {

	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;
	private TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler;
	private TradeOptionCombinationHandler tradeOptionCombinationHandler;
	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;

	/**
	 * If <code>true</code>, the results will include {@link CompliancePositionValueHolder position value holders} which match the constraints. Otherwise, the results will include
	 * all value holders which do not match the constraints.
	 */
	private boolean includeMatches;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Guard-clause: Ignore processing for empty position lists
		if (positionValueList.isEmpty()) {
			return positionValueList;
		}

		// Partition by valid groupings with existence of security targets
		InvestmentAccount clientInvestmentAccount = getClientInvestmentAccount(positionValueList);
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByValidStructure = partitionValueHolderListByValidStructure(positionValueList, clientInvestmentAccount, processDate, runSubDetailList);
		List<CompliancePositionValueHolder<AccountingPosition>> validStructureValueHolderList = valueHolderListByValidStructure.get(true);
		List<CompliancePositionValueHolder<AccountingPosition>> invalidStructureValueHolderList = valueHolderListByValidStructure.get(false);

		// Find groups which violate security targets
		List<CompliancePositionValueHolder<AccountingPosition>> violatingValueHolderList = getSecurityTargetViolatingValueHolderList(validStructureValueHolderList, clientInvestmentAccount, processDate, runSubDetailList);
		return isIncludeMatches()
				? CollectionUtils.getFiltered(validStructureValueHolderList, valueHolder -> !violatingValueHolderList.contains(valueHolder))
				: CollectionUtils.combineCollections(invalidStructureValueHolderList, violatingValueHolderList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Value Holder Structure Validation Methods       ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Partitions the value holder list into those which are structurally valid and those which are not structurally valid.
	 * <p>
	 * Structural validation is performed by comparing the underlying securities and the security targets. The following conditions must hold true for a value holder to be
	 * considered structurally valid:
	 * <ul>
	 * <li>All securities held in the position group must have an underlying security
	 * <li>All position groups must correspond to exactly one security target
	 * <li>Security targets may not be shared between position groups
	 * </ul>
	 * <p>
	 * In the resulting map, the <code>true</code> key will be mapped to all value holders which are considered <b>valid</b> and the <code>false</code> key will be mapped to all
	 * value holders which are considered <b>invalid</b>.
	 */
	private Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> partitionValueHolderListByValidStructure(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, InvestmentAccount clientInvestmentAccount, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Evaluate validity of value holder lists
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByHasUnderlying = partitionValueHolderListByHasUnderlying(valueHolderList, runSubDetailList);
		Map<List<InvestmentAccountSecurityTarget>, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListBySecurityTargetList = groupValueHolderListBySecurityTargetList(valueHolderListByHasUnderlying.get(true), clientInvestmentAccount, processDate);
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByValidSecurityTarget = partitionValueHolderListByValidSecurityTarget(valueHolderListBySecurityTargetList, runSubDetailList);

		// Partition into valid and invalid lists
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByValid = new HashMap<>();
		valueHolderListByValid.put(true, valueHolderListByValidSecurityTarget.get(true));
		valueHolderListByValid.put(false, CollectionUtils.combineCollections(valueHolderListByHasUnderlying.get(false), valueHolderListByValidSecurityTarget.get(false)));
		return valueHolderListByValid;
	}


	/**
	 * Partitions the list of value holders by satisfaction of underlying security constraints. Value holders satisfy underlying security constraints if all securities within their
	 * position list have an underlying security.
	 */
	private Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> partitionValueHolderListByHasUnderlying(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByHasUnderlying = CollectionUtils.getPartitioned(valueHolderList,
				valueHolder -> CollectionUtils.anyMatch(valueHolder.getPositionList(), position -> position.getInvestmentSecurity().getUnderlyingSecurity() != null));
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderListWithNoUnderlying = valueHolderListByHasUnderlying.get(false);
		if (!valueHolderListWithNoUnderlying.isEmpty()) {
			// Add positions to run details
			List<AccountingPosition> invalidPositionList = valueHolderListWithNoUnderlying.stream()
					.flatMap(valueHolder -> valueHolder.getPositionList().stream())
					.filter(position -> position.getInvestmentSecurity().getUnderlyingSecurity() == null)
					.collect(Collectors.toList());
			runSubDetailList.add(ComplianceRuleRunSubDetail.create(String.format("Positions were found with no underlying security (Size: %d).", invalidPositionList.size()), CollectionUtils.getConverted(invalidPositionList, getComplianceRuleReportHandler()::createSubDetailForPosition)));
		}
		return valueHolderListByHasUnderlying;
	}


	/**
	 * Groups the list of value holders by the list of security targets which apply to each value holder.
	 */
	private Map<List<InvestmentAccountSecurityTarget>, List<CompliancePositionValueHolder<AccountingPosition>>> groupValueHolderListBySecurityTargetList(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, InvestmentAccount clientInvestmentAccount, Date processDate) {
		// Find all security targets for distinct underlying securities
		return BeanUtils.getBeansMap(valueHolderList, valueHolder -> valueHolder.getPositionList().stream()
				.map(position -> position.getInvestmentSecurity().getUnderlyingSecurity())
				.distinct()
				.map(underlyingSecurity -> getInvestmentAccountSecurityTargetForAccountAndSecurity(clientInvestmentAccount, underlyingSecurity, processDate))
				.collect(Collectors.toList()));
	}


	/**
	 * Partitions value holders into valid and invalid groups based on their security target mappings. Security target mappings are considered valid when they are one-to-one. That
	 * is, security target mappings are valid when each group has one and only one security target and each security target maps to one and only one group.
	 * <p>
	 * Validation failures will be attached to the provided <code>runSubDetailList</code> with messages corresponding to the violation type.
	 */
	private Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> partitionValueHolderListByValidSecurityTarget(Map<List<InvestmentAccountSecurityTarget>, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListBySecurityTargetList, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Group value holders by violation type
		List<CompliancePositionValueHolder<AccountingPosition>> validValueHolderList = new ArrayList<>();
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderListWithMultiSecurityTarget = new ArrayList<>();
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderListWithMissingSecurityTarget = new ArrayList<>();
		MultiValueMap<InvestmentAccountSecurityTarget, CompliancePositionValueHolder<AccountingPosition>> valueHolderListBySharedSecurityTarget = new MultiValueHashMap<>(false);
		for (Map.Entry<List<InvestmentAccountSecurityTarget>, List<CompliancePositionValueHolder<AccountingPosition>>> securityTargetToValueHolderList : valueHolderListBySecurityTargetList.entrySet()) {
			List<InvestmentAccountSecurityTarget> currentTargetList = securityTargetToValueHolderList.getKey();
			List<CompliancePositionValueHolder<AccountingPosition>> currentValueHolderList = securityTargetToValueHolderList.getValue();
			if (currentTargetList.size() > 1) {
				valueHolderListWithMultiSecurityTarget.addAll(currentValueHolderList);
			}
			else if (currentTargetList.contains(null)) {
				valueHolderListWithMissingSecurityTarget.addAll(currentValueHolderList);
			}
			else if (currentValueHolderList.size() > 1) {
				for (InvestmentAccountSecurityTarget securityTarget : currentTargetList) {
					valueHolderListBySharedSecurityTarget.putAll(securityTarget, currentValueHolderList);
				}
			}
			else {
				validValueHolderList.addAll(currentValueHolderList);
			}
		}

		// Report run details for violating value holders
		if (!valueHolderListWithMultiSecurityTarget.isEmpty()) {
			runSubDetailList.add(ComplianceRuleRunSubDetail.create("Position groups were found with multiple security targets. Positions must be grouped such that each group only uses a single security target.", getComplianceRuleReportHandler().createSubDetailForValueHolderList(valueHolderListWithMultiSecurityTarget)));
		}
		if (!valueHolderListWithMissingSecurityTarget.isEmpty()) {
			runSubDetailList.add(ComplianceRuleRunSubDetail.create("Position groups were found with missing security targets. Security targets must be created for all positions.", getComplianceRuleReportHandler().createSubDetailForValueHolderList(valueHolderListWithMissingSecurityTarget)));
		}
		if (!valueHolderListBySharedSecurityTarget.isEmpty()) {
			List<ComplianceRuleRunSubDetail> sharedSecurityTargetSubDetailList = CollectionUtils.getConverted(valueHolderListBySharedSecurityTarget.entrySet(), securityTargetToValueHolderList -> {
				InvestmentAccountSecurityTarget securityTarget = securityTargetToValueHolderList.getKey();
				Collection<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = securityTargetToValueHolderList.getValue();
				return ComplianceRuleRunSubDetail.create(String.format("Security Target: [%s]", securityTarget.getLabel()), getComplianceRuleReportHandler().createSubDetailForValueHolderList(valueHolderList));
			});
			runSubDetailList.add(ComplianceRuleRunSubDetail.create("Position groups were found with shared security targets. Positions must be grouped such that the same security target is not used across multiple groups.", sharedSecurityTargetSubDetailList));
		}

		// Partition into valid and invalid lists
		Map<Boolean, List<CompliancePositionValueHolder<AccountingPosition>>> valueHolderListByValid = new HashMap<>();
		valueHolderListByValid.put(true, validValueHolderList);
		valueHolderListByValid.put(false, CollectionUtils.combineCollections(valueHolderListWithMissingSecurityTarget, valueHolderListWithMultiSecurityTarget, valueHolderListBySharedSecurityTarget.values()));
		return valueHolderListByValid;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Security Target Validation Methods              ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of value holders which violate their corresponding security targets for the given date. Entries are appended to the run detail list as necessary.
	 * <p>
	 * Value holders are regarded as violating their corresponding security targets if the value holder does not match the configured {@link TradeOptionCombinationTypes option
	 * combination type} for the account.
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> getSecurityTargetViolatingValueHolderList(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, InvestmentAccount clientInvestmentAccount, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		if (valueHolderList.isEmpty()) {
			return Collections.emptyList();
		}

		TradeOptionCombinationTypes combinationType = getOptionCombinationTypeForAccount(clientInvestmentAccount);
		return CollectionUtils.getFiltered(valueHolderList, valueHolder -> {
			Collection<TradeOptionCombination> optionCombinationList = getOptionCombinationList(valueHolder, combinationType);
			Map<Boolean, List<TradeOptionCombination>> optionCombinationListByIsValid = partitionOptionCombinationListByIsValid(optionCombinationList, combinationType, valueHolder, runSubDetailList);
			List<TradeOptionCombination> invalidOptionCombinationList = optionCombinationListByIsValid.get(false);
			List<TradeOptionCombination> validOptionCombinationList = optionCombinationListByIsValid.get(true);
			return !(invalidOptionCombinationList.isEmpty() && isSecurityTargetValid(validOptionCombinationList, clientInvestmentAccount, processDate, runSubDetailList));
		});
	}


	/**
	 * Validates the value holder against its security target. This validates only that the constraint specified in the security target, such as the notional limit, is not
	 * exceeded.
	 */
	private boolean isSecurityTargetValid(Collection<TradeOptionCombination> optionCombinationList, InvestmentAccount clientInvestmentAccount, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Retrieve security target info
		InvestmentSecurity underlyingSecurity = getUnderlyingSecurityForCombinationList(optionCombinationList);
		InvestmentAccountSecurityTarget investmentAccountSecurityTarget = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(clientInvestmentAccount.getId(), underlyingSecurity.getId(), processDate);
		TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDate(investmentAccountSecurityTarget, processDate);

		// Evaluate security target
		return getTradeGroupTradeCycleUtilHandler().isUsesValueAtRisk(clientInvestmentAccount.getServiceProcessingType())
				? isValueAtRiskSatisfied(tradeSecurityTarget, optionCombinationList, runSubDetailList)
				: isTotalQuantitySatisfied(tradeSecurityTarget, optionCombinationList, runSubDetailList);
	}


	private boolean isValueAtRiskSatisfied(TradeSecurityTarget tradeSecurityTarget, Collection<TradeOptionCombination> optionCombinationList, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Get security target downside potential limit
		InvestmentAccount clientInvestmentAccount = tradeSecurityTarget.getSecurityTarget().getClientInvestmentAccount();
		BigDecimal clientValueAtRiskPercent = (BigDecimal) getInvestmentAccountUtilHandler().getClientInvestmentAccountPortfolioSetupCustomColumnValue(clientInvestmentAccount, InvestmentAccountUtilHandler.VALUE_AT_RISK_PERCENT_COLUMN_NAME);
		ValidationUtils.assertNotNull(clientValueAtRiskPercent, String.format("Unable to obtain Value At Risk property for client account [%s].", clientInvestmentAccount.getLabel()));
		BigDecimal clientValueAtRiskMultiplier = MathUtils.divide(clientValueAtRiskPercent, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		BigDecimal maxClientDownsidePotential = MathUtils.multiply(clientValueAtRiskMultiplier, tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs());

		// Get actual downside potential
		Map<TradeOptionCombination, BigDecimal> combinationToDownsidePotentialMap = CollectionUtils.getMapped(optionCombinationList, optionCombination -> getTradeOptionCombinationHandler().getOptionCombinationDownsidePotential(optionCombination));
		BigDecimal actualDownsidePotential = combinationToDownsidePotentialMap.values().contains(null)
				? null // Null indicates infinite downside potential
				: CoreMathUtils.sum(combinationToDownsidePotentialMap.values());

		// Compare downside potential against security target
		boolean downsideSatisfied = actualDownsidePotential != null && MathUtils.isGreaterThanOrEqual(maxClientDownsidePotential, actualDownsidePotential);
		if (!downsideSatisfied) {
			// Report violation
			String totalDownsidePotentialStr = StringUtils.coalesce(false, CoreMathUtils.formatNumberMoney(actualDownsidePotential), "Infinity");
			List<ComplianceRuleRunSubDetail> combinationSubDetailList = MapUtils.getConverted(combinationToDownsidePotentialMap, getComplianceRuleReportHandler()::createSubDetailForOptionCombination);
			runSubDetailList.add(ComplianceRuleRunSubDetail.create(String.format("Violation: The security target for [%s] is exceeded. Notional [%s] is greater than [%s].",
					tradeSecurityTarget.getSecurityTarget().getTargetUnderlyingSecurity().getSymbol(),
					totalDownsidePotentialStr,
					CoreMathUtils.formatNumberMoney(maxClientDownsidePotential)
			), actualDownsidePotential, null, combinationSubDetailList));
		}
		return downsideSatisfied;
	}


	private boolean isTotalQuantitySatisfied(TradeSecurityTarget tradeSecurityTarget, Collection<TradeOptionCombination> optionCombinationList, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Compare total quantity against security target
		Map<TradeOptionCombination, BigDecimal> combinationToQuantityMap = CollectionUtils.getMapped(optionCombinationList, TradeOptionCombination::getUnderlyingRemainingQuantity);
		BigDecimal totalUnderlyingQuantity = CoreMathUtils.sum(combinationToQuantityMap.values());
		boolean quantitySatisfied = MathUtils.isGreaterThanOrEqual(tradeSecurityTarget.getTargetUnderlyingQuantityAdjustedAbs(), totalUnderlyingQuantity);
		if (!quantitySatisfied) {
			// Report violation
			List<ComplianceRuleRunSubDetail> combinationSubDetailList = MapUtils.getConverted(combinationToQuantityMap, getComplianceRuleReportHandler()::createSubDetailForOptionCombination);
			runSubDetailList.add(ComplianceRuleRunSubDetail.create(String.format("Violation: The security target for [%s] is exceeded. Quantity [%s] is greater than [%s].",
					tradeSecurityTarget.getSecurityTarget().getTargetUnderlyingSecurity().getSymbol(),
					CoreMathUtils.formatNumberMoney(totalUnderlyingQuantity),
					CoreMathUtils.formatNumberMoney(tradeSecurityTarget.getTargetUnderlyingQuantityAdjustedAbs())
			), totalUnderlyingQuantity, null, combinationSubDetailList));
		}
		return quantitySatisfied;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Collection<TradeOptionCombination> getOptionCombinationList(CompliancePositionValueHolder<AccountingPosition> valueHolder, TradeOptionCombinationTypes combinationType) {
		return getTradeOptionCombinationHandler().getTradeOptionCombinationList(valueHolder.getPositionList(), combinationType);
	}


	private Map<Boolean, List<TradeOptionCombination>> partitionOptionCombinationListByIsValid(Collection<TradeOptionCombination> optionCombinationList, TradeOptionCombinationTypes combinationType, CompliancePositionValueHolder<AccountingPosition> valueHolder, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Locate invalid combinations
		Map<Boolean, List<TradeOptionCombination>> optionCombinationListByIsValid = CollectionUtils.getPartitioned(optionCombinationList, optionCombination -> combinationType == optionCombination.getCombinationType());
		List<TradeOptionCombination> invalidOptionCombinationList = optionCombinationListByIsValid.get(false);

		// Process run details
		if (!invalidOptionCombinationList.isEmpty()) {
			// Option combinations are invalid; create run sub-detail and mark invalid
			ComplianceRuleRunSubDetail invalidCombinationSubDetail = ComplianceRuleRunSubDetail.create(String.format("Violation: Positions cannot be grouped into an option combination of type [%s].", BeanUtils.getLabel(combinationType)));

			// Create run sub-detail describing ungroupable positions
			invalidCombinationSubDetail.addSubDetail(getComplianceRuleReportHandler().createSubDetailForValueHolder(valueHolder));
			List<ComplianceRuleRunSubDetail> ungroupedPositionSubDetailList = invalidOptionCombinationList.stream()
					.flatMap(optionCombination -> getCombinationPositionList(optionCombination).stream())
					.map(position -> getComplianceRuleReportHandler().createSubDetailForPosition(position))
					.collect(Collectors.toList());
			invalidCombinationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Ungrouped Positions (Size: %d)", ungroupedPositionSubDetailList.size()), ungroupedPositionSubDetailList));
			runSubDetailList.add(invalidCombinationSubDetail);
		}
		return optionCombinationListByIsValid;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getClientInvestmentAccount(Collection<CompliancePositionValueHolder<AccountingPosition>> valueHolderList) {
		List<InvestmentAccount> clientInvestmentAccountList = valueHolderList.stream()
				.flatMap(valueHolder -> valueHolder.getPositionList().stream())
				.map(AccountingPosition::getClientInvestmentAccount)
				.distinct()
				.collect(Collectors.toList());
		return CollectionUtils.getOnlyElementStrict(clientInvestmentAccountList);
	}


	private InvestmentSecurity getUnderlyingSecurityForCombinationList(Collection<TradeOptionCombination> combinationList) {
		List<InvestmentSecurity> underlyingSecurityList = combinationList.stream()
				.map(TradeOptionCombination::getUnderlyingSecurity)
				.distinct()
				.collect(Collectors.toList());
		return CollectionUtils.getOnlyElementStrict(underlyingSecurityList);
	}


	private List<AccountingPosition> getCombinationPositionList(TradeOptionCombination combination) {
		return combination.getOptionLegList().stream()
				.flatMap(leg -> leg.getPositionList().stream())
				.collect(Collectors.toList());
	}


	private TradeOptionCombinationTypes getOptionCombinationTypeForAccount(InvestmentAccount clientInvestmentAccount) {
		return getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(clientInvestmentAccount.getServiceProcessingType());
	}


	private InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForAccountAndSecurity(InvestmentAccount clientInvestmentAccount, InvestmentSecurity underlyingSecurity, Date date) {
		return underlyingSecurity != null
				? getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(clientInvestmentAccount.getId(), underlyingSecurity.getId(), date)
				: null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}


	public TradeGroupTradeCycleUtilHandler getTradeGroupTradeCycleUtilHandler() {
		return this.tradeGroupTradeCycleUtilHandler;
	}


	public void setTradeGroupTradeCycleUtilHandler(TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler) {
		this.tradeGroupTradeCycleUtilHandler = tradeGroupTradeCycleUtilHandler;
	}


	public TradeOptionCombinationHandler getTradeOptionCombinationHandler() {
		return this.tradeOptionCombinationHandler;
	}


	public void setTradeOptionCombinationHandler(TradeOptionCombinationHandler tradeOptionCombinationHandler) {
		this.tradeOptionCombinationHandler = tradeOptionCombinationHandler;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}


	public boolean isIncludeMatches() {
		return this.includeMatches;
	}


	public void setIncludeMatches(boolean includeMatches) {
		this.includeMatches = includeMatches;
	}
}
