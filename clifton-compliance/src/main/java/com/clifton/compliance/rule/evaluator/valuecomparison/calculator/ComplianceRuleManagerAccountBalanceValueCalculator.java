package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.math.MathTransformationTypes;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceTypes;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The {@link ComplianceRuleManagerAccountBalanceValueCalculator} is an implementor of {@link ComplianceRuleValueCalculator}. This type allows for calculations based on manager
 * account balances.
 * <p>
 * This class allows for filtering based on {@link InvestmentManagerAccount#managerCompany manager company} and {@link InvestmentManagerAccount individual manager account}. The
 * calculated result shall be the aggregated total of the balance value as determined by the selected {@link InvestmentManagerAccountBalanceTypes balance type}.
 *
 * @author MikeH
 */
public class ComplianceRuleManagerAccountBalanceValueCalculator extends BaseComplianceRuleDateAwareValueCalculator implements ValidationAware {

	/**
	 * The name of the {@link SystemHierarchyCategory} tag for value-type tags.
	 */
	private static final String HIERARCHY_CATEGORY_VALUE_TYPE_TAGS = "Manager Account Balance Value Type Tags";

	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	/**
	 * The account balance type to use. This balance type determines the which value which will be extracted from each manager account balance.
	 */
	private InvestmentManagerAccountBalanceTypes accountBalanceType;

	// Manager account filters
	private List<Integer> managerCompanyIdList;
	private List<Short> managerTagIdList;
	private List<Integer> managerAccountIdList;
	private boolean includeInactiveManagers;

	// Transformations, in order of application
	private MathTransformationTypes transformationType;
	private BigDecimal multiplier;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected BigDecimal calculateImplWithCalculateDate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		List<InvestmentManagerAccount> managerAccountList = getInvestmentManagerAccountList(ruleEvaluationConfig.retrieveAccountIdList());
		List<InvestmentManagerAccountBalance> accountBalanceList = getInvestmentManagerAccountBalanceList(managerAccountList, ruleEvaluationConfig.getCalculationDate(), ruleEvaluationConfig.isRealTime());
		BigDecimal balanceTotal = calculateBalanceTotal(accountBalanceList, ruleEvaluationConfig.retrieveClientAccount(), ruleEvaluationConfig.getCalculationDate(), runSubDetailList);
		return getTransformedValue(balanceTotal, runSubDetailList);
	}


	@Override
	public void validate() throws ValidationException {
		boolean hasCompanyOrTagFilter = ObjectUtils.coalesce(getManagerCompanyIdList(), getManagerTagIdList()) != null;
		boolean hasAccountFilter = getManagerAccountIdList() != null;
		ValidationUtils.assertFalse(hasCompanyOrTagFilter && hasAccountFilter, "The \"Manager Companies\"/\"Manager Tags\" and \"Manager Accounts\" filters may not be specified simultaneously.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of applicable manager accounts for the given client accounts. Configured filters will be applied when retrieving the list of manager accounts.
	 * <p>
	 * <i>Inactive</i> managers are intentionally included here to prevent issues with historic evaluations. Since we do not track the deactivation date for managers, we opt to
	 * evaluate values for all applicable managers regardless of <i>active</i> status.
	 *
	 * @param clientAccountIdList the list of client accounts for which to retrieve applicable manager accounts
	 * @return the list of applicable manager accounts
	 */
	private List<InvestmentManagerAccount> getInvestmentManagerAccountList(List<Integer> clientAccountIdList) {
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setAssignmentAccountIds(CollectionUtils.toArray(clientAccountIdList, Integer.class));
		searchForm.setManagerAccountIds(CollectionUtils.toArrayOrNull(getManagerAccountIdList(), Integer.class));
		searchForm.setManagerCompanyIds(CollectionUtils.toArrayOrNull(getManagerCompanyIdList(), Integer.class));
		if (!isIncludeInactiveManagers()) {
			// Exclude inactive managers
			searchForm.setAssignmentInactive(false);
		}
		if (!CollectionUtils.isEmpty(getManagerTagIdList())) {
			// Query by tag
			searchForm.setCategoryName(HIERARCHY_CATEGORY_VALUE_TYPE_TAGS);
			searchForm.setCategoryHierarchyIds(CollectionUtils.toArrayOrNull(getManagerTagIdList(), Short.class));
		}
		return getInvestmentManagerAccountService().getInvestmentManagerAccountList(searchForm);
	}


	/**
	 * Retrieves the list of manager account balances for the given list of manager accounts on the given date.
	 * <p>
	 * This method enforces balance existence constraints. Each given manager, if active, must have a discoverable balance. If no balance is found for an active manager then an
	 * exception will be thrown.
	 *
	 * @param managerAccountList the list of manager accounts for which to retrieve balances
	 * @param processDate        the date for which the balance values should be retrieved
	 * @param useFlexibleLookup  if <tt>true</tt>, seek the latest balances as of the provided date; otherwise, only seek balances for the exact date given
	 * @return the list of retrieved manager account balances
	 */
	private List<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(List<InvestmentManagerAccount> managerAccountList, Date processDate, boolean useFlexibleLookup) {
		return CollectionUtils.getStream(managerAccountList)
				.map(managerAccount -> {
					// Retrieve the manager account balance
					InvestmentManagerAccountBalance balance = useFlexibleLookup
							? getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDateFlexible(managerAccount.getId(), processDate, false)
							: getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), processDate, false);
					// Validate the account balance exists unless the account is inactive
					AssertUtils.assertTrue(balance != null || managerAccount.isInactive(),
							"Unable to find a manager balance for account [%s] on date [%tD]%s.", managerAccount.getLabel(), processDate, useFlexibleLookup ? " or any prior dates" : "");
					return balance;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}


	/**
	 * Calculates the total value from the given list of {@link InvestmentManagerAccountBalance} entities in the currency of the given client.
	 * <p>
	 * The {@link #accountBalanceType} will be used to determine the value which shall be retrieved.
	 *
	 * @param managerAccountBalanceList the list of manager account balances
	 * @param clientAccount             the client account determining the base currency to which balance values should be converted
	 * @param processDate               the process date for which aggregated values are desired
	 * @param runSubDetailList          the list of rule run details to which evaluation details should be added
	 * @return the total aggregated balance value
	 */
	private BigDecimal calculateBalanceTotal(List<InvestmentManagerAccountBalance> managerAccountBalanceList, InvestmentAccount clientAccount, Date processDate, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Get balance total
		Map<InvestmentManagerAccountBalance, BigDecimal> balanceToValueMap = CollectionUtils.getMapped(managerAccountBalanceList, balance -> calculateManagerAccountBalanceForClient(balance, getAccountBalanceType(), clientAccount, processDate));
		BigDecimal balanceTotal = CoreMathUtils.sum(balanceToValueMap.values());

		// Add details
		ComplianceRuleRunSubDetail balanceListSubDetail = ComplianceRuleRunSubDetail.create("Manager Account Balance Values", balanceTotal);
		List<ComplianceRuleRunSubDetail> balanceValueSubDetailList = CollectionUtils.getConverted(balanceToValueMap.entrySet(),
				entry -> ComplianceRuleRunSubDetail.create(String.format("Manager Account: [%s]", entry.getKey().getLabel()), entry.getValue()));
		balanceValueSubDetailList.sort((detail1, detail2) -> StringUtils.compare(detail1.getLabel(), detail2.getLabel()));
		balanceListSubDetail.setChildren(balanceValueSubDetailList);
		runSubDetailList.add(balanceListSubDetail);

		return balanceTotal;
	}


	/**
	 * Calculates the manager account balance of the specified balance type.
	 * <p>
	 * If the manager currency is not the same as the client base currency, then the balance value will be converted to the client base currency using the exchange rate for the
	 * specified date.
	 *
	 * @param managerAccountBalance  the manager account balance
	 * @param accountBalanceType     the balance type which should be retrieved
	 * @param clientAccount          the client account whose base currency should be used
	 * @param currencyConversionDate the date for which exchange rates should be used for currency conversion
	 * @return the manager account balance value in the base currency of the client
	 */
	private BigDecimal calculateManagerAccountBalanceForClient(InvestmentManagerAccountBalance managerAccountBalance, InvestmentManagerAccountBalanceTypes accountBalanceType, InvestmentAccount clientAccount, Date currencyConversionDate) {
		// Get manager balance
		BigDecimal managerBalance = accountBalanceType.getValue(managerAccountBalance);

		// Get exchange rate
		String managerCurrency = managerAccountBalance.getManagerAccount().getBaseCurrency().getSymbol();
		String clientCurrency = clientAccount.getBaseCurrency().getSymbol();
		BigDecimal fxRate = BigDecimal.ONE;
		if (!CompareUtils.isEqual(managerCurrency, clientCurrency)) {
			fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(clientAccount.toClientAccount(), managerCurrency, clientCurrency, currencyConversionDate).flexibleLookup());
		}

		// Apply exchange rate
		return MathUtils.multiply(managerBalance, fxRate);
	}


	/**
	 * Transforms the given calculated value according to the configured transformation flags.
	 *
	 * @param calculatedValue  the calculated value
	 * @param runSubDetailList the list of rule run details to which evaluation details should be added
	 * @return the transformed value
	 */
	private BigDecimal getTransformedValue(BigDecimal calculatedValue, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Transformation order is relevant and should be documented for client
		BigDecimal transformedValue = getTransformationType().transform(calculatedValue);
		BigDecimal multipliedValue = getMultiplier().multiply(transformedValue);

		// Add details
		ComplianceRuleRunSubDetail transformationSubDetail = ComplianceRuleRunSubDetail.create("Final transformed value", multipliedValue);
		transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Transformation: [%s]", StringUtils.capitalize(getTransformationType().toString())), transformedValue));
		transformationSubDetail.addSubDetail(ComplianceRuleRunSubDetail.create(String.format("Multiplier: [%s]", getMultiplier()), multipliedValue));
		runSubDetailList.add(transformationSubDetail);

		return multipliedValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public InvestmentManagerAccountBalanceTypes getAccountBalanceType() {
		return this.accountBalanceType;
	}


	public void setAccountBalanceType(InvestmentManagerAccountBalanceTypes accountBalanceType) {
		this.accountBalanceType = accountBalanceType;
	}


	public List<Integer> getManagerCompanyIdList() {
		return this.managerCompanyIdList;
	}


	public void setManagerCompanyIdList(List<Integer> managerCompanyIdList) {
		this.managerCompanyIdList = managerCompanyIdList;
	}


	public List<Short> getManagerTagIdList() {
		return this.managerTagIdList;
	}


	public void setManagerTagIdList(List<Short> managerTagIdList) {
		this.managerTagIdList = managerTagIdList;
	}


	public List<Integer> getManagerAccountIdList() {
		return this.managerAccountIdList;
	}


	public void setManagerAccountIdList(List<Integer> managerAccountIdList) {
		this.managerAccountIdList = managerAccountIdList;
	}


	public boolean isIncludeInactiveManagers() {
		return this.includeInactiveManagers;
	}


	public void setIncludeInactiveManagers(boolean includeInactiveManagers) {
		this.includeInactiveManagers = includeInactiveManagers;
	}


	public MathTransformationTypes getTransformationType() {
		return this.transformationType;
	}


	public void setTransformationType(MathTransformationTypes transformationType) {
		this.transformationType = transformationType;
	}


	public BigDecimal getMultiplier() {
		return this.multiplier;
	}


	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}
}
