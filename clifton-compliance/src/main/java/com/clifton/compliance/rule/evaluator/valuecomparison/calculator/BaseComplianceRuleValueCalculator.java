package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A base implementation of {@link ComplianceRuleValueCalculator} which applies a basic set of functionality behind subclassed calculators.
 * <p>
 * Features include:
 * <ul>
 * <li>Storing the {@link ComplianceRuleEvaluationConfig rule evaluation config} in the context under the {@link #CONTEXT_RULE_CONFIG} key
 * <li>Caching rule-run results in the current run by calculator and client account ID
 * <li>Validating recursion and maximum calculator call-stack depth
 * </ul>
 * <p>
 * Calculators such as the {@link ComplianceRuleFixedValueCalculator} which do not benefit from these features should bypass this class and implement {@link
 * ComplianceRuleValueCalculator} directly.
 *
 * @author MikeH
 */
public abstract class BaseComplianceRuleValueCalculator implements ComplianceRuleValueCalculator {

	public static final String CONTEXT_RULE_CONFIG = BaseComplianceRuleValueCalculator.class.getSimpleName() + "-RuleConfig";
	public static final Integer CALCULATOR_MAX_RECURSION_DEPTH = 5;
	private static final String CONTEXT_CALCULATOR_STACK = BaseComplianceRuleValueCalculator.class.getSimpleName() + "-CalculatorStack";
	private static final String CONTEXT_CALCULATOR_CACHE = BaseComplianceRuleValueCalculator.class.getSimpleName() + "-CalculatorCache";
	private static final String RETRIEVED_FROM_CACHE_DETAIL_NOTE = "Note: Result was retrieved from cache";


	@Override
	public final BigDecimal calculate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Prepare context
		Context context = ruleEvaluationConfig.getContext();
		context.setBean(CONTEXT_RULE_CONFIG, ruleEvaluationConfig);

		// Calculate value with cache
		Map<ComplianceRuleValueCalculator, EvaluationResult> calculatorCacheForAccount =
				context.getOrSupplyBean(CONTEXT_CALCULATOR_CACHE + "-" + ruleEvaluationConfig.getClientAccountId(), HashMap::new);
		EvaluationResult result = calculatorCacheForAccount.compute(this, (calculator, lastResult) -> getCurrentEvaluationResult(ruleEvaluationConfig, lastResult));

		// Apply resulting detail list
		runSubDetailList.addAll(result.detailList);
		return result.value;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates the value for the given {@link ComplianceRuleEvaluationConfig}.
	 * <p>
	 * Calculators implementing this method will have their logic wrapped by the basic functionality included in the {@link BaseComplianceRuleValueCalculator}.
	 *
	 * @param ruleEvaluationConfig the configuration object
	 * @param runSubDetailList     the list of rule run details to which evaluation details should be added
	 * @return the calculated value
	 */
	protected abstract BigDecimal calculateImpl(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the current evaluation result based on the provided last evaluation result.
	 * <p>
	 * If the last evaluation result is not present, then this will calculate a new result. Otherwise, this will return the last evaluation result, adding a note to indicate that
	 * it is a previously existing value.
	 *
	 * @param ruleEvaluationConfig the configuration object
	 * @param lastResult           the previously-evaluated result, or {@code null} if no previous result exists
	 * @return the current evaluation result
	 */
	private EvaluationResult getCurrentEvaluationResult(ComplianceRuleEvaluationConfig ruleEvaluationConfig, EvaluationResult lastResult) {
		final EvaluationResult newResult = new EvaluationResult();
		if (lastResult != null) {
			// Use cached result, adding cache note if necessary
			newResult.value = lastResult.value;
			newResult.detailList = new ArrayList<>(lastResult.detailList);
			ComplianceRuleRunSubDetail firstDetailOfLastResult = CollectionUtils.getFirstElement(newResult.detailList);
			if (firstDetailOfLastResult != null && !RETRIEVED_FROM_CACHE_DETAIL_NOTE.equals(firstDetailOfLastResult.getLabel())) {
				newResult.detailList.add(0, ComplianceRuleRunSubDetail.create(RETRIEVED_FROM_CACHE_DETAIL_NOTE));
			}
		}
		else {
			// Validate any recursion currently in the stack
			Deque<ComplianceRuleValueCalculator> calculatorStack = ruleEvaluationConfig.getContext().getOrSupplyBean(CONTEXT_CALCULATOR_STACK, ArrayDeque::new);
			validateRuntimeRecursion(calculatorStack);

			// Execute with propagated value in stack
			calculatorStack.push(this);
			try {
				newResult.detailList = new ArrayList<>();
				newResult.value = calculateImpl(ruleEvaluationConfig, newResult.detailList);
			}
			finally {
				calculatorStack.pop();
			}
		}
		return newResult;
	}


	/**
	 * Validates the current state of the calculator stack during runtime.
	 * <p>
	 * This method validates the following characteristics of the calculator stack:
	 * <ul>
	 * <li>The executing calculator does not already exist in the calculator stack. This would indicate an infinite loop.
	 * <li>The size of the stack is less than {@link #CALCULATOR_MAX_RECURSION_DEPTH}. This limit is used for performance and as a fall-back for preventing infinite loops that
	 * would not otherwise be detected.
	 * </ul>
	 *
	 * @param calculatorStack the call stack of calculators that are currently being executed
	 */
	private void validateRuntimeRecursion(Deque<ComplianceRuleValueCalculator> calculatorStack) {
		/* Validate recursion in current stack */
		// Take advantage of singleton state and use reference comparison
		AssertUtils.assertFalse(calculatorStack.contains(this), "Recursion detected. The position value calculator was executed in a loop.");
		/*
		 * Validate max recursion for performance. This also provides a fall-back to covers the case in which singleton state comparison fails, such as if the singleton cache fails
		 * and new instances are created during each bean instance retrieval.
		 */
		AssertUtils.assertTrue(calculatorStack.size() < CALCULATOR_MAX_RECURSION_DEPTH, "The maximum position value calculator recursion depth (%d) has been exceeded.", CALCULATOR_MAX_RECURSION_DEPTH);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class EvaluationResult {

		private BigDecimal value;
		private List<ComplianceRuleRunSubDetail> detailList;
	}
}
