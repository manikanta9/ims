package com.clifton.compliance.rule.evaluator;


import com.clifton.business.company.BusinessCompanyService;
import com.clifton.compliance.ComplianceBaseHandler;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.filter.ComplianceRuleFilterHandler;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


public abstract class BaseComplianceRuleRatioEvaluator implements ComplianceRuleEquationEvaluator {

	private BusinessCompanyService businessCompanyService;
	private ComplianceBaseHandler complianceBaseHandler;
	private ComplianceRuleAccountHandler complianceRuleAccountHandler;
	private ComplianceRuleFilterHandler complianceRuleFilterHandler;
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private boolean useTotalAssets;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return evaluateBatch(ruleEvaluationConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComplianceRuleRun evaluateBatch(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {

		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setResultValue(BigDecimal.ZERO);
		run.setPercentOfAssets(BigDecimal.ZERO);

		run.setPass(true);
		List<ComplianceRuleEvaluationConfig> configList = null;

		try {
			configList = getBatchRuleEvaluationConfigList(ruleEvaluationConfig);
		}
		catch (Exception e) {
			LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error occurred while batch evaluating compliance rules. Rule Evaluation Config: " + BeanUtils.describe(ruleEvaluationConfig)));
			run.setError(e.getLocalizedMessage());
			run.setDescription(e.getLocalizedMessage());
			run.setPass(false);
		}

		BigDecimal denominator = BigDecimal.ZERO;

		for (ComplianceRuleEvaluationConfig config : CollectionUtils.getIterable(configList)) {
			config.setComplianceRuleLimitType(ruleEvaluationConfig.getComplianceRuleLimitType());
			if (!config.isCleanContext()) {
				config.setTradeRuleEvaluatorContext(ruleEvaluationConfig.getTradeRuleEvaluatorContext());
			}
			config.setRuleAssignment(ruleEvaluationConfig.getRuleAssignment());
			ComplianceRuleRunDetail detail = null;

			try {
				detail = evaluateNumerator(config);
				detail.setComplianceRuleRun(run);
				denominator = sanitizeDenominator(evaluateDenominator(config));
				getComplianceRuleReportHandler().propagateChildValues(detail, denominator);
				//TODO - should this actually be applied back to itself?
				BigDecimal result = detail.getResultValue().divide(denominator, 5, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
				detail.setDescription(generateFormattedViolation(result, denominator, config));
			}
			catch (Exception e) {
				LogUtils.error(getClass(), String.format("Error occurred during rule execution for account [%s]", ruleEvaluationConfig.retrieveClientAccount().getLabel()), e);
				if (detail == null) {
					detail = getComplianceRuleReportHandler().createBasicRuleRunDetail(e.getLocalizedMessage(), BigDecimal.ZERO, true, null);
				}
				detail.setDescription("Error [" + e.getLocalizedMessage() + "]");
				detail.setPass(false);
				run.setError(e.getLocalizedMessage());
				run.setDescription(e.getLocalizedMessage());
				run.setPass(false);
				run.addDetail(detail);
				return run;
			}
			run.addDetail(detail);
			ruleEvaluationConfig.setSharePosition(config.isSharePosition() || config.isDebtPosition());
		}

		if (!ruleEvaluationConfig.isSharePosition() && !CollectionUtils.isEmpty(run.getChildren())) {
			String description = (isUseTotalAssets()) ? "Total Assets: $" : "Net Assets: $";
			run.setDescription(description + CoreMathUtils.formatNumberMoney(denominator));
		}
		return run;
	}


	private BigDecimal sanitizeDenominator(BigDecimal denominator) {
		if (MathUtils.isEqual(denominator, 0)) {
			return BigDecimal.ONE;
		}
		return MathUtils.abs(denominator);
	}


	/**
	 * Generate a properly formatted detail level description.
	 *
	 * @param numerator
	 * @param denominator
	 * @param ruleEvaluationConfig
	 */
	private String generateFormattedViolation(BigDecimal numerator, BigDecimal denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		if (!ruleEvaluationConfig.isSharePosition() && !ruleEvaluationConfig.isDebtPosition()) {
			return generateViolation("$" + CoreMathUtils.formatNumberMoney(numerator), "$" + CoreMathUtils.formatNumberMoney(denominator), ruleEvaluationConfig);
		}

		return generateViolation(CoreMathUtils.formatNumberMoney(numerator), CoreMathUtils.formatNumberDecimal(denominator), ruleEvaluationConfig);
	}


	protected BigDecimal calculateDebtOutstanding(ComplianceRuleEvaluationConfig ruleEvaluationConfig) throws ValidationException {
		BigDecimal value;

		value = (BigDecimal) (ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext().getBean("calculateDebtOutstanding-" + ruleEvaluationConfig.getInvestmentSecurityId() + "-" + ruleEvaluationConfig.getProcessDate()));

		if (value == null) {

			MarketDataValueHolder marketValue = getMarketDataFieldService().getMarketDataValueForDateFlexible(ruleEvaluationConfig.getInvestmentSecurityId(), ruleEvaluationConfig.getProcessDate(),
					MarketDataField.FIELD_DEBT_OUTSTANDING);

			if (marketValue != null) {
				value = marketValue.getMeasureValue();
			}

			if (value == null) {
				value = new BigDecimal(-1); //can't cache nulls
			}

			ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext().setBean("calculateDebtOutstanding-" + ruleEvaluationConfig.getInvestmentSecurityId() + "-" + ruleEvaluationConfig.getProcessDate(), value);
		}

		if (MathUtils.isEqual(value, new BigDecimal(-1))) {
			value = null;
		}

		ValidationUtils.assertNotNull(value, "Missing Debt Outstanding Data Field for Security with id" + ruleEvaluationConfig.getInvestmentSecurityId());

		return value;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public ComplianceBaseHandler getComplianceBaseHandler() {
		return this.complianceBaseHandler;
	}


	public void setComplianceBaseHandler(ComplianceBaseHandler complianceBaseHandler) {
		this.complianceBaseHandler = complianceBaseHandler;
	}


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public void setComplianceRuleAccountHandler(ComplianceRuleAccountHandler complianceRuleAccountHandler) {
		this.complianceRuleAccountHandler = complianceRuleAccountHandler;
	}


	public ComplianceRuleFilterHandler getComplianceRuleFilterHandler() {
		return this.complianceRuleFilterHandler;
	}


	public void setComplianceRuleFilterHandler(ComplianceRuleFilterHandler complianceRuleFilterHandler) {
		this.complianceRuleFilterHandler = complianceRuleFilterHandler;
	}


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public boolean isUseTotalAssets() {
		return this.useTotalAssets;
	}


	public void setUseTotalAssets(boolean useTotalAssets) {
		this.useTotalAssets = useTotalAssets;
	}
}
