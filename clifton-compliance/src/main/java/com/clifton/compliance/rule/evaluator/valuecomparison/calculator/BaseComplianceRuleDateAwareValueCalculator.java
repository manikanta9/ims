package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseComplianceRuleDataAwareCalculator</code> class defines the properties that can be supplied for looking up the value
 * as of a different date and not the process date.
 *
 * @author manderson
 */
public abstract class BaseComplianceRuleDateAwareValueCalculator extends BaseComplianceRuleValueCalculator implements ValidationAware {

	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/*
	 * This field is not used and is present for display purposes only. This field is added to this class only for system bean validation purposes.
	 */
	private Boolean dateOptionsHeader;

	/**
	 * Optionally calculate the value as of another date (2 Business Days Back, Previous Month End, etc.)
	 */
	private DateGenerationOptions dateGenerationOptions;

	/**
	 * Used with the selected Date Generation Option for the number of days from process date value should be calculated for.
	 * Not all Date Generation Option selected require a value for this, validation is performed on save when applicable.
	 */
	private Integer daysFromProcessDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected final BigDecimal calculateImpl(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		Date calculationDate = getCalculationDate(ruleEvaluationConfig.getProcessDate());
		// Set the Date to Use
		ruleEvaluationConfig.setCalculationDate(calculationDate);
		// Perform the Calculation
		BigDecimal result = calculateImplWithCalculateDate(ruleEvaluationConfig, runSubDetailList);
		// Clear the date
		ruleEvaluationConfig.setCalculationDate(null);
		return result;
	}


	protected abstract BigDecimal calculateImplWithCalculateDate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList);


	@Override
	public void validate() {
		if (getDateGenerationOptions() != null && getDateGenerationOptions().isDayCountRequired()) {
			ValidationUtils.assertNotNull(getDaysFromProcessDate(), "A number of days from process date must be specified for Date Generation Option [" + getDateGenerationOptions().getLabel() + "].");
		}
	}


	private Date getCalculationDate(Date processDate) {
		if (getDateGenerationOptions() == null) {
			return processDate;
		}
		return getCalendarDateGenerationHandler().generateDate(getDateGenerationOptions(), processDate, getDaysFromProcessDate());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public Boolean getDateOptionsHeader() {
		return this.dateOptionsHeader;
	}


	public void setDateOptionsHeader(Boolean dateOptionsHeader) {
		this.dateOptionsHeader = dateOptionsHeader;
	}


	public DateGenerationOptions getDateGenerationOptions() {
		return this.dateGenerationOptions;
	}


	public void setDateGenerationOptions(DateGenerationOptions dateGenerationOptions) {
		this.dateGenerationOptions = dateGenerationOptions;
	}


	public Integer getDaysFromProcessDate() {
		return this.daysFromProcessDate;
	}


	public void setDaysFromProcessDate(Integer daysFromProcessDate) {
		this.daysFromProcessDate = daysFromProcessDate;
	}
}
