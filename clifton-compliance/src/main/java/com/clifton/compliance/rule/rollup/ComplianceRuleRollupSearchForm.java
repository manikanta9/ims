package com.clifton.compliance.rule.rollup;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ComplianceRuleRollupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer parentRuleId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer childRuleId;

	@SearchField(searchFieldCustomType = SearchFieldCustomTypes.OR, searchField = "referenceOne.id,referenceTwo.id")
	private Integer parentOrChildRuleId;


	/////////////////////////////////////////////////////////////////////////
	/////////            Getter and Setter Methods                ///////////
	/////////////////////////////////////////////////////////////////////////


	public Integer getParentRuleId() {
		return this.parentRuleId;
	}


	public void setParentRuleId(Integer parentRuleId) {
		this.parentRuleId = parentRuleId;
	}


	public Integer getChildRuleId() {
		return this.childRuleId;
	}


	public void setChildRuleId(Integer childRuleId) {
		this.childRuleId = childRuleId;
	}


	public Integer getParentOrChildRuleId() {
		return this.parentOrChildRuleId;
	}


	public void setParentOrChildRuleId(Integer parentOrChildRuleId) {
		this.parentOrChildRuleId = parentOrChildRuleId;
	}
}
