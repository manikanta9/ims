package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.marketdata.TradeMarketDataField;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionOpeningDateFilteringTransformation} is a {@link CompliancePositionTransformation} bean type for filtering position groups based on the position
 * opening date.
 * <p>
 * This transformation can be useful for situations where we only want to look at positions that were opened before or after a specific date. For example, if new functionality
 * (such as {@link TradeMarketDataField trade market data fields}) which only applies to newly-opened positions is added during a release, we may want to restrict the application
 * of one or more rules to positions opened after that release, since those positions will be the only ones that may take advantage of the new functionality.
 *
 * @author MikeH
 */
public class CompliancePositionOpeningDateFilteringTransformation implements CompliancePositionTransformation {

	private PositionDirection includedDirection;
	private Date openingDateBound;
	private boolean useOriginalTransactionDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return CollectionUtils.getFiltered(positionValueList, this::isPositionValueHolderSatisfied);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isPositionValueHolderSatisfied(CompliancePositionValueHolder<AccountingPosition> valueHolder) {
		final boolean valueHolderSatisfied;
		if (valueHolder.getPositionList().isEmpty()) {
			// Value holders with empty position lists always satisfy
			valueHolderSatisfied = true;
		}
		else {
			Date positionStartDate = getPositionValueHolderOpeningDate(valueHolder);
			valueHolderSatisfied = isOpeningDateSatisfied(positionStartDate);
		}
		return valueHolderSatisfied;
	}


	private Date getPositionValueHolderOpeningDate(CompliancePositionValueHolder<AccountingPosition> valueHolder) {
		Function<AccountingPosition, Date> openingDateGetterFunction = isUseOriginalTransactionDate()
				? AccountingPosition::getOriginalTransactionDate
				: AccountingPosition::getTransactionDate;
		List<Date> distinctOpeningDateList = valueHolder.getPositionList().stream()
				.map(openingDateGetterFunction)
				.distinct()
				.collect(Collectors.toList());
		ValidationUtils.assertTrue(distinctOpeningDateList.size() <= 1, () -> String.format("Conflicting dates were found within one or more position groups while processing the transformation of type [%s]. Place this transformation first or group positions by date before executing this transformation. Conflicting dates: %s.", CompliancePositionOpeningDateFilteringTransformation.class.getSimpleName(), CollectionUtils.toString(CollectionUtils.getConverted(distinctOpeningDateList, DateUtils::fromDate), 5)));
		return CollectionUtils.getOnlyElement(distinctOpeningDateList);
	}


	private boolean isOpeningDateSatisfied(Date positionOpeningDate) {
		// Missing start dates are assumed to occur far in the past
		final boolean openingDateSatisfied;
		switch (getIncludedDirection()) {
			case ON_OR_BEFORE:
				openingDateSatisfied = DateUtils.isDateBeforeOrEqual(positionOpeningDate, getOpeningDateBound(), false);
				break;
			case ON_OR_AFTER:
				openingDateSatisfied = DateUtils.isDateBeforeOrEqual(getOpeningDateBound(), positionOpeningDate, false);
				break;
			default:
				throw new IllegalStateException("Unexpected missing property for field [direction].");
		}
		return openingDateSatisfied;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public enum PositionDirection {ON_OR_AFTER, ON_OR_BEFORE}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PositionDirection getIncludedDirection() {
		return this.includedDirection;
	}


	public void setIncludedDirection(PositionDirection includedDirection) {
		this.includedDirection = includedDirection;
	}


	public Date getOpeningDateBound() {
		return this.openingDateBound;
	}


	public void setOpeningDateBound(Date openingDateBound) {
		this.openingDateBound = openingDateBound;
	}


	public boolean isUseOriginalTransactionDate() {
		return this.useOriginalTransactionDate;
	}


	public void setUseOriginalTransactionDate(boolean useOriginalTransactionDate) {
		this.useOriginalTransactionDate = useOriginalTransactionDate;
	}
}
