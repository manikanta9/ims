package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationService;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.trade.restriction.type.TradeRestrictionTypeService;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeEvaluator;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>IncludeExcludeSecurityRule</code> allows the inclusion or exclusion of specific security and investment groups.
 * <p>
 * Any security that falls under the specific group for the case of exclusion will simply ensure that the security is not in those groups.
 * <p>
 * If the rule is configured for inclusion we will ensure that it is in the group. The rule will only fail if it is not included in the group
 * but is included in a wider defined group scope.  A good example would be to say we want to disallow the trading of all Bonds except T-Bills.
 * We can specify the group T-Bills but create an inclusion scope of Bonds.
 *
 * @author apopp
 */
public class IncludeExcludeSecurityRule implements ComplianceRuleEvaluator {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private InvestmentGroupService investmentGroupService;
	private SystemBeanService systemBeanService;
	private TradeRestrictionTypeService tradeRestrictionTypeService;
	private InvestmentSecurityAllocationService investmentSecurityAllocationService;

	/**
	 * Rule will check if the security's instrument is in this group.
	 */
	private Short investmentGroupId;

	/**
	 * Rule will check if securities are in this group.
	 */
	private Short securityGroupId;

	/**
	 * If this field is populated, the rule will only be applied if the security''s instrument in this group. If the security's instrument is not in this group, the rule will always pass for that security, regardless of other configuration.
	 */
	private Short investmentGroupIdScope;

	/**
	 * Whether the rule should include/exclude the security. If true, the rule will fail if the security IS NOT in the group(s). If false, the rule will fail if the security IS in the group(s).
	 */
	private boolean inclusion;

	/**
	 * Specifies which security|ies to use to evaluate the rule
	 */
	private SecuritySelectionOptions securitySelectionOption;

	/**
	 * The trade restriction type. If the traded security passes the included/excluded portion, it can optionally be evaluated to see if the trade passes the restriction. The property will only be evaluated for real-time evaluation, NOT batch execution.
	 */
	private Short tradeRestrictionTypeId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		if (ruleEvaluationConfig.isRealTime()) {
			return evaluateBaseRealTime(ruleEvaluationConfig);
		}
		else {
			return evaluateBatch(ruleEvaluationConfig);
		}
	}


	private ComplianceRuleRun evaluateBatch(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(ruleEvaluationConfig.getPositionList());

		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setPass(true);

		for (InvestmentSecurity security : securityPositionMap.keySet()) {
			generateResult(security, run, null, ruleEvaluationConfig);
		}

		return run;
	}


	private ComplianceRuleRun evaluateBaseRealTime(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setPass(true);

		generateResult(ruleEvaluationConfig.retrieveInvestmentSecurity(), run, ruleEvaluationConfig.getBean(), ruleEvaluationConfig);

		return run;
	}


	private boolean isSecurityInGroup(InvestmentSecurity security) {
		boolean isIncluded = isInvestmentGroupSatisfied(security);
		return (isIncluded || (getSecurityGroupId() != null && getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), getSecurityGroupId())));
	}


	private boolean isInvestmentGroupSatisfied(InvestmentSecurity security) {
		if (getInvestmentGroupId() != null) {
			InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupId());
			return getInvestmentGroupService().isInvestmentSecurityInGroup(group.getName(), security.getId());
		}
		return false;
	}


	private String getGroupName() {
		String groupName = "Not Defined";
		if (getInvestmentGroupId() != null) {
			InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupId());
			if (group != null) {
				groupName = group.getLabel();
			}
		}
		else {
			InvestmentSecurityGroup securityGroup = getInvestmentSecurityGroupService().getInvestmentSecurityGroup(getSecurityGroupId());
			if (securityGroup != null) {
				groupName = securityGroup.getLabel();
			}
		}
		return groupName;
	}


	private String generateFailureMessage(InvestmentSecurity security) {
		StringBuilder result = new StringBuilder();

		result.append(security.getLabel());

		if (isInclusion()) {
			result.append(" is not included in");
		}
		else {
			result.append(" is contained in exclusion");
		}

		result.append(" group ")
				.append(getGroupName());

		return result.toString();
	}


	private boolean isGroupScopeSatisfied(InvestmentSecurity security) {
		//If there is a group scope we will check if the security falls within that scope
		// if there is no group scope defined this rule applies therefore we will set it to true
		InvestmentGroup groupScope = (getInvestmentGroupIdScope() != null) ? getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupIdScope()) : null;

		if (groupScope != null) {
			return !getSecuritySelectionOption().isUseActualSecurity() || getInvestmentGroupService().isInvestmentSecurityInGroup(groupScope.getName(), security.getId());
		}
		else {
			return true;
		}
	}


	private boolean isTradeRestrictionAllowed(AccountingBean bean) {
		if (getTradeRestrictionTypeId() != null && bean instanceof Trade) {
			Trade trade = (Trade) bean;
			TradeRestriction restriction = new TradeRestriction();
			restriction.setRestrictionType(getTradeRestrictionTypeService().getTradeRestrictionType(getTradeRestrictionTypeId()));
			restriction.setInvestmentSecurity(trade.getInvestmentSecurity());
			restriction.setClientInvestmentAccount(trade.getClientInvestmentAccount());
			restriction.setStartDate(trade.getTradeDate());
			restriction.setEndDate(trade.getTradeDate());

			TradeRestrictionTypeEvaluator restrictionTypeEvaluator = (TradeRestrictionTypeEvaluator) getSystemBeanService().getBeanInstance(restriction.getRestrictionType().getEvaluatorBean());

			if (!restrictionTypeEvaluator.isValidTrade(trade, restriction)) {
				return false;
			}
		}
		return true;
	}


	private ComplianceRuleRunDetail generateResult(InvestmentSecurity security, ComplianceRuleRun run, AccountingBean bean, ComplianceRuleEvaluationConfig config) {

		String result = null;

		Date evalDate;
		if (run.getComplianceRun() != null) {
			evalDate = run.getComplianceRun().getRunDate();
		}
		else if (bean != null) {
			evalDate = bean.getTransactionDate();
		}
		else {
			evalDate = new Date();
		}

		if (isGroupScopeSatisfied(security)) {
			// If the security is restricted based on group membership, evaluate the trade restriction (if defined) and only block trades that fail the restriction
			if (!isSecurityAllowed(security, evalDate, config)) {
				if (getTradeRestrictionTypeId() == null) {
					result = generateFailureMessage(security);
				}
				else if (!isTradeRestrictionAllowed(bean)) {
					result = generateTradeRestrictionFailureMessage((Trade) bean);
				}
			}
		}


		return generateDetail(run, result, security);
	}


	private boolean isSecurityAllowed(InvestmentSecurity security, Date balanceDate, ComplianceRuleEvaluationConfig config) {
		Set<InvestmentSecurity> securitiesToEvaluate = getSecuritiesToEvaluate(security, balanceDate, config);

		for (InvestmentSecurity evalSecurity : CollectionUtils.getIterable(securitiesToEvaluate)) {
			if (isSecurityInGroup(evalSecurity)) {
				// If exclusion rule, break and fail, if inclusion, break and pass
				return isInclusion();
			}
		}

		// If this is an inclusion rule and none of the securities were in the group, fail
		return !isInclusion();
	}


	private Set<InvestmentSecurity> getSecuritiesToEvaluate(InvestmentSecurity primarySecurity, Date balanceDate, ComplianceRuleEvaluationConfig config) {
		Set<InvestmentSecurity> securitiesToEvaluate = new HashSet<>();

		if (getSecuritySelectionOption().isUseActualSecurity()) {
			securitiesToEvaluate.add(primarySecurity);
		}
		if (getSecuritySelectionOption().isUseUnderlyingSecurity() && primarySecurity.getUnderlyingSecurity() != null) {
			securitiesToEvaluate.add(primarySecurity.getUnderlyingSecurity());
		}
		if (getSecuritySelectionOption().isUseConstituents()) {
			addConstituents(primarySecurity, balanceDate, securitiesToEvaluate, config);
		}

		return securitiesToEvaluate;
	}


	private void addConstituents(InvestmentSecurity primarySecurity, Date balanceDate, Set<InvestmentSecurity> securitiesToEvaluate, ComplianceRuleEvaluationConfig config) {
		if (balanceDate != null) {
			if (primarySecurity.isAllocationOfSecurities()) {
				Set<InvestmentSecurity> allocations = config.getSecurityConstituents().computeIfAbsent(BeanUtils.createKeyFromBeans(primarySecurity.getId(), balanceDate), k -> getAllocations(primarySecurity, balanceDate));
				securitiesToEvaluate.addAll(allocations);
			}
			if (primarySecurity.getUnderlyingSecurity() != null && primarySecurity.getUnderlyingSecurity().isAllocationOfSecurities()) {
				Set<InvestmentSecurity> allocations = config.getSecurityConstituents().computeIfAbsent(BeanUtils.createKeyFromBeans(primarySecurity.getUnderlyingSecurity().getId(), balanceDate), k -> getAllocations(primarySecurity.getUnderlyingSecurity(), balanceDate));
				securitiesToEvaluate.addAll(allocations);
			}
		}
		else {
			throw new ValidationException("If Security Selection Option uses constituents, a balance date must be provided on the run to determine the active allocations on that date. No balance date for " + primarySecurity.getLabel());
		}
	}


	private ComplianceRuleRunDetail generateDetail(ComplianceRuleRun run, String message, InvestmentSecurity security) {
		ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
		detail.setDescription(security.getName());
		detail.setPass(true);
		detail.setComplianceRuleRun(run);
		run.addDetail(detail);

		if (message != null) {
			detail.setDescription(message);
			detail.setPass(false);

			if (run.getDescription() == null) {
				run.setDescription(message);
				run.setPass(false);
			}
		}

		return detail;
	}


	private Set<InvestmentSecurity> getAllocations(InvestmentSecurity security, Date activeOnDate) {
		SecurityAllocationSearchForm sf = new SecurityAllocationSearchForm();
		sf.setActiveOnDate(activeOnDate);
		sf.setParentInvestmentSecurityId(security.getId());
		List<InvestmentSecurityAllocation> allocations = getInvestmentSecurityAllocationService().getInvestmentSecurityAllocationList(sf);
		if (!CollectionUtils.isEmpty(allocations)) {
			return allocations.stream().map(InvestmentSecurityAllocation::getInvestmentSecurity).collect(Collectors.toSet());
		}
		return new HashSet<>();
	}


	private String generateTradeRestrictionFailureMessage(Trade trade) {
		StringBuilder result = new StringBuilder();

		TradeRestrictionType type = getTradeRestrictionTypeService().getTradeRestrictionType(getTradeRestrictionTypeId());

		result.append("Trade [")
				.append(trade.getLabel())
				.append("] is not allowed because it fails the Restriction of type [")
				.append(type.getLabel())
				.append("].");

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public enum SecuritySelectionOptions {

		ACTUAL_SECURITY(true, false, false, "Actual Security"), UNDERLYING_SECURITY(false, true, false, "Underlying Security"), BOTH(true, true, false, "Security or Underlying"), ALL(true, true, true, "All Securities");


		SecuritySelectionOptions(boolean actual, boolean underlying, boolean constituents, String label) {
			this.actual = actual;
			this.underlying = underlying;
			this.constituents = constituents;
			this.label = label;
		}


		private final boolean actual;
		private final boolean underlying;
		private final boolean constituents;
		private final String label;


		public boolean isUseActualSecurity() {
			return this.actual;
		}


		public boolean isUseUnderlyingSecurity() {
			return this.underlying;
		}


		public boolean isUseConstituents() {
			return this.constituents;
		}


		public String getLabel() {
			return this.label;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentGroupIdScope() {
		return this.investmentGroupIdScope;
	}


	public void setInvestmentGroupIdScope(Short investmentGroupIdScope) {
		this.investmentGroupIdScope = investmentGroupIdScope;
	}


	public boolean isInclusion() {
		return this.inclusion;
	}


	public void setInclusion(boolean inclusion) {
		this.inclusion = inclusion;
	}


	public SecuritySelectionOptions getSecuritySelectionOption() {
		return this.securitySelectionOption;
	}


	public void setSecuritySelectionOption(SecuritySelectionOptions securitySelectionOption) {
		this.securitySelectionOption = securitySelectionOption;
	}


	public Short getTradeRestrictionTypeId() {
		return this.tradeRestrictionTypeId;
	}


	public void setTradeRestrictionTypeId(Short tradeRestrictionTypeId) {
		this.tradeRestrictionTypeId = tradeRestrictionTypeId;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TradeRestrictionTypeService getTradeRestrictionTypeService() {
		return this.tradeRestrictionTypeService;
	}


	public void setTradeRestrictionTypeService(TradeRestrictionTypeService tradeRestrictionTypeService) {
		this.tradeRestrictionTypeService = tradeRestrictionTypeService;
	}


	public InvestmentSecurityAllocationService getInvestmentSecurityAllocationService() {
		return this.investmentSecurityAllocationService;
	}


	public void setInvestmentSecurityAllocationService(InvestmentSecurityAllocationService investmentSecurityAllocationService) {
		this.investmentSecurityAllocationService = investmentSecurityAllocationService;
	}
}
