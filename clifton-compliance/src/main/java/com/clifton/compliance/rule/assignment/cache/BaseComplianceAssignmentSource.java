package com.clifton.compliance.rule.assignment.cache;


import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The {@link SpecificityCache} <i>source type</i> for {@link ComplianceRuleAssignment} specificity entry retrievals.
 * <p>
 * This type is used as the input when querying implementors of {@link BaseComplianceAssignmentSpecificityCache} for rule assignment results based on specificity. This type and its
 * implementors include all of the necessary information for a query to the associated cache.
 * <p>
 * This type and its subtypes are used as the input for specificity queries which produce {@link BaseComplianceAssignmentTargetHolder} results.
 *
 * @author MikeH
 */
public abstract class BaseComplianceAssignmentSource {

	private final InvestmentAccount clientAccount;
	private final boolean realTime;
	private final Date date;

	// Cache value at instantiation rather than generating each time this is used
	private final SpecificityKeySource keySource;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseComplianceAssignmentSource(InvestmentAccount clientAccount, Date date, boolean realTime, SpecificityKeySource keySource) {
		this.clientAccount = clientAccount;
		this.realTime = realTime;
		this.date = date;
		this.keySource = keySource;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public boolean isRealTime() {
		return this.realTime;
	}


	public Date getDate() {
		return this.date;
	}


	public SpecificityKeySource getKeySource() {
		return this.keySource;
	}
}
