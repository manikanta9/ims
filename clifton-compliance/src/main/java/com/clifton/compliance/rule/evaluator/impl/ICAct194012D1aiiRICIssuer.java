package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>ICAct194012D1aiiRICIssuer</code>
 * <p>
 * <p>
 * Investment Company Act of 1940 Section 12(d)(1)(a)(ii): 5% RIC issuer Test
 * <p>
 * Immediately after the acquisition of
 * shares of any RIC, a fund shall not have invested more
 * than 5% of its total assets in the securities of any one
 * RIC.
 *
 * @author apopp
 */
public class ICAct194012D1aiiRICIssuer extends BaseComplianceRuleRatioEvaluator {

	private ComplianceRulePositionHandler complianceRulePositionHandler;

	private boolean excludeCashCollateral;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String securityName = "Missing Name";
		InvestmentSecurity security = ruleEvaluationConfig.retrieveInvestmentSecurity();
		if (security != null) {
			securityName = security.getName();
		}
		return securityName;
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();
		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		getComplianceRuleFilterHandler().filterByRIC(securityIdList, true);

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		for (InvestmentSecurity security : securityPositionMap.keySet()) {
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());
			config.setInvestmentSecurityId(security.getId());
			config.setIncludeCashCollateral(!isExcludeCashCollateral());
			configList.add(config);
		}

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<Integer> securityIdList = CollectionUtils.createList(ruleEvaluationConfig.getInvestmentSecurityId());
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityPositionMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getComplianceRulePositionHandler().getAccountTotalValue(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ruleEvaluationConfig.getPositionList(), ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE,
				ruleEvaluationConfig.retrieveAccountIdList(), isUseTotalAssets(), isExcludeCashCollateral());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public boolean isExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}
}
