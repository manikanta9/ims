package com.clifton.compliance.rule;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;

import java.math.BigDecimal;


public class ComplianceRuleSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "ruleType.name")
	private String typeName;

	@SearchField(searchField = "ruleType.id")
	private Short typeId;

	@SearchField(searchField = "batch")
	private Boolean batchRule;

	@SearchField(searchField = "realTime")
	private Boolean realTimeRule;

	@SearchField(searchFieldPath = "ruleType", searchField = "rollupRule")
	private Boolean rollupRule;

	@SearchField(searchFieldPath = "ruleType", searchField = "investmentSecuritySpecific")
	private Boolean investmentSecuritySpecific;

	@SearchField(searchFieldPath = "complianceRuleLimitType", searchField = "name")
	private String limitTypeName;
	@SearchField
	private BigDecimal limitValue;
	@SearchField
	private BigDecimal limitValueRange;

	@SearchField(searchField = "ruleEvaluatorBean.id")
	private Integer ruleEvaluatorBean;


	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType.name")
	private String investmentTypeSubTypeName;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "investmentTypeSubType2.name")
	private String investmentTypeSubType2Name;

	@SearchField(searchField = "investmentHierarchy.id")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentInstrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "underlyingInstrument.id")
	private Integer underlyingInstrumentId;

	@SearchField
	private InvestmentCurrencyTypes currencyType;

	@SearchField(searchField = "parentRuleList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer parentComplianceRuleId;

	@SearchField(searchField = "childRuleList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer childComplianceRuleId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Integer getRuleEvaluatorBean() {
		return this.ruleEvaluatorBean;
	}


	public void setRuleEvaluatorBean(Integer ruleEvaluatorBean) {
		this.ruleEvaluatorBean = ruleEvaluatorBean;
	}


	public Integer getParentComplianceRuleId() {
		return this.parentComplianceRuleId;
	}


	public void setParentComplianceRuleId(Integer parentComplianceRuleId) {
		this.parentComplianceRuleId = parentComplianceRuleId;
	}


	public Integer getChildComplianceRuleId() {
		return this.childComplianceRuleId;
	}


	public void setChildComplianceRuleId(Integer childComplianceRuleId) {
		this.childComplianceRuleId = childComplianceRuleId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getBatchRule() {
		return this.batchRule;
	}


	public void setBatchRule(Boolean batchRule) {
		this.batchRule = batchRule;
	}


	public Boolean getRealTimeRule() {
		return this.realTimeRule;
	}


	public void setRealTimeRule(Boolean realTimeRule) {
		this.realTimeRule = realTimeRule;
	}


	public Boolean getRollupRule() {
		return this.rollupRule;
	}


	public void setRollupRule(Boolean rollupRule) {
		this.rollupRule = rollupRule;
	}


	public Boolean getInvestmentSecuritySpecific() {
		return this.investmentSecuritySpecific;
	}


	public void setInvestmentSecuritySpecific(Boolean investmentSecuritySpecific) {
		this.investmentSecuritySpecific = investmentSecuritySpecific;
	}


	public String getLimitTypeName() {
		return this.limitTypeName;
	}


	public void setLimitTypeName(String limitTypeName) {
		this.limitTypeName = limitTypeName;
	}


	public BigDecimal getLimitValue() {
		return this.limitValue;
	}


	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}


	public BigDecimal getLimitValueRange() {
		return this.limitValueRange;
	}


	public void setLimitValueRange(BigDecimal limitValueRange) {
		this.limitValueRange = limitValueRange;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public String getInvestmentTypeSubTypeName() {
		return this.investmentTypeSubTypeName;
	}


	public void setInvestmentTypeSubTypeName(String investmentTypeSubTypeName) {
		this.investmentTypeSubTypeName = investmentTypeSubTypeName;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getInvestmentTypeSubType2Name() {
		return this.investmentTypeSubType2Name;
	}


	public void setInvestmentTypeSubType2Name(String investmentTypeSubType2Name) {
		this.investmentTypeSubType2Name = investmentTypeSubType2Name;
	}


	public Integer getUnderlyingInstrumentId() {
		return this.underlyingInstrumentId;
	}


	public void setUnderlyingInstrumentId(Integer underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}


	public InvestmentCurrencyTypes getCurrencyType() {
		return this.currencyType;
	}


	public void setCurrencyType(InvestmentCurrencyTypes currencyType) {
		this.currencyType = currencyType;
	}
}
