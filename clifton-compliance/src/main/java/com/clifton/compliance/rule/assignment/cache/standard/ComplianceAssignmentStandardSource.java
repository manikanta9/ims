package com.clifton.compliance.rule.assignment.cache.standard;


import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentSource;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The <i>source type</i> for the {@link ComplianceAssignmentStandardSpecificityCache} specificity cache.
 * <p>
 * This type is used as the input for specificity queries which produce {@link ComplianceAssignmentStandardTargetHolder} results.
 *
 * @author MikeH
 */
public class ComplianceAssignmentStandardSource extends BaseComplianceAssignmentSource {

	public ComplianceAssignmentStandardSource(InvestmentAccount clientAccount, Date date, boolean realTime) {
		super(clientAccount, date, realTime, generateKeySource(clientAccount));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static SpecificityKeySource generateKeySource(InvestmentAccount clientAccount) {
		Object[] accountSpecificity = {
				clientAccount.getId(),
				BeanUtils.getBeanIdentity(clientAccount.getBusinessService()),
				null
		};
		return SpecificityKeySource
				.builder(accountSpecificity)
				.build();
	}
}
