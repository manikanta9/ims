package com.clifton.compliance.rule.assignment.cache.standard;

import com.clifton.business.service.BusinessService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;


/**
 * The specificity cache type for all {@link ComplianceRuleAssignment} entities.
 * <p>
 * This specificity cache is used to find the most specific assignments which apply to accounts for each rule instance. For example, a rule may have a global assignment, a {@link
 * BusinessService} assignment, and a {@link InvestmentAccount} assignment applying the rule to the same account. These assignments may have different traits, such as the
 * subsidiary account purpose, so the most specific such assignment must be found. This cache achieves this purpose.
 *
 * @author MikeH
 */
public interface ComplianceAssignmentStandardSpecificityCache {

	/**
	 * Retrieves the most specific set of results that apply for the given account.
	 */
	List<ComplianceRuleAssignment> getMostSpecificResultList(InvestmentAccount clientAccount, Date processDate, boolean realTime);


	/**
	 * Retrieves the most specific set of results that apply for the given account, excluding results for {@link ComplianceRuleType#investmentSecuritySpecific security-specific}
	 * rule types.
	 */
	List<ComplianceRuleAssignment> getMostSpecificNonSecuritySpecificResultList(InvestmentAccount clientAccount, Date processDate, boolean realTime);


	/**
	 * Pre-builds the cache for later use.
	 */
	void prebuildCache();
}
