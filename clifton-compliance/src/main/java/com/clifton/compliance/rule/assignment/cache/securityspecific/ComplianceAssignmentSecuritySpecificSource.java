package com.clifton.compliance.rule.assignment.cache.securityspecific;


import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentSource;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;

import java.util.Date;


/**
 * The <i>source type</i> for the {@link ComplianceAssignmentSecuritySpecificSpecificityCache} specificity cache.
 * <p>
 * This type is used as the input for specificity queries which produce {@link ComplianceAssignmentSecuritySpecificTargetHolder} results.
 *
 * @author MikeH
 */
public class ComplianceAssignmentSecuritySpecificSource extends BaseComplianceAssignmentSource {

	private final InvestmentSecurity security;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceAssignmentSecuritySpecificSource(InvestmentAccount clientAccount, InvestmentSecurity security, Date date, boolean realTime) {
		super(clientAccount, date, realTime, generateKeySource(clientAccount, security));
		this.security = security;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static SpecificityKeySource generateKeySource(InvestmentAccount clientAccount, InvestmentSecurity security) {
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentType investmentType = hierarchy.getInvestmentType();
		InvestmentTypeSubType investmentTypeSubType = hierarchy.getInvestmentTypeSubType();
		InvestmentTypeSubType2 investmentTypeSubType2 = hierarchy.getInvestmentTypeSubType2();
		InvestmentInstrument underlyingInstrument = security.getInstrument().getUnderlyingInstrument();

		return SpecificityKeySource
				.builder(BeanUtils.getBeanIdentity(instrument), BeanUtils.getBeanIdentity(hierarchy), null)
				.addHierarchy(BeanUtils.getBeanIdentity(underlyingInstrument), null)
				.addHierarchy(BeanUtils.getBeanIdentity(investmentTypeSubType2), null)
				.addHierarchy(BeanUtils.getBeanIdentity(investmentTypeSubType), null)
				.addHierarchy(BeanUtils.getBeanIdentity(investmentType), null)
				.addHierarchy(BeanUtils.getBeanIdentity(clientAccount), BeanUtils.getBeanIdentity(clientAccount.getBusinessService()), null)
				.addHierarchy(InvestmentUtils.isInternationalSecurity(security, clientAccount.getBaseCurrency()) ? InvestmentCurrencyTypes.INTERNATIONAL : InvestmentCurrencyTypes.DOMESTIC, null)
				.build();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}
}
