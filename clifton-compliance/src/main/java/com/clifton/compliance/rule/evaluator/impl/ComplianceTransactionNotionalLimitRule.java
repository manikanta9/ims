package com.clifton.compliance.rule.evaluator.impl;

import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>ComplianceTransactionNotionalLimitRule</code> ensures the transaction notional in base currency
 * does not exceed the maximum notional value defined for the implementing rule and assignment.
 * <p>
 * It's pass/fail status depends on a specified range where a min, max, or both is specified.
 *
 * @author stevenf
 */
public class ComplianceTransactionNotionalLimitRule implements ComplianceRuleEvaluator {

	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private InvestmentGroupService investmentGroupService;

	//Optionally limits rule evaluation to instruments in this group
	private Short instrumentGroupId;
	private BigDecimal maxNotional;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		AccountingBean accountingBean = ruleEvaluationConfig.getBean();
		//If instrument group id is set; do not process if this beans instrument group id does not match.
		if (getInstrumentGroupId() != null && !getInvestmentGroupService().isInvestmentInstrumentInGroup(getInstrumentGroupId(), accountingBean.getInvestmentSecurity().getInstrument().getId())) {
			return null;
		}
		BigDecimal notionalValue = accountingBean.getAccountingNotional();
		boolean success = MathUtils.isLessThan(notionalValue, getMaxNotional());
		String failureMessage = "Notional value of '" + CoreMathUtils.formatNumberMoney(notionalValue) +
				"' exceeds maximum notional value of '" + CoreMathUtils.formatNumberMoney(getMaxNotional()) + "'";
		//TODO - refactor all instances of ruleReportHandler when addressing other issues
		return getComplianceRuleReportHandler().createBasicRuleRun(failureMessage, notionalValue, success, null);
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public Short getInstrumentGroupId() {
		return this.instrumentGroupId;
	}


	public void setInstrumentGroupId(Short instrumentGroupId) {
		this.instrumentGroupId = instrumentGroupId;
	}


	public BigDecimal getMaxNotional() {
		return this.maxNotional;
	}


	public void setMaxNotional(BigDecimal maxNotional) {
		this.maxNotional = maxNotional;
	}
}
