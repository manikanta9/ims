package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IRSSection851b3B</code>
 * <p>
 * <p>
 * <p>
 * IRS Section 851(b)(3)B (To qualify as a RIC for tax treatment):  No single Ultimate issuer may represent  greater than 25% of the Fund's total assets excluding:
 * <p>
 * 2) Government securities;
 * <p>
 * 3) Securities of other investment companies;
 * <p>
 * Denominator:  Funds Total Assets or Account Market Value
 * <p>
 * Numerator:  Ultimate issuers market value of positions (Excluding US Governments and Agencies)
 *
 * @author apopp
 */
public class IRSSection851b3B extends BaseComplianceRuleRatioEvaluator {

	private ComplianceRulePositionHandler complianceRulePositionHandler;

	/*
	 * This rule needs to be able to exclude U.S. Gov issued securities. To be more flexible we allow the rule to
	 * specify a business company issuer to exclude. All securities whose ultimate issuer falls under this business
	 * company will be excluded from the processing of the rule
	 */
	private Integer excludeBusinessCompanyId;

	//This rule needs to be able to allow exclusion of RIC securities, defined by TYPE = "Registered Investment Company"
	private Short excludeBusinessCompanyTypeId;

	private boolean excludeCashCollateral;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return ruleEvaluationConfig.getIssuer().getLabel();
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();

		Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap = ComplianceUtils.mapPositionListByUltimateIssuer(ComplianceUtils.filterByCompanyType(ruleEvaluationConfig.getPositionList(),
				getExcludeBusinessCompanyTypeId(), false));

		ComplianceUtils.filterByCompany(issuerPositionMap, getExcludeBusinessCompanyId(), false);
		issuerPositionMap.remove(null);

		for (BusinessCompany company : issuerPositionMap.keySet()) {
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());
			config.setIncludeCashCollateral(!isExcludeCashCollateral());
			config.setIssuer(company);
			configList.add(config);
		}

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRulePosition> filteredPositionList = ComplianceUtils.filterByCompanyType(ruleEvaluationConfig.getPositionList(), getExcludeBusinessCompanyTypeId(), false);
		Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionMap = ComplianceUtils.mapPositionListByUltimateIssuer(filteredPositionList);

		Map<BusinessCompany, Map<InvestmentSecurity, List<ComplianceRulePosition>>> issuerSecurityPositionMap = ComplianceUtils.mapIssuerBySecurityPositionList(issuerPositionMap);

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), issuerSecurityPositionMap.get(ruleEvaluationConfig.getIssuer()), ruleEvaluationConfig.getProcessDate(),
				ComplianceRulePositionValues.MARKET_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getComplianceRulePositionHandler().getAccountTotalValue(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ruleEvaluationConfig.getPositionList(), ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE,
				ruleEvaluationConfig.retrieveAccountIdList(), isUseTotalAssets(), isExcludeCashCollateral());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public Integer getExcludeBusinessCompanyId() {
		return this.excludeBusinessCompanyId;
	}


	public void setExcludeBusinessCompanyId(Integer excludeBusinessCompanyId) {
		this.excludeBusinessCompanyId = excludeBusinessCompanyId;
	}


	public Short getExcludeBusinessCompanyTypeId() {
		return this.excludeBusinessCompanyTypeId;
	}


	public void setExcludeBusinessCompanyTypeId(Short excludeBusinessCompanyTypeId) {
		this.excludeBusinessCompanyTypeId = excludeBusinessCompanyTypeId;
	}


	public boolean isExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}
}
