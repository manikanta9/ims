package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.List;


/**
 * The {@link CompliancePositionEndDateFilteringTransformation} bean type for filtering positions based the associated InvestmentSecurity's EndDate or LastDeliveryDate.
 * <p>
 * This transformation compares the EndDate or LastDeliveryDate (depending on the value chosen for the dateField) property of the InvestmentSecurity within the {@link CompliancePositionValueHolder}
 * against a specified, inclusive date range via the 'apply' method.  Entities matching the criteria are returned (in a list of {@link CompliancePositionValueHolder}) if the excludeWithinRange property is set to false.
 * If excludeWithinRange is set to true, the matching entities are excluded from the returned list.
 *
 * @author DavidI
 */
public class CompliancePositionEndDateFilteringTransformation implements CompliancePositionTransformation, ValidationAware {

	private DateFields dateField;
	private boolean useProcessDate;
	private boolean excludeWithinRange;
	private Integer minimumDays;
	private Integer maximumDays;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		return CollectionUtils.getFiltered(positionValueList, valueHolder -> isGroupInDateRange(valueHolder, processDate) ^ isExcludeWithinRange());
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getDateField(), "A DateType is required for the date field.");
		ValidationUtils.assertFalse(getMinimumDays() == null && getMaximumDays() == null, "At least one field value must be set (Minimum Days or Maximum Days).", "minimumDays");
		ValidationUtils.assertTrue(getMinimumDays() == null || getMinimumDays() >= 0, "The 'minimumDays' value cannot be negative.", "minimumDays");
		ValidationUtils.assertTrue(getMaximumDays() == null || getMaximumDays() >= 0, "The 'maximumDays' value cannot be negative.", "maximumDays");
		ValidationUtils.assertTrue(getMinimumDays() == null || getMaximumDays() == null || getMaximumDays() >= getMinimumDays(), "The 'maximumDays' value must be greater than or equal to the 'minimumDays' value.", "maximumDays");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isGroupInDateRange(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, Date processDate) {
		ValidationUtils.assertTrue(!positionValueHolder.getPositionList().isEmpty(), "Empty position groups are not supported by the end date filtering transformation.");
		Date baseDate = getBaseDate(positionValueHolder, processDate);
		Date compareDate = getCompareDate(positionValueHolder);
		Date minimumDate = getMinimumDays() != null ? DateUtils.addDays(baseDate, getMinimumDays()) : null;
		Date maximumDate = getMaximumDays() != null ? DateUtils.addDays(baseDate, getMaximumDays()) : null;
		// Special case: Allow no end date if and only if no maximum is set
		boolean passedNoEndDate = compareDate == null && maximumDate == null;
		return passedNoEndDate || DateUtils.isDateBetween(compareDate, minimumDate, maximumDate, false);
	}


	private Date getBaseDate(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, Date processDate) {
		// Guard-clause: Use process date when flag is set
		if (isUseProcessDate()) {
			return processDate;
		}

		// Find common open date for positions
		List<AccountingPosition> distinctPositionListByDate = CollectionUtils.getDistinct(positionValueHolder.getPositionList(), AccountingPosition::getTransactionDate);
		ValidationUtils.assertTrue(distinctPositionListByDate.size() == 1, () -> String.format("When using the position date for date comparisons, all positions within each position group must have an equivalent transaction date. Discovered positions with differing dates: %s.", CollectionUtils.toString(distinctPositionListByDate, 3)));
		return CollectionUtils.getOnlyElementStrict(distinctPositionListByDate).getTransactionDate();
	}


	private Date getCompareDate(CompliancePositionValueHolder<AccountingPosition> positionValueHolder) {
		List<AccountingPosition> distinctPositionListByEndDate = CollectionUtils.getDistinct(positionValueHolder.getPositionList(), getDateField() == DateFields.LAST_DELIVERY_DATE
				? position -> position.getInvestmentSecurity().getLastDeliveryDate()
				: position -> position.getInvestmentSecurity().getEndDate());
		ValidationUtils.assertTrue(distinctPositionListByEndDate.size() == 1, () -> String.format("All positions within each position group have an equivalent end date of type [%s]. Discovered positions with differing dates: %s.", getDateField(), CollectionUtils.toString(distinctPositionListByEndDate, 3)));
		AccountingPosition representativePosition = CollectionUtils.getOnlyElementStrict(distinctPositionListByEndDate);
		return getDateField() == DateFields.LAST_DELIVERY_DATE
				? representativePosition.getInvestmentSecurity().getLastDeliveryDate()
				: representativePosition.getInvestmentSecurity().getEndDate();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public enum DateFields {END_DATE, LAST_DELIVERY_DATE}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DateFields getDateField() {
		return this.dateField;
	}


	public void setDateField(DateFields dateField) {
		this.dateField = dateField;
	}


	public boolean isUseProcessDate() {
		return this.useProcessDate;
	}


	public void setUseProcessDate(boolean useProcessDate) {
		this.useProcessDate = useProcessDate;
	}


	public boolean isExcludeWithinRange() {
		return this.excludeWithinRange;
	}


	public void setExcludeWithinRange(boolean excludeWithinRange) {
		this.excludeWithinRange = excludeWithinRange;
	}


	public Integer getMinimumDays() {
		return this.minimumDays;
	}


	public void setMinimumDays(Integer minimumDays) {
		this.minimumDays = minimumDays;
	}


	public Integer getMaximumDays() {
		return this.maximumDays;
	}


	public void setMaximumDays(Integer maximumDays) {
		this.maximumDays = maximumDays;
	}
}
