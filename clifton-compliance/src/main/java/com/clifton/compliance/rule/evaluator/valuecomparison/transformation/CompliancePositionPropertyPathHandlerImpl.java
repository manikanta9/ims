package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.marketdata.TradeMarketDataField;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * The standard {@link CompliancePositionPropertyPathHandler} implementation.
 *
 * @author Davidi
 */
@Component
public class CompliancePositionPropertyPathHandlerImpl implements CompliancePositionPropertyPathHandler {

	/**
	 * The collection of column group mappings for entity types. These mappings are used to determine the column group for which to retrieve the column values for an entity when
	 * grouping by custom column values.
	 */
	private static final Map<Class<? extends SystemColumnCustomAware>, String> COLUMN_GROUP_MAPPINGS = ((Supplier<Map<Class<? extends SystemColumnCustomAware>, String>>) () -> {
		Map<Class<? extends SystemColumnCustomAware>, String> groupMappings = new HashMap<>();
		groupMappings.put(InvestmentSecurity.class, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		groupMappings.put(InvestmentInstrument.class, InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME);
		groupMappings.put(BusinessCompany.class, BusinessCompany.BUSINESS_COMPANY_CUSTOM_FIELDS_GROUP_NAME);
		groupMappings.put(InvestmentAccount.class, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME);
		return groupMappings;
	}).get();

	private MarketDataFieldService marketDataFieldService;
	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getPositionPropertyValue(AccountingPosition position, String field, Date transactionDate) {
		return PropertyPathType.forField(field).retrieveValue(this, position, field, transactionDate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateField(String field) {
		PropertyPathType.forField(field).validateField(this, field);
	}


	@Override
	public boolean fieldDataTypeIsNumeric(String field) {
		return PropertyPathType.forField(field).getDataTypeForField(this, field) == DataTypeNames.DECIMAL;
	}


	@Override
	public boolean fieldDataTypeIsString(String field) {
		return PropertyPathType.forField(field).getDataTypeForField(this, field) == DataTypeNames.STRING;
	}


	@Override
	public boolean fieldDataTypeIsDate(String field) {
		return PropertyPathType.forField(field).getDataTypeForField(this, field) == DataTypeNames.DATE;
	}


	@Override
	public boolean fieldDataTypeIsBoolean(String field) {
		return PropertyPathType.forField(field).getDataTypeForField(this, field) == DataTypeNames.BOOLEAN;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Object getBeanPropertyValue(AccountingPosition position, String propertyPath) {
		// Retrieve bean property
		return BeanUtils.getPropertyValue(position, propertyPath);
	}


	private Object getMarketDataFieldValue(AccountingPosition position, String marketDataFieldName, Date transactionDate, boolean isFlexible) {
		// Retrieve market data field value
		MarketDataValueHolder marketDataValueHolder = getMarketDataFieldService().getMarketDataValueForDate(position.getInvestmentSecurity(), transactionDate, marketDataFieldName, null, isFlexible, false);
		return marketDataValueHolder != null ? marketDataValueHolder.getMeasureValue() : null;
	}


	private Object getCustomColumnValue(AccountingPosition position, String propertyPath, String columnName) {
		/* Retrieve custom column value */
		final Object value;
		// Get entity for custom column value
		Object entity = BeanUtils.getPropertyValue(position, propertyPath);
		if (entity == null) {
			value = null;
		}
		else {
			@SuppressWarnings("unchecked")
			Class<? extends SystemColumnCustomValueAware> propertyType = (Class<? extends SystemColumnCustomValueAware>) BeanUtils.getPropertyType(AccountingPosition.class, propertyPath);
			AssertUtils.assertTrue(COLUMN_GROUP_MAPPINGS.containsKey(propertyType), () -> String.format("Custom column fields are not supported for the entity of type [%s] for path [%s].", propertyType.getSimpleName(), propertyPath));

			// Get custom column value
			String columnGroupName = COLUMN_GROUP_MAPPINGS.get(propertyType);
			SystemColumnCustomValueAware columnCustomAwareProperty = (SystemColumnCustomValueAware) entity;
			value = getSystemColumnValueHandler().getSystemColumnValueForEntity(columnCustomAwareProperty, columnGroupName, columnName, false);
		}
		return value;
	}


	private Object getOpeningTradePropertyValue(AccountingPosition position, String tradePropertyPath) {
		// Get trade property
		Trade openingTrade = getTradeService().getOpeningTradeForAccountingPosition(position);
		return BeanUtils.getPropertyValue(openingTrade, tradePropertyPath);
	}


	private Object getTradeMarketDataFieldValue(AccountingPosition position, String marketDataFieldName) {
		Trade trade = getTradeService().getOpeningTradeForAccountingPosition(position);
		MarketDataField marketDataField = getMarketDataFieldService().getMarketDataFieldByName(marketDataFieldName);
		TradeMarketDataValue tradeMarketDataValue = getTradeMarketDataFieldService().getTradeMarketDataValueLatestForTradeAndBackingFieldUnfiltered(trade, marketDataField);
		return tradeMarketDataValue != null ? tradeMarketDataValue.getReferenceTwo().getMeasureValue() : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that the given field exists as a valid entity property on positions.
	 */
	private void validateBeanField(String propertyPath) {
		ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(AccountingPosition.class, propertyPath), () -> String.format("The property [%s] could not be found for objects of type [%s].", propertyPath, AccountingPosition.class.getSimpleName()));
	}


	/**
	 * Validates that the given field exists as a market data field.
	 */
	private void validateMarketDataField(String marketDataFieldName) {
		MarketDataField marketDataField = getMarketDataFieldService().getMarketDataFieldByName(marketDataFieldName);
		ValidationUtils.assertNotNull(marketDataField, () -> String.format("Unable to find market data field with name [%s].", marketDataFieldName));
	}


	/**
	 * Validates that the given field exists as a custom column value under a supported type. Supported types are enumerated by {@link #COLUMN_GROUP_MAPPINGS}.
	 */
	private void validateCustomColumnField(String propertyPath, String columnName) {
		// Validate entity path presence
		validateBeanField(propertyPath);

		// Validate entity type
		@SuppressWarnings("unchecked")
		Class<? extends SystemColumnCustomValueAware> propertyType = (Class<? extends SystemColumnCustomValueAware>) BeanUtils.getPropertyType(AccountingPosition.class, propertyPath);
		ValidationUtils.assertTrue(COLUMN_GROUP_MAPPINGS.containsKey(propertyType), () -> String.format("Custom column fields are not supported for properties of type [%s].", propertyType.getSimpleName()));

		// Validate existence of column
		SystemColumnCustom column = getCustomColumn(COLUMN_GROUP_MAPPINGS.get(propertyType), columnName);
		ValidationUtils.assertNotNull(column, () -> String.format("Unable to find custom column field [%s] for objects of type [%s].", columnName, propertyType.getSimpleName()));
	}


	/**
	 * Validates that the given field exists as a valid entity property on trades.
	 */
	private void validateOpeningTradeField(String tradePropertyPath) {
		ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(Trade.class, tradePropertyPath), () -> String.format("The property [%s] could not be found for objects of type [%s].", tradePropertyPath, Trade.class.getSimpleName()));
	}


	/**
	 * Validates that the given field exists as a trade market data field.
	 */
	private void validateTradeMarketDataField(String marketDataFieldName) {
		TradeMarketDataFieldSearchForm searchForm = new TradeMarketDataFieldSearchForm();
		searchForm.setMarketDataFieldNameEquals(marketDataFieldName);
		List<TradeMarketDataField> fieldList = getTradeMarketDataFieldService().getTradeMarketDataFieldList(searchForm);
		/*
		 * Given that multiple trade market data fields may exist for the same market data field (e.g., trade fields targeting the market data field at multiple workflow stages),
		 * we allow any number of fields here.
		 */
		ValidationUtils.assertTrue(!fieldList.isEmpty(), () -> String.format("Unable to find trade market data field with name [%s].", marketDataFieldName));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTypeNames getCustomColumnDataType(String propertyPath, String columnName) {
		// Validate property path
		@SuppressWarnings("unchecked")
		Class<? extends SystemColumnCustomValueAware> propertyType = (Class<? extends SystemColumnCustomValueAware>) BeanUtils.getPropertyType(AccountingPosition.class, propertyPath);
		AssertUtils.assertTrue(COLUMN_GROUP_MAPPINGS.containsKey(propertyType), () -> String.format("Custom column fields are not supported for the entity of type [%s] for path [%s].", propertyType.getSimpleName(), propertyPath));

		// Get column metadata
		SystemColumnCustom column = getCustomColumn(COLUMN_GROUP_MAPPINGS.get(propertyType), columnName);
		ValidationUtils.assertNotNull(column, () -> String.format("Unable to find custom column field [%s] for objects of type [%s].", columnName, propertyType.getSimpleName()));

		// Retrieve column type
		SystemDataTypeConverter converter = new SystemDataTypeConverter(column.getDataType());
		final DataTypeNames type;
		if (converter.isNumber()) {
			type = DataTypeNames.DECIMAL;
		}
		else if (converter.isString()) {
			type = DataTypeNames.STRING;
		}
		else {
			type = column.getDataType().asDataTypeNames();
		}
		return type;
	}


	/**
	 * Returns a list of SystemColumnCustom with the specified columnGroupName and columnName
	 */
	private SystemColumnCustom getCustomColumn(String columnGroupName, String columnName) {
		List<SystemColumnCustom> customColumnList = CollectionUtils.asNonNullList(getSystemColumnService().getSystemColumnCustomListForGroup(columnGroupName));

		// Apply case-sensitive filtering
		return customColumnList.stream()
				.filter(col -> Objects.equals(col.getName(), columnName))
				.findAny()
				.orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The <code>PropertyPathType</code> enumeration type coordinates logic for each available type of property path.
	 * <p>
	 * Instances of this type direct logic for property {@link #validateField(CompliancePositionPropertyPathHandlerImpl, String) path validation}, {@link
	 * #getDataTypeForField(CompliancePositionPropertyPathHandlerImpl, String) type retrieval}, and {@link #retrieveValue(CompliancePositionPropertyPathHandlerImpl,
	 * AccountingPosition, String, Date) value retrieval}.
	 */
	private enum PropertyPathType {

		BEAN_PROPERTY(
				handler -> handler::validateBeanField,
				handler -> field -> PropertyPathType.getDataTypeName(BeanUtils.getPropertyType(AccountingPosition.class, field)),
				handler -> pos -> field -> date -> handler.getBeanPropertyValue(pos, field)
		),
		MARKET_DATA(
				handler -> field -> handler.validateMarketDataField(field.substring(MARKET_DATA_FIELD_NAME_PREFIX.length())),
				handler -> field -> DataTypeNames.DECIMAL,
				handler -> pos -> field -> date -> handler.getMarketDataFieldValue(pos, field.substring(MARKET_DATA_FIELD_NAME_PREFIX.length()), date, false)
		),
		FLEXIBLE_MARKET_DATA(
				handler -> field -> handler.validateMarketDataField(field.substring(FLEXIBLE_MARKET_DATA_FIELD_NAME_PREFIX.length())),
				handler -> field -> DataTypeNames.DECIMAL,
				handler -> pos -> field -> date -> handler.getMarketDataFieldValue(pos, field.substring(FLEXIBLE_MARKET_DATA_FIELD_NAME_PREFIX.length()), date, true)
		),
		CUSTOM_COLUMN(
				handler -> field -> handler.validateCustomColumnField(
						field.substring(0, field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER)),
						field.substring(field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER) + 1)),
				handler -> field -> handler.getCustomColumnDataType(
						field.substring(0, field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER)),
						field.substring(field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER) + 1)),
				handler -> pos -> field -> date -> handler.getCustomColumnValue(pos,
						field.substring(0, field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER)),
						field.substring(field.indexOf(CUSTOM_COLUMN_NAME_DELIMITER) + 1))
		),
		OPENING_TRADE_PROPERTY(
				handler -> field -> handler.validateOpeningTradeField(field.substring(OPENING_TRADE_PREFIX.length())),
				handler -> field -> PropertyPathType.getDataTypeName(BeanUtils.getPropertyType(Trade.class, field.substring(OPENING_TRADE_PREFIX.length()))),
				handler -> pos -> field -> date -> handler.getOpeningTradePropertyValue(pos, field.substring(OPENING_TRADE_PREFIX.length()))
		),
		TRADE_MARKET_DATA(
				handler -> field -> handler.validateTradeMarketDataField(field.substring(TRADE_MARKET_DATA_FIELD_NAME_PREFIX.length())),
				handler -> field -> DataTypeNames.DECIMAL,
				handler -> pos -> field -> date -> handler.getTradeMarketDataFieldValue(pos, field.substring(TRADE_MARKET_DATA_FIELD_NAME_PREFIX.length()))
		);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private Function<CompliancePositionPropertyPathHandlerImpl, Consumer<String>> validator;
		private Function<CompliancePositionPropertyPathHandlerImpl, Function<String, DataTypeNames>> dataTypeRetriever;
		private Function<CompliancePositionPropertyPathHandlerImpl, Function<AccountingPosition, Function<String, Function<Date, Object>>>> valueRetriever;


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		PropertyPathType(Function<CompliancePositionPropertyPathHandlerImpl, Consumer<String>> validator,
		                 Function<CompliancePositionPropertyPathHandlerImpl, Function<String, DataTypeNames>> dataTypeRetriever,
		                 Function<CompliancePositionPropertyPathHandlerImpl, Function<AccountingPosition, Function<String, Function<Date, Object>>>> valueRetriever) {
			this.validator = validator;
			this.valueRetriever = valueRetriever;
			this.dataTypeRetriever = dataTypeRetriever;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private static PropertyPathType forField(String field) {
			final PropertyPathType type;
			if (field.startsWith(FLEXIBLE_MARKET_DATA_FIELD_NAME_PREFIX)) {
				type = FLEXIBLE_MARKET_DATA;
			}
			else if (field.startsWith(MARKET_DATA_FIELD_NAME_PREFIX)) {
				type = MARKET_DATA;
			}
			else if (field.contains(CUSTOM_COLUMN_NAME_DELIMITER)) {
				type = CUSTOM_COLUMN;
			}
			else if (field.startsWith(TRADE_MARKET_DATA_FIELD_NAME_PREFIX)) {
				type = TRADE_MARKET_DATA;
			}
			else if (field.startsWith(OPENING_TRADE_PREFIX)) {
				type = OPENING_TRADE_PROPERTY;
			}
			else {
				type = BEAN_PROPERTY;
			}
			return type;
		}


		private static DataTypeNames getDataTypeName(Class<?> clazz) {
			if (clazz == Date.class) {
				return DataTypeNames.DATE;
			}
			else if (clazz == String.class) {
				return DataTypeNames.STRING;
			}
			else if (Number.class.isAssignableFrom(clazz)) {
				return DataTypeNames.DECIMAL;
			}
			else if (ClassUtils.primitiveToWrapperIfNecessary(clazz) == Boolean.class) {
				return DataTypeNames.BOOLEAN;
			}
			throw new IllegalArgumentException(String.format("Unexpected data type found during bean property path retrieval. Class [%s] is not supported.", clazz.getName()));
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private void validateField(CompliancePositionPropertyPathHandlerImpl handler, String field) {
			this.validator.apply(handler).accept(field);
		}


		private DataTypeNames getDataTypeForField(CompliancePositionPropertyPathHandlerImpl handler, String field) {
			return this.dataTypeRetriever.apply(handler).apply(field);
		}


		private Object retrieveValue(CompliancePositionPropertyPathHandlerImpl handler, AccountingPosition position, String field, Date date) {
			return this.valueRetriever.apply(handler).apply(position).apply(field).apply(date);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
