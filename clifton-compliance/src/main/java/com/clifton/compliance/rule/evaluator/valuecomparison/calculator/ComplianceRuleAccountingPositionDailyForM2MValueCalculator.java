package com.clifton.compliance.rule.evaluator.valuecomparison.calculator;

import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.position.AccountingPositionTypes;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.context.Context;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The implementation of {@link BaseComplianceRuleAccountingPositionValueCalculator} which produces calculated values for {@link AccountingPositionDaily} entities.
 * <p>
 * This calculator uses a configured {@link #valueField value field} to derive a value for each position. The type of the given value field must be a {@link BigDecimal}.
 * <p>
 * Using {@link AccountingPositionDaily} entities provides a means to retrieve pre-calculated values for positions as well as mark-to-market data.
 *
 * @author MikeH
 */
public class ComplianceRuleAccountingPositionDailyForM2MValueCalculator extends BaseComplianceRuleAccountingPositionValueCalculator<AccountingPositionDaily> implements ValidationAware {

	private AccountingM2MService accountingM2MService;

	private String valueField;
	/**
	 * The configurable calculator to use during real-time evaluations. If this is set and the current evaluation is a real-time evaluation, then the current calculator will
	 * delegate to this configured calculator.
	 * <p>
	 * This calculator may be necessary in cases where real-time evaluation cannot be performed correctly. For example, this calculator is currently unable to generate M2M position
	 * entities when M2M data does not yet exist for the execution date. In this case, a fall-back method may be used.
	 */
	private SystemBean realTimeCalculator;

	/*
	 * These fields are not used and are present for display purposes only. These fields are added to this class only for system bean validation purposes.
	 */
	private Boolean filterOptionsHeader;
	private Boolean calculatorOptionsHeader;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected BigDecimal calculateImplWithCalculateDate(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Resort to configured real-time calculator if necessary
		final ComplianceRuleValueCalculator calculator = (ruleEvaluationConfig.isRealTime() && getRealTimeCalculator() != null)
				? generateRealTimeCalculator(getRealTimeCalculator())
				: super::calculateImplWithCalculateDate;
		return calculator.calculate(ruleEvaluationConfig, runSubDetailList);
	}


	@Override
	protected List<AccountingPositionDaily> getPositionList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Verify that the calculator is not being run for an empty account list (i.e., globally)
		List<Integer> accountIdList = ruleEvaluationConfig.retrieveAccountIdList();
		AssertUtils.assertNotEmpty(accountIdList, "The [%s] calculator is not supported when no account IDs are provided.", this.getClass().getSimpleName());

		// Retrieve M2M position data
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setClientAccountIds(CollectionUtils.toArray(accountIdList, Integer.class));
		searchForm.setInvestmentGroupId(getInvestmentGroupId());
		searchForm.setSnapshotDate(ruleEvaluationConfig.getCalculationDate());
		return getAccountingM2MService().getAccountingPositionDailyLiveForM2MList(searchForm);
	}


	@Override
	protected BigDecimal getPositionValue(AccountingPositionDaily position, Date transactionDate, Context context) {
		return (BigDecimal) BeanUtils.getPropertyValue(position, getValueField());
	}


	@Override
	protected AccountingTransactionInfo getAccountingTransactionInfo(AccountingPositionDaily position) {
		return position.getAccountingTransaction();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		// Validate value field existence
		ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(AccountingPositionDaily.class, getValueField()), () -> String.format("The property [%s] could not be found for objects of type [%s].", getValueField(), AccountingPositionDaily.class.getSimpleName()), "valueField");

		// Validate value field type
		Class<?> valuePropertyType = BeanUtils.getPropertyType(AccountingPositionDaily.class, getValueField());
		ValidationUtils.assertEquals(BigDecimal.class, valuePropertyType, () -> String.format("The selected value field must be numeric. The property [%s] with type [%s] is not of type [%s].", getValueField(), valuePropertyType.getSimpleName(), BigDecimal.class.getSimpleName()), "valueField");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the {@link ComplianceRuleValueCalculator} entity from the given bean. This bean must correspond to the correct entity type.
	 *
	 * @param realTimeCalculatorBean the {@link SystemBean} entity
	 * @return the {@link ComplianceRuleValueCalculator} entity
	 */
	private ComplianceRuleValueCalculator generateRealTimeCalculator(SystemBean realTimeCalculatorBean) {
		return (ComplianceRuleValueCalculator) getSystemBeanService().getBeanInstance(realTimeCalculatorBean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public AccountingPositionTypes getPositionType() {
		return AccountingPositionTypes.ANY_POSITION;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public String getValueField() {
		return this.valueField;
	}


	public void setValueField(String valueField) {
		this.valueField = valueField;
	}


	public SystemBean getRealTimeCalculator() {
		return this.realTimeCalculator;
	}


	public void setRealTimeCalculator(SystemBean realTimeCalculator) {
		this.realTimeCalculator = realTimeCalculator;
	}


	public Boolean getFilterOptionsHeader() {
		return this.filterOptionsHeader;
	}


	public void setFilterOptionsHeader(Boolean filterOptionsHeader) {
		this.filterOptionsHeader = filterOptionsHeader;
	}


	public Boolean getCalculatorOptionsHeader() {
		return this.calculatorOptionsHeader;
	}


	public void setCalculatorOptionsHeader(Boolean calculatorOptionsHeader) {
		this.calculatorOptionsHeader = calculatorOptionsHeader;
	}
}
