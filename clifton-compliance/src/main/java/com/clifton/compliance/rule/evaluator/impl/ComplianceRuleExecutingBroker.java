package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ComplianceRuleExecutingBroker</code> defines which executing brokers are allowed and not allowed
 * for real-time trade.
 * <p>
 * This is very similar to the Short/Long rule.
 *
 * @author apopp
 */
public class ComplianceRuleExecutingBroker implements ComplianceRuleEvaluator {

	private static final String SUCCESS_DESCRIPTION = "All Executing Brokers are approved.";

	private TradeService tradeService;

	private List<Integer> executingBrokerCompanyIdList;
	private boolean inclusion;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		if (!ruleEvaluationConfig.isRealTime()) {
			return evaluateBatch(ruleEvaluationConfig);
		}

		return evaluateRealTime(ruleEvaluationConfig);
	}


	private ComplianceRuleRun evaluateBatch(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountId(ruleEvaluationConfig.getClientAccountId());
		searchForm.setTradeDate(ruleEvaluationConfig.getProcessDate());
		searchForm.setSecurityId(ruleEvaluationConfig.getBean().getInvestmentSecurity().getId());
		searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);

		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setPass(true);
		run.setResultValue(BigDecimal.ZERO);
		run.setPercentOfAssets(BigDecimal.ZERO);

		AccountingPosition pos = null;
		Trade tradeFillTrade = null;
		if (ruleEvaluationConfig.getBean() instanceof AccountingPosition) {
			pos = (AccountingPosition) ruleEvaluationConfig.getBean();
			if (pos.getOpeningTransaction().getJournal().getJournalType().getName().equals(AccountingJournalType.TRADE_JOURNAL)) {
				tradeFillTrade = this.tradeService.getTradeFillForAccountingPosition(pos).getTrade();
			}
		}

		for (Trade trade : CollectionUtils.getIterable(getTradeService().getTradeList(searchForm))) {
			if (trade.getExecutingBrokerCompany() == null) {
				continue;
			}

			ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
			detail.setPass(true);

			boolean equal = getExecutingBrokerCompanyIdList().contains(trade.getExecutingBrokerCompany().getId());

			if (equal) {
				if (!isInclusion()) {
					detail.setPass(false);
				}
			}
			else {
				if (isInclusion()) {
					detail.setPass(false);
				}
			}

			detail.setComplianceRuleRun(run);
			run.addDetail(detail);

			StringBuilder description = new StringBuilder();
			if (pos != null) {
				description.append(pos.getAccountingTransactionId());
				description.append(": Position ");
				if (tradeFillTrade != null && tradeFillTrade.equals(trade)) {
					description.append("source ");
				}
				else {
					description.append("related ");
				}
			}
			description.append("Trade [");
			description.append(trade.getId());
			description.append("] for security [");
			description.append(trade.getInvestmentSecurity().getSymbol());
			description.append("] has ");
			description.append(detail.isPass() ? "allowed" : "disallowed");
			description.append(" executing broker [");
			description.append(trade.getExecutingBrokerCompany().getName());
			description.append("]");

			detail.setDescription(description.toString());

			if (!detail.isPass()) {
				run.setPass(false);
				run.setDescription(detail.getDescription());
			}
		}

		return run;
	}


	public ComplianceRuleRun evaluateRealTime(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setPass(true);

		ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
		detail.setPass(true);
		detail.setComplianceRuleRun(run);
		run.addDetail(detail);

		String result = null;

		if (ruleEvaluationConfig.getBean() instanceof Trade) {
			Trade trade = (Trade) ruleEvaluationConfig.getBean();
			if (trade.getExecutingBrokerCompany() != null) {

				boolean equal = getExecutingBrokerCompanyIdList().contains(trade.getExecutingBrokerCompany().getId());

				if (equal) {
					if (!isInclusion()) {
						result = "Executing Broker [" + trade.getExecutingBrokerCompany().getName() + "] is not approved as it is in the broker exclusion list.";
					}
				}
				else {
					if (isInclusion()) {
						result = "Executing Broker [" + trade.getExecutingBrokerCompany().getName() + "] is not approved in the broker inclusion list.";
					}
				}
			}
		}

		if (result != null) {
			detail.setPass(false);
			run.setPass(false);

			detail.setDescription(result);
			run.setDescription(result);
		}
		else {
			detail.setDescription(SUCCESS_DESCRIPTION);
			run.setDescription(SUCCESS_DESCRIPTION);
		}
		return run;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isInclusion() {
		return this.inclusion;
	}


	public void setInclusion(boolean inclusion) {
		this.inclusion = inclusion;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public List<Integer> getExecutingBrokerCompanyIdList() {
		return this.executingBrokerCompanyIdList;
	}


	public void setExecutingBrokerCompanyIdList(List<Integer> executingBrokerCompanyIdList) {
		this.executingBrokerCompanyIdList = executingBrokerCompanyIdList;
	}
}
