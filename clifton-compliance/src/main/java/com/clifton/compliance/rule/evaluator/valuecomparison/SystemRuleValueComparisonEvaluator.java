package com.clifton.compliance.rule.evaluator.valuecomparison;

import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SystemRuleValueComparisonEvaluator</code> is the generic rule evaluator for value comparison rules.
 * <p>
 * This rule evaluator differs from {@link ComplianceRuleValueComparisonEvaluator} in that it is used for <i>system rule</i> evaluations. This allows this evaluator to be used in
 * triggerable evaluations via the primary rules engine, such as "Trade Rules", for example.
 *
 * @author MikeH
 */
public class SystemRuleValueComparisonEvaluator<T extends IdentityObject & AccountingBean> extends BaseRuleValueComparisonEvaluator implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private ComplianceRuleAccountHandler complianceRuleAccountHandler;
	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private RuleViolationService ruleViolationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T ruleBean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			// Prepare and evaluate rule
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(getComplianceRuleAccountHandler(), getComplianceRulePositionHandler(), getInvestmentAccountService(), getInvestmentInstrumentService());
			config.setBean(ruleBean);
			config.setClientAccountId(ruleBean.getClientInvestmentAccount().getId());
			config.setInvestmentSecurityId(ruleBean.getInvestmentSecurity().getId());
			config.setProcessDate(ruleBean.getTransactionDate());
			config.setRealTime(true);
			config.setTradeRuleEvaluatorContext(context);
			ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
			assignment.setClientInvestmentAccount(ruleBean.getClientInvestmentAccount());
			config.setRuleAssignment(assignment);
			try {
				ComplianceRuleRun run = evaluateRule(config);
				// Process rule results
				if (run.getStatus() != null && run.getStatus() == ComplianceRuleRun.STATUS_FAILED) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put(VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, run.getDescription() + " : Failed");
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, ruleBean.getIdentity(), ruleBean.getSourceEntityId(), null, templateValues));
				}
			}
			catch (Exception e) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put(VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, String.format("Error: [%s]", e.getMessage()));
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, ruleBean.getIdentity(), ruleBean.getSourceEntityId(), null, templateValues));
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public void setComplianceRuleAccountHandler(ComplianceRuleAccountHandler complianceRuleAccountHandler) {
		this.complianceRuleAccountHandler = complianceRuleAccountHandler;
	}


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
