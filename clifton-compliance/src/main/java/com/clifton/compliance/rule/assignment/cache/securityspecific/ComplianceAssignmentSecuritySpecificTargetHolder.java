package com.clifton.compliance.rule.assignment.cache.securityspecific;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.cache.BaseComplianceAssignmentTargetHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.key.AbstractSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;

import java.util.Objects;


/**
 * The <i>target type</i> for the {@link ComplianceAssignmentSecuritySpecificSpecificityCache} specificity cache.
 * <p>
 * This target type contains relevant information for specificity evaluations and for retrieval for rule assignment entities.
 *
 * @author MikeH
 */
public class ComplianceAssignmentSecuritySpecificTargetHolder extends BaseComplianceAssignmentTargetHolder {

	private final InvestmentTypeSubType subType;
	private final InvestmentTypeSubType2 subType2;
	private final InvestmentInstrument underlyingInstrument;
	private final InvestmentCurrencyTypes currencyType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceAssignmentSecuritySpecificTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment, AbstractSpecificityKeyGenerator generator) {
		// The rule type ID is used as the scope ID since only a single applicable assignment may precipitate per rule type
		super(rule, assignment, generateKey(rule, assignment, generator), rule.getRuleType().getId());
		this.subType = rule.getInvestmentTypeSubType();
		this.subType2 = rule.getInvestmentTypeSubType2();
		this.underlyingInstrument = rule.getUnderlyingInstrument();
		this.currencyType = rule.getCurrencyType();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static String generateKey(ComplianceRule rule, ComplianceRuleAssignment assignment, AbstractSpecificityKeyGenerator generator) {
		return generator.generateKeyForTarget(SpecificityKeySource
				.builder(BeanUtils.getBeanIdentity(rule.getInvestmentInstrument()), BeanUtils.getBeanIdentity(rule.getInvestmentHierarchy()), null)
				.addHierarchy(BeanUtils.getBeanIdentity(rule.getUnderlyingInstrument()), null)
				.addHierarchy(BeanUtils.getBeanIdentity(rule.getInvestmentTypeSubType2()), null)
				.addHierarchy(BeanUtils.getBeanIdentity(rule.getInvestmentTypeSubType()), null)
				.addHierarchy(BeanUtils.getBeanIdentity(rule.getInvestmentType()), null)
				.addHierarchy(BeanUtils.getBeanIdentity(assignment.getClientInvestmentAccount()), BeanUtils.getBeanIdentity(assignment.getBusinessService()), null)
				.addHierarchy(rule.getCurrencyType() != InvestmentCurrencyTypes.BOTH ? rule.getCurrencyType() : null, null)
				.build());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int hashCode() {
		return Objects.hash(
				super.hashCode(),
				this.currencyType
		);
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		ComplianceAssignmentSecuritySpecificTargetHolder that = (ComplianceAssignmentSecuritySpecificTargetHolder) o;
		return this.currencyType == that.currencyType;
	}


	@Override
	public String toString() {
		return "ComplianceAssignmentSecuritySpecificTargetHolder{" +
				"subType=" + this.subType +
				", subType2=" + this.subType2 +
				", underlyingInstrument=" + this.underlyingInstrument +
				", currencyType=" + this.currencyType +
				"} " + super.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeSubType getSubType() {
		return this.subType;
	}


	public InvestmentTypeSubType2 getSubType2() {
		return this.subType2;
	}


	public InvestmentInstrument getUnderlyingInstrument() {
		return this.underlyingInstrument;
	}


	public InvestmentCurrencyTypes getCurrencyType() {
		return this.currencyType;
	}
}
