package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.util.Date;


/**
 * The <code>ComplianceRuleDaysToSecurityMaturity</code> allows to restrict
 * trades or positions to securities that mature before or after the specified number of days.
 *
 * @author apopp
 */
public class ComplianceRuleDaysToSecurityMaturity implements ComplianceRuleEvaluator, ValidationAware {

	private Integer minDaysToMaturity;
	private Integer maxDaysToMaturity;


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(getMinDaysToMaturity() != null || getMaxDaysToMaturity() != null, "Min or Max Days to Maturity is Required.");
		ValidationUtils.assertTrue((getMinDaysToMaturity() == null || MathUtils.isGreaterThanOrEqual(getMinDaysToMaturity(), 0)) && (getMaxDaysToMaturity() == null || MathUtils.isGreaterThanOrEqual(getMaxDaysToMaturity(), 0)), "Min/Max days must be greater than zero if specified.");
		if (getMinDaysToMaturity() != null && getMaxDaysToMaturity() != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(getMaxDaysToMaturity(), getMinDaysToMaturity()), "Max days must be greater than or Equal to Min days.");
		}
	}


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return evaluateRealTime(ruleEvaluationConfig);
	}


	public ComplianceRuleRun evaluateRealTime(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		ComplianceRuleRun run = new ComplianceRuleRun();
		ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
		detail.setComplianceRuleRun(run);
		run.addDetail(detail);

		boolean pass = false;
		String description = "";

		InvestmentSecurity security = ruleEvaluationConfig.getBean().getInvestmentSecurity();
		Date maturityDate = security.getEndDate();

		if (maturityDate == null) {
			pass = true;
			description = "Maturity Date is NULL for Security [" + security.getLabel() + "]";
		}
		else {
			String violationMessage = "";

			int daysDiff = Math.abs(DateUtils.getDaysDifference(maturityDate, ruleEvaluationConfig.getProcessDate()));
			description += "Days to Maturity for Security [" + security.getLabel() + "] is [" + daysDiff + "]";

			if (getMaxDaysToMaturity() != null && MathUtils.isGreaterThan(daysDiff, getMaxDaysToMaturity())) {
				violationMessage += " Days which Violates Rule Max [" + getMaxDaysToMaturity() + "]";
			}

			if (getMinDaysToMaturity() != null && MathUtils.isGreaterThan(getMinDaysToMaturity(), daysDiff)) {
				violationMessage += " Days which Violates Rule Min [" + getMinDaysToMaturity() + "]";
			}

			if (StringUtils.isEmpty(violationMessage)) {
				pass = true;
			}
			else {
				description += violationMessage;
			}
		}

		detail.setPass(pass);
		run.setPass(pass);
		detail.setDescription(description);
		run.setDescription(description);

		return run;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getMinDaysToMaturity() {
		return this.minDaysToMaturity;
	}


	public void setMinDaysToMaturity(Integer minDaysToMaturity) {
		this.minDaysToMaturity = minDaysToMaturity;
	}


	public Integer getMaxDaysToMaturity() {
		return this.maxDaysToMaturity;
	}


	public void setMaxDaysToMaturity(Integer maxDaysToMaturity) {
		this.maxDaysToMaturity = maxDaysToMaturity;
	}
}
