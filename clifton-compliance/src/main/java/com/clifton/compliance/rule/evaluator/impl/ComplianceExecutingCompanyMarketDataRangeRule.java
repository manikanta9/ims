package com.clifton.compliance.rule.evaluator.impl;

import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author MitchellF
 */
public class ComplianceExecutingCompanyMarketDataRangeRule implements ComplianceRuleEvaluator, ValidationAware {

	private List<Integer> executingCompanyIds;
	private Integer benchmarkSecurityId;
	private Short marketDataFieldId;

	private BigDecimal minValue;
	private BigDecimal maxValue;

	private boolean flexibleMarketDataLookup;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataRetriever marketDataRetriever;
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (getMinValue() == null && getMaxValue() == null) {
			throw new ValidationException("At least one of [minValue, maxValue] must be defined.");
		}
		if (getMinValue() != null && getMaxValue() != null && MathUtils.isGreaterThan(getMinValue(), getMaxValue())) {
			throw new ValidationException("Minimum value must be less than maximum value.");
		}
	}


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {

		ComplianceRuleRun complianceRuleRun = new ComplianceRuleRun();
		complianceRuleRun.setPass(true);

		AccountingBean bean = ruleEvaluationConfig.getBean();

		BusinessCompany executingCompany;
		if (bean instanceof Trade) {
			executingCompany = ((Trade) bean).getExecutingBrokerCompany();
		}
		else if (bean instanceof AccountingPosition) {
			executingCompany = ((AccountingPosition) bean).getExecutingCompany();
		}
		else {
			return complianceRuleRun;
		}

		if (CollectionUtils.contains(this.executingCompanyIds, executingCompany.getId())) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(getBenchmarkSecurityId());
			ValidationUtils.assertNotNull(security, "Invalid benchmark security id provided: " + getBenchmarkSecurityId());
			MarketDataField field = getMarketDataFieldService().getMarketDataField(getMarketDataFieldId());
			ValidationUtils.assertNotNull(field, "Invalid Market Data Field Id provided: " + getMarketDataFieldId());
			String fieldName = field.getName();
			BigDecimal marketDataValue = isFlexibleMarketDataLookup() ?
					getMarketDataRetriever().getSecurityDataFieldValueFlexible(security, ruleEvaluationConfig.getProcessDate(), fieldName, true) :
					getMarketDataRetriever().getSecurityDataFieldValue(security, ruleEvaluationConfig.getProcessDate(), fieldName, true);
			StringBuilder description = getDescriptionPrefix(bean, security, field);
			if (getMinValue() != null && getMaxValue() != null && !MathUtils.isBetween(marketDataValue, getMinValue(), getMaxValue())) {
				description.append("[")
						.append(CoreMathUtils.formatNumberDecimal(marketDataValue))
						.append("], which is outside the range [")
						.append(CoreMathUtils.formatNumberDecimal(getMinValue()))
						.append(", ")
						.append(CoreMathUtils.formatNumberDecimal(getMaxValue()))
						.append("].");
			}
			else if (getMinValue() != null && !MathUtils.isGreaterThanOrEqual(marketDataValue, getMinValue())) {
				description.append("[")
						.append(CoreMathUtils.formatNumberDecimal(marketDataValue))
						.append("], which is less than the minimum value [")
						.append(CoreMathUtils.formatNumberDecimal(getMinValue()))
						.append("].");
			}
			else if (getMaxValue() != null && !MathUtils.isLessThanOrEqual(marketDataValue, getMaxValue())) {
				description.append("[")
						.append(CoreMathUtils.formatNumberDecimal(marketDataValue))
						.append("], which is greater than the maximum value [")
						.append(CoreMathUtils.formatNumberDecimal(getMaxValue()))
						.append("].");
			}
			else {
				return complianceRuleRun;
			}
			ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
			detail.setComplianceRuleRun(complianceRuleRun);
			detail.setDescription(description.toString());
			detail.setPass(false);

			complianceRuleRun.setDescription(description.toString());
			complianceRuleRun.addDetail(detail);
			complianceRuleRun.setPass(false);
		}

		return complianceRuleRun;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StringBuilder getDescriptionPrefix(AccountingBean bean, InvestmentSecurity security, MarketDataField dataField) {
		StringBuilder description = new StringBuilder();
		if (bean instanceof Trade) {
			description.append(((Trade) bean).getId());
		}
		else if (bean instanceof AccountingPosition) {
			description.append(((AccountingPosition) bean).getId());
		}
		description.append(": Market data field [")
				.append(dataField.getName())
				.append("] for security [")
				.append(security.getSymbol())
				.append("] on ")
				.append(DateUtils.fromDateShort(bean.getTransactionDate()))
				.append(" is ");

		return description;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Integer> getExecutingCompanyIds() {
		return this.executingCompanyIds;
	}


	public void setExecutingCompanyIds(List<Integer> executingCompanyIds) {
		this.executingCompanyIds = executingCompanyIds;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public BigDecimal getMinValue() {
		return this.minValue;
	}


	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}


	public BigDecimal getMaxValue() {
		return this.maxValue;
	}


	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}


	public boolean isFlexibleMarketDataLookup() {
		return this.flexibleMarketDataLookup;
	}


	public void setFlexibleMarketDataLookup(boolean flexibleMarketDataLookup) {
		this.flexibleMarketDataLookup = flexibleMarketDataLookup;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
