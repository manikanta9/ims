package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentGroupConcentrationRule</code> allows us to set a maximum allowed investment in a certain
 * investment grouping.  If this rule is configured against TIPs, it will aggregate the total amount invested in TIPs
 * and compare it to the funds total market value.
 * <p>
 * This way we can easily know how much percentage of our fund is invested in a certain investment group.
 *
 * @author apopp
 */
public class InvestmentGroupConcentrationRule extends BaseComplianceRuleRatioEvaluator {

	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private InvestmentGroupService investmentGroupService;

	private boolean excludeCashCollateral;
	private boolean useNotionalValueOfPositions;

	private Short investmentGroupId;

	/**
	 * Defines a type of an instrument: Stock, Future, Bond, etc.
	 */
	private Short investmentTypeId;

	/**
	 * A sub type classification for similar instruments falling under different investment types.
	 * <p>
	 * Examples, Total Return Swaps, Interest Rate Swaps, Credit Default Swaps.
	 */
	private Short investmentTypeSubTypeId;

	/**
	 * Defines a hierarchical structure for instruments
	 * <p>
	 * Example: Fixed Income / Futures, Options, Swaps; Equity / Futures, Options; Currency / Futures, Options; etc.
	 */
	private Short investmentInstrumentHierarchyId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String groupName = "Missing group";
		InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupId());

		if (group != null) {
			groupName = group.getName();
		}

		return groupName + " Bucket";
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();

		ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
		config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
		config.setProcessDate(ruleEvaluationConfig.getProcessDate());
		config.setIncludeCashCollateral(!isExcludeCashCollateral());
		config.setTradeRuleEvaluatorContext(ruleEvaluationConfig.getTradeRuleEvaluatorContext());

		configList.add(config);

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRulePosition> positionList = ruleEvaluationConfig.getPositionList();

		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(positionList);
		applyRuleFilters(securityIdList);

		List<ComplianceRulePosition> filteredPositionList = CollectionUtils.getFiltered(positionList, position -> securityIdList.contains(position.getRealSecurityOrPseudo().getId()));

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ComplianceUtils.mapPositionListBySecurity(filteredPositionList), ruleEvaluationConfig.getProcessDate(),
				(isUseNotionalValueOfPositions()) ? ComplianceRulePositionValues.NOTIONAL_VALUE : ComplianceRulePositionValues.MARKET_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getComplianceRulePositionHandler().getAccountTotalValue(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ruleEvaluationConfig.getPositionList(), ruleEvaluationConfig.getProcessDate(),
				(isUseNotionalValueOfPositions()) ? ComplianceRulePositionValues.NOTIONAL_VALUE : ComplianceRulePositionValues.MARKET_VALUE, ruleEvaluationConfig.retrieveAccountIdList(),
				isUseTotalAssets(), isExcludeCashCollateral());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void applyRuleFilters(List<Integer> securityIdList) {
		getComplianceRuleFilterHandler().filterByInvestmentGroup(securityIdList, getInvestmentGroupId(), true);
		getComplianceRuleFilterHandler().filterByInvestmentType(securityIdList, getInvestmentTypeId(), true);
		getComplianceRuleFilterHandler().filterByInvestmentTypeSubType(securityIdList, getInvestmentTypeSubTypeId(), true);
		getComplianceRuleFilterHandler().filterByInvestmentHierarchy(securityIdList, getInvestmentInstrumentHierarchyId(), true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public boolean isExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}


	public boolean isUseNotionalValueOfPositions() {
		return this.useNotionalValueOfPositions;
	}


	public void setUseNotionalValueOfPositions(boolean useNotionalValueOfPositions) {
		this.useNotionalValueOfPositions = useNotionalValueOfPositions;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentInstrumentHierarchyId() {
		return this.investmentInstrumentHierarchyId;
	}


	public void setInvestmentInstrumentHierarchyId(Short investmentInstrumentHierarchyId) {
		this.investmentInstrumentHierarchyId = investmentInstrumentHierarchyId;
	}
}
