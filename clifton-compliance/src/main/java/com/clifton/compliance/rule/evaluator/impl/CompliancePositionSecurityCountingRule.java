package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.filter.ComplianceRuleFilterHandler;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>CompliancePositionSecurityCountingRule</code> counts unique securities under an account given various filters.
 * <p>
 * It's pass/fail status depends on a specified range where a min, max, or both is specified.
 *
 * @author apopp
 */
public class CompliancePositionSecurityCountingRule implements ComplianceRuleEvaluator {

	private ComplianceRuleFilterHandler complianceRuleFilterHandler;
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	private InvestmentGroupService investmentGroupService;

	/*
	 * Evaluated securities/instruments must fall under this group if specified
	 */
	private Short investmentGroupId;

	/*
	 * Minimum amount of securities we can be invested in
	 */
	private Integer minCount;

	/*
	 * Maximum amount of securities we can be invested in
	 */
	private Integer maxCount;

	/*
	 * Roll up account positions by security or by underlying instrument
	 */
	private Boolean countUnderlying;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		Map<? extends NamedObject, List<ComplianceRulePosition>> positionListMap = ComplianceUtils.mapPositionListBySecurityOrUnderlying(ruleEvaluationConfig.getPositionList(), getCountUnderlying());

		getComplianceRuleFilterHandler().filterByInvestmentGroup(positionListMap, getInvestmentGroupId(), getCountUnderlying());

		/*
		 * Remove entries for which we have zero exposure
		 */
		positionListMap = ComplianceUtils.filterPositionMapByExposure(positionListMap);

		int count = positionListMap.size();

		/*
		 * Evaluate result of rule
		 */
		boolean success = MathUtils.isBetween(count, getMinCount(), getMaxCount());

		/*
		 * Build run report
		 */
		ComplianceRuleRunDetail detail = getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), positionListMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE);
		detail.setDescription("Totals Bucket -" + buildCountString());
		detail.setResultValue(new BigDecimal(count));
		detail.setPass(success);

		return getComplianceRuleReportHandler().createBasicRuleRun(buildCountString(), new BigDecimal(count), success, CollectionUtils.createList(detail));
	}


	private String buildCountString() {
		String message = "";

		if (getMinCount() != null) {
			message = message + " Minimum: " + getMinCount();
		}

		if (getMaxCount() != null) {
			message = message + " Maximum: " + getMaxCount();
		}

		return message;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleFilterHandler getComplianceRuleFilterHandler() {
		return this.complianceRuleFilterHandler;
	}


	public void setComplianceRuleFilterHandler(ComplianceRuleFilterHandler complianceRuleFilterHandler) {
		this.complianceRuleFilterHandler = complianceRuleFilterHandler;
	}


	public ComplianceRuleReportHandler getComplianceRuleReportHandler() {
		return this.complianceRuleReportHandler;
	}


	public void setComplianceRuleReportHandler(ComplianceRuleReportHandler complianceRuleReportHandler) {
		this.complianceRuleReportHandler = complianceRuleReportHandler;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getMinCount() {
		return this.minCount;
	}


	public void setMinCount(Integer minCount) {
		this.minCount = minCount;
	}


	public Integer getMaxCount() {
		return this.maxCount;
	}


	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}


	public Boolean getCountUnderlying() {
		return this.countUnderlying;
	}


	public void setCountUnderlying(Boolean countUnderlying) {
		this.countUnderlying = countUnderlying;
	}
}
