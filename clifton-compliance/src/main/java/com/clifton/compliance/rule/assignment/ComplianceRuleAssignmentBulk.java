package com.clifton.compliance.rule.assignment;


/**
 * The <code>ComplianceRuleAssignmentBulk</code> class is used to simplify creation of multiple
 * rule assignments for the same rule.
 *
 * @author apopp
 */
public class ComplianceRuleAssignmentBulk extends ComplianceRuleAssignment {

	private Integer[] clientAccountIds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}
}
