package com.clifton.compliance.rule.evaluator.valuecomparison;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleValueCalculator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.ComparisonTypes;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * The {@link BaseRuleValueComparisonEvaluator} type houses rule evaluation logic for performing validation based on a logical comparison of values. Instances of this evaluator may
 * be used to compare two collections of account values.
 * <p>
 * The types of account values used for this rule may vary based on the selected {@link ComplianceRuleValueCalculator calculators}. For example, a calculator may derive the market
 * value or the notional value for account positions. By combining these calculators, two accumulated values can be created (via {@link #firstValueBeanList} and {@link
 * #secondValueBeanList}). This evaluator will then compare these two accumulated values using the selected {@link #comparisonType comparison evaluator} and produce a pass or
 * fail result based on the comparison results.
 *
 * @author MikeH
 */
public abstract class BaseRuleValueComparisonEvaluator implements ComplianceRuleEvaluator, ValidationAware {

	private SystemBeanService systemBeanService;
	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ComparisonTypes comparisonType;
	private List<SystemBean> firstValueBeanList;
	private List<SystemBean> secondValueBeanList;
	private BigDecimal firstValueMultiplier;
	private BigDecimal secondValueMultiplier;

	// Message options
	private String successMessage; // supports FreeMarket template with firstValue and secondValue variables
	private String failureMessage; // supports FreeMarket template with firstValue and secondValue variables

	private List<SystemBean> criticalMessageSelectorBeanList;
	private String criticalMessageSelectorFilter;
	private boolean useCriticalMessagesForFailures;

	/*
	 * This field is not used and is present for display purposes only. This field is added to this class only for system bean validation purposes.
	 */
	private Boolean messageOptionsHeader;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		ComplianceRuleRunDetail runDetail = new ComplianceRuleRunDetail();

		// Get first value
		ComplianceRuleRunSubDetail firstValueRunSubDetail = new ComplianceRuleRunSubDetail();
		BigDecimal firstValueRaw = calculateValue(ruleEvaluationConfig, getFirstValueBeanList(), firstValueRunSubDetail);
		BigDecimal firstValue = getFirstValueMultiplier().multiply(firstValueRaw);
		firstValueRunSubDetail.setDescription("First Value");
		firstValueRunSubDetail.setResultValue(firstValue);
		runDetail.addSubDetail(firstValueRunSubDetail);

		// Get second value
		ComplianceRuleRunSubDetail secondValueRunSubDetail = new ComplianceRuleRunSubDetail();
		BigDecimal secondValueRaw = calculateValue(ruleEvaluationConfig, getSecondValueBeanList(), secondValueRunSubDetail);
		BigDecimal secondValue = getSecondValueMultiplier().multiply(secondValueRaw);
		secondValueRunSubDetail.setDescription("Second Value");
		secondValueRunSubDetail.setResultValue(secondValue);
		runDetail.addSubDetail(secondValueRunSubDetail);

		// Compare values
		boolean pass = getComparisonType().compare(firstValue, secondValue);

		// Generate result messages
		ComplianceRuleRun run = new ComplianceRuleRun();
		ComplianceRuleRunDetail criticalMessageDetail = pass ? null : getCriticalMessageDetail(runDetail);
		if (criticalMessageDetail != null) {
			run.addDetail(criticalMessageDetail);
		}
		String resultDescription = getResultDescription(pass, firstValue, secondValue, criticalMessageDetail);

		// Attach result data
		BigDecimal valueDifference = MathUtils.subtract(firstValue, secondValue);
		runDetail.setDescription(resultDescription);
		runDetail.setPass(pass);
		runDetail.setResultValue(valueDifference);
		runDetail.setComplianceRuleRun(run);
		run.setDescription(resultDescription);
		run.setPass(pass);
		run.setResultValue(valueDifference);
		run.addDetail(runDetail);
		return run;
	}


	@Override
	public void validate() throws ValidationException {
		// Validate beans are of the correct type
		Class<?> requiredClass = ComplianceRuleValueCalculator.class;
		for (SystemBean bean : CollectionUtils.getIterable(getFirstValueBeanList())) {
			Class<?> clazz = CoreClassUtils.getClass(bean.getType().getClassName());
			ValidationUtils.assertTrue(requiredClass.isAssignableFrom(clazz),
					() -> String.format("The bean [%s] is not of the required type: [%s].", bean.getLabel(), requiredClass.getName()),
					"firstValueBeanList");
		}
		for (SystemBean bean : CollectionUtils.getIterable(getSecondValueBeanList())) {
			Class<?> clazz = CoreClassUtils.getClass(bean.getType().getClassName());
			ValidationUtils.assertTrue(requiredClass.isAssignableFrom(clazz),
					() -> String.format("The bean [%s] is not of the required type: [%s].", bean.getLabel(), requiredClass.getName()),
					"secondValueBeanList");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates an account value at the provided snapshot date using the given configuration object and the given list of calculator beans.
	 * <p>
	 * The result of this method will be the accumulation of the results from each of the given calculators.
	 *
	 * @param ruleEvaluationConfig the configuration containing the data which shall be passed to the calculator
	 * @param calculatorBeanList   the list of calculator beans with which to evaluate each position
	 * @param runSubDetail         the rule run detail entity to which evaluation details should be added
	 * @return the accumulation of all calculated values
	 */
	private BigDecimal calculateValue(ComplianceRuleEvaluationConfig ruleEvaluationConfig, List<SystemBean> calculatorBeanList, ComplianceRuleRunSubDetail runSubDetail) {
		return CollectionUtils.getStream(calculatorBeanList)
				.map(calculatorBean -> {
					// Calculate value
					ComplianceRuleValueCalculator calculator = (ComplianceRuleValueCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
					List<ComplianceRuleRunSubDetail> calculatorSubDetailList = new ArrayList<>();
					BigDecimal value = calculator.calculate(ruleEvaluationConfig, calculatorSubDetailList);
					// Add rule run details
					ComplianceRuleRunSubDetail calculatorSubDetail = ComplianceRuleRunSubDetail.create(calculatorBean.getLabelShort(), value, null, calculatorSubDetailList);
					calculatorSubDetail.setCauseEntity(calculatorBean);
					runSubDetail.addSubDetail(calculatorSubDetail);
					return value;
				})
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the "Critical Message" sub-detail based on messages from {@link #criticalMessageSelectorBeanList selected transformations}, or <code>null</code> if no suitable
	 * messages exist.
	 */
	private ComplianceRuleRunDetail getCriticalMessageDetail(ComplianceRuleRunDetail detail) {
		// Guard-clause: No critical message detail if no selector exists
		if (CollectionUtils.isEmpty(getCriticalMessageSelectorBeanList())) {
			return null;
		}

		// Get selected transformations
		List<ComplianceRuleRunSubDetail> selectedTransformationDetailList = CollectionUtils.getConvertedFlattened(detail.getChildren(), this::getDescendantSubDetailListForSelector);

		// Get messages from selected transformations
		Pattern filterPattern = StringUtils.getGlobPattern(StringUtils.coalesce(getCriticalMessageSelectorFilter(), "*"));
		List<ComplianceRuleRunSubDetail> criticalMessageSubDetailList = selectedTransformationDetailList.stream()
				.flatMap(subDetail -> CollectionUtils.getStream(subDetail.getChildren()))
				.filter(subDetail -> subDetail.getLabel().startsWith("Messages (Size: ")) // Extract "Messages" sub-detail
				.flatMap(subDetail -> CollectionUtils.getStream(subDetail.getChildren()))
				.filter(messageSubDetail -> filterPattern == null || filterPattern.matcher(messageSubDetail.getDescription()).matches()) // Filter to qualifying messages
				.collect(Collectors.toList());

		// Generate critical message detail
		final ComplianceRuleRunDetail criticalMessageDetail;
		if (criticalMessageSubDetailList.isEmpty()) {
			criticalMessageDetail = null;
		}
		else {
			criticalMessageDetail = new ComplianceRuleRunDetail();
			criticalMessageDetail.setComplianceRuleRun(detail.getComplianceRuleRun());
			criticalMessageDetail.setDescription(String.format("Critical Messages (Size: %d)", criticalMessageSubDetailList.size()));
			criticalMessageDetail.setChildren(criticalMessageSubDetailList);
		}
		return criticalMessageDetail;
	}


	/**
	 * Recursively retrieves all sub-details which match any of the configured {@link #criticalMessageSelectorBeanList selectors}.
	 */
	private List<ComplianceRuleRunSubDetail> getDescendantSubDetailListForSelector(ComplianceRuleRunSubDetail subDetail) {
		return isSelectedTransformationDetail(subDetail)
				? Collections.singletonList(subDetail)
				: CollectionUtils.getConvertedFlattened(subDetail.getChildren(), this::getDescendantSubDetailListForSelector);
	}


	@SuppressWarnings("SuspiciousMethodCalls")
	private boolean isSelectedTransformationDetail(ComplianceRuleRunSubDetail subDetail) {
		return subDetail.getCauseEntity() instanceof SystemBean && getCriticalMessageSelectorBeanList().contains(subDetail.getCauseEntity());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the description to use for the provided run results.
	 */
	private String getResultDescription(boolean pass, BigDecimal firstValue, BigDecimal secondValue, ComplianceRuleRunDetail criticalMessageDetail) {
		final String resultDescription;
		if (pass) {
			// Success result description
			if (getSuccessMessage() != null) {
				resultDescription = getConvertedMessage(this::getSuccessMessage, firstValue, secondValue);
			}
			else {
				resultDescription = getDefaultResultDescription(firstValue, secondValue);
			}
		}
		else {
			// Failure result description
			if (criticalMessageDetail != null && isUseCriticalMessagesForFailures()) {
				resultDescription = getCriticalMessageResultDescription(criticalMessageDetail);
			}
			else if (getFailureMessage() != null) {
				resultDescription = getConvertedMessage(this::getFailureMessage, firstValue, secondValue);
			}
			else {
				resultDescription = getDefaultResultDescription(firstValue, secondValue);
			}
		}
		return resultDescription;
	}


	private String getConvertedMessage(Supplier<String> freeMarkerTemplateSupplier, BigDecimal firstValue, BigDecimal secondValue) {
		TemplateConfig config = new TemplateConfig(freeMarkerTemplateSupplier.get());
		config.addBeanToContext("firstValue", firstValue);
		config.addBeanToContext("secondValue", secondValue);
		return getTemplateConverter().convert(config);
	}


	private String getDefaultResultDescription(BigDecimal firstValue, BigDecimal secondValue) {
		return String.format("[%s] %s [%s]", CoreMathUtils.formatNumberMoney(firstValue), getComparisonType(), CoreMathUtils.formatNumberMoney(secondValue));
	}


	private String getCriticalMessageResultDescription(ComplianceRuleRunDetail criticalMessageDetail) {
		final String resultDescription;
		if (CollectionUtils.getSize(criticalMessageDetail.getChildren()) == 1) {
			resultDescription = CollectionUtils.getFirstElementStrict(criticalMessageDetail.getChildren()).getDescription();
		}
		else if (CollectionUtils.getSize(criticalMessageDetail.getChildren()) > 1) {
			resultDescription = String.format("%s (and %d more...)", CollectionUtils.getFirstElementStrict(criticalMessageDetail.getChildren()).getDescription(), criticalMessageDetail.getChildren().size() - 1);
		}
		else {
			throw new IllegalStateException("No critical messages were found while attempting to generate a critical message result description.");
		}
		return resultDescription;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public ComparisonTypes getComparisonType() {
		return this.comparisonType;
	}


	public void setComparisonType(ComparisonTypes comparisonType) {
		this.comparisonType = comparisonType;
	}


	public List<SystemBean> getFirstValueBeanList() {
		return this.firstValueBeanList;
	}


	public void setFirstValueBeanList(List<SystemBean> firstValueBeanList) {
		this.firstValueBeanList = firstValueBeanList;
	}


	public List<SystemBean> getSecondValueBeanList() {
		return this.secondValueBeanList;
	}


	public void setSecondValueBeanList(List<SystemBean> secondValueBeanList) {
		this.secondValueBeanList = secondValueBeanList;
	}


	public BigDecimal getFirstValueMultiplier() {
		return this.firstValueMultiplier;
	}


	public void setFirstValueMultiplier(BigDecimal firstValueMultiplier) {
		this.firstValueMultiplier = firstValueMultiplier;
	}


	public BigDecimal getSecondValueMultiplier() {
		return this.secondValueMultiplier;
	}


	public void setSecondValueMultiplier(BigDecimal secondValueMultiplier) {
		this.secondValueMultiplier = secondValueMultiplier;
	}


	public String getSuccessMessage() {
		return this.successMessage;
	}


	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}


	public String getFailureMessage() {
		return this.failureMessage;
	}


	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}


	public List<SystemBean> getCriticalMessageSelectorBeanList() {
		return this.criticalMessageSelectorBeanList;
	}


	public void setCriticalMessageSelectorBeanList(List<SystemBean> criticalMessageSelectorBeanList) {
		this.criticalMessageSelectorBeanList = criticalMessageSelectorBeanList;
	}


	public String getCriticalMessageSelectorFilter() {
		return this.criticalMessageSelectorFilter;
	}


	public void setCriticalMessageSelectorFilter(String criticalMessageSelectorFilter) {
		this.criticalMessageSelectorFilter = criticalMessageSelectorFilter;
	}


	public boolean isUseCriticalMessagesForFailures() {
		return this.useCriticalMessagesForFailures;
	}


	public void setUseCriticalMessagesForFailures(boolean useCriticalMessagesForFailures) {
		this.useCriticalMessagesForFailures = useCriticalMessagesForFailures;
	}


	public Boolean getMessageOptionsHeader() {
		return this.messageOptionsHeader;
	}


	public void setMessageOptionsHeader(Boolean messageOptionsHeader) {
		this.messageOptionsHeader = messageOptionsHeader;
	}
}
