package com.clifton.compliance.rule.limit;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>ComplianceRuleLimitType</code> represents rule execution limit types.
 * <p/>
 * Ex. MAXIMUM, MINIMUM
 *
 * @author apopp
 */
public class ComplianceRuleLimitType extends NamedEntity<Short> {
	//LimitTypeName and LimitTypeDescription
}
