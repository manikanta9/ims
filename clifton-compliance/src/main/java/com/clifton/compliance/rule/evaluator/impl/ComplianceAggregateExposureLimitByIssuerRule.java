package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ComplianceAggregateExposureLimitByIssuerRule</code> specifies MIN or MAX limit for the
 * specified securities (either individually or as a basket) that cannot be exceeded.
 *
 * @author stevenf
 */
public class ComplianceAggregateExposureLimitByIssuerRule implements ComplianceRuleEvaluator, ValidationAware {

	private AccountingPositionHandler accountingPositionHandler;

	/**
	 * Only one of the following limits can/will be specified for a single rule type.
	 */
	private BigDecimal marketValueLimit;
	private BigDecimal notionalLimit;
	private BigDecimal quantityLimit;

	private boolean groupByUltimateIssuer;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertMutuallyExclusive("One and only one of either Market Value, Notional, or Quantity Limit may be specified.",
				getMarketValueLimit(), getNotionalLimit(), getQuantityLimit());
	}


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {

		String valueName = "";
		BigDecimal aggregateValue = BigDecimal.ZERO;
		BigDecimal limitValue = BigDecimal.ZERO;

		List<ComplianceRulePosition> positionByIssuerList = getPositionByIssuerList(ruleEvaluationConfig);
		final AccountingPositionHandler positionHandler = getAccountingPositionHandler();
		if (getMarketValueLimit() != null) {
			valueName = "market value";
			aggregateValue = CoreMathUtils.sumProperty(positionByIssuerList, position -> position.getMarketValueBase(positionHandler));
			limitValue = getMarketValueLimit();
		}
		else if (getNotionalLimit() != null) {
			valueName = "notional value";
			aggregateValue = CoreMathUtils.sumProperty(positionByIssuerList, position -> position.getNotionalValueBase(positionHandler));
			limitValue = getNotionalLimit();
		}
		else if (getQuantityLimit() != null) {
			valueName = "quantity";
			aggregateValue = CoreMathUtils.sumProperty(positionByIssuerList, position -> {
				BigDecimal quantity = BigDecimal.ZERO;
				AccountingBean accountingBean = position.getAccountingBean();
				if (accountingBean != null) {
					quantity = accountingBean.getQuantity();
					if (InvestmentUtils.isInternationalSecurity(accountingBean.getInvestmentSecurity(), InvestmentUtils.getClientAccountBaseCurrency(accountingBean))) {
						// TODO: initially this logic will only be applicable to bonds: Quantity really means Face and is used to represent Exposure
						// after we implement "Exposure" calculators and functionality, this logic should be refactored
						quantity = MathUtils.multiply(quantity, accountingBean.getExchangeRateToBase());
					}
				}
				return quantity;
			});
			limitValue = getQuantityLimit();
		}

		ComplianceRuleRun complianceRuleRun = new ComplianceRuleRun();
		complianceRuleRun.setPass(true);
		if (MathUtils.isGreaterThan(aggregateValue, limitValue)) {
			BusinessCompany issuer = ruleEvaluationConfig.retrieveInvestmentSecurity().getBusinessCompany();
			String issuerLabel = isGroupByUltimateIssuer() ? issuer.getRootParent().getLabel() : issuer.getLabel();
			Set<String> securityList = positionByIssuerList.stream().filter(Objects::nonNull).map(ComplianceRulePosition::getRealSecurityOrPseudo).map(InvestmentSecurity::getSymbol).collect(Collectors.toSet());

			String description = "The aggregate " + valueName + " [" + CoreMathUtils.formatNumber(aggregateValue, MathUtils.NUMBER_FORMAT_MONEY)
					+ "] for securities " + securityList + " issued by [" + issuerLabel + "] exceeds the specified limit of [" +
					CoreMathUtils.formatNumber(limitValue, MathUtils.NUMBER_FORMAT_MONEY) + "]";

			ComplianceRuleRunDetail detail = new ComplianceRuleRunDetail();
			detail.setComplianceRuleRun(complianceRuleRun);
			detail.setDescription(description);
			detail.setPass(false);

			complianceRuleRun.setDescription(description);
			complianceRuleRun.addDetail(detail);
			complianceRuleRun.setPass(false);
		}
		return complianceRuleRun;
	}


	private List<ComplianceRulePosition> getPositionByIssuerList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		Map<BusinessCompany, List<ComplianceRulePosition>> positionByIssuerMap;
		BusinessCompany issuer = ruleEvaluationConfig.retrieveInvestmentSecurity().getBusinessCompany();
		if (!isGroupByUltimateIssuer()) {
			positionByIssuerMap = ComplianceUtils.mapPositionListByIssuer(ruleEvaluationConfig.getPositionList());
			return positionByIssuerMap.get(issuer);
		}
		else {
			positionByIssuerMap = ComplianceUtils.mapPositionListByUltimateIssuer(ruleEvaluationConfig.getPositionList());
			return positionByIssuerMap.get(issuer.getRootParent());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public BigDecimal getMarketValueLimit() {
		return this.marketValueLimit;
	}


	public void setMarketValueLimit(BigDecimal marketValueLimit) {
		this.marketValueLimit = marketValueLimit;
	}


	public BigDecimal getNotionalLimit() {
		return this.notionalLimit;
	}


	public void setNotionalLimit(BigDecimal notionalLimit) {
		this.notionalLimit = notionalLimit;
	}


	public BigDecimal getQuantityLimit() {
		return this.quantityLimit;
	}


	public void setQuantityLimit(BigDecimal quantityLimit) {
		this.quantityLimit = quantityLimit;
	}


	public boolean isGroupByUltimateIssuer() {
		return this.groupByUltimateIssuer;
	}


	public void setGroupByUltimateIssuer(boolean groupByUltimateIssuer) {
		this.groupByUltimateIssuer = groupByUltimateIssuer;
	}
}
