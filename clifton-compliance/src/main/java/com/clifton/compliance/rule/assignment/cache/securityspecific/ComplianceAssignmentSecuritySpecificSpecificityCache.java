package com.clifton.compliance.rule.assignment.cache.securityspecific;

import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;
import java.util.List;


/**
 * The specificity cache type for {@link ComplianceRuleAssignment} entities on security-specific {@link ComplianceRuleType rule types}.
 * <p>
 * A rule type is considered security-specific if its {@link ComplianceRuleType#investmentSecuritySpecific} field is <code>true</code>. Security-specific rule types are different
 * from standard rule types in that their applicability for a position depends on the specificity hierarchy for the security-specific fields on the rule instance (such as
 * investment type, investment sub type, and instrument). Rather than applying all assigned instances of any given security-specific rule type to all positions in an account,
 * assignments are instead compared to find the assignment on the <i>most-specific</i> instance of the rule type which applies to the security.
 * <p>
 * For example, take the following account and rule configuration:
 * <ul>
 * <li>Account: MyCorp
 * <li>Positions:
 * <ul>
 * <li>Security AAPL
 * <li>Security XYZ
 * </ul>
 * <li>Applicable security-specific rules (for a single rule type):
 * <ul>
 * <li>Rule 1: Instrument for security AAPL
 * <li>Rule 2: Hierarchy for all equities
 * </ul>
 * </ul>
 * <p>
 * When rule evaluation is done, only Rule 1 will apply to the position for security AAPL since Rule 1 corresponds directly to the instrument for AAPL, which is more specific than
 * Rule 2 which applies to all equity positions. In contrast, Rule 2 will apply to the position for security XYZ since XYZ does not fall into the specificity constraints for Rule
 * 1.
 * <p>
 * This behavior is repeated for each rule type that applies to the account via rule assignments.
 *
 * @author MikeH
 */
public interface ComplianceAssignmentSecuritySpecificSpecificityCache {

	/**
	 * Retrieves the most specific set of results that apply for the given account and security.
	 */
	List<ComplianceRuleAssignment> getMostSpecificResultList(InvestmentAccount clientAccount, InvestmentSecurity security, Date processDate, boolean realTime);


	/**
	 * Pre-builds the cache for later use.
	 */
	void prebuildCache();
}
