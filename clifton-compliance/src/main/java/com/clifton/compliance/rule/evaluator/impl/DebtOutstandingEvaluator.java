package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>DebtOutstandingEvaluator</code>
 * <p>
 * <p>
 * <p>
 * Investment Company Act of 1940 Section 12d3-1(b)(2): 10% Debt Outstanding
 * <p>
 * The Fund may not purchase more than 10% of
 * the total debt outstanding in the aggregate of a broker,
 * dealer, principal underwriter or investment adviser.
 * <p>
 * In order to test this rule an identification is needed to identify all securities that are considered to derive more that 15% revenue from Securities Related Business.  Look to identify by Industry Classifications
 * <p>
 * Denominator:  Par amount debt securities outstanding of a single issuer of a Security Related Business Company.
 * <p>
 * Numerator:  Par amount debt securities held in the Fund of a single issuer of a Security Related Business Company
 * <p>
 * Formula:   Par amount debt securities held in the Fund of a single issuer of a Security Related Business Company divided by Par amount debt securities outstanding of a single issuer of a Security Related Business Company must be less than 10%.
 *
 * @author apopp
 */
public class DebtOutstandingEvaluator extends BaseComplianceRuleRatioEvaluator {

	private Short includeInvestmentGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return "";
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {

		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();
		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		getComplianceRuleFilterHandler().filterBySRB(securityIdList, true);
		getComplianceRuleFilterHandler().filterByInvestmentGroup(securityIdList, getIncludeInvestmentGroupId(), true);

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		for (InvestmentSecurity security : securityPositionMap.keySet()) {
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());
			config.setInvestmentSecurityId(security.getId());
			config.setDebtPosition(true);
			configList.add(config);
		}

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<Integer> securityIdList = CollectionUtils.createList(ruleEvaluationConfig.getInvestmentSecurityId());
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityPositionMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.DEBT_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) throws ValidationException {
		return calculateDebtOutstanding(ruleEvaluationConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getIncludeInvestmentGroupId() {
		return this.includeInvestmentGroupId;
	}


	public void setIncludeInvestmentGroupId(Short includeInvestmentGroupId) {
		this.includeInvestmentGroupId = includeInvestmentGroupId;
	}
}
