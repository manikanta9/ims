package com.clifton.compliance.rule.filter;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitTypes;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.context.Context;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


public interface ComplianceRuleFilterHandler {

	public void filterByInvestmentGroup(Map<? extends NamedObject, List<ComplianceRulePosition>> positionListMap, Short investmentGroupId, boolean instrument);


	public void filterByInvestmentGroup(List<Integer> securityIdList, Short investmentGroupId, boolean inclusion);


	public void filterByInvestmentType(List<Integer> securityIdList, Short investmentTypeId, boolean inclusion);


	public void filterByInvestmentTypeSubType(List<Integer> securityIdList, Short investmentTypeSubTypeId, boolean inclusion);


	public void filterByInvestmentHierarchy(List<Integer> securityIdList, Short investmentHierarchyId, boolean inclusion);


	public void filterByBusinessCompany(List<Integer> securityIdList, int businessCompanyId, boolean inclusion);


	public void filterByIlliquid(List<Integer> securityIdList, boolean inclusion);


	public void filterByRIC(List<Integer> securityIdList, boolean inclusion);


	public void filterBySRB(List<Integer> securityIdList, boolean inclusion);


	public void filterByIndustryExclusionList(Set<SystemHierarchy> hierarchies, List<Short> exclusionIdList, boolean inclusion);


	public void filterByUltimateIssuerValue(Context context, Map<BusinessCompany, List<ComplianceRulePosition>> issuerPositionList, boolean inclusion, BigDecimal percentLimit, ComplianceRuleLimitTypes limitType,
	                                        Date processDate, BigDecimal denominator);


	public void filterByVotingShares(Context context, Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap, boolean inclusion, BigDecimal percentLimit, ComplianceRuleLimitTypes limitType,
	                                 Date processDate);


	public Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> mapSecurityPositionListToIndustry(Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap,
	                                                                                                                     Integer level);


	public boolean isSecuritySRB(InvestmentSecurity security);
}
