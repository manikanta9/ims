package com.clifton.compliance.rule.assignment;

import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.Validator;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The self-registering {@link Validator} for {@link ComplianceRuleAssignment} entities.
 * <p>
 * This validator ensures that assignment entities satisfy application-side constraints and that assignment entities do not conflict.
 *
 * @author MikeH
 */
@Component
public class ComplianceRuleAssignmentValidator extends SelfRegisteringDaoValidator<ComplianceRuleAssignment> {

	private ComplianceRuleService complianceRuleService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(ComplianceRuleAssignment assignment, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertFalse(assignment.getClientInvestmentAccount() != null && assignment.getBusinessService() != null, "A Compliance Rule Assignment may not be assigned to both a client account and business service simultaneously.");
		if (assignment.getStartDate() != null && assignment.getEndDate() != null) {
			ValidationUtils.assertBefore(assignment.getStartDate(), assignment.getEndDate(), "endDate");
		}

		List<ComplianceRuleAssignment> existingAssignmentList = getAssignmentListForRuleAndTarget(assignment.getRule().getId(),
				BeanUtils.getBeanIdentity(assignment.getClientInvestmentAccount()),
				BeanUtils.getBeanIdentity(assignment.getBusinessService()),
				assignment.isGlobal());
		validateNoOverlapWithExistingAssignmentList(assignment, existingAssignmentList);
		validateSecuritySpecificConstraints(assignment);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of all assignments for the given rule which apply to the given target(s).
	 */
	private List<ComplianceRuleAssignment> getAssignmentListForRuleAndTarget(Integer ruleId, Integer clientAccountId, Short businessServiceId, boolean global) {
		ComplianceRuleAssignmentSearchForm searchForm = new ComplianceRuleAssignmentSearchForm();
		searchForm.setRuleId(ruleId);
		searchForm.setAccountId(clientAccountId);
		searchForm.setBusinessServiceId(businessServiceId);
		searchForm.setGlobal(global ? true : null);
		return getComplianceRuleService().getComplianceRuleAssignmentList(searchForm);
	}


	/**
	 * Validates that the new assignment does not overlap with any existing assignments for the same target.
	 */
	private void validateNoOverlapWithExistingAssignmentList(ComplianceRuleAssignment newAssignment, List<ComplianceRuleAssignment> targetAssignmentList) {
		for (ComplianceRuleAssignment assignment : targetAssignmentList) {
			if (newAssignment.equals(assignment)) {
				continue;
			}

			// Validate no overlap
			ValidationUtils.assertFalse(DateUtils.isOverlapInDates(assignment.getStartDate(), assignment.getEndDate(), newAssignment.getStartDate(), newAssignment.getEndDate()), () -> String.format("The given assignment for rule [%s] overlaps in applicable dates with an existing assignment on target [%s]. Overlapping assignments are not allowed.", newAssignment.getRule().getName(), newAssignment.getScopeLabel()));
		}
	}


	/**
	 * Validates certain constraints that apply to security-specific rules only.
	 */
	private void validateSecuritySpecificConstraints(ComplianceRuleAssignment assignment) {
		if (assignment.getRule().getRuleType().isInvestmentSecuritySpecific()) {
			// The system does not currently re-evaluate the position list for each security-specific assignment; only positions on the assigned account will be considered
			ValidationUtils.assertNull(assignment.getSubAccountPurpose(), "Sub-account evaluation is not supported for security-specific rule types.", "subAccountPurpose");
			ValidationUtils.assertNull(assignment.getSubsidiaryPurpose(), "Subsidiary evaluation is not supported for security-specific rule types.", "subsidiaryPurpose");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}
}
