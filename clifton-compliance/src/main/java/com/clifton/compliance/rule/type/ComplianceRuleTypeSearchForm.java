package com.clifton.compliance.rule.type;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ComplianceRuleTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean rollupRule;

	//custom
	private Short complianceRuleLimitTypeId;


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getRollupRule() {
		return this.rollupRule;
	}


	public void setRollupRule(Boolean rollupRule) {
		this.rollupRule = rollupRule;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getComplianceRuleLimitTypeId() {
		return this.complianceRuleLimitTypeId;
	}


	public void setComplianceRuleLimitTypeId(Short complianceRuleLimitTypeId) {
		this.complianceRuleLimitTypeId = complianceRuleLimitTypeId;
	}
}
