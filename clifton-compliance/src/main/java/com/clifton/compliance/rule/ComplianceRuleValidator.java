package com.clifton.compliance.rule;

import com.clifton.compliance.rule.limit.ComplianceRuleLimitService;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitTypeSearchForm;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.List;


/**
 * The {@link ComplianceRuleValidator} is the {@link SelfRegisteringDaoValidator} to validate {@link ComplianceRule} entities on save.
 *
 * @author MikeH
 */
public class ComplianceRuleValidator extends SelfRegisteringDaoValidator<ComplianceRule> {

	private ComplianceRuleLimitService complianceRuleLimitService;
	private ComplianceRuleService complianceRuleService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(ComplianceRule bean, DaoEventTypes config) throws ValidationException {
		if (!bean.isNewBean() && bean.getChildComplianceRuleList() != null) {
			ValidationUtils.assertFalse(containsRollupCycle(bean.getChildComplianceRuleList(), bean), "A rollup cannot directly or indirectly contain itself.");
		}

		if (bean.getComplianceRuleLimitType() != null) {
			validateLimitType(bean.getRuleType(), bean.getComplianceRuleLimitType());
		}

		// Validate exclusivity specificity elements
		BaseEntity<Short> investmentType = ObjectUtils.coalesce(bean.getInvestmentTypeSubType2(), bean.getInvestmentTypeSubType(), bean.getInvestmentType());
		if (investmentType != null || bean.getInvestmentHierarchy() != null || bean.getInvestmentInstrument() != null) {
			ValidationUtils.assertMutuallyExclusive("Only one specificity category (investment type, hierarchy, or instrument) may be selected.",
					investmentType, bean.getInvestmentHierarchy(), bean.getInvestmentInstrument());
		}
		ValidationUtils.assertTrue(bean.getInvestmentInstrument() == null || bean.getUnderlyingInstrument() == null,
				"An underlying instrument and investment instrument may not be simultaneously selected.", "underlyingInstrument");

		// Validate hierarchy of specificity elements
		ValidationUtils.assertTrue(bean.getInvestmentTypeSubType() == null || bean.getInvestmentTypeSubType().getInvestmentType().equals(bean.getInvestmentType()), "The investment sub-type must be a descendant of the given investment type.", "investmentTypeSubType");
		ValidationUtils.assertTrue(bean.getInvestmentTypeSubType2() == null || bean.getInvestmentTypeSubType2().getInvestmentType().equals(bean.getInvestmentType()), "The investment sub-type 2 must be a descendant of the given investment type.", "investmentTypeSubType2");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Rollup cycle detection algorithm
	 * <p>
	 * Traverse the graph in a depth first search manner seeing if the parent has a route to itself anywhere.
	 *
	 * @param children
	 * @param parent
	 * @return - cycle or not
	 */
	private boolean containsRollupCycle(List<ComplianceRule> children, ComplianceRule parent) {
		for (ComplianceRule child : CollectionUtils.getIterable(children)) {
			if (parent.equals(child)) {
				//cycle detected
				return true;
			}

			child.setChildComplianceRuleList(getComplianceRuleService().getComplianceRuleChildrenList(child));
			if (child.getChildComplianceRuleList() != null) {
				if (containsRollupCycle(child.getChildComplianceRuleList(), parent)) {
					return true;
				}
			}
		}

		return false;
	}


	private void validateLimitType(ComplianceRuleType ruleType, ComplianceRuleLimitType complianceRuleLimitType) {
		ComplianceRuleLimitTypeSearchForm searchForm = new ComplianceRuleLimitTypeSearchForm();
		searchForm.setComplianceRuleTypeId(ruleType.getId());
		searchForm.setName(complianceRuleLimitType.getName());

		ValidationUtils.assertFalse(CollectionUtils.isEmpty(getComplianceRuleLimitService().getComplianceRuleLimitTypeList(searchForm)),
				"Limit type [" + complianceRuleLimitType.getName() + "] not supported by rules of type [" + ruleType.getName() + "]");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleLimitService getComplianceRuleLimitService() {
		return this.complianceRuleLimitService;
	}


	public void setComplianceRuleLimitService(ComplianceRuleLimitService complianceRuleLimitService) {
		this.complianceRuleLimitService = complianceRuleLimitService;
	}


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}
}
