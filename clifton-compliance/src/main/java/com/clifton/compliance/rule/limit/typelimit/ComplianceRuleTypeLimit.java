package com.clifton.compliance.rule.limit.typelimit;


import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.beans.BaseEntity;


/**
 * The <code>ComplianceRuleTypeLimit</code> maintains an association between a rule type
 * and a limit type it supports.
 *
 * @author apopp
 */
public class ComplianceRuleTypeLimit extends BaseEntity<Integer> {

	/**
	 * Rule Type
	 */
	private ComplianceRuleType complianceRuleType;

	/**
	 * Supported limit type
	 */
	private ComplianceRuleLimitType complianceRuleLimitType;


	public ComplianceRuleType getComplianceRuleType() {
		return this.complianceRuleType;
	}


	public void setComplianceRuleType(ComplianceRuleType complianceRuleType) {
		this.complianceRuleType = complianceRuleType;
	}


	public ComplianceRuleLimitType getComplianceRuleLimitType() {
		return this.complianceRuleLimitType;
	}


	public void setComplianceRuleLimitType(ComplianceRuleLimitType complianceRuleLimitType) {
		this.complianceRuleLimitType = complianceRuleLimitType;
	}
}
