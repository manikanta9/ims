package com.clifton.compliance.rule.evaluator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.context.Context;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * The <code>ComplianceRuleEvaluationConfig</code> defines a set of available parameters for controlling
 * real-time and batch processing logic flow
 *
 * @author apopp
 */
public class ComplianceRuleEvaluationConfig extends ComplianceRunExecutionCommand {

	private static final String POSITION_LIST = "positionList";

	private final ComplianceRuleAccountHandler complianceRuleAccountHandler;
	private final ComplianceRulePositionHandler complianceRulePositionHandler;
	private final InvestmentAccountService investmentAccountService;
	private final InvestmentInstrumentService investmentInstrumentService;
	/**
	 * Acts as a cache of constituent securities making up the allocations of securities to decrease database hits
	 */
	private final Map<String, Set<InvestmentSecurity>> securityConstituents = new HashMap<>();
	private Object type;
	private BusinessCompany issuer;
	private ComplianceRuleLimitType complianceRuleLimitType;
	private ComplianceRuleAssignment ruleAssignment;
	private boolean includeCash = false;
	private boolean includeCashCollateral = false;
	private boolean sharePosition = false;
	private boolean debtPosition = false;
	private boolean ultimateIssuer = false;
	private boolean useNotionalValueOfPositions = false;
	/**
	 * The rule determines it wishes to control it's context
	 */
	private boolean cleanContext;
	/**
	 * Stored here for each re-use.  For calculators that optionally support getting values as of a relative date (2 business days back, previous month end, etc.)
	 * we can run as of this date.
	 */
	private Date calculationDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleEvaluationConfig(ComplianceRuleEvaluationConfig other) {
//		// Copy constructor
		super(other);
		this.complianceRuleAccountHandler = other.complianceRuleAccountHandler;
		this.complianceRulePositionHandler = other.complianceRulePositionHandler;
		this.investmentAccountService = other.investmentAccountService;
		this.investmentInstrumentService = other.investmentInstrumentService;
		this.type = other.type;
		this.issuer = other.issuer;
		this.complianceRuleLimitType = other.complianceRuleLimitType;
		this.ruleAssignment = other.ruleAssignment;
		this.includeCash = other.includeCash;
		this.includeCashCollateral = other.includeCashCollateral;
		this.sharePosition = other.sharePosition;
		this.debtPosition = other.debtPosition;
		this.ultimateIssuer = other.ultimateIssuer;
		this.useNotionalValueOfPositions = other.useNotionalValueOfPositions;
		this.cleanContext = other.cleanContext;
	}


	public ComplianceRuleEvaluationConfig(ComplianceRuleAccountHandler complianceRuleAccountHandler, ComplianceRulePositionHandler complianceRulePositionHandler, InvestmentAccountService investmentAccountService, InvestmentInstrumentService investmentInstrumentService) {
		this.complianceRuleAccountHandler = Objects.requireNonNull(complianceRuleAccountHandler, "Compliance Rule Account Handler may not be null.");
		this.complianceRulePositionHandler = Objects.requireNonNull(complianceRulePositionHandler, "Compliance Rule Position Handler may not be null.");
		this.investmentAccountService = Objects.requireNonNull(investmentAccountService, "Investment Account Service may not be null.");
		this.investmentInstrumentService = Objects.requireNonNull(investmentInstrumentService, "Investment Instrument Service may not be null.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getCalculationDate() {
		return ObjectUtils.coalesce(this.calculationDate, getProcessDate());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}


	public List<AccountingPosition> getAccountingPositionList() {
		return getComplianceRulePositionHandler().getAccountingPositionList(getContext(), getRuleAssignment(), getClientAccountId(), getCalculationDate());
	}


	@SuppressWarnings("unchecked")
	public List<ComplianceRulePosition> getPositionList() {
		if (isCleanContext()) {
			return (List<ComplianceRulePosition>) getTradeRuleEvaluatorContext().getContext().getBean(POSITION_LIST);
		}
		return getComplianceRulePositionHandler().getPositionListForAccountByRule(getTradeRuleEvaluatorContext().getContext(), getRuleAssignment(), getClientAccountId(), getCalculationDate());
	}


	@ValueChangingSetter
	public void setPositionList(List<ComplianceRulePosition> positionList) {
		getTradeRuleEvaluatorContext().getContext().setBean(POSITION_LIST, positionList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Context getContext() {
		return getTradeRuleEvaluatorContext().getContext();
	}


	public List<Integer> retrieveAccountIdList() {
		Short subAccountPurposeId = (getRuleAssignment().getSubAccountPurpose() != null) ? getRuleAssignment().getSubAccountPurpose().getId() : null;
		Short subsidiaryPurposeId = (getRuleAssignment().getSubsidiaryPurpose() != null) ? getRuleAssignment().getSubsidiaryPurpose().getId() : null;
		List<Integer> accountIdList = getComplianceRuleAccountHandler().getAccountRelatedAccountListByPurpose(getTradeRuleEvaluatorContext().getContext(), getRuleAssignment().getClientInvestmentAccount().getId(), getCalculationDate(), subAccountPurposeId, subsidiaryPurposeId);
		if (!accountIdList.contains(getRuleAssignment().getClientInvestmentAccount().getId())) {
			accountIdList.add(getRuleAssignment().getClientInvestmentAccount().getId());
		}
		return accountIdList;
	}


	public InvestmentAccount retrieveClientAccount() {
		return getClientAccountId() != null ? getInvestmentAccountService().getInvestmentAccount(getClientAccountId()) : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity retrieveInvestmentSecurity() {
		return getInvestmentSecurityId() != null ? getInvestmentInstrumentService().getInvestmentSecurity(getInvestmentSecurityId()) : null;
	}


	public ComplianceRuleAccountHandler getComplianceRuleAccountHandler() {
		return this.complianceRuleAccountHandler;
	}


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public Object getType() {
		return this.type;
	}


	public void setType(Object type) {
		this.type = type;
	}


	public BusinessCompany getIssuer() {
		return this.issuer;
	}


	public void setIssuer(BusinessCompany issuer) {
		this.issuer = issuer;
	}


	public ComplianceRuleLimitType getComplianceRuleLimitType() {
		return this.complianceRuleLimitType;
	}


	public void setComplianceRuleLimitType(ComplianceRuleLimitType complianceRuleLimitType) {
		this.complianceRuleLimitType = complianceRuleLimitType;
	}


	public ComplianceRuleAssignment getRuleAssignment() {
		return this.ruleAssignment;
	}


	public void setRuleAssignment(ComplianceRuleAssignment ruleAssignment) {
		this.ruleAssignment = ruleAssignment;
	}


	public boolean isIncludeCash() {
		return this.includeCash;
	}


	public void setIncludeCash(boolean includeCash) {
		this.includeCash = includeCash;
	}


	public boolean isIncludeCashCollateral() {
		return this.includeCashCollateral;
	}


	public void setIncludeCashCollateral(boolean includeCashCollateral) {
		this.includeCashCollateral = includeCashCollateral;
	}


	public boolean isSharePosition() {
		return this.sharePosition;
	}


	public void setSharePosition(boolean sharePosition) {
		this.sharePosition = sharePosition;
	}


	public boolean isDebtPosition() {
		return this.debtPosition;
	}


	public void setDebtPosition(boolean debtPosition) {
		this.debtPosition = debtPosition;
	}


	public boolean isUltimateIssuer() {
		return this.ultimateIssuer;
	}


	public void setUltimateIssuer(boolean ultimateIssuer) {
		this.ultimateIssuer = ultimateIssuer;
	}


	public boolean isUseNotionalValueOfPositions() {
		return this.useNotionalValueOfPositions;
	}


	public void setUseNotionalValueOfPositions(boolean useNotionalValueOfPositions) {
		this.useNotionalValueOfPositions = useNotionalValueOfPositions;
	}


	public boolean isCleanContext() {
		return this.cleanContext;
	}


	public void setCleanContext(boolean cleanContext) {
		this.cleanContext = cleanContext;
	}


	public Map<String, Set<InvestmentSecurity>> getSecurityConstituents() {
		return this.securityConstituents;
	}
}
