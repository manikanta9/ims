package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>ICAct194012D31b2</code>
 * <p>
 * <p>
 * <p>
 * Investment Company Act of 1940 Section 12d3-1(b)(2): 5% Total of Equity Outstanding of a Security Related Business company
 * <p>
 * The Fund may not purchase more than 5% of
 * the total equity outstanding in the aggregate of a broker,
 * dealer, principal underwriter or investment advisor.
 * <p>
 * In order to test this rule an identification is needed to identify all securities that are considered to derive more that 15% revenue from Securities Related Business.  Look to identify by Industry Classifications
 * <p>
 * We can use shares outstanding instead of total equity outstanding for this rule.
 * <p>
 * Denominator:  Total Equity Outstanding of a Security Related Business Company.
 * <p>
 * Numerator:  Total Equity held in the Fund of a single issuer of a Security Related Business Company
 * <p>
 * Formula:   Total Equity held in the Fund of a single issuer of a Security Related Business Company divided by Total Equity outstanding of a single issuer of a Security Related Business Company must be less than 5%.
 *
 * @author apopp
 */
public class ICAct194012D31b2 extends BaseComplianceRuleRatioEvaluator {

	@SuppressWarnings("unused")
	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return "";
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();
		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		getComplianceRuleFilterHandler().filterBySRB(securityIdList, true);

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		for (InvestmentSecurity security : securityPositionMap.keySet()) {
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());
			config.setInvestmentSecurityId(security.getId());
			config.setSharePosition(true);
			configList.add(config);
		}

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<Integer> securityIdList = CollectionUtils.createList(ruleEvaluationConfig.getInvestmentSecurityId());
		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityPositionMap, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.SHARES_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) throws ValidationException {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(ruleEvaluationConfig.getInvestmentSecurityId());
		return getComplianceBaseHandler().calculateSharesOutstanding(security, ruleEvaluationConfig.getProcessDate());
	}
}
