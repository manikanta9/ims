package com.clifton.compliance.rule.assignment.cache;

import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.CompositeSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.DynamicSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * The base {@link SpecificityCache} type for {@link ComplianceRuleAssignment} caches.
 * <p>
 * Subtypes of this class provide cache implementations for subsets of rule assignment entities.
 *
 * @author MikeH
 */
public abstract class BaseComplianceAssignmentSpecificityCache<S extends BaseComplianceAssignmentSource, T extends BaseComplianceAssignmentTargetHolder> extends AbstractSpecificityCache<S, ComplianceRuleAssignment, T> {

	private final SpecificityCacheUpdater<T> cacheUpdater = new ComplianceAssignmentCompositeCacheUpdater();
	private final boolean investmentSecuritySpecific;

	private ComplianceRuleService complianceRuleService;
	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private BusinessServiceService businessServiceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseComplianceAssignmentSpecificityCache(boolean investmentSecuritySpecific) {
		super(SpecificityCacheTypes.MATCHING_RESULT, new SimpleOrderingSpecificityKeyGenerator());
		this.investmentSecuritySpecific = investmentSecuritySpecific;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Pre-builds the cache. This can be used to eagerly execute database operations ahead-of-time to save time during later cache usages. Running this operation in a background
	 * thread can reduce latency due to synchronous database calls on a primary thread.
	 */
	public void prebuildCache() {
		getOrBuildCache();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected abstract T generateTargetHolder(ComplianceRule rule, ComplianceRuleAssignment assignment);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean isMatch(S source, T holder) {
		boolean evaluationTypeMatched = source.isRealTime() ? holder.isRealTime() : holder.isBatch();
		if (!evaluationTypeMatched) {
			return false;
		}

		boolean dateRangeMatched = DateUtils.isDateBetween(source.getDate(), holder.getStartDate(), holder.getEndDate(), false);
		if (!dateRangeMatched) {
			return false;
		}

		return true;
	}


	@Override
	protected SpecificityKeySource getKeySource(S source) {
		return source.getKeySource();
	}


	@Override
	protected ComplianceRuleAssignment getTarget(T targetHolder) {
		ComplianceRuleAssignment assignment = getComplianceRuleService().getComplianceRuleAssignment(targetHolder.getRuleAssignmentId());
		final ComplianceRuleAssignment appliedAssignment;
		if (assignment.getRule().getId().equals(targetHolder.getRuleId())) {
			appliedAssignment = assignment;
		}
		else {
			appliedAssignment = BeanUtils.cloneBean(assignment, false, true);
			appliedAssignment.setRule(getComplianceRuleService().getComplianceRule(targetHolder.getRuleId()));
		}
		return appliedAssignment;
	}


	@Override
	protected SpecificityCacheUpdater<T> getCacheUpdater() {
		return this.cacheUpdater;
	}


	@Override
	protected Collection<T> getTargetHolders() {
		long startTime = System.currentTimeMillis();
		List<ComplianceRuleAssignment> assignmentList = getComplianceRuleAssignmentHandler().getFullAssignmentList(isInvestmentSecuritySpecific());
		List<T> targetHolderList = getAssignmentTargetHolderList(assignmentList);
		long timeInSeconds = (System.currentTimeMillis() - startTime) / 1000;
		LogUtils.info(getClass(), "It took " + timeInSeconds + " second(s) to rebuild '" + getClass().getSimpleName() + "'.");
		return targetHolderList;
	}


	@Override
	public List<ComplianceRuleAssignment> getMostSpecificResultList(S source) {
		// Override default implementation to process results
		List<ComplianceRuleAssignment> assignmentList = super.getMostSpecificResultList(source);
		return mergeAssignmentList(assignmentList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets all target holders for assignments within the given assignment list.
	 */
	private List<T> getAssignmentTargetHolderList(List<ComplianceRuleAssignment> assignmentList) {
		List<T> targetHolderList = new ArrayList<>();
		BusinessServiceSearchForm sf = new BusinessServiceSearchForm();
		sf.setSkipDefaultSorting(true);
		List<BusinessService> businessServiceList = getBusinessServiceService().getBusinessServiceList(sf);
		for (ComplianceRuleAssignment assignment : getFlattenedAssignmentList(assignmentList)) {
			ComplianceRule rule = assignment.getRule();
			targetHolderList.add(generateTargetHolder(rule, assignment));
			if (assignment.getBusinessService() != null) {
				// Get pseudo-assignments for sub-business-services
				List<ComplianceRuleAssignment> pseudoAssignmentList = getComplianceRuleAssignmentHandler().getPseudoBusinessServiceAssignmentList(assignment, businessServiceList);
				List<T> pseudoAssignmentTargetList = CollectionUtils.getConverted(pseudoAssignmentList, ruleAssignment -> generateTargetHolder(rule, ruleAssignment));
				targetHolderList.addAll(pseudoAssignmentTargetList);
			}
		}
		return targetHolderList;
	}


	/**
	 * Retrieves the provided assignment list with all rollup rule assignments flattened. This will produce pseudo-assignments for all sub-rules contained in rollup rule
	 * assignments.
	 *
	 * @param assignmentList the list of assignments to flatten
	 * @return the flattened list with all rollup rule assignments converted into pseudo-assignments for their applicable rules
	 */
	private List<ComplianceRuleAssignment> getFlattenedAssignmentList(List<ComplianceRuleAssignment> assignmentList) {
		List<ComplianceRuleAssignment> applicableAssignmentList = new ArrayList<>();
		for (ComplianceRuleAssignment assignment : assignmentList) {
			// Guard-clause: Do no transformations for non-rollup rules
			if (!assignment.getRule().getRuleType().isRollupRule()) {
				applicableAssignmentList.add(assignment);
				continue;
			}

			// Convert roll-up rules to pseudo-assignments from children
			List<ComplianceRuleAssignment> rollupAssignmentList = getComplianceRuleAssignmentHandler().getPseudoRollupAssignmentList(assignment);
			applicableAssignmentList.addAll(rollupAssignmentList);
		}
		return applicableAssignmentList;
	}


	/**
	 * Reduces the given assignment list into distinct assignments, removing duplicate assignments which would result in duplicate evaluations of the same {@link ComplianceRule}.
	 * <p>
	 * The resulting list will include one and only one assignment for each {@link ComplianceRule} which exists in the provided assignment list. Assignments are considered unique
	 * by rule ID and by assignment-specific characteristics such as {@link ComplianceRuleAssignment#subsidiaryPurpose subisidiary purpose} and {@link
	 * ComplianceRuleAssignment#subAccountPurpose sub-account purpose}.
	 */
	private List<ComplianceRuleAssignment> mergeAssignmentList(List<ComplianceRuleAssignment> assignmentList) {
		List<ComplianceRuleAssignment> distinctAssignmentList = CollectionUtils.getDistinct(assignmentList, assignment -> StringUtils.generateKey(
				BeanUtils.getBeanIdentity(assignment.getRule()),
				BeanUtils.getBeanIdentity(assignment.getSubAccountPurpose()),
				BeanUtils.getBeanIdentity(assignment.getSubsidiaryPurpose()),
				BeanUtils.getBeanIdentity(assignment.getIgnorePassSystemCondition()),
				BeanUtils.getBeanIdentity(assignment.getIgnoreFailSystemCondition())
		));
		return distinctAssignmentList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isInvestmentSecuritySpecific() {
		return this.investmentSecuritySpecific;
	}


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of a {@link CompositeSpecificityCacheUpdater}, using the following strategy
	 * <p>
	 * For updates to {@link ComplianceRuleAssignment}, it uses a custom cache update strategy found in {@link ComplianceAssignmentCacheUpdater}
	 * For updates to {@link ComplianceRule} and {@link BusinessService}, it uses a clear cache update strategy
	 */
	class ComplianceAssignmentCompositeCacheUpdater extends CompositeSpecificityCacheUpdater<T> {

		ComplianceAssignmentCompositeCacheUpdater() {
			super(Arrays.asList(
					new ComplianceAssignmentCacheUpdater(),
					new ClearSpecificityCacheUpdater<>(ComplianceRule.class),
					new ClearSpecificityCacheUpdater<>(BusinessService.class)
			));
		}
	}

	/**
	 * Cache updater for {@link ComplianceRuleAssignment}
	 * <p>
	 * Uses a dynamic update strategy to find which entities should be updated by calling {@link BaseComplianceAssignmentSpecificityCache#getAssignmentTargetHolderList}
	 */
	class ComplianceAssignmentCacheUpdater extends DynamicSpecificityCacheUpdater<T> {

		public ComplianceAssignmentCacheUpdater() {
			super(ComplianceRuleAssignment.class);
		}


		@Override
		protected List<T> getTargetHoldersForCacheUpdate(Object updatedInstance) {
			if (!(updatedInstance instanceof ComplianceRuleAssignment)) {
				return Collections.emptyList();
			}

			ComplianceRuleType ruleType = ((ComplianceRuleAssignment) updatedInstance).getRule().getRuleType();
			return (!isInvestmentSecuritySpecific() || ruleType.isInvestmentSecuritySpecific() == isInvestmentSecuritySpecific() || ruleType.isRollupRule())
					? getAssignmentTargetHolderList(Collections.singletonList((ComplianceRuleAssignment) updatedInstance))
					: Collections.emptyList();
		}
	}
}
