package com.clifton.compliance.rule;


import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentBulk;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.compliance.rule.limit.typelimit.ComplianceRuleTypeLimit;
import com.clifton.compliance.rule.rollup.ComplianceRuleRollup;
import com.clifton.compliance.rule.rollup.ComplianceRuleRollupSearchForm;
import com.clifton.compliance.rule.rollup.cache.ComplianceRuleRollupCache;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.rule.type.ComplianceRuleTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Service
public class ComplianceRuleServiceImpl implements ComplianceRuleService {

	private AdvancedUpdatableDAO<ComplianceRule, Criteria> complianceRuleDAO;
	private AdvancedUpdatableDAO<ComplianceRuleType, Criteria> complianceRuleTypeDAO;
	private AdvancedUpdatableDAO<ComplianceRuleAssignment, Criteria> complianceRuleAssignmentDAO;
	private AdvancedUpdatableDAO<ComplianceRuleRollup, Criteria> complianceRuleRollupDAO;

	private ComplianceRuleRollupCache complianceRuleRollupCache;

	private InvestmentAccountService investmentAccountService;
	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Assignment Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleAssignment getComplianceRuleAssignment(int id) {
		return getComplianceRuleAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ComplianceRuleAssignment> getComplianceRuleAssignmentList(final ComplianceRuleAssignmentSearchForm searchForm) {
		return getComplianceRuleAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public void saveComplianceRuleAssignmentBulk(ComplianceRuleAssignmentBulk bean) {
		ValidationUtils.assertNotNull(bean.getClientAccountIds(), "Must specify at least one client account.");
		for (Integer clientAccountId : bean.getClientAccountIds()) {
			//Save individually since dao list save does the same
			ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
			assignment.setRule(bean.getRule());
			assignment.setSubAccountPurpose(bean.getSubAccountPurpose());
			assignment.setSubsidiaryPurpose(bean.getSubsidiaryPurpose());
			assignment.setIgnoreFailSystemCondition(bean.getIgnoreFailSystemCondition());
			assignment.setIgnorePassSystemCondition(bean.getIgnorePassSystemCondition());
			assignment.setStartDate(bean.getStartDate());
			assignment.setEndDate(bean.getEndDate());
			assignment.setClientInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(clientAccountId));
			assignment.setNote(bean.getNote());
			assignment.setExclusionAssignment(bean.isExclusionAssignment());
			saveComplianceRuleAssignment(assignment);
		}
	}


	@Override
	@Transactional
	public void copyComplianceRuleAssignment(int fromClientAccountId, int[] toClientAccountIds, Date startDate) {
		ComplianceRuleAssignmentSearchForm searchForm = new ComplianceRuleAssignmentSearchForm();
		searchForm.setAccountId(fromClientAccountId);

		List<ComplianceRuleAssignment> assignmentList = getComplianceRuleAssignmentList(searchForm);

		for (int toClientAccountId : toClientAccountIds) {
			for (ComplianceRuleAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
				ComplianceRuleAssignment newAssignment = new ComplianceRuleAssignment();
				newAssignment.setRule(assignment.getRule());
				newAssignment.setSubAccountPurpose(assignment.getSubAccountPurpose());
				newAssignment.setSubsidiaryPurpose(assignment.getSubsidiaryPurpose());
				newAssignment.setIgnoreFailSystemCondition(assignment.getIgnoreFailSystemCondition());
				newAssignment.setIgnorePassSystemCondition(assignment.getIgnorePassSystemCondition());
				newAssignment.setStartDate(startDate);
				newAssignment.setClientInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(toClientAccountId));
				newAssignment.setNote(assignment.getNote());
				saveComplianceRuleAssignment(newAssignment);
			}
		}
	}


	@Override
	public ComplianceRuleAssignment saveComplianceRuleAssignment(ComplianceRuleAssignment assignment) {
		return getComplianceRuleAssignmentDAO().save(assignment);
	}


	@Override
	public void deleteComplianceRuleAssignment(int id) {
		getComplianceRuleAssignmentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRule getComplianceRule(int id) {
		ComplianceRule complianceRule = getComplianceRuleDAO().findByPrimaryKey(id);

		if (complianceRule != null) {
			if (complianceRule.getRuleEvaluatorBean() != null) {
				complianceRule.setRuleEvaluatorBean(getSystemBeanService().getSystemBean(complianceRule.getRuleEvaluatorBean().getId()));
			}
			complianceRule.setChildComplianceRuleList(getComplianceRuleChildrenList(complianceRule));
		}
		return complianceRule;
	}


	@Override
	public List<ComplianceRule> getComplianceRuleChildrenList(ComplianceRule rule) {
		if (rule != null && rule.getRuleType().isRollupRule()) {
			List<ComplianceRuleRollup> rollupList = getComplianceRuleRollupListByParentOrChild(rule.getId(), true);
			List<ComplianceRule> childList = new ArrayList<>();
			for (ComplianceRuleRollup rollup : CollectionUtils.getIterable(rollupList)) {
				childList.add(rollup.getReferenceTwo());
			}
			return childList;
		}
		return null;
	}


	@Override
	@Transactional(readOnly = true)
	public List<ComplianceRule> getComplianceRuleFullChildrenList(ComplianceRule rule) {
		final List<ComplianceRule> childRules;
		if (rule.getRuleType().isRollupRule()) {
			// Roll-up all children rules
			List<ComplianceRule> childrenList = getComplianceRuleChildrenList(rule);
			childRules = CollectionUtils.getConvertedFlattened(childrenList, this::getComplianceRuleFullChildrenList);
		}
		else {
			// End of recursion depth: Return single rule
			childRules = Collections.singletonList(rule);
		}
		return childRules;
	}


	@Override
	public List<ComplianceRule> getComplianceRuleList(final ComplianceRuleSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm);
		return getComplianceRuleDAO().findBySearchCriteria(config);
	}


	/**
	 * {@inheritDoc}
	 *
	 * @see ComplianceRuleValidator
	 */
	@Override
	@Transactional
	public ComplianceRule saveComplianceRule(ComplianceRule bean) {
		if (bean.getRuleEvaluatorBean() != null) {
			//make bean name unique
			bean.getRuleEvaluatorBean().setName(bean.getName() + " : " + bean.getId());
			getSystemBeanService().saveSystemBean(bean.getRuleEvaluatorBean());
		}

		List<ComplianceRule> childComplianceRuleList = bean.getChildComplianceRuleList();
		ComplianceRule savedBean = getComplianceRuleDAO().save(bean);
		if (savedBean.getRuleType().isRollupRule()) {
			// new rollup compliance rule
			if (savedBean.isNewBean()) {
				List<ComplianceRuleRollup> newRollupList = populateRollupList(savedBean, childComplianceRuleList, true);
				saveComplianceRuleRollupList(newRollupList);
			}
			else {
				ComplianceRuleSearchForm searchForm = new ComplianceRuleSearchForm();
				searchForm.setParentComplianceRuleId(savedBean.getId());
				List<ComplianceRule> originalChildComplianceRuleList = getComplianceRuleList(searchForm);

				// a list of child schedules on the bean, that are not already on the parent
				List<ComplianceRule> newChildComplianceRuleList = CoreCollectionUtils.removeAll(childComplianceRuleList, originalChildComplianceRuleList);

				if (!CollectionUtils.isEmpty(newChildComplianceRuleList)) {
					// save new rollups
					List<ComplianceRuleRollup> newRollupList = populateRollupList(savedBean, newChildComplianceRuleList, true);
					saveComplianceRuleRollupList(newRollupList);
				}

				// a list of child rules on the parent, that are no longer on the bean
				List<ComplianceRule> deletedChildComplianceRuleList = CoreCollectionUtils.removeAll(originalChildComplianceRuleList, childComplianceRuleList);

				if (!CollectionUtils.isEmpty(deletedChildComplianceRuleList)) {
					List<ComplianceRuleRollup> deletedRollupList = populateRollupList(savedBean, deletedChildComplianceRuleList, false);
					deleteComplianceRuleRollupList(deletedRollupList);
				}
			}
		}
		savedBean.setChildComplianceRuleList(childComplianceRuleList);
		return savedBean;
	}


	@Override
	@Transactional
	public void deleteComplianceRule(int id) {
		ComplianceRule complianceRule = getComplianceRule(id);

		if (complianceRule.getRuleType().isRollupRule()) {
			// delete many to many relationships to child
			ComplianceRuleRollupSearchForm searchForm = new ComplianceRuleRollupSearchForm();
			searchForm.setParentRuleId(id);
			List<ComplianceRuleRollup> childRollupList = getComplianceRuleRollupList(searchForm);

			if (!CollectionUtils.isEmpty(childRollupList)) {
				deleteComplianceRuleRollupList(childRollupList);
			}
		}
		else {
			getSystemBeanService().deleteSystemBean(complianceRule.getRuleEvaluatorBean().getId());
		}

		getComplianceRuleDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Rollup Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	private List<ComplianceRuleRollup> getComplianceRuleRollupList(ComplianceRuleRollupSearchForm searchForm) {
		return getComplianceRuleRollupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private ComplianceRuleRollup getComplianceRuleRollup(int parentId, int childId) {
		return getComplianceRuleRollupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{parentId, childId});
	}


	/**
	 * Helper method for parent/child rollup look ups.  Cache stores full list where rule is a parent or a child, this method gets/sets from the cache and returns filtered list
	 * based on parent flag (if parent = true, returns list where rule is the parent, else child)
	 */
	private List<ComplianceRuleRollup> getComplianceRuleRollupListByParentOrChild(int complianceRuleId, boolean parent) {
		List<ComplianceRuleRollup> rollupList = null;
		Integer[] rollupIds = getComplianceRuleRollupCache().getComplianceRuleRollupList(complianceRuleId);
		// Nothing cached yet - look it up
		if (rollupIds == null) {
			ComplianceRuleRollupSearchForm searchForm = new ComplianceRuleRollupSearchForm();
			searchForm.setParentOrChildRuleId(complianceRuleId);
			rollupList = getComplianceRuleRollupList(searchForm);
			getComplianceRuleRollupCache().storeComplianceRuleRollupList(complianceRuleId, rollupList);
		}
		// Otherwise lookup rollups by id to get real list (only if length of array > 0
		else if (rollupIds.length > 0) {
			rollupList = getComplianceRuleRollupDAO().findByPrimaryKeys(rollupIds);
		}
		// Filter out the list where rule = parent or = child depending on which one looking for.
		if (!CollectionUtils.isEmpty(rollupList)) {
			return BeanUtils.filter(rollupList, complianceRuleRollup -> (parent ? complianceRuleRollup.getReferenceOne().getId() : complianceRuleRollup.getReferenceTwo().getId()), complianceRuleId);
		}
		return rollupList;
	}


	private void saveComplianceRuleRollupList(List<ComplianceRuleRollup> beanList) {
		getComplianceRuleRollupDAO().saveList(beanList);
	}


	private void deleteComplianceRuleRollupList(List<ComplianceRuleRollup> beanList) {
		getComplianceRuleRollupDAO().deleteList(beanList);
	}


	/**
	 * Helper method that will populate a list of ComplianceRuleRollups for a given parent and list of child rules.  If createNewRollups is true, new rollup beans will be created,
	 * else existing rollups will be retrieved from the DB.
	 */
	private List<ComplianceRuleRollup> populateRollupList(ComplianceRule parent, List<ComplianceRule> childComplianceRuleList, boolean createNewRollups) {
		List<ComplianceRuleRollup> result = null;
		if (parent != null && !CollectionUtils.isEmpty(childComplianceRuleList)) {
			result = new ArrayList<>();

			if (createNewRollups) {
				for (ComplianceRule child : CollectionUtils.getIterable(childComplianceRuleList)) {
					ComplianceRuleRollup rollup = new ComplianceRuleRollup();
					rollup.setReferenceOne(parent);
					rollup.setReferenceTwo(child);

					result.add(rollup);
				}
			}
			else {
				// get existing rollups
				for (ComplianceRule child : CollectionUtils.getIterable(childComplianceRuleList)) {
					ComplianceRuleRollup rollup = getComplianceRuleRollup(parent.getId(), child.getId());

					result.add(rollup);
				}
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Type Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleType getComplianceRuleType(short id) {
		return getComplianceRuleTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ComplianceRuleType getComplianceRuleTypeByName(String name) {
		return getComplianceRuleTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<ComplianceRuleType> getComplianceRuleTypeList(final ComplianceRuleTypeSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getComplianceRuleLimitTypeId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(ComplianceRuleTypeLimit.class, "limit");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("limit.complianceRuleType.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("complianceRuleLimitType.id", searchForm.getComplianceRuleLimitTypeId()));

					criteria.add(Subqueries.exists(sub));
				}
			}
		};

		return getComplianceRuleTypeDAO().findBySearchCriteria(searchConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ComplianceRule, Criteria> getComplianceRuleDAO() {
		return this.complianceRuleDAO;
	}


	public void setComplianceRuleDAO(AdvancedUpdatableDAO<ComplianceRule, Criteria> complianceRuleDAO) {
		this.complianceRuleDAO = complianceRuleDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleType, Criteria> getComplianceRuleTypeDAO() {
		return this.complianceRuleTypeDAO;
	}


	public void setComplianceRuleTypeDAO(AdvancedUpdatableDAO<ComplianceRuleType, Criteria> complianceRuleTypeDAO) {
		this.complianceRuleTypeDAO = complianceRuleTypeDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleAssignment, Criteria> getComplianceRuleAssignmentDAO() {
		return this.complianceRuleAssignmentDAO;
	}


	public void setComplianceRuleAssignmentDAO(AdvancedUpdatableDAO<ComplianceRuleAssignment, Criteria> complianceRuleAssignmentDAO) {
		this.complianceRuleAssignmentDAO = complianceRuleAssignmentDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleRollup, Criteria> getComplianceRuleRollupDAO() {
		return this.complianceRuleRollupDAO;
	}


	public void setComplianceRuleRollupDAO(AdvancedUpdatableDAO<ComplianceRuleRollup, Criteria> complianceRuleRollupDAO) {
		this.complianceRuleRollupDAO = complianceRuleRollupDAO;
	}


	public ComplianceRuleRollupCache getComplianceRuleRollupCache() {
		return this.complianceRuleRollupCache;
	}


	public void setComplianceRuleRollupCache(ComplianceRuleRollupCache complianceRuleRollupCache) {
		this.complianceRuleRollupCache = complianceRuleRollupCache;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
