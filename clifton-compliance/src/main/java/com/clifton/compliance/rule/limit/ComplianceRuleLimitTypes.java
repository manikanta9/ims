package com.clifton.compliance.rule.limit;


import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>ComplianceRuleLimitTypes</code> represents rule execution limit types. Each type also contains
 * the logic for how the pass status is evaluated.
 *
 * @author apopp
 */
public enum ComplianceRuleLimitTypes {

	MAXIMUM() {
		@SuppressWarnings("unused")
		@Override
		public boolean isPass(BigDecimal resultValue, BigDecimal limitValue, BigDecimal limitValueExtended) {
			return !MathUtils.isGreaterThan(resultValue, limitValue);
		}
	},

	MINIMUM() {
		@SuppressWarnings("unused")
		@Override
		public boolean isPass(BigDecimal resultValue, BigDecimal limitValue, BigDecimal limitValueExtended) {
			return MathUtils.isGreaterThan(resultValue, limitValue);
		}
	},

	RANGE() {
		@Override
		public boolean isPass(BigDecimal resultValue, BigDecimal limitValue, BigDecimal limitValueExtended) {
			return MathUtils.isBetween(resultValue, limitValue, limitValueExtended);
		}
	};


	ComplianceRuleLimitTypes() {
	}


	public abstract boolean isPass(BigDecimal resultValue, BigDecimal limitValue, BigDecimal limitValueExtended);
}
