package com.clifton.compliance.rule.assignment;

import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The auto-registered search form configurer for {@link ComplianceRuleAssignment} searches. This search form configurer applies to any search executed using {@link
 * ComplianceRuleAssignmentSearchForm} search form types.
 *
 * @author MikeH
 */
@Component
public class ComplianceRuleAssignmentSearchFormConfigurer implements SearchFormCustomConfigurer<ComplianceRuleAssignmentSearchForm> {

	private BusinessServiceService businessServiceService;
	private InvestmentAccountService investmentAccountService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<ComplianceRuleAssignmentSearchForm> getSearchFormInterfaceClassName() {
		return ComplianceRuleAssignmentSearchForm.class;
	}


	@Override
	public void configureCriteria(Criteria criteria, ComplianceRuleAssignmentSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		if (searchForm.getAppliesToAccountId() != null) {
			applyAppliesToAccountIdCriteria(criteria, searchForm.getAppliesToAccountId());
		}
		if (searchForm.getAppliesToBusinessServiceId() != null) {
			applyAppliesToBusinessServiceIdCriteria(criteria, searchForm.getAppliesToBusinessServiceId());
		}
		if (searchForm.getGlobal() != null) {
			if (BooleanUtils.isTrue(searchForm.getGlobal())) {
				criteria.add(Restrictions.and(Restrictions.isNull("clientInvestmentAccount.id"), Restrictions.isNull("businessService.id")));
			}
			else {
				criteria.add(Restrictions.or(Restrictions.isNotNull("clientInvestmentAccount.id"), Restrictions.isNotNull("businessService.id")));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Apply constraints to the criteria to filter results on those which apply to the given account ID.
	 * <p>
	 * This includes direct assignments to the given {@link InvestmentAccount}, assignments which apply to the {@link BusinessService} hierarchy for the account, and global
	 * assignments.
	 *
	 * @param criteria           the criteria to which the constraints should be added
	 * @param appliesToAccountId the account ID for which results should be filtered
	 */
	private void applyAppliesToAccountIdCriteria(Criteria criteria, Integer appliesToAccountId) {
		InvestmentAccount account = getInvestmentAccountService().getInvestmentAccount(appliesToAccountId);
		ValidationUtils.assertNotNull(account, "No account found for ID [" + appliesToAccountId + "].", "appliesToAccountId");

		// Apply account ID constraints
		Criterion accountIsNull = Restrictions.isNull("clientInvestmentAccount.id");
		Criterion accountEq = Restrictions.eq("clientInvestmentAccount.id", account.getId());
		criteria.add(Restrictions.or(accountIsNull, accountEq));

		// Apply business service constraints
		Criterion businessServiceIsNull = Restrictions.isNull("businessService.id");
		Criterion businessServiceConstraint = account.getBusinessService() != null
				? Restrictions.or(businessServiceIsNull, Subqueries.exists(getBusinessServiceForAssignmentExistsCriteria(criteria, account.getBusinessService().getId())))
				: businessServiceIsNull;
		criteria.add(businessServiceConstraint);
	}


	/**
	 * Apply constraints to the criteria to filter results on those which apply to the given business service ID.
	 * <p>
	 * This includes direct assignments to the given {@link BusinessService} as well as global assignments.
	 *
	 * @param criteria                   the criteria to which the constraints should be added
	 * @param appliesToBusinessServiceId the business service ID for which results should be filtered
	 */
	private void applyAppliesToBusinessServiceIdCriteria(Criteria criteria, Short appliesToBusinessServiceId) {
		BusinessService businessService = getBusinessServiceService().getBusinessService(appliesToBusinessServiceId);
		ValidationUtils.assertNotNull(businessService, "No business service found for ID [" + appliesToBusinessServiceId + "].", "appliesToBusinessServiceId");

		// Apply account ID constraints
		Criterion accountIsNull = Restrictions.isNull("clientInvestmentAccount.id");
		criteria.add(accountIsNull);

		// Apply business service constraints
		DetachedCriteria businessServiceOrParentEq = getBusinessServiceForAssignmentExistsCriteria(criteria, businessService.getId());
		Criterion businessServiceIsNull = Restrictions.isNull("businessService.id");
		criteria.add(Restrictions.or(businessServiceIsNull, Subqueries.exists(businessServiceOrParentEq)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a sub-select {@link DetachedCriteria} used for filtering {@link ComplianceRuleAssignment} entities to those which are applicable to a given {@link BusinessService}.
	 * This sub-select traverses the business service hierarchy (up to 4-levels deep to accompany the maximum hierarchy depth) to determine if the given business service is
	 * matched.
	 *
	 * @param assignmentCriteria the {@link ComplianceRuleAssignment} criteria which shall be used to build sub-select constraints
	 * @param businessServiceId  the ID of the {@link BusinessService} to evaluate membership
	 * @return the {@link DetachedCriteria} which returns results for any {@link ComplianceRuleAssignment} which matches the given {@link BusinessService}
	 */
	private DetachedCriteria getBusinessServiceForAssignmentExistsCriteria(Criteria assignmentCriteria, Short businessServiceId) {
		/*
		 * Create sub-select for applicable business services for account. This sub-select traverses children of assignment business service (up to 4-levels deep to accompany
		 * hierarchy maximum) to determine if the business service is matched.
		 */
		DetachedCriteria businessServiceExistsCriteria = DetachedCriteria.forClass(BusinessService.class, "bs1");
		businessServiceExistsCriteria.setProjection(Projections.id());
		businessServiceExistsCriteria.add(Restrictions.eq("id", businessServiceId));
		businessServiceExistsCriteria.createAlias("bs1.parent", "bs2", JoinType.LEFT_OUTER_JOIN);
		businessServiceExistsCriteria.createAlias("bs2.parent", "bs3", JoinType.LEFT_OUTER_JOIN);
		businessServiceExistsCriteria.createAlias("bs3.parent", "bs4", JoinType.LEFT_OUTER_JOIN);
		String businessServiceIdProperty = assignmentCriteria.getAlias() + ".businessService.id";
		Criterion businessServiceHierarchyEq = Restrictions.or(
				Restrictions.eqProperty(businessServiceIdProperty, "bs1.id"),
				Restrictions.eqProperty(businessServiceIdProperty, "bs2.id"),
				Restrictions.eqProperty(businessServiceIdProperty, "bs3.id"),
				Restrictions.eqProperty(businessServiceIdProperty, "bs4.id")
		);
		businessServiceExistsCriteria.add(businessServiceHierarchyEq);
		return businessServiceExistsCriteria;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
