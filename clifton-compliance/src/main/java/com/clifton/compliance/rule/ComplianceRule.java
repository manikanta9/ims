package com.clifton.compliance.rule;


import com.clifton.compliance.rule.limit.ComplianceRuleLimitType;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ComplianceRule</code> class defines a specific instance of a compliance rule.
 * <p>
 * This instance holds information related to the rule itself, it also contains a rule evaluator bean
 * that holds the logic for evaluating this rule
 *
 * @author apopp
 */
public class ComplianceRule extends NamedEntity<Integer> implements Comparable<ComplianceRule>, SystemEntityModifyConditionAware {

	/**
	 * The type this rule falls under. Ex: long/short, fat finger, notional limits, etc
	 */
	private ComplianceRuleType ruleType;

	/**
	 * Ability to restrict who can edit compliance rules and assignments
	 */
	private SystemCondition entityModifyCondition;


	/**
	 * The bean that handles the evaluation of this specific rule instance.
	 */
	private SystemBean ruleEvaluatorBean;

	/**
	 * Defines if this rule is executed on a nightly basis
	 */
	private boolean batch;

	/**
	 * Defines if this rule is ran in real-time at transaction entry
	 */
	private boolean realTime;


	///////////////////// Specificity Scope Fields /////////////////////////////
	private InvestmentType investmentType;
	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentTypeSubType2 investmentTypeSubType2;
	private InvestmentInstrumentHierarchy investmentHierarchy;
	private InvestmentInstrument investmentInstrument;
	/**
	 * Identifies an ability to filter rules by a specific underlying instrument
	 */
	private InvestmentInstrument underlyingInstrument;
	/**
	 * Identifies international vs domestic securities
	 */
	private InvestmentCurrencyTypes currencyType;

	//////////////////////// Limit Rule Fields /////////////////////////////////
	/**
	 * Determines how the result of an execution of this run should be interpreted
	 * Ex. Percentage rules have minimum or maximum allowed results.
	 */
	private ComplianceRuleLimitType complianceRuleLimitType;

	/**
	 * Specifies a limit value to be used in conjunction with the rule limit type
	 * Ex: Limit value is used by rules that require percentage limits: Max 25% invested in Currencies.
	 */
	private BigDecimal limitValue;

	/**
	 * In the case that rule limit type is set to RANGE, limit value specifies the lower bound of the range
	 * while limit value range specifies the upper bound of the range
	 */
	private BigDecimal limitValueRange;


	/**
	 * List of child rules that are rolled up into this rule
	 */
	private List<ComplianceRule> childComplianceRuleList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getUnderlyingInstrument() {
		return this.underlyingInstrument;
	}


	public void setUnderlyingInstrument(InvestmentInstrument underlyingInstrument) {
		this.underlyingInstrument = underlyingInstrument;
	}


	public ComplianceRuleType getRuleType() {
		return this.ruleType;
	}


	public InvestmentCurrencyTypes getCurrencyType() {
		return this.currencyType;
	}


	public void setCurrencyType(InvestmentCurrencyTypes currencyType) {
		this.currencyType = currencyType;
	}


	public void setRuleType(ComplianceRuleType ruleType) {
		this.ruleType = ruleType;
	}


	public SystemBean getRuleEvaluatorBean() {
		return this.ruleEvaluatorBean;
	}


	public void setRuleEvaluatorBean(SystemBean ruleEvaluatorBean) {
		this.ruleEvaluatorBean = ruleEvaluatorBean;
	}


	public List<ComplianceRule> getChildComplianceRuleList() {
		return this.childComplianceRuleList;
	}


	public void setChildComplianceRuleList(List<ComplianceRule> childComplianceRuleList) {
		this.childComplianceRuleList = childComplianceRuleList;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public InvestmentInstrumentHierarchy getInvestmentHierarchy() {
		return this.investmentHierarchy;
	}


	public void setInvestmentHierarchy(InvestmentInstrumentHierarchy investmentHierarchy) {
		this.investmentHierarchy = investmentHierarchy;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	/**
	 * Define sorting comparison on precedence related to
	 * Investment Type, Investment Hierarchy, and Investment Instrument
	 */
	@Override
	public int compareTo(ComplianceRule otherRule) {
		if (getInvestmentInstrument() != null && otherRule.getInvestmentInstrument() == null) {
			return 1;
		}

		if (getInvestmentInstrument() == null && otherRule.getInvestmentInstrument() != null) {
			return -1;
		}

		if (getInvestmentHierarchy() != null && otherRule.getInvestmentHierarchy() == null) {
			return 1;
		}

		if (getInvestmentHierarchy() == null && otherRule.getInvestmentHierarchy() != null) {
			return -1;
		}

		if (getInvestmentType() != null && otherRule.getInvestmentType() == null) {
			return 1;
		}

		if (getInvestmentType() == null && otherRule.getInvestmentType() != null) {
			return -1;
		}

		return 0;
	}


	public boolean isBatch() {
		return this.batch;
	}


	public void setBatch(boolean batch) {
		this.batch = batch;
	}


	public boolean isRealTime() {
		return this.realTime;
	}


	public void setRealTime(boolean realTime) {
		this.realTime = realTime;
	}


	public ComplianceRuleLimitType getComplianceRuleLimitType() {
		return this.complianceRuleLimitType;
	}


	public void setComplianceRuleLimitType(ComplianceRuleLimitType complianceRuleLimitType) {
		this.complianceRuleLimitType = complianceRuleLimitType;
	}


	public BigDecimal getLimitValue() {
		return this.limitValue;
	}


	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}


	public BigDecimal getLimitValueRange() {
		return this.limitValueRange;
	}


	public void setLimitValueRange(BigDecimal limitValueRange) {
		this.limitValueRange = limitValueRange;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
