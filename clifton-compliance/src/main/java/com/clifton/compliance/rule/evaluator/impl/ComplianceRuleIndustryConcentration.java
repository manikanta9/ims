package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.compliance.ComplianceUtils;
import com.clifton.compliance.rule.evaluator.BaseComplianceRuleRatioEvaluator;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.rule.position.ComplianceRulePositionValues;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>ComplianceRuleIndustryConcentration</code>
 * <p>
 * <p>
 * Industry Concentration
 * <p>
 * Max 25% of the Funds Total Assets may be invested in any one industry
 * (Excluding US Government and Agencies)
 *
 * @author apopp
 */
public class ComplianceRuleIndustryConcentration extends BaseComplianceRuleRatioEvaluator {

	private ComplianceRulePositionHandler complianceRulePositionHandler;
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	private boolean excludeCashCollateral;

	private Short investmentGroupId;
	private Short systemHierarchyCategoryId;
	private List<Short> industryExclusionIdList;

	private Integer aggregationLevel;

	/*
	 * This rule needs to be able to exclude U.S. Gov issued securities. To be more flexible we allow the rule to
	 * specify a business company issuer to exclude. All securities whose ultimate issuer falls under this business
	 * company will be excluded from the processing of the rule
	 */
	private Integer excludeBusinessCompanyId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String generateViolation(String numerator, String denominator, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		String industryLevel = "unrecognized";

		if (ruleEvaluationConfig.getType() instanceof SystemHierarchy) {
			SystemHierarchy hierarchy = ((SystemHierarchy) ruleEvaluationConfig.getType());
			industryLevel = hierarchy.getName();
		}

		return "Industry " + industryLevel;
	}


	@Override
	public List<ComplianceRuleEvaluationConfig> getBatchRuleEvaluationConfigList(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		List<ComplianceRuleEvaluationConfig> configList = new ArrayList<>();

		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		getComplianceRuleFilterHandler().filterByBusinessCompany(securityIdList, getExcludeBusinessCompanyId(), false);
		getComplianceRuleFilterHandler().filterByInvestmentGroup(securityIdList, getInvestmentGroupId(), false);

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> industryMapping = getComplianceRuleFilterHandler().mapSecurityPositionListToIndustry(securityPositionMap,
				getAggregationLevel());

		getComplianceRuleFilterHandler().filterByIndustryExclusionList(industryMapping.keySet(), getIndustryExclusionIdList(), false);

		for (SystemHierarchy hierarchy : industryMapping.keySet()) {
			if (hierarchy == null) {
				continue;
			}
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());
			config.setType(hierarchy);
			config.setIncludeCashCollateral(!isExcludeCashCollateral());
			configList.add(config);
		}

		if (industryMapping.containsKey(null)) {
			ComplianceRuleEvaluationConfig config = new ComplianceRuleEvaluationConfig(ruleEvaluationConfig.getComplianceRuleAccountHandler(), ruleEvaluationConfig.getComplianceRulePositionHandler(), ruleEvaluationConfig.getInvestmentAccountService(), ruleEvaluationConfig.getInvestmentInstrumentService());
			config.setClientAccountId(ruleEvaluationConfig.getClientAccountId());
			config.setProcessDate(ruleEvaluationConfig.getProcessDate());

			SystemHierarchy naHierarchy = new SystemHierarchy();
			naHierarchy.setName("Securities without Defined Industry");
			naHierarchy.setId((short) -1);

			config.setType(naHierarchy);
			config.setIncludeCashCollateral(!isExcludeCashCollateral());
			configList.add(config);
		}

		return configList;
	}


	@Override
	public ComplianceRuleRunDetail evaluateNumerator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {

		List<Integer> securityIdList = ComplianceUtils.getUniqueSecurityListFromPositionList(ruleEvaluationConfig.getPositionList());
		getComplianceRuleFilterHandler().filterByBusinessCompany(securityIdList, getExcludeBusinessCompanyId(), false);

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityPositionMap = ComplianceUtils.mapPositionListBySecurity(securityIdList, ruleEvaluationConfig.getPositionList());

		Map<SystemHierarchy, Map<InvestmentSecurity, List<ComplianceRulePosition>>> industryMapping = getComplianceRuleFilterHandler().mapSecurityPositionListToIndustry(securityPositionMap,
				getAggregationLevel());

		Map<InvestmentSecurity, List<ComplianceRulePosition>> securityList = (((SystemHierarchy) ruleEvaluationConfig.getType()).getId() == -1) ? industryMapping.get(null)
				: industryMapping.get(ruleEvaluationConfig.getType());

		return getComplianceRuleReportHandler().createFullRunDetail(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), securityList, ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE);
	}


	@Override
	public BigDecimal evaluateDenominator(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return getComplianceRulePositionHandler().getAccountTotalValue(ruleEvaluationConfig.getTradeRuleEvaluatorContext().getContext(), ruleEvaluationConfig.getPositionList(), ruleEvaluationConfig.getProcessDate(), ComplianceRulePositionValues.MARKET_VALUE,
				ruleEvaluationConfig.retrieveAccountIdList(), isUseTotalAssets(), isExcludeCashCollateral());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRulePositionHandler getComplianceRulePositionHandler() {
		return this.complianceRulePositionHandler;
	}


	public void setComplianceRulePositionHandler(ComplianceRulePositionHandler complianceRulePositionHandler) {
		this.complianceRulePositionHandler = complianceRulePositionHandler;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public boolean isExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getSystemHierarchyCategoryId() {
		return this.systemHierarchyCategoryId;
	}


	public void setSystemHierarchyCategoryId(Short systemHierarchyCategoryId) {
		this.systemHierarchyCategoryId = systemHierarchyCategoryId;
	}


	public List<Short> getIndustryExclusionIdList() {
		return this.industryExclusionIdList;
	}


	public void setIndustryExclusionIdList(List<Short> industryExclusionIdList) {
		this.industryExclusionIdList = industryExclusionIdList;
	}


	public Integer getAggregationLevel() {
		return this.aggregationLevel;
	}


	public void setAggregationLevel(Integer aggregationLevel) {
		this.aggregationLevel = aggregationLevel;
	}


	public Integer getExcludeBusinessCompanyId() {
		return this.excludeBusinessCompanyId;
	}


	public void setExcludeBusinessCompanyId(Integer excludeBusinessCompanyId) {
		this.excludeBusinessCompanyId = excludeBusinessCompanyId;
	}
}
