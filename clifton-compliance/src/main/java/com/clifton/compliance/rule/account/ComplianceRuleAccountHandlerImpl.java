package com.clifton.compliance.rule.account;


import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class ComplianceRuleAccountHandlerImpl implements ComplianceRuleAccountHandler {

	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentAccount> filterAccountList(List<InvestmentAccount> investmentAccountList, Date processDate) {
		return investmentAccountList.stream()
				.filter(InvestmentAccount::isActive)
				.filter(account -> !getComplianceRuleAssignmentHandler().getAssignmentListForAccount(account, processDate).isEmpty())
				.collect(Collectors.toList());
	}


	@Override
	public List<Integer> getAccountRelatedAccountListByPurpose(Context context, Integer clientAccountId, Date processDate, Short relationshipPurposeId) {
		String contextKey = "getAccountRelatedAccountListByPurpose-" + clientAccountId + "-" + relationshipPurposeId + "-" + processDate;
		@SuppressWarnings("unchecked")
		List<Integer> accountIdList = (List<Integer>) context.getBean(contextKey);

		if (accountIdList != null) {
			return new ArrayList<>(accountIdList);
		}

		if (relationshipPurposeId == null) {
			return new ArrayList<>();
		}

		InvestmentAccountRelationshipPurpose purpose = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipPurpose(relationshipPurposeId);

		if (purpose == null) {
			return new ArrayList<>();
		}

		List<InvestmentAccountRelationship> accountRelationshipList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(clientAccountId, purpose.getName(),
				processDate, true);

		accountIdList = Arrays.asList(BeanUtils.getPropertyValues(accountRelationshipList, "referenceTwo.id", Integer.class));
		accountIdList = CollectionUtils.removeDuplicates(accountIdList);

		context.setBean(contextKey, accountIdList);

		return new ArrayList<>(accountIdList);
	}


	@Override
	public List<Integer> getAccountRelatedAccountListByPurpose(Context context, Integer clientAccountId, Date processDate, Short relationshipPurposeOneId, Short relationshipPurposeTwoId) {
		List<Integer> subAccountIdList = getAccountRelatedAccountListByPurpose(context, clientAccountId, processDate, relationshipPurposeOneId);
		List<Integer> subsidiaryAccountIdList = getAccountRelatedAccountListByPurpose(context, clientAccountId, processDate, relationshipPurposeTwoId);

		subAccountIdList.addAll(subsidiaryAccountIdList);

		return CollectionUtils.removeDuplicates(subAccountIdList);
	}


	@Override
	public List<Integer> getSubAccountListUniqueWithoutSubsidiary(Context context, Integer clientAccountId, Date processDate, Short subsidiaryPurposeId, Short subaccountPurposeId) {
		List<Integer> subAccountIdList = getAccountRelatedAccountListByPurpose(context, clientAccountId, processDate, subaccountPurposeId);
		List<Integer> subsidiaryAccountIdList = getAccountRelatedAccountListByPurpose(context, clientAccountId, processDate, subsidiaryPurposeId);

		return CoreCollectionUtils.removeAll(subAccountIdList, subsidiaryAccountIdList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
