package com.clifton.compliance.rule.rollup;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>RuleRollup</code> class associates two Rules with one another
 * as parent and child rules.  Parent rules are considered "rollups" of child rules.
 * Child rules can be rolled up into different parent rules.
 *
 * @author apopp
 */
public class ComplianceRuleRollup extends ManyToManyEntity<ComplianceRule, ComplianceRule, Integer> {
	// empty
}
