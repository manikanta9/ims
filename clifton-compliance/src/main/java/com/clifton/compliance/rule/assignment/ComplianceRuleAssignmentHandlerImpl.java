package com.clifton.compliance.rule.assignment;

import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.cache.securityspecific.ComplianceAssignmentSecuritySpecificSpecificityCache;
import com.clifton.compliance.rule.assignment.cache.standard.ComplianceAssignmentStandardSpecificityCache;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The standard implementation for {@link ComplianceRuleAssignmentHandler}.
 *
 * @author MikeH
 */
@Component
public class ComplianceRuleAssignmentHandlerImpl implements ComplianceRuleAssignmentHandler {

	private ComplianceRuleService complianceRuleService;

	private BusinessServiceService businessServiceService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private ComplianceAssignmentStandardSpecificityCache complianceAssignmentStandardSpecificityCache;
	private ComplianceAssignmentSecuritySpecificSpecificityCache complianceAssignmentSecuritySpecificSpecificityCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void buildComplianceCaches() {
		getComplianceAssignmentStandardSpecificityCache().prebuildCache();
		getComplianceAssignmentSecuritySpecificSpecificityCache().prebuildCache();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Rule Assignment Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRuleAssignment> getAssignmentListForAccount(InvestmentAccount clientAccount, Date processDate) {
		List<ComplianceRuleAssignment> batchAssignmentList = getAssignmentBatchList(clientAccount, processDate);
		List<ComplianceRuleAssignment> realTimeAssignmentList = getAssignmentRealTimeList(clientAccount, processDate);
		return CollectionUtils.combineCollections(batchAssignmentList, realTimeAssignmentList);
	}


	@Override
	public List<ComplianceRuleAssignment> getAssignmentBatchList(InvestmentAccount clientAccount, Date processDate) {
		return getComplianceAssignmentStandardSpecificityCache().getMostSpecificResultList(clientAccount, processDate, false);
	}


	@Override
	public List<ComplianceRuleAssignment> getAssignmentBatchSecuritySpecificList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date) {
		return getComplianceAssignmentSecuritySpecificSpecificityCache().getMostSpecificResultList(clientAccount, security, date, false);
	}


	@Override
	public List<ComplianceRuleAssignment> getAssignmentRealTimeList(InvestmentAccount clientAccount, Date processDate) {
		return getComplianceAssignmentStandardSpecificityCache().getMostSpecificResultList(clientAccount, processDate, true);
	}


	@Override
	public List<ComplianceRuleAssignment> getAssignmentRealTimeList(InvestmentAccount clientAccount, InvestmentSecurity security, Date processDate, boolean includeSecuritySpecificRules, boolean includeParentRules) {
		final List<ComplianceRuleAssignment> assignmentList = new ArrayList<>();

		// Add all real-time and non-security-specific rules for the given account
		if (includeSecuritySpecificRules) {
			assignmentList.addAll(getAssignmentRealTimeSecuritySpecificList(clientAccount, security, processDate));
		}
		assignmentList.addAll(getComplianceAssignmentStandardSpecificityCache().getMostSpecificNonSecuritySpecificResultList(clientAccount, processDate, true));

		// Add all direct parent main account rules that are non security specific to the execution of this child
		if (includeParentRules) {
			InvestmentAccountRelationshipConfig childRelationshipConfig = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipConfig(clientAccount.getId());
			if (childRelationshipConfig != null) {
				List<Integer> mainAccountIds = childRelationshipConfig.getMainInvestmentAccountList(clientAccount.getId(), processDate);
				for (Integer mainAccountId : CollectionUtils.getIterable(mainAccountIds)) {
					InvestmentAccount mainAccount = getInvestmentAccountService().getInvestmentAccount(mainAccountId);
					assignmentList.addAll(getComplianceAssignmentStandardSpecificityCache().getMostSpecificNonSecuritySpecificResultList(mainAccount, processDate, true));
				}
			}
		}

		return assignmentList;
	}


	@Override
	public List<ComplianceRuleAssignment> getAssignmentRealTimeSecuritySpecificList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date) {
		return getComplianceAssignmentSecuritySpecificSpecificityCache().getMostSpecificResultList(clientAccount, security, date, true);
	}


	@Override
	public ComplianceRuleAssignment getAssignmentForAccountOnDate(ComplianceRunExecutionCommand command) {
		// Guard-clause: If no rule or account was provided, then no assignment can be found or generated
		if (command.getComplianceRuleId() == null || command.getClientAccountId() == null) {
			return null;
		}

		// Get full assignment list for the given command
		InvestmentAccount clientAccount = getInvestmentAccountService().getInvestmentAccount(command.getClientAccountId());
		List<ComplianceRuleAssignment> fullAssignmentList = command.isRealTime()
				? getAssignmentRealTimeList(clientAccount, command.getProcessDate())
				: getAssignmentBatchList(clientAccount, command.getProcessDate());
		List<ComplianceRuleAssignment> filteredAssignmentList = CollectionUtils.getFiltered(fullAssignmentList, assignment -> Objects.equals(command.getComplianceRuleId(), assignment.getRule().getId()));

		// Get singular assignment
		ComplianceRuleAssignment assignment = CollectionUtils.getOnlyElement(filteredAssignmentList);
		if (assignment == null) {
			// Fall-back to pseudo-assignment when no explicit assignment is found
			/*
			 * This allows us to evaluate individual rules on a per-account basis, even if that rule is not actually assigned to that account. This is useful during the manual
			 * rule-rebuild process when a specific rule is selected.
			 */
			assignment = new ComplianceRuleAssignment();
			assignment.setRule(getComplianceRuleService().getComplianceRule(command.getComplianceRuleId()));
			assignment.setClientInvestmentAccount(clientAccount);
		}

		return assignment;
	}


	@Override
	public List<ComplianceRuleAssignment> getFullAssignmentList(boolean filterToSecuritySpecific) {
		ComplianceRuleAssignmentSearchForm sf = new ComplianceRuleAssignmentSearchForm();
		sf.setSkipDefaultSorting(true);
		List<ComplianceRuleAssignment> assignmentList = getComplianceRuleService().getComplianceRuleAssignmentList(sf);
		return CollectionUtils.getFiltered(assignmentList, assignment -> !filterToSecuritySpecific
				|| assignment.getRule().getRuleType().isInvestmentSecuritySpecific()
				|| assignment.getRule().getRuleType().isRollupRule());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Pseudo-Assignment Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ComplianceRuleAssignment> getPseudoBusinessServiceAssignmentList(ComplianceRuleAssignment businessServiceAssignment, List<BusinessService> businessServiceList) {
		AssertUtils.assertNotNull(businessServiceAssignment.getBusinessService(), "The provided rule assignment [%s] must be for a rollup rule.", businessServiceAssignment);

		if (businessServiceList == null) {
			// Find direct descendents
			BusinessServiceSearchForm sf = new BusinessServiceSearchForm();
			sf.setSkipDefaultSorting(true);
			businessServiceList = getBusinessServiceService().getBusinessServiceList(sf);
		}

		List<BusinessService> childBusinessServiceList = CollectionUtils.getFiltered(businessServiceList, s -> s.getParent() != null && s.getParent().equals(businessServiceAssignment.getBusinessService()));

		// Create child target holders and recurse down trees for sub-business-services
		List<ComplianceRuleAssignment> pseudoAssignmentList = new ArrayList<>();
		for (BusinessService businessService : CollectionUtils.getIterable(childBusinessServiceList)) {
			ComplianceRuleAssignment pseudoAssignment = BeanUtils.cloneBean(businessServiceAssignment, false, true);
			pseudoAssignment.setBusinessService(businessService);
			pseudoAssignmentList.add(pseudoAssignment);
			pseudoAssignmentList.addAll(getPseudoBusinessServiceAssignmentList(pseudoAssignment, businessServiceList));
		}

		return pseudoAssignmentList;
	}


	@Override
	@Transactional(readOnly = true)
	public List<ComplianceRuleAssignment> getPseudoRollupAssignmentList(ComplianceRuleAssignment rollupRuleAssignment) {
		// Validate inputs
		AssertUtils.assertTrue(rollupRuleAssignment.getRule().getRuleType().isRollupRule(), "The provided rule assignment [%s] must be for a rollup rule.", rollupRuleAssignment);

		// Generate pseudo-assignment list
		List<ComplianceRuleAssignment> pseudoAssignmentList = new ArrayList<>();
		List<ComplianceRule> childRuleList = getComplianceRuleService().getComplianceRuleFullChildrenList(rollupRuleAssignment.getRule());
		for (ComplianceRule rule : childRuleList) {
			ComplianceRuleAssignment pseudoAssignment = BeanUtils.cloneBean(rollupRuleAssignment, false, true);
			pseudoAssignment.setRule(rule);
			pseudoAssignmentList.add(pseudoAssignment);
		}

		return pseudoAssignmentList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public ComplianceAssignmentStandardSpecificityCache getComplianceAssignmentStandardSpecificityCache() {
		return this.complianceAssignmentStandardSpecificityCache;
	}


	public void setComplianceAssignmentStandardSpecificityCache(ComplianceAssignmentStandardSpecificityCache complianceAssignmentStandardSpecificityCache) {
		this.complianceAssignmentStandardSpecificityCache = complianceAssignmentStandardSpecificityCache;
	}


	public ComplianceAssignmentSecuritySpecificSpecificityCache getComplianceAssignmentSecuritySpecificSpecificityCache() {
		return this.complianceAssignmentSecuritySpecificSpecificityCache;
	}


	public void setComplianceAssignmentSecuritySpecificSpecificityCache(ComplianceAssignmentSecuritySpecificSpecificityCache complianceAssignmentSecuritySpecificSpecificityCache) {
		this.complianceAssignmentSecuritySpecificSpecificityCache = complianceAssignmentSecuritySpecificSpecificityCache;
	}
}
