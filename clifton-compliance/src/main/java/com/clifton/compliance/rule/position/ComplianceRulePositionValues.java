package com.clifton.compliance.rule.position;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ComplianceRulePositionValues</code> encapsulates different types of position value calculations.
 *
 * @author apopp
 */
public enum ComplianceRulePositionValues {

	MARKET_VALUE() {
		@Override
		public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate) {
			if (position.getAccountingBalanceValue() != null) {
				return position.getAccountingBalanceValue().getBaseMarketValue();
			}

			if (position.getPendingTrade() != null) {
				Trade trade = position.getPendingTrade();
				AccountingTransaction tran = new AccountingTransaction();
				tran.setInvestmentSecurity(trade.getInvestmentSecurity());
				tran.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
				tran.setClientInvestmentAccount(trade.getClientInvestmentAccount());
				tran.setSettlementDate(trade.getSettlementDate());
				tran.setPrice(trade.getAverageOrExpectedUnitPrice());

				AccountingPosition tradeAsPosition = AccountingPosition.forOpeningTransaction(tran);

				tradeAsPosition.setRemainingQuantity((trade.isBuy() ? trade.getQuantityNormalized() : MathUtils.negate(trade.getQuantityNormalized())));
				tradeAsPosition.setRemainingCostBasis((trade.isBuy() ? trade.getAccountingNotional() : MathUtils.negate(trade.getAccountingNotional())));

				return accountingPositionHandler.getAccountingPositionMarketValue(tradeAsPosition, processDate);
			}

			if (position.getPosition() != null && !position.isCash()) {
				return accountingPositionHandler.getAccountingPositionMarketValue(position.getPosition(), processDate);
			}

			return BigDecimal.ZERO;
		}
	},

	NOTIONAL_VALUE() {
		@Override
		public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate) {
			if (position.getAccountingBalanceValue() != null) {
				return position.getAccountingBalanceValue().getBaseNotional();
			}

			if (position.getPendingTrade() != null) {
				return position.getPendingTrade().getAccountingNotional();
			}

			return accountingPositionHandler.getAccountingPositionNotional(position.getPosition(), processDate);
		}
	},

	SHARES_VALUE() {
		@SuppressWarnings("unused")
		@Override
		public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate) {
			if (position.getAccountingBalanceValue() != null) {
				return position.getAccountingBalanceValue().getQuantityNormalized();
			}

			if (position.getPendingTrade() != null) {
				return position.getPendingTrade().getQuantityNormalized();
			}

			return position.getPosition().getRemainingQuantity();
		}
	},

	DEBT_VALUE() {
		@SuppressWarnings("unused")
		@Override
		public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate) {
			if (position.getAccountingBalanceValue() != null) {
				return position.getAccountingBalanceValue().getQuantityNormalized();
			}

			if (position.getPendingTrade() != null) {
				return position.getPendingTrade().getQuantityNormalized();
			}

			return position.getPosition().getRemainingQuantity();
		}
	},
	NO_VALUE() {
		@SuppressWarnings("unused")
		@Override
		public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate) {
			return BigDecimal.ZERO;
		}
	};


	ComplianceRulePositionValues() {
	}


	abstract public BigDecimal calculateValue(AccountingPositionHandler accountingPositionHandler, ComplianceRulePosition position, Date processDate);
}
