package com.clifton.compliance.rule.account;


import com.clifton.core.context.Context;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;


/**
 * The <code>ComplianceRuleAccountHandler</code> defines methods for how account relationships are retrieved for rules.
 *
 * @author apopp
 */
public interface ComplianceRuleAccountHandler {

	/**
	 * Given a list of accounts return back a list of all accounts that are active and have at least one rule to be
	 * executed for them.
	 */
	public List<InvestmentAccount> filterAccountList(List<InvestmentAccount> investmentAccountList, Date processDate);


	/**
	 * Retrieve a list of account ids for all directly related children accounts with the specified relationship.
	 */
	public List<Integer> getAccountRelatedAccountListByPurpose(Context context, Integer clientAccountId, Date processDate, Short relationshipPurposeId);


	/**
	 * Retrieve a unique list of account ids for all directly related children accounts with the specified relationships.
	 */
	public List<Integer> getAccountRelatedAccountListByPurpose(Context context, Integer clientAccountId, Date processDate, Short relationshipPurposeOneId, Short relationshipPurposeTwoId);


	/**
	 * Retrieve a list of accounts related the main account by subaccount purpose id excluding any of these accounts
	 * that are also related to the main account by subsidiary purpose id.
	 */
	public List<Integer> getSubAccountListUniqueWithoutSubsidiary(Context context, Integer clientAccountId, Date processDate, Short subsidiaryPurposeId, Short subaccountPurposeId);
}
