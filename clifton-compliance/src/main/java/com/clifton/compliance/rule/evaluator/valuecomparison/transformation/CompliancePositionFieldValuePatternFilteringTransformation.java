package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


/**
 * A {@link CompliancePositionTransformation} which filters {@link CompliancePositionValueHolder} objects based on a configured position property field and comparison pattern.
 * <p>
 * The position property field value is converted to a string for comparison. The comparison pattern is case-insensitive and supports glob-style wildcards (<code>*</code> for any
 * number of characters and <code>?</code> for single characters).
 *
 * @author MikeH
 */
public class CompliancePositionFieldValuePatternFilteringTransformation implements CompliancePositionTransformation, ValidationAware {

	private CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler;

	/**
	 * The position field which shall be used for the comparison, as retrieved by {@link CompliancePositionPropertyPathHandler}.
	 */
	private String positionField;
	/**
	 * The comparison pattern against which the {@link #positionField} shall be compared. This value will be treated as a glob-style pattern.
	 */
	private String comparisonPattern;
	/**
	 * If true, the results will be reversed. Positions which match the {@link #comparisonPattern} will be excluded and those which do not match will be returned.
	 */
	private boolean excludeMatched;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		Pattern comparisonPatternCompiled = StringUtils.getGlobPattern(getComparisonPattern());
		return CollectionUtils.getFiltered(positionValueList, valueHolder -> {
			boolean patternMatched = comparisonPatternCompiled.matcher(getValueForPositionValueHolder(valueHolder, processDate)).matches();
			return isExcludeMatched() ? !patternMatched : patternMatched;
		});
	}


	@Override
	public void validate() throws ValidationException {
		getCompliancePositionPropertyPathHandler().validateField(getPositionField());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getValueForPositionValueHolder(CompliancePositionValueHolder<AccountingPosition> valueHolder, Date processDate) {
		List<Object> valueList = CollectionUtils.getConverted(valueHolder.getPositionList(), position -> getCompliancePositionPropertyPathHandler().getPositionPropertyValue(position, getPositionField(), processDate));

		// Each group is treated as an individual object, so validate that all values obtained from positions within the group are equivalent
		List<Object> distinctValueList = CollectionUtils.getDistinct(valueList);
		ValidationUtils.assertTrue(distinctValueList.size() < 2, () -> String.format("Mismatched values were found for positions within the same position group. Values: %s. Positions: %s.", CollectionUtils.toString(distinctValueList, 3), CollectionUtils.toString(valueHolder.getPositionList(), 3)));

		// Convert result to string
		Object resultValue = CollectionUtils.getOnlyElement(distinctValueList);
		return String.valueOf(resultValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CompliancePositionPropertyPathHandler getCompliancePositionPropertyPathHandler() {
		return this.compliancePositionPropertyPathHandler;
	}


	public void setCompliancePositionPropertyPathHandler(CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler) {
		this.compliancePositionPropertyPathHandler = compliancePositionPropertyPathHandler;
	}


	public String getPositionField() {
		return this.positionField;
	}


	public void setPositionField(String positionField) {
		this.positionField = positionField;
	}


	public String getComparisonPattern() {
		return this.comparisonPattern;
	}


	public void setComparisonPattern(String comparisonPattern) {
		this.comparisonPattern = comparisonPattern;
	}


	public boolean isExcludeMatched() {
		return this.excludeMatched;
	}


	public void setExcludeMatched(boolean excludeMatched) {
		this.excludeMatched = excludeMatched;
	}
}
