package com.clifton.compliance.rule.evaluator.impl;

import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.position.ComplianceRulePosition;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


public class ComplianceRuleLongShortEvaluator implements ComplianceRuleEvaluator {

	private boolean longPositionAllowed;
	private boolean shortPositionAllowed;
	private boolean settlementDateUsed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		return evaluateRealTime(ruleEvaluationConfig);
	}


	public ComplianceRuleRun evaluateRealTime(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		boolean pass = false;

		ComplianceRuleRun ruleRun = new ComplianceRuleRun();
		ruleRun.setPass(true);

		ComplianceRuleRunDetail runDetail = new ComplianceRuleRunDetail();
		runDetail.setPass(true);
		runDetail.setComplianceRuleRun(ruleRun);
		ruleRun.addDetail(runDetail);

		// This rule does not apply if short and long is allowed
		if ((isLongPositionAllowed() && isShortPositionAllowed())) {
			pass = true;
		}
		// If it is a long buy or short sell this rule does not apply
		AccountingBean accountingBean = ruleEvaluationConfig.getBean();
		if ((accountingBean.isBuy() && isLongPositionAllowed()) || (!accountingBean.isBuy() && isShortPositionAllowed())) {
			pass = true;
		}

		String result = null;
		if (!pass) {
			BigDecimal qtyAfterTrade;
			InvestmentSecurity security = accountingBean.getInvestmentSecurity();
			InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
			if (ruleEvaluationConfig.isRealTime()) {
				qtyAfterTrade = ruleEvaluationConfig.getTradeRuleEvaluatorContext().getSharesAfterTrade(accountingBean, isSettlementDateUsed());
			}
			else if (hierarchy.isCloseOnMaturityOnly()) {
				// during batch evaluation we just want to ensure that the sum of all positions for the security in the holding account is not short/long based on ruleConfig
				List<ComplianceRulePosition> positionList = ruleEvaluationConfig.getPositionList();
				positionList = BeanUtils.filter(positionList, (ComplianceRulePosition p) ->
						p.getAccountingBean().getInvestmentSecurity().equals(security)
								&& p.getAccountingBean().getHoldingInvestmentAccount().equals(accountingBean.getHoldingInvestmentAccount())
				);
				qtyAfterTrade = CoreMathUtils.sumProperty(positionList, ComplianceRulePosition::getQuantity);
			}
			else {
				qtyAfterTrade = accountingBean.getQuantityNormalized();
			}

			if (((qtyAfterTrade.compareTo(BigDecimal.ZERO) < 0) && !isShortPositionAllowed()) || ((qtyAfterTrade.compareTo(BigDecimal.ZERO) > 0) && !isLongPositionAllowed())) {
				result = MathUtils.round(qtyAfterTrade, 0) + " " + InvestmentUtils.getQuantityFieldName(security) +
						" for " + security.getSymbol() + " open on " + DateUtils.fromDateShort(accountingBean.getTransactionDate());
				runDetail.setPass(false);
				runDetail.setDescription(result);
				ruleRun.setPass(false);
				ruleRun.setDescription(result);
			}
		}

		String positionDescription = getPositionDescription();
		runDetail.setDescription(getDetailedDescription(ruleEvaluationConfig, runDetail, result));
		if (ruleRun.getDescription() == null) {
			ruleRun.setDescription(positionDescription != null ? positionDescription : "");
		}
		else {
			ruleRun.setDescription(positionDescription != null ? ruleRun.getDescription() + " : " + positionDescription : ruleRun.getDescription());
		}
		return ruleRun;
	}


	private String getDetailedDescription(ComplianceRuleEvaluationConfig ruleEvaluationConfig, ComplianceRuleRunDetail detail, String result) {
		StringBuilder detailedDescription = new StringBuilder();
		if (ruleEvaluationConfig.getBean() instanceof AccountingPosition) {
			detailedDescription.append(((AccountingPosition) ruleEvaluationConfig.getBean()).getId()).append(": ");
		}
		//TODO - this logic/message appears to be the same as above - should decouple the detail message from the failure message and simply append the message once as needed.
		if (result == null) {
			detailedDescription.append(MathUtils.round(ruleEvaluationConfig.getBean().getQuantityNormalized(), 0)).append(" ")
					.append(InvestmentUtils.getQuantityFieldName(ruleEvaluationConfig.getBean().getInvestmentSecurity()))
					.append(" for ").append(ruleEvaluationConfig.getBean().getInvestmentSecurity().getSymbol()).append(" open on ")
					.append(DateUtils.fromDateShort(ruleEvaluationConfig.getBean().getTransactionDate()));
		}
		detailedDescription.append(detail.getDescription() != null ? ": " + detail.getDescription() : "");
		return detailedDescription.toString();
	}


	private String getPositionDescription() {
		StringBuilder description = new StringBuilder();
		if (isLongPositionAllowed()) {
			description.append("Long");
			if (isShortPositionAllowed()) {
				description.append(" and Short");
			}
			description.append(" Allowed");
		}
		else if (isShortPositionAllowed()) {
			description.append("Short Allowed");
		}
		return description.length() > 0 ? description.toString() : null;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isLongPositionAllowed() {
		return this.longPositionAllowed;
	}


	public void setLongPositionAllowed(boolean longPositionAllowed) {
		this.longPositionAllowed = longPositionAllowed;
	}


	public boolean isShortPositionAllowed() {
		return this.shortPositionAllowed;
	}


	public void setShortPositionAllowed(boolean shortPositionAllowed) {
		this.shortPositionAllowed = shortPositionAllowed;
	}


	public boolean isSettlementDateUsed() {
		return this.settlementDateUsed;
	}


	public void setSettlementDateUsed(boolean settlementDateUsed) {
		this.settlementDateUsed = settlementDateUsed;
	}
}
