package com.clifton.compliance.rule.evaluator;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.ComplianceCreditRatingService;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Compliance rule evaluator for validating credit ratings.
 * <p>
 * Evaluators of this type verify that assignee credit ratings meet the given minimum requirements. This evaluator can be used to verify that an assignee is
 * eligible for trade by client or company standards or to alert when a currently-held assignee falls beneath a given credit rating threshold. The minimum
 * requirements enforced by this evaluator are customizable.
 *
 * @param <T> the evaluator target type (e.g., {@link InvestmentSecurity} or {@link InvestmentAccount})
 * @param <S> the credit rating assignee type (e.g., {@link InvestmentSecurity} or {@link BusinessCompany})
 * @author MikeH
 */
public abstract class BaseComplianceRuleCreditRatingEvaluator<T extends NamedEntityWithoutLabel<?>, S extends NamedEntityWithoutLabel<?>> implements ComplianceRuleEvaluator, ValidationAware {


	private ComplianceCreditRatingService complianceCreditRatingService;

	private Integer creditRatingFitchId;
	private Integer creditRatingMoodysId;
	private Integer creditRatingStandardAndPoorsId;

	/**
	 * If {@code true}, a credit rating must <i>exist</i> for the assignee for each agency for which a minimum credit rating is specified.
	 * <p>
	 * This does not indicate that all minimum credit ratings must be satisfied. To indicate the number of minimum credit ratings which must be satisfied, use
	 * {@link #count}.
	 */
	private boolean requireAllRatings;

	/**
	 * The number of credit rating minimum constraints which must be satisfied in order for an assignee to pass this rule.
	 */
	private int count;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleRun evaluateRule(ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		T target = getEvaluationTarget(ruleEvaluationConfig);
		AssertUtils.assertNotNull(target, "No target was retrieved for credit rating validation. A target is required.");

		// Run evaluation
		return evaluateTarget(target, ruleEvaluationConfig);
	}


	@Override
	public void validate() throws ValidationException {
		// Validate number of minimum ratings provided
		final int numRatings = ((getCreditRatingFitchId() != null) ? 1 : 0)
				+ ((getCreditRatingMoodysId() != null) ? 1 : 0)
				+ ((getCreditRatingStandardAndPoorsId() != null) ? 1 : 0);
		ValidationUtils.assertTrue(numRatings > 0, "At least one minimum credit rating must be provided.");
		ValidationUtils.assertTrue(numRatings >= getCount(), () -> String.format("The number of ratings provided [%d] must be equal to or greater than the minimum pass count [%d].", numRatings, getCount()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the target for credit rating evaluation. For example, this may retrieve the security or company associated with the given configuration.
	 *
	 * @param ruleEvaluationConfig the configuration object
	 * @return the target for credit rating evaluation
	 */
	protected abstract T getEvaluationTarget(ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Retrieves the list of credit rating assignees for the given target. These are the assignees whose credit ratings will be validated against the
	 * configured requirements. For example, if the target is an account then the securities held by the account (or the issuers of those securities) may be
	 * returned as the credit rating assignees.
	 *
	 * @param target               the target for credit rating evaluation
	 * @param ruleEvaluationConfig the rule evaluation configuration
	 * @return the list of credit rating assignees for the given target
	 */
	protected abstract List<S> getTargetAssigneeList(T target, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Retrieves the list of credit ratings applicable to the given assignee. The list of credit ratings shall be restricted to those active on the process date
	 * given in the configuration.
	 *
	 * @param assignee             the entity currently being evaluated
	 * @param ruleEvaluationConfig the rule evaluation configuration
	 * @return the list of credit ratings which apply to the assignee on the configured date
	 */
	protected abstract List<ComplianceCreditRating> getAssigneeCreditRatingList(S assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Gets the target type for this evaluator. Implementors of this method must return a non-capitalized string characterizing the type of assignee evaluated
	 * by the implementor. This string will be used in messages built by the evaluator.
	 * <p>
	 * Examples:
	 * <ul>
	 * <li><tt>"security"</tt>
	 * <li><tt>"client account"</tt>
	 * </ul>
	 *
	 * @return the target type for this evaluator
	 */
	protected abstract String getTargetType();


	/**
	 * Gets the assignee type for this evaluator. Implementors of this method must return a non-capitalized string characterizing the type of assignee evaluated
	 * by the implementor. This string will be used in messages built by the evaluator.
	 * <p>
	 * Examples:
	 * <ul>
	 * <li><tt>"security"</tt>
	 * <li><tt>"company"</tt>
	 * </ul>
	 *
	 * @return the assignee type for this evaluator
	 */
	protected abstract String getAssigneeType();


	/**
	 * Determines if credit rating validation should be performed for the given assignee.
	 *
	 * @param assignee             the entity to be evaluated
	 * @param ruleEvaluationConfig the configuration object
	 * @return {@code true} if credit rating validation should be performed, or {@code false} otherwise
	 */
	protected abstract boolean isEvaluationRequired(S assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Gets the {@link ComplianceRuleRunDetail} description prefix. If a position is currently being evaluated, this may be an accounting position ID to
	 * facilitate automated UI linking.
	 *
	 * @param target               the target for credit rating evaluation
	 * @param assignee             the entity currently being evaluated
	 * @param ruleEvaluationConfig configuration rule configuration object
	 * @return the description prefix, as used in messages generated by the evaluator, or {@code null} if no prefix is needed
	 */
	protected abstract String getDetailDescriptionPrefix(T target, S assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig);


	/**
	 * Gets the label to use for the assignee. This label is used in description messages.
	 *
	 * @param assignee the assignee
	 * @return the label to use for the assignee
	 */
	protected abstract String getAssigneeLabel(S assignee);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Performs validation on the given target.
	 *
	 * @param target               the target to validate
	 * @param ruleEvaluationConfig the configuration object to use
	 * @return the validation results
	 */
	private ComplianceRuleRun evaluateTarget(T target, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		// Validate all credit rating assignees for the evaluation target
		ComplianceRuleRun run = generateRuleRun();
		boolean success = true;
		try {
			for (S assignee : CollectionUtils.getIterable(getTargetAssigneeList(target, ruleEvaluationConfig))) {
				// Validate assignee
				if (isEvaluationRequired(assignee, ruleEvaluationConfig)) {
					ComplianceRuleRunDetail runDetail = evaluateAssignee(target, assignee, ruleEvaluationConfig);
					addRuleRunDetail(run, runDetail);
					success = success && runDetail.isPass();
				}
			}
		}
		catch (Exception e) {
			// Unexpected exception occurred during processing
			success = false;
			String errorMsg = String.format("An error occurred while validating credit ratings for the target %s: %s", getAssigneeType(), target.getLabel());
			LogUtils.error(getClass(), errorMsg, e);
			ComplianceRuleRunDetail runDetail = generateRuleRunDetail(false, errorMsg);
			addRuleRunDetail(run, runDetail);
		}
		finally {
			run.setPass(success);
		}
		return run;
	}


	/**
	 * Performs validation on the given assignee.
	 *
	 * @param target               the target being validated
	 * @param assignee             the assignee to validate
	 * @param ruleEvaluationConfig the configuration object to use
	 * @return the validation results
	 */
	private ComplianceRuleRunDetail evaluateAssignee(T target, S assignee, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		ComplianceRuleRunDetail runDetail = generateRuleRunDetail(false, generateRuleRunDetailDescription(target, assignee, null, ruleEvaluationConfig));
		try {
			// Perform validation
			List<ComplianceCreditRating> assigneeCreditRatingList = getAssigneeCreditRatingList(assignee, ruleEvaluationConfig);
			List<ComplianceCreditRating> minimumCreditRatingList = getMinimumCreditRatingList();
			validateCreditRatings(assigneeCreditRatingList, minimumCreditRatingList);
			setRuleRunDetailStatus(runDetail, true, generateRuleRunDetailDescription(target, assignee, null, ruleEvaluationConfig));
		}
		catch (ValidationException e) {
			// Assignee credit ratings do not satisfy requirements
			setRuleRunDetailStatus(runDetail, false, generateRuleRunDetailDescription(target, assignee, e.getMessage(), ruleEvaluationConfig));
		}
		return runDetail;
	}


	/**
	 * Validates the assignee credit rating list against the minimum credit rating list using the constraint-specifics defined by the configuration of this
	 * evaluator.
	 *
	 * @param assigneeCreditRatingList the list of credit ratings for the assignee
	 * @param minimumCreditRatingList  the list of minimum credit ratings which must be satisfied
	 * @throws ValidationException on validation failure
	 */
	private void validateCreditRatings(List<ComplianceCreditRating> assigneeCreditRatingList, List<ComplianceCreditRating> minimumCreditRatingList) {
		// Index credit ratings by agency ID
		Map<BusinessCompany, ComplianceCreditRating> assigneeCreditRatingByAgency =
				BeanUtils.getBeanMapUnique(assigneeCreditRatingList, ComplianceCreditRating::getRatingAgency);
		Map<BusinessCompany, ComplianceCreditRating> minimumCreditRatingByAgency =
				BeanUtils.getBeanMapUnique(minimumCreditRatingList, ComplianceCreditRating::getRatingAgency);
		validateRequiredCreditRatingPresence(assigneeCreditRatingByAgency, minimumCreditRatingByAgency);

		// Get the number of minimum credit ratings which are satisfied
		int passedCreditRatingCount = 0;
		for (ComplianceCreditRating minimumCreditRating : minimumCreditRatingList) {
			ComplianceCreditRating assigneeCreditRating = assigneeCreditRatingByAgency.get(minimumCreditRating.getRatingAgency());
			boolean isPassed = (assigneeCreditRating != null && validateCreditRatingAgainstMinimum(assigneeCreditRating, minimumCreditRating));
			if (isPassed) {
				passedCreditRatingCount++;
			}
		}

		// Validate minimum number of satisfied credit ratings
		final int finalPassedCreditRatingCount = passedCreditRatingCount;
		ValidationUtils.assertTrue(passedCreditRatingCount >= getCount(),
				() -> String.format("The %s passed [%d] of [%d] required credit rating minimums.", getAssigneeType(), finalPassedCreditRatingCount, getCount()));
	}


	/**
	 * Validates the presence of necessary credit ratings based on the configuration for this entity.
	 *
	 * @param assigneeCreditRatingByAgency the map of assignee credit ratings by agency
	 * @param minimumCreditRatingByAgency  the map of minimum credit ratings by agency
	 * @see #isRequireAllRatings()
	 */
	private void validateRequiredCreditRatingPresence(Map<BusinessCompany, ComplianceCreditRating> assigneeCreditRatingByAgency, Map<BusinessCompany, ComplianceCreditRating> minimumCreditRatingByAgency) {
		if (isRequireAllRatings()) {
			// Determine which minimum credit ratings have matches in the list of existing credit ratings
			List<String> existingAgencyRatingList = minimumCreditRatingByAgency.entrySet().stream()
					.filter(entry -> assigneeCreditRatingByAgency.containsKey(entry.getKey()))
					.map(entry -> entry.getValue().getLabel())
					.collect(Collectors.toList());

			// Validate that an assignee credit rating exists for each of the minimum credit ratings
			ValidationUtils.assertEquals(existingAgencyRatingList.size(), minimumCreditRatingByAgency.size(),
					() -> String.format("The %s must contain ratings from all of these agencies: %s", getAssigneeType(),
							CollectionUtils.getConverted(minimumCreditRatingByAgency.values(), ComplianceCreditRating::getLabel)));
		}
	}


	/**
	 * Validates that the given assignee credit rating satisfies the given minimum credit rating constraint.
	 *
	 * @param assigneeCreditRating the credit rating to validate
	 * @param minimumCreditRating  the credit rating to compare against
	 * @return {@code true} if the given credit rating is at least as good as the given minimum credit rating, or {@code false} otherwise
	 */
	private boolean validateCreditRatingAgainstMinimum(ComplianceCreditRating assigneeCreditRating, ComplianceCreditRating minimumCreditRating) {
		/*
		 * If the credit ratings have different short-term/long-term states, attempt to find and compare the short-term equivalents. Fall-back to using original
		 * ratings if short-term equivalents cannot be found.
		 */
		ComplianceCreditRating compAssigneeCreditRating = assigneeCreditRating;
		ComplianceCreditRating compMinimumCreditRating = minimumCreditRating;
		if (assigneeCreditRating.isShortTerm() != minimumCreditRating.isShortTerm()) {
			if (!assigneeCreditRating.isShortTerm()) {
				// Get assignee short-term equivalent, or retain long-term if not found
				compAssigneeCreditRating = ObjectUtils.coalesce(assigneeCreditRating.getEquivalentShortTermRating(), assigneeCreditRating);
			}
			else if (!minimumCreditRating.isShortTerm()) {
				// Get minimum short-term equivalent, or retain long-term if not found
				compMinimumCreditRating = ObjectUtils.coalesce(minimumCreditRating.getEquivalentShortTermRating(), minimumCreditRating);
			}
		}

		return compAssigneeCreditRating.getRatingWeight() >= compMinimumCreditRating.getRatingWeight();
	}


	/**
	 * Generates the list of minimum credit ratings as configured for this rule.
	 *
	 * @return the generated list of minimum credit ratings
	 */
	private List<ComplianceCreditRating> getMinimumCreditRatingList() {
		// Add each minimum credit rating if present
		List<ComplianceCreditRating> minimumCreditRatingList = new ArrayList<>();
		ObjectUtils.map(getCreditRatingFitchId(), getComplianceCreditRatingService()::getComplianceCreditRating).thenConsume(minimumCreditRatingList::add);
		ObjectUtils.map(getCreditRatingMoodysId(), getComplianceCreditRatingService()::getComplianceCreditRating).thenConsume(minimumCreditRatingList::add);
		ObjectUtils.map(getCreditRatingStandardAndPoorsId(), getComplianceCreditRatingService()::getComplianceCreditRating).thenConsume(minimumCreditRatingList::add);
		return minimumCreditRatingList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs a {@link ComplianceRuleRun} entity using the default status and description.
	 */
	private ComplianceRuleRun generateRuleRun() {
		ComplianceRuleRun run = new ComplianceRuleRun();
		run.setLimitValue(new BigDecimal(getCount()));

		// Construct run description
		StringBuilder statusSb = new StringBuilder();
		statusSb.append(StringUtils.capitalize(getTargetType()));
		statusSb.append(" satisfies at least ").append(getCount());
		statusSb.append(" of credit ratings ").append(CollectionUtils.toString(
				CollectionUtils.getConverted(getMinimumCreditRatingList(), ComplianceCreditRating::getLabel), 3));
		run.setDescription(statusSb.toString());

		return run;
	}


	/**
	 * Creates a new {@link ComplianceRuleRunDetail} entity for the provided {@link ComplianceRuleRun}, using the default status and the given description.
	 */
	private ComplianceRuleRunDetail generateRuleRunDetail(boolean success, String description) {
		ComplianceRuleRunDetail runDetail = new ComplianceRuleRunDetail();
		setRuleRunDetailStatus(runDetail, success, description);
		return runDetail;
	}


	/**
	 * Adds the given {@link ComplianceRuleRunDetail} to the provided {@link ComplianceRuleRun}.
	 */
	private void addRuleRunDetail(ComplianceRuleRun run, ComplianceRuleRunDetail runDetail) {
		run.addDetail(runDetail);
		runDetail.setComplianceRuleRun(run);
	}


	/**
	 * Updates the given run detail entity properties. Automatically sets the success state of the owning {@link ComplianceRuleRun} object to {@code false} if
	 * the given success state is {@code false}.
	 */
	private void setRuleRunDetailStatus(ComplianceRuleRunDetail runDetail, boolean success, String description) {
		runDetail.setDescription(description);
		runDetail.setPass(success);
	}


	/**
	 * Builds a description string for this rule evaluator for a {@link ComplianceRuleRunDetail} entity. This provides a description using the expected
	 * front-end format to facilitate automated links to the trade window when viewing run details.
	 * <p>
	 * The resulting description is in the following format:
	 * <pre>
	 * <tt>&lt;Description Prefix&gt;: &lt;Assignee Label&gt; - &lt;Message&gt;</tt>
	 * </pre>
	 *
	 * @param target               the target for credit rating evaluation
	 * @param assignee             the entity currently being evaluated
	 * @param message              the description to append to the description
	 * @param ruleEvaluationConfig configuration rule configuration object
	 * @return the formatted description message
	 */
	private String generateRuleRunDetailDescription(T target, S assignee, String message, ComplianceRuleEvaluationConfig ruleEvaluationConfig) {
		StringBuilder statusSb = new StringBuilder();
		String prefix = getDetailDescriptionPrefix(target, assignee, ruleEvaluationConfig);
		ObjectUtils.doIfPresent(prefix, pfx -> statusSb.append(pfx).append(": "));
		statusSb.append(getAssigneeLabel(assignee));
		ObjectUtils.doIfPresent(message, msg -> statusSb.append(" - ").append(msg));
		return statusSb.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceCreditRatingService getComplianceCreditRatingService() {
		return this.complianceCreditRatingService;
	}


	public void setComplianceCreditRatingService(ComplianceCreditRatingService complianceCreditRatingService) {
		this.complianceCreditRatingService = complianceCreditRatingService;
	}


	public Integer getCreditRatingFitchId() {
		return this.creditRatingFitchId;
	}


	public void setCreditRatingFitchId(Integer creditRatingFitchId) {
		this.creditRatingFitchId = creditRatingFitchId;
	}


	public Integer getCreditRatingMoodysId() {
		return this.creditRatingMoodysId;
	}


	public void setCreditRatingMoodysId(Integer creditRatingMoodysId) {
		this.creditRatingMoodysId = creditRatingMoodysId;
	}


	public Integer getCreditRatingStandardAndPoorsId() {
		return this.creditRatingStandardAndPoorsId;
	}


	public void setCreditRatingStandardAndPoorsId(Integer creditRatingStandardAndPoorsId) {
		this.creditRatingStandardAndPoorsId = creditRatingStandardAndPoorsId;
	}


	public boolean isRequireAllRatings() {
		return this.requireAllRatings;
	}


	public void setRequireAllRatings(boolean requireAllRatings) {
		this.requireAllRatings = requireAllRatings;
	}


	public int getCount() {
		return this.count;
	}


	public void setCount(int count) {
		this.count = count;
	}
}
