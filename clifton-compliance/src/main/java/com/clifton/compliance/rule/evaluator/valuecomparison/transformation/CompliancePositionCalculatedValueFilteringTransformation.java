package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.BaseComplianceRuleValueCalculator;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleAccountingPositionValueCalculator;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleValueCalculator;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.ComparisonTypes;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionTransformation} bean type for filtering positions based on a logical comparison against a calculated value.
 * <p>
 * This transformation compares the value of each {@link CompliancePositionValueHolder} against a comparison value. The comparison value is the evaluation result from a selected
 * {@link ComplianceRuleValueCalculator} evaluated in the current context.
 *
 * @author MikeH
 */
public class CompliancePositionCalculatedValueFilteringTransformation implements CompliancePositionTransformation, ValidationAware {

	private SystemBeanService systemBeanService;

	private ComparisonTypes comparisonType;
	private SystemBean comparisonCalculator;
	private BigDecimal multiplier;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		ComplianceRuleEvaluationConfig ruleEvaluationConfig = (ComplianceRuleEvaluationConfig) context.getBean(BaseComplianceRuleValueCalculator.CONTEXT_RULE_CONFIG);
		ComplianceRuleValueCalculator generatedComparisonCalculator = generateValueCalculator(getComparisonCalculator());
		BigDecimal compareCalculatedValue = generatedComparisonCalculator.calculate(ruleEvaluationConfig, runSubDetailList);
		BigDecimal compareResultValue = MathUtils.multiply(compareCalculatedValue, getMultiplier());
		return CollectionUtils.getStream(positionValueList)
				.filter(positionValue -> getComparisonType().compare(positionValue.getValue(), compareResultValue))
				.collect(Collectors.toList());
	}


	@Override
	public void validate() throws ValidationException {
		validateSavedStateRecursion(new ArrayDeque<>(), generateValueCalculator(getComparisonCalculator()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Performs best-effort validation of any recursive calls detected in the saved state of the current entity.
	 * <p>
	 * Validation cannot be guaranteed because actual execution may differ from the saved state and because other instances of potential recursion may exist that are not known to
	 * this method. Since validation cannot be guaranteed during save, this class relies on additional runtime validation implemented in {@link BaseComplianceRuleValueCalculator}.
	 * <p>
	 * This method attempts to validate the following constraints:
	 * <ul>
	 * <li>The executing calculator does not already exist in the calculator stack. This would indicate an infinite loop.
	 * <li>The size of the stack is less than {@link BaseComplianceRuleValueCalculator#CALCULATOR_MAX_RECURSION_DEPTH}. This limit is used for performance and as a fall-back for
	 * preventing infinite loops that would not otherwise be detected.
	 * </ul>
	 *
	 * @param calculatorStack the call stack of calculators that would currently be in execution during the emulation
	 * @param calculator      the current calculator to execute
	 */
	private void validateSavedStateRecursion(Deque<ComplianceRuleValueCalculator> calculatorStack, ComplianceRuleValueCalculator calculator) {
		calculatorStack.push(calculator);

		// Validate recursion depth; this also provides a fall-back in case by-reference calculator comparisons fail due to issues with the singleton bean cache
		Integer maxRecursion = BaseComplianceRuleValueCalculator.CALCULATOR_MAX_RECURSION_DEPTH;
		ValidationUtils.assertFalse(calculatorStack.size() > maxRecursion - 1, // Assume that the current level includes a calculator
				() -> String.format("The maximum position value calculator recursion depth (%d) is exceeded.", maxRecursion));

		// Find and validate recursive calculator calls for calculators which may use recursion
		if (calculator instanceof ComplianceRuleAccountingPositionValueCalculator) {
			ComplianceRuleAccountingPositionValueCalculator positionValueCalculator = (ComplianceRuleAccountingPositionValueCalculator) calculator;
			for (SystemBean calculatorBean : CollectionUtils.getIterable(positionValueCalculator.getTransformationList())) {
				CompliancePositionTransformation transformation = (CompliancePositionTransformation) getSystemBeanService().getBeanInstance(calculatorBean);
				if (transformation instanceof CompliancePositionCalculatedValueFilteringTransformation) {
					// Detect recursion at current level using by-reference comparison
					SystemBean childCalculatorBean = ((CompliancePositionCalculatedValueFilteringTransformation) transformation).getComparisonCalculator();
					ComplianceRuleValueCalculator childCalculator = generateValueCalculator(childCalculatorBean);
					ValidationUtils.assertFalse(calculatorStack.contains(childCalculator), () -> String.format("Recursion detected in the [%s] calculator. Recursive calls are not allowed.", childCalculatorBean.getName()));

					// Validate recursion within the child calculator
					validateSavedStateRecursion(calculatorStack, childCalculator);
				}
			}
		}

		calculatorStack.pop();
	}


	/**
	 * Generates the {@link ComplianceRuleValueCalculator} entity from the given bean.
	 */
	private ComplianceRuleValueCalculator generateValueCalculator(SystemBean calculatorBean) {
		return (ComplianceRuleValueCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ComparisonTypes getComparisonType() {
		return this.comparisonType;
	}


	public void setComparisonType(ComparisonTypes comparisonType) {
		this.comparisonType = comparisonType;
	}


	public SystemBean getComparisonCalculator() {
		return this.comparisonCalculator;
	}


	public void setComparisonCalculator(SystemBean comparisonCalculator) {
		this.comparisonCalculator = comparisonCalculator;
	}


	public BigDecimal getMultiplier() {
		return this.multiplier;
	}


	public void setMultiplier(BigDecimal multiplier) {
		this.multiplier = multiplier;
	}
}
