package com.clifton.compliance.rule.limit.typelimit;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ComplianceRuleTypeLimitSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "complianceRuleType.id")
	private Short complianceRuleTypeId;

	@SearchField(searchField = "complianceRuleType.name")
	private String complianceRuleTypeName;

	@SearchField(searchField = "complianceRuleLimitType.id")
	private Short complianceRuleLimitTypeId;

	@SearchField(searchField = "complianceRuleLimitType.name")
	private String complianceRuleLimitTypeName;


	public Short getComplianceRuleTypeId() {
		return this.complianceRuleTypeId;
	}


	public void setComplianceRuleTypeId(Short complianceRuleTypeId) {
		this.complianceRuleTypeId = complianceRuleTypeId;
	}


	public String getComplianceRuleTypeName() {
		return this.complianceRuleTypeName;
	}


	public void setComplianceRuleTypeName(String complianceRuleTypeName) {
		this.complianceRuleTypeName = complianceRuleTypeName;
	}


	public Short getComplianceRuleLimitTypeId() {
		return this.complianceRuleLimitTypeId;
	}


	public void setComplianceRuleLimitTypeId(Short complianceRuleLimitTypeId) {
		this.complianceRuleLimitTypeId = complianceRuleLimitTypeId;
	}


	public String getComplianceRuleLimitTypeName() {
		return this.complianceRuleLimitTypeName;
	}


	public void setComplianceRuleLimitTypeName(String complianceRuleLimitTypeName) {
		this.complianceRuleLimitTypeName = complianceRuleLimitTypeName;
	}
}
