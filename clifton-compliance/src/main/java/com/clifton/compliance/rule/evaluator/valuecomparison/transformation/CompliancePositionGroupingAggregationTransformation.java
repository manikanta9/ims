package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.context.Context;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.math.MathTransformationTypes;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionTransformation} bean type for grouping positions into {@link CompliancePositionValueHolder} objects.
 * <p>
 * This bean type is used for grouping and aggregating positions and their values. Positions may be grouped by their {@link #groupingFields fields}, including market data fields.
 * Grouped positions are aggregated by the specified {@link #aggregationOperation aggregation operation} in order to produce a single value for each group.
 *
 * @author MikeH
 */
public class CompliancePositionGroupingAggregationTransformation implements CompliancePositionTransformation, ValidationAware {

	private CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler;

	/**
	 * The list of field paths by which the provided positions will be grouped.
	 * <p>
	 * Groupings shall be applied by finding all positions which have the same values for <i>all</i> of the given grouping fields. These positions will be grouped into a single
	 * {@link CompliancePositionValueHolder}. Their values shall be aggregated via the given {@link #aggregationOperation} aggregation operation.
	 * <p>
	 * Groupings must be a subset of all previously-applied groupings. For example, if <tt>investmentSecurity.id</tt> is included in this grouping transformation then it must also
	 * be included in all previous grouping transformations, if any exist.
	 * <p>
	 * Special syntaxes may be used to retrieve non-standard fields. See {@link CompliancePositionPropertyPathHandler#getPositionPropertyValue(AccountingPosition, String, Date)} for more information.
	 */
	private String[] groupingFields;
	/**
	 * The aggregation operation which will be used when aggregating positions into groups. Since each group must have a single value, this operation shall be used to combine
	 * position values into a single value.
	 */
	private AggregationOperations aggregationOperation;
	/**
	 * The transformation type which shall be applied to each group. This is applied after position values have been aggregated into their corresponding groups.
	 */
	private MathTransformationTypes transformationType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueHolderList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		validateValueHolderList(positionValueHolderList);

		// Group positions into bags according to the grouping fields
		Map<PositionPropertyKey, PositionValueHolderGroup> groupedPositionValueHolderMap = getGroupedPositionValueHolderMap(new PositionValueHolderGroup(positionValueHolderList), processDate);

		// Apply combinations and operations for each position value holder list group
		return combineGroupedPositionValueHolderList(groupedPositionValueHolderMap.values());
	}


	@Override
	public void validate() throws ValidationException {
		// Guard-clause: No validation necessary
		if (getGroupingFields() == null) {
			return;
		}

		// Validate all provided grouping fields
		List<String> missingFieldMessages = new ArrayList<>();
		for (String field : getGroupingFields()) {
			try {
				getCompliancePositionPropertyPathHandler().validateField(field);
			}
			catch (ValidationException e) {
				missingFieldMessages.add(String.format("[%s]: %s", field, e.getMessage()));
			}
		}

		// Fail if any errors were found
		if (!CollectionUtils.isEmpty(missingFieldMessages)) {
			String missingFieldMessageHtml = CollectionUtils.getStream(missingFieldMessages)
					.collect(Collectors.joining("</li><li>", "Invalid grouping field(s): <ul><li>", "</li></ul>"));
			ValidationUtils.fail("%s", missingFieldMessageHtml);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the map of grouped position value holders. Each key in this group corresponds to a unique {@link PositionPropertyKey key} for the configured {@link #groupingFields
	 * grouping fields}.
	 *
	 * @param valueHolderGroup the collection of all position value holders
	 * @param processDate      the process date for which to generate the keys for the value holders
	 * @return the map of position value holder groups grouped by {@link PositionPropertyKey}
	 */
	private Map<PositionPropertyKey, PositionValueHolderGroup> getGroupedPositionValueHolderMap(PositionValueHolderGroup valueHolderGroup, Date processDate) {
		List<String> fields = Arrays.asList(getGroupingFields());
		return CollectionUtils.getStream(valueHolderGroup)
				.collect(Collectors.groupingBy(valueHolder -> generateKeyFromFieldValues(valueHolder, fields, processDate),
						Collectors.toCollection(PositionValueHolderGroup::new)));
	}


	/**
	 * Combines the provided groups of position value holders. Each group shall be combined into a single {@link CompliancePositionValueHolder} according to the selected {@link
	 * #aggregationOperation aggregation operation}.
	 *
	 * @param valueHolderGroupList the collection of position value holder groups, each of which shall have its child position value holders aggregated into a single value holder
	 * @return the resulting collection of value holders
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> combineGroupedPositionValueHolderList(Collection<PositionValueHolderGroup> valueHolderGroupList) {
		return CollectionUtils.getStream(valueHolderGroupList)
				// Combine the values within each bag into a single position value holder
				.map(this::aggregatePositionValueHolderList)
				// Transform each value using the supplied transformation
				.map(valueHolder -> new CompliancePositionValueHolder<>(getTransformationType().transform(valueHolder.getValue()), valueHolder.getPositionList()))
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Combines the group of position value holders into a single position value holder. This uses the selected aggregation operator to combine all position values into a single
	 * position value. The new position value holder will contain the concatenated positions for all of the provided position value holders.
	 *
	 * @param valueHolderGroup the collection of position value holders to combine
	 * @return the aggregated position value holder
	 */
	private CompliancePositionValueHolder<AccountingPosition> aggregatePositionValueHolderList(PositionValueHolderGroup valueHolderGroup) {
		// Combine positions into a single list
		List<AccountingPosition> positionList = CollectionUtils.getStream(valueHolderGroup)
				.map(CompliancePositionValueHolder::getPositionList)
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());

		// Aggregate values
		List<BigDecimal> positionValueList = CollectionUtils.getConverted(valueHolderGroup, CompliancePositionValueHolder::getValue);
		BigDecimal positionValue = getAggregationOperation().aggregate(positionValueList);

		// Generate new position value holder
		return new CompliancePositionValueHolder<>(positionValue, positionList);
	}


	/**
	 * Generates the key for the given position value holder on the given fields using properties from the positions. This key will be identical for all position value holders with
	 * the same values for the given fields.
	 * <p>
	 * This method will fail if the values for the given fields are not equal for all positions. In order to ensure that the given fields are equal for all positions, the given set
	 * of fields should be a subset of all previous grouping operators.
	 *
	 * @param positionValueHolder the position value holder for which to generate the key
	 * @param fieldList           the list of fields which will be used in the key
	 * @param processDate         the process date which will be used when retrieving property values
	 * @return the key for the position value holder
	 */
	private PositionPropertyKey generateKeyFromFieldValues(CompliancePositionValueHolder<AccountingPosition> positionValueHolder, Collection<String> fieldList, Date processDate) {
		// Convert position list to map of fields -> properties
		List<PositionPropertyKey> propertyMapList = CollectionUtils.getStream(positionValueHolder.getPositionList())
				// Obtain the property map for the position
				.map(position -> getPositionPropertyKey(position, fieldList, processDate))
				// Find all different property maps
				.distinct()
				.collect(Collectors.toList());

		// Verify that only one distinct property map is found
		AssertUtils.assertTrue(CollectionUtils.getSize(propertyMapList) == 1, "Non-identical values found while attempting to group by fields [%s]. Verify that the grouping fields currently being applied are a subset of any previously-applied grouping fields.", fieldList);
		return propertyMapList.get(0);
	}


	/**
	 * Retrieves the {@link  PositionPropertyKey} for the position and the given fields.
	 *
	 * @param position    the position for which to retrieve property values
	 * @param fieldList   the list of fields for which to retrieve properties
	 * @param processDate the process date which will be used when retrieving property values
	 * @return the property key for the given position
	 */
	private PositionPropertyKey getPositionPropertyKey(AccountingPosition position, Collection<String> fieldList, Date processDate) {
		return new PositionPropertyKey(CollectionUtils.getMapped(fieldList, field -> getCompliancePositionPropertyPathHandler().getPositionPropertyValue(position, field, processDate)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that the list of {@link CompliancePositionValueHolder} entities satisfies runtime constraints.
	 * <p>
	 * This is a runtime validation.
	 *
	 * @param valueHolderList the list of {@link CompliancePositionValueHolder} entities that are being evaluated
	 */
	private void validateValueHolderList(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList) {
		// Do not allow "dummy" value holders
		boolean emptyValueHolderExists = CollectionUtils.getStream(valueHolderList)
				.map(CompliancePositionValueHolder::getPositionList)
				.anyMatch(List::isEmpty);
		AssertUtils.assertFalse(emptyValueHolderExists, "One or more empty groupings were encountered while attempting to determine representative values for each" +
				" group. Please verify that no prior transformations exist that may create empty dummy groups.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The type representing the property key for a single position or a group of positions. This map is a one-to-one mapping between the given fields and their corresponding
	 * values for a given position.
	 * <p>
	 * The resulting map uses the following structure:
	 * <ul>
	 * <li>Key: Property field name
	 * <li>Value: Property value
	 * </ul>
	 *
	 * @see #getPositionPropertyKey(AccountingPosition, Collection, Date)
	 */
	private static class PositionPropertyKey extends HashMap<String, Object> {

		private PositionPropertyKey(Map<String, Object> map) {
			super(map);
		}
	}


	/**
	 * The type representing a collection of {@link CompliancePositionValueHolder} entities.
	 * <p>
	 * This type, as its name suggests, is intended to represent a single group of value holders. This group is intended to be a collection of value holders that is grouped by a
	 * common {@link PositionPropertyKey}.
	 */
	private static class PositionValueHolderGroup extends ArrayList<CompliancePositionValueHolder<AccountingPosition>> {

		private PositionValueHolderGroup() {
		}


		private PositionValueHolderGroup(Collection<CompliancePositionValueHolder<AccountingPosition>> collection) {
			super(collection);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getGroupingFields() {
		return this.groupingFields;
	}


	public void setGroupingFields(String[] groupingFields) {
		this.groupingFields = groupingFields;
	}


	public AggregationOperations getAggregationOperation() {
		return this.aggregationOperation;
	}


	public void setAggregationOperation(AggregationOperations aggregationOperation) {
		this.aggregationOperation = aggregationOperation;
	}


	public MathTransformationTypes getTransformationType() {
		return this.transformationType;
	}


	public void setTransformationType(MathTransformationTypes transformationType) {
		this.transformationType = transformationType;
	}


	public CompliancePositionPropertyPathHandler getCompliancePositionPropertyPathHandler() {
		return this.compliancePositionPropertyPathHandler;
	}


	public void setCompliancePositionPropertyPathHandler(CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler) {
		this.compliancePositionPropertyPathHandler = compliancePositionPropertyPathHandler;
	}
}
