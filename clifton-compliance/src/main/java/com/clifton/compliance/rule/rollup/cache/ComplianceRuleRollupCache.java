package com.clifton.compliance.rule.rollup.cache;

import com.clifton.compliance.rule.rollup.ComplianceRuleRollup;

import java.util.List;


/**
 * @author manderson
 */
public interface ComplianceRuleRollupCache {

	public Integer[] getComplianceRuleRollupList(int complianceRuleId);


	public void storeComplianceRuleRollupList(int complianceRuleId, List<ComplianceRuleRollup> list);
}
