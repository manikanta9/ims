package com.clifton.compliance.rule;


import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentBulk;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.compliance.rule.rollup.ComplianceRuleRollup;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.rule.type.ComplianceRuleTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.Date;
import java.util.List;


public interface ComplianceRuleService {

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Assignment Business Methods   ///////// 
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleAssignment getComplianceRuleAssignment(int id);


	public List<ComplianceRuleAssignment> getComplianceRuleAssignmentList(ComplianceRuleAssignmentSearchForm searchForm);


	public void saveComplianceRuleAssignmentBulk(ComplianceRuleAssignmentBulk bean);


	public void copyComplianceRuleAssignment(int fromClientAccountId, int[] toClientAccountIds, Date startDate);


	public ComplianceRuleAssignment saveComplianceRuleAssignment(ComplianceRuleAssignment assignment);


	public void deleteComplianceRuleAssignment(int id);


	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Business Methods              ///////// 
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRule getComplianceRule(int id);


	/**
	 * Returns a list of all child rules directly assigned to the given rule (through {@link ComplianceRuleRollup}
	 */
	@DoNotAddRequestMapping
	public List<ComplianceRule> getComplianceRuleChildrenList(ComplianceRule rule);


	/**
	 * Recursively searches through a compliance rule's tree of compliance rule children and returns the full list of all children of the rollup rule.
	 */
	@DoNotAddRequestMapping
	public List<ComplianceRule> getComplianceRuleFullChildrenList(ComplianceRule rule);


	public List<ComplianceRule> getComplianceRuleList(ComplianceRuleSearchForm searchForm);


	public ComplianceRule saveComplianceRule(ComplianceRule bean);


	public void deleteComplianceRule(int id);


	////////////////////////////////////////////////////////////////////////////
	///////           Compliance Rule Type Business Methods            ///////// 
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleType getComplianceRuleType(short id);


	public ComplianceRuleType getComplianceRuleTypeByName(String name);


	public List<ComplianceRuleType> getComplianceRuleTypeList(final ComplianceRuleTypeSearchForm searchForm);
}
