package com.clifton.compliance.old.rule.evaluator;


import com.clifton.accounting.AccountingBean;
import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ComplianceShortLongChangeRestrictionGenerator</code> generates a warning if the trade
 * will change current position from long to short and vice versa
 *
 * @author manderson
 */
public class ComplianceShortLongChangeRestrictionRuleEvaluator<T extends IdentityObject & AccountingBean> implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private RuleViolationService ruleViolationService;

	/**
	 * Use settlement date to look up positions.
	 */
	private boolean useSettlementDate = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T bean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Integer linkedFkFieldId = (Integer) bean.getIdentity();
			BigDecimal beforeQuantity = context.getSharesBeforeTrade(bean, isUseSettlementDate());
			// Continue only if we had an existing position
			if (!MathUtils.isNullOrZero(beforeQuantity)) {
				BigDecimal afterQuantity = context.getSharesAfterTrade(bean, isUseSettlementDate());
				// Continue only if not just closing the existing position
				if (!MathUtils.isNullOrZero(afterQuantity)) {
					boolean beforeLong = MathUtils.isGreaterThan(beforeQuantity, BigDecimal.ZERO);
					boolean afterLong = MathUtils.isGreaterThan(afterQuantity, BigDecimal.ZERO);
					if (beforeLong != afterLong) {
						Integer causeFkFieldId = null;
						if (!MathUtils.isEqual(bean.getSourceEntityId(), linkedFkFieldId)) {
							causeFkFieldId = bean.getSourceEntityId();
						}
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("afterLong", afterLong);
						templateValues.put("afterQuantity", afterQuantity);
						templateValues.put("beforeLong", beforeLong);
						templateValues.put("beforeQuantity", beforeQuantity);
						if (causeFkFieldId != null) {
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, linkedFkFieldId, causeFkFieldId, null, templateValues));
						}
						else {
							ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, templateValues));
						}
					}
				}
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}
}
