package com.clifton.compliance.old.exchangelimit.position;

import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitService;
import com.clifton.compliance.old.exchangelimit.position.calculators.CompliancePositionExchangeLimitTypeCalculator;
import com.clifton.compliance.old.exchangelimit.position.calculators.CompliancePositionExchangeLimitTypeCalculatorLocator;
import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitPositionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>CompliancePositionExchangeLimitPositionServiceImpl</code> class
 *
 * @author manderson
 */
@Service
public class CompliancePositionExchangeLimitPositionServiceImpl implements CompliancePositionExchangeLimitPositionService {

	private AccountingPositionService accountingPositionService;

	private CompliancePositionExchangeLimitService compliancePositionExchangeLimitService;
	private CompliancePositionExchangeLimitTypeCalculatorLocator compliancePositionExchangeLimitTypeCalculatorLocator;

	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	///////     Compliance Position Exchange Limit Position Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true) // use first level cache and avoid new transaction creation for each DB call
	public List<CompliancePositionExchangeLimitPosition> getCompliancePositionExchangeLimitPositionList(CompliancePositionExchangeLimitPositionSearchForm searchForm, boolean excludePendingTrades) {
		ValidationUtils.assertNotNull(searchForm.getActiveOnDate(), "In order to view positions vs. limit, please enter a date to search against.");

		// Percent of Limit is a field that needs to be filtered on for notifications and in UI sorting is important, so implementing special logic
		// Unfortunately, can only apply this filter after getting all.
		BigDecimal minPercent = null;
		BigDecimal maxPercent = null;
		BigDecimal exactPercent = null;
		OrderByField percentOfLimitOrder = null;

		// Check the order by string first to see if the column is there...recreate order by that gets sent to hibernate so the percent field isn't passed
		if (!StringUtils.isEmpty(searchForm.getOrderBy()) && searchForm.getOrderBy().contains("percentOfLimit")) {
			List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
			StringBuilder orderBy = new StringBuilder();
			for (OrderByField field : CollectionUtils.getIterable(orderByList)) {
				if ("percentOfLimit".equals(field.getName())) {
					percentOfLimitOrder = field;
				}
				else {
					orderBy.append(field.getName()).append(':').append(field.getDirection()).append('#');
				}
			}
			searchForm.setOrderBy(orderBy.toString());
		}

		for (SearchRestriction restriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			if ("percentOfLimit".equals(restriction.getField()) && restriction.getValue() != null) {
				BigDecimal value;
				if (restriction.getValue() instanceof BigDecimal) {
					value = (BigDecimal) restriction.getValue();
				}
				else {
					value = new BigDecimal(restriction.getValue().toString());
				}
				if (ComparisonConditions.GREATER_THAN == restriction.getComparison()) {
					minPercent = value;
				}
				else if (ComparisonConditions.LESS_THAN == restriction.getComparison()) {
					maxPercent = value;
				}
				else if (ComparisonConditions.EQUALS == restriction.getComparison()) {
					exactPercent = value;
				}
			}
		}
		searchForm.removeSearchRestriction("percentOfLimit");

		// Track Start/Limit and Reset On SearchForm so Exchange Limits aren't filtered by this:
		int start = searchForm.getStart();
		int pageSize = searchForm.getLimit();

		searchForm.setStart(PagingCommand.DEFAULT_START);
		searchForm.setLimit(PagingCommand.DEFAULT_LIMIT);

		List<CompliancePositionExchangeLimit> limitList = getCompliancePositionExchangeLimitService().getCompliancePositionExchangeLimitList(searchForm);
		List<CompliancePositionExchangeLimitPosition> list = getCompliancePositionExchangeLimitPositionListImpl(limitList, searchForm.getActiveOnDate(), excludePendingTrades);

		if (minPercent != null || maxPercent != null || exactPercent != null) {
			List<CompliancePositionExchangeLimitPosition> filteredList = new ArrayList<>();
			for (CompliancePositionExchangeLimitPosition limitPosition : CollectionUtils.getIterable(list)) {
				if (exactPercent != null) {
					if (MathUtils.isEqual(limitPosition.getPercentOfLimit(), exactPercent)) {
						filteredList.add(limitPosition);
					}
				}
				else {
					if (MathUtils.isGreaterThan(limitPosition.getPercentOfLimit(), minPercent)) {
						if (maxPercent == null || MathUtils.isLessThan(limitPosition.getPercentOfLimit(), maxPercent)) {
							filteredList.add(limitPosition);
						}
					}
				}
			}
			list = filteredList;
		}

		if (percentOfLimitOrder != null) {
			list = BeanUtils.sortWithFunction(list, CompliancePositionExchangeLimitPosition::getPercentOfLimit, OrderByDirections.ASC == percentOfLimitOrder.getDirection());
		}

		// Reset Limits on the Search Form to what they were
		searchForm.setStart(start);
		searchForm.setLimit(pageSize);

		// TODO - IF COMMON - REFACTOR TO CORE COLLECTION UTILS SO CAN BE RE-USED...

		// Need to return PagingArrayList with proper start/size/page size so filtering in UI works properly
		// If more than one page of data
		int size = CollectionUtils.getSize(list);
		if (size > pageSize) {
			int maxIndex = start + pageSize;
			if (maxIndex > size) {
				maxIndex = size;
			}
			// Filter actual rows returned to the current page
			list = list.subList(start, maxIndex);
		}
		// Return Paging Array List with (if more than one page, then list is already a subset of data, else all the data), start index and the total size of the original list without paging
		return new PagingArrayList<>(list, start, size);
	}


	/**
	 * Returns limits defined for the trade's security's instrument ONLY if the limit is affected by the trade.
	 * NOTE: Always includes pending trades to ensure limits as this is used to block trades that have crossed the limit
	 * <p>
	 * !!! If the accounting bean passed is a Trade, then pending Trades will EXCLUDE the specified Trade !!!
	 * <p>
	 * i.e. Spot Month
	 *
	 * @param accountingBean              (i.e. Trade)
	 * @param includeAccountabilityLimits - by default we wouldn't want to stop trading for accountability limits, but gives the option
	 */
	@Override
	public List<CompliancePositionExchangeLimitPosition> getCompliancePositionExchangeLimitPositionListForAccountingBean(AccountingBean accountingBean, boolean includeAccountabilityLimits) {
		List<CompliancePositionExchangeLimit> limitList = getCompliancePositionExchangeLimitService().getCompliancePositionExchangeLimitListForInstrumentOrBigInstrument(accountingBean.getInvestmentSecurity().getInstrument().getId(), accountingBean.getTransactionDate(), includeAccountabilityLimits);
		List<CompliancePositionExchangeLimitPosition> positionLimitList = new ArrayList<>();

		List<AccountingPosition> instrumentPositionList = null;
		List<Trade> instrumentPendingTradeList = null;
		boolean positionsRetrieved = false;

		for (CompliancePositionExchangeLimit limit : CollectionUtils.getIterable(limitList)) {
			CompliancePositionExchangeLimitTypeCalculator calculator = getCompliancePositionExchangeLimitTypeCalculatorLocator().locate(limit.getLimitType());
			if (calculator.isCompliancePositionExchangeLimitApplied(limit, accountingBean)) {
				if (!positionsRetrieved) {
					// Look up positions by instrument & big Instrument
					instrumentPositionList = getAccountingPositionListForInstrumentAndBigInstrument(limit.getInstrument(), accountingBean.getTransactionDate());
					// Look up trades by instrument & big Instrument
					instrumentPendingTradeList = getTradePendingListForInstrumentAndBigInstrument(limit.getInstrument(), accountingBean);
					positionsRetrieved = true;
				}
				positionLimitList.addAll(calculator.calculate(limit, accountingBean.getTransactionDate(), instrumentPositionList, instrumentPendingTradeList));
			}
		}
		return positionLimitList;
	}


	private List<CompliancePositionExchangeLimitPosition> getCompliancePositionExchangeLimitPositionListImpl(List<CompliancePositionExchangeLimit> limitList, Date date, boolean excludePendingTrades) {
		ValidationUtils.assertNotNull(date, "In order to view positions vs. limit, please enter a date to search against.");
		List<CompliancePositionExchangeLimitPosition> positionLimitList = new ArrayList<>();

		Map<Integer, List<Trade>> instrumentPendingTradeMap = (excludePendingTrades ? null : getInstrumentTradePendingMap());
		Map<Integer, List<AccountingPosition>> instrumentPositionListMap = getInstrumentAccountingPositionListMapForExchangeLimitList(limitList, date);

		for (CompliancePositionExchangeLimit limit : CollectionUtils.getIterable(limitList)) {
			CompliancePositionExchangeLimitTypeCalculator calculator = getCompliancePositionExchangeLimitTypeCalculatorLocator().locate(limit.getLimitType());

			List<AccountingPosition> instrumentPositionList = CollectionUtils.getValue(instrumentPositionListMap, limit.getInstrument().getId(), Collections::emptyList);

			List<Trade> instrumentPendingTradeList = null;
			if (!excludePendingTrades) {
				instrumentPendingTradeList = instrumentPendingTradeMap.get(limit.getInstrument().getId());
				if (limit.getInstrument().getBigInstrument() != null) {
					List<Trade> bigInstrumentPendingTradeList = instrumentPendingTradeMap.get(limit.getInstrument().getBigInstrument().getId());
					if (!CollectionUtils.isEmpty(bigInstrumentPendingTradeList)) {
						if (CollectionUtils.isEmpty(instrumentPendingTradeList)) {
							instrumentPendingTradeList = bigInstrumentPendingTradeList;
						}
						else {
							instrumentPendingTradeList.addAll(bigInstrumentPendingTradeList);
						}
					}
				}
			}

			positionLimitList.addAll(calculator.calculate(limit, date, instrumentPositionList, instrumentPendingTradeList));
		}

		return positionLimitList;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Related Methods               /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPosition> getCompliancePositionExchangeLimitAccountingPositionList(int limitId, Date date) {
		ValidationUtils.assertNotNull(date, "A date to view open positions for is required");
		CompliancePositionExchangeLimit limit = getCompliancePositionExchangeLimitService().getCompliancePositionExchangeLimit(limitId);
		return getAccountingPositionListForInstrumentAndBigInstrument(limit.getInstrument(), date);
	}


	private List<AccountingPosition> getAccountingPositionListForInstrumentAndBigInstrument(InvestmentInstrument instrument, Date date) {
		return getAccountingPositionService().getAccountingPositionListForInstrument(instrument.getId(), date, true);
	}


	/**
	 * Looks up {@link AccountingPosition}s for all {@link InvestmentInstrument}s referenced in the list of {@link CompliancePositionExchangeLimit}s.
	 * The returned positions are mapped by instrument ID.
	 */
	private Map<Integer, List<AccountingPosition>> getInstrumentAccountingPositionListMapForExchangeLimitList(List<CompliancePositionExchangeLimit> limitList, Date date) {
		if (CollectionUtils.isEmpty(limitList)) {
			return Collections.emptyMap();
		}

		Integer[] instrumentIds = CollectionUtils.getStream(limitList)
				.map(limit -> limit.getInstrument().getId())
				.toArray(Integer[]::new);

		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(date);
		positionCommand.setInvestmentInstrumentIds(instrumentIds);
		positionCommand.setIncludeBigInstrumentPositions(true);

		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand);
		return BeanUtils.getBeansMap(positionList, position -> position.getInvestmentSecurity().getInstrument().getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                Trade Related Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Trade> getCompliancePositionExchangeLimitPendingTradeList(int limitId) {
		CompliancePositionExchangeLimit limit = getCompliancePositionExchangeLimitService().getCompliancePositionExchangeLimit(limitId);
		return getTradePendingListForInstrumentAndBigInstrument(limit.getInstrument(), null);
	}


	private List<Trade> getTradePendingListForInstrumentAndBigInstrument(InvestmentInstrument instrument, AccountingBean excludeAccountingBean) {
		TradeSearchForm searchForm = new TradeSearchForm();
		if (excludeAccountingBean != null && excludeAccountingBean.getSourceEntityId() != null && excludeAccountingBean.getClass().equals(Trade.class)) {
			searchForm.setExcludeId(excludeAccountingBean.getSourceEntityId());
		}
		searchForm.setIncludeBigTradesInvestmentInstrumentId(instrument.getId());
		searchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		return getTradeService().getTradeList(searchForm);
	}


	/**
	 * Useful when looking up limits across instruments - returns a map of instrument to a list of pending trades for that instrument
	 * Note: Does not include big instruments in the instrument's key - calling method will need to check if instrument has a big instrument, to get their pending trades as well
	 */
	private Map<Integer, List<Trade>> getInstrumentTradePendingMap() {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);

		return BeanUtils.getBeansMap(tradeList, trade -> trade.getInvestmentSecurity().getInstrument().getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public CompliancePositionExchangeLimitService getCompliancePositionExchangeLimitService() {
		return this.compliancePositionExchangeLimitService;
	}


	public void setCompliancePositionExchangeLimitService(CompliancePositionExchangeLimitService compliancePositionExchangeLimitService) {
		this.compliancePositionExchangeLimitService = compliancePositionExchangeLimitService;
	}


	public CompliancePositionExchangeLimitTypeCalculatorLocator getCompliancePositionExchangeLimitTypeCalculatorLocator() {
		return this.compliancePositionExchangeLimitTypeCalculatorLocator;
	}


	public void setCompliancePositionExchangeLimitTypeCalculatorLocator(CompliancePositionExchangeLimitTypeCalculatorLocator compliancePositionExchangeLimitTypeCalculatorLocator) {
		this.compliancePositionExchangeLimitTypeCalculatorLocator = compliancePositionExchangeLimitTypeCalculatorLocator;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
