package com.clifton.compliance.old.exchangelimit;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrument;

import java.util.Date;


/**
 * The <code>CompliancePositionExchangeLimit</code> ...
 *
 * @author Mary Anderson
 */
public class CompliancePositionExchangeLimit extends BaseEntity<Integer> implements LabeledObject, DocumentFileCountAware {

	private CompliancePositionExchangeLimitTypes limitType;
	private InvestmentInstrument instrument;

	/**
	 * Dates this limit is effective for *
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * Optional description of this limit
	 */
	private String description;

	/**
	 * Total number of attachments tied to the exchange limit
	 */
	private Short documentFileCount;

	/**
	 * If true, this is a soft warning, not a hard stop
	 * Generally at this point, the exchange needs to be contacted on how to proceed
	 */
	private boolean accountabilityLimit;

	/**
	 * By default all limits are compared to net positions (long - short)
	 * however in some cases, the limits should be compared twice - once to longs separately and once to shorts separately
	 */
	private boolean applyToLongsAndShortsSeparately;

	private Integer exchangeLimit;
	/**
	 * If set, overrides the exchange limit *
	 */
	private Integer ourLimit;

	/**
	 * Used for SCALE_DOWN_SPOT_MONTH limit types to consider the security in the scale down period during the last x business days prior to the calculated spot month end date
	 */
	private Integer spotMonthStartBusinessDaysBack;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder lbl = new StringBuilder();
		if (getInstrument() != null) {
			lbl.append(getInstrument().getLabelShort());
			lbl.append(": ");
		}
		if (getLimitType() != null) {
			lbl.append(getLimitTypeLabel());
		}
		if (isAccountabilityLimit()) {
			lbl.append(" (Accountability)");
		}
		lbl.append(" ").append(DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false));
		return lbl.toString();
	}


	public String getLimitTypeLabel() {
		if (getLimitType() != null) {
			return getLimitType().getLabel();
		}
		return null;
	}


	public Integer getLimit() {
		if (getOurLimit() == null) {
			return getExchangeLimit();
		}
		return getOurLimit();
	}


	/**
	 * Used for Notifications to flag accountability always as Low Priority
	 */
	public String getPriorityOverride() {
		if (isAccountabilityLimit()) {
			return "Low";
		}
		return null;
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CompliancePositionExchangeLimitTypes getLimitType() {
		return this.limitType;
	}


	public void setLimitType(CompliancePositionExchangeLimitTypes limitType) {
		this.limitType = limitType;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isAccountabilityLimit() {
		return this.accountabilityLimit;
	}


	public void setAccountabilityLimit(boolean accountabilityLimit) {
		this.accountabilityLimit = accountabilityLimit;
	}


	public Integer getExchangeLimit() {
		return this.exchangeLimit;
	}


	public void setExchangeLimit(Integer exchangeLimit) {
		this.exchangeLimit = exchangeLimit;
	}


	public Integer getOurLimit() {
		return this.ourLimit;
	}


	public void setOurLimit(Integer ourLimit) {
		this.ourLimit = ourLimit;
	}


	public Integer getSpotMonthStartBusinessDaysBack() {
		return this.spotMonthStartBusinessDaysBack;
	}


	public void setSpotMonthStartBusinessDaysBack(Integer spotMonthStartBusinessDaysBack) {
		this.spotMonthStartBusinessDaysBack = spotMonthStartBusinessDaysBack;
	}


	public boolean isApplyToLongsAndShortsSeparately() {
		return this.applyToLongsAndShortsSeparately;
	}


	public void setApplyToLongsAndShortsSeparately(boolean applyToLongsAndShortsSeparately) {
		this.applyToLongsAndShortsSeparately = applyToLongsAndShortsSeparately;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}
}
