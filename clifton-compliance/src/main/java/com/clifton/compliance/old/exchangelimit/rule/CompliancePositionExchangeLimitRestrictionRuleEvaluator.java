package com.clifton.compliance.old.exchangelimit.rule;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPositionService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This rule tracks the total positions of a given instrument across the exchange, not the total positions for a given client.
 * <p>
 * The <code>CompliancePositionExchangeLimitRestrictionGenerator</code> validates if the trade will take the instrument outside of specified % of warning (if one exists)
 * <p>
 * Uses contextHandler for supporting grouped trades for the same instrument and adds trade quantities to it for quantitative checks
 *
 * @author manderson
 */
public class CompliancePositionExchangeLimitRestrictionRuleEvaluator extends BaseRuleEvaluator<IdentityObject, TradeRuleEvaluatorContext> {

	private CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService;
	private ContextHandler contextHandler;

	/**
	 * Can optionally include accountability limits.
	 */
	private boolean includeAccountabilityLimits;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject entity, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		AccountingBean bean = (AccountingBean) entity;
		EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(bean.getSourceEntityId()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			BigDecimal minAmount = entityConfig.getMinAmount();
			BigDecimal maxAmount = entityConfig.getMaxAmount();
			List<CompliancePositionExchangeLimitPosition> currentPositionLimits = getCurrentPositionLimits(bean);
			for (CompliancePositionExchangeLimitPosition positionLimit : CollectionUtils.getIterable(currentPositionLimits)) {
				BigDecimal originalLimit = positionLimit.getPercentOfLimit();

				positionLimit.addAccountingBean(bean);
				BigDecimal adjustedLimit = positionLimit.getPercentOfLimit();

				//System.out.println(((Trade) bean).getLabel() + ": Original " + CoreMathUtils.formatNumberDecimal(originalLimit) + ", Adjusted: " + CoreMathUtils.formatNumberDecimal(adjustedLimit));

				// Only Generate the Warning if we have increased the limit and we are within specified percentage range;
				if (MathUtils.isGreaterThan(adjustedLimit, originalLimit) && (minAmount == null || MathUtils.isGreaterThan(adjustedLimit, minAmount))
						&& (maxAmount == null || MathUtils.isLessThanOrEqual(adjustedLimit, maxAmount))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("adjustedLimit", adjustedLimit);
					templateValues.put("originalLimit", originalLimit);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, bean.getSourceEntityId(), positionLimit.getLimit().getId(), null, templateValues));
				}
			}
			// Put the retrieved list in - if it's empty - keep it as an empty list so don't try to look it up again
			// Need to set it here ALWAYS, not when first retrieved from the database so it has the updated limits using this accounting bean
			getContextHandler().setBean(getContextKeyForAccountingBean(bean), (currentPositionLimits != null ? currentPositionLimits : new ArrayList<CompliancePositionExchangeLimitPosition>()));
		}
		return ruleViolationList;
	}


	@SuppressWarnings("unchecked")
	private List<CompliancePositionExchangeLimitPosition> getCurrentPositionLimits(AccountingBean bean) {
		List<CompliancePositionExchangeLimitPosition> currentPositionLimits = (List<CompliancePositionExchangeLimitPosition>) getContextHandler().getBean(getContextKeyForAccountingBean(bean));

		// If null - retrieve it
		if (currentPositionLimits == null) {
			currentPositionLimits = getCompliancePositionExchangeLimitPositionService().getCompliancePositionExchangeLimitPositionListForAccountingBean(bean, isIncludeAccountabilityLimits());
		}
		// Don't put it in the context yet since need to add this trade to it first...
		return currentPositionLimits;
	}


	private String getContextKeyForAccountingBean(AccountingBean bean) {
		return "POSITION_LIMITS_" + bean.getInvestmentSecurity().getInstrument().getId() + (isIncludeAccountabilityLimits() ? "_ACCOUNTABILITY" : "");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CompliancePositionExchangeLimitPositionService getCompliancePositionExchangeLimitPositionService() {
		return this.compliancePositionExchangeLimitPositionService;
	}


	public void setCompliancePositionExchangeLimitPositionService(CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService) {
		this.compliancePositionExchangeLimitPositionService = compliancePositionExchangeLimitPositionService;
	}


	public boolean isIncludeAccountabilityLimits() {
		return this.includeAccountabilityLimits;
	}


	public void setIncludeAccountabilityLimits(boolean includeAccountabilityLimits) {
		this.includeAccountabilityLimits = includeAccountabilityLimits;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}

