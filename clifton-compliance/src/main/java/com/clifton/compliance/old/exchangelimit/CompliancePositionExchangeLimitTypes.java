package com.clifton.compliance.old.exchangelimit;


/**
 * The <code>CompliancePositionExchangeLimitType</code> ...
 *
 * @author Mary Anderson
 */
public enum CompliancePositionExchangeLimitTypes {

	ALL_MONTHS("All Months"), //

	SPOT_MONTH("Spot Month", true, false), //

	SCALE_DOWN_SPOT_MONTH("Scale Down Spot Month", true, true), //

	SINGLE_MONTH("Single Month");

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	CompliancePositionExchangeLimitTypes(String label) {
		this(label, false, false);
	}


	CompliancePositionExchangeLimitTypes(String label, boolean instrumentSpotMonthCalculatorRequired, boolean scaleDownSpotMonth) {
		this.label = label;
		this.instrumentSpotMonthCalculatorRequired = instrumentSpotMonthCalculatorRequired;
		this.scaleDownSpotMonth = scaleDownSpotMonth;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private final String label;

	/**
	 * Instrument Spot Month Calculators are only required for Spot Month limit types.  Otherwise the limit can be applied without spot month dates
	 */
	private final boolean instrumentSpotMonthCalculatorRequired;

	private final boolean scaleDownSpotMonth;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public boolean isInstrumentSpotMonthCalculatorRequired() {
		return this.instrumentSpotMonthCalculatorRequired;
	}


	public boolean isScaleDownSpotMonth() {
		return this.scaleDownSpotMonth;
	}
}
