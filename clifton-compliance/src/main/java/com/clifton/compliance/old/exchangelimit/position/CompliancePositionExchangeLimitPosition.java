package com.clifton.compliance.old.exchangelimit.position;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CompliancePositionExchangeLimitPosition</code> is a virtual class
 * built when comparing existing positions to a limit.  Most limits will have one
 * record for each limit defined, however in the case of SINGLE_MONTH types,
 * there could be multiple, one for each month we hold a security in (defined by Delivery Month)
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class CompliancePositionExchangeLimitPosition {

	private CompliancePositionExchangeLimit limit;

	private boolean isLongOnly;
	private boolean isShortOnly;

	private Date monthDate;

	/**
	 * In the GL already
	 */
	private BigDecimal longInstrumentCount = BigDecimal.ZERO;
	private BigDecimal shortInstrumentCount = BigDecimal.ZERO;

	/**
	 * Pending Trades (i.e. Not Draft or Closed)
	 */
	private BigDecimal longPendingInstrumentCount = BigDecimal.ZERO;
	private BigDecimal shortPendingInstrumentCount = BigDecimal.ZERO;

	/**
	 * In the GL already
	 */
	private BigDecimal longBigInstrumentCount = BigDecimal.ZERO;
	private BigDecimal shortBigInstrumentCount = BigDecimal.ZERO;

	/**
	 * Pending Trades (i.e. Not Draft or Closed)
	 */
	private BigDecimal longPendingBigInstrumentCount = BigDecimal.ZERO;
	private BigDecimal shortPendingBigInstrumentCount = BigDecimal.ZERO;


	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////


	public String getPositionLimitLabel() {
		StringBuilder lbl = new StringBuilder();
		if (getMonthDate() != null) {
			lbl.append(DateUtils.fromDate(getMonthDate(), DateUtils.DATE_FORMAT_MONTH_YEAR)).append(" ");
		}
		if (isLongOnly()) {
			lbl.append("Long");
		}
		else if (isShortOnly()) {
			lbl.append("Short");
		}
		return lbl.toString();
	}


	public BigDecimal getTotalInstrumentCount() {
		return MathUtils.subtract(getLongInstrumentCount(), getShortInstrumentCount());
	}


	public BigDecimal getTotalPendingInstrumentCount() {
		return MathUtils.subtract(getLongPendingInstrumentCount(), getShortPendingInstrumentCount());
	}


	public BigDecimal getTotalBigInstrumentCount() {
		return MathUtils.subtract(getLongBigInstrumentCount(), getShortBigInstrumentCount());
	}


	public BigDecimal getTotalPendingBigInstrumentCount() {
		return MathUtils.subtract(getLongPendingBigInstrumentCount(), getShortPendingBigInstrumentCount());
	}


	public BigDecimal getBigFactor() {
		if (getLimit().getInstrument().getBigInstrument() != null) {
			return MathUtils.divide(getLimit().getInstrument().getBigInstrument().getPriceMultiplier(), getLimit().getInstrument().getPriceMultiplier());
		}
		return BigDecimal.ONE;
	}


	public BigDecimal getTotalCount() {
		return MathUtils.add(MathUtils.add(getTotalInstrumentCount(), MathUtils.multiply(getBigFactor(), getTotalBigInstrumentCount())),
				MathUtils.add(getTotalPendingInstrumentCount(), MathUtils.multiply(getBigFactor(), getTotalPendingBigInstrumentCount())));
	}


	// Even if we hold net short positions, use positive percentages of limits
	public BigDecimal getPercentOfLimit() {
		return MathUtils.abs(CoreMathUtils.getPercentValue(getTotalCount(), BigDecimal.valueOf(getLimit().getLimit()), true));
	}


	public void addPosition(AccountingPosition position) {
		addPositionQuantity(position.getInvestmentSecurity().getInstrument(), MathUtils.isGreaterThan(position.getRemainingQuantity(), 0), position.getRemainingQuantity(), false);
	}


	public void addAccountingBean(AccountingBean accountingBean) {
		addPositionQuantity(accountingBean.getInvestmentSecurity().getInstrument(), accountingBean.isBuy(), accountingBean.getQuantity(), true);
	}


	private void addPositionQuantity(InvestmentInstrument instrument, boolean longPosition, BigDecimal qty, boolean pending) {
		if (longPosition && isShortOnly()) {
			return;
		}
		if (!longPosition && isLongOnly()) {
			return;
		}
		qty = MathUtils.abs(qty);
		if (getLimit().getInstrument().equals(instrument)) {
			if (longPosition) {
				if (pending) {
					this.longPendingInstrumentCount = MathUtils.add(this.longPendingInstrumentCount, qty);
				}
				else {
					this.longInstrumentCount = MathUtils.add(this.longInstrumentCount, qty);
				}
			}
			else {
				if (pending) {
					this.shortPendingInstrumentCount = MathUtils.add(this.shortPendingInstrumentCount, qty);
				}
				else {
					this.shortInstrumentCount = MathUtils.add(this.shortInstrumentCount, qty);
				}
			}
		}
		else if (longPosition) {
			if (pending) {
				this.longPendingBigInstrumentCount = MathUtils.add(this.longPendingBigInstrumentCount, qty);
			}
			else {
				this.longBigInstrumentCount = MathUtils.add(this.longBigInstrumentCount, qty);
			}
		}
		else {
			if (pending) {
				this.shortPendingBigInstrumentCount = MathUtils.add(this.shortPendingBigInstrumentCount, qty);
			}
			else {
				this.shortBigInstrumentCount = MathUtils.add(this.shortBigInstrumentCount, qty);
			}
		}
	}


	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////


	public CompliancePositionExchangeLimit getLimit() {
		return this.limit;
	}


	public void setLimit(CompliancePositionExchangeLimit limit) {
		this.limit = limit;
	}


	public boolean isLongOnly() {
		return this.isLongOnly;
	}


	public void setLongOnly(boolean isLongOnly) {
		this.isLongOnly = isLongOnly;
	}


	public boolean isShortOnly() {
		return this.isShortOnly;
	}


	public void setShortOnly(boolean isShortOnly) {
		this.isShortOnly = isShortOnly;
	}


	public Date getMonthDate() {
		return this.monthDate;
	}


	public void setMonthDate(Date monthDate) {
		this.monthDate = monthDate;
	}


	public BigDecimal getLongInstrumentCount() {
		return this.longInstrumentCount;
	}


	public void setLongInstrumentCount(BigDecimal longInstrumentCount) {
		this.longInstrumentCount = longInstrumentCount;
	}


	public BigDecimal getShortInstrumentCount() {
		return this.shortInstrumentCount;
	}


	public void setShortInstrumentCount(BigDecimal shortInstrumentCount) {
		this.shortInstrumentCount = shortInstrumentCount;
	}


	public BigDecimal getLongPendingInstrumentCount() {
		return this.longPendingInstrumentCount;
	}


	public void setLongPendingInstrumentCount(BigDecimal longPendingInstrumentCount) {
		this.longPendingInstrumentCount = longPendingInstrumentCount;
	}


	public BigDecimal getShortPendingInstrumentCount() {
		return this.shortPendingInstrumentCount;
	}


	public void setShortPendingInstrumentCount(BigDecimal shortPendingInstrumentCount) {
		this.shortPendingInstrumentCount = shortPendingInstrumentCount;
	}


	public BigDecimal getLongBigInstrumentCount() {
		return this.longBigInstrumentCount;
	}


	public void setLongBigInstrumentCount(BigDecimal longBigInstrumentCount) {
		this.longBigInstrumentCount = longBigInstrumentCount;
	}


	public BigDecimal getShortBigInstrumentCount() {
		return this.shortBigInstrumentCount;
	}


	public void setShortBigInstrumentCount(BigDecimal shortBigInstrumentCount) {
		this.shortBigInstrumentCount = shortBigInstrumentCount;
	}


	public BigDecimal getLongPendingBigInstrumentCount() {
		return this.longPendingBigInstrumentCount;
	}


	public void setLongPendingBigInstrumentCount(BigDecimal longPendingBigInstrumentCount) {
		this.longPendingBigInstrumentCount = longPendingBigInstrumentCount;
	}


	public BigDecimal getShortPendingBigInstrumentCount() {
		return this.shortPendingBigInstrumentCount;
	}


	public void setShortPendingBigInstrumentCount(BigDecimal shortPendingBigInstrumentCount) {
		this.shortPendingBigInstrumentCount = shortPendingBigInstrumentCount;
	}
}
