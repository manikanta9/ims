package com.clifton.compliance.old.rule;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ComplianceRuleOld</code> class defines a specific instance of a compliance rule.
 * For example, ABC client account is allowed long positions for any stock (investment type).
 *
 * @author vgomelsky
 */
public class ComplianceRuleOld extends BaseEntity<Integer> implements SystemColumnCustomValueAware {

	private ComplianceRuleTypeOld ruleType;

	/**
	 * Only one of the three can be specified for a rule: instrument, hierarchy or type.
	 */
	private InvestmentInstrument investmentInstrument;
	private InvestmentInstrumentHierarchy investmentHierarchy;
	private InvestmentType investmentType;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;

	private SecurityUser trader;

	////////////////////////////////////////////////////////////////////////////

	private boolean shortPositionAllowed;
	private boolean longPositionAllowed;

	private BigDecimal maxBuyQuantity;
	private BigDecimal maxLongPositionQuantity;
	private BigDecimal maxSellQuantity;
	private BigDecimal maxShortPositionQuantity;

	private BigDecimal maxBuyAccountingNotional;
	private BigDecimal maxLongPositionAccountingNotional;
	private BigDecimal maxSellAccountingNotional;
	private BigDecimal maxShortPositionAccountingNotional;

	/**
	 * Is this warning allowed to be ignorable during compliance checks
	 * i.e. Warnings on trades will allow ignorable only if this flag is set
	 * ignorable = "soft warning" otherwise it's a hard stop and not allowed at all.
	 */
	private boolean ignorable;

	/**
	 * Use the settlement date to look up positions.
	 */
	private boolean useSettlementDate;

	/**
	 * A List of custom column values for the rules
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isGlobalRule() {
		return getClientInvestmentAccount() == null && getHoldingInvestmentAccount() == null && getTrader() == null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleTypeOld getRuleType() {
		return this.ruleType;
	}


	public void setRuleType(ComplianceRuleTypeOld ruleType) {
		this.ruleType = ruleType;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public InvestmentInstrumentHierarchy getInvestmentHierarchy() {
		return this.investmentHierarchy;
	}


	public void setInvestmentHierarchy(InvestmentInstrumentHierarchy investmentHierarchy) {
		this.investmentHierarchy = investmentHierarchy;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public SecurityUser getTrader() {
		return this.trader;
	}


	public void setTrader(SecurityUser trader) {
		this.trader = trader;
	}


	public boolean isShortPositionAllowed() {
		return this.shortPositionAllowed;
	}


	public void setShortPositionAllowed(boolean shortPositionAllowed) {
		this.shortPositionAllowed = shortPositionAllowed;
	}


	public boolean isLongPositionAllowed() {
		return this.longPositionAllowed;
	}


	public void setLongPositionAllowed(boolean longPositionAllowed) {
		this.longPositionAllowed = longPositionAllowed;
	}


	public BigDecimal getMaxBuyQuantity() {
		return this.maxBuyQuantity;
	}


	public void setMaxBuyQuantity(BigDecimal maxBuyQuantity) {
		this.maxBuyQuantity = maxBuyQuantity;
	}


	public BigDecimal getMaxLongPositionQuantity() {
		return this.maxLongPositionQuantity;
	}


	public void setMaxLongPositionQuantity(BigDecimal maxLongPositionQuantity) {
		this.maxLongPositionQuantity = maxLongPositionQuantity;
	}


	public BigDecimal getMaxSellQuantity() {
		return this.maxSellQuantity;
	}


	public void setMaxSellQuantity(BigDecimal maxSellQuantity) {
		this.maxSellQuantity = maxSellQuantity;
	}


	public BigDecimal getMaxShortPositionQuantity() {
		return this.maxShortPositionQuantity;
	}


	public void setMaxShortPositionQuantity(BigDecimal maxShortPositionQuantity) {
		this.maxShortPositionQuantity = maxShortPositionQuantity;
	}


	public BigDecimal getMaxBuyAccountingNotional() {
		return this.maxBuyAccountingNotional;
	}


	public void setMaxBuyAccountingNotional(BigDecimal maxBuyAccountingNotional) {
		this.maxBuyAccountingNotional = maxBuyAccountingNotional;
	}


	public BigDecimal getMaxLongPositionAccountingNotional() {
		return this.maxLongPositionAccountingNotional;
	}


	public void setMaxLongPositionAccountingNotional(BigDecimal maxLongPositionAccountingNotional) {
		this.maxLongPositionAccountingNotional = maxLongPositionAccountingNotional;
	}


	public BigDecimal getMaxSellAccountingNotional() {
		return this.maxSellAccountingNotional;
	}


	public void setMaxSellAccountingNotional(BigDecimal maxSellAccountingNotional) {
		this.maxSellAccountingNotional = maxSellAccountingNotional;
	}


	public BigDecimal getMaxShortPositionAccountingNotional() {
		return this.maxShortPositionAccountingNotional;
	}


	public void setMaxShortPositionAccountingNotional(BigDecimal maxShortPositionAccountingNotional) {
		this.maxShortPositionAccountingNotional = maxShortPositionAccountingNotional;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public boolean isIgnorable() {
		return this.ignorable;
	}


	public void setIgnorable(boolean ignorable) {
		this.ignorable = ignorable;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}
}
