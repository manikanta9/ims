package com.clifton.compliance.old.rule;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>ComplianceRuleTypeOld</code> class defines the types of compliance rules:
 * Client Approved Contracts: investmentAccountAllowed = true,  investmentAccountRequired = true,  investmentSpecific = true,  traderSpecific = false, limitRule = false
 * Fat Finger Trader Limits:  investmentAccountAllowed = false, investmentAccountRequired = false, investmentSpecific = true,  traderSpecific = true,  limitRule = true
 * Notional Limits:           investmentAccountAllowed = true,  investmentAccountRequired = false, investmentSpecific = true,  traderSpecific = false, limitRule = true
 * Cash Exposure Violation:   investmentAccountAllowed = false, investmentAccountRequired = false, investmentSpecific = false, traderSpecific = false, limitRule = false
 * <p>
 * Other rules: Exchange Contract Limits, ...
 *
 * @author vgomelsky
 */
@CacheByName
public class ComplianceRuleTypeOld extends NamedEntity<Short> {

	/**
	 * Specifies whether this rule applies to a specific client/holding account or to all investment accounts.
	 */
	private boolean investmentAccountAllowed;
	private boolean investmentAccountRequired;

	/**
	 * Specifies whether or not this rule is investment specific and requires investment type, hierarchy or instrument to be selected.
	 */
	private boolean investmentSpecific;

	/**
	 * Specifies whether this rule is trader specific and requires "trader" to be selected.
	 */
	private boolean traderSpecific;

	/**
	 * Limit rules specify transaction or total position limits (see "max*" ComplianceRuleOld properties)
	 */
	private boolean limitRule;

	/**
	 * Short/Long rules specify weather a position can be held long or short.
	 */
	private boolean shortLongRule;

	/**
	 * Are rules for this type allowed to be flagged as ignorable.
	 */
	private boolean ignorableAllowed;

	/**
	 * Are rules for this type allowed to be flagged with useSettlementDate
	 */
	private boolean useSettlementDateAllowed;


	public boolean isInvestmentAccountAllowed() {
		return this.investmentAccountAllowed;
	}


	public void setInvestmentAccountAllowed(boolean investmentAccountAllowed) {
		this.investmentAccountAllowed = investmentAccountAllowed;
	}


	public boolean isInvestmentAccountRequired() {
		return this.investmentAccountRequired;
	}


	public void setInvestmentAccountRequired(boolean investmentAccountRequired) {
		this.investmentAccountRequired = investmentAccountRequired;
	}


	public boolean isInvestmentSpecific() {
		return this.investmentSpecific;
	}


	public void setInvestmentSpecific(boolean investmentSpecific) {
		this.investmentSpecific = investmentSpecific;
	}


	public boolean isTraderSpecific() {
		return this.traderSpecific;
	}


	public void setTraderSpecific(boolean traderSpecific) {
		this.traderSpecific = traderSpecific;
	}


	public boolean isLimitRule() {
		return this.limitRule;
	}


	public void setLimitRule(boolean limitRule) {
		this.limitRule = limitRule;
	}


	public boolean isShortLongRule() {
		return this.shortLongRule;
	}


	public void setShortLongRule(boolean shortLongRule) {
		this.shortLongRule = shortLongRule;
	}


	public boolean isIgnorableAllowed() {
		return this.ignorableAllowed;
	}


	public void setIgnorableAllowed(boolean ignorableAllowed) {
		this.ignorableAllowed = ignorableAllowed;
	}


	public boolean isUseSettlementDateAllowed() {
		return this.useSettlementDateAllowed;
	}


	public void setUseSettlementDateAllowed(boolean useSettlementDateAllowed) {
		this.useSettlementDateAllowed = useSettlementDateAllowed;
	}
}
