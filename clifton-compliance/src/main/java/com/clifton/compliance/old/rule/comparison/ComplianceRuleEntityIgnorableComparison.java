package com.clifton.compliance.old.rule.comparison;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.comparison.RuleViolationComparison;


/**
 * The <code>ComplianceRuleEntityIgnorableComparison</code> handles checking the ignore condition for warnings related to Compliance.
 * <p>
 * Warning Ignore Conditions are passed the warning itself -
 * Check the cause entity first, which should be the ComplianceRuleOld,
 * If it's not a ComplianceRuleOld, then checks if it's an {@AccountingBean}, if not, checks the linked entity is an {@link AccountingBean}
 * <p>
 * For backwards compatibility, we need to handle a few different scenarios...
 * 1.  Trades
 * These warnings for Trades didn't have a cause table/entity set, so needs to handle a Trade as a bean
 * Going forward, for easier/quicker checks, the {@link ComplianceRuleOld} will be set as the cause entity
 * 2. Repos
 * Repos pass the RepoDetail as the cause
 * <p>
 * Both Trades and Repos are {@link AccountingBean} and that is the bean type that is checked
 * <p>
 * If the bean passed is not a ComplianceRuleOld and not an AccountingBean the result will be false and the context will have the message
 * <p>
 * Special Case is for Client Approved Contract Warnings.  These warnings are generated only if there is no rule.
 * So for these cases will only check type level.
 * <p>
 * <p>
 * In cases where a compliance rule is not explicitly passed the complianceRuleTypeName is required in order to lookup
 * the compliance rule for the AccountingBean.
 *
 * @author Mary Anderson
 */
public class ComplianceRuleEntityIgnorableComparison extends RuleViolationComparison<IdentityObject> {

	private ComplianceOldService complianceOldService;

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////

	private Short typeId;


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		Boolean result = null;
		String msg = "";

		ComplianceRuleOld rule = null;
		AccountingBean accountingBean = null;
		if (bean instanceof RuleViolation) {
			IdentityObject entity = getCauseEntity((RuleViolation) bean);
			if (entity != null) {
				if (entity instanceof ComplianceRuleOld) {
					rule = (ComplianceRuleOld) entity;
				}
				if (AccountingBean.class.isAssignableFrom(entity.getClass())) {
					accountingBean = (AccountingBean) entity;
				}
			}
			else {
				entity = getLinkedEntity((RuleViolation) bean);
				if (entity instanceof ComplianceRuleOld) {
					rule = (ComplianceRuleOld) entity;
				}
				if (AccountingBean.class.isAssignableFrom(entity.getClass())) {
					accountingBean = (AccountingBean) entity;
				}
			}
		}
		else if (bean instanceof ComplianceRuleOld) {
			rule = (ComplianceRuleOld) bean;
		}
		else if (AccountingBean.class.isAssignableFrom(bean.getClass())) {
			accountingBean = (AccountingBean) bean;
		}
		if (rule == null) {
			// If no explicit rule, first check the Rule Type, if Ignorable Not Allowed, don't even bother looking up the rule
			ValidationUtils.assertNotNull(getTypeId(), "Compliance Rule Type is required.");
			ComplianceRuleTypeOld type = getComplianceOldService().getComplianceRuleTypeOld(getTypeId());
			ValidationUtils.assertNotNull(type, "Compliance Rule Type with ID [" + getTypeId() + " is missing from the database.");
			if (type.isIgnorableAllowed()) {
				// Type doesn't exist, or allows ignores, then let's check the specific rule
				if (accountingBean != null) {
					rule = getComplianceOldService().getComplianceRuleOldForBeanAndType(accountingBean, type.getId());
				}
			}
			else {
				result = false;
				msg = "Compliance Rule Type [" + type.getName() + "] is not ignorable.";
			}
		}
		if (rule != null) {
			if (rule.isIgnorable()) {
				result = true;
				msg = "Compliance Rule Type [" + rule.getRuleType().getName() + "] for this entity is ignorable.";
			}
			else {
				result = false;
				msg = "Compliance Rule Type [" + rule.getRuleType().getName() + "] for this entity is NOT ignorable.";
			}
		}

		// If result hasn't been set, then couldn't determine rule or rule type
		// Should probably never happen, but just in case give a user friendly message
		if (result == null) {
			result = false;
			msg = "Could not determine compliance rule for type with id [" + getTypeId() + "] and entity [" + bean.getClass() + "] with id [" + bean.getIdentity()
					+ "].  Supported classes are [RuleViolation where linked or cause is a ComplianceRuleOld or AccountingBean, ComplianceRuleOld, or AccountingBean].";
		}

		if (context != null) {
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}


	public ComplianceOldService getComplianceOldService() {
		return this.complianceOldService;
	}


	public void setComplianceOldService(ComplianceOldService complianceOldService) {
		this.complianceOldService = complianceOldService;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}
}
