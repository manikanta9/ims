package com.clifton.compliance.old.rule.cache;


import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;


/**
 * The <code>ComplianceRuleOldTargetHolder</code> holds the cached meta data for retrieving a cached {@link ComplianceRuleOld}.
 *
 * @author jgommels
 */
public class ComplianceRuleOldTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final int ruleId;


	ComplianceRuleOldTargetHolder(String key, ComplianceRuleOld rule) {
		this.key = key;
		this.ruleId = rule.getId();
	}


	public int getRuleId() {
		return this.ruleId;
	}


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public SpecificityScope getScope() {
		//Scopes are not needed for ComplianceRuleOld
		return null;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		result = prime * result + this.ruleId;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComplianceRuleOldTargetHolder other = (ComplianceRuleOldTargetHolder) obj;
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		return this.ruleId == other.ruleId;
	}
}
