package com.clifton.compliance.old.exchangelimit;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitValidator</code> validates inserts/updates for position limits
 * based on type/instrument/accountability and date ranges
 *
 * @author Mary Anderson
 */
@Component
public class CompliancePositionExchangeLimitValidator extends SelfRegisteringDaoValidator<CompliancePositionExchangeLimit> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(CompliancePositionExchangeLimit bean, DaoEventTypes config) throws ValidationException {
		// NOT USED - USES DAO METHOD INSTEAD
	}


	@Override
	public void validate(CompliancePositionExchangeLimit bean, @SuppressWarnings("unused") DaoEventTypes config, ReadOnlyDAO<CompliancePositionExchangeLimit> dao) throws ValidationException {
		// Validate at least one of the limits is set.
		ValidationUtils.assertTrue(bean.getExchangeLimit() != null || bean.getOurLimit() != null, "A position limit is required.  Please enter an exchange and/or Clifton limit.", "exchangeLimit");

		// Validate Start/End Date
		if (bean.getStartDate() != null && bean.getEndDate() != null) {
			ValidationUtils.assertTrue(bean.getEndDate().after(bean.getStartDate()), "Invalid Start/End Dates entered. Start Date must be before End Date.", "startDate");
		}

		// UX CHECK type, instrument.id, accountability, and Date Ranges
		List<CompliancePositionExchangeLimit> list = dao.findByFields(new String[]{"instrument.id", "accountabilityLimit", "limitType"},
				new Object[]{bean.getInstrument().getId(), bean.isAccountabilityLimit(), bean.getLimitType()});
		for (CompliancePositionExchangeLimit rel : CollectionUtils.getIterable(list)) {
			// Same bean
			if (rel.equals(bean)) {
				continue;
			}
			ValidationUtils.assertFalse(
					DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), rel.getStartDate(), rel.getEndDate()),
					"Instrument position limit [" + bean.getInstrument().getLabel() + "] for type [" + bean.getLimitType().getLabel() + "] and accountability limit [" + bean.isAccountabilityLimit()
							+ "] should be unique per date ranges.  The following instrument position limit is already defined for the given date range ["
							+ (rel.getStartDate() == null ? "N/A" : DateUtils.fromDateShort(rel.getStartDate())) + " - "
							+ (rel.getEndDate() == null ? "N/A" : DateUtils.fromDateShort(rel.getEndDate())) + "]");
		}
	}
}
