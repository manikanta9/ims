package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitTypes;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.trade.Trade;

import java.util.Date;
import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitTypeCalculator</code> defines the interface
 * implemented for each {@link CompliancePositionExchangeLimitTypes} to return the list of
 * {@link CompliancePositionExchangeLimitPosition} calculated for the limit for the given List of
 * positions.
 *
 * @author Mary Anderson
 */
public interface CompliancePositionExchangeLimitTypeCalculator {

	/**
	 * Returns true if the trade will apply to the limit, i.e. spot month calculators won't be calculated
	 * for Trade warnings if the trade is for a security that doesn't fall into the spot month
	 */
	public boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, AccountingBean accountingBean);


	/**
	 * Returns the Limit vs. Position list for the given limit and positions/pending trades
	 */
	public List<CompliancePositionExchangeLimitPosition> calculate(CompliancePositionExchangeLimit limit, Date date, List<AccountingPosition> accountingPositionList, List<Trade> pendingTradeList);
}
