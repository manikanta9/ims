package com.clifton.compliance.old.exchangelimit;


import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitService</code> contains methods for managing compliance exchange limits, limit types, and related setup.
 *
 * @author Mary Anderson
 */
public interface CompliancePositionExchangeLimitService {

	////////////////////////////////////////////////////////////////////////////////
	/////////        Compliance Position Exchange Limit Methods           //////////
	////////////////////////////////////////////////////////////////////////////////


	public CompliancePositionExchangeLimit getCompliancePositionExchangeLimit(int id);


	public List<CompliancePositionExchangeLimit> getCompliancePositionExchangeLimitListForInstrumentOrBigInstrument(int instrumentId, Date activeOnDate, boolean includeAccountableLimits);


	public List<CompliancePositionExchangeLimit> getCompliancePositionExchangeLimitList(CompliancePositionExchangeLimitSearchForm searchForm);


	public CompliancePositionExchangeLimit saveCompliancePositionExchangeLimit(CompliancePositionExchangeLimit bean);


	public void deleteCompliancePositionExchangeLimit(int id);
}
