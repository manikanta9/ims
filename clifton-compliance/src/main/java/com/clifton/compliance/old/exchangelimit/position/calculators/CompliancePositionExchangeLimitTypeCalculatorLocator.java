package com.clifton.compliance.old.exchangelimit.position.calculators;

import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitTypes;


/**
 * The <code>CompliancePositionExchangeLimitTypeCalculatorLocator</code> class finds and returns CompliancePositionExchangeLimitTypeCalculator
 * that corresponds to a CompliancePositionExchangeLimitType.
 *
 * @author manderson
 */
public interface CompliancePositionExchangeLimitTypeCalculatorLocator {

	/**
	 * Returns CompliancePositionExchangeLimitPositionCalculator for the specified limit type.
	 */
	public CompliancePositionExchangeLimitTypeCalculator locate(CompliancePositionExchangeLimitTypes limitType);
}
