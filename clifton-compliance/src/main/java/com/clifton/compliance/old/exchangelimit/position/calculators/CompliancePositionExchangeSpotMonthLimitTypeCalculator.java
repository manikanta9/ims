package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;
import com.clifton.trade.Trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CompliancePositionExchangeSpotMonthLimitTypeCalculator extends BaseCompliancePositionExchangeLimitTypeCalculator {

	/**
	 * Only applies if the trade is for a security during it's spot month
	 */
	@Override
	protected boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, InvestmentSecurity security, Date date, Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap) {
		return isSecurityInSpotMonth(limit, security, date, securitySpotMonthMap);
	}


	@Override
	public List<CompliancePositionExchangeLimitPosition> calculate(CompliancePositionExchangeLimit limit, Date date, List<AccountingPosition> accountingPositionList, List<Trade> pendingTradeList) {
		Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap = new HashMap<>();

		List<AccountingPosition> filteredPositions = new ArrayList<>();
		List<Trade> filteredTrades = new ArrayList<>();

		Date spotMonthDate = null;

		Map<InvestmentSecurity, List<AccountingPosition>> securityPositionMap = BeanUtils.getBeansMap(accountingPositionList, AccountingPosition::getInvestmentSecurity);
		Map<InvestmentSecurity, List<Trade>> securityTradeMap = BeanUtils.getBeansMap(pendingTradeList, Trade::getInvestmentSecurity);

		for (Map.Entry<InvestmentSecurity, List<AccountingPosition>> investmentSecurityListEntry : securityPositionMap.entrySet()) {
			if (isCompliancePositionExchangeLimitApplied(limit, investmentSecurityListEntry.getKey(), date, securitySpotMonthMap)) {
				if (spotMonthDate == null) {
					spotMonthDate = DateUtils.getFirstDayOfMonth((investmentSecurityListEntry.getKey()).getLastDeliveryOrEndDate());
				}
				// Applies
				filteredPositions.addAll(investmentSecurityListEntry.getValue());
			}
		}

		for (Map.Entry<InvestmentSecurity, List<Trade>> investmentSecurityListEntry : securityTradeMap.entrySet()) {
			if (isCompliancePositionExchangeLimitApplied(limit, investmentSecurityListEntry.getKey(), date, securitySpotMonthMap)) {
				if (spotMonthDate == null) {
					spotMonthDate = DateUtils.getFirstDayOfMonth((investmentSecurityListEntry.getKey()).getLastDeliveryOrEndDate());
				}
				// Applies
				filteredTrades.addAll(investmentSecurityListEntry.getValue());
			}
		}

		List<CompliancePositionExchangeLimitPosition> list = new ArrayList<>();
		if (limit.isApplyToLongsAndShortsSeparately()) {
			list.add(getPositionLimitPopulated(limit, true, spotMonthDate, filteredPositions, filteredTrades));
			list.add(getPositionLimitPopulated(limit, false, spotMonthDate, filteredPositions, filteredTrades));
		}
		else {
			list.add(getPositionLimitPopulated(limit, null, spotMonthDate, filteredPositions, filteredTrades));
		}
		return list;
	}
}
