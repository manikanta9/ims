package com.clifton.compliance.old.rule.cache;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.search.ComplianceRuleOldSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.DynamicSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.ScoredOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


@Component
public class ComplianceRuleOldSpecificityCacheImpl extends AbstractSpecificityCache<ComplianceRuleOldSource, ComplianceRuleOld, ComplianceRuleOldTargetHolder> implements ComplianceRuleOldSpecificityCache {

	private ComplianceOldService complianceOldService;

	private final ComplianceOldCacheUpdater cacheUpdater = new ComplianceOldCacheUpdater();


	private ComplianceRuleOldSpecificityCacheImpl() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new ScoredOrderingSpecificityKeyGenerator());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ComplianceRuleOld getMostSpecificComplianceRuleOld(AccountingBean bean, final short typeId) {
		List<ComplianceRuleOld> results = getMostSpecificResultList(new ComplianceRuleOldSource(bean, typeId));

		return CollectionUtils.getFirstElement(results);
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected ComplianceRuleOld getTarget(ComplianceRuleOldTargetHolder targetHolder) {
		return getComplianceOldService().getComplianceRuleOld(targetHolder.getRuleId());
	}


	@Override
	protected Collection<ComplianceRuleOldTargetHolder> getTargetHolders() {
		ComplianceRuleOldSearchForm form = new ComplianceRuleOldSearchForm();
		List<ComplianceRuleOld> rules = getComplianceOldService().getComplianceRuleOldList(form);

		List<ComplianceRuleOldTargetHolder> holders = new ArrayList<>();
		for (ComplianceRuleOld rule : CollectionUtils.getIterable(rules)) {
			holders.add(buildTargetHolder(rule));
		}

		return holders;
	}


	private ComplianceRuleOldTargetHolder buildTargetHolder(ComplianceRuleOld rule) {
		ComplianceRuleOldTargetHolder holder = new ComplianceRuleOldTargetHolder(generateKey(rule), rule);
		this.cacheUpdater.registerCacheUpdateEvent(rule, holder);

		return holder;
	}


	@Override
	protected SpecificityKeySource getKeySource(ComplianceRuleOldSource source) {
		return source.getKeySource();
	}


	private String generateKey(ComplianceRuleOld rule) {
		Object[] instrumentSpecificity = {BeanUtils.getBeanIdentity(rule.getInvestmentInstrument()), BeanUtils.getBeanIdentity(rule.getInvestmentHierarchy()),
				BeanUtils.getBeanIdentity(rule.getInvestmentType()), null};

		Object[] holdingSpecificity = {BeanUtils.getBeanIdentity(rule.getHoldingInvestmentAccount()), BeanUtils.getBeanIdentity(rule.getClientInvestmentAccount()), null};

		//TODO Currently trader-specific rules are not supported. Revisit this later.
		Object[] valuesToAppend = {rule.getRuleType().getId(), "notTraderSpecific"};

		return getKeyGenerator().generateKeyForTarget(SpecificityKeySource.builder(instrumentSpecificity).addHierarchy(holdingSpecificity).setValuesToAppend(valuesToAppend).build());
	}


	@SuppressWarnings("unused")
	@Override
	protected boolean isMatch(ComplianceRuleOldSource source, ComplianceRuleOldTargetHolder holder) {
		/*
		 * At this point it is assumed source and holder have matching "specificities". In the case of
		 * ComplianceRuleOldSpecificityCacheImpl, no further comparison is needed.
		 */
		return true;
	}


	@Override
	protected SpecificityCacheUpdater<ComplianceRuleOldTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}


	class ComplianceOldCacheUpdater extends DynamicSpecificityCacheUpdater<ComplianceRuleOldTargetHolder> {

		ComplianceOldCacheUpdater() {
			super(ComplianceRuleOld.class);
		}


		@Override
		protected List<ComplianceRuleOldTargetHolder> getTargetHoldersForCacheUpdate(Object sourceComponent) {
			if (sourceComponent instanceof ComplianceRuleOld) {
				return Collections.singletonList(buildTargetHolder((ComplianceRuleOld) sourceComponent));
			}

			return null;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ComplianceOldService getComplianceOldService() {
		return this.complianceOldService;
	}


	public void setComplianceOldService(ComplianceOldService complianceOldService) {
		this.complianceOldService = complianceOldService;
	}
}
