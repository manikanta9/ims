package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonthService;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseCompliancePositionExchangeLimitTypeCalculator</code> ...
 *
 * @author Mary Anderson
 */
public abstract class BaseCompliancePositionExchangeLimitTypeCalculator implements CompliancePositionExchangeLimitTypeCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentSecuritySpotMonthService investmentSecuritySpotMonthService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, AccountingBean accountingBean) {
		return isCompliancePositionExchangeLimitApplied(limit, accountingBean.getInvestmentSecurity(), accountingBean.getTransactionDate(), null);
	}


	protected abstract boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, InvestmentSecurity security, Date date, Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap);


	private void populateLimitPositions(CompliancePositionExchangeLimitPosition positionLimit, Boolean longOnly, List<AccountingPosition> positionList, List<Trade> pendingTradeList) {
		if (longOnly != null) {
			if (longOnly) {
				positionLimit.setLongOnly(true);
			}
			else {
				positionLimit.setLongOnly(false);
			}
		}
		for (AccountingPosition pos : CollectionUtils.getIterable(positionList)) {
			positionLimit.addPosition(pos);
		}

		for (Trade trade : CollectionUtils.getIterable(pendingTradeList)) {
			positionLimit.addAccountingBean(trade);
		}
	}


	protected boolean isSecurityInSpotMonth(CompliancePositionExchangeLimit limit, InvestmentSecurity security, Date date, Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap) {
		InvestmentSecuritySpotMonth spotMonth;
		if (securitySpotMonthMap != null && securitySpotMonthMap.containsKey(security.getId())) {
			spotMonth = securitySpotMonthMap.get(security.getId());
		}
		else {
			spotMonth = getInvestmentSecuritySpotMonthService().getInvestmentSecuritySpotMonth(security, limit.getLimitType().isInstrumentSpotMonthCalculatorRequired());
			if (securitySpotMonthMap != null) {
				securitySpotMonthMap.put(security.getId(), spotMonth);
			}
		}
		if (spotMonth == null) {
			return false;
		}

		// Scale Down Spot Month is the Last X days of the Spot Month Period
		if (limit.getLimitType().isScaleDownSpotMonth() && !MathUtils.isNullOrZero(limit.getSpotMonthStartBusinessDaysBack())) {
			Date startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(spotMonth.getEndDate(), getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId()),
					-limit.getSpotMonthStartBusinessDaysBack());
			return DateUtils.isDateBetween(date, startDate, spotMonth.getEndDate(), false);
		}
		return spotMonth.isSecurityInSpotMonth(date);
	}


	protected CompliancePositionExchangeLimitPosition getPositionLimitPopulated(CompliancePositionExchangeLimit limit, Boolean longOnly, Date monthDate,
	                                                                            List<AccountingPosition> accountingPositionList, List<Trade> pendingTradeList) {
		CompliancePositionExchangeLimitPosition positionLimit = new CompliancePositionExchangeLimitPosition();
		positionLimit.setLimit(limit);
		positionLimit.setMonthDate(monthDate);
		populateLimitPositions(positionLimit, longOnly, accountingPositionList, pendingTradeList);
		return positionLimit;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecuritySpotMonthService getInvestmentSecuritySpotMonthService() {
		return this.investmentSecuritySpotMonthService;
	}


	public void setInvestmentSecuritySpotMonthService(InvestmentSecuritySpotMonthService investmentSecuritySpotMonthService) {
		this.investmentSecuritySpotMonthService = investmentSecuritySpotMonthService;
	}
}
