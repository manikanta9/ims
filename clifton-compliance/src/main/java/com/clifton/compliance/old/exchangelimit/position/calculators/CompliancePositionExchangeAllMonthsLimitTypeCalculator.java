package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;
import com.clifton.trade.Trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>CompliancePositionExchangeLimitAllMonthLimitTypeCalculator</code> ...
 *
 * @author Mary Anderson
 */
public class CompliancePositionExchangeAllMonthsLimitTypeCalculator extends BaseCompliancePositionExchangeLimitTypeCalculator {

	/**
	 * All months includes everything
	 */
	@SuppressWarnings("unused")
	@Override
	protected boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, InvestmentSecurity security, Date date, Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap) {
		return true;
	}


	@Override
	public List<CompliancePositionExchangeLimitPosition> calculate(CompliancePositionExchangeLimit limit, @SuppressWarnings("unused") Date date, List<AccountingPosition> accountingPositionList,
	                                                               List<Trade> pendingTradeList) {
		// All Months Includes ALL!
		List<CompliancePositionExchangeLimitPosition> list = new ArrayList<>();
		if (limit.isApplyToLongsAndShortsSeparately()) {
			list.add(getPositionLimitPopulated(limit, true, null, accountingPositionList, pendingTradeList));
			list.add(getPositionLimitPopulated(limit, false, null, accountingPositionList, pendingTradeList));
		}
		else {
			list.add(getPositionLimitPopulated(limit, null, null, accountingPositionList, pendingTradeList));
		}
		return list;
	}
}
