package com.clifton.compliance.old.exchangelimit.search;


import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitTypes;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>CompliancePositionExchangeLimitSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class CompliancePositionExchangeLimitSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private CompliancePositionExchangeLimitTypes limitType;

	@SearchField(searchField = "instrument.id", sortField = "instrument.identifierPrefix")
	private Integer instrumentId;

	@SearchField(searchFieldPath = "instrument", searchField = "bigInstrument.id")
	private Integer bigInstrumentId;

	@SearchField(searchField = "instrument.id,instrument.bigInstrument.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Integer instrumentOrBigInstrumentId;

	@SearchField(searchFieldPath = "instrument", searchField = "hierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchFieldPath = "instrument", searchField = "exchange.id", sortField = "exchange.name")
	private Short instrumentExchangeId;

	@SearchField(searchFieldPath = "instrument", searchField = "spotMonthCalculatorBean.id", sortField = "spotMonthCalculatorBean.name")
	private Integer spotMonthCalculatorBeanId;

	@SearchField
	private Integer exchangeLimit;

	@SearchField
	private Boolean accountabilityLimit;

	@SearchField
	private Integer ourLimit;

	@SearchField(searchField = "ourLimit,exchangeLimit", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer coalesceOurExchangeLimit;

	@SearchField
	private Short documentFileCount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInstrumentExchangeId() {
		return this.instrumentExchangeId;
	}


	public void setInstrumentExchangeId(Short instrumentExchangeId) {
		this.instrumentExchangeId = instrumentExchangeId;
	}


	public Integer getExchangeLimit() {
		return this.exchangeLimit;
	}


	public void setExchangeLimit(Integer exchangeLimit) {
		this.exchangeLimit = exchangeLimit;
	}


	public Integer getOurLimit() {
		return this.ourLimit;
	}


	public void setOurLimit(Integer ourLimit) {
		this.ourLimit = ourLimit;
	}


	public Boolean getAccountabilityLimit() {
		return this.accountabilityLimit;
	}


	public void setAccountabilityLimit(Boolean accountabilityLimit) {
		this.accountabilityLimit = accountabilityLimit;
	}


	public CompliancePositionExchangeLimitTypes getLimitType() {
		return this.limitType;
	}


	public void setLimitType(CompliancePositionExchangeLimitTypes limitType) {
		this.limitType = limitType;
	}


	public Integer getBigInstrumentId() {
		return this.bigInstrumentId;
	}


	public void setBigInstrumentId(Integer bigInstrumentId) {
		this.bigInstrumentId = bigInstrumentId;
	}


	public Integer getInstrumentOrBigInstrumentId() {
		return this.instrumentOrBigInstrumentId;
	}


	public void setInstrumentOrBigInstrumentId(Integer instrumentOrBigInstrumentId) {
		this.instrumentOrBigInstrumentId = instrumentOrBigInstrumentId;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public Integer getCoalesceOurExchangeLimit() {
		return this.coalesceOurExchangeLimit;
	}


	public void setCoalesceOurExchangeLimit(Integer coalesceOurExchangeLimit) {
		this.coalesceOurExchangeLimit = coalesceOurExchangeLimit;
	}


	public Integer getSpotMonthCalculatorBeanId() {
		return this.spotMonthCalculatorBeanId;
	}


	public void setSpotMonthCalculatorBeanId(Integer spotMonthCalculatorBeanId) {
		this.spotMonthCalculatorBeanId = spotMonthCalculatorBeanId;
	}
}
