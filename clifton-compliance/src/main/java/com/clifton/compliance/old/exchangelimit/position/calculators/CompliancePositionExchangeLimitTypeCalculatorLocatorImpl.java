package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitTypes;
import com.clifton.core.context.ApplicationContextService;
import org.springframework.stereotype.Component;


/**
 * The <code>CompliancePositionExchangeLimitTypeCalculatorLocatorImpl</code> class is responsible for locating CompliancePositionExchangeLimitTypeCalculator
 * implementation beans for specific CompliancePositionExchangeLimitType
 *
 * @author manderson
 */
@Component
public class CompliancePositionExchangeLimitTypeCalculatorLocatorImpl implements CompliancePositionExchangeLimitTypeCalculatorLocator {

	private ApplicationContextService applicationContextService;


	/**
	 * Returns CompliancePositionExchangeLimitTypeCalculator implementation for the specified limit type
	 *
	 * @param limitType
	 */
	@Override
	public CompliancePositionExchangeLimitTypeCalculator locate(CompliancePositionExchangeLimitTypes limitType) {
		String name = limitType.getLabel();

		String calculatorName = "compliancePositionExchange" + name.replaceAll(" ", "") + "LimitTypeCalculator";
		return (CompliancePositionExchangeLimitTypeCalculator) getApplicationContextService().getContextBean(calculatorName);
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
