package com.clifton.compliance.old.exchangelimit.search;


import com.clifton.core.dataaccess.search.form.SearchForm;

import java.math.BigDecimal;


/**
 * The <code>CompliancePositionExchangeLimitPositionSearchForm</code> ...
 *
 * @author Mary Anderson
 */
@SearchForm(hasOrmDtoClass = false)
public class CompliancePositionExchangeLimitPositionSearchForm extends CompliancePositionExchangeLimitSearchForm {

	// Custom support for filtering/sorting on the calculated percentage field
	private BigDecimal percentOfLimit;


	public BigDecimal getPercentOfLimit() {
		return this.percentOfLimit;
	}


	public void setPercentOfLimit(BigDecimal percentOfLimit) {
		this.percentOfLimit = percentOfLimit;
	}
}
