package com.clifton.compliance.old.rule.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ComplianceRuleOldSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "ruleType.name")
	private String typeName;

	@SearchField(searchField = "ruleType.id")
	private Short typeId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "clientInvestmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.name")
	private String clientInvestmentAccountName;

	@SearchField(searchField = "holdingInvestmentAccount.name")
	private String holdingInvestmentAccountName;

	@SearchField(searchField = "investmentInstrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "investmentHierarchy.id")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "trader.id")
	private Short traderId;

	@SearchField
	private Boolean shortPositionAllowed;

	@SearchField
	private Boolean longPositionAllowed;


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getClientInvestmentAccountName() {
		return this.clientInvestmentAccountName;
	}


	public void setClientInvestmentAccountName(String clientInvestmentAccountName) {
		this.clientInvestmentAccountName = clientInvestmentAccountName;
	}


	public String getHoldingInvestmentAccountName() {
		return this.holdingInvestmentAccountName;
	}


	public void setHoldingInvestmentAccountName(String holdingInvestmentAccountName) {
		this.holdingInvestmentAccountName = holdingInvestmentAccountName;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Boolean getShortPositionAllowed() {
		return this.shortPositionAllowed;
	}


	public void setShortPositionAllowed(Boolean shortPositionAllowed) {
		this.shortPositionAllowed = shortPositionAllowed;
	}


	public Boolean getLongPositionAllowed() {
		return this.longPositionAllowed;
	}


	public void setLongPositionAllowed(Boolean longPositionAllowed) {
		this.longPositionAllowed = longPositionAllowed;
	}
}
