package com.clifton.compliance.old.exchangelimit.cache;

import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;

import java.util.List;


/**
 * @author manderson
 */
public interface CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache {

	public Integer[] getCompliancePositionExchangeLimitList(int instrumentId);


	public void storeCompliancePositionExchangeLimitList(int instrumentId, List<CompliancePositionExchangeLimit> list);
}
