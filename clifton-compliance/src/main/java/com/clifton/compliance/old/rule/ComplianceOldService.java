package com.clifton.compliance.old.rule;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.search.ComplianceRuleOldSearchForm;
import com.clifton.compliance.old.rule.search.ComplianceRuleTypeOldSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


public interface ComplianceOldService {

	////////////////////////////////////////////////////////////////////////////
	///////              Compliance Rule Business Methods              ///////// 
	////////////////////////////////////////////////////////////////////////////
	public ComplianceRuleOld getComplianceRuleOld(int id);


	@DoNotAddRequestMapping
	public ComplianceRuleOld getComplianceRuleOldForBeanAndType(AccountingBean bean, short typeId);


	public List<ComplianceRuleOld> getComplianceRuleOldList(ComplianceRuleOldSearchForm searchForm);


	public ComplianceRuleOld saveComplianceRuleOld(ComplianceRuleOld bean);


	public void deleteComplianceRuleOld(int id);


	////////////////////////////////////////////////////////////////////////////
	///////           Compliance Rule Type Business Methods            ///////// 
	////////////////////////////////////////////////////////////////////////////
	public ComplianceRuleTypeOld getComplianceRuleTypeOld(short id);


	public ComplianceRuleTypeOld getComplianceRuleTypeOldByName(String typeName);


	public List<ComplianceRuleTypeOld> getComplianceRuleTypeOldList(ComplianceRuleTypeOldSearchForm searchForm);
}
