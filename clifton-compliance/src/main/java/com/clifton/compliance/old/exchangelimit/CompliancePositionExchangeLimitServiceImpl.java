package com.clifton.compliance.old.exchangelimit;


import com.clifton.compliance.old.exchangelimit.cache.CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache;
import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class CompliancePositionExchangeLimitServiceImpl implements CompliancePositionExchangeLimitService {

	private AdvancedUpdatableDAO<CompliancePositionExchangeLimit, Criteria> compliancePositionExchangeLimitDAO;

	private CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache compliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache;

	////////////////////////////////////////////////////////////////////////////
	///////        Compliance Position Exchange Limit Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CompliancePositionExchangeLimit getCompliancePositionExchangeLimit(int id) {
		return getCompliancePositionExchangeLimitDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CompliancePositionExchangeLimit> getCompliancePositionExchangeLimitListForInstrumentOrBigInstrument(int instrumentId, Date activeOnDate, boolean includeAccountableLimits) {
		Integer[] ids = getCompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache().getCompliancePositionExchangeLimitList(instrumentId);
		List<CompliancePositionExchangeLimit> limitList = null;
		// Only look it up if null, if empty array, then nothing there
		if (ids == null) {
			CompliancePositionExchangeLimitSearchForm searchForm = new CompliancePositionExchangeLimitSearchForm();
			searchForm.setInstrumentOrBigInstrumentId(instrumentId);
			limitList = getCompliancePositionExchangeLimitList(searchForm);
			getCompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache().storeCompliancePositionExchangeLimitList(instrumentId, limitList);
		}
		else if (ids.length > 0) {
			limitList = getCompliancePositionExchangeLimitDAO().findByPrimaryKeys(ids);
		}

		List<CompliancePositionExchangeLimit> filteredList = new ArrayList<>();
		for (CompliancePositionExchangeLimit limit : CollectionUtils.getIterable(limitList)) {
			if ((!limit.isAccountabilityLimit() || includeAccountableLimits) && (DateUtils.isDateBetween(activeOnDate, limit.getStartDate(), limit.getEndDate(), false))) {
				filteredList.add(limit);
			}
		}
		return filteredList;
	}


	@Override
	public List<CompliancePositionExchangeLimit> getCompliancePositionExchangeLimitList(final CompliancePositionExchangeLimitSearchForm searchForm) {
		return getCompliancePositionExchangeLimitDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public CompliancePositionExchangeLimit saveCompliancePositionExchangeLimit(CompliancePositionExchangeLimit bean) {
		validateInstrumentSelection(bean);
		if (bean.getLimitType().isScaleDownSpotMonth()) {
			ValidationUtils.assertNotNull(bean.getSpotMonthStartBusinessDaysBack(), "Spot Month Scale Down: Business Days Back is Required.");
		}
		return getCompliancePositionExchangeLimitDAO().save(bean);
	}


	@Override
	public void deleteCompliancePositionExchangeLimit(int id) {
		getCompliancePositionExchangeLimitDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                     Helper Methods                      //////////
	////////////////////////////////////////////////////////////////////////////


	private void validateInstrumentSelection(CompliancePositionExchangeLimit bean) {
		ValidationUtils.assertNotNull(bean.getInstrument(), "Instrument selection is required", "instrument.labelShort");
		// If instrument has a big instrument
		if (bean.getInstrument().getBigInstrument() != null) {
			// And Hierarchy on the Instrument or the Big Instrument Allows Security Overrides for Price Multiplier
			// Then cannot create an exchange limit for the instrument, because no way of determining adjustment factor between the two
			String msgPrefix = "Cannot create an exchange limit for instrument [" + bean.getInstrument().getLabel() + "], because ";
			if (!bean.getInstrument().getHierarchy().isOneSecurityPerInstrument() && bean.getInstrument().getHierarchy().isSecurityPriceMultiplierOverrideAllowed()) {
				throw new ValidationException(msgPrefix + "this instrument has a big instrument, but supports security price multiplier overrides.");
			}
			else if (!bean.getInstrument().getBigInstrument().getHierarchy().isOneSecurityPerInstrument() && bean.getInstrument().getBigInstrument().getHierarchy().isSecurityPriceMultiplierOverrideAllowed()) {
				throw new ValidationException(msgPrefix + "this instrument has a big instrument whose hierarchy supports security price multiplier overrides.");
			}
		}
		if (bean.getLimitType().isInstrumentSpotMonthCalculatorRequired()) {
			ValidationUtils.assertNotNull(bean.getInstrument().getSpotMonthCalculatorBean(), "Spot Month Calculator is Required for Instruments used by exchange limit type [" + bean.getLimitType().getLabel() + "]. Please review/update the instrument spot month calculator.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<CompliancePositionExchangeLimit, Criteria> getCompliancePositionExchangeLimitDAO() {
		return this.compliancePositionExchangeLimitDAO;
	}


	public void setCompliancePositionExchangeLimitDAO(AdvancedUpdatableDAO<CompliancePositionExchangeLimit, Criteria> compliancePositionExchangeLimitDAO) {
		this.compliancePositionExchangeLimitDAO = compliancePositionExchangeLimitDAO;
	}


	public CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache getCompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache() {
		return this.compliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache;
	}


	public void setCompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache(CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache compliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache) {
		this.compliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache = compliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache;
	}
}
