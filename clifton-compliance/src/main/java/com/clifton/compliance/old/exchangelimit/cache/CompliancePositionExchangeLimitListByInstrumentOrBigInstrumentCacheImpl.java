package com.clifton.compliance.old.exchangelimit.cache;

import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCacheImpl caches the list of limits that apply to a specified instrument either directly or through the big instrument reference
 * Used for Trade (Accounting Bean) Warning Processing to quickly retrieve the limits that may need to be processed
 * <p>
 * When limits are updated - the entire cache is cleared.  This is fine because limits themselves are only updated a few times a year at most if ever.
 *
 * @author manderson
 */
@Component
public class CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCacheImpl extends SelfRegisteringSimpleDaoCache<CompliancePositionExchangeLimit, Integer, Integer[]> implements CompliancePositionExchangeLimitListByInstrumentOrBigInstrumentCache {


	@Override
	public Integer[] getCompliancePositionExchangeLimitList(int instrumentId) {
		return getCacheHandler().get(getCacheName(), instrumentId);
	}


	@Override
	public void storeCompliancePositionExchangeLimitList(int instrumentId, List<CompliancePositionExchangeLimit> list) {
		Integer[] ids;
		// Create a 0 size array so not null and we know not to look it up
		if (CollectionUtils.isEmpty(list)) {
			ids = new Integer[0];
		}
		else {
			ids = BeanUtils.getBeanIdentityArray(list, Integer.class);
		}
		getCacheHandler().put(getCacheName(), instrumentId, ids);
	}
}
