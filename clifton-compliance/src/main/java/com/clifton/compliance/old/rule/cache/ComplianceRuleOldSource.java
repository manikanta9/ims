package com.clifton.compliance.old.rule.cache;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;


/**
 * The <code>ComplianceRuleOldSource</code> is used with the {@link ComplianceRuleOldSpecificityCache} for
 * retrieving the most specific {@link ComplianceRuleOld} for a given {@link AccountingBean} and rule type.
 *
 * @author jgommels
 */
public class ComplianceRuleOldSource {

	private final AccountingBean bean;
	private final short ruleTypeId;

	private final SpecificityKeySource keySource;


	public ComplianceRuleOldSource(AccountingBean bean, short ruleTypeId) {
		this.bean = bean;
		this.ruleTypeId = ruleTypeId;

		InvestmentInstrument instrument = bean.getInvestmentSecurity().getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentType investmentType = hierarchy.getInvestmentType();

		Object[] instrumentSpecificity = {instrument.getId(), hierarchy.getId(), investmentType.getId(), null};

		Object[] holdingSpecificity = {bean.getHoldingInvestmentAccount() != null ? bean.getHoldingInvestmentAccount().getId() : null, bean.getClientInvestmentAccount().getId(), null};

		//TODO Currently trader-specific rules are not supported. Revisit this later.
		Object[] valuesToAppendToKey = {ruleTypeId, "notTraderSpecific"};

		this.keySource = SpecificityKeySource.builder(instrumentSpecificity).addHierarchy(holdingSpecificity).setValuesToAppend(valuesToAppendToKey).build();
	}


	public SpecificityKeySource getKeySource() {
		return this.keySource;
	}


	public AccountingBean getAccountingBean() {
		return this.bean;
	}


	public short getRuleTypeId() {
		return this.ruleTypeId;
	}


	public AccountingBean getBean() {
		return this.bean;
	}
}
