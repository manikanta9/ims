package com.clifton.compliance.old.rule.evaluator;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ComplianceLimitRestrictionRuleEvaluator<T extends IdentityObject & AccountingBean> implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private static final String SHORT_LONG_RESTRICTION = "Short/Long";
	private static final String FAT_FINGER_RESTRICTION = "Fat Finger Trader Limit";
	private static final String NOTIONAL_RESTRICTION = "Notional Limit";

	private ComplianceOldService complianceOldService;
	private RuleViolationService ruleViolationService;
	private RuleDefinitionService ruleDefinitionService;

	private String typeName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T bean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Integer linkedFkFieldId = (Integer) bean.getIdentity();
			ComplianceRuleTypeOld type = getComplianceOldService().getComplianceRuleTypeOldByName(getTypeName());
			ComplianceRuleOld restriction = getComplianceOldService().getComplianceRuleOldForBeanAndType(bean, type.getId());

			if (restriction == null || (restriction.isLongPositionAllowed() && restriction.isShortPositionAllowed())) {
				return ruleViolationList;
			}
			BigDecimal sharesAfterTrade = context.getSharesAfterTrade(bean, restriction.isUseSettlementDate());
			if (SHORT_LONG_RESTRICTION.equals(getTypeName())) {
				if ((bean.isBuy() && restriction.isLongPositionAllowed()) || (!bean.isBuy() && restriction.isShortPositionAllowed())) {
					return ruleViolationList;
				}
				if ((sharesAfterTrade.compareTo(BigDecimal.ZERO) < 0) && !restriction.isShortPositionAllowed()) {
					ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "shortPositionAllowed", null, bean, sharesAfterTrade));
				}
				else if ((sharesAfterTrade.compareTo(BigDecimal.ZERO) > 0) && !restriction.isLongPositionAllowed()) {
					ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "longPositionAllowed", null, bean, sharesAfterTrade));
				}
			}
			else if (FAT_FINGER_RESTRICTION.equals(getTypeName()) || NOTIONAL_RESTRICTION.equals(getTypeName())) {
				BigDecimal accountingNotionalAfterTrade = context.getNotionalAfterTrade(bean, restriction.isUseSettlementDate());
				ruleViolationList.addAll(checkBuySell(entityConfig, restriction, bean, linkedFkFieldId));
				ruleViolationList.addAll(checkPosition(entityConfig, restriction, bean, linkedFkFieldId, sharesAfterTrade, accountingNotionalAfterTrade));
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                       Helper Methods                       /////////
	////////////////////////////////////////////////////////////////////////////


	private List<RuleViolation> checkBuySell(EntityConfig entityConfig, ComplianceRuleOld restriction, AccountingBean bean, Integer linkedFkFieldId) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (bean.isBuy()) {
			if ((restriction.getMaxBuyQuantity() != null) && (restriction.getMaxBuyQuantity().compareTo(bean.getQuantity()) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxBuyQuantity", restriction.getMaxBuyQuantity(), bean, bean.getQuantity()));
			}
			if ((restriction.getMaxBuyAccountingNotional() != null) && (restriction.getMaxBuyAccountingNotional().compareTo(bean.getAccountingNotional()) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxBuyAccountingNotional", restriction.getMaxBuyAccountingNotional(), bean, bean.getAccountingNotional()));
			}
		}
		else {
			if ((restriction.getMaxSellQuantity() != null) && (restriction.getMaxSellQuantity().compareTo(bean.getQuantity()) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxSellQuantity", restriction.getMaxSellQuantity(), bean, bean.getQuantity()));
			}
			if ((restriction.getMaxSellAccountingNotional() != null) && (MathUtils.compare(MathUtils.abs(restriction.getMaxSellAccountingNotional()), MathUtils.abs(bean.getAccountingNotional())) == -1)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxSellAccountingNotional", MathUtils.abs(restriction.getMaxSellAccountingNotional()), bean, MathUtils.abs(bean.getAccountingNotional())));
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> checkPosition(EntityConfig entityConfig, ComplianceRuleOld restriction, AccountingBean bean, Integer linkedFkFieldId, BigDecimal
			sharesAfterTrade, BigDecimal accountingNotionalAfterTrade) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (MathUtils.isLessThan(accountingNotionalAfterTrade, BigDecimal.ZERO)) {
			if ((restriction.getMaxShortPositionAccountingNotional() != null) && (MathUtils.compare(MathUtils.abs(restriction.getMaxShortPositionAccountingNotional()), MathUtils.abs(accountingNotionalAfterTrade)) == -1)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxShortPositionAccountingNotional", restriction.getMaxShortPositionAccountingNotional(), bean, accountingNotionalAfterTrade));
			}
		}
		else {
			if ((restriction.getMaxLongPositionAccountingNotional() != null) && (restriction.getMaxLongPositionAccountingNotional().compareTo(accountingNotionalAfterTrade) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxLongPositionAccountingNotional", restriction.getMaxLongPositionAccountingNotional(), bean, accountingNotionalAfterTrade));
			}
		}
		if (MathUtils.isLessThan(sharesAfterTrade, BigDecimal.ZERO)) {
			if ((restriction.getMaxShortPositionQuantity() != null) && (MathUtils.compare(MathUtils.abs(restriction.getMaxShortPositionQuantity()), MathUtils.abs(sharesAfterTrade)) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxShortPositionQuantity", restriction.getMaxShortPositionQuantity(), bean, sharesAfterTrade));
			}
		}
		else {
			if ((restriction.getMaxLongPositionQuantity() != null) && (restriction.getMaxLongPositionQuantity().compareTo(sharesAfterTrade) < 0)) {
				ruleViolationList.add(createViolationForRestriction(entityConfig, linkedFkFieldId, restriction, "maxLongPositionQuantity", restriction.getMaxLongPositionQuantity(), bean, sharesAfterTrade));
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a new {@link RuleViolation} object for the given violation type and restriction with all of the links defined.
	 * NOTE: DOES NOT SET THE VIOLATION NOTE - Use generateViolationNote to apply the specific warning for the violation
	 *
	 * @param entityConfig
	 * @param linkedFkFieldId
	 * @param restriction
	 * @param bean
	 */


	private RuleViolation createViolationForRestriction(EntityConfig entityConfig, Integer linkedFkFieldId, ComplianceRuleOld restriction, String restrictionFieldName, BigDecimal restrictionAmount, AccountingBean bean, BigDecimal beanAmount) {
		RuleViolation ruleViolation;
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignment(entityConfig.getRuleAssignmentId());
		Integer causeFkFieldId = null;
		if (ruleAssignment.getRuleDefinition().getCauseTable() != null && "ComplianceRuleOld".equals(ruleAssignment.getRuleDefinition().getCauseTable().getName())) {
			causeFkFieldId = restriction.getId();
		}
		else if (!MathUtils.isEqual(bean.getSourceEntityId(), linkedFkFieldId)) {
			causeFkFieldId = bean.getSourceEntityId();
		}
		//add necessary values to the template config.
		Map<String, Object> templateValues = getTemplateValues(bean, beanAmount, restriction, restrictionFieldName, restrictionAmount);
		if (causeFkFieldId == null) {
			ruleViolation = getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, templateValues);
		}
		else {
			ruleViolation = getRuleViolationService().createRuleViolationWithCause(entityConfig, linkedFkFieldId, causeFkFieldId, null, templateValues);
		}
		return ruleViolation;
	}


	private Map<String, Object> getTemplateValues(AccountingBean bean, BigDecimal beanAmount, ComplianceRuleOld restriction, String restrictionFieldName, BigDecimal restrictionAmount) {
		Map<String, Object> templateValues = new HashMap<>();
		templateValues.put("bean", bean);
		if (beanAmount != null) {
			templateValues.put("beanAmount", beanAmount);
		}
		templateValues.put("restriction", restriction);
		templateValues.put("restrictionFieldName", restrictionFieldName);
		if (restrictionAmount != null) {
			templateValues.put("restrictionAmount", restrictionAmount);
		}
		return templateValues;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceOldService getComplianceOldService() {
		return this.complianceOldService;
	}


	public void setComplianceOldService(ComplianceOldService complianceOldService) {
		this.complianceOldService = complianceOldService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
