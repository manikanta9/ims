package com.clifton.compliance.old.rule.evaluator;

import com.clifton.accounting.AccountingBean;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public class ComplianceRuleCurrentContractRuleEvaluator<T extends IdentityObject & AccountingBean> implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private RuleViolationService ruleViolationService;

	/**
	 * Use settlement date to look up positions.
	 */
	private boolean useSettlementDate = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T bean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		Integer linkedFkFieldId = (Integer) bean.getIdentity();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			InvestmentSecurity currentSecurity = getInvestmentSecurityGroupService().getInvestmentSecurityCurrentByInstrument(bean.getInvestmentSecurity().getInstrument().getId(), bean.getTransactionDate(), true);
			if (currentSecurity != null && !currentSecurity.equals(bean.getInvestmentSecurity())) {
				BigDecimal currentQuantity = context.getSharesBeforeTrade(bean, isUseSettlementDate());

				BigDecimal afterTradeQuantity = (bean.isBuy() ? MathUtils.add(currentQuantity, bean.getQuantity()) : MathUtils.subtract(currentQuantity, bean.getQuantity()));
				if (isOpenMoreTrades(currentQuantity, afterTradeQuantity)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("currentSecurity", currentSecurity);
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	protected boolean isOpenMoreTrades(BigDecimal beforeTradeQuantity, BigDecimal afterTradeQuantity) {
		// If ending quantity is 0
		if (MathUtils.isEqual(afterTradeQuantity, 0)) {
			return false;
		}
		// If beginning quantity is 0
		if (MathUtils.isEqual(beforeTradeQuantity, 0)) {
			return true;
		}
		// If short and new quantity is less than original or long
		if (MathUtils.isLessThan(beforeTradeQuantity, 0)) {
			if (MathUtils.isGreaterThan(afterTradeQuantity, 0)) {
				return true;
			}
			return MathUtils.isLessThan(afterTradeQuantity, beforeTradeQuantity);
		}
		// Otherwise long - so if new is greater than original, or less than 0
		if (MathUtils.isLessThan(afterTradeQuantity, 0)) {
			return true;
		}
		return MathUtils.isGreaterThan(afterTradeQuantity, beforeTradeQuantity);
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}
}
