package com.clifton.compliance.old.rule;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.cache.ComplianceRuleOldSpecificityCache;
import com.clifton.compliance.old.rule.search.ComplianceRuleOldSearchForm;
import com.clifton.compliance.old.rule.search.ComplianceRuleTypeOldSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ComplianceOldServiceImpl implements ComplianceOldService {

	private AdvancedUpdatableDAO<ComplianceRuleOld, Criteria> complianceRuleOldDAO;
	private AdvancedUpdatableDAO<ComplianceRuleTypeOld, Criteria> complianceRuleTypeOldDAO;

	private DaoNamedEntityCache<ComplianceRuleTypeOld> complianceRuleTypeOldCache;
	private ComplianceRuleOldSpecificityCache complianceRuleOldSpecificityCache;


	//////////////////////////////////////////////////////////////////////////// 
	///////       Compliance Trade Restriction Business Methods        ///////// 
	////////////////////////////////////////////////////////////////////////////
	@Override
	public ComplianceRuleOld getComplianceRuleOld(int id) {
		return getComplianceRuleOldDAO().findByPrimaryKey(id);
	}


	@Override
	public ComplianceRuleOld getComplianceRuleOldForBeanAndType(final AccountingBean bean, final short typeId) {
		return getComplianceRuleOldSpecificityCache().getMostSpecificComplianceRuleOld(bean, typeId);
	}


	@Override
	public List<ComplianceRuleOld> getComplianceRuleOldList(ComplianceRuleOldSearchForm searchForm) {
		return getComplianceRuleOldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// Validation: {@link ComplianceRuleOldValidator}  
	@Override
	public ComplianceRuleOld saveComplianceRuleOld(ComplianceRuleOld bean) {
		return getComplianceRuleOldDAO().save(bean);
	}


	// Validation: {@link ComplianceRuleOldValidator}  
	@Override
	public void deleteComplianceRuleOld(int id) {
		getComplianceRuleOldDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////     Compliance Trade Restriction Type Business Methods     ///////// 
	////////////////////////////////////////////////////////////////////////////
	@Override
	public ComplianceRuleTypeOld getComplianceRuleTypeOld(short id) {
		return getComplianceRuleTypeOldDAO().findByPrimaryKey(id);
	}


	@Override
	public ComplianceRuleTypeOld getComplianceRuleTypeOldByName(String typeName) {
		return getComplianceRuleTypeOldCache().getBeanForKeyValueStrict(getComplianceRuleTypeOldDAO(), typeName);
	}


	@Override
	public List<ComplianceRuleTypeOld> getComplianceRuleTypeOldList(ComplianceRuleTypeOldSearchForm searchForm) {
		return getComplianceRuleTypeOldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//////////////////////////////////////////////////////////////////////////// 
	///////                   Getter & Setter Methods                  ///////// 
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<ComplianceRuleOld, Criteria> getComplianceRuleOldDAO() {
		return this.complianceRuleOldDAO;
	}


	public void setComplianceRuleOldDAO(AdvancedUpdatableDAO<ComplianceRuleOld, Criteria> complianceRuleOldDAO) {
		this.complianceRuleOldDAO = complianceRuleOldDAO;
	}


	public AdvancedUpdatableDAO<ComplianceRuleTypeOld, Criteria> getComplianceRuleTypeOldDAO() {
		return this.complianceRuleTypeOldDAO;
	}


	public void setComplianceRuleTypeOldDAO(AdvancedUpdatableDAO<ComplianceRuleTypeOld, Criteria> complianceRuleTypeOldDAO) {
		this.complianceRuleTypeOldDAO = complianceRuleTypeOldDAO;
	}


	public DaoNamedEntityCache<ComplianceRuleTypeOld> getComplianceRuleTypeOldCache() {
		return this.complianceRuleTypeOldCache;
	}


	public void setComplianceRuleTypeOldCache(DaoNamedEntityCache<ComplianceRuleTypeOld> complianceRuleTypeOldCache) {
		this.complianceRuleTypeOldCache = complianceRuleTypeOldCache;
	}


	public ComplianceRuleOldSpecificityCache getComplianceRuleOldSpecificityCache() {
		return this.complianceRuleOldSpecificityCache;
	}


	public void setComplianceRuleOldSpecificityCache(ComplianceRuleOldSpecificityCache complianceRuleOldSpecificityCache) {
		this.complianceRuleOldSpecificityCache = complianceRuleOldSpecificityCache;
	}
}
