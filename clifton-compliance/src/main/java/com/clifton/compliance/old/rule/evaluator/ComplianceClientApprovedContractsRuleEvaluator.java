package com.clifton.compliance.old.rule.evaluator;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ComplianceClientApprovedContractsRuleEvaluator<T extends IdentityObject & AccountingBean> implements RuleEvaluator<T, TradeRuleEvaluatorContext> {

	private ComplianceOldService complianceOldService;
	private RuleViolationService ruleViolationService;

	private String typeName = "Client Approved Contracts";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T bean, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			// don't run the rule for Cash: client account base currency is always allowed
			if (!InvestmentUtils.isSecurityEqualToClientAccountBaseCurrency(bean)) {
				ComplianceRuleTypeOld type = getComplianceOldService().getComplianceRuleTypeOldByName(getTypeName());
				ComplianceRuleOld restriction = getComplianceOldService().getComplianceRuleOldForBeanAndType(bean, type.getId());

				// a restriction must exist or the security is not allowed
				if (restriction == null) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("holdingInvestmentAccountLabel", bean.getHoldingInvestmentAccount() != null ? bean.getHoldingInvestmentAccount().getLabel() : "NOT SET");
					Integer linkedFkFieldId = (Integer) bean.getIdentity();
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, linkedFkFieldId, bean.getInvestmentSecurity().getInstrument().getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ComplianceOldService getComplianceOldService() {
		return this.complianceOldService;
	}


	public void setComplianceOldService(ComplianceOldService complianceOldService) {
		this.complianceOldService = complianceOldService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
