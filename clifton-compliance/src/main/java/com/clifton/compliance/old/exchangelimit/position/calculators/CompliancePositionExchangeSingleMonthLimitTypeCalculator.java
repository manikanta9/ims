package com.clifton.compliance.old.exchangelimit.position.calculators;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitService;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;
import com.clifton.trade.Trade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CompliancePositionExchangeSingleMonthLimitTypeCalculator extends BaseCompliancePositionExchangeLimitTypeCalculator {

	private CompliancePositionExchangeLimitService compliancePositionExchangeLimitService;


	/**
	 * Only applies if the trade is for a security NOT during it's spot month OR there doesn't exist a Spot Month Limit Type for the Instrument
	 */
	@Override
	protected boolean isCompliancePositionExchangeLimitApplied(CompliancePositionExchangeLimit limit, InvestmentSecurity security, Date date, Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap) {
		// First Check if we are In the Spot Month
		if (isSecurityInSpotMonth(limit, security, date, securitySpotMonthMap)) {
			// If we are, then also confirm there is a spot month limit defined for the instrument
			// Note: This look up is cached
			List<CompliancePositionExchangeLimit> instrumentLimitList = getCompliancePositionExchangeLimitService().getCompliancePositionExchangeLimitListForInstrumentOrBigInstrument(limit.getInstrument().getId(), date, true);
			// If we find a matching spot month limit type defined - break out of loop, we don't need to include the single month for this security
			for (CompliancePositionExchangeLimit exchangeLimit : CollectionUtils.getIterable(instrumentLimitList)) {
				if (exchangeLimit.getLimitType().isInstrumentSpotMonthCalculatorRequired() && exchangeLimit.isAccountabilityLimit() == limit.isAccountabilityLimit()) {
					return false;
				}
			}
		}
		return true;
	}


	@Override
	public List<CompliancePositionExchangeLimitPosition> calculate(CompliancePositionExchangeLimit limit, Date date, List<AccountingPosition> accountingPositionList, List<Trade> pendingTradeList) {
		Map<Integer, InvestmentSecuritySpotMonth> securitySpotMonthMap = new HashMap<>();

		Map<Date, List<AccountingPosition>> monthPositions = new HashMap<>();
		Map<Date, List<Trade>> monthTrades = new HashMap<>();

		Map<InvestmentSecurity, List<AccountingPosition>> securityPositionMap = BeanUtils.getBeansMap(accountingPositionList, AccountingPosition::getInvestmentSecurity);
		Map<InvestmentSecurity, List<Trade>> securityTradeMap = BeanUtils.getBeansMap(pendingTradeList, Trade::getInvestmentSecurity);

		for (Map.Entry<InvestmentSecurity, List<AccountingPosition>> investmentSecurityListEntry : securityPositionMap.entrySet()) {
			if (isCompliancePositionExchangeLimitApplied(limit, investmentSecurityListEntry.getKey(), date, securitySpotMonthMap)) {
				Date monthDate = DateUtils.getFirstDayOfMonth((investmentSecurityListEntry.getKey()).getLastDeliveryOrEndDate());
				List<AccountingPosition> posList = monthPositions.get(monthDate);
				if (posList == null) {
					posList = new ArrayList<>();
				}
				posList.addAll(investmentSecurityListEntry.getValue());
				monthPositions.put(monthDate, posList);
			}
		}

		for (Map.Entry<InvestmentSecurity, List<Trade>> investmentSecurityListEntry : securityTradeMap.entrySet()) {
			if (isCompliancePositionExchangeLimitApplied(limit, investmentSecurityListEntry.getKey(), date, securitySpotMonthMap)) {
				Date monthDate = DateUtils.getFirstDayOfMonth((investmentSecurityListEntry.getKey()).getLastDeliveryOrEndDate());
				List<Trade> tradeList = monthTrades.get(monthDate);
				if (tradeList == null) {
					tradeList = new ArrayList<>();
				}
				tradeList.addAll(investmentSecurityListEntry.getValue());
				monthTrades.put(monthDate, tradeList);
				if (!monthPositions.containsKey(monthDate)) {
					monthPositions.put(monthDate, null);
				}
			}
		}


		List<CompliancePositionExchangeLimitPosition> list = new ArrayList<>();
		for (Map.Entry<Date, List<AccountingPosition>> dateListEntry : monthPositions.entrySet()) {
			if (limit.isApplyToLongsAndShortsSeparately()) {
				list.add(getPositionLimitPopulated(limit, true, dateListEntry.getKey(), dateListEntry.getValue(), monthTrades.get(dateListEntry.getKey())));
				list.add(getPositionLimitPopulated(limit, false, dateListEntry.getKey(), dateListEntry.getValue(), monthTrades.get(dateListEntry.getKey())));
			}
			else {
				list.add(getPositionLimitPopulated(limit, null, dateListEntry.getKey(), dateListEntry.getValue(), monthTrades.get(dateListEntry.getKey())));
			}
		}
		return list;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public CompliancePositionExchangeLimitService getCompliancePositionExchangeLimitService() {
		return this.compliancePositionExchangeLimitService;
	}


	public void setCompliancePositionExchangeLimitService(CompliancePositionExchangeLimitService compliancePositionExchangeLimitService) {
		this.compliancePositionExchangeLimitService = compliancePositionExchangeLimitService;
	}
}
