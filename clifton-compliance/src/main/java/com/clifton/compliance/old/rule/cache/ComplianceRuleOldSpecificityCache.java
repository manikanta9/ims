package com.clifton.compliance.old.rule.cache;


import com.clifton.accounting.AccountingBean;
import com.clifton.compliance.old.rule.ComplianceRuleOld;


public interface ComplianceRuleOldSpecificityCache {


	public ComplianceRuleOld getMostSpecificComplianceRuleOld(AccountingBean bean, final short typeId);
}
