package com.clifton.compliance.old.rule.validation;


import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;


/**
 * The <code>ComplianceRuleOldValidator</code> performs validation on ComplianceRuleOld object based on the rule type options for what is allowed.
 * Also enforces:
 * Only one of Hierarchy, Type, Instrument is selected and at least one of them is selected
 * Either Client Account OR Holding Account can be selected, but not both
 * If Global rule (i.e. Client and Holding Account) are both blank, user is required to be an admin
 * <p/>
 * For Deletes - Only validates Admin condition for global rules
 *
 * @author manderson
 */
@Component
public class ComplianceRuleOldValidator extends SelfRegisteringDaoValidator<ComplianceRuleOld> {

	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ComplianceRuleOld bean, DaoEventTypes config) throws ValidationException {

		ComplianceRuleOld originalBean = null;
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
		}
		if ((originalBean != null && originalBean.isGlobalRule()) || bean.isGlobalRule()) {
			boolean isAdmin = getSecurityAuthorizationService().isSecurityUserAdmin();
			ValidationUtils.assertTrue(isAdmin, "Only Administrators are allowed to create/edit/delete global rules.");
		}

		if (config.isInsert() || config.isUpdate()) {
			if (bean.getRuleType().isInvestmentSpecific()) {
				// Validate that EITHER Hierarchy, Type, OR Instrument is Populated
				ValidationUtils.assertMutuallyExclusive("Investment Hierarchy, Instrument, OR Investment Type is required and only one of these selections is allowed.", bean.getInvestmentHierarchy(),
						bean.getInvestmentInstrument(), bean.getInvestmentType());
			}
			if (bean.getRuleType().isInvestmentAccountRequired() && (bean.getClientInvestmentAccount() == null)) {
				throw new FieldValidationException("Client Account selection is Required.", "clientInvestmentAccount");
			}
			else if (!bean.getRuleType().isInvestmentAccountAllowed() && (bean.getClientInvestmentAccount() != null)) {
				throw new FieldValidationException("Client Account selection is not allowed.", "clientInvestmentAccount");
			}
			if (bean.getClientInvestmentAccount() != null && bean.getHoldingInvestmentAccount() != null) {
				throw new ValidationException("Client Account OR Holding Account is allowed, but not both.");
			}

			// Note: UI should have proper configuration to hide the field if the type isn't ignorable, but just in case
			if (!bean.getRuleType().isIgnorableAllowed() && bean.isIgnorable()) {
				throw new ValidationException("Rule Type [" + bean.getRuleType().getName() + "] does not allow ignorable option to be set.");
			}
		}
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
