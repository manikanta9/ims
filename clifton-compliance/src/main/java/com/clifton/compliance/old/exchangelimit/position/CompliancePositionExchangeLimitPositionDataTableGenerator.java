package com.clifton.compliance.old.exchangelimit.position;


import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitPositionSearchForm;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitDataTableGenerator</code> ...
 *
 * @author Mary Anderson
 */
public class CompliancePositionExchangeLimitPositionDataTableGenerator implements DataTableGenerator {

	private ApplicationContextService applicationContextService;
	private CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService;

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	// TODO May want to move this to be a bean property, or allow additional properties through bean properties
	// Should probably have these as standard and allow adding more through bean properties?
	// Only concern with using limit id as the id is if the limit applies to more than one month and we are over for more than one
	private static final String COLUMNS = "ID:limit.id|" + //
			"LimitID:limit.id|" + //
			"Limit:limit.label|" + //
			"Exchange:limit.instrument.exchange.name|" + //
			"Instrument:limit.instrument.label|" + //
			"Limit Type:limit.limitType.label|" + //
			"Accountability:limit.accountabilityLimit|" + //
			"Description:positionLimitLabel|" + //
			"Start Date:limit.startDate|" + //
			"End Date:limit.endDate|" + //
			"Position Limit:limit.limit|" + //
			"Total Positions:totalCount|" + //
			"Percent of Limit:percentOfLimit|" + //
			"SystemPriority:limit.priorityOverride|";

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	private BigDecimal minimumWarningLimit;
	private BigDecimal maximumWarningLimit;

	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////


	@Override
	public DataTable getDataTable() {
		ValidationUtils.assertNotNull(getMinimumWarningLimit(), "Minimum Warning Limit is required.");
		CompliancePositionExchangeLimitPositionSearchForm searchForm = new CompliancePositionExchangeLimitPositionSearchForm();
		searchForm.setActiveOnDate(new Date());
		searchForm.addSearchRestriction(new SearchRestriction("percentOfLimit", ComparisonConditions.GREATER_THAN, getMinimumWarningLimit()));
		if (getMaximumWarningLimit() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("percentOfLimit", ComparisonConditions.LESS_THAN, getMaximumWarningLimit()));
		}
		List<CompliancePositionExchangeLimitPosition> overList = getCompliancePositionExchangeLimitPositionService().getCompliancePositionExchangeLimitPositionList(searchForm, false);
		BeanCollectionToDataTableConverter<CompliancePositionExchangeLimitPosition> converter = new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(),
				CompliancePositionExchangeLimitPosition.class, COLUMNS));
		return converter.convert(overList);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public CompliancePositionExchangeLimitPositionService getCompliancePositionExchangeLimitPositionService() {
		return this.compliancePositionExchangeLimitPositionService;
	}


	public void setCompliancePositionExchangeLimitPositionService(CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService) {
		this.compliancePositionExchangeLimitPositionService = compliancePositionExchangeLimitPositionService;
	}


	public BigDecimal getMinimumWarningLimit() {
		return this.minimumWarningLimit;
	}


	public void setMinimumWarningLimit(BigDecimal minimumWarningLimit) {
		this.minimumWarningLimit = minimumWarningLimit;
	}


	public BigDecimal getMaximumWarningLimit() {
		return this.maximumWarningLimit;
	}


	public void setMaximumWarningLimit(BigDecimal maximumWarningLimit) {
		this.maximumWarningLimit = maximumWarningLimit;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
