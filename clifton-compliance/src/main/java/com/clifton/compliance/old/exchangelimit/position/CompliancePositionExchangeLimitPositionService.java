package com.clifton.compliance.old.exchangelimit.position;

import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitPositionSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;

import java.util.Date;
import java.util.List;


/**
 * The <code>CompliancePositionExchangeLimitPositionService</code> contains methods for comparing positions to defined exchange limits.
 *
 * @author manderson
 */
public interface CompliancePositionExchangeLimitPositionService {


	////////////////////////////////////////////////////////////////////////////////
	/////////     Compliance Position Exchange Limit Position Methods     //////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CompliancePositionExchangeLimit.class)
	public List<CompliancePositionExchangeLimitPosition> getCompliancePositionExchangeLimitPositionList(CompliancePositionExchangeLimitPositionSearchForm searchForm, boolean excludePendingTrades);


	public List<CompliancePositionExchangeLimitPosition> getCompliancePositionExchangeLimitPositionListForAccountingBean(AccountingBean accountingBean, boolean includeAccountabilityLimits);

	////////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Related Methods               /////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CompliancePositionExchangeLimit.class, namingConventionViolation = true)
	public List<AccountingPosition> getCompliancePositionExchangeLimitAccountingPositionList(int limitId, Date date);


	////////////////////////////////////////////////////////////////////////////////
	/////////////                Trade Related Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CompliancePositionExchangeLimit.class)
	public List<Trade> getCompliancePositionExchangeLimitPendingTradeList(int limitId);
}
