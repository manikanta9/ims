package com.clifton.compliance;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class ComplianceBaseHandlerImpl implements ComplianceBaseHandler {

	private MarketDataFieldService marketDataFieldService;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;


	@Override
	public BigDecimal calculateSharesOutstanding(InvestmentSecurity security, Date processDate) {
		BigDecimal value = null;

		MarketDataValueHolder marketValue = getMarketDataFieldService().getMarketDataValueForDateFlexible(security.getId(), processDate, MarketDataField.FIELD_SHARES_OUTSTANDING);

		if (marketValue != null) {
			value = marketValue.getMeasureValue();
		}

		if (value == null) {
			value = new BigDecimal(-1); //can't cache nulls
		}

		if (MathUtils.isEqual(value, new BigDecimal(-1))) {
			value = null;
		}

		ValidationUtils.assertNotNull(value, "Missing Shares Outstanding Market Data Field for Security " + security.getLabel());

		return value;
	}


	@Override
	public SystemHierarchy getIndustryFromSecurity(InvestmentSecurity security, Integer level) {
		SystemHierarchy industry = getIndustry(security);
		return ComplianceUtils.traverseHierarchy(industry, level);
	}


	@Override
	public List<SystemHierarchy> createHierarchyExclusionList(List<Short> exclusionIdList) {
		List<SystemHierarchy> hierarchyExclusionList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(exclusionIdList)) {
			for (Short industryExclusionId : CollectionUtils.getIterable(exclusionIdList)) {
				SystemHierarchy hierarchyToExclude = getSystemHierarchyDefinitionService().getSystemHierarchy(industryExclusionId);
				if (hierarchyToExclude != null) {
					hierarchyExclusionList.add(hierarchyToExclude);
				}
			}
		}

		return hierarchyExclusionList;
	}


	@Override
	public SystemHierarchy getIndustryFromSecurity(InvestmentSecurity security) {

		if (security.getInstrument() == null) {
			return null;
		}
		return getSystemHierarchy("InvestmentInstrument", security.getInstrument().getId(), "BICS Hierarchy");
	}


	@Override
	public SystemHierarchy getIndustryFromIssuer(InvestmentSecurity security) {

		if (security.getBusinessCompany() == null) {
			return null;
		}

		return getSystemHierarchy("BusinessCompany", security.getBusinessCompany().getId(), "BICS Hierarchy");
	}


	private SystemHierarchy getSystemHierarchy(String tableName, int fkFieldId, String hierarchyCategoryName) {
		return getSystemHierarchyAssignmentService().getSystemHierarchyByLink(tableName, fkFieldId, hierarchyCategoryName);
	}


	private SystemHierarchy getIndustry(InvestmentSecurity security) {
		SystemHierarchy hierarchyIndustry = getIndustryFromIssuer(security);

		if (hierarchyIndustry == null) {
			hierarchyIndustry = getIndustryFromSecurity(security);
		}

		return hierarchyIndustry;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
