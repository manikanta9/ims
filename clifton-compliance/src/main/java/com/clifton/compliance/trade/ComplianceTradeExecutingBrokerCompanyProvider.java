package com.clifton.compliance.trade;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluator;
import com.clifton.compliance.rule.evaluator.impl.ComplianceRuleExecutingBroker;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyProvider;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyProviderResult;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Provides a list of allowed brokers for given client and/or security
 *
 * @author mwacker
 */
@Component
public class ComplianceTradeExecutingBrokerCompanyProvider implements TradeExecutingBrokerCompanyProvider {

	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private SystemBeanService systemBeanService;
	private BusinessCompanyService businessCompanyService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeExecutingBrokerCompanyProviderResult getAllowedBrokerCompanyList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date) {
		List<ComplianceRuleAssignment> assignmentList = getComplianceRuleAssignmentHandler().getAssignmentRealTimeList(clientAccount, security, date, true, false);
		assignmentList = BeanUtils.filter(assignmentList, assignment -> ComplianceRuleType.EXECUTING_BROKER_RULE_TYPE_NAME.equals(assignment.getRule().getRuleType().getName()));

		Set<Integer> excludeExecutingBrokerIdList = new HashSet<>();
		Set<Integer> includeExecutingBrokerIdList = new HashSet<>();
		for (ComplianceRuleAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			ComplianceRuleEvaluator ruleEvaluator = (ComplianceRuleEvaluator) getSystemBeanService().getBeanInstance(assignment.getRule().getRuleEvaluatorBean());
			if (ruleEvaluator instanceof ComplianceRuleExecutingBroker) {
				ComplianceRuleExecutingBroker evaluator = (ComplianceRuleExecutingBroker) ruleEvaluator;
				if (evaluator.isInclusion()) {
					includeExecutingBrokerIdList.addAll(evaluator.getExecutingBrokerCompanyIdList());
				}
				else {
					excludeExecutingBrokerIdList.addAll(evaluator.getExecutingBrokerCompanyIdList());
				}
			}
		}

		TradeExecutingBrokerCompanyProviderResult result = new TradeExecutingBrokerCompanyProviderResult();
		result.setIncludeExecutingBrokerCompanyList(populateBrokerList(includeExecutingBrokerIdList));
		result.setExcludedExecutingBrokerCompanyList(populateBrokerList(excludeExecutingBrokerIdList));
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Set<BusinessCompany> populateBrokerList(Set<Integer> brokerIdList) {
		Set<BusinessCompany> executingBrokerList = new HashSet<>();
		for (Integer executingBrokerId : CollectionUtils.getIterable(brokerIdList)) {
			executingBrokerList.add(getBusinessCompanyService().getBusinessCompany(executingBrokerId));
		}
		return executingBrokerList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
