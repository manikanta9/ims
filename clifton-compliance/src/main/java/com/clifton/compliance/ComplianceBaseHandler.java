package com.clifton.compliance;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.hierarchy.definition.SystemHierarchy;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public interface ComplianceBaseHandler {

	public BigDecimal calculateSharesOutstanding(InvestmentSecurity security, Date processDate);


	public SystemHierarchy getIndustryFromSecurity(InvestmentSecurity security, Integer level);


	public List<SystemHierarchy> createHierarchyExclusionList(List<Short> exclusionIdList);


	public SystemHierarchy getIndustryFromIssuer(InvestmentSecurity security);


	public SystemHierarchy getIndustryFromSecurity(InvestmentSecurity security);
}
