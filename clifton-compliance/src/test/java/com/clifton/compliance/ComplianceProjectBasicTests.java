package com.clifton.compliance;


import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceProjectBasicTests extends BasicProjectTests {


	@Override
	protected boolean isMethodSkipped(Method method) {
		List<String> skippedMethodList = Arrays.asList(
				"saveComplianceRule",
				"deleteComplianceRule"
		);
		return skippedMethodList.contains(method.getName());
	}


	@Override
	public String getProjectPrefix() {
		return "compliance";
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveComplianceCreditRatingValueForIssuer");
		ignoredVoidSaveMethodSet.add("saveComplianceCreditRatingValueForSecurity");
		ignoredVoidSaveMethodSet.add("saveComplianceRuleAssignmentBulk");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("old");
	}


	@Override
	protected Map<String, IdentityObject> configureCacheDtoInstanceOverrideMap() {
		Map<String, IdentityObject> map = new HashMap<>();
		map.put("complianceCreditRatingValueForIssuerCache", new ComplianceCreditRatingValueForIssuer());
		return map;
	}
}
