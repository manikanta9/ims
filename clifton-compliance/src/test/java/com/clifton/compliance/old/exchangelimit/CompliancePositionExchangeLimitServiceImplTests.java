package com.clifton.compliance.old.exchangelimit;


import com.clifton.compliance.old.exchangelimit.position.calculators.CompliancePositionExchangeLimitTypeCalculator;
import com.clifton.compliance.old.exchangelimit.position.calculators.CompliancePositionExchangeLimitTypeCalculatorLocator;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class CompliancePositionExchangeLimitServiceImplTests {

	@Resource
	private CompliancePositionExchangeLimitService compliancePositionExchangeLimitService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private CompliancePositionExchangeLimitTypeCalculatorLocator compliancePositionExchangeLimitTypeCalculatorLocator;


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	@Test
	public void testAllLimitTypesHaveCalculators() {
		for (CompliancePositionExchangeLimitTypes limitType : CompliancePositionExchangeLimitTypes.values()) {
			CompliancePositionExchangeLimitTypeCalculator calculator = this.compliancePositionExchangeLimitTypeCalculatorLocator.locate(limitType);
			Assertions.assertNotNull(calculator, "Cannot find calculator for limit type [" + limitType + "].");
		}
	}


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	@Test
	public void testNoLimitEntered() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(1);
			CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
			bean.setInstrument(i);
			bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);
		}, "A position limit is required.  Please enter an exchange and/or Clifton limit.");
	}


	@Test
	public void testExchangeLimitEnteredOnly() {
		InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(1);
		CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
		bean.setInstrument(i);
		bean.setExchangeLimit(1000);
		bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);
	}


	@Test
	public void testOurLimitEnteredOnly() {
		InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(1);
		CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
		bean.setInstrument(i);
		bean.setOurLimit(1000);
		bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);
	}


	@Test
	public void testBadDateRange() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(1);
			CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
			bean.setInstrument(i);
			bean.setOurLimit(1000);
			bean.setStartDate(DateUtils.toDate("01/01/2011"));
			bean.setEndDate(DateUtils.toDate("11/20/2010"));
			bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);
		}, "Invalid Start/End Dates entered. Start Date must be before End Date.");
	}


	@Test
	public void testNoOverlapDates() {
		InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(4);
		CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
		bean.setInstrument(i);
		bean.setOurLimit(1000);
		bean.setEndDate(DateUtils.toDate("5/1/2011"));
		bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

		CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
		bean2.setInstrument(i);
		bean2.setOurLimit(1005);
		bean2.setStartDate(DateUtils.toDate("5/2/2011"));
		bean2.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
	}


	@Test
	public void testOverlapDatesButDifferentTypes() {
		InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(4);
		CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
		bean.setInstrument(i);
		bean.setOurLimit(1000);
		bean.setEndDate(DateUtils.toDate("5/1/2011"));
		bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

		CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
		bean2.setInstrument(i);
		bean2.setOurLimit(1005);
		bean2.setEndDate(DateUtils.toDate("5/2/2011"));
		bean2.setLimitType(CompliancePositionExchangeLimitTypes.SPOT_MONTH);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
	}


	@Test
	public void testOverlapDatesButDifferentAccountability() {
		InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(4);
		CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
		bean.setInstrument(i);
		bean.setOurLimit(1000);
		bean.setStartDate(DateUtils.toDate("5/1/2011"));
		bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);
		bean.setAccountabilityLimit(true);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

		CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
		bean2.setInstrument(i);
		bean2.setOurLimit(1005);
		bean2.setEndDate(DateUtils.toDate("5/2/2011"));
		bean2.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
	}


	@Test
	public void testOverlapEndAndStartDates() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(2);
			CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
			bean.setInstrument(i);
			bean.setOurLimit(1000);
			bean.setEndDate(DateUtils.toDate("5/2/2011"));
			bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

			CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
			bean2.setInstrument(i);
			bean2.setOurLimit(1005);
			bean2.setStartDate(DateUtils.toDate("4/1/2011"));
			bean2.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
		}, "Instrument position limit [Test Instrument 2] for type [All Months] and accountability limit [false] should be unique per date ranges.  The following instrument position limit is already defined for the given date range [N/A - 05/02/2011]");
	}


	@Test
	public void testOverlapWithNoEndDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(3);
			CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
			bean.setInstrument(i);
			bean.setOurLimit(1000);
			bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

			CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
			bean2.setInstrument(i);
			bean2.setOurLimit(1005);
			bean2.setStartDate(DateUtils.toDate("4/1/2011"));
			bean2.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
		}, "Instrument position limit [Test Instrument 3] for type [All Months] and accountability limit [false] should be unique per date ranges.  The following instrument position limit is already defined for the given date range [N/A - N/A]");
	}


	@Test
	public void testOverlapNoStartDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentInstrument i = this.investmentInstrumentService.getInvestmentInstrument(1);
			CompliancePositionExchangeLimit bean = new CompliancePositionExchangeLimit();
			bean.setInstrument(i);
			bean.setOurLimit(1000);
			bean.setEndDate(DateUtils.toDate("5/1/2011"));
			bean.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean);

			CompliancePositionExchangeLimit bean2 = new CompliancePositionExchangeLimit();
			bean2.setInstrument(i);
			bean2.setOurLimit(1005);
			bean2.setEndDate(DateUtils.toDate("10/1/2011"));
			bean2.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);

			this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(bean2);
		}, "Instrument position limit [Test Instrument 1] for type [All Months] and accountability limit [false] should be unique per date ranges.  The following instrument position limit is already defined for the given date range [N/A - 05/01/2011]");
	}
}
