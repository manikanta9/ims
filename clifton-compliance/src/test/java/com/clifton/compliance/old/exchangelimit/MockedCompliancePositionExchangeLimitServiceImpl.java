package com.clifton.compliance.old.exchangelimit;


import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitSearchForm;

import java.util.List;


/**
 * The <code>MockedCompliancePositionExchangeLimitServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
public class MockedCompliancePositionExchangeLimitServiceImpl extends CompliancePositionExchangeLimitServiceImpl {

	@Override
	public List<CompliancePositionExchangeLimit> getCompliancePositionExchangeLimitList(CompliancePositionExchangeLimitSearchForm searchForm) {
		if (searchForm.getInstrumentOrBigInstrumentId() != null) {
			Integer i = searchForm.getInstrumentOrBigInstrumentId();
			searchForm.setInstrumentOrBigInstrumentId(null);
			searchForm.setInstrumentId(i);

			List<CompliancePositionExchangeLimit> list = super.getCompliancePositionExchangeLimitList(searchForm);
			CompliancePositionExchangeLimitSearchForm bigSearchForm = new CompliancePositionExchangeLimitSearchForm();
			bigSearchForm.setActiveOnDate(searchForm.getActiveOnDate());
			bigSearchForm.setBigInstrumentId(i);
			List<CompliancePositionExchangeLimit> secondList = super.getCompliancePositionExchangeLimitList(bigSearchForm);
			if (list == null) {
				return secondList;
			}
			if (secondList == null) {
				return list;
			}
			list.addAll(secondList);
			return list;
		}
		return super.getCompliancePositionExchangeLimitList(searchForm);
	}
}
