package com.clifton.compliance.old.rule.evaluator;

import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.accounting.position.TradePositionService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class ComplianceNotionalLimitRestrictionRuleEvaluatorTests {

	@Resource
	private ApplicationContext applicationContext;

	@Resource
	private ComplianceOldService complianceOldService;

	@Resource
	private RuleDefinitionService ruleDefinitionService;

	@Resource
	private TradePositionService tradePositionService;

	private ComplianceLimitRestrictionRuleEvaluator complianceLimitRestrictionRuleEvaluator;


	@BeforeEach
	public void setupTest() {
		this.complianceLimitRestrictionRuleEvaluator = new ComplianceLimitRestrictionRuleEvaluator();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(this.complianceLimitRestrictionRuleEvaluator, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		this.complianceLimitRestrictionRuleEvaluator.setTypeName("Notional Limit");
	}


	@Test
	public void testGeneratorTradeBuySellViolations() {
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Notional Limit");
		ComplianceRuleOld restriction = new ComplianceRuleOld();
		restriction.setRuleType(type);
		restriction.setMaxSellQuantity(BigDecimal.valueOf(200));
		restriction.setMaxSellAccountingNotional(BigDecimal.valueOf(-25000));
		restriction.setMaxBuyQuantity(BigDecimal.valueOf(500));
		restriction.setMaxBuyAccountingNotional(BigDecimal.valueOf(5000));
		Mockito.when(this.ruleDefinitionService.getRuleDefinition(ArgumentMatchers.anyShort())).thenReturn(getRuleDefinition());
		Mockito.when(this.ruleDefinitionService.getRuleAssignment(ArgumentMatchers.anyInt())).thenReturn(getRuleAssignment());
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOldByName("Notional Limit")).thenReturn(type);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(restriction);

		TradeRuleEvaluatorContext tradeRuleEvaluatorContext = new TradeRuleEvaluatorContext();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(tradeRuleEvaluatorContext, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// Sell Quantity
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FUH0").build())
				.withId(43)
				.withQuantityIntended(500)
				.build();

		// Specific Trade Check - Consider nothing before
		Mockito.when(this.tradePositionService.getSharesBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.ZERO);
		Mockito.when(this.tradePositionService.getAccountingNotionalBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.ZERO);

		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-500));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-5000));

		RuleConfig ruleConfig = getRuleConfig();
		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);
		Assertions.assertEquals("The order to sell <span class='amountPositive'>500.00</span> FUH0 Contracts exceeds the DMA order limit of <span class='amountPositive'>200.00</span>.  This order must be executed in multiple tranches and/or via an algo", violation.getViolationNote());


		// Sell Notional
		trade.setQuantityIntended(BigDecimal.valueOf(150));
		trade.setAccountingNotional(BigDecimal.valueOf(-26000));
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-150));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-26000));

		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The notional value of the trade <span class='amountPositive'>26,000.00</span> exceeds the sell limit of <span class='amountPositive'>25,000.00</span>", violation.getViolationNote());


		// Buy Quantity
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(600));
		trade.setAccountingNotional(BigDecimal.valueOf(5000));
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(600));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(5000));

		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The order to buy <span class='amountPositive'>600.00</span> FUH0 Contracts exceeds the DMA order limit of <span class='amountPositive'>500.00</span>.  This order must be executed in multiple tranches and/or via an algo", violation.getViolationNote());


		// Buy Notional
		trade.setQuantityIntended(BigDecimal.valueOf(150));
		trade.setAccountingNotional(BigDecimal.valueOf(15000));
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(150));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(15000));

		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The notional value of the trade <span class='amountPositive'>15,000.00</span> exceeds the buy limit of <span class='amountPositive'>5,000.00</span>", violation.getViolationNote());
	}


	@Test
	public void testGeneratorTradePositionViolations() {
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Notional Limit");
		ComplianceRuleOld restriction = new ComplianceRuleOld();
		restriction.setRuleType(type);
		restriction.setMaxLongPositionQuantity(BigDecimal.valueOf(500));
		restriction.setMaxLongPositionAccountingNotional(BigDecimal.valueOf(50000));
		restriction.setMaxShortPositionQuantity(BigDecimal.valueOf(-200));
		restriction.setMaxShortPositionAccountingNotional(BigDecimal.valueOf(-20000));
		Mockito.when(this.ruleDefinitionService.getRuleDefinition(ArgumentMatchers.anyShort())).thenReturn(getRuleDefinition());
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOldByName("Notional Limit")).thenReturn(type);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(restriction);

		TradeRuleEvaluatorContext tradeRuleEvaluatorContext = new TradeRuleEvaluatorContext();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(tradeRuleEvaluatorContext, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// After Sell Quantity
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FUH0").build())
				.withId(43)
				.withQuantityIntended(100)
				.build();

		// Consider Held Short -150 contracts, -15000 notional
		Mockito.when(this.tradePositionService.getSharesBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.valueOf(-150));
		Mockito.when(this.tradePositionService.getAccountingNotionalBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.valueOf(-15000));

		// Trade Results in -250 contracts, and -20000 notional
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-250));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-2000));

		RuleConfig ruleConfig = getRuleConfig();
		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);
		Assertions.assertEquals("The Contracts after the trade <span class='amountNegative'>-250.00</span> will exceed the short limit of <span class='amountNegative'>-200.00</span>.", violation.getViolationNote());

		// Sell Notional - Consider the trade to go over max short notional
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-200));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(-25000));
		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The notional value after the trade <span class='amountNegative'>-25,000.00</span> will exceed the short limit of <span class='amountNegative'>-20,000.00</span>", violation.getViolationNote());

		// Long: Consider Held 150 contracts, 15000 notional
		Mockito.when(this.tradePositionService.getSharesBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.valueOf(150));
		Mockito.when(this.tradePositionService.getAccountingNotionalBeforeTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean())).thenReturn(BigDecimal.valueOf(15000));

		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(400));
		trade.setAccountingNotional(BigDecimal.valueOf(5000));

		// Trade Results in 550 contracts, and 20000 notional
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(550));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(20000));

		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The Contracts after the trade <span class='amountPositive'>550.00</span> will exceed the long limit of <span class='amountPositive'>500.00</span>.", violation.getViolationNote());

		// Buy Notional - Consider the trade to go over max long notional
		Mockito.when(this.tradePositionService.getSharesAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(500));
		Mockito.when(this.tradePositionService.getAccountingNotionalAfterTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(55000));
		violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(trade, ruleConfig, tradeRuleEvaluatorContext);
		Assertions.assertEquals(1, violationList.size());
		violation = violationList.get(0);
		Assertions.assertEquals("The notional value after the trade <span class='amountPositive'>55,000.00</span> will exceed the long limit of <span class='amountPositive'>50,000.00</span>", violation.getViolationNote());
	}


	private ComplianceRuleTypeOld getComplianceRuleTypeObject(String typeName) {
		ComplianceRuleTypeOld type = new ComplianceRuleTypeOld();
		type.setId(new Short("5"));
		type.setName(typeName);
		return type;
	}


	private RuleConfig getRuleConfig() {
		RuleAssignment assignment = getRuleAssignment();
		RuleConfig ruleConfig = new RuleConfig(assignment);
		EntityConfig entityConfig = new EntityConfig(assignment);
		ruleConfig.addEntityConfig(entityConfig);
		return ruleConfig;
	}


	private RuleAssignment getRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setId(10);
		ruleAssignment.setRuleDefinition(getRuleDefinition());
		return ruleAssignment;
	}


	private RuleDefinition getRuleDefinition() {
		RuleDefinition definition = new RuleDefinition();
		definition.setId(new Short("10"));
		RuleCategory category = new RuleCategory();
		SystemTable table = new SystemTable();
		table.setName("Trade");
		category.setCategoryTable(table);
		definition.setRuleCategory(category);

		SystemTable causeTable = new SystemTable();
		causeTable.setName("ComplianceRuleOld");
		definition.setCauseTable(causeTable);
		definition.setViolationNoteTemplate("<#if restrictionFieldName == \"maxBuyQuantity\" || restrictionFieldName == \"maxSellQuantity\">The order to <#if bean.buy>buy<#else>sell</#if> ${formatNumberMoney(beanAmount)} ${bean.investmentSecurity.symbol} ${bean.investmentSecurity.instrument.hierarchy.investmentType.quantityName} exceeds the DMA order limit of ${formatNumberMoney(restrictionAmount)}.  This order must be executed in multiple tranches and/or via an algo<#elseif restrictionFieldName == \"maxLongPositionQuantity\" || restrictionFieldName == \"maxShortPositionQuantity\">The ${bean.investmentSecurity.instrument.hierarchy.investmentType.quantityName} after the trade ${formatNumberMoney(beanAmount)} will exceed the <#if bean.buy>long<#else>short</#if> limit of ${formatNumberMoney(restrictionAmount)}.<#elseif restrictionFieldName == \"maxBuyAccountingNotional\" || restrictionFieldName == \"maxSellAccountingNotional\">The notional value of the trade ${formatNumberMoney(beanAmount)} exceeds the <#if bean.buy>buy<#else>sell</#if> limit of ${formatNumberMoney(restrictionAmount)}<#elseif restrictionFieldName == \"maxLongPositionAccountingNotional\" || restrictionFieldName == \"maxShortPositionAccountingNotional\">The notional value after the trade ${formatNumberMoney(beanAmount)} will exceed the <#if bean.buy>long<#else>short</#if> limit of ${formatNumberMoney(restrictionAmount)}<#else>Undefined Violation Note Template for field ${restrictionFieldName}. Violation: $formatNumberMoney(beanAmount)} exceeded limit of ${formatNumberMoney(restrictionAmount)}</#if>");
		return definition;
	}
}
