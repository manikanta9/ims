package com.clifton.compliance.old.rule.comparison;


import com.clifton.compliance.builder.ComplianceRuleBuilder;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>ComplianceRuleEntity</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceRuleEntityIgnorableComparisonTests {

	private static final short SHORT_LONG = 1; // IGNORABLE
	private static final short CLIENT_APPROVED_CONTRACTS = 2; // NOT IGNORABLE
	private static final short FAT_FINGER = 3; // IGNORABLE
	private static final short NOTIONAL_LIMIT = 4; // NOT IGNORABLE

	@Resource
	private ComplianceOldService complianceOldService;

	@Resource
	private ReadOnlyDAO<ComplianceRuleTypeOld> complianceRuleTypeOldDAO;


	@Test
	public void testComplianceRuleIgnorableByRule() {
		ComplianceRuleTypeOld type = this.complianceRuleTypeOldDAO.findByPrimaryKey(SHORT_LONG);
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOld(SHORT_LONG)).thenReturn(type);

		ComplianceRuleEntityIgnorableComparison bean = new ComplianceRuleEntityIgnorableComparison();
		bean.setComplianceOldService(this.complianceOldService);
		bean.setTypeId(SHORT_LONG);

		ComplianceRuleOld rule = new ComplianceRuleOld();
		rule.setRuleType(type);

		Assertions.assertFalse(bean.evaluate(rule, null), "Warning should not be ignorable");
		rule.setIgnorable(true);
		Assertions.assertTrue(bean.evaluate(rule, null), "Warning should be ignorable");
	}


	@Test
	public void testComplianceRuleNotIgnorableByRuleType() {
		ComplianceRuleTypeOld type = this.complianceRuleTypeOldDAO.findByPrimaryKey(CLIENT_APPROVED_CONTRACTS);
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOld(CLIENT_APPROVED_CONTRACTS)).thenReturn(type);

		Trade futureTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FUH0").build())
				.withQuantityIntended(10)
				.build();

		// Use a type that ignorable allowed = false
		ComplianceRuleEntityIgnorableComparison comparison = new ComplianceRuleEntityIgnorableComparison();
		comparison.setComplianceOldService(this.complianceOldService);
		comparison.setTypeId(CLIENT_APPROVED_CONTRACTS);

		Assertions.assertFalse(comparison.evaluate(futureTrade, null), "Warning should not be ignorable");
	}


	@Test
	public void testComplianceRuleIgnorableByAccountingBean() {
		ComplianceRuleTypeOld fatType = this.complianceRuleTypeOldDAO.findByPrimaryKey(FAT_FINGER);
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOld(FAT_FINGER)).thenReturn(fatType);
		ComplianceRuleTypeOld notionalType = this.complianceRuleTypeOldDAO.findByPrimaryKey(NOTIONAL_LIMIT);
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOld(NOTIONAL_LIMIT)).thenReturn(notionalType);

		Trade futureTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FUH0").build())
				.withQuantityIntended(10)
				.build();

		// Use a type that ignorable allowed = false
		ComplianceRuleEntityIgnorableComparison comparison = new ComplianceRuleEntityIgnorableComparison();
		comparison.setComplianceOldService(this.complianceOldService);
		comparison.setTypeId(FAT_FINGER);

		ComplianceRuleOld restriction = ComplianceRuleBuilder.createComplianceRule().toComplianceRule();
		restriction.setRuleType(fatType);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(futureTrade, FAT_FINGER)).thenReturn(restriction);

		Assertions.assertFalse(comparison.evaluate(futureTrade, null), "Warning should not be ignorable");

		restriction.setIgnorable(true);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(futureTrade, FAT_FINGER)).thenReturn(restriction);

		Assertions.assertTrue(comparison.evaluate(futureTrade, null), "Warning should be ignorable");

		// Notional Limit Type is Not Ignorable, so even setting the rule as ignorable (which never really could happen)
		// the type lookup in this case wins
		comparison.setTypeId(NOTIONAL_LIMIT);

		restriction = ComplianceRuleBuilder.createComplianceRule().toComplianceRule();
		restriction.setRuleType(notionalType);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(futureTrade, NOTIONAL_LIMIT)).thenReturn(restriction);

		Assertions.assertFalse(comparison.evaluate(futureTrade, null), "Warning should not be ignorable");

		restriction.setIgnorable(true);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(futureTrade, NOTIONAL_LIMIT)).thenReturn(restriction);

		Assertions.assertFalse(comparison.evaluate(futureTrade, null), "Warning should not be ignorable");
	}
}
