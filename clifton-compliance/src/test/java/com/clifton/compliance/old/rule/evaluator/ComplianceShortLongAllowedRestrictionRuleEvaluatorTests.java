package com.clifton.compliance.old.rule.evaluator;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.compliance.builder.ComplianceRuleBuilder;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class ComplianceShortLongAllowedRestrictionRuleEvaluatorTests {

	private static final InvestmentSecurity FUH0 = InvestmentSecurityBuilder.newFuture("FUH0").withId(1).build();

	@Resource
	private ApplicationContext applicationContext;

	@Resource
	private AccountingPositionService accountingPositionService;

	@Resource
	private RuleDefinitionService ruleDefinitionService;


	@Resource
	private ComplianceOldService complianceOldService;

	@Resource
	private TradeService tradeService;


	private ComplianceLimitRestrictionRuleEvaluator complianceLimitRestrictionRuleEvaluator;


	@BeforeEach
	public void setupTest() {
		this.complianceLimitRestrictionRuleEvaluator = new ComplianceLimitRestrictionRuleEvaluator();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(this.complianceLimitRestrictionRuleEvaluator, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		this.complianceLimitRestrictionRuleEvaluator.setTypeName("Short/Long");

		AccountingTransaction transaction = new AccountingTransaction();
		AccountingAccount gl = new AccountingAccount();
		gl.setCollateral(false);
		transaction.setAccountingAccount(gl);
		transaction.setJournal(new AccountingJournal());

		AccountingPosition position = AccountingPosition.forOpeningTransaction(transaction);
		position.setRemainingQuantity(new BigDecimal("10"));

		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(CollectionUtils.createList(position));

		Trade previousTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.buy()
				.withId(42)
				.withQuantityIntended(1)
				.build();

		Mockito.when(this.tradeService.getTradePendingList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(CollectionUtils.createList(previousTrade));

		ComplianceRuleOld restriction = ComplianceRuleBuilder.createComplianceRule(42).setShortAllowed(false).setLongAllowed(true).toComplianceRule();
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Short/Long");
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOldByName("Short/Long")).thenReturn(type);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(restriction);
		Mockito.when(this.ruleDefinitionService.getRuleAssignment(ArgumentMatchers.anyInt())).thenReturn(getRuleAssignment());
		Mockito.when(this.ruleDefinitionService.getRuleDefinition(ArgumentMatchers.anyShort())).thenReturn(getRuleDefinition());
	}


	@Test
	public void testGenerateWarningsWithTrades() {
		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);

		Assertions.assertEquals(10, (int) violation.getRuleAssignment().getRuleDefinition().getId());
		Assertions.assertEquals(43, violation.getLinkedFkFieldId());
		Assertions.assertEquals("This trade will generate a short position of <span class='amountNegative'>-1</span> Contracts which is not allowed.", violation.getViolationNote());
	}


	@Test
	public void testGenerateWarningsNoTrades() {
		Mockito.when(this.tradeService.getTradePendingList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).thenReturn(Arrays.asList(new Trade[]{}));

		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);

		Assertions.assertEquals(10, (int) violation.getRuleAssignment().getRuleDefinition().getId());
		Assertions.assertEquals(43, violation.getLinkedFkFieldId());
		Assertions.assertEquals("This trade will generate a short position of <span class='amountNegative'>-2</span> Contracts which is not allowed.", violation.getViolationNote());
	}


	@Test
	public void testGenerateWarningsTradeNoPosition() {
		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(Arrays.asList(new AccountingPosition[]{}));

		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);

		Assertions.assertEquals(10, (int) violation.getRuleAssignment().getRuleDefinition().getId());
		Assertions.assertEquals(43, violation.getLinkedFkFieldId());
		Assertions.assertEquals("This trade will generate a short position of <span class='amountNegative'>-11</span> Contracts which is not allowed.", violation.getViolationNote());
	}


	@Test
	public void testGenerateWarningsTradeNotShort() {
		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(10)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(0, violationList.size());
	}


	@Test
	public void testGenerateWarningsShortAllowed() {
		ComplianceRuleOld restriction = ComplianceRuleBuilder.createComplianceRule(42).setShortAllowed(true).setLongAllowed(true).toComplianceRule();
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Short/Long");
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOldByName("Short/Long")).thenReturn(type);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(restriction);

		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(0, violationList.size());
	}


	@Test
	public void testGenerateWarningsShortAllowedNoRestriction() {
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Short/Long");
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(null);

		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(0, violationList.size());
	}


	@Test
	public void testGenerateWarningsLongNotAllowed() {
		ComplianceRuleOld restriction = ComplianceRuleBuilder.createComplianceRule(42).setShortAllowed(true).setLongAllowed(false).toComplianceRule();
		ComplianceRuleTypeOld type = getComplianceRuleTypeObject("Short/Long");
		Mockito.when(this.complianceOldService.getComplianceRuleTypeOldByName("Short/Long")).thenReturn(type);
		Mockito.when(this.complianceOldService.getComplianceRuleOldForBeanAndType(ArgumentMatchers.any(Trade.class), ArgumentMatchers.eq(type.getId()))).thenReturn(restriction);

		AccountingPosition position = AccountingPosition.forOpeningTransaction(new AccountingTransaction());
		position.setRemainingQuantity(new BigDecimal("-10"));
		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(CollectionUtils.createList(position));

		Trade futureTrade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(43)
				.buy()
				.withQuantityIntended(12)
				.build();

		List<RuleViolation> violationList = this.complianceLimitRestrictionRuleEvaluator.evaluateRule(futureTrade, getRuleConfig(), getTradeRuleEvaluatorContext());
		Assertions.assertEquals(1, violationList.size());
		RuleViolation violation = violationList.get(0);

		Assertions.assertEquals(10, (int) violation.getRuleAssignment().getRuleDefinition().getId());
		Assertions.assertEquals(43, violation.getLinkedFkFieldId());
		Assertions.assertEquals("This trade will generate a long position of <span class='amountPositive'>3</span> Contracts which is not allowed.", violation.getViolationNote());
	}


	private ComplianceRuleTypeOld getComplianceRuleTypeObject(String typeName) {
		ComplianceRuleTypeOld type = new ComplianceRuleTypeOld();
		type.setId(new Short("5"));
		type.setName(typeName);
		return type;
	}


	private RuleConfig getRuleConfig() {
		RuleAssignment assignment = getRuleAssignment();
		RuleConfig ruleConfig = new RuleConfig(assignment);
		EntityConfig entityConfig = new EntityConfig(assignment);
		ruleConfig.addEntityConfig(entityConfig);
		return ruleConfig;
	}


	private RuleAssignment getRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setId(10);
		ruleAssignment.setRuleDefinition(getRuleDefinition());
		return ruleAssignment;
	}


	private RuleDefinition getRuleDefinition() {
		RuleDefinition definition = new RuleDefinition();
		definition.setId(new Short("10"));
		RuleCategory category = new RuleCategory();
		SystemTable table = new SystemTable();
		table.setName("Trade");
		category.setCategoryTable(table);
		definition.setRuleCategory(category);

		SystemTable causeTable = new SystemTable();
		causeTable.setName("ComplianceRuleOld");
		definition.setCauseTable(causeTable);
		definition.setViolationNoteTemplate("This trade will generate a <#if bean.buy>long<#else>short</#if> position of ${formatNumberInteger(beanAmount)} ${bean.investmentSecurity.instrument.hierarchy.investmentType.quantityName} which is not allowed.");
		return definition;
	}


	private TradeRuleEvaluatorContext getTradeRuleEvaluatorContext() {
		TradeRuleEvaluatorContext tradeRuleEvaluatorContext = new TradeRuleEvaluatorContext();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(tradeRuleEvaluatorContext, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return tradeRuleEvaluatorContext;
	}
}
