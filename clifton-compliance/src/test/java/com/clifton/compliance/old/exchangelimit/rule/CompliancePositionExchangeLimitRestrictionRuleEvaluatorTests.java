package com.clifton.compliance.old.exchangelimit.rule;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.calculators.InvestmentSecuritySpotMonthCalculator;
import com.clifton.investment.instrument.spot.calculators.InvestmentSecuritySpotMonthDateFieldCalculator;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class CompliancePositionExchangeLimitRestrictionRuleEvaluatorTests {

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private AccountingPositionService accountingPositionService;

	@Resource
	private RuleDefinitionService ruleDefinitionService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private TradeService tradeService;

	@Resource
	private ReadOnlyDAO<InvestmentSecurity> investmentSecurityDAO;


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupCalendar() {
		// Setup Once
		short yr = 2012;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}

		yr = 2013;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
	}


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	@Test
	public void testNoLimitDefined() {
		RuleConfig ruleConfig = getRuleConfig(80, null);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(5));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(1));
		trade.setTradeDate(DateUtils.toDate("01/15/2012"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no violations for instrument with no limits defined");

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitNotReached() {

		RuleConfig ruleConfig = getRuleConfig(80, null);

		// Buy Some Trades
		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(5));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(2));
		trade.setTradeDate(DateUtils.toDate("01/15/2012"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no warnings for instrument with limit not being reached");

		// Sell Some Trades
		trade.setBuy(false);
		violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no warnings for instrument with limit not being reached - And Selling positions, so limit would be reduced anyway");

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitReached() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		RuleConfig ruleConfig = getRuleConfig(80, 100);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(1050));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(3));
		trade.setTradeDate(DateUtils.toDate("02/28/2012"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertEquals(1, CollectionUtils.getSize(violationList));
		RuleViolation violation = violationList.get(0);
		Assertions.assertEquals("This trade will increase our positions against the I3: Test Instrument 3: Single Month ( - ) limit from <span class='amountPositive'>70%</span> to <span class='amountPositive'>80.5%</span> which is over the <span class='amountPositive'>80%</span> threshold",
				violation.getViolationNote());

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitReachedBySecondTradeUsingContext() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		// Use the same context - Individually they won't trigger warning without the other Trade
		// By using the same context the second trade should reuse the lookup from the first trade, and also trigger the warning
		RuleConfig ruleConfig = getRuleConfig(80, 100);

		Trade trade1 = new Trade();
		trade1.setId(1);
		trade1.setBuy(true);
		trade1.setQuantityIntended(BigDecimal.valueOf(500));
		trade1.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(3));
		trade1.setTradeDate(DateUtils.toDate("02/28/2012"));

		TradeRuleEvaluatorContext context = new TradeRuleEvaluatorContext();
		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade1, ruleConfig, context);
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no violations for first trade.");

		Trade trade2 = new Trade();
		trade2.setId(2);
		trade2.setBuy(true);
		trade2.setQuantityIntended(BigDecimal.valueOf(550));
		trade2.setInvestmentSecurity(trade1.getInvestmentSecurity());
		trade2.setTradeDate(DateUtils.toDate("02/28/2012"));

		violationList = getRuleEvaluator().evaluateRule(trade2, ruleConfig, context);

		Assertions.assertEquals(1, CollectionUtils.getSize(violationList));
		RuleViolation violation = violationList.get(0);
		Assertions.assertEquals("This trade will increase our positions against the I3: Test Instrument 3: Single Month ( - ) limit from <span class='amountPositive'>75%</span> to <span class='amountPositive'>80.5%</span> which is over the <span class='amountPositive'>80%</span> threshold",
				violation.getViolationNote());

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade1.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitReachedButReduced() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		RuleConfig ruleConfig = getRuleConfig(80, 100);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(false);
		trade.setQuantityIntended(BigDecimal.valueOf(500));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(41));
		trade.setTradeDate(DateUtils.toDate("02/28/2012"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no warnings for instrument with limit being reached but reduced by trade");

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitReachedByBigTrade() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		RuleConfig ruleConfig = getRuleConfig(70, 100);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(230));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(110));
		trade.setTradeDate(DateUtils.toDate("01/15/2012"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertEquals(2, CollectionUtils.getSize(violationList));
		RuleViolation violation = violationList.get(0);
		RuleViolation violation2 = violationList.get(1);

		if (violation.getCauseFkFieldId() == 1000) {
			Assertions.assertEquals(
					"This trade will increase our positions against the I100: Test Instrument 100: All Months ( - ) limit from <span class='amountPositive'>60.28%</span> to <span class='amountPositive'>73.06%</span> which is over the <span class='amountPositive'>70%</span> threshold",
					violation.getViolationNote());
			Assertions.assertEquals(
					"This trade will increase our positions against the I100: Test Instrument 100: Spot Month ( - ) limit from <span class='amountPositive'>77.5%</span> to <span class='amountPositive'>93.93%</span> which is over the <span class='amountPositive'>70%</span> threshold",
					violation2.getViolationNote());
		}
		else {
			Assertions.assertEquals(
					"This trade will increase our positions against the I100: Test Instrument 100: Spot Month ( - ) limit from <span class='amountPositive'>77.5%</span> to <span class='amountPositive'>93.93%</span> which is over the <span class='amountPositive'>70%</span> threshold",
					violation.getViolationNote());
			Assertions.assertEquals(
					"This trade will increase our positions against the I100: Test Instrument 100: All Months ( - ) limit from <span class='amountPositive'>60.28%</span> to <span class='amountPositive'>73.06%</span> which is over the <span class='amountPositive'>70%</span> threshold",
					violation2.getViolationNote());
		}

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testNoLimitReachedByBigTrade_ScaleDownType_NotApplicableYet() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		// Starts being Applied 10 Days Before Delivery Date
		RuleConfig ruleConfig = getRuleConfig(90, null);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(150));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(50));
		// More than 10 days before 9/30 - so shouldn't apply
		trade.setTradeDate(DateUtils.toDate("07/31/2013"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertTrue(CollectionUtils.isEmpty(violationList), "Expected no warnings for instrument with scale down spot month limit being reached because not within x days of last delivery.");

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	@Test
	public void testLimitReachedByBigTrade_ScaleDownType_Applies() {
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any(SystemBean.class))).thenReturn(getInvestmentSecuritySpotMonthCalculator());
		// Starts being Applied 10 Days Before Delivery Date
		RuleConfig ruleConfig = getRuleConfig(90, null);

		Trade trade = new Trade();
		trade.setId(1);
		trade.setBuy(true);
		trade.setQuantityIntended(BigDecimal.valueOf(150));
		trade.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(50));
		// Within 10 days of 9/30 - so SHOULD apply
		trade.setTradeDate(DateUtils.toDate("09/21/2013"));

		List<RuleViolation> violationList = getRuleEvaluator().evaluateRule(trade, ruleConfig, new TradeRuleEvaluatorContext());
		Assertions.assertEquals(1, CollectionUtils.getSize(violationList));
		RuleViolation violation = violationList.get(0);
		Assertions.assertEquals("This trade will increase our positions against the I5: Test Instrument 5: Scale Down Spot Month ( - ) limit from <span class='amountPositive'>75%</span> to <span class='amountPositive'>112.5%</span> which is over the <span class='amountPositive'>90%</span> threshold",
				violation.getViolationNote());

		// Clean Up
		this.contextHandler.removeBean("POSITION_LIMITS_" + trade.getInvestmentSecurity().getInstrument().getId());
	}


	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	private CompliancePositionExchangeLimitRestrictionRuleEvaluator getRuleEvaluator() {
		CompliancePositionExchangeLimitRestrictionRuleEvaluator bean = new CompliancePositionExchangeLimitRestrictionRuleEvaluator();
		this.applicationContextService.autowireBean(bean);
		return bean;
	}


	private RuleConfig getRuleConfig(Integer min, Integer max) {
		RuleAssignment ruleAssignment = getRuleAssignment();
		ruleAssignment.setMinAmount(min == null ? null : BigDecimal.valueOf(min));
		ruleAssignment.setMaxAmount(max == null ? null : BigDecimal.valueOf(max));
		RuleConfig ruleConfig = new RuleConfig(ruleAssignment);
		EntityConfig entityConfig = new EntityConfig(ruleAssignment);
		ruleConfig.addEntityConfig(entityConfig);
		return ruleConfig;
	}


	private RuleAssignment getRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setId(10);
		ruleAssignment.setRuleDefinition(getRuleDefinition());
		return ruleAssignment;
	}


	private RuleDefinition getRuleDefinition() {
		RuleDefinition definition = new RuleDefinition();
		definition.setId(new Short("10"));
		RuleCategory category = new RuleCategory();
		SystemTable table = new SystemTable();
		table.setName("Trade");
		category.setCategoryTable(table);
		definition.setRuleCategory(category);

		SystemTable causeTable = new SystemTable();
		causeTable.setName("CompliancePositionExchangeLimit");
		definition.setCauseTable(causeTable);
		definition.setViolationNoteTemplate("This trade will increase our positions against the ${causeEntity.label} limit from ${formatNumberPercent(originalLimit)} to ${formatNumberPercent(adjustedLimit)} which is over the ${formatNumberPercent(entityConfig.minAmount)} threshold");
		return definition;
	}


	private InvestmentSecuritySpotMonthCalculator getInvestmentSecuritySpotMonthCalculator() {
		InvestmentSecuritySpotMonthDateFieldCalculator calculator = new InvestmentSecuritySpotMonthDateFieldCalculator();
		calculator.setStartDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setStartFromFirstDayOfMonth(true);
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");
		this.applicationContextService.autowireBean(calculator);
		return calculator;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupMock() {
		Mockito.when(this.ruleDefinitionService.getRuleAssignment(ArgumentMatchers.anyInt())).thenReturn(getRuleAssignment());
		Mockito.when(this.ruleDefinitionService.getRuleDefinition(ArgumentMatchers.anyShort())).thenReturn(getRuleDefinition());

		final ReadOnlyDAO<InvestmentSecurity> securityDAO = this.investmentSecurityDAO;
		Mockito.when(this.accountingPositionService.getAccountingPositionListForInstrument(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenAnswer(
				(Answer<List<AccountingPosition>>) invocation -> {
					Object[] args = invocation.getArguments();

					int instrumentId = (Integer) args[0];
					// Break Down for Mocked Tests:

					List<AccountingPosition> pList = new ArrayList<>();

					// Instrument 1 = 100 Long Positions Currently Held
					if (instrumentId == 1) {
						pList.add(newAccountingPosition(1, BigDecimal.valueOf(100)));
					}

					// Instrument 2 = Current Positions: 50, 150 Long, 100 short
					if (instrumentId == 2) {
						pList.add(newAccountingPosition(2, BigDecimal.valueOf(150)));
						pList.add(newAccountingPosition(2, BigDecimal.valueOf(-100)));
					}

					// Instrument 3 = Limit of 10,000, Current Positions: 7,000
					if (instrumentId == 3) {
						pList.add(newAccountingPosition(3, BigDecimal.valueOf(8500)));

						pList.add(newAccountingPosition(3, BigDecimal.valueOf(-1500)));
					}
					// Instrument 4 = Limit of 10,000, Current Positions: 9,000
					if (instrumentId == 4) {
						pList.add(newAccountingPosition(41, BigDecimal.valueOf(6500)));
						pList.add(newAccountingPosition(41, BigDecimal.valueOf(-1500)));

						pList.add(newAccountingPosition(42, BigDecimal.valueOf(4500)));
						pList.add(newAccountingPosition(42, BigDecimal.valueOf(-3000)));

						pList.add(newAccountingPosition(43, BigDecimal.valueOf(-500)));

						pList.add(newAccountingPosition(44, BigDecimal.valueOf(2500)));
					}
					// Instrument 100
					if (instrumentId == 100) {
						pList.add(newAccountingPosition(100, BigDecimal.valueOf(5500)));
						pList.add(newAccountingPosition(100, BigDecimal.valueOf(-500)));

						pList.add(newAccountingPosition(110, BigDecimal.valueOf(85)));
					}

					// Instrument 5 = Limit of 400, Current Positions: 300
					if (instrumentId == 5) {
						pList.add(newAccountingPosition(50, BigDecimal.valueOf(300)));
					}
					return pList;
				});
		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenAnswer(
				(Answer<List<AccountingPosition>>) invocation -> {
					Object[] args = invocation.getArguments();

					AccountingPositionCommand command = (AccountingPositionCommand) args[0];
					// Break Down for Mocked Tests:

					List<AccountingPosition> pList = new ArrayList<>();

					if (command.getInvestmentInstrumentIds() != null) {
						Set<Integer> instrumentIdSet = ArrayUtils.getStream(command.getInvestmentInstrumentIds()).collect(Collectors.toSet());

						// Instrument 1 = 100 Long Positions Currently Held
						if (instrumentIdSet.contains(1)) {
							pList.add(newAccountingPosition(1, BigDecimal.valueOf(100)));
						}

						// Instrument 2 = Current Positions: 50, 150 Long, 100 short
						if (instrumentIdSet.contains(2)) {
							pList.add(newAccountingPosition(2, BigDecimal.valueOf(150)));
							pList.add(newAccountingPosition(2, BigDecimal.valueOf(-100)));
						}

						// Instrument 3 = Limit of 10,000, Current Positions: 7,000
						if (instrumentIdSet.contains(3)) {
							pList.add(newAccountingPosition(3, BigDecimal.valueOf(8500)));

							pList.add(newAccountingPosition(3, BigDecimal.valueOf(-1500)));
						}
						// Instrument 4 = Limit of 10,000, Current Positions: 9,000
						if (instrumentIdSet.contains(4)) {
							pList.add(newAccountingPosition(41, BigDecimal.valueOf(6500)));
							pList.add(newAccountingPosition(41, BigDecimal.valueOf(-1500)));

							pList.add(newAccountingPosition(42, BigDecimal.valueOf(4500)));
							pList.add(newAccountingPosition(42, BigDecimal.valueOf(-3000)));

							pList.add(newAccountingPosition(43, BigDecimal.valueOf(-500)));

							pList.add(newAccountingPosition(44, BigDecimal.valueOf(2500)));
						}
						// Instrument 100
						if (instrumentIdSet.contains(100)) {
							pList.add(newAccountingPosition(100, BigDecimal.valueOf(5500)));
							pList.add(newAccountingPosition(100, BigDecimal.valueOf(-500)));

							pList.add(newAccountingPosition(110, BigDecimal.valueOf(85)));
						}

						// Instrument 5 = Limit of 400, Current Positions: 300
						if (instrumentIdSet.contains(5)) {
							pList.add(newAccountingPosition(50, BigDecimal.valueOf(300)));
						}
					}
					return pList;
				});

		Mockito.when(this.tradeService.getTradeList(ArgumentMatchers.any(TradeSearchForm.class))).thenAnswer((Answer<List<Trade>>) invocation -> {
			Object[] args = invocation.getArguments();

			TradeSearchForm searchForm = (TradeSearchForm) args[0];

			if (searchForm == null) {
				return null;
			}

			List<Trade> tList = new ArrayList<>();

			// Instrument 4 = Limit of 10,000, Current Positions: 9,000
			if (searchForm.getIncludeBigTradesInvestmentInstrumentId() == 4) {
				// Security 41
				Trade t = new Trade();
				t.setBuy(true);
				t.setInvestmentSecurity(securityDAO.findByPrimaryKey(41));
				t.setQuantityIntended(BigDecimal.valueOf(500));
				tList.add(t);

				// Security 42
				Trade t2 = new Trade();
				t2.setInvestmentSecurity(securityDAO.findByPrimaryKey(42));
				t2.setBuy(true);
				t2.setQuantityIntended(BigDecimal.valueOf(1500));
				tList.add(t2);

				Trade t3 = new Trade();
				t3.setInvestmentSecurity(t2.getInvestmentSecurity());
				t3.setBuy(false);
				t3.setQuantityIntended(BigDecimal.valueOf(1000));
				tList.add(t3);
			}
			return tList;
		});
	}


	@AfterEach
	public void cleanUpMocks() {
		Mockito.reset(this.ruleDefinitionService);
		Mockito.reset(this.accountingPositionService);
		Mockito.reset(this.tradeService);
	}


	private AccountingPosition newAccountingPosition(int investmentSecurityId, BigDecimal remainingQuantity) {
		AccountingTransaction openingTransaction = new AccountingTransaction();
		openingTransaction.setInvestmentSecurity(this.investmentSecurityDAO.findByPrimaryKey(investmentSecurityId));

		AccountingPosition result = AccountingPosition.forOpeningTransaction(openingTransaction);
		result.setRemainingQuantity(remainingQuantity);
		return result;
	}
}
