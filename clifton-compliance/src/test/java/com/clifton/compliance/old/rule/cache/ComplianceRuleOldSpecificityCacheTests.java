package com.clifton.compliance.old.rule.cache;

import com.clifton.business.company.BusinessCompanyService;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceRuleOldSpecificityCacheTests {

	private static final int RULE_HOLDING_ACCOUNT_AND_INSTRUMENT = 1;
	private static final int RULE_HOLDING_ACCOUNT_AND_HIERARCHY = 2;
	private static final int RULE_HOLDING_ACCOUNT_AND_INVESTMENT_TYPE = 3;
	private static final int RULE_CLIENT_ACCOUNT_AND_INSTRUMENT = 4;
	private static final int RULE_CLIENT_ACCOUNT_AND_HIERARCHY = 5;
	private static final int RULE_CLIENT_ACCOUNT_AND_INVESTMENT_TYPE = 6;
	private static final int RULE_TRADER_AND_INSTRUMENT = 7;
	private static final int RULE_TRADER_AND_HIERARCHY = 8;
	private static final int RULE_TRADER_AND_INVESTMENT_TYPE = 9;
	private static final int RULE_ONLY_HOLDING_ACCOUNT = 10;
	private static final int RULE_ONLY_CLIENT_ACCOUNT = 11;
	private static final int RULE_ONLY_TRADER = 12;
	private static final int RULE_ONLY_INSTRUMENT = 13;
	private static final int RULE_ONLY_HIERARCHY = 14;
	private static final int RULE_ONLY_TYPE = 15;

	@Resource
	private ComplianceOldService complianceOldService;

	@Resource
	private ComplianceRuleOldSpecificityCache cache;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SecurityUserService securityUserService;


	@Test
	public void testRuleWithHoldingAccountAndInstrument() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(1), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 1);
		Assertions.assertEquals(getComplianceRuleOld(RULE_HOLDING_ACCOUNT_AND_INSTRUMENT), result);
	}


	@Test
	public void testRuleWithHoldingAccountAndHierarchy() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(4), getTrader((short) 1), getHoldingAccount(2), getClientAccount(1), (short) 2);
		Assertions.assertEquals(getComplianceRuleOld(RULE_HOLDING_ACCOUNT_AND_HIERARCHY), result);
	}


	@Test
	public void testRuleWithHoldingAccountAndInvestmentType() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(12), getTrader((short) 1), getHoldingAccount(3), getClientAccount(1), (short) 3);
		Assertions.assertEquals(getComplianceRuleOld(RULE_HOLDING_ACCOUNT_AND_INVESTMENT_TYPE), result);
	}


	@Test
	public void testRuleWithClientAccountAndInstrument() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(1), getTrader((short) 1), getHoldingAccount(2), getClientAccount(2), (short) 1);
		Assertions.assertEquals(getComplianceRuleOld(RULE_CLIENT_ACCOUNT_AND_INSTRUMENT), result);
	}


	@Test
	public void testRuleWithClientAccountAndHierarchy() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(7), getTrader((short) 1), getHoldingAccount(1), getClientAccount(3), (short) 2);
		Assertions.assertEquals(getComplianceRuleOld(RULE_CLIENT_ACCOUNT_AND_HIERARCHY), result);
	}


	@Test
	public void testRuleWithClientAccountAndInvestmentType() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(2), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 3);
		Assertions.assertEquals(getComplianceRuleOld(RULE_CLIENT_ACCOUNT_AND_INVESTMENT_TYPE), result);
	}


	@Test
	@Disabled
	public void testRuleWithTraderAndInstrument() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(10), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 1);
		Assertions.assertEquals(getComplianceRuleOld(RULE_TRADER_AND_INSTRUMENT), result);
	}


	@Test
	@Disabled
	public void testRuleWithTraderAndHierarchy() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(2), getTrader((short) 2), getHoldingAccount(1), getClientAccount(1), (short) 2);
		Assertions.assertEquals(getComplianceRuleOld(RULE_TRADER_AND_HIERARCHY), result);
	}


	@Test
	@Disabled
	public void testRuleWithTraderAndInvestmentType() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(4), getTrader((short) 3), getHoldingAccount(1), getClientAccount(1), (short) 3);
		Assertions.assertEquals(getComplianceRuleOld(RULE_TRADER_AND_INVESTMENT_TYPE), result);
	}


	@Test
	public void testRuleWithOnlyHoldingAccount() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(2), getTrader((short) 1), getHoldingAccount(3), getClientAccount(2), (short) 1);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_HOLDING_ACCOUNT), result);
	}


	@Test
	public void testRuleWithOnlyClientAccount() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(11), getTrader((short) 1), getHoldingAccount(4), getClientAccount(4), (short) 2);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_CLIENT_ACCOUNT), result);
	}


	@Test
	@Disabled
	public void testRuleWithOnlyTrader() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(12), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 3);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_TRADER), result);
	}


	@Test
	public void testRuleWithOnlyInstrument() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(11), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 1);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_INSTRUMENT), result);
	}


	@Test
	public void testRuleWithOnlyHierarchy() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(8), getTrader((short) 1), getHoldingAccount(2), getClientAccount(1), (short) 2);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_HIERARCHY), result);
	}


	@Test
	public void testRuleWithOnlyInvestmentType() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(12), getTrader((short) 1), getHoldingAccount(2), getClientAccount(2), (short) 3);
		Assertions.assertEquals(getComplianceRuleOld(RULE_ONLY_TYPE), result);
	}


	@Test
	public void testRuleWithNoResults() {
		ComplianceRuleOld result = getMostSpecificRule(getSecurity(13), getTrader((short) 1), getHoldingAccount(1), getClientAccount(1), (short) 3);
		Assertions.assertEquals(null, result);
	}


	private ComplianceRuleOld getMostSpecificRule(InvestmentSecurity security, SecurityUser trader, InvestmentAccount holdingAccount, InvestmentAccount clientAccount, short ruleTypeId) {
		Trade trade = new Trade();
		trade.setInvestmentSecurity(security);
		trade.setTraderUser(trader);
		trade.setHoldingInvestmentAccount(holdingAccount);
		trade.setClientInvestmentAccount(clientAccount);

		return this.cache.getMostSpecificComplianceRuleOld(trade, ruleTypeId);
	}


	private SecurityUser getTrader(short id) {
		return this.securityUserService.getSecurityUser(id);
	}


	private InvestmentAccount getHoldingAccount(int id) {
		return this.investmentAccountService.getInvestmentAccount(id);
	}


	private InvestmentAccount getClientAccount(int id) {
		return this.investmentAccountService.getInvestmentAccount(id);
	}


	private ComplianceRuleOld getComplianceRuleOld(int id) {
		return this.complianceOldService.getComplianceRuleOld(id);
	}


	private InvestmentSecurity getSecurity(int id) {
		return this.investmentInstrumentService.getInvestmentSecurity(id);
	}
}
