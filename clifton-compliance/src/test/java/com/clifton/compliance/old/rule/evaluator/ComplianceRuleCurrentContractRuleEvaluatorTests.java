package com.clifton.compliance.old.rule.evaluator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class ComplianceRuleCurrentContractRuleEvaluatorTests {

	@Test
	public void testIsOpenMoreTrades() {
		ComplianceRuleCurrentContractRuleEvaluator evaluator = new ComplianceRuleCurrentContractRuleEvaluator();

		BigDecimal shortQuantity = BigDecimal.valueOf(-5);
		BigDecimal shortQuantity2 = BigDecimal.valueOf(-50);
		BigDecimal zeroQuantity = BigDecimal.ZERO;
		BigDecimal longQuantity = BigDecimal.valueOf(5);
		BigDecimal longQuantity2 = BigDecimal.valueOf(7);

		// Always false if after trade quantity is zero
		Assertions.assertFalse(evaluator.isOpenMoreTrades(zeroQuantity, zeroQuantity));
		Assertions.assertFalse(evaluator.isOpenMoreTrades(shortQuantity, zeroQuantity));
		Assertions.assertFalse(evaluator.isOpenMoreTrades(shortQuantity2, zeroQuantity));
		Assertions.assertFalse(evaluator.isOpenMoreTrades(longQuantity, zeroQuantity));
		Assertions.assertFalse(evaluator.isOpenMoreTrades(longQuantity2, zeroQuantity));

		// Always true if before trade quantity is zero (and after trade quantity is not zero)
		Assertions.assertTrue(evaluator.isOpenMoreTrades(zeroQuantity, shortQuantity));
		Assertions.assertTrue(evaluator.isOpenMoreTrades(zeroQuantity, shortQuantity2));
		Assertions.assertTrue(evaluator.isOpenMoreTrades(zeroQuantity, longQuantity));
		Assertions.assertTrue(evaluator.isOpenMoreTrades(zeroQuantity, longQuantity2));

		// Always true if More Short or More Long
		Assertions.assertTrue(evaluator.isOpenMoreTrades(shortQuantity, shortQuantity2));
		Assertions.assertTrue(evaluator.isOpenMoreTrades(longQuantity, longQuantity2));

		// Always false if Less Short or Less Long
		Assertions.assertFalse(evaluator.isOpenMoreTrades(shortQuantity2, shortQuantity));
		Assertions.assertFalse(evaluator.isOpenMoreTrades(longQuantity2, longQuantity));

		// Always True if Short to Long or Long to Short
		Assertions.assertTrue(evaluator.isOpenMoreTrades(shortQuantity2, longQuantity));
		Assertions.assertTrue(evaluator.isOpenMoreTrades(longQuantity2, shortQuantity));
	}
}
