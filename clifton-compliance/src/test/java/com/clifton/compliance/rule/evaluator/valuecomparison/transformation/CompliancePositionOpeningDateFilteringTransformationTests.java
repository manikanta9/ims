package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The test class for {@link CompliancePositionOpeningDateFilteringTransformation}.
 *
 * @author MikeH
 */
@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
public class CompliancePositionOpeningDateFilteringTransformationTests {

	// Test data constants
	private static final Date DATE0 = DateUtils.toDate("12/01/2017");
	private static final Date DATE1 = DateUtils.toDate("01/01/2018");
	private static final Date DATE2 = DateUtils.toDate("02/01/2018");
	private static final Date DATE3 = DateUtils.toDate("03/01/2018");
	private static final Date DATE4 = DateUtils.toDate("04/01/2018");

	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_DATE1 = createPositionValueHolder(DATE1, DATE1, DATE1);
	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_DATE2 = createPositionValueHolder(DATE2, DATE2, DATE2);
	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_DATE3 = createPositionValueHolder(DATE3, DATE3, DATE3);
	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_DATE_MISSING = createPositionValueHolder(null, null, null);
	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_DATE_MIXED = createPositionValueHolder(DATE1, DATE2, DATE3);
	private static final CompliancePositionValueHolder<AccountingPosition> VALUE_HOLDER_EMPTY = createPositionValueHolder();


	// Resources
	private CompliancePositionOpeningDateFilteringTransformation compliancePositionOpeningDateFilteringTransformation = new CompliancePositionOpeningDateFilteringTransformation();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOpeningDateBefore() {
		expectTransformationResultsOnOrBefore(DATE0,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList());
		expectTransformationResultsOnOrBefore(DATE1,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1));
		expectTransformationResultsOnOrBefore(DATE2,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2));
		expectTransformationResultsOnOrBefore(DATE3,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrBefore(DATE4,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
	}


	@Test
	public void testOpeningDateAfter() {
		expectTransformationResultsOnOrAfter(DATE0,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE1,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE2,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE3,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE4,
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList());
	}


	@Test
	public void testOpeningDateNonUniform() {
		TestUtils.expectException(ValidationException.class, () -> {
			expectTransformationResultsOnOrBefore(DATE1,
					Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3, VALUE_HOLDER_DATE_MIXED),
					Arrays.asList());
		}, String.format("Conflicting dates were found within one or more position groups while processing the transformation of type [%s]. Place this transformation first or group positions by date before executing this transformation. Conflicting dates: %s.", CompliancePositionOpeningDateFilteringTransformation.class.getSimpleName(), CollectionUtils.getConverted(Arrays.asList(DATE1, DATE2, DATE3), DateUtils::fromDate)));
	}


	@Test
	public void testOpeningDateMissing() {
		expectTransformationResultsOnOrBefore(DATE0,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE_MISSING));
		expectTransformationResultsOnOrBefore(DATE1,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1));
		expectTransformationResultsOnOrBefore(DATE2,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2));
		expectTransformationResultsOnOrBefore(DATE3,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrBefore(DATE4,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE0,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE1,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE2,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE3,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE4,
				Arrays.asList(VALUE_HOLDER_DATE_MISSING, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList());
	}


	@Test
	public void testEmptyValueHolderList() {
		expectTransformationResultsOnOrBefore(DATE0,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY));
		expectTransformationResultsOnOrBefore(DATE1,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1));
		expectTransformationResultsOnOrBefore(DATE2,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2));
		expectTransformationResultsOnOrBefore(DATE3,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrBefore(DATE4,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE0,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE1,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE2,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE3,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE3));
		expectTransformationResultsOnOrAfter(DATE4,
				Arrays.asList(VALUE_HOLDER_EMPTY, VALUE_HOLDER_DATE1, VALUE_HOLDER_DATE2, VALUE_HOLDER_DATE3),
				Arrays.asList(VALUE_HOLDER_EMPTY));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static CompliancePositionValueHolder<AccountingPosition> createPositionValueHolder(Date... openingDates) {
		List<AccountingPosition> positionList = CollectionUtils.getConverted(Arrays.asList(openingDates), openingDate -> {
			AccountingTransaction transaction = TestUtils.generateEntity(AccountingTransaction.class, Long.class);
			transaction.setTransactionDate(openingDate);
			transaction.setQuantity(BigDecimal.ONE);
			return AccountingPosition.forOpeningTransaction(transaction);
		});
		return new CompliancePositionValueHolder<>(BigDecimal.ONE, positionList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<CompliancePositionValueHolder<AccountingPosition>> executeTransformation(Date configuredOpeningDate, CompliancePositionOpeningDateFilteringTransformation.PositionDirection direction, List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList) {
		this.compliancePositionOpeningDateFilteringTransformation.setOpeningDateBound(configuredOpeningDate);
		this.compliancePositionOpeningDateFilteringTransformation.setIncludedDirection(direction);
		return this.compliancePositionOpeningDateFilteringTransformation.apply(valueHolderList, null, null, null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void expectTransformationResultsOnOrBefore(Date configuredOpeningDate, List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, List<CompliancePositionValueHolder<AccountingPosition>> expectedResultList) {
		Assertions.assertEquals(expectedResultList, executeTransformation(configuredOpeningDate, CompliancePositionOpeningDateFilteringTransformation.PositionDirection.ON_OR_BEFORE, valueHolderList));
	}


	private void expectTransformationResultsOnOrAfter(Date configuredOpeningDate, List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList, List<CompliancePositionValueHolder<AccountingPosition>> expectedResultList) {
		Assertions.assertEquals(expectedResultList, executeTransformation(configuredOpeningDate, CompliancePositionOpeningDateFilteringTransformation.PositionDirection.ON_OR_AFTER, valueHolderList));
	}
}
