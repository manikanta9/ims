package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A set of unit tests to test the CompliancePositionFieldValueRangeFilteringTransformation class.
 *
 * @author DavidI
 */
public class CompliancePositionFieldValueRangeFilteringTransformationTests {


	@Test
	public void testApply_positions_in_numericRange() {
		Date transactionDate = new Date();

		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = CreateCompliancePositionValueHolderList(new BigDecimal(30), 3, "IBM US 11/01/99 C10");
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = CreateCompliancePositionValueHolderList(new BigDecimal(5), 3, "EFA US 11/01/99 C20");
		List<CompliancePositionValueHolder<AccountingPosition>> allPositions = CollectionUtils.combineCollections(inRangePositions, outOfRangePositions);
		final List<String> expectedSubdetailLabelValues = CollectionUtils.createList(
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C101] is in range [25] to [30].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C102] is in range [25] to [30].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C103] is in range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C201] is out of range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C202] is out of range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C203] is out of range [25] to [30].");

		CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("remainingQuantity", new BigDecimal(25), new BigDecimal(30), false);
		transformation.validate();

		List<ComplianceRuleRunSubDetail> runSubDetailList = new ArrayList<>();
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = transformation.apply(allPositions, transactionDate, null, runSubDetailList);

		Assertions.assertTrue(verifySubDetailOutput(expectedSubdetailLabelValues, runSubDetailList));
		Assertions.assertEquals(inRangePositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_numericRange_negate() {
		Date transactionDate = new Date();

		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = CreateCompliancePositionValueHolderList(new BigDecimal(30), 3, "IBM US 11/01/99 C10");
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = CreateCompliancePositionValueHolderList(new BigDecimal(5), 3, "EFA US 11/01/99 C20");
		List<CompliancePositionValueHolder<AccountingPosition>> allPositions = CollectionUtils.combineCollections(inRangePositions, outOfRangePositions);
		final List<String> expectedSubdetailLabelValues = CollectionUtils.createList(
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C101] is in range [25] to [30].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C102] is in range [25] to [30].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C103] is in range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C201] is out of range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C202] is out of range [25] to [30].",
				"[remainingQuantity] value [5] for [EFA US 11/01/99 C203] is out of range [25] to [30].");

		CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("remainingQuantity", new BigDecimal(25), new BigDecimal(30), true);
		transformation.validate();

		List<ComplianceRuleRunSubDetail> runSubDetailList = new ArrayList<>();
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = transformation.apply(allPositions, transactionDate, null, runSubDetailList);

		Assertions.assertTrue(verifySubDetailOutput(expectedSubdetailLabelValues, runSubDetailList));
		Assertions.assertEquals(outOfRangePositions, returnedPositions);
	}


	@Test
	public void testApply_no_positions_in_numericRange() {
		Date transactionDate = new Date();

		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions1 = CreateCompliancePositionValueHolderList(new BigDecimal(30), 3, "IBM US 11/01/99 C10");
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions2 = CreateCompliancePositionValueHolderList(new BigDecimal(55), 6, "EFA US 11/01/99 C20");
		List<CompliancePositionValueHolder<AccountingPosition>> allPositions = CollectionUtils.combineCollections(outOfRangePositions1, outOfRangePositions2);
		final List<String> expectedSubdetailLabelValues = CollectionUtils.createList(
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C101] is out of range [35] to [40].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C102] is out of range [35] to [40].",
				"[remainingQuantity] value [30] for [IBM US 11/01/99 C103] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C201] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C202] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C203] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C204] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C205] is out of range [35] to [40].",
				"[remainingQuantity] value [55] for [EFA US 11/01/99 C206] is out of range [35] to [40]."
		);

		CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("remainingQuantity", new BigDecimal(35), new BigDecimal(40), false);
		transformation.validate();

		List<ComplianceRuleRunSubDetail> runSubDetailList = new ArrayList<>();
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = transformation.apply(allPositions, transactionDate, null, runSubDetailList);

		Assertions.assertTrue(verifySubDetailOutput(expectedSubdetailLabelValues, runSubDetailList));
		Assertions.assertEquals(0, returnedPositions.size());
	}


	@Test
	public void testValidate_minVal_maxVal_areNull() {
		TestUtils.expectException(ValidationException.class, () -> {
			CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("remainingQuantity", null, null, false);
			transformation.validate();
		}, "At least one field value must be set (Minimum or maximum value).");
	}


	@Test
	public void testValidate_minVal_greater_than_maxVal() {
		TestUtils.expectException(ValidationException.class, () -> {
			CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("remainingQuantity", new BigDecimal(30), new BigDecimal(20), true);
			transformation.validate();
		}, "The maximum value must be greater than or equal to the minimum value.");
	}


	@Test
	public void testValidate_numericFieldNull() {
		TestUtils.expectException(ValidationException.class, () -> {
			CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation(null, new BigDecimal(20), new BigDecimal(30), true);
			transformation.validate();
		}, "A path to the numeric field used for filtering is required.");
	}


	@Test
	public void testValidate_numericFieldIsEmpty() {
		TestUtils.expectException(ValidationException.class, () -> {
			CompliancePositionFieldValueRangeFilteringTransformation transformation = createTransformation("", new BigDecimal(20), new BigDecimal(30), true);
			transformation.validate();
		}, "A path to the numeric field used for filtering is required.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates and returns a list of CompliancePositionValueHolder<AccountingPosition> for testing purposes with the EndDate or LastDeliveryDate of the configured with the
	 * a date derived from the date parameter and dateOffset.
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> CreateCompliancePositionValueHolderList(BigDecimal remainingQuantity, int count, String symbol) {
		List<CompliancePositionValueHolder<AccountingPosition>> positionValueList = new ArrayList<>();
		for (int counter = 1; counter <= count; ++counter) {
			InvestmentSecurity investmentSecurity = new InvestmentSecurity();
			investmentSecurity.setSymbol(symbol + counter);
			List<AccountingPosition> positionList = new ArrayList<>();
			AccountingTransaction openingTransaction = new AccountingTransaction();
			openingTransaction.setInvestmentSecurity(investmentSecurity);
			AccountingPosition position = AccountingPosition.forOpeningTransaction(openingTransaction);
			position.setRemainingQuantity(remainingQuantity);
			positionList.add(position);
			CompliancePositionValueHolder<AccountingPosition> compliancePositionValueHolder = new CompliancePositionValueHolder<>(new BigDecimal(2 * counter), positionList);
			positionValueList.add(compliancePositionValueHolder);
		}

		return positionValueList;
	}


	private CompliancePositionFieldValueRangeFilteringTransformation createTransformation(String numericField, BigDecimal minVal, BigDecimal maxVal, boolean negate) {
		CompliancePositionFieldValueRangeFilteringTransformation transformation = new CompliancePositionFieldValueRangeFilteringTransformation();
		transformation.setCompliancePositionPropertyPathHandler(new CompliancePositionPropertyPathHandlerImpl());
		transformation.setNumericField(numericField);
		transformation.setMinValue(minVal);
		transformation.setMaxValue(maxVal);
		transformation.setExcludeWithinRange(negate);

		return transformation;
	}


	/**
	 * Checks the Label property value in a list of ComplianceRunSubDetail entries and compares them to a list of expected String values.
	 * Returns true if both lists match, false if not.
	 */
	private boolean verifySubDetailOutput(List<String> expectedSubDetailLabelOutput, List<ComplianceRuleRunSubDetail> subDetailList) {

		if (subDetailList == null || subDetailList.size() != expectedSubDetailLabelOutput.size()) {
			return false;
		}

		for (int index = 0; index < expectedSubDetailLabelOutput.size(); ++index) {
			String expectedOutput = expectedSubDetailLabelOutput.get(index);
			String actualOutput = subDetailList.get(index).getLabel();
			if (!expectedOutput.equals(actualOutput)) {
				return false;
			}
		}

		return true;
	}
}
