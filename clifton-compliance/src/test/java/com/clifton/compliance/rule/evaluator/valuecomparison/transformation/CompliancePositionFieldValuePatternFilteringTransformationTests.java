package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.TradeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The test class for {@link CompliancePositionFieldValuePatternFilteringTransformation}.
 *
 * @author MikeH
 */
@SuppressWarnings({"ArraysAsListWithZeroOrOneArgument", "unused"})
public class CompliancePositionFieldValuePatternFilteringTransformationTests {

	private CompliancePositionFieldValuePatternFilteringTransformation compliancePositionFieldValueFilteringTransformation;

	@InjectMocks
	private CompliancePositionPropertyPathHandlerImpl compliancePositionPropertyPathHandler;
	@Mock
	private MarketDataFieldService marketDataFieldService;
	@Mock
	private SystemColumnService systemColumnService;
	@Mock
	private SystemColumnValueHandler systemColumnValueHandler;
	@Mock
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.compliancePositionFieldValueFilteringTransformation = new CompliancePositionFieldValuePatternFilteringTransformation();
		this.compliancePositionFieldValueFilteringTransformation.setCompliancePositionPropertyPathHandler(this.compliancePositionPropertyPathHandler);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFieldValueInteger() {
		List<AccountingPosition> list1 = createPositionList("id", 1, 1, 1);
		List<AccountingPosition> list2 = createPositionList("id", 2, 2, 2);
		List<AccountingPosition> list3 = createPositionList("id", 10, 10, 10);
		List<AccountingPosition> list4 = createPositionList("id", 22, 22, 22);
		List<AccountingPosition> list5 = createPositionList("id", 100, 100, 100);
		List<AccountingPosition> list6 = createPositionList("id", 102, 102, 102);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list1), "id", "1", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list2), "id", "2", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list3), "id", "1?", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list5, list6), "id", "1??", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list1, list3, list5, list6), "id", "1*", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list4), "id", "2?", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list1, list2, list3, list4, list5, list6), "id", "*", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(), "id", "null", false);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(), "id", "*", true);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list2, list4), "id", "1*", true);
		evaluate(Arrays.asList(list1, list2, list3, list4, list5, list6), Arrays.asList(list1, list2, list4, list5, list6), "id", "1?", true);
	}


	@Test
	public void testFieldValueBoolean() {
		List<AccountingPosition> list1 = createPositionList("pendingActivity", true, true, true);
		List<AccountingPosition> list2 = createPositionList("pendingActivity", false, false, false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1), "pendingActivity", "true", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1), "pendingActivity", "True", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1), "pendingActivity", "TRUE", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "false", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "False", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "FALSE", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1), "pendingActivity", "t*", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "f*", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "F*", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1, list2), "pendingActivity", "*e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(), "pendingActivity", "?????e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "*????e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "*????E", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "????e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list2), "pendingActivity", "????E", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1, list2), "pendingActivity", "*???e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1, list2), "pendingActivity", "*???E", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1, list2), "pendingActivity", "*??e", false);
		evaluate(Arrays.asList(list1, list2), Arrays.asList(list1, list2), "pendingActivity", "*??E", false);
	}


	@Test
	public void testFieldValueBigDecimal() {
		List<AccountingPosition> list1 = createPositionList("remainingQuantity", new BigDecimal("1.111"), new BigDecimal("1.111"), new BigDecimal("1.111"));
		List<AccountingPosition> list2 = createPositionList("remainingQuantity", new BigDecimal("0.001"), new BigDecimal("0.001"), new BigDecimal("0.001"));
		List<AccountingPosition> list3 = createPositionList("remainingQuantity", null, null, null);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1), "remainingQuantity", "1.111", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list2), "remainingQuantity", "0.001", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1, list2), "remainingQuantity", "*.*", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1, list2), "remainingQuantity", "?.*", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1), "remainingQuantity", "1.*", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list2), "remainingQuantity", "*.001", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(), "remainingQuantity", ".", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1, list2, list3), "remainingQuantity", ".", true);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list3), "remainingQuantity", "null", false);
		evaluate(Arrays.asList(list1, list2, list3), Arrays.asList(list1, list2), "remainingQuantity", "null", true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingPosition> createPositionList(String fieldName, Object... values) {
		return Arrays.stream(values)
				.map(value -> createPosition(fieldName, value))
				.collect(Collectors.toList());
	}


	private AccountingPosition createPosition(String fieldName, Object value) {
		AccountingPosition position = AccountingPosition.forOpeningTransaction(new AccountingTransaction());
		BeanUtils.setPropertyValue(position, fieldName, value);
		return position;
	}


	private void evaluate(Collection<Collection<AccountingPosition>> positionGroupList, Collection<Collection<AccountingPosition>> expectedPositionGroupList, String field, String comparisonPattern, boolean exclude) {
		// Configure transformation
		this.compliancePositionFieldValueFilteringTransformation.setPositionField(field);
		this.compliancePositionFieldValueFilteringTransformation.setComparisonPattern(comparisonPattern);
		this.compliancePositionFieldValueFilteringTransformation.setExcludeMatched(exclude);

		// Evaluate transformation
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = CollectionUtils.getConverted(positionGroupList, valueHolder -> new CompliancePositionValueHolder<>(BigDecimal.ZERO, new ArrayList<>(valueHolder)));
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = this.compliancePositionFieldValueFilteringTransformation.apply(valueHolderList, new Date(), new ContextImpl(), null);

		// Validate results
		List<List<AccountingPosition>> resultPositionGroupList = CollectionUtils.getConverted(resultValueHolderList, CompliancePositionValueHolder::getPositionList);
		Assertions.assertTrue(CompareUtils.isEqual(resultPositionGroupList, new ArrayList<>(expectedPositionGroupList)), String.format("Expected: %s. Actual: %s.", resultPositionGroupList, expectedPositionGroupList));
	}
}
