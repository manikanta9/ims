package com.clifton.compliance.rule.cache;


import com.clifton.business.service.BusinessService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.compliance.rule.rollup.ComplianceRuleRollup;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The test suite to verify the specificity functionality dictated for compliance rule assignments. This suite verifies that the correct assignments are selected for each account/
 * security combination.
 *
 * @author jgommels
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceRuleCacheTests {

	private static final Date PROCESS_DATE = DateUtils.toDate("9/1/2013");

	@Resource
	private XmlReadOnlyDAO<ComplianceRuleRollup> complianceRuleRollupDAO;
	@Resource
	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private XmlReadOnlyDAO<BusinessService> businessServiceDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		// Bypass unsupported logical expressions in rollup queries
		this.complianceRuleRollupDAO.setLogicalEvaluatesToTrue(true);
		this.businessServiceDAO.setIgnoreJoins(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/*
	 * Items to consider when manually updating these rules:
	 * - Order of precedence for specificity (instrument hierarchy and assignment target)
	 * - Applicability of each assignment (real-time vs batch and date)
	 *   - Mentally filter out all assignments that are not applicable before attempting evaluation
	 * - Scope for specificity (rule type for security-specific rules and rule ID for standard rules)
	 *   - For security-specific rules, only a maximum of one assignment for each type can be produced
	 *   - For standard rules, assignments that are more specific for a given rule (e.g. account instead of business service) take priority
	 */


	@Test
	public void testRealTimeAccountAssignmentList() {
		// Direct assignments
		assertEqualContents(getRealTimeAssignmentIdList(1), 1, 2, 3, 4, 5, 23, 24, 27, 28, 31);
		assertEqualContents(getRealTimeAssignmentIdList(2), 8, 9, 23, 24, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(3), 22, 23, 24, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(4), 22, 23, 24, 27, 28);

		// Business service assignments
		assertEqualContents(getRealTimeAssignmentIdList(5), 13, 19, 23, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(6), 14, 23, 24, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(7), 15, 17, 23, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(8), 17, 18, 19, 23, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(9), 17, 19, 23, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(10), 17, 19, 23, 27, 28);
	}


	@Test
	public void testRealTimeSecuritySpecificAssignmentList() {
		// Direct assignments
		assertEqualContents(getRealTimeAssignmentIdList(1, 1), 1, 3, 5);
		assertEqualContents(getRealTimeAssignmentIdList(1, 2), 2, 5);
		assertEqualContents(getRealTimeAssignmentIdList(1, 4), 2);
		assertEqualContents(getRealTimeAssignmentIdList(1, 5), 2);
		assertEqualContents(getRealTimeAssignmentIdList(1, 7), 4, 24, 28);
		assertEqualContents(getRealTimeAssignmentIdList(1, 9), 24, 27, 28);
		assertEqualContents(getRealTimeAssignmentIdList(1, 10), 4, 23, 31);
		assertEqualContents(getRealTimeAssignmentIdList(2, 1), 8);
		assertEqualContents(getRealTimeAssignmentIdList(2, 6), 24);
		assertEqualContents(getRealTimeAssignmentIdList(2, 8), 24, 28);
		assertEqualContents(getRealTimeAssignmentIdList(2, 11), 9, 23);
		assertEqualContents(getRealTimeAssignmentIdList(3, 3), 24);
		assertEqualContents(getRealTimeAssignmentIdList(4, 12), 23, 28);

		// Business service assignments
		assertEqualContents(getRealTimeAssignmentIdList(5, 1), 13);
		assertEqualContents(getRealTimeAssignmentIdList(5, 2), 19);
		assertEqualContents(getRealTimeAssignmentIdList(5, 12), 23, 28);
	}


	@Test
	public void testRealTimeConflictingAssignmentList() {
		assertEqualContents(getRealTimeAssignmentIdList(11, 1), 31, 32);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBatchAccountAssignmentList() {
		// Direct assignments
		assertEqualContents(getBatchAssignmentIdList(1), 6, 7, 29, 30, 31);
		assertEqualContents(getBatchAssignmentIdList(2), 10, 29, 30);
		assertEqualContents(getBatchAssignmentIdList(3), 29, 30);
		assertEqualContents(getBatchAssignmentIdList(4), 29, 30);

		// Business service assignments
		assertEqualContents(getBatchAssignmentIdList(5), 21, 29, 30);
		assertEqualContents(getBatchAssignmentIdList(6), 29, 30);
		assertEqualContents(getBatchAssignmentIdList(7), 16, 29, 30);
		assertEqualContents(getBatchAssignmentIdList(8), 21, 29, 30);
		assertEqualContents(getBatchAssignmentIdList(9), 21, 29, 30);
		assertEqualContents(getBatchAssignmentIdList(10), 21, 29, 30);
	}


	@Test
	public void testBatchSecuritySpecificAssignmentList() {
		// Business service assignments
		assertEqualContents(getBatchAssignmentIdList(1, 1), 6);
		assertEqualContents(getBatchAssignmentIdList(1, 2), 29);
		assertEqualContents(getBatchAssignmentIdList(1, 4), 29);
		assertEqualContents(getBatchAssignmentIdList(1, 5), 29);
		assertEqualContents(getBatchAssignmentIdList(1, 7), 7, 31);
		assertEqualContents(getBatchAssignmentIdList(1, 9), 7, 31);
		assertEqualContents(getBatchAssignmentIdList(1, 10), 7, 31);
		assertEqualContents(getBatchAssignmentIdList(2, 1), 29);
		assertEqualContents(getBatchAssignmentIdList(2, 6), 29);
		assertEqualContents(getBatchAssignmentIdList(2, 8));
		assertEqualContents(getBatchAssignmentIdList(2, 11), 10);
		assertEqualContents(getBatchAssignmentIdList(3, 3), 29);
		assertEqualContents(getBatchAssignmentIdList(4, 12), 30);

		// Business service assignments
		assertEqualContents(getBatchAssignmentIdList(5, 1), 29);
		assertEqualContents(getBatchAssignmentIdList(5, 2), 29);
		assertEqualContents(getBatchAssignmentIdList(5, 12), 21, 30);
	}


	@Test
	public void testBatchConflictingAssignmentList() {
		assertEqualContents(getBatchAssignmentIdList(12, 7), 33, 34);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Integer> getBatchAssignmentIdList(int accountId) {
		return this.complianceRuleAssignmentHandler.getAssignmentBatchList(getAccount(accountId), PROCESS_DATE).stream()
				.map(BaseSimpleEntity::getId)
				.collect(Collectors.toList());
	}


	private List<Integer> getBatchAssignmentIdList(int accountId, int securityId) {
		return this.complianceRuleAssignmentHandler.getAssignmentBatchSecuritySpecificList(getAccount(accountId), getSecurity(securityId), PROCESS_DATE).stream()
				.map(BaseSimpleEntity::getId)
				.collect(Collectors.toList());
	}


	private List<Integer> getRealTimeAssignmentIdList(int accountId) {
		return this.complianceRuleAssignmentHandler.getAssignmentRealTimeList(getAccount(accountId), PROCESS_DATE).stream()
				.map(BaseSimpleEntity::getId)
				.collect(Collectors.toList());
	}


	private List<Integer> getRealTimeAssignmentIdList(int accountId, int securityId) {
		return this.complianceRuleAssignmentHandler.getAssignmentRealTimeList(getAccount(accountId), getSecurity(securityId), PROCESS_DATE, true, true).stream()
				.map(BaseSimpleEntity::getId)
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	private final <T> void assertEqualContents(List<T> actualList, T... expectedElements) {
		Assertions.assertTrue(CompareUtils.isEqual(actualList, Arrays.asList(expectedElements)), String.format("Expected: %s. Actual: %s", Arrays.toString(expectedElements), actualList));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity getSecurity(int id) {
		return this.investmentInstrumentService.getInvestmentSecurity(id);
	}


	private InvestmentAccount getAccount(int id) {
		return this.investmentAccountService.getInvestmentAccount(id);
	}
}
