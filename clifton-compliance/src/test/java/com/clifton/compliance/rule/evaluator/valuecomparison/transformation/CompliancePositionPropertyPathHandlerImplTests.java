package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.trade.TradeService;
import com.clifton.trade.marketdata.TradeMarketDataField;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * A set of unit tests to test the {@link CompliancePositionPropertyPathHandler} class.
 *
 * @author DavidI
 */
@ContextConfiguration("CompliancePositionPropertyPathHandlerImplTests-context.xml")
@Transactional
public class CompliancePositionPropertyPathHandlerImplTests extends BaseInMemoryDatabaseTests {

	private static final long ACCOUNTING_TRANSACTION_ID = 7434041;
	private static final int TRADE_ID = 745037;
	private static final int TRADE_FILL_ID = 932110;
	private static final String SECURITY_SYMBOL = "EEM US 09/21/18 P34";
	private static final Date MEASURE_DATE = DateUtils.toDate("08/31/2018");
	private static final BigDecimal TRADE_MARKET_DATA_MEASURE_VALUE = new BigDecimal("1.234500000000000");
	// normally not on an option, but we are using this to test custom value retrieval

	@Resource
	private CompliancePositionPropertyPathHandler compliancePositionPropertyPathHandler;
	@Resource
	private AccountingTransactionService accountingTransactionService;
	@Resource
	private TradeService tradeService;
	@Resource
	private MarketDataFieldService marketDataFieldService;
	@Resource
	private MarketDataSourceService marketDataSourceService;
	@Resource
	private TradeMarketDataFieldService tradeMarketDataFieldService;

	private AccountingPosition position;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {

		// Set-up position
		AccountingTransaction openingTransaction = this.accountingTransactionService.getAccountingTransaction(ACCOUNTING_TRANSACTION_ID);
		openingTransaction.setFkFieldId(TRADE_FILL_ID);
		this.position = AccountingPosition.forOpeningTransaction(openingTransaction);

		// Create trade market data value (no trade market data in the system at the time of test creation)
		MarketDataValue marketDataValue = new MarketDataValue();
		TradeMarketDataField tradeMarketDataField = this.tradeMarketDataFieldService.getTradeMarketDataFieldByName("Delta - On Executed");
		marketDataValue.setDataField(tradeMarketDataField.getMarketDataField());
		marketDataValue.setDataSource(this.marketDataSourceService.getMarketDataSourceByName("Bloomberg"));
		marketDataValue.setMeasureDate(MEASURE_DATE);
		marketDataValue.setMeasureTime(TimeUtils.toTime("00:00:00"));
		marketDataValue.setMeasureValue(TRADE_MARKET_DATA_MEASURE_VALUE);
		marketDataValue.setInvestmentSecurity(this.position.getInvestmentSecurity());
		this.marketDataFieldService.saveMarketDataValue(marketDataValue);
		TradeMarketDataValue tradeMarketDataValue = new TradeMarketDataValue();
		tradeMarketDataValue.setTradeMarketDataField(tradeMarketDataField);
		tradeMarketDataValue.setReferenceOne(this.tradeService.getTrade(TRADE_ID));
		tradeMarketDataValue.setReferenceTwo(marketDataValue);
		this.tradeMarketDataFieldService.saveTradeMarketDataValue(tradeMarketDataValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("SimplifiableJUnitAssertion")
	@Test
	public void testGetPositionPropertyValue() {
		// Position fields
		Assertions.assertEquals(SECURITY_SYMBOL, getPositionPropertyValue("investmentSecurity.symbol"));
		// Test for null object paths
		AccountingPosition positionMissingValues = AccountingPosition.forOpeningTransaction(new AccountingTransaction());
		Assertions.assertEquals(null, getPositionPropertyValue(positionMissingValues, "openingTransaction.executingCompany.name"));

		// Market data fields
		Assertions.assertEquals(new BigDecimal("0.006883000000000"), getPositionPropertyValue(getMarketDataField("Delta")));

		// Custom column fields
		Assertions.assertEquals("TRS", getPositionPropertyValue(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR)));

		// Trade fields
		Assertions.assertEquals(new BigDecimal("1612.0000000000"), getPositionPropertyValue(getTradeField("quantityIntended")));
		Assertions.assertEquals(false, getPositionPropertyValue(getTradeField("buy")));
		Assertions.assertEquals("SELL TO OPEN", getPositionPropertyValue(getTradeField("openCloseType.label")));
		Assertions.assertEquals(DateUtils.toDate("09/15/2017"), getPositionPropertyValue(getTradeField("tradeDate")));
		Assertions.assertEquals(null, getPositionPropertyValue(getTradeField("limitPrice")));

		// Trade market data fields
		Assertions.assertEquals(TRADE_MARKET_DATA_MEASURE_VALUE, getPositionPropertyValue(getTradeMarketDataField("Delta (Executed) - Trade Data Field")));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateField_pass() {
		this.compliancePositionPropertyPathHandler.validateField("investmentSecurity.id");
		this.compliancePositionPropertyPathHandler.validateField(getMarketDataField("Delta"));
		this.compliancePositionPropertyPathHandler.validateField(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR));
		this.compliancePositionPropertyPathHandler.validateField(getTradeField("limitPrice"));
		this.compliancePositionPropertyPathHandler.validateField(getTradeMarketDataField("Delta (Executed) - Trade Data Field"));
	}


	@Test
	public void testValidateField_fail_position_property() {
		TestUtils.expectException(ValidationException.class,
				() -> this.compliancePositionPropertyPathHandler.validateField("unknown"),
				"The property [unknown] could not be found for objects of type [AccountingPosition].");
	}


	@Test
	public void testValidateField_fail_market_data_property() {
		TestUtils.expectException(ValidationException.class,
				() -> this.compliancePositionPropertyPathHandler.validateField(getMarketDataField("unknown")),
				"Unable to find Market Data Field with properties [name] and value(s) [unknown]");
	}


	@Test
	public void testValidateField_fail_custom_column() {
		TestUtils.expectException(ValidationException.class,
				() -> this.compliancePositionPropertyPathHandler.validateField(getCustomColumnField("investmentSecurity", "unknown")),
				"Unable to find custom column field [unknown] for objects of type [InvestmentSecurity].");
	}


	@Test
	public void testValidateField_fail_trade_property() {
		TestUtils.expectException(ValidationException.class,
				() -> this.compliancePositionPropertyPathHandler.validateField(getTradeField("unknown")),
				"The property [unknown] could not be found for objects of type [Trade].");
	}


	@Test
	public void testValidateField_fail_trade_market_data() {
		TestUtils.expectException(ValidationException.class,
				() -> this.compliancePositionPropertyPathHandler.validateField(getTradeMarketDataField("unknown")),
				"Unable to find trade market data field with name [unknown].");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFieldTypeIsNumeric() {
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric("remainingQuantity"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric("pendingActivity"));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric(getMarketDataField("delta")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR)));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric(getTradeField("limitPrice")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric(getTradeField("tradeDate")));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsNumeric(getTradeMarketDataField("Delta (Executed) - Trade Data Field")));
	}


	@Test
	public void testFieldTypeIsString() {
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString("positionGroupId"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString("pendingActivity"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString(getMarketDataField("delta")));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR)));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString(getTradeField("openCloseType.label")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString(getTradeField("tradeDate")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsString(getTradeMarketDataField("Delta (Executed) - Trade Data Field")));
	}


	@Test
	public void testFieldTypeIsDate() {
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate("investmentSecurity.endDate"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate("pendingActivity"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate(getMarketDataField("delta")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR)));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate(getTradeField("tradeDate")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate(getTradeField("limitPrice")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsDate(getTradeMarketDataField("Delta (Executed) - Trade Data Field")));
	}


	@Test
	public void testFieldTypeIsBoolean() {
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean("remainingQuantity"));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean("pendingActivity"));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean(getMarketDataField("delta")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean(getCustomColumnField("investmentSecurity", InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR)));
		Assertions.assertTrue(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean(getTradeField("blockTrade")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean(getTradeField("limitPrice")));
		Assertions.assertFalse(this.compliancePositionPropertyPathHandler.fieldDataTypeIsBoolean(getTradeMarketDataField("Delta (Executed) - Trade Data Field")));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Object getPositionPropertyValue(String field) {
		return getPositionPropertyValue(this.position, field);
	}


	private Object getPositionPropertyValue(AccountingPosition position, String field) {
		return this.compliancePositionPropertyPathHandler.getPositionPropertyValue(position, field, MEASURE_DATE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getMarketDataField(String field) {
		return CompliancePositionPropertyPathHandler.MARKET_DATA_FIELD_NAME_PREFIX + field;
	}


	private String getTradeField(String field) {
		return CompliancePositionPropertyPathHandler.OPENING_TRADE_PREFIX + field;
	}


	private String getTradeMarketDataField(String field) {
		return CompliancePositionPropertyPathHandler.TRADE_MARKET_DATA_FIELD_NAME_PREFIX + field;
	}


	private String getCustomColumnField(String propertyPath, String columnName) {
		return propertyPath + CompliancePositionPropertyPathHandler.CUSTOM_COLUMN_NAME_DELIMITER + columnName;
	}
}
