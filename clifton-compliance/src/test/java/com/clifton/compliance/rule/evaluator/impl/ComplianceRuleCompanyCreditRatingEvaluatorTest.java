package com.clifton.compliance.rule.evaluator.impl;


import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.ComplianceCreditRatingService;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.compliance.rule.account.ComplianceRuleAccountHandler;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.position.ComplianceRulePositionHandler;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.trade.Trade;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Test class for {@link ComplianceRuleCompanyCreditRatingEvaluator}.
 */
public class ComplianceRuleCompanyCreditRatingEvaluatorTest {

	@InjectMocks
	private ComplianceRuleCompanyCreditRatingEvaluator complianceRuleCompanyCreditRatingEvaluator;
	@InjectMocks
	private ComplianceRuleEvaluationConfig complianceRuleEvaluationConfig;

	@Mock
	private ComplianceRuleAccountHandler complianceRuleAccountHandler;
	@Mock
	private ComplianceRulePositionHandler complianceRulePositionHandler;
	@Mock
	private InvestmentAccountService investmentAccountService;
	@Mock
	private InvestmentInstrumentService investmentInstrumentService;
	@Mock
	private BusinessContractService businessContractService;
	@Mock
	private AccountingBalanceService accountingBalanceService;
	@Mock
	private ComplianceCreditRatingService complianceCreditRatingService;

	private BusinessCompany fitchAgency;
	private BusinessCompany moodysAgency;
	private BusinessCompany spAgency;
	private InvestmentAccountType investmentAccountType;
	private WorkflowStatus investmentAccountActiveStatus;
	private WorkflowStatus investmentAccountOpenStatus;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		setupCreditRatingAgencies();
		setupInvestmentAccountType();
		setupInvestmentAccountStatuses();
		setupConfigDefaults();
		setupEvaluatorDefaults();
	}


	private void setupCreditRatingAgencies() {
		this.fitchAgency = generateCompany("Fitch");
		this.moodysAgency = generateCompany("Moody's");
		this.spAgency = generateCompany("Standard and Poor's");
	}


	private void setupInvestmentAccountType() {
		this.investmentAccountType = TestUtils.generateEntity(InvestmentAccountType.class, Short.class);
		this.complianceRuleCompanyCreditRatingEvaluator.setAccountTypeIds(new Short[]{this.investmentAccountType.getId()});
	}


	private void setupInvestmentAccountStatuses() {
		this.investmentAccountActiveStatus = TestUtils.generateEntity(WorkflowStatus.class, Short.class);
		this.investmentAccountActiveStatus.setName(WorkflowStatus.STATUS_ACTIVE);
		this.investmentAccountOpenStatus = TestUtils.generateEntity(WorkflowStatus.class, Short.class);
		this.investmentAccountOpenStatus.setName(WorkflowStatus.STATUS_OPEN);
	}


	private void setupConfigDefaults() {
		configureProcessDate(new Date());
		configureRealTime(false);
		this.complianceRuleEvaluationConfig.setTradeRuleEvaluatorContext(new TradeRuleEvaluatorContext());
		this.complianceRuleEvaluationConfig.setBean(TestUtils.generateEntity(Trade.class, Integer.class));
	}


	private void setupEvaluatorDefaults() {
		configureValidateAllCompanies(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMissingClientAccountFail() {
		TestUtils.expectException(RuntimeException.class, () -> {
			this.complianceRuleEvaluationConfig.setClientAccountId(-1);
			evaluateRuleWithFail();
		}, "No target was retrieved for credit rating validation. A target is required.");
	}


	@Test
	public void testNoHoldingAccountsSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		configureHoldingAccountList();

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoCounterpartiesSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		configureHoldingAccountList(generateAccountWithHoldings("Holding account 1"), generateAccountWithHoldings("Holding account 2"));

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoHoldingsSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccount("Holding account 1");
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(99, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoHoldingsIncludeEmptySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccount("Holding account 1");
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(99, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureIncludeEmptyAccounts(true);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoHoldingsActiveSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccount("Holding account 1");
		setAccountActive(holdingAccount1, true);
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureIncludeEmptyAccounts(true);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoHoldingsActiveFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccount("Holding account 1");
		setAccountActive(holdingAccount1, true);
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(99, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureIncludeEmptyAccounts(true);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testIncludedCompanySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		BusinessCompany company1 = generateCompany("Company 1", generateCreditRating(100, this.fitchAgency));
		setCounterparties(holdingAccount1, company1);
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureValidateAllCompanies(false);
		configureIncludedCompanies(company1);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testIncludedCompanyFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		BusinessCompany company1 = generateCompany("Company 1", generateCreditRating(99, this.fitchAgency));
		setCounterparties(holdingAccount1, company1);
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureValidateAllCompanies(false);
		configureIncludedCompanies(company1);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testExcludedCompanySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		BusinessCompany company1 = generateCompany("Company 1", generateCreditRating(99, this.fitchAgency));
		BusinessCompany company2 = generateCompany("Company 2");
		setCounterparties(holdingAccount1, company1);
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureValidateAllCompanies(false);
		configureIncludedCompanies(company2);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testSingleCounterpartySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testSingleCounterpartyFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setCounterparties(holdingAccount1, generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(101);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testMultiCounterpartySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setCounterparties(holdingAccount1,
				generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)),
				generateCompany("Company 2", generateCreditRating(100, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testMultiCounterpartyFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setCounterparties(holdingAccount1,
				generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)),
				generateCompany("Company 2", generateCreditRating(99, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1", "Counterparty: Company 2 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testIssuingCompanySuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1", generateCreditRating(101, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testIssuingCompanyFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1", generateCreditRating(99, this.fitchAgency)));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testNoCreditRatingsSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1"));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(0);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testNoCreditRatingsFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1"));
		configureHoldingAccountList(holdingAccount1);

		// Configure rule
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	@Test
	public void testRealTimeAccountSuccess() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1", generateCreditRating(100, this.fitchAgency)));
		configureRealTimeHoldingAccount(holdingAccount1);

		// Configure rule
		configureRealTime(true);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testRealTimeAccountFail() {
		// Configure data
		configureClientAccount(generateAccount("Client account"));
		InvestmentAccount holdingAccount1 = generateAccountWithHoldings("Holding account 1");
		setIssuingCompany(holdingAccount1, generateCompany("Company 1", generateCreditRating(99, this.fitchAgency)));
		configureRealTimeHoldingAccount(holdingAccount1);

		// Configure rule
		configureRealTime(true);
		configureRequiredCount(1);
		configureRequiredFitchRating(100);

		// Evaluate rule
		evaluateRuleWithFail("Counterparty: Company 1 - The counterparty passed [0] of [1] required credit rating minimums.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComplianceRuleRun evaluateRuleWithPass() {
		ComplianceRuleRun ruleRun = this.complianceRuleCompanyCreditRatingEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);
		// Verify run status
		if (ruleRun.getStatus() != ComplianceRuleRun.STATUS_PASSED) {
			StringBuilder errorMsgSb = new StringBuilder();
			errorMsgSb.append("The rule evaluation failed. The evaluation was expected to pass.");
			errorMsgSb.append(StringUtils.NEW_LINE).append("Result description: [").append(ruleRun.getDescription()).append("].");
			for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(ruleRun.getChildren())) {
				errorMsgSb.append(StringUtils.NEW_LINE).append("Child description: [").append(runDetail.getDescription()).append("].");
			}
			AssertUtils.fail(errorMsgSb.toString());
		}
		return ruleRun;
	}


	private ComplianceRuleRun evaluateRuleWithFail(String... detailDescriptions) {
		ComplianceRuleRun ruleRun = this.complianceRuleCompanyCreditRatingEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);

		// Verify run status
		if (ruleRun.getStatus() != ComplianceRuleRun.STATUS_FAILED) {
			StringBuilder errorMsgSb = new StringBuilder();
			errorMsgSb.append("The rule evaluation passed. The evaluation was expected to fail.");
			errorMsgSb.append(StringUtils.NEW_LINE).append("Result description: [").append(ruleRun.getDescription()).append("].");
			for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(ruleRun.getChildren())) {
				errorMsgSb.append(StringUtils.NEW_LINE).append("Child description: [").append(runDetail.getDescription()).append("].");
			}
			AssertUtils.fail(errorMsgSb.toString());
		}

		// Number of run details
		int numExpectedChildren = ArrayUtils.getLength(detailDescriptions);
		int numActualChildren = CollectionUtils.getSize(ruleRun.getChildren());
		AssertUtils.assertEquals(numExpectedChildren, numActualChildren,
				"The number of expected detail descriptions [%d] did not match the actual number of run details [%d].",
				numExpectedChildren, numActualChildren);

		// Validate run detail descriptions
		for (int detailIndex = 0; detailIndex < CollectionUtils.getSize(ruleRun.getChildren()); detailIndex++) {
			ComplianceRuleRunDetail runDetail = ruleRun.getChildren().get(detailIndex);
			String expectedDescription = detailDescriptions[detailIndex];
			AssertUtils.assertEquals(expectedDescription, runDetail.getDescription(), "The message did not match the expected description. Expected: [%s]. Actual: [%s].", expectedDescription, runDetail.getDescription());
		}

		return ruleRun;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureProcessDate(Date date) {
		this.complianceRuleEvaluationConfig.setProcessDate(date);
	}


	private void configureRealTime(boolean realTime) {
		this.complianceRuleEvaluationConfig.setRealTime(realTime);
	}


	private void configureClientAccount(InvestmentAccount account) {
		this.complianceRuleEvaluationConfig.setClientAccountId(BeanUtils.getBeanIdentity(account));
	}


	private void configureHoldingAccountList(InvestmentAccount... accounts) {
		Mockito.when(this.investmentAccountService.getInvestmentAccountList(ArgumentMatchers.any())).thenReturn(CollectionUtils.createList(accounts));
	}


	private void configureRealTimeHoldingAccount(InvestmentAccount account) {
		((Trade) this.complianceRuleEvaluationConfig.getBean()).setHoldingInvestmentAccount(account);
	}


	private void configureIncludeEmptyAccounts(boolean includeEmptyAccounts) {
		this.complianceRuleCompanyCreditRatingEvaluator.setIncludeEmptyAccounts(includeEmptyAccounts);
	}


	private void configureValidateAllCompanies(boolean validateAllCompanies) {
		this.complianceRuleCompanyCreditRatingEvaluator.setValidateAllCompanies(validateAllCompanies);
	}


	private void configureIncludedCompanies(BusinessCompany... companies) {
		this.complianceRuleCompanyCreditRatingEvaluator.setCompanyIds(ArrayUtils.getStream(companies)
				.map(BaseEntity::getId)
				.toArray(Integer[]::new));
	}


	private void configureRequiredCount(int count) {
		this.complianceRuleCompanyCreditRatingEvaluator.setCount(count);
	}


	private void configureRequiredFitchRating(int ratingWeight) {
		this.complianceRuleCompanyCreditRatingEvaluator.setCreditRatingFitchId(generateCreditRating(ratingWeight, this.fitchAgency).getId());
	}


	private void configureRequiredMoodysRating(int ratingWeight) {
		this.complianceRuleCompanyCreditRatingEvaluator.setCreditRatingMoodysId(generateCreditRating(ratingWeight, this.moodysAgency).getId());
	}


	private void configureRequiredSpRating(int ratingWeight) {
		this.complianceRuleCompanyCreditRatingEvaluator.setCreditRatingStandardAndPoorsId(generateCreditRating(ratingWeight, this.spAgency).getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setCounterparties(InvestmentAccount account, BusinessCompany... counterparties) {
		List<BusinessContractParty> businessContractPartyList = ArrayUtils.getStream(counterparties)
				.map(this::generateBusinessContractParty)
				.collect(Collectors.toList());

		// Generate contract party list to return
		Mockito.when(this.businessContractService.getBusinessContractPartyListByContractAndRole(ArgumentMatchers.eq(account.getBusinessContract().getId()), ArgumentMatchers.eq(BusinessContractPartyRole.PARTY_ROLE_COUNTERPARTY_NAME)))
				.thenReturn(businessContractPartyList);
	}


	private void setIssuingCompany(InvestmentAccount account, BusinessCompany company) {
		account.setIssuingCompany(company);
	}


	private void setAccountActive(InvestmentAccount account, boolean active) {
		account.setWorkflowStatus(active ? this.investmentAccountActiveStatus : this.investmentAccountOpenStatus);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComplianceCreditRating generateCreditRating(int ratingWeight, BusinessCompany ratingAgency) {
		ComplianceCreditRating creditRating = TestUtils.generateEntity(ComplianceCreditRating.class, Integer.class);
		creditRating.setName(String.format("Credit rating for [%s]", ratingAgency.getLabel()));
		creditRating.setRatingWeight(ratingWeight);
		creditRating.setRatingAgency(ratingAgency);
		creditRating.setShortTerm(true);
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRating(ArgumentMatchers.eq(creditRating.getId()))).thenReturn(creditRating);
		return creditRating;
	}


	private ComplianceCreditRatingValueForIssuer generateCreditRatingValue(ComplianceCreditRating creditRating) {
		ComplianceCreditRatingValueForIssuer creditRatingValue = TestUtils.generateEntity(ComplianceCreditRatingValueForIssuer.class, Integer.class);
		creditRatingValue.setCreditRating(creditRating);
		creditRatingValue.setStartDate(new Date(0L));
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRatingValue(ArgumentMatchers.eq(creditRatingValue.getId()))).thenReturn(creditRatingValue);
		return creditRatingValue;
	}


	private InvestmentAccount generateAccount(String name) {
		InvestmentAccount account = TestUtils.generateEntity(InvestmentAccount.class, Integer.class);
		account.setName(name);
		account.setType(this.investmentAccountType);
		account.setBusinessContract(TestUtils.generateEntity(BusinessContract.class, Integer.class));
		Mockito.when(this.investmentAccountService.getInvestmentAccount(ArgumentMatchers.eq(account.getId()))).thenReturn(account);
		return account;
	}


	private InvestmentAccount generateAccountWithHoldings(String name) {
		InvestmentAccount account = generateAccount(name);
		Mockito.doReturn(Collections.singletonList(new AccountingBalance()))
				.when(this.accountingBalanceService)
				.getAccountingBalanceList(ArgumentMatchers.argThat(searchForm -> CompareUtils.isEqual(searchForm.getHoldingInvestmentAccountId(), account.getId())));
		return account;
	}


	private BusinessContractParty generateBusinessContractParty(BusinessCompany company) {
		BusinessContractParty party = TestUtils.generateEntity(BusinessContractParty.class, Integer.class);
		party.setCompany(company);
		return party;
	}


	private BusinessCompany generateCompany(String name, ComplianceCreditRating... creditRatings) {
		final BusinessCompany company = TestUtils.generateEntity(BusinessCompany.class, Integer.class);
		company.setName(name);

		// Generate credit rating values to return
		List<ComplianceCreditRatingValueForIssuer> creditRatingValueList = ArrayUtils.getStream(creditRatings)
				.map(this::generateCreditRatingValue)
				.collect(Collectors.toList());
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRatingValueForIssuersForIssuer(ArgumentMatchers.eq(company.getId()))).thenReturn(creditRatingValueList);

		return company;
	}
}
