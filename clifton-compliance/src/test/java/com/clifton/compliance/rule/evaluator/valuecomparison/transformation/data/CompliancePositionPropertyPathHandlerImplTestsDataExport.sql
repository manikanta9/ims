SELECT 'TradeFill' AS entityTableName, tf.TradeFillID AS entityId
FROM TradeFill tf
WHERE TradeFillID = 932110

UNION

SELECT 'AccountingTransaction' AS entityTableName, at.AccountingTransactionID AS entityId
FROM AccountingTransaction at
WHERE AccountingTransactionID = 7434041

UNION

SELECT 'MarketDataValue' AS entityTableName, mdv.MarketDataValueID AS entityId
FROM MarketDataValue mdv
	 JOIN InvestmentSecurity [is] ON mdv.InvestmentSecurityID = [is].InvestmentSecurityID
WHERE mdv.MeasureDate = '2018-08-31'
  AND [is].Symbol = 'EEM US 09/21/18 P34'

UNION

SELECT 'TradeMarketDataField' AS entityTableName, tmdf.TradeMarketDataFieldID AS entityId
FROM TradeMarketDataField tmdf
WHERE tmdf.TradeMarketDataFieldName = 'Delta - On Executed';


