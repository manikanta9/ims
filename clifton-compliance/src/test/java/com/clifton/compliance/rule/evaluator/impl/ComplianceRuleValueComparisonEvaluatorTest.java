package com.clifton.compliance.rule.evaluator.impl;

import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.valuecomparison.ComplianceRuleValueComparisonEvaluator;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.ComplianceRuleValueCalculator;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.ComparisonTypes;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Test class for {@link ComplianceRuleValueComparisonEvaluator}.
 *
 * @author MikeH
 */
public class ComplianceRuleValueComparisonEvaluatorTest {


	@InjectMocks
	private ComplianceRuleValueComparisonEvaluator complianceRuleValueComparisonEvaluator;
	@Mock
	private SystemBeanService systemBeanService;

	@Mock
	private ComplianceRuleEvaluationConfig complianceRuleEvaluationConfig;

	/**
	 * Map holding the pre-determined calculation results for generated system beans.
	 */
	private final Map<SystemBean, BigDecimal> beanToValueMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.any())).then(invocation ->
				// When retrieving a bean instance, generate a calculator which returns the pre-determined value from the value map
				(ComplianceRuleValueCalculator) (ruleEvaluationConfig, runSubDetailList) -> this.beanToValueMap.get(invocation.<SystemBean>getArgument(0)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSingleValueLeftCompareSuccess() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.GREATER_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10));
		configureSecondValueBeanList();
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testSingleValueLeftCompareFail() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.LESS_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10));
		configureSecondValueBeanList();
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithFail(String.format("[%s] %s [%s]", new BigDecimal("10.00"), comparisonType, new BigDecimal("0.00")));
	}


	@Test
	public void testSingleValueRightCompareSuccess() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.LESS_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList();
		configureSecondValueBeanList(generateCalculatorBean(10));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testSingleValueRightCompareFail() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.GREATER_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList();
		configureSecondValueBeanList(generateCalculatorBean(10));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithFail(String.format("[%s] %s [%s]", new BigDecimal("0.00"), comparisonType, new BigDecimal("10.00")));
	}


	@Test
	public void testBothValueCompareSuccess() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.EQUAL_TO;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10));
		configureSecondValueBeanList(generateCalculatorBean(10));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testBothValueCompareFail() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.EQUAL_TO;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(-10));
		configureSecondValueBeanList(generateCalculatorBean(10));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithFail(String.format("[%s] %s [%s]", new BigDecimal("-10.00"), comparisonType, new BigDecimal("10.00")));
	}


	@Test
	public void testMultiValueCompareSuccess() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.LESS_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10), generateCalculatorBean(20), generateCalculatorBean(30));
		configureSecondValueBeanList(generateCalculatorBean(100));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testMultiValueCompareFail() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.GREATER_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10), generateCalculatorBean(20), generateCalculatorBean(30));
		configureSecondValueBeanList(generateCalculatorBean(100));
		configureFirstValueMultiplier(BigDecimal.ONE);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithFail(String.format("[%s] %s [%s]", new BigDecimal("60.00"), comparisonType, new BigDecimal("100.00")));
	}


	@Test
	public void testMultiplierCompareSuccess() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.EQUAL_TO;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10));
		configureSecondValueBeanList(generateCalculatorBean(100));
		configureFirstValueMultiplier(BigDecimal.TEN);
		configureSecondValueMultiplier(BigDecimal.ONE);

		// Evaluate rule
		evaluateRuleWithPass();
	}


	@Test
	public void testMultiplierCompareFail() {
		// Configure data
		ComparisonTypes comparisonType = ComparisonTypes.LESS_THAN;
		configureComparisonType(comparisonType);
		configureFirstValueBeanList(generateCalculatorBean(10));
		configureSecondValueBeanList(generateCalculatorBean(100));
		configureFirstValueMultiplier(BigDecimal.TEN);
		configureSecondValueMultiplier(BigDecimal.valueOf(0.25));

		// Evaluate rule
		evaluateRuleWithFail(String.format("[%s] %s [%s]", new BigDecimal("100.00"), comparisonType, new BigDecimal("25.00")));
	}


	@Test
	public void testValidateSuccess() {
		SystemBeanType calculatorBeanType = TestUtils.generateEntity(SystemBeanType.class, Integer.class);
		SystemBean calculatorBean1 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean2 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean3 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean4 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		calculatorBeanType.setClassName(ComplianceRuleValueCalculator.class.getName());
		calculatorBean1.setType(calculatorBeanType);
		calculatorBean2.setType(calculatorBeanType);
		calculatorBean3.setType(calculatorBeanType);
		calculatorBean4.setType(calculatorBeanType);
		calculatorBean1.setName("Bean 1");
		calculatorBean2.setName("Bean 2");
		calculatorBean3.setName("Bean 3");
		calculatorBean4.setName("Bean 4");
		configureFirstValueBeanList(calculatorBean1, calculatorBean2);
		configureSecondValueBeanList(calculatorBean3, calculatorBean4);
		this.complianceRuleValueComparisonEvaluator.validate();
	}


	@Test
	public void testValidateFirstListFail() {
		SystemBeanType calculatorBeanType = TestUtils.generateEntity(SystemBeanType.class, Integer.class);
		SystemBeanType nonCalculatorBeanType = TestUtils.generateEntity(SystemBeanType.class, Integer.class);
		SystemBean calculatorBean1 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean2 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean3 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean4 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		calculatorBeanType.setClassName(ComplianceRuleValueCalculator.class.getName());
		nonCalculatorBeanType.setClassName(this.getClass().getName());
		calculatorBean1.setType(calculatorBeanType);
		calculatorBean2.setType(nonCalculatorBeanType);
		calculatorBean3.setType(calculatorBeanType);
		calculatorBean4.setType(calculatorBeanType);
		calculatorBean1.setName("Bean 1");
		calculatorBean2.setName("Bean 2");
		calculatorBean3.setName("Bean 3");
		calculatorBean4.setName("Bean 4");
		configureFirstValueBeanList(calculatorBean1, calculatorBean2);
		configureSecondValueBeanList(calculatorBean3, calculatorBean4);
		TestUtils.expectException(FieldValidationException.class, () -> {
			this.complianceRuleValueComparisonEvaluator.validate();
		}, String.format("The bean [%s] is not of the required type: [%s].", calculatorBean2.getName(), ComplianceRuleValueCalculator.class.getName()));
	}


	@Test
	public void testValidateSecondListFail() {
		SystemBeanType calculatorBeanType = TestUtils.generateEntity(SystemBeanType.class, Integer.class);
		SystemBeanType nonCalculatorBeanType = TestUtils.generateEntity(SystemBeanType.class, Integer.class);
		SystemBean calculatorBean1 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean2 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean3 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		SystemBean calculatorBean4 = TestUtils.generateEntity(SystemBean.class, Integer.class);
		calculatorBeanType.setClassName(ComplianceRuleValueCalculator.class.getName());
		nonCalculatorBeanType.setClassName(this.getClass().getName());
		calculatorBean1.setType(calculatorBeanType);
		calculatorBean2.setType(calculatorBeanType);
		calculatorBean3.setType(nonCalculatorBeanType);
		calculatorBean4.setType(nonCalculatorBeanType);
		calculatorBean1.setName("Bean 1");
		calculatorBean2.setName("Bean 2");
		calculatorBean3.setName("Bean 3");
		calculatorBean4.setName("Bean 4");
		configureFirstValueBeanList(calculatorBean1, calculatorBean2);
		configureSecondValueBeanList(calculatorBean3, calculatorBean4);
		TestUtils.expectException(FieldValidationException.class, () -> {
			this.complianceRuleValueComparisonEvaluator.validate();
		}, String.format("The bean [%s] is not of the required type: [%s].", calculatorBean3.getName(), ComplianceRuleValueCalculator.class.getName()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComplianceRuleRun evaluateRuleWithPass() {
		ComplianceRuleRun ruleRun = this.complianceRuleValueComparisonEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);
		// Verify run status
		if (ruleRun.getStatus() != ComplianceRuleRun.STATUS_PASSED) {
			StringBuilder errorMsgSb = new StringBuilder();
			errorMsgSb.append("The rule evaluation failed. The evaluation was expected to pass.");
			errorMsgSb.append(StringUtils.NEW_LINE).append("Result description: [").append(ruleRun.getDescription()).append("].");
			for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(ruleRun.getChildren())) {
				errorMsgSb.append(StringUtils.NEW_LINE).append("Child description: [").append(runDetail.getDescription()).append("].");
			}
			AssertUtils.fail(errorMsgSb.toString());
		}
		return ruleRun;
	}


	private ComplianceRuleRun evaluateRuleWithFail(String... detailDescriptions) {
		ComplianceRuleRun ruleRun = this.complianceRuleValueComparisonEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);

		// Verify run status
		if (ruleRun.getStatus() != ComplianceRuleRun.STATUS_FAILED) {
			StringBuilder errorMsgSb = new StringBuilder();
			errorMsgSb.append("The rule evaluation passed. The evaluation was expected to fail.");
			errorMsgSb.append(StringUtils.NEW_LINE).append("Result description: [").append(ruleRun.getDescription()).append("].");
			for (ComplianceRuleRunDetail runDetail : CollectionUtils.getIterable(ruleRun.getChildren())) {
				errorMsgSb.append(StringUtils.NEW_LINE).append("Child description: [").append(runDetail.getDescription()).append("].");
			}
			AssertUtils.fail(errorMsgSb.toString());
		}

		// Number of run details
		int numExpectedChildren = ArrayUtils.getLength(detailDescriptions);
		int numActualChildren = CollectionUtils.getSize(ruleRun.getChildren());
		AssertUtils.assertEquals(numExpectedChildren, numActualChildren,
				"The number of expected detail descriptions [%d] did not match the actual number of run details [%d].",
				numExpectedChildren, numActualChildren);

		// Validate run detail descriptions
		for (int detailIndex = 0; detailIndex < CollectionUtils.getSize(ruleRun.getChildren()); detailIndex++) {
			ComplianceRuleRunDetail runDetail = ruleRun.getChildren().get(detailIndex);
			String expectedDescription = detailDescriptions[detailIndex];
			AssertUtils.assertEquals(expectedDescription, runDetail.getDescription(), "The message did not match the expected description. Expected: [%s]. Actual: [%s].", expectedDescription, runDetail.getDescription());
		}

		return ruleRun;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureProcessDate(Date date) {
		this.complianceRuleEvaluationConfig.setProcessDate(date);
	}


	private void configureRealTime(boolean realTime) {
		this.complianceRuleEvaluationConfig.setRealTime(realTime);
	}


	private void configureFirstValueBeanList(SystemBean... beans) {
		this.complianceRuleValueComparisonEvaluator.setFirstValueBeanList(Arrays.asList(beans));
	}


	private void configureSecondValueBeanList(SystemBean... beans) {
		this.complianceRuleValueComparisonEvaluator.setSecondValueBeanList(Arrays.asList(beans));
	}


	private void configureFirstValueMultiplier(BigDecimal multiplier) {
		this.complianceRuleValueComparisonEvaluator.setFirstValueMultiplier(multiplier);
	}


	private void configureSecondValueMultiplier(BigDecimal multiplier) {
		this.complianceRuleValueComparisonEvaluator.setSecondValueMultiplier(multiplier);
	}


	private void configureComparisonType(ComparisonTypes comparisonType) {
		this.complianceRuleValueComparisonEvaluator.setComparisonType(comparisonType);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBean generateCalculatorBean(long value) {
		return generateCalculatorBean(BigDecimal.valueOf(value));
	}


	private SystemBean generateCalculatorBean(BigDecimal value) {
		SystemBean bean = TestUtils.generateEntity(SystemBean.class, Integer.class);
		this.beanToValueMap.put(bean, value);
		return bean;
	}
}
