SELECT 'BusinessServiceProcessingType' AS entityTableName, BusinessServiceProcessingTypeID AS entityId FROM BusinessServiceProcessingType
UNION SELECT 'SystemColumnGroup' AS entityTableName, SystemColumnGroupID  as entityID FROM SystemColumnGroup WHERE ColumnGroupName = 'Business Service Processing Type Custom Fields'
UNION SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID as entityId FROM SystemColumnValue WHERE SystemColumnID IN (SELECT SystemColumnID FROM SystemColumn WHERE SystemColumnGroupID = (SELECT SystemColumnGroupID FROM SystemColumnGroup WHERE  ColumnGroupName = 'Business Service Processing Type Custom Fields'));

