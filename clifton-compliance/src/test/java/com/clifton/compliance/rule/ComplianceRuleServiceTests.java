package com.clifton.compliance.rule;

import com.clifton.compliance.rule.assignment.cache.securityspecific.ComplianceAssignmentSecuritySpecificTargetHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


//Creates a rule assignment like this (CR=ComplianceRule):
//									ComplianceRuleAssignment:assignmentWithRollup
//														|
//												 CR:rollupRule1
//											  /			|		  \
//								 CR:rollupRule2  CR:nonRollupRule1 CR:rollupRule3
//								/			\					   /				\
//			  CR:nonRollupRule2				CR:nonRollupRule3 CR:nonRollupRule4		CR:nonRollupRule5
//
// There is also a assignmentWithoutRollup with a non-rollup ComplianceRule 'nonRollupRule6'.
// All non-rollup rules except nonRollupRule5 are real-time rules.

//TODO Need to do some mocking or something to test this functionality since using the mock DAOs won't work
//for this case due to the more complex search criteria involved with many-to-many relationships. Ideally
//for cases like this we'd just have an in-memory database like HSQLDB that we use for the unit tests.
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceRuleServiceTests {

	private static final int ROLLUP_RULE1 = 1;
	@SuppressWarnings("unused")
	private static final int ROLLUP_RULE2 = 2;
	@SuppressWarnings("unused")
	private static final int ROLLUP_RULE3 = 3;
	private static final int NON_ROLLUP_RULE1 = 4;
	private static final int NON_ROLLUP_RULE2 = 5;
	private static final int NON_ROLLUP_RULE3 = 6;
	private static final int NON_ROLLUP_RULE4 = 7;
	private static final int NON_ROLLUP_RULE5 = 8;
	private static final int NON_ROLLUP_RULE6 = 9;

	@Resource
	private ComplianceRuleService complianceRuleService;


	/**
	 * Tests the recursive processing of {@link ComplianceRuleService#getComplianceRuleFullChildrenList(ComplianceRule)} to
	 * ensure that it builds the correct list of {@link ComplianceAssignmentSecuritySpecificTargetHolder} objects.
	 */
	@Test
	@Disabled
	public void testGetComplianceRuleFullChildListWithoutSearch() {

		List<ComplianceRule> rules = getComplianceRuleService().getComplianceRuleChildrenList(getComplianceRule(ROLLUP_RULE1));
		Assertions.assertEquals(5, rules.size());
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE1)));
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE2)));
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE3)));
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE4)));
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE5)));
		Assertions.assertTrue(rules.contains(getComplianceRule(NON_ROLLUP_RULE6)));
	}


	private ComplianceRule getComplianceRule(int id) {
		return getComplianceRuleService().getComplianceRule(id);
	}


	public ComplianceRuleService getComplianceRuleService() {
		return this.complianceRuleService;
	}


	public void setComplianceRuleService(ComplianceRuleService complianceRuleService) {
		this.complianceRuleService = complianceRuleService;
	}
}
