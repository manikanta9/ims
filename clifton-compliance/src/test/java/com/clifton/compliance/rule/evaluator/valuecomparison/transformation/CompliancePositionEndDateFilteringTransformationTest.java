package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.clifton.compliance.rule.evaluator.valuecomparison.transformation.CompliancePositionEndDateFilteringTransformation.DateFields;


/**
 * A set of unit tests to test the CompliancePositionEndDateFilteringTransformation class.
 *
 * @author davidi
 */
public class CompliancePositionEndDateFilteringTransformationTest {

	private static final Date PROCESS_DATE = DateUtils.toDate("2018/11/08");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testApply_positions_in_daterange_withEndDate() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(-3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_daterange_with_minmumDays_null() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(33, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_daterange_negate_withEndDate() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).max(10).excludeWithinRange().useProcessDate().run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(outOfRangePositions, returnedPositions);
	}


	@Test
	public void testApply_no_positions_in_daterange_withEndDate() {
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(-3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).max(10).useProcessDate().run(outOfRangePositions);
		Assertions.assertEquals(0, returnedPositions.size());
	}


	@Test
	public void testApply_positions_withSecurityEndDateNull() {
		List<CompliancePositionValueHolder<AccountingPosition>> positions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		positions.get(0).getPositionList().get(0).getInvestmentSecurity().setEndDate(null);
		positions.get(1).getPositionList().get(0).getInvestmentSecurity().setEndDate(null);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).max(10).useProcessDate().run(positions, outOfRangePositions);
		// No returned positions because the end date on the options is null (interpreted as being in the very distant future, and therefore out of date range)
		Assertions.assertEquals(0, returnedPositions.size());
	}


	@Test
	public void testApply_positions_inrange_withSecurityEndDateNull() {
		List<CompliancePositionValueHolder<AccountingPosition>> positions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		positions.get(0).getPositionList().get(0).getInvestmentSecurity().setEndDate(null);
		positions.get(1).getPositionList().get(0).getInvestmentSecurity().setEndDate(null);
		List<CompliancePositionValueHolder<AccountingPosition>> positions2 = createPositionValueHolderList(12, 5, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> allPositions = CollectionUtils.combineCollections(positions, positions2);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).useProcessDate().run(allPositions);
		// MaxDays set to null -- so all positions should be included
		Assertions.assertEquals(allPositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_daterange_withLastDeliveryDate() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.LAST_DELIVERY_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(-3, 2, DateFields.LAST_DELIVERY_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.LAST_DELIVERY_DATE).min(0).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_daterange_withEndDate_maximumDaysNull() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> allPositions = CollectionUtils.combineCollections(inRangePositions, outOfRangePositions);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).min(0).useProcessDate().run(allPositions);
		Assertions.assertEquals(allPositions, returnedPositions);
	}


	@Test
	public void testApply_positions_in_daterange_withEndDate_minimumDaysNull() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> returnedPositions = TransformationCommand.ofType(DateFields.END_DATE).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, returnedPositions);
	}


	@Test
	public void testValidate_null_DateField() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(null).min(0).max(10).useProcessDate().run();
		}, "A DateType is required for the date field.");
	}


	@Test
	public void testValidate_MinDaysGreaterThanMaxDays() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).min(3).max(2).useProcessDate().run();
		}, "The 'maximumDays' value must be greater than or equal to the 'minimumDays' value.");
	}


	@Test
	public void testValidate_MinDaysIsNegative() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).min(-3).max(2).useProcessDate().run();
		}, "The 'minimumDays' value cannot be negative.");
	}


	@Test
	public void testValidate_MaxDaysIsNegative() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).min(3).max(-2).useProcessDate().run();
		}, "The 'maximumDays' value cannot be negative.");
	}


	@Test
	public void testValidate_MinimumDays_MaximumDays_both_null() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).useProcessDate().run();
		}, "At least one field value must be set (Minimum Days or Maximum Days).");
	}


	@Test
	public void testValidate_no_positions() {
		TestUtils.expectException(ValidationException.class, () -> {
			List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
			inRangePositions.get(0).getPositionList().clear();
			List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);
			TransformationCommand.ofType(DateFields.END_DATE).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		}, "Empty position groups are not supported by the end date filtering transformation.");
	}


	@Test
	public void testValidate_multiple_positions_different_endDates() {


		List<CompliancePositionValueHolder<AccountingPosition>> extraPositions = createPositionValueHolderList(4, 1, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(3, 2, DateFields.END_DATE);
		// add security with different end date
		inRangePositions.get(0).getPositionList().add(extraPositions.get(0).getPositionList().get(0));
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(12, 5, DateFields.END_DATE);

		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).max(10).useProcessDate().run(inRangePositions, outOfRangePositions);
		}, String.format("All positions within each position group have an equivalent end date of type [%s]. Discovered positions with differing dates: ", DateFields.END_DATE), inRangePositions.get(0).getPositionList().get(0).toString(), extraPositions.get(0).getPositionList().get(0).toString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPositionEndDateMin() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(2, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(1, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> resultList = TransformationCommand.ofType(DateFields.END_DATE).min(3).run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, resultList);
	}


	@Test
	public void testPositionEndDateMax() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(1, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(2, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> resultList = TransformationCommand.ofType(DateFields.END_DATE).max(3).run(inRangePositions, outOfRangePositions);
		Assertions.assertEquals(inRangePositions, resultList);
	}


	@Test
	public void testPositionEndDateMinAndMax() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions = createPositionValueHolderList(1, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions1 = createPositionValueHolderList(2, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions2 = createPositionValueHolderList(0, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> resultList = TransformationCommand.ofType(DateFields.END_DATE).min(1).max(3).run(inRangePositions, outOfRangePositions1, outOfRangePositions2);
		Assertions.assertEquals(inRangePositions, resultList);
	}


	@Test
	public void testPositionEndDateNegated() {
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions1 = createPositionValueHolderList(2, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> inRangePositions2 = createPositionValueHolderList(0, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> outOfRangePositions = createPositionValueHolderList(1, 2, DateFields.END_DATE);
		List<CompliancePositionValueHolder<AccountingPosition>> resultList = TransformationCommand.ofType(DateFields.END_DATE).min(1).max(3).excludeWithinRange().run(inRangePositions1, inRangePositions2, outOfRangePositions);
		Assertions.assertEquals(CollectionUtils.combineCollections(inRangePositions1, inRangePositions2), resultList);
	}


	@Test
	public void testPositionEndDateValidation() {
		List<CompliancePositionValueHolder<AccountingPosition>> positionValueHolderList = CollectionUtils.combineCollections(
				createPositionValueHolderList(2, 2, DateFields.END_DATE),
				createPositionValueHolderList(0, 2, DateFields.END_DATE)
		);
		List<AccountingPosition> positionList = CollectionUtils.getConvertedFlattened(positionValueHolderList, CompliancePositionValueHolder::getPositionList);
		CompliancePositionValueHolder<AccountingPosition> valueHolder = new CompliancePositionValueHolder<>(new BigDecimal("1.0"), positionList);

		TestUtils.expectException(ValidationException.class, () -> {
			TransformationCommand.ofType(DateFields.END_DATE).min(0).run(Collections.singletonList(valueHolder));
		}, "When using the position date for date comparisons, all positions within each position group must have an equivalent transaction date. Discovered positions with differing dates: ", positionList.get(0).toString(), positionList.get(2).toString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates and returns a list of CompliancePositionValueHolder<AccountingPosition> for testing purposes with the EndDate or LastDeliveryDate of the configured with the
	 * a date derived from the date parameter and dateOffset.
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> createPositionValueHolderList(int dateOffset, int count, DateFields dateType) {
		List<CompliancePositionValueHolder<AccountingPosition>> positionValueList = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			int id = TestUtils.generateId();
			Date endDate = DateUtils.addDays(PROCESS_DATE, dateOffset);
			InvestmentSecurity investmentSecurity = new InvestmentSecurity();
			investmentSecurity.setSymbol("Security " + id);
			if (dateType == DateFields.END_DATE) {
				investmentSecurity.setEndDate(endDate);
			}
			else {
				investmentSecurity.setLastDeliveryDate(endDate);
			}
			List<AccountingPosition> positionList = new ArrayList<>();
			AccountingTransaction openingTransaction = new AccountingTransaction();
			openingTransaction.setInvestmentSecurity(investmentSecurity);
			openingTransaction.setTransactionDate(DateUtils.addDays(PROCESS_DATE, -1 * dateOffset));
			AccountingPosition position = AccountingPosition.forOpeningTransaction(openingTransaction);
			positionList.add(position);
			CompliancePositionValueHolder<AccountingPosition> compliancePositionValueHolder = new CompliancePositionValueHolder<>(new BigDecimal(2 * id), positionList);
			positionValueList.add(compliancePositionValueHolder);
		}

		return positionValueList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class TransformationCommand {

		private Integer minimumDays;
		private Integer maximumDays;
		private boolean excludeWithinRange;
		private boolean useProcessDate;
		private DateFields dateField;


		private TransformationCommand(DateFields dateField) {
			this.dateField = dateField;
		}


		private static TransformationCommand ofType(DateFields dateField) {
			return new TransformationCommand(dateField);
		}


		private TransformationCommand min(Integer minimumDays) {
			this.minimumDays = minimumDays;
			return this;
		}


		private TransformationCommand max(Integer maximumDays) {
			this.maximumDays = maximumDays;
			return this;
		}


		private TransformationCommand excludeWithinRange() {
			this.excludeWithinRange = true;
			return this;
		}


		private TransformationCommand useProcessDate() {
			this.useProcessDate = true;
			return this;
		}


		@SafeVarargs
		private final List<CompliancePositionValueHolder<AccountingPosition>> run(List<CompliancePositionValueHolder<AccountingPosition>>... positionLists) {
			CompliancePositionEndDateFilteringTransformation transformation = new CompliancePositionEndDateFilteringTransformation();
			transformation.setMinimumDays(this.minimumDays);
			transformation.setMaximumDays(this.maximumDays);
			transformation.setExcludeWithinRange(this.excludeWithinRange);
			transformation.setUseProcessDate(this.useProcessDate);
			transformation.setDateField(this.dateField);
			transformation.validate();
			return transformation.apply(CollectionUtils.combineCollections(positionLists), PROCESS_DATE, null, null);
		}
	}
}
