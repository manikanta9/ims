package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import com.clifton.trade.options.combination.TradeOptionCombination;
import com.clifton.trade.options.combination.TradeOptionCombinationHandler;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.combination.TradeOptionLeg;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.clifton.trade.options.combination.TradeOptionCombinationTypes.BULL_PUT_SPREAD;
import static com.clifton.trade.options.combination.TradeOptionCombinationTypes.IRON_CONDOR;
import static com.clifton.trade.options.combination.TradeOptionCombinationTypes.SHORT_CALL;


/**
 * The test class for {@link CompliancePositionOptionCombinationFilteringTransformation}.
 *
 * @author MikeH
 */
public class CompliancePositionOptionCombinationFilteringTransformationTests {

	private static final Date TRANSACTION_DATE = DateUtils.toDate("01/01/2018");
	private static final Date PROCESS_DATE = DateUtils.toDate("01/01/2019");

	// Mocks
	@InjectMocks
	private CompliancePositionOptionCombinationFilteringTransformation transformation;
	@SuppressWarnings("unused")
	@Mock
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	@SuppressWarnings("unused")
	@Mock
	private MarketDataFieldService marketDataFieldService;
	@Mock
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	@Mock
	private TradeService tradeService;
	@Mock
	private TradeOptionCombinationHandler tradeOptionCombinationHandler;

	// Test data
	private boolean ruleConfigured;
	private InvestmentAccount clientAccount;
	private InvestmentSecurity security;
	private MarketDataField field;
	private TradeMarketDataValue tradeMarketDataValue;
	private List<TradeOptionCombination> combinationList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		// Test data
		this.ruleConfigured = false;
		this.clientAccount = TestUtils.generateEntity(InvestmentAccount.class);
		this.security = TestUtils.generateEntity(InvestmentSecurity.class);
		this.security.setUnderlyingSecurity(TestUtils.generateEntity(InvestmentSecurity.class));
		this.security.setEndDate(DateUtils.toDate("01/01/2019"));
		this.security.setOptionType(InvestmentSecurityOptionTypes.CALL);
		this.field = TestUtils.generateEntity(MarketDataField.class, Short.class);
		this.combinationList = new ArrayList<>();
		// Mocked actions
		Mockito.when(this.tradeOptionCombinationHandler.getTradeOptionCombinationList(Mockito.any(), Mockito.any())).thenReturn(this.combinationList);
		Trade trade = TestUtils.generateEntity(Trade.class);
		Mockito.when(this.tradeService.getOpeningTradeForAccountingPosition(Mockito.any())).thenReturn(trade);
		Mockito.when(this.tradeMarketDataFieldService.getTradeMarketDataValueListUnfiltered(Mockito.any())).then(arguments -> Collections.singletonList(this.tradeMarketDataValue));
		Mockito.when(this.complianceRuleReportHandler.createSubDetailForOptionCombination(Mockito.any())).then(arguments -> ComplianceRuleRunSubDetail.create(((TradeOptionCombination) arguments.getArgument(0)).getLabel(), Collections.emptyList()));
	}


	@AfterEach
	public void tearDown() {
		Assertions.assertTrue(this.ruleConfigured, "The rule must be configured for each test.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Combination Type Validation                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testEmptyPositionList() {
		configureRule(true);
		assertTransformationResults(Collections.emptyList(), true);
	}


	@Test
	public void testEmptyPositionListIronCondor() {
		configureRule(true, IRON_CONDOR);
		assertTransformationResults(Collections.emptyList(), true);
	}


	@Test
	public void testSinglePositionListShortCallMatch() {
		configureRule(true, SHORT_CALL);
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(SHORT_CALL)
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListShortCallMatch() {
		configureRule(true, SHORT_CALL);
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL)
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListShortCallMismatch() {
		configureRule(false, SHORT_CALL);
		List<AccountingPosition> mismatchedPositionList = generateMismatchedCombinationPositionList();
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL),
				generateCombinationPositionList(SHORT_CALL),
				mismatchedPositionList
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListIronCondorMatch() {
		configureRule(true, IRON_CONDOR);
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR)
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListIronCondorMismatch() {
		configureRule(false, IRON_CONDOR);
		List<AccountingPosition> mismatchedPositionList = generateMismatchedCombinationPositionList();
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR),
				generateCombinationPositionList(IRON_CONDOR),
				mismatchedPositionList
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListBullPutSpreadMatch() {
		configureRule(true, BULL_PUT_SPREAD);
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD)
		);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testMultiPositionListBullPutSpreadMismatch() {
		configureRule(false, BULL_PUT_SPREAD);
		List<AccountingPosition> mismatchedPositionList = generateMismatchedCombinationPositionList();
		List<AccountingPosition> inputPositionList = flatten(
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD),
				generateCombinationPositionList(BULL_PUT_SPREAD),
				mismatchedPositionList
		);
		assertTransformationResults(inputPositionList, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Downside Potential Validation                   ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDownsidePotentialLimitNoMinimumMatched() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, null, 1.5);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 1.0);
		assertTransformationResults(inputPositionList, false);
	}


	@Test
	public void testDownsidePotentialLimitNoMinimumMismatched() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, null, 1.5);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 2.0);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testDownsidePotentialLimitNoMaximumMatched() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, 1.5, null);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 100.0);
		assertTransformationResults(inputPositionList, false);
	}


	@Test
	public void testDownsidePotentialLimitNoMaximumMismatched() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, 1.5, null);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 1.0);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testDownsidePotentialLimitRangeMatched() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, 0.5, 1.5);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 1.0);
		assertTransformationResults(inputPositionList, false);
	}


	@Test
	public void testDownsidePotentialLimitRangeMismatchedBelow() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, 0.5, 1.5);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 0.4);
		assertTransformationResults(inputPositionList, true);
	}


	@Test
	public void testDownsidePotentialLimitRangeMismatchedAbove() {
		configureRule(false, BULL_PUT_SPREAD);
		configureDownsideLimit(1, 0.5, 1.5);
		List<AccountingPosition> inputPositionList = generateCombinationPositionList(BULL_PUT_SPREAD, 1.6);
		assertTransformationResults(inputPositionList, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingPosition> generateCombinationPositionList(TradeOptionCombinationTypes combinationType) {
		return generateCombinationPositionList(combinationType, null);
	}


	private List<AccountingPosition> generateCombinationPositionList(TradeOptionCombinationTypes combinationType, Double downsidePotential) {
		List<TradeOptionLeg> optionLegList = new ArrayList<>();
		for (int i = 0; i < combinationType.getOptionLegTypes().length; i++) {
			AccountingPosition position = generatePosition();
			optionLegList.add(new TradeOptionLeg(this.security, Collections.singletonList(position)));
		}
		TradeOptionCombination combination = new TradeOptionCombination(combinationType, optionLegList);
		this.combinationList.add(combination);
		if (downsidePotential != null) {
			Mockito.when(this.tradeOptionCombinationHandler.getOptionCombinationDownsidePotential(Mockito.same(combination))).thenReturn(BigDecimal.valueOf(downsidePotential));
		}
		return combination.getOptionLegList().stream()
				.flatMap(leg -> leg.getPositionList().stream())
				.collect(Collectors.toList());
	}


	private List<AccountingPosition> generateMismatchedCombinationPositionList() {
		AccountingPosition position = generatePosition();
		TradeOptionLeg optionLeg = new TradeOptionLeg(this.security, Collections.singletonList(position));
		TradeOptionCombination combination = TradeOptionCombination.unknownCombination(optionLeg);
		this.combinationList.add(combination);
		return Collections.singletonList(position);
	}


	private AccountingPosition generatePosition() {
		AccountingTransaction transaction = TestUtils.generateEntity(AccountingTransaction.class, Long.class);
		transaction.setClientInvestmentAccount(this.clientAccount);
		transaction.setInvestmentSecurity(this.security);
		transaction.setTransactionDate(TRANSACTION_DATE);
		transaction.setQuantity(BigDecimal.TEN);
		return AccountingPosition.forOpeningTransaction(transaction);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	private final <T> List<T> flatten(Collection<T>... collections) {
		return Arrays.stream(collections)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureRule(boolean includeMatches, TradeOptionCombinationTypes... combinationTypes) {
		this.ruleConfigured = true;
		this.transformation.setIncludeMatches(includeMatches);
		this.transformation.setCombinationTypes(combinationTypes);
	}


	private void configureDownsideLimit(int limitValue, Double minValueMultiplier, Double maxValueMultiplier) {
		this.transformation.setDownsidePotentialMarketDataFieldId(this.field.getId());
		this.transformation.setDownsidePotentialLowerBoundMultiplier(minValueMultiplier != null ? BigDecimal.valueOf(minValueMultiplier) : null);
		this.transformation.setDownsidePotentialUpperBoundMultiplier(maxValueMultiplier != null ? BigDecimal.valueOf(maxValueMultiplier) : null);
		this.tradeMarketDataValue = TestUtils.generateEntity(TradeMarketDataValue.class);
		this.tradeMarketDataValue.setReferenceTwo(TestUtils.generateEntity(MarketDataValue.class));
		this.tradeMarketDataValue.getReferenceTwo().setMeasureDate(TRANSACTION_DATE);
		this.tradeMarketDataValue.getReferenceTwo().setMeasureValue(BigDecimal.valueOf(limitValue));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertTransformationResults(List<AccountingPosition> inputPositionList, boolean expectInputsReturned) {
		List<CompliancePositionValueHolder<AccountingPosition>> inputValueHolderList = Collections.singletonList(new CompliancePositionValueHolder<>(BigDecimal.ONE, inputPositionList));
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = this.transformation.apply(inputValueHolderList, PROCESS_DATE, null, new ArrayList<>());
		List<AccountingPosition> resultPositionList = resultValueHolderList.stream()
				.flatMap(valueHolder -> valueHolder.getPositionList().stream())
				.collect(Collectors.toList());
		List<AccountingPosition> expectedPositionList = expectInputsReturned ? inputPositionList : Collections.emptyList();
		Assertions.assertEquals(expectedPositionList, resultPositionList);
	}
}
