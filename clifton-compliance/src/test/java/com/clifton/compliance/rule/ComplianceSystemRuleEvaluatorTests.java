package com.clifton.compliance.rule;

import com.clifton.compliance.rule.evaluator.impl.ComplianceRuleDaysToSecurityMaturity;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Test;


/**
 * These ComplianceSystemRuleEvaluatorTests methods are working as expected.
 *
 * @author stevenf on 9/29/2016.
 */
public class ComplianceSystemRuleEvaluatorTests {


	@Test
	public void testDaysToSecurityMaturityMinMaxRequired() {
		TestUtils.expectException(ValidationException.class, () -> {
			ComplianceRuleDaysToSecurityMaturity daysToSecurityMaturity = new ComplianceRuleDaysToSecurityMaturity();
			daysToSecurityMaturity.setMinDaysToMaturity(null);
			daysToSecurityMaturity.setMaxDaysToMaturity(null);
			daysToSecurityMaturity.validate();
		}, "Min or Max Days to Maturity is Required.");
	}


	@Test
	public void testDaysToSecurityMaturityMaxGreaterThanMin() {
		TestUtils.expectException(ValidationException.class, () -> {
			ComplianceRuleDaysToSecurityMaturity daysToSecurityMaturity = new ComplianceRuleDaysToSecurityMaturity();
			daysToSecurityMaturity.setMinDaysToMaturity(2);
			daysToSecurityMaturity.setMaxDaysToMaturity(1);
			daysToSecurityMaturity.validate();
			//TODO - cause it to fail via configuration/trade data
			//ComplianceRunExecutionCommand command = new ComplianceRunExecutionCommand();
			//runConfig.setBean(CollectionUtils.getFirstElementStrict(this.tradeDAO.findAll()));
			//daysToSecurityMaturity.evaluateRule(command.toRuleEvaluationConfig(this.investmentAccountService, this.investmentInstrumentService));

		}, "Max days must be greater than or Equal to Min days.");
	}
}
