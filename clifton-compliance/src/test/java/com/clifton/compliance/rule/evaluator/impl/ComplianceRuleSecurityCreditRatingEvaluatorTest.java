package com.clifton.compliance.rule.evaluator.impl;

import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.creditrating.ComplianceCreditRating;
import com.clifton.compliance.creditrating.ComplianceCreditRatingService;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;


public class ComplianceRuleSecurityCreditRatingEvaluatorTest {

	private static final String SECURITY_NAME = "Evaluated security";
	private static final String COMPANY_NAME = "Issuer company";

	private final AtomicInteger idGenerator = new AtomicInteger(1);
	private final BusinessCompany creditRatingAgencyFitch = new BusinessCompany();
	private final BusinessCompany creditRatingAgencyMoodys = new BusinessCompany();
	private final BusinessCompany creditRatingAgencyStandardAndPoors = new BusinessCompany();
	private final Map<Integer, Map<BusinessCompany, ComplianceCreditRatingValue>> securityCreditRatingValueMap = new HashMap<>();
	private final Map<Integer, Map<BusinessCompany, ComplianceCreditRatingValue>> issuerCreditRatingValueMap = new HashMap<>();

	private int creditRatingFitchId = getNextId();
	private int creditRatingMoodysId = getNextId();
	private int creditRatingStandardAndPoorsId = getNextId();


	@InjectMocks
	private ComplianceRuleSecurityCreditRatingEvaluator complianceRuleSecurityCreditRatingEvaluator;
	@Mock
	private AccountingPositionService accountingPositionService;
	@Mock
	private ComplianceCreditRatingService complianceCreditRatingService;
	@Mock
	private InvestmentInstrumentService investmentInstrumentService;
	@Mock
	private InvestmentSecurityEventService investmentSecurityEventService;
	@Mock
	private SystemColumnValueHandler systemColumnValueHandler;
	@Mock
	private ComplianceRuleEvaluationConfig complianceRuleEvaluationConfig;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		setupCreditRatingAgencies();
		setupCreditRatingValueList();
	}


	private void setupCreditRatingAgencies() {
		this.creditRatingAgencyFitch.setId(getNextId());
		this.creditRatingAgencyFitch.setName("Fitch");
		this.creditRatingAgencyMoodys.setId(getNextId());
		this.creditRatingAgencyMoodys.setName("Moody's");
		this.creditRatingAgencyStandardAndPoors.setId(getNextId());
		this.creditRatingAgencyStandardAndPoors.setName("Standard and Poor's");
	}


	private void setupCreditRatingValueList() {
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRatingValueForSecuritiesForSecurity(anyInt()))
				.thenAnswer(invocation -> {
					Integer securityId = invocation.getArgument(0);
					return new ArrayList<>(this.securityCreditRatingValueMap.computeIfAbsent(securityId, HashMap::new).values());
				});
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRatingValueForIssuersForIssuer(anyInt()))
				.thenAnswer(invocation -> {
					Integer companyId = invocation.getArgument(0);
					return new ArrayList<>(this.issuerCreditRatingValueMap.computeIfAbsent(companyId, HashMap::new).values());
				});
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Rating Retrieval Logic                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMissingSecurityFail() {
		TestUtils.expectException(RuntimeException.class, () -> {
			evaluateRuleWithFail(generateSecurity(), 0, 0);
		}, "No target was retrieved for credit rating validation. A target is required.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Security Rating Pass Logic                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSingleRatingSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 10, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testSingleRatingFail() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 0, 1);
	}


	@Test
	public void testTwoRatingsSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 12, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 10, 15, CreditRatingAgency.MOODYS, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 100, 20, CreditRatingAgency.STANDARD_AND_POORS, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testTwoRatingsFail() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 12, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 100, 15, CreditRatingAgency.MOODYS, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 100, 20, CreditRatingAgency.STANDARD_AND_POORS, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 1, 2);
	}


	@Test
	public void testThreeRatingsSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 3, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 12, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 10, 15, CreditRatingAgency.MOODYS, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 10, 20, CreditRatingAgency.STANDARD_AND_POORS, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testThreeRatingsFail() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 3, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 10, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 12, 10, CreditRatingAgency.MOODYS, CreditRatingType.SECURITY);
		configureCreditRating(security.getId(), 10, 10, CreditRatingAgency.STANDARD_AND_POORS, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 2, 3);
	}


	@Test
	public void testCashEquivalenceThresholdMaturityIgnoredSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 10);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 11, true);
		configureCreditRating(security.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testCashEquivalenceThresholdMaturitySuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 10);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 10, true);
		configureCreditRating(security.getId(), 10, 10, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testCashEquivalenceThresholdMaturityFailure() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 10);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 10, true);
		configureCreditRating(security.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 0, 1);
	}


	@Test
	public void testCashEquivalenceThresholdCouponIgnoreSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 10);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 11, false);
		configureCreditRating(security.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testCashEquivalenceThresholdCouponSuccess() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 10);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 10, false);
		configureCreditRating(security.getId(), 10, 10, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithPass();
	}


	@Test
	public void testCashEquivalenceThresholdCouponFailure() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 1, 11);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCashEquivalence(security, 10, false);
		configureCreditRating(security.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 0, 1);
	}


	@Test
	public void testPassCountFail() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(security.getId(), 10, 15, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		evaluateRuleWithFail(security, 1, 2);
	}


	@Test
	public void testRequireAllRatingsFail() {
		InvestmentSecurity security = generateSecurity();
		configureCreditRatingEvaluator(true, 0, null);
		configureRuleEvaluationConfig(security, true, new Date());
		ComplianceCreditRating creditRatingFitch = configureCreditRating(security.getId(), 10, 15, CreditRatingAgency.FITCH, CreditRatingType.SECURITY);
		ComplianceCreditRating creditRatingMoodys = configureCreditRating(security.getId(), 10, null, CreditRatingAgency.MOODYS, CreditRatingType.SECURITY);
		evaluateRunWithFailRequireAll(security, Arrays.asList(creditRatingFitch, creditRatingMoodys));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Issuer Credit Rating Traversal             ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIssuerMissingRatingFail() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		configureCreditRatingEvaluator(false, 1, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, null, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		evaluateRuleWithFail(security, 0, 1);
	}


	@Test
	public void testIssuerSingleRatingPass() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		configureCreditRatingEvaluator(false, 1, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		evaluateRuleWithPass();
	}


	@Test
	public void testIssuerSingleRatingFail() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		configureCreditRatingEvaluator(false, 1, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, 9, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		evaluateRuleWithFail(security, 0, 1);
	}


	@Test
	public void testIssuerTwoRatingsPass() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(issuerCompany.getId(), 10, 11, CreditRatingAgency.MOODYS, CreditRatingType.ISSUER);
		evaluateRuleWithPass();
	}


	@Test
	public void testIssuerTwoRatingsFail() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(issuerCompany.getId(), 10, 9, CreditRatingAgency.MOODYS, CreditRatingType.ISSUER);
		evaluateRuleWithFail(security, 1, 2);
	}


	@Test
	public void testIssuerParentTwoRatingsPass() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		BusinessCompany parentIssuerCompany = generateParentIssuer(issuerCompany);
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(parentIssuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(parentIssuerCompany.getId(), 10, 11, CreditRatingAgency.MOODYS, CreditRatingType.ISSUER);
		evaluateRuleWithPass();
	}


	@Test
	public void testIssuerParentTwoRatingsFail() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		BusinessCompany parentIssuerCompany = generateParentIssuer(issuerCompany);
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(parentIssuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(parentIssuerCompany.getId(), 10, 9, CreditRatingAgency.MOODYS, CreditRatingType.ISSUER);
		evaluateRuleWithFail(security, 1, 2);
	}


	@Test
	public void testIssuerParentTwoRatingsWithChildrenFail() {
		InvestmentSecurity security = generateSecurity();
		BusinessCompany issuerCompany = generateIssuer(security);
		BusinessCompany parentIssuerCompany = generateParentIssuer(issuerCompany);
		configureCreditRatingEvaluator(false, 2, null);
		configureRuleEvaluationConfig(security, true, new Date());
		configureCreditRating(issuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(parentIssuerCompany.getId(), 10, 11, CreditRatingAgency.FITCH, CreditRatingType.ISSUER);
		configureCreditRating(parentIssuerCompany.getId(), 10, 11, CreditRatingAgency.MOODYS, CreditRatingType.ISSUER);
		evaluateRuleWithFail(security, 1, 2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void evaluateRuleWithPass() {
		ComplianceRuleRun ruleRun = this.complianceRuleSecurityCreditRatingEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);
		Assertions.assertEquals((short) ruleRun.getStatus(), ComplianceRuleRun.STATUS_PASSED, "The rule evaluation failed. The evaluation was expected to succeed.");
	}


	private void evaluateRuleWithFail(InvestmentSecurity security, int passedCount, int requiredCount) {
		ComplianceRuleRun ruleRun = this.complianceRuleSecurityCreditRatingEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);
		Assertions.assertEquals(String.format("%s - The security passed [%d] of [%d] required credit rating minimums.", security.getName(), passedCount, requiredCount), ruleRun.getChildren().get(0).getDescription());
	}


	private void evaluateRunWithFailRequireAll(InvestmentSecurity security, List<ComplianceCreditRating> requiredRatings) {
		ComplianceRuleRun ruleRun = this.complianceRuleSecurityCreditRatingEvaluator.evaluateRule(this.complianceRuleEvaluationConfig);
		Assertions.assertEquals(String.format("%s - The security must contain ratings from all of these agencies: %s", security.getName(), CollectionUtils.getConverted(requiredRatings, ComplianceCreditRating::getLabel)), ruleRun.getChildren().get(0).getDescription());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureCreditRatingEvaluator(boolean requireAllRatings, int minPassCount, Integer cashEquivalentThreshold) {
		this.complianceRuleSecurityCreditRatingEvaluator.setRequireAllRatings(requireAllRatings);
		if (cashEquivalentThreshold != null) {
			this.complianceRuleSecurityCreditRatingEvaluator.setCashEquivalent(true);
			this.complianceRuleSecurityCreditRatingEvaluator.setCashEquivalentThreshold(cashEquivalentThreshold);
		}
		this.complianceRuleSecurityCreditRatingEvaluator.setCount(minPassCount);
	}


	private void configureRuleEvaluationConfig(InvestmentSecurity security, boolean realTime, Date processDate) {
		Mockito.when(this.complianceRuleEvaluationConfig.getInvestmentSecurityId()).thenReturn(security.getId());
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(eq(security.getId()))).thenReturn(security);
		Mockito.when(this.complianceRuleEvaluationConfig.getProcessDate()).thenReturn(processDate);
		Mockito.when(this.complianceRuleEvaluationConfig.isRealTime()).thenReturn(realTime);
	}


	private void configureCashEquivalence(InvestmentSecurity security, int daysToPayment, boolean useMaturityDate) {
		// Select appropriate cash equivalence criteria type: Cash-equivalent at next coupon payment or cash-equivalent at maturity
		Mockito.when(this.systemColumnValueHandler.getSystemColumnValueForEntity(any(InvestmentSecurity.class), eq("Security Custom Fields"), eq("Coupon Type"), anyBoolean()))
				.thenReturn(useMaturityDate ? null : "Floating");
		Date paymentDate = DateUtils.addDays(this.complianceRuleEvaluationConfig.getProcessDate(), daysToPayment);
		if (useMaturityDate) {
			// Use maturity date
			security.setEndDate(paymentDate);
		}
		else {
			// Use coupon payment date
			Mockito.when(this.investmentSecurityEventService.getInvestmentSecurityEventList(any())).thenAnswer(invocation -> {
				InvestmentSecurityEvent daysToMaturityEvent = new InvestmentSecurityEvent();
				daysToMaturityEvent.setEventDate(paymentDate);
				return Collections.singletonList(daysToMaturityEvent);
			});
		}
	}


	private ComplianceCreditRating configureCreditRating(Integer assigneeId, Integer minimumRatingWeight, Integer actualRatingWeight, CreditRatingAgency ratingAgency, CreditRatingType ratingType) {
		// Determine agency for configuration
		final int compareCreditRatingId;
		final BusinessCompany ratingAgencyCompany;
		final Consumer<Integer> evaluatorCreditRatingIdSetter;
		switch (ratingAgency) {
			case MOODYS:
				compareCreditRatingId = this.creditRatingMoodysId;
				ratingAgencyCompany = this.creditRatingAgencyMoodys;
				evaluatorCreditRatingIdSetter = this.complianceRuleSecurityCreditRatingEvaluator::setCreditRatingMoodysId;
				break;
			case FITCH:
				compareCreditRatingId = this.creditRatingFitchId;
				ratingAgencyCompany = this.creditRatingAgencyFitch;
				evaluatorCreditRatingIdSetter = this.complianceRuleSecurityCreditRatingEvaluator::setCreditRatingFitchId;
				break;
			case STANDARD_AND_POORS:
				compareCreditRatingId = this.creditRatingStandardAndPoorsId;
				ratingAgencyCompany = this.creditRatingAgencyStandardAndPoors;
				evaluatorCreditRatingIdSetter = this.complianceRuleSecurityCreditRatingEvaluator::setCreditRatingStandardAndPoorsId;
				break;
			default:
				throw new IllegalStateException("No rating agency found for type [" + ratingAgency + "].");
		}

		// Determine credit rating type to create
		final Map<BusinessCompany, ComplianceCreditRatingValue> assigneeCreditRatingMap;
		switch (ratingType) {
			case SECURITY:
				assigneeCreditRatingMap = this.securityCreditRatingValueMap.computeIfAbsent(assigneeId, HashMap::new);
				break;
			case ISSUER:
				assigneeCreditRatingMap = this.issuerCreditRatingValueMap.computeIfAbsent(assigneeId, HashMap::new);
				break;
			default:
				throw new IllegalStateException("Rating type [" + ratingType + "] is not recognized.");
		}

		// Reset credit rating
		evaluatorCreditRatingIdSetter.accept(null);
		Mockito.when(this.complianceCreditRatingService.getComplianceCreditRating(eq(compareCreditRatingId))).thenReturn(null);
		assigneeCreditRatingMap.remove(ratingAgencyCompany);

		// Set minimum credit rating
		ComplianceCreditRating creditRating = null;
		if (minimumRatingWeight != null) {
			creditRating = generateCreditRating(compareCreditRatingId, minimumRatingWeight, ratingAgencyCompany);
			evaluatorCreditRatingIdSetter.accept(creditRating.getId());
			Mockito.when(this.complianceCreditRatingService.getComplianceCreditRating(eq(creditRating.getId()))).thenReturn(creditRating);
		}

		// Set existing credit rating
		if (actualRatingWeight != null) {
			ComplianceCreditRatingValueForSecurity creditRatingValue = generateCreditRatingValue(actualRatingWeight, ratingAgencyCompany);
			assigneeCreditRatingMap.put(ratingAgencyCompany, creditRatingValue);
		}
		return creditRating;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getNextId() {
		return this.idGenerator.getAndIncrement();
	}


	private InvestmentSecurity generateSecurity() {
		InvestmentSecurity security = new InvestmentSecurity();
		security.setId(getNextId());
		security.setName(SECURITY_NAME);
		return security;
	}


	private BusinessCompany generateIssuer(InvestmentSecurity security) {
		BusinessCompany company = new BusinessCompany();
		company.setId(getNextId());
		company.setName(COMPANY_NAME);
		security.setBusinessCompany(company);
		return company;
	}


	private BusinessCompany generateParentIssuer(BusinessCompany company) {
		BusinessCompany parentCompany = new BusinessCompany();
		parentCompany.setId(getNextId());
		parentCompany.setName(COMPANY_NAME);
		company.setParent(parentCompany);
		return parentCompany;
	}


	private ComplianceCreditRating generateCreditRating(Integer id, int ratingWeight, BusinessCompany ratingAgency) {
		ComplianceCreditRating creditRating = new ComplianceCreditRating();
		creditRating.setId(id != null ? id : getNextId());
		creditRating.setName(String.format("Credit rating for [%s]", ratingAgency.getLabel()));
		creditRating.setRatingWeight(ratingWeight);
		creditRating.setRatingAgency(ratingAgency);
		creditRating.setShortTerm(true);
		return creditRating;
	}


	private ComplianceCreditRatingValueForSecurity generateCreditRatingValue(int ratingWeight, BusinessCompany ratingAgency) {
		ComplianceCreditRatingValueForSecurity creditRatingValue = new ComplianceCreditRatingValueForSecurity();
		creditRatingValue.setCreditRating(generateCreditRating(null, ratingWeight, ratingAgency));
		creditRatingValue.setStartDate(new Date(0L));
		return creditRatingValue;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private enum CreditRatingType {
		ISSUER,
		SECURITY
	}


	private enum CreditRatingAgency {
		MOODYS,
		FITCH,
		STANDARD_AND_POORS
	}
}
