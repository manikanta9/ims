package com.clifton.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.run.rule.report.ComplianceRuleReportHandler;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.ExpiryTimeValues;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.options.combination.TradeOptionCombinationHandler;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.combination.TradeOptionLegTypes;
import com.clifton.trade.options.securitytarget.group.util.TradeGroupTradeCycleUtilHandler;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * A suite of unit tests to verify the CompliancePositionSecurityTargetFilteringTransformation class.
 *
 * @author davidi
 */
@ContextConfiguration
@Transactional
public class CompliancePositionSecurityTargetFilteringTransformationTests extends BaseInMemoryDatabaseTests {

	private static final String PROCESSING_TYPE_NAME_CALL_SELLING = "Call Selling";
	private static final String PROCESSING_TYPE_NAME_DHE = "DHE";
	private static final String PROCESSING_TYPE_NAME_OARS = "OARS";
	private static final String PROCESSING_TYPE_NAME_RPS = "RPS";

	@InjectMocks
	private CompliancePositionSecurityTargetFilteringTransformation transformation;
	@Mock
	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;
	@Mock
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;
	@Mock
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	@Mock
	private TradeService tradeService;

	@Resource
	private ComplianceRuleReportHandler complianceRuleReportHandler;
	@Resource
	private TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler;
	@InjectMocks
	@Resource
	private TradeOptionCombinationHandler tradeOptionCombinationHandler;
	@Resource
	private AdvancedReadOnlyDAO<BusinessServiceProcessingType, Criteria> businessServiceProcessingTypeDAO;

	private InvestmentAccount clientAccount = TestUtils.generateEntity(InvestmentAccount.class);
	private InvestmentSecurity underlyingSecurity1 = TestUtils.generateEntity(InvestmentSecurity.class);
	private InvestmentSecurity underlyingSecurity2 = TestUtils.generateEntity(InvestmentSecurity.class);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.transformation.setComplianceRuleReportHandler(this.complianceRuleReportHandler);
		this.transformation.setTradeGroupTradeCycleUtilHandler(this.tradeGroupTradeCycleUtilHandler);
		this.transformation.setTradeOptionCombinationHandler(this.tradeOptionCombinationHandler);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testApply_fullSpread_Options_no_violations() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 3, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 3, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 3, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 4, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		Assertions.assertEquals(0, resultValueHolderList.size());
	}


	@Test
	public void testApply_fullSpread_Options_no_violations_multiple_fills() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 5_000_000, 5000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 2, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 3, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 3, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createPositionList(this.underlyingSecurity1, "11/23/2018", 7, Arrays.asList(TradeOptionLegTypes.LONG_PUT, TradeOptionLegTypes.SHORT_PUT, TradeOptionLegTypes.SHORT_CALL), 700, 850, 1100),
				createMultiFillPositionList(this.underlyingSecurity1, "11/23/2018", TradeOptionLegTypes.LONG_CALL, 1200, 4, 2, 1)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_fullSpread_Options_notional_violation_put_spread() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 400_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 3, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 700, 850),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 3, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 700, 850),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 3, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 700, 850),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 4, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 700, 850)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Notional violation in put spread found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_fullSpread_Options_no_violations_10_percent_risk() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 10);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 1300);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 5, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// No SecurityTarget violations, returned list should be empty
		Assertions.assertEquals(0, resultValueHolderList.size());
	}


	@Test
	public void testApply_fullSpread_Options_notional_violation_10_percent_risk() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 10);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 7, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 6, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Notional violation in put spread found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_fullSpread_Options_notional_violation_call_spread() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 400_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 980, 990, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 980, 990, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 980, 990, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 980, 990, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Notional violation in call spread found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_putSpread_Options_no_violations() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_RPS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// No SecurityTarget violations, returned list should be empty
		Assertions.assertEquals(0, resultValueHolderList.size());
	}


	@Test
	public void testApply_putSpread_Options_notional_violation() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_RPS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 400_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 1140, 1280)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Notional violation in put spread found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_single_Options_no_violations() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_CALL_SELLING);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// No SecurityTarget violations, returned list should be empty
		Assertions.assertEquals(0, resultValueHolderList.size());
	}


	@Test
	public void testApply_single_Options_quantity_violation() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_CALL_SELLING);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 390);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.SHORT_CALL, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Quantity violation found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_shortCallLongPut_Options_no_violations() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_DHE);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// No SecurityTarget violations, returned list should be empty
		Assertions.assertEquals(0, resultValueHolderList.size());
	}


	@Test
	public void testApply_shortCallLongPut_Options_quantity_violation() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_DHE);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 390);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 2, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.COLLAR, 950, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		// Quantity violation found, expect group to be returned
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_no_security_target_exception() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_multiple_security_targets_exception() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity2, 1000, 4_000_000, 4000);
		CompliancePositionValueHolder<AccountingPosition> conflictingValueHolder = createValueHolder(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity2, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(CollectionUtils.combineCollections(valueHolderList, Collections.singletonList(conflictingValueHolder)));
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_security_target_does_not_match_underlying_exception() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity2, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	@Test
	public void testApply_differing_option_spread_structures_exception() {
		setBusinessServiceProcessingType(PROCESSING_TYPE_NAME_OARS);
		setClientValueAtRiskPercent(this.clientAccount, 5);
		createTradeSecurityTarget(this.clientAccount, this.underlyingSecurity1, 1000, 4_000_000, 4000);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList = createValueHolderList(
				createCombinationPositionList(this.underlyingSecurity1, "11/02/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/09/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/16/2018", 1, TradeOptionCombinationTypes.IRON_CONDOR, 700, 850, 1100, 1200),
				createCombinationPositionList(this.underlyingSecurity1, "11/23/2018", 1, TradeOptionCombinationTypes.BEAR_CALL_SPREAD, 1100, 1200)
		);
		List<CompliancePositionValueHolder<AccountingPosition>> resultValueHolderList = evaluateTransformation(valueHolderList);
		Assertions.assertEquals(1, resultValueHolderList.size());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Configuration Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	private void setBusinessServiceProcessingType(String processingTypeName) {
		BusinessServiceProcessingType businessServiceProcessingType = this.businessServiceProcessingTypeDAO.findOneByField("name", processingTypeName);
		this.clientAccount.setServiceProcessingType(businessServiceProcessingType);
	}


	private void setClientValueAtRiskPercent(InvestmentAccount clientAccount, int valueAtRiskPercent) {
		Mockito.when(this.investmentAccountUtilHandler.getClientInvestmentAccountPortfolioSetupCustomColumnValue(ArgumentMatchers.eq(clientAccount), ArgumentMatchers.eq(InvestmentAccountUtilHandler.VALUE_AT_RISK_PERCENT_COLUMN_NAME))).thenReturn(BigDecimal.valueOf(valueAtRiskPercent));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Data Generation Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPosition createAccountingPosition(InvestmentAccount clientAccount, InvestmentSecurity underlyingSecurity, int quantity, int strikePrice, String expirationDate, boolean isPutOption) {
		// Create security
		InvestmentSecurity option = TestUtils.generateEntity(InvestmentSecurity.class);
		String prefix = isPutOption ? "P" : "C";
		// Symbol will encode information useful for mocking custom properties, etc.
		String symbol = prefix + " " + expirationDate + " " + strikePrice;
		option.setSettlementExpiryTime(ExpiryTimeValues.PM);
		option.setOptionType(isPutOption ? InvestmentSecurityOptionTypes.PUT : InvestmentSecurityOptionTypes.CALL);
		option.setOptionStyle(OptionStyleTypes.AMERICAN);
		option.setOptionStrikePrice(new BigDecimal(strikePrice));
		option.setSymbol(symbol);
		option.setEndDate(DateUtils.toDate(expirationDate));
		option.setUnderlyingSecurity(underlyingSecurity);
		option.setInstrument(TestUtils.generateEntity(InvestmentInstrument.class));
		option.getInstrument().setPriceMultiplier(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		option.getInstrument().setHierarchy(TestUtils.generateEntity(InvestmentInstrumentHierarchy.class, Short.class));
		option.getInstrument().getHierarchy().setInvestmentType(TestUtils.generateEntity(InvestmentType.class, Short.class));
		option.getInstrument().getHierarchy().getInvestmentType().setName(InvestmentType.OPTIONS);

		// Create trade fill
		Trade trade = TestUtils.generateEntity(Trade.class);
		TradeFill tradeFill = TestUtils.generateEntity(TradeFill.class);
		tradeFill.setTrade(trade);

		// Create position
		SystemTable tradeFillTable = TestUtils.generateEntity(SystemTable.class, Short.class);
		tradeFillTable.setName(TradeFill.TABLE_NAME);
		AccountingTransaction openingTransaction = TestUtils.generateEntity(AccountingTransaction.class, Long.class);
		openingTransaction.setClientInvestmentAccount(clientAccount);
		openingTransaction.setFkFieldId(tradeFill.getId());
		openingTransaction.setSystemTable(tradeFillTable);
		openingTransaction.setInvestmentSecurity(option);
		openingTransaction.setQuantity(BigDecimal.valueOf(quantity));
		AccountingPosition position = AccountingPosition.forOpeningTransaction(openingTransaction);
		Mockito.when(this.tradeService.getTradeFillForAccountingPosition(ArgumentMatchers.eq(position))).thenReturn(tradeFill);
		return position;
	}


	private List<AccountingPosition> createPositionList(InvestmentSecurity underlyingSecurity, String expirationDate, int quantity, List<TradeOptionLegTypes> legTypeList, int... strikePrices) {
		Assertions.assertEquals(legTypeList.size(), strikePrices.length);
		List<AccountingPosition> positionList = new ArrayList<>();
		for (int i = 0; i < strikePrices.length; i++) {
			TradeOptionLegTypes legType = legTypeList.get(i);
			positionList.add(createAccountingPosition(this.clientAccount, underlyingSecurity, legType.isLong() ? quantity : (-1 * quantity), strikePrices[i], expirationDate, legType.isPut()));
		}
		return positionList;
	}


	private List<AccountingPosition> createCombinationPositionList(InvestmentSecurity underlyingSecurity, String expirationDate, int quantity, TradeOptionCombinationTypes combinationType, int... strikePrices) {
		return createPositionList(underlyingSecurity, expirationDate, quantity, Arrays.asList(combinationType.getOptionLegTypes()), strikePrices);
	}


	private List<AccountingPosition> createMultiFillPositionList(InvestmentSecurity underlyingSecurity, String expirationDate, TradeOptionLegTypes legType, int strikePrice, int... quantities) {
		return Arrays.stream(quantities)
				.mapToObj(quantity -> createPositionList(underlyingSecurity, expirationDate, quantity, Collections.singletonList(legType), strikePrice))
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}


	private InvestmentAccountSecurityTarget createInvestmentAccountSecurityTarget(InvestmentAccount clientAccount, InvestmentSecurity underlyingSecurity, int notionalLimit, int quantityLimit) {
		InvestmentAccountSecurityTarget investmentAccountSecurityTarget = TestUtils.generateEntity(InvestmentAccountSecurityTarget.class);
		investmentAccountSecurityTarget.setTrancheCount((short) 12);
		investmentAccountSecurityTarget.setCycleDurationWeeks((short) 4);
		investmentAccountSecurityTarget.setClientInvestmentAccount(clientAccount);
		investmentAccountSecurityTarget.setTargetUnderlyingSecurity(underlyingSecurity);
		investmentAccountSecurityTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(notionalLimit));
		investmentAccountSecurityTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(quantityLimit));
		Mockito.when(this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(ArgumentMatchers.eq(clientAccount.getId()), ArgumentMatchers.eq(underlyingSecurity.getId()), ArgumentMatchers.any(Date.class))).thenReturn(investmentAccountSecurityTarget);
		return investmentAccountSecurityTarget;
	}


	private TradeSecurityTarget createTradeSecurityTarget(InvestmentAccount clientAccount, InvestmentSecurity underlyingSecurity, int underlyingPrice, int notionalLimit, int quantityLimit) {
		InvestmentAccountSecurityTarget investmentAccountSecurityTarget = createInvestmentAccountSecurityTarget(clientAccount, underlyingSecurity, notionalLimit, quantityLimit);
		TradeSecurityTarget tradeSecurityTarget = new TradeSecurityTarget(investmentAccountSecurityTarget);
		tradeSecurityTarget.setUnderlyingSecurityPrice(BigDecimal.valueOf(underlyingPrice));
		tradeSecurityTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(notionalLimit));
		tradeSecurityTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(quantityLimit));
		Mockito.when(this.tradeSecurityTargetUtilHandler.getTradeSecurityTargetForDate(ArgumentMatchers.eq(tradeSecurityTarget.getSecurityTarget()), ArgumentMatchers.any(Date.class))).thenReturn(tradeSecurityTarget);
		return tradeSecurityTarget;
	}


	@SafeVarargs
	private final CompliancePositionValueHolder<AccountingPosition> createValueHolder(List<AccountingPosition>... positionLists) {
		return new CompliancePositionValueHolder<>(BigDecimal.ZERO, CollectionUtils.combineCollections(positionLists));
	}


	@SafeVarargs
	private final List<CompliancePositionValueHolder<AccountingPosition>> createValueHolderList(List<AccountingPosition>... positionLists) {
		return Collections.singletonList(createValueHolder(positionLists));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<CompliancePositionValueHolder<AccountingPosition>> evaluateTransformation(List<CompliancePositionValueHolder<AccountingPosition>> valueHolderList) {
		return this.transformation.apply(valueHolderList, DateUtils.toDate("10/29/2018"), null, new ArrayList<>());
	}
}
