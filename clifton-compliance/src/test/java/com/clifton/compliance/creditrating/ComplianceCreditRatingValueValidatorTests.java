package com.clifton.compliance.creditrating;

import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.creditrating.type.ComplianceCreditRatingType;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForIssuer;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceCreditRatingValueValidatorTests {

	@Resource
	private AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateStartDate() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			ComplianceCreditRatingValueForSecurity complianceCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
			complianceCreditRatingValue.setStartDate(DateUtils.toDate("01/01/2011"));
			complianceCreditRatingValue.setEndDate(DateUtils.toDate("01/01/2010"));
			complianceCreditRatingValue.setSecurity(new InvestmentSecurity());
			complianceCreditRatingValue.getSecurity().setId(1);
			getComplianceCreditRatingValueDAO().save(complianceCreditRatingValue);
		}, "The End Date must be later than the Start Date.");
	}


	@Test
	public void testValidateSecurityType() {
		ComplianceCreditRatingType complianceCreditRatingType = new ComplianceCreditRatingType();
		complianceCreditRatingType.setName("Security type");
		complianceCreditRatingType.setSecurityRating(true);
		ComplianceCreditRatingValueForIssuer complianceCreditRatingValue = new ComplianceCreditRatingValueForIssuer();
		complianceCreditRatingValue.setType(complianceCreditRatingType);
		complianceCreditRatingValue.setIssuerCompany(new BusinessCompany());
		complianceCreditRatingValue.getIssuerCompany().setId(1);

		TestUtils.expectException(ValidationException.class, () -> {
			getComplianceCreditRatingValueDAO().save(complianceCreditRatingValue);
		}, String.format("Credit rating values of type [%s] must correspond to securities.", complianceCreditRatingValue.getType().getName()));
	}


	@Test
	public void testValidateNonSecurityType() {
		ComplianceCreditRatingType complianceCreditRatingType = new ComplianceCreditRatingType();
		complianceCreditRatingType.setName("Non-security type");
		complianceCreditRatingType.setSecurityRating(false);
		ComplianceCreditRatingValueForSecurity complianceCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		complianceCreditRatingValue.setType(complianceCreditRatingType);
		complianceCreditRatingValue.setSecurity(new InvestmentSecurity());
		complianceCreditRatingValue.getSecurity().setId(1);
		TestUtils.expectException(ValidationException.class, () -> {

			getComplianceCreditRatingValueDAO().save(complianceCreditRatingValue);
		}, String.format("Credit rating values of type [%s] must correspond to issuer companies.", complianceCreditRatingValue.getType().getName()));
	}


	@Test
	public void testValidateValid() {
		ComplianceCreditRatingType complianceCreditRatingType = new ComplianceCreditRatingType();
		complianceCreditRatingType.setName("Security type");
		complianceCreditRatingType.setSecurityRating(true);
		ComplianceCreditRatingValueForSecurity complianceCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		complianceCreditRatingValue.setType(complianceCreditRatingType);
		complianceCreditRatingValue.setStartDate(DateUtils.toDate("01/01/2011"));
		complianceCreditRatingValue.setEndDate(DateUtils.toDate("01/01/2012"));
		complianceCreditRatingValue.setSecurity(new InvestmentSecurity());
		complianceCreditRatingValue.getSecurity().setId(1);
		getComplianceCreditRatingValueDAO().save(complianceCreditRatingValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> getComplianceCreditRatingValueDAO() {
		return this.complianceCreditRatingValueDAO;
	}


	public void setComplianceCreditRatingValueDAO(AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO) {
		this.complianceCreditRatingValueDAO = complianceCreditRatingValueDAO;
	}
}
