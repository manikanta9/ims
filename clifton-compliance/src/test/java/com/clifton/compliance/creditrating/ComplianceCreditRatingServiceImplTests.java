package com.clifton.compliance.creditrating;

import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValue;
import com.clifton.compliance.creditrating.value.ComplianceCreditRatingValueForSecurity;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ComplianceCreditRatingServiceImplTests {

	@Resource
	private AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Test behavior when a new credit rating value overlaps the dates of an existing credit rating value.
	 */
	@Test
	public void testSaveCreditRatingValueOverlapping() {
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue1 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(1);
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue2 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(2);
		ComplianceCreditRatingValueForSecurity dateOverlappingCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		dateOverlappingCreditRatingValue.setSecurity(existingCreditRatingValue1.getSecurity());
		dateOverlappingCreditRatingValue.setStartDate(existingCreditRatingValue1.getStartDate());
		dateOverlappingCreditRatingValue.setCreditRating(existingCreditRatingValue1.getCreditRating());
		dateOverlappingCreditRatingValue.setType(existingCreditRatingValue1.getType());

		TestUtils.expectException(ValidationException.class, () -> {
			getComplianceCreditRatingServiceWithMocks(existingCreditRatingValue1, existingCreditRatingValue2).saveComplianceCreditRatingValueForSecurity(dateOverlappingCreditRatingValue);
		}, String.format("Start and end dates cannot overlap with those of the given existing rating: [%s].", existingCreditRatingValue1));
	}


	/**
	 * Test behavior when an assignee has multiple credit rating values under the same type/issuer combination that have not yet ended.
	 */
	@Test
	public void testSaveCreditRatingValueConflictingEndDates() {
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue1 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(3);
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue2 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(4);
		ComplianceCreditRatingValueForSecurity conflictingEndDateCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		conflictingEndDateCreditRatingValue.setSecurity(existingCreditRatingValue1.getSecurity());
		conflictingEndDateCreditRatingValue.setStartDate(existingCreditRatingValue1.getStartDate());
		conflictingEndDateCreditRatingValue.setCreditRating(existingCreditRatingValue1.getCreditRating());
		conflictingEndDateCreditRatingValue.setType(existingCreditRatingValue1.getType());
		TestUtils.expectException(ValidationException.class, () -> {
			getComplianceCreditRatingServiceWithMocks(existingCreditRatingValue1, existingCreditRatingValue2).saveComplianceCreditRatingValueForSecurity(conflictingEndDateCreditRatingValue);
		}, String.format("Data integrity error: Multiple conflicting ratings with no End Date were found: %s.",
				CollectionUtils.toString(Arrays.asList(existingCreditRatingValue1, existingCreditRatingValue2), 5)));
	}


	/**
	 * Test behavior when the start date for a new credit rating value is earlier then the start date of the current value (which would normally be
	 * automatically ended).
	 */
	@Test
	public void testSaveCreditRatingValueStartDateEarlierThanExistingRating() {
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue1 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(1);
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue2 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(2);
		ComplianceCreditRatingValueForSecurity startDateEarlierThankExistingRatingCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		startDateEarlierThankExistingRatingCreditRatingValue.setSecurity(existingCreditRatingValue1.getSecurity());
		startDateEarlierThankExistingRatingCreditRatingValue.setStartDate(DateUtils.addDays(existingCreditRatingValue1.getStartDate(), -2));
		startDateEarlierThankExistingRatingCreditRatingValue.setEndDate(DateUtils.addDays(existingCreditRatingValue1.getStartDate(), -1));
		startDateEarlierThankExistingRatingCreditRatingValue.setCreditRating(existingCreditRatingValue1.getCreditRating());
		startDateEarlierThankExistingRatingCreditRatingValue.setType(existingCreditRatingValue1.getType());
		TestUtils.expectException(ValidationException.class, () -> {
			getComplianceCreditRatingServiceWithMocks(existingCreditRatingValue1, existingCreditRatingValue2).saveComplianceCreditRatingValueForSecurity(startDateEarlierThankExistingRatingCreditRatingValue);
		}, String.format("Unable to automatically end existing rating [%s]: Invalid End Date [%s]. The Start Date of the rating to be saved must be later than the Start Date of the existing rating ([%s]).",
				existingCreditRatingValue2,
				DateUtils.fromDateShort(DateUtils.addDays(startDateEarlierThankExistingRatingCreditRatingValue.getStartDate(), -1)),
				DateUtils.fromDateShort(existingCreditRatingValue2.getStartDate())));
	}


	/**
	 * Test behavior when saving a credit rating value that is equal to the current value.
	 * <p>
	 * Currently, duplicate consecutive credit rating values are allowed. In the future, we may wish to consolidate these on save.
	 */
	@Test
	public void testSaveCreditRatingValueRatingsEqual() {
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue1 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(1);
		ComplianceCreditRatingValueForSecurity existingCreditRatingValue2 = (ComplianceCreditRatingValueForSecurity) getComplianceCreditRatingValueDAO().findByPrimaryKey(2);
		ComplianceCreditRatingValueForSecurity equalCreditRatingValue = new ComplianceCreditRatingValueForSecurity();
		equalCreditRatingValue.setSecurity(existingCreditRatingValue2.getSecurity());
		Date startDate = DateUtils.addDays(existingCreditRatingValue2.getStartDate(), 5);
		equalCreditRatingValue.setStartDate(startDate);
		equalCreditRatingValue.setCreditRating(existingCreditRatingValue2.getCreditRating());
		equalCreditRatingValue.setType(existingCreditRatingValue2.getType());
		ComplianceCreditRatingServiceImpl complianceCreditRatingService = getComplianceCreditRatingServiceWithMocks(existingCreditRatingValue1, existingCreditRatingValue2);
		complianceCreditRatingService.saveComplianceCreditRatingValueForSecurity(equalCreditRatingValue);
		Mockito.verify(complianceCreditRatingService.getComplianceCreditRatingValueDAO(), Mockito.times(1)).save(ArgumentMatchers.any());
		Mockito.verify(complianceCreditRatingService.getComplianceCreditRatingValueDAO(), Mockito.times(1)).save(existingCreditRatingValue2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComplianceCreditRatingServiceImpl getComplianceCreditRatingServiceWithMocks(ComplianceCreditRatingValue... existingCreditRatingValues) {
		ComplianceCreditRatingServiceImpl complianceCreditRatingService = new ComplianceCreditRatingServiceImpl();
		@SuppressWarnings("unchecked")
		AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> mockComplianceCreditRatingValueDAO = Mockito.mock(AdvancedUpdatableDAO.class);
		Mockito.when(mockComplianceCreditRatingValueDAO.findBySearchCriteria(ArgumentMatchers.any())).thenReturn(Arrays.asList(existingCreditRatingValues));
		complianceCreditRatingService.setComplianceCreditRatingValueDAO(mockComplianceCreditRatingValueDAO);
		return complianceCreditRatingService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> getComplianceCreditRatingValueDAO() {
		return this.complianceCreditRatingValueDAO;
	}


	public void setComplianceCreditRatingValueDAO(AdvancedUpdatableDAO<ComplianceCreditRatingValue, Criteria> complianceCreditRatingValueDAO) {
		this.complianceCreditRatingValueDAO = complianceCreditRatingValueDAO;
	}
}
