package com.clifton.compliance.builder;


import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;


public class ComplianceRuleBuilder {

	private ComplianceRuleOld complianceRule;


	private ComplianceRuleBuilder(ComplianceRuleOld complianceRule) {
		this.complianceRule = complianceRule;
	}


	public static ComplianceRuleBuilder createComplianceRule() {
		return createComplianceRule(42);
	}


	public static ComplianceRuleBuilder createComplianceRule(int id) {
		ComplianceRuleOld innerComplianceRule = new ComplianceRuleOld();
		innerComplianceRule.setId(id);
		return new ComplianceRuleBuilder(innerComplianceRule);
	}


	public ComplianceRuleOld toComplianceRule() {
		return this.complianceRule;
	}


	public ComplianceRuleBuilder forClientAccount(String clientName, Integer clientID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setName(clientName);
		investAcc.setBaseCurrency(baseCurrency);
		investAcc.setId(clientID);
		investAcc.setNumber(accountNumber);
		getComplianceRule().setClientInvestmentAccount(investAcc);
		getComplianceRule().setHoldingInvestmentAccount(investAcc);
		return this;
	}


	public ComplianceRuleBuilder forHoldingAccount(String clientName, Integer clientID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setName(clientName);
		investAcc.setBaseCurrency(baseCurrency);
		investAcc.setId(clientID);
		investAcc.setNumber(accountNumber);
		getComplianceRule().setHoldingInvestmentAccount(investAcc);
		return this;
	}


	public ComplianceRuleBuilder setShortAllowed(boolean shortAllowed) {
		getComplianceRule().setShortPositionAllowed(shortAllowed);
		return this;
	}


	public ComplianceRuleBuilder setLongAllowed(boolean longAllowed) {
		getComplianceRule().setLongPositionAllowed(longAllowed);
		return this;
	}


	public ComplianceRuleOld getComplianceRule() {
		return this.complianceRule;
	}


	public void setComplianceRule(ComplianceRuleOld complianceRule) {
		this.complianceRule = complianceRule;
	}
}
