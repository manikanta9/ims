Clifton.otc.swap.TotalReturnSwapTradesFormGrid = {
	xtype: 'formgrid',
	storeRoot: 'tradeList',
	detailPageClass: 'Clifton.trade.TradeWindow',
	title: 'No trades found during selected reset period',
	bindToFormLoad: false,
	readOnly: true,
	collapsible: true,
	collapsed: true,
	anchor: '-30',
	columnsConfig: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
		{header: 'Workflow State', width: 120, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{
			header: 'Buy/Sell', width: 80, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Quantity', width: 100, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Notional Multiplier', width: 100, dataIndex: 'notionalMultiplier', type: 'float', useNull: true, hidden: true},
		{header: 'Price Multiplier', width: 80, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', hidden: true},
		{header: 'Exchange Rate', width: 100, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Price', width: 100, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Accounting Notional', width: 130, dataIndex: 'accountingNotional', type: 'currency', useNull: true},
		{header: 'Accrual 1', width: 80, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 80, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 80, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Commission Amount', width: 120, dataIndex: 'commissionAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Fee', width: 80, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Traded On', width: 90, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Settled On', width: 90, dataIndex: 'settlementDate', hidden: true}
	]
};


Clifton.otc.swap.TotalReturnSwapResetWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Total Return Swap',
	iconCls: 'event',
	height: 780,
	width: 800,

	tbar: [
		{
			text: 'View Report',
			iconCls: 'pdf',
			tooltip: 'View PDF report of Total Return Swap Reset details',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				fp.viewPDFReport();
			}
		},
		'-',
		{
			text: 'Generate and Post',
			iconCls: 'run',
			tooltip: 'Generate and Post Equity and Interest leg payment event journals as viewed on this screen.',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				fp.generateAndPostEventsWithConfirmation(
					'POST_PREVIEWED',
					'Please save your changes and review updated payments before generating and posting event journals.',
					'Are you sure you would like to generate and immediately post Equity & Interest Leg Event Journals?',
					false);
			}
		},
		'-',
		{
			text: 'Generate and Post (FORCE ACCRUAL POSTING)',
			tooltip: 'Force accrual posting even if now is before Ex Date.',
			iconCls: 'run',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				fp.generateAndPostEventsWithConfirmation(
					'POST_FORCE_ACCRUAL',
					'Please save your changes and review updated payments before generating and posting (accrual) event journals.',
					'Are you sure you would like to generate and immediately post Equity & Interest Leg (accrual) Event Journals?',
					false);
			}
		},
		'-',
		{
			text: 'Generate Only',
			iconCls: 'run',
			tooltip: 'Generate and save Equity and Interest leg payment event journals as viewed on this screen.',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				fp.generateAndPostEventsWithConfirmation(
					'GENERATE_PREVIEWED',
					'Please save your changes and review updated payments before generating event journals.',
					'Are you sure you would like to generate and save Equity & Interest Leg Event Journals?',
					false);
			}
		},
		'-',
		{
			text: 'Reload',
			tooltip: 'Reload latest data',
			iconCls: 'table-refresh',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				const w = fp.getWindow();
				if (w.isModified() || !w.isMainFormSaved()) {
					Ext.Msg.confirm('Reload', 'You have un-saved changes.  Are you sure you want to discard your changes and reload the page?', function(a) {
						if (a === 'yes') {
							fp.reload();
						}
					});
				}
				else {
					fp.reload();
				}
			}
		},
		'-',
		{
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				if (this.pressed) {
					fp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: fp.toggleInstructions
					});
				}
				else {
					fp.remove(0);
				}
				fp.doLayout();
			}
		}
	],


	items: [{
		xtype: 'formpanel',
		url: 'otcTotalReturnSwapEvent.json',
		toggleInstructions: 'Combines Equity, Interest, and Dividend Leg events in one screen. Prices and rates are updated automatically by the system when they become available.',
		loadValidation: false,
		loadDefaultDataAfterRender: true,

		listeners: {
			afterload: function() {
				const fp = this;
				const securityId = fp.getFormValue('security.id');
				if (securityId) {
					fp.setFieldLabel('equityPayment', 'Payment Amount (' + fp.getFormValue('equityLeg.valuationCurrency.symbol') + '):');
					fp.setFieldLabel('dividendPayment', 'Payment Amount (' + fp.getFormValue('dividendLeg.valuationCurrency.symbol') + '):');
					fp.setFieldLabel('interestPayment', 'Payment Amount (' + fp.getFormValue('interestLeg.valuationCurrency.symbol') + '):');

					TCG.data.getDataPromise('investmentSecurityEventTypeAllowedForSecurity.json?eventTypeName=Dividend Leg Payment&securityId=' + securityId, this)
						.then(function(allowed) {
							if (TCG.isFalse(allowed)) {
								// hide Dividend Leg section
								fp.findByType('fieldset')[1].hide();
							}
						});

					const fgs = fp.findByType('formgrid');
					let fg = null;
					for (let i = 0; i < fgs.length; i++) {
						if (fgs[i].storeRoot === 'tradeList') {
							fg = fgs[i];
							break;
						}
					}
					if (fg) {
						const fromDate = TCG.parseDate(fp.getFormValue('equityLeg.declareDate')).dateFormat('m/d/Y');
						const toDate = TCG.parseDate(fp.getFormValue('equityLeg.recordDate')).dateFormat('m/d/Y');
						let url = 'tradeListFind.json?securityId=' + securityId + '&excludeWorkflowStateName=Cancelled&restrictionList=[{"comparison":"LESS_THAN_OR_EQUALS","value":"' + toDate + '","field":"tradeDate"},{"comparison":"GREATER_THAN_OR_EQUALS","value":"' + fromDate + '","field":"tradeDate"}]';
						url += '&requestedMaxDepth=4&requestedPropertiesRoot=data';
						const data = TCG.data.getData(url, fp);
						fg.store.loadData({tradeList: data});
						if (data && data.length > 0) {
							fg.setTitle('Existing trades for selected reset period: ' + data.length);
							fg.expand(false);
						}
						else {
							fg.setTitle('No trades found during selected reset period');
							fg.collapse(false);
						}
						this.doLayout();
					}
				}
			}
		},

		getWarningMessage: function(form) {
			if (TCG.isTrue(form.formValues.paymentPreview)) {
				if (TCG.isNotBlank(form.formValues.previewSkipMessage)) {
					return 'Preview payment amounts have not been fully generated: ' + form.formValues.previewSkipMessage;
				}
				return 'The payment amounts below are <b>Previews</b> of the accounting event journals.  If correct, click <i>"Generate & Post"</i> to generate and post them to the General Ledger.  To preview the effects of changes, update information below and click <i>"Apply"</i> to recalculate.';
			}
			return undefined;
		},

		reload: function() {
			const w = this.getWindow();
			this.getForm().setValues(TCG.data.getData('otcTotalReturnSwapEvent.json?id=' + w.getMainFormId(), this), true);
			w.setModified(w.isModified());
			// to load the formgrid
			this.fireEvent('afterload', this);
		},

		viewPDFReport: function() {
			const fp = this;
			const detailId = TCG.getValue('firstEventJournalDetailId', fp.getForm().formValues);
			if (TCG.isNotBlank(detailId)) {
				Clifton.accounting.eventjournal.downloadEventJournalDetailReport(detailId, null, fp);
			}
			else {
				TCG.showError('PDF report for Total Return Swap Reset is available only if event journal details have been generated.');
			}
		},

		generateAndPostEventsWithConfirmation: function(generatorType, saveMessage, confirmationMessage, requireActualSettlementDate) {
			const fp = this;
			const w = fp.getWindow();
			if (w.isModified() || !w.isMainFormSaved()) {
				TCG.showError(saveMessage);
				return false;
			}
			Ext.Msg.confirm('Event Journal', confirmationMessage, function(a) {
				if (a === 'yes') {
					fp.generateAndPostEvents(generatorType, requireActualSettlementDate);
				}
			});
		},

		generateAndPostEvents: function(generatorType, requireActualSettlementDate) {
			const fp = this;
			if (TCG.isNull(TCG.getValue('interestPayment', fp.getForm().formValues)) || TCG.isNull(TCG.getValue('equityPayment', fp.getForm().formValues))) {
				TCG.showError('Previews did not generate payment amounts.  Please enter all necessary information, apply changes and review payment amounts prior to generating and posting.');
				return;
			}
			const interestEventId = fp.getFormValue('interestLeg.id');
			const dividendEventId = fp.getFormValue('dividendLeg.id');
			const equityEventId = fp.getFormValue('equityLeg.id');
			if (TCG.isBlank(interestEventId)) {
				TCG.showError('Missing Interest Leg Event.');
				return;
			}
			if (TCG.isBlank(equityEventId)) {
				TCG.showError('Missing Equity Leg Event.');
				return;
			}
			// equity settlement date is copied to interest and dividend legs
			const equityEventActualSettlementDate = fp.getFormValue('equityLeg.actualSettlementDate');
			if (requireActualSettlementDate && TCG.isBlank(equityEventActualSettlementDate)) {
				TCG.showError('Actual Settlement Date is required when posting accrual reversals.');
				return;
			}

			let journalCount = 0;
			const equityLoader = new TCG.data.JsonLoader({
				waitTarget: fp,
				root: 'result',
				waitMsg: 'Generating Equity Leg Event Journal',
				params: {
					eventId: equityEventId,
					generatorType: generatorType
				},
				onLoad: function(legThreeCount, conf) {
					journalCount += legThreeCount;
					Ext.Msg.alert('Journals Generated/Posted', journalCount + ' total journals generated.' + (generatorType === 'POST_PREVIEWED' ? '  All existing un-booked journals posted.' : ''));
					fp.reload();
				}
			});

			TCG.data.getDataPromise('investmentSecurityEventTypeAllowedForSecurity.json?eventTypeName=Dividend Leg Payment&securityId=' + fp.getFormValue('security.id'), this)
				.then(function(dividendAllowed) {
					if (TCG.isTrue(dividendAllowed)) {
						if (TCG.isBlank(dividendEventId)) {
							TCG.showError('Missing Dividend Leg Event.');
							return;
						}
					}
					const interestLoader = new TCG.data.JsonLoader({
						waitTarget: fp,
						waitMsg: 'Generating Interest Leg Event Journal',
						root: 'result',
						params: {
							eventId: interestEventId,
							generatorType: generatorType
						},
						onLoad: function(legOneCount, conf) {
							journalCount += legOneCount;
							if (TCG.isTrue(dividendAllowed)) {
								const dividendLoader = new TCG.data.JsonLoader({
									waitTarget: fp,
									root: 'result',
									waitMsg: 'Generating Dividend Leg Event Journal',
									params: {
										eventId: dividendEventId,
										generatorType: generatorType
									},
									onLoad: function(legTwoCount) {
										journalCount += legTwoCount;
										equityLoader.load('accountingEventJournalListGenerate.json');
									}
								});
								dividendLoader.load('accountingEventJournalListGenerate.json');
							}
							else { // skip dividend loader
								equityLoader.load('accountingEventJournalListGenerate.json');
							}
						}
					});
					interestLoader.load('accountingEventJournalListGenerate.json');
				});
		},

		items: [
			{fieldLabel: 'Swap', name: 'security.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'security.id'},
			{fieldLabel: 'Net Local Payment', name: 'totalPayment', xtype: 'displayfield', type: 'currency', cls: 'important'},
			{fieldLabel: 'Net Base Payment', name: 'totalPaymentBase', xtype: 'displayfield', type: 'currency', cls: 'important'},

			{
				xtype: 'fieldset',
				title: 'Equity Leg',
				anchor: '0',
				labelWidth: 130,
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Valuation Date', name: 'equityLeg.declareDate', xtype: 'datefield', allowBlank: false, qtip: 'Valuation price for the beginning of reset period is looked up on this date'},
										{fieldLabel: 'Next Valuation Date', name: 'equityLeg.recordDate', xtype: 'datefield', allowBlank: false, qtip: 'Valuation price for the end of reset period is looked up on this date'},
										{fieldLabel: 'Contract Settle Date', name: 'equityLeg.eventDate', xtype: 'datefield', allowBlank: false, qtip: 'Settlement Date agreed upon in the Confirmation contract.'},
										{fieldLabel: 'Actual Settle Date', name: 'equityLeg.actualSettlementDate', xtype: 'datefield', qtip: 'The Date payments were sent/received at the Custodian, which should equal the Contract Settle Date or later (if payments were sent/received late).'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Valuation Price', name: 'equityLeg.beforeEventValue', xtype: 'floatfield', qtip: 'Equity Leg Underlying price on Valuation Date'},
										{fieldLabel: 'Next Valuation Price', name: 'equityLeg.afterEventValue', xtype: 'floatfield', qtip: 'Equity Leg Underlying price on Next Valuation Date'},
										{fieldLabel: 'Payment Amount', name: 'equityPayment', xtype: 'currencyfield', readOnly: true}
									]
								}]
							}
						]
					},
					{fieldLabel: '&nbsp;Equity Event', labelWidth: 120, name: 'equityLeg.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow', detailIdField: 'equityLeg.id', colspan: 3}
				]
			},


			{
				xtype: 'fieldset',
				title: 'Dividend Leg',
				anchor: '0',
				labelWidth: 130,
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Dividend Points', name: 'dividendLeg.afterEventValue', xtype: 'floatfield', qtip: 'Dividend points are the net dividend amount per unit inclusive of pass through rate (i.e. Gross Dividend Points * Pass-Through %). Dividend Leg dates match corresponding Equity Leg dates.'},
										{fieldLabel: 'Actual Settle Date', name: 'dividendLeg.actualSettlementDate', xtype: 'datefield', readOnly: true, qtip: ' The Date payments were sent/received at the Custodian, which should equal the Contract Settle Date or later (if payments were sent/received late).'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Payment Amount', name: 'dividendPayment', xtype: 'currencyfield', readOnly: true}
									]
								}]
							}
						]
					},
					{fieldLabel: '&nbsp;Dividend Event', name: 'dividendLeg.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow', detailIdField: 'dividendLeg.id'}
				]
			},

			{
				xtype: 'fieldset',
				title: 'Interest Leg',
				anchor: '0',
				labelWidth: 130,
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Accrual Start Date', name: 'interestLeg.declareDate', xtype: 'datefield', allowBlank: false, qtip: 'Industry standard End Of Day convention is used: accrual will NOT be included on this date.'},
										{fieldLabel: 'Fixing Date', name: 'interestLeg.additionalDate', xtype: 'datefield', allowBlank: false, qtip: 'Interest Rate on this date is used in Effective Rate calculation'},
										{fieldLabel: 'Contract Settle Date', name: 'interestLeg.eventDate', xtype: 'datefield', readOnly: true, qtip: 'Settlement Date agreed upon in the Confirmation contract.'},
										{fieldLabel: 'Actual Settle Date', name: 'interestLeg.actualSettlementDate', xtype: 'datefield', readOnly: true, qtip: 'The Date payments were sent/received at the Custodian, which should equal the Contract Settle Date or later (if payments were sent/received late).'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Accrual End Date', name: 'interestLeg.recordDate', xtype: 'datefield', qtip: 'Interest Leg payment amount will include accrual on this Date. It is usually the same as Payment Date.'},
										{fieldLabel: 'Effective Rate', name: 'interestLeg.beforeEventValue', xtype: 'floatfield', qtip: 'Interest Rate Index value plus the Spread on Fixing Date'},
										{fieldLabel: 'Payment Amount', name: 'interestPayment', xtype: 'currencyfield', readOnly: true}
									]
								}]
							}
						]
					},
					{fieldLabel: '&nbsp;Interest Event', name: 'interestLeg.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow', detailIdField: 'interestLeg.id'}
				]
			},

			{fieldLabel: 'Notes', name: 'equityLeg.eventDescription', xtype: 'textarea', grow: true, height: 35},
			Clifton.otc.swap.TotalReturnSwapTradesFormGrid
		]
	}]
});

