TCG.use('Clifton.otc.swap.copy.SwapCopyFormBase');

Clifton.otc.swap.copy.SwapCDSCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Copy CDS Swap',
	iconCls: 'swap',
	height: 640,

	items: [{
		xtype: 'otc-swap-copy-form',

		copyFieldNames: ['endDate', 'lastDeliveryDate', 'firstDeliveryDate'],

		copyItems: [
			{
				xtype: 'fieldset',
				title: 'Copy From (Template)',
				instructions: 'Defaults will be populated based on the first CDS swap found for selected instrument. All CDS for the same instrument share the same events, so all events (Coupons, Credit Events) will be copied as well.',
				labelWidth: 130,

				items: [
					{name: 'copyFromSecurityId', xtype: 'hidden'},
					{fieldLabel: 'Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					// If passed used to filter the swaps to specific instrument group
					{hidden: true, name: 'investmentGroupId', submitValue: false},
					{
						fieldLabel: 'Instrument', xtype: 'combo', name: 'instrument.name', hiddenName: 'instrument.id', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						requestedProps: 'hierarchy.id|hierarchy.labelExpanded',
						listeners: {
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								const params = {investmentTypeSubType: 'Credit Default Swaps'};
								// if set - pass hierarchy id
								params.hierarchyId = f.findField('instrument.hierarchy.id').value;
								params.investmentGroupId = f.findField('investmentGroupId').value;
								queryEvent.combo.store.baseParams = params;
							},

							select: function(combo, record, index) {
								// On Select - Attempt to Load all defaults
								const fp = combo.getParentForm();
								fp.setFormValue('instrument.hierarchy.labelExpanded', record.json.hierarchy.labelExpanded, true);
								fp.setFormValue('instrument.hierarchy.id', record.json.hierarchy.id, true);

								const params = {instrumentId: record.id};
								params.requestedPropertiesRoot = 'data';
								params.requestedProperties = 'id|endDate|lastDeliveryDate|firstDeliveryDate|instrument.hierarchy.labelExpanded|instrument.hierarchy.id';
								const loader = new TCG.data.JsonLoader({
									params: params,
									onLoad: function(record, conf) {
										const l = record.length || 0;
										if (l === 0) {
											TCG.showError('Cannot find any securities for the selected instrument.  You can still create a new security, however there is no template to copy any existing information from.', 'New Setup');
										}
										fp.updateCopyFromValues(record[0]);
									}
								});
								loader.load('investmentSecurityListFind.json?orderBy=endDate:DESC', fp);
							}
						}
					},
					{xtype: 'checkbox', name: 'copySymbolOverrides', checked: true, boxLabel: 'Copy Symbol Overrides (Data Source Mappings)'},
					{xtype: 'checkbox', name: 'copyCommissions', boxLabel: 'Copy Commission Definitions', qtip: 'Copies commission definitions directly tied to the security you are copying from'},
					{xtype: 'checkbox', name: 'copyNotesFromToday', boxLabel: 'Copy Notes From Today (Applies to Notes that support link to multiple)', qtip: 'Adds a link for all notes created today on the template security to the new security.  If the note type does not support links to multiple securities the note will be skipped.'}
				]
			}
		]
	}]
});




