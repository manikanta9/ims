TCG.use('Clifton.investment.instrument.copy.SecurityCopyForm');

Clifton.otc.swap.copy.SwapCopyFormBase = Ext.extend(Clifton.investment.instrument.copy.SecurityCopyForm, {
	submitEmptyText: true,
	getSaveURL: function() {
		return 'otcInvestmentSecuritySwapSave.json';
	},

	securityItems: [
		{fieldLabel: 'Reference Number', name: 'symbol', qtip: 'Counterparty identifier for this swap'},
		{fieldLabel: 'Name', name: 'name'},
		{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
		{fieldLabel: 'Initial Valuation Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
		{fieldLabel: 'Maturity Date', name: 'endDate', xtype: 'datefield', allowBlank: false},
		{
			fieldLabel: 'Final Payment Date', name: 'lastDeliveryDate', xtype: 'datefield', allowBlank: false,
			listeners: {
				change: function(field, newValue, oldValue) {
					TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
				},
				select: function(field, newValue) {
					TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
				}
			}
		},
		{name: 'firstDeliveryDate', xtype: 'datefield', hidden: true}
	]

});
Ext.reg('otc-swap-copy-form', Clifton.otc.swap.copy.SwapCopyFormBase);
