TCG.use('Clifton.otc.swap.copy.SwapCopyFormBase');

Clifton.otc.swap.copy.SwapTRSCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Copy TRS Swap',
	iconCls: 'swap',
	height: 640,

	items: [{
		xtype: 'otc-swap-copy-form',
		// After create and open security window - switch to the second i.e. "Events" tab
		securityDefaultActiveTab: 1,
		copyFieldNames: ['firstDeliveryDate', 'startDate', 'endDate', 'lastDeliveryDate', 'settlementCurrency'],
		skipCopyCustomFieldNames: ['Spread', 'Reset Accrual Start Date'],
		doNotCopyContract: true,

		copyItems: [
			{
				xtype: 'fieldset',
				title: 'Copy From (Template)',
				instructions: 'Defaults will be populated based on the selected TRS swap.',
				labelWidth: 130,

				items: [
					{fieldLabel: 'Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{fieldLabel: 'Instrument', name: 'instrument.labelShort', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
					// If passed used to filter the swaps to specific instrument group
					{hidden: true, name: 'investmentGroupId', submitValue: false},
					{
						fieldLabel: 'Swap', xtype: 'combo', name: 'copyFromSecurityIdLabel', hiddenName: 'copyFromSecurityId', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label',
						allowBlank: false, // required to select one, if not need to add an instrument selection so for new ones we know which instrument/hierarchy to put it under
						requestedProps: 'endDate|lastDeliveryDate|firstDeliveryDate|instrument.hierarchy.labelExpanded|instrument.hierarchy.id|instrument.labelShort|instrument.id',
						listeners: {
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								const params = {hierarchyName: 'Total Return Swaps'};
								params.investmentGroupId = f.findField('investmentGroupId').value;
								const filterOn = f.findField('filterSwaps').checked;
								if (filterOn === true) {
									const companyId = f.findField('businessCompany.id').value;
									const contractId = f.findField('businessContract.id').value;
									if (companyId !== '') {
										params.businessCompanyId = companyId;
									}
									if (contractId !== '') {
										params.businessContractId = contractId;
									}
								}
								queryEvent.combo.store.baseParams = params;
							},
							select: function(combo, record, index) {
								// On Select - Attempt to Load all defaults
								const fp = combo.getParentForm();
								fp.setFormValue('instrument.hierarchy.labelExpanded', record.json.instrument.hierarchy.labelExpanded, true);
								fp.setFormValue('instrument.hierarchy.id', record.json.instrument.hierarchy.id, true);

								fp.setFormValue('instrument.labelShort', record.json.instrument.labelShort, true);
								fp.setFormValue('instrument.id', record.json.instrument.id, true);

								fp.updateCopyFromValues(record.json);
							}
						}
					},
					{
						boxLabel: 'Restrict template Swaps to selected ISDA and Counterparty', xtype: 'checkbox', name: 'filterSwaps', submitValue: 'false', checked: true,
						listeners: {
							check: function(f) {
								// On changes reset contract dropdown
								const p = TCG.getParentFormPanel(f);
								const bc = p.getForm().findField('copyFromSecurityIdLabel');
								bc.clearValue();
								bc.store.removeAll();
								bc.lastQuery = null;
							}
						}
					},
					{xtype: 'checkbox', name: 'copyEvents', checked: false, boxLabel: 'Copy All Security Events (Resets)', qtip: 'Copies all events from the selected template Swap to the new swap where the event date falls between start/end dates.'},
					{xtype: 'checkbox', name: 'copyCommissions', boxLabel: 'Copy Commission Definitions', qtip: 'Copies commission definitions directly tied to the security you are copying from'},
					{xtype: 'checkbox', name: 'copyNotesFromToday', boxLabel: 'Copy Notes From Today (Applies to Notes that support link to multiple)', qtip: 'Adds a link for all notes created today on the template security to the new security.  If the note type does not support links to multiple securities the note will be skipped.'}

				]
			}
		]
	}]
});




