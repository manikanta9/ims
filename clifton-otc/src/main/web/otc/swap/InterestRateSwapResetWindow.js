Clifton.otc.swap.InterestRateSwapResetWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Swap',
	iconCls: 'calendar',
	height: 690,
	width: 750,

	tbar: [
		{
			text: 'Generate & Post',
			iconCls: 'book-red',
			tooltip: 'Generate and Post Fixed and Floating leg payment event journals as viewed on this screen.',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				const w = fp.getWindow();
				if (w.isModified() || !w.isMainFormSaved()) {
					TCG.showError('Please save your changes and review updated payments before generating and posting event journals.');
					return false;
				}
				Ext.Msg.confirm('Event Journal', 'Are you sure you would like to generate and immediately post Fixed & Floating Leg Event Journals?', function(a) {
					if (a === 'yes') {
						fp.generateAndPostEvents();
					}
				});
			}
		},
		'-',
		{
			text: 'Reload',
			tooltip: 'Reload latest data',
			iconCls: 'table-refresh',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				const w = fp.getWindow();
				if (w.isModified() || !w.isMainFormSaved()) {
					Ext.Msg.confirm('Reload', 'You have un-saved changes.  Are you sure you want to discard your changes and reload the page?', function(a) {
						if (a === 'yes') {
							fp.reload();
						}
					});
				}
				else {
					fp.reload();
				}
			}
		},
		'-',
		{
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				if (this.pressed) {
					fp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: fp.toggleInstructions
					});
				}
				else {
					fp.remove(0);
				}
				fp.doLayout();
			}
		}
	],

	items: [{
		xtype: 'formpanel',
		url: 'otcInterestRateSwapEvent.json',
		loadDefaultDataAfterRender: true,
		toggleInstructions: 'Combines Fixed and Floating Leg events in one screen. Prices and rates are updated automatically by the system when they become available.',
		loadValidation: false,
		listeners: {
			afterload: function() {
				const detailArray = this.getFormValue('floatingDetailList');
				if (detailArray.length > 1) {
					this.disableField('floatingDetailList[0].referenceRate');
					this.disableField('floatingDetailList[0].effectiveDate');
				}
				this.setFieldLabel('fixedPayment', 'Payment Amount (' + this.getFormValue('fixedLeg.valuationCurrency.symbol') + '):');
				this.setFieldLabel('floatingPayment', 'Payment Amount (' + this.getFormValue('floatingLeg.valuationCurrency.symbol') + '):');
			}
		},
		getWarningMessage: function(form) {
			if (TCG.isTrue(form.formValues.paymentPreview)) {
				if (TCG.isNotBlank(form.formValues.previewSkipMessage)) {
					return 'Preview payment amounts have not been fully generated: ' + form.formValues.previewSkipMessage;
				}
				return 'The payment amounts below are <b>Previews</b> of the accounting event journals.  If correct, click <i>"Generate & Post"</i> to generate and post them to the General Ledger.  To preview the effects of changes, update information below and click <i>"Apply"</i> to recalculate.';
			}
			return undefined;
		},
		reload: function() {
			const w = this.getWindow();
			this.getForm().setValues(TCG.data.getData('otcInterestRateSwapEvent.json?id=' + w.getMainFormId(), this), true);
			w.setModified(w.isModified());
			// to load the formgrid
			this.fireEvent('afterload', this);
		},
		generateAndPostEvents: function() {
			const fp = this;
			if (TCG.isNull(TCG.getValue('fixedPayment', fp.getForm().formValues)) || TCG.isNull(TCG.getValue('floatingPayment', fp.getForm().formValues))) {
				TCG.showError('Previews did not generate payment amounts.  Please enter all necessary information, apply changes and review payment amounts prior to generating and posting.');
				return;
			}
			const fixedEventId = TCG.getValue('fixedLeg.id', fp.getForm().formValues);
			const floatingEventId = TCG.getValue('floatingLeg.id', fp.getForm().formValues);
			if (TCG.isBlank(fixedEventId)) {
				TCG.showError('Missing Fixed Leg Event.');
				return;
			}
			if (TCG.isBlank(floatingEventId)) {
				TCG.showError('Missing Floating Leg Event.');
				return;
			}
			const fixedLoader = new TCG.data.JsonLoader({
				waitTarget: fp,
				waitMsg: 'Generating Fixed Leg Event Journal',
				root: 'result',
				params: {
					eventId: fixedEventId,
					generatorType: 'POST_PREVIEWED'
				},
				onLoad: function(record, conf) {

					let totalGP = record;
					const floatingLoader = new TCG.data.JsonLoader({
						waitTarget: fp,
						root: 'result',
						waitMsg: 'Generating Floating Leg Event Journal',
						params: {
							eventId: floatingEventId,
							generatorType: 'POST_PREVIEWED'
						},
						onLoad: function(record, conf) {
							totalGP += record;
							Ext.Msg.alert('Journals Generated/Posted', totalGP + ' total journals generated.  All existing un-booked journals posted.');
							fp.reload();
						}
					});
					floatingLoader.load('accountingEventJournalListGenerate.json');
				}
			});
			fixedLoader.load('accountingEventJournalListGenerate.json');
		},

		items: [
			{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'security.id'},
			{fieldLabel: 'Net Local Payment', name: 'totalPayment', xtype: 'displayfield', type: 'currency', cls: 'important'},
			{fieldLabel: 'Net Base Payment', name: 'totalPaymentBase', xtype: 'displayfield', type: 'currency', cls: 'important'},

			{
				xtype: 'panel',
				layout: 'column',
				anchor: '0',
				items: [
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 130,
							items: [
								{fieldLabel: 'Accrual Start', name: 'fixedLeg.declareDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: 'Accrual End', name: 'fixedLeg.recordDate', xtype: 'datefield', allowBlank: false}
							]
						}]
					},
					{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
					{
						columnWidth: .49,
						items: [{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 140,
							items: [
								{fieldLabel: 'Ex Date', name: 'fixedLeg.exDate', xtype: 'datefield', allowBlank: false},
								{fieldLabel: 'Payment Date', name: 'fixedLeg.paymentDate', xtype: 'datefield', allowBlank: false}
							]
						}]
					}
				]
			},

			{
				xtype: 'fieldset',
				title: 'Fixed Leg',
				anchor: '0',
				labelWidth: 125,
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Rate (%)', name: 'fixedLeg.afterEventValue', xtype: 'floatfield'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Payment Amount', name: 'fixedPayment', xtype: 'currencyfield', readOnly: true}
									]
								}]
							}
						]
					},
					{fieldLabel: '&nbsp;Fixed Leg Event', labelWidth: 120, name: 'fixedLeg.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow', detailIdField: 'fixedLeg.id', colspan: 3}

				]
			},
			{
				xtype: 'fieldset',
				title: 'Floating Leg',
				anchor: '0',
				labelWidth: 125,
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Rate (%)', name: 'floatingLeg.afterEventValue', xtype: 'floatfield'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Payment Amount', name: 'floatingPayment', xtype: 'currencyfield', readOnly: true}
									]
								}]
							}
						]
					},
					{xtype: 'label', html: '&nbsp;Optional date in the middle of Floating Leg period when new interest rate for the second part of the period is determined.'},
					{
						xtype: 'panel',
						layout: 'column',
						anchor: '0',
						items: [
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 120,
									items: [
										{fieldLabel: 'Effective Date', name: 'floatingDetailList[0].effectiveDate', xtype: 'datefield'}
									]
								}]
							},
							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
							{
								columnWidth: .49,
								items: [{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Rate (%)', name: 'floatingDetailList[0].referenceRate', xtype: 'floatfield'}
									]
								}]
							}
						]


					},
					{fieldLabel: '&nbsp;Floating Leg Event', labelWidth: 120, name: 'floatingLeg.label', xtype: 'linkfield', submitDetailField: true, detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow', detailIdField: 'floatingLeg.id', colspan: 3}
				]
			},
			{fieldLabel: 'Notes', name: 'fixedLeg.eventDescription', xtype: 'textarea', height: 70}
		]
	}]
});
