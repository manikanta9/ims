Clifton.otc.swap.upload.InterestRateSwapResetEventUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'otcInterestRateSwapResetEventUploadWindow',
	title: 'Interest Rate Swap Reset Events Upload',
	iconCls: 'import',
	height: 450,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('otc/swap/upload/InterestRateSwapResetEventUploadFileSample.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Interest Rate Swap Reset Events Upload allows using a customized file format to easily upload both Fixed Leg and Floating Leg events for Interest Rate Swap resets.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'If uploaded row already exists in the system'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'existingBeans', items: [
					{fieldLabel: '', boxLabel: 'Skip upload for rows that already exist.', name: 'existingBeans', inputValue: 'SKIP', checked: true},
					{fieldLabel: '', boxLabel: 'Overwrite existing data with uploaded information. (Columns not included in the file will NOT overwrite existing data in the database)', name: 'existingBeans', inputValue: 'UPDATE'},
					{fieldLabel: '', boxLabel: 'Insert all data to improve upload performance (Fails if a constraint is violated).', name: 'existingBeans', inputValue: 'INSERT'}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],

		getSaveURL: function() {
			return 'otcInterestRateSwapResetEventUploadFileUpload.json';
		}

	}]
});
