// NOTE - NOT CURRENTLY USED
Clifton.otc.swap.InterestRateSwapWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Swap',
	iconCls: 'coins',
	height: 690,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Info',
			items: [{
				xtype: 'formpanel',
				url: 'otcInterestRateSwap.json',
				loadValidation: false,
				labelFieldName: 'investmentSecurity.symbol',
				labelWidth: 140,

				items: [
					{
						fieldLabel: 'ISDA', name: 'isda.name', hiddenName: 'isda.id', xtype: 'combo', url: 'businessContractListFind.json?contractTypeName=ISDA', detailPageClass: 'Clifton.business.contract.ContractWindow', allowBlank: false,
						listeners: {
							select: function(combo, record, index) {
								// update client and counterparty fields from selected ISDA contract
								const form = combo.getParentForm().getForm();
								const companyField = form.findField('isda.company.name');
								const companyIdField = form.findField('isda.company.id');
								const company = record.json.company;
								companyField.setValue(company.name);
								companyIdField.setValue(company.id);

								const roles = TCG.data.getData('businessContractPartyListByContractAndRole.json?roleNames=Counterparty&contractId=' + record.json.id, combo);
								if (roles.length > 0) {
									const cp = roles[0].company;
									const cpField = form.findField('counterparty.name');
									const cpIdField = form.findField('counterparty.id');
									cpField.setValue(cp.name);
									cpIdField.setValue(cp.id);
								}
							}
						}
					},
					{fieldLabel: 'Client', name: 'isda.company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'isda.company.id'},
					{fieldLabel: 'Counterparty', name: 'counterparty.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'counterparty.id'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Investment Hierarchy', name: 'investmentSecurity.instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'investmentSecurity.instrument.hierarchy.id'},
					{fieldLabel: 'Ticker Symbol', name: 'investmentSecurity.symbol', allowBlank: false},
					{fieldLabel: 'Name', name: 'investmentSecurity.name', allowBlank: false},
					{fieldLabel: 'Description', name: 'investmentSecurity.description', xtype: 'textarea', allowBlank: false},
					{fieldLabel: 'Notional', name: 'notional', xtype: 'currencyfield', allowBlank: false},
					{fieldLabel: 'Currency Denomination', name: 'investmentSecurity.instrument.tradingCurrency.name', hiddenName: 'investmentSecurity.instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{
						fieldLabel: 'Structure', hiddenName: 'fixedLegReceivedByClient', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', allowBlank: false,
						store: new Ext.data.ArrayStore({
							fields: ['value', 'name'],
							data: [[true, 'Client pays Floating Leg and receives Fixed Leg'], [false, 'Client pays Fixed Leg and receives Floating Leg']]
						})
					},
					{fieldLabel: 'Start Date', name: 'investmentSecurity.startDate', xtype: 'datefield', allowBlank: false},
					{fieldLabel: 'Maturity Date', name: 'investmentSecurity.endDate', xtype: 'datefield', allowBlank: false},
					{
						xtype: 'fieldset',
						title: 'Fixed Leg',
						labelWidth: 130,
						anchor: '-5',
						items: [
							{fieldLabel: 'Coupon Percent', name: 'fixedCouponPercent', xtype: 'currencyfield', decimalPrecision: 6, allowBlank: false},
							{fieldLabel: 'Market Convention', name: 'fixedRollScheduleMarketConvention.label', hiddenName: 'fixedRollScheduleMarketConvention.id', xtype: 'combo', displayField: 'label', url: 'fincadObjectListFind.json?repositoryName=MarketConventions', allowBlank: false, detailPageClass: 'Clifton.fincad.object.ObjectWindow'}
						]
					},
					{
						xtype: 'fieldset',
						title: 'Floating Leg',
						labelWidth: 130,
						anchor: '-5',
						items: [
							{fieldLabel: 'Rate Index', name: 'floatingRateIndex.label', hiddenName: 'floatingRateIndex.id', xtype: 'combo', displayField: 'label', url: 'fincadObjectListFind.json?repositoryName=Index', allowBlank: false, detailPageClass: 'Clifton.fincad.object.ObjectWindow'},
							{fieldLabel: 'Market Convention', name: 'floatingRollScheduleMarketConvention.label', hiddenName: 'floatingRollScheduleMarketConvention.id', xtype: 'combo', displayField: 'label', url: 'fincadObjectListFind.json?repositoryName=MarketConventions', allowBlank: false, detailPageClass: 'Clifton.fincad.object.ObjectWindow'}
						]
					}
				],
				prepareDefaultData: function(data) {
					// re-map hierarchy because of non-standard InvestmentSecurity structure
					if (data.hierarchy) {
						data['investmentSecurity'] = {};
						data.investmentSecurity['instrument'] = {};
						data.investmentSecurity.instrument['hierarchy'] = data.hierarchy;
						delete data.hierarchy;
					}
					return data;
				}
			}]
		},


			{
				title: 'Events',
				items: [{
					name: 'investmentSecurityEventListFind',
					xtype: 'gridpanel',
					instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Additional Security', width: 100, dataIndex: 'additionalSecurity.label', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Before Value', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
						{header: 'After Value', width: 60, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
						{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
						{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
						{header: 'Event Date', width: 50, dataIndex: 'eventDate'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add an event of selected type to this security',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const eventType = TCG.getChildByName(toolBar, 'eventType');
									const eventTypeId = eventType.getValue();
									if (eventTypeId === '') {
										TCG.showError('You must first select desired Event Type from the list.');
									}
									else {
										this.savedDefaultData = {
											security: gridPanel.getWindow().getMainForm().formValues,
											type: {
												id: eventTypeId,
												name: eventType.lastSelectionText
											}
										};
										this.openDetailPage(this.getDetailPageClass(), gridPanel);
									}
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {'securityId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Market Data',
				items: [{
					xtype: 'marketdata-security-datavalue-grid',
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataValueWindow',
						getDefaultData: function(gridPanel) {
							return {
								investmentSecurity: gridPanel.getWindow().getMainForm().formValues.investmentSecurity
							};
						}
					}
				}]
			},


			{
				title: 'Open Positions',
				items: [{xtype: 'accounting-securityLiveHoldingsTradingGrid'}]
			}]
	}]
});
