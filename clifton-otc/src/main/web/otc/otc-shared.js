Ext.ns('Clifton.otc.swap', 'Clifton.otc.swap.copy', 'Clifton.otc.swap.upload');


// add OTC screens to available options
Clifton.investment.setup.InstrumentHierarchyScreens[Clifton.investment.setup.InstrumentHierarchyScreens.length] = ['Swap: IRS', 'Swap: IRS', 'The screen for Interest Rate Swaps.'];

//used to allow creating CDS and TRS swaps by copying properties of an existing one
//defined by the "Investment Type Sub Type Name" of the instrument (note: can cross hierarchies)
// If not defined here, uses default copy window
Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap['Credit Default Swaps'] = 'Clifton.otc.swap.copy.SwapCDSCopyWindow';
Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap['Total Return Swaps'] = 'Clifton.otc.swap.copy.SwapTRSCopyWindow';

// used to open combined events together by overridding the class mapping for the specific security type, sub type and event type name
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Swaps_Total Return Swaps_Equity Leg Payment'] = {
	className: 'Clifton.otc.swap.TotalReturnSwapResetWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcTotalReturnSwapEvent.json?id=' + entity.id, this);
		}
		return entity;
	}
};
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Swaps_Total Return Swaps_Interest Leg Payment'] = {
	className: 'Clifton.otc.swap.TotalReturnSwapResetWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcTotalReturnSwapEvent.json?id=' + entity.id, this);
		}
		return entity;
	}
};
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Swaps_Total Return Swaps_Dividend Leg Payment'] = {
	className: 'Clifton.otc.swap.TotalReturnSwapResetWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcTotalReturnSwapEvent.json?id=' + entity.id, this);
		}
		return entity;
	}
};

Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Swaps_Interest Rate Swaps_Fixed Leg Payment'] = {
	className: 'Clifton.otc.swap.InterestRateSwapResetWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcInterestRateSwapEvent.json?id=' + entity.id, this);
		}
		return entity;
	}

};
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Swaps_Interest Rate Swaps_Floating Leg Payment'] = {
	className: 'Clifton.otc.swap.InterestRateSwapResetWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcInterestRateSwapEvent.json?id=' + entity.id, this);
		}
		return entity;
	}
};


// NOTE - NOT CURRENTLY USED
Clifton.investment.instrument.SecurityWindowClassMap['Swap: IRS'] = {
	className: 'Clifton.otc.swap.InterestRateSwapWindow',
	getEntity: function(config, entity) {
		if (entity) {
			return TCG.data.getData('otcInterestRateSwap.json?id=' + entity.id, this);
		}
		return entity;
	}
};
