package com.clifton.otc;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>OtcSecurity</code> class is a base class that should be extended by all specific
 * instances of OTC securities.
 *
 * @author vgomelsky
 */
public abstract class OtcSecurity extends BaseSimpleEntity<Integer> {

	private BusinessContract isda;
	private BusinessCompany counterparty;
	private InvestmentSecurity investmentSecurity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContract getIsda() {
		return this.isda;
	}


	public void setIsda(BusinessContract isda) {
		this.isda = isda;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BusinessCompany getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(BusinessCompany counterparty) {
		this.counterparty = counterparty;
	}
}
