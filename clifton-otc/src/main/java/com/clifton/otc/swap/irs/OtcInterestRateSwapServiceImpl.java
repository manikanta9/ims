package com.clifton.otc.swap.irs;


import com.clifton.accounting.eventjournal.eventrollup.AccountingSecurityEventRollupHandler;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class OtcInterestRateSwapServiceImpl implements OtcInterestRateSwapService {

	private AccountingSecurityEventRollupHandler<OtcInterestRateSwapEvent> accountingSecurityEventRollupHandler;

	private BusinessContractService businessContractService;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////
	//////           Interest Rate Swaps                 ///////
	////////////////////////////////////////////////////////////


	@Override
	public OtcInterestRateSwap getOtcInterestRateSwap(int id) {
		OtcInterestRateSwap swap = null;
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(id);
		if (security != null) {
			swap = new OtcInterestRateSwap();
			// populate security based properties
			swap.setId(security.getId());
			swap.setInvestmentSecurity(security);
			swap.setIsda(security.getBusinessContract());
			if (swap.getIsda() != null) {
				List<BusinessContractParty> partyList = getBusinessContractService().getBusinessContractPartyListByContract(swap.getIsda().getId());
				for (BusinessContractParty party : CollectionUtils.getIterable(partyList)) {
					if (BusinessContractPartyRole.PARTY_ROLE_COUNTERPARTY_NAME.equals(party.getRole().getName())) {
						swap.setCounterparty(party.getCompany());
						break;
					}
				}
			}
		}
		return swap;
	}


	@Override
	@Transactional
	public void saveOtcInterestRateSwap(OtcInterestRateSwap swap) {
		// 1. save security and it properties
		InvestmentSecurity security = swap.getInvestmentSecurity();
		if (!swap.isNewBean()) {
			// need to update existing security fields
			security = getInvestmentInstrumentService().getInvestmentSecurity(swap.getId());
			ValidationUtils.assertNotNull(security, "Cannot find Investment Security for id = " + swap.getId());
			InvestmentSecurity updatedSecurity = swap.getInvestmentSecurity();
			security.setSymbol(updatedSecurity.getSymbol());
			security.setName(updatedSecurity.getName());
			security.setDescription(updatedSecurity.getDescription());
			security.getInstrument().setTradingCurrency(getInvestmentInstrumentService().getInvestmentSecurity(updatedSecurity.getInstrument().getTradingCurrency().getId()));
			security.setStartDate(updatedSecurity.getStartDate());
			security.setEndDate(updatedSecurity.getEndDate());
		}
		security.setBusinessContract(swap.getIsda());
		getInvestmentInstrumentService().saveInvestmentSecurity(security);
	}


	////////////////////////////////////////////////////////////////////////////
	////////         OtcInterestRateSwapEvent Business Methods        //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OtcInterestRateSwapEvent getOtcInterestRateSwapEvent(int id) {
		return getOtcInterestRateSwapEventImpl(id, true);
	}


	private OtcInterestRateSwapEvent getOtcInterestRateSwapEventImpl(int id, boolean populatePayments) {
		OtcInterestRateSwapEvent event = getAccountingSecurityEventRollupHandler().getAccountingSecurityEventRollup(OtcInterestRateSwapEvent.class, id, populatePayments);
		if (event.getFloatingLeg() != null && !event.getFloatingLeg().isNewBean()) {
			event.setFloatingDetailList(getOrderFloatingDetailListByEffectiveDate(event));
		}
		return event;
	}


	@Override
	public List<OtcInterestRateSwapEvent> getOtcInterestRateSwapEventList(int securityId) {
		// TODO ADD FLOATING DETAIL HERE TOO?
		return getAccountingSecurityEventRollupHandler().getAccountingSecurityEventRollupList(OtcInterestRateSwapEvent.class, securityId);
	}


	@Override
	@Transactional
	public void saveOtcInterestRateSwapEvent(OtcInterestRateSwapEvent event) {
		InvestmentSecurityEvent fixedLeg = event.getFixedLeg();
		InvestmentSecurityEvent floatingLeg = event.getFloatingLeg();
		List<InvestmentSecurityEventDetail> floatingLegDetailList = event.getFloatingDetailList();

		if (fixedLeg.getBeforeEventValue() == null) {
			fixedLeg.setBeforeEventValue(BigDecimal.ZERO);
		}
		if (fixedLeg.getAfterEventValue() == null) {
			fixedLeg.setAfterEventValue(BigDecimal.ZERO);
		}
		if (floatingLeg.getBeforeEventValue() == null) {
			floatingLeg.setBeforeEventValue(BigDecimal.ZERO);
		}
		if (floatingLeg.getAfterEventValue() == null) {
			floatingLeg.setAfterEventValue(BigDecimal.ZERO);
		}

		floatingLeg.setDeclareDate(fixedLeg.getDeclareDate());
		floatingLeg.setExDate(fixedLeg.getExDate());
		floatingLeg.setRecordDate(fixedLeg.getRecordDate());
		floatingLeg.setPaymentDate(fixedLeg.getPaymentDate());
		floatingLeg.setActualSettlementDate(fixedLeg.getActualSettlementDate());

		// same description from the equity leg is used for both fields
		if (!StringUtils.isEmpty(fixedLeg.getEventDescription())) {
			floatingLeg.setEventDescription(fixedLeg.getEventDescription());
		}

		// validate inputs
		ValidationUtils.assertTrue(fixedLeg.getActualSettlementDate() == null || DateUtils.isDateAfterOrEqual(fixedLeg.getActualSettlementDate(), fixedLeg.getPaymentDate()),
				"Actual Settlement Date must be equal or after Contract Settlement Date.");

		// save both events in the same transaction
		getAccountingSecurityEventRollupHandler().saveAccountingSecurityEventRollup(event);
		InvestmentSecurityEventDetail floatingLegDetail = null;

		// Save event detail with event if there is only one but don't if there is more than one.
		if (floatingLegDetailList != null && floatingLegDetailList.size() == 1) {
			floatingLegDetail = floatingLegDetailList.get(0);
		}
		List<InvestmentSecurityEventDetail> originalFloatingLegDetailList = getOrderFloatingDetailListByEffectiveDate(event);
		InvestmentSecurityEventDetail originalFloatingLegDetail = null;
		if (originalFloatingLegDetailList != null && originalFloatingLegDetailList.size() == 1) {
			originalFloatingLegDetail = originalFloatingLegDetailList.get(0);
		}
		if (floatingLegDetail != null && floatingLegDetail.getEffectiveDate() != null) {
			if (floatingLegDetail.getReferenceRate() == null) {
				floatingLegDetail.setReferenceRate(BigDecimal.ZERO);
			}
			if (originalFloatingLegDetail != null) {
				originalFloatingLegDetail.setEffectiveDate(floatingLegDetail.getEffectiveDate());
				originalFloatingLegDetail.setReferenceRate(floatingLegDetail.getReferenceRate());
				floatingLegDetail = originalFloatingLegDetail;
			}
			else {
				floatingLegDetail.setEvent(event.getFloatingLeg());
			}
			getInvestmentSecurityEventService().saveInvestmentSecurityEventDetail(floatingLegDetail);
		}
		else if (originalFloatingLegDetail != null) {
			getInvestmentSecurityEventService().deleteInvestmentSecurityEventDetail(originalFloatingLegDetail.getId());
		}

		if (floatingLegDetail != null) {
			floatingLegDetail.setEvent(floatingLeg);
			getInvestmentSecurityEventService().saveInvestmentSecurityEventDetail(floatingLegDetail);
		}
	}


	private List<InvestmentSecurityEventDetail> getOrderFloatingDetailListByEffectiveDate(OtcInterestRateSwapEvent event) {
		List<InvestmentSecurityEventDetail> detailList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailListByEvent(event.getFloatingLeg().getId());
		return CollectionUtils.getStream(detailList).sorted(Comparator.comparing(InvestmentSecurityEventDetail::getEffectiveDate).reversed()).collect(Collectors.toList());
	}


	@Override
	@Transactional
	public void deleteOtcInterestRateSwapEvent(int id) {
		OtcInterestRateSwapEvent event = getOtcInterestRateSwapEventImpl(id, false);
		if (event.getFloatingDetailList() != null) {
			getInvestmentSecurityEventService().deleteInvestmentSecurityEventDetailList(event.getFloatingDetailList());
		}
		getAccountingSecurityEventRollupHandler().deleteAccountingSecurityEventRollup(event);
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public AccountingSecurityEventRollupHandler<OtcInterestRateSwapEvent> getAccountingSecurityEventRollupHandler() {
		return this.accountingSecurityEventRollupHandler;
	}


	public void setAccountingSecurityEventRollupHandler(AccountingSecurityEventRollupHandler<OtcInterestRateSwapEvent> accountingSecurityEventRollupHandler) {
		this.accountingSecurityEventRollupHandler = accountingSecurityEventRollupHandler;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
