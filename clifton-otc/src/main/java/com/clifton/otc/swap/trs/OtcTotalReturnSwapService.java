package com.clifton.otc.swap.trs;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.List;


/**
 * The <code>OtcTotalReturnSwapService</code> interface defines methods for working with
 * Total Returns Swaps (TRS)
 *
 * @author vgomelsky
 */
public interface OtcTotalReturnSwapService {

	/**
	 * Either Equity or Interest Leg id: Event Date bust be the same so can get both
	 * events this way.
	 *
	 * @param id
	 */
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public OtcTotalReturnSwapEvent getOtcTotalReturnSwapEvent(int id);


	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public List<OtcTotalReturnSwapEvent> getOtcTotalReturnSwapEventList(int securityId);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void saveOtcTotalReturnSwapEvent(OtcTotalReturnSwapEvent event);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void deleteOtcTotalReturnSwapEvent(int id);
}
