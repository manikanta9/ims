package com.clifton.otc.swap.trs;


import com.clifton.accounting.eventjournal.eventrollup.AccountingSecurityEventRollup;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;


/**
 * The <code>OtcTotalReturnSwapEvent</code> class combines Equity Leg, Interest Leg, and Dividend Leg.
 * Total Return Swap events into a single object: improves usability.
 *
 * @author vgomelsky
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class OtcTotalReturnSwapEvent extends AccountingSecurityEventRollup {


	@Override
	public String[] getEventLegTypes() {
		return new String[]{InvestmentSecurityEventType.EQUITY_LEG_PAYMENT, InvestmentSecurityEventType.INTEREST_LEG_PAYMENT, InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getEquityLeg() {
		return getEventLeg(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);
	}


	public void setEquityLeg(InvestmentSecurityEvent event) {
		setEventLeg(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT, event);
	}


	public BigDecimal getEquityPayment() {
		return getEventPayment(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getInterestLeg() {
		return getEventLeg(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);
	}


	public void setInterestLeg(InvestmentSecurityEvent event) {
		setEventLeg(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT, event);
	}


	public BigDecimal getInterestPayment() {
		return getEventPayment(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getDividendLeg() {
		return getEventLeg(InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT);
	}


	public void setDividendLeg(InvestmentSecurityEvent event) {
		setEventLeg(InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT, event);
	}


	public BigDecimal getDividendPayment() {
		return getEventPayment(InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT);
	}
}
