package com.clifton.otc.swap.irs;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.List;


public interface OtcInterestRateSwapService {

	////////////////////////////////////////////////////////////
	//////           Interest Rate Swaps                 ///////
	////////////////////////////////////////////////////////////


	/**
	 * Returns OtcInterestRateSwap object for the specified InvestmentSecurity id
	 *
	 * @param id
	 */
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public OtcInterestRateSwap getOtcInterestRateSwap(int id);


	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public void saveOtcInterestRateSwap(OtcInterestRateSwap swap);


	////////////////////////////////////////////////////////////////////////////
	////////         OtcInterestRateSwapEvent Business Methods        //////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public OtcInterestRateSwapEvent getOtcInterestRateSwapEvent(int id);


	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public List<OtcInterestRateSwapEvent> getOtcInterestRateSwapEventList(int securityId);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void saveOtcInterestRateSwapEvent(OtcInterestRateSwapEvent event);


	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void deleteOtcInterestRateSwapEvent(int id);
}
