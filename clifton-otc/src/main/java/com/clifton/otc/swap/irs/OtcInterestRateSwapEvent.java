package com.clifton.otc.swap.irs;


import com.clifton.accounting.eventjournal.eventrollup.AccountingSecurityEventRollup;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>OtcInterestRateSwapEvent</code> class combines Fixed Leg and Floating Leg
 * Interest Rate Swap events into a single object: improves usability.
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class OtcInterestRateSwapEvent extends AccountingSecurityEventRollup {

	private List<InvestmentSecurityEventDetail> floatingDetailList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getEventLegTypes() {
		return new String[]{InvestmentSecurityEventType.FLOATING_LEG_PAYMENT, InvestmentSecurityEventType.FIXED_LEG_PAYMENT};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getFloatingLeg() {
		return getEventLeg(InvestmentSecurityEventType.FLOATING_LEG_PAYMENT);
	}


	public void setFloatingLeg(InvestmentSecurityEvent event) {
		setEventLeg(InvestmentSecurityEventType.FLOATING_LEG_PAYMENT, event);
	}


	public BigDecimal getFloatingPayment() {
		return getEventPayment(InvestmentSecurityEventType.FLOATING_LEG_PAYMENT);
	}


	public InvestmentSecurityEvent getFixedLeg() {
		return getEventLeg(InvestmentSecurityEventType.FIXED_LEG_PAYMENT);
	}


	public void setFixedLeg(InvestmentSecurityEvent event) {
		setEventLeg(InvestmentSecurityEventType.FIXED_LEG_PAYMENT, event);
	}


	public BigDecimal getFixedPayment() {
		return getEventPayment(InvestmentSecurityEventType.FIXED_LEG_PAYMENT);
	}


	public List<InvestmentSecurityEventDetail> getFloatingDetailList() {
		return this.floatingDetailList;
	}


	public void setFloatingDetailList(List<InvestmentSecurityEventDetail> floatingDetailList) {
		this.floatingDetailList = floatingDetailList;
	}
}
