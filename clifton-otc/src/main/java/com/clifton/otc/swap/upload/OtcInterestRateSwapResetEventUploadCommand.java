package com.clifton.otc.swap.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>OtcInterestRateSwapResetEventUploadCommand</code> ...
 *
 * @author manderson
 */
public class OtcInterestRateSwapResetEventUploadCommand extends FileUploadCommand<OtcInterestRateSwapResetEvent> {

	public static final String[] REQUIRED_PROPERTIES = {"symbol", "fixedLegRate", "accrualStartDate", "exDate", "accrualEndDate", "paymentDate"};

	private Map<String, InvestmentSecurity> securityMap = new HashMap<>();
	private Map<Integer, Short> securityFixingCalendarMap = new HashMap<>();
	private Map<Integer, Integer> securityFixingDaysMap = new HashMap<>();
	private Map<Integer, List<InvestmentSecurityEvent>> securityExistingEventsListMap = new HashMap<>();

	private int eventSkipCount;


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<OtcInterestRateSwapResetEvent> getUploadBeanClass() {
		return OtcInterestRateSwapResetEvent.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	////////////////////////////////////////////////////////////////////////////////


	public void setupForUpload() {
		this.securityMap = new HashMap<>();
		this.securityFixingCalendarMap = new HashMap<>();
		this.securityFixingDaysMap = new HashMap<>();
		this.securityExistingEventsListMap = new HashMap<>();
		this.eventSkipCount = 0;
		setUploadResultsString(null);
	}


	public void addEventSkipCount() {
		this.eventSkipCount = this.eventSkipCount + 1;
	}


	public void addSecurity(OtcInterestRateSwapResetEvent event, InvestmentSecurity security) {
		this.securityMap.put(event.getSecurityUniqueMapKey(), security);
	}


	public InvestmentSecurity getSecurity(OtcInterestRateSwapResetEvent event) {
		if (this.securityMap.containsKey(event.getSecurityUniqueMapKey())) {
			return this.securityMap.get(event.getSecurityUniqueMapKey());
		}
		return null;
	}


	public void addFixingCalendar(InvestmentSecurity security, Short calendarId) {
		this.securityFixingCalendarMap.put(security.getId(), calendarId);
	}


	public Short getFixingCalendar(InvestmentSecurity security) {
		if (this.securityFixingCalendarMap.containsKey(security.getId())) {
			return this.securityFixingCalendarMap.get(security.getId());
		}
		return null;
	}


	public void addFixingDays(InvestmentSecurity security, Integer fixingDays) {
		this.securityFixingDaysMap.put(security.getId(), fixingDays);
	}


	public Integer getFixingDays(InvestmentSecurity security) {
		if (this.securityFixingDaysMap.containsKey(security.getId())) {
			return this.securityFixingDaysMap.get(security.getId());
		}
		return null;
	}


	public void addInvestmentSecurityEventList(InvestmentSecurity security, List<InvestmentSecurityEvent> eventList) {
		this.securityExistingEventsListMap.put(security.getId(), eventList == null ? new ArrayList<>() : eventList);
	}


	public InvestmentSecurityEvent getExistingEventForInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (this.securityExistingEventsListMap.containsKey(event.getSecurity().getId())) {
			List<InvestmentSecurityEvent> list = this.securityExistingEventsListMap.get(event.getSecurity().getId());
			list = BeanUtils.filter(list, securityEvent -> securityEvent.getType() == null ? null : securityEvent.getType().getName(), event.getType().getName());
			list = BeanUtils.filter(list, InvestmentSecurityEvent::getEventDate, event.getEventDate());
			list = BeanUtils.filter(list, InvestmentSecurityEvent::getEventDescription, event.getEventDescription());
			if (CollectionUtils.getSize(list) > 1) {
				throw new ValidationException("Found multiple [" + event.getType().getName() + "] events for Security [" + event.getSecurity().getSymbol() + "] on ["
						+ DateUtils.fromDateShort(event.getEventDate()) + "].");
			}
			return CollectionUtils.getFirstElement(list);
		}
		return null;
	}


	public int getEventSkipCount() {
		return this.eventSkipCount;
	}


	public void setEventSkipCount(int eventSkipCount) {
		this.eventSkipCount = eventSkipCount;
	}
}
