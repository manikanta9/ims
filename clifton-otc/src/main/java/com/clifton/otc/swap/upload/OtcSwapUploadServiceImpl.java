package com.clifton.otc.swap.upload;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Service
public class OtcSwapUploadServiceImpl implements OtcSwapUploadService {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private FileUploadHandler fileUploadHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadOtcInterestRateSwapResetEventUploadFile(OtcInterestRateSwapResetEventUploadCommand uploadCommand) {
		List<OtcInterestRateSwapResetEvent> beanList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);

		List<InvestmentSecurityEvent> eventList = new ArrayList<>();
		List<InvestmentSecurityEventDetail> eventDetailList = new ArrayList<>();

		// Clears all data maps for fresh upload
		uploadCommand.setupForUpload();

		for (OtcInterestRateSwapResetEvent resetEvent : CollectionUtils.getIterable(beanList)) {
			// Security Info Needed
			setupInvestmentSecurityInfoForResetEvent(resetEvent, uploadCommand);
			processEventsForReset(resetEvent, uploadCommand, eventList, eventDetailList);
		}
		saveEventsFromUpload(eventList, eventDetailList);

		uploadCommand.appendResult("Investment Security Events: [" + CollectionUtils.getSize(eventList) + "] added" + (FileUploadExistingBeanActions.UPDATE == uploadCommand.getExistingBeans() ? "/updated" : ""));
		uploadCommand.appendResult("Investment Security Event Details (Floating Leg): [" + CollectionUtils.getSize(eventDetailList) + "] added"
				+ (FileUploadExistingBeanActions.UPDATE == uploadCommand.getExistingBeans() ? "/updated" : ""));
		if (uploadCommand.getEventSkipCount() > 0) {
			uploadCommand.appendResult("[" + uploadCommand.getEventSkipCount() + "] existing events skipped.");
		}
	}


	@Transactional
	protected void saveEventsFromUpload(List<InvestmentSecurityEvent> eventList, List<InvestmentSecurityEventDetail> eventDetailList) {
		getInvestmentSecurityEventService().saveInvestmentSecurityEventList(eventList, null);
		getInvestmentSecurityEventService().saveInvestmentSecurityEventDetailList(eventDetailList);
	}


	private void processEventsForReset(OtcInterestRateSwapResetEvent resetEvent, OtcInterestRateSwapResetEventUploadCommand uploadCommand, List<InvestmentSecurityEvent> eventList,
	                                   List<InvestmentSecurityEventDetail> eventDetailList) {
		// Populate Events
		InvestmentSecurityEvent fixedLeg = populateEmptyEventForSecurity(resetEvent, uploadCommand, InvestmentSecurityEventType.FIXED_LEG_PAYMENT);
		if (fixedLeg == null) {
			uploadCommand.addEventSkipCount();
		}
		else {
			if (resetEvent.getFixedLegRate() != null) {
				fixedLeg.setAfterEventValue(resetEvent.getFixedLegRate());
			}
			eventList.add(fixedLeg);
		}

		InvestmentSecurityEvent floatingLeg = populateEmptyEventForSecurity(resetEvent, uploadCommand, InvestmentSecurityEventType.FLOATING_LEG_PAYMENT);
		if (floatingLeg == null) {
			uploadCommand.addEventSkipCount();
		}
		else {
			if (resetEvent.getFloatingLegRate() != null) {
				floatingLeg.setAfterEventValue(resetEvent.getFloatingLegRate());
			}

			if (resetEvent.getFixingDate() != null) {
				InvestmentSecurityEventDetail floatingDetail = new InvestmentSecurityEventDetail();
				floatingDetail.setEvent(floatingLeg);
				floatingDetail.setReferenceRate(resetEvent.getFixingRate());
				floatingDetail.setEffectiveDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(resetEvent.getFixingDate(), uploadCommand.getFixingCalendar(floatingLeg.getSecurity())),
						uploadCommand.getFixingDays(floatingLeg.getSecurity())));
				if (!floatingLeg.isNewBean()) {
					InvestmentSecurityEventDetail existingDetail = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailByEventAndEffectiveDate(floatingLeg.getId(),
							floatingDetail.getEffectiveDate());
					// NOTE: DO NOT HAVE TO CHECK IF SKIPPING HERE BECAUSE THEN EVENT ITSELF WOULD HAVE BEEN SKIPPED
					if (existingDetail != null) {
						floatingDetail.setId(existingDetail.getId());
					}
				}
				eventDetailList.add(floatingDetail);
			}
			eventList.add(floatingLeg);
		}
	}


	private InvestmentSecurityEvent populateEmptyEventForSecurity(OtcInterestRateSwapResetEvent resetEvent, OtcInterestRateSwapResetEventUploadCommand uploadCommand, String typeName) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setType(getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(typeName));
		event.setStatus(event.getType().getForceEventStatus());
		event.setSecurity(uploadCommand.getSecurity(resetEvent));

		event.setBeforeAndAfterEventValue(BigDecimal.ZERO);

		event.setDeclareDate(resetEvent.getAccrualStartDate());
		event.setRecordDate(resetEvent.getAccrualEndDate());
		event.setExDate(resetEvent.getExDate());
		event.setPaymentDate(resetEvent.getPaymentDate());

		InvestmentSecurityEvent existingEvent = uploadCommand.getExistingEventForInvestmentSecurityEvent(event);
		if (existingEvent != null) {
			if (FileUploadExistingBeanActions.SKIP == uploadCommand.getExistingBeans()) {
				event = null;
			}
			else {
				event.setId(existingEvent.getId());
			}
		}
		return event;
	}


	private void setupInvestmentSecurityInfoForResetEvent(OtcInterestRateSwapResetEvent resetEvent, OtcInterestRateSwapResetEventUploadCommand uploadCommand) {
		InvestmentSecurity security = uploadCommand.getSecurity(resetEvent);
		// If not set up yet - look up the security and also populate all relevant information needed for look ups
		if (security == null) {
			List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(resetEvent.getSymbol());
			securityList = BeanUtils.filter(securityList, swap -> {
				if (!StringUtils.isEmpty(resetEvent.getHierarchyName()) && !InvestmentUtils.isSecurityInHierarchy(swap, resetEvent.getHierarchyName())) {
					return false;
				}
				return InvestmentUtils.isSecurityOfType(swap, InvestmentType.SWAPS);
			});
			if (CollectionUtils.isEmpty(securityList)) {
				throw new ValidationException("Unable to find Swap with Symbol [" + resetEvent.getSymbol() + "]" + (!StringUtils.isEmpty(resetEvent.getHierarchyName()) ? " in hierarchy [" + resetEvent.getHierarchyName() + "]" : ""));
			}
			if (CollectionUtils.getSize(securityList) > 1) {
				throw new ValidationException("Found multiple Swaps with Symbol [" + resetEvent.getSymbol() + "]" + (!StringUtils.isEmpty(resetEvent.getHierarchyName()) ? " in hierarchy [" + resetEvent.getHierarchyName() + "]" : ""));
			}
			security = securityList.get(0);
			uploadCommand.addSecurity(resetEvent, security);

			// Setup Relevant Security Information
			uploadCommand.addFixingCalendar(
					security,
					((Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
							InvestmentSecurity.CUSTOM_FIELD_FIXING_CALENDAR, false)).shortValue());

			uploadCommand.addFixingDays(
					security,
					(Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CYCLE,
							false));

			// Only get existing events if not set up to Insert Always
			if (FileUploadExistingBeanActions.INSERT != uploadCommand.getExistingBeans()) {
				InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
				eventSearchForm.setSecurityId(security.getId());
				uploadCommand.addInvestmentSecurityEventList(security, getInvestmentSecurityEventService().getInvestmentSecurityEventList(eventSearchForm));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////                Getter & Setters                  ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
