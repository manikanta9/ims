package com.clifton.otc.swap;


import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class OtcSwapServiceImpl implements OtcSwapService {

	private AccountingCommissionService accountingCommissionService;

	private InvestmentInstrumentCopyService investmentInstrumentCopyService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataSourceService marketDataSourceService;

	private SystemNoteService systemNoteService;


	////////////////////////////////////////////////////////////
	////////                Swaps                      /////////
	////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void saveOtcInvestmentSecuritySwap(InvestmentSecurity bean, Integer copyFromSecurityId, boolean copySymbolOverrides, boolean copyEvents, boolean copyCommissions, boolean copyNotesFromToday) {
		// Save Security & Custom Fields (Properly creates new instruments as well for 1:1 securities)
		getInvestmentInstrumentCopyService().saveInvestmentSecurityFromTemplate(bean, copyFromSecurityId);

		// Copy Events (only if the events aren't already flagged on the hierarchy to system manage copying events)
		if (copyEvents && copyFromSecurityId != null && !bean.getInstrument().getHierarchy().isEventSameForInstrumentSecurities()) {
			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(copyFromSecurityId);
			List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
			if (!CollectionUtils.isEmpty(eventList)) {
				List<InvestmentSecurityEvent> newEventList = new ArrayList<>();
				for (InvestmentSecurityEvent event : eventList) {
					if (DateUtils.isDateBetween(event.getEventDate(), bean.getStartDate(), bean.getLastDeliveryOrEndDate(), false)) {
						InvestmentSecurityEvent newEvent = BeanUtils.cloneBean(event, false, false);
						newEvent.setId(null);
						newEvent.setSecurity(bean);

						// Validation on save of the event - Need to round so that scale matches what is excepted
						// Not sure if there is a better way to handle this?  i.e. 5.0000000000 would fail for Interest Leg Payment which excepts a scale of 8
						newEvent.setBeforeEventValue(MathUtils.round(event.getBeforeEventValue(), event.getType().getDecimalPrecision()));
						newEvent.setAfterEventValue(MathUtils.round(event.getAfterEventValue(), event.getType().getDecimalPrecision()));

						newEventList.add(newEvent);
					}
				}
				if (!CollectionUtils.isEmpty(newEventList)) {
					getInvestmentSecurityEventService().saveInvestmentSecurityEventList(newEventList, null);
				}
			}
		}

		// Copy Symbol Overrides
		if (copySymbolOverrides && copyFromSecurityId != null) {
			List<MarketDataSourceSecurity> symbolList = getMarketDataSourceService().getMarketDataSourceSecurityListBySecurity(copyFromSecurityId);
			if (!CollectionUtils.isEmpty(symbolList)) {
				for (MarketDataSourceSecurity symbol : symbolList) {
					MarketDataSourceSecurity newSymbol = BeanUtils.cloneBean(symbol, false, false);
					newSymbol.setSecurity(bean);
					getMarketDataSourceService().saveMarketDataSourceSecurity(newSymbol);
				}
			}
		}

		// Copy Commission Definitions
		if (copyCommissions && copyFromSecurityId != null) {
			getAccountingCommissionService().copyAccountingCommissionDefinitionListForSecurity(copyFromSecurityId, bean);
		}

		// Copy (Add Link to Notes)
		if (copyNotesFromToday && copyFromSecurityId != null) {
			SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
			searchForm.setEntityTableName("InvestmentSecurity");
			searchForm.setFkFieldId(MathUtils.getNumberAsLong(copyFromSecurityId));
			// Restrict to notes with create date after yesterday
			searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN, DateUtils.getEndOfDay(DateUtils.addDays(new Date(), -1))));
			List<SystemNote> noteList = getSystemNoteService().getSystemNoteListForEntity(searchForm);
			for (SystemNote note : CollectionUtils.getIterable(noteList)) {
				if (note.getNoteType().isLinkToMultipleAllowed()) {
					getSystemNoteService().saveSystemNoteLink(note.getId(), bean.getId());
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentCopyService getInvestmentInstrumentCopyService() {
		return this.investmentInstrumentCopyService;
	}


	public void setInvestmentInstrumentCopyService(InvestmentInstrumentCopyService investmentInstrumentCopyService) {
		this.investmentInstrumentCopyService = investmentInstrumentCopyService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingCommissionService getAccountingCommissionService() {
		return this.accountingCommissionService;
	}


	public void setAccountingCommissionService(AccountingCommissionService accountingCommissionService) {
		this.accountingCommissionService = accountingCommissionService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}
}
