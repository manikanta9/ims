package com.clifton.otc.swap.trs;


import com.clifton.accounting.eventjournal.eventrollup.AccountingSecurityEventRollupHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>OtcTotalReturnSwapServiceImpl</code> class provides basic implementation of the OtcTotalReturnSwapService interface.
 *
 * @author vgomelsky
 */
@Service
public class OtcTotalReturnSwapServiceImpl implements OtcTotalReturnSwapService {

	private AccountingSecurityEventRollupHandler<OtcTotalReturnSwapEvent> accountingSecurityEventRollupHandler;

	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;

	private static final BigDecimal PRICE_DIFFERENCE_THRESHOLD = new BigDecimal("0.05");


	////////////////////////////////////////////////////////////////////////////
	/////////         OtcTotalReturnSwapEvent Business Methods        //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OtcTotalReturnSwapEvent getOtcTotalReturnSwapEvent(int id) {
		return getOtcTotalReturnSwapEventImpl(id, true);
	}


	private OtcTotalReturnSwapEvent getOtcTotalReturnSwapEventImpl(int securityEventId, boolean populatePayments) {
		return getAccountingSecurityEventRollupHandler().getAccountingSecurityEventRollup(OtcTotalReturnSwapEvent.class, securityEventId, populatePayments);
	}


	@Override
	public List<OtcTotalReturnSwapEvent> getOtcTotalReturnSwapEventList(int securityId) {
		return getAccountingSecurityEventRollupHandler().getAccountingSecurityEventRollupList(OtcTotalReturnSwapEvent.class, securityId);
	}


	@Override
	@Transactional
	public void saveOtcTotalReturnSwapEvent(OtcTotalReturnSwapEvent event) {
		final InvestmentSecurityEvent equityLeg = event.getEquityLeg();
		InvestmentSecurityEvent interestLeg = event.getInterestLeg();
		InvestmentSecurity swap = equityLeg.getSecurity();

		equityLeg.setExDate(DateUtils.addDays(equityLeg.getAccrualEndDate(), 1));
		if (equityLeg.getBeforeEventValue() == null) {
			equityLeg.setBeforeEventValue(BigDecimal.ZERO);
		}
		if (equityLeg.getAfterEventValue() == null) {
			equityLeg.setAfterEventValue(BigDecimal.ZERO);
		}

		if (getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForSecurity(swap.getId(), InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT)) {
			InvestmentSecurityEvent dividendLeg = event.getDividendLeg();
			dividendLeg.setDeclareDate(equityLeg.getDeclareDate());
			dividendLeg.setExDate(equityLeg.getExDate());
			dividendLeg.setRecordDate(equityLeg.getRecordDate());
			dividendLeg.setEventDate(equityLeg.getEventDate());
			dividendLeg.setActualSettlementDate(equityLeg.getActualSettlementDate());
		}
		else {
			event.setDividendLeg(null);
		}

		interestLeg.setExDate(equityLeg.getExDate());
		AccrualDateCalculators calculatorType = InvestmentCalculatorUtils.getAccrualDateCalculator(event.getSecurity());
		if (AccrualDateCalculators.SETTLEMENT_CYCLE == calculatorType) {
			// AccrualMethods accrualMethod = InvestmentCalculatorUtils.getAccrualMethod(event.getSecurity());
			// TODO: Greg L is looking into OIS Compound and how to change Swap configuration. When he's done, use that logic to do decide whether to allow overrides or not
			//Issue with Swap: 18920479 Accrual End Date was not generated correctly, and when trying to override it, the following logic always reverts it back.  Changed to only check if null so it can be overridden
			//if (accrualMethod == AccrualMethods.DAYCOUNT || interestLeg.getAccrualEndDate() == null) {
			if (interestLeg.getAccrualEndDate() == null) {
				// accrual using swap Settlement date convention
				interestLeg.setAccrualEndDate(equityLeg.getEventDate());
			}
		}
		else if (interestLeg.getAccrualEndDate() == null) {
			// accrual using Transaction date
			interestLeg.setAccrualEndDate(equityLeg.getAccrualEndDate());
		}
		interestLeg.setEventDate(equityLeg.getEventDate());
		interestLeg.setActualSettlementDate(equityLeg.getActualSettlementDate());
		if (interestLeg.getBeforeEventValue() == null) {
			interestLeg.setBeforeEventValue(BigDecimal.ZERO);
		}
		interestLeg.setAfterEventValue(interestLeg.getBeforeEventValue());

		// same description from the equity leg is used for both fields
		if (!StringUtils.isEmpty(equityLeg.getEventDescription())) {
			interestLeg.setEventDescription(equityLeg.getEventDescription());
		}

		// validate inputs
		ValidationUtils.assertTrue(equityLeg.getAccrualEndDate().before(equityLeg.getEventDate()),
				"Period end Valuation Date cannot be after Payment Date. Cannot calculate Payment Amount without period end Valuation Price.");
		ValidationUtils.assertTrue(equityLeg.getActualSettlementDate() == null || DateUtils.isDateAfterOrEqual(equityLeg.getActualSettlementDate(), equityLeg.getPaymentDate()),
				"Actual Settlement Date must be equal or after Contract Settlement Date.");

		MarketDataPriceFieldMapping priceFieldMapping = getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(swap.getInstrument());
		MarketDataField closingPriceField = MarketDataPriceFieldTypes.CLOSING.getPriceField(priceFieldMapping, getMarketDataFieldService());
		MarketDataValue startingPrice = null;
		MarketDataValue endingPrice = null;

		if (!equityLeg.isNewBean()) {
			// get original equity leg so that we can see if before/after values changed
			InvestmentSecurityEvent originalEquityLeg = DaoUtils.executeDetached(true, () -> getInvestmentSecurityEventService().getInvestmentSecurityEvent(equityLeg.getId()));

			if (!MathUtils.isEqual(equityLeg.getBeforeEventValue(), originalEquityLeg.getBeforeEventValue())) {
				// DO NOT allow updating before price if it mismatches after price of previous event
				InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
				searchForm.setSecurityId(swap.getId());
				searchForm.setTypeName(equityLeg.getType().getName());
				searchForm.setRecordDate(equityLeg.getDeclareDate());
				InvestmentSecurityEvent previousEquityLeg = CollectionUtils.getOnlyElement(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm));

				if (previousEquityLeg != null && !MathUtils.isEqual(equityLeg.getBeforeEventValue(), previousEquityLeg.getAfterEventValue())) {
					throw new ValidationException("Valuation Price " + equityLeg.getBeforeEventValue() + " must match Next Valuation Price for previous event: "
							+ previousEquityLeg.getAfterEventValue());
				}


				startingPrice = getClosingPriceValue(swap, closingPriceField, priceFieldMapping, equityLeg.getAccrualStartDate());
			}

			if (!MathUtils.isEqual(equityLeg.getAfterEventValue(), originalEquityLeg.getAfterEventValue())) {
				// when updating existing Equity Leg end price, also update start price for the next event, if any
				InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
				searchForm.setSecurityId(swap.getId());
				searchForm.setTypeName(equityLeg.getType().getName());
				searchForm.setDeclareDate(equityLeg.getAccrualEndDate());
				InvestmentSecurityEvent nextEquityLeg = CollectionUtils.getOnlyElement(getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm));


				endingPrice = getClosingPriceValue(swap, closingPriceField, priceFieldMapping, equityLeg.getAccrualEndDate());
				if (endingPrice == null && MathUtils.isNullOrZero(originalEquityLeg.getAfterEventValue())) {
					// when price changes from 0, auto create price entry if one doesn't exist
					endingPrice = new MarketDataValue();
					endingPrice.setDataField(closingPriceField);
					endingPrice.setDataSource(MarketDataPriceFieldTypes.CLOSING.getPriceDataSource(priceFieldMapping));
					if (endingPrice.getDataSource() == null) {
						ValidationUtils.assertNotNull(swap.getBusinessCompany(), "Counterparty field must be populated for " + swap.getLabel());
						MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSourceByCompany(swap.getBusinessCompany());
						endingPrice.setDataSource(dataSource);
					}
					endingPrice.setInvestmentSecurity(swap);
					endingPrice.setMeasureDate(equityLeg.getRecordDate());
					if (closingPriceField.isTimeSensitive()) {
						endingPrice.setMeasureTime(new Time(0));
					}
				}

				if (nextEquityLeg != null) {
					// found next event: rely on before value of next event to be previous value
					if (!MathUtils.isEqual(equityLeg.getAfterEventValue(), nextEquityLeg.getBeforeEventValue())) {
						// update the price for the NEXT equity leg
						nextEquityLeg.setBeforeEventValue(equityLeg.getAfterEventValue());
						getInvestmentSecurityEventService().saveInvestmentSecurityEvent(nextEquityLeg);
					}
				}
			}
		}
		getAccountingSecurityEventRollupHandler().saveAccountingSecurityEventRollup(event);
		saveOtcTotalReturnSwapPriceUpdates(event, startingPrice, endingPrice);
	}


	/**
	 * NOTE: The retrievals above cannot be marked as Transactional because the MarketDataValue retrievals above appear to later (when saving) return the updated object
	 * in saveMarketDataValueWithOptions retrievals which would cause NO market data value update when it should. So we do all retrievals above, and then use transactions around the saves only.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void saveOtcTotalReturnSwapPriceUpdates(OtcTotalReturnSwapEvent event, MarketDataValue startingPrice, MarketDataValue endingPrice) {
		// update prices if the change is within 0.05 threshold
		saveMarketDataValueIfWithinThreshold(startingPrice, event.getEquityLeg().getBeforeEventValue());
		saveMarketDataValueIfWithinThreshold(endingPrice, event.getEquityLeg().getAfterEventValue());
	}


	@Override
	@Transactional
	public void deleteOtcTotalReturnSwapEvent(int id) {
		OtcTotalReturnSwapEvent event = getOtcTotalReturnSwapEventImpl(id, false);
		getAccountingSecurityEventRollupHandler().deleteAccountingSecurityEventRollup(event);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////                   Helper Methods                 ///////////////
	////////////////////////////////////////////////////////////////////////////////


	// Note: Once price field mappings are updated to require mappings for all - this can be refactored to just take the mapping since dataField currently would perform additional look up if no mapping
	private MarketDataValue getClosingPriceValue(InvestmentSecurity swap, MarketDataField dataField, MarketDataPriceFieldMapping priceFieldMapping, Date measureDate) {
		MarketDataValueSearchForm priceSearchForm = new MarketDataValueSearchForm();
		priceSearchForm.setInvestmentSecurityId(swap.getId());
		priceSearchForm.setDataFieldId(dataField.getId());
		priceSearchForm.setDataSourceId(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(priceFieldMapping));
		priceSearchForm.setMeasureDate(measureDate);
		priceSearchForm.setOrderBy("measureDate:desc#measureTime:desc");
		priceSearchForm.setLimit(1); // only use the first price at this point
		return CollectionUtils.getFirstElement(getMarketDataFieldService().getMarketDataValueList(priceSearchForm));
	}


	/**
	 * Updates MeasureValue to expectedValue and saves if object is a new bean, or difference between existing value and expected value is within threshold
	 */
	private void saveMarketDataValueIfWithinThreshold(MarketDataValue priceValue, BigDecimal expectedValue) {
		// update the price if the change is within 0.05 threshold
		if (priceValue != null
				&& (priceValue.isNewBean() || MathUtils.subtract(priceValue.getMeasureValue(), expectedValue).abs().compareTo(PRICE_DIFFERENCE_THRESHOLD) <= 0)) {
			priceValue.setMeasureValue(expectedValue);
			getMarketDataFieldService().saveMarketDataValue(priceValue);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public AccountingSecurityEventRollupHandler<OtcTotalReturnSwapEvent> getAccountingSecurityEventRollupHandler() {
		return this.accountingSecurityEventRollupHandler;
	}


	public void setAccountingSecurityEventRollupHandler(AccountingSecurityEventRollupHandler<OtcTotalReturnSwapEvent> accountingSecurityEventRollupHandler) {
		this.accountingSecurityEventRollupHandler = accountingSecurityEventRollupHandler;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
