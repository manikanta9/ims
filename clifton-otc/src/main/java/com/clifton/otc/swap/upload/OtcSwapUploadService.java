package com.clifton.otc.swap.upload;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;


/**
 * The <code>OtcInterestRateSwapService</code> ...
 *
 * @author manderson
 */
public interface OtcSwapUploadService {

	/**
	 * Custom upload to populate {@link OtcInterestRateSwapResetEvent} beans and then
	 * process those beans to create actual {@link InvestmentSecurityEvent} objects to save
	 */
	@SecureMethod(dtoClass = InvestmentSecurityEvent.class)
	public void uploadOtcInterestRateSwapResetEventUploadFile(OtcInterestRateSwapResetEventUploadCommand uploadCommand);
}
