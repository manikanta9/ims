package com.clifton.otc.swap.irs;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.otc.OtcSecurity;

import java.math.BigDecimal;


/**
 * The <code>OtcInterestRateSwap</code> class represents an Interest Rate Swap.
 *
 * @author vgomelsky
 */
@NonPersistentObject
public class OtcInterestRateSwap extends OtcSecurity {

	private BigDecimal notional;

	/**
	 * Contract Structure: specifies whether the client receives fixed leg and pays floating leg to the counterparty or vice versa.
	 */
	private boolean fixedLegReceivedByClient;
	private BigDecimal fixedCouponPercent;


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public boolean isFixedLegReceivedByClient() {
		return this.fixedLegReceivedByClient;
	}


	public void setFixedLegReceivedByClient(boolean fixedLegReceivedByClient) {
		this.fixedLegReceivedByClient = fixedLegReceivedByClient;
	}


	public BigDecimal getFixedCouponPercent() {
		return this.fixedCouponPercent;
	}


	public void setFixedCouponPercent(BigDecimal fixedCouponPercent) {
		this.fixedCouponPercent = fixedCouponPercent;
	}
}
