package com.clifton.otc.swap;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>OtcSwapService</code> interface defines methods for working with Swaps.
 *
 * @author vgomelsky
 */
public interface OtcSwapService {

	////////////////////////////////////////////////////////////
	////////                  Swaps                      ///////
	////////////////////////////////////////////////////////////


	/**
	 * Special Save method that creates a swap from existing with options to copy additional information
	 * <p/>
	 * Generic to support all types of swaps - what/how copies are done are based on parameters passed
	 *
	 * @param bean
	 * @param copyFromSecurityId  // Allowed to be null to support creating a new security
	 * @param copySymbolOverrides // Used only for CDS Swaps
	 * @param copyEvents          // If set and copyFromSecurityId is not null will also copy events where event date is between security start/end date (NOTE: copyEvents in UI is available for TRS, not CDS because CDS hierarchy automatically copies events)
	 * @param copyCommissions     // If set will also copy commission definitions defined for the copy from to the copy to
	 * @param copyNotesFromToday  // If set and copyFromSecurityId is not null will also add a link to all notes for the from security created today to the new security (Does not actually copy, just creates another link, so is supported by security notes types that support link to multiple (all others skipped)
	 */
	@SecureMethod(dtoClass = InvestmentSecurity.class)
	public void saveOtcInvestmentSecuritySwap(InvestmentSecurity bean, Integer copyFromSecurityId, boolean copySymbolOverrides, boolean copyEvents, boolean copyCommissions, boolean copyNotesFromToday);
}
