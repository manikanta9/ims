package com.clifton.otc.swap.upload;


import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>OtcInterestRateSwapResetEvent</code> is a custom class used for uploading reset events
 * into the system
 *
 * @author manderson
 */
public class OtcInterestRateSwapResetEvent {

	/**
	 * Security symbol & hierarchy are used to look up the security in the system
	 * Hierarchy is required only if there are multiple swaps in the system with the same symbol
	 * under different hierarchies
	 */
	private String symbol;

	private String hierarchyName;

	private BigDecimal fixedLegRate;

	/**
	 * Leave blank (will default to 0) or Enter "0" if the rate is not known
	 */
	private BigDecimal floatingLegRate;

	/**
	 * Declare Date
	 */
	private Date accrualStartDate;

	private Date exDate;

	/**
	 * Record Date
	 */
	private Date accrualEndDate;
	private Date paymentDate;

	/**
	 * Optional date in the middle of Floating Leg period when new interest rate for the second part
	 * of the period is determined.
	 * Settlement Cycle will be used to calculate the date when the new rate becomes effective.
	 */
	private Date fixingDate;

	/**
	 * Leave blank (will default to 0) or Enter "0" if the rate is not known
	 */
	private BigDecimal fixingRate;


	///////////////////////////////////////////////


	public String getSecurityUniqueMapKey() {
		if (StringUtils.isEmpty(getHierarchyName())) {
			return getSymbol();
		}
		return getHierarchyName() + "_" + getSymbol();
	}


	///////////////////////////////////////////////


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getHierarchyName() {
		return this.hierarchyName;
	}


	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}


	public BigDecimal getFixedLegRate() {
		return this.fixedLegRate;
	}


	public void setFixedLegRate(BigDecimal fixedLegRate) {
		this.fixedLegRate = fixedLegRate;
	}


	public BigDecimal getFloatingLegRate() {
		return this.floatingLegRate;
	}


	public void setFloatingLegRate(BigDecimal floatingLegRate) {
		this.floatingLegRate = floatingLegRate;
	}


	public Date getAccrualStartDate() {
		return this.accrualStartDate;
	}


	public void setAccrualStartDate(Date accrualStartDate) {
		this.accrualStartDate = accrualStartDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getAccrualEndDate() {
		return this.accrualEndDate;
	}


	public void setAccrualEndDate(Date accrualEndDate) {
		this.accrualEndDate = accrualEndDate;
	}


	public Date getPaymentDate() {
		return this.paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}


	public Date getFixingDate() {
		return this.fixingDate;
	}


	public void setFixingDate(Date fixingDate) {
		this.fixingDate = fixingDate;
	}


	public BigDecimal getFixingRate() {
		return this.fixingRate;
	}


	public void setFixingRate(BigDecimal fixingRate) {
		this.fixingRate = fixingRate;
	}
}
