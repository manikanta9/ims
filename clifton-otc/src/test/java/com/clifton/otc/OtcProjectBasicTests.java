package com.clifton.otc;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;


/**
 * The <code>OtcProjectBasicTests</code> ...
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OtcProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "otc";
	}


	@Override
	protected boolean isBeanSkipped(String beanName) {
		switch (beanName) {
			case "investmentSecurityEventRetrieverLocatorMapInsert":
				return true;
			case "otcTotalReturnSwapEvent":
				return true;
			default:
				break;
		}
		return super.isBeanSkipped(beanName);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveOtcInterestRateSwapEvent");
		ignoredVoidSaveMethodSet.add("saveOtcInterestRateSwap");
		ignoredVoidSaveMethodSet.add("saveOtcInvestmentSecuritySwap");
		ignoredVoidSaveMethodSet.add("saveOtcTotalReturnSwapEvent");
		return ignoredVoidSaveMethodSet;
	}
}
