package com.clifton.collateral;

import com.clifton.collateral.balance.CollateralBalanceCategories;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.Workflow;

import java.util.Date;


/**
 * @author terrys
 */
public final class CollateralTypeBuilder {

	private String name;
	private short createUserId;
	private String description;
	private Date createDate;
	private short updateUserId;
	private Date updateDate;
	private Short id;
	private boolean bookable;
	private boolean collateralInLocalCurrencySupported;
	private int transferDateOffset;
	private InvestmentAccountGroup accountGroup;
	private Workflow workflow;
	private SystemTable detailTable;
	private SystemBeanType systemBeanType;
	private CollateralBalanceCategories category;


	private CollateralTypeBuilder() {
	}


	public static CollateralTypeBuilder aCollateralType() {
		return new CollateralTypeBuilder();
	}


	public CollateralTypeBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public CollateralTypeBuilder withCreateUserId(short createUserId) {
		this.createUserId = createUserId;
		return this;
	}


	public CollateralTypeBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public CollateralTypeBuilder withCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}


	public CollateralTypeBuilder withUpdateUserId(short updateUserId) {
		this.updateUserId = updateUserId;
		return this;
	}


	public CollateralTypeBuilder withUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}


	public CollateralTypeBuilder withId(Short id) {
		this.id = id;
		return this;
	}


	public CollateralTypeBuilder withBookable(boolean bookable) {
		this.bookable = bookable;
		return this;
	}


	public CollateralTypeBuilder withCollateralInLocalCurrencySupported(boolean collateralInLocalCurrencySupported) {
		this.collateralInLocalCurrencySupported = collateralInLocalCurrencySupported;
		return this;
	}


	public CollateralTypeBuilder withTransferDateOffset(int transferDateOffset) {
		this.transferDateOffset = transferDateOffset;
		return this;
	}


	public CollateralTypeBuilder withAccountGroup(InvestmentAccountGroup accountGroup) {
		this.accountGroup = accountGroup;
		return this;
	}


	public CollateralTypeBuilder withWorkflow(Workflow workflow) {
		this.workflow = workflow;
		return this;
	}


	public CollateralTypeBuilder withDetailTable(SystemTable detailTable) {
		this.detailTable = detailTable;
		return this;
	}


	public CollateralTypeBuilder withSystemBeanType(SystemBeanType systemBeanType) {
		this.systemBeanType = systemBeanType;
		return this;
	}


	public CollateralTypeBuilder withCategory(CollateralBalanceCategories category) {
		this.category = category;
		return this;
	}


	public CollateralType build() {
		CollateralType collateralType = new CollateralType();
		collateralType.setName(this.name);
		collateralType.setCreateUserId(this.createUserId);
		collateralType.setDescription(this.description);
		collateralType.setCreateDate(this.createDate);
		collateralType.setUpdateUserId(this.updateUserId);
		collateralType.setUpdateDate(this.updateDate);
		collateralType.setId(this.id);
		collateralType.setBookable(this.bookable);
		collateralType.setCollateralInLocalCurrencySupported(this.collateralInLocalCurrencySupported);
		collateralType.setTransferDateOffset(this.transferDateOffset);
		collateralType.setAccountGroup(this.accountGroup);
		collateralType.setWorkflow(this.workflow);
		collateralType.setDetailTable(this.detailTable);
		collateralType.setSystemBeanType(this.systemBeanType);
		collateralType.setCategory(this.category);
		return collateralType;
	}
}
