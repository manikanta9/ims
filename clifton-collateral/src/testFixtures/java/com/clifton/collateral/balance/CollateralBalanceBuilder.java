package com.clifton.collateral.balance;

import com.clifton.collateral.CollateralType;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author terrys
 */
public final class CollateralBalanceBuilder {

	private Integer id;
	private CollateralType collateralType;
	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;
	private RuleViolationStatus violationStatus;
	private InvestmentAccount holdingInvestmentAccount;
	private boolean parentCollateralBalance;
	private InvestmentAccountGroup holdingInvestmentAccountGroup;
	private InvestmentSecurity collateralCurrency;
	private Date balanceDate;
	private String note;
	private BigDecimal independentAmount;
	private BigDecimal positionsMarketValue;
	private BigDecimal totalCashValue;
	private BigDecimal cashCollateralValue;
	private BigDecimal postedCollateralValue;
	private BigDecimal postedCollateralHaircutValue;
	private BigDecimal endingPostedCollateralValue;
	private BigDecimal postedCounterpartyCollateralValue;
	private BigDecimal postedCounterpartyCollateralHaircutValue;
	private BigDecimal endingPostedCounterpartyCollateralValue;
	private BigDecimal thresholdAmount;
	private BigDecimal contractRounding;
	private BigDecimal thresholdCounterpartyAmount;
	private BigDecimal minTransferAmount;
	private BigDecimal minTransferCounterpartyAmount;
	private BigDecimal collateralChangeAmount;
	private BigDecimal counterpartyCollateralChangeAmount;
	private BigDecimal counterpartyCollateralCallAmount;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount collateralInvestmentAccount;
	private BigDecimal externalCollateralAmount;
	private BigDecimal externalCollateralRequirement;
	private BigDecimal collateralRequirement;
	private boolean reconciled;
	private BigDecimal transferAmount;
	private BigDecimal securitiesTransferAmount;
	private BigDecimal agreedMovementAmount;
	private Date bookingDate;
	private List<CollateralBalanceDetail> detailList;


	private CollateralBalanceBuilder() {
	}


	public static CollateralBalanceBuilder aCollateralBalance(CollateralType collateralType) {
		CollateralBalanceBuilder builder = new CollateralBalanceBuilder();
		return builder.withCollateralType(collateralType);
	}


	public CollateralBalanceBuilder withId(Integer id) {
		this.id = id;
		return this;
	}


	public CollateralBalanceBuilder withCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
		return this;
	}


	public CollateralBalanceBuilder withWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
		return this;
	}


	public CollateralBalanceBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
		return this;
	}


	public CollateralBalanceBuilder withViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
		return this;
	}


	public CollateralBalanceBuilder withHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
		return this;
	}


	public CollateralBalanceBuilder withParentCollateralBalance(boolean parentCollateralBalance) {
		this.parentCollateralBalance = parentCollateralBalance;
		return this;
	}


	public CollateralBalanceBuilder withHoldingInvestmentAccountGroup(InvestmentAccountGroup holdingInvestmentAccountGroup) {
		this.holdingInvestmentAccountGroup = holdingInvestmentAccountGroup;
		return this;
	}


	public CollateralBalanceBuilder withCollateralCurrency(InvestmentSecurity collateralCurrency) {
		this.collateralCurrency = collateralCurrency;
		return this;
	}


	public CollateralBalanceBuilder withBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
		return this;
	}


	public CollateralBalanceBuilder withNote(String note) {
		this.note = note;
		return this;
	}


	public CollateralBalanceBuilder withIndependentAmount(BigDecimal independentAmount) {
		this.independentAmount = independentAmount;
		return this;
	}


	public CollateralBalanceBuilder withPositionsMarketValue(BigDecimal positionsMarketValue) {
		this.positionsMarketValue = positionsMarketValue;
		return this;
	}


	public CollateralBalanceBuilder withTotalCashValue(BigDecimal totalCashValue) {
		this.totalCashValue = totalCashValue;
		return this;
	}


	public CollateralBalanceBuilder withCashCollateralValue(BigDecimal cashCollateralValue) {
		this.cashCollateralValue = cashCollateralValue;
		return this;
	}


	public CollateralBalanceBuilder withPostedCollateralValue(BigDecimal postedCollateralValue) {
		this.postedCollateralValue = postedCollateralValue;
		return this;
	}


	public CollateralBalanceBuilder withPostedCollateralHaircutValue(BigDecimal postedCollateralHaircutValue) {
		this.postedCollateralHaircutValue = postedCollateralHaircutValue;
		return this;
	}


	public CollateralBalanceBuilder withEndingPostedCollateralValue(BigDecimal endingPostedCollateralValue) {
		this.endingPostedCollateralValue = endingPostedCollateralValue;
		return this;
	}


	public CollateralBalanceBuilder withPostedCounterpartyCollateralValue(BigDecimal postedCounterpartyCollateralValue) {
		this.postedCounterpartyCollateralValue = postedCounterpartyCollateralValue;
		return this;
	}


	public CollateralBalanceBuilder withPostedCounterpartyCollateralHaircutValue(BigDecimal postedCounterpartyCollateralHaircutValue) {
		this.postedCounterpartyCollateralHaircutValue = postedCounterpartyCollateralHaircutValue;
		return this;
	}


	public CollateralBalanceBuilder withEndingPostedCounterpartyCollateralValue(BigDecimal endingPostedCounterpartyCollateralValue) {
		this.endingPostedCounterpartyCollateralValue = endingPostedCounterpartyCollateralValue;
		return this;
	}


	public CollateralBalanceBuilder withThresholdAmount(BigDecimal thresholdAmount) {
		this.thresholdAmount = thresholdAmount;
		return this;
	}


	public CollateralBalanceBuilder withContractRounding(BigDecimal contractRounding) {
		this.contractRounding = contractRounding;
		return this;
	}


	public CollateralBalanceBuilder withThresholdCounterpartyAmount(BigDecimal thresholdCounterpartyAmount) {
		this.thresholdCounterpartyAmount = thresholdCounterpartyAmount;
		return this;
	}


	public CollateralBalanceBuilder withMinTransferAmount(BigDecimal minTransferAmount) {
		this.minTransferAmount = minTransferAmount;
		return this;
	}


	public CollateralBalanceBuilder withMinTransferCounterpartyAmount(BigDecimal minTransferCounterpartyAmount) {
		this.minTransferCounterpartyAmount = minTransferCounterpartyAmount;
		return this;
	}


	public CollateralBalanceBuilder withCollateralChangeAmount(BigDecimal collateralChangeAmount) {
		this.collateralChangeAmount = collateralChangeAmount;
		return this;
	}


	public CollateralBalanceBuilder withCounterpartyCollateralChangeAmount(BigDecimal counterpartyCollateralChangeAmount) {
		this.counterpartyCollateralChangeAmount = counterpartyCollateralChangeAmount;
		return this;
	}


	public CollateralBalanceBuilder withCounterpartyCollateralCallAmount(BigDecimal counterpartyCollateralCallAmount) {
		this.counterpartyCollateralCallAmount = counterpartyCollateralCallAmount;
		return this;
	}


	public CollateralBalanceBuilder withClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
		return this;
	}


	public CollateralBalanceBuilder withCollateralInvestmentAccount(InvestmentAccount collateralInvestmentAccount) {
		this.collateralInvestmentAccount = collateralInvestmentAccount;
		return this;
	}


	public CollateralBalanceBuilder withExternalCollateralAmount(BigDecimal externalCollateralAmount) {
		this.externalCollateralAmount = externalCollateralAmount;
		return this;
	}


	public CollateralBalanceBuilder withExternalCollateralRequirement(BigDecimal externalCollateralRequirement) {
		this.externalCollateralRequirement = externalCollateralRequirement;
		return this;
	}


	public CollateralBalanceBuilder withCollateralRequirement(BigDecimal collateralRequirement) {
		this.collateralRequirement = collateralRequirement;
		return this;
	}


	public CollateralBalanceBuilder withReconciled(boolean reconciled) {
		this.reconciled = reconciled;
		return this;
	}


	public CollateralBalanceBuilder withTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
		return this;
	}


	public CollateralBalanceBuilder withSecuritiesTransferAmount(BigDecimal securitiesTransferAmount) {
		this.securitiesTransferAmount = securitiesTransferAmount;
		return this;
	}


	public CollateralBalanceBuilder withAgreedMovementAmount(BigDecimal agreedMovementAmount) {
		this.agreedMovementAmount = agreedMovementAmount;
		return this;
	}


	public CollateralBalanceBuilder withBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
		return this;
	}


	public CollateralBalanceBuilder withDetailList(List<CollateralBalanceDetail> detailList) {
		this.detailList = detailList;
		return this;
	}


	public CollateralBalance build() {
		CollateralBalance collateralBalance = CollateralBalanceFactory.createCollateralBalance(this.collateralType);
		collateralBalance.setId(this.id);
		collateralBalance.setCollateralType(this.collateralType);
		collateralBalance.setWorkflowState(this.workflowState);
		collateralBalance.setWorkflowStatus(this.workflowStatus);
		collateralBalance.setViolationStatus(this.violationStatus);
		collateralBalance.setHoldingInvestmentAccount(this.holdingInvestmentAccount);
		collateralBalance.setParentCollateralBalance(this.parentCollateralBalance);
		collateralBalance.setHoldingInvestmentAccountGroup(this.holdingInvestmentAccountGroup);
		collateralBalance.setCollateralCurrency(this.collateralCurrency);
		collateralBalance.setBalanceDate(this.balanceDate);
		collateralBalance.setNote(this.note);
		collateralBalance.setIndependentAmount(this.independentAmount);
		collateralBalance.setPositionsMarketValue(this.positionsMarketValue);
		collateralBalance.setTotalCashValue(this.totalCashValue);
		collateralBalance.setCashCollateralValue(this.cashCollateralValue);
		collateralBalance.setPostedCollateralValue(this.postedCollateralValue);
		collateralBalance.setPostedCollateralHaircutValue(this.postedCollateralHaircutValue);
		collateralBalance.setEndingPostedCollateralValue(this.endingPostedCollateralValue);
		collateralBalance.setPostedCounterpartyCollateralValue(this.postedCounterpartyCollateralValue);
		collateralBalance.setPostedCounterpartyCollateralHaircutValue(this.postedCounterpartyCollateralHaircutValue);
		collateralBalance.setEndingPostedCounterpartyCollateralValue(this.endingPostedCounterpartyCollateralValue);
		collateralBalance.setThresholdAmount(this.thresholdAmount);
		collateralBalance.setContractRounding(this.contractRounding);
		collateralBalance.setThresholdCounterpartyAmount(this.thresholdCounterpartyAmount);
		collateralBalance.setMinTransferAmount(this.minTransferAmount);
		collateralBalance.setMinTransferCounterpartyAmount(this.minTransferCounterpartyAmount);
		collateralBalance.setCollateralChangeAmount(this.collateralChangeAmount);
		collateralBalance.setCounterpartyCollateralChangeAmount(this.counterpartyCollateralChangeAmount);
		collateralBalance.setCounterpartyCollateralCallAmount(this.counterpartyCollateralCallAmount);
		collateralBalance.setClientInvestmentAccount(this.clientInvestmentAccount);
		collateralBalance.setCollateralInvestmentAccount(this.collateralInvestmentAccount);
		collateralBalance.setExternalCollateralAmount(this.externalCollateralAmount);
		collateralBalance.setExternalCollateralRequirement(this.externalCollateralRequirement);
		collateralBalance.setCollateralRequirement(this.collateralRequirement);
		collateralBalance.setReconciled(this.reconciled);
		collateralBalance.setTransferAmount(this.transferAmount);
		collateralBalance.setSecuritiesTransferAmount(this.securitiesTransferAmount);
		collateralBalance.setAgreedMovementAmount(this.agreedMovementAmount);
		collateralBalance.setBookingDate(this.bookingDate);
		collateralBalance.setDetailList(this.detailList);
		return collateralBalance;
	}
}
