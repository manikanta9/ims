Clifton.collateral.BaseCollateralManagementWindow = Ext.extend(TCG.app.Window, {
	id: 'collateralSetupWindow',
	iconCls: 'bank',
	title: 'Collateral Management',
	width: 1500,
	height: 700,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		Ext.each(this.additionalItems, function(f) {
			tabs.add(f);
		});
		tabs.add(this.baseTabs);
		tabs.setActiveTab(tabs.get(0));
	},

	items: {
		xtype: 'tabpanel',
		items: []
	},
	baseTabs: [
		{
			title: 'Collateral Transactions',
			items: [{
				xtype: 'accounting-transactionGrid',
				instructions: 'General Ledger transactions for Collateral GL accounts.',
				getLoadParams: function(firstLoad) {
					const collateralType = TCG.data.getData('collateralTypeByName.json?requestedMaxDepth=3&name=' + this.getWindow().collateralTypeName, this, this.getWindow().collateralTypeName);
					let holdingAccountTypeId = undefined;
					let holdingAccountTypeName = undefined;
					if (!TCG.isBlank(collateralType.holdingInvestmentAccountType)) {
						holdingAccountTypeId = collateralType.holdingInvestmentAccountType.id;
						holdingAccountTypeName = collateralType.holdingInvestmentAccountType.name;
					}
					if (firstLoad) {
						// default to last 7 days of transactions
						this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
						this.setFilterValue('holdingInvestmentAccount.type.name', {text: holdingAccountTypeName, value: holdingAccountTypeId});
					}
					return {
						collateralAccountingAccount: true
					};
				}
			}]
		},


		{
			title: 'Collateral Balances',
			items: [{
				xtype: 'accounting-balanceSheetGrid',
				instructions: 'Collateral GL account balances on the specified date grouped by client account, holding account, GL account and security.',

				getCustomFilters: function() {
					return {collateralAccountingAccount: true};
				}
			}]
		},


		{
			title: 'Admin',
			layout: 'vbox',
			layoutConfig: {align: 'stretch'},
			items: [{
				xtype: 'formpanel',
				instructions: 'Optionally select a client or holding account (if none selected will rebuild all) and a balance date (required) to rebuild.  If a balance already exists, and you do not want to rebuild it, select Skip Existing checkbox.',
				listeners: {
					destroy: function() {
						Ext.TaskMgr.stopAll();
					},
					afterrender: function(fp) {
						const f = this.getForm();
						const bd = f.findField('balanceDate');
						if (TCG.isEqualsStrict(bd.getValue(), '')) {
							const prevBD = TCG.getPreviousWeekday();
							bd.setValue(prevBD.format('m/d/Y'));
						}
						const collateralType = TCG.data.getData('collateralTypeByName.json?requestedMaxDepth=3&name=' + this.getWindow().collateralTypeName, fp, this.getWindow().collateralTypeName);
						const ct = f.findField('collateralTypeLabel');
						ct.setValue(collateralType.name);
						const ctId = f.findField('collateralTypeId');
						ctId.setValue(collateralType.id);
					}

				},
				height: 195,
				labelWidth: 140,
				loadValidation: false, // using the form only to get background color/padding
				buttonAlign: 'right',
				items: [{
					xtype: 'panel',
					layout: 'column',
					items: [
						{
							columnWidth: .49,
							items: [{
								xtype: 'formfragment',
								frame: false,
								labelWidth: 150,
								items: [
									{fieldLabel: 'Client', name: 'clientLabel', hiddenName: 'businessClientId', xtype: 'combo', url: 'businessClientListFind.json', displayField: 'label', mutuallyExclusiveFields: ['holdingAccountId', 'issuingCompanyId']},
									{
										fieldLabel: 'Holding Account Type', xtype: 'combo', hiddenName: 'accountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10
										, qtip: 'Use to Filter holding company and/or holding account selections by account type only.'
										, mutuallyExclusiveFields: ['businessClientId']
									},
									{
										fieldLabel: 'Holding Account', name: 'accountLabel', hiddenName: 'holdingAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false&workflowStatusNameEquals=Active', displayField: 'label'
										, mutuallyExclusiveFields: ['businessClientId', 'issuingCompanyId'],
										listeners: {
											beforequery: function(queryEvent) {
												const bp = {};

												const combo = queryEvent.combo;
												const f = combo.getParentForm().getForm();
												const atv = f.findField('accountTypeId').getValue();
												if (atv) {
													bp.accountTypeId = atv;
												}
												combo.store.baseParams = bp;
											}
										}
									},
									{
										fieldLabel: 'Holding Company', name: 'issuingCompanyLabel', hiddenName: 'issuingCompanyId', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true'
										, mutuallyExclusiveFields: ['businessClientId', 'holdingAccountId'],
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const f = combo.getParentForm().getForm();
											const atv = f.findField('accountTypeId').getValue();
											combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
										}
									}
								]
							}]
						},
						{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
						{
							columnWidth: .49,
							items: [{
								xtype: 'formfragment',
								frame: false,
								labelWidth: 150,
								items: [
									{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield', allowBlank: false},
									{fieldLabel: 'Collateral Type', xtype: 'combo', name: 'collateralTypeLabel', hiddenName: 'collateralTypeId', valueField: 'id', url: 'collateralTypeListAll.json', loadAll: true, allowBlank: false},
									{fieldLabel: 'Skip Existing', name: 'skipExisting', xtype: 'checkbox'},
									{name: 'synchronous', xtype: 'hidden', value: 'false'}
								]
							}]
						}
					]
				}],
				buttons: [{
					text: 'Rebuild Balance(s)',
					iconCls: 'run',
					width: 140,
					handler: function() {
						const owner = this.findParentByType('formpanel');
						const form = owner.getForm();
						const accountTypeId = form.findField('accountTypeId').getValue();
						if (!TCG.isEqualsStrict(accountTypeId, '')) {
							const accountId = form.findField('holdingAccountId').getValue();
							const issuerId = form.findField('issuingCompanyId').getValue();
							if (TCG.isEqualsStrict(accountId, '') && TCG.isEqualsStrict(issuerId, '')) {
								TCG.showError('Holding Investment Account Type selection is used to filter the holding account and issuer selections only. You did not select an issuer or holding account to filter on.', 'Invalid Selection');
								return;
							}
						}
						const collateralTypeId = form.findField('collateralTypeId').getValue();
						if (TCG.isEqualsStrict(collateralTypeId, '')) {
							TCG.showError('Collateral Type selection is required.', 'Invalid Selection');
						}

						Ext.Msg.confirm('Rebuild?', 'Are you sure you want to rebuild collateral balances for selected accounts?', function(a) {
							if (TCG.isEqualsStrict(a, 'yes')) {
								form.submit(Ext.applyIf({
									url: 'collateralBalanceRebuild.json',
									waitMsg: 'Rebuilding...',
									success: function(form, action) {
										Ext.Msg.alert('Processing Started', action.result.result.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						});
					}
				}]
			}, {
				xtype: 'core-scheduled-runner-grid',
				typeName: 'COLLATERAL-BALANCE',
				instantRunner: true,
				title: 'Executing Processes',
				flex: 1
			}]
		},


		{
			title: 'Collateral Instructions',
			items: [{
				xtype: 'investment-instruction-grid',
				defaultTagName: 'Collateral',
				defaultViewName: 'Group by Recipient'

			}]
		},


		{
			title: 'External Loads',
			items: [{
				xtype: 'integration-importRunEventGrid',
				targetApplicationName: 'IMS',
				importEventName: 'Broker Daily M2M'
			}]
		}
	]
});
