TCG.use('Clifton.collateral.BaseCollateralManagementWindow');
Clifton.collateral.FuturesCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'futuresCollateralManagementWindow',
	title: 'Futures Collateral',
	iconCls: 'bank',
	width: 1500,
	collateralTypeName: 'Futures Collateral',

	additionalItems: [{
		title: 'Futures Collateral',
		items: [{
			isPagingEnabled: function() {
				return false;
			},
			rowSelectionModel: 'checkbox',
			xtype: 'non-otc-collateral-grid',
			configureToolsMenu: function(menu) {
				const gridPanel = this;
				menu.add('-');
				menu.add({
					text: 'Preview Instruction Message',
					tooltip: 'Preview Instruction SWIFT Message',
					iconCls: 'shopping-cart',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						const collateral = sm.getSelections();
						if (sm.getCount() !== 1) {
							TCG.showError('Please select a single collateral to preview.', 'Incorrect Selection');
						}
						else {
							const id = collateral[0].id;
							Clifton.instruction.openInstructionPreview(id, 'CollateralBalance', gridPanel);
						}
					}
				});
			},
			editor: {
				ptype: 'workflow-transition-grid',
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true,
				tableName: 'CollateralBalance',
				transitionWindowTitle: 'Transition Balance Records',
				transitionWindowInstructions: 'Select Transition for selected collateral balance records',
				addAfterTransitionButtons: function(t, gridPanel) {
					t.add({
						text: 'Book and Post',
						tooltip: 'Book unbooked non-zero collateral transfers and immediately post them to the General Ledger',
						iconCls: 'book-red',
						scope: this,
						handler: function() {
							TCG.createComponent('Clifton.collateral.CollateralBookAndPostWindow', {
								defaultData: {
									balanceDate: TCG.getChildByName(t, 'balanceDate').getValue().format('m/d/Y'),
									collateralTypeName: this.getWindow().collateralTypeName
								},
								openerCt: gridPanel
							});
						}
					});
					t.add('-');
				}
			}
		}]
	}]
});
