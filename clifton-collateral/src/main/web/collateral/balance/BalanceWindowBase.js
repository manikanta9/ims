Clifton.collateral.balance.BalanceWindowBase = Ext.extend(TCG.app.DetailWindow, {
	height: 700,
	width: 1200,

	generalInfoTab: {
		title: 'OVERRIDE ME',
		items: [{}]
	},
	balanceDetailTab: {
		id: 'balanceDetailTab',
		title: 'OVERRIDE ME',
		items: [{}]
	},
	pledgedCollateralTab: {
		id: 'pledgedCollateralTab',
		title: 'OVERRIDE ME',
		items: [{}]
	},

	notesTab: {
		title: 'Notes',
		items: [{
			xtype: 'document-system-note-grid',
			tableName: 'CollateralBalance'
		}]
	},

	positionsTab: {
		title: 'Positions',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralAccountingPositionDailyLiveList',
			instructions: 'Daily lot-level positions are built from the General Ledger and market data. They may get stale and may need to be rebuilt in order to see accurate data when GL or market data changes.  NOTE: For REPO collateral the positions are calculated with tomorrow\'s index ratio.',
			pageSize: 200,
			columns: [
				{header: 'Position Group ID', dataIndex: 'positionGroupId', width: 50, hidden: true},
				{header: 'Trade Strategy', dataIndex: 'tradeStrategyName', width: 50, hidden: true},
				{header: 'Transaction ID', dataIndex: 'accountingTransaction.id', width: 50, hidden: true},
				{header: 'Client Account', dataIndex: 'accountingTransaction.clientInvestmentAccount.label', width: 140, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'Used For Speculation', dataIndex: 'accountingTransaction.holdingInvestmentAccount.usedForSpeculation', width: 40, filter: false, hidden: true, type: 'boolean'},
				{header: 'GL Account', dataIndex: 'accountingTransaction.accountingAccount.name', width: 90, filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json'}, hidden: true},
				{header: 'Security', dataIndex: 'accountingTransaction.investmentSecurity.symbol', width: 100, filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
				{header: 'Requires Collateral', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hierarchy.collateralUsed', type: 'boolean', width: 100, tooltip: 'If this is true, the position will be included in the collateral balance market value.  This can be changed be setting \'Securities in this hierarchy use collateral\' on the security hierarchy.'},
				{header: 'Prior Quantity', dataIndex: 'priorQuantity', type: 'float', useNull: true, negativeInRed: true, width: 70, hidden: true},
				{header: 'Quantity', dataIndex: 'remainingQuantity', type: 'float', useNull: true, negativeInRed: true, width: 70},
				{header: 'Market Price', dataIndex: 'marketPrice', type: 'float', useNull: true, negativeInRed: true, width: 70},
				{header: 'FX Rate', dataIndex: 'marketFxRate', type: 'float', width: 55},
				{header: 'Local Cost Basis', dataIndex: 'remainingCostBasisLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true},
				{header: 'Base Cost Basis', dataIndex: 'remainingCostBasisBase', type: 'currency', negativeInRed: true, width: 80, hidden: true, summaryType: 'sum'},
				{header: 'Local OTE', dataIndex: 'openTradeEquityLocal', type: 'currency', negativeInRed: true, width: 80, hidden: true},
				{header: 'Base OTE', dataIndex: 'openTradeEquityBase', type: 'currency', negativeInRed: true, width: 80, summaryType: 'sum'},
				{header: 'Local Accrual', dataIndex: 'accrualLocal', type: 'currency', negativeInRed: true, width: 85, hidden: true},
				{header: 'Base Accrual', dataIndex: 'accrualBase', type: 'currency', negativeInRed: true, width: 85, hidden: true, summaryType: 'sum'},
				{header: 'Local Market Value', dataIndex: 'marketValueLocal', type: 'currency', negativeInRed: true, width: 85, sortable: false},
				{header: 'Base Market Value', dataIndex: 'marketValueBase', type: 'currency', negativeInRed: true, width: 85, summaryType: 'sum', sortable: false},
				{header: 'Required Collateral', dataIndex: 'requiredCollateralBase', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', sortable: false},

				{header: 'Speculator Initial Margin', dataIndex: 'accountingTransaction.investmentSecurity.instrument.speculatorInitialMarginPerUnit', width: 90, type: 'currency', useNull: true, hidden: true},
				{header: 'Hedger Initial Margin', dataIndex: 'accountingTransaction.investmentSecurity.instrument.hedgerInitialMarginPerUnit', width: 90, type: 'currency', useNull: true, hidden: true},
				{header: 'Initial Margin per Qty', dataIndex: 'accountingTransaction.initialMarginPerUnit', width: 92, type: 'currency', useNull: true, sortable: false, tooltip: 'Actual Initial Margin Requirement for one Unit of Quantity. The calculation takes into account whether the holding account is sued for hedging or speculation as well as account specific Initial Margin Multiplier.'}
			],
			plugins: {ptype: 'gridsummary'},
			isPagingEnabled: function() {
				return false;
			},
			editor: {
				detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
				drillDownOnly: true,
				getDetailPageId: function(gridPanel, row) {
					return row.json.accountingTransaction.id;
				}
			},
			getLoadParams: function() {
				const tb = this.getTopToolbar();
				const win = this.getWindow();
				const collateralTypeName = TCG.getValue('collateralType.name', this.getWindow().getMainForm().formValues);
				const summarizePositions = TCG.isEqualsStrict(collateralTypeName, 'Options Escrow Collateral');
				const params = {
					collateralTypeName: TCG.getValue('collateralType.name', this.getWindow().getMainForm().formValues),
					holdingAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues),
					positionDate: TCG.getChildByName(tb, 'positionDate').getValue().format('m/d/Y'),
					summarizePositions: summarizePositions
				};
				if (TCG.isTrue(win.useNextDayIndexRatio)) {
					params['useNextDayIndexRatio'] = true;
				}
				return params;
			},
			getTopToolbarFilters: function(toolbar) {
				const filters = [];
				const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
				const dv = dateV ? dateV.dateFormat('m/d/Y') : '';
				filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'positionDate', value: dv});
				return filters;
			}
		}]
	},

	marginPositionsTab: {
		id: 'marginPositionsTab',
		title: 'OVERRIDE ME',
		items: [{}]
	},

	collateralPositionsTab: {
		title: 'Collateral Positions',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralPositionList',
			root: 'data',
			instructions: 'Collateral Positions for the holding account on the balance date.',
			getTopToolbarFilters: function(toolbar) {
				const filters = [];
				const grid = this;

				let dateV = TCG.parseDate(TCG.getValue('balanceDate', grid.getWindow().getMainForm().formValues));
				dateV = Clifton.calendar.getBusinessDayFromDate(dateV, 1);
				filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'transactionDate', value: dateV.dateFormat('m/d/Y')});
				return filters;
			},
			getLoadParams: function(firstLoad) {
				const grid = this;
				const transactionDate = new Date(TCG.getChildByName(grid.getTopToolbar(), 'transactionDate').value).format('m/d/Y');
				return {
					holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', grid.getWindow().getMainForm().formValues),
					transactionDate: transactionDate
				};
			},
			columns: [
				{header: 'GL Account', width: 175, dataIndex: 'accountingAccount.label', filter: false},
				{header: 'Security', width: 75, dataIndex: 'security.symbol', filter: false},
				{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
				{header: 'Quantity', width: 75, dataIndex: 'quantity', type: 'currency'},
				{header: 'Cost Price', width: 100, dataIndex: 'costPrice', type: 'currency', numberFormat: '0,000.0000'},
				{header: 'Market Price', width: 100, dataIndex: 'marketPrice', type: 'currency', numberFormat: '0,000.0000'},
				{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},

				{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency'},
				{
					header: 'Collateral Market Value', width: 120, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
					},
					summaryType: 'sum',
					summaryRenderer: function(v) {
						if (v !== '') {
							return TCG.renderAmount(v, false, '0,000.00');
						}
					}
				}
			],
			plugins: {ptype: 'gridsummary'}
		}]
	},

	balanceHistoryTab: {
		title: 'Balance History',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralBalanceListFind',
			instructions: 'Daily collateral balance history for this account.',
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 30 days of balance history since balance date
					const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
					this.setFilterValue('balanceDate', {'after': dateV.add(Date.DAY, -30)});
				}
				return {
					holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues),
					parentIsNull: true
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Date', width: 60, dataIndex: 'balanceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
				{header: 'Positions Market Value', width: 85, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Client Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Client Posted Collateral', width: 85, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCollateralValue'], r.data['postedCollateralValue'], '0,000.00');
					}
				},


				{header: 'Counterparty Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCounterpartyCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Counterparty Posted Collateral', width: 100, dataIndex: 'postedCounterpartyCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCounterpartyCollateralValue'], r.data['postedCounterpartyCollateralHaircutValue'], '0,000.00');
					}
				},

				{header: 'Threshold', hidden: true, dataIndex: 'thresholdAmount', width: 100, useNull: true, type: 'currency'},
				{header: 'Counterparty Threshold', hidden: true, width: 100, dataIndex: 'thresholdCounterpartyAmount', useNull: true, type: 'currency'},

				{header: 'Minimum Transfer Amount', hidden: true, dataIndex: 'minTransferAmount', width: 100, useNull: true, type: 'currency'},
				{header: 'Counterparty Minimum Transfer Amount', hidden: true, dataIndex: 'minTransferCounterpartyAmount', width: 100, useNull: true, type: 'currency'},

				{header: 'Client Collateral Change', width: 90, dataIndex: 'collateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Counterparty Collateral Change', width: 100, dataIndex: 'counterpartyCollateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-RIGHT: #c0c0c0 1px solid;'}
			],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			}
		}]
	},

	collateralTransfersTab: {
		title: 'Transfers',
		items: [{
			name: 'accountingPositionTransferListFind',
			xtype: 'gridpanel',
			instructions: 'Position transfers generated from this Collateral Balance.',
			columns: [
				{header: 'Transfer ID', width: 60, dataIndex: 'id', hidden: true},
				{header: 'Transfer Type', width: 150, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'transferTypeId', url: 'accountingPositionTransferTypeListFind.json'}},
				{header: 'From (Client Account)', width: 180, dataIndex: 'fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'From (Holding Account)', width: 180, dataIndex: 'fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
				{header: 'To (Client Account)', width: 180, dataIndex: 'toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
				{header: 'To (Holding Account)', width: 180, dataIndex: 'toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
				{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
				{
					header: 'Violation Status', width: 80, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
					renderer: function(v, p, r) {
						return (Clifton.rule.violation.renderViolationStatus(v));
					}
				},
				{header: 'Price Date', width: 60, dataIndex: 'exposureDate', hidden: true},
				{header: 'Transfered', width: 60, dataIndex: 'transactionDate'},
				{header: 'Settled', width: 60, dataIndex: 'settlementDate'},
				{header: 'Booked', width: 80, dataIndex: 'bookingDate'}
			],
			getLoadParams: function(firstLoad) {
				return {
					sourceSystemTableNameEquals: 'CollateralBalance',
					sourceFkFieldId: this.getWindow().getMainFormId()
				};
			},
			editor: {
				detailPageClass: 'Clifton.accounting.positiontransfer.TransferWindow',
				drillDownOnly: true
			}
		}]
	},

	showClientPositionTransferBtns: true, // false to skip
	positionTransferBtns: [
		'->',
		{
			fieldLabel: 'Transfer Type', xtype: 'combo', name: 'transferType', width: 200, url: 'accountingPositionTransferTypeListFind.json?collateral=true', limitRequestedProperties: false
			, listeners: {
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = queryEvent.combo.getWindow().getAccountingPositionTransferTypeParams();
				}
			}
		},
		{
			text: 'Create Transfer',
			iconCls: 'transfer',
			handler: function() {
				const win = this.ownerCt.ownerCt.ownerCt.getWindow();
				const fp = win.items.get(0).items.get(0).items.get(0);
				const tabs = win.items.get(0).items;
				const tb = tabs.get(0).getTopToolbar();
				const transferType = TCG.getChildByName(tb, 'transferType').getValueObject();
				if (transferType === '') {
					TCG.showError('You must first select a transfer type.');
				}
				else {
					win.createTransfer(fp, transferType);
				}
			}
		}
	],

	getAccountingPositionTransferTypeParams: function() {
		//OVERRIDE TO FILTER TRANSFER TYPES

		//SHOW ONLY CASH OPTIONS = cash:true
		//SHOW ONLY POSITION OPTIONS = cash:false
		//SHOW ONLY COUNTER PARTY OPTIONS = counterpartyCollateral:false
		//SHOW ONLY CLIENT OPTIONS = counterpartyCollateral:true
		return {};
	},

	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items;
		tabs[0] = this.generalInfoTab;
		tabs[1] = {
			title: 'Violations',
			items: [{
				xtype: 'rule-violation-grid',
				tableName: 'CollateralBalance'
			}]
		};
		tabs[2] = this.balanceDetailTab;
		tabs[3] = this.notesTab;

		tabs[4] = this.positionsTab;
		tabs[5] = this.marginPositionsTab;
		tabs[6] = this.collateralPositionsTab;
		if (this.collateralTransactionTab) {
			tabs[7] = this.collateralTransactionTab;
		}
		tabs[8] = this.pledgedCollateralTab;
		tabs[9] = this.balanceHistoryTab;
		tabs[10] = this.collateralTransfersTab;
		const tbarItems = [];
		if (tabs[0].additionalToolbarItems) {
			for (let i = 0; i < tabs[0].additionalToolbarItems.length; i++) {
				tbarItems.push(tabs[0].additionalToolbarItems[i]);
			}
		}
		this.addAdditionalActionButtons(tbarItems);
		this.addCollateralActionButtons(tbarItems);

		if (tbarItems.length > 0) {
			tabs[0].tbar = new Ext.Toolbar({items: tbarItems});
		}
		Clifton.collateral.balance.BalanceWindowBase.superclass.init.apply(this, arguments);
	},

	addCollateralActionButtons: function(tbarItems) {
		if (this.showClientPositionTransferBtns === true) {
			tbarItems.push(this.positionTransferBtns);
		}
	},
	addAdditionalActionButtons: function(tbarItems) {
		// Override to define additional action buttons
	},
	createTransfer: function(fp, transferType) {
		// Default Logic Can Override Where Necessary
		const win = this;

		const dd = win.applyTransferDefaultDataForType(fp, transferType, 'CollateralBalance');
		TCG.createComponent('Clifton.accounting.positiontransfer.TransferWindow', {
			defaultData: dd,
			openerCt: win
		});

	},
	applyTransferDefaultDataForType: function(fp, transferType, tableName) {
		const clientAcct = this.getClientAccount(fp);
		const collateralAcct = this.getCollateralAccount(fp);
		const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tableName, fp, 'system.table.' + tableName);


		const dd = {
			type: transferType,
			fromClientInvestmentAccount: clientAcct,
			toClientInvestmentAccount: clientAcct,
			useOriginalTransactionDate: true,
			sourceSystemTable: tbl,
			sourceFkFieldId: fp.getWindow().getMainFormId()
		};

		const form = fp.form;

		const balanceDate = TCG.parseDate(TCG.getValue('balanceDate', form.formValues));
		dd.transactionDate = Clifton.calendar.getBusinessDayFromDate(balanceDate, TCG.getValue('collateralType.transferDateOffset', form.formValues)).format('Y-m-d 00:00:00');

		if ((transferType.collateralPosting && !transferType.counterpartyCollateral) || (!transferType.collateralPosting && transferType.counterpartyCollateral)) {
			dd.toHoldingInvestmentAccount = TCG.getValue('holdingInvestmentAccount', form.formValues);
			dd.fromHoldingInvestmentAccount = collateralAcct;
		}
		else {
			dd.fromHoldingInvestmentAccount = TCG.getValue('holdingInvestmentAccount', form.formValues);
			dd.toHoldingInvestmentAccount = collateralAcct;
		}

		dd.transferAmount = TCG.getValue('transferAmount', form.formValues);
		dd.detailList = [];
		dd.detailList.push({positionCostBasis: dd.transferAmount});
		dd.note = (transferType.collateralPosting ? 'Posting' : 'Return') + ' of ' + (transferType.counterpartyCollateral ? 'Counterparty' : 'Client') + ' Owned Collateral from Collateral Balance Date [' + TCG.parseDate(TCG.getValue('balanceDate', form.formValues)).format('m/d/Y') + ']';
		return dd;
	},

	getClientAccount: function(fp) {
		const form = fp.form;
		let clientAcct = TCG.getValue('clientInvestmentAccount', form.formValues);
		if (!clientAcct) {
			clientAcct = this.lookupAccount(fp, true, 'Collateral', 'Custodian');
		}
		return clientAcct;
	},
	getCollateralAccount: function(fp) {
		const form = fp.form;
		let collateralAcct = TCG.getValue('collateralInvestmentAccount', form.formValues);
		if (!collateralAcct) {
			collateralAcct = this.lookupAccount(fp, false, 'Collateral', 'Custodian');
		}
		return collateralAcct;
	},
	setCustomCollateralTransactionFilters: function(gridPanel) {
		//OVERRIDE ME
	},

	lookupAccount: function(fp, ourAccount, purpose, secondPurpose, mainAccount, holdingInvestmentAccountId, businessContractId) {
		const form = fp.form;
		let acct = undefined;

		let url;
		const holdingAccountId = holdingInvestmentAccountId ? holdingInvestmentAccountId : TCG.getValue('holdingInvestmentAccount', form.formValues).id;
		if (!mainAccount) {
			url = 'investmentAccountListFind.json?relatedAccountId=' + holdingAccountId + '&relatedPurposeActiveOnDate=' + new Date().format('m/d/Y') + '&ourAccount=' + ourAccount;
			url += '&relatedPurpose=' + purpose;
		}
		else {
			url = 'investmentAccountListFind.json?mainAccountId=' + holdingAccountId + '&mainPurposeActiveOnDate=' + new Date().format('m/d/Y') + '&ourAccount=' + ourAccount;
			url += '&mainPurpose=' + purpose;
		}
		if (TCG.isNotBlank(businessContractId)) {
			url += '&businessContractId=' + businessContractId;
		}

		let result = TCG.data.getData(url, fp);
		if (result && result.length === 1) {
			acct = result[0];
		}
		if (!acct && secondPurpose) {
			result = TCG.data.getData(url + '&relatedPurpose=' + secondPurpose, fp);
			if (result && result.length === 1) {
				acct = result[0];
			}
		}
		return acct;
	},

	items: [{
		xtype: 'tabpanel',
		listeners: {
			// hide the balanceDetailTab if it wasn't overridden
			afterrender: function(component) {
				const balanceDetailTab = Ext.getCmp('balanceDetailTab');
				if (balanceDetailTab) {
					component.hideTabStripItem(balanceDetailTab);
				}
				const positionsTab = Ext.getCmp('positionsTab');
				if (positionsTab) {
					component.hideTabStripItem(positionsTab);
				}
				const marginPositionsTab = Ext.getCmp('marginPositionsTab');
				if (marginPositionsTab) {
					component.hideTabStripItem(marginPositionsTab);
				}
				const collateralPositionsTab = Ext.getCmp('collateralPositionsTab');
				if (collateralPositionsTab) {
					component.hideTabStripItem(collateralPositionsTab);
				}
				const pledgedCollateralTab = Ext.getCmp('pledgedCollateralTab');
				if (pledgedCollateralTab) {
					component.hideTabStripItem(pledgedCollateralTab);
				}
				const balanceHistoryTab = Ext.getCmp('balanceHistoryTab');
				if (balanceHistoryTab) {
					component.hideTabStripItem(balanceHistoryTab);
				}
				const collateralTransfersTab = Ext.getCmp('collateralTransfersTab');
				if (collateralTransfersTab) {
					component.hideTabStripItem(collateralTransfersTab);
				}
			}
		},
		items: [
			{}, // first tab to be overridden
			{}, // violations tab
			{}, // third tab to be overridden
			{}, // fourth tab to be overridden
			{}, // positions
			{}, // margin positions
			{}, // collateral positions

			{
				title: 'Collateral Transactions',
				items: [{
					instructions: 'These are detailed collateral entries found in the general ledger for the given holding account.',
					xtype: 'accounting-transactionGrid',
					includeClientAccountFilter: false,
					columnOverrides: [
						{dataIndex: 'clientInvestmentAccount.label', hidden: true},
						{dataIndex: 'holdingInvestmentAccount.label', hidden: true},
						{dataIndex: 'localDebit', hidden: true},
						{dataIndex: 'localCredit', hidden: true},
						{dataIndex: 'exchangeRateToBase', hidden: true},
						{dataIndex: 'positionCostBasis', hidden: true},
						{dataIndex: 'settlementDate', hidden: true}
					],
					getCustomFilters: function(firstLoad) {
						const fp = this.getWindow().getMainFormPanel();
						if (firstLoad) {
							// default to last -30 days of transactions (since balance date) to now
							const dateV = TCG.parseDate(fp.getFormValue('balanceDate'));
							this.setFilterValue('transactionDate', {'after': dateV.add(Date.DAY, -30)});
							this.getWindow().setCustomCollateralTransactionFilters(this);
						}
						return {
							collateralAccountingAccount: true,
							holdingInvestmentAccountId: fp.getFormValue('holdingInvestmentAccount.id')
						};
					}
				}]
			},


			{},
			{},
			{},


			{
				title: 'Collateral Instructions',
				layout: 'border',
				items: [
					{
						title: 'Collateral Balance Instructions',
						xtype: 'investment-instruction-item-grid',
						region: 'north',
						height: 250,
						instructions: 'The following instructions are associated with the Collateral Balance.',
						tableName: 'CollateralBalance',
						getInstructionDate: function() {
							return TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues);
						}
					},
					{
						title: 'Collateral Cash/Position Transfer Instructions',
						xtype: 'investment-instruction-item-grid',
						region: 'center',
						instructions: 'The following instructions are associated with a position transfer linked to this Collateral Balance.',
						name: 'investmentInstructionItemListForEntityList',
						tableName: 'AccountingPositionTransfer',

						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'InstructionID', width: 15, dataIndex: 'instruction.id', hidden: true},
							{header: 'Category', width: 75, dataIndex: 'instruction.definition.category.name'},
							{header: 'Cash', width: 30, dataIndex: 'instruction.definition.category.cash', type: 'boolean', hidden: true},
							{header: 'Client', width: 30, dataIndex: 'instruction.definition.category.client', type: 'boolean', hidden: true},
							{header: 'Definition', width: 125, dataIndex: 'instruction.definition.name'},
							{header: 'Instruction Item', width: 125, dataIndex: 'fkFieldLabel', sortable: false, renderer: TCG.renderValueWithTooltip},
							{header: 'Recipient Company', width: 100, dataIndex: 'instruction.recipientCompany.name'},
							{header: 'Date', width: 40, dataIndex: 'instruction.instructionDate'},
							{
								header: 'Status', width: 40, dataIndex: 'status.name', defaultSortColumn: true, defaultSortDirection: 'DESC',
								renderer: function(v, m, r) {
									return (Clifton.investment.instruction.renderStatus(v, m, r));
								}
							}
						],

						getInstructionDate: function() {
							return new Date();
						},

						getLoadParams: function(firstLoad) {
							const gp = this;
							return TCG.data.getDataPromise('accountingPositionTransferListFind.json?requestedPropertiesRoot=data&requestedProperties=id&sourceSystemTableNameEquals=CollateralBalance&sourceFkFieldId=' + this.getEntityId(), this)
								.then(function(positionTransferList) {
									if (!positionTransferList || positionTransferList.length === 0) {
										return false;
									}
									const fkFieldIds = [];
									for (let i = 0; i < positionTransferList.length; i++) {
										fkFieldIds.push(positionTransferList[i].id);
									}
									return {
										tableName: gp.tableName,
										fkFieldIds: fkFieldIds,
										populateFkFieldLabel: true
									};
								});
						}
					}
				]
			},


			{
				title: 'SWIFT Messages',
				items: [{
					xtype: 'swift-message-grid-panel',
					instructions: 'SWIFT messages created for these Collateral Instructions.',
					tableName: 'CollateralBalance'
				}]
			},


			{
				title: 'Collateral Balance Rules',
				items: [{
					xtype: 'rule-assignment-grid-forAdditionalScopeEntity',
					scopeTableName: 'CollateralType',
					entityTableName: 'InvestmentAccount',
					defaultCategoryName: 'Collateral Balance Rules',
					getAdditionalScopeFkFieldId: function() {
						return TCG.getValue('collateralType.id', this.getWindow().getMainForm().formValues);
					},
					getAdditionalScopeFkFieldLabel: function() {
						return TCG.getValue('collateralType.name', this.getWindow().getMainForm().formValues);
					},
					getEntityFkFieldId: function() {
						return TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues);
					},
					getEntityFkFieldLabel: function() {
						return TCG.getValue('holdingInvestmentAccount.label', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
