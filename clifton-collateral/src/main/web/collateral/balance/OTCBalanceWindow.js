TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.OTCBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	titlePrefix: 'OTC Collateral Balance',
	iconCls: 'run',
	height: 725,

	showGeneralInfoTbar: true,

	collateralPositionsTab: {id: 'collateralPositionsTab'}, // hide: redundant to info tab

	getCollateralAccount: function(fp) {
		const form = fp.form;
		let collateralAcct = TCG.getValue('collateralInvestmentAccount', form.formValues);
		if (!collateralAcct) {
			collateralAcct = this.lookupAccount(fp, false, 'Collateral', 'Custodian', true);
		}
		return collateralAcct;
	},

	getTransferAccounts: function(fp, transferType) {
		const clientAcct = this.getClientAccount(fp);
		let collateralAcct;
		let balanceHoldingAccount = fp.getFormValue('holdingInvestmentAccount');

		const result = {
			fromClientInvestmentAccount: clientAcct,
			toClientInvestmentAccount: clientAcct
		};

		if (!TCG.isFalse(fp.getFormValue('parentCollateralBalance'))) {
			if (!transferType.counterpartyCollateral) {
				collateralAcct = this.lookupAccount(fp, false, 'Custodian', 'Custodian', true, clientAcct.id);
				balanceHoldingAccount = this.lookupAccount(fp, false, 'Collateral', 'Custodian', true, collateralAcct.id, balanceHoldingAccount.businessContract.id);
			}
			else {
				collateralAcct = this.lookupAccount(fp, false, 'Collateral', 'Custodian', true, balanceHoldingAccount.id, balanceHoldingAccount.businessContract.id);
			}
		}
		else {
			collateralAcct = this.getCollateralAccount(fp);
		}

		if ((transferType.collateralPosting && !transferType.counterpartyCollateral) || (!transferType.collateralPosting && transferType.counterpartyCollateral)) {
			result.toHoldingInvestmentAccount = balanceHoldingAccount;
			result.fromHoldingInvestmentAccount = collateralAcct;
		}
		else {
			result.fromHoldingInvestmentAccount = balanceHoldingAccount;
			result.toHoldingInvestmentAccount = collateralAcct;
		}

		return result;
	},

	addAdditionalActionButtons(tbarItems) {
		const emailMarginCallButton = {
			text: 'Email Margin Call',
			tooltip: 'Generates a Margin Call Email for this entry. An email will be generated only when Client Collateral Change is negative or Counterparty Collateral Change is positive. Default email to "Collateral Call Recipient" party role(s) from the ISDA.',
			iconCls: 'email',
			handler: function() {
				const fp = this.ownerCt.ownerCt.items.get(0);
				Clifton.collateral.generateMarginCallEmail(fp.getForm().formValues, this);
			}
		};
		tbarItems.push('-');
		tbarItems.push(emailMarginCallButton);
	},

	createTransfer: function(fp, transferType) {
		// Default Logic Can Override Where Necessary
		const win = this;
		Promise.resolve(win.applyTransferDefaultDataForType(fp, transferType, 'CollateralBalance'))
			.then(function(dd) {
				TCG.createComponent('Clifton.accounting.positiontransfer.TransferWindow', {
					defaultData: dd,
					openerCt: win
				});
			});
	},

	addCollateralActionButtons: function(tbarItems) {
		tbarItems.push({
				iconCls: 'workflow',
				tooltip: 'Show workflow history for this entity.',
				handler: function() {
					const fp = this.ownerCt.ownerCt.items.get(0);
					Clifton.workflow.Toolbar.prototype.doShowWorkflowHistory('CollateralBalance', fp);
				}
			}, '-',
			{
				xtype: 'combo', name: 'workflowTransition', width: 150, url: 'workflowTransitionNextList.json', loadAll: true, emptyText: '< Select Transition >',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.ownerCt.ownerCt.items.get(0);
						combo.store.baseParams = {
							tableName: 'CollateralBalance',
							id: fp.getFormValue('id'),
							excludeNoTransition: true,
							excludeSystemDefined: true
						};
					}
				}
			},
			{
				iconCls: 'run',
				tooltip: 'Execute selected Workflow Transition',
				handler: function() {
					const win = this.ownerCt.ownerCt.ownerCt.getWindow();
					const fp = win.getMainFormPanel();
					const tb = win.items.get(0).items.get(0).getTopToolbar();
					const wTran = TCG.getChildByName(tb, 'workflowTransition');
					const transition = wTran.getValueObject();
					if (TCG.isBlank(transition)) {
						TCG.showError('You must first select a workflow transition.');
					}
					else if (win.isModified()) {
						TCG.showError('You must save the form that was modified before performing workflow transition.');
					}
					else {
						Ext.Msg.confirm('Confirm Transition: ' + transition.name, 'Are you sure you want to execute "' + transition.name + '" workflow transition?', function(a) {
							if (a === 'yes') {
								TCG.data.getDataPromise('workflowTransitionExecuteByTransition.json?tableName=CollateralBalance&id=' + fp.getFormValue('id') + '&workflowTransitionId=' + transition.id, this)
									.then(function(balance) {
										fp.loadJsonResult({data: balance});
										fp.fireEvent('afterload', fp);
										wTran.clearAndReset();
									});
							}
						});
					}
				}
			}, '-',
			{text: 'Workflow Status: ', disabled: true},
			{id: 'workflowStatusField'},
			'-',
			{text: 'Workflow State: ', disabled: true},
			{id: 'workflowStateField'});
		tbarItems.push(this.positionTransferBtns);
	},

	applyTransferDefaultDataForType: function(fp, transferType, tableName) {
		const transferAccounts = this.getTransferAccounts(fp, transferType);
		const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tableName, fp, 'system.table.' + tableName);
		const balanceDate = TCG.parseDate(fp.getFormValue('balanceDate'));

		const dd = Ext.apply({
			type: transferType,
			useOriginalTransactionDate: true,
			sourceSystemTable: tbl,
			sourceFkFieldId: fp.getWindow().getMainFormId()
		}, transferAccounts);


		dd.transferAmount = fp.getFormValue('transferAmount');
		dd.detailList = [];
		dd.detailList.push({positionCostBasis: dd.transferAmount});
		dd.note = (transferType.collateralPosting ? 'Posting' : 'Return') + ' of ' + (transferType.counterpartyCollateral ? 'Counterparty' : 'Client') + ' Owned Collateral from Collateral Balance Date [' + balanceDate.format('m/d/Y') + ']';

		// Transaction Date is always the next business day
		return Clifton.calendar.getBusinessDayFromDatePromise(balanceDate, 1)
			.then(function(transactionDate) {
				dd.transactionDate = transactionDate.format('Y-m-d 00:00:00');
				return TCG.data.getDataValuePromise('systemColumnCustomValue.json?columnGroupName=Contract Clauses&columnName=Transfer Timing&entityId=' + fp.getFormValue('holdingInvestmentAccount.businessContract.id'), fp);
			})
			.then(function(timing) {
				if (timing) {
					return Clifton.calendar.getBusinessDayFromDatePromise(TCG.parseDate(balanceDate), parseInt(timing.substring(2, timing.length)))
						.then(function(settlementDate) {
							dd.settlementDate = settlementDate.format('Y-m-d 00:00:00');
							return dd;
						});
				}
				return dd;
			});
	},

	generalInfoTab: {
		title: 'Info',
		layout: 'border',
		layoutConfig: {align: 'stretch'},
		items: [
			{
				xtype: 'formpanel',
				url: 'collateralBalance.json',
				region: 'north',
				loadValidation: false,
				labelWidth: 140,
				height: 420,
				items: [
					{
						xtype: 'columnpanel',
						columns: [
							{
								rows: [
									{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
									{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
									{fieldLabel: 'ISDA', name: 'holdingInvestmentAccount.businessContract.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id'},
									{fieldLabel: 'Violation Status', name: 'violationStatus.name', submitValue: false, xtype: 'displayfield'}
								],
								config: {columnWidth: 0.7}
							}, {
								rows: [
									{fieldLabel: 'Collateral Type', name: 'collateralType.name', xtype: 'linkfield', detailPageClass: 'Clifton.collateral.setup.CollateralTypeWindow', detailIdField: 'collateralType.id', submitDetailField: false},
									{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield', submitValue: false, readOnly: true},
									{fieldLabel: 'Counterparty', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', submitDetailField: false}
								],
								config: {columnWidth: 0.3, labelWidth: 90}
							}
						]
					},
					{xtype: 'label', html: '<hr/>'},

					{
						xtype: 'columnpanel',
						defaults: {xtype: 'currencyfield', readOnly: true, submitValue: false},
						columns: [
							{
								rows: [{xtype: 'spacer', height: 1}],
								config: {columnWidth: .125}
							},
							{
								rows: [
									{fieldLabel: 'Exposure/MTM', name: 'positionsMarketValue'},
									{fieldLabel: 'Independent Amount', name: 'independentAmount', qtip: 'A potential additional Collateral Requirement (% of Notional) outlined in the Credit Support Document, or Trade Confirmation. Formula: Percentage * Notional (typically \'Cost\' Notional)'},
									{fieldLabel: 'Threshold', name: 'calculatedThresholdAmount', qtip: 'Collateral is only required to be posted to the extent that the other party’s Exposure (as adjusted by any Independent Amounts) exceeds that Threshold.'},
									{xtype: 'spacer', height: 4, style: 'border-top: 2px solid #909090'},
									{fieldLabel: 'Total Requirement', name: 'calculatedTotalRequirement', qtip: 'The total amount of Collateral required (prior to Rounding & Minimum Transfer Amount being applied).'},
									{xtype: 'spacer', height: 10},
									{fieldLabel: 'Client Posted Collateral', name: 'postedCollateralHaircutValue', qtip: 'After haircut Collateral previously posted by the Client.'},
									{fieldLabel: 'Counterparty Posted Collateral', name: 'postedCounterpartyCollateralHaircutValue', qtip: 'After haircut Collateral previously posted by the Counterparty.'},
									{xtype: 'spacer', height: 10},
									{fieldLabel: 'Net Requirement', name: 'calculatedNetRequirement', qtip: 'Collateral movement prior to applying Rounding & Minimum Transfer Amount. Formula: Total Requirement + Client Posted Collateral - Counterparty Posted Collateral'},
									{fieldLabel: 'Delivery/Return Amount', name: 'calculatedChangeAmount', qtip: 'The Net Requirement after Rounding, and then Minimum Transfer Amount have been applied. Positive Amount means Client should be Receiving Collateral. Negative Amount means Client should be Sending Collateral.'},
									{xtype: 'spacer', height: 10},
									{fieldLabel: 'Client Collateral Change', name: 'collateralChangeAmount', qtip: 'How we see the Client Collateral balance should change based on today’s calculations. Negative Amount means Client should be Sending a Collateral Call. Positive Amount means Client should be Receiving a Collateral Call.'},
									{fieldLabel: 'Counterparty Collateral Change', name: 'counterpartyCollateralChangeAmount', qtip: 'How we see the Counterparty Collateral balance should change based on today’s calculations. Positive Amount means Client should be Sending a Collateral Call. Negative Amount means Client should be Receiving a Collateral Call.'}
								],
								config: {columnWidth: 0.3, labelWidth: 180}
							}, {
								rows: [
									{xtype: 'spacer', height: 180},
									{fieldLabel: 'Rounding', name: 'contractRounding'},
									{fieldLabel: 'Minimum Transfer Amount', name: 'calculatedMinTransferAmount', qtip: 'The Minimum Amount of total Collateral to be Delivered or Returned for a given Margin Call.'},
									{xtype: 'spacer', height: 10},
									{fieldLabel: 'Counterparty Call Amount', name: 'counterpartyCollateralCallAmount', readOnly: false, submitValue: true, qtip: 'If the Counterparty sends a Collateral Call, enter that amount here (regardless if we agree to it).'},
									{fieldLabel: 'Agreed Upon Amount', name: 'transferAmount', readOnly: false, submitValue: true, qtip: 'Enter the total Collateral amount agreed upon here.'}
								],
								config: {columnWidth: 0.3, labelWidth: 180}
							}
						]
					}
				],
				listeners: {
					afterload: function() {
						this.updateCounterparty();
						const tb = this.ownerCt.getTopToolbar();
						tb.getComponent('workflowStatusField').setText(this.getFormValue('workflowStatus.name'));
						tb.getComponent('workflowStateField').setText(this.getFormValue('workflowState.name'));
						if (TCG.isFalse(this.getFormValue('parentCollateralBalance'))) {
							const tabPanel = this.ownerCt.ownerCt;
							tabPanel.getComponent('balanceDetailTab').hide();
						}
					}
				}

			},

			{
				xtype: 'panel',
				flex: 1,
				region: 'center',
				split: true,
				layout: 'fit',
				layoutConfig: {align: 'stretch'},
				items: [{
					xtype: 'gridpanel',
					name: 'collateralPositionList',
					instructions: 'Posted Collateral for Exposure/MTM and Independent Amount.',
					getTopToolbarFilters: function(toolbar) {
						return ['-', {fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'positionDate'}];
					},
					getLoadParams: function(firstLoad) {
						const fp = this.getWindow().getMainFormPanel();
						const tb = this.getTopToolbar();
						const positionDate = TCG.getChildByName(tb, 'positionDate');
						if (TCG.isBlank(positionDate.getValue())) {
							const balanceDate = fp.getFormValue('balanceDate', false, false);
							positionDate.setValue(TCG.parseDate(Clifton.calendar.getBusinessDayFromDate(TCG.parseDate(balanceDate), 1)).format('m/d/Y'));
						}
						const holdingAccountId = fp.getFormValue('holdingInvestmentAccount.id', false, false);
						return {
							holdingInvestmentAccountId: holdingAccountId,
							transactionDate: TCG.parseDate(positionDate.getValue()).format('m/d/Y')
						};
					},
					columns: [
						{header: 'GL Account', width: 175, dataIndex: 'accountingAccount.label', filter: false},
						{header: 'Security', width: 100, dataIndex: 'security.symbol', filter: false},
						{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
						{header: 'Quantity', width: 75, dataIndex: 'quantity', type: 'currency', summaryType: 'sum', useNull: true},
						{header: 'Cost Price', width: 75, dataIndex: 'costPrice', type: 'currency', numberFormat: '0,000.0000', useNull: true},
						{header: 'Market Price', width: 75, dataIndex: 'marketPrice', type: 'currency', numberFormat: '0,000.0000', useNull: true},
						{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},
						{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency', summaryType: 'sum'},
						{
							header: 'Collateral Market Value', width: 120, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
							},
							summaryType: 'sum',
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, false, '0,000.00');
								}
							}
						},
						{width: 1}
					],
					plugins: {ptype: 'gridsummary'}
				}]
			}
		]
	},

	balanceDetailTab: {
		title: 'Balance Details',
		items: [{
			name: 'collateralBalanceListFind',
			xtype: 'gridpanel',
			instructions: 'Includes child collateral balances for selected Collateral Balance. This happens when collateral balances from multiple accounts roll up into a single entry. Child entries reference parent balance via "parent" field.',
			getLoadParams: function(firstLoad) {
				const grid = this;
				return {
					requestedMaxDepth: 4,
					parentId: grid.getWindow().getMainFormId()
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=OTC Collateral Workflow'}, hidden: true, viewNames: ['Export Friendly']},
				{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View', 'Export Friendly']},
				{header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'}},
				{header: 'Holding Account ID', width: 75, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
				{header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
				{header: 'Holding Company', width: 150, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},

				{header: 'ISDA', width: 100, dataIndex: 'holdingInvestmentAccount.businessContract.name', hidden: true, filter: {type: 'combo', searchFieldName: 'businessContractId', url: 'businessContractListFind.json?contractTypeName=ISDA'}},

				{header: 'Exposure/MTM', width: 100, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Client Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Client Posted Collateral', width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCollateralValue'], r.data['postedCollateralValue'], '0,000.00');
					}
				},


				{header: 'Counterparty Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCounterpartyCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Counterparty Posted Collateral', width: 100, dataIndex: 'postedCounterpartyCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCounterpartyCollateralValue'], r.data['postedCounterpartyCollateralHaircutValue'], '0,000.00');
					}
				},
				{
					//Doesn't respect the width - 23px is the element style that comes through; can't seem to override in extJs3
					width: 10, menuDisabled: true, draggable: false, resizable: true,
					renderer: function(v, p, r) {
						//Couldn't figure out why it was basically inheriting the next columns values...
						return '';
					}
				},
				{header: 'Client Collateral Change', width: 100, dataIndex: 'collateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0;'},
				{header: 'Counterparty Collateral Change', width: 100, dataIndex: 'counterpartyCollateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0;'},
				{
					width: 10, menuDisabled: true, draggable: false, resizable: true,
					renderer: function(v, p, r) {
						return '';
					}
				},
				{header: 'Client Ending Collateral', width: 100, dataIndex: 'endingPostedCollateralValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;'},
				{header: 'Counterparty Ending Collateral', width: 100, dataIndex: 'endingPostedCounterpartyCollateralValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;'},

				{header: 'Agreed Upon Amount', dataIndex: 'transferAmount', width: 125, useNull: true, type: 'currency'},

				{header: 'Threshold', dataIndex: 'thresholdAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Rounding', dataIndex: 'contractRounding', width: 100, hidden: true, useNull: true, type: 'float'},
				{header: 'Counterparty Threshold', width: 100, hidden: true, dataIndex: 'thresholdCounterpartyAmount', useNull: true, type: 'currency'},
				{header: 'Minimum Transfer Amount', dataIndex: 'minTransferAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Counterparty Minimum Transfer Amount', dataIndex: 'minTransferCounterpartyAmount', width: 100, hidden: true, useNull: true, type: 'currency'}
			],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			}
		}]
	},

	collateralTransactionTab: {
		title: 'Collateral Transactions',
		items: [{
			instructions: 'These are detailed collateral entries found in the general ledger for the given holding account.',
			xtype: 'accounting-transactionGrid',
			includeClientAccountFilter: false,
			columnOverrides: [
				{dataIndex: 'clientInvestmentAccount.label', hidden: true},
				{dataIndex: 'holdingInvestmentAccount.label', hidden: true},
				{dataIndex: 'localDebit', hidden: true},
				{dataIndex: 'localCredit', hidden: true},
				{dataIndex: 'exchangeRateToBase', hidden: true},
				{dataIndex: 'positionCostBasis', hidden: true},
				{dataIndex: 'settlementDate', hidden: true}
			],
			getCustomFilters: function(firstLoad) {
				const fp = this.getWindow().getMainFormPanel();
				if (firstLoad) {
					// default to last -30 days of transactions (since balance date) to now
					const dateV = TCG.parseDate(fp.getFormValue('balanceDate'));
					this.setFilterValue('transactionDate', {'after': dateV.add(Date.DAY, -30)});
					this.getWindow().setCustomCollateralTransactionFilters(this);
				}
				if (TCG.isFalse(fp.getFormValue('parentCollateralBalance'))) {
					return {
						collateralAccountingAccount: true,
						holdingInvestmentAccountId: fp.getFormValue('holdingInvestmentAccount.id')
					};
				}
				else {
					return {
						collateralAccountingAccount: true,
						holdingInvestmentAccountGroupId: fp.getFormValue('holdingInvestmentAccountGroup.id')
					};
				}

			}
		}]
	}
});
