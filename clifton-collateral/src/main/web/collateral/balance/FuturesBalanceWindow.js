TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.FuturesBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	titlePrefix: 'Futures Collateral Balance',
	iconCls: 'run',
	height: 870,
	showCounterpartyPositionTransferBtns: true,
	getAccountingPositionTransferTypeParams: function() {
		return {cash: false, counterpartyCollateral: false};
	},
	addAdditionalActionButtons: function(tbarItems) {

		tbarItems.push({
				iconCls: 'workflow',
				tooltip: 'Show workflow history for this entity.',
				handler: function() {
					const fp = this.ownerCt.ownerCt.items.get(0);
					Clifton.workflow.Toolbar.prototype.doShowWorkflowHistory('CollateralBalance', fp);
				}
			}, '-',
			{
				text: 'Open M2M',
				iconCls: 'exchange',
				handler: function() {

					const formValues = TCG.getParentTabPanel(this).getWindow().getMainForm().formValues;
					const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', formValues));
					const holdingInvestmentAccountId = TCG.getValue('holdingInvestmentAccount.id', formValues);
					const m2mLoader = new TCG.data.JsonLoader({
						params: {holdingInvestmentAccountId: holdingInvestmentAccountId, markDate: transactionDate.format('m/d/Y')},
						onLoad: function(record, conf) {
							if (record && record.length === 1) {
								const m2m = record[0];
								const config = {
									defaultDataIsReal: true,
									defaultData: m2m,
									params: {id: m2m.id}
								};
								TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
							}
							else {
								TCG.showError('Cannot find M2M daily associated with that record.');
							}
						}
					});
					m2mLoader.load('accountingM2MDailyListPopulatedFind.json?enableOpenSessionInView=true&requestedMaxDepth=4');
				}
			});
	},
	balanceDetailTab: {
		title: 'Balance Details',
		items: [{
			name: 'collateralBalanceListFind',
			xtype: 'gridpanel',
			instructions: 'Includes child collateral balances for selected Collateral Balance. This happens when collateral balances from multiple accounts roll up into a single entry. Child entries reference parent balance via "parent" field.',
			getLoadParams: function(firstLoad) {
				const grid = this;
				return {
					requestedMaxDepth: 4,
					parentId: grid.getWindow().getMainFormId()
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'PID', width: 15, dataIndex: 'parent.id', useNull: true, type: 'int', doNotFormat: true, filter: {searchFieldName: 'parentId'}, hidden: true},
				{
					header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'},
					renderer: function(v, metaData, r) {
						const note = r.data.note;
						if (TCG.isNotBlank(note)) {
							const qtip = r.data.note;
							metaData.css = 'amountAdjusted';
							metaData.attr = TCG.renderQtip(qtip);
						}
						return v;
					}
				},
				{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
				{header: 'Client Account', width: 75, dataIndex: 'clientInvestmentAccount.number', filter: {searchFieldName: 'clientInvestmentAccountLabel'}},

				{header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', defaultSortColumn: true, filter: {searchFieldName: 'holdingInvestmentAccountLabel'}},
				{header: 'Holding Company', width: 130, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},

				{header: 'Collateral Account', width: 75, dataIndex: 'collateralInvestmentAccount.number', filter: {searchFieldName: 'collateralInvestmentAccountLabel'}},
				{header: 'Collateral Company', width: 130, dataIndex: 'collateralInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'collateralCompanyName'}},

				{header: 'Collateral CCY', width: 50, hidden: true, dataIndex: 'collateralCurrency.symbol', filter: {type: 'combo', searchFieldName: 'collateralCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Positions Market Value', width: 100, hidden: true, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Required Collateral (External)', width: 100, dataIndex: 'externalCollateralRequirement', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Posted Collateral (External)', width: 100, dataIndex: 'externalCollateralAmount', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Excess Collateral (External)', width: 100, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true},

				{
					header: 'Required Collateral', width: 100, hidden: true, dataIndex: 'collateralRequirement', useNull: true, negativeInRed: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, metaData, r) {
						const ext = r.data.externalCollateralRequirement;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralRequirement'], r.data['externalCollateralRequirement'], '0,000.00');
					}
				},
				{header: 'Posted Collateral (Original)', width: 100, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Posted Collateral', hidden: true, width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency',
					renderer: function(v, metaData, r) {
						const ext = r.data.externalCollateralAmount;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton (Original):</td><td align="right">' + Ext.util.Format.number(r.data.postedCollateralValue, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Clifton (Adjusted):</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralAmount'], r.data['externalCollateralAmount'], '0,000.00');
					}
				},
				{
					header: 'Excess Collateral', width: 100, dataIndex: 'excessCollateral', type: 'currency', negativeInRed: true, hidden: true,
					renderer: function(v, metaData, r) {
						const ext = r.data.externalExcessCollateral;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalExcessCollateral'], r.data['externalExcessCollateral'], '0,000.00');
					}
				},
				{header: 'Transfer Amount', width: 100, dataIndex: 'transferAmount', type: 'currency', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Booking Date', width: 50, dataIndex: 'bookingDate', hidden: true},
				{header: 'Booked', width: 50, dataIndex: 'booked', type: 'boolean'}
			],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			}
		}]
	},

	generalInfoTab: {
		title: 'Info',

		items: [{
			xtype: 'formpanel',
			url: 'collateralBalance.json',
			loadValidation: false,
			labelWidth: 130,

			listeners: {
				afterload: function(panel) {
					const f = this.getForm();
					const booked = TCG.getValue('booked', f.formValues);
					if (TCG.isTrue(booked)) {
						this.setReadOnly(true);
						const fs = TCG.getChildByName(panel, 'transferAmountActionFieldSet');
						if (fs) {
							fs.setVisible(false);
						}
					}
				}
			},

			getWarningMessage: function(form) {
				let msg = undefined;
				if (form.formValues.bookingDate) {
					msg = 'This collateral balance transfer was processed on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
				}
				return msg;
			},

			items: [
				{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
				{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .70,
							layout: 'form',
							items: [
								{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Collateral Account', name: 'collateralInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Company', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
								{fieldLabel: 'Company', name: 'collateralInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'}
							]
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .40,
							layout: 'form',
							items: [
								{fieldLabel: 'Currency', name: 'collateralCurrency.symbol', xtype: 'linkfield', detailIdField: 'collateralCurrency.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'displayfield', type: 'date'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{xtype: 'accounting-bookingdate', sourceTable: 'CollateralBalance'}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 150,
					items: [
						{
							columnWidth: .42,
							layout: 'form',
							defaults: {anchor: '-10'},
							items: [
								{fieldLabel: 'Positions Market Value', name: 'positionsMarketValue', xtype: 'currencyfield', readOnly: true},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Our Amount'},
										{value: 'Broker Amount'}
									]
								},
								{
									fieldLabel: 'Required Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'collateralRequirement'},
										{name: 'externalCollateralRequirement'}
									]
								},
								{
									fieldLabel: 'Posted (Original)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralValue'},
										{xtype: 'label', html: '&nbsp;'}
									]
								},
								{
									fieldLabel: 'Posted (After Haircut)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralHaircutValue'},
										{name: 'externalCollateralAmount'}
									]
								},
								{
									fieldLabel: 'Excess Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'excessCollateral'},
										{name: 'externalExcessCollateral'}
									]
								},
								{xtype: 'label', html: '<hr/>'},
								{fieldLabel: '<b>Transfer Amount</b>', name: 'transferAmount', xtype: 'currencyfield'},
								{xtype: 'label', html: '<hr/>'},
								{
									name: 'accountingBalanceValueListFind',
									xtype: 'gridpanel',
									title: 'Positions Available for Collateral',
									border: true,
									hideStandardButtons: true,
									height: 200,
									columns: [
										{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
										{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
										{header: 'Hierarchy', width: 130, dataIndex: 'investmentSecurity.instrument.hierarchy.nameExpanded'},
										{header: 'GL Account', width: 50, dataIndex: 'accountingAccount.name'},
										{header: 'Open Date', width: 50, dataIndex: 'originalTransactionDate', hidden: true},
										{header: 'Open Price', width: 60, dataIndex: 'price', type: 'float', negativeInRed: true, useNull: true, hidden: true},
										{header: 'FX Rate', width: 50, dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000', negativeInRed: true, hidden: true},
										{header: 'Open Qty', width: 50, dataIndex: 'quantity', type: 'float', negativeInRed: true, useNull: true, hidden: true},
										{header: 'Quantity', dataIndex: 'quantity', width: 45, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
										{header: 'Base Market Value', dataIndex: 'baseMarketValue', type: 'currency', negativeInRed: true, positiveInGreen: true, width: 75, summaryType: 'sum', nonPersistentField: true}

									],
									plugins: {ptype: 'gridsummary'},

									getLoadParams: function() {
										const formValues = this.getWindow().getMainForm().formValues;
										const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', formValues));
										const clientInvestmentAccountId = TCG.getValue('clientInvestmentAccount.id', formValues);
										const holdingInvestmentAccountId = TCG.getValue('collateralInvestmentAccount.id', formValues);
										return {
											clientInvestmentAccountId: clientInvestmentAccountId == null ? -1 : clientInvestmentAccountId,
											holdingInvestmentAccountId: holdingInvestmentAccountId == null ? -1 : holdingInvestmentAccountId,
											transactionDate: transactionDate.format('m/d/Y'),
											accountingAccountTypes: ['Asset'],
											accountingAccountIdName: 'EXCLUDE_RECEIVABLE_COLLATERAL',
											includeUnderlyingPrice: false
										};
									}
								}
							]
						},
						{
							columnWidth: .58,
							layout: 'form',
							defaults: {anchor: '0'},
							labelWidth: 50,
							items: [
								{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 55},
								{
									layout: 'fit',
									flex: 1,
									height: 160,
									items: [{
										name: 'collateralBalanceListFind',
										xtype: 'gridpanel',
										title: 'Balance History',
										border: true,
										hideStandardButtons: true,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
											{header: 'Date', width: 35, dataIndex: 'balanceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
											{header: 'Positions Market Value', width: 50, hidden: true, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

											{header: 'Req\'d (External)', width: 50, hidden: true, dataIndex: 'externalCollateralRequirement', useNull: true, type: 'currency', negativeInRed: true},
											{header: 'Posted (External)', width: 50, hidden: true, dataIndex: 'externalCollateralAmount', useNull: true, type: 'currency', negativeInRed: true},
											{header: 'Excess (External)', width: 50, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true},

											{header: 'Transfer Amount', width: 50, dataIndex: 'transferAmount', type: 'currency', negativeInRed: true},
											{header: 'Booking Date', width: 50, dataIndex: 'bookingDate', hidden: true},
											{header: 'Booked', width: 30, dataIndex: 'booked', type: 'boolean'},
											{header: 'Note', width: 90, dataIndex: 'note'}
										],
										getLoadParams: function() {
											// Show previous 5 Balance Records
											const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
											const start = Clifton.calendar.getBusinessDayFromDate(dateV, -5);
											const parent = TCG.getValue('parent', this.getWindow().getMainForm().formValues);
											this.setFilterValue('balanceDate', {'after': start, 'before': dateV});
											return {
												parentIsNull: TCG.isNull(parent),
												holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues)
											};
										},
										editor: {
											detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
											drillDownOnly: true
										}
									}]
								},
								{
									layout: 'fit',
									flex: 1,
									height: 175,
									items: [{
										name: 'collateralAggregatePositionList',
										xtype: 'gridpanel',
										title: 'Collateral Positions',
										border: true,
										hideStandardButtons: true,
										appendStandardColumns: false,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'Security', width: 100, dataIndex: 'security.symbol', filter: false},
											{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
											{header: 'Qty', width: 100, dataIndex: 'quantity', type: 'float', useNull: true},
											{header: 'Cost Price', width: 90, dataIndex: 'costPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000', hidden: true},
											{header: 'Market Price', width: 90, dataIndex: 'marketPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000'},
											{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},

											{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency', summaryType: 'sum'},
											{
												header: 'Collateral Market Value', width: 150, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
												renderer: function(v, p, r) {
													return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
												},
												summaryType: 'sum',
												summaryRenderer: function(v) {
													if (v !== '') {
														return TCG.renderAmount(v, false, '0,000.00');
													}
												}
											}
										],
										plugins: {ptype: 'gridsummary'},
										getLoadParams: function() {
											const formValues = this.getWindow().getMainForm().formValues;
											const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', formValues));
											const holdingInvestmentAccountId = TCG.getValue('holdingInvestmentAccount.id', formValues);
											return {
												holdingInvestmentAccountId: holdingInvestmentAccountId == null ? -1 : holdingInvestmentAccountId,
												transactionDate: transactionDate.format('m/d/Y')
											};
										}
									}]
								}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					title: 'Associated Position Transfers',
					height: 160,
					name: 'accountingPositionTransferListFind',
					xtype: 'gridpanel',
					instructions: 'Position transfers are used to transfer existing positions (use Simple Journals for cash movements) between investment accounts. Pending transfers are transfers that have not been booked yet.',
					importTableName: 'AccountingPositionTransfer',
					importComponentName: 'Clifton.accounting.positiontransfer.upload.PositionTransferUploadWindow',
					columns: [
						{
							header: 'Transfer ID', width: 50, dataIndex: 'id',
							renderer: function(v, metaData, r) {
								const transactionID = v;
								const detailList = TCG.data.getData('accountingPositionTransferDetailListFind.json?positionTransferId=' + transactionID, this);
								let qtip = '<table  style="border-collapse: separate; border-spacing: 5px;">';
								qtip += '<thead><tr><th>Security</th><th>Quantity</th></tr></thead>';
								for (let i = 0; i < detailList.length; i++) {
									qtip += '<tr><td>' + detailList[i].security.label + '</td><td align="right">' + Ext.util.Format.number(detailList[i].quantity, '0,000') + '</td></tr>';
								}
								qtip += '</table>';
								metaData.attr = 'qtip=\'' + qtip + '\'';

								return transactionID;
							}
						},
						{header: 'Transfer Type', width: 130, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'transferTypeId', url: 'accountingPositionTransferTypeListFind.json'}},
						{header: 'From (Client Account)', width: 180, dataIndex: 'fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'From (Holding Account)', width: 180, dataIndex: 'fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'To (Client Account)', width: 180, dataIndex: 'toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'To (Holding Account)', width: 180, dataIndex: 'toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Collateral', width: 50, dataIndex: 'collateralTransfer', type: 'boolean'},
						{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
						{
							header: 'Violation Status', width: 80, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
							renderer: function(v, p, r) {
								return (Clifton.rule.violation.renderViolationStatus(v));
							}
						},
						{header: 'FK Field ID', width: 50, dataIndex: 'sourceFkFieldId', useNull: true, hidden: true, tooltip: 'Source Entity ID for when this transfer was initiated from a different part of the system'},
						{header: 'Price Date', width: 50, dataIndex: 'exposureDate', hidden: true},
						{header: 'Settlement Date', width: 50, dataIndex: 'settlementDate', hidden: true},
						{header: 'Price Date', width: 60, dataIndex: 'exposureDate', hidden: true},
						{header: 'Transaction Date', width: 60, dataIndex: 'transactionDate'},
						{header: 'Booked', width: 80, dataIndex: 'bookingDate'}
					],
					getLoadParams: function(firstLoad) {
						const params = {
							sourceSystemTableNameEquals: 'CollateralBalance',
							sourceFkFieldId: this.getWindow().getMainForm().formValues.id
						};
						return params;
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								return '';
							}
						},
						getDetailPageClassByText: function(text) {
							return 'Clifton.accounting.positiontransfer.TransferWindow';
						},
						getDeleteURL: function() {
							return 'accountingPositionTransferDelete.json';
						},
						addEditButtons: function(toolBar, gridPanel) {
							this.addToolbarDeleteButton(toolBar, gridPanel);
							toolBar.add({
								text: 'Book',
								tooltip: 'Book selected transfer',
								iconCls: 'book-open-blue',
								scope: this,
								handler: function() {
									this.bookSelectedTransfer(false);
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Book and Post',
								tooltip: 'Book selected transfer and immediately post it to the General Ledger',
								iconCls: 'book-red',
								scope: this,
								handler: function() {
									this.bookSelectedTransfer(true);
								}
							});
							toolBar.add('-');
						},
						bookSelectedTransfer: function(post) {
							let confirmTitle = 'Book Transfer';
							let confirmMessage = 'Would you like to book selected transfer?';
							if (post) {
								confirmTitle = 'Book & Post Transfer';
								confirmMessage = 'Would you like to book selected transfer and immediately post it to the General Ledger?';
							}

							const grid = this.grid;
							const sm = grid.getSelectionModel();
							if (TCG.isEquals(sm.getCount(), 0)) {
								TCG.showError('Please select a row to be booked.', 'No Row(s) Selected');
							}
							else if (TCG.isNotEquals(sm.getCount(), 1)) {
								TCG.showError('Multi-selection bookings are not supported yet.  Please select one row.', 'NOT SUPPORTED');
							}
							else {
								Ext.Msg.confirm(confirmTitle, confirmMessage, function(a) {
									if (TCG.isEquals(a, 'yes')) {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid,
											waitMsg: post ? 'Booking and Posting...' : 'Booking...',
											params: {
												journalTypeName: 'Transfer Journal',
												sourceEntityId: sm.getSelected().id,
												postJournal: post
											},
											timeout: 180000,
											onLoad: function(record, conf) {
												grid.ownerGridPanel.reload();
											}
										});
										loader.load('accountingJournalBook.json?enableOpenSessionInView=true&requestedPropertiesRoot=data&requestedProperties=id');
									}
								});
							}
						}
					}
				}
			]

		}]
	}
});


