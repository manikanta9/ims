// not the actual window but a window selector based on 'instant' property
Clifton.collateral.balance.BalanceWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'collateralBalance.json',

	getClassName: function(config, entity) {
		const collateralTypeName = entity ? entity.collateralType.name : config.collateralType.name;
		if (TCG.isEqualsStrict('OTC Collateral', collateralTypeName)) {
			return 'Clifton.collateral.balance.OTCBalanceWindow';
		}
		else if (TCG.isEqualsStrict('REPO Collateral', collateralTypeName)) {
			return 'Clifton.collateral.balance.REPOBalanceWindow';
		}
		else if (TCG.isEqualsStrict('Cleared OTC Collateral', collateralTypeName)) {
			return 'Clifton.collateral.balance.ClearedOTCBalanceWindow';
		}
		else if (TCG.isEqualsStrict('Options Margin Collateral', collateralTypeName)) {
			return 'Clifton.collateral.balance.options.OptionsMarginBalanceWindow';
		}
		else if (TCG.isEqualsStrict('Options Escrow Collateral', collateralTypeName)) {
			return 'Clifton.collateral.balance.options.OptionsEscrowBalanceWindow';
		}
		return 'Clifton.collateral.balance.FuturesBalanceWindow';
	}
});
