TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.options.OptionsMarginBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	titlePrefix: 'Options Collateral Balance (Margin)',
	iconCls: 'run',
	height: 540,
	getAccountingPositionTransferTypeParams: function() {
		return {counterpartyCollateral: false};
	},
	addInstructionPreview: function(tbarItems) {
		//Don't add instruction preview for options
	},
	generalInfoTab: {
		title: 'Info',
		items: [{
			xtype: 'formpanel',
			url: 'collateralBalance.json',
			additionalPropertiesToRequest: 'collateralRequirement',
			loadValidation: false,
			labelWidth: 130,
			listeners: {
				afterload: function(panel) {
					const form = this.getForm();
					const formValues = form.formValues;
					const booked = TCG.getValue('booked', formValues);
					if (TCG.isEqualsStrict(booked, true)) {
						this.setReadOnly(true);
						const fs = TCG.getChildByName(panel, 'transferAmountActionFieldSet');
						if (fs) {
							fs.setVisible(false);
						}
					}
					const type = TCG.getValue('holdingInvestmentAccount.type.name', formValues);
					const composite = (type === 'Portfolio Margin') ? form.findField('optionsRequirementComposite', true) : form.findField('requirementComposite', true);
					if (composite) {
						const el = composite.el.up('.x-form-item');
						composite.ownerCt.items.remove(composite);
						form.remove(composite, true);
						if (el) {
							el.remove();
						}
						panel.doLayout();
					}
				}
			},
			getWarningMessage: function(form) {
				let msg = undefined;
				if (form.formValues.bookingDate) {
					msg = 'This collateral balance transfer was processed on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
				}
				return msg;
			},
			resetTransferAmount: function() {
				const f = this.getForm();

				const pt = f.findField('chooseTransferAmountPosted');
				if (pt.getValue()) {
					let posted;
					if (TCG.isEqualsStrict(pt.getValue().getGroupValue(), 'OUR')) {
						posted = this.getFormValue('postedCollateralHaircutValue');
					}
					else {
						posted = this.getFormValue('externalCollateralAmount');
					}


					const rt = f.findField('chooseTransferAmountRequired');
					if (rt.getValue()) {
						let required;
						if (TCG.isEqualsStrict(rt.getValue().getGroupValue(), 'OUR')) {
							required = this.getFormValue('collateralRequirement');
						}
						else {
							required = this.getFormValue('externalCollateralRequirement');
						}
						if (!posted) {
							posted = 0;
						}
						if (!required) {
							required = 0;
						}
						let trans = posted - required;
						const balanceDate = TCG.parseDate(TCG.getValue('balanceDate', f.formValues)).format('m/d/Y');
						trans = TCG.getResponseText('collateralTransferAmountRounded.json?collateralTypeName=Futures Collateral&holdingAccountId=' + TCG.getValue('holdingInvestmentAccount.id', f.formValues) + '&amount=' + trans + '&balanceDate=' + balanceDate, this);
						this.setFormValue('transferAmount', trans);
					}
				}
			},
			items: [
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .40,
							layout: 'form',
							items: [
								{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
								{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Collateral Account', name: 'collateralInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Currency', name: 'collateralCurrency.symbol', xtype: 'linkfield', detailIdField: 'collateralCurrency.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Servicing Team', name: 'clientInvestmentAccount.teamSecurityGroup.name', detailIdField: 'clientInvestmentAccount.teamSecurityGroup.id', xtype: 'linkfield', url: 'securityGroupTeamList.json', loadAll: true, detailPageClass: 'Clifton.security.user.GroupWindow'},
								{fieldLabel: 'Service', detailIdField: 'clientInvestmentAccount.businessService.id', name: 'clientInvestmentAccount.businessService.name', xtype: 'linkfield', url: 'businessServiceListFind.json?serviceLevelType=SERVICE', detailPageClass: 'Clifton.business.service.ServiceWindow'},
								{fieldLabel: 'Company', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
								{fieldLabel: 'Company', name: 'collateralInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							items: [
								{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'displayfield', type: 'date'},
								{xtype: 'accounting-bookingdate', sourceTable: 'CollateralBalance'},
								{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield'},
								{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .42,
							layout: 'form',
							defaults: {anchor: '-10'},
							items: [
								{
									xtype: 'hidden',
									name: 'holdingInvestmentAccount.type.name'
								},
								{
									xtype: 'hidden',
									name: 'postedHaircutAmount'
								},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Our Amount'},
										{value: 'Broker Amount'}
									]
								},
								{
									fieldLabel: 'Equity',
									xtype: 'compositefield',
									qtip: 'Market value of holding account (including collateral), cash and 25% of long options market value if greater than 9 months to expiration.',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'optionsPositionsMarketValue'},
										{name: 'externalCollateralAmount'}
									]
								},
								{
									fieldLabel: 'Requirement',
									name: 'requirementComposite',
									xtype: 'compositefield',
									qtip: 'Collateral requirement (Margin Positions) plus posted haircut.',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{
											name: 'collateralRequirement'
										},
										{xtype: 'displayfield', value: '&nbsp;'}
									]
								},
								{
									fieldLabel: 'Requirement',
									name: 'optionsRequirementComposite',
									xtype: 'compositefield',
									qtip: 'Collateral requirement (Margin Positions) plus posted haircut.',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'optionsCollateralRequirement',
											listeners: {
												beforerender: function(f) {
													const fp = TCG.getParentFormPanel(f);
													const hc = TCG.getValue('postedHaircutAmount', fp.form.formValues);
													f.qtip = 'Haircut<\p>' + Ext.util.Format.number(hc, '0,000.00');
												}
											}
										},
										{
											name: 'externalCollateralRequirement'
										}
									]
								},
								{
									fieldLabel: 'Excess/(Deficit)',
									xtype: 'compositefield',
									qtip: 'Equity - Requirement.  Difference is an Excess (pull-back) when positive otherwise Deficit (post). ',
									defaults: {
										xtype: 'textfield',
										style: 'text-align: right;',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'excessOptionsEquityCollateral',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0,000.00'));
												}
											}
										},
										{
											name: 'externalExcessCollateral',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0,000.00'));
												}
											}
										}
									]
								},
								{
									fieldLabel: 'Excess/(Deficit) %',
									xtype: 'compositefield',
									labelSeparator: '',
									defaults: {
										xtype: 'textfield',
										style: 'text-align: right',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'excessOptionsEquityCollateralPercentage',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0.00%'));
												}
											}
										},
										{
											name: 'externalExcessCollateralPercentage',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0.00%'));
												}
											}
										}
									]
								},
								{
									fieldLabel: 'Target Movement',
									xtype: 'compositefield',
									qtip: 'If Excess/(Deficit) is a Deficit, post to 107.5% of requirement. If positive and over 115.0% collateralized, pull back 7.5%. If positive and requirement = 0, pull back all collateral.',
									defaults: {
										xtype: 'textfield',
										style: 'text-align: right;',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'optionsTargetMovement',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0,000.00'));
												}
											}
										},
										{xtype: 'displayfield', value: '&nbsp;'}
									]
								},
								{
									fieldLabel: 'Target Movement %',
									xtype: 'compositefield',
									defaults: {
										xtype: 'textfield',
										style: 'text-align: right;',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'optionsTargetMovement',
											listeners: {
												beforerender: function(field) {
													field.setValue(Ext.util.Format.number(field.value, '0,000.00'));
												}
											}
										},
										{xtype: 'displayfield', value: '&nbsp;'}
									]
								},
								{
									fieldLabel: 'Agreed Movement',
									xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										style: 'text-align: right;',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'agreedMovementAmount',
											qtip: 'Market value of the position or cash being posted as collateral.  Once the movement is booked, its the market value.'
										},
										{xtype: 'displayfield', value: '&nbsp;'}
									]
								}
							]

						},
						{
							columnWidth: .58,
							layout: 'form',
							defaults: {anchor: '0'},
							labelWidth: 50,
							items: [
								{
									layout: 'fit',
									flex: 1,
									height: 160,
									items: [{
										name: 'collateralAggregatePositionList',
										xtype: 'gridpanel',
										title: 'Collateral Positions',
										border: true,
										hideStandardButtons: true,
										appendStandardColumns: false,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'Security', width: 100, dataIndex: 'security.symbol', filter: false},
											{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
											{header: 'Qty', width: 100, dataIndex: 'quantity', type: 'float', useNull: true},
											{header: 'Cost Price', width: 90, dataIndex: 'costPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000', hidden: true},
											{header: 'Market Price', width: 90, dataIndex: 'marketPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000'},
											{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},

											{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency', summaryType: 'sum'},
											{
												header: 'Collateral Market Value', width: 150, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
												renderer: function(v, p, r) {
													return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
												},
												summaryType: 'sum',
												summaryRenderer: function(v) {
													if (v !== '') {
														return TCG.renderAmount(v, false, '0,000.00');
													}
												}
											}
										],
										plugins: {ptype: 'gridsummary'},
										getLoadParams: function() {
											const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
											return {
												holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues),
												transactionDate: transactionDate.format('m/d/Y')
											};
										}
									}]
								},
								{fieldLabel: 'Notes', xtype: 'label', style: {marginTop: '10px'}},
								{hideLabel: true, xtype: 'textarea', name: 'note', height: 55}
							]
						}
					]
				}
			]
		}]
	},
	balanceDetailTab: {
		id: 'balanceDetailTab'
	},
	positionsTab: {
		id: 'positionsTab'
	},
	marginPositionsTab: {
		title: 'Margin Positions',
		items: [{
			name: 'collateralOptionsStrategyPositionList',
			xtype: 'gridpanel',
			appendStandardColumns: false,
			useDynamicTooltipColumns: true,
			dynamicTooltipTitle: 'Calculation Details',
			cacheDynamicTooltip: false,
			additionalPropertiesToRequest: 'calculationExplanation',
			columns: [
				{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 100},
				{header: 'Quantity', dataIndex: 'remainingQuantity', type: 'float', useNull: true, negativeInRed: true, width: 70},
				{header: 'End Date', dataIndex: 'investmentSecurity.endDate', width: 50, defaultSortColumn: true},
				{header: 'Market Price', dataIndex: 'optionPrice', type: 'float', useNull: true, negativeInRed: true, width: 70},
				{header: 'Option Proceeds', dataIndex: 'optionProceeds', type: 'float', width: 70},
				{header: 'Strike Price', dataIndex: 'strikePrice', type: 'float', width: 70},
				{header: 'Option Notional', dataIndex: 'strikeValue', type: 'float', width: 70},
				{header: 'Underlying Price', dataIndex: 'underlyingPrice', type: 'float', width: 70},
				{header: 'Underlying Value', dataIndex: 'underlyingValue', type: 'float', width: 70},
				{header: 'Required Collateral', dataIndex: 'requiredCollateral', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', sortable: false},
				{header: 'Base Required Collateral', dataIndex: 'baseRequiredCollateral', type: 'currency', negativeInRed: true, width: 90, summaryType: 'sum', sortable: false, hidden: true},
				{header: 'Strategy', dataIndex: 'positionGroupId', width: 80, hidden: true},
				{header: 'Transaction ID', dataIndex: 'accountingTransactionId', width: 50, hidden: true},
				{
					header: '&nbsp;', dataIndex: 'calculationExplanation', width: 10, filter: false, sortable: false, hidden: true,
					renderer: function(v, metaData, r) {
						return TCG.renderDynamicTooltipColumn('calculator', 'ShowCalculationExplanation');
					}
				}
			],
			plugins: {ptype: 'gridsummary'},
			isPagingEnabled: function() {
				return false;
			},
			getTooltipTextForRow: function(row, id, tooltipEventName) {
				return '<pre>' + row.json.calculationExplanation + '</pre>';
			},
			editor: {
				detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
				drillDownOnly: true,
				getDetailPageId: function(gridPanel, row) {
					return row.json.accountingTransactionId;
				}
			},
			getLoadParams: function() {
				const tb = this.getTopToolbar();
				const win = this.getWindow();
				const columnModel = this.getColumnModel();
				const calculationExplanationColumnIndex = columnModel.findColumnIndex('calculationExplanation');
				let explanationVisibility = true;
				if (calculationExplanationColumnIndex > -1) {
					explanationVisibility = !columnModel.isHidden(calculationExplanationColumnIndex);
				}
				const params = {
					collateralTypeName: TCG.getValue('collateralType.name', win.getMainForm().formValues),
					holdingAccountId: TCG.getValue('holdingInvestmentAccount.id', win.getMainForm().formValues),
					positionDate: TCG.getChildByName(tb, 'positionDate').getValue().format('m/d/Y'),
					showExplanation: explanationVisibility
				};
				return params;
			},
			getTopToolbarFilters: function(toolbar) {
				const filters = [];
				const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
				const dv = dateV ? dateV.dateFormat('m/d/Y') : '';
				filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'positionDate', value: dv});
				return filters;
			}
		}]
	},
	collateralTransfersTab: {
		id: 'collateralTransfersTab'
	}
});


