Clifton.collateral.balance.options.OptionsCollateralPledgedTransactionPreviewWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'optionsCollateralTransferWindow',
	title: 'Pledged Collateral Transactions Preview Window',
	iconCls: 'run',
	width: 800,
	height: 600,
	items: [
		{
			xtype: 'editorgrid',
			name: 'collateralBalancePledgedTransactionListByCommand',
			border: true,
			height: 160,
			rowSelectionModel: 'multiple',
			isPagingEnabled: function() {
				return false;
			},
			plugins: {ptype: 'groupsummary'},
			additionalPropertiesToRequest: 'holdingInvestmentAccount.id|positionInvestmentSecurity.id|postedCollateralInvestmentSecurity.id',
			groupField: 'postedCollateralInvestmentSecurity.symbol',
			groupTextTpl: 'Pledged Collateral {values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]}',
			appendStandardColumns: false,
			updateRecord: function() {
				// DO NOT REMOVE: This is here to avoid automatic saving of the grid
			},
			columns: [
				{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
				{header: 'Date', width: 50, dataIndex: 'postedDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
				{header: 'Holding Account', width: 100, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', hidden: true},
				{
					header: 'Position Security', width: 50, dataIndex: 'positionInvestmentSecurity.label', idDataIndex: 'positionInvestmentSecurity.id',
					editor: {
						xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label',
						selectHandler: function(combo, selection, index) {
							const gridRecord = combo.gridEditor.record;
							const id = TCG.getValue('id', selection.json);
							if (TCG.isNotBlank(id)) {
								gridRecord.data['positionInvestmentSecurity.id'] = id;
							}
						}
					}
				},
				{header: 'Collateral Security Symbol', width: 50, dataIndex: 'postedCollateralInvestmentSecurity.symbol', hidden: true},
				{
					header: 'Collateral Security', width: 50, dataIndex: 'postedCollateralInvestmentSecurity.label', idDataIndex: 'postedCollateralInvestmentSecurity.id',
					editor: {
						xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label',
						selectHandler: function(combo, selection, index) {
							const gridRecord = combo.gridEditor.record;
							const id = TCG.getValue('id', selection.json);
							if (TCG.isNotBlank(id)) {
								gridRecord.data['postedCollateralInvestmentSecurity.id'] = id;
							}
						}
					}
				},
				{
					header: 'Collateral Quantity', width: 50, dataIndex: 'postedCollateralQuantity', type: 'currency', negativeInRed: true, summaryType: 'sum',
					editor: {
						xtype: 'floatfield',
						onChangeHandler: function(field, newValue, oldValue) {
							const grid = field.gridEditor.containerGrid;
							const row = field.gridEditor.row;
							grid.updateCollateralValue(grid, row, newValue);
						}
					}
				},
				{header: 'Collateral Value', width: 50, dataIndex: 'postedCollateralMarketValue', type: 'currency', negativeInRed: true, summaryType: 'sum'}
			],
			updateCollateralValue: function(gridPanel, row, quantity) {
				const grid = gridPanel.grid;
				const rowData = grid.getStore().getAt(row).data;
				const params = {
					postedDate: TCG.parseDate(rowData['postedDate']).format('m/d/Y'),
					postedCollateralInvestmentSecurityId: rowData['postedCollateralInvestmentSecurity.id'],
					postedCollateralQuantity: quantity
				};
				TCG.data.getDataValue('collateralPledgedMarketValue.json', this, function(data) {
					rowData['postedCollateralMarketValue'] = data;
					grid.getView().refresh();
				}, this, params);
			},
			getLoadParams: function() {
				return {
					generatePledgedCollateral: true,
					collateralBalanceId: TCG.getValue('id', this.getWindow().openerCt.ownerCt.form.formValues)
				};
			},
			editor: {
				drillDownOnly: true,
				addEditButtons: function(toolBar) {
					const grid = this.grid;
					toolBar.add({
						text: 'Add',
						tooltip: 'Add a new item',
						iconCls: 'add',
						scope: this,
						handler: function() {
							const store = grid.getStore();
							const RowClass = store.recordType;
							grid.stopEditing();
							store.insert(0, new RowClass(this.getNewRowDefaults()));
							grid.startEditing(0, 0);
						}
					});
					toolBar.add('-');
					toolBar.add({
						text: 'Remove',
						tooltip: 'Remove selected item',
						iconCls: 'remove',
						scope: this,
						handler: function() {
							const store = grid.getStore();
							const selectedItemList = grid.getSelectionModel().getSelections();
							for (let i = 0; i < selectedItemList.length; i++) {
								const index = store.indexOf(selectedItemList[i]);
								store.remove(store.getAt(index));
							}
							grid.doLayout();
						}
					});
				},
				getNewRowDefaults: function() {
					return {'postedDate': this.getWindow().balanceDate, 'holdingInvestmentAccount.id': this.getWindow().holdingInvestmentAccountId, 'holdingInvestmentAccount.label': this.getWindow().holdingInvestmentAccountLabel};
				}
			}
		}
	],
	saveWindow: function(closeOnSuccess, forceSubmit) {
		const pledgedCollateralTransactionList = [];
		const window = this;
		const gridPanel = this.items.items[0];
		const grid = gridPanel.grid;
		grid.getStore().each(function(record) {
			pledgedCollateralTransactionList.push(window.getPledgedCollateralTransactionParams(record));
		});

		const onLoadFunction = function(record, conf) {
			window.openerCt.reload();
			window.close();
		};

		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Saving...',
			params: {
				collateralBalanceId: TCG.getValue('id', this.openerCt.ownerCt.form.formValues),
				beanList: Ext.util.JSON.encode(pledgedCollateralTransactionList)
			},
			onLoad: onLoadFunction
		});
		loader.load('collateralBalancePledgedTransactionListSaveByCommand.json');
	},
	getPledgedCollateralTransactionParams: function(record) {
		return {
			class: 'com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransactionCommand',
			id: record.data['id'],
			postedDate: TCG.parseDate(this.balanceDate).format('m/d/Y'),
			holdingInvestmentAccountId: this.holdingInvestmentAccountId,
			positionInvestmentSecurityId: record.data['positionInvestmentSecurity.id'],
			postedCollateralInvestmentSecurityId: record.data['postedCollateralInvestmentSecurity.id'],
			postedCollateralQuantity: record.data['postedCollateralQuantity']
		};
	}
});


