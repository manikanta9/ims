TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.options.OptionsEscrowBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	titlePrefix: 'Options Collateral Balance (Escrow Receipt)',
	iconCls: 'run',
	addInstructionPreview: function(tbarItems) {
		//Don't add instruction preview for options
	},
	showClientPositionTransferBtns: false,
	height: 875,
	width: 1215,
	generalInfoTab: {
		title: 'Info',
		items: [{
			xtype: 'formpanel',
			url: 'collateralBalance.json',
			loadValidation: false,
			labelWidth: 130,
			listeners: {
				afterload: function(panel) {
					const form = this.getForm();
					const booked = TCG.getValue('booked', form.formValues);
					if (TCG.isEqualsStrict(booked, true)) {
						this.setReadOnly(true);
						const fs = TCG.getChildByName(panel, 'transferAmountActionFieldSet');
						if (fs) {
							fs.setVisible(false);
						}
					}
				}
			},
			getWarningMessage: function(form) {
				let msg = undefined;
				if (form.formValues.bookingDate) {
					msg = 'This collateral balance transfer was processed on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
				}
				return msg;
			},
			resetTransferAmount: function() {
				const f = this.getForm();

				const pt = f.findField('chooseTransferAmountPosted');
				if (pt.getValue()) {
					let posted;
					if (TCG.isEqualsStrict(pt.getValue().getGroupValue(), 'OUR')) {
						posted = this.getFormValue('postedCollateralHaircutValue');
					}
					else {
						posted = this.getFormValue('externalCollateralAmount');
					}


					const rt = f.findField('chooseTransferAmountRequired');
					if (rt.getValue()) {
						let required;
						if (TCG.isEqualsStrict(rt.getValue().getGroupValue(), 'OUR')) {
							required = this.getFormValue('collateralRequirement');
						}
						else {
							required = this.getFormValue('externalCollateralRequirement');
						}
						if (!posted) {
							posted = 0;
						}
						if (!required) {
							required = 0;
						}
						let trans = posted - required;
						const balanceDate = TCG.parseDate(TCG.getValue('balanceDate', f.formValues)).format('m/d/Y');
						trans = TCG.getResponseText('collateralTransferAmountRounded.json?collateralTypeName=Futures Collateral&holdingAccountId=' + TCG.getValue('holdingInvestmentAccount.id', f.formValues) + '&amount=' + trans + '&balanceDate=' + balanceDate, this);
						this.setFormValue('transferAmount', trans);
					}
				}
			},
			items: [
				{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
				{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .70,
							layout: 'form',
							items: [
								{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Collateral Account', name: 'collateralInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Company', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
								{fieldLabel: 'Company', name: 'collateralInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'}
							]
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .40,
							layout: 'form',
							items: [
								{fieldLabel: 'Currency', name: 'collateralCurrency.symbol', xtype: 'linkfield', detailIdField: 'collateralCurrency.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'displayfield', type: 'date'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{xtype: 'accounting-bookingdate', sourceTable: 'CollateralBalance'}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 150,
					items: [
						{
							columnWidth: .42,
							layout: 'form',
							defaults: {anchor: '-10'},
							items: [
								{xtype: 'label', html: '<br /><br />'},
								{fieldLabel: 'Positions Market Value', name: 'positionsMarketValue', xtype: 'currencyfield', readOnly: true},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Our Amount'}
									]
								},
								{
									fieldLabel: 'Required Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'collateralRequirement'}
									]
								},
								{
									fieldLabel: 'Posted (Original)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralValue'}
									]
								},
								{
									fieldLabel: 'Posted (After Haircut)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralHaircutValue'}
									]
								},
								{
									fieldLabel: 'Excess Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'excessCollateral'}
									]
								},
								{xtype: 'label', html: '<hr/>'}
							]

						},
						{
							columnWidth: .58,
							layout: 'form',
							defaults: {anchor: '0'},
							labelWidth: 50,
							items: [
								{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 55},
								{
									layout: 'fit',
									flex: 1,
									height: 145,
									items: [{
										name: 'collateralPositionList',
										xtype: 'gridpanel',
										title: 'Collateral Positions',
										border: true,
										hideStandardButtons: true,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'GL Account', width: 170, dataIndex: 'accountingAccount.name', filter: false},
											{header: 'Security', width: 100, dataIndex: 'security.symbol', filter: false},
											{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
											{header: 'Qty', width: 100, dataIndex: 'quantity', type: 'float', useNull: true},
											{header: 'Cost Price', width: 90, dataIndex: 'costPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000', hidden: true},
											{header: 'Market Price', width: 90, dataIndex: 'marketPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000'},
											{header: 'Haircut', width: 80, dataIndex: 'haircut', type: 'currency'},
											{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency'},
											{
												header: 'Collateral Market Value', width: 120, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
												renderer: function(v, p, r) {
													return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
												},
												summaryType: 'sum',
												summaryRenderer: function(v) {
													if (v !== '') {
														return TCG.renderAmount(v, false, '0,000.00');
													}
												}
											}
										],
										plugins: {ptype: 'gridsummary'},
										getLoadParams: function() {
											const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
											return {
												holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues),
												transactionDate: transactionDate.format('m/d/Y')
											};
										}
									}]
								}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'gridpanel',
					name: 'collateralBalancePledgedTransactionListByCommand',
					title: 'Pledged Collateral',
					border: true,
					height: 400,
					instructions: 'Shows pledged collateral that has been created for the current day, or allows generation of pledged collateral.',
					isPagingEnabled: function() {
						return false;
					},
					additionalPropertiesToRequest: 'holdingInvestmentAccount.id|positionInvestmentSecurity.id|postedCollateralInvestmentSecurity.id|positionTransfer.id',
					plugins: {ptype: 'groupsummary'},
					groupField: 'postedCollateralInvestmentSecurity.symbol',
					groupTextTpl: 'Pledged Collateral {values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]}',
					appendStandardColumns: false,
					addFirstToolbarButtons: function(toolBar, gridPanel) {
						const win = gridPanel.getWindow();
						const formPanel = win.items.get(0).items.get(0).items.get(0);
						toolBar.add({
							text: 'Load/Edit Pledged Collateral',
							tooltip: 'Generate pledged collateral transactions based on collateral requirements for existing positions.',
							iconCls: 'preview',
							handler: function() {
								TCG.createComponent('Clifton.collateral.balance.options.OptionsCollateralPledgedTransactionPreviewWindow', {
									openerCt: gridPanel,
									balanceDate: TCG.parseDate(TCG.getValue('balanceDate', gridPanel.getWindow().getMainForm().formValues)),
									holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', gridPanel.getWindow().getMainForm().formValues),
									holdingInvestmentAccountLabel: TCG.getValue('holdingInvestmentAccount.label', gridPanel.getWindow().getMainForm().formValues)
								});
							}
						});

						toolBar.add('-');
						const button = toolBar.add({
							text: 'Upload',
							iconCls: 'import',
							tooltip: 'Upload pledged transactions.',
							scope: this,
							handler: function() {
								TCG.createComponent('Clifton.collateral.balance.options.OptionsCollateralPledgedTransactionsUploadWindow', {
									defaultData: {
										openerCt: gridPanel,
										collateralBalanceId: formPanel.getFormValue('id')
									}
								});
							}
						});
						toolBar.add('-');
						TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), null, gridPanel, 'collateralBalancePledgedTransactionsFileUpload.json?collateralBalanceId=' + formPanel.getFormValue('id'), true);

						toolBar.add({
							text: 'Clear Pledged Collateral',
							tooltip: 'Generate pledged collateral transactions based on collateral requirements for existing positions.',
							iconCls: 'clear',
							handler: function() {
								Ext.Msg.confirm('Clear Pledged Collateral', 'Are you sure you would like to clear pledged collateral and delete associated position transfer(s)?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel.ownerCt,
											waitMsg: 'Clearing Pledged Collateral and Associated Position Transfers...',
											params: {
												collateralBalanceId: formPanel.getFormValue('id')
											},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.createComponent('Clifton.core.StatusWindow', {
													defaultData: {status: record},
													render: function() {
														TCG.Window.superclass.render.apply(this, arguments);
													}
												});
											}
										});
										loader.load('collateralBalancePledgedTransactionListClear.json');
									}
								});
							}
						});
						toolBar.add('-');
						toolBar.add({
							text: 'Generate Return Position Transfer(s)',
							tooltip: 'Generate position transfers for posting collateral.',
							iconCls: 'transfer',
							handler: function() {
								Ext.Msg.confirm('Generate Return position transfer(s)', 'Would you like to generate the position transfer(s)?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										const onLoadFunction = function(record, conf) {
											gridPanel.reload();
											TCG.createComponent('Clifton.accounting.positiontransfer.PositionTransferListWindow')
												.then(function(positionTransferWindow) {
													//reload pending transfers tab grid
													TCG.getChildByName(positionTransferWindow.win, 'accountingPositionTransferListFind', null, 0).reload();
												});
										};
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel.ownerCt,
											waitMsg: 'Booking and Posting Transfers...',
											params: {
												postCollateral: false,
												bookAndPost: false,
												collateralBalanceId: formPanel.getFormValue('id')
											},
											onLoad: onLoadFunction
										});
										loader.load('collateralBalancePositionTransfersCreate.json');
									}
								});
							}
						});
						toolBar.add('-');
						toolBar.add({
							text: 'Generate Post Position Transfer(s)',
							tooltip: 'Generate position transfers for posting collateral.',
							iconCls: 'transfer',
							handler: function() {
								Ext.Msg.confirm('Generate Post position transfer(s)', 'Would you like to generate the position transfer(s)?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										const onLoadFunction = function(record, conf) {
											gridPanel.reload();
											TCG.createComponent('Clifton.accounting.positiontransfer.PositionTransferListWindow')
												.then(function(positionTransferWindow) {
													//reload pending transfers tab grid
													TCG.getChildByName(positionTransferWindow.win, 'accountingPositionTransferListFind', null, 0).reload();
												});
										};
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel.ownerCt,
											waitMsg: 'Generating Transfers...',
											params: {
												postCollateral: true,
												bookAndPost: false,
												collateralBalanceId: formPanel.getFormValue('id')
											},
											onLoad: onLoadFunction
										});
										loader.load('collateralBalancePositionTransfersCreate.json');
									}
								});
							}
						});
						toolBar.add('-');
					},
					columns: [
						{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
						{header: 'Date', width: 50, dataIndex: 'postedDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', hidden: true},
						{header: 'Position Security', width: 50, dataIndex: 'positionInvestmentSecurity.label', idDataIndex: 'positionInvestmentSecurity.id'},
						{header: 'Position Security Symbol', width: 50, dataIndex: 'positionInvestmentSecurity.symbol', hidden: true},
						{header: 'Collateral Security', width: 50, dataIndex: 'postedCollateralInvestmentSecurity.label', idDataIndex: 'postedCollateralInvestmentSecurity.id'},
						{header: 'Collateral Security Symbol', width: 50, dataIndex: 'postedCollateralInvestmentSecurity.symbol', hidden: true},
						{header: 'Collateral Quantity', width: 50, dataIndex: 'postedCollateralQuantity', type: 'currency', negativeInRed: true, summaryType: 'sum'},
						{header: 'Collateral Value', width: 50, dataIndex: 'postedCollateralMarketValue', type: 'currency', negativeInRed: true, summaryType: 'sum'},
						{header: 'Transaction Date', width: 50, dataIndex: 'positionTransfer.transactionDate', type: 'date'}
					],
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.accounting.positiontransfer.TransferWindow',
						getDetailPageId: function(gridPanel, row) {
							if (TCG.isNotBlank(row.json.positionTransfer) && TCG.isNotBlank(row.json.positionTransfer.id)) {
								return row.json.positionTransfer.id;
							}
							return undefined;
						}
					},
					getLoadParams: function() {
						const win = this.ownerCt.ownerCt.ownerCt.getWindow();
						const formPanel = win.items.get(0).items.get(0).items.get(0);
						return {
							collateralBalanceId: formPanel.getFormValue('id')
						};
					}
				}
			]
		}]
	},
	collateralPositionsTab: {
		id: 'collateralPositionsTab',
		title: 'OVERRIDE ME',
		items: [{}]
	},
	pledgedCollateralTab: {
		title: 'Pledged Collateral',
		items: [{
			name: 'collateralBalancePledgedBalanceListFind',
			xtype: 'gridpanel',
			plugins: {ptype: 'groupsummary'},
			remoteSort: true,
			groupField: 'postedCollateralInvestmentSecurity.label',
			groupTextTpl: 'Security {values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]}',
			instructions: 'Pledged collateral for the holding account on the balance date.',
			getTopToolbarFilters: function(toolbar) {
				const grid = this;
				const filters = [];
				filters.push(
					{
						boxLabel: 'Group by Position Security', xtype: 'checkbox', name: 'groupByPositionSecurity',
						listeners: {
							check: function(field, checked) {
								let groupField = 'postedCollateralInvestmentSecurity.label';
								if (TCG.isTrue(checked, true)) {
									groupField = 'positionInvestmentSecurity.label';
								}
								grid.groupField = groupField;
								grid.ds.groupField = groupField;

								TCG.getParentByClass(field, Ext.Panel).reload();
							}
						}
					}
				);
				filters.push('-');
				const postedDate = TCG.parseDate(TCG.getValue('balanceDate', grid.getWindow().getMainForm().formValues));
				filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'postedDate', value: postedDate.dateFormat('m/d/Y')});
				return filters;
			},
			getLoadParams: function(firstLoad) {
				const grid = this;
				const toolbar = grid.getTopToolbar();
				const postedDate = new Date(TCG.getChildByName(toolbar, 'postedDate').value).format('m/d/Y');
				let orderBy = 'postedCollateralInvestmentSecurity:ASC';
				if (grid.groupField === 'positionInvestmentSecurity.label') {
					orderBy = 'positionInvestmentSecurityId:ASC';
				}
				return {
					requestedMaxDepth: 4,
					holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', grid.getWindow().getMainForm().formValues),
					excludeUnbooked: true,
					includeMarketValue: true,
					postedDate: postedDate,
					orderBy: orderBy
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'uuid', hidden: true},
				{header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.label', hidden: true},
				{header: 'Position Security', width: 150, dataIndex: 'positionInvestmentSecurity.label', filter: {type: 'combo', searchFieldName: 'positionInvestmentSecurityId', url: 'investmentSecurityListFind.json'}},
				{header: 'Position Security Symbol', width: 150, dataIndex: 'positionInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'positionInvestmentSecurityId', url: 'investmentSecurityListFind.json'}},
				{header: 'Collateral Security', width: 150, dataIndex: 'postedCollateralInvestmentSecurity.label', filter: {type: 'combo', searchFieldName: 'postedCollateralInvestmentSecurityId', url: 'investmentSecurityListFind.json'}},
				{header: 'Collateral Security Symbol', width: 150, dataIndex: 'postedCollateralInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'postedCollateralInvestmentSecurityId', url: 'investmentSecurityListFind.json'}},
				{header: 'Collateral Quantity', width: 100, dataIndex: 'postedCollateralQuantity', useNull: true, negativeInRed: true, type: 'currency', summaryType: 'sum'},
				{header: 'Collateral Market Value', width: 100, dataIndex: 'postedCollateralMarketValue', useNull: true, negativeInRed: true, type: 'currency', summaryType: 'sum'}
			]
		}]
	},
	balanceHistoryTab: {
		title: 'Balance History',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralBalanceListFind',
			instructions: 'Daily collateral balance history for this account.',
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 30 days of balance history since balance date
					const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
					this.setFilterValue('balanceDate', {'after': dateV.add(Date.DAY, -30)});
				}
				return {
					holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues)
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Date', width: 60, dataIndex: 'balanceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
				{header: 'Positions Market Value', width: 85, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Client Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Client Posted Collateral', width: 85, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCollateralValue'], r.data['postedCollateralValue'], '0,000.00');
					}
				},
				{header: 'Threshold', hidden: true, dataIndex: 'thresholdAmount', width: 100, useNull: true, type: 'currency'},
				{header: 'Minimum Transfer Amount', hidden: true, dataIndex: 'minTransferAmount', width: 100, useNull: true, type: 'currency'},
				{header: 'Client Collateral Change', width: 90, dataIndex: 'collateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;'}
			],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			}
		}]
	},
	setCustomCollateralTransactionFilters: function(gridPanel) {
		//TODO - query for value?
		gridPanel.setFilterValue('accountingAccount.name', {'equals': {value: 753, text: 'Position Collateral (Asset)'}});
	}
})
;


