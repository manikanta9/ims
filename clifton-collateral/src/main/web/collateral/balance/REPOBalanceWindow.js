TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.REPOBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	height: 625,
	width: 1150,
	titlePrefix: 'REPO Collateral Balance',
	iconCls: 'run',
	useNextDayIndexRatio: true,

	getClientAccount: function(fp) {
		const form = fp.form;
		let clientAcct = TCG.getValue('clientInvestmentAccount', form.formValues);
		if (!clientAcct) {
			clientAcct = this.lookupAccount(fp, true, 'REPO');
		}
		return clientAcct;
	},
	getCollateralAccount: function(fp) {
		const form = fp.form;
		let collateralAcct = TCG.getValue('collateralInvestmentAccount', form.formValues);
		if (!collateralAcct) {
			collateralAcct = this.lookupAccount(fp, false, 'REPO');
		}
		return collateralAcct;
	},

	createTransfer: function(fp, transferType) {
		// Default Logic Can Override Where Necessary
		const win = this;

		const dd = win.applyTransferDefaultDataForType(fp, transferType, 'CollateralBalance');
		TCG.createComponent('Clifton.accounting.positiontransfer.TransferWindow', {
			defaultData: dd,
			openerCt: win
		});

	},

	addCollateralActionButtons: function(tbarItems) {
		tbarItems.push(this.positionTransferBtns);
	},

	applyTransferDefaultDataForType: function(fp, transferType, tableName) {
		const clientAcct = this.getClientAccount(fp);
		const collateralAcct = this.getCollateralAccount(fp);
		const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tableName, fp, 'system.table.' + tableName);


		const dd = {
			type: transferType,
			fromClientInvestmentAccount: clientAcct,
			toClientInvestmentAccount: clientAcct,
			useOriginalTransactionDate: true,
			sourceSystemTable: tbl,
			sourceFkFieldId: fp.getWindow().getMainFormId()
		};

		const form = fp.form;

		const balanceDate = TCG.parseDate(TCG.getValue('balanceDate', form.formValues));
		dd.transactionDate = Clifton.calendar.getBusinessDayFromDate(balanceDate, TCG.getValue('collateralType.transferDateOffset', form.formValues)).format('Y-m-d 00:00:00');

		if ((transferType.collateralPosting && !transferType.counterpartyCollateral) || (!transferType.collateralPosting && transferType.counterpartyCollateral)) {
			dd.toHoldingInvestmentAccount = TCG.getValue('holdingInvestmentAccount', form.formValues);
			dd.fromHoldingInvestmentAccount = collateralAcct;
		}
		else {
			dd.fromHoldingInvestmentAccount = TCG.getValue('holdingInvestmentAccount', form.formValues);
			dd.toHoldingInvestmentAccount = collateralAcct;
		}

		dd.transferAmount = TCG.getValue('transferAmount', form.formValues);
		dd.detailList = [];
		dd.detailList.push({positionCostBasis: dd.transferAmount});
		dd.note = (transferType.collateralPosting ? 'Posting' : 'Return') + ' of ' + (transferType.counterpartyCollateral ? 'Counterparty' : 'Client') + ' Owned Collateral from Collateral Balance Date [' + TCG.parseDate(TCG.getValue('balanceDate', form.formValues)).format('m/d/Y') + ']';
		return dd;
	},

	generalInfoTab: {
		title: 'Info',
		items: [{
			xtype: 'formpanel',
			url: 'collateralBalance.json',
			loadValidation: false,
			readOnly: true,
			labelWidth: 160,
			items: [
				{
					xtype: 'fieldset',
					title: 'Holding Account',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
						{fieldLabel: 'Issuer', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'holdingInvestmentAccount.issuingCompany.id'},
						{fieldLabel: 'Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
						{fieldLabel: 'Contract', name: 'holdingInvestmentAccount.businessContract.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id'},
						{fieldLabel: 'Counterparty', name: 'counterparty.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'counterparty.id'}
					]
				},
				{
					fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
					defaults: {
						xtype: 'displayfield',
						style: 'text-align: right',
						flex: 1
					},
					items: [
						{value: 'Current'},
						{value: 'Original'},
						{value: 'Difference'}
					]
				},
				{
					fieldLabel: 'Positions Market Value', xtype: 'compositefield',
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'postedCollateralValue'},
						{name: 'positionsMarketValue'},
						{name: 'positionsMarketValueDifference', disabled: true}
					]
				},

				{
					fieldLabel: 'Net Cash', xtype: 'compositefield',
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'postedCollateralHaircutValue'},
						{name: 'collateralRequirement'},
						{name: 'excessCollateral'}
					]
				},
				{fieldLabel: 'Expected Transfer Amount', name: 'collateralChangeAmount', xtype: 'currencyfield'},

				{
					xtype: 'fieldset',
					title: 'Min Transfers Amounts and Thresholds',
					labelWidth: 150,
					items: [
						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								style: 'text-align: right',
								flex: 1
							},
							items: [
								{value: 'Client'},
								{value: 'Counterparty'}
							]
						},
						{
							fieldLabel: 'Threshold', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								flex: 1
							},
							items: [
								{name: 'thresholdAmount'},
								{name: 'thresholdCounterpartyAmount'}
							]
						},
						{
							fieldLabel: 'Min Transfer Amount', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								flex: 1
							},
							items: [
								{name: 'minTransferAmount'},
								{name: 'minTransferCounterpartyAmount'}
							]
						}
					]
				}
			],
			listeners: {
				afterload: function() {
					this.updateCounterparty();

					const cpmv = TCG.getValue('positionsMarketValue', this.getForm().formValues);
					const opmv = TCG.getValue('postedCollateralValue', this.getForm().formValues);

					const diff = opmv - cpmv;
					this.getForm().findField('positionsMarketValueDifference').setValue(diff);
				}
			},
			updateCounterparty: function() {
				const contractId = this.getForm().findField('holdingInvestmentAccount.businessContract.id').getValue();
				if (TCG.isBlank(contractId)) {
					return;
				}
				const roles = TCG.data.getData('businessContractPartyListByContractAndRole.json?roleNames=Counterparty&contractId=' + contractId, this);
				if (roles && roles.length > 0) {
					const cp = roles[0].company;
					this.setFormValue('counterparty.id', cp.id, true);
					this.setFormValue('counterparty.name', cp.name, true);
				}
			}
		}]
	},

	collateralPositionsTab: {
		title: 'REPO Positions',
		layout: 'border',
		items: [
			{
				region: 'north',
				height: 260,
				xtype: 'gridpanel',
				name: 'collateralBalanceLendingRepoListFind',
				instructions: 'REPO Collateral Balance Breakdown for Repo Positions for the REPO account on the balance date.',
				pageSize: 1000,
				forceFit: false,
				appendStandardColumns: false,

				columns: [
					{header: 'ID', hidden: true, width: 25, dataIndex: 'id'},
					{header: 'Repo ID', width: 25, hidden: true, dataIndex: 'repoIdentifier', type: 'int', doNotFormat: true},
					{header: 'Type', width: 75, hidden: true, dataIndex: 'repo.repoType', nonPersistentField: true},
					{header: 'Workflow Status', width: 100, hidden: true, dataIndex: 'repo.workflowStatus', nonPersistentField: true},
					{header: 'Workflow State', width: 100, hidden: true, dataIndex: 'repo.workflowState', nonPersistentField: true},
					{header: 'Client Account', width: 100, hidden: true, dataIndex: 'repo.clientAccount.label', nonPersistentField: true},
					{header: 'Holding Account', width: 100, hidden: true, dataIndex: 'repo.holdingAccount.label', nonPersistentField: true},
					{header: 'REPO Account', width: 100, hidden: true, dataIndex: 'repo.repoAccount.label', nonPersistentField: true},
					{header: 'Counterparty', width: 100, hidden: true, dataIndex: 'repo.counterparty.name', nonPersistentField: true},
					{header: 'Description', width: 100, hidden: true, dataIndex: 'repo.description', nonPersistentField: true},
					{header: 'Term (days)', width: 75, hidden: true, dataIndex: 'repo.term', useNull: true, type: 'int', nonPersistentField: true},
					{header: 'Rate', width: 75, hidden: true, dataIndex: 'repo.interestRate', type: 'float', useNull: true, nonPersistentField: true},
					{header: 'Reverse', width: 75, hidden: true, dataIndex: 'repo.reverse', type: 'boolean', nonPersistentField: true},

					{header: 'Security', width: 100, dataIndex: 'repo.security.symbol', nonPersistentField: true},
					{header: 'CCY', width: 45, dataIndex: 'repo.security.currencyDenominationSymbol', nonPersistentField: true},

					{header: 'Index Ratio', width: 60, dataIndex: 'repo.indexRatio', type: 'float', nonPersistentField: true},
					{header: 'Quantity', width: 75, hidden: true, dataIndex: 'repo.quantityIntended', nonPersistentField: true},
					{header: 'Face', width: 75, dataIndex: 'repo.originalFace', type: 'float', nonPersistentField: true},
					{header: 'Trade Price', width: 75, hidden: true, dataIndex: 'repo.price', type: 'float', nonPersistentField: true},
					{header: 'Market Price', width: 75, dataIndex: 'price', type: 'float'},
					{header: 'Current Day Index Ratio', width: 75, dataIndex: 'indexRatio', type: 'float'},
					{header: 'Haircut %', width: 75, dataIndex: 'repo.haircutPercent', type: 'percent', nonPersistentField: true, tooltip: 'Net Cash = (Haircut Percent > 100) ? (100 * Market Value / Haircut Percent) : (Market Value * Haircut Percent / 100)'},

					{header: 'Roll', width: 75, hidden: true, dataIndex: 'repo.roll', type: 'boolean', nonPersistentField: true},

					{header: 'Trader', width: 100, hidden: true, dataIndex: 'repo.traderUser.label', nonPersistentField: true},
					{header: 'Confirmed', width: 75, hidden: true, dataIndex: 'repo.confirmed', type: 'boolean', renderer: TCG.renderCheck, align: 'center', nonPersistentField: true},
					{header: 'Traded On', width: 100, hidden: true, dataIndex: 'repo.tradeDate', nonPersistentField: true},
					{header: 'Action Date', width: 100, hidden: true, dataIndex: 'repo.maturityDate', nonPersistentField: true},
					{header: 'Settled On', width: 100, hidden: true, dataIndex: 'repo.settlementDate', nonPersistentField: true},
					{
						header: 'Matures On', width: 100, hidden: true, dataIndex: 'repo.maturitySettlementDate', nonPersistentField: true,
						filter: {
							searchFieldName: 'maturityDate',
							orNull: true
						}
					},

					{header: 'FX Rate', width: 75, dataIndex: 'exchangeRateToBase', type: 'float'},

					{header: 'Market Value Base', width: 100, hidden: true, dataIndex: 'marketValue', type: 'currency', summaryType: 'sum'},
					{header: 'REPO Notional Base', width: 100, hidden: true, dataIndex: 'originalMarketValue', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'Market Value with Haircut Base', width: 100, hidden: true, dataIndex: 'netCash', type: 'currency', summaryType: 'sum'},
					{header: 'Loan Amount with Haircut Base', width: 100, hidden: true, dataIndex: 'originalNetCash', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'REPO Interest Base', width: 100, hidden: true, dataIndex: 'repoInterestAmount', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'Accrued Interest Base', width: 100, hidden: true, dataIndex: 'accruedInterest', type: 'currency', summaryType: 'sum'},

					{header: 'Market Value Local', width: 100, dataIndex: 'marketValueLocal', type: 'currency', summaryType: 'sum'},
					{header: 'REPO Notional Local', width: 100, hidden: true, dataIndex: 'originalMarketValueLocal', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'Market Value with Haircut Local', width: 100, dataIndex: 'netCashLocal', type: 'currency', summaryType: 'sum'},
					{header: 'Loan Amount with Haircut Local', width: 100, dataIndex: 'originalNetCashLocal', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'REPO Interest Local', width: 100, hidden: true, dataIndex: 'repo.interestAmount', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'Accrued Interest Local', width: 100, dataIndex: 'accruedInterestLocal', type: 'currency', summaryType: 'sum'},

					{header: 'Exposure Local', width: 100, hidden: true, dataIndex: 'exposureLocal', type: 'currency', summaryType: 'sum', nonPersistentField: true},
					{header: 'Exposure Base', width: 100, dataIndex: 'exposure', type: 'currency', summaryType: 'sum', nonPersistentField: true}

				],
				plugins: {ptype: 'gridsummary'},
				getLoadParams: function(firstLoad) {
					const grid = this;
					return {
						collateralBalanceId: grid.getWindow().getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.lending.repo.RepoWindow',
					drillDownOnly: true,
					getDetailPageId: function(gridPanel, row) {
						return row.json.repoIdentifier;
					}
				}
			},


			{
				title: 'Collateral Positions',
				region: 'center',
				xtype: 'gridpanel',
				name: 'collateralPositionList',
				instructions: 'Collateral Positions for the holding account on the balance date.',
				getTopToolbarFilters: function(toolbar) {
					const filters = [];
					const grid = this;
					const dateV = TCG.parseDate(TCG.getValue('balanceDate', grid.getWindow().getMainForm().formValues));
					filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'transactionDate', value: dateV.dateFormat('m/d/Y')});
					return filters;
				},
				getLoadParams: function(firstLoad) {
					const grid = this;
					const transactionDate = new Date(TCG.getChildByName(grid.getTopToolbar(), 'transactionDate').value).format('m/d/Y');
					return {
						holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', grid.getWindow().getMainForm().formValues),
						transactionDate: transactionDate
					};
				},
				columns: [
					{header: 'GL Account', width: 175, dataIndex: 'accountingAccount.label', filter: false},
					{header: 'Security', width: 75, dataIndex: 'security.symbol', filter: false},
					{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
					{header: 'Quantity', width: 75, dataIndex: 'quantity', type: 'currency'},
					{header: 'Cost Price', width: 100, dataIndex: 'costPrice', type: 'currency', numberFormat: '0,000.0000'},
					{header: 'Market Price', width: 100, dataIndex: 'marketPrice', type: 'currency', numberFormat: '0,000.0000'},
					{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},

					{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency'},
					{
						header: 'Collateral Market Value', width: 120, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
						},
						summaryType: 'sum',
						summaryRenderer: function(v) {
							if (TCG.isNotBlank(v)) {
								return TCG.renderAmount(v, false, '0,000.00');
							}
						}
					}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}
});


