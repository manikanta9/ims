TCG.use('Clifton.collateral.balance.BalanceWindowBase');

Clifton.collateral.balance.ClearedOTCBalanceWindow = Ext.extend(Clifton.collateral.balance.BalanceWindowBase, {
	titlePrefix: 'Cleared OTC Collateral Balance',
	iconCls: 'run',
	getAccountingPositionTransferTypeParams: function() {
		return {counterpartyCollateral: false, cash: false};
	},
	balanceDetailTab: {
		title: 'Balance Details',
		items: [{
			name: 'collateralBalanceListFind',
			xtype: 'gridpanel',
			instructions: 'Includes child collateral balances for selected Collateral Balance. This happens when collateral balances from multiple accounts roll up into a single entry. Child entries reference parent balance via "parent" field.',
			getLoadParams: function(firstLoad) {
				const grid = this;
				return {
					requestedMaxDepth: 4,
					parentId: grid.getWindow().getMainFormId()
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'PID', width: 15, dataIndex: 'parent.id', useNull: true, type: 'int', doNotFormat: true, filter: {searchFieldName: 'parentId'}, hidden: true},
				{
					header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'},
					renderer: function(v, metaData, r) {
						const note = r.data.note;
						if (TCG.isNotBlank(note)) {
							const qtip = r.data.note;
							metaData.css = 'amountAdjusted';
							metaData.attr = TCG.renderQtip(qtip);
						}
						return v;
					}
				},
				{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
				{header: 'Client Account', width: 75, dataIndex: 'clientInvestmentAccount.number', filter: {searchFieldName: 'clientInvestmentAccountLabel'}},

				{header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', defaultSortColumn: true, filter: {searchFieldName: 'holdingInvestmentAccountLabel'}},
				{header: 'Holding Company', width: 130, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},

				{header: 'Collateral Account', width: 75, dataIndex: 'collateralInvestmentAccount.number', filter: {searchFieldName: 'collateralInvestmentAccountLabel'}},
				{header: 'Collateral Company', width: 130, dataIndex: 'collateralInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'collateralCompanyName'}},

				{header: 'Currency', width: 50, hidden: true, dataIndex: 'collateralCurrency.symbol', filter: {type: 'combo', searchFieldName: 'collateralCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Positions Market Value', width: 100, hidden: true, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Required Collateral (External)', width: 100, dataIndex: 'externalCollateralRequirement', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Posted Collateral (External)', width: 100, dataIndex: 'externalCollateralAmount', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Excess Collateral (External)', width: 100, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true},

				{
					header: 'Required Collateral', width: 100, hidden: true, dataIndex: 'collateralRequirement', useNull: true, negativeInRed: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, metaData, r) {
						const ext = r.data.externalCollateralRequirement;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralRequirement'], r.data['externalCollateralRequirement'], '0,000.00');
					}
				},
				{header: 'Posted Collateral (Original)', width: 100, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Posted Collateral', hidden: true, width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency',
					renderer: function(v, metaData, r) {
						const ext = r.data.externalCollateralAmount;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton (Original):</td><td align="right">' + Ext.util.Format.number(r.data.postedCollateralValue, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Clifton (Adjusted):</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralAmount'], r.data['externalCollateralAmount'], '0,000.00');
					}
				},
				{
					header: 'Excess Collateral', width: 100, dataIndex: 'excessCollateral', type: 'currency', negativeInRed: true, hidden: true,
					renderer: function(v, metaData, r) {
						const ext = r.data.externalExcessCollateral;
						let extVal = 0;
						if (Ext.isNumber(ext)) {
							extVal = ext;
						}
						let val = 0;
						if (Ext.isNumber(v)) {
							val = v;
						}
						const dif = extVal - val;

						let qtip = '<table>';
						qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
						return TCG.renderAdjustedAmount(v, v !== r.data['externalExcessCollateral'], r.data['externalExcessCollateral'], '0,000.00');
					}
				},
				{header: 'Transfer Amount', width: 100, dataIndex: 'transferAmount', type: 'currency', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Booking Date', width: 50, dataIndex: 'bookingDate', hidden: true},
				{header: 'Booked', width: 50, dataIndex: 'booked', type: 'boolean'}
			],
			columnOverrides: [{dataIndex: 'workflowStatus.name', hidden: true}, {dataIndex: 'workflowState.name', hidden: true}],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			}
		}]
	},

	generalInfoTab: {
		title: 'Info',

		items: [{
			xtype: 'formpanel',
			url: 'collateralBalance.json',
			loadValidation: false,
			labelWidth: 130,

			listeners: {
				afterload: function(panel) {
					const f = this.getForm();
					const booked = TCG.getValue('booked', f.formValues);
					if (TCG.isTrue(booked)) {
						this.setReadOnly(true);
						const fs = TCG.getChildByName(panel, 'transferAmountActionFieldSet');
						if (fs) {
							fs.setVisible(false);
						}
					}
				}
			},

			getWarningMessage: function(form) {
				let msg = undefined;
				if (form.formValues.bookingDate) {
					msg = 'This collateral balance transfer was processed on ' + TCG.renderDate(form.formValues.bookingDate) + ' and can no longer be modified.';
				}
				return msg;
			},

			items: [
				{fieldLabel: 'Client', name: 'holdingInvestmentAccount.businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'holdingInvestmentAccount.businessClient.id'},
				{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .70,
							layout: 'form',
							items: [
								{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Collateral Account', name: 'collateralInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Company', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'holdingInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
								{fieldLabel: 'Company', name: 'collateralInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailIdField: 'collateralInvestmentAccount.issuingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'}
							]
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 130,
					items: [
						{
							columnWidth: .40,
							layout: 'form',
							items: [
								{fieldLabel: 'Currency', name: 'collateralCurrency.symbol', xtype: 'linkfield', detailIdField: 'collateralCurrency.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'displayfield', type: 'date'}
							]
						},
						{
							columnWidth: .30,
							layout: 'form',
							labelWidth: 100,
							items: [
								{xtype: 'accounting-bookingdate', sourceTable: 'CollateralBalance'}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 150,
					items: [
						{
							columnWidth: .42,
							layout: 'form',
							defaults: {anchor: '-10'},
							items: [
								{fieldLabel: 'Positions Market Value', name: 'positionsMarketValue', xtype: 'currencyfield', readOnly: true},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Our Amount'},
										{value: 'Broker Amount'}
									]
								},
								{
									fieldLabel: 'Required Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: ''},
										{name: 'externalCollateralRequirement'}
									]
								},
								{
									fieldLabel: 'Posted (Original)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralValue'},
										{xtype: 'label', html: '&nbsp;'}
									]
								},
								{
									fieldLabel: 'Posted (After Haircut)', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'postedCollateralHaircutValue'},
										{name: 'externalCollateralAmount'}
									]
								},
								{
									fieldLabel: 'Excess Collateral', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{name: 'excessCollateral'},
										{name: 'externalExcessCollateral'}
									]
								},
								{
									fieldLabel: 'Excess Collateral %', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										readOnly: true,
										flex: 1
									},
									items: [
										{
											name: 'excessCollateralPercentage'
										},
										{
											name: 'externalExcessCollateralPercentage'
										}
									]
								},
								{xtype: 'label', html: '<hr/>'},
								{fieldLabel: '<b>Transfer Amount</b>', name: 'transferAmount', xtype: 'currencyfield'},
								{
									xtype: 'fieldset',
									title: 'Transfer Amount Actions',
									name: 'transferAmountActionFieldSet',
									instructions: 'Transfer amount is calculated as Required less Posted.  To automatically calculate the transfer amount based on the above entries, select from the options below.',
									labelWidth: 1,
									items: [
										{
											xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'chooseTransferAmountPosted',
											items: [
												{boxLabel: 'Use <b>Our</b> Posted Collateral', xtype: 'radio', name: 'chooseTransferAmountPosted', inputValue: 'OUR'},
												{boxLabel: 'Use <b>Broker</b> Posted Collateral', xtype: 'radio', name: 'chooseTransferAmountPosted', inputValue: 'EXTERNAL'}
											],
											listeners: {
												change: function(rg, r) {
													const p = TCG.getParentFormPanel(rg);
													p.resetTransferAmount();
												}
											}
										},
										{xtype: 'label', html: '<hr/>'},
										{
											xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'chooseTransferAmountRequired',
											items: [
												{boxLabel: 'Use <b>Our</b> Req\'d Collateral', xtype: 'radio', name: 'chooseTransferAmountRequired', inputValue: 'OUR'},
												{boxLabel: 'Use <b>Broker</b> Req\'d Collateral', xtype: 'radio', name: 'chooseTransferAmountRequired', inputValue: 'EXTERNAL', checked: true}
											],
											listeners: {
												change: function(rg, r) {
													const p = TCG.getParentFormPanel(rg);
													p.resetTransferAmount();
												}
											}
										}
									]
								}
							]

						},
						{
							columnWidth: .58,
							layout: 'form',
							defaults: {anchor: '0'},
							labelWidth: 50,
							items: [
								{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 55},
								{
									layout: 'fit',
									flex: 1,
									height: 160,
									items: [{
										name: 'collateralBalanceListFind',
										xtype: 'gridpanel',
										title: 'Balance History',
										border: true,
										hideStandardButtons: true,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
											{header: 'Date', width: 35, dataIndex: 'balanceDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
											{header: 'Positions Market Value', width: 50, hidden: true, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

											{header: 'Req\'d (External)', width: 50, hidden: true, dataIndex: 'externalCollateralRequirement', useNull: true, type: 'currency', negativeInRed: true},
											{header: 'Posted (External)', width: 50, hidden: true, dataIndex: 'externalCollateralAmount', useNull: true, type: 'currency', negativeInRed: true},
											{header: 'Excess (External)', width: 50, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true},

											{header: 'Transfer Amount', width: 50, dataIndex: 'transferAmount', type: 'currency', negativeInRed: true},
											{header: 'Booking Date', width: 50, dataIndex: 'bookingDate', hidden: true},
											{header: 'Booked', width: 30, dataIndex: 'booked', type: 'boolean'},
											{header: 'Note', width: 90, dataIndex: 'note'}
										],
										getLoadParams: function() {
											// Show previous 5 Balance Records
											const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
											const start = Clifton.calendar.getBusinessDayFromDate(dateV, -5);
											const parent = TCG.getValue('parent', this.getWindow().getMainForm().formValues);
											this.setFilterValue('balanceDate', {'after': start, 'before': dateV});
											return {
												parentIsNull: TCG.isNull(parent),
												holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues)
											};
										},
										editor: {
											detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
											drillDownOnly: true
										}
									}]
								},
								{
									layout: 'fit',
									flex: 1,
									height: 160,
									items: [{
										name: 'collateralPositionList',
										xtype: 'gridpanel',
										title: 'Collateral Positions',
										border: true,
										hideStandardButtons: true,
										isPagingEnabled: function() {
											return false;
										},
										columns: [
											{header: 'GL Account', width: 150, dataIndex: 'accountingAccount.name', filter: false},
											{header: 'Security', width: 100, dataIndex: 'security.symbol', filter: false},
											{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', hidden: true, type: 'float', filter: false},
											{header: 'Qty', width: 100, dataIndex: 'quantity', type: 'float', useNull: true},
											{header: 'Cost Price', width: 90, dataIndex: 'costPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000', hidden: true},
											{header: 'Market Price', width: 90, dataIndex: 'marketPrice', type: 'currency', useNull: true, numberFormat: '0,000.0000'},
											{header: 'Haircut', width: 75, dataIndex: 'haircut', type: 'currency'},

											{header: 'Collateral Market Value (Original)', width: 100, hidden: true, dataIndex: 'collateralMarketValue', useNull: true, type: 'currency'},
											{
												header: 'Collateral Market Value', width: 150, dataIndex: 'collateralMarketValueAdjusted', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid;',
												renderer: function(v, p, r) {
													return TCG.renderAdjustedAmount(v, v !== r.data['collateralMarketValue'], r.data['collateralMarketValue'], '0,000.00');
												},
												summaryType: 'sum',
												summaryRenderer: function(v) {
													if (TCG.isNotBlank(v)) {
														return TCG.renderAmount(v, false, '0,000.00');
													}
												}
											}
										],
										plugins: {ptype: 'gridsummary'},
										getLoadParams: function() {
											const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
											return {
												holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', this.getWindow().getMainForm().formValues),
												transactionDate: transactionDate.format('m/d/Y')
											};
										}
									}]
								}
							]
						}
					]
				}
			],
			resetTransferAmount: function() {
				const f = this.getForm();

				const pt = f.findField('chooseTransferAmountPosted');
				if (pt.getValue()) {
					let posted;
					if (pt.getValue().getGroupValue() === 'OUR') {
						posted = this.getFormValue('postedCollateralHaircutValue');
					}
					else {
						posted = this.getFormValue('externalCollateralAmount');
					}


					const rt = f.findField('chooseTransferAmountRequired');
					if (rt.getValue()) {
						let required;
						if (rt.getValue().getGroupValue() === 'OUR') {
							required = this.getFormValue('collateralRequirement');
						}
						else {
							required = this.getFormValue('externalCollateralRequirement');
						}
						if (!posted) {
							posted = 0;
						}
						if (!required) {
							required = 0;
						}
						let trans = posted - required;
						const balanceDate = TCG.parseDate(TCG.getValue('balanceDate', f.formValues)).format('m/d/Y');
						trans = TCG.getResponseText('collateralTransferAmountRounded.json?collateralTypeName=Futures Collateral&holdingAccountId=' + TCG.getValue('holdingInvestmentAccount.id', f.formValues) + '&amount=' + trans + '&balanceDate=' + balanceDate, this);
						this.setFormValue('transferAmount', trans);
					}
				}
			}
		}]
	}
});


