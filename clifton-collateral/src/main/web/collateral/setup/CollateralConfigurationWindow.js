Clifton.collateral.setup.CollateralConfigurationWindow = Ext.extend(TCG.app.DetailWindow, {


	titlePrefix: 'Collateral Configuration',
	iconCls: 'bank',
	width: 750,
	height: 550,
	items: [
		{
			xtype: 'tabpanel',
			items: [{
				title: 'Collateral Configuration',
				items: [{
					xtype: 'formwithdynamicfields',
					labelWidth: 170,
					instructions: 'Collateral Configuration is used to customize collateral calculation/rebuild logic by holding/collateral company or account. Collateral account is looked up for the holding account based on corresponding relationship purpose(s). The logic uses specificity with Collateral Account being most specific and Holding Company being less specific.',
					labelFieldName: 'collateralType.name',
					url: 'collateralConfiguration.json?requestedMaxDepth=8',
					additionalPropertiesToRequest: 'rebuildProcessorBean.propertyList',

					dynamicTriggerFieldName: 'collateralType.name',
					dynamicTriggerValueFieldName: 'collateralType.systemBeanType.id',
					dynamicFieldTypePropertyName: 'type',
					dynamicFieldListPropertyName: 'rebuildProcessorBean.propertyList',
					dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
					dynamicFieldsUrlParameterName: 'typeId',

					items: [
						{name: 'collateralType.systemBeanType.id', xtype: 'hidden', submitValue: false},
						{name: 'rebuildProcessorBean.type.id', xtype: 'hidden'},
						{name: 'rebuildProcessorBean.id', xtype: 'hidden', submitValue: false},
						{
							fieldLabel: 'Collateral Type', name: 'collateralType.name', hiddenName: 'collateralType.id', xtype: 'combo',
							url: 'collateralTypeListFind.json?maxDepth=3',
							detailPageClass: 'Clifton.collateral.setup.CollateralTypeWindow',
							requestedProps: 'systemBeanType.id',
							disableAddNewItem: true,
							listeners: {
								// reset object bean property fields on changes to bean type (triggerField)
								select: function(combo, record, index) {
									const formPanel = combo.getParentForm();
									formPanel.setFormValue('rebuildProcessorBean.type.id', record.json.systemBeanType.id, true);
									formPanel.setFormValue('collateralType.systemBeanType.id', record.json.systemBeanType.id, true);
									combo.ownerCt.resetDynamicFields(combo, false);
								}
							}
						},
						{fieldLabel: 'Holding Company', name: 'holdingCompany.name', hiddenName: 'holdingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['holdingAccount.id']},
						{
							fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label',
							mutuallyExclusiveFields: ['holdingCompany.id'], detailPageClass: 'Clifton.investment.account.AccountWindow',
							beforequery: function(queryEvent) {
								const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
								queryEvent.combo.store.baseParams = {
									issuingCompanyId: form.findField('holdingCompany.id').value,
									ourAccount: false
								};
							},
							getDefaultData: function(form) { // defaults our account & holding company for "add new" drill down
								return {
									ourAccount: false,
									issuingCompany: TCG.getValue('holdingCompany', form.formValues)
								};
							}
						},
						{fieldLabel: 'Collateral Company', name: 'collateralCompany.name', hiddenName: 'collateralCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['collateralAccount.id']},
						{
							fieldLabel: 'Collateral Account', name: 'collateralAccount.label', hiddenName: 'collateralAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label',
							mutuallyExclusiveFields: ['collateralCompany.id'], detailPageClass: 'Clifton.investment.account.AccountWindow',
							beforequery: function(queryEvent) {
								const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
								queryEvent.combo.store.baseParams = {
									issuingCompanyId: form.findField('collateralCompany.id').value,
									ourAccount: false
								};
							},
							getDefaultData: function(form) { // defaults our account & holding company for "add new" drill down
								return {
									ourAccount: false,
									issuingCompany: TCG.getValue('collateralCompany', form.formValues)
								};
							}
						},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'},
						{xtype: 'label', html: '<hr/>'}
					]
				}]
			}, {
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'CollateralConfiguration',
					showAttachmentInfo: true,
					showInternalInfo: false,
					showPrivateInfo: false,
					showGlobalNoteMenu: true,
					showGlobalColumn: true,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			}]
		}
	]
});
