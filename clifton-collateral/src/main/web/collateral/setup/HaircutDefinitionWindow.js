Clifton.collateral.setup.HaircutDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Collateral Haircut Definition',
	iconCls: 'bank',
	width: 780,
	height: 760,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Collateral Haircut Definition defines haircuts that should be applied to market value of collateral positions. For example, treasuries are riskier than cash and may get 5% haircut while equities may get 30% haircut.',
		url: 'collateralHaircutDefinition.json?requestedMaxDepth=4',
		dtoClass: 'com.clifton.collateral.haircut.CollateralHaircutDefinition',
		items: [
			{
				xtype: 'fieldset',
				title: 'Holding Company/ISDA (Optional)',
				instructions: 'Optionally apply this haircut to a specific broker and/or broker account or a specific ISDA contract.',
				items: [
					{fieldLabel: 'Holding Company', name: 'holdingCompany.name', hiddenName: 'holdingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['businessContract.id']},
					{
						fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label',
						requiredFields: ['holdingCompany.id'], detailPageClass: 'Clifton.investment.account.AccountWindow',
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							queryEvent.combo.store.baseParams = {
								issuingCompanyId: form.findField('holdingCompany.id').value,
								ourAccount: false
							};
						},
						getDefaultData: function(f) { // defaults our account & holding company for "add new" drill down
							return {
								ourAccount: false,
								issuingCompany: TCG.getValue('holdingCompany', f.formValues)
							};
						}
					},
					{
						fieldLabel: 'ISDA', name: 'businessContract.name', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json?contractTypeName=ISDA', detailPageClass: 'Clifton.business.contract.ContractWindow', mutuallyExclusiveFields: ['holdingCompany.id'], requestedProps: 'company',
						listeners: {
							select: function(combo, record, index) {
								// update company and counterparty fields from selected ISDA contract
								const fp = combo.getParentForm();
								const form = fp.getForm();
								const companyField = form.findField('businessContract.company.name');
								const companyIdField = form.findField('businessContract.company.id');
								const company = record.json.company;
								companyField.setValue(company.name);
								companyIdField.setValue(company.id);

								fp.updateCounterparty();
							}
						}
					},
					{fieldLabel: 'Client', name: 'businessContract.company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'businessContract.company.id', submitValue: false, submitDetailField: false},
					{fieldLabel: 'Counterparty', name: 'counterparty.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'counterparty.id', submitValue: false, submitDetailField: false}
				]
			},
			{
				xtype: 'fieldset',
				title: 'Security Specificity Scope',
				instructions: 'Apply this haircut to the following securities. If multiple definitions are found for a given position, the most specific definition will be applied.',
				items: [
					{
						fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', xtype: 'combo', loadAll: true,
						url: 'investmentTypeList.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
						listeners: {
							beforeselect: function(combo, record) {
								const f = combo.getParentForm().getForm();
								const subType = f.findField('investmentTypeSubType.name');
								subType.clearAndReset();
								const subType2 = f.findField('investmentTypeSubType2.name');
								subType2.clearAndReset();
							}
						}
					},
					{
						fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo', loadAll: true, requiredFields: ['investmentType.name'],
						url: 'investmentTypeSubTypeListByType.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubTypeWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
							}
						}
					},
					{
						fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', displayField: 'name', xtype: 'combo', loadAll: true, requiredFields: ['investmentType.name'],
						url: 'investmentTypeSubType2ListByType.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubType2Window',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
							}
						}
					},
					{fieldLabel: 'Investment Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600, mutuallyExclusiveFields: ['investmentType.id', 'investmentTypeSubType.id', 'investmentTypeSubType2.id', 'investmentInstrument.id'], detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
					{fieldLabel: 'Investment Instrument', name: 'investmentInstrument.name', hiddenName: 'investmentInstrument.id', displayField: 'name', xtype: 'combo', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', mutuallyExclusiveFields: ['investmentType.id', 'investmentTypeSubType.id', 'investmentTypeSubType2.id', 'instrumentHierarchy.id']}
				]
			},
			{
				xtype: 'formgrid',
				title: 'Haircuts',
				instructions: 'Adjustment factor to market value where 0.0  means no haircut, 0.3  means 30% haircut or 70% of the value left.',
				storeRoot: 'collateralHaircutList',
				dtoClass: 'com.clifton.collateral.haircut.CollateralHaircut',
				collapsed: false,
				height: 175,
				heightResized: true,
				columnsConfig: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Haircut', width: 120, dataIndex: 'haircutPercent', type: 'float', editor: {xtype: 'floatfield'}},
					{header: 'Start Years to Maturity', width: 150, dataIndex: 'startYearsToMaturity', type: 'int', editor: {xtype: 'integerfield', allowDecimals: false}, useNull: true},
					{header: 'End Years to Maturity', width: 150, dataIndex: 'endYearsToMaturity', type: 'int', editor: {xtype: 'integerfield', allowDecimals: false}, useNull: true},
					{header: 'Client Haircut', width: 125, dataIndex: 'clientHaircut', type: 'boolean', editor: {xtype: 'checkbox'}},
					{header: 'Counterparty Haircut', width: 125, dataIndex: 'counterpartyHaircut', type: 'boolean', editor: {xtype: 'checkbox'}}
				],
				getNewRowDefaults: function() {
					return {clientHaircut: true, counterpartyHaircut: true};
				}
			}
		],
		listeners: {
			afterload: function() {
				this.updateCounterparty();
			}
		},
		updateCounterparty: function() {
			const contractId = this.getForm().findField('businessContract.id').getValue();
			if (TCG.isBlank(contractId)) {
				return;
			}
			const roles = TCG.data.getData('businessContractPartyListByContractAndRole.json?roleNames=Counterparty&contractId=' + contractId, this);
			if (roles && roles.length > 0) {
				const cp = roles[0].company;
				this.setFormValue('counterparty.id', cp.id, true);
				this.setFormValue('counterparty.name', cp.name, true);
			}
		}
	}
	]
});
