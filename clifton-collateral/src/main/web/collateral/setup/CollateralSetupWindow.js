Clifton.collateral.setup.CollateralSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'collateralSetupWindow',
	iconCls: 'bank',
	title: 'Collateral Management Setup',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Haircut Definitions',
				items: [{
					name: 'collateralHaircutDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A Collateral Haircut defines haircuts that should be applied to market value of collateral positions. For example, treasuries are riskier than cash and may get 5% haircut while equities may get 30% haircut.' +
						'<ul class="c-list">Most specific haircuts are applied first in the following order:' +
						'<li>ISDA, Holding Account, Holding Company</li>' +
						'<li>Investment Instrument, Investment Hierarchy, Investment Sub Type 2, Investment Sub Type, Investment Type</li>' +
						'<li>Days To Maturity</li>' +
						'</ul>',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Company', width: 100, dataIndex: 'holdingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, defaultSortColumn: true},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.label', filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'ISDA', width: 100, dataIndex: 'businessContract.name', filter: {type: 'combo', searchFieldName: 'businessContractId', url: 'businessContractListFind.json?contractTypeName=ISDA'}},
						{header: 'Type', width: 40, dataIndex: 'investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
						{header: 'Sub Type 2', width: 60, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
						{header: 'Investment Hierarchy', width: 80, dataIndex: 'instrumentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 80, dataIndex: 'investmentInstrument.name', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.collateral.setup.HaircutDefinitionWindow'
					}
				}]
			},


			{
				title: 'Collateral Types',
				items: [{
					name: 'collateralTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Collateral Type determines how collateral balance is processed/managed and where the data is pulled from in the system.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 50, dataIndex: 'name', filter: false},
						{header: 'Description', width: 100, hidden: true, dataIndex: 'description', filter: false},
						{header: 'Holding Account Group', width: 60, dataIndex: 'accountGroup.name'},
						{header: 'Workflow', width: 70, dataIndex: 'workflow.name', filter: false, tooltip: 'Optional Workflow that can be used for reconciliation/affirmation/confirmation process'},
						{header: 'Detail Table', width: 50, dataIndex: 'detailTable.name', filter: false},
						{header: 'Collateral Rebuild Processor', width: 70, dataIndex: 'systemBeanType.name', filter: false, tooltip: 'Only beans of this type are allowed to be used as the RebuildProcessorBean for this collateral type on the Collateral Configuration tab.'},
						{header: 'Transfer Date Offset', width: 37, dataIndex: 'transferDateOffset', type: 'int'},
						{header: 'Local CCY Supported', width: 35, dataIndex: 'collateralInLocalCurrencySupported', type: 'boolean'},
						{header: 'Bookable', width: 20, dataIndex: 'bookable', type: 'boolean'},
						{header: 'Use Settlement Date', width: 20, dataIndex: 'useSettlementDate', type: 'boolean', hidden: true},
						{header: 'Category', width: 50, dataIndex: 'category', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.collateral.setup.CollateralTypeWindow'
					}
				}]
			},


			{
				title: 'Collateral Configuration',
				items: [{
					name: 'collateralConfigurationListFind',
					xtype: 'gridpanel',
					instructions: 'Collateral Configuration is used to customize collateral calculation/rebuild logic by holding/collateral company or account. Collateral account is looked up for the holding account based on corresponding relationship purpose(s). The logic uses specificity with Collateral Account being most specific and Holding Company being less specific.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Collateral Type', width: 100, dataIndex: 'collateralType.name', filter: {searchFieldName: 'collateralTypeName'}, defaultSortColumn: true},
						{header: 'Holding Company', width: 100, dataIndex: 'holdingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Holding Account', width: 150, dataIndex: 'holdingAccount.label', filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Collateral Company', width: 100, dataIndex: 'collateralCompany.name', filter: {type: 'combo', searchFieldName: 'collateralCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Collateral Account', width: 150, dataIndex: 'collateralAccount.label', filter: {type: 'combo', searchFieldName: 'collateralAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Rebuild Processor Bean', width: 100, dataIndex: 'rebuildProcessorBean.name', hidden: true},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.collateral.setup.CollateralConfigurationWindow'
					}
				}]
			},


			{
				title: 'Collateral Account Groups',
				items: [{
					xtype: 'investment-account-group-grid',
					instructions: 'The following Investment Account Groups of type "Collateral" are defined in the system.',
					groupTypeName: 'Collateral'
				}]
			}
		]
	}]
});
