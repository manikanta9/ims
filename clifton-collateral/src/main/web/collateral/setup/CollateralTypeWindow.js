Clifton.collateral.setup.CollateralTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Collateral Type',
	iconCls: 'bank',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 160,
		instructions: 'Collateral Type determines how collateral balance is processed/managed and where the data is pulled from in the system.',
		url: 'collateralType.json?requestedMaxDepth=4',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Holding Account Group', name: 'accountGroup.name', hiddenName: 'accountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json?typeName=Collateral Type Accounts', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Workflow', name: 'workflow.name', hiddenName: 'workflow.id', xtype: 'combo', url: 'workflowListFind.json', detailPageClass: 'Clifton.workflow.definition.WorkflowWindow', qtip: 'Optional Workflow that can be used for reconciliation/affirmation/confirmation process'},
			{fieldLabel: 'Detail Table', name: 'detailTable.name', hiddenName: 'detailTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'Collateral Rebuild Processor', name: 'systemBeanType.name', hiddenName: 'systemBeanType.id', xtype: 'combo', url: 'systemBeanTypeListFind.json', detailPageClass: 'Clifton.system.bean.TypeWindow', qtip: 'Only beans of this type are allowed to be used as the RebuildProcessorBean for this collateral type on the Collateral Configuration tab.'},
			{fieldLabel: 'Transfer Date Offset', name: 'transferDateOffset', xtype: 'spinnerfield', minValue: 0, qtip: 'OTC Collateral transfers are T+1 so when rebuilding the balance date we are subtracting 2 days. For OTC Cleared, Futures and REPO Collateral, transfers are T+0 so when rebuilding the balance date we are subtracting 1 day'},
			{name: 'collateralInLocalCurrencySupported', xtype: 'checkbox', boxLabel: 'Collateral transfers support Local Currency'},
			{name: 'bookable', xtype: 'checkbox', boxLabel: 'Collateral transfers are booked to the General Ledger'},
			{name: 'useSettlementDate', xtype: 'checkbox', boxLabel: 'Collateral Balances are calculated based on settlement dates.'},
			{name: 'allowRebuildOnTransferEvent', xtype: 'checkbox', boxLabel: 'Rebuild Collateral Balances after transfers.'},
			{
				fieldLabel: 'Category', name: 'category',
				mode: 'local', xtype: 'combo', allowBlank: false,
				store: {
					xtype: 'arraystore',
					fields: ['value', 'name'],
					data: Clifton.collateral.CollateralBalanceCategories
				}
			}
		]
	}]
});
