TCG.use('Clifton.collateral.BaseCollateralManagementWindow');

Clifton.collateral.OTCCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'otcCollateralManagementWindow',
	iconCls: 'bank',
	title: 'OTC Collateral',
	width: 1500,
	maximized: true,
	collateralTypeName: 'OTC Collateral',

	additionalItems: [{
		title: 'OTC Collateral',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralBalanceListFind',
			importTableName: 'CollateralBalance',
			instructions: 'OTC Collateral information for selected selected Date and other filter(s). Use "Admin" tab to rebuild based on latest information.',

			additionalPropertiesToRequest: 'id|balanceDate|parentCollateralBalance|holdingInvestmentAccountGroup.id|holdingInvestmentAccountGroup.groupAlias|workflowState.id|holdingInvestmentAccount.businessContract.id|holdingInvestmentAccount.issuingCompany.id|holdingInvestmentAccount.issuingCompany.abbreviation|holdingInvestmentAccount.baseCurrency.symbol|holdingInvestmentAccount.businessContract.id',
			useDynamicTooltipColumns: true,
			cacheDynamicTooltip: true,
			rowSelectionModel: 'checkbox',
			remoteSort: true,

			getTopToolbarFilters: function(toolbar) {
				const filters = [];
				filters.push({fieldLabel: '', boxLabel: 'Exclude Accounts without Collateral Change &nbsp; &nbsp;', xtype: 'toolbar-checkbox', name: 'excludeAccountsWithoutCollateralChange'});
				filters.push({fieldLabel: '', boxLabel: 'Exclude Accounts without Positions &nbsp;', xtype: 'toolbar-checkbox', name: 'excludeAccountsWithoutPositions', checked: true});
				filters.push({fieldLabel: '', boxLabel: 'Exclude "Closed" Workflow Status &nbsp;', xtype: 'toolbar-checkbox', name: 'excludeClosedWorkflowStatus'});
				filters.push('-');
				filters.push({fieldLabel: 'Client', xtype: 'combo', name: 'businessClientId', width: 180, url: 'businessClientListFind.json', linkedFilter: 'holdingInvestmentAccount.businessClient.label', displayField: 'label'});
				filters.push({fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'});
				return filters;
			},
			configureToolsMenu: function(menu) {
				const gridPanel = this;
				menu.add('-');
				menu.add({
					text: 'Preview Instruction Message',
					tooltip: 'Preview Instruction SWIFT Message',
					iconCls: 'shopping-cart',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						const collateral = sm.getSelections();
						if (sm.getCount() !== 1) {
							TCG.showError('Please select a single collateral to preview.', 'Incorrect Selection');
						}
						else {
							const id = collateral[0].id;
							Clifton.instruction.openInstructionPreview(id, 'CollateralBalance', gridPanel);
						}
					}
				});
			},
			getLoadParams: function(firstLoad) {
				const t = this.getTopToolbar();
				if (firstLoad) {
					const dd = this.getWindow().defaultData;
					let dateValue;
					if (dd && dd.balanceDate) { // balanceDate can be defaulted by the caller of window open
						dateValue = dd.balanceDate.format('m/d/Y');
					}
					else {
						// default balance date to previous business day
						dateValue = TCG.getPreviousWeekday();
					}
					TCG.getChildByName(t, 'balanceDate').setValue(dateValue);
					// TODO: support != filter: this.setFilterValue('positionsMarketValue', {'ne': 0});
				}
				const params = {
					parentIsNull: true, // only retrieve top level balances
					populateHoldingInvestmentAccountGroup: true,
					collateralTypeName: this.getWindow().collateralTypeName,
					excludeAccountsWithoutCollateralChange: TCG.getChildByName(t, 'excludeAccountsWithoutCollateralChange').getValue(),
					excludeAccountsWithoutPositions: TCG.getChildByName(t, 'excludeAccountsWithoutPositions').getValue(),
					balanceDate: TCG.getChildByName(t, 'balanceDate').getValue().format('m/d/Y')
				};
				if (TCG.getChildByName(t, 'excludeClosedWorkflowStatus').getValue()) {
					params.excludeWorkflowStatusName = 'Closed';

				}
				return params;
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=CollateralBalance', showNotEquals: true}, hidden: true, viewNames: ['Export Friendly']},
				{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View', 'Export Friendly']},
				{
					header: 'Violation Status', width: 125, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
					renderer: function(v, p, r) {
						return (Clifton.rule.violation.renderViolationStatus(v));
					}
				},
				{header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'}, defaultSortColumn: true},
				{header: 'Holding Account ID', width: 75, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
				{
					header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', defaultSortColumn: true, filter: {searchFieldName: 'holdingInvestmentAccountLabelOrGroupAlias', sortField: 'holdingInvestmentAccount.number'},
					renderer: function(v, metaData, r) {
						if (TCG.isTrue(r.json.parentCollateralBalance)) {
							// need to get the account group to display the alias in the column
							if (r.json.holdingInvestmentAccountGroup && TCG.isNotBlank(r.json.holdingInvestmentAccountGroup.groupAlias)) {
								return '<div class="tooltip-column" tooltipEventName="ShowAccountList"><div class="buy-light">' + r.json.holdingInvestmentAccountGroup.groupAlias + '</div></div>';
							}
						}
						return v;
					}
				},

				{header: 'Holding Company', width: 150, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},

				{header: 'ISDA', width: 100, dataIndex: 'holdingInvestmentAccount.businessContract.name', hidden: true, filter: {type: 'combo', searchFieldName: 'businessContractId', url: 'businessContractListFind.json?contractTypeName=ISDA'}},

				{header: 'Exposure/MTM', width: 100, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency', hidden: true},
				{header: 'Independent Amount', width: 100, dataIndex: 'independentAmount', useNull: true, negativeInRed: true, type: 'currency', hidden: true},
				{header: 'Total Requirement', width: 100, dataIndex: 'calculatedTotalRequirement', useNull: true, negativeInRed: true, type: 'currency', filter: false, sortable: false, tooltip: 'The total amount of Collateral required (prior to Rounding & Minimum Transfer Amount being applied).'},

				{header: 'Client Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Client Posted Collateral', width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
					renderer: function(v, metaData, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCollateralValue'], r.data['postedCollateralValue'], '0,000.00');
					}
				},

				{header: 'Counterparty Collateral (Original)', width: 100, hidden: true, dataIndex: 'postedCounterpartyCollateralValue', useNull: true, type: 'currency'},
				{
					header: 'Counterparty Posted Collateral', width: 100, dataIndex: 'postedCounterpartyCollateralHaircutValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, v !== r.data['postedCounterpartyCollateralValue'], r.data['postedCounterpartyCollateralHaircutValue'], '0,000.00');
					}
				},

				{header: 'Client Collateral Change', width: 100, dataIndex: 'collateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0;'},
				{header: 'Counterparty Collateral Change', width: 100, dataIndex: 'counterpartyCollateralChangeAmount', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #fff0f0;'},

				{header: 'Client Ending Collateral', width: 100, dataIndex: 'endingPostedCollateralValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;'},
				{header: 'Counterparty Ending Collateral', width: 100, dataIndex: 'endingPostedCounterpartyCollateralValue', useNull: true, type: 'currency', css: 'BACKGROUND-COLOR: #f0f0ff;'},

				{header: 'Counterparty Call Amount', dataIndex: 'counterpartyCollateralCallAmount', width: 100, useNull: true, type: 'currency', hidden: true},
				{header: 'Agreed Upon Amount', dataIndex: 'transferAmount', width: 100, useNull: true, type: 'currency'},

				{header: 'Threshold', dataIndex: 'thresholdAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Rounding', dataIndex: 'contractRounding', width: 100, hidden: true, useNull: true, type: 'float'},
				{header: 'Counterparty Threshold', width: 100, hidden: true, dataIndex: 'thresholdCounterpartyAmount', useNull: true, type: 'currency'},
				{header: 'Minimum Transfer Amount', dataIndex: 'minTransferAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Counterparty Minimum Transfer Amount', dataIndex: 'minTransferCounterpartyAmount', width: 100, hidden: true, useNull: true, type: 'currency'}
			],
			editor: {
				ptype: 'workflow-transition-grid',
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true,
				tableName: 'CollateralBalance',
				transitionWindowTitle: 'Transition Collateral Balance Record(s)',
				transitionWindowInstructions: 'Select Workflow Transition for selected collateral balance record(s)',
				addAfterTransitionButtons: function(toolBar, gridPanel) {
					toolBar.add({
						text: 'Email Margin Call',
						tooltip: 'Generates a Margin Call Email for selected accounts. An email will be generated only when Client Collateral Change is negative or Counterparty Collateral Change is positive. Default email to "Collateral Call Recipient" party role(s) from the ISDA.',
						iconCls: 'email',
						scope: this,
						handler: function() {
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select at least one row for Margin Call.', 'No Row(s) Selected');
							}
							else {
								let valid = true;
								const selections = sm.getSelections();
								for (let i = 0; i < selections.length; i++) {
									const s = selections[i].json;
									if (s.collateralChangeAmount > 0 || s.counterpartyCollateralChangeAmount < 0 || (s.collateralChangeAmount === 0 && s.counterpartyCollateralChangeAmount === 0)) {
										valid = false;
										TCG.showError('Cannot email Margin Call for \'' + s.holdingInvestmentAccount.businessClient.label + '\' that is not in Client\'s favor. Client Collateral Change must be negative or Counterparty Collateral Change must be positive.', 'Invalid Selection');
										break;
									}
								}

								if (valid) {
									for (let i = 0; i < selections.length; i++) {
										Clifton.collateral.generateMarginCallEmail(selections[i].json, this);
									}
								}
							}
						}
					});
					toolBar.add('-');
				}
			},
			getTooltipTextForRow: function(row, id, tooltipEventName) {
				if (TCG.isEquals('ShowAccountList', tooltipEventName) && TCG.isTrue(row.json.parentCollateralBalance)) {
					const balanceList = TCG.data.getData('collateralBalanceListFind.json?parentId=' + id + '&requestedPropertiesRoot=data&requestedProperties=holdingInvestmentAccount.number|externalExcessCollateral|cashCollateralValue|securitiesCollateralValue|transferAmount', this);
					let result = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px;">';
					result += '<style>.collateral-child-table th{font-weight: bold; text-align:right;} .collateral-child-table td{text-align: right; width: 100px;} }</style>';
					result += '<table class="collateral-child-table"><tr><th style="text-align: left;">Holding Account</th><th>Excess<br/>Collateral<br/>(External)</th><th>Cash<br/>Collateral</th><th>Securities<br/>Collateral</th><th>Transfer<br/>Amount</th></tr><tbody>';

					const numFormat = '0,000.00';
					Ext.each(balanceList, function(balance) {
						result += '<tr><td style="text-align: left;">' + balance.holdingInvestmentAccount.number + '</td><td>'
							+ TCG.renderAmount(balance.externalExcessCollateral ? balance.externalExcessCollateral : 0, true, +numFormat) + '</td><td>'
							+ TCG.renderAmount(balance.cashCollateralValue, true, +numFormat) + '</td><td>'
							+ TCG.renderAmount(balance.securitiesCollateralValue, true, +numFormat) + '</td><td>'
							+ TCG.renderAmount(balance.transferAmount ? balance.transferAmount : 0, true, +numFormat) + '</td></tr>';
					});

					result += '<tr><td width="75"></td><td align="right" width="100"></td><td align="right" width="100"></td><td align="right" width="100"></td><td align="right" width="100">Records: '
						+ balanceList.length + '</td></tr>';

					result += '</tbody></table></div>';
					return result;
				}
				return null;
			},
			plugins: [{ptype: 'collateral-balance-grideditor'}]
		}]
	}]
});
