Clifton.collateral.CashCollateralTransactionsCreationWindow = Ext.extend(TCG.app.OKCancelWindow, {
	id: 'cashCollateralTransactionsCreationWindow',
	iconCls: 'bank',
	title: 'Cash Collateral Transactions Creation',
	width: 400,
	height: 200,
	modal: true,
	okButtonText: 'Generate',
	okButtonTooltip: 'Generate cash collateral transactions for the selected trade date',
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Select a trade date to generate cash collateral transactions for.  The simple journal entries will be created, and then displayed.' +
			'From there the journal entries may be booked and posted after being manually reviewed.',
		items: [
			{xtype: 'datefield', fieldLabel: 'Trade Date', name: 'tradeDate', value: new Date()}
		]
	}],
	saveWindow: function(closeOnSuccess, forceSubmit) {
		const window = this;
		const formPanel = this.getMainFormPanel();
		let tradeDate = formPanel.getForm().findField('tradeDate').getValue();
		if (tradeDate !== '') {
			tradeDate = tradeDate.format('m/d/Y');
		}
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Generating...',
			waitTarget: formPanel,
			params: {tradeDate: tradeDate},
			success: function(response) {
				window.closeWindow();
				const result = Ext.decode(response.responseText);
				TCG.createComponent('Clifton.core.StatusWindow', {
					defaultData: {status: result.data},
					render: function() {
						TCG.Window.superclass.render.apply(this, arguments);
						const win = this;
						win.on('beforeclose', function() {
							const params = {};
							params.journalNotBooked = true;
							params.category2Name = 'Accounting Simple Journal Type Tags';
							params.category2TableName = 'AccountingSimpleJournalType';
							params.category2LinkFieldPath = 'journalType';
							params.category2HierarchyName = 'Cash Collateral Transactions';
							TCG.createComponent('Clifton.accounting.simplejournal.SimpleJournalListWindow', params);
						});
					}
				});
			}
		});
		loader.load('collateralCashSimpleJournalEntriesGenerate.json');
	}
});
