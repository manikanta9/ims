TCG.use('Clifton.collateral.BaseCollateralManagementWindow');
Clifton.collateral.OptionsEscrowCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'optionsEscrowCollateralManagementWindow',
	iconCls: 'bank',
	title: 'Options Escrow Collateral',
	width: 1500,
	collateralTypeName: 'Options Escrow Collateral',
	additionalItems: [{
		title: 'Options Escrow Collateral',
		items: [{
			xtype: 'options-collateral-grid',
			appendedColumns: [
				{header: 'Positions Market Value', width: 100, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency', hidden: true},
				{header: 'Collateral Requirement', width: 100, dataIndex: 'collateralRequirement', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Posted Collateral', width: 100, dataIndex: 'postedCollateralValue', useNull: true, negativeInRed: true, type: 'currency', hidden: true},
				{header: 'Posted Collateral (After Haircut)', width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Required Movement', width: 100, dataIndex: 'optionsCollateralRequiredMovement', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Required Movement %', width: 100, dataIndex: 'optionsCollateralRequiredMovementPercentage', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Agreed Upon Amount', width: 100, dataIndex: 'transferAmount', useNull: true, negativeInRed: true, type: 'currency'}
			], editor: {
				ptype: 'workflow-transition-grid',
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true,
				tableName: 'CollateralBalance',
				timeout: 900000,
				transitionWindowTitle: 'Transition Balance Records',
				transitionWindowInstructions: 'Select Transition for selected collateral balance records'
			},
			configureToolsMenu: function(menu) {
				const gridPanel = this;
				menu.add('-');
				menu.add({
					text: 'Preview Instruction Message',
					tooltip: 'Preview Instruction SWIFT Message',
					iconCls: 'shopping-cart',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						const collateral = sm.getSelections();
						if (sm.getCount() !== 1) {
							TCG.showError('Please select a single collateral to preview.', 'Incorrect Selection');
						}
						else {
							const id = collateral[0].id;
							Clifton.instruction.openInstructionPreview(id, 'CollateralBalance', gridPanel);
						}
					}
				});
			}
		}]
	}]
});
