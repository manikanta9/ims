TCG.use('Clifton.collateral.BaseCollateralManagementWindow');
Clifton.collateral.ClearedOTCCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'clearedOTCCollateralManagementWindow',
	iconCls: 'bank',
	title: 'Cleared OTC Collateral',
	width: 1500,
	collateralTypeName: 'Cleared OTC Collateral',
	additionalItems: [{
		title: 'Cleared OTC Collateral',
		items: [{
			xtype: 'non-otc-collateral-grid',
			columnOverrides: [{dataIndex: 'workflowStatus.name', hidden: true}, {dataIndex: 'workflowState.name', hidden: true}]
		}]
	}]
});
