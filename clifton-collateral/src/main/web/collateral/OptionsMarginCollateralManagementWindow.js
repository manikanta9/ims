TCG.use('Clifton.collateral.BaseCollateralManagementWindow');
Clifton.collateral.OptionsMarginCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'optionsMarginCollateralManagementWindow',
	iconCls: 'bank',
	title: 'Options Margin Collateral',
	width: 1500,
	maximized: true,
	collateralTypeName: 'Options Margin Collateral',

	additionalItems: [{
		title: 'Options Margin Collateral',
		items: [{
			xtype: 'options-collateral-grid',
			remoteSort: false,
			forceLocalFiltering: true,
			isPagingEnabled: function() {
				return false;
			},
			appendedColumns: [
				{header: 'Equity Value', width: 100, dataIndex: 'optionsPositionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Collateral Requirement', width: 100, dataIndex: 'optionsCollateralRequirement', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Excess/(Deficit)', width: 100, dataIndex: 'excessOptionsEquityCollateral', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Excess/(Deficit) %', width: 100, dataIndex: 'excessOptionsEquityCollateralPercentage', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Target  Movement', width: 100, dataIndex: 'optionsTargetMovement', useNull: true, negativeInRed: true, type: 'currency', nonPersistentField: true},
				{header: 'Target  Movement %', width: 100, dataIndex: 'optionsTargetMovementPercentage', useNull: true, negativeInRed: true, type: 'currency', nonPersistentField: true},
				{header: 'Agreed Movement', width: 100, dataIndex: 'agreedMovementAmount', useNull: true, negativeInRed: true, type: 'currency'},
				{header: 'Equity Value(External)', width: 100, dataIndex: 'externalCollateralAmount', useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Required Collateral (External)', width: 100, dataIndex: 'externalCollateralRequirement', useNull: true, type: 'currency', negativeInRed: true},
				{header: 'Excess Collateral (External)', width: 100, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true}
			],
			configureToolsMenu: function(menu) {
				const gridPanel = this;
				menu.add('-');
				menu.add({
					text: 'Preview Instruction Message',
					tooltip: 'Preview Instruction SWIFT Message',
					iconCls: 'shopping-cart',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						const collateral = sm.getSelections();
						if (sm.getCount() !== 1) {
							TCG.showError('Please select a single collateral to preview.', 'Incorrect Selection');
						}
						else {
							const id = collateral[0].id;
							Clifton.instruction.openInstructionPreview(id, 'CollateralBalance', gridPanel);
						}
					}
				});
			}
		}]
	}]
});
