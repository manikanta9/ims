Ext.ns('Clifton.collateral', 'Clifton.collateral.balance', 'Clifton.collateral.balance.options', 'Clifton.collateral.setup');


Clifton.collateral.OptionsCollateralAllowableCollateral = [
	['Cash Only', 'Cash', 'Allow only cash as pledged collateral.'],
	['Securities Only', 'Securities', 'Allow only securities as pledged collateral.']
];

Clifton.collateral.OptionsCollateralRequirementTypes = [
	['Margin', 'Margin', 'Sets the calculation of collateral to Margin.'],
	['Escrow Receipt', 'Escrow Receipt', 'Sets the calculation of collateral to Escrow.']
];

Clifton.collateral.CollateralBalanceCategories = [
	['DEFAULT', 'Default Collateral'],
	['OPTIONS_MARGIN', 'Options Margin Collateral'],
	['OPTIONS_ESCROW', 'Options Escrow Collateral'],
	['OTC_COLLATERAL', 'OTC Collateral']
];

Clifton.collateral.balance.options.OptionsCollateralPledgedTransactionsUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload (Pledged Transactions)',
	iconCls: 'import',
	height: 150,
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	width: 500,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 80,
		instructions: 'Browse for the external file, and click OK to upload.',
		items: [
			{fieldLabel: 'External File', name: 'file', xtype: 'fileuploadfield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'collateralBalancePledgedTransactionsFileUpload.json?collateralBalanceId=' + this.getFormValue('collateralBalanceId');
		},
		listeners: {
			afterload: function(panel) {
				panel.getWindow().defaultData.openerCt.reload();
			}
		}
	}]
});


Clifton.collateral.NonOTCCollateralGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'collateralBalanceListFind',
	importTableName: 'CollateralBalance',
	instructions: 'Daily collateral information for selected filters.',
	additionalPropertiesToRequest: 'holdingInvestmentAccountGroup.id|holdingInvestmentAccountGroup.groupAlias|workflowState.id',
	useDynamicTooltipColumns: true,
	cacheDynamicTooltip: true,

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Holding Acct #', width: 100, xtype: 'toolbar-textfield', name: 'holdAccountNumber'});
		filters.push({fieldLabel: 'Collateral Company', xtype: 'toolbar-combo', name: 'collateralCompanyId', width: 150, url: 'businessCompanyListFind.json?issuer=true'});
		filters.push({fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		let dateValue;
		const bd = TCG.getChildByName(t, 'balanceDate');
		if (TCG.isNotBlank(bd.getValue())) {
			dateValue = (bd.getValue()).format('m/d/Y');
		}
		else {
			const dd = this.getWindow().defaultData;
			if (firstLoad && dd && dd.balanceDate) { // balanceDate can be defaulted by the caller of window open
				dateValue = dd.balanceDate.format('m/d/Y');
			}
			else {
				dateValue = TCG.getPreviousWeekday().format('m/d/Y');
			}
			bd.setValue(dateValue);
		}

		const params = {
			collateralTypeName: this.getWindow().collateralTypeName,
			balanceDate: dateValue,
			parentIsNull: true, // only retrieve top level balances
			populateHoldingInvestmentAccountGroup: true
		};
		const holdAccountNumber = TCG.getChildByName(this.getTopToolbar(), 'holdAccountNumber');
		if (TCG.isNotBlank(holdAccountNumber.getValue())) {
			params.holdingInvestmentAccountLabel = holdAccountNumber.getValue();
		}
		const ccId = TCG.getChildByName(t, 'collateralCompanyId').getValue();
		if (TCG.isNotBlank(ccId)) {
			params.collateralCompanyId = ccId;
		}
		return params;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'PID', width: 15, dataIndex: 'parent.id', hidden: true, useNull: true, type: 'int', doNotFormat: true, filter: {searchFieldName: 'parentId'}},
		{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Options Collateral Workflow'}, hidden: true},
		{header: 'Workflow State', width: 150, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{
			header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'},
			renderer: function(v, metaData, r) {
				const note = r.data.note;
				if (TCG.isNotBlank(note)) {
					const qtip = r.data.note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
		{header: 'Client Account', width: 75, dataIndex: 'clientInvestmentAccount.number', filter: {searchFieldName: 'clientInvestmentAccountLabel'}},
		{header: 'Holding Account ID', width: 75, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
		{
			header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', defaultSortColumn: true, filter: {searchFieldName: 'holdingInvestmentAccountLabelOrGroupAlias', sortField: 'holdingInvestmentAccount.number'},
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.json.parentCollateralBalance)) {
					// need to get the account group to display the alias in the column
					const accountGroupAlias = r.json.holdingInvestmentAccountGroup.groupAlias;
					if (TCG.isNotBlank(accountGroupAlias)) {
						return '<div class="tooltip-column" tooltipEventName="ShowAccountList"><div class="buy-light">' + accountGroupAlias + '</div></div>';
					}
				}
				return v;
			}
		},
		{header: 'Holding Company', width: 130, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},
		{header: 'Initial Margin Multiplier', width: 100, dataIndex: 'holdingInvestmentAccount.initialMarginMultiplier', filter: false, sortable: false, hidden: true, tooltip: 'This field applies to holding accounts and is usually equal to 1 (no additional requirements: use standard). However, a broker may deem a certain account to be more risky and impose additional requirement: 1.5, 2.5, 3.25, etc.'},

		{header: 'Collateral Account', width: 75, dataIndex: 'collateralInvestmentAccount.number', filter: {searchFieldName: 'collateralInvestmentAccountLabel'}},
		{header: 'Collateral Company', width: 130, dataIndex: 'collateralInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'collateralCompanyName'}},

		{header: 'Hold', width: 50, dataIndex: 'holdAccount', type: 'boolean', filter: {searchFieldName: 'holdingAccountIsHold'}},
		{
			header: 'Violation Status', width: 125, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{header: 'Currency', width: 50, hidden: true, dataIndex: 'collateralCurrency.symbol', filter: {type: 'combo', searchFieldName: 'collateralCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
		{header: 'Positions Market Value', width: 100, hidden: true, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

		{header: 'Required Collateral (External)', width: 100, dataIndex: 'externalCollateralRequirement', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
		{header: 'Posted Collateral (External)', width: 100, dataIndex: 'externalCollateralAmount', hidden: true, useNull: true, type: 'currency', negativeInRed: true},
		{header: 'Excess Collateral (External)', width: 100, dataIndex: 'externalExcessCollateral', type: 'currency', negativeInRed: true},
		{
			header: 'Excess Collateral % (External)', width: 100, dataIndex: 'externalExcessCollateralPercentage', type: 'currency', negativeInRed: true,
			renderer: function(v, metaData, r) {
				//Change to whole percentage with a precision of 2 and do not color positive values
				return TCG.renderAmount(v * 100, false, '0,000.00 %');
			}
		},
		{
			header: 'Required Collateral', width: 100, hidden: true, dataIndex: 'collateralRequirement', useNull: true, negativeInRed: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(v, metaData, r) {
				const ext = r.data.externalCollateralRequirement;
				let extVal = 0;
				if (Ext.isNumber(ext)) {
					extVal = ext;
				}
				let val = 0;
				if (Ext.isNumber(v)) {
					val = v;
				}
				const dif = extVal - val;

				let qtip = '<table>';
				qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr></table>';
				metaData.attr = 'qtip=\'' + qtip + '\'';
				return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralRequirement'], r.data['externalCollateralRequirement'], '0,000.00');
			}
		},
		{header: 'Posted Collateral (Original)', width: 100, dataIndex: 'postedCollateralValue', useNull: true, type: 'currency', hidden: true},
		{header: 'Cash Collateral', width: 100, dataIndex: 'cashCollateralValue', useNull: true, type: 'currency'},
		{header: 'Securities Collateral', width: 100, dataIndex: 'securitiesCollateralValue', useNull: true, type: 'currency'},
		{
			header: 'Posted Collateral', width: 100, dataIndex: 'postedCollateralHaircutValue', useNull: true, type: 'currency',
			renderer: function(v, metaData, r) {
				const ext = r.data.externalCollateralAmount;
				let extVal = 0;
				if (Ext.isNumber(ext)) {
					extVal = ext;
				}
				let val = 0;
				if (Ext.isNumber(v)) {
					val = v;
				}
				const dif = extVal - val;

				let qtip = '<table>';
				qtip += '<tr><td>Clifton (Original):</td><td align="right">' + Ext.util.Format.number(r.data.postedCollateralValue, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Clifton (Adjusted):</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr></table>';
				metaData.attr = 'qtip=\'' + qtip + '\'';
				return TCG.renderAdjustedAmount(v, v !== r.data['externalCollateralAmount'], r.data['externalCollateralAmount'], '0,000.00');
			}
		},
		{
			header: 'Excess Collateral', width: 100, dataIndex: 'excessCollateral', type: 'currency', negativeInRed: true,
			renderer: function(v, metaData, r) {
				const ext = r.data.externalExcessCollateral;
				let extVal = 0;
				if (Ext.isNumber(ext)) {
					extVal = ext;
				}
				let val = 0;
				if (Ext.isNumber(v)) {
					val = v;
				}
				const dif = extVal - val;

				let qtip = '<table>';
				qtip += '<tr><td>Clifton:</td><td align="right">' + Ext.util.Format.number(v, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Broker:</td><td align="right">' + Ext.util.Format.number(ext, '0,000.00') + '</td></tr>';
				qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(dif, true) + '</td></tr></table>';
				metaData.attr = 'qtip=\'' + qtip + '\'';
				return TCG.renderAdjustedAmount(v, v !== r.data['externalExcessCollateral'], r.data['externalExcessCollateral'], '0,000.00');
			}
		},
		{
			header: 'Excess Collateral Percentage', width: 100, dataIndex: 'excessCollateralPercentage', type: 'currency', negativeInRed: true, hidden: true,
			renderer: function(v, metaData, r) {
				//Change to whole percentage with a precision of 2 and do not color positive values
				return TCG.renderAmount(v * 100, false, '0,000.00 %');
			}
		},
		{
			header: 'Transfer Amount', width: 100, dataIndex: 'transferAmount', type: 'currency', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(v, metaData, r) {
				let qtip = '<table>';
				qtip += '<tr><td>Cash Transfer Amount:</td><td align="right">' + TCG.renderAmount(v) + '</td></tr>';
				qtip += '<tr><td>Securities Transfer Amount:</td><td align="right">' + TCG.renderAmount(r.data['securitiesTransferAmount']) + '</td></tr></table>';
				metaData.attr = 'qtip=\'' + qtip + '\'';
				//Change to whole percentage with a precision of 2 and do not color positive values
				return TCG.renderAmount(v + r.data['securitiesTransferAmount']);
			}
		},
		{header: 'Securities Transfer Amount', width: 100, hidden: true, dataIndex: 'securitiesTransferAmount', type: 'currency', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
		{header: 'Reconciled', width: 75, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true},
		{header: 'Booking Date', width: 50, dataIndex: 'bookingDate', hidden: true},
		{header: 'Booked', width: 50, dataIndex: 'booked', type: 'boolean'},
		{header: 'Parent Balance', width: 50, dataIndex: 'parentCollateralBalance', type: 'boolean', hidden: 'true'}
	],
	editor: {
		detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Book and Post',
				tooltip: 'Book unbooked non-zero collateral transfers and immediately post them to the General Ledger',
				iconCls: 'book-red',
				scope: this,
				handler: function() {
					TCG.createComponent('Clifton.collateral.CollateralBookAndPostWindow', {
						defaultData: {
							balanceDate: TCG.getChildByName(t, 'balanceDate').getValue().format('m/d/Y'),
							collateralTypeName: this.getWindow().collateralTypeName
						},
						openerCt: gridPanel
					});
				}
			});
			t.add('-');
		}
	},
	getTooltipTextForRow: function(row, id, tooltipEventName) {
		if (TCG.isEquals('ShowAccountList', tooltipEventName) && TCG.isTrue(row.json.parentCollateralBalance)) {
			const balanceList = TCG.data.getData('collateralBalanceListFind.json?parentId=' + id + '&requestedPropertiesRoot=data&requestedProperties=holdingInvestmentAccount.number|externalExcessCollateral|cashCollateralValue|securitiesCollateralValue|transferAmount', this);
			let result = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px;">';
			result += '<style>.collateral-child-table th{font-weight: bold; text-align:right;} .collateral-child-table td{text-align: right; width: 100px;} }</style>';
			result += '<table class="collateral-child-table"><tr><th style="text-align: left;">Holding Account</th><th>Excess<br/>Collateral<br/>(External)</th><th>Cash<br/>Collateral</th><th>Securities<br/>Collateral</th><th>Transfer<br/>Amount</th></tr><tbody>';

			const numFormat = '0,000.00';
			Ext.each(balanceList, function(balance) {
				result += '<tr><td style="text-align: left;">' + balance.holdingInvestmentAccount.number + '</td><td>'
					+ TCG.renderAmount(balance.externalExcessCollateral ? balance.externalExcessCollateral : 0, true, +numFormat) + '</td><td>'
					+ TCG.renderAmount(balance.cashCollateralValue, true, +numFormat) + '</td><td>'
					+ TCG.renderAmount(balance.securitiesCollateralValue, true, +numFormat) + '</td><td>'
					+ TCG.renderAmount(balance.transferAmount ? balance.transferAmount : 0, true, +numFormat) + '</td></tr>';
			});

			result += '<tr><td width="75"></td><td align="right" width="100"></td><td align="right" width="100"></td><td align="right" width="100"></td><td align="right" width="100">Records: '
				+ balanceList.length + '</td></tr>';

			result += '</tbody></table></div>';
			return result;
		}
		return null;
	},
	plugins: [{ptype: 'collateral-balance-grideditor'}]
});
Ext.reg('non-otc-collateral-grid', Clifton.collateral.NonOTCCollateralGrid);

Clifton.collateral.OptionsCollateralGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'collateralBalanceListFind',
	importTableName: 'CollateralBalance',
	instructions: 'Daily collateral information for selected filters.',

	additionalPropertiesToRequest: 'id|workflowState.id',
	rowSelectionModel: 'checkbox',
	remoteSort: true,

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: '', boxLabel: 'Exclude Accounts without Positions &nbsp;', xtype: 'toolbar-checkbox', name: 'excludeAccountsWithoutPositions', checked: true});
		filters.push({fieldLabel: '', boxLabel: 'Exclude "Closed" Workflow Status &nbsp;', xtype: 'toolbar-checkbox', name: 'excludeClosedWorkflowStatus'});
		filters.push('-');
		filters.push({fieldLabel: 'Client', xtype: 'toolbar-combo', name: 'businessClientId', width: 180, url: 'businessClientListFind.json', linkedFilter: 'holdingInvestmentAccount.businessClient.label', displayField: 'label'});
		filters.push({fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			const dd = this.getWindow().defaultData;
			let dateValue;
			if (dd && dd.balanceDate) { // balanceDate can be defaulted by the caller of window open
				dateValue = dd.balanceDate.format('m/d/Y');
			}
			else {
				// default balance date to previous business day
				dateValue = TCG.getPreviousWeekday();
			}
			TCG.getChildByName(t, 'balanceDate').setValue(dateValue);
			// TODO: support != filter: this.setFilterValue('positionsMarketValue', {'ne': 0});
		}
		const params = {
			parentIsNull: true,
			collateralTypeName: this.getWindow().collateralTypeName,
			excludeAccountsWithoutPositions: TCG.getChildByName(t, 'excludeAccountsWithoutPositions').getValue(),
			balanceDate: TCG.getChildByName(t, 'balanceDate').getValue().format('m/d/Y')
		};
		if (TCG.getChildByName(t, 'excludeClosedWorkflowStatus').getValue()) {
			params.excludeWorkflowStatusName = 'Closed';

		}
		return params;
	},
	appendedColumns: [],
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Options Collateral Workflow'}, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Workflow State', width: 150, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Note', width: 200, hidden: true, dataIndex: 'note', filter: false, sortable: false},
		{
			header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'},
			renderer: function(v, metaData, r) {
				const note = r.data.note;
				if (!TCG.isBlank(note)) {
					const qtip = r.data.note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Holding Account ID', width: 75, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
		{header: 'Holding Account', width: 75, dataIndex: 'holdingInvestmentAccount.number', defaultSortColumn: true, filter: {searchFieldName: 'holdingInvestmentAccountLabelOrGroupAlias', sortField: 'holdingInvestmentAccount.number'}},
		{header: 'Holding Account Type', width: 75, dataIndex: 'holdingInvestmentAccount.type.name', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountTypeId', displayField: 'label', url: 'investmentAccountTypeListFind.json?ourAccount=false'}},
		{header: 'Holding Company', width: 150, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Collateral Account', width: 75, dataIndex: 'collateralInvestmentAccount.number', filter: {searchFieldName: 'collateralInvestmentAccountLabel'}},
		{header: 'Collateral Company', width: 150, dataIndex: 'collateralInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'collateralCompanyId', url: 'businessCompanyListFind.json?issuer=true'}}
	],
	initColumns: function() {
		this.columns = TCG.clone(this.columns.concat(this.appendedColumns));
	},
	editor: {
		ptype: 'workflow-transition-grid',
		detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
		drillDownOnly: true,
		tableName: 'CollateralBalance',
		transitionWindowTitle: 'Transition Balance Records',
		transitionWindowInstructions: 'Select Transition for selected collateral balance records'
	},
	plugins: [{ptype: 'collateral-balance-grideditor'}]
});
Ext.reg('options-collateral-grid', Clifton.collateral.OptionsCollateralGrid);

Clifton.collateral.balance.CollateralBalanceGridEditor = function(config) {
	Ext.apply(this, config);
};

Ext.extend(Clifton.collateral.balance.CollateralBalanceGridEditor, Ext.util.Observable, {
	init: function(grid) {
		this.grid = grid.grid;
		grid.editor.init(this.grid);
		this.grid.on('afterrender', function() {
			const el = this.getEl();
			el.on('contextmenu', function(e, target) {
				e.preventDefault();
				const g = this;
				g.contextRowIndex = g.view.findRowIndex(target);
				if (!g.drillDownMenu) {
					g.drillDownMenu = new Ext.menu.Menu({
						items: [{
							text: 'Rebuild Collateral Balance', iconCls: 'reconcile', handler: function() {
								const row = g.getStore().getAt(g.contextRowIndex);
								if (TCG.isNotNull(row)) {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										waitMsg: 'Rebuilding...',
										params: {
											requestedMaxDepth: 1,
											balanceDate: TCG.parseDate(TCG.getChildByName(grid.getTopToolbar(), 'balanceDate').getValue()).format('m/d/Y'),
											collateralTypeName: grid.getLoadParams().collateralTypeName,
											holdingAccountId: row.data['holdingInvestmentAccount.id'],
											skipExisting: 0,
											synchronous: true
										},
										onLoad: function(record, conf) {
											grid.reload();
										}
									});
									loader.load('collateralBalanceRebuild.json');
								}
							}
						}]
					});
				}
				TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
			}, this);
		});
	}
});
Ext.preg('collateral-balance-grideditor', Clifton.collateral.balance.CollateralBalanceGridEditor);


Clifton.collateral.CollateralBookAndPostWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Book and Post',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Book any unbooked non-otc collateral transfer entries for the following selection and immediately post them to the General Ledger.',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Balance Date', name: 'balanceDate', readOnly: true},
			{fieldLabel: 'Collateral Type', name: 'collateralTypeName', readOnly: true},
			{fieldLabel: 'Client', xtype: 'combo', hiddenName: 'clientId', url: 'businessClientListFind.json?ourAccount=true', displayField: 'label', pageSize: 10},
			{fieldLabel: 'Holding Company', xtype: 'combo', hiddenName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, mutuallyExclusiveFields: ['holdingInvestmentAccountId']},
			{fieldLabel: 'Holding Account', xtype: 'combo', hiddenName: 'holdingInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', pageSize: 10, mutuallyExclusiveFields: ['issuingCompanyId']}
		],
		getSaveURL: function() {
			return 'collateralBalanceListBookAndPost.json';
		},
		listeners: {
			afterload: function(panel) {
				TCG.showInfo('Booked and posted ' + panel.getForm().formValues + ' accounts.', 'Posting Status');
			}
		}
	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		const opener = this.openerCt;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				TCG.showInfo('Booked and posted ' + action.result.data + ' accounts.', 'Posting Status');

				opener.setFilterValue('violationStatus.name', ['UNPROCESSED', 'PROCESSED_FAILED', 'PROCESSED_IGNORED', 'PROCESSED_VIOLATIONS']);
				opener.grid.filters.getFilter('violationStatus.name').setActive(true, true);

				win.savedSinceOpen = true;
				if (win.closeOnSuccess) {
					win.closeWindow();
				}
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});


Clifton.collateral.generateMarginCallEmail = function(balance, componentScope) {
	const clientName = balance.holdingInvestmentAccount.businessClient.label;
	if (balance.collateralChangeAmount > 0 || balance.counterpartyCollateralChangeAmount < 0 || (balance.collateralChangeAmount === 0 && balance.counterpartyCollateralChangeAmount === 0)) {
		TCG.showError('Cannot email Margin Call for \'' + clientName + '\' that is not in Client\'s favor. Client Collateral Change must be negative or Counterparty Collateral Change must be positive.', 'Invalid Selection');
	}
	else {
		let contact, timing, toEmail;
		TCG.data.getDataPromise('securityUserCurrent.json', componentScope)
			.then(function(user) {
				return TCG.data.getDataPromise('businessContact.json?id=' + user.contactIdentifier, componentScope);
			})
			.then(function(c) {
				contact = c;
				return TCG.data.getDataPromise('businessContractPartyListFind.json?roleNameEquals=Collateral Call Recipient&contractId=' + balance.holdingInvestmentAccount.businessContract.id, componentScope);
			})
			.then(function(to) {
				toEmail = '';
				if (to && to.length > 0) {
					for (let j = 0; j < to.length; j++) {
						const toContact = to[j].contact;
						if (toContact && toContact.emailAddress) {
							if (toEmail.length > 0) {
								toEmail += '; ';
							}
							toEmail += toContact.emailAddress;
						}
					}
				}
				const contract = balance.holdingInvestmentAccount.businessContract;
				return TCG.data.getDataValuePromise('systemColumnCustomValue.json?columnGroupName=Contract Clauses&columnName=Transfer Timing&entityId=' + contract.id, componentScope);
			})
			.then(function(t) {
				timing = t || 'T+0';
				return Clifton.calendar.getBusinessDayFromDatePromise(TCG.parseDate(balance.balanceDate), parseInt(timing.substring(2, timing.length)));
			})
			.then(function(valueDate) {
				const ccEmail = 'MN-OTCCollateral@paraport.com';
				const counterpartyAbbreviation = balance.holdingInvestmentAccount.issuingCompany.abbreviation;
				const emailBody = '<!DOCTYPE html><html><head><style>p{font-family:arial,sans-serif;font-size:14px;}</style></head><body>'
					+ '<p>Please advise if you agree to the below margin call amount for the ' + clientName + ' account.</p>'
					+ '<p>Margin Call Amount (' + balance.holdingInvestmentAccount.baseCurrency.symbol + '): ' + TCG.numberFormat(balance.counterpartyCollateralChangeAmount - balance.collateralChangeAmount, '0,000.00')
					+ '<br />Value Date: ' + valueDate.format('m/d/Y') + '</p>'
					+ '<br />'
					+ '<p>If agreed, please send the collateral as one(two) separate movement(s):</p>'
					+ '<p>Client Collateral to Return: ' + TCG.numberFormat(balance.collateralChangeAmount < 0 ? balance.collateralChangeAmount : 0, '0,000.00')
					+ '<br />Counterparty Collateral to Post: ' + TCG.numberFormat(balance.counterpartyCollateralChangeAmount > 0 ? balance.counterpartyCollateralChangeAmount : 0, '0,000.00') + '</p>'
					+ '<p>Exposure/MTM: ' + TCG.numberFormat(balance.positionsMarketValue, '0,000.00') + '</p><br />'
					+ '<p>Thank you,</p>'
					+ '<p><b>' + contact.nameLabel + '</b><br />' + contact.title + '<br /><br /><b>O</b> ' + contact.phoneNumber + '<br />' + ccEmail + '</p>'
					+ '</body></html>';

				TCG.generateEmailFile('Parametric Margin Call: ' + clientName + ' - ' + counterpartyAbbreviation, 'text/html', emailBody, 'CollateralMarginCall.eml', toEmail, ccEmail);
			});
	}
};
