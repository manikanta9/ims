TCG.use('Clifton.collateral.BaseCollateralManagementWindow');
Clifton.collateral.REPOCollateralManagementWindow = Ext.extend(Clifton.collateral.BaseCollateralManagementWindow, {
	id: 'repoCollateralManagementWindow',
	iconCls: 'bank',
	title: 'REPO Collateral',
	width: 1300,
	collateralTypeName: 'REPO Collateral',
	additionalItems: [{
		title: 'REPO Collateral',
		items: [{
			xtype: 'gridpanel',
			name: 'collateralBalanceListFind',
			importTableName: 'CollateralBalance',
			instructions: 'Daily collateral information for selected filters.',
			getTopToolbarFilters: function(toolbar) {
				const filters = [];
				filters.push({fieldLabel: 'Client', xtype: 'combo', name: 'businessClientId', width: 180, url: 'businessClientListFind.json', linkedFilter: 'holdingInvestmentAccount.businessClient.label', displayField: 'label'});
				filters.push({fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'});
				return filters;
			},
			configureToolsMenu: function(menu) {
				const gridPanel = this;
				menu.add('-');
				menu.add({
					text: 'Preview Instruction Message',
					tooltip: 'Preview Instruction SWIFT Message',
					iconCls: 'shopping-cart',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						const collateral = sm.getSelections();
						if (sm.getCount() !== 1) {
							TCG.showError('Please select a single collateral to preview.', 'Incorrect Selection');
						}
						else {
							const id = collateral[0].id;
							Clifton.instruction.openInstructionPreview(id, 'CollateralBalance', gridPanel);
						}
					}
				});
			},
			getLoadParams: function(firstLoad) {
				const t = this.getTopToolbar();
				if (firstLoad) {
					const dd = this.getWindow().defaultData;
					let dateValue;
					if (dd && dd.balanceDate) { // balanceDate can be defaulted by the caller of window open
						dateValue = dd.balanceDate.format('m/d/Y');
					}
					else {
						// default balance date to previous business day
						dateValue = TCG.getPreviousWeekday();
					}
					TCG.getChildByName(t, 'balanceDate').setValue(dateValue);
					// TODO: support != filter: this.setFilterValue('positionsMarketValue', {'ne': 0});
				}
				return {
					collateralTypeName: this.getWindow().collateralTypeName,
					balanceDate: TCG.getChildByName(t, 'balanceDate').getValue().format('m/d/Y')
				};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Client', width: 200, dataIndex: 'holdingInvestmentAccount.businessClient.label', filter: {type: 'combo', searchFieldName: 'businessClientId', displayField: 'label', url: 'businessClientListFind.json'}},
				{header: 'Holding Account ID', width: 75, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
				{header: 'Holding Account', width: 150, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccountLabel'}},
				{header: 'Holding Company', width: 150, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},


				{header: 'Contract', width: 100, dataIndex: 'holdingInvestmentAccount.businessContract.name', hidden: true, filter: {type: 'combo', searchFieldName: 'businessContractId', url: 'businessContractListFind.json?contractTypeName=ISDA'}},
				{header: 'Positions Market Value', width: 90, dataIndex: 'postedCollateralValue', useNull: true, negativeInRed: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Positions Market Value (Original)', width: 90, dataIndex: 'positionsMarketValue', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Net Cash (Current)', width: 90, dataIndex: 'postedCollateralHaircutValue', useNull: true, negativeInRed: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Net Cash (Original)', width: 90, dataIndex: 'collateralRequirement', useNull: true, negativeInRed: true, type: 'currency'},

				{header: 'Difference', width: 90, dataIndex: 'excessCollateral', useNull: true, type: 'currency'},
				{header: 'Expected Transfer Amount', width: 90, dataIndex: 'collateralChangeAmount', useNull: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;'},
				{header: 'Transfer Amount', width: 90, dataIndex: 'transferAmount', hidden: true, useNull: true, type: 'currency', css: 'BORDER-LEFT: #c0c0c0 1px solid;'},

				// Contract Clauses
				{header: 'Threshold', dataIndex: 'thresholdAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Counterparty Threshold', width: 100, hidden: true, dataIndex: 'thresholdCounterpartyAmount', useNull: true, type: 'currency'},
				{header: 'Minimum Transfer Amount', dataIndex: 'minTransferAmount', width: 100, hidden: true, useNull: true, type: 'currency'},
				{header: 'Counterparty Minimum Transfer Amount', dataIndex: 'minTransferCounterpartyAmount', width: 100, hidden: true, useNull: true, type: 'currency'}

			],
			editor: {
				detailPageClass: 'Clifton.collateral.balance.BalanceWindow',
				drillDownOnly: true
			},
			plugins: [{ptype: 'collateral-balance-grideditor'}]
		}]
	}]
});
