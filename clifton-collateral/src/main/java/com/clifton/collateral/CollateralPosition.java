package com.clifton.collateral;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralPosition</code> class represents a collateral position at a specific point in time.
 *
 * @author vgomelsky
 */
@NonPersistentObject
public class CollateralPosition {

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;
	private AccountingAccount accountingAccount; // must have isCollateral() == true
	private InvestmentSecurity security;

	/**
	 * By default null but can be used to store the date the position was queried for.
	 */
	private Date positionDate;
	/**
	 * If base currency of client account is different than this of security then FX is likely not 1.
	 */
	private BigDecimal exchangeRateToBase = BigDecimal.ONE;
	/**
	 * For "Position" collateral, the number of units (shares/contracts/etc.) held.
	 */
	private BigDecimal quantity;
	/**
	 * For "Position" collateral, the cost per unit of security.
	 */
	private BigDecimal costPrice;
	/**
	 * For "Position" collateral, the market price of security.
	 */
	private BigDecimal marketPrice;

	private BigDecimal collateralMarketValue;

	/**
	 * Haircut is used to adjust collateral amount based on worthiness of the underlying security.
	 * adjustedCollateralAmount = collateralAmount * (1 - haircut)
	 * For example, haircut for Cash is 0, haircut for treasuries could be 0.05 and haircut for stocks could be 0.3
	 */
	private BigDecimal haircut = BigDecimal.ZERO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CollateralPosition forAccountingPosition(AccountingPosition position) {
		CollateralPosition collateral = new CollateralPosition();
		collateral.setAccountingAccount(position.getAccountingAccount());
		collateral.setClientAccount(position.getClientInvestmentAccount());
		collateral.setHoldingAccount(position.getHoldingInvestmentAccount());
		collateral.setSecurity(position.getInvestmentSecurity());
		collateral.setCostPrice(position.getPrice());
		collateral.setQuantity(position.getRemainingQuantity());
		return collateral;
	}


	public boolean isPosition() {
		return this.accountingAccount.isPosition();
	}


	public BigDecimal getNotional() {
		if (getQuantity() != null) {
			return getQuantity();
		}
		return getCollateralMarketValue();
	}


	public String getDirection() {
		if (MathUtils.isNegative(getCollateralMarketValue())) {
			return "Collected";
		}
		return "Posted";
	}


	/**
	 * Calculates and returns market value of collateral adjusted for the haircut.
	 */
	public BigDecimal getCollateralMarketValueAdjusted() {
		return MathUtils.getValueAfterHaircut(this.collateralMarketValue, this.haircut, false);
	}


	public BigDecimal getCollateralMarketValueLocal() {
		return MathUtils.divide(getCollateralMarketValue(), getExchangeRateToBase());
	}


	public BigDecimal getCollateralMarketValueLocalAdjusted() {
		return MathUtils.getValueAfterHaircut(getCollateralMarketValueLocal(), this.haircut, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getHaircut() {
		return this.haircut;
	}


	public void setHaircut(BigDecimal haircut) {
		this.haircut = haircut;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getCostPrice() {
		return this.costPrice;
	}


	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getCollateralMarketValue() {
		return this.collateralMarketValue;
	}


	public void setCollateralMarketValue(BigDecimal collateralMarketValue) {
		this.collateralMarketValue = collateralMarketValue;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}
}
