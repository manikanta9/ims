package com.clifton.collateral;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.search.CollateralConfigurationSearchForm;
import com.clifton.collateral.search.CollateralTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralService</code> interface defines methods for working with collateral.
 *
 * @author vgomelsky
 */
public interface CollateralService {

	public static final int DAYS_IN_YEAR = 365;


	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<CollateralPosition> getCollateralPositionListByDateAndAccountGroup(Date date, Integer investmentAccountGroupId);


	/**
	 * Returns the total market value of collateral adjusted for the haircut for the specified arguments.
	 * The value returned is in the base currency of the specified client account.
	 *
	 * @param clientAccountId
	 * @param holdingAccountId
	 * @param balanceDate
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getCollateralMarketValue(Integer clientAccountId, Integer holdingAccountId, Date balanceDate);


	/**
	 * Returns detail collateral position List for the specified arguments.
	 */
	@RequestMapping("collateralPositionList")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<CollateralPosition> getCollateralPositionList(AccountingPositionCommand command);


	/**
	 * Returns aggregated detail collateral position List for the specified arguments.
	 */
	@RequestMapping("collateralAggregatePositionList")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<CollateralPosition> getCollateralAggregatePositionList(AccountingPositionCommand command);


	/**
	 * Returns detail collateral position List for the specified arguments.
	 * <p>
	 * NOTE: Uses positionList passed in and doesn't do another lookup to get the positions.
	 *
	 * @param positionList
	 */
	@DoNotAddRequestMapping
	public List<CollateralPosition> getCollateralPositionListUsingPositions(AccountingPositionCommand command, List<AccountingPosition> positionList);


	////////////////////////////////////////////////////////////////////////////
	/////////         Collateral Type Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralType getCollateralType(short id);


	public CollateralType getCollateralTypeByAccount(InvestmentAccount holdingAccount);


	public CollateralType getCollateralTypeByName(String name);


	public List<CollateralType> getCollateralTypeListAll();


	public List<CollateralType> getCollateralTypeList(CollateralTypeSearchForm searchForm);


	public CollateralType saveCollateralType(CollateralType collateralType);


	public void deleteCollateralType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////         Collateral Configuration Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralConfiguration getCollateralConfiguration(short id);


	public CollateralConfiguration getCollateralConfigurationByTypeAndHoldingAccount(CollateralType collateralType, InvestmentAccount holdingAccount);


	public CollateralConfiguration getCollateralConfigurationByTypeAndCollateralAccount(CollateralType collateralType, InvestmentAccount collateralAccount);


	public CollateralConfiguration getCollateralConfigurationByType(CollateralType collateralType);


	public CollateralConfiguration getCollateralConfigurationByTypeAndHoldingCompany(CollateralType collateralType, BusinessCompany holdingCompany);


	public CollateralConfiguration getCollateralConfigurationByTypeAndCollateralCompany(CollateralType collateralType, BusinessCompany collateralCompany);


	public List<CollateralConfiguration> getCollateralConfigurationListAll();


	public List<CollateralConfiguration> getCollateralConfigurationList(CollateralConfigurationSearchForm searchForm);


	public CollateralConfiguration saveCollateralConfiguration(CollateralConfiguration collateralConfiguration);


	public void deleteCollateralConfiguration(short id);
}
