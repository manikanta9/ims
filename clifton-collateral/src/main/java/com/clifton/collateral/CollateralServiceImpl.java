package com.clifton.collateral;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.haircut.CollateralHaircut;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.collateral.search.CollateralConfigurationSearchForm;
import com.clifton.collateral.search.CollateralTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CollateralServiceImpl</code> class provides basic implementation of CollateralService interface.
 *
 * @author vgomelsky
 */
@Service
public class CollateralServiceImpl implements CollateralService {

	private AdvancedUpdatableDAO<CollateralType, Criteria> collateralTypeDAO;
	private AdvancedUpdatableDAO<CollateralConfiguration, Criteria> collateralConfigurationDAO;

	private AccountingBalanceService accountingBalanceService;
	private AccountingAccountService accountingAccountService;
	private AccountingPositionHandler accountingPositionHandler;
	private AccountingPositionService accountingPositionService;
	private CollateralBalanceService collateralBalanceService;

	private DaoNamedEntityCache<CollateralType> collateralTypeCache;

	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService investmentAccountService;
	private SystemBeanService systemBeanService;

	private MarketDataRetriever marketDataRetriever;
	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////         Collateral Position Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	/**
	 * This method is used for the Select Service Call TriResolveExportQuery It is not used in the ui or called internally.
	 */
	public List<CollateralPosition> getCollateralPositionListByDateAndAccountGroup(Date date, Integer investmentAccountGroupId) {
		ValidationUtils.assertNotNull(date, "A date must be set");
		ValidationUtils.assertNotNull(investmentAccountGroupId, "Account Group is a Required field and must be provided.");
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setInvestmentAccountGroupId(investmentAccountGroupId);
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		List<CollateralPosition> extendedPositionList = new ArrayList<>();
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(investmentAccountList)) {
			// Need to use settlement date for positions and transaction date for collateral.
			AccountingPositionCommand command = new AccountingPositionCommand();
			command.setSettlementDate(date);
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
			command = new AccountingPositionCommand();
			command.setTransactionDate(date);
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			List<CollateralPosition> collateralPositionList = getCollateralPositionListUsingPositions(command, positionList);
			List<CollateralPosition> aggregatedPositionList = new ArrayList<>();
			if (!CollectionUtils.isEmpty(collateralPositionList)) {
				aggregatedPositionList = aggregateCollateralPositions(collateralPositionList);
			}
			else if (!CollectionUtils.isEmpty(positionList)) {
				aggregatedPositionList.add(generateEmptyCollateralPosition(holdingAccount, positionList.get(0).getClientInvestmentAccount()));
			}
			else {
				//Because of CLS accounts we need to use the collateralPosition service but we try to avoid if we can.
				List<AccountingPositionDaily> accountingPositionList = getCollateralBalanceService().getCollateralAccountingPositionDailyLiveList("OTC Collateral", holdingAccount.getId(), date, false);
				if (!CollectionUtils.isEmpty(accountingPositionList)) {
					aggregatedPositionList.add(generateEmptyCollateralPosition(holdingAccount, accountingPositionList.get(0).getClientInvestmentAccount()));
				}
			}
			for (CollateralPosition collateralPosition : CollectionUtils.getIterable(aggregatedPositionList)) {
				collateralPosition.setPositionDate(date);
				extendedPositionList.add(collateralPosition);
			}
		}
		return extendedPositionList;
	}


	private CollateralPosition generateEmptyCollateralPosition(InvestmentAccount holdingAccount, InvestmentAccount clientAccount) {
		CollateralPosition emptyPosition = new CollateralPosition();
		emptyPosition.setHoldingAccount(holdingAccount);
		emptyPosition.setClientAccount(clientAccount);
		emptyPosition.setSecurity(holdingAccount.getBaseCurrency());
		emptyPosition.setMarketPrice(BigDecimal.ZERO);
		emptyPosition.setCollateralMarketValue(BigDecimal.ZERO);
		emptyPosition.setHaircut(BigDecimal.ZERO);
		return emptyPosition;
	}


	@Override
	public BigDecimal getCollateralMarketValue(Integer clientAccountId, Integer holdingAccountId, Date balanceDate) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(balanceDate);
		command.setClientInvestmentAccountId(clientAccountId);
		command.setHoldingInvestmentAccountId(holdingAccountId);
		List<CollateralPosition> collateralList = getCollateralPositionList(command);
		return CoreMathUtils.sumProperty(collateralList, CollateralPosition::getCollateralMarketValueAdjusted);
	}


	@Override
	//This would normally map to 'collateralPositionListFind' because the command objects extends a search form.
	//but in this case we want to return as an array list, not paging array, so we want to use List instead of ListFind.
	public List<CollateralPosition> getCollateralPositionList(AccountingPositionCommand command) {
		command.setCollateral(true);
		//TODO this should be changed or collateral positions should get its own command.
		if (command.getSettlementDate() == null) {
			command.setSettlementDate(command.getTransactionDate());
			command.setTransactionDate(null);
		}
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		return getCollateralPositionListUsingPositions(command, positionList);
	}


	@Override
	public List<CollateralPosition> getCollateralAggregatePositionList(AccountingPositionCommand command) {
		List<CollateralPosition> positionList = getCollateralPositionList(command);
		// BigDecimal equals not compareTo
		return aggregateCollateralPositions(positionList);
	}


	private List<CollateralPosition> aggregateCollateralPositions(List<CollateralPosition> positionList) {
		return CollectionUtils.getDistinct(positionList, p -> Arrays.asList(p.getClientAccount(), p.getHoldingAccount(), p.getSecurity(), p.getHaircut(), p.getDirection()),
				(a, b) -> {
					a.setQuantity(MathUtils.add(a.getQuantity(), b.getQuantity()));
					a.setCollateralMarketValue(MathUtils.add(a.getCollateralMarketValue(), b.getCollateralMarketValue()));
					return a;
				}
		);
	}


	@Override
	public List<CollateralPosition> getCollateralPositionListUsingPositions(AccountingPositionCommand command, List<AccountingPosition> positionList) {
		List<CollateralPosition> collateralPositionList = new ArrayList<>();
		// get cash collateral accounts
		List<AccountingAccount> cashCollateralAccountList = getCashCollateralAccountList(command);
		ValidationUtils.assertNotEmpty(cashCollateralAccountList, "Cannot find Cash Collateral GL Accounts");
		for (AccountingAccount cashCollateralAccount : CollectionUtils.getIterable(cashCollateralAccountList)) {

			BigDecimal cashCollateral = getCashCollateralFromAccount(cashCollateralAccount, command);
			if (!MathUtils.isNullOrZero(cashCollateral)) {
				CollateralPosition collateral = new CollateralPosition();
				collateral.setAccountingAccount(cashCollateralAccount);
				collateral.setCollateralMarketValue(cashCollateral);
				collateral.setHoldingAccount(getInvestmentAccountService().getInvestmentAccount(command.getHoldingInvestmentAccountId()));
				if (command.getClientInvestmentAccountId() != null) {
					collateral.setClientAccount(getInvestmentAccountService().getInvestmentAccount(command.getClientInvestmentAccountId()));
					collateral.setSecurity(collateral.getClientAccount().getBaseCurrency());
				}
				else {
					collateral.setSecurity(collateral.getHoldingAccount().getBaseCurrency());
				}
				collateralPositionList.add(collateral);
			}
		}

		Map<String, CollateralHaircut> haircutMap = new HashMap<>();
		// get position collateral (TODO: make it more efficient by getting collateral positions only)
		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			if (position.getAccountingAccount().isCollateral() && AccountingAccountType.ASSET.equals(position.getAccountingAccount().getAccountType().getName())
					&& !position.getAccountingAccount().isReceivable()) {

				boolean isCounterpartyHaircut = position.getAccountingAccount().isNotOurAccount();

				CollateralPosition collateral = CollateralPosition.forAccountingPosition(position);

				CollateralHaircut haircut = null;
				//Do not apply haircuts to positions in 'Escrow Receipt' accounts.
				if (!InvestmentAccountType.ESCROW_RECEIPT.equals(position.getHoldingInvestmentAccount().getType().getName())) {
					String haircutKey = BeanUtils.getBeanIdentity(collateral.getHoldingAccount()) + "_" +
							BeanUtils.getBeanIdentity(collateral.getSecurity()) + "_" + (isCounterpartyHaircut ? "1" : "0");
					haircut = haircutMap.get(haircutKey);
					if (haircut == null) {
						haircut = getCollateralHaircutDefinitionService().getCollateralHaircut(collateral.getHoldingAccount(), collateral.getSecurity(), isCounterpartyHaircut, command.getTransactionOrSettlementDate());
						haircutMap.put(haircutKey, haircut);
					}
				}
				BigDecimal marketPrice = getMarketDataRetriever().getPriceFlexible(collateral.getSecurity(), command.getTransactionOrSettlementDate(), true);
				collateral.setMarketPrice(marketPrice);
				collateral.setHaircut((haircut == null) ? BigDecimal.ZERO : haircut.getHaircutPercent());
				collateral.setCollateralMarketValue(getAccountingPositionHandler().getAccountingPositionMarketValue(position, command.getTransactionOrSettlementDate()));
				collateral.setExchangeRateToBase(position.getExchangeRateToBase());
				collateralPositionList.add(collateral);
			}
		}

		//For all types except OTC Collateral we want to load positions from related accounts
		if (!getInvestmentAccountGroupService().isInvestmentAccountInGroup(command.getHoldingInvestmentAccountId(), "OTC Collateral Accounts")) {
			InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
			searchForm.setMainAccountId(command.getHoldingInvestmentAccountId());
			List<InvestmentAccountRelationship> relAccountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm);
			for (InvestmentAccountRelationship rel : CollectionUtils.getIterable(relAccountList)) {
				if (rel.isActive() && InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME.equals(rel.getPurpose().getName())) {
					command = AccountingPositionCommand.onPositionTransactionDate(command.getTransactionOrSettlementDate());
					command.setHoldingInvestmentAccountId(rel.getReferenceTwo().getId());
					command.setExcludeNotOurGLAccounts(true);
					collateralPositionList.addAll(getCollateralPositionList(command));
				}
			}
		}
		return collateralPositionList;
	}


	private List<AccountingAccount> getCashCollateralAccountList(AccountingPositionCommand command) {
		AccountingAccountSearchForm accountingAccountSearchForm = new AccountingAccountSearchForm();
		accountingAccountSearchForm.setCash(true);
		accountingAccountSearchForm.setCollateral(true);
		accountingAccountSearchForm.setPosition(false);
		accountingAccountSearchForm.setAccountType(AccountingAccountType.ASSET);
		accountingAccountSearchForm.setReceivable(false);
		if (command.getExcludeNotOurGLAccounts() != null && command.getExcludeNotOurGLAccounts()) {
			accountingAccountSearchForm.setNotOurAccount(false);
		}
		return getAccountingAccountService().getAccountingAccountList(accountingAccountSearchForm);
	}


	private BigDecimal getCashCollateralFromAccount(AccountingAccount cashCollateralAccount, AccountingPositionCommand command) {
		AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
		searchForm.setAccountingAccountId(cashCollateralAccount.getId());
		searchForm.setClientAccountId(command.getClientInvestmentAccountId());
		searchForm.setHoldingAccountId(command.getHoldingInvestmentAccountId());
		searchForm.setSettlementDate(command.getSettlementDate());
		searchForm.setTransactionDate(command.getTransactionDate());
		return getAccountingBalanceService().getAccountingAccountBalanceBase(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////         Collateral Type Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralType getCollateralType(short id) {
		return getCollateralTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public CollateralType getCollateralTypeByAccount(InvestmentAccount holdingAccount) {
		CollateralType typeByAccount = null;
		List<CollateralType> collateralTypeList = getCollateralTypeListAll();
		for (CollateralType collateralType : CollectionUtils.getIterable(collateralTypeList)) {
			if (getInvestmentAccountGroupService().isInvestmentAccountInGroup(holdingAccount.getId(), collateralType.getAccountGroup().getName())) {
				typeByAccount = collateralType;
				break;
			}
		}
		return typeByAccount;
	}


	@Override
	public CollateralType getCollateralTypeByName(String name) {
		return getCollateralTypeCache().getBeanForKeyValueStrict(getCollateralTypeDAO(), name);
	}


	@Override
	public List<CollateralType> getCollateralTypeListAll() {
		return getCollateralTypeDAO().findAll();
	}


	@Override
	public List<CollateralType> getCollateralTypeList(CollateralTypeSearchForm searchForm) {
		return getCollateralTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public CollateralType saveCollateralType(CollateralType collateralType) {
		return getCollateralTypeDAO().save(collateralType);
	}


	@Override
	public void deleteCollateralType(short id) {
		getCollateralTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Collateral Configuration Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralConfiguration getCollateralConfiguration(short id) {
		CollateralConfiguration collateralConfiguration = getCollateralConfigurationDAO().findByPrimaryKey(id);
		if (collateralConfiguration != null && collateralConfiguration.getRebuildProcessorBean() != null) {
			collateralConfiguration.setRebuildProcessorBean(getSystemBeanService().getSystemBean(collateralConfiguration.getRebuildProcessorBean().getId()));
		}
		return collateralConfiguration;
	}


	@Override
	public CollateralConfiguration getCollateralConfigurationByTypeAndHoldingAccount(CollateralType collateralType, InvestmentAccount holdingAccount) {
		CollateralConfiguration configuration = null;
		List<CollateralConfiguration> collateralConfigurationList = getCollateralConfigurationListAll();
		for (CollateralConfiguration collateralConfiguration : CollectionUtils.getIterable(collateralConfigurationList)) {
			if (collateralType.equals(collateralConfiguration.getCollateralType()) && holdingAccount.equals(collateralConfiguration.getHoldingAccount())) {
				configuration = collateralConfiguration;
				break;
			}
		}
		return configuration;
	}


	@Override
	public CollateralConfiguration getCollateralConfigurationByTypeAndCollateralAccount(CollateralType collateralType, InvestmentAccount collateralAccount) {
		CollateralConfiguration configuration = null;
		List<CollateralConfiguration> collateralConfigurationList = getCollateralConfigurationListAll();
		for (CollateralConfiguration collateralConfiguration : CollectionUtils.getIterable(collateralConfigurationList)) {
			if (collateralType.equals(collateralConfiguration.getCollateralType()) && collateralAccount.equals(collateralConfiguration.getCollateralAccount())) {
				configuration = collateralConfiguration;
				break;
			}
		}
		return configuration;
	}


	@Override
	public CollateralConfiguration getCollateralConfigurationByType(CollateralType collateralType) {
		CollateralConfiguration configuration = null;
		List<CollateralConfiguration> collateralConfigurationList = getCollateralConfigurationListAll();
		for (CollateralConfiguration collateralConfiguration : CollectionUtils.getIterable(collateralConfigurationList)) {
			if (collateralType.equals(collateralConfiguration.getCollateralType())) {
				configuration = collateralConfiguration;
				break;
			}
		}
		return configuration;
	}


	@Override
	public CollateralConfiguration getCollateralConfigurationByTypeAndHoldingCompany(CollateralType collateralType, BusinessCompany holdingCompany) {
		CollateralConfiguration configuration = null;
		List<CollateralConfiguration> collateralConfigurationList = getCollateralConfigurationListAll();
		for (CollateralConfiguration collateralConfiguration : CollectionUtils.getIterable(collateralConfigurationList)) {
			if (collateralType.equals(collateralConfiguration.getCollateralType()) && holdingCompany.equals(collateralConfiguration.getHoldingCompany())) {
				configuration = collateralConfiguration;
				break;
			}
		}
		return configuration;
	}


	@Override
	public CollateralConfiguration getCollateralConfigurationByTypeAndCollateralCompany(CollateralType collateralType, BusinessCompany collateralCompany) {
		CollateralConfiguration configuration = null;
		List<CollateralConfiguration> collateralConfigurationList = getCollateralConfigurationListAll();
		for (CollateralConfiguration collateralConfiguration : CollectionUtils.getIterable(collateralConfigurationList)) {
			if (collateralType.equals(collateralConfiguration.getCollateralType()) && collateralCompany.equals(collateralConfiguration.getCollateralCompany())) {
				configuration = collateralConfiguration;
				break;
			}
		}
		return configuration;
	}


	@Override
	public List<CollateralConfiguration> getCollateralConfigurationListAll() {
		return getCollateralConfigurationDAO().findAll();
	}


	@Override
	public List<CollateralConfiguration> getCollateralConfigurationList(CollateralConfigurationSearchForm searchForm) {
		return getCollateralConfigurationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public CollateralConfiguration saveCollateralConfiguration(CollateralConfiguration collateralConfiguration) {
		if (collateralConfiguration.getRebuildProcessorBean() != null) {
			//make bean name unique
			String name = "CollateralType (" + collateralConfiguration.getCollateralType().getId() + ") : ";
			if (collateralConfiguration.getHoldingCompany() != null) {
				name += "HoldingCompany (" + collateralConfiguration.getHoldingCompany().getId() + ")";
			}
			else if (collateralConfiguration.getHoldingAccount() != null) {
				name += "HoldingAccount (" + collateralConfiguration.getHoldingAccount().getId() + ")";
			}
			name += " : " + new Date().toInstant().getEpochSecond();
			collateralConfiguration.getRebuildProcessorBean().setName(name);
			getSystemBeanService().saveSystemBean(collateralConfiguration.getRebuildProcessorBean());
		}
		return getCollateralConfigurationDAO().save(collateralConfiguration);
	}


	@Override
	public void deleteCollateralConfiguration(short id) {
		getCollateralConfigurationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AdvancedUpdatableDAO<CollateralType, Criteria> getCollateralTypeDAO() {
		return this.collateralTypeDAO;
	}


	public void setCollateralTypeDAO(AdvancedUpdatableDAO<CollateralType, Criteria> collateralTypeDAO) {
		this.collateralTypeDAO = collateralTypeDAO;
	}


	public DaoNamedEntityCache<CollateralType> getCollateralTypeCache() {
		return this.collateralTypeCache;
	}


	public void setCollateralTypeCache(DaoNamedEntityCache<CollateralType> collateralTypeCache) {
		this.collateralTypeCache = collateralTypeCache;
	}


	public CollateralHaircutDefinitionService getCollateralHaircutDefinitionService() {
		return this.collateralHaircutDefinitionService;
	}


	public void setCollateralHaircutDefinitionService(CollateralHaircutDefinitionService collateralHaircutDefinitionService) {
		this.collateralHaircutDefinitionService = collateralHaircutDefinitionService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public AdvancedUpdatableDAO<CollateralConfiguration, Criteria> getCollateralConfigurationDAO() {
		return this.collateralConfigurationDAO;
	}


	public void setCollateralConfigurationDAO(AdvancedUpdatableDAO<CollateralConfiguration, Criteria> collateralConfigurationDAO) {
		this.collateralConfigurationDAO = collateralConfigurationDAO;
	}
}
