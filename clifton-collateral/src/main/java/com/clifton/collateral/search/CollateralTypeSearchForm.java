package com.clifton.collateral.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author theodorez
 */
public class CollateralTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String collateralTypeName;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String collateralTypeNameEquals;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] collateralTypeNames;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getCollateralTypeName() {
		return this.collateralTypeName;
	}


	public void setCollateralTypeName(String collateralTypeName) {
		this.collateralTypeName = collateralTypeName;
	}


	public String getCollateralTypeNameEquals() {
		return this.collateralTypeNameEquals;
	}


	public void setCollateralTypeNameEquals(String collateralTypeNameEquals) {
		this.collateralTypeNameEquals = collateralTypeNameEquals;
	}


	public String[] getCollateralTypeNames() {
		return this.collateralTypeNames;
	}


	public void setCollateralTypeNames(String[] collateralTypeNames) {
		this.collateralTypeNames = collateralTypeNames;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
