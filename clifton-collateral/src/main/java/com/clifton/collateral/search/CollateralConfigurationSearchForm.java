package com.clifton.collateral.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author stevenf
 */
public class CollateralConfigurationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "collateralType.id")
	private Short[] collateralTypeId;

	@SearchField(searchField = "name", searchFieldPath = "collateralType", comparisonConditions = ComparisonConditions.EQUALS)
	private String[] collateralTypeName;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "name", searchFieldPath = "holdingAccount")
	private String holdingAccountName;

	@SearchField(searchField = "holdingCompany.id")
	private Integer holdingCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "holdingCompany")
	private String holdingCompanyName;

	@SearchField(searchField = "rebuildProcessorBean.id")
	private Integer rebuildProcessorBeanId;

	@SearchField(searchField = "name", searchFieldPath = "rebuildProcessorBean")
	private String rebuildProcessorBeanName;


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Short[] getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(Short[] collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public String[] getCollateralTypeName() {
		return this.collateralTypeName;
	}


	public void setCollateralTypeName(String[] collateralTypeName) {
		this.collateralTypeName = collateralTypeName;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public String getHoldingAccountName() {
		return this.holdingAccountName;
	}


	public void setHoldingAccountName(String holdingAccountName) {
		this.holdingAccountName = holdingAccountName;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public String getHoldingCompanyName() {
		return this.holdingCompanyName;
	}


	public void setHoldingCompanyName(String holdingCompanyName) {
		this.holdingCompanyName = holdingCompanyName;
	}


	public Integer getRebuildProcessorBeanId() {
		return this.rebuildProcessorBeanId;
	}


	public void setRebuildProcessorBeanId(Integer rebuildProcessorBeanId) {
		this.rebuildProcessorBeanId = rebuildProcessorBeanId;
	}


	public String getRebuildProcessorBeanName() {
		return this.rebuildProcessorBeanName;
	}


	public void setRebuildProcessorBeanName(String rebuildProcessorBeanName) {
		this.rebuildProcessorBeanName = rebuildProcessorBeanName;
	}
}
