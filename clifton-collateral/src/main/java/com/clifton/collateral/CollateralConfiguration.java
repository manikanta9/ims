package com.clifton.collateral;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.bean.SystemBean;


/**
 * The CollateralConfiguration class is used to customize collateral calculation/rebuild logic by holding/collateral company or account.
 * Collateral account is looked up for the holding account based on corresponding relationship purpose(s).
 * The logic uses specificity with Collateral Account being most specific and Holding Company being less specific.
 *
 * @author StevenF
 */
public class CollateralConfiguration extends BaseEntity<Short> implements LabeledObject {

	// specificity for rebuild bean scope in specificity order: collateralAccount is most specific.
	private CollateralType collateralType;

	private BusinessCompany holdingCompany;
	private InvestmentAccount holdingAccount;
	private BusinessCompany collateralCompany;
	private InvestmentAccount collateralAccount;


	private SystemBean rebuildProcessorBean;

	private boolean systemDefined;


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		if (isSystemDefined()) {
			label.append("System Defined ");
		}
		label.append(getCollateralType().getName()).append("Configuration");
		if (getHoldingCompany() != null || getHoldingAccount() != null || getCollateralCompany() != null || getCollateralAccount() != null) {
			label.append(" for ");
			String andString = " and ";
			if (getHoldingCompany() != null) {
				label.append(getHoldingCompany().getLabel()).append(andString);
			}
			if (getHoldingAccount() != null) {
				label.append(getHoldingAccount().getLabelShort()).append(andString);
			}
			if (getCollateralCompany() != null) {
				label.append(getCollateralCompany().getLabel()).append(andString);
			}
			if (getCollateralAccount() != null) {
				label.append(getCollateralAccount().getLabelShort()).append(andString);
			}
			label.delete(label.lastIndexOf(andString), label.length());
		}
		return label.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public CollateralType getCollateralType() {
		return this.collateralType;
	}


	public void setCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public InvestmentAccount getCollateralAccount() {
		return this.collateralAccount;
	}


	public void setCollateralAccount(InvestmentAccount collateralAccount) {
		this.collateralAccount = collateralAccount;
	}


	public BusinessCompany getCollateralCompany() {
		return this.collateralCompany;
	}


	public void setCollateralCompany(BusinessCompany collateralCompany) {
		this.collateralCompany = collateralCompany;
	}


	public SystemBean getRebuildProcessorBean() {
		return this.rebuildProcessorBean;
	}


	public void setRebuildProcessorBean(SystemBean rebuildProcessorBean) {
		this.rebuildProcessorBean = rebuildProcessorBean;
	}
}
