package com.clifton.collateral.validation;

import com.clifton.collateral.CollateralConfiguration;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>CollateralConfigurationValidator</code> validates changes to system defined Collateral Configurations
 *
 * @author stevenf
 */
@Component
public class CollateralConfigurationValidator extends SelfRegisteringDaoValidator<CollateralConfiguration> {

	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(CollateralConfiguration bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(CollateralConfiguration collateralConfiguration, DaoEventTypes config, ReadOnlyDAO<CollateralConfiguration> collateralConfigurationDAO) throws ValidationException {
		if (collateralConfiguration.isSystemDefined()) {
			if (config.isInsert() || config.isDelete()) {
				throw new ValidationException("System Defined Collateral Configurations cannot be " + (config.isInsert() ? "added." : "deleted."));
			}
			else if (config.isUpdate()) {
				boolean admin = getSecurityAuthorizationService().isSecurityUserAdmin();
				ValidationUtils.assertTrue(admin, "Only Administrators are allowed to update system defined collateral configurations.");
			}
		}

		if (config.isInsert() || config.isUpdate()) {
			//Locate existing assignments
			List<CollateralConfiguration> existingCollateralConfigurationList =
					collateralConfigurationDAO.findByFields(
							new String[]{"collateralType.id", "holdingCompany.id", "holdingAccount.id", "collateralCompany.id", "collateralAccount.id"},
							new Object[]{
									collateralConfiguration.getCollateralType().getId(),
									collateralConfiguration.getHoldingCompany() != null ? collateralConfiguration.getHoldingCompany().getId() : null,
									collateralConfiguration.getHoldingAccount() != null ? collateralConfiguration.getHoldingAccount().getId() : null,
									collateralConfiguration.getCollateralCompany() != null ? collateralConfiguration.getCollateralCompany().getId() : null,
									collateralConfiguration.getCollateralAccount() != null ? collateralConfiguration.getCollateralAccount().getId() : null
							});
			if (CollectionUtils.getSize(existingCollateralConfigurationList) > 0) {
				//If there is only one item and it has the same id, then skip validation
				if (config.isInsert() || (CollectionUtils.getSize(existingCollateralConfigurationList) == 1 && collateralConfiguration.getId().shortValue() != CollectionUtils.getFirstElementStrict(existingCollateralConfigurationList).getId().shortValue())) {
					String validationMessage = "A collateral configuration for this Collateral Type (" + collateralConfiguration.getCollateralType().getName() + ")";
					if (collateralConfiguration.getHoldingCompany() != null) {
						validationMessage += " and Holding Company (" + collateralConfiguration.getHoldingCompany().getLabel() + ")";
					}
					if (collateralConfiguration.getHoldingAccount() != null) {
						validationMessage += " and Holding Account (" + collateralConfiguration.getHoldingAccount().getLabel() + ")";
					}
					if (collateralConfiguration.getCollateralCompany() != null) {
						validationMessage += " and Collateral Company (" + collateralConfiguration.getCollateralCompany().getLabel() + ")";
					}
					if (collateralConfiguration.getCollateralAccount() != null) {
						validationMessage += " and Collateral Account (" + collateralConfiguration.getCollateralAccount().getLabel() + ")";
					}
					validationMessage += " already exists.";
					throw new ValidationException(validationMessage);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
