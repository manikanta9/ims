package com.clifton.collateral.haircut;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>CollateralHaircutDefinition</code> class defines haircuts that should be applied to market value of collateral positions.
 * For example, treasuries are riskier than cash and may get 5% haircut while equities may get 30% haircut.
 * <p>
 * Most specific haircuts are applied first in the following order:
 * - ISDA
 * - holdingAccount
 * - holdingCompany
 * - neither holdingAccount or holdingCompany
 * <p>
 * - investmentInstrument, then instrumentHierarchy, then investmentTypeSubTye2, then, investmentTypeSubType, then investmentType
 *
 * @author vgomelsky
 */
public class CollateralHaircutDefinition extends BaseEntity<Integer> {

	private InvestmentAccount holdingAccount;
	private BusinessCompany holdingCompany;
	// If this haircut is defined for a specific ISDA
	private BusinessContract businessContract;

	private InvestmentType investmentType;
	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentTypeSubType2 investmentTypeSubType2;
	private InvestmentInstrumentHierarchy instrumentHierarchy;
	private InvestmentInstrument investmentInstrument;


	@OneToManyEntity(serviceBeanName = "collateralHaircutDefinitionService", serviceMethodName = "getCollateralHaircutListByDefinitionId")
	private List<CollateralHaircut> collateralHaircutList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public BusinessContract getBusinessContract() {
		return this.businessContract;
	}


	public void setBusinessContract(BusinessContract businessContract) {
		this.businessContract = businessContract;
	}


	public List<CollateralHaircut> getCollateralHaircutList() {
		return this.collateralHaircutList != null ? this.collateralHaircutList : new ArrayList<>();
	}


	public void setCollateralHaircutList(List<CollateralHaircut> collateralHaircutList) {
		this.collateralHaircutList = collateralHaircutList;
	}
}
