package com.clifton.collateral.haircut.cache;

import com.clifton.collateral.haircut.CollateralHaircutDefinition;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;


/**
 * The <class>CollateralHaircutDefinitionTargetHolder</class> is the object stored in the CollateralHaircutDefinitionSpecificityCache cache
 * and used to determine the most specific result.
 */
public class CollateralHaircutDefinitionTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final int id;
	private final SpecificityScope scope;


	public CollateralHaircutDefinitionTargetHolder(String key, CollateralHaircutDefinition haircutDefinition) {
		this.id = haircutDefinition.getId();
		this.key = key;
		this.scope = null;// new SimpleSpecificityScope(h);
	}


	@Override
	public SpecificityScope getScope() {
		return null;
	}


	public int getId() {
		return this.id;
	}


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.id;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CollateralHaircutDefinitionTargetHolder other = (CollateralHaircutDefinitionTargetHolder) obj;
		if (this.id != other.id) {
			return false;
		}
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		if (!this.scope.equals(other.scope)) {
			return false;
		}
		return true;
	}
}
