package com.clifton.collateral.haircut;


import com.clifton.collateral.haircut.search.CollateralHaircutDefinitionSearchForm;
import com.clifton.collateral.haircut.search.CollateralHaircutSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralHaircutDefinitionService</code> interface defines methods for working with collateral.
 *
 * @author stevenf
 */
public interface CollateralHaircutDefinitionService {

	public CollateralHaircutDefinition getCollateralHaircutDefinition(int id);


	public List<CollateralHaircutDefinition> getCollateralHaircutDefinitionList(CollateralHaircutDefinitionSearchForm searchForm);


	public CollateralHaircutDefinition saveCollateralHaircutDefinition(CollateralHaircutDefinition haircutDefinition);


	public void deleteCollateralHaircutDefinition(int id);


	public List<CollateralHaircut> getCollateralHaircutList(CollateralHaircutSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<CollateralHaircut> getCollateralHaircutListByDefinitionId(int definitionId);


	/**
	 * Returns haircut for the specified holding investment account and security.
	 * Finds the most specific haircut that matches argument criteria.
	 * Returns BigDecimal.ZERO if no haircut should be applied.
	 *
	 * @param holdingAccountId
	 * @param securityId
	 */
	@ResponseBody
	@SecureMethod(dtoClass = CollateralHaircutDefinition.class)
	public BigDecimal getCollateralHaircutValue(int holdingAccountId, int securityId, boolean isCounterpartyHaircut, Date balanceDate);


	@DoNotAddRequestMapping
	public BigDecimal getCollateralHaircutValue(InvestmentAccount holdingAccount, InvestmentSecurity security, boolean isCounterpartyHaircut, Date balanceDate);


	/**
	 * Returns CollateralHaircutDefinition for the specified holding investment account and security.
	 * Finds the most specific haircut that matches argument criteria.
	 *
	 * @param holdingAccountId
	 * @param securityId
	 */
	@DoNotAddRequestMapping
	public CollateralHaircut getCollateralHaircut(int holdingAccountId, int securityId, boolean isCounterpartyHaircut, Date balanceDate);


	@DoNotAddRequestMapping
	public CollateralHaircut getCollateralHaircut(InvestmentAccount holdingAccount, InvestmentSecurity security, boolean isCounterpartyHaircut, Date balanceDate);
}
