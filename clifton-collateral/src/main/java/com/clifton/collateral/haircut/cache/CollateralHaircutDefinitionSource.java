package com.clifton.collateral.haircut.cache;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>CollateralHaircutDefinitionSource</code> is used by the CollateralHaircutDefinitionSpecificityCache as the
 * target object to find the most specific result for.
 */
public class CollateralHaircutDefinitionSource {

	private final InvestmentAccount holdingAccount;
	private final InvestmentSecurity security;
	private final Integer securityDaysToMaturity;


	public CollateralHaircutDefinitionSource(InvestmentAccount holdingAccount, InvestmentSecurity security, Integer securityDaysToMaturity) {
		this.holdingAccount = holdingAccount;
		this.securityDaysToMaturity = securityDaysToMaturity;
		this.security = security;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public Integer getSecurityDaysToMaturity() {
		return this.securityDaysToMaturity;
	}
}
