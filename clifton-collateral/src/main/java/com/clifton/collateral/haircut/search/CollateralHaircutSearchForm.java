package com.clifton.collateral.haircut.search;


import com.clifton.collateral.CollateralService;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>CollateralHaircutSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralHaircutSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer startDaysToMaturity;

	@SearchField
	private Integer endDaysToMaturity;

	/**
	 * Will find all CollateralHaircutDefinition where (startDaysToMaturity is null or <= value and endDaysToMaturity is null or > endDaysToMaturity).
	 * Used to validate that there are not overlapping CollateralHaircuts.
	 */
	// custom search field
	private Integer startDaysToMaturityForValidation;

	/**
	 * Will find all CollateralHaircutDefinition where (startDaysToMaturity is null or < value and endDaysToMaturity is null or >= endDaysToMaturity).
	 * Used to validate that there are not overlapping CollateralHaircuts.
	 */
	// custom search field
	private Integer endDaysToMaturityForValidation;

	/**
	 * Will find all CollateralHaircutDefinition where startDaysToMaturity is null or <= value and endDaysToMaturity is null or > endDaysToMaturity.
	 * Can be used to find the applicable CollateralHaircuts give the days to maturity of a security.
	 */
	// custom search field
	private Integer securityDaysToMaturity;

	@SearchField
	private Boolean clientHaircut;

	@SearchField
	private Boolean counterpartyHaircut;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public Integer getStartYearsToMaturity() {
		return getStartDaysToMaturity() == null ? null : getStartDaysToMaturity() / CollateralService.DAYS_IN_YEAR;
	}


	public void setStartYearsToMaturity(Integer startYearsToMaturity) {
		setStartDaysToMaturity(startYearsToMaturity == null ? null : startYearsToMaturity * CollateralService.DAYS_IN_YEAR);
	}


	public Integer getEndYearsToMaturity() {
		return getEndDaysToMaturity() == null ? null : getEndDaysToMaturity() / CollateralService.DAYS_IN_YEAR;
	}


	public void setEndYearsToMaturity(Integer endYearsToMaturity) {
		setEndDaysToMaturity(endYearsToMaturity == null ? null : endYearsToMaturity * CollateralService.DAYS_IN_YEAR);
	}


	public Integer getSecurityYearsToMaturity() {
		return getSecurityDaysToMaturity() == null ? null : getSecurityDaysToMaturity() / CollateralService.DAYS_IN_YEAR;
	}


	public void setSecurityYearsToMaturity(Integer securityYearsToMaturity) {
		setSecurityDaysToMaturity(securityYearsToMaturity == null ? null : securityYearsToMaturity * CollateralService.DAYS_IN_YEAR);
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public Integer getStartDaysToMaturity() {
		return this.startDaysToMaturity;
	}


	public void setStartDaysToMaturity(Integer startDaysToMaturity) {
		this.startDaysToMaturity = startDaysToMaturity;
	}


	public Integer getEndDaysToMaturity() {
		return this.endDaysToMaturity;
	}


	public void setEndDaysToMaturity(Integer endDaysToMaturity) {
		this.endDaysToMaturity = endDaysToMaturity;
	}


	public Integer getSecurityDaysToMaturity() {
		return this.securityDaysToMaturity;
	}


	public void setSecurityDaysToMaturity(Integer securityDaysToMaturity) {
		this.securityDaysToMaturity = securityDaysToMaturity;
	}


	public Integer getStartDaysToMaturityForValidation() {
		return this.startDaysToMaturityForValidation;
	}


	public void setStartDaysToMaturityForValidation(Integer startDaysToMaturityForValidation) {
		this.startDaysToMaturityForValidation = startDaysToMaturityForValidation;
	}


	public Integer getEndDaysToMaturityForValidation() {
		return this.endDaysToMaturityForValidation;
	}


	public void setEndDaysToMaturityForValidation(Integer endDaysToMaturityForValidation) {
		this.endDaysToMaturityForValidation = endDaysToMaturityForValidation;
	}


	public Boolean getClientHaircut() {
		return this.clientHaircut;
	}


	public void setClientHaircut(Boolean clientHaircut) {
		this.clientHaircut = clientHaircut;
	}


	public Boolean getCounterpartyHaircut() {
		return this.counterpartyHaircut;
	}


	public void setCounterpartyHaircut(Boolean counterpartyHaircut) {
		this.counterpartyHaircut = counterpartyHaircut;
	}
}
