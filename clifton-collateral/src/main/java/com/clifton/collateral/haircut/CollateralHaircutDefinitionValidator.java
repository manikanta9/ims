package com.clifton.collateral.haircut;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>CollateralHaircutDefinitionValidator</code> validates that the CollateralHaircutDefinition is configured correctly before it is saved.
 *
 * @author stevenf
 */
@Component
public class CollateralHaircutDefinitionValidator extends SelfRegisteringDaoValidator<CollateralHaircutDefinition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(CollateralHaircutDefinition haircutDefinition, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		// Make sure not changing from Instant to Batched Notification
		if (haircutDefinition.getInstrumentHierarchy() != null) {
			ValidationUtils.assertNull(haircutDefinition.getInvestmentType(), "Investment Type or Hierarchy must be selected, but not more than one.", "investmentType.id");
			ValidationUtils.assertTrue(haircutDefinition.getInstrumentHierarchy().isAssignmentAllowed(), "The Hierarchy selected [" + haircutDefinition.getInstrumentHierarchy().getNameExpanded()
					+ "] does not allow assignments.  Please select a hierarchy that allows assignments only.", "instrumentHierarchy.id");
		}
		else if (haircutDefinition.getInvestmentType() != null) {
			ValidationUtils.assertNull(haircutDefinition.getInstrumentHierarchy(), "Investment Type or Hierarchy must be selected, but not more than one.", "instrumentHierarchy.id");
		}
		else {
			throw new ValidationException("Investment Type or Hierarchy must be selected");
		}
		validateHaircuts(haircutDefinition);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateHaircuts(CollateralHaircutDefinition haircutDefinition) {
		List<CollateralHaircut> haircutList = haircutDefinition.getCollateralHaircutList();
		for (CollateralHaircut haircut : haircutList) {
			if (haircut.getStartDaysToMaturity() == null && haircut.getEndDaysToMaturity() == null && haircutList.size() > 1) {
				throw new ValidationException("You may only define one haircut if there is not a start or end date defined.");
			}
			else {
				if ((haircut.getStartDaysToMaturity() != null) || (haircut.getEndDaysToMaturity() != null)) {
					if ((haircut.getStartDaysToMaturity() != null) && (haircut.getEndDaysToMaturity() != null)) {
						ValidationUtils.assertTrue(MathUtils.isGreaterThan(haircut.getEndDaysToMaturity(), haircut.getStartDaysToMaturity()),
								"End Days To Maturity entered [" + haircut.getEndDaysToMaturity() + "] must be larger than Start Days To Maturity [" +
										haircut.getStartDaysToMaturity() + "]");
					}
					ValidationUtils.assertTrue((haircut.getStartDaysToMaturity() == null) || haircut.getStartDaysToMaturity() >= 0,
							"Start Days To Maturity entered [" + haircut.getStartDaysToMaturity() + "] cannot be negative.  Please enter a value 0 or greater.", "daysToMaturity");
					ValidationUtils.assertTrue((haircut.getEndDaysToMaturity() == null) || haircut.getEndDaysToMaturity() >= 0,
							"End Days To Maturity entered [" + haircut.getEndDaysToMaturity() + "] cannot be negative.  Please enter a value 0 or greater.", "daysToMaturity");
				}
				ValidationUtils.assertTrue(MathUtils.isInRange(haircut.getHaircutPercent(), BigDecimal.ZERO, BigDecimal.ONE),
						"Haircut Value [" + CoreMathUtils.formatNumberDecimal(haircut.getHaircutPercent()) + "] is out of range.  Please enter a value between 0.00 and 1.00", "haircut");
				validateMaturityDatesDoNotMatchOrOverlap(haircut, haircutList);
			}
		}
	}


	private void validateMaturityDatesDoNotMatchOrOverlap(CollateralHaircut haircut, List<CollateralHaircut> collateralHaircutList) {
		int matchCount = 0;
		boolean overlappingMaturity = false;
		Integer startDays = haircut.getStartDaysToMaturity();
		Integer endDays = haircut.getEndDaysToMaturity();
		for (CollateralHaircut collateralHaircut : collateralHaircutList) {
			if (haircut.isClientHaircut() == collateralHaircut.isClientHaircut() || haircut.isCounterpartyHaircut() == collateralHaircut.isCounterpartyHaircut()) {
				if (startDays == null) {
					if (collateralHaircut.getStartDaysToMaturity() == null && endDays.equals(collateralHaircut.getEndDaysToMaturity())) {
						matchCount++;
					}
					else if (MathUtils.isGreaterThanOrEqual(endDays, collateralHaircut.getEndDaysToMaturity())) {
						overlappingMaturity = true;
					}
				}
				else if (endDays == null) {
					if (collateralHaircut.getEndDaysToMaturity() == null && startDays.equals(collateralHaircut.getStartDaysToMaturity())) {
						matchCount++;
					}
					else if (MathUtils.isLessThanOrEqual(startDays, collateralHaircut.getStartDaysToMaturity())) {
						overlappingMaturity = true;
					}
				}
				else {
					if (startDays.equals(collateralHaircut.getStartDaysToMaturity()) && endDays.equals(collateralHaircut.getEndDaysToMaturity())) {
						matchCount++;
					}
					else if (MathUtils.isLessThan(startDays, collateralHaircut.getStartDaysToMaturity()) && MathUtils.isGreaterThan(endDays, collateralHaircut.getStartDaysToMaturity())) {
						overlappingMaturity = true;
					}
				}
				if (matchCount > 1) {
					throw new ValidationException("Cannot have two haircuts with the same maturity values.");
				}
				if (overlappingMaturity) {
					throw new ValidationException(haircut.toString() + " overlaps with " + collateralHaircut + " in this definition.");
				}
			}
		}
	}
}
