package com.clifton.collateral.haircut;


import com.clifton.collateral.haircut.cache.CollateralHaircutDefinitionSource;
import com.clifton.collateral.haircut.cache.CollateralHaircutDefinitionSpecificityCache;
import com.clifton.collateral.haircut.search.CollateralHaircutDefinitionSearchForm;
import com.clifton.collateral.haircut.search.CollateralHaircutSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralHaircutDefinitionServiceImpl</code> class provides basic implementation of CollateralHaircutDefinitionService interface.
 *
 * @author stevenf
 */
@Service
public class CollateralHaircutDefinitionServiceImpl implements CollateralHaircutDefinitionService {

	private AdvancedUpdatableDAO<CollateralHaircut, Criteria> collateralHaircutDAO;

	private AdvancedUpdatableDAO<CollateralHaircutDefinition, Criteria> collateralHaircutDefinitionDAO;

	private CollateralHaircutDefinitionSpecificityCache collateralHaircutDefinitionSpecificityCache;

	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////     Collateral Haircut Definition Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralHaircutDefinition getCollateralHaircutDefinition(int id) {
		CollateralHaircutDefinition haircutDefinition = getCollateralHaircutDefinitionDAO().findByPrimaryKey(id);
		populateHaircutDefinition(haircutDefinition);
		return haircutDefinition;
	}


	@Override
	public List<CollateralHaircutDefinition> getCollateralHaircutDefinitionList(final CollateralHaircutDefinitionSearchForm searchForm) {
		return getCollateralHaircutDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public CollateralHaircutDefinition saveCollateralHaircutDefinition(CollateralHaircutDefinition haircutDefinition) {
		List<CollateralHaircut> oldCollateralHaircutList = null;
		if (!haircutDefinition.isNewBean()) {
			CollateralHaircutDefinition oldHaircutDefinition = getCollateralHaircutDefinition(haircutDefinition.getId());
			oldCollateralHaircutList = oldHaircutDefinition.getCollateralHaircutList();
		}
		List<CollateralHaircut> newCollateralHaircutList = haircutDefinition.getCollateralHaircutList();
		final CollateralHaircutDefinition originalHaircutDefinition = haircutDefinition;
		haircutDefinition = getCollateralHaircutDefinitionDAO().save(haircutDefinition);
		CollectionUtils.getIterable(haircutDefinition.getCollateralHaircutList()).forEach(haircut -> haircut.setHaircutDefinition(originalHaircutDefinition));
		getCollateralHaircutDAO().saveList(newCollateralHaircutList, oldCollateralHaircutList);
		haircutDefinition.setCollateralHaircutList(newCollateralHaircutList);
		return haircutDefinition;
	}


	@Override
	@Transactional
	public void deleteCollateralHaircutDefinition(int id) {
		CollateralHaircutDefinition haircutDefinition = getCollateralHaircutDefinition(id);
		getCollateralHaircutDAO().deleteList(haircutDefinition.getCollateralHaircutList());
		getCollateralHaircutDefinitionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CollateralHaircut> getCollateralHaircutList(CollateralHaircutSearchForm searchForm) {
		return getCollateralHaircutDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//Used in json migrations via OneToManyEntity annotation
	@Override
	public List<CollateralHaircut> getCollateralHaircutListByDefinitionId(int definitionId) {
		return getCollateralHaircutDAO().findByField("haircutDefinition.id", definitionId);
	}


	@Override
	public BigDecimal getCollateralHaircutValue(int holdingAccountId, int securityId, boolean isCounterpartyHaircut, Date balanceDate) {
		CollateralHaircut haircut = getCollateralHaircut(holdingAccountId, securityId, isCounterpartyHaircut, balanceDate);
		return (haircut == null) ? BigDecimal.ZERO : haircut.getHaircutPercent();
	}


	@Override
	public BigDecimal getCollateralHaircutValue(InvestmentAccount holdingAccount, InvestmentSecurity security, boolean isCounterpartyHaircut, Date balanceDate) {
		CollateralHaircut haircut = getCollateralHaircut(holdingAccount, security, isCounterpartyHaircut, balanceDate);
		return (haircut == null) ? BigDecimal.ZERO : haircut.getHaircutPercent();
	}


	@Override
	public CollateralHaircut getCollateralHaircut(int holdingAccountId, int securityId, boolean isCounterpartyHaircut, Date balanceDate) {
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(holdingAccountId);
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getCollateralHaircut(holdingAccount, security, isCounterpartyHaircut, balanceDate);
	}


	@Override
	public CollateralHaircut getCollateralHaircut(InvestmentAccount holdingAccount, InvestmentSecurity security, boolean isCounterpartyHaircut, Date balanceDate) {
		CollateralHaircut result = null;
		// First, get the number of days to maturity
		Integer securityDaysToMaturity = security.getEndDate() == null ? null : DateUtils.getDaysDifference(security.getEndDate(), balanceDate);
		if ((securityDaysToMaturity != null) && (securityDaysToMaturity < 0)) {
			securityDaysToMaturity = 0;
		}
		CollateralHaircutDefinition haircutDefinition = getCollateralHaircutDefinitionSpecificityCache().getMostSpecificResult(new CollateralHaircutDefinitionSource(holdingAccount, security, securityDaysToMaturity));
		if (haircutDefinition != null) {
			for (CollateralHaircut haircut : haircutDefinition.getCollateralHaircutList()) {
				if ((!isCounterpartyHaircut && haircut.isClientHaircut()) || (isCounterpartyHaircut && haircut.isCounterpartyHaircut())) {
					//if no maturity date is found, then return either a haircut with no start/end maturity dates, or the haircut with no end date.
					if (securityDaysToMaturity == null &&
							(haircut.getStartYearsToMaturity() == null && haircut.getEndDaysToMaturity() == null) ||
							(haircut.getStartYearsToMaturity() != null && haircut.getEndDaysToMaturity() == null)) {
						result = haircut;
						break;
					}
					if (MathUtils.isBetweenAndNotEqualEnd(securityDaysToMaturity, haircut.getStartDaysToMaturity(), haircut.getEndDaysToMaturity())) {
						result = haircut;
						break;
					}
				}
			}
		}
		return result;
	}


	private void populateHaircutDefinition(CollateralHaircutDefinition haircutDefinition) {
		if (haircutDefinition != null) {
			haircutDefinition.setCollateralHaircutList(getCollateralHaircutListByDefinitionId(haircutDefinition.getId()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public AdvancedUpdatableDAO<CollateralHaircut, Criteria> getCollateralHaircutDAO() {
		return this.collateralHaircutDAO;
	}


	public void setCollateralHaircutDAO(AdvancedUpdatableDAO<CollateralHaircut, Criteria> collateralHaircutDAO) {
		this.collateralHaircutDAO = collateralHaircutDAO;
	}


	public AdvancedUpdatableDAO<CollateralHaircutDefinition, Criteria> getCollateralHaircutDefinitionDAO() {
		return this.collateralHaircutDefinitionDAO;
	}


	public void setCollateralHaircutDefinitionDAO(AdvancedUpdatableDAO<CollateralHaircutDefinition, Criteria> collateralHaircutDefinitionDAO) {
		this.collateralHaircutDefinitionDAO = collateralHaircutDefinitionDAO;
	}


	public CollateralHaircutDefinitionSpecificityCache getCollateralHaircutDefinitionSpecificityCache() {
		return this.collateralHaircutDefinitionSpecificityCache;
	}


	public void setCollateralHaircutDefinitionSpecificityCache(CollateralHaircutDefinitionSpecificityCache collateralHaircutDefinitionSpecificityCache) {
		this.collateralHaircutDefinitionSpecificityCache = collateralHaircutDefinitionSpecificityCache;
	}
}
