package com.clifton.collateral.haircut.cache;

import com.clifton.collateral.haircut.CollateralHaircutDefinition;


/**
 * The <class>CollateralHaircutDefinition</class> is a specificity cache used to lookup CollateralHaircutDefinition based first on the holding account, contract and holding company.
 * Then it uses the instrument, hierarchy, type, sub type and sub type 2 to identify the correct CollateralHaircutDefinition for a security and account.
 */
public interface CollateralHaircutDefinitionSpecificityCache {

	public CollateralHaircutDefinition getMostSpecificResult(CollateralHaircutDefinitionSource source);
}
