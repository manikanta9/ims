package com.clifton.collateral.haircut;


import com.clifton.collateral.CollateralService;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>CollateralHaircut</code> class defines haircut values and duration and application
 * that should be applied to market value of collateral positions.
 * For example, treasuries are riskier than cash and may get 5% haircut while equities may get 30% haircut.
 * Haircuts are linked to a haircutDefinition {@link CollateralHaircutDefinition}
 * <p>
 *
 * @author stevenf
 */
public class CollateralHaircut extends BaseEntity<Integer> {

	private CollateralHaircutDefinition haircutDefinition;
	/**
	 * Adjustment factor to market value:
	 * 0.0  means no haircut
	 * 0.3  means 30% haircut or 70% of the value left
	 */
	private BigDecimal haircutPercent;

	/**
	 * Determines whether the haircut should be applied to client collateral.
	 */
	private boolean clientHaircut;

	/**
	 * Determines whether the haircut should be applied to counterparty collateral.
	 */
	private boolean counterpartyHaircut;
	/**
	 * Start days that haircut will be applied.  For example, if this equals 730, then haircut will be
	 * applied if the years to maturity is >= 2 or 730 days
	 * <p>
	 * NOTE: This is entered as years on the UI and the number of days is calculated by years*365.
	 */
	private Integer startDaysToMaturity;
	/**
	 * End days that haircut will be applied.  For example, if this equals 1095, then haircut will be
	 * applied if the years to maturity is < 3 or 1095 days.
	 * <p>
	 * NOTE: This is entered as years on the UI and the number of days is calculated by years*365.
	 */
	private Integer endDaysToMaturity;


	@Override
	public String toString() {
		StringBuilder haircutString = new StringBuilder();
		if (isClientHaircut() && !isCounterpartyHaircut()) {
			haircutString.append("Client ");
		}
		else if (!isClientHaircut() && isCounterpartyHaircut()) {
			haircutString.append("Counterparty ");
		}
		String trimmedHaircutPercent = CoreMathUtils.formatNumber(MathUtils.multiply(getHaircutPercent(), BigDecimal.valueOf(100)).stripTrailingZeros(), "#,###.##");
		haircutString.append("[Haircut Percent: ").append(trimmedHaircutPercent).append("%")
				.append("; Start Year: ").append(getStartYearsToMaturity())
				.append("; End Year: ").append(this.getEndYearsToMaturity()).append("]");
		return haircutString.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public CollateralHaircutDefinition getHaircutDefinition() {
		return this.haircutDefinition;
	}


	public void setHaircutDefinition(CollateralHaircutDefinition haircutDefinition) {
		this.haircutDefinition = haircutDefinition;
	}


	public BigDecimal getHaircutPercent() {
		return this.haircutPercent;
	}


	public void setHaircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
	}


	public boolean isClientHaircut() {
		return this.clientHaircut;
	}


	public void setClientHaircut(boolean clientHaircut) {
		this.clientHaircut = clientHaircut;
	}


	public boolean isCounterpartyHaircut() {
		return this.counterpartyHaircut;
	}


	public void setCounterpartyHaircut(boolean counterpartyHaircut) {
		this.counterpartyHaircut = counterpartyHaircut;
	}


	public Integer getStartYearsToMaturity() {
		return getStartDaysToMaturity() == null ? null : getStartDaysToMaturity() / CollateralService.DAYS_IN_YEAR;
	}


	public void setStartYearsToMaturity(Integer startYearsToMaturity) {
		setStartDaysToMaturity(startYearsToMaturity == null ? null : startYearsToMaturity * CollateralService.DAYS_IN_YEAR);
	}


	public Integer getEndYearsToMaturity() {
		return getEndDaysToMaturity() == null ? null : getEndDaysToMaturity() / CollateralService.DAYS_IN_YEAR;
	}


	public void setEndYearsToMaturity(Integer endYearsToMaturity) {
		setEndDaysToMaturity(endYearsToMaturity == null ? null : endYearsToMaturity * CollateralService.DAYS_IN_YEAR);
	}


	public Integer getStartDaysToMaturity() {
		return this.startDaysToMaturity;
	}


	public void setStartDaysToMaturity(Integer startDaysToMaturity) {
		this.startDaysToMaturity = startDaysToMaturity;
	}


	public Integer getEndDaysToMaturity() {
		return this.endDaysToMaturity;
	}


	public void setEndDaysToMaturity(Integer endDaysToMaturity) {
		this.endDaysToMaturity = endDaysToMaturity;
	}
}
