package com.clifton.collateral.haircut.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>CollateralHaircutSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralHaircutDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "holdingAccount.number,holdingAccount.name,holdingCompany.name")
	private String searchPattern;

	@SearchField(searchField = "businessContract.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Integer businessContractId;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeId;

	@SearchField(searchField = "holdingAccount.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Integer holdingAccountId;

	@SearchField(searchField = "holdingCompany.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Integer holdingCompanyId;

	@SearchField(searchField = "investmentType.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Short investmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType.name")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "investmentTypeSubType2.name")
	private String investmentTypeSubType2;

	@SearchField(searchField = "investmentTypeSubType2.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType2;

	@SearchField(searchField = "instrumentHierarchy.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Short instrumentHierarchyId;

	@SearchField(searchField = "investmentInstrument.id", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL})
	private Integer investmentInstrumentId;

	@SearchField(searchField = "investmentInstrument.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] investmentInstrumentIdList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public Boolean getNullInvestmentTypeSubType() {
		return this.nullInvestmentTypeSubType;
	}


	public void setNullInvestmentTypeSubType(Boolean nullInvestmentTypeSubType) {
		this.nullInvestmentTypeSubType = nullInvestmentTypeSubType;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Boolean getNullInvestmentTypeSubType2() {
		return this.nullInvestmentTypeSubType2;
	}


	public void setNullInvestmentTypeSubType2(Boolean nullInvestmentTypeSubType2) {
		this.nullInvestmentTypeSubType2 = nullInvestmentTypeSubType2;
	}


	public Integer getBusinessContractId() {
		return this.businessContractId;
	}


	public void setBusinessContractId(Integer businessContractId) {
		this.businessContractId = businessContractId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer[] getInvestmentInstrumentIdList() {
		return this.investmentInstrumentIdList;
	}


	public void setInvestmentInstrumentIdList(Integer[] investmentInstrumentIdList) {
		this.investmentInstrumentIdList = investmentInstrumentIdList;
	}


	public Integer getExcludeId() {
		return this.excludeId;
	}


	public void setExcludeId(Integer excludeId) {
		this.excludeId = excludeId;
	}
}
