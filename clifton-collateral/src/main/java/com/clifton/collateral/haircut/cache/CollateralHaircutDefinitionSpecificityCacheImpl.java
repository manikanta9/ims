package com.clifton.collateral.haircut.cache;

import com.clifton.collateral.haircut.CollateralHaircutDefinition;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.collateral.haircut.search.CollateralHaircutDefinitionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <class>CollateralHaircutDefinition</class> is a specificity cache used to lookup CollateralHaircutDefinition based first on the holding account, contract and holding company.
 * Then it uses the instrument, hierarchy, type, sub type and sub type 2 to identify the correct CollateralHaircutDefinition for a security and account.
 */
@Component
public class CollateralHaircutDefinitionSpecificityCacheImpl extends AbstractSpecificityCache<CollateralHaircutDefinitionSource, CollateralHaircutDefinition, CollateralHaircutDefinitionTargetHolder> implements CollateralHaircutDefinitionSpecificityCache {

	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;

	private final ClearSpecificityCacheUpdater<CollateralHaircutDefinitionTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(CollateralHaircutDefinition.class);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralHaircutDefinition getMostSpecificResult(CollateralHaircutDefinitionSource source) {
		return CollectionUtils.getFirstElement(this.getMostSpecificResultList(source));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralHaircutDefinitionSpecificityCacheImpl() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	@Override
	protected boolean isMatch(CollateralHaircutDefinitionSource source, CollateralHaircutDefinitionTargetHolder holder) {
		return true;
	}


	@Override
	protected CollateralHaircutDefinition getTarget(CollateralHaircutDefinitionTargetHolder targetHolder) {
		return getCollateralHaircutDefinitionService().getCollateralHaircutDefinition(targetHolder.getId());
	}


	@Override
	protected Collection<CollateralHaircutDefinitionTargetHolder> getTargetHolders() {
		List<CollateralHaircutDefinitionTargetHolder> holders = new ArrayList<>();
		List<CollateralHaircutDefinition> haircutDefinitionList = getCollateralHaircutDefinitionService().getCollateralHaircutDefinitionList(new CollateralHaircutDefinitionSearchForm());
		for (CollateralHaircutDefinition haircutDefinition : CollectionUtils.getIterable(haircutDefinitionList)) {
			holders.add(new CollateralHaircutDefinitionTargetHolder(generateKey(haircutDefinition), haircutDefinition));
		}
		return holders;
	}


	private String generateKey(CollateralHaircutDefinition haircutDefinition) {
		return getKeyGenerator().generateKeyForTarget(generateKeySource(haircutDefinition));
	}


	private SpecificityKeySource generateKeySource(CollateralHaircutDefinition haircutDefinition) {
		Object[] holdingSpecificity = {BeanUtils.getBeanIdentity(haircutDefinition.getHoldingAccount()), BeanUtils.getBeanIdentity(haircutDefinition.getBusinessContract()),
				BeanUtils.getBeanIdentity(haircutDefinition.getHoldingCompany()), null};

		Object[] instrumentHierarchy = {BeanUtils.getBeanIdentity(haircutDefinition.getInvestmentInstrument()), BeanUtils.getBeanIdentity(haircutDefinition.getInstrumentHierarchy()), null};
		Object[] investmentTypeHierarchy = {BeanUtils.getBeanIdentity(haircutDefinition.getInvestmentType()), null};
		// NOTE: SUB TYPE 2 is more specific than SUB TYPE 1
		Object[] investmentTypeSubType2Hierarchy = {BeanUtils.getBeanIdentity(haircutDefinition.getInvestmentTypeSubType2()), null};
		Object[] investmentTypeSubTypeHierarchy = {BeanUtils.getBeanIdentity(haircutDefinition.getInvestmentTypeSubType()), null};

		Object[] valuesToAppend = {};

		return SpecificityKeySource.builder(holdingSpecificity)
				.addHierarchy(instrumentHierarchy)
				.addHierarchy(investmentTypeHierarchy)
				.addHierarchy(investmentTypeSubType2Hierarchy)
				.addHierarchy(investmentTypeSubTypeHierarchy)
				.setValuesToAppend(valuesToAppend)
				.build();
	}


	@Override
	protected SpecificityKeySource getKeySource(CollateralHaircutDefinitionSource source) {
		InvestmentInstrumentHierarchy hierarchy = source.getSecurity().getInstrument().getHierarchy();

		CollateralHaircutDefinition haircutDefinition = new CollateralHaircutDefinition();
		haircutDefinition.setInstrumentHierarchy(hierarchy);
		haircutDefinition.setInvestmentInstrument(source.getSecurity().getInstrument());
		haircutDefinition.setInvestmentType(hierarchy.getInvestmentType());
		haircutDefinition.setInvestmentTypeSubType(hierarchy.getInvestmentTypeSubType());
		haircutDefinition.setInvestmentTypeSubType2(hierarchy.getInvestmentTypeSubType2());
		haircutDefinition.setBusinessContract(source.getSecurity().getBusinessContract());
		haircutDefinition.setHoldingAccount(source.getHoldingAccount());
		haircutDefinition.setHoldingCompany(source.getHoldingAccount().getIssuingCompany());

		return generateKeySource(haircutDefinition);
	}


	@Override
	protected SpecificityCacheUpdater<CollateralHaircutDefinitionTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralHaircutDefinitionService getCollateralHaircutDefinitionService() {
		return this.collateralHaircutDefinitionService;
	}


	public void setCollateralHaircutDefinitionService(CollateralHaircutDefinitionService collateralHaircutDefinitionService) {
		this.collateralHaircutDefinitionService = collateralHaircutDefinitionService;
	}
}
