package com.clifton.collateral.balance;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.collateral.balance.search.CollateralBalanceLendingRepoSearchForm;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.collateral.balance.search.CollateralBalanceSearchFormConfigurer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.lending.api.repo.LendingRepoApiService;
import com.clifton.lending.api.repo.Repo;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CollateralBalanceServiceImpl</code> class provides basic implementation of CollateralBalanceService interface.
 *
 * @author manderson
 */
@Service
public class CollateralBalanceServiceImpl<D extends CollateralBalanceDetail> implements CollateralBalanceService {

	private AdvancedUpdatableDAO<CollateralBalance, Criteria> collateralBalanceDAO;
	private AdvancedUpdatableDAO<CollateralBalanceLendingRepo, Criteria> collateralBalanceLendingRepoDAO;

	private CollateralService collateralService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private RuleViolationStatusService ruleViolationStatusService;
	private InvestmentAccountGroupService investmentAccountGroupService;

	private LendingRepoApiService lendingRepoApiService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	/////////         Collateral Balance Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingPositionDailyService accountingPositionDailyService;


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	private CollateralBalanceRebuildService collateralBalanceRebuildService;


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	@Override
	public List<AccountingPositionDaily> getCollateralAccountingPositionDailyLiveList(String collateralTypeName, int holdingAccountId, Date positionDate, boolean summarizePositions) {
		CollateralType collateralType = getCollateralService().getCollateralTypeByName(collateralTypeName);
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(holdingAccountId);
		InvestmentAccount collateralAccount = getCollateralBalanceRebuildService().getCollateralAccount(collateralType, holdingAccount, positionDate);
		CollateralBalanceRebuildProcessor<?> processor = getCollateralBalanceRebuildService().getCollateralBalanceRebuildProcessor(collateralType, holdingAccount, collateralAccount);
		return getCollateralAccountingPositionDailyLiveList(processor, holdingAccountId, positionDate, collateralType.isUseSettlementDate(), summarizePositions);
	}


	@Override
	public List<AccountingPositionDaily> getCollateralAccountingPositionDailyLiveList(CollateralBalanceRebuildProcessor<?> processor, int holdingAccountId, Date positionDate, boolean useSettlementDate, boolean summarizePositions) {
		AccountingPositionDailyLiveSearchForm positionDailyLiveSearchForm = new AccountingPositionDailyLiveSearchForm();
		positionDailyLiveSearchForm.setHoldingAccountId(holdingAccountId);
		positionDailyLiveSearchForm.setSnapshotDate(positionDate);
		positionDailyLiveSearchForm.setCollateralAccountingAccount(false);
		positionDailyLiveSearchForm.setUseSettlementDate(useSettlementDate);
		positionDailyLiveSearchForm.setIncludeUnsettledLegPayments(true);
		List<AccountingPositionDaily> positionDailyList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(positionDailyLiveSearchForm);
		if (summarizePositions) {
			positionDailyList = summarizePositions(positionDailyList);
		}
		processor.appendAccountingPositionDaily(getInvestmentAccountService().getInvestmentAccount(holdingAccountId), positionDate, positionDailyList);
		return processor.calculateRequiredCollateralForAccountingPositionDailyList(positionDailyList, positionDate);
	}


	private List<AccountingPositionDaily> summarizePositions(List<AccountingPositionDaily> accountingPositionDailyList) {
		Map<Integer, AccountingPositionDaily> positionDailyMap = new LinkedHashMap<>();
		for (AccountingPositionDaily positionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
			Integer investmentSecurityId = positionDaily.getAccountingTransaction().getInvestmentSecurity().getId();
			AccountingPositionDaily existingPositionDaily = positionDailyMap.get(investmentSecurityId);
			if (existingPositionDaily != null) {
				//Update quantity on transaction info
				AccountingPosition position = AccountingPosition.forOpeningTransaction((AccountingTransaction) existingPositionDaily.getAccountingTransaction());
				position.getOpeningTransaction().setQuantity(MathUtils.add(positionDaily.getAccountingTransaction().getQuantity(), existingPositionDaily.getAccountingTransaction().getQuantity()));

				//update remaining quantity on position daily
				positionDaily.setPriorQuantity(MathUtils.add(positionDaily.getPriorQuantity(), existingPositionDaily.getPriorQuantity()));
				positionDaily.setRemainingQuantity(MathUtils.add(positionDaily.getRemainingQuantity(), existingPositionDaily.getRemainingQuantity()));
				positionDaily.setTodayClosedQuantity(MathUtils.add(positionDaily.getTodayClosedQuantity(), existingPositionDaily.getTodayClosedQuantity()));
				positionDaily.setAccountingTransaction(position.getOpeningTransaction());
			}
			positionDailyMap.put(investmentSecurityId, positionDaily);
		}
		return new ArrayList<>(positionDailyMap.values());
	}


	@Override
	public CollateralBalance getCollateralBalance(int id) {
		CollateralBalance result = getCollateralBalanceDAO().findByPrimaryKey(id);
		if (result.isParentCollateralBalance()) {
			populateInvestmentAccountGroup(result);
		}
		return result;
	}


	@Override
	public List<CollateralBalance> getCollateralBalanceList(CollateralBalanceSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getCollateralTypeName())) {
			searchForm.setCollateralTypeId(getCollateralService().getCollateralTypeByName(searchForm.getCollateralTypeName()).getId());
			searchForm.setCollateralTypeName(null);
		}
		return getCollateralBalanceListImpl(searchForm);
	}


	@Transactional(readOnly = true)
	protected List<CollateralBalance> getCollateralBalanceListImpl(final CollateralBalanceSearchForm searchForm) {
		List<CollateralBalance> result = getCollateralBalanceDAO().findBySearchCriteria(new CollateralBalanceSearchFormConfigurer(searchForm));

		if (BooleanUtils.isTrue(searchForm.getPopulateHoldingInvestmentAccountGroup())) {
			for (CollateralBalance collateralBalance : CollectionUtils.getIterable(result)) {
				if (collateralBalance.isParentCollateralBalance()) {
					populateInvestmentAccountGroup(collateralBalance);
				}
			}
		}

		return result;
	}


	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public CollateralBalance saveCollateralBalance(CollateralBalance bean) {
		CollateralBalance result;
		if (bean.getViolationStatus() == null) {
			bean.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));
		}
		// Only Overwrite the Detail List if it's actually on the bean - otherwise leave it alone
		if (bean.getCollateralType().getDetailTable() != null && !CollectionUtils.isEmpty(bean.getDetailList())) {
			UpdatableDAO<D> detailDao = (UpdatableDAO<D>) getDaoLocator().<D>locate(bean.getCollateralType().getDetailTable().getName());
			List<D> originalList = null;
			if (!bean.isNewBean()) {
				originalList = detailDao.findByField("collateralBalance.id", bean.getId());
			}
			result = getCollateralBalanceDAO().save(bean);
			List<D> newList = new ArrayList<>();
			for (CollateralBalanceDetail d : CollectionUtils.getIterable(bean.getDetailList())) {
				d.setCollateralBalance(result);
				newList.add((D) d);
			}
			detailDao.saveList(newList, originalList);
			result.setDetailList((List<CollateralBalanceDetail>) newList);
			return result;
		}
		else {
			return getCollateralBalanceDAO().save(bean);
		}
	}


	@Override
	@Transactional
	public void deleteCollateralBalance(int id) {
		CollateralBalance bean = getCollateralBalance(id);
		if (bean != null && bean.getCollateralType().getDetailTable() != null) {
			UpdatableDAO<D> detailDao = (UpdatableDAO<D>) getDaoLocator().<D>locate(bean.getCollateralType().getDetailTable().getName());
			detailDao.deleteList(detailDao.findByField("collateralBalance.id", id));
		}
		getCollateralBalanceDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Repo Collateral Balance Detail Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CollateralBalanceLendingRepo> getCollateralBalanceLendingRepoList(final CollateralBalanceLendingRepoSearchForm searchForm) {
		// The only place this is available is as a tab on the Balance Window, so enforcing this restriction
		ValidationUtils.assertNotNull(searchForm.getCollateralBalanceId(), "Collateral Balance Repo Details are available only for a specific balance record.");
		List<CollateralBalanceLendingRepo> list = getCollateralBalanceLendingRepoDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		// Hydrate the Repo Object based on repoIdentifier values
		BeanUtils.hydrateBeanList(list, CollateralBalanceLendingRepo::getRepoIdentifier, getLendingRepoApiService()::getRepoListForIds, Repo::getId, CollateralBalanceLendingRepo::setRepo, Integer.class);
		return list;
	}

	////////////////////////////////////////////////////////////////////////////
	//////       Repo Collateral Balance Account Business Methods         //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getCollateralBalanceRelatedAccountByPurposes(InvestmentAccount holdingAccount, Date balanceDate, boolean ourAccount, boolean validate, String... relationshipPurposes) {
		ValidationUtils.assertNotNull(holdingAccount, "Required argument 'holdingAccount' is missing.");
		try {
			InvestmentAccount relatedInvestmentAccount = null;
			String ourAccountMessage = ourAccount ? "'our'" : "NOT 'our'";
			for (String relationshipPurpose : relationshipPurposes) {
				if (relatedInvestmentAccount == null) {
					List<InvestmentAccount> relatedAccountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null, relationshipPurpose, balanceDate);
					ValidationUtils.assertTrue(CollectionUtils.getSize(relatedAccountList) < 3, "Cannot have more than 2 '" + relationshipPurpose + "' purpose links for: " + holdingAccount.getLabel());
					for (InvestmentAccount relatedAccount : CollectionUtils.getIterable(relatedAccountList)) {
						if (ourAccount == relatedAccount.getType().isOurAccount()) {
							ValidationUtils.assertNull(relatedInvestmentAccount, "Cannot have more than 1 " + ourAccountMessage + " account for '" + relationshipPurpose + "' purpose for: " + holdingAccount.getLabel());
							relatedInvestmentAccount = relatedAccount;
						}
					}
				}
			}
			ValidationUtils.assertNotNull(relatedInvestmentAccount, "Cannot find main account for purposes [" + Arrays.toString(relationshipPurposes) + "] for: " + holdingAccount.getLabel());
			return relatedInvestmentAccount;
		}
		catch (ValidationException e) {
			if (validate) {
				throw e;
			}
			return null;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateInvestmentAccountGroup(CollateralBalance collateralBalance) {
		if (collateralBalance.isParentCollateralBalance()) {
			InvestmentAccountGroupAccountSearchForm accountGroupAccountSearchForm = new InvestmentAccountGroupAccountSearchForm();
			accountGroupAccountSearchForm.setAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
			accountGroupAccountSearchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
			accountGroupAccountSearchForm.setLimit(1);
			InvestmentAccountGroupAccount accountGroupAccount = CollectionUtils.getFirstElement(getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(accountGroupAccountSearchForm));
			if (accountGroupAccount != null) {
				collateralBalance.setHoldingInvestmentAccountGroup(accountGroupAccount.getReferenceOne());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<CollateralBalance, Criteria> getCollateralBalanceDAO() {
		return this.collateralBalanceDAO;
	}


	public void setCollateralBalanceDAO(AdvancedUpdatableDAO<CollateralBalance, Criteria> collateralBalanceDAO) {
		this.collateralBalanceDAO = collateralBalanceDAO;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AdvancedUpdatableDAO<CollateralBalanceLendingRepo, Criteria> getCollateralBalanceLendingRepoDAO() {
		return this.collateralBalanceLendingRepoDAO;
	}


	public void setCollateralBalanceLendingRepoDAO(AdvancedUpdatableDAO<CollateralBalanceLendingRepo, Criteria> collateralBalanceLendingRepoDAO) {
		this.collateralBalanceLendingRepoDAO = collateralBalanceLendingRepoDAO;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public LendingRepoApiService getLendingRepoApiService() {
		return this.lendingRepoApiService;
	}


	public void setLendingRepoApiService(LendingRepoApiService lendingRepoApiService) {
		this.lendingRepoApiService = lendingRepoApiService;
	}
}
