package com.clifton.collateral.balance.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.action.processor.AccountingJournalActionProcessor;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>AccountingPositionTransferEventListener</code> handles rebuilding the CollateralBalance object when a collateral transfer is
 * booked or unbooked.  It will also handle updating the securitiesTransferAmount on the CollateralBalance when a transfer linked to a
 * collateral balance is saved or deleted.
 *
 * @author mwacker
 */
@Component
public class AccountingPositionTransferEventListener extends BaseEventListener<Event<AccountingPositionTransfer, Object>> implements AccountingJournalActionProcessor<AccountingPositionTransfer> {

	private AccountingPositionTransferService accountingPositionTransferService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CollateralService collateralService;
	private CollateralBalanceService collateralBalanceService;
	private CollateralBalanceRebuildService collateralBalanceRebuildService;
	private InvestmentAccountGroupService investmentAccountGroupService;

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void processAction(AccountingPositionTransfer transfer, AccountingJournal journal) {
		rebuildCollateralBalance(transfer);
	}


	@Override
	public void rollbackAction(AccountingPositionTransfer transfer, AccountingJournal journal) {
		rebuildCollateralBalance(transfer);
	}


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(AccountingPositionTransferService.ACCOUNTING_POSITION_TRANSFER_DELETED_EVENT_NAME,
				AccountingPositionTransferService.ACCOUNTING_POSITION_TRANSFER_SAVED_EVENT_NAME);
	}


	@Override
	public void onEvent(Event<AccountingPositionTransfer, Object> event) {
		AccountingPositionTransfer transfer = event.getTarget();
		// TODO: We could build in filters to the event listeners to do this check.
		if (transfer.getSourceSystemTable() != null && "CollateralBalance".equals(transfer.getSourceSystemTable().getName())) {
			updateSecuritiesTransferAmount(transfer);
		}
	}


	private void updateSecuritiesTransferAmount(AccountingPositionTransfer transfer) {
		InvestmentAccount collateralHoldingAccount = getCollateralHoldingAccount(transfer);
		CollateralType collateralType = getCollateralService().getCollateralTypeByAccount(collateralHoldingAccount);
		if (collateralType != null && collateralType.isAllowRebuildOnTransferEvent()) {
			BigDecimal securitiesTransferAmount = BigDecimal.ZERO;
			CollateralBalance balance = getCollateralBalanceService().getCollateralBalance(transfer.getSourceFkFieldId());

			AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
			searchForm.setSourceSystemTableId(transfer.getSourceSystemTable().getId());
			searchForm.setSourceFkFieldId(transfer.getSourceFkFieldId());
			searchForm.setTransactionDate(DateUtils.addDays(balance.getBalanceDate(), balance.getCollateralType().getTransferDateOffset()));
			List<AccountingPositionTransfer> accountingPositionTransferList = getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);
			for (AccountingPositionTransfer positionTransfer : CollectionUtils.getIterable(accountingPositionTransferList)) {
				positionTransfer = getAccountingPositionTransferService().getAccountingPositionTransfer(positionTransfer.getId());
				for (AccountingPositionTransferDetail transferDetail : CollectionUtils.getIterable(positionTransfer.getDetailList())) {
					securitiesTransferAmount = MathUtils.add(securitiesTransferAmount, transferDetail.getPositionCostBasis());
				}
			}
			balance.setSecuritiesTransferAmount(securitiesTransferAmount);
			getCollateralBalanceService().saveCollateralBalance(balance);
		}
	}


	private void rebuildCollateralBalance(AccountingPositionTransfer transfer) {
		// only rebuild for collateral transfers
		if (!transfer.getType().isCollateral()) {
			return;
		}
		InvestmentAccount collateralHoldingAccount = getCollateralHoldingAccount(transfer);
		CollateralType collateralType = getCollateralService().getCollateralTypeByAccount(collateralHoldingAccount);
		if (collateralType != null && collateralType.isAllowRebuildOnTransferEvent()) {
			CollateralBalanceRebuildCommand rebuildCommand = new CollateralBalanceRebuildCommand();
			rebuildCommand.setAsynchronousRebuildDelay(10);
			//We use the negative value of the transferDateOffset to compute the proper balance date
			rebuildCommand.setBalanceDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(transfer.getTransactionDate()), -collateralType.getTransferDateOffset()));
			rebuildCommand.setCollateralType(collateralType);
			rebuildCommand.setHoldingAccount(collateralHoldingAccount);
			rebuildCommand.setSkipExisting(false);
			rebuildCommand.setSynchronous(false);
			getCollateralBalanceRebuildService().rebuildCollateralBalance(rebuildCommand);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getCollateralHoldingAccount(AccountingPositionTransfer transfer) {
		InvestmentAccount collateralHoldingAccount;
		AccountingPositionTransferType transferType = transfer.getType();
		if (transferType.isCollateralPosting() == transferType.isCounterpartyCollateral()) {
			collateralHoldingAccount = transfer.getFromHoldingInvestmentAccount();
		}
		else {
			collateralHoldingAccount = transfer.getToHoldingInvestmentAccount();
		}

		//Search for account groups and select the primary account from the group if this holding account belongs to one
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setAccountId(collateralHoldingAccount.getId());
		searchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		InvestmentAccountGroupAccount accountGroupAccount = CollectionUtils.getFirstElement(getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm));
		if (accountGroupAccount != null) {
			InvestmentAccountGroupAccount primary = getInvestmentAccountGroupService().getPrimaryInvestmentAccountGroupAccountByGroup(accountGroupAccount.getReferenceOne().getId());
			if (primary != null) {
				collateralHoldingAccount = primary.getReferenceTwo();
			}
		}
		return collateralHoldingAccount;
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}
}
