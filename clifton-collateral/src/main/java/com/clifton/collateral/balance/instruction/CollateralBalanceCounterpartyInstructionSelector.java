package com.clifton.collateral.balance.instruction;


import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;


/**
 * The <code>CollateralBalanceCashCounterpartyInstructionSelector</code> defines the bean type
 * used to select corresponding {@link InvestmentInstructionDefinition} for each {@link CollateralBalance} record
 * that defines the instructions for the "counterparty" i.e. Holding Account
 * that is sent to the custodian/collateral account "Recipient"
 *
 * @author manderson
 */
public class CollateralBalanceCounterpartyInstructionSelector extends BaseCollateralBalanceInstructionSelector {

	@Override
	public BusinessCompany getRecipient(CollateralBalance balance) {
		InvestmentAccount collateralAccount = getCoalesceCollateralCustodianAccountForBalance(balance);

		if (collateralAccount == null) {
			throw new ValidationException("Unable to Determine Collateral or Custodian account for Collateral Balance Record: [" + balance.getLabel()
					+ "].  Unable to generate instructions without a custodian issuer to use as the recipient.");
		}
		return collateralAccount.getIssuingCompany();
	}
}
