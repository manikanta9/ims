package com.clifton.collateral.balance.workflow.escrow;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransactionCommand;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;


/**
 * The <code>CollateralBalanceCollateralTransferWorkflowAction</code> class is a workflow transition action
 * that creates collateral transfers and books them as well.
 * <p/>
 *
 * @author stevenf
 */
public class CollateralBalanceCollateralTransferEscrowWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	private boolean bookAndPost;
	private boolean postCollateral;


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
		command.setAutomatic(true);
		command.setBookAndPost(isBookAndPost());
		command.setPostCollateral(isPostCollateral());
		command.setCollateralBalanceId(collateralBalance.getId());
		getCollateralBalancePledgedService().createCollateralBalancePositionTransfers(command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBookAndPost() {
		return this.bookAndPost;
	}


	public void setBookAndPost(boolean bookAndPost) {
		this.bookAndPost = bookAndPost;
	}


	public boolean isPostCollateral() {
		return this.postCollateral;
	}


	public void setPostCollateral(boolean postCollateral) {
		this.postCollateral = postCollateral;
	}
}
