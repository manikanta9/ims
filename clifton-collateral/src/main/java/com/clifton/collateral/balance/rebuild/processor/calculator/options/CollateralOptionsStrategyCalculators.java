package com.clifton.collateral.balance.rebuild.processor.calculator.options;


import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;


/**
 * The <code>CollateralOptionsStrategyCalculators</code> defines the options collateral calculators for
 * various trading strategies used in margin and escrow accounts.  At this time very few of the
 * calculators are used in our strategies, but these act as a placeholder in the event we begin to
 * use alternate strategies.
 *
 * @author StevenF
 */
public enum CollateralOptionsStrategyCalculators {

	LONG_CALL_UNDER_9_MONTHS("LongCallUnder9Months", new CollateralOptionsStrategyZeroCalculator()),
	LONG_CALL_OVER_9_MONTHS("LongCallOver9Months", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_CALL("ShortCall", new CollateralOptionsStrategyShortCallCalculator()),
	LONG_PUT_UNDER_9_MONTHS("LongPutUnder9Months", new CollateralOptionsStrategyZeroCalculator()),
	LONG_PUT_OVER_9_MONTHS("LongPutOver9Months", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_PUT("ShortPut", new CollateralOptionsStrategyShortPutCalculator()),
	COVERED_CALL("CoveredCall", new CollateralOptionStrategyCoveredCallCalculator()),
	LONG_CALL_SHORT_UNDERLYING("LongCallShortUnderlying", new CollateralOptionsStrategyZeroCalculator()),
	LONG_PUT_LONG_UNDERLYING("LongPutLongUnderlying", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_PUT_SHORT_UNDERLYING("ShortPutShortUnderlying", new CollateralOptionsStrategyZeroCalculator()),
	LONG_STRADDLE("LongStraddle", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_STRADDLE("ShortStraddle", new CollateralOptionsStrategyZeroCalculator()),
	LONG_STRANGLE("LongStrangle", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_STRANGLE("ShortStrangle", new CollateralOptionsStrategyShortStrangleCalculator()),
	LONG_CALL_SPREAD("LongCallSpread", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_CALL_SPREAD("ShortCallSpread", new CollateralOptionsStrategyZeroCalculator()),
	LONG_PUT_SPREAD("LongPutSpread", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_PUT_SPREAD("ShortPutSpread", new CollateralOptionsStrategyShortPutSpreadCalculator()),
	LONG_CALL_TIME_SPREAD("LongCallTimeSpread", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_CALL_TIME_SPREAD("ShortCallTimeSpread", new CollateralOptionsStrategyZeroCalculator()),
	LONG_PUT_TIME_SPREAD("LongPutTimeSpread", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_PUT_TIME_SPREAD("ShortPutTimeSpread", new CollateralOptionsStrategyZeroCalculator()),
	LONG_CALL_BUTTERFLY("LongCallButterfly", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_CALL_BUTTERFLY("ShortCallButterfly", new CollateralOptionsStrategyZeroCalculator()),
	LONG_PUT_BUTTERFLY("LongPutButterfly", new CollateralOptionsStrategyZeroCalculator()),
	SHORT_PUT_BUTTERFLY("ShortPutButterfly", new CollateralOptionsStrategyZeroCalculator()),
	CALL_BACK_SPREAD("CallBackSpread", new CollateralOptionsStrategyZeroCalculator()),
	PUT_BACK_SPREAD("PutBackSpread", new CollateralOptionsStrategyZeroCalculator()),
	RATIO_CALL_SPREAD("RatioCallSpread", new CollateralOptionsStrategyZeroCalculator()),
	RATIO_PUT_SPREAD("RatioPutSpread", new CollateralOptionsStrategyZeroCalculator()),
	COLLAR("Collar", new CollateralOptionsStrategyZeroCalculator()),
	ZERO("Zero", new CollateralOptionsStrategyZeroCalculator());

	////////////////////////////////////////////////////////////////////////////
	private final String tradeStrategyName;
	private final CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> tradeStrategyCalculator;


	////////////////////////////////////////////////////////////////////////////
	CollateralOptionsStrategyCalculators(String tradeStrategyName, CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> tradeStrategyCalculator) {
		this.tradeStrategyName = tradeStrategyName;
		this.tradeStrategyCalculator = tradeStrategyCalculator;
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getter and Setter Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	public String getTradeStrategyName() {
		return this.tradeStrategyName;
	}


	public CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> getTradeStrategyCalculator() {
		return this.tradeStrategyCalculator;
	}
}
