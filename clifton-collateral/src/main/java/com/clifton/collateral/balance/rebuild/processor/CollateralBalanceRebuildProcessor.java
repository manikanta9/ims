package com.clifton.collateral.balance.rebuild.processor;


import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralBalanceRebuildProcessor</code> interface defines the methods to be implemented by
 * different CollateralType(s) specific processors.
 *
 * @author Mary Anderson
 */
public interface CollateralBalanceRebuildProcessor<S extends CollateralStrategyPosition> {

	public CollateralType getCollateralType();


	public void setCollateralType(CollateralType collateralType);


	/////////////////////////////////////////////////////////////////
	////////                Business Methods                 ////////
	/////////////////////////////////////////////////////////////////


	public List<AccountingPositionDaily> appendAccountingPositionDaily(InvestmentAccount holdingAccount, Date balanceDate, List<AccountingPositionDaily> positionDailyList);


	/**
	 * Gets the rounding value for the account and applies it to the supplied amount.
	 */
	public BigDecimal getCollateralTransferAmountRounded(int holdingInvestmentAccountId, BigDecimal amount);


	/////////////////////////////////////////////////////////////////
	////////                Rebuild Methods                 /////////
	/////////////////////////////////////////////////////////////////


	/**
	 * Allows for individual processors to apply additional logic to determine whether an account should be processed or not.
	 */
	public boolean processAccount(CollateralBalanceRebuildCommand rebuildCommand);


	/**
	 * Rebuilds collateral balance for the specified parameters and returns user friendly message with rebuild statistics and errors if any.
	 */
	public boolean rebuildCollateralBalance(CollateralBalanceRebuildCommand rebuildCommand, List<CollateralBalance> existingList);


	public List<AccountingPositionDaily> calculateRequiredCollateralForAccountingPositionDailyList(List<AccountingPositionDaily> accountingPositionDailyList, Date date);


	public List<S> calculateRequiredCollateralList(List<? extends AccountingPositionInfo> accountingPositionDailyList, List<CollateralPosition> collateralPositionList, Date positionDate, InvestmentAccount holdingAccount, boolean showExplanation);
}
