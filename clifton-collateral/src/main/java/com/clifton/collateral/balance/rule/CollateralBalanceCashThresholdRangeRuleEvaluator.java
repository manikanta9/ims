package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * This class can be used to specify the minimum and maximum threshold percent of cashCollateralValue.
 * By default the rule is excluded and has a min and max value of 0 and .1 respectively.  This can be overridden at the account level.
 *
 * @author stevenf
 */
public class CollateralBalanceCashThresholdRangeRuleEvaluator extends BaseCollateralBalanceThresholdRangeRuleEvaluator {

	@Override
	public boolean evaluateThresholdRangeRule(CollateralBalance balance) {
		if (balance.getCashCollateralValue() != null && MathUtils.isGreaterThan(balance.getCashCollateralValue(), BigDecimal.ZERO)) {
			return true;
		}
		return false;
	}
}
