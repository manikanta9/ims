package com.clifton.collateral.balance.rule;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This <code>CollateralBalancePricingMissingForSecurityRuleEvaluator</code> generates a rule violation if a position is missing market data on the
 * collateral balance date.
 *
 * @author jonathanr
 */
public class CollateralBalancePricingMissingForSecurityRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {

	private CollateralBalanceService collateralBalanceService;
	private MarketDataRetriever marketDataRetriever;


	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance collateralBalance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(collateralBalance.getHoldingInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<AccountingPositionDaily> positions = getCollateralBalanceService().getCollateralAccountingPositionDailyLiveList(collateralBalance.getCollateralType().getName(), collateralBalance.getHoldingInvestmentAccount().getId(), collateralBalance.getBalanceDate(), false);
			for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(positions)) {
				Date priceDate = getMarketDataRetriever().getPriceDate(accountingPositionDaily.getInvestmentSecurity(), collateralBalance.getBalanceDate(), false, null);
				if (priceDate == null) {
					//This could be a violation but in the case where the security matured it hangs around an extra day to realize gain/loss
					//so if the position has Remaining Cost Basis Local = 0 and  Remaining Quantity = 0 do not generate violation.
					if (!(MathUtils.isEqual(accountingPositionDaily.getRemainingCostBasisLocal(), BigDecimal.ZERO) && MathUtils.isEqual(accountingPositionDaily.getRemainingQuantity(), BigDecimal.ZERO))) {
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, collateralBalance.getId(), accountingPositionDaily.getInvestmentSecurity().getId(), null));
					}
				}
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
