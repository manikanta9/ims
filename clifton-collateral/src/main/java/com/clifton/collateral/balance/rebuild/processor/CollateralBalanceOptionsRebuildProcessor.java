package com.clifton.collateral.balance.rebuild.processor;

import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedService;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommandHandler;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyCoveredMatcher;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyLongMatcher;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyShortMatcher;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategySpreadMatcher;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyStrangleMatcher;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyZeroMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyAggregatePosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.haircut.CollateralHaircut;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>CollateralBalanceOptionsRebuildProcessor</code> is used to rebuild Options Margin and Options Escrow collateral accounts.
 *
 * @author stevenf
 */
public class CollateralBalanceOptionsRebuildProcessor extends BaseCollateralBalanceRebuildProcessor<CollateralOptionsStrategyPosition> {

	public static final String COLLATERAL_MARGIN = "Margin";
	public static final String COLLATERAL_ESCROW = "Escrow Receipt";
	public static final BigDecimal DEFAULT_MINIMUM_MARGIN_REQUIREMENT_PERCENTAGE = new BigDecimal(".1");

	/**
	 * This is used for 'Escrow Collateral' currently, since position transfers are created off of the
	 * pledged transaction values that are created, the data needs to stay synchronized so we specify
	 * and entityModifyCondition here that is used to set the condition on the position transfer when
	 * it is created from the pledged transaction data so that the transfer can not be modified directly.
	 */
	private Integer entityModifyConditionId;

	private String putRequirementType;
	private String callRequirementType;
	private Short[] allowedPutCollateralInvestmentTypeIds;
	private Short[] allowedCallCollateralInvestmentTypeIds;
	private Short[] restrictedPutCollateralInvestmentTypeIds;
	private Short[] restrictedCallCollateralInvestmentTypeIds;

	private BigDecimal indexMarginRequirementPercentage;
	private BigDecimal singleNameEquityMarginRequirementPercentage;

	private CollateralBalancePledgedService collateralBalancePledgedService;
	private AccountingBalanceService accountingBalanceService;
	private AccountingTransactionService accountingTransactionService;
	private CollateralOptionsStrategyCommandHandler collateralOptionsStrategyCommandHandler;
	private MarketDataRetriever marketDataRetriever;
	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;
	private CalendarSetupService calendarSetupService;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////
	//////////// Options Escrow Pledged Collateral Configuration Values ////////////
	////////////////////////////////////////////////////////////////////////////////
	//////// These values are used by the 'CollateralBalancePledgedService' ////////
	//////// when generating pledged collateral for options escrow accounts ////////
	////////////////////////////////////////////////////////////////////////////////

	//Specifies how to round the calculated quantity for a put (standard config of 1000)
	private BigDecimal putRoundingValue;
	//Specifies that if after applying the roundingAmount if the difference between the original value and the rounded value
	// is under the rounding threshold, then the roundingAmount will be added to the rounded value (standard config of 300)
	private BigDecimal putRoundingThreshold;
	//Specifies how to round the calculated quantity for a call (standard config of 1)
	private BigDecimal callRoundingValue;
	//Specifies that if after applying the roundingAmount if the difference between the original value and the rounded value
	// is under the rounding threshold, then the roundingAmount will be added to the rounded value (standard config of 0)
	private BigDecimal callRoundingThreshold;
	//Also known as 'Maintenance Collateral Threshold' this value is a percentage, expressed as a decimal, that collateral is
	// allowed to deviate from the required collateral after initially fully collateralized.  This allows for some leeway when
	// market changes make the required collateral value fluctuate. (e.g. a value of .15 indicates required collateral can not
	// be lower than 85% at any given time after initially collateralized.)
	private BigDecimal requiredCollateralThreshold;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyCollateralTypeOverrides(CollateralBalance balance) {
		balance.setClientInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), balance.getBalanceDate(), true, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.TRADING_OPTIONS_PURPOSE_NAME));
		balance.setCollateralInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), balance.getBalanceDate(), false, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.TRADING_OPTIONS_PURPOSE_NAME));
	}


	@Override
	protected void appendAccountingPositionList(CollateralBalanceRebuildCommand command) {
		command.setPositionList(
				getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
						.onPositionTransactionDate(command.getBalanceDate())
						.forHoldingAccount(command.getHoldingAccountId())
						.excludeReceivableGLAccounts()
				)
		);
	}


	@Override
	protected void applyAccountingPositionsToCollateralBalance(CollateralBalance balance, CollateralBalanceRebuildCommand command) {
		BigDecimal collateralRequirement = BigDecimal.ZERO;
		BigDecimal positionsMarketValue = BigDecimal.ZERO;
		BigDecimal longOptionsPositionsAdjustment = BigDecimal.ZERO;
		Map<Long, BigDecimal> requiredCollateralMap;
		final LocalDate nineMonthsOut = DateUtils.asLocalDate(command.getBalanceDate()).plus(9, ChronoUnit.MONTHS);
		if (CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN.equals(getCollateralType().getName())) {
			requiredCollateralMap = calculateRequiredCollateralMap(command.getPositionList(), command.getCollateralPositionList(), command.getBalanceDate(), command.getHoldingAccount());
			// for options margin, includes cash balance: cash and cash collateral without receivables.

			AccountingBalanceCommand accountingBalanceCommand = AccountingBalanceCommand.forClientAccountAndHoldingAccount(command.getBusinessClientId(), command.getHoldingAccountId())
					.withTransactionDate(command.getBalanceDate())
					.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL);

			BigDecimal totalCashValue = getAccountingBalanceService().getAccountingBalance(accountingBalanceCommand);

			balance.setTotalCashValue(totalCashValue);
			// get total of accounting transactions for balance date + 1
			Calendar calendar = getCalendarSetupService().getDefaultCalendar();
			Date nextBalanceDay = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(command.getBalanceDate(), calendar.getId()));
			balance.setAgreedMovementAmount(getAgreedMovementAmount(nextBalanceDay, command.getHoldingAccount()));
		}
		else {
			requiredCollateralMap = calculateRequiredCollateralForAccountingPositionInfoList(command.getPositionList(), command.getBalanceDate());
		}
		//Sum the calculated requirement of all positions to determine the total requirement.
		for (AccountingPosition position : CollectionUtils.getIterable(command.getPositionList())) {
			collateralRequirement = MathUtils.add(collateralRequirement, requiredCollateralMap.get(position.getId()));
			try {
				BigDecimal marketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(position, balance.getBalanceDate());
				positionsMarketValue = MathUtils.add(positionsMarketValue, marketValue);
				// track the amount of equity to subtract from the total market value due to long option positions
				if (InvestmentUtils.isOption(position.getInvestmentSecurity()) && MathUtils.isPositive(position.getRemainingQuantity())) {
					if (DateUtils.asLocalDate(position.getInvestmentSecurity().getEndDate()).isAfter(nineMonthsOut)) {
						// only include 25% of market value for long option position over 9 months to expiry.
						longOptionsPositionsAdjustment = MathUtils.add(longOptionsPositionsAdjustment, MathUtils.multiply(new BigDecimal(".75"), marketValue));
					}
					else {
						// do not include market value for long option position less than 9 months from expiry.
						longOptionsPositionsAdjustment = MathUtils.add(longOptionsPositionsAdjustment, marketValue);
					}
				}
			}
			catch (Exception e) {
				//TODO - actually throw error?  Load last trade price?  What?
				//Price data was unavailable for this position - log error, but continue?
				//LogUtils.error(getClass(), e.getMessage(), e);
			}
		}
		if (InvestmentAccountType.PORTFOLIO_MARGIN.equals(command.getHoldingAccount().getType().getName())) {
			balance.setCollateralRequirement(BigDecimal.ZERO);
		}
		else {
			balance.setCollateralRequirement(collateralRequirement);
		}
		balance.setPositionsMarketValue(positionsMarketValue);
		if (CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN.equals(getCollateralType().getName())) {
			balance.setPositionsMarketValue(MathUtils.subtract(balance.getPositionsMarketValue(), longOptionsPositionsAdjustment));
		}
	}


	@Override
	public List<AccountingPositionDaily> calculateRequiredCollateralForAccountingPositionDailyList(List<AccountingPositionDaily> accountingPositionDailyList, Date positionDate) {
		Map<Long, BigDecimal> positionInfoBigDecimalMap = calculateRequiredCollateralForAccountingPositionInfoList(accountingPositionDailyList, positionDate);
		for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
			BigDecimal requiredCollateral = positionInfoBigDecimalMap.get(accountingPositionDaily.getAccountingTransaction().getId());
			accountingPositionDaily.setRequiredCollateralBase(requiredCollateral != null ? requiredCollateral : BigDecimal.ZERO);
		}
		return accountingPositionDailyList;
	}


	@Override
	public List<CollateralOptionsStrategyPosition> calculateRequiredCollateralList(List<? extends AccountingPositionInfo> accountingPositionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate, InvestmentAccount holdingAccount, boolean showExplanation) {
		AssertUtils.assertNotNull(holdingAccount, "The Holding Account is required.");

		List<CollateralOptionsStrategyPosition> strategyPositionList = CollectionUtils.buildStream(accountingPositionInfoList)
				.map(p -> getCollateralOptionsStrategyCommandHandler().createCollateralOptionsStrategyPosition(p, positionDate))
				.collect(Collectors.toList());

		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> matches = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		Consumer<Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>>> matchAccumulator = m -> Optional.ofNullable(m)
				.filter(c -> !CollectionUtils.isEmpty(c))
				.filter(c -> !CollectionUtils.isEmpty(c.values()))
				.ifPresent(matches::putAll);

		// matchers mutate the strategyPositionList parameter.
		matchAccumulator.accept(CollateralStrategyZeroMatcher.getCollateralStrategyZeroPositionList(strategyPositionList, positionDate));
		matchAccumulator.accept(CollateralStrategySpreadMatcher.getCollateralStrategySpreadPositionMap(strategyPositionList, positionDate));
		matchAccumulator.accept(CollateralStrategyStrangleMatcher.getCollateralStrategyStranglePositionList(strategyPositionList, positionDate));
		matchAccumulator.accept(CollateralStrategyCoveredMatcher.getCollateralStrategyCoveredCallPositionList(strategyPositionList, collateralPositionList, positionDate));
		matchAccumulator.accept(CollateralStrategyShortMatcher.getCollateralStrategyShortPositionList(strategyPositionList, positionDate));
		matchAccumulator.accept(CollateralStrategyLongMatcher.getCollateralStrategyLongPositionList(strategyPositionList, positionDate));

		AssertUtils.assertTrue(strategyPositionList.isEmpty(), "All positions should have matched a strategy.");

		Map<String, BigDecimal> fxCache = new HashMap<>();
		String toCurrency = holdingAccount.getBaseCurrency().getSymbol();
		for (Map.Entry<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> entry : matches.entrySet()) {
			CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> calculator = entry.getKey().getTradeStrategyCalculator();
			for (CollateralOptionsStrategyPosition collateralOptionsStrategyPosition : entry.getValue()) {
				String fromCurrency = collateralOptionsStrategyPosition.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol();
				BigDecimal fxRate = fxCache.computeIfAbsent(fromCurrency, k -> getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(holdingAccount.toHoldingAccount(), fromCurrency, toCurrency, positionDate).flexibleLookup()));
				collateralOptionsStrategyPosition.setFxRateToBase(fxRate);
				calculator.populateCollateral(this, collateralOptionsStrategyPosition, showExplanation);
			}
		}

		// convert the map to a list again.
		List<CollateralOptionsStrategyPosition> positionList = matches.values().stream().flatMap(Collection::stream).collect(Collectors.toList());

		// flatten aggregated positions
		List<CollateralOptionsStrategyPosition> aggregatePositionList = new ArrayList<>();
		positionList = CollectionUtils.getStream(positionList)
				.flatMap(p -> {
					if (p instanceof CollateralOptionsStrategyAggregatePosition) {
						aggregatePositionList.add(p);
						return ((CollateralOptionsStrategyAggregatePosition) p).getCompositeStrategyPositionList().stream();
					}
					return Stream.of(p);
				})
				.collect(Collectors.toList());
		if (!aggregatePositionList.isEmpty()) {
			positionList.removeAll(aggregatePositionList);
		}
		return positionList;
	}


	private BigDecimal getAgreedMovementAmount(Date transactionDate, InvestmentAccount holdingAccount) {
		AssertUtils.assertNotNull(transactionDate, "Transaction Date is required.");
		AssertUtils.assertNotNull(holdingAccount, "Holding Account is required.");

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setDeleted(false);
		searchForm.setTransactionDate(transactionDate);
		searchForm.setCollateralAccountingAccount(true);
		// exclude Cash Collateral Payable
		searchForm.setReceivableAccountingAccount(false);
		searchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		List<AccountingTransaction> accountingTransactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		BigDecimal agreedMovementAmount = BigDecimal.ZERO;
		for (AccountingTransaction accountingTransaction : CollectionUtils.getIterable(accountingTransactionList)) {
			BigDecimal marketValue;
			if (accountingTransaction.getAccountingAccount().isCash()) {
				marketValue = accountingTransaction.getBaseDebitCredit();
			}
			else {
				AccountingPosition position = AccountingPosition.forOpeningTransaction(accountingTransaction);
				marketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(position, transactionDate);
				if (position.getAccountingAccount().isCollateral() && AccountingAccountType.ASSET.equals(position.getAccountingAccount().getAccountType().getName())
						&& !position.getAccountingAccount().isReceivable()) {
					boolean isCounterpartyHaircut = position.getAccountingAccount().isNotOurAccount();
					CollateralPosition collateralPosition = CollateralPosition.forAccountingPosition(position);
					if (!InvestmentAccountType.ESCROW_RECEIPT.equals(position.getHoldingInvestmentAccount().getType().getName())) {
						CollateralHaircut haircut = getCollateralHaircutDefinitionService().getCollateralHaircut(collateralPosition.getHoldingAccount(), collateralPosition.getSecurity(), isCounterpartyHaircut, transactionDate);
						if (haircut != null) {
							collateralPosition.setHaircut(haircut.getHaircutPercent());
							collateralPosition.setCollateralMarketValue(marketValue);
							marketValue = collateralPosition.getCollateralMarketValueAdjusted();
						}
					}
				}
			}
			agreedMovementAmount = MathUtils.add(agreedMovementAmount, marketValue);
		}
		return agreedMovementAmount;
	}


	private Map<Long, BigDecimal> calculateRequiredCollateralMap(List<? extends AccountingPositionInfo> accountingPositionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate, InvestmentAccount holdingAccount) {
		List<CollateralOptionsStrategyPosition> collateralOptionsStrategyPositionList = calculateRequiredCollateralList(accountingPositionInfoList, collateralPositionList, positionDate, holdingAccount, false);
		return CollectionUtils.getStream(collateralOptionsStrategyPositionList)
				.collect(Collectors.groupingBy(CollateralOptionsStrategyPosition::getAccountingTransactionId,
						Collectors.mapping(CollateralOptionsStrategyPosition::getBaseRequiredCollateral, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
	}


	private Map<Long, BigDecimal> calculateRequiredCollateralForAccountingPositionInfoList(List<? extends AccountingPositionInfo> positionInfoList, Date positionDate) {
		ValidationUtils.assertNotNull(getPutRequirementType(), "Collateral Configuration must specify a 'PUT' requirement calculation type.");
		ValidationUtils.assertNotNull(getCallRequirementType(), "Collateral Configuration must specify a 'CALL' requirement calculation type.");
		//Now we calculate the required collateral for positions and overall market value
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();

		Map<CollateralOptionsStrategyCalculators, List<AccountingPositionInfo>> strategyCalculatorPositionInfoListMap = getTradeStrategyCalculatorsMap(positionInfoList, positionDate);
		for (Map.Entry<CollateralOptionsStrategyCalculators, List<AccountingPositionInfo>> strategyCalculatorsListEntry : strategyCalculatorPositionInfoListMap.entrySet()) {
			CollateralOptionsStrategyCommand command = getCollateralOptionsStrategyCommandHandler().populateCommand(this, strategyCalculatorsListEntry.getValue(), positionDate);
			collateralRequirementMap.putAll(strategyCalculatorsListEntry.getKey().getTradeStrategyCalculator().calculateCollateral(command));
		}

		return collateralRequirementMap;
	}


	/**
	 * Iterates positions and determines the proper CollateralStrategyCalculator to be used.
	 */
	private Map<CollateralOptionsStrategyCalculators, List<AccountingPositionInfo>> getTradeStrategyCalculatorsMap(List<? extends AccountingPositionInfo> positionInfoList, Date positionDate) {
		Map<CollateralOptionsStrategyCalculators, List<AccountingPositionInfo>> strategyCalculatorMap = new EnumMap<>(CollateralOptionsStrategyCalculators.class);
		for (AccountingPositionInfo positionInfo : CollectionUtils.getIterable(positionInfoList)) {
			final boolean shortPosition = MathUtils.isLessThan(positionInfo.getRemainingQuantity(), BigDecimal.ZERO);

			CollateralOptionsStrategyCalculators calculator;
			//First let's calculate the normal strategies and set them - typically these will apply.
			if (isPutSpread(positionInfo, positionInfoList, positionInfo.getInvestmentSecurity().getOptionType())) {
				calculator = shortPosition ? CollateralOptionsStrategyCalculators.SHORT_PUT_SPREAD : CollateralOptionsStrategyCalculators.LONG_PUT_SPREAD;
			}
			//For option strangles, we first check for matched quantities, items that aren't matched in the first statement could still be matched in the second.
			else if (isOptionStrangle(positionInfo, positionInfoList, false) || isOptionStrangle(positionInfo, positionInfoList, true)) {
				calculator = shortPosition ? CollateralOptionsStrategyCalculators.SHORT_STRANGLE : CollateralOptionsStrategyCalculators.LONG_STRANGLE;
			}
			else if (shortPosition) {
				calculator = positionInfo.getInvestmentSecurity().isPutOption() ? CollateralOptionsStrategyCalculators.SHORT_PUT : CollateralOptionsStrategyCalculators.SHORT_CALL;
			}
			else {
				calculator = positionInfo.getInvestmentSecurity().isPutOption() ? CollateralOptionsStrategyCalculators.LONG_PUT_OVER_9_MONTHS : CollateralOptionsStrategyCalculators.LONG_CALL_OVER_9_MONTHS;
				if (positionInfo.getInvestmentSecurity().getEndDate() != null) {
					if (DateUtils.isDateAfter(positionDate, DateUtils.addMonths(positionInfo.getInvestmentSecurity().getEndDate(), -9))) {
						calculator = positionInfo.getInvestmentSecurity().isPutOption() ? CollateralOptionsStrategyCalculators.LONG_PUT_UNDER_9_MONTHS : CollateralOptionsStrategyCalculators.LONG_CALL_UNDER_9_MONTHS;
					}
				}
			}
			List<AccountingPositionInfo> strategyPositionInfoList = strategyCalculatorMap.get(calculator);
			if (strategyPositionInfoList == null) {
				strategyPositionInfoList = new ArrayList<>();
			}
			strategyPositionInfoList.add(positionInfo);
			strategyCalculatorMap.put(calculator, strategyPositionInfoList);
		}
		return strategyCalculatorMap;
	}


	private boolean isOptionStrangle(AccountingPositionInfo positionInfo, List<? extends AccountingPositionInfo> positionInfoList, boolean allowMismatched) {
		if (CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN.equals(getCollateralType().getName())) {
			if (positionInfo.getPositionGroupId() != null && positionInfo.getPositionGroupId().startsWith("OS_")) {
				return true;
			}
			//We manufacture a positionGroupId here for strangles we 'discover' manually (e.g. that weren't necessarily grouped properly by trading)
			for (AccountingPositionInfo existingPositionInfo : CollectionUtils.getIterable(positionInfoList)) {
				if (positionInfo.getAccountingTransactionId().longValue() != existingPositionInfo.getAccountingTransactionId().longValue() && existingPositionInfo.getPositionGroupId() == null) {
					//Find positions with different optionType, and same end date to strangle this position with.
					if (InvestmentUtils.isSecurityOfType(positionInfo.getInvestmentSecurity(), InvestmentType.OPTIONS)) {
						if (positionInfo.getInvestmentSecurity().getOptionType() != existingPositionInfo.getInvestmentSecurity().getOptionType() &&
								DateUtils.isEqualWithoutTime(positionInfo.getInvestmentSecurity().getEndDate(), existingPositionInfo.getInvestmentSecurity().getEndDate())) {
							if (allowMismatched || MathUtils.isEqual(positionInfo.getRemainingQuantity(), existingPositionInfo.getRemainingQuantity())) {
								String positionGroupId = "OS_" + positionInfo.getInvestmentSecurity().getId() + "_" + existingPositionInfo.getInvestmentSecurity().getId();
								positionInfo.setPositionGroupId(positionGroupId);
								existingPositionInfo.setPositionGroupId(positionGroupId);
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}


	private boolean isPutSpread(AccountingPositionInfo positionInfo, List<? extends AccountingPositionInfo> positionInfoList, InvestmentSecurityOptionTypes optionType) {
		//Look for a corresponding PUT of the reverse quantity (PUT_SPREAD)
		if (optionType == InvestmentSecurityOptionTypes.PUT) {
			if (positionInfo.getPositionGroupId() != null && positionInfo.getPositionGroupId().startsWith("PS_")) {
				return true;
			}
			//We manufacture a positionGroupId here for put spreads we 'discover' manually (e.g. that weren't necessarily grouped properly by trading)
			for (AccountingPositionInfo existingPositionInfo : CollectionUtils.getIterable(positionInfoList)) {
				if (positionInfo.getAccountingTransactionId().longValue() != existingPositionInfo.getAccountingTransactionId().longValue()) {
					if (optionType == existingPositionInfo.getInvestmentSecurity().getOptionType() &&
							DateUtils.isEqualWithoutTime(positionInfo.getInvestmentSecurity().getEndDate(), existingPositionInfo.getInvestmentSecurity().getEndDate()) &&
							MathUtils.isEqual(MathUtils.negate(positionInfo.getRemainingQuantity()), existingPositionInfo.getRemainingQuantity())) {
						String positionGroupId = "PS_" + positionInfo.getInvestmentSecurity().getId() + "_" + existingPositionInfo.getInvestmentSecurity().getId();
						positionInfo.setPositionGroupId(positionGroupId);
						existingPositionInfo.setPositionGroupId(positionGroupId);
						return true;
					}
				}
			}
		}
		return false;
	}


	//Default for index based securities is 15%, else 20%.
	//Citibank is always 20%, they only trade non-indexed based?
	//Called by short put and short call calculators
	public BigDecimal getMarginPercentage(InvestmentSecurity security) {
		if ("Options On Indices".equals(security.getInstrument().getHierarchy().getInvestmentTypeSubType().getName())) {
			return getIndexMarginRequirementPercentage() != null ? getIndexMarginRequirementPercentage() : new BigDecimal(".15");
		}
		return getSingleNameEquityMarginRequirementPercentage() != null ? getSingleNameEquityMarginRequirementPercentage() : new BigDecimal(".15");
	}


	//Overrides default of 'true' for copy note functionality
	@Override
	public boolean copyPreviousCollateralBalanceNote() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}


	public CollateralOptionsStrategyCommandHandler getCollateralOptionsStrategyCommandHandler() {
		return this.collateralOptionsStrategyCommandHandler;
	}


	public void setCollateralOptionsStrategyCommandHandler(CollateralOptionsStrategyCommandHandler collateralOptionsStrategyCommandHandler) {
		this.collateralOptionsStrategyCommandHandler = collateralOptionsStrategyCommandHandler;
	}


	public BigDecimal getIndexMarginRequirementPercentage() {
		return this.indexMarginRequirementPercentage;
	}


	public void setIndexMarginRequirementPercentage(BigDecimal indexMarginRequirementPercentage) {
		this.indexMarginRequirementPercentage = indexMarginRequirementPercentage;
	}


	public BigDecimal getSingleNameEquityMarginRequirementPercentage() {
		return this.singleNameEquityMarginRequirementPercentage;
	}


	public void setSingleNameEquityMarginRequirementPercentage(BigDecimal singleNameEquityMarginRequirementPercentage) {
		this.singleNameEquityMarginRequirementPercentage = singleNameEquityMarginRequirementPercentage;
	}


	public Short[] getRestrictedPutCollateralInvestmentTypeIds() {
		return this.restrictedPutCollateralInvestmentTypeIds;
	}


	public void setRestrictedPutCollateralInvestmentTypeIds(Short[] restrictedPutCollateralInvestmentTypeIds) {
		this.restrictedPutCollateralInvestmentTypeIds = restrictedPutCollateralInvestmentTypeIds;
	}


	public Short[] getRestrictedCallCollateralInvestmentTypeIds() {
		return this.restrictedCallCollateralInvestmentTypeIds;
	}


	public void setRestrictedCallCollateralInvestmentTypeIds(Short[] restrictedCallCollateralInvestmentTypeIds) {
		this.restrictedCallCollateralInvestmentTypeIds = restrictedCallCollateralInvestmentTypeIds;
	}


	public String getPutRequirementType() {
		return this.putRequirementType;
	}


	public void setPutRequirementType(String putRequirementType) {
		this.putRequirementType = putRequirementType;
	}


	public String getCallRequirementType() {
		return this.callRequirementType;
	}


	public void setCallRequirementType(String callRequirementType) {
		this.callRequirementType = callRequirementType;
	}


	public Short[] getAllowedPutCollateralInvestmentTypeIds() {
		return this.allowedPutCollateralInvestmentTypeIds;
	}


	public void setAllowedPutCollateralInvestmentTypeIds(Short[] allowedPutCollateralInvestmentTypeIds) {
		this.allowedPutCollateralInvestmentTypeIds = allowedPutCollateralInvestmentTypeIds;
	}


	public Short[] getAllowedCallCollateralInvestmentTypeIds() {
		return this.allowedCallCollateralInvestmentTypeIds;
	}


	public void setAllowedCallCollateralInvestmentTypeIds(Short[] allowedCallCollateralInvestmentTypeIds) {
		this.allowedCallCollateralInvestmentTypeIds = allowedCallCollateralInvestmentTypeIds;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public CollateralBalancePledgedService getCollateralBalancePledgedService() {
		return this.collateralBalancePledgedService;
	}


	public void setCollateralBalancePledgedService(CollateralBalancePledgedService collateralBalancePledgedService) {
		this.collateralBalancePledgedService = collateralBalancePledgedService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CollateralHaircutDefinitionService getCollateralHaircutDefinitionService() {
		return this.collateralHaircutDefinitionService;
	}


	public void setCollateralHaircutDefinitionService(CollateralHaircutDefinitionService collateralHaircutDefinitionService) {
		this.collateralHaircutDefinitionService = collateralHaircutDefinitionService;
	}


	public BigDecimal getPutRoundingValue() {
		return this.putRoundingValue;
	}


	public void setPutRoundingValue(BigDecimal putRoundingValue) {
		this.putRoundingValue = putRoundingValue;
	}


	public BigDecimal getPutRoundingThreshold() {
		return this.putRoundingThreshold;
	}


	public void setPutRoundingThreshold(BigDecimal putRoundingThreshold) {
		this.putRoundingThreshold = putRoundingThreshold;
	}


	public BigDecimal getCallRoundingValue() {
		return this.callRoundingValue;
	}


	public void setCallRoundingValue(BigDecimal callRoundingValue) {
		this.callRoundingValue = callRoundingValue;
	}


	public BigDecimal getCallRoundingThreshold() {
		return this.callRoundingThreshold;
	}


	public void setCallRoundingThreshold(BigDecimal callRoundingThreshold) {
		this.callRoundingThreshold = callRoundingThreshold;
	}


	public BigDecimal getRequiredCollateralThreshold() {
		return this.requiredCollateralThreshold;
	}


	public void setRequiredCollateralThreshold(BigDecimal requiredCollateralThreshold) {
		this.requiredCollateralThreshold = requiredCollateralThreshold;
	}
}
