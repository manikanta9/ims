package com.clifton.collateral.balance.pledged.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class CollateralBalancePledgedTransactionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "positionTransfer.id")
	private Integer positionTransferId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "positionInvestmentSecurity.id", sortField = "positionInvestmentSecurity.symbol")
	private Integer positionInvestmentSecurityId;

	@SearchField(searchField = "postedCollateralInvestmentSecurity.id", sortField = "postedCollateralInvestmentSecurity.symbol")
	private Integer postedCollateralInvestmentSecurityId;

	@SearchField
	private BigDecimal postedCollateralQuantity;

	@SearchField
	private Date postedDate;

	// Custom Search Filter
	private Boolean transferNotBooked;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getPositionTransferId() {
		return this.positionTransferId;
	}


	public void setPositionTransferId(Integer positionTransferId) {
		this.positionTransferId = positionTransferId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getPositionInvestmentSecurityId() {
		return this.positionInvestmentSecurityId;
	}


	public void setPositionInvestmentSecurityId(Integer positionInvestmentSecurityId) {
		this.positionInvestmentSecurityId = positionInvestmentSecurityId;
	}


	public Integer getPostedCollateralInvestmentSecurityId() {
		return this.postedCollateralInvestmentSecurityId;
	}


	public void setPostedCollateralInvestmentSecurityId(Integer postedCollateralInvestmentSecurityId) {
		this.postedCollateralInvestmentSecurityId = postedCollateralInvestmentSecurityId;
	}


	public BigDecimal getPostedCollateralQuantity() {
		return this.postedCollateralQuantity;
	}


	public void setPostedCollateralQuantity(BigDecimal postedCollateralQuantity) {
		this.postedCollateralQuantity = postedCollateralQuantity;
	}


	public Date getPostedDate() {
		return this.postedDate;
	}


	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}


	public Boolean getTransferNotBooked() {
		return this.transferNotBooked;
	}


	public void setTransferNotBooked(Boolean transferNotBooked) {
		this.transferNotBooked = transferNotBooked;
	}
}
