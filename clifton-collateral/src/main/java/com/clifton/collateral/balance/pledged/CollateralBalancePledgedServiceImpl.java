package com.clifton.collateral.balance.pledged;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosing;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingCommand;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingSecurityQuantity;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedBalanceSearchForm;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedTransactionSearchForm;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedTransactionSearchFormConfigurer;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableUtils;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import org.hibernate.Criteria;
import org.hibernate.type.DateType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>CollateralBalancePledgedServiceImpl</code> handle creation of pledged collateral transaction entries,
 * as well as position transfers for posting and returning collateral for options.
 *
 * @author stevenf
 */
@Service
public class CollateralBalancePledgedServiceImpl implements CollateralBalancePledgedService {

	private AdvancedReadOnlyDAO<CollateralBalancePledgedBalance, Criteria> collateralBalancePledgedBalanceDAO;
	private AdvancedUpdatableDAO<CollateralBalancePledgedTransaction, Criteria> collateralBalancePledgedTransactionDAO;

	private AccountingBookingService<AccountingPositionTransfer> accountingBookingService;
	private AccountingPositionClosingService accountingPositionClosingService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingValuationService accountingValuationService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private CollateralBalanceService collateralBalanceService;
	private CollateralBalanceRebuildService collateralBalanceRebuildService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemConditionService systemConditionService;
	private SystemSchemaService systemSchemaService;
	private TradeService tradeService;

	private static final BigDecimal DEFAULT_REQUIRED_COLLATERAL_THRESHOLD = new BigDecimal(".15");

	private static final List<String> UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST = new ArrayList<>();

	private static final String UPLOAD_PLEDGED_TRANSACTION_POSITION_SECURITY_COLUMN_NAME = "Position Security Symbol";
	private static final String UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_SECURITY_COLUMN_NAME = "Collateral Security Symbol";
	private static final String UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_QUANTITY_COLUMN_NAME = "Collateral Quantity";


	static {
		UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST.add(UPLOAD_PLEDGED_TRANSACTION_POSITION_SECURITY_COLUMN_NAME);
		UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST.add(UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_SECURITY_COLUMN_NAME);
		UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST.add(UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_QUANTITY_COLUMN_NAME);
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Command Methods   ////////
	////////////////////////////////////////////////////////////////////////////


	private CollateralBalancePledgedServiceContext initializeContext(Integer collateralBalanceId, boolean generatePledgedCollateral, boolean skipMaturityEventAndTradeProcessing) {
		CollateralBalancePledgedServiceContext context = new CollateralBalancePledgedServiceContext();

		ValidationUtils.assertNotNull(collateralBalanceId, "Collateral Balance ID cannot be null!");
		CollateralBalance balance = getCollateralBalanceService().getCollateralBalance(collateralBalanceId);
		ValidationUtils.assertNotNull(balance, "Collateral Balance cannot be null!");
		ValidationUtils.assertNotNull(balance.getBalanceDate(), "Balance Date cannot be null!");
		ValidationUtils.assertNotNull(balance.getCollateralType(), "Collateral Type cannot be null!");
		ValidationUtils.assertNotNull(balance.getClientInvestmentAccount(), "Client Investment Account cannot be null!");
		ValidationUtils.assertNotNull(balance.getCollateralInvestmentAccount(), "Collateral Investment Account cannot be null!");
		ValidationUtils.assertNotNull(balance.getHoldingInvestmentAccount(), "Holding Investment Account cannot be null!");

		context.setCollateralBalance(balance);
		context.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(context.getPostedDate()), balance.getCollateralType().getTransferDateOffset()));
		CollateralBalanceRebuildProcessor<?> processor = getCollateralBalanceRebuildService().getCollateralBalanceRebuildProcessor(balance.getCollateralType(), balance.getHoldingInvestmentAccount(), balance.getCollateralInvestmentAccount());
		ValidationUtils.assertNotNull(processor, "Unable to local CollateralBalanceRebuildProcessor for type [" + balance.getCollateralType().getName() + "]");
		if (processor instanceof CollateralBalanceOptionsRebuildProcessor) {
			context.setRebuildProcessor((CollateralBalanceOptionsRebuildProcessor) processor);
		}
		else {
			throw new ValidationException("Collateral Balance Pledge functionality only applies to options rebuild processors.");
		}

		/*
		 * When generating pledged collateral we need to prepare the context object to contain the objects we will use during processing.
		 * This includes the AccountingBalanceValue of collateral securities held in the collateral account as well as those that
		 * have already been pledged to the holding account for options.
		 */
		if (generatePledgedCollateral) {
			Map<Integer, CollateralBalancePledgedCollateralValue> pledgedCollateralValueMap = context.getPledgedCollateralValueMap();
			List<AccountingBalanceValue> collateralAccountAccountingBalanceValueList = getAccountingValuationService().getAccountingBalanceValueList(context.getAccountingBalanceSearchForm(true));

			//Get the available pledged collateral from the collateral account for collateral balance and initialize the pledgedCollateralValueMap
			for (AccountingBalanceValue balanceValue : CollectionUtils.getIterable(collateralAccountAccountingBalanceValueList)) {
				if (balanceValue.getAccountingAccount().getName().equals(AccountingAccount.ASSET_POSITION) || balanceValue.getAccountingAccount().getName().equals(AccountingAccount.ASSET_CASH)) {
					CollateralBalancePledgedCollateralValue pledgedCollateralValue = new CollateralBalancePledgedCollateralValue();
					pledgedCollateralValue.setCollateralSecurity(balanceValue.getInvestmentSecurity());
					pledgedCollateralValue.setCashCollateral(balanceValue.getAccountingAccount().getName().equals(AccountingAccount.ASSET_CASH));
					//Set the original and remaining quantities - this will be used to determine collateral movements later
					pledgedCollateralValue.setOriginalQuantity(pledgedCollateralValue.isCashCollateral() ? balanceValue.getBaseMarketValue() : balanceValue.getQuantity());
					pledgedCollateralValue.setRemainingQuantity(pledgedCollateralValue.isCashCollateral() ? balanceValue.getBaseMarketValue() : balanceValue.getQuantity());
					pledgedCollateralValueMap.put(balanceValue.getInvestmentSecurity().getId(), pledgedCollateralValue);
				}
			}

			//Now find the pledged collateral for the collateral balance and update the pledgedCollateralValueMap
			for (CollateralBalancePledgedBalance pledgedBalance : CollectionUtils.getIterable(getCollateralBalancePledgedBalanceList(context.getCollateralBalancePledgedBalanceSearchForm()))) {
				CollateralBalancePledgedCollateralValue pledgedCollateralValue = pledgedCollateralValueMap.get(pledgedBalance.getPostedCollateralInvestmentSecurity().getId());
				if (pledgedCollateralValue == null) {
					pledgedCollateralValue = new CollateralBalancePledgedCollateralValue();
					pledgedCollateralValue.setCollateralSecurity(pledgedBalance.getPostedCollateralInvestmentSecurity());
					//We assume this is not cash, but a fully pledged security
					//All cash should never be pledged, so this 'should' be safe since the original cash value should be found in the map
					pledgedCollateralValue.setCashCollateral(false);
					//And, since the value wasn't found in the map, we know it is fully pledged, so original and remaining quantity is set to ZERO
					pledgedCollateralValue.setOriginalQuantity(BigDecimal.ZERO);
					pledgedCollateralValue.setRemainingQuantity(BigDecimal.ZERO);
				}
				//We now sum the pledgedQuantity since multiple positions may be pledged with this collateralSecurity
				pledgedCollateralValue.setPledgedQuantity(MathUtils.add(pledgedCollateralValue.getPledgedQuantity(), pledgedBalance.getPostedCollateralQuantity()));
				//We also store the individual pledged quantities for each position we've pledged so we can determine if collateral was moved later
				pledgedCollateralValue.getOriginalPositionSecurityQuantityMap().put(pledgedBalance.getPositionInvestmentSecurity().getId(), pledgedBalance.getPostedCollateralQuantity());
				pledgedCollateralValue.getPledgedPositionSecurityQuantityMap().put(pledgedBalance.getPositionInvestmentSecurity().getId(), pledgedBalance.getPostedCollateralQuantity());
				pledgedCollateralValueMap.put(pledgedBalance.getPostedCollateralInvestmentSecurity().getId(), pledgedCollateralValue);
			}

			List<Integer> optionTradeIdList = new ArrayList<>();
			//We only look for security maturity events and bond/fund trades if there is not any pledged collateral for the current day already.
			//This is because these trigger collateral to be dynamically returned between positions in the collateral value map, and we don't want
			//to return them if they were previously handled that day or if can result in collateral being constantly shifted for no reason.
			//During validation while saving the pledged transactions we DO NOT process the maturity events because the pledged transactions
			//will already account for the returns, if we process, then validation will fail when attempting to return the collateral again.
			List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedTransactionList(context.getCollateralBalancePledgedTransactionSearchForm(true));
			if (CollectionUtils.getSize(pledgedTransactionList) == 0 && !skipMaturityEventAndTradeProcessing) {
				if (!ArrayUtils.isEmpty(context.getPledgedSecurityIds())) {
					//First locate maturity events - we need to 'un-pledge' any and all collateral that is pledged for a bond that has matured.
					List<InvestmentSecurityEvent> securityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(context.getInvestmentSecurityEventSearchForm());
					for (InvestmentSecurityEvent securityEvent : CollectionUtils.getIterable(securityEventList)) {
						CollateralBalancePledgedCollateralValue pledgedCollateralValue = pledgedCollateralValueMap.get(securityEvent.getSecurity().getId());
						if (pledgedCollateralValue != null && MathUtils.isGreaterThan(pledgedCollateralValue.getPledgedQuantity(), BigDecimal.ZERO)) {
							pledgedCollateralValue.returnAllCollateral();
						}
					}
				}

				//TODO - once pending positions/transactions functionality is in place we want to remove the dependency on trade.
				//Now locate 'sells' of bonds or funds - we need to return pledged collateral up to the quantity of the trade
				TradeType bondsTradeType = getTradeService().getTradeTypeByName(TradeType.BONDS);
				TradeType fundsTradeType = getTradeService().getTradeTypeByName(TradeType.FUNDS);
				List<Trade> collateralTradeList = getTradeService().getTradeList(context.getCollateralTradeSearchForm(new Short[]{bondsTradeType.getId(), fundsTradeType.getId()}));
				for (Trade collateralTrade : CollectionUtils.getIterable(collateralTradeList)) {
					CollateralBalancePledgedCollateralValue pledgedCollateralValue = pledgedCollateralValueMap.get(collateralTrade.getInvestmentSecurity().getId());
					if (pledgedCollateralValue != null) {
						pledgedCollateralValue.returnCollateral(collateralTrade.getQuantity());
						//Since we returned collateral, we need to add all options positions that have been pledged to by this
						//to the optionsTradeIdList so that they will be pledged at the 100% threshold instead of (1 - requiredCollateralThreshold) (default of .15))
						optionTradeIdList.addAll(pledgedCollateralValue.getPositionSecurityIdSet());
					}
				}
				//Now locate buys/sells of options for the balance date, traded options must be fully collateralized, rather than checked against the default 15% threshold.
				TradeType optionsTradeType = getTradeService().getTradeTypeByName(TradeType.OPTIONS);
				List<Trade> optionsTradeList = getTradeService().getTradeList(context.getOptionsTradeSearchForm(new Short[]{optionsTradeType.getId()}));
				optionTradeIdList.addAll(optionsTradeList.stream().map(optionsTrade -> optionsTrade.getInvestmentSecurity().getId()).collect(Collectors.toList()));
			}
			context.setOptionTradeIdList(optionTradeIdList);
			context.setPledgedCollateralValueMap(pledgedCollateralValueMap);
		}
		return context;
	}


	@Override
	@Transactional
	public void uploadCollateralBalancePledgedTransactionsFile(CollateralBalancePledgedTransactionCommand command) {
		try {
			//Uploading a file forces clearing of existing transactions for this balance
			Status status = clearCollateralBalancePledgedTransactionList(command);
			CollateralBalance balance = getCollateralBalanceService().getCollateralBalance(command.getCollateralBalanceId());
			if (!CollectionUtils.isEmpty(status.getErrorList())) {
				throw new ValidationException("Failed to clear existing pledged transactions; you must manually clear or reset the workflow before you can upload transactions.");
			}
			//Initialize the context;
			CollateralBalancePledgedServiceContext context = initializeContext(command.getCollateralBalanceId(), true, true);

			//Now process the uploaded file
			DataTable dataTable = DataTableUtils.convertFileToDataTable(command.getFile());
			List<String> columnNameList = Arrays.stream(dataTable.getColumnList()).map(DataColumn::getColumnName).collect(Collectors.toList());
			AssertUtils.assertTrue(columnNameList.containsAll(UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST), "Uploaded file does not contain all required columns of: " + UPLOAD_PLEDGED_TRANSACTION_COLUMN_NAME_LIST);
			for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
				Map<String, Object> objectDataMap = dataTable.getRow(i).getRowValueMap();
				CollateralBalancePledgedTransaction pledgedTransaction = new CollateralBalancePledgedTransaction();
				pledgedTransaction.setPostedDate(balance.getBalanceDate());
				pledgedTransaction.setHoldingInvestmentAccount(balance.getHoldingInvestmentAccount());
				pledgedTransaction.setPositionInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(String.valueOf(objectDataMap.get(UPLOAD_PLEDGED_TRANSACTION_POSITION_SECURITY_COLUMN_NAME)), false));
				pledgedTransaction.setPostedCollateralInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(String.valueOf(objectDataMap.get(UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_SECURITY_COLUMN_NAME)), false));
				pledgedTransaction.setPostedCollateralQuantity(new BigDecimal(String.valueOf(objectDataMap.get(UPLOAD_PLEDGED_TRANSACTION_COLLATERAL_QUANTITY_COLUMN_NAME))));
				validateAndSaveCollateralBalancePledgedTransaction(context, pledgedTransaction);
			}
		}
		catch (Throwable e) {
			if (command.getFile() != null) {
				throw new RuntimeException("Failed to upload pledged transactions from file [" + command.getFile().getOriginalFilename() + "].", e);
			}
			throw new RuntimeException("Failed to upload pledged transactions.", e);
		}
	}


	/**
	 * Retrieves or generates pledged collateral transactions for the specified balance date
	 */
	@Override
	public List<CollateralBalancePledgedTransaction> getCollateralBalancePledgedTransactionListByCommand(CollateralBalancePledgedTransactionCommand command) {
		CollateralBalancePledgedServiceContext context = initializeContext(command.getCollateralBalanceId(), command.isGeneratePledgedCollateral(), false);

		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedTransactionList(context.getCollateralBalancePledgedTransactionSearchForm(command.getTransferNotBooked()));
		if (command.isGeneratePledgedCollateral()) {
			if (command.isAutomatic() && !CollectionUtils.isEmpty(pledgedTransactionList)) {
				//If we are generating via an automatic transition and the pledged transaction list is not empty
				//then that means the pledged collateral was generated manually - so we return an empty list
				//and just allow the transition to happen so we don't duplicate the pledged transactions.
				return new ArrayList<>();
			}
			//Now we process returns and posting of collateral - first we need to compute required collateral
			//by checking what is already pledged and what is required - we also set the threshold used for determining
			//whether a position should be processed.  If a position was traded that day it needs to be fully collateralized
			//otherwise it falls under the maintenance rule and is only processed and fully collateralized if under that threshold.
			BigDecimal requiredCollateralThreshold = context.getRebuildProcessor().getRequiredCollateralThreshold();
			if (requiredCollateralThreshold == null) {
				requiredCollateralThreshold = DEFAULT_REQUIRED_COLLATERAL_THRESHOLD;
			}
			Map<Integer, BigDecimal> pledgedThresholdCollateralByPositionMap = new HashMap<>();
			Map<Integer, BigDecimal> remainingRequiredCollateralByPositionMap = new HashMap<>();
			List<AccountingPositionDaily> accountingPositionDailyList = getCollateralBalanceService().getCollateralAccountingPositionDailyLiveList(context.getRebuildProcessor(), context.getHoldingAccount().getId(), context.getPostedDate(), false, true);
			//If the maintenance threshold is 0, then we only only process positions that have had collateral returned.
			//This is a special case for the Guidestone accounts in which trades are managed by Seattle
			if (MathUtils.isEqual(requiredCollateralThreshold, BigDecimal.ZERO)) {
				accountingPositionDailyList = processReturnedCollateralOnly(context, accountingPositionDailyList);
			}
			for (AccountingPositionDaily positionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
				InvestmentSecurity positionSecurity = positionDaily.getAccountingTransaction().getInvestmentSecurity();
				BigDecimal pledgedCollateral = getPledgedCollateralForPosition(context, positionSecurity);
				BigDecimal remainingRequiredCollateral = MathUtils.subtract(positionDaily.getRequiredCollateralBase(), pledgedCollateral);

				//If an option was traded today the threshold is zero because we need to fully fund or return;
				// otherwise we look to see if it deviates by requiredCollateralThreshold (default .15) since we
				// need to maintain collateral above that threshold (default 85%)
				BigDecimal pledgeThreshold = BigDecimal.ZERO;
				if (!CollectionUtils.contains(context.getOptionTradeIdList(), positionSecurity.getId())) {
					pledgeThreshold = MathUtils.getPercentageOf(requiredCollateralThreshold, positionDaily.getRequiredCollateralBase());
				}
				remainingRequiredCollateralByPositionMap.put(positionSecurity.getId(), remainingRequiredCollateral);
				pledgedThresholdCollateralByPositionMap.put(positionSecurity.getId(), pledgeThreshold);
			}

			//Sort the position list by required collateral - negative collateral means we need to return.
			//We need to process those transactions first so the returned collateral is available to be pledged to any remaining positions.
			remainingRequiredCollateralByPositionMap = sortMap(remainingRequiredCollateralByPositionMap);
			for (Map.Entry<Integer, BigDecimal> remainingRequiredCollateralByPositionEntry : CollectionUtils.getIterable(remainingRequiredCollateralByPositionMap.entrySet())) {
				Integer positionSecurityId = remainingRequiredCollateralByPositionEntry.getKey();
				BigDecimal remainingRequiredCollateral = remainingRequiredCollateralByPositionEntry.getValue();
				if (MathUtils.isLessThan(remainingRequiredCollateral, MathUtils.negate(pledgedThresholdCollateralByPositionMap.get(positionSecurityId)))) {
					//We send the absolute value of the collateral so that we can consistently process regardless of being a post or return.
					processCollateralRequirement(context, MathUtils.abs(remainingRequiredCollateral), positionSecurityId, true);
				}
				else if (MathUtils.isGreaterThan(remainingRequiredCollateral, pledgedThresholdCollateralByPositionMap.get(positionSecurityId))) {
					processCollateralRequirement(context, remainingRequiredCollateral, positionSecurityId, false);
				}
			}
			pledgedTransactionList = createPledgedTransactions(context);
		}
		else {
			for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
				pledgedTransaction.setPostedCollateralMarketValue(getPledgedCollateralMarketValue(pledgedTransaction.getPostedCollateralInvestmentSecurity(), pledgedTransaction.getPostedCollateralQuantity(), pledgedTransaction.getPostedDate()));
			}
		}
		return pledgedTransactionList;
	}


	private List<AccountingPositionDaily> processReturnedCollateralOnly(CollateralBalancePledgedServiceContext context, List<AccountingPositionDaily> accountingPositionDailyList) {
		List<Integer> positionsSecuritiesWithCollateralReturned = new ArrayList<>();
		CollateralBalancePledgedTransactionSearchForm searchForm = new CollateralBalancePledgedTransactionSearchForm();
		searchForm.setPostedDate(context.getPostedDate());
		searchForm.setHoldingInvestmentAccountId(context.getHoldingAccount().getId());
		searchForm.setTransferNotBooked(false);
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedTransactionList(searchForm);
		for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
			//Ensure it was a return
			if (MathUtils.isLessThan(pledgedTransaction.getPostedCollateralQuantity(), BigDecimal.ZERO)) {
				positionsSecuritiesWithCollateralReturned.add(pledgedTransaction.getPositionInvestmentSecurity().getId());
			}
		}
		return accountingPositionDailyList.stream().filter(positionDaily -> positionsSecuritiesWithCollateralReturned.contains(positionDaily.getInvestmentSecurity().getId())).collect(Collectors.toList());
	}


	//Grabbed this from stack overflow - could be made more generic and added to MapUtils.
	private Map<Integer, BigDecimal> sortMap(Map<Integer, BigDecimal> passedMap) {
		List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
		List<BigDecimal> mapValues = new ArrayList<>(passedMap.values());
		Collections.sort(mapValues);
		Collections.sort(mapKeys);
		LinkedHashMap<Integer, BigDecimal> sortedMap = new LinkedHashMap<>();
		for (BigDecimal val : mapValues) {
			Iterator<Integer> keyIt = mapKeys.iterator();
			while (keyIt.hasNext()) {
				Integer key = keyIt.next();
				BigDecimal comp1 = passedMap.get(key);
				if (MathUtils.isEqual(comp1, val)) {
					keyIt.remove();
					sortedMap.put(key, val);
					break;
				}
			}
		}
		return sortedMap;
	}


	private BigDecimal getPledgedCollateralForPosition(CollateralBalancePledgedServiceContext context, InvestmentSecurity positionSecurity) {
		BigDecimal pledgedCollateral = BigDecimal.ZERO;
		for (CollateralBalancePledgedCollateralValue pledgedCollateralValue : CollectionUtils.getIterable(context.getPledgedCollateralValueMap().values())) {
			BigDecimal pledgedQuantity = pledgedCollateralValue.getPledgedPositionSecurityQuantityMap().get(positionSecurity.getId());
			if (!MathUtils.isNullOrZero(pledgedQuantity)) {
				pledgedCollateral = MathUtils.add(pledgedCollateral, getPledgedCollateralMarketValue(pledgedCollateralValue.getCollateralSecurity(), pledgedQuantity, context.getPostedDate()));
			}
		}
		return pledgedCollateral;
	}


	private void processCollateralRequirement(CollateralBalancePledgedServiceContext context, BigDecimal requiredCollateral, Integer positionSecurityId, boolean returnCollateral) {
		InvestmentSecurity positionSecurity = getInvestmentInstrumentService().getInvestmentSecurity(positionSecurityId);
		processCollateralRequirement(context, positionSecurity, requiredCollateral, returnCollateral);
	}


	private void processCollateralRequirement(CollateralBalancePledgedServiceContext context, InvestmentSecurity positionSecurity, BigDecimal requiredCollateral, boolean returnCollateral) {
		CollateralBalancePledgedCollateralValue pledgedCollateralValue = getPledgedCollateralValueForPosition(context, positionSecurity, returnCollateral);
		if (pledgedCollateralValue != null) {
			//We compute the collateralQuantity and set it to the returnVal of the return/post functions because in a return the actual value may be less than what is passed.
			BigDecimal collateralQuantity = getCollateralQuantity(context, pledgedCollateralValue, positionSecurity, requiredCollateral, returnCollateral);
			if (returnCollateral) {
				collateralQuantity = pledgedCollateralValue.returnCollateralForPosition(positionSecurity.getId(), collateralQuantity);
			}
			else {
				collateralQuantity = pledgedCollateralValue.postCollateral(positionSecurity.getId(), collateralQuantity);
			}
			//Check to ensure no collateral is still required; if it is, recursively call this method until complete.
			//We don't do the threshold check here - only at the beginning - once we determine to process we pledge for it all
			//If cash was pledged we simply use the quantity for the market value, otherwise compute it.
			if (pledgedCollateralValue.isCashCollateral()) {
				requiredCollateral = MathUtils.subtract(requiredCollateral, collateralQuantity);
			}
			else {
				BigDecimal postedCollateralMarketValue = getPledgedCollateralMarketValue(pledgedCollateralValue.getCollateralSecurity(), collateralQuantity, context.getPostedDate());
				requiredCollateral = MathUtils.subtract(requiredCollateral, postedCollateralMarketValue);
			}
			if (MathUtils.isGreaterThan(requiredCollateral, BigDecimal.ZERO)) {
				processCollateralRequirement(context, positionSecurity, requiredCollateral, returnCollateral);
			}
		}
		else {
			//If no eligible collateral was found, then we add the position to the unpledgedCollateralSet because we still need to create
			//a transaction for it so Ops is aware that there is a position that failed to be collateralized which they will need to deal with.
			//This may occur due to trading making a mistake, or also due to a configuration issue on the account that was too restrictive
			//in defining what collateral is allowed to be pledged.
			context.getUnpledgedCollateralSet().add(positionSecurity.getId());
		}
	}


	private CollateralBalancePledgedCollateralValue getPledgedCollateralValueForPosition(CollateralBalancePledgedServiceContext context, InvestmentSecurity positionSecurity, boolean returnCollateral) {
		CollateralBalancePledgedCollateralValue pledgedCollateralValueForPosition = null;
		//Iterate available collateral and grab the first usable collateral security
		for (CollateralBalancePledgedCollateralValue pledgedCollateralValue : CollectionUtils.getIterable(context.getPledgedCollateralValueMap().values())) {
			if (returnCollateral) {
				BigDecimal pledgedQuantityForPositionSecurity = pledgedCollateralValue.getPledgedPositionSecurityQuantityMap().get(positionSecurity.getId());
				if (MathUtils.isGreaterThan(pledgedQuantityForPositionSecurity, BigDecimal.ZERO)) {
					pledgedCollateralValueForPosition = pledgedCollateralValue;
					break;
				}
			}
			else {
				if (!pledgedCollateralValue.isCashCollateral()) {
					BigDecimal remainingQuantity = pledgedCollateralValue.getRemainingQuantity();
					if (MathUtils.isGreaterThan(remainingQuantity, BigDecimal.ZERO)) {
						Short[] allowedInvestmentTypeIds = positionSecurity.isPutOption() ? context.getRebuildProcessor().getAllowedPutCollateralInvestmentTypeIds() : context.getRebuildProcessor().getAllowedCallCollateralInvestmentTypeIds();
						if (ArrayUtils.contains(allowedInvestmentTypeIds, pledgedCollateralValue.getCollateralSecurity().getInstrument().getHierarchy().getInvestmentType().getId())) {    //Now we ensure the security here is not restricted as collateral for this processor.
							Short[] restrictedInvestmentTypeIds = positionSecurity.isPutOption() ? context.getRebuildProcessor().getRestrictedPutCollateralInvestmentTypeIds() : context.getRebuildProcessor().getRestrictedCallCollateralInvestmentTypeIds();
							if (!ArrayUtils.contains(restrictedInvestmentTypeIds, pledgedCollateralValue.getCollateralSecurity().getInstrument().getHierarchy().getInvestmentType().getId())) {
								pledgedCollateralValueForPosition = pledgedCollateralValue;
							}
							break;
						}
					}
				}
			}
		}
		//If we didn't find a security that could be pledged, then we need to use cash for collateral
		if (pledgedCollateralValueForPosition == null) {
			for (CollateralBalancePledgedCollateralValue pledgedCollateralValue : CollectionUtils.getIterable(context.getPledgedCollateralValueMap().values())) {
				if (pledgedCollateralValue.isCashCollateral()) {
					if (MathUtils.isGreaterThan(pledgedCollateralValue.getRemainingQuantity(), BigDecimal.ZERO)) {
						Short[] subType2Ids = positionSecurity.isPutOption() ? context.getRebuildProcessor().getRestrictedPutCollateralInvestmentTypeIds() : context.getRebuildProcessor().getRestrictedCallCollateralInvestmentTypeIds();
						if (!ArrayUtils.contains(subType2Ids, pledgedCollateralValue.getCollateralSecurity().getInstrument().getHierarchy().getInvestmentType().getId())) {
							pledgedCollateralValueForPosition = pledgedCollateralValue;
						}
						break;
					}
				}
			}
		}
		return pledgedCollateralValueForPosition;
	}


	private BigDecimal getCollateralQuantity(CollateralBalancePledgedServiceContext context, CollateralBalancePledgedCollateralValue pledgedCollateralValue, InvestmentSecurity positionSecurity, BigDecimal requiredCollateral, boolean returnCollateral) {
		InvestmentSecurity collateralSecurity = pledgedCollateralValue.getCollateralSecurity();
		BigDecimal remainingQuantity = returnCollateral ? pledgedCollateralValue.getPledgedPositionSecurityQuantityMap().get(positionSecurity.getId()) : pledgedCollateralValue.getRemainingQuantity();

		BigDecimal collateralQuantity;
		BigDecimal roundingValue = context.getRebuildProcessor().getCallRoundingValue();
		BigDecimal roundingThreshold = context.getRebuildProcessor().getCallRoundingThreshold();
		if (pledgedCollateralValue.isCashCollateral()) {
			collateralQuantity = requiredCollateral;
		}
		else {
			//Compute the required collateral quantity and collateralMarketValue
			BigDecimal price = MathUtils.multiply(getMarketDataRetriever().getPrice(collateralSecurity, context.getBalanceDate(),
					true, "Failed to locate price for security: " + collateralSecurity.getSymbol()), collateralSecurity.getPriceMultiplier());
			collateralQuantity = MathUtils.divide(requiredCollateral, price);
			//Apply rounding, then add to the list
			if (positionSecurity.isPutOption()) {
				roundingValue = context.getRebuildProcessor().getPutRoundingValue();
				roundingThreshold = context.getRebuildProcessor().getPutRoundingThreshold();
			}
		}

		//The value may be negative here (e.g. on a return), but when we
		//transfer collateral one way or the other the value must always positive
		//If we are pledging cash, then we don't round the values.
		collateralQuantity = MathUtils.abs(collateralQuantity);
		if (MathUtils.isGreaterThan(collateralQuantity, remainingQuantity)) {
			collateralQuantity = pledgedCollateralValue.isCashCollateral() ? remainingQuantity : roundAmount(remainingQuantity, roundingValue, roundingThreshold);
		}
		else {
			collateralQuantity = pledgedCollateralValue.isCashCollateral() ? collateralQuantity : roundAmount(collateralQuantity, roundingValue, roundingThreshold);
		}
		return collateralQuantity;
	}


	private BigDecimal roundAmount(BigDecimal amount, BigDecimal roundingValue, BigDecimal roundingThreshold) {
		// If either number to round or rounding is null or 0, then no rounding needs to take place
		if (MathUtils.isNullOrZero(amount) || MathUtils.isNullOrZero(roundingValue)) {
			return amount;
		}
		// Divide Amount By Rounding Value and multiply by roundingValue to get rounded amount
		BigDecimal roundedAmount = MathUtils.multiply(roundingValue, MathUtils.round(MathUtils.divide(amount, roundingValue), 0, BigDecimal.ROUND_UP));
		//Now if the difference between the rounded amount and the original amount is less than the roundingThreshold then add the roundingValue to the rounded amount
		// (e.g. if we rounded 2800 to nearest 1000 then the difference would be 200 which is under the default threshold of 300 for puts, so we add an additional 1000
		if (MathUtils.isLessThan(MathUtils.subtract(roundedAmount, amount), roundingThreshold)) {
			roundedAmount = MathUtils.add(roundedAmount, roundingValue);
		}
		return roundedAmount;
	}


	private List<CollateralBalancePledgedTransaction> createPledgedTransactions(CollateralBalancePledgedServiceContext context) {
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = new ArrayList<>();
		for (CollateralBalancePledgedCollateralValue pledgedCollateralValue : CollectionUtils.getIterable(context.getPledgedCollateralValueMap().values())) {
			//Only process when there is some overall collateral change
			if (pledgedCollateralValue.hasCollateralMovements()) {
				for (Integer positionSecurityId : CollectionUtils.getIterable(pledgedCollateralValue.getPositionSecurityIdSet())) {
					//Only create a transaction when there is some overall collateral change for the position
					if (!MathUtils.isNullOrZero(pledgedCollateralValue.getPledgedTransactionPositionSecurityQuantity(positionSecurityId))) {
						CollateralBalancePledgedTransaction pledgedTransaction = new CollateralBalancePledgedTransaction();
						pledgedTransaction.setPostedDate(context.getPostedDate());
						pledgedTransaction.setHoldingInvestmentAccount(context.getHoldingAccount());
						pledgedTransaction.setPositionInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(positionSecurityId));
						pledgedTransaction.setPostedCollateralInvestmentSecurity(pledgedCollateralValue.getCollateralSecurity());
						pledgedTransaction.setPostedCollateralQuantity(pledgedCollateralValue.getPledgedTransactionPositionSecurityQuantity(positionSecurityId));
						if (pledgedCollateralValue.isCashCollateral()) {
							pledgedTransaction.setPostedCollateralMarketValue(pledgedTransaction.getPostedCollateralQuantity());
						}
						else {
							pledgedTransaction.setPostedCollateralMarketValue(getPledgedCollateralMarketValue(pledgedTransaction.getPostedCollateralInvestmentSecurity().getId(), pledgedTransaction.getPostedCollateralQuantity(), pledgedTransaction.getPostedDate()));
						}
						pledgedTransactionList.add(pledgedTransaction);
					}
				}
			}
		}
		//Now create dummy transactions for any unpledged collateral
		for (Integer positionSecurityId : CollectionUtils.getIterable(context.getUnpledgedCollateralSet())) {
			CollateralBalancePledgedTransaction pledgedTransaction = new CollateralBalancePledgedTransaction();
			pledgedTransaction.setPostedDate(context.getPostedDate());
			pledgedTransaction.setHoldingInvestmentAccount(context.getHoldingAccount());
			pledgedTransaction.setPositionInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(positionSecurityId));
			pledgedTransactionList.add(pledgedTransaction);
		}
		return pledgedTransactionList;
	}


	@Override
	@Transactional
	public Status clearCollateralBalancePledgedTransactionList(CollateralBalancePledgedTransactionCommand command) {
		Status status = Status.ofMessage("Clear Collateral Balance Pledged Transaction Results");
		HashSet<Integer> positionTransferIdSet = new HashSet<>();
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedTransactionListByCommand(command);
		for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
			boolean allowDeletion = true;
			AccountingPositionTransfer positionTransfer = pledgedTransaction.getPositionTransfer();
			if (positionTransfer != null) {
				allowDeletion = false;
				if (positionTransfer.getBookingDate() != null) {
					status.addError("Cannot delete pledged transaction with id [" + pledgedTransaction.getId() + "].\nYou must first un-book the position transfer: " + positionTransfer.getId());
				}
				else {
					positionTransferIdSet.add(positionTransfer.getId());
					allowDeletion = true;
				}
			}
			if (allowDeletion) {
				getCollateralBalancePledgedTransactionDAO().delete(pledgedTransaction.getId());
				status.addMessage("Successfully deleted pledged transaction with id [" + pledgedTransaction.getId() + "].");
			}
		}
		for (Integer positionTransferId : CollectionUtils.getIterable(positionTransferIdSet)) {
			getAccountingPositionTransferService().deleteAccountingPositionTransfer(positionTransferId);
		}
		return status;
	}


	@Override
	@Transactional
	public void saveCollateralBalancePledgedTransactionListByCommand(int collateralBalanceId, BeanListCommand<CollateralBalancePledgedTransactionCommand> command) {
		CollateralBalancePledgedServiceContext context = initializeContext(collateralBalanceId, true, true);
		//Sort the list because we will validate the moves the transactions indicate and we need to process the 'returns' first.
		List<CollateralBalancePledgedTransactionCommand> pledgedTransactionCommandList = BeanUtils.sortWithFunction(command.getBeanList(), CollateralBalancePledgedTransactionCommand::getPostedCollateralQuantity, true);
		for (CollateralBalancePledgedTransactionCommand pledgedTransactionCommand : CollectionUtils.getIterable(pledgedTransactionCommandList)) {
			//If ops zeroes out the quantity of an entry for whatever reason then we don't want to save it so we ignore those
			if (!MathUtils.isNullOrZero(pledgedTransactionCommand.getPostedCollateralQuantity())) {
				CollateralBalancePledgedTransaction pledgedTransaction = new CollateralBalancePledgedTransaction();
				pledgedTransaction.setPostedDate(pledgedTransactionCommand.getPostedDate());
				pledgedTransaction.setHoldingInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(pledgedTransactionCommand.getHoldingInvestmentAccountId()));
				pledgedTransaction.setPositionInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(pledgedTransactionCommand.getPositionInvestmentSecurityId()));
				pledgedTransaction.setPostedCollateralInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(pledgedTransactionCommand.getPostedCollateralInvestmentSecurityId()));
				pledgedTransaction.setPostedCollateralQuantity(pledgedTransactionCommand.getPostedCollateralQuantity());
				validateAndSaveCollateralBalancePledgedTransaction(context, pledgedTransaction);
			}
		}
	}


	private void validateAndSaveCollateralBalancePledgedTransaction(CollateralBalancePledgedServiceContext context, CollateralBalancePledgedTransaction pledgedTransaction) {
		validatePledgedTransaction(context, pledgedTransaction);
		saveCollateralBalancePledgedTransaction(pledgedTransaction);
	}


	private void validatePledgedTransaction(CollateralBalancePledgedServiceContext context, CollateralBalancePledgedTransaction pledgedTransaction) {
		BigDecimal postedCollateralQuantity = pledgedTransaction.getPostedCollateralQuantity();
		InvestmentSecurity positionSecurity = pledgedTransaction.getPositionInvestmentSecurity();
		InvestmentSecurity postedCollateralSecurity = pledgedTransaction.getPostedCollateralInvestmentSecurity();
		try {
			//Validate that the posted collateral for the position is not restricted - this will catch when entries were manually created incorrectly
			Short[] allowedInvestmentTypeIds = positionSecurity.isPutOption() ? context.getRebuildProcessor().getAllowedPutCollateralInvestmentTypeIds() : context.getRebuildProcessor().getAllowedCallCollateralInvestmentTypeIds();
			if (!ArrayUtils.contains(allowedInvestmentTypeIds, postedCollateralSecurity.getInstrument().getHierarchy().getInvestmentType().getId())) {
				throw new ValidationException("The type of collateral posted [" + postedCollateralSecurity.getInstrument().getHierarchy().getInvestmentType().getName() + "] is not allowed for this type of option [" + positionSecurity.getOptionType() + "]");
			}
			Short[] restrictedInvestmentTypeIds = positionSecurity.isPutOption() ? context.getRebuildProcessor().getRestrictedPutCollateralInvestmentTypeIds() : context.getRebuildProcessor().getRestrictedCallCollateralInvestmentTypeIds();
			if (ArrayUtils.contains(restrictedInvestmentTypeIds, postedCollateralSecurity.getInstrument().getHierarchy().getInvestmentType().getId())) {
				throw new ValidationException("The type of collateral posted [" + postedCollateralSecurity.getInstrument().getHierarchy().getInvestmentType().getName() + "] is restricted for this type of option [" + positionSecurity.getOptionType() + "]");
			}

			//Attempt to post or return the collateral - this will catch when values were manually updated incorrectly.
			CollateralBalancePledgedCollateralValue pledgedCollateralValue = context.getPledgedCollateralValueMap().get(postedCollateralSecurity.getId());
			if (MathUtils.isGreaterThan(postedCollateralQuantity, BigDecimal.ZERO)) {
				BigDecimal pledgedQuantity = pledgedCollateralValue.postCollateral(positionSecurity.getId(), postedCollateralQuantity);
				//Validation to deal with manually manipulation of quantities
				if (MathUtils.isNotEqual(postedCollateralQuantity, pledgedQuantity)) {
					throw new ValidationException("Actual pledged quantity [" + MathUtils.abs(pledgedQuantity) + " does not match intended pledged quantity [" + MathUtils.abs(postedCollateralQuantity) + "]");
				}
			}
			else {
				//The quantity is negative here so we know it is a return, but when we actually process the return we pass the value as a positive quantity.
				BigDecimal returnedQuantity = pledgedCollateralValue.returnCollateralForPosition(positionSecurity.getId(), MathUtils.abs(postedCollateralQuantity));
				//Validation to deal with manually manipulation of quantities
				if (MathUtils.isNotEqual(returnedQuantity, MathUtils.abs(postedCollateralQuantity))) {
					throw new ValidationException("Actual returned quantity [" + MathUtils.abs(returnedQuantity) + " does not match intended returned quantity [" + MathUtils.abs(postedCollateralQuantity) + "]");
				}
			}
		}
		catch (Exception e) {
			throw new ValidationException("Failed to create pledged transaction.  Attempt to pledge [" + postedCollateralQuantity + "] of [" +
					postedCollateralSecurity.getSymbol() + "] for position [" + positionSecurity.getSymbol() + "] resulted in the error: " + e.getMessage());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Position Transfer Business Methods    ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void createCollateralBalancePositionTransfers(CollateralBalancePledgedTransactionCommand command) {
		CollateralBalancePledgedServiceContext context = initializeContext(command.getCollateralBalanceId(), command.isGeneratePledgedCollateral(), false);
		//Get unbooked transaction
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedTransactionList(context.getCollateralBalancePledgedTransactionSearchForm(true));
		if (CollectionUtils.getSize(pledgedTransactionList) > 0) {
			if (command.isPostCollateral()) {
				List<CollateralBalancePledgedTransaction> returnCollateralPledgedTransactionList =
						pledgedTransactionList.stream().filter(pledgedTransaction -> MathUtils.isLessThan(pledgedTransaction.getPostedCollateralQuantity(), BigDecimal.ZERO)
								&& (pledgedTransaction.getPositionTransfer() == null || (pledgedTransaction.getPositionTransfer() != null && pledgedTransaction.getPositionTransfer().getBookingDate() == null))).collect(Collectors.toList());
				ValidationUtils.assertTrue(CollectionUtils.isEmpty(returnCollateralPledgedTransactionList), "You cannot post collateral while there are unbooked collateral returns.");
				pledgedTransactionList = pledgedTransactionList.stream().filter(pledgedTransaction -> MathUtils.isGreaterThanOrEqual(pledgedTransaction.getPostedCollateralQuantity(), BigDecimal.ZERO)).collect(Collectors.toList());
			}
			else {
				pledgedTransactionList = pledgedTransactionList.stream().filter(pledgedTransaction -> MathUtils.isLessThan(pledgedTransaction.getPostedCollateralQuantity(), BigDecimal.ZERO)).collect(Collectors.toList());
			}
			createCollateralBalancePositionTransfers(command, context, pledgedTransactionList);
		}
	}


	private void createCollateralBalancePositionTransfers(CollateralBalancePledgedTransactionCommand command, CollateralBalancePledgedServiceContext context, List<CollateralBalancePledgedTransaction> pledgedTransactionList) {
		List<AccountingPositionClosing> positionClosingList = getCollateralPositionClosingList(command, context, pledgedTransactionList);
		List<Integer> securityIdList = pledgedTransactionList.stream().map(transaction -> transaction.getPostedCollateralInvestmentSecurity().getId()).distinct().collect(Collectors.toList());
		for (Integer securityId : CollectionUtils.getIterable(securityIdList)) {
			List<CollateralBalancePledgedTransaction> securityPledgedTransactionList = pledgedTransactionList.stream().filter(transaction -> transaction.getPostedCollateralInvestmentSecurity().getId().equals(securityId)).collect(Collectors.toList());
			if (CollectionUtils.getSize(securityPledgedTransactionList) > 0) {
				CollateralBalancePledgedTransaction securityPledgedTransaction = CollectionUtils.getFirstElementStrict(securityPledgedTransactionList);
				String investmentTypeName = securityPledgedTransaction.getPostedCollateralInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName();
				AccountingPositionTransfer positionTransfer = getAccountingPositionTransfer(command, context, securityPledgedTransaction);
				if ("Currency".equals(investmentTypeName) && CollectionUtils.getSize(securityPledgedTransactionList) > 0) {
					positionTransfer.setDetailList(getCashTransferDetailList(context, securityPledgedTransactionList));
					positionTransfer.setType(getAccountingPositionTransferService().getAccountingPositionTransferTypeByName(command.isPostCollateral() ? AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_POST : AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_RETURN));
					positionTransfer.setNote((command.isPostCollateral() ? "Posting " : "Returning ") + "Cash Collateral");
				}
				else {
					List<AccountingPositionClosing> investmentTypePositionClosingList = positionClosingList.stream().filter(position -> position.getInvestmentSecurity().getId().equals(securityId)).collect(Collectors.toList());
					if (CollectionUtils.getSize(investmentTypePositionClosingList) > 0) {
						List<AccountingPositionTransferDetail> detailList = getPositionTransferDetailList(investmentTypePositionClosingList);
						positionTransfer.setDetailList(detailList);
						positionTransfer.setType(getAccountingPositionTransferService().getAccountingPositionTransferTypeByName(command.isPostCollateral() ? AccountingPositionTransferType.COLLATERAL_TRANSFER_POST : AccountingPositionTransferType.COLLATERAL_TRANSFER_RETURN));
						positionTransfer.setNote((command.isPostCollateral() ? "Posting " : "Returning ") + "Position Collateral");
					}
				}

				//Check for system condition and add to the transfer if it exists;
				Integer entityModifyConditionId = context.getRebuildProcessor().getEntityModifyConditionId();
				if (entityModifyConditionId != null) {
					positionTransfer.setEntityModifyCondition(getSystemConditionService().getSystemCondition(entityModifyConditionId));
				}
				saveCollateralBalancePositionTransfer(command, positionTransfer, securityPledgedTransactionList);
			}
		}
	}


	private List<AccountingPositionClosing> getCollateralPositionClosingList(CollateralBalancePledgedTransactionCommand command, CollateralBalancePledgedServiceContext context, List<CollateralBalancePledgedTransaction> pledgedTransactionList) {
		//First find and then sum the pledged quantities by security
		Map<InvestmentSecurity, BigDecimal> collateralQuantityBySecurityMap = new HashMap<>();
		for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
			BigDecimal collateralQuantityBySecurity = collateralQuantityBySecurityMap.get(pledgedTransaction.getPostedCollateralInvestmentSecurity());
			if (collateralQuantityBySecurity == null) {
				collateralQuantityBySecurity = BigDecimal.ZERO;
			}
			collateralQuantityBySecurity = MathUtils.add(collateralQuantityBySecurity, pledgedTransaction.getPostedCollateralQuantity());
			collateralQuantityBySecurityMap.put(pledgedTransaction.getPostedCollateralInvestmentSecurity(), collateralQuantityBySecurity);
		}
		//Now close the positions by security
		List<AccountingPositionClosingSecurityQuantity> closingSecurityQuantityList = new ArrayList<>();
		AccountingPositionClosingCommand closingCommand = AccountingPositionClosingCommand.onSettlementDate(context.getSettlementDate());
		closingCommand.setExcludeReceivableGLAccounts(true);
		closingCommand.setIncludeBigInstrumentPositions(false);
		closingCommand.setUseHoldingAccountBaseCurrency(false);
		closingCommand.setClientInvestmentAccountId(context.getClientAccount().getId());
		if (command.isPostCollateral()) {
			closingCommand.setCollateral(false);
			closingCommand.setOrder(AccountingPositionOrders.LIFO);
			closingCommand.setHoldingInvestmentAccountId(context.getCollateralAccount().getId());
		}
		else {
			closingCommand.setCollateral(true);
			closingCommand.setOrder(AccountingPositionOrders.FIFO);
			closingCommand.setHoldingInvestmentAccountId(context.getHoldingAccount().getId());
		}
		for (Map.Entry<InvestmentSecurity, BigDecimal> collateralQuantityBySecurityEntry : collateralQuantityBySecurityMap.entrySet()) {
			AccountingPositionClosingSecurityQuantity closingSecurityQuantity = new AccountingPositionClosingSecurityQuantity();
			closingSecurityQuantity.setQuantity(MathUtils.abs(collateralQuantityBySecurityEntry.getValue()));
			closingSecurityQuantity.setInvestmentSecurityId((collateralQuantityBySecurityEntry.getKey()).getId());
			closingSecurityQuantityList.add(closingSecurityQuantity);
		}
		closingCommand.setSecurityQuantityList(closingSecurityQuantityList);
		return getAccountingPositionClosingService().getAccountingPositionClosingLotList(closingCommand);
	}


	@Transactional(timeout = 180)
	protected void saveCollateralBalancePositionTransfer(CollateralBalancePledgedTransactionCommand command, AccountingPositionTransfer positionTransfer, List<CollateralBalancePledgedTransaction> pledgedTransactionList) {
		if (CollectionUtils.getSize(positionTransfer.getDetailList()) > 0) {
			//Save the transfer
			getAccountingPositionTransferService().saveAccountingPositionTransfer(positionTransfer);
			//Book and post the transfers
			if (command.isBookAndPost()) {
				getAccountingBookingService().bookAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, positionTransfer, true);
			}
			//finally we update the original pledged transactions with the transfer id and save
			for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
				pledgedTransaction.setPositionTransfer(positionTransfer);
				saveCollateralBalancePledgedTransaction(pledgedTransaction);
			}
		}
	}


	private AccountingPositionTransfer getAccountingPositionTransfer(CollateralBalancePledgedTransactionCommand command, CollateralBalancePledgedServiceContext context, CollateralBalancePledgedTransaction pledgedTransaction) {
		AccountingPositionTransfer positionTransfer = new AccountingPositionTransfer();
		if (pledgedTransaction.getPositionTransfer() == null) {
			positionTransfer.setCollateralTransfer(true);
			positionTransfer.setUseOriginalTransactionDate(true);
			positionTransfer.setTransactionDate(context.getTransactionDate());
			positionTransfer.setSettlementDate(context.getSettlementDate());
			positionTransfer.setFromClientInvestmentAccount(context.getClientAccount());
			positionTransfer.setFromHoldingInvestmentAccount(command.isPostCollateral() ? context.getCollateralAccount() : context.getHoldingAccount());
			positionTransfer.setToClientInvestmentAccount(context.getClientAccount());
			positionTransfer.setToHoldingInvestmentAccount(command.isPostCollateral() ? context.getHoldingAccount() : context.getCollateralAccount());
			positionTransfer.setSourceFkFieldId(context.getCollateralBalance().getId());
			positionTransfer.setSourceSystemTable(getSystemSchemaService().getSystemTableByName(CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME));
		}
		else {
			positionTransfer = getAccountingPositionTransferService().getAccountingPositionTransfer(pledgedTransaction.getPositionTransfer().getId());
		}
		return positionTransfer;
	}


	private List<AccountingPositionTransferDetail> getCashTransferDetailList(CollateralBalancePledgedServiceContext context, List<CollateralBalancePledgedTransaction> pledgedTransactionList) {
		BigDecimal positionCostBasis = BigDecimal.ZERO;
		AccountingPositionTransfer positionTransfer = new AccountingPositionTransfer();
		List<AccountingPositionTransferDetail> transferDetailList = new ArrayList<>();
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		CollateralBalancePledgedTransaction cashTransaction = new CollateralBalancePledgedTransaction();
		for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
			positionCostBasis = MathUtils.add(positionCostBasis, pledgedTransaction.getPostedCollateralQuantity());
			cashTransaction = pledgedTransaction;
		}
		transferDetail.setPositionCostBasis(positionCostBasis);
		transferDetail.setSecurity(cashTransaction.getPostedCollateralInvestmentSecurity());
		transferDetail.setOriginalPositionOpenDate(cashTransaction.getPostedDate());
		transferDetail.setExchangeRateToBase(getExchangeRateToBase(context.getClientAccount(), cashTransaction.getPostedCollateralInvestmentSecurity().getInstrument().getTradingCurrency(), context.getClientAccount().getBaseCurrency(), context.getBalanceDate()));
		transferDetail.setPositionTransfer(positionTransfer);
		transferDetailList.add(transferDetail);
		return transferDetailList;
	}


	private BigDecimal getExchangeRateToBase(InvestmentAccount clientAccount, InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date date) {
		return getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(clientAccount.toClientAccount(), fromCurrency.getSymbol(), toCurrency.getSymbol(), date).flexibleLookup());
	}


	private List<AccountingPositionTransferDetail> getPositionTransferDetailList(List<AccountingPositionClosing> positionClosingList) {
		List<AccountingPositionTransferDetail> transferDetailList = new ArrayList<>();
		for (AccountingPositionClosing positionClosing : CollectionUtils.getIterable(positionClosingList)) {
			AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
			transferDetail.setSecurity(positionClosing.getInvestmentSecurity());
			transferDetail.setQuantity(positionClosing.getRemainingQuantity());
			if (getInvestmentSecurityEventService().isInvestmentSecurityEventTypeAllowedForHierarchy(positionClosing.getInvestmentSecurity().getInstrument().getHierarchy().getId(), InvestmentSecurityEventType.FACTOR_CHANGE)) {
				transferDetail.setOriginalFace(positionClosing.getQuantity());
			}
			transferDetail.setOriginalPositionOpenDate(positionClosing.getOriginalTransactionDate());
			transferDetail.setExchangeRateToBase(positionClosing.getExchangeRateToBase());
			transferDetail.setCostPrice(positionClosing.getPrice());
			transferDetail.setTransferPrice(positionClosing.getPrice());
			transferDetail.setPositionCostBasis(positionClosing.getRemainingCostBasis());
			transferDetail.setExistingPosition(getAccountingTransactionService().getAccountingTransaction(positionClosing.getId()));
			transferDetailList.add(transferDetail);
		}
		return transferDetailList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////     Collateral Balance Pledged Balance Command Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the aggregated values of pledged transactions for the time period (and holding account as specified in the search form). It will include all transactions that
	 * PRIOR to the POSTED DATE that belong to POSITIONS TRANSFERS that are BOOKED, as well as any pledged transactions for the CURRENT POSTED DATE regardless of them belonging to
	 * position transfers or being booked. This allows for historical transactions that were never booked to not affect the pledging of collateral at a later date Realistically,
	 * this situation should not occur in day to day activities, but was found by accident during testing and would be difficult to troubleshoot in the rare case it did happen in
	 * production - I mean, Mark could be feeling malicious one day and go back to a random day in 2015 and pledged transactions then laugh maniacally, so, there's that.
	 */
	@Override
	public List<CollateralBalancePledgedBalance> getCollateralBalancePledgedBalanceList(CollateralBalancePledgedBalanceSearchForm searchForm) {
		List<CollateralBalancePledgedBalance> collateralBalancePledgedBalanceList = getCollateralBalancePledgedBalanceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(new SubselectParameterExpression(new DateType(), searchForm.getPostedStartDate()));
				criteria.add(new SubselectParameterExpression(new DateType(), searchForm.getPostedStartDate()));
				criteria.add(new SubselectParameterExpression(new DateType(), searchForm.getPostedDate()));
				criteria.add(new SubselectParameterExpression(new DateType(), searchForm.getPostedDate()));
				if (BooleanUtils.isTrue(searchForm.getExcludeUnbooked())) {
					criteria.add(new SubselectParameterExpression(new DateType(), null));
				}
				else {
					criteria.add(new SubselectParameterExpression(new DateType(), searchForm.getPostedDate()));
				}
				super.configureCriteria(criteria);
			}
		});
		if (BooleanUtils.isTrue(searchForm.getIncludeMarketValue())) {
			for (CollateralBalancePledgedBalance pledgedBalance : CollectionUtils.getIterable(collateralBalancePledgedBalanceList)) {
				pledgedBalance.setPostedCollateralMarketValue(getPledgedCollateralMarketValue(pledgedBalance.getPostedCollateralInvestmentSecurity(), pledgedBalance.getPostedCollateralQuantity(), searchForm.getPostedDate()));
			}
		}
		return collateralBalancePledgedBalanceList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Collateral Balance Pledged Helper Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getPledgedCollateralMarketValue(int postedCollateralInvestmentSecurityId, BigDecimal postedCollateralQuantity, Date postedDate) {
		InvestmentSecurity collateralSecurity = getInvestmentInstrumentService().getInvestmentSecurity(postedCollateralInvestmentSecurityId);
		return getPledgedCollateralMarketValue(collateralSecurity, postedCollateralQuantity, postedDate);
	}


	private BigDecimal getPledgedCollateralMarketValue(InvestmentSecurity collateralSecurity, BigDecimal postedCollateralQuantity, Date postedDate) {
		BigDecimal price = getMarketDataRetriever().getPrice(collateralSecurity, postedDate,
				true, "Failed to locate price for security: " + collateralSecurity.getSymbol());
		return MathUtils.multiply(postedCollateralQuantity, MathUtils.multiply(price, collateralSecurity.getPriceMultiplier()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Business Methods  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralBalancePledgedTransaction getCollateralBalancePledgedTransaction(long id) {
		return getCollateralBalancePledgedTransactionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<CollateralBalancePledgedTransaction> getCollateralBalancePledgedTransactionList(CollateralBalancePledgedTransactionSearchForm searchForm) {
		return getCollateralBalancePledgedTransactionDAO().findBySearchCriteria(new CollateralBalancePledgedTransactionSearchFormConfigurer(searchForm));
	}


	@Override
	public CollateralBalancePledgedTransaction saveCollateralBalancePledgedTransaction(CollateralBalancePledgedTransaction bean) {
		return getCollateralBalancePledgedTransactionDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<AccountingPositionTransfer> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingPositionTransfer> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingPositionClosingService getAccountingPositionClosingService() {
		return this.accountingPositionClosingService;
	}


	public void setAccountingPositionClosingService(AccountingPositionClosingService accountingPositionClosingService) {
		this.accountingPositionClosingService = accountingPositionClosingService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AdvancedUpdatableDAO<CollateralBalancePledgedTransaction, Criteria> getCollateralBalancePledgedTransactionDAO() {
		return this.collateralBalancePledgedTransactionDAO;
	}


	public void setCollateralBalancePledgedTransactionDAO(AdvancedUpdatableDAO<CollateralBalancePledgedTransaction, Criteria> collateralBalancePledgedTransactionDAO) {
		this.collateralBalancePledgedTransactionDAO = collateralBalancePledgedTransactionDAO;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public AdvancedReadOnlyDAO<CollateralBalancePledgedBalance, Criteria> getCollateralBalancePledgedBalanceDAO() {
		return this.collateralBalancePledgedBalanceDAO;
	}


	public void setCollateralBalancePledgedBalanceDAO(AdvancedReadOnlyDAO<CollateralBalancePledgedBalance, Criteria> collateralBalancePledgedBalanceDAO) {
		this.collateralBalancePledgedBalanceDAO = collateralBalancePledgedBalanceDAO;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
