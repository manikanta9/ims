package com.clifton.collateral.balance.workflow.escrow;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransaction;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransactionCommand;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>CollateralBalanceResetEscrowWorkflowAction</code> class is a workflow transition action
 * that unbooks post and return transfers, then clears collateral and sets the workflow back to Pending.
 * <p/>
 *
 * @author stevenf
 */
public class CollateralBalanceResetEscrowWorkflowAction extends BaseCollateralBalanceWorkflowAction {


	private AccountingJournalService accountingJournalService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingPostingService accountingPostingService;


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
		command.setCollateralBalanceId(collateralBalance.getId());
		command.setAutomatic(true);
		command.setGeneratePledgedCollateral(false);
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedService().getCollateralBalancePledgedTransactionListByCommand(command);
		//Sort by quantity in descending order so we un-book posts before dependent returns.
		pledgedTransactionList = BeanUtils.sortWithFunction(pledgedTransactionList, CollateralBalancePledgedTransaction::getPostedCollateralQuantity, false);

		//Unpost transfers (posts first, then returns)
		List<Integer> positionTransferIdList = pledgedTransactionList.stream()
				.filter(transaction -> Objects.nonNull(transaction.getPositionTransfer()))
				.map(transaction -> transaction.getPositionTransfer().getId()).collect(Collectors.toList());
		for (Integer positionTransferId : CollectionUtils.getIterable(positionTransferIdList)) {
			AccountingPositionTransfer positionTransfer = getAccountingPositionTransferService().getAccountingPositionTransfer(positionTransferId);
			if (positionTransfer.getBookingDate() != null) {
				AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySource(AccountingPositionTransfer.ACCOUNTING_POSITION_TRANSFER_TABLE_NAME, positionTransferId);
				getAccountingPostingService().unpostAccountingJournal(journal.getId());
			}
		}

		//Now we can clear the pledged transactions, which will also delete the transfers
		Status status = getCollateralBalancePledgedService().clearCollateralBalancePledgedTransactionList(command);
		ValidationUtils.assertTrue(CollectionUtils.isEmpty(status.getErrorList()), "Failed to reset workflow: " + status.getErrorList().iterator());
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}
}
