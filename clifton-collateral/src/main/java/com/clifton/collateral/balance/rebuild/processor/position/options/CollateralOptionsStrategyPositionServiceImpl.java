package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @author terrys
 */
@Service
public class CollateralOptionsStrategyPositionServiceImpl implements CollateralOptionsStrategyPositionService {

	private CollateralService collateralService;
	private InvestmentAccountService investmentAccountService;
	private CollateralBalanceRebuildService collateralBalanceRebuildService;
	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingPositionService accountingPositionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CollateralOptionsStrategyPosition> getCollateralOptionsStrategyPositionList(String collateralTypeName, int holdingAccountId, Date positionDate, boolean showExplanation) {
		CollateralType collateralType = getCollateralService().getCollateralTypeByName(collateralTypeName);
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(holdingAccountId);
		InvestmentAccount collateralAccount = getCollateralBalanceRebuildService().getCollateralAccount(collateralType, holdingAccount, positionDate);
		@SuppressWarnings("unchecked")
		CollateralBalanceRebuildProcessor<CollateralOptionsStrategyPosition> processor = (CollateralBalanceRebuildProcessor<CollateralOptionsStrategyPosition>)
				getCollateralBalanceRebuildService().getCollateralBalanceRebuildProcessor(collateralType, holdingAccount, collateralAccount);

		AccountingPositionDailyLiveSearchForm positionDailyLiveSearchForm = new AccountingPositionDailyLiveSearchForm();
		positionDailyLiveSearchForm.setHoldingAccountId(holdingAccountId);
		positionDailyLiveSearchForm.setSnapshotDate(positionDate);
		positionDailyLiveSearchForm.setCollateralAccountingAccount(false);
		List<AccountingPositionDaily> positionDailyList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(positionDailyLiveSearchForm);

		List<AccountingPosition> accountingPositions = getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
				.onPositionTransactionDate(positionDate)
				.forHoldingAccount(holdingAccountId)
		);
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionSettlementDate(positionDate);
		positionCommand.setHoldingInvestmentAccountId(holdingAccountId);
		List<CollateralPosition> collateralPositions = getCollateralService().getCollateralPositionListUsingPositions(positionCommand, accountingPositions);

		return processor.calculateRequiredCollateralList(positionDailyList, collateralPositions, positionDate, holdingAccount, showExplanation);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}
}
