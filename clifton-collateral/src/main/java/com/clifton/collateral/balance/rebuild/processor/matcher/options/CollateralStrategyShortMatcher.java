package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.matcher.CollateralStrategyMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralStrategyShortMatcher implements CollateralStrategyMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategyShortMatcher strangleMatcher = new CollateralStrategyShortMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyShortPositionList(List<CollateralOptionsStrategyPosition> positionInfoList, Date positionDate) {
		return strangleMatcher.getCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), positionDate);
	}


	@Override
	public Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		Map<String, List<CollateralOptionsStrategyPosition>> shortPositionMap = CollectionUtils.getStream(positionInfoList)
				.filter(p -> InvestmentUtils.isOption(p.getInvestmentSecurity()))
				.filter(p -> MathUtils.isNegative(p.getRemainingQuantity()))
				.collect(Collectors.groupingBy(p -> p.getOptionType().name()));
		List<CollateralOptionsStrategyPosition> positionList = shortPositionMap.values().stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
		if (!positionList.isEmpty()) {
			positionInfoList.removeAll(positionList);
		}
		if (shortPositionMap.containsKey("CALL")) {
			results.put(CollateralOptionsStrategyCalculators.SHORT_CALL, shortPositionMap.get("CALL"));
		}
		if (shortPositionMap.containsKey("PUT")) {
			results.put(CollateralOptionsStrategyCalculators.SHORT_PUT, shortPositionMap.get("PUT"));
		}

		// update group IDs
		results.forEach((key, value) -> value.forEach(p -> p.setPositionGroupId(key.getTradeStrategyName())));

		// remove matches from the position list
		List<CollateralOptionsStrategyPosition> removals = results.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		positionInfoList.removeAll(removals);

		return results;
	}
}
