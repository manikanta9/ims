package com.clifton.collateral.balance;


import com.clifton.collateral.balance.rule.CollateralBalanceRuleEvaluatorHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import org.springframework.stereotype.Component;


/**
 * The <code>CollateralBalanceObserver</code> is used to execute the rules when saving a CollateralBalance object.
 *
 * @author manderson
 */
@Component
public class CollateralBalanceObserver extends SelfRegisteringDaoObserver<CollateralBalance> {

	private CollateralBalanceRuleEvaluatorHandler collateralBalanceRuleEvaluatorHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<CollateralBalance> dao, @SuppressWarnings("unused") DaoEventTypes event, CollateralBalance bean) {
		if (!event.isInsert()) {
			getCollateralBalanceRuleEvaluatorHandler().executeCollateralBalanceRules(bean);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceRuleEvaluatorHandler getCollateralBalanceRuleEvaluatorHandler() {
		return this.collateralBalanceRuleEvaluatorHandler;
	}


	public void setCollateralBalanceRuleEvaluatorHandler(CollateralBalanceRuleEvaluatorHandler collateralBalanceRuleEvaluatorHandler) {
		this.collateralBalanceRuleEvaluatorHandler = collateralBalanceRuleEvaluatorHandler;
	}
}
