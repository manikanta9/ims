package com.clifton.collateral.balance.pledged.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class CollateralBalancePledgedBalanceSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "positionInvestmentSecurity.id", sortField = "positionInvestmentSecurity.symbol")
	private Integer positionInvestmentSecurityId;

	@SearchField(searchField = "postedCollateralInvestmentSecurity.id", sortField = "postedCollateralInvestmentSecurity.symbol")
	private Integer postedCollateralInvestmentSecurityId;

	@SearchField
	private BigDecimal postedCollateralQuantity;

	//Custom search field
	private Date postedStartDate;

	//Custom search field (AccountingBalance does not have a transactionDate field)
	private Date postedDate;

	//Custom search field (Used from UI so unbooked transactions are not included in overall balance)
	private Boolean excludeUnbooked;

	private Boolean includeMarketValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getPositionInvestmentSecurityId() {
		return this.positionInvestmentSecurityId;
	}


	public void setPositionInvestmentSecurityId(Integer positionInvestmentSecurityId) {
		this.positionInvestmentSecurityId = positionInvestmentSecurityId;
	}


	public Integer getPostedCollateralInvestmentSecurityId() {
		return this.postedCollateralInvestmentSecurityId;
	}


	public void setPostedCollateralInvestmentSecurityId(Integer postedCollateralInvestmentSecurityId) {
		this.postedCollateralInvestmentSecurityId = postedCollateralInvestmentSecurityId;
	}


	public BigDecimal getPostedCollateralQuantity() {
		return this.postedCollateralQuantity;
	}


	public void setPostedCollateralQuantity(BigDecimal postedCollateralQuantity) {
		this.postedCollateralQuantity = postedCollateralQuantity;
	}


	public Date getPostedStartDate() {
		return this.postedStartDate;
	}


	public void setPostedStartDate(Date postedStartDate) {
		this.postedStartDate = postedStartDate;
	}


	public Date getPostedDate() {
		return this.postedDate;
	}


	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}


	public Boolean getExcludeUnbooked() {
		return this.excludeUnbooked;
	}


	public void setExcludeUnbooked(Boolean excludeUnbooked) {
		this.excludeUnbooked = excludeUnbooked;
	}


	public Boolean getIncludeMarketValue() {
		return this.includeMarketValue;
	}


	public void setIncludeMarketValue(Boolean includeMarketValue) {
		this.includeMarketValue = includeMarketValue;
	}
}
