package com.clifton.collateral.balance.booking;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>CollateralBalanceBookingService</code> defines a service that allows booking multiple CollateralBalance objects at once.
 *
 * @author stevenf
 */
public interface CollateralBalanceBookingService {

	//////////////////////////////////////////////////////////////////////////// 
	////////         Collateral Balance Business Methods               ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	@ModelAttribute("data")
	@RequestMapping("collateralBalanceListBookAndPost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public int bookAndPostCollateralBalanceList(CollateralBalanceSearchForm searchForm);


	@DoNotAddRequestMapping
	public boolean bookAndPostCollateralBalance(CollateralBalance balance);

}
