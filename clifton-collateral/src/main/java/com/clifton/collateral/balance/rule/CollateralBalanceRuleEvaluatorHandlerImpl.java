package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.logging.LogUtils;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class CollateralBalanceRuleEvaluatorHandlerImpl implements CollateralBalanceRuleEvaluatorHandler {

	private RuleDefinitionService ruleDefinitionService;
	private RuleEvaluatorService ruleEvaluatorService;
	private RuleViolationStatusService ruleViolationStatusService;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	@Override
	public void executeCollateralBalanceRules(CollateralBalance balance) {
		//NOTE: This is called by 'CollateralBalanceObserver' class in method 'beforeMethodCallImpl'
		try {
			if (balance.getParent() == null) {
				//Call the executeRules directly with the category and full object since it hasn't been saved yet and
				//the unsaved values are necessary to properly evaluate the rules.
				RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategoryByName(CollateralBalance.RULE_CATEGORY_COLLATERAL_BALANCE);
				getRuleEvaluatorService().executeRules(ruleCategory, balance, null);
				balance.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));
			}
		}
		catch (Exception e) {
			balance.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED));
			LogUtils.error(getClass(), "Failed process rule for collateral balance [" + balance + "].", e);
		}
	}


	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
