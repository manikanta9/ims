package com.clifton.collateral.balance.rebuild.processor.position;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;


/**
 * @author terrys
 */
public interface CollateralStrategyPosition {

	public UUID getUuid();


	public Date getPositionDate();


	public InvestmentSecurity getInvestmentSecurity();


	public BigDecimal getPriceMultiplier();


	public BigDecimal getRemainingQuantity();


	public String getPositionGroupId();


	public void setPositionGroupId(String positionGroupId);


	public BigDecimal getRequiredCollateral();


	public void setRequiredCollateral(BigDecimal requiredCollateral);


	public String buildPositionExplanation();


	public BigDecimal getFxRateToBase();


	public void setFxRateToBase(BigDecimal fxRateToBase);


	public BigDecimal getBaseRequiredCollateral();
}
