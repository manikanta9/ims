package com.clifton.collateral.balance.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.AbstractTransferMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Will convert a CollateralBalance e to either a GeneralFinancialInstitutionTransferMessage or NoticeToReceiveTransferMessage instruction messaging
 * depending on the direction of the instruction.
 *
 * @author mwacker
 */
@Component
public class CollateralBalanceCounterpartyInstructionGenerator implements InstructionGenerator<InstructionMessage, CollateralBalance, InstructionDeliveryFieldCommand> {

	private SystemSchemaUtilHandler systemSchemaUtilHandler;
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	private InstructionCancellationHandler instructionCancellationHandler;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;
	private DaoLocator daoLocator;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public Class<CollateralBalance> getSupportedMessageClass() {
		return CollateralBalance.class;
	}


	@Override
	public boolean isCancellationSupported() {
		return true;
	}


	@Override
	public List<InstructionMessage> generateInstructionCancelMessageList(Instruction instruction) {
		return getInstructionCancellationHandler().generateCancellationMessageList(instruction);
	}


	@Override
	public InstructionDeliveryFieldCommand getDeliveryInstructionProvider(Instruction instruction, CollateralBalance collateralBalance) {
		return new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return instruction;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return collateralBalance.getHoldingInvestmentAccount().getIssuingCompany();
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return collateralBalance.getHoldingInvestmentAccount();
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return collateralBalance.getCollateralCurrency() == null ? collateralBalance.getHoldingInvestmentAccount().getBaseCurrency() : collateralBalance.getCollateralCurrency();
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return collateralBalance.getClientInvestmentAccount();
			}
		};
	}


	@Override
	@SuppressWarnings("ConstantConditions")
	public List<InstructionMessage> generateInstructionMessageList(Instruction instruction) {
		CollateralBalance collateralBalance = getIdentityObject(instruction);
		InstructionDeliveryFieldCommand deliveryFieldProvider = getDeliveryInstructionProvider(instruction, collateralBalance);

		if (MathUtils.isNullOrZero(collateralBalance.getTransferAmount())) {
			return Collections.emptyList();
		}

		AbstractTransferMessage result;
		if (MathUtils.isNegative(collateralBalance.getTransferAmount())) {
			result = new GeneralFinancialInstitutionTransferMessage();
		}
		else {
			result = new NoticeToReceiveTransferMessage();
		}
		List<InstructionMessage> resultList = new ArrayList<>();
		resultList.add(result);

		String senderBIC = getBusinessCompanyUtilHandler().getParametricBusinessIdentifierCode();
		ValidationUtils.assertNotEmpty(senderBIC, "The sender BIC is required.");
		result.setSenderBIC(senderBIC);

		String custodianBIC = Optional.of(collateralBalance)
				.map(CollateralBalance::getCollateralInvestmentAccount)
				.map(InvestmentAccount::getIssuingCompany)
				.map(BusinessCompany::getBusinessIdentifierCode)
				.orElse(null);
		ValidationUtils.assertNotEmpty(custodianBIC, () ->
				String.format("The Receiver BIC is empty on the Issuing Company [%s] for the Collateral Investment Account [%s].",
						Optional.of(collateralBalance).map(CollateralBalance::getCollateralInvestmentAccount).map(InvestmentAccount::getIssuingCompany).map(BusinessCompany::getName).orElse(""),
						Optional.of(collateralBalance).map(CollateralBalance::getCollateralInvestmentAccount).map(InvestmentAccount::getName).orElse("")
				)
		);
		result.setReceiverBIC(custodianBIC);

		if (InvestmentAccountType.OTC_CLEARED.equals(collateralBalance.getHoldingInvestmentAccount().getType().getName())) {
			result.setMessagePurpose(TransferMessagePurposes.INITIAL_MARGIN_OTC);
		}
		else {
			result.setMessagePurpose(TransferMessagePurposes.INITIAL_MARGIN);
		}

		result.setTransactionReferenceNumber(getTransactionReferenceNumber(instruction, collateralBalance));

		result.setAmount(MathUtils.abs(collateralBalance.getTransferAmount()));
		result.setCurrency(deliveryFieldProvider.getDeliveryCurrency().getSymbol());
		result.setValueDate(new Date());

		if (MathUtils.isNegative(collateralBalance.getTransferAmount())) {
			GeneralFinancialInstitutionTransferMessage transferMessage = (GeneralFinancialInstitutionTransferMessage) result;
			transferMessage.setCustodyBIC(custodianBIC);
			transferMessage.setCustodyAccountNumber(collateralBalance.getCollateralInvestmentAccount().getNumber());

			transferMessage.setBeneficiaryBIC(collateralBalance.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode());
			transferMessage.setBeneficiaryAccountNumber(collateralBalance.getHoldingInvestmentAccount().getNumber());

			InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction,
					deliveryFieldProvider.getDeliveryCompany(), deliveryFieldProvider.getDeliveryCurrency());
			if (deliveryInstruction != null) {
				String intermediaryBIC = ObjectUtils.coalesce(deliveryInstruction.getDeliverySwiftCode(), deliveryInstruction.getDeliveryCompany().getBusinessIdentifierCode());
				if (!StringUtils.isEmpty(intermediaryBIC)) {
					transferMessage.setIntermediaryBIC(intermediaryBIC);
				}
				Optional.ofNullable(deliveryInstruction.getDeliveryAccountNumber()).ifPresent(transferMessage::setIntermediaryAccountNumber);
			}
		}
		else {
			NoticeToReceiveTransferMessage transferMessage = (NoticeToReceiveTransferMessage) result;

			transferMessage.setReceiverAccountNumber(collateralBalance.getCollateralInvestmentAccount().getNumber());
			transferMessage.setOrderingBIC(collateralBalance.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode());

			InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction,
					deliveryFieldProvider.getDeliveryCompany(), deliveryFieldProvider.getDeliveryCurrency());
			String intermediaryBIC = ObjectUtils.coalesce(deliveryInstruction.getDeliverySwiftCode(), deliveryInstruction.getDeliveryCompany().getBusinessIdentifierCode());
			if (!StringUtils.isEmpty(intermediaryBIC)) {
				transferMessage.setIntermediaryBIC(intermediaryBIC);
			}
		}

		return resultList;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private CollateralBalance getIdentityObject(Instruction instruction) {
		ReadOnlyDAO<CollateralBalance> dao = getDaoLocator().locate(instruction.getSystemTable().getName());
		return dao.findByPrimaryKey(instruction.getFkFieldId());
	}


	public String getTransactionReferenceNumber(Instruction instruction, CollateralBalance collateralBalance) {
		SystemSchemaUtilHandler schemaUtilHandler = getSystemSchemaUtilHandler();
		List<String> referenceComponents = Stream.of(instruction, collateralBalance)
				.filter(Objects::nonNull)
				.map(schemaUtilHandler::getEntityUniqueId)
				.collect(Collectors.toList());
		return String.join(InstructionGenerator.REFERENCE_COMPONENT_DELIMITER, referenceComponents);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public BusinessCompanyUtilHandler getBusinessCompanyUtilHandler() {
		return this.businessCompanyUtilHandler;
	}


	public void setBusinessCompanyUtilHandler(BusinessCompanyUtilHandler businessCompanyUtilHandler) {
		this.businessCompanyUtilHandler = businessCompanyUtilHandler;
	}


	public SystemSchemaUtilHandler getSystemSchemaUtilHandler() {
		return this.systemSchemaUtilHandler;
	}


	public void setSystemSchemaUtilHandler(SystemSchemaUtilHandler systemSchemaUtilHandler) {
		this.systemSchemaUtilHandler = systemSchemaUtilHandler;
	}


	public InstructionCancellationHandler getInstructionCancellationHandler() {
		return this.instructionCancellationHandler;
	}


	public void setInstructionCancellationHandler(InstructionCancellationHandler instructionCancellationHandler) {
		this.instructionCancellationHandler = instructionCancellationHandler;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
