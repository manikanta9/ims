package com.clifton.collateral.balance.simplejournal;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalService;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalType;
import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalSearchForm;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CollateralBalanceSimpleJournalServiceImpl</code> class provides basic implementation of CollateralBalanceSimpleJournalService interface.
 *
 * @author stevenf
 */
@Service
public class CollateralBalanceSimpleJournalServiceImpl implements CollateralBalanceSimpleJournalService {

	private static final String CASH_COLLATERAL_CONTRIBUTION = "Cash Collateral Contribution";
	private static final String CASH_COLLATERAL_DISTRIBUTION = "Cash Collateral Distribution";

	private AccountingSimpleJournalService accountingSimpleJournalService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public Status generateCashCollateralSimpleJournalEntries(Date tradeDate) {
		Status status = Status.ofTitle("Cash Collateral Transactions Generation Details");
		Map<Integer, InvestmentAccount> custodianAccountMap = new HashMap<>();
		Map<String, Trade> tradeMap = new HashMap<>();
		Map<String, AccountingSimpleJournal> existingSimpleJournalMap = getExistingSimpleJournals(tradeDate);
		Map<String, CollateralBalanceSimpleJournal> collateralBalanceSimpleJournalMap = new HashMap<>();

		int existingEntries = 0;
		int processedEntries = 0;
		int entriesCreated = 0;
		List<Trade> tradeList = getTradeList(tradeDate);

		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			try {
				InvestmentAccount custodianAccount = getCustodianAccountForTrade(trade, custodianAccountMap);

				String key = String.valueOf(trade.getClientInvestmentAccount().getId()) + "_" +
						String.valueOf(trade.getHoldingInvestmentAccount().getId() + "_" +
								String.valueOf(custodianAccount.getId()));

				Trade existingTrade = tradeMap.get(key);
				if (existingTrade != null && existingTrade.getPayingSecurity().getId().intValue() != trade.getPayingSecurity().getId().intValue()) {
					status.addSkipped("Trades (" + existingTrade.getId() + " : " + trade.getId() +
							") have the same key, but different paying securities: Processing for Trade (" + trade.getId() + ") was skipped.");
				}
				else {
					AccountingSimpleJournal simpleJournal = existingSimpleJournalMap.get(key);
					if (simpleJournal == null) {
						CollateralBalanceSimpleJournal collateralBalanceSimpleJournal = collateralBalanceSimpleJournalMap.get(key);
						collateralBalanceSimpleJournal = populateCollateralBalanceSimpleJournalTradeData(collateralBalanceSimpleJournal, trade, custodianAccount);
						collateralBalanceSimpleJournalMap.put(key, collateralBalanceSimpleJournal);
					}
					else {
						existingEntries++;
					}
				}
				tradeMap.put(key, trade);
			}
			catch (Exception e) {
				String errorMessage = "Failed to create entry for trade [" + trade.getId() + "] with client account [" + trade.getClientInvestmentAccount().getLabel() + "]";
				status.addError(errorMessage + e.getMessage());
				LogUtils.error(getClass(), errorMessage, e);
			}
			processedEntries++;
		}
		String statusMessage = "No trade(s) to process for " + DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_SHORT);
		if (processedEntries > 0) {
			statusMessage = "Processed " + processedEntries + " trade(s) out of " + tradeList.size() + ".";
			createSimpleJournalEntries(collateralBalanceSimpleJournalMap, status);
			Map<String, AccountingSimpleJournal> accrualJournalMap = mapAccrualJournals(tradeMap, existingSimpleJournalMap, collateralBalanceSimpleJournalMap, status);
			entriesCreated = saveGeneratedJournals(tradeMap, collateralBalanceSimpleJournalMap, accrualJournalMap, status);
		}
		status.addDetail(StatusDetail.CATEGORY_SUCCESS, "Successfully generated " + entriesCreated + " cash collateral simple journal entries.");
		existingEntries += status.getStatusCount("Existing");

		if (existingEntries > 0) {
			status.addDetail(StatusDetail.CATEGORY_SKIPPED, existingEntries + " entries already existed.");
		}
		status.setMessage(statusMessage);
		return status;
	}


	private int saveGeneratedJournals(Map<String, Trade> tradeMap, Map<String, CollateralBalanceSimpleJournal> collateralBalanceSimpleJournalMap, Map<String, AccountingSimpleJournal> accrualJournalMap, Status status) {
		int entriesCreated = 0;
		for (Map.Entry<String, Trade> tradeEntry : CollectionUtils.getIterable(tradeMap.entrySet())) {
			String key = tradeEntry.getKey();
			CollateralBalanceSimpleJournal principalJournal = collateralBalanceSimpleJournalMap.get(key);
			try {
				if (principalJournal != null) {
					getAccountingSimpleJournalService().saveAccountingSimpleJournal(principalJournal.toAccountingSimpleJournal());
					entriesCreated++;
				}
			}
			catch (Exception e) {
				String errorMessage = "Failed to create entry for trade(s) [" +
						principalJournal.getTradeIdList() + "] with client account [" +
						principalJournal.getClientInvestmentAccount().getLabel() + "]";
				status.addError(errorMessage + e.getMessage());
				LogUtils.error(getClass(), errorMessage, e);
			}
			AccountingSimpleJournal accrualJournal = accrualJournalMap.get(key);
			try {
				if (accrualJournal != null) {
					getAccountingSimpleJournalService().saveAccountingSimpleJournal(accrualJournal);
					entriesCreated++;
				}
			}
			catch (Exception e) {
				String errorMessage = "Failed to create accrual entry for trade [" +
						tradeEntry.getValue().getLabel() + "] with client account [" +
						accrualJournal.getClientInvestmentAccount().getLabel() + "]";
				status.addError(errorMessage + e.getMessage());
				LogUtils.error(getClass(), errorMessage, e);
			}
		}

		return entriesCreated;
	}


	private Map<String, AccountingSimpleJournal> mapAccrualJournals(Map<String, Trade> tradeMap, Map<String, AccountingSimpleJournal> existingSimpleJournalMap, Map<String, CollateralBalanceSimpleJournal> collateralBalanceSimpleJournalMap, Status status) {
		Map<String, AccountingSimpleJournal> accrualJournalMap = new HashMap<>();
		AccountingSimpleJournalType transferType = getAccountingSimpleJournalService().getAccountingSimpleJournalTypeByName("Cash Transfer");
		for (Map.Entry<String, Trade> stringTrade : tradeMap.entrySet()) {
			String key = stringTrade.getKey();
			Trade trade = stringTrade.getValue();
			AccountingSimpleJournal accrualJournal = null;
			AccountingSimpleJournal principalJournal = existingSimpleJournalMap.get(key) != null ? existingSimpleJournalMap.get(key) : collateralBalanceSimpleJournalMap.get(key);
			if (principalJournal != null) {
				accrualJournal = populateAccrualJournal(trade, principalJournal, transferType);
				try {
					AccountingSimpleJournal existingAccrualJournal = getExistingAccrualJournal(accrualJournal, status);
					if (existingAccrualJournal == null && MathUtils.isGreaterThan(accrualJournal.getAmount(), BigDecimal.ZERO)) {
						accrualJournalMap.put(key, accrualJournal);
					}
					else {
						status.incrementStatusCount("Existing");
					}
				}
				catch (IllegalArgumentException e) {
					status.addError("Too many possible accrual Journals for trade [" + trade.getLabel() + "] Unable to determine if one already exists.");
				}
			}
			else {
				status.addSkipped("Skipped Accrual for trade [" + trade.getLabelShort() + "] because no Principal journal found.");
			}
		}
		return accrualJournalMap;
	}


	private AccountingSimpleJournal populateAccrualJournal(Trade trade, AccountingSimpleJournal principalJournal, AccountingSimpleJournalType type) {
		AccountingSimpleJournal accrualJournal = new AccountingSimpleJournal();
		accrualJournal.setDescription("True-up of Cash due to T-Bill transaction (Accrued Interest)");

		InvestmentAccount mainHoldingInvestmentAccount = principalJournal.getMainHoldingInvestmentAccount();
		InvestmentAccount offsettingHoldingInvestmentAccount = principalJournal.getOffsettingHoldingInvestmentAccount();
		if (CASH_COLLATERAL_DISTRIBUTION.equals(principalJournal.getJournalType().getName())) {
			mainHoldingInvestmentAccount = principalJournal.getOffsettingHoldingInvestmentAccount();
			offsettingHoldingInvestmentAccount = principalJournal.getMainHoldingInvestmentAccount();
		}
		accrualJournal.setMainHoldingInvestmentAccount(mainHoldingInvestmentAccount);
		accrualJournal.setOffsettingHoldingInvestmentAccount(offsettingHoldingInvestmentAccount);

		accrualJournal.setMainAccountingAccount(type.getMainAccountingAccount());
		accrualJournal.setOffsettingAccountingAccount(type.getOffsettingAccountingAccount());
		accrualJournal.setJournalType(type);
		accrualJournal.setAmount(trade.getAccrualAmount());
		accrualJournal.setMainInvestmentSecurity(principalJournal.getMainInvestmentSecurity());
		accrualJournal.setOffsettingInvestmentSecurity(principalJournal.getOffsettingInvestmentSecurity());
		accrualJournal.setClientInvestmentAccount(principalJournal.getClientInvestmentAccount());
		accrualJournal.setTransactionDate(principalJournal.getTransactionDate());
		accrualJournal.setOriginalTransactionDate(principalJournal.getOriginalTransactionDate());
		accrualJournal.setSettlementDate(principalJournal.getSettlementDate());
		accrualJournal.setPrivateNote("Accrued Interest");
		accrualJournal.setExchangeRateToBase(principalJournal.getExchangeRateToBase());

		return accrualJournal;
	}


	private CollateralBalanceSimpleJournal populateCollateralBalanceSimpleJournalTradeData(CollateralBalanceSimpleJournal collateralBalanceSimpleJournal, Trade trade, InvestmentAccount custodianAccount) {
		//If the entry is null then this is the first trade to be processed for it - initialize and populate values.
		if (collateralBalanceSimpleJournal == null) {
			collateralBalanceSimpleJournal = new CollateralBalanceSimpleJournal();
			collateralBalanceSimpleJournal.setDescription("True-up of Cash Collateral due to T-Bill transaction");
			collateralBalanceSimpleJournal.setBuy(trade.isBuy());
			collateralBalanceSimpleJournal.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
			collateralBalanceSimpleJournal.setCustodianAccount(custodianAccount);
			collateralBalanceSimpleJournal.setMainInvestmentSecurity(trade.getPayingSecurity());
			collateralBalanceSimpleJournal.setOffsettingInvestmentSecurity(trade.getPayingSecurity());
			collateralBalanceSimpleJournal.setClientInvestmentAccount(trade.getClientInvestmentAccount());
			collateralBalanceSimpleJournal.setTransactionDate(trade.getSettlementDate());
			collateralBalanceSimpleJournal.setOriginalTransactionDate(trade.getSettlementDate());
			collateralBalanceSimpleJournal.setSettlementDate(trade.getSettlementDate());
			collateralBalanceSimpleJournal.setExchangeRateToBase(getExchangeRate(trade));

			computeCashCollateralBalance(collateralBalanceSimpleJournal, trade);
			computeCollateralPositionMaturityProceeds(collateralBalanceSimpleJournal, trade);
		}
		//The net amount is the only value that is added to itself when subsequent trades are processed for the same entry.
		computeNetAmount(collateralBalanceSimpleJournal, trade);
		collateralBalanceSimpleJournal.getTradeIdList().add(trade.getId());
		return collateralBalanceSimpleJournal;
	}


	private void createSimpleJournalEntries(Map<String, CollateralBalanceSimpleJournal> collateralBalanceSimpleJournalMap, Status detailList) {
		List<String> skipKeyList = new ArrayList<>();
		for (Map.Entry<String, CollateralBalanceSimpleJournal> stringCollateralBalanceSimpleJournalEntry : collateralBalanceSimpleJournalMap.entrySet()) {
			CollateralBalanceSimpleJournal collateralBalanceSimpleJournal = stringCollateralBalanceSimpleJournalEntry.getValue();

			//Now that all trades have been iterated and the netAmount, cash collateral balance and/or maturity proceeds
			//have been computed we can determine the journal type.
			computeJournalTypeAndAmount(collateralBalanceSimpleJournal);

			//If no journal type was computed, then we don't create an entry as there is no need.
			AccountingSimpleJournalType simpleJournalType = collateralBalanceSimpleJournal.getJournalType();
			if (simpleJournalType != null) {
				InvestmentAccount mainHoldingInvestmentAccount = collateralBalanceSimpleJournal.getHoldingInvestmentAccount();
				InvestmentAccount offsettingHoldingInvestmentAccount = collateralBalanceSimpleJournal.getCustodianAccount();
				if (CASH_COLLATERAL_DISTRIBUTION.equals(simpleJournalType.getName())) {
					mainHoldingInvestmentAccount = collateralBalanceSimpleJournal.getCustodianAccount();
					offsettingHoldingInvestmentAccount = collateralBalanceSimpleJournal.getHoldingInvestmentAccount();
				}
				collateralBalanceSimpleJournal.setMainHoldingInvestmentAccount(mainHoldingInvestmentAccount);
				collateralBalanceSimpleJournal.setOffsettingHoldingInvestmentAccount(offsettingHoldingInvestmentAccount);
				collateralBalanceSimpleJournal.setMainAccountingAccount(simpleJournalType.getMainAccountingAccount());
				collateralBalanceSimpleJournal.setOffsettingAccountingAccount(simpleJournalType.getOffsettingAccountingAccount());
			}
			else {
				skipKeyList.add(stringCollateralBalanceSimpleJournalEntry.getKey());
			}
		}
		for (String mapKey : CollectionUtils.getIterable(skipKeyList)) {
			CollateralBalanceSimpleJournal skippedJournal = collateralBalanceSimpleJournalMap.remove(mapKey);
			detailList.addSkipped("Did not create journal entry for trade(s) '" + skippedJournal.getTradeIdList() + "' as no contribution or distribution is necessary.");
		}
	}


	private void computeJournalTypeAndAmount(CollateralBalanceSimpleJournal collateralBalanceSimpleJournal) {
		BigDecimal cashCollateralBalance = collateralBalanceSimpleJournal.getCashCollateralBalance();
		BigDecimal netAmount = collateralBalanceSimpleJournal.getNetAmount();
		BigDecimal maturityProceeds = collateralBalanceSimpleJournal.getMaturityProceeds();
		BigDecimal totalCollateral = MathUtils.add(maturityProceeds, cashCollateralBalance);

		// computeNetAmount() will now accumulate buy amounts as negative values and sell amounts as positive values
		BigDecimal amount = MathUtils.add(totalCollateral, netAmount);

		//We default to a distribution as all sells are distributions, and buys are conditionally distributions.
		AccountingSimpleJournalType journalType = getAccountingSimpleJournalService().getAccountingSimpleJournalTypeByName(CASH_COLLATERAL_DISTRIBUTION);
		//If the trade was a buy AND the amount is negative (e.g. the netAmount of the trades exceeds the totalCollateral, then it is a contribution)
		if (collateralBalanceSimpleJournal.isBuy() && MathUtils.isLessThan(amount, BigDecimal.ZERO)) {
			journalType = getAccountingSimpleJournalService().getAccountingSimpleJournalTypeByName(CASH_COLLATERAL_CONTRIBUTION);
		}
		collateralBalanceSimpleJournal.setAmount(MathUtils.abs(amount));
		collateralBalanceSimpleJournal.setJournalType(journalType);
	}


	//Searches for existing simple journal entries so duplicates can be avoided
	private Map<String, AccountingSimpleJournal> getExistingSimpleJournals(Date tradeDate) {
		Map<String, AccountingSimpleJournal> simpleJournalMap = new HashMap<>();
		AccountingSimpleJournalSearchForm accountingSimpleJournalSearchForm = new AccountingSimpleJournalSearchForm();
		accountingSimpleJournalSearchForm.setTransactionDate(DateUtils.addDays(tradeDate, 1));
		accountingSimpleJournalSearchForm.setCategory2Name("Accounting Simple Journal Type Tags");
		accountingSimpleJournalSearchForm.setCategory2TableName("AccountingSimpleJournalType");
		accountingSimpleJournalSearchForm.setCategory2LinkFieldPath("journalType");
		accountingSimpleJournalSearchForm.setCategory2HierarchyName("Cash Collateral Transactions");
		List<AccountingSimpleJournal> accountingSimpleJournalList = getAccountingSimpleJournalService().getAccountingSimpleJournalList(accountingSimpleJournalSearchForm);
		for (AccountingSimpleJournal simpleJournal : CollectionUtils.getIterable(accountingSimpleJournalList)) {
			//We always build the key using clientAccountId, holdingAccountId, custodianAccountId for the other maps.
			//So here, based on the journal type we build the key the same way so the type doesn't need to be computed
			//while building the values maps for the trades to set the key - this avoids a fair amount of possibly
			//unnecessary computation in order to determine if an entry already exists.
			String key = String.valueOf(simpleJournal.getClientInvestmentAccount().getId()) + "_" +
					String.valueOf(simpleJournal.getMainHoldingInvestmentAccount().getId()) + "_" +
					String.valueOf(simpleJournal.getOffsettingHoldingInvestmentAccount().getId());
			if (CASH_COLLATERAL_DISTRIBUTION.equals(simpleJournal.getJournalType().getName())) {
				key = String.valueOf(simpleJournal.getClientInvestmentAccount().getId()) + "_" +
						String.valueOf(simpleJournal.getOffsettingHoldingInvestmentAccount().getId()) + "_" +
						String.valueOf(simpleJournal.getMainHoldingInvestmentAccount().getId());
			}
			simpleJournalMap.put(key, simpleJournal);
		}
		return simpleJournalMap;
	}


	private List<Trade> getTradeList(Date tradeDate) {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeDate(tradeDate);
		tradeSearchForm.setCollateralTrade(true);
		tradeSearchForm.setWorkflowStateName("Booked");
		return getTradeService().getTradeList(tradeSearchForm);
	}


	private InvestmentAccount getCustodianAccountForTrade(Trade trade, Map<Integer, InvestmentAccount> custodianAccountMap) {
		return custodianAccountMap.computeIfAbsent(trade.getClientInvestmentAccount().getId(), k -> getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(trade.getClientInvestmentAccount().getId(), "Custodian", trade.getTradeDate()));
	}


	private void computeNetAmount(CollateralBalanceSimpleJournal collateralBalanceSimpleJournal, Trade trade) {
		BigDecimal existingNetAmount = collateralBalanceSimpleJournal.getNetAmount();
		BigDecimal netAmount = BigDecimal.ZERO;
		for (TradeFill tradeFill : CollectionUtils.getIterable(getTradeService().getTradeFillListByTrade(trade.getId()))) {
			netAmount = MathUtils.add(netAmount, trade.isBuy() ? MathUtils.negate(tradeFill.getNotionalTotalPrice()) : tradeFill.getNotionalTotalPrice());
		}

		collateralBalanceSimpleJournal.setNetAmount(MathUtils.add(netAmount, existingNetAmount));
	}


	//Sums accounting transactions for the given trade
	private void computeCollateralPositionMaturityProceeds(CollateralBalanceSimpleJournal collateralBalanceSimpleJournal, Trade trade) {
		BigDecimal maturityProceeds = collateralBalanceSimpleJournal.getMaturityProceeds();
		if (maturityProceeds == null) {
			AccountingTransactionSearchForm transactionSearchForm = new AccountingTransactionSearchForm();
			transactionSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.NON_POSITION_COLLATERAL);
			transactionSearchForm.setTableName("AccountingEventJournalDetail");
			transactionSearchForm.setTransactionDate(trade.getSettlementDate());
			transactionSearchForm.setClientInvestmentAccountId(trade.getClientInvestmentAccount().getId());
			transactionSearchForm.setHoldingInvestmentAccountId(trade.getHoldingInvestmentAccount().getId());
			collateralBalanceSimpleJournal.setMaturityProceeds(getAccountingTransactionValue(transactionSearchForm, getExchangeRate(trade)));
		}
	}


	private void computeCashCollateralBalance(CollateralBalanceSimpleJournal collateralBalanceSimpleJournal, Trade trade) {
		BigDecimal cashCollateralBalance = collateralBalanceSimpleJournal.getCashCollateralBalance();
		if (cashCollateralBalance == null) {
			AccountingTransactionSearchForm transactionSearchForm = new AccountingTransactionSearchForm();
			transactionSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);
			transactionSearchForm.setMaxSettlementDate(trade.getTradeDate());
			transactionSearchForm.setHoldingInvestmentAccountId(trade.getHoldingInvestmentAccount().getId());
			collateralBalanceSimpleJournal.setCashCollateralBalance(getAccountingTransactionValue(transactionSearchForm, getExchangeRate(trade)));
		}
	}


	private BigDecimal getAccountingTransactionValue(AccountingTransactionSearchForm searchForm, BigDecimal exchangeRate) {
		BigDecimal accountingTransactionValue = BigDecimal.ZERO;
		List<AccountingTransaction> accountingTransactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (AccountingTransaction transaction : CollectionUtils.getIterable(accountingTransactionList)) {
			accountingTransactionValue = MathUtils.add(accountingTransactionValue, MathUtils.multiply(transaction.getLocalDebitCredit(), exchangeRate));
		}
		return accountingTransactionValue;
	}


	private BigDecimal getExchangeRate(Trade trade) {
		BigDecimal exchangeRate = BigDecimal.ONE;
		if (!InvestmentUtils.getHoldingAccountBaseCurrency(trade).equals(trade.getInvestmentSecurity())) {
			String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(trade);
			String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(trade);
			exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(trade.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, trade.getTradeDate()));
		}
		return exchangeRate;
	}


	private AccountingSimpleJournal getExistingAccrualJournal(AccountingSimpleJournal journal, Status status) {
		AccountingSimpleJournalSearchForm searchForm = new AccountingSimpleJournalSearchForm();
		searchForm.setClientInvestmentAccountId(journal.getClientInvestmentAccount().getId());
		searchForm.setTransactionDate(journal.getTransactionDate());
		searchForm.setJournalTypeName("Cash Transfer");
		searchForm.setMainHoldingInvestmentAccountId(journal.getMainHoldingInvestmentAccount().getId());
		searchForm.setMainInvestmentSecurityId(journal.getMainInvestmentSecurity().getId());

		List<AccountingSimpleJournal> accountingSimpleJournalList = getAccountingSimpleJournalService().getAccountingSimpleJournalList(searchForm);
		return CollectionUtils.getOnlyElement(accountingSimpleJournalList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingSimpleJournalService getAccountingSimpleJournalService() {
		return this.accountingSimpleJournalService;
	}


	public void setAccountingSimpleJournalService(AccountingSimpleJournalService accountingSimpleJournalService) {
		this.accountingSimpleJournalService = accountingSimpleJournalService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
