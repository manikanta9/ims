package com.clifton.collateral.balance.workflow.futures;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.booking.CollateralBalanceBookingService;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;


/**
 * @author jrichter
 */
public class CollateralBalanceBookFuturesCollateralWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	private CollateralBalanceBookingService collateralBalanceBookingService;


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {

		getCollateralBalanceBookingService().bookAndPostCollateralBalance(collateralBalance);
	}


	public CollateralBalanceBookingService getCollateralBalanceBookingService() {
		return this.collateralBalanceBookingService;
	}


	public void setCollateralBalanceBookingService(CollateralBalanceBookingService collateralBalanceBookingService) {
		this.collateralBalanceBookingService = collateralBalanceBookingService;
	}
}
