package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.collateral.balance.rebuild.processor.position.BaseCollateralStrategyPosition;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralOptionsStrategyJoinedPosition extends CollateralOptionsStrategyPosition implements CollateralOptionsStrategyAggregatePosition {

	private final List<CollateralOptionsStrategyPosition> compositeStrategyPositionList;
	private final List<Long> joinedAccountingTransactionIdList;

	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyJoinedPosition(List<CollateralOptionsStrategyPosition> compositeStrategyPositionList, BigDecimal remainingQuantity) {
		super(CollectionUtils.getFirstElementStrict(compositeStrategyPositionList), remainingQuantity);
		this.compositeStrategyPositionList = compositeStrategyPositionList;
		this.joinedAccountingTransactionIdList = compositeStrategyPositionList.stream().map(CollateralOptionsStrategyPosition::getAccountingTransactionId).collect(Collectors.toList());

		this.compositeStrategyPositionList.forEach(p -> {
			p.setParentStrategyPosition(this);
			p.setPositionGroupId(this.getPositionGroupId());
			p.setExplanationBuilder(this.getExplanationBuilder());
		});
		this.setAccountingTransactionId(null);

		AssertUtils.assertEquals(getRemainingQuantity().compareTo(getCompositeStrategyPositionList().stream()
				.map(BaseCollateralStrategyPosition::getRemainingQuantity)
				.reduce(BigDecimal.ZERO, BigDecimal::add)), 0, "Expected the sum of joined position to equal " + getRemainingQuantity());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String buildPositionExplanation() {
		StringBuilder results = new StringBuilder();

		results.append("Joined Position: \n");
		results.append(super.buildPositionExplanation()).append('\n');
		results.append("Child Positions: \n");
		String positionExplanation = CollectionUtils.buildStream(getCompositeStrategyPositionList())
				.map(CollateralOptionsStrategyPosition::buildPositionExplanation)
				.collect(Collectors.joining("\n"));
		results.append(positionExplanation);

		return results.toString();
	}


	@Override
	@ValueChangingSetter
	public void setRequiredCollateral(BigDecimal requiredCollateral) {
		super.setRequiredCollateral(requiredCollateral);
		// distribute the required Collateral to constituent positions
		// HALF_UP rounding mode to the specified number of decimal places
		this.compositeStrategyPositionList.forEach(p ->
				p.setRequiredCollateral(MathUtils.divide(p.getRemainingQuantity(), this.getRemainingQuantity(), 4).multiply(requiredCollateral).abs()));
	}


	@Override
	@ValueChangingSetter
	public void setFxRateToBase(BigDecimal fxRateToBase) {
		super.setFxRateToBase(fxRateToBase);
		this.compositeStrategyPositionList.forEach(p ->	p.setFxRateToBase(fxRateToBase));
	}


	@Override
	@ValueChangingSetter
	public void setPositionGroupId(String positionGroupId) {
		super.setPositionGroupId(positionGroupId);
		this.compositeStrategyPositionList.forEach(p -> p.setPositionGroupId(positionGroupId));
	}


	@Override
	void setExplanationBuilder(StringBuilder explanation) {
		super.setExplanationBuilder(explanation);
		this.compositeStrategyPositionList.forEach(p -> p.setExplanationBuilder(explanation));
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Long> getJoinedAccountingTransactionIdList() {
		return this.joinedAccountingTransactionIdList;
	}


	@Override
	public List<CollateralOptionsStrategyPosition> getCompositeStrategyPositionList() {
		return this.compositeStrategyPositionList;
	}
}
