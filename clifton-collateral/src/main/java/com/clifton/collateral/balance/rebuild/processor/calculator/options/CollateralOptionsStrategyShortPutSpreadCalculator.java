package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySpreadPosition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * Short Put Spread
 * Initial margin requirement:
 * the amount by which the long put aggregate strike price is below the short put aggregate strike price (aggregate strike price = number of contracts x strike price x $100)
 * long put(s) must be paid for in full
 * proceeds received from sale of short put(s) may be applied to the initial margin requirement
 * the short put(s) may expire before the long put(s) and not affect margin requirement
 *
 * @author StevenF
 */
public class CollateralOptionsStrategyShortPutSpreadCalculator extends CollateralOptionsStrategySpreadCalculator {

	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal amount = doCalculateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(amount);
	}


	@Override
	protected BigDecimal doCalculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getPutRequirementType())
				&& strategyPosition instanceof CollateralOptionsStrategySpreadPosition) {

			CollateralOptionsStrategySpreadPosition spreadPosition = (CollateralOptionsStrategySpreadPosition) strategyPosition;
			CollateralOptionsStrategyPosition longPosition = spreadPosition.getLongStrategyPosition();
			BigDecimal longAggregatePrice = longPosition.getRemainingQuantity().abs().multiply(longPosition.getStrikePrice()).multiply(longPosition.getPriceMultiplier());

			CollateralOptionsStrategyPosition shortPosition = spreadPosition.getShortStrategyPosition();
			BigDecimal shortAggregatePrice = shortPosition.getRemainingQuantity().abs().multiply(shortPosition.getStrikePrice().multiply(shortPosition.getPriceMultiplier()));
			if (shortAggregatePrice.compareTo(longAggregatePrice) > 0) {
				BigDecimal diffProceeds = shortAggregatePrice.subtract(longAggregatePrice);
				BigDecimal optionProceeds = longPosition.getOptionProceeds();
				BigDecimal marginAmount = diffProceeds.add(optionProceeds);

				appender.accept(() -> "Short Put Spread");
				appender.accept(() -> String.format("The Amount long put aggregate strike price [%s] is below the short put [%s]", CoreMathUtils.formatNumber(longAggregatePrice, null), CoreMathUtils.formatNumber(shortAggregatePrice, null)));
				appender.accept(() -> String.format("Aggregate strike price difference [%s]", CoreMathUtils.formatNumber(diffProceeds, null)));
				appender.accept(() -> String.format("Proceeds received from the sale of short puts [%s]", CoreMathUtils.formatNumber(optionProceeds, null)));
				appender.accept(() -> "Positions:");
				appender.accept(strategyPosition::buildPositionExplanation);
				appender.accept(() -> String.format("Margin requirement: [%s]", CoreMathUtils.formatNumber(marginAmount, null)));

				// distribute strangle total to individual constituent long and short
				spreadPosition.getLongStrategyPosition().setRequiredCollateral(optionProceeds);
				spreadPosition.getShortStrategyPosition().setRequiredCollateral(diffProceeds);

				return InvestmentCalculatorUtils.roundLocalAmount(marginAmount, strategyPosition.getInvestmentSecurity());
			}
		}
		return BigDecimal.ZERO;
	}
}
