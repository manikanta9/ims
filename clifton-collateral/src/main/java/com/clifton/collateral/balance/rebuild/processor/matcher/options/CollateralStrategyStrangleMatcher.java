package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyStranglePosition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralStrategyStrangleMatcher extends AggregateCollateralStrategyOptionMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategyStrangleMatcher strangleMatcher = new CollateralStrategyStrangleMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyStranglePositionList(List<CollateralOptionsStrategyPosition> positionInfoList, Date positionDate) {
		return strangleMatcher.getCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), positionDate);
	}


	@Override
	protected Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> doGetCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, PositionMutations positionMutations) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		Map<InstrumentStrangleGrouping, Map<TYPE, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>>> strangleMap = getGroupedPositionsMap(positionInfoList);
		for (Map<TYPE, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>> candidate : strangleMap.values()) {
			// strangle must have PUT and CALL of the same instrument long / short and expiry
			if (candidate.containsKey(TYPE.PUT) && candidate.containsKey(TYPE.CALL)) {
				Map<PositionGrouping, BigDecimal> putQuantities = candidate.get(TYPE.PUT).entrySet().stream()
						.collect(Collectors.toMap
								(
										Map.Entry::getKey,
										e -> e.getValue().stream().map(CollateralOptionsStrategyPosition::getRemainingQuantity).reduce(BigDecimal.ZERO, BigDecimal::add)
								)
						);
				Map<PositionGrouping, BigDecimal> callQuantities = candidate.get(TYPE.CALL).entrySet().stream()
						.collect(Collectors.toMap
								(
										Map.Entry::getKey,
										e -> e.getValue().stream().map(CollateralOptionsStrategyPosition::getRemainingQuantity).reduce(BigDecimal.ZERO, BigDecimal::add)
								)
						);

				final Optional<Map.Entry<PositionGrouping, BigDecimal>> maximumPut = getMaximumPositionGroupingByQuantity(putQuantities);
				final Optional<Map.Entry<PositionGrouping, BigDecimal>> maximumCall = getMaximumPositionGroupingByQuantity(callQuantities);

				if (maximumPut.isPresent() && maximumCall.isPresent()) {
					int comparison = maximumPut.get().getValue().abs().compareTo(maximumCall.get().getValue().abs());
					CollateralOptionsStrategyStranglePosition stranglePosition;
					if (comparison == 0) {
						// puts = calls
						BigDecimal quantity = maximumCall.get().getValue();

						CollateralOptionsStrategyPosition putPosition = getMatchingPositionList(positionMutations, candidate.get(TYPE.PUT).get(maximumPut.get().getKey()), quantity);
						AssertUtils.assertNotNull(putPosition, "Could not determine put position");

						CollateralOptionsStrategyPosition callPosition = getMatchingPositionList(positionMutations, candidate.get(TYPE.CALL).get(maximumCall.get().getKey()), quantity);
						AssertUtils.assertNotNull(callPosition, "Could not determine call position");

						stranglePosition = new CollateralOptionsStrategyStranglePosition(putPosition, callPosition);
					}
					else if (comparison > 0) {
						// puts > calls - call quantity
						BigDecimal quantity = maximumCall.get().getValue();
						CollateralOptionsStrategyPosition callPosition = getMatchingPositionList(positionMutations, candidate.get(TYPE.CALL).get(maximumCall.get().getKey()), quantity);
						AssertUtils.assertNotNull(callPosition, "Could not determine call position");

						List<CollateralOptionsStrategyPosition> positionList = candidate.get(TYPE.PUT).values().stream()
								.flatMap(List::stream)
								.collect(Collectors.toList());
						CollateralOptionsStrategyPosition putPosition = getMatchingPositionList(positionMutations, positionList, quantity);
						AssertUtils.assertNotNull(callPosition, "Could not determine put position");

						stranglePosition = new CollateralOptionsStrategyStranglePosition(putPosition, callPosition);
					}
					else {
						// calls > puts - put quantity
						BigDecimal quantity = maximumPut.get().getValue();
						CollateralOptionsStrategyPosition putPosition = getMatchingPositionList(positionMutations, candidate.get(TYPE.PUT).get(maximumPut.get().getKey()), quantity);
						AssertUtils.assertNotNull(putPosition, "Could not determine put position");

						List<CollateralOptionsStrategyPosition> positionList = candidate.get(TYPE.CALL).values().stream()
								.flatMap(List::stream)
								.collect(Collectors.toList());
						CollateralOptionsStrategyPosition callPosition = getMatchingPositionList(positionMutations, positionList, quantity);
						AssertUtils.assertNotNull(callPosition, "Could not determine call position");

						stranglePosition = new CollateralOptionsStrategyStranglePosition(putPosition, callPosition);
					}
					CollateralOptionsStrategyCalculators calculators = MathUtils.isNegative(stranglePosition.getRemainingQuantity())
							? CollateralOptionsStrategyCalculators.SHORT_STRANGLE : CollateralOptionsStrategyCalculators.LONG_STRANGLE;
					List<CollateralOptionsStrategyPosition> list = results.computeIfAbsent(calculators, k -> new ArrayList<>());
					list.add(stranglePosition);
				}
			}
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<InstrumentStrangleGrouping, Map<TYPE, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>>> getGroupedPositionsMap(List<CollateralOptionsStrategyPosition> positionInfoList) {
		return CollectionUtils.buildStream(CollectionUtils.asNonNullList(positionInfoList))
				// include only options
				.filter(p -> InvestmentUtils.isSecurityOfType(p.getInvestmentSecurity(), InvestmentType.OPTIONS))
				.collect(
						// group primarily on instrument long / short and expiry
						Collectors.groupingBy(
								t -> new InstrumentStrangleGrouping(
										t.getInvestmentSecurity().getInstrument().getId(),
										DateUtils.asLocalDate(t.getInvestmentSecurity().getEndDate()),
										HOLDING.find(t.getRemainingQuantity())
								),
								// group same instruments and expiry further by PUT CALL
								Collectors.groupingBy(
										t -> TYPE.find(t.getOptionType()),
										// group same instruments expiry and type further by strike price, option price and underlying price
										Collectors.groupingBy(
												t -> new PositionGrouping(
														t.getStrikePrice(),
														t.getUnderlyingPrice(),
														t.getOptionPrice()
												)
										)
								)
						)
				);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * group by: long/short, instrument and expiry
	 */
	protected static class InstrumentStrangleGrouping {

		private final long instrumentId;
		private final LocalDate expiry;
		private final HOLDING holding;


		public InstrumentStrangleGrouping(long instrumentId, LocalDate expiry, HOLDING holding) {
			this.instrumentId = instrumentId;
			this.expiry = expiry;
			this.holding = holding;
		}


		@SuppressWarnings("unused")
		public long getInstrumentId() {
			return this.instrumentId;
		}


		public LocalDate getExpiry() {
			return this.expiry;
		}


		public HOLDING getHolding() {
			return this.holding;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			InstrumentStrangleGrouping that = (InstrumentStrangleGrouping) o;
			return this.instrumentId == that.instrumentId &&
					Objects.equals(this.expiry, that.expiry) &&
					this.holding == that.holding;
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.instrumentId, this.expiry);
		}


		@Override
		public String toString() {
			return "InstrumentGrouping{" +
					"instrumentId=" + this.instrumentId +
					", expiry=" + this.expiry +
					", holding=" + this.holding +
					'}';
		}
	}
}
