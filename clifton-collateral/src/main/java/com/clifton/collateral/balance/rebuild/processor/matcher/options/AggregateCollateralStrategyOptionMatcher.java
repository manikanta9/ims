package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.matcher.CollateralStrategyMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyJoinedPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySplitPosition;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public abstract class AggregateCollateralStrategyOptionMatcher<O extends CollateralOptionsStrategyPosition> implements CollateralStrategyMatcher<O> {

	protected static final int MAXIMUM_RECURSIONS = 5;

	public enum TYPE {
		PUT,
		CALL;


		static TYPE find(InvestmentSecurityOptionTypes type) {
			if (type == null) {
				return null;
			}
			return Arrays.stream(TYPE.values()).filter(v -> StringUtils.isEqual(v.name(), type.name())).findFirst().orElse(null);
		}
	}


	protected enum HOLDING {
		LONG,
		SHORT;


		static HOLDING find(BigDecimal qty) {
			if (qty == null) {
				return null;
			}
			return MathUtils.isPositive(qty) ? LONG : SHORT;
		}
	}

	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<CollateralOptionsStrategyCalculators, List<O>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		Map<CollateralOptionsStrategyCalculators, List<O>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);
		boolean mutated = true;
		int maximumRecursions = MAXIMUM_RECURSIONS;
		while (mutated && maximumRecursions > 0) {
			PositionMutations positionMutations = new PositionMutations();
			Map<CollateralOptionsStrategyCalculators, List<O>> groupingMap = doGetCollateralStrategyPositionMap(positionInfoList, collateralPositionList, positionMutations);
			mergeResults(results, groupingMap);
			mutated = positionMutations.mutate(positionInfoList);
			if (groupingMap.isEmpty()) {
				break;
			}
			maximumRecursions--;
		}
		if (maximumRecursions <= 0) {
			LogUtils.error(this.getClass(), String.format("The maximum number of recursions [%s] exceeded.", MAXIMUM_RECURSIONS));
		}

		// update group IDs
		results.forEach((key, value) -> CollectionUtils.getStream(value).forEach(p -> p.setPositionGroupId(key.getTradeStrategyName())));

		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void mergeResults(Map<CollateralOptionsStrategyCalculators, List<O>> results, Map<CollateralOptionsStrategyCalculators, List<O>> groupingMap) {
		for (Map.Entry<CollateralOptionsStrategyCalculators, List<O>> entry : groupingMap.entrySet()) {
			List<O> positions = results.computeIfAbsent(entry.getKey(), k -> new ArrayList<>());
			positions.addAll(entry.getValue());
		}
	}


	/**
	 * Get positions (combining them if necessary) to equal the provided quantity (absolute value).
	 */
	protected CollateralOptionsStrategyPosition getMatchingPositionList(PositionMutations positionMutations, List<CollateralOptionsStrategyPosition> positionDailyList, BigDecimal quantity) {
		if (!CollectionUtils.isEmpty(positionDailyList)) {
			// position quantities greater than or equal to desired
			List<CollateralOptionsStrategyPosition> dailyList = positionDailyList.stream()
					.filter(p -> p.getRemainingQuantity().abs().compareTo(quantity.abs()) >= 0)
					.collect(Collectors.toList());
			if (!dailyList.isEmpty()) {
				// split or existing
				Optional<CollateralOptionsStrategyPosition> equalQuantity = dailyList.stream()
						.filter(p -> p.getRemainingQuantity().abs().compareTo(quantity.abs()) == 0)
						.findFirst();
				CollateralOptionsStrategyPosition optionsStrategyPosition = equalQuantity.orElseGet(() -> new CollateralOptionsStrategySplitPosition(dailyList.get(0), quantity));
				positionMutations.processPosition(optionsStrategyPosition);
				return optionsStrategyPosition;
			}
			else {
				// join or join and split
				List<CollateralOptionsStrategyPosition> sortedPositionList = positionDailyList.stream()
						.sorted(Comparator.comparing(CollateralOptionsStrategyPosition::getRemainingQuantity))
						.collect(Collectors.toList());
				if (MathUtils.isPositive(quantity)) {
					Collections.reverse(sortedPositionList);
				}
				BigDecimal runningQuantity = quantity.abs();
				List<CollateralOptionsStrategyPosition> joinedPositionList = new ArrayList<>();
				for (CollateralOptionsStrategyPosition positionDaily : sortedPositionList) {
					joinedPositionList.add(positionDaily);
					runningQuantity = runningQuantity.subtract(positionDaily.getRemainingQuantity().abs());
					if (runningQuantity.compareTo(BigDecimal.ZERO) <= 0) {
						break;
					}
				}
				if (runningQuantity.compareTo(BigDecimal.ZERO) != 0) {
					CollateralOptionsStrategyPosition splitPosition = joinedPositionList.get(joinedPositionList.size() - 1);
					joinedPositionList.remove(joinedPositionList.size() - 1);
					BigDecimal splitQuantity = splitPosition.getRemainingQuantity().abs().subtract(runningQuantity.abs());
					if (MathUtils.isNegative(quantity)) {
						splitQuantity = splitQuantity.negate();
					}
					CollateralOptionsStrategySplitPosition strategySplitPosition = new CollateralOptionsStrategySplitPosition(splitPosition, splitQuantity);
					positionMutations.processSplitPosition(strategySplitPosition);
					joinedPositionList.add(strategySplitPosition);
				}
				CollateralOptionsStrategyJoinedPosition joinedPosition = new CollateralOptionsStrategyJoinedPosition(joinedPositionList, quantity);
				positionMutations.processJoinPosition(joinedPosition);
				return joinedPosition;
			}
		}
		return null;
	}


	protected Optional<Map.Entry<PositionGrouping, BigDecimal>> getMaximumPositionGroupingByQuantity(Map<PositionGrouping, BigDecimal> positionGroupingMap) {
		return positionGroupingMap.entrySet().stream()
				.filter(e -> Objects.nonNull(e.getValue()))
				.max(Comparator.comparing(e -> e.getValue().abs()));
	}


	protected abstract Map<CollateralOptionsStrategyCalculators, List<O>> doGetCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, PositionMutations positionMutations);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * combining quantities
	 */
	protected static class PositionGrouping {

		private final BigDecimal strikePrice;
		private final BigDecimal underlyingPrice;
		private final BigDecimal optionPrice;


		public PositionGrouping(BigDecimal strikePrice, BigDecimal underlyingPrice, BigDecimal optionPrice) {
			this.strikePrice = strikePrice;
			this.underlyingPrice = underlyingPrice;
			this.optionPrice = optionPrice;
		}


		public BigDecimal getStrikePrice() {
			return this.strikePrice;
		}


		@SuppressWarnings("unused")
		public BigDecimal getUnderlyingPrice() {
			return this.underlyingPrice;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			PositionGrouping that = (PositionGrouping) o;
			return MathUtils.isEqual(this.strikePrice, that.strikePrice) &&
					MathUtils.isEqual(this.underlyingPrice, that.underlyingPrice) &&
					MathUtils.isEqual(this.optionPrice, that.optionPrice);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.strikePrice, this.underlyingPrice);
		}


		@Override
		public String toString() {
			return "PositionGrouping{" +
					"strikePrice=" + this.strikePrice +
					", underlyingPrice=" + this.underlyingPrice +
					", optionPrice=" + this.optionPrice +
					'}';
		}
	}


	/**
	 * updating the list of strategy-potential position for further (recursive) consideration
	 */
	protected static class PositionMutations {

		private List<CollateralOptionsStrategyPosition> additions = new ArrayList<>();
		private List<CollateralOptionsStrategyPosition> removals = new ArrayList<>();


		public void processPosition(CollateralOptionsStrategyPosition strategyPosition) {
			if (strategyPosition instanceof CollateralOptionsStrategySplitPosition) {
				processSplitPosition((CollateralOptionsStrategySplitPosition) strategyPosition);
			}
			else if (strategyPosition instanceof CollateralOptionsStrategyJoinedPosition) {
				processJoinPosition((CollateralOptionsStrategyJoinedPosition) strategyPosition);
			}
			else {
				this.removals.add(strategyPosition);
			}
		}


		public void processSplitPosition(CollateralOptionsStrategySplitPosition splitPosition) {
			this.removals.add(splitPosition.getOriginalCollateralPosition());
			this.additions.add(splitPosition.getRemainderCollateralPosition());
		}


		public void processJoinPosition(CollateralOptionsStrategyJoinedPosition joinedPosition) {
			this.removals.addAll(joinedPosition.getCompositeStrategyPositionList());
		}


		public boolean mutate(List<CollateralOptionsStrategyPosition> positionInfoList) {
			boolean mutated = false;
			if (!CollectionUtils.isEmpty(this.removals)) {
				mutated = positionInfoList.removeAll(this.removals);
			}
			for (CollateralOptionsStrategyPosition addition : CollectionUtils.getIterable(this.additions)) {
				if (!positionInfoList.contains(addition)) {
					positionInfoList.add(addition);
					mutated = true;
				}
			}
			return mutated;
		}
	}
}
