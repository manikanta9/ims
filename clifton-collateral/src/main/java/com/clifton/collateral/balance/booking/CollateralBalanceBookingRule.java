package com.clifton.collateral.balance.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralBalanceBookingRule</code> class is a booking rule that handles NonOTC
 * cash transfers: transfer cash from broker to custodian or vice versa.
 *
 * @author vgomelsky
 */
public class CollateralBalanceBookingRule implements AccountingBookingRule<CollateralBalance> {

	private AccountingAccountService accountingAccountService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CollateralBalanceService collateralBalanceService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<CollateralBalance> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		CollateralBalance balance = bookingSession.getBookableEntity();

		// skip zero amount transfers
		if (!MathUtils.isNullOrZero(balance.getTransferAmount())) {
			String description = (isBaseCurrency(balance) ? "Cash" : "Currency") + " Collateral Transfer";
			journal.setDescription(description);
			journal.addJournalDetail(generateJournalDetail(journal, balance, true, description));
			journal.addJournalDetail(generateJournalDetail(journal, balance, false, description));
		}
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, CollateralBalance balance, boolean mainEntry, String description) {
		// collateral analysis is done for previous business day so the transfer must be on the next day
		Date nextDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(balance.getBalanceDate()), 1);

		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(journal.getFkFieldId());
		detail.setSystemTable(journal.getSystemTable());
		detail.setClientInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), nextDay, true, true, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME));

		// usually full collateral is in base currency of holding account but it could also be in local currency of each position
		if (isBaseCurrency(balance)) {
			detail.setInvestmentSecurity(balance.getHoldingInvestmentAccount().getBaseCurrency());
		}
		else {
			detail.setInvestmentSecurity(balance.getCollateralCurrency());
		}

		detail.setDescription(description);

		BigDecimal amount = balance.getTransferAmount();
		if (mainEntry) {
			amount = amount.negate();
			detail.setHoldingInvestmentAccount(balance.getHoldingInvestmentAccount());
			if (isBaseCurrency(balance)) {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH_COLLATERAL));
			}
			else {
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY_COLLATERAL));
			}
		}
		else {
			detail.setHoldingInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), nextDay, false, true, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME));
			// money market accounts have purchase/sale of Money Market as opposed to Cash
			if (AccountingAccount.ASSET_MONEY_MARKET.equals(detail.getHoldingInvestmentAccount().getType().getName())) {
				ValidationUtils.assertTrue(isBaseCurrency(balance), "Money Market transfers are not supported for currencies. Holding Account: " + balance.getHoldingInvestmentAccount().getLabel());
				detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_MONEY_MARKET));
			}
			else {
				if (isBaseCurrency(balance)) {
					detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
				}
				else {
					detail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
				}
			}
		}

		detail.setLocalDebitCredit(amount);
		if (isBaseCurrency(balance)) {
			detail.setBaseDebitCredit(amount);
			detail.setLocalDebitCredit(amount);
			detail.setPositionCostBasis(BigDecimal.ZERO);
			detail.setExchangeRateToBase(BigDecimal.ONE);
			detail.setPositionCostBasis(BigDecimal.ZERO);
		}
		else {
			// need to lookup latest FX rate from holding company
			String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(detail);
			String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(detail);
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(detail.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, nextDay).flexibleLookup());
			detail.setExchangeRateToBase(fxRate);
			detail.setLocalDebitCredit(amount);
			detail.setBaseDebitCredit(MathUtils.multiply(amount, fxRate, 2));
			detail.setPositionCostBasis(amount);
		}
		detail.setQuantity(null);
		detail.setPrice(null);

		detail.setSettlementDate(nextDay);
		detail.setOriginalTransactionDate(nextDay);
		detail.setTransactionDate(nextDay);

		// questionable fields other than possibly cost basis (may need to delete later)
		detail.setOpening(true);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}


	private boolean isBaseCurrency(CollateralBalance balance) {
		if (balance.getCollateralCurrency() != null) {
			if (!balance.getCollateralCurrency().equals(balance.getHoldingInvestmentAccount().getBaseCurrency())) {
				return false;
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
