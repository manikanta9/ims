package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * <p>
 * Short Put (Equity)
 * Initial margin requirement:
 * 100% of option proceeds, plus 20% of underlying security value less out-of-the-money amount, if any
 * minimum requirement is option proceeds plus 10% of the put’s aggregate exercise price (number of contracts x exercise price x $100)
 * proceeds received from sale of puts(s) may be applied to the initial margin requirement
 * after position is established, ongoing maintenance margin requirement applies, and an increase (or decrease) in the margin required is possible
 * <p>
 * Short Put (Index)
 * Initial margin requirement:l
 * 100% of option proceeds, plus 15% of aggregate underlying index value (number of contracts x index level x $100) less out-of-the-money amount, if any
 * minimum requirement is option proceeds plus 10% of the put’s aggregate strike price (number of contracts x strike price x $100)
 * proceeds received from sale of puts(s) may be applied to the initial margin requirement
 * after position is established, ongoing maintenance margin requirement applies, and an increase (or decrease) in the margin required is possible
 *
 * @author StevenF
 */
public class CollateralOptionsStrategyShortPutCalculator implements CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> {

	@Override
	public Map<Long, BigDecimal> calculateCollateral(CollateralOptionsStrategyCommand command) {
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();
		for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(command.getPositionList())) {
			collateralRequirementMap.put(strategyPosition.getAccountingTransactionId(), calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition));
		}
		return collateralRequirementMap;
	}


	@Override
	public BigDecimal calculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition) {
		return doCalculateCollateral(processor, strategyPosition, s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s)));
	}


	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal requiredCollateral = doCalculateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(requiredCollateral);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal doCalculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		BigDecimal results = strategyPosition.getStrikeValue();

		BigDecimal strikePrice = strategyPosition.getStrikePrice();
		BigDecimal strikeValue = strategyPosition.getStrikeValue();
		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getPutRequirementType())) {
			BigDecimal optionProceeds = strategyPosition.getOptionProceeds();
			BigDecimal quantity = strategyPosition.getRemainingQuantity();
			BigDecimal underlyingPrice = strategyPosition.getUnderlyingPrice();
			BigDecimal marginPercent = processor.getMarginPercentage(strategyPosition.getInvestmentSecurity());
			BigDecimal underlyingValue = strategyPosition.getUnderlyingValue();

			appender.accept(() -> "Short Put");
			appender.accept(() -> String.format("100 percent of Option Proceeds [%s]", CoreMathUtils.formatNumber(optionProceeds, null)));
			appender.accept(() -> String.format("Plus [%s] percent of aggregate underlying value [%s]", CoreMathUtils.formatNumber(marginPercent, null), CoreMathUtils.formatNumber(strategyPosition.getUnderlyingValue(), null)));

			BigDecimal calculatedRequirement = MathUtils.add(optionProceeds, MathUtils.multiply(underlyingValue, marginPercent));
			//Now subtract 'out-of-the-money' if applicable:
			if (MathUtils.isGreaterThan(underlyingPrice, strikePrice)) {
				BigDecimal outOfTheMoney = MathUtils.multiply(MathUtils.subtract(underlyingPrice, strikePrice), MathUtils.multiply(MathUtils.abs(quantity), strategyPosition.getPriceMultiplier()));
				calculatedRequirement = MathUtils.subtract(calculatedRequirement, outOfTheMoney);

				appender.accept(() -> "Put is Out-Of-Money");
				appender.accept(() -> String.format("Underlying price [%s] > strike price [%s].", CoreMathUtils.formatNumber(underlyingPrice, null), CoreMathUtils.formatNumber(strikePrice, null)));
				appender.accept(() -> String.format("Subtract out of money amount [%s]", CoreMathUtils.formatNumber(outOfTheMoney, null)));
			}
			BigDecimal defaultMarginRequirement = MathUtils.add(optionProceeds, MathUtils.multiply(strikeValue, CollateralBalanceOptionsRebuildProcessor.DEFAULT_MINIMUM_MARGIN_REQUIREMENT_PERCENTAGE));
			//Use the default if greater than the calculated
			if (MathUtils.isGreaterThan(defaultMarginRequirement, calculatedRequirement)) {

				String calculatedAmountString = CoreMathUtils.formatNumber(calculatedRequirement, null);
				appender.accept(() -> String.format("Minimum margin [%s] < calculated [%s], use minimum margin amount.", CoreMathUtils.formatNumber(defaultMarginRequirement, null), calculatedAmountString));
				appender.accept(() -> String.format("Minimum Margin: 10 percent of aggregate strike price [%s]x[%s]x100.", CoreMathUtils.formatNumber(strikePrice, null), CoreMathUtils.formatNumber(quantity, null)));

				calculatedRequirement = defaultMarginRequirement;
			}
			results = calculatedRequirement;

			String marginRequirement = CoreMathUtils.formatNumber(results, null);
			results = results.subtract(optionProceeds);
			String marginCall = CoreMathUtils.formatNumber(results, null);

			appender.accept(() -> "Positions:");
			appender.accept(strategyPosition::buildPositionExplanation);
			appender.accept(() -> "Initial Margin");
			appender.accept(() -> String.format("Margin Requirement [%s]", marginRequirement));
			appender.accept(() -> String.format("Proceeds from sale of short put(s) [%s]", CoreMathUtils.formatNumber(optionProceeds, null)));
			appender.accept(() -> String.format("Margin Call [%s]", marginCall));
		}

		return InvestmentCalculatorUtils.roundLocalAmount(results, strategyPosition.getInvestmentSecurity());
	}
}
