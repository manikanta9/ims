package com.clifton.collateral.balance.instruction;


import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;


/**
 * The <code>CollateralBalanceClientInstructionSelector</code> defines the bean type
 * used to select corresponding {@link InvestmentInstructionDefinition} for each {@link CollateralBalance} record
 * that defines the instructions for the "client" i.e. custodian/collateral account
 * that is sent to the holding account "Recipient"
 *
 * @author manderson
 */
public class CollateralBalanceClientInstructionSelector extends BaseCollateralBalanceInstructionSelector {

	@Override
	public BusinessCompany getRecipient(CollateralBalance entity) {
		return entity.getHoldingInvestmentAccount().getIssuingCompany();
	}
}
