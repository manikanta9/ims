package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class can be used to specify the minimum and maximum threshold percent of excess external collateral percentage.
 * This can be overridden at the holding account level.
 * Different rules are implemented that allow for different threshold values based on accounts with cash and/or securities collateral.
 * {@link CollateralBalanceCashThresholdRangeRuleEvaluator}
 * {@link CollateralBalanceSecuritiesThresholdRangeRuleEvaluator}
 *
 * @author stevenf
 */
public abstract class BaseCollateralBalanceThresholdRangeRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance balance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (evaluateThresholdRangeRule(balance) || isAccountWithoutPositionsWithCollateralRequirement(balance)) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(balance.getHoldingInvestmentAccount()));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				BigDecimal rangeValue = context.getCollateralBalanceThresholdRangeValue(balance);
				if (rangeValue != null) {
					Map<String, Object> templateValues = new HashMap<>();
					BigDecimal externalExcessCollateral = context.getExternalExcessCollateral(balance);
					//We pass the value as the 'deviationValue' to the 'isEntityConfigRangeViolated' function for evaluation which also
					// adds values to the 'templateValues' map for use in the freemarker template; however, even if the range is not violated,
					// if the excess collateral is negative then operations must take action and we throw the violation anyway.
					if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, externalExcessCollateral, entityConfig, templateValues) || MathUtils.isLessThan(externalExcessCollateral, BigDecimal.ZERO)) {
						//Because we have a two part conditional above and it is possible the first condition is not violated, but the second is
						// we need to explicitly set the template values when the second condition is violated to ensure the values are available
						// for the freemarker template evaluation.
						if (MathUtils.isLessThan(externalExcessCollateral, BigDecimal.ZERO)) {
							templateValues.put("lessThanRange", true);
							templateValues.put("greaterThanRange", false);
						}
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntity(entityConfig, BeanUtils.getIdentityAsLong(balance), BeanUtils.getIdentityAsLong(balance), templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}


	public abstract boolean evaluateThresholdRangeRule(CollateralBalance balance);


	/**
	 * When there is a new account that has no positions or cash, but has an externalCollateralRequirement we still want
	 * to process the rule so that it triggers a violation and Operations knows they need to take action on the account.
	 */
	private boolean isAccountWithoutPositionsWithCollateralRequirement(CollateralBalance balance) {
		if (balance.getExternalCollateralRequirement() != null && MathUtils.isGreaterThan(balance.getExternalCollateralRequirement(), BigDecimal.ZERO)
				&& (MathUtils.isNullOrZero(balance.getCashCollateralValue())) && (MathUtils.isNullOrZero(balance.getSecuritiesCollateralValue()))) {
			return true;
		}
		return false;
	}
}
