package com.clifton.collateral.balance;

import com.clifton.collateral.CollateralType;


/**
 * @author terrys
 */
public class CollateralBalanceFactory {

	private CollateralBalanceFactory() {
		// factory
	}


	public static CollateralBalance createCollateralBalance(CollateralType type) {
		CollateralBalanceCategories category = (type.getCategory() == null) ? CollateralBalanceCategories.DEFAULT : type.getCategory();
		CollateralBalance collateralBalance;
		try {
			collateralBalance = category.getImplementationClass().newInstance();
		}
		catch (Exception e) {
			throw new RuntimeException("Error instantiating an object of type: " + category.getImplementationClass().getName(), e);
		}
		collateralBalance.setCollateralType(type);
		return collateralBalance;
	}
}
