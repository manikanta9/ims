package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.matcher.CollateralStrategyMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Zero quantities or NOT AN OPTION
 *
 * @author terrys
 */
public class CollateralStrategyZeroMatcher implements CollateralStrategyMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategyZeroMatcher zeroMatcher = new CollateralStrategyZeroMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyZeroPositionList(List<CollateralOptionsStrategyPosition> positionInfoList, Date positionDate) {
		return zeroMatcher.getCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), positionDate);
	}


	@Override
	public Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		List<CollateralOptionsStrategyPosition> zeroList = positionInfoList.stream()
				.filter(p -> MathUtils.isNullOrZero(p.getRemainingQuantity()) || !InvestmentUtils.isOption(p.getInvestmentSecurity()))
				.collect(Collectors.toList());

		if (!zeroList.isEmpty()) {
			results.put(CollateralOptionsStrategyCalculators.ZERO, zeroList);
		}

		// update group IDs
		results.forEach((key, value) -> value.forEach(p -> p.setPositionGroupId(key.getTradeStrategyName())));

		// remove matches from the position list
		List<CollateralOptionsStrategyPosition> removals = results.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		positionInfoList.removeAll(removals);

		return results;
	}
}
