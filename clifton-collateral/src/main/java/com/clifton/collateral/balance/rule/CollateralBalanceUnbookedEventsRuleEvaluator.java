package com.clifton.collateral.balance.rule;

import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * This <code>CollateralBalanceUnbookedEventsRuleEvaluator</code> is a rule evaluator that generates a rule violation if there exists any
 * unbooked InvestmentSecurityEvent related to the OTC collateral balance holding account.
 *
 * @author jonathanr
 */
public class CollateralBalanceUnbookedEventsRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {

	private AccountingEventJournalService accountingEventJournalService;


	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance collateralBalance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(collateralBalance.getHoldingInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			AccountingEventJournalDetailSearchForm accountingEventJournalDetailSearchForm = new AccountingEventJournalDetailSearchForm();
			accountingEventJournalDetailSearchForm.setHoldingInvestmentAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
			accountingEventJournalDetailSearchForm.setJournalNotBooked(true);
			accountingEventJournalDetailSearchForm.setEventDateBefore(collateralBalance.getBalanceDate());

			List<AccountingEventJournalDetail> eventJournalDetails = getAccountingEventJournalService().getAccountingEventJournalDetailList(accountingEventJournalDetailSearchForm);

			for (AccountingEventJournalDetail detail : CollectionUtils.getIterable(eventJournalDetails)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, collateralBalance.getId(), detail.getJournal().getSecurityEvent().getId(), null));
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
