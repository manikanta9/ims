package com.clifton.collateral.balance;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.collateral.balance.search.CollateralBalanceLendingRepoSearchForm;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralBalanceService</code> defines methods for working with CollateralBalance objects.
 *
 * @author Mary Anderson
 */
public interface CollateralBalanceService {

	public static final String OTC_COLLATERAL_BALANCE_WORKFLOW_NAME = "Options and OTC Collateral Workflow";
	public static final String OPTIONS_COLLATERAL_BALANCE_WORKFLOW_NAME = "Options and OTC Collateral Workflow";

	//////////////////////////////////////////////////////////////////////////// 
	////////         Collateral Balance Business Methods               ///////// 
	//////////////////////////////////////////////////////////////////////////// 


	public CollateralBalance getCollateralBalance(int id);


	public List<CollateralBalance> getCollateralBalanceList(CollateralBalanceSearchForm searchForm);


	public CollateralBalance saveCollateralBalance(CollateralBalance bean);


	public void deleteCollateralBalance(int id);

	////////////////////////////////////////////////////////////////////////////
	//////          Collateral Position Daily Business Methods            //////
	////////////////////////////////////////////////////////////////////////////


	public List<AccountingPositionDaily> getCollateralAccountingPositionDailyLiveList(String collateralTypeName, int holdingAccountId, Date positionDate, boolean summarizePositions);


	@DoNotAddRequestMapping
	public List<AccountingPositionDaily> getCollateralAccountingPositionDailyLiveList(CollateralBalanceRebuildProcessor<?> processor, int holdingAccountId, Date positionDate, boolean useSettlementDate, boolean summarizePositions);
	////////////////////////////////////////////////////////////////////////////
	//////       Repo Collateral Balance Detail Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CollateralBalance.class)
	public List<CollateralBalanceLendingRepo> getCollateralBalanceLendingRepoList(CollateralBalanceLendingRepoSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	//////       Repo Collateral Balance Account Business Methods         //////
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public InvestmentAccount getCollateralBalanceRelatedAccountByPurposes(InvestmentAccount holdingAccount, Date balanceDate, boolean ourAccount, boolean validate, String... relationshipPurposes);
}
