package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.balance.rebuild.processor.position.BaseCollateralStrategyPosition;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Supplier;


/**
 * The <code>CollateralOptionsStrategyPosition</code> is an Options specific implementation of
 * <code>CollateralStrategyPosition</code>.  It provides all of the necessary field and values
 * used in properly calculating required collateral for a given trade strategy. (e.g. PUT_SPREAD, OPTION_STRANGLE, etc.)
 *
 * @author StevenF
 */
public class CollateralOptionsStrategyPosition extends BaseCollateralStrategyPosition<AccountingPositionDaily> {

	private final BigDecimal strikeValue;
	private final BigDecimal underlyingPrice;
	private final BigDecimal underlyingValue;
	private final BigDecimal optionPrice;
	private final BigDecimal optionProceeds;

	// mutable
	private Long accountingTransactionId;
	private CollateralOptionsStrategyPosition parentStrategyPosition;

	private StringBuilder explanation = new StringBuilder();

	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyPosition(AccountingPositionDaily accountingPositionInfo, BigDecimal strikeValue,
	                                         BigDecimal underlyingPrice, BigDecimal underlyingValue, BigDecimal optionPrice, BigDecimal optionProceeds, Long accountingTransactionId) {
		this(accountingPositionInfo.getPositionDate(), accountingPositionInfo.getInvestmentSecurity(), accountingPositionInfo.getInvestmentSecurity().getPriceMultiplier(),
				accountingPositionInfo.getRemainingQuantity(), accountingPositionInfo.getPositionGroupId(), strikeValue, underlyingPrice, underlyingValue, optionPrice,
				optionProceeds, accountingTransactionId);
	}


	public CollateralOptionsStrategyPosition(CollateralOptionsStrategyPosition collateralPosition) {
		this(
				collateralPosition.getPositionDate(),
				collateralPosition.getInvestmentSecurity(),
				collateralPosition.getPriceMultiplier(),
				collateralPosition.getRemainingQuantity(),
				collateralPosition.getPositionGroupId(),

				collateralPosition.strikeValue,
				collateralPosition.underlyingPrice,
				collateralPosition.underlyingValue,
				collateralPosition.optionPrice,
				collateralPosition.optionProceeds,

				collateralPosition.getAccountingTransactionId()
		);
	}


	/**
	 * Clones the values from the provided collateral Position and recalculates values based on the provided quantity.
	 */
	public CollateralOptionsStrategyPosition(CollateralOptionsStrategyPosition collateralPosition, BigDecimal remainingQuantity) {
		this(
				collateralPosition.getPositionDate(),
				collateralPosition.getInvestmentSecurity(),
				collateralPosition.getPriceMultiplier(),
				remainingQuantity,
				collateralPosition.getPositionGroupId(),

				MathUtils.abs(MathUtils.multiply(remainingQuantity, MathUtils.multiply(collateralPosition.getStrikePrice(), collateralPosition.getPriceMultiplier()))),
				collateralPosition.underlyingPrice,
				MathUtils.abs(MathUtils.multiply(remainingQuantity, MathUtils.multiply(collateralPosition.getUnderlyingPrice(), collateralPosition.getPriceMultiplier()))),
				collateralPosition.optionPrice,
				MathUtils.abs(MathUtils.multiply(remainingQuantity, MathUtils.multiply(collateralPosition.getOptionPrice(), collateralPosition.getPriceMultiplier()))),

				collateralPosition.getAccountingTransactionId()
		);
	}


	public CollateralOptionsStrategyPosition(Date positionDate, InvestmentSecurity investmentSecurity, BigDecimal priceMultiplier, BigDecimal remainingQuantity, String positionGroupId,
	                                         BigDecimal strikeValue, BigDecimal underlyingPrice, BigDecimal underlyingValue, BigDecimal optionPrice, BigDecimal optionProceeds, Long accountingTransactionId) {
		super(positionDate, investmentSecurity, priceMultiplier, remainingQuantity, positionGroupId);

		this.strikeValue = strikeValue;
		this.underlyingPrice = underlyingPrice;
		this.underlyingValue = underlyingValue;
		this.optionPrice = optionPrice;
		this.optionProceeds = optionProceeds;

		this.accountingTransactionId = accountingTransactionId;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String buildPositionExplanation() {
		StringBuilder builder = new StringBuilder();

		builder
				.append(MathUtils.isNegative(getRemainingQuantity()) ? "Short " : "Long ")
				.append(CoreMathUtils.formatNumber(getRemainingQuantity().abs(), null)).append(" ")
				.append(getInvestmentSecurity().getSymbol())
				.append(" at ")
				.append(CoreMathUtils.formatNumber(getStrikePrice(), null));
		if (getAccountingTransactionId() != null) {
			builder.append(" ID ").append(getAccountingTransactionId());
		}

		return builder.toString();
	}


	@Override
	public String toString() {
		return "{" +
				super.toString() +
				"optionType='" + getInvestmentSecurity().getOptionType() + '\'' +
				", strikePrice=" + getInvestmentSecurity().getOptionStrikePrice() +
				", strikeValue=" + this.strikeValue +
				", underlyingPrice=" + this.underlyingPrice +
				", underlyingValue=" + this.underlyingValue +
				", optionPrice=" + this.optionPrice +
				", optionProceeds=" + this.optionProceeds +
				", accountingTransactionId=" + this.accountingTransactionId +
				'}';
	}


	StringBuilder getExplanationBuilder() {
		return this.explanation;
	}


	void setExplanationBuilder(StringBuilder explanation) {
		this.explanation = explanation;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityOptionTypes getOptionType() {
		return getInvestmentSecurity().getOptionType();
	}


	public BigDecimal getStrikePrice() {
		return getInvestmentSecurity().getOptionStrikePrice();
	}


	public BigDecimal getStrikeValue() {
		return this.strikeValue;
	}


	public BigDecimal getUnderlyingPrice() {
		return this.underlyingPrice;
	}


	public BigDecimal getUnderlyingValue() {
		return this.underlyingValue;
	}


	public BigDecimal getOptionPrice() {
		return this.optionPrice;
	}


	public BigDecimal getOptionProceeds() {
		return this.optionProceeds;
	}


	public String getCalculationExplanation() {
		return this.explanation.toString();
	}


	public void appendToExplanation(Supplier<String> messageSource) {
		this.explanation.append('\n');
		this.explanation.append(messageSource.get());
	}


	public Long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(Long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public CollateralOptionsStrategyPosition getParentStrategyPosition() {
		return this.parentStrategyPosition;
	}


	public void setParentStrategyPosition(CollateralOptionsStrategyPosition parentStrategyPosition) {
		this.parentStrategyPosition = parentStrategyPosition;
	}
}
