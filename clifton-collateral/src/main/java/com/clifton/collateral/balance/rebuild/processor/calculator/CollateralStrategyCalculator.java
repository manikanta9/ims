package com.clifton.collateral.balance.rebuild.processor.calculator;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.CollateralStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.BaseCollateralStrategyPosition;

import java.math.BigDecimal;
import java.util.Map;


/**
 * The <code>CollateralStrategyCalculator</code> provides an implementation framework
 * for allowing various collateral calculation strategies.  For instance, Options utilize
 * Margin and Escrow calculations which vary widely based on the trade strategies used.
 * e.g. Option Strangle, Short Call, Long Put, Put Spread, etc.
 * The calculators simply utilize the strategy specific command objects values to compute
 * the appropriate collateral for a given position.
 *
 * @author StevenF
 */
public interface CollateralStrategyCalculator<C extends CollateralStrategyCommand<T>, T extends CollateralBalanceRebuildProcessor, S extends BaseCollateralStrategyPosition<?>> {

	/**
	 * Used by collateral types other than {@link com.clifton.collateral.CollateralType#COLLATERAL_TYPE_OPTIONS_MARGIN}
	 */
	public Map<Long, BigDecimal> calculateCollateral(C command);


	/**
	 * Used by collateral types other than {@link com.clifton.collateral.CollateralType#COLLATERAL_TYPE_OPTIONS_MARGIN}
	 */
	public BigDecimal calculateCollateral(T processor, S strategyPosition);


	/**
	 * Used by {@link com.clifton.collateral.CollateralType#COLLATERAL_TYPE_OPTIONS_MARGIN}
	 */
	public void populateCollateral(T processor, S strategyPosition, boolean showExplanation);
}
