package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author terrys
 */
public class CollateralOptionsStrategySpreadPosition extends CollateralOptionsStrategyPosition implements CollateralOptionsStrategyAggregatePosition {

	private final CollateralOptionsStrategyPosition longStrategyPosition;
	private final CollateralOptionsStrategyPosition shortStrategyPosition;

	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategySpreadPosition(CollateralOptionsStrategyPosition longStrategyPosition, CollateralOptionsStrategyPosition shortStrategyPosition) {
		// select the long, no reason
		super(longStrategyPosition);

		this.longStrategyPosition = longStrategyPosition;
		this.shortStrategyPosition = shortStrategyPosition;

		this.longStrategyPosition.setParentStrategyPosition(this);
		this.shortStrategyPosition.setParentStrategyPosition(this);
		this.longStrategyPosition.setExplanationBuilder(this.getExplanationBuilder());
		this.shortStrategyPosition.setExplanationBuilder(this.getExplanationBuilder());
		this.setAccountingTransactionId(null);

		AssertUtils.assertEquals(0, this.longStrategyPosition.getRemainingQuantity().compareTo(this.shortStrategyPosition.getRemainingQuantity().negate()),
				String.format("Spreads must have opposite short [%s] and long [%s] quantities", this.longStrategyPosition.getRemainingQuantity(),
						this.shortStrategyPosition.getRemainingQuantity()));
		AssertUtils.assertTrue(this.longStrategyPosition.getOptionType() == this.shortStrategyPosition.getOptionType(),
				"Spreads must have the same types (PUT or CALL).");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String buildPositionExplanation() {
		StringBuilder results = new StringBuilder();

		results.append("Long Position: \n");
		results.append(getLongStrategyPosition().buildPositionExplanation()).append('\n');
		results.append("Short Position: \n");
		results.append(getShortStrategyPosition().buildPositionExplanation());

		return results.toString();
	}


	@Override
	@ValueChangingSetter
	public void setRequiredCollateral(BigDecimal requiredCollateral) {
		super.setRequiredCollateral(requiredCollateral);
		AssertUtils.assertTrue(MathUtils.isEqual(getRequiredCollateral(), this.longStrategyPosition.getRequiredCollateral().add(this.shortStrategyPosition.getRequiredCollateral())),
				() -> String.format("Expected the long [%s] and short [%s] positions required collateral to equal the spread collateral [%s].",
						this.longStrategyPosition.getRequiredCollateral(), this.shortStrategyPosition.getRequiredCollateral(), this.getRequiredCollateral()));
	}


	@Override
	@ValueChangingSetter
	public void setFxRateToBase(BigDecimal fxRateToBase) {
		super.setFxRateToBase(fxRateToBase);
		this.shortStrategyPosition.setFxRateToBase(fxRateToBase);
		this.longStrategyPosition.setFxRateToBase(fxRateToBase);
	}


	@Override
	@ValueChangingSetter
	public void setPositionGroupId(String positionGroupId) {
		super.setPositionGroupId(positionGroupId);
		this.shortStrategyPosition.setPositionGroupId(positionGroupId);
		this.longStrategyPosition.setPositionGroupId(positionGroupId);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyPosition getLongStrategyPosition() {
		return this.longStrategyPosition;
	}


	public CollateralOptionsStrategyPosition getShortStrategyPosition() {
		return this.shortStrategyPosition;
	}


	@Override
	public List<CollateralOptionsStrategyPosition> getCompositeStrategyPositionList() {
		List<CollateralOptionsStrategyPosition> removals = new ArrayList<>();
		List<CollateralOptionsStrategyPosition> results = Stream.of(this.shortStrategyPosition, this.longStrategyPosition)
				.flatMap(p ->
						{
							if (p instanceof CollateralOptionsStrategyJoinedPosition) {
								removals.add(p);
								return ((CollateralOptionsStrategyJoinedPosition) p).getCompositeStrategyPositionList().stream();
							}
							else {
								return Stream.of(p);
							}
						}
				)
				.collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(removals)) {
			results.removeAll(removals);
		}
		return results;
	}
}
