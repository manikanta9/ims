package com.clifton.collateral.balance.pledged;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralBalancePledgedTransaction</code> class defines the amount of a collateral security that is
 * posted for position of a specific security.  For example, this table will be used to track bonds posted as
 * collateral for specific options securities.
 *
 * @author mwacker
 */
public class CollateralBalancePledgedTransaction extends BaseEntity<Long> {

	/**
	 * The parent transaction.  Used when removing a posted collateral transaction.
	 */
	@NonPersistentField
	private CollateralBalancePledgedTransaction parentTransaction;

	private Date postedDate;
	private InvestmentAccount holdingInvestmentAccount;

	/**
	 * The security of the position for which the collateral is posted.
	 */
	private InvestmentSecurity positionInvestmentSecurity;

	/**
	 * The collateral security.
	 */
	private InvestmentSecurity postedCollateralInvestmentSecurity;

	/**
	 * The quantity of the collateral security.
	 */
	private BigDecimal postedCollateralQuantity;

	/**
	 * The transfer that was created for this pledged transaction.
	 */
	private AccountingPositionTransfer positionTransfer;


	////////////////////////////////////////////////////////////////////////////

	//transient field used for UI display only
	@NonPersistentField
	private BigDecimal postedCollateralMarketValue;

	////////////////////////////////////////////////////////////////////////////


	public String toStringFormatted() {
		return DateUtils.fromDate(getPostedDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + getPositionInvestmentSecurity().getSymbol() + "\t" + getPostedCollateralInvestmentSecurity().getLabel() + "\t" + CoreMathUtils.formatNumber(getPostedCollateralQuantity(), "#,###.00");
	}
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalancePledgedTransaction getParentTransaction() {
		return this.parentTransaction;
	}


	public void setParentTransaction(CollateralBalancePledgedTransaction parentTransaction) {
		this.parentTransaction = parentTransaction;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public Date getPostedDate() {
		return this.postedDate;
	}


	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}


	public InvestmentSecurity getPositionInvestmentSecurity() {
		return this.positionInvestmentSecurity;
	}


	public void setPositionInvestmentSecurity(InvestmentSecurity positionInvestmentSecurity) {
		this.positionInvestmentSecurity = positionInvestmentSecurity;
	}


	public InvestmentSecurity getPostedCollateralInvestmentSecurity() {
		return this.postedCollateralInvestmentSecurity;
	}


	public void setPostedCollateralInvestmentSecurity(InvestmentSecurity postedCollateralInvestmentSecurity) {
		this.postedCollateralInvestmentSecurity = postedCollateralInvestmentSecurity;
	}


	public BigDecimal getPostedCollateralQuantity() {
		return this.postedCollateralQuantity;
	}


	public void setPostedCollateralQuantity(BigDecimal postedCollateralQuantity) {
		this.postedCollateralQuantity = postedCollateralQuantity;
	}


	public AccountingPositionTransfer getPositionTransfer() {
		return this.positionTransfer;
	}


	public void setPositionTransfer(AccountingPositionTransfer positionTransfer) {
		this.positionTransfer = positionTransfer;
	}


	public BigDecimal getPostedCollateralMarketValue() {
		return this.postedCollateralMarketValue;
	}


	public void setPostedCollateralMarketValue(BigDecimal postedCollateralMarketValue) {
		this.postedCollateralMarketValue = postedCollateralMarketValue;
	}
}
