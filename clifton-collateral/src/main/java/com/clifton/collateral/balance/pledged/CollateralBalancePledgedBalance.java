package com.clifton.collateral.balance.pledged;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralBalancePledgedBalance</code> represents an aggregate of CollateralBalancePledgedTransaction for a particular
 * holding account, security and posted collateral security.
 *
 * @author mwacker
 */
public class CollateralBalancePledgedBalance extends BaseSimpleEntity<String> {

	//Explicitly set because hibernate can't bind to the generics.
	private String uuid;

	private InvestmentAccount holdingInvestmentAccount;

	/**
	 * The security of the position for which the collateral is posted.
	 */
	private InvestmentSecurity positionInvestmentSecurity;

	/**
	 * The collateral security.
	 */
	private InvestmentSecurity postedCollateralInvestmentSecurity;

	/**
	 * The quantity of the collateral security.
	 */
	private BigDecimal postedCollateralQuantity;

	/**
	 * Only set when requested by search form (e.g. for UI display);
	 */
	private BigDecimal postedCollateralMarketValue;
	/**
	 * Used when searching for most recent prior pledged collateral for returns
	 */
	private Date postedDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentSecurity getPositionInvestmentSecurity() {
		return this.positionInvestmentSecurity;
	}


	public void setPositionInvestmentSecurity(InvestmentSecurity positionInvestmentSecurity) {
		this.positionInvestmentSecurity = positionInvestmentSecurity;
	}


	public InvestmentSecurity getPostedCollateralInvestmentSecurity() {
		return this.postedCollateralInvestmentSecurity;
	}


	public void setPostedCollateralInvestmentSecurity(InvestmentSecurity postedCollateralInvestmentSecurity) {
		this.postedCollateralInvestmentSecurity = postedCollateralInvestmentSecurity;
	}


	public BigDecimal getPostedCollateralQuantity() {
		return this.postedCollateralQuantity;
	}


	public void setPostedCollateralQuantity(BigDecimal postedCollateralQuantity) {
		this.postedCollateralQuantity = postedCollateralQuantity;
	}


	public BigDecimal getPostedCollateralMarketValue() {
		return this.postedCollateralMarketValue;
	}


	public void setPostedCollateralMarketValue(BigDecimal postedCollateralMarketValue) {
		this.postedCollateralMarketValue = postedCollateralMarketValue;
	}


	public Date getPostedDate() {
		return this.postedDate;
	}


	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
}
