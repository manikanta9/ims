package com.clifton.collateral.balance.rebuild.processor;


import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.trade.TradeService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>CollateralBalanceOTCRebuildProcessor</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralBalanceOTCRebuildProcessor<S extends CollateralStrategyPosition> extends BaseCollateralBalanceRebuildProcessor<S> {

	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private TradeService tradeService;

	/////////////////////////////////////////////////////////////////
	////////                Business Methods                /////////
	/////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionDaily> appendAccountingPositionDaily(InvestmentAccount holdingAccount, Date balanceDate, List<AccountingPositionDaily> positionDailyList) {
		if (InvestmentAccountType.OTC_ISDA.equals(holdingAccount.getType().getName())) {
			boolean isGroupedAccount = getInvestmentAccountGroupService().isInvestmentAccountInGroupOfType(holdingAccount.getId(),
					InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
			if (isGroupedAccount) {
				List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupAccountList(holdingAccount, true);
				InvestmentAccountGroupAccount groupAccount = CollectionUtils.getOnlyElement(groupAccountList);
				if (groupAccount != null) {
					List<AccountingPositionDaily> positionList = getGroupAccountAccountingPositionDailyList(groupAccount, balanceDate);
					updatePositionFxRate(positionList, groupAccount.getReferenceTwo().getBaseCurrency());
					positionDailyList.addAll(positionList);
				}
			}
			else {
				positionDailyList.addAll(getCLSAccountingPositionDailyList(holdingAccount, balanceDate));
			}
		}
		return positionDailyList;
	}


	List<InvestmentAccountGroupAccount> getInvestmentAccountGroupAccountList(InvestmentAccount holdingAccount, boolean primary) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setAccountId(holdingAccount.getId());
		searchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		searchForm.setPrimary(primary);
		return getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
	}


	/**
	 * For group accounts, we have to recursively crawl all the accounts in the group and get their individual holdings and their CLS account positions
	 * They are then added to the 'Primary' accounts position list for display in the UI, we exclude the primary accounts direct holdings and only add
	 * the positions from its respective CLS account or otherwise we'd duplicate those positions.
	 */
	private List<AccountingPositionDaily> getGroupAccountAccountingPositionDailyList(InvestmentAccountGroupAccount groupAccount, Date balanceDate) {
		List<AccountingPositionDaily> positionDailyList = new ArrayList<>();
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(getInvestmentAccountGroupService().getInvestmentAccountListByGroup(groupAccount.getReferenceOne().getName()))) {
			//Add positions from the CLS account
			positionDailyList.addAll(getCLSAccountingPositionDailyList(holdingAccount, balanceDate));
			//Now add non-cls positions (held directly in the account) - but not from the primary we don't want to pull those since they are already included
			if (!CompareUtils.isEqual(holdingAccount, groupAccount.getReferenceTwo())) {
				positionDailyList.addAll(getAccountingPositionDailyList(holdingAccount, holdingAccount.getIssuingCompany(), balanceDate));
			}
		}
		return positionDailyList;
	}


	private List<AccountingPositionDaily> getCLSAccountingPositionDailyList(InvestmentAccount holdingAccount, Date balanceDate) {
		InvestmentAccount clsAccount = getClsAccount(holdingAccount, balanceDate);
		return clsAccount != null ? getAccountingPositionDailyList(clsAccount, holdingAccount.getIssuingCompany(), balanceDate) : Collections.emptyList();
	}


	private List<AccountingPositionDaily> getAccountingPositionDailyList(InvestmentAccount investmentAccount, BusinessCompany issuingCompany, Date balanceDate) {
		List<AccountingPositionDaily> positionDailyList = new ArrayList<>();
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setSnapshotDate(balanceDate);
		searchForm.setHoldingAccountId(investmentAccount.getId());
		searchForm.setIncludeUnsettledLegPayments(true);
		for (AccountingPositionDaily positionDaily : CollectionUtils.getIterable(getAccountingPositionDailyService().getAccountingPositionDailyLiveList(searchForm))) {
			if (CompareUtils.isEqual(issuingCompany, positionDaily.getAccountingTransaction().getExecutingCompany())) {
				positionDailyList.add(positionDaily);
			}
		}
		return positionDailyList;
	}


	/////////////////////////////////////////////////////////////////
	////////                Rebuild Methods                 /////////
	/////////////////////////////////////////////////////////////////


	@Override
	public boolean processAccount(CollateralBalanceRebuildCommand rebuildCommand) {
		return rebuildCommand.getHoldingAccount().getBusinessContract() != null;
	}


	@Override
	protected void appendAccountingPositionList(CollateralBalanceRebuildCommand command) {
		AccountingPositionCommand positionCommand = AccountingPositionCommand
				.onDate(command.getCollateralType().isUseSettlementDate(), command.getBalanceDate())
				.forHoldingAccount(command.getHoldingAccountId());
		positionCommand.setIncludeUnsettledLegPayments(true);
		command.setPositionList(getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand));
		InvestmentAccount holdingAccount = command.getHoldingAccount();
		AssertUtils.assertNotNull(holdingAccount, "Holding account is expected.");
		if (InvestmentAccountType.OTC_ISDA.equals(holdingAccount.getType().getName())) {
			boolean isGroupedAccount = getInvestmentAccountGroupService().isInvestmentAccountInGroupOfType(holdingAccount.getId(),
					InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
			if (isGroupedAccount) {
				List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupAccountList(holdingAccount, true);
				InvestmentAccountGroupAccount groupAccount = CollectionUtils.getOnlyElement(groupAccountList);
				if (groupAccount != null) {
					command.getPositionList().addAll(getGroupAccountAccountingPositionList(groupAccount, command.getBalanceDate()));
				}
			}
			else {
				command.getPositionList().addAll(getCLSAccountingPositionList(holdingAccount, command.getBalanceDate()));
			}
		}
	}


	/**
	 * Override base class implementation to enable the inclusion of pending leg payments.
	 */
	@Override
	protected BigDecimal getBaseMarketValue(CollateralBalance balance, AccountingPosition position, FxRateLookupCache cache, boolean useSettlementDate, boolean includeAccrualLegPayments) {
		return super.getBaseMarketValue(balance, position, cache, useSettlementDate, true);
	}


	/**
	 * Intercept the {@link AccountingPositionDaily} and include the pending accrual payments with the accrual leg amounts.
	 */
	@Override
	public List<AccountingPositionDaily> calculateRequiredCollateralForAccountingPositionDailyList(List<AccountingPositionDaily> accountingPositionDailyList, Date date) {
		for (AccountingPositionDaily daily : CollectionUtils.getIterable(accountingPositionDailyList)) {
			// update OTE with pending accrual 1 and accrual 2 leg payments.
			daily.setOpenTradeEquityLocal(MathUtils.add(daily.getOpenTradeEquityLocal(), MathUtils.add(daily.getAccrual1PaymentLocal(), daily.getAccrual2PaymentLocal())));
			// update calculated accrual amounts based on changes to OTE
			daily.updateCalculations();
			// include equity leg payments
			daily.setOpenTradeEquityLocal(MathUtils.add(daily.getOpenTradeEquityLocal(), daily.getEquityLegPaymentLocal()));
			daily.setOpenTradeEquityBase(MathUtils.multiply(daily.getOpenTradeEquityLocal(), daily.getMarketFxRate(), 2));
		}
		return accountingPositionDailyList;
	}


	/**
	 * For group accounts, we have to recursively crawl all the accounts in the group and get their individual holdings and their CLS account positions
	 * They are then added to the 'Primary' accounts position list for display in the UI, we exclude the primary accounts direct holdings and only add
	 * the positions from its respective CLS account or otherwise we'd duplicate those positions.
	 */
	private List<AccountingPosition> getGroupAccountAccountingPositionList(InvestmentAccountGroupAccount groupAccount, Date balanceDate) {
		List<AccountingPosition> positionList = new ArrayList<>();
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(getInvestmentAccountGroupService().getInvestmentAccountListByGroup(groupAccount.getReferenceOne().getName()))) {
			//Add positions from the CLS account
			positionList.addAll(getCLSAccountingPositionList(holdingAccount, balanceDate));
		}
		return positionList;
	}


	@Override
	protected void appendCollateralPositionList(CollateralBalanceRebuildCommand command) {
		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(command.getBalanceDate()), 1);
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
				.onPositionSettlementDate(nextBusinessDay)
				.forHoldingAccount(command.getHoldingAccountId())
		);
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(nextBusinessDay);
		positionCommand.setHoldingInvestmentAccountId(command.getHoldingAccountId());
		command.setCollateralPositionList(getCollateralService().getCollateralPositionListUsingPositions(positionCommand, positionList));
	}


	@Override
	protected void appendCollateralEndingPositionList(CollateralBalanceRebuildCommand command) {
		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(command.getBalanceDate()), 2);
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
				.onPositionSettlementDate(nextBusinessDay)
				.forHoldingAccount(command.getHoldingAccountId())
		);
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(nextBusinessDay);
		positionCommand.setHoldingInvestmentAccountId(command.getHoldingAccountId());
		command.setEndingCollateralPositionList(getCollateralService().getCollateralPositionListUsingPositions(positionCommand, positionList));
	}


	@Override
	public void applyCollateralTypeOverrides(CollateralBalance balance) {
		BigDecimal roundingValue = null;
		if (balance.getHoldingInvestmentAccount().getBusinessContract() != null) {
			roundingValue = (BigDecimal) getBusinessContractCustomFieldValue(balance.getHoldingInvestmentAccount().getBusinessContract(), BusinessContract.CONTRACT_ROUNDING_CUSTOM_COLUMN);
		}
		setRoundedCollateralChangeValues(balance, roundingValue);
	}

	/////////////////////////////////////////////////////////////////
	////////                Context Methods                 /////////
	/////////////////////////////////////////////////////////////////


	protected void setRoundedCollateralChangeValues(CollateralBalance balance, BigDecimal roundingValue) {
		//Collateral defaults to ZERO; negative exposure greater than the threshold indicates clientRequiredCollateral, else, counterparty.
		BigDecimal clientCollateralChange = BigDecimal.ZERO;
		BigDecimal clientRequiredCollateral = BigDecimal.ZERO;
		BigDecimal counterpartyCollateralChange = BigDecimal.ZERO;
		BigDecimal counterpartyRequiredCollateral = BigDecimal.ZERO;

		BigDecimal totalRequirement = balance.getCalculatedTotalRequirement();
		BigDecimal netRequirement = balance.getCalculatedNetRequirement();
		BigDecimal deliveryReturnAmount = roundCollateralChangeAmount(netRequirement, roundingValue);

		// handle the case when we are returning all collateral from one side and posting to the other
		if (isCollateralBeingFullyReturnedAndPosted(balance, deliveryReturnAmount)) {
			BigDecimal minimumTransferAmount = balance.isInClientFavor() ? balance.getMinTransferCounterpartyAmount() : balance.getMinTransferAmount();
			// return all collateral but only post if the transfer amount is big enough
			boolean returnOnly = !MathUtils.isGreaterThanOrEqual(MathUtils.abs(deliveryReturnAmount), minimumTransferAmount);
			if (MathUtils.isNegative(deliveryReturnAmount)) {
				clientCollateralChange = returnOnly ? BigDecimal.ZERO : MathUtils.subtract(MathUtils.abs(deliveryReturnAmount), balance.getPostedCounterpartyCollateralHaircutValue());
				counterpartyCollateralChange = balance.getPostedCounterpartyCollateralHaircutValue().negate();
			}
			else {
				clientCollateralChange = balance.getPostedCollateralHaircutValue().negate();
				counterpartyCollateralChange = returnOnly ? BigDecimal.ZERO : MathUtils.subtract(deliveryReturnAmount, balance.getPostedCollateralHaircutValue());
			}
		}
		else if (!MathUtils.isNullOrZero(balance.getPositionsMarketValue())) {
			if (!balance.isInClientFavor()) {
				clientRequiredCollateral = totalRequirement;
			}
			else {
				counterpartyRequiredCollateral = totalRequirement;
			}

			clientCollateralChange = computeCollateralChange(clientRequiredCollateral, balance.getPostedCollateralHaircutValue(), balance.getMinTransferAmount(), roundingValue);
			counterpartyCollateralChange = computeCollateralChange(counterpartyRequiredCollateral, balance.getPostedCounterpartyCollateralHaircutValue(), balance.getMinTransferCounterpartyAmount(), roundingValue);
		}
		// this handles returning all collateral when there are not positions.  These values are not rounded, we simply return ALL collateral
		else if (!MathUtils.isNullOrZero(netRequirement) && !MathUtils.isLessThanOrEqual(netRequirement, balance.getCalculatedThresholdAmount())) {
			clientCollateralChange = computeCollateralChange(clientRequiredCollateral, balance.getPostedCollateralHaircutValue(), balance.getMinTransferAmount());
			counterpartyCollateralChange = computeCollateralChange(counterpartyRequiredCollateral, balance.getPostedCounterpartyCollateralHaircutValue(), balance.getMinTransferCounterpartyAmount());
		}

		balance.setCollateralChangeAmount(clientCollateralChange);
		balance.setCounterpartyCollateralChangeAmount(counterpartyCollateralChange);
	}


	/**
	 * Return true when all collateral is being returned from either the client or counterparty and more collateral is being posted to the other side.
	 */
	private boolean isCollateralBeingFullyReturnedAndPosted(CollateralBalance balance, BigDecimal deliveryReturnAmount) {
		return (balance.isInClientFavor()
				&& MathUtils.isGreaterThan(balance.getPostedCollateralHaircutValue(), BigDecimal.ZERO)
				&& MathUtils.isGreaterThan(MathUtils.abs(deliveryReturnAmount), balance.getPostedCollateralHaircutValue()))

				|| (!balance.isInClientFavor()
				&& MathUtils.isGreaterThan(balance.getPostedCounterpartyCollateralHaircutValue(), BigDecimal.ZERO)
				&& MathUtils.isGreaterThan(MathUtils.abs(deliveryReturnAmount), balance.getPostedCounterpartyCollateralHaircutValue()));
	}


	private BigDecimal computeCollateralChange(BigDecimal requiredCollateral, BigDecimal currentCollateral, BigDecimal minTransferAmount) {
		return computeCollateralChange(requiredCollateral, currentCollateral, minTransferAmount, BigDecimal.ZERO);
	}


	private BigDecimal computeCollateralChange(BigDecimal requiredCollateral, BigDecimal currentCollateral, BigDecimal minTransferAmount, BigDecimal roundingValue) {
		BigDecimal collateralChange = roundCollateralChangeAmount(MathUtils.subtract(MathUtils.abs(requiredCollateral), currentCollateral), roundingValue);
		//If the requiredCollateral is zero, then the minimum transfer amount does not apply.
		if (!MathUtils.isEqual(BigDecimal.ZERO, requiredCollateral)) {
			//If the collateral change is less than the minimum transfer amount, then there is no change.
			if (MathUtils.isLessThan(MathUtils.abs(collateralChange), minTransferAmount)) {
				collateralChange = BigDecimal.ZERO;
			}
		}
		return collateralChange;
	}


	private InvestmentAccount getClsAccount(InvestmentAccount holdingAccount, Date balanceDate) {
		InvestmentAccount clsAccount = null;
		InvestmentAccount clientAccount = CollectionUtils.getFirstElement(getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null, InvestmentAccountRelationshipPurpose.TRADING_FORWARDS_PURPOSE_NAME, balanceDate));
		if (clientAccount != null) {
			clsAccount = CollectionUtils.getFirstElement(BeanUtils.filter(getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(clientAccount.getId(), null, InvestmentAccountRelationshipPurpose.TRADING_FORWARDS_PURPOSE_NAME, balanceDate),
					account -> StringUtils.isEqual(InvestmentAccountType.OTC_CLS, account.getType().getName())));
		}
		return clsAccount;
	}


	private List<AccountingPosition> getCLSAccountingPositionList(InvestmentAccount holdingAccount, Date balanceDate) {
		InvestmentAccount clsAccount = getClsAccount(holdingAccount, balanceDate);
		if (clsAccount == null) {
			return Collections.emptyList();
		}
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand.onPositionSettlementDate(balanceDate).forHoldingAccount(clsAccount.getId()));
		return CollectionUtils.getStream(positionList).filter(position -> CompareUtils.isEqual(position.getExecutingCompany(), holdingAccount.getIssuingCompany())).collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
