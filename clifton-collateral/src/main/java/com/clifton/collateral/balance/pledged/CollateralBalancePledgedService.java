package com.clifton.collateral.balance.pledged;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedBalanceSearchForm;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedTransactionSearchForm;
import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public interface CollateralBalancePledgedService {

	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Command Methods   ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CollateralBalance.class)
	public void uploadCollateralBalancePledgedTransactionsFile(CollateralBalancePledgedTransactionCommand command);


	@SecureMethod(dtoClass = CollateralBalance.class)
	@RequestMapping("collateralBalancePledgedTransactionListByCommand")
	public List<CollateralBalancePledgedTransaction> getCollateralBalancePledgedTransactionListByCommand(CollateralBalancePledgedTransactionCommand command);


	@SecureMethod(dtoClass = CollateralBalance.class)
	@RequestMapping("collateralBalancePledgedTransactionListSaveByCommand")
	public void saveCollateralBalancePledgedTransactionListByCommand(int collateralBalanceId, BeanListCommand<CollateralBalancePledgedTransactionCommand> command);


	@SecureMethod(dtoClass = CollateralBalance.class)
	public Status clearCollateralBalancePledgedTransactionList(CollateralBalancePledgedTransactionCommand command);

	////////////////////////////////////////////////////////////////////////////
	////////     Collateral Balance Pledged Balance Command Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = CollateralBalance.class)
	public List<CollateralBalancePledgedBalance> getCollateralBalancePledgedBalanceList(CollateralBalancePledgedBalanceSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Position Transfer Business Methods    ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = AccountingPositionTransfer.class)
	public void createCollateralBalancePositionTransfers(CollateralBalancePledgedTransactionCommand command);

	////////////////////////////////////////////////////////////////////////////
	////////         Collateral Balance Pledged Helper Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	@RequestMapping("collateralPledgedMarketValue")
	@SecureMethod(dtoClass = CollateralBalance.class)
	public BigDecimal getPledgedCollateralMarketValue(int postedCollateralInvestmentSecurityId, BigDecimal postedCollateralQuantity, Date postedDate);


	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Business Methods  ////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalancePledgedTransaction getCollateralBalancePledgedTransaction(long id);


	public List<CollateralBalancePledgedTransaction> getCollateralBalancePledgedTransactionList(CollateralBalancePledgedTransactionSearchForm searchForm);


	@DoNotAddRequestMapping
	public CollateralBalancePledgedTransaction saveCollateralBalancePledgedTransaction(CollateralBalancePledgedTransaction bean);
}
