package com.clifton.collateral.balance.rebuild.processor.command;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;


/**
 * The <code>CollateralStrategyCommand</code> is used to provide an implementation framework
 * for getting collateral specific rebuild processor that are used for building collateral balances
 * and computing collateral.
 *
 * @author StevenF
 */
public interface CollateralStrategyCommand<T extends CollateralBalanceRebuildProcessor> {

	public T getCollateralBalanceRebuildProcessor();
}
