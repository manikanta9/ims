package com.clifton.collateral.balance.pledged;

import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedBalanceSearchForm;
import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedTransactionSearchForm;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author stevenf
 */
public class CollateralBalancePledgedServiceContext {

	private Date settlementDate;
	private CollateralBalance collateralBalance;
	private CollateralBalanceOptionsRebuildProcessor rebuildProcessor;

	//Stores a list of options traded that day - used to determine pledged collateral threshold.
	private List<Integer> optionTradeIdList = new ArrayList<>();
	private Set<Integer> unpledgedCollateralSet = new HashSet<>();
	private Map<Integer, CollateralBalancePledgedCollateralValue> pledgedCollateralValueMap = new HashMap<>();


	/**
	 * Iterates the pledged collateral map and returns the ids of all collateral securities that have been pledged
	 * to and positions.  These are used to search for maturity events for pledged collateral.
	 */
	public Integer[] getPledgedSecurityIds() {
		return getPledgedCollateralValueMap().keySet().toArray(new Integer[0]);
	}


	public InvestmentSecurityEventSearchForm getInvestmentSecurityEventSearchForm() {
		InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
		eventSearchForm.setTypeName(InvestmentSecurityEventType.SECURITY_MATURITY);
		eventSearchForm.setEventDate(getSettlementDate());
		eventSearchForm.setSecurityIds(getPledgedSecurityIds());
		return eventSearchForm;
	}


	//We only use this to find sells of bond/fund in the custodian (collateral) account for return collateral functionality.
	public TradeSearchForm getCollateralTradeSearchForm(Short[] tradeTypeIds) {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setBuy(false);
		tradeSearchForm.setClientInvestmentAccountId(getClientAccount().getId());
		tradeSearchForm.setHoldingInvestmentAccountId(getCollateralAccount().getId());
		tradeSearchForm.setTradeTypeIds(tradeTypeIds);
		tradeSearchForm.setWorkflowStateName(TradeService.TRADE_BOOKED_STATE_NAME);
		tradeSearchForm.setSettlementDate(getSettlementDate());
		return tradeSearchForm;
	}


	public TradeSearchForm getOptionsTradeSearchForm(Short[] tradeTypeIds) {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setClientInvestmentAccountId(getClientAccount().getId());
		tradeSearchForm.setHoldingInvestmentAccountId(getHoldingAccount().getId());
		tradeSearchForm.setTradeTypeIds(tradeTypeIds);
		tradeSearchForm.setWorkflowStateName(TradeService.TRADE_BOOKED_STATE_NAME);
		tradeSearchForm.setTradeDate(getBalanceDate());
		return tradeSearchForm;
	}


	public CollateralBalancePledgedBalanceSearchForm getCollateralBalancePledgedBalanceSearchForm() {
		CollateralBalancePledgedBalanceSearchForm searchForm = new CollateralBalancePledgedBalanceSearchForm();
		searchForm.setPostedDate(getPostedDate());
		searchForm.setHoldingInvestmentAccountId(getHoldingAccount().getId());
		return searchForm;
	}


	public CollateralBalancePledgedTransactionSearchForm getCollateralBalancePledgedTransactionSearchForm(Boolean transferNotBooked) {
		CollateralBalancePledgedTransactionSearchForm searchForm = new CollateralBalancePledgedTransactionSearchForm();
		searchForm.setPostedDate(getPostedDate());
		searchForm.setHoldingInvestmentAccountId(getHoldingAccount().getId());
		if (transferNotBooked != null) {
			searchForm.setTransferNotBooked(transferNotBooked);
		}
		return searchForm;
	}


	public AccountingBalanceSearchForm getAccountingBalanceSearchForm(boolean useCollateralAccount) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onSettlementDate(getSettlementDate());
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.EXCLUDE_RECEIVABLE_COLLATERAL);
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		searchForm.setClientInvestmentAccountId(getClientAccount().getId());
		searchForm.setHoldingInvestmentAccountId(useCollateralAccount ? getCollateralAccount().getId() : getHoldingAccount().getId());
		searchForm.setOrderBy("quantity:desc");
		return searchForm;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods             //////////////
	/////////////////////////////////////////////////////////////////////////////


	public CollateralBalance getCollateralBalance() {
		return this.collateralBalance;
	}


	public void setCollateralBalance(CollateralBalance collateralBalance) {
		this.collateralBalance = collateralBalance;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getBalanceDate() {
		return this.collateralBalance.getBalanceDate();
	}


	public Date getPostedDate() {
		return this.collateralBalance.getBalanceDate();
	}


	public Date getTransactionDate() {
		return this.collateralBalance.getBalanceDate();
	}


	public InvestmentAccount getClientAccount() {
		return this.collateralBalance.getClientInvestmentAccount();
	}


	public InvestmentAccount getHoldingAccount() {
		return this.collateralBalance.getHoldingInvestmentAccount();
	}


	public InvestmentAccount getCollateralAccount() {
		return this.collateralBalance.getCollateralInvestmentAccount();
	}


	public CollateralBalanceOptionsRebuildProcessor getRebuildProcessor() {
		return this.rebuildProcessor;
	}


	public void setRebuildProcessor(CollateralBalanceOptionsRebuildProcessor rebuildProcessor) {
		this.rebuildProcessor = rebuildProcessor;
	}


	public List<Integer> getOptionTradeIdList() {
		return this.optionTradeIdList;
	}


	public void setOptionTradeIdList(List<Integer> optionTradeIdList) {
		this.optionTradeIdList = optionTradeIdList;
	}


	public Set<Integer> getUnpledgedCollateralSet() {
		return this.unpledgedCollateralSet;
	}


	public void setUnpledgedCollateralSet(Set<Integer> unpledgedCollateralSet) {
		this.unpledgedCollateralSet = unpledgedCollateralSet;
	}


	@ValueIgnoringGetter
	public Map<Integer, CollateralBalancePledgedCollateralValue> getPledgedCollateralValueMap() {
		if (this.pledgedCollateralValueMap == null) {
			this.pledgedCollateralValueMap = new HashMap<>();
		}
		return this.pledgedCollateralValueMap;
	}


	public void setPledgedCollateralValueMap(Map<Integer, CollateralBalancePledgedCollateralValue> pledgedCollateralValueMap) {
		this.pledgedCollateralValueMap = pledgedCollateralValueMap;
	}
}
