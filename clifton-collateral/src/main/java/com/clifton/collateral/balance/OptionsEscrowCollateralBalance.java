package com.clifton.collateral.balance;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author terrys
 */
public class OptionsEscrowCollateralBalance extends CollateralBalance {

	/**
	 * Required collateral movement in order to bring the balance up to date for collateral requirements.
	 * Applies to options collateral only
	 */
	public BigDecimal getOptionsCollateralRequiredMovement() {
		return MathUtils.subtract(getPostedCollateralHaircutValue(), getCollateralRequirement());
	}


	/**
	 * Required collateral movement percentage in order to bring the balance up to date for collateral requirements.
	 * Applies to options collateral only - this is an odd case, because normally this would be expressing the
	 * percentage value of what the 'Required movement' represents (which is based on the collateral requirement)
	 * however, ops wanted the percentage to actually be based on the percentage of the postedCollateralHaircutValue,
	 * rather than the requirement for options collateral - we may update this to be the same for other types, but
	 * currently only applies to options.
	 */
	public BigDecimal getOptionsCollateralRequiredMovementPercentage() {
		//Calculate the percentage if required collateral is not 0
		if (!MathUtils.isNullOrZero(getPostedCollateralHaircutValue())) {
			return CoreMathUtils.getPercentValue(getOptionsCollateralRequiredMovement(), getPostedCollateralHaircutValue(), true);
		}
		//If posted collateral was null or 0, but there is still a requirement movement, that means we need to return all
		if (!MathUtils.isNullOrZero(getOptionsCollateralRequiredMovement())) {
			return new BigDecimal("-100");
		}
		//Otherwise no movement.
		return BigDecimal.ZERO;
	}
}
