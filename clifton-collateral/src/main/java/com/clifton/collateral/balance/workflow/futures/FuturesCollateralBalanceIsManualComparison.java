package com.clifton.collateral.balance.workflow.futures;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceFuturesRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * @author jonathanr
 */
public class FuturesCollateralBalanceIsManualComparison implements Comparison<CollateralBalance> {

	private CollateralBalanceRebuildService collateralBalanceRebuildService;


	@Override
	public boolean evaluate(CollateralBalance bean, ComparisonContext context) {
		CollateralBalanceRebuildProcessor<?> processor = getCollateralBalanceRebuildService().getCollateralBalanceRebuildProcessor(bean.getCollateralType(), bean.getHoldingInvestmentAccount(), bean.getCollateralInvestmentAccount());
		ValidationUtils.assertTrue(processor instanceof CollateralBalanceFuturesRebuildProcessor, "Comparison requires Futures Collateral Balance.");
		CollateralBalanceFuturesRebuildProcessor<?> futuresRebuildProcessor = (CollateralBalanceFuturesRebuildProcessor<?>) processor;
		if (futuresRebuildProcessor.isManualAccount()) {
			context.recordTrueMessage("Configured Is manual: true");
		}
		else {
			context.recordFalseMessage("Not a manual Collateral Balance");
		}
		return futuresRebuildProcessor.isManualAccount();
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}
}
