package com.clifton.collateral.balance.rebuild.processor.matcher;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author terrys
 */
public interface CollateralStrategyMatcher<S extends CollateralStrategyPosition> {

	public Map<CollateralOptionsStrategyCalculators, List<S>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate);
}
