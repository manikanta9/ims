package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * Long Put/Call Equity or Index – more than 9 months until expiration
 * 75% of the total cost of the puts(s)
 * .75 * 1508.00 = $1,131.00
 * Initial Margin
 * Margin requirement:  $1,131.00
 * Margin call (SMA debit): $1,131.00
 * <p>
 * Long Call/Put Equity or Index – 9 months or less until expiration
 * Option Proceeds
 * Initial Margin
 * Margin requirement:  $1,508.00
 *
 * @author terrys
 */
public class CollateralOptionsStrategyLongCalculator implements CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> {

	private static final BigDecimal MarginPercentage = new BigDecimal(".75");


	@Override
	public Map<Long, BigDecimal> calculateCollateral(CollateralOptionsStrategyCommand command) {
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();
		for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(command.getPositionList())) {
			collateralRequirementMap.put(strategyPosition.getAccountingTransactionId(), calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition));
		}
		return collateralRequirementMap;
	}


	@Override
	public BigDecimal calculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition) {
		return doCalculateCollateral(processor, strategyPosition, s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s)));
	}


	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal requiredCollateral = doCalculateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(requiredCollateral);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal doCalculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		BigDecimal results = BigDecimal.ZERO;

		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getCallRequirementType())) {
			LocalDate expiry = DateUtils.asLocalDate(strategyPosition.getInvestmentSecurity().getEndDate());
			if (expiry.isAfter(LocalDate.now().plus(9, ChronoUnit.MONTHS))) {
				// more than 9 months
				results = MathUtils.multiply(strategyPosition.getOptionProceeds(), MarginPercentage);
				appender.accept(() -> String.format("Long [%s] - more than 9 months until expiration", strategyPosition.getOptionType()));
				appender.accept(() -> String.format("[%s] of option proceeds [%s]", MarginPercentage, strategyPosition.getOptionProceeds()));
			}
			else {
				// 9 months or less
				results = strategyPosition.getOptionProceeds();

				appender.accept(() -> String.format("Long [%s] -  9 months or less until expiration", strategyPosition.getOptionType()));
				appender.accept(() -> String.format("Option proceeds [%s]", strategyPosition.getOptionProceeds()));
			}
			String marginRequirement = CoreMathUtils.formatNumber(results, null);
			appender.accept(() -> "Positions");
			appender.accept(strategyPosition::buildPositionExplanation);
			appender.accept(() -> String.format("Initial Margin Requirement [%s]", marginRequirement));
		}

		return InvestmentCalculatorUtils.roundLocalAmount(results, strategyPosition.getInvestmentSecurity());
	}
}
