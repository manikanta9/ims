package com.clifton.collateral.balance.workflow.escrow;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransaction;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransactionCommand;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;
import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>CollateralBalancePledgedTransactionsWorkflowAction</code> class is a workflow transition action
 * that creates pledged transactions for options escrow collateral balances.
 * <p/>
 * Note: the action only create the pledged collateral transactions, if pledged transactions were created manually (not via workflow)
 * then this action will not do anything as there will be no additional transactions to create.
 *
 * @author stevenf
 */
public class CollateralBalancePledgedTransactionsEscrowWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
		command.setAutomatic(true);
		command.setGeneratePledgedCollateral(true);
		command.setCollateralBalanceId(collateralBalance.getId());
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedService().getCollateralBalancePledgedTransactionListByCommand(command);
		if (CollectionUtils.getSize(pledgedTransactionList) > 0) {
			//Convert to beanListCommand and save.
			getCollateralBalancePledgedService().saveCollateralBalancePledgedTransactionListByCommand(collateralBalance.getId(), getSavePledgedTransactionCommandList(collateralBalance, pledgedTransactionList));
		}
	}


	public BeanListCommand<CollateralBalancePledgedTransactionCommand> getSavePledgedTransactionCommandList(CollateralBalance collateralBalance, List<CollateralBalancePledgedTransaction> pledgedTransactionList) {
		//First validate that collateral was assigned to each position properly - otherwise force manual review.
		List<CollateralBalancePledgedTransaction> failedPledgedTransactionList = pledgedTransactionList.stream().filter(transaction -> transaction.getPostedCollateralInvestmentSecurity() == null).collect(Collectors.toList());
		ValidationUtils.assertTrue(CollectionUtils.getSize(failedPledgedTransactionList) == 0, "Collateral balance (" + collateralBalance.getClientInvestmentAccount().getLabel() + " [" + collateralBalance.getHoldingInvestmentAccount().getNumber() + "]) had positions that could not be assigned collateral automatically.<br />You must review and manually pledge collateral.");

		List<CollateralBalancePledgedTransactionCommand> pledgedTransactionCommandList = new ArrayList<>();
		for (CollateralBalancePledgedTransaction pledgedTransaction : CollectionUtils.getIterable(pledgedTransactionList)) {
			CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
			command.setPostedDate(pledgedTransaction.getPostedDate());
			command.setHoldingInvestmentAccountId(pledgedTransaction.getHoldingInvestmentAccount().getId());
			command.setPositionInvestmentSecurityId(pledgedTransaction.getPositionInvestmentSecurity().getId());
			command.setPostedCollateralInvestmentSecurityId(pledgedTransaction.getPostedCollateralInvestmentSecurity().getId());
			command.setPostedCollateralQuantity(pledgedTransaction.getPostedCollateralQuantity());
			pledgedTransactionCommandList.add(command);
		}
		BeanListCommand<CollateralBalancePledgedTransactionCommand> savePledgedTransactionsCommand = new BeanListCommand<>();
		savePledgedTransactionsCommand.setBeanList(pledgedTransactionCommandList);
		return savePledgedTransactionsCommand;
	}
}
