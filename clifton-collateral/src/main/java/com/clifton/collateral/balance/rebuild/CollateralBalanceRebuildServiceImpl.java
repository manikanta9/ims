package com.clifton.collateral.balance.rebuild;


import com.clifton.business.company.BusinessCompanyService;
import com.clifton.collateral.CollateralConfiguration;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.collateral.search.CollateralTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * The <code>CollateralBalanceRebuildServiceImpl</code> class provides basic implementation of CollateralBalanceProcessService interface.
 *
 * @author manderson
 */
@Service
public class CollateralBalanceRebuildServiceImpl implements CollateralBalanceRebuildService {

	private BusinessCompanyService businessCompanyService;
	private CollateralBalanceService collateralBalanceService;
	private CollateralService collateralService;
	private InvestmentAccountService investmentAccountService;
	private SystemBeanService systemBeanService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private SynchronizationHandler synchronizationHandler;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status rebuildCollateralBalance(final CollateralBalanceRebuildCommand command) {
		populateCommand(command);

		// If explicitly set; run synchronously, else, run asynchronously
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doRebuildCollateralBalance(command, status, true);
			if (command.isThrowExceptionOnError() && status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}

		String runId = command.getRunId();
		final Date runDate = DateUtils.addSeconds(new Date(), command.getAsynchronousRebuildDelay());
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(runDate));
		Runner runner = new AbstractStatusAwareRunner("COLLATERAL-BALANCE", runId, runDate, statusHolder) {

			@Override
			public void run() {
				try {
					ValidationUtils.assertNotNull(command.getBalanceDate(), "Balance Date is required to rebuild Collateral Balances.");
					doRebuildCollateralBalance(command, statusHolder.getStatus(), false);
				}
				catch (Exception e) {
					getStatus().setMessage("Error rebuilding snapshots for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding collateral for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
		return Status.ofMessage("Started rebuilding collateral balances for requested holding account(s) on " + DateUtils.fromDateShort(command.getBalanceDate()) + ". Processing will be completed shortly.");
	}


	@Override
	public BigDecimal getCollateralTransferAmountRounded(String collateralTypeName, int holdingAccountId, BigDecimal amount, Date balanceDate) {
		ValidationUtils.assertNotNull(collateralTypeName, "Collateral Type is required to calculate amount rounded.");
		CollateralType collateralType = getCollateralService().getCollateralTypeByName(collateralTypeName);
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(holdingAccountId);
		InvestmentAccount collateralAccount = getCollateralAccount(collateralType, holdingAccount, balanceDate);
		CollateralBalanceRebuildProcessor<?> processor = getCollateralBalanceRebuildProcessor(collateralType, holdingAccount, collateralAccount);
		return processor.getCollateralTransferAmountRounded(holdingAccountId, amount);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Status doRebuildCollateralBalance(CollateralBalanceRebuildCommand command, Status status, boolean synchronous) {
		ValidationUtils.assertNotNull(command.getBalanceDate(), "Balance Date is required to rebuild Collateral Balances.");

		// If only running for a specific collateral type
		if (command.getCollateralType() != null) {
			rebuildCollateralBalanceByCommand(command, status, synchronous);
		}
		else {
			// Otherwise - Run For All
			List<CollateralType> typeList = getCollateralService().getCollateralTypeList(new CollateralTypeSearchForm());
			StringBuilder fullMessage = new StringBuilder();
			for (CollateralType type : CollectionUtils.getIterable(typeList)) {
				command.setCollateralType(type);
				rebuildCollateralBalanceByCommand(command, status, synchronous);
				if (fullMessage.length() > 0) {
					fullMessage.append("\n");
				}
				fullMessage.append(status.getMessage());
			}
			status.setMessage(fullMessage.toString());
		}
		return status;
	}


	private Status rebuildCollateralBalanceByCommand(CollateralBalanceRebuildCommand command, Status status, boolean synchronous) {
		// Get all holding accounts for the given rebuild command, then get any existing balances for them
		List<InvestmentAccount> holdingAccountRebuildList = getHoldingAccountRebuildList(command);
		List<CollateralBalance> existingCollateralBalanceList = getCollateralBalanceService().getCollateralBalanceList(command.getCollateralBalanceSearchForm());

		RebuildCounts rebuildCounts = new RebuildCounts(CollectionUtils.getSize(holdingAccountRebuildList));
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(holdingAccountRebuildList)) {
			try {
				getSynchronizationHandler().execute(SynchronizableBuilder.forLocking("COLLATERAL-REBUILD", getLockMessage(command), command.getRunId())
						.withMillisecondsToWaitIfBusy(synchronous ? 0 : 2000)
						.buildWithExecutableAction(() ->
								rebuildCollateralBalanceByHoldingAccount(command, holdingAccount, existingCollateralBalanceList, rebuildCounts, status)
						));
			}
			catch (Exception e) {
				rebuildCounts.incrementFailedCount();
				status.addError("[" + holdingAccount.getNumber() + "] on [" + DateUtils.fromDateShort(command.getBalanceDate()) + "]: " + ExceptionUtils.getOriginalMessage(e));
				LogUtils.errorOrInfo(getClass(), "Error obtaining lock to rebuild collateral balance for account [" + holdingAccount.getNumber() + "] on [" + DateUtils.fromDateShort(command.getBalanceDate()) + "]: "
						+ ExceptionUtils.getOriginalMessage(e), e);
			}
		}
		// update status outside of the loop, all exceptions are caught.
		status.setMessage(command.getCollateralType().getName() + " on " + DateUtils.fromDateShort(command.getBalanceDate()) + ":  " + rebuildCounts + "; ");

		return status;
	}


	private String getLockMessage(CollateralBalanceRebuildCommand command) {
		return command.getCollateralType().getName() + " rebuild on " + DateUtils.fromDateShort(command.getBalanceDate()) +
				(command.getHoldingAccount() != null ? " for " + command.getHoldingAccount().getLabel() : "");
	}


	private void rebuildCollateralBalanceByHoldingAccount(CollateralBalanceRebuildCommand command, InvestmentAccount holdingAccount, List<CollateralBalance> existingCollateralBalanceList, RebuildCounts rebuildCounts, Status status) {
		try {
			//Create new command and set holding account id on it.
			CollateralBalanceRebuildCommand holdingAccountRebuildCommand = new CollateralBalanceRebuildCommand();
			BeanUtils.copyProperties(command, holdingAccountRebuildCommand);
			holdingAccountRebuildCommand.setHoldingAccount(holdingAccount);
			//Filter just existing collateral balance(s) for this holding account
			List<CollateralBalance> existingCollateralBalanceListForAccount = BeanUtils.filter(existingCollateralBalanceList, CollateralBalance::getHoldingInvestmentAccount, holdingAccount);
			//Get the collateral account related to this holding account if applicable by collateralType and date.
			InvestmentAccount collateralAccount = getCollateralAccount(command.getCollateralType(), holdingAccount, holdingAccountRebuildCommand.getBalanceDate());

			boolean processed = false;
			CollateralBalanceRebuildProcessor<?> processor = getCollateralBalanceRebuildProcessor(command.getCollateralType(), holdingAccount, collateralAccount);
			if (processor.processAccount(holdingAccountRebuildCommand)) {
				processed = processor.rebuildCollateralBalance(holdingAccountRebuildCommand, existingCollateralBalanceListForAccount);
			}
			if (processed) {
				rebuildCounts.incrementSuccessCount();
			}
			else {
				rebuildCounts.incrementSkippedCount();
			}
		}
		catch (Exception e) {
			rebuildCounts.incrementFailedCount();
			status.addError("[" + holdingAccount.getNumber() + "] on [" + DateUtils.fromDateShort(command.getBalanceDate()) + "]: " + ExceptionUtils.getOriginalMessage(e));
			LogUtils.errorOrInfo(getClass(), "Error rebuilding collateral balance for account [" + holdingAccount.getNumber() + "] on [" + DateUtils.fromDateShort(command.getBalanceDate()) + "]: "
					+ ExceptionUtils.getOriginalMessage(e), e);
		}
	}


	private List<InvestmentAccount> getHoldingAccountRebuildList(final CollateralBalanceRebuildCommand rebuildCommand) {
		CollateralType collateralType = rebuildCommand.getCollateralType();
		AssertUtils.assertNotNull(collateralType, "Collateral Type is required to determine the proper investment account group for processing.");

		Map<InvestmentAccount, Integer> collateralAccountGroupMap = getRelatedCollateralAccountMap();
		Set<InvestmentAccount> holdingAccountRebuildList = new HashSet<>();
		InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
		investmentAccountSearchForm.setInvestmentAccountGroupId(collateralType.getAccountGroup().getId());
		List<InvestmentAccount> collateralTypeHoldingAccountList = getInvestmentAccountService().getInvestmentAccountList(investmentAccountSearchForm);
		for (InvestmentAccount collateralTypeHoldingAccount : CollectionUtils.getIterable(collateralTypeHoldingAccountList)) {
			if (processHoldingAccount(collateralTypeHoldingAccount, rebuildCommand)) {
				holdingAccountRebuildList.add(collateralTypeHoldingAccount);
				// always rebuild all related accounts
				addRelatedCollateralAccounts(collateralTypeHoldingAccount, holdingAccountRebuildList, collateralAccountGroupMap);
			}
		}
		AssertUtils.assertNotEmpty(holdingAccountRebuildList, collateralType.getName() + ": No active holding accounts defined for selected criteria to rebuild");
		return new ArrayList<>(holdingAccountRebuildList);
	}


	/**
	 * Provides a map of holding account to group id which will be used to determine additional account that need to be built.
	 */
	private Map<InvestmentAccount, Integer> getRelatedCollateralAccountMap() {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		List<InvestmentAccountGroupAccount> accountGroupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
		Map<InvestmentAccount, Integer> accountMap = new HashMap<>();
		for (InvestmentAccountGroupAccount accountGroupAccount : CollectionUtils.getIterable(accountGroupAccountList)) {
			accountMap.put(accountGroupAccount.getReferenceTwo(), accountGroupAccount.getReferenceOne().getId());
		}
		return accountMap;
	}


	private void addRelatedCollateralAccounts(InvestmentAccount holdingAccount, Set<InvestmentAccount> holdingAccountRebuildList, Map<InvestmentAccount, Integer> collateralAccountGroupMap) {
		if (collateralAccountGroupMap.containsKey(holdingAccount)) {
			Integer accountGroupId = collateralAccountGroupMap.get(holdingAccount);
			collateralAccountGroupMap.entrySet().stream().filter(entry -> accountGroupId.equals(entry.getValue())).forEach(entry -> holdingAccountRebuildList.add(entry.getKey()));
		}
	}


	private boolean processHoldingAccount(InvestmentAccount holdingAccount, CollateralBalanceRebuildCommand rebuildCommand) {
		boolean process = true;
		if (rebuildCommand.getHoldingAccount() != null) {
			process = rebuildCommand.getHoldingAccountId().intValue() == holdingAccount.getId().intValue();
		}
		if (process && rebuildCommand.getIssuingCompanyId() != null) {
			process = rebuildCommand.getIssuingCompanyId().intValue() == holdingAccount.getIssuingCompany().getId().intValue();
		}
		if (process && rebuildCommand.getBusinessClientId() != null) {
			process = rebuildCommand.getBusinessClientId().intValue() == holdingAccount.getBusinessClient().getId().intValue();
		}
		if (process && rebuildCommand.getHoldingAccountTypeId() != null) {
			process = rebuildCommand.getHoldingAccountTypeId().intValue() == holdingAccount.getType().getId();
		}
		if (process && rebuildCommand.getExcludeHoldingAccountTypeIds() != null) {
			process = !ArrayUtils.contains(rebuildCommand.getExcludeHoldingAccountTypeIds(), holdingAccount.getType().getId());
		}
		return process;
	}


	@Override
	public InvestmentAccount getCollateralAccount(CollateralType collateralType, InvestmentAccount holdingAccount, Date balanceDate) {
		InvestmentAccount collateralAccount;
		final String collateralTypeName = collateralType.getName();
		switch (collateralTypeName) {
			case CollateralType.COLLATERAL_TYPE_FUTURES:
			case CollateralType.COLLATERAL_TYPE_CLEARED_OTC:
				collateralAccount = getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(holdingAccount, balanceDate, false, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME);
				break;
			case CollateralType.COLLATERAL_TYPE_OPTIONS_ESCROW:
			case CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN:
				collateralAccount = getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(holdingAccount, balanceDate, false, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.TRADING_OPTIONS_PURPOSE_NAME);
				break;
			default:
				collateralAccount = null;
		}
		return collateralAccount;
	}


	@Override
	public CollateralBalanceRebuildProcessor<?> getCollateralBalanceRebuildProcessor(CollateralType collateralType, InvestmentAccount holdingAccount, InvestmentAccount collateralAccount) {
		CollateralConfiguration collateralTypeConfiguration = getCollateralService().getCollateralConfigurationByType(collateralType);
		ValidationUtils.assertNotNull(collateralTypeConfiguration, "Collateral type configuration cannot be null!");
		ValidationUtils.assertNotNull(collateralTypeConfiguration.getRebuildProcessorBean(), "Collateral type configuration [" + collateralTypeConfiguration.getId() + "] must specify a rebuild processor!");
		CollateralConfiguration holdingCompanyConfiguration = getCollateralService().getCollateralConfigurationByTypeAndHoldingCompany(collateralType, holdingAccount.getIssuingCompany());
		CollateralConfiguration holdingAccountConfiguration = getCollateralService().getCollateralConfigurationByTypeAndHoldingAccount(collateralType, holdingAccount);
		//If collateral account exists then load and configuration for it.
		CollateralConfiguration collateralCompanyConfiguration = null;
		CollateralConfiguration collateralAccountConfiguration = null;
		if (collateralAccount != null) {
			collateralCompanyConfiguration = getCollateralService().getCollateralConfigurationByTypeAndCollateralCompany(collateralType, collateralAccount.getIssuingCompany());
			collateralAccountConfiguration = getCollateralService().getCollateralConfigurationByTypeAndCollateralAccount(collateralType, collateralAccount);
		}
		//Now pass all ids and get an instance of the most specific bean with the merged configuration
		CollateralBalanceRebuildProcessor<?> processor = (CollateralBalanceRebuildProcessor<?>) getSystemBeanService().getBeanInstanceWithMergedConfiguration(
				collateralTypeConfiguration.getRebuildProcessorBean().getId(),
				holdingCompanyConfiguration != null ? holdingCompanyConfiguration.getRebuildProcessorBean().getId() : null,
				holdingAccountConfiguration != null ? holdingAccountConfiguration.getRebuildProcessorBean().getId() : null,
				collateralCompanyConfiguration != null ? collateralCompanyConfiguration.getRebuildProcessorBean().getId() : null,
				collateralAccountConfiguration != null ? collateralAccountConfiguration.getRebuildProcessorBean().getId() : null
		);
		processor.setCollateralType(collateralType);
		return processor;
	}


	private void populateCommand(CollateralBalanceRebuildCommand command) {
		if (command.getCollateralTypeId() != null) {
			command.setCollateralType(getCollateralService().getCollateralType(command.getCollateralTypeId()));
		}
		else if (command.getCollateralTypeName() != null) {
			command.setCollateralType(getCollateralService().getCollateralTypeByName(command.getCollateralTypeName()));
		}
		if (command.getHoldingAccountId() != null) {
			command.setHoldingAccount(getInvestmentAccountService().getInvestmentAccount(command.getHoldingAccountId()));
		}
		if (command.getIssuingCompanyId() != null) {
			command.setIssuingCompany(getBusinessCompanyService().getBusinessCompany(command.getIssuingCompanyId()));
		}
		if (command.getStatus() == null) {
			command.setStatus(Status.ofMessage("Starting Collateral Rebuild"));
		}
	}


	private static class RebuildCounts {

		private final int totalCount;

		private int failedCount;
		private int skippedCount;
		private int successCount;


		RebuildCounts(int totalCount) {
			this.totalCount = totalCount;

			this.failedCount = 0;
			this.skippedCount = 0;
			this.successCount = 0;
		}


		@Override
		public String toString() {
			return "Total = " + this.totalCount + "; Success = " + this.successCount + "; Skipped = " + this.skippedCount + "; Failed = " + this.failedCount;
		}


		public void incrementFailedCount() {
			this.failedCount++;
		}


		public void incrementSkippedCount() {
			this.skippedCount++;
		}


		public void incrementSuccessCount() {
			this.successCount++;
		}


		public int getTotalCount() {
			return this.totalCount;
		}


		public int getFailedCount() {
			return this.failedCount;
		}


		public int getSkippedCount() {
			return this.skippedCount;
		}


		public int getSuccessCount() {
			return this.successCount;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}
}
