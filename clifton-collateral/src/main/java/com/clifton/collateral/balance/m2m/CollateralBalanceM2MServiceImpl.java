package com.clifton.collateral.balance.m2m;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchFormConfigurer;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.util.date.DateUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CollateralBalanceM2MServiceImpl implements CollateralBalanceM2MService {

	private AdvancedReadOnlyDAO<AccountingM2MDailyExtended, Criteria> accountingM2MDailyExtendedDAO;

	private AccountingM2MService accountingM2MService;
	private CalendarBusinessDayService calendarBusinessDayService;


	@Override
	public List<AccountingM2MDailyExtended> getCollateralBalanceM2MDailyExtendedList(AccountingM2MDailyExtendedSearchForm searchForm) {
		if (searchForm.getAccountingM2MDailyId() != null) {
			AccountingM2MDaily daily = getAccountingM2MService().getAccountingM2MDaily(searchForm.getAccountingM2MDailyId());
			searchForm.setHoldingInvestmentAccountId(daily.getHoldingInvestmentAccount().getId());
			searchForm.setEndMarkDate(DateUtils.addDays(daily.getMarkDate(), -1));
			searchForm.setStartMarkDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(searchForm.getEndMarkDate()), -4));
		}
		return getAccountingM2MDailyExtendedDAO().findBySearchCriteria(new AccountingM2MDailySearchFormConfigurer(searchForm));
	}


	public AdvancedReadOnlyDAO<AccountingM2MDailyExtended, Criteria> getAccountingM2MDailyExtendedDAO() {
		return this.accountingM2MDailyExtendedDAO;
	}


	public void setAccountingM2MDailyExtendedDAO(AdvancedReadOnlyDAO<AccountingM2MDailyExtended, Criteria> accountingM2MDailyExtendedDAO) {
		this.accountingM2MDailyExtendedDAO = accountingM2MDailyExtendedDAO;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
