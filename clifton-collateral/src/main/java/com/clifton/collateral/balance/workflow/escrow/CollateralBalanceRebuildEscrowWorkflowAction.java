package com.clifton.collateral.balance.workflow.escrow;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;


/**
 * The <code>CollateralBalanceRebuildEscrowWorkflowAction</code> class is a workflow transition action that rebuilds the collateral balance.
 * <p/>
 *
 * @author stevenf
 */
public class CollateralBalanceRebuildEscrowWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	private CollateralBalanceRebuildService collateralBalanceRebuildService;


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		CollateralBalanceRebuildCommand rebuildCommand = CollateralBalanceRebuildCommand.ofSynchronousWithThrow();
		rebuildCommand.setBalanceDate(collateralBalance.getBalanceDate());
		rebuildCommand.setCollateralType(collateralBalance.getCollateralType());
		rebuildCommand.setHoldingAccount(collateralBalance.getHoldingInvestmentAccount());
		rebuildCommand.setSkipExisting(false);
		getCollateralBalanceRebuildService().rebuildCollateralBalance(rebuildCommand);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}
}
