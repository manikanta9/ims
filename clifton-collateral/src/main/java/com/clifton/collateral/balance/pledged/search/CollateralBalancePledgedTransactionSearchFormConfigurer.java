package com.clifton.collateral.balance.pledged.search;


import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransaction;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;


/**
 * The <code>CollataralBalanceSearchFormConfigurer</code> class configures search criteria for the CollateralBalancePledgedTransactionSearchForm search form.
 * <p>
 * This allows the search form to not return results when the pledged transaction already belongs to a transfer that has been booked.
 * This prevents users from attempting to book multiple transfers for pledged transactions.
 *
 * @author vgomelsky
 */
public class CollateralBalancePledgedTransactionSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private CollateralBalancePledgedTransactionSearchForm collateralBalancePledgedTransactionSearchForm;


	public CollateralBalancePledgedTransactionSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);
		this.collateralBalancePledgedTransactionSearchForm = (CollateralBalancePledgedTransactionSearchForm) searchForm;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);
		CollateralBalancePledgedTransactionSearchForm searchForm = this.collateralBalancePledgedTransactionSearchForm;
		if (searchForm.getTransferNotBooked() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(CollateralBalancePledgedTransaction.class, "link");
			sub.createAlias("positionTransfer", "transfer", JoinType.LEFT_OUTER_JOIN);
			sub.setProjection(Projections.property("transfer.id"));
			if (BooleanUtils.isTrue(searchForm.getTransferNotBooked())) {
				sub.add(
						Restrictions.or(
								Restrictions.isNull("positionTransfer"),
								Restrictions.isNull("transfer.bookingDate")
						)
				);
			}
			else {
				sub.add(
						Restrictions.and(
								Restrictions.isNotNull("positionTransfer"),
								Restrictions.isNotNull("transfer.bookingDate")
						)
				);
			}
			sub.add(Restrictions.eqProperty("id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(sub));
		}
	}
}
