package com.clifton.collateral.balance.simplejournal;

import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Holds data necessary for properly creating cash collateral simple journal entries
 * <p>
 *
 * @author stevenf on 8/10/2016.
 */
@NonPersistentObject
public class CollateralBalanceSimpleJournal extends AccountingSimpleJournal {

	//Specifies if the trade used for this entry was a buy or sell.
	private boolean buy;

	private List<Integer> tradeIdList;

	private InvestmentAccount custodianAccount;
	private InvestmentAccount holdingInvestmentAccount;

	private BigDecimal cashCollateralBalance;
	private BigDecimal maturityProceeds;
	private BigDecimal netAmount;

	////////////////////////////////////////////////////////////////////////////////


	public AccountingSimpleJournal toAccountingSimpleJournal() {
		AccountingSimpleJournal simpleJournal = new AccountingSimpleJournal();
		BeanUtils.copyProperties(this, simpleJournal);
		return simpleJournal;
	}
	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public List<Integer> getTradeIdList() {
		if (this.tradeIdList == null) {
			this.tradeIdList = new ArrayList<>();
		}
		return this.tradeIdList;
	}


	public void setTradeIdList(List<Integer> tradeIdList) {
		this.tradeIdList = tradeIdList;
	}


	public InvestmentAccount getCustodianAccount() {
		return this.custodianAccount;
	}


	public void setCustodianAccount(InvestmentAccount custodianAccount) {
		this.custodianAccount = custodianAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public BigDecimal getCashCollateralBalance() {
		return this.cashCollateralBalance;
	}


	public void setCashCollateralBalance(BigDecimal cashCollateralBalance) {
		this.cashCollateralBalance = cashCollateralBalance;
	}


	public BigDecimal getMaturityProceeds() {
		return this.maturityProceeds;
	}


	public void setMaturityProceeds(BigDecimal maturityProceeds) {
		this.maturityProceeds = maturityProceeds;
	}


	public BigDecimal getNetAmount() {
		return this.netAmount;
	}


	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
}
