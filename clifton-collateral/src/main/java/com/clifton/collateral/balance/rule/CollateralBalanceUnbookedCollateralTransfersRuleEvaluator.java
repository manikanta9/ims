package com.clifton.collateral.balance.rule;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * This <code>CollateralBalanceUnbookedCollateralTransfersRuleEvaluator</code> is a rule evaluator that generates a rule violation if there exists
 * a unbooked collateral transfer relating to the collateral balance holding account.
 *
 * @author jonathanr
 */
public class CollateralBalanceUnbookedCollateralTransfersRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {


	private AccountingPositionTransferService accountingPositionTransferService;
	private CollateralBalanceService collateralBalanceService;


	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance collateralBalance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(collateralBalance.getHoldingInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			CollateralBalanceSearchForm collateralBalanceSearchForm = new CollateralBalanceSearchForm();
			collateralBalanceSearchForm.setHoldingInvestmentAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
			collateralBalanceSearchForm.addSearchRestriction("balanceDate", ComparisonConditions.GREATER_THAN, DateUtils.addMonths(collateralBalance.getBalanceDate(), -1));
			collateralBalanceSearchForm.addSearchRestriction("balanceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, collateralBalance.getBalanceDate());
			List<CollateralBalance> balances = getCollateralBalanceService().getCollateralBalanceList(collateralBalanceSearchForm);
			AccountingPositionTransferSearchForm accountingPositionTransferSearchForm = new AccountingPositionTransferSearchForm();
			accountingPositionTransferSearchForm.setJournalNotBooked(true);
			accountingPositionTransferSearchForm.setSourceSystemTableNameEquals("CollateralBalance");
			accountingPositionTransferSearchForm.setSourceFkFieldIdList(CollectionUtils.getStream(balances).mapToInt(BaseSimpleEntity::getId).boxed().toArray(Integer[]::new));
			List<AccountingPositionTransfer> accountingPositionTransfersList = getAccountingPositionTransferService().getAccountingPositionTransferList(accountingPositionTransferSearchForm);

			for (AccountingPositionTransfer accountingPositionTransfer : CollectionUtils.getIterable(accountingPositionTransfersList)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, collateralBalance.getId(), accountingPositionTransfer.getId(), null));
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}
}
