package com.clifton.collateral.balance.search;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateOrderBySqlFormula;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.math.BigDecimal;
import java.util.List;


public class CollateralBalanceSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final CollateralBalanceSearchForm collateralBalanceSearchForm;
	private final SearchRestriction excessCounterpartyCollateralRestriction;
	private final SearchRestriction externalExcessCollateralRestriction;


	public CollateralBalanceSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);

		this.excessCounterpartyCollateralRestriction = searchForm.getSearchRestriction("excessCounterpartyCollateral");
		this.externalExcessCollateralRestriction = searchForm.getSearchRestriction("externalExcessCollateral");
		this.collateralBalanceSearchForm = (CollateralBalanceSearchForm) searchForm;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		CollateralBalanceSearchForm searchForm = this.collateralBalanceSearchForm;

		if (searchForm.getInvestmentAccountGroupTypeName() != null && searchForm.getHoldingInvestmentAccountId() != null) {
			createCollateralBalanceGroupTypeNameCriteria(searchForm, criteria);
		}

		super.configureCriteria(criteria);

		if (searchForm.getBookable() != null) {
			// collateral type allows booking and parent is not set
			String ct = getPathAlias("collateralType", criteria);
			if (searchForm.getBookable()) {
				criteria.add(Restrictions.eq(ct + ".bookable", true));
				criteria.add(Restrictions.isNull("parent.id"));
			}
			else {
				criteria.add(Restrictions.or(
						Restrictions.eq(ct + ".bookable", false),
						Restrictions.isNotNull("parent.id")
				));
			}
		}

		if (searchForm.getExcessCounterpartyCollateral() != null) {
			String comparison = "=";
			if (this.excessCounterpartyCollateralRestriction != null) {
				comparison = this.excessCounterpartyCollateralRestriction.getComparison().getComparisonExpression();
			}
			criteria.add(Restrictions.sqlRestriction("(PostedCounterpartyCollateralHaircutValue - BrokerCollateralRequirement) " + comparison + " ?", searchForm.getExcessCounterpartyCollateral(),
					new org.hibernate.type.BigDecimalType()));
		}
		if (searchForm.getExternalExcessCollateral() != null) {
			String comparison = "=";
			if (this.externalExcessCollateralRestriction != null) {
				comparison = this.externalExcessCollateralRestriction.getComparison().getComparisonExpression();
			}
			criteria.add(Restrictions.sqlRestriction("(ExternalCollateralAmount - ExternalCollateralRequirement) " + comparison + " ?", searchForm.getExternalExcessCollateral(),
					new org.hibernate.type.BigDecimalType()));
		}
		if (searchForm.getSummaryId() != null) {
			criteria.add(Restrictions.or(Restrictions.eq("id", searchForm.getSummaryId()), Restrictions.eq("parent.id", searchForm.getSummaryId())));
		}
		if (searchForm.getAccountGroupId() != null || searchForm.getAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "hag");
			sub.setProjection(Projections.property("id"));
			if (searchForm.getAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getAccountGroupIds()));
			}
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getExcludeAccountGroupId() != null || searchForm.getExcludeAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "aga");
			sub.setProjection(Projections.property("id"));
			if (searchForm.getExcludeAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getExcludeAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getExcludeAccountGroupIds()));
			}
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			criteria.add(Subqueries.notExists(sub));
		}
		if (searchForm.getAccountGroupAlias() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "acctGroup");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "group");
			sub.add(Restrictions.like("group.groupAlias", "%" + searchForm.getAccountGroupAlias() + "%"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			criteria.add(Subqueries.exists(sub));
		}
		if (searchForm.getHoldingInvestmentAccountLabelOrGroupAlias() != null) {
			String value = searchForm.getHoldingInvestmentAccountLabelOrGroupAlias();

			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "acctGroup");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "group");
			sub.add(Restrictions.like("group.groupAlias", "%" + value + "%"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));

			criteria.add(Restrictions.or(
					Restrictions.or(Restrictions.like(getPathAlias("holdingInvestmentAccount", criteria) + ".name", "%" + value + "%"),
							Restrictions.like(getPathAlias("holdingInvestmentAccount", criteria) + ".number", "%" + value + "%")), Subqueries.exists(sub)));

			//criteria.add(Subqueries.exists(sub));
		}
		if (searchForm.getHoldingAccountIsHold() != null) {
			// "Hold" accounts do not move M2M daily but are over-funded instead and only have M2M move when the balance goes negative
			// not the best way to identify hold accounts but works for now
			String type = super.getPathAlias("holdingInvestmentAccount.type", criteria);
			if (Boolean.TRUE.equals(searchForm.getHoldingAccountIsHold())) {
				criteria.add(Restrictions.like(type + ".name", AccountingM2MDaily.HOLD_ACCOUNT_PATTERN, MatchMode.ANYWHERE));
			}
			else {
				criteria.add(Restrictions.not(Restrictions.like(type + ".name", AccountingM2MDaily.HOLD_ACCOUNT_PATTERN, MatchMode.ANYWHERE)));
			}
		}
		if (searchForm.getCoalesceCollateralHoldingAccountCurrencyId() != null) {
			String holdingAccountAlias = super.getPathAlias("holdingInvestmentAccount", criteria);
			criteria.add(Restrictions.or(
					Restrictions.eq("collateralCurrency.id", searchForm.getCoalesceCollateralHoldingAccountCurrencyId()),
					Restrictions.and(Restrictions.isNull("collateralCurrency.id"),
							Restrictions.eq(holdingAccountAlias + ".baseCurrency.id", searchForm.getCoalesceCollateralHoldingAccountCurrencyId()))));
		}
		if (BooleanUtils.isTrue(searchForm.getExcludeAccountsWithoutCollateralChange())) {
			Conjunction conjunction = Restrictions.conjunction(
					Restrictions.eq("collateralChangeAmount", BigDecimal.ZERO),
					Restrictions.eq("counterpartyCollateralChangeAmount", BigDecimal.ZERO)
			);
			if (BooleanUtils.isTrue(searchForm.getExcludeAccountsWithoutCollateralChange())) {
				criteria.add(Restrictions.not(conjunction));
			}
			else {
				criteria.add(conjunction);
			}
		}
		if (BooleanUtils.isTrue(searchForm.getExcludeAccountsWithoutPositions()) || BooleanUtils.isTrue(searchForm.getExcludeAccountsWithoutPositionsAndNoRequiredCollateral())) {
			Conjunction conjunction = Restrictions.conjunction(Restrictions.eq("positionsMarketValue", BigDecimal.ZERO),
					Restrictions.eq("postedCollateralValue", BigDecimal.ZERO),
					Restrictions.eq("postedCounterpartyCollateralValue", BigDecimal.ZERO),
					Restrictions.eq("collateralChangeAmount", BigDecimal.ZERO),
					Restrictions.eq("counterpartyCollateralChangeAmount", BigDecimal.ZERO));
			if (BooleanUtils.isTrue(searchForm.getExcludeAccountsWithoutPositionsAndNoRequiredCollateral())) {
				conjunction.add(Restrictions.eq("externalCollateralRequirement", BigDecimal.ZERO));
				conjunction.add(Restrictions.eq("collateralRequirement", BigDecimal.ZERO));
			}
			criteria.add(Restrictions.not(conjunction));
		}
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		// Special support for ExcessBrokerCollateral & ExternalExcessBrokerCollateral
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(this.collateralBalanceSearchForm.getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			return false;
		}
		for (OrderByField field : orderByList) {
			String name = getOrderByFieldName(field, criteria);
			if ("booked".equals(name)) {
				name = "bookingDate";
			}
			boolean asc = OrderByDirections.ASC == field.getDirection();
			if ("holdingInvestmentAccountLabelOrGroupAlias".equals(name)) {
				criteria.addOrder(Order.asc(getPathAlias("holdingInvestmentAccount", criteria) + ".number"));
			}
			else if ("excessCounterpartyCollateral".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(PostedCounterpartyCollateralHaircutValue - BrokerCollateralRequirement) " + (asc ? "ASC" : "DESC")));
			}
			else if ("externalExcessCollateral".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(ExternalCollateralAmount - ExternalCollateralRequirement) " + (asc ? "ASC" : "DESC")));
			}
			else if ("externalExcessCollateralPercentage".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(MathUtil.GetPercentValue((ExternalCollateralAmount - ExternalCollateralRequirement), ExternalCollateralRequirement)) " + (asc ? "ASC" : "DESC")));
			}
			else if ("excessCollateralPercentage".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(MathUtil.GetPercentValue((PostedCollateralHaircutValue - BrokerCollateralRequirement), BrokerCollateralRequirement)) " + (asc ? "ASC" : "DESC")));
			}
			else if ("optionsCollateralRequiredMovement".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(PostedCollateralHaircutValue - BrokerCollateralRequirement) " + (asc ? "ASC" : "DESC")));
			}
			else if ("optionsCollateralRequiredMovementPercentage".equals(name)) {
				/*
				 * Applies to options collateral only - this is an odd case, because normally this would be expressing the
				 * percentage value of what the 'Required movement' represents (which is based on the collateral requirement)
				 * however, ops wanted the percentage to actually be based on the percentage of the postedCollateralHaircutValue,
				 * rather than the requirement for options collateral - we may update this to be the same for other types, but
				 * currently only applies to options.
				 */
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("CASE WHEN PostedCollateralHaircutValue = 0 AND BrokerCollateralRequirement != 0 THEN -100 ELSE (MathUtil.GetPercentValue((PostedCollateralHaircutValue - BrokerCollateralRequirement), PostedCollateralHaircutValue)) END " + (asc ? "ASC" : "DESC")));
			}
			else if ("optionsPositionsMarketValue".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(COALESCE(PositionsMarketValue, 0) + COALESCE(TotalCashValue, 0)) " + (asc ? "ASC" : "DESC")));
			}
			else if ("optionsCollateralRequirement".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(COALESCE(BrokerCollateralRequirement, 0) + COALESCE(PostedCollateralValue, 0) - COALESCE(PostedCollateralHaircutValue, 0)) " + (asc ? "ASC" : "DESC")));
			}
			else if ("excessOptionsEquityCollateral".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("((COALESCE(PositionsMarketValue, 0) + COALESCE(TotalCashValue, 0)) - (COALESCE(BrokerCollateralRequirement, 0) + COALESCE(PostedCollateralValue, 0) - COALESCE(PostedCollateralHaircutValue, 0))) " + (asc ? "ASC" : "DESC")));
			}
			else if ("excessOptionsEquityCollateralPercentage".equals(name)) {
				criteria.addOrder(HibernateOrderBySqlFormula.sqlFormula("(MathUtil.GetPercentValue((COALESCE(PositionsMarketValue, 0) + COALESCE(TotalCashValue, 0)) - (COALESCE(BrokerCollateralRequirement, 0) + COALESCE(PostedCollateralValue, 0) - COALESCE(PostedCollateralHaircutValue, 0)), (COALESCE(BrokerCollateralRequirement, 0) + COALESCE(PostedCollateralValue, 0) - COALESCE(PostedCollateralHaircutValue, 0)))) " + (asc ? "ASC" : "DESC")));
			}
			else {
				criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
			}
		}
		return true;
	}


	public void createCollateralBalanceGroupTypeNameCriteria(CollateralBalanceSearchForm searchForm, Criteria criteria) {

		//Find account groups for the specified holding investment account id
		DetachedCriteria accountGroups = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "accountGroupAccount");
		accountGroups.createAlias("referenceOne", "accountGroup");
		accountGroups.createAlias("referenceTwo", "account");
		accountGroups.createAlias("account.type", "accountType");
		accountGroups.setProjection(Projections.property("accountGroup.id"));
		accountGroups.add(
				Restrictions.and(
						Restrictions.eq("account.id", searchForm.getHoldingInvestmentAccountId()),
						Restrictions.eq("accountType.id", searchForm.getHoldingInvestmentAccountTypeId())
				)
		);

		//Find accounts inside the group that was found with the holding investment account id that were found containing at least one account of the specified account type.
		DetachedCriteria accounts = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "accountGroupAccount");
		accounts.createAlias("referenceOne", "accountGroup");
		accounts.createAlias("referenceTwo", "account");
		accounts.createAlias("accountGroup.type", "accountGroupType");
		accounts.setProjection(Projections.property("account.id"));
		accounts.add(
				Restrictions.and(
						Restrictions.eq("accountGroupType.name", searchForm.getInvestmentAccountGroupTypeName()),
						Subqueries.propertyIn("accountGroup.id", accountGroups)
				)
		);
		criteria.add(
				Restrictions.or(
						Subqueries.propertyIn("holdingInvestmentAccount.id", accounts),
						Restrictions.eq("holdingInvestmentAccount.id", searchForm.getHoldingInvestmentAccountId())
				)
		);

		//Since the accounts are found by the custom statement remove from the global criteria and set only
		searchForm.removeSearchRestriction("holdingInvestmentAccountId");
		searchForm.removeSearchRestriction("holdingInvestmentAccountTypeId");
	}
}
