package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author terrys
 */
public class CollateralOptionsStrategySplitPosition extends CollateralOptionsStrategyPosition {

	private final CollateralOptionsStrategyPosition originalCollateralPosition;
	private final CollateralOptionsStrategyPosition remainderCollateralPosition;

	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategySplitPosition(CollateralOptionsStrategyPosition originalCollateralPosition, BigDecimal remainingQuantity) {
		super(originalCollateralPosition, MathUtils.isSameSignum(remainingQuantity, originalCollateralPosition.getRemainingQuantity()) ? remainingQuantity : MathUtils.negate(remainingQuantity));
		this.originalCollateralPosition = originalCollateralPosition;

		BigDecimal splitQuantity = getOriginalCollateralPosition().getRemainingQuantity().abs().subtract(getRemainingQuantity().abs());
		if (MathUtils.isNegative(getOriginalCollateralPosition().getRemainingQuantity())) {
			splitQuantity = splitQuantity.negate();
		}
		this.remainderCollateralPosition = new CollateralOptionsStrategyPosition(this, splitQuantity);

		AssertUtils.assertEquals(0, this.remainderCollateralPosition.getRemainingQuantity().add(this.getRemainingQuantity()).
						compareTo(this.originalCollateralPosition.getRemainingQuantity()), "Split quantities[%s] and [%s] must equal original position [%s].",
				this.remainderCollateralPosition.getRemainingQuantity(), this.getRemainingQuantity(), originalCollateralPosition.getRemainingQuantity());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String buildPositionExplanation() {
		StringBuilder results = new StringBuilder();

		results.append("Split Position: \n");
		results.append(super.buildPositionExplanation()).append('\n');
		results.append("Original Position: \n");
		results.append(getOriginalCollateralPosition().buildPositionExplanation());

		return results.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyPosition getOriginalCollateralPosition() {
		return this.originalCollateralPosition;
	}


	public CollateralOptionsStrategyPosition getRemainderCollateralPosition() {
		return this.remainderCollateralPosition;
	}
}
