package com.clifton.collateral.balance;

import com.clifton.core.util.MathUtils;

import javax.persistence.Table;
import java.math.BigDecimal;


/**
 * @author terrys
 */
@Table(name = "CollateralBalance")
public class OTCCollateralBalance extends CollateralBalance {


	/**
	 * Returns Client or Counterparty Min Transfer Amount depending on which one should be used.
	 */
	public BigDecimal getCalculatedMinTransferAmount() {
		if (isInClientFavor()) {
			return getMinTransferCounterpartyAmount();
		}
		return getMinTransferAmount();
	}


	public BigDecimal getCalculatedChangeAmount() {
		return MathUtils.subtract(getCounterpartyCollateralChangeAmount(), getCollateralChangeAmount());
	}
}
