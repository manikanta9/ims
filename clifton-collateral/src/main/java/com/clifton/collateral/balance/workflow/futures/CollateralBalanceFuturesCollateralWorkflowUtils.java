package com.clifton.collateral.balance.workflow.futures;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * @author jonathanr
 */
public class CollateralBalanceFuturesCollateralWorkflowUtils {

	/**
	 * Returns only positive differences values between maps and returns them as a new <code>FuturesCollateralPositionMap</code>
	 * with new <code>FuturesCollateralPositionPledge</code> objects containing the differences values between maps.
	 * The map is positive in that it contains no negative values including cash such that caller contains value 'A'
	 * and argument contains corresponding value 'B':
	 * <ul>
	 *     <li>If 'A' exists and 'B' is null returned map contains 'A'</li>
	 *     <li>If 'A' is null and 'B' exists returned map contains null</li>
	 *     <li>If 'A' exists and 'B' exists and 'A' > 'B' returned map contains 'A' - 'B'</li>
	 *     <li>If 'A' exists and 'B' exists and 'A' < 'B' returned map contains null</li>
	 * </ul>
	 *
	 * @param pledgeMap1
	 * @param pledgeMap2
	 * @return new map containing only positive differences between pledgeMap1 and pledgeMap2
	 */
	public static Map<InvestmentSecurity, FuturesCollateralPositionPledge> getPositiveDifferenceMap(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap1, Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap2) {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> difMap = new LinkedHashMap<>();
		for (FuturesCollateralPositionPledge thisPledge : pledgeMap1.values()) {
			FuturesCollateralPositionPledge targetPledge = pledgeMap2.get(thisPledge.getInvestmentSecurity());
			if (targetPledge != null) {
				BigDecimal quantity = MathUtils.subtract(thisPledge.getQuantity(), targetPledge.getQuantity());
				BigDecimal marketValue = MathUtils.subtract(thisPledge.getMarketValue(), targetPledge.getMarketValue());
				if (MathUtils.isPositive(quantity)) {
					FuturesCollateralPositionPledge diffPledge = new FuturesCollateralPositionPledge(thisPledge.getInvestmentSecurity(), quantity, marketValue, thisPledge.getHaircutValue());
					aggregateAndPut(diffPledge, difMap);
				}
			}
			else {
				aggregateAndPut(thisPledge, difMap);
			}
		}
		return difMap;
	}


	/**
	 * Aggregates <code>sourceMap</code> values into <code>targetMap</code>. <code>targetMap</code> is modified <code>sourceMap</code> is unchanged.
	 *
	 * @param targetMap
	 * @param sourceMap
	 */
	public static void aggregateMaps(Map<InvestmentSecurity, FuturesCollateralPositionPledge> targetMap, Map<InvestmentSecurity, FuturesCollateralPositionPledge> sourceMap) {
		for (FuturesCollateralPositionPledge sourcePledge : CollectionUtils.getIterable(sourceMap.values())) {
			aggregateAndPut(sourcePledge, targetMap);
		}
	}


	/**
	 * Aggregates <code>futuresCollateralPositionPledge</code> in to <code>pledgeMap</code>. Modifies pledgeMap.
	 *
	 * @param futuresCollateralPositionPledge
	 * @param pledgeMap
	 */
	public static void aggregateAndPut(FuturesCollateralPositionPledge futuresCollateralPositionPledge, Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		InvestmentSecurity key = futuresCollateralPositionPledge.getInvestmentSecurity();
		FuturesCollateralPositionPledge existingPledge;
		if (pledgeMap.containsKey(key)) {
			existingPledge = pledgeMap.get(key);
		}
		else {
			existingPledge = new FuturesCollateralPositionPledge(futuresCollateralPositionPledge.getInvestmentSecurity(), BigDecimal.ZERO, BigDecimal.ZERO, futuresCollateralPositionPledge.getHaircutValue());
		}
		pledgeMap.put(key, aggregateFuturesCollateralPositionPledges(futuresCollateralPositionPledge, existingPledge));
	}


	/**
	 * Aggregates the quantity and the market value of of pledge1 and pledge2 and returns a new <code>FuturesCollateralPositionPledge</code> with the
	 *
	 * @param pledge1
	 * @param pledge2
	 */
	public static FuturesCollateralPositionPledge aggregateFuturesCollateralPositionPledges(FuturesCollateralPositionPledge pledge1, FuturesCollateralPositionPledge pledge2) {
		if (!CompareUtils.isEqual(pledge1.getInvestmentSecurity(), pledge2.getInvestmentSecurity())) {
			throw new IllegalArgumentException("The argument and the calling object must have the same InvestmentSecurity inorder to aggregate.");
		}
		return new FuturesCollateralPositionPledge(pledge1.getInvestmentSecurity(), MathUtils.add(pledge1.getQuantity(), pledge2.getQuantity()), MathUtils.add(pledge1.getMarketValue(), pledge2.getMarketValue()), pledge1.getHaircutValue());
	}


	public static BigDecimal getTotalPositionMarketValueIncludingCash(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		if (CollectionUtils.isEmpty(pledgeMap.values())) {
			return BigDecimal.ZERO;
		}
		return CollectionUtils.getStream(pledgeMap.values()).map(FuturesCollateralPositionPledge::getMarketValue).reduce(BigDecimal.ZERO, MathUtils::add);
	}


	public static BigDecimal getTotalPositionCollateralValueIncludingCash(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		if (CollectionUtils.isEmpty(pledgeMap.values())) {
			return BigDecimal.ZERO;
		}
		return CollectionUtils.getStream(pledgeMap.values()).map(FuturesCollateralPositionPledge::getCollateralValue).reduce(BigDecimal.ZERO, MathUtils::add);
	}


	public static BigDecimal getTotalPositionMarketValue(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		BigDecimal totalWithCash = getTotalPositionMarketValueIncludingCash(pledgeMap);
		return MathUtils.subtract(totalWithCash, getCashValue(pledgeMap));
	}


	public static BigDecimal getTotalPositionCollateralValue(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		BigDecimal totalWithCash = getTotalPositionCollateralValueIncludingCash(pledgeMap);
		return MathUtils.subtract(totalWithCash, getCashValue(pledgeMap));
	}


	public static BigDecimal getTotalPositionQuantity(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		return CollectionUtils.isEmpty(pledgeMap.values()) ? BigDecimal.ZERO : CoreMathUtils.sumProperty(pledgeMap.values(), FuturesCollateralPositionPledge::getQuantity);
	}


	public static BigDecimal getCashValue(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		return CollectionUtils.getStream(pledgeMap.values()).filter(positionPledge -> positionPledge.getInvestmentSecurity().isCurrency()).map(FuturesCollateralPositionPledge::getMarketValue).reduce(BigDecimal.ZERO, MathUtils::add);
	}


	public static Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> getPositionPledgedListByHierarchyMap(Map<InvestmentSecurity, FuturesCollateralPositionPledge> pledgeMap) {
		Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> investmentInstrumentHierarchyListMap = new HashMap<>();
		for (InvestmentSecurity investmentSecurity : CollectionUtils.getIterable(pledgeMap.keySet())) {
			if (investmentInstrumentHierarchyListMap.containsKey(investmentSecurity.getInstrument().getHierarchy())) {
				investmentInstrumentHierarchyListMap.get(investmentSecurity.getInstrument().getHierarchy()).add(pledgeMap.get(investmentSecurity));
			}
			else {
				List<FuturesCollateralPositionPledge> pledgeList = new ArrayList<>(1);
				pledgeList.add(pledgeMap.get(investmentSecurity));
				investmentInstrumentHierarchyListMap.put(investmentSecurity.getInstrument().getHierarchy(), pledgeList);
			}
		}
		return investmentInstrumentHierarchyListMap;
	}
}
