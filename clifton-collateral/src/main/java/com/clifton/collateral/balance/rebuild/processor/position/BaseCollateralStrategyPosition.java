package com.clifton.collateral.balance.rebuild.processor.position;

import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;


/**
 * @author StevenF
 */
public abstract class BaseCollateralStrategyPosition<P extends AccountingPositionInfo> implements CollateralStrategyPosition {

	private final transient UUID uuid = UUID.randomUUID();

	private final Date positionDate;
	private final InvestmentSecurity investmentSecurity;
	private final BigDecimal priceMultiplier;
	private final BigDecimal remainingQuantity;

	private String positionGroupId;
	private BigDecimal requiredCollateral;
	private BigDecimal fxRateToBase;

	////////////////////////////////////////////////////////////////////////////////


	protected BaseCollateralStrategyPosition(P accountingPositionInfo, Date positionDate) {
		this(
				positionDate,
				accountingPositionInfo.getInvestmentSecurity(),
				accountingPositionInfo.getInvestmentSecurity().getPriceMultiplier(),
				accountingPositionInfo.getRemainingQuantity(),
				accountingPositionInfo.getPositionGroupId()
		);
	}


	protected BaseCollateralStrategyPosition(Date positionDate, InvestmentSecurity investmentSecurity, BigDecimal priceMultiplier, BigDecimal remainingQuantity, String positionGroupId) {
		this.positionDate = positionDate;
		this.investmentSecurity = investmentSecurity;
		this.priceMultiplier = priceMultiplier;
		this.remainingQuantity = remainingQuantity;
		this.positionGroupId = positionGroupId;

		this.requiredCollateral = BigDecimal.ZERO;
		this.fxRateToBase = BigDecimal.ONE;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getBaseRequiredCollateral() {
		return MathUtils.multiply(getRequiredCollateral(), getFxRateToBase());
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BaseCollateralStrategyPosition<?> that = (BaseCollateralStrategyPosition<?>) o;
		return Objects.equals(this.uuid, that.uuid);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.uuid);
	}


	@Override
	public String toString() {
		return "{" +
				", positionDate=" + DateUtils.fromDateShort(this.positionDate) +
				", investmentSecurity=" + this.investmentSecurity.getSymbol() +
				", remainingQuantity=" + this.remainingQuantity +
				", positionGroupId='" + this.positionGroupId + '\'' +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public UUID getUuid() {
		return this.uuid;
	}


	@Override
	public Date getPositionDate() {
		return this.positionDate;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	@Override
	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	@Override
	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	@Override
	public String getPositionGroupId() {
		return this.positionGroupId;
	}


	@Override
	public void setPositionGroupId(String positionGroupId) {
		this.positionGroupId = positionGroupId;
	}


	@Override
	public BigDecimal getRequiredCollateral() {
		return this.requiredCollateral;
	}


	@Override
	public void setRequiredCollateral(BigDecimal requiredCollateral) {
		this.requiredCollateral = requiredCollateral;
	}


	@Override
	public BigDecimal getFxRateToBase() {
		return this.fxRateToBase;
	}


	@Override
	public void setFxRateToBase(BigDecimal fxRateToBase) {
		this.fxRateToBase = fxRateToBase;
	}
}
