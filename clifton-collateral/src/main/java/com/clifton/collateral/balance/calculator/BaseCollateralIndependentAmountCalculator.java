package com.clifton.collateral.balance.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The {@link BaseCollateralIndependentAmountCalculator} is the default implementation of the Independent Amount calculator.
 *
 * @author mwacker
 */
public class BaseCollateralIndependentAmountCalculator implements CollateralIndependentAmountCalculator {

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	////////////////////////////////////////////////////////////////////////////////


	public BaseCollateralIndependentAmountCalculator() {
	}


	public BaseCollateralIndependentAmountCalculator(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Because the client always pays independent amount, our convention is to always have it as a negative number.
	 */
	@Override
	public BigDecimal calculateIndependentAmount(CollateralBalance balance, AccountingPosition position, BigDecimal balanceIndependentAmountPercent) {
		balanceIndependentAmountPercent = getBalanceIndependentAmountPercent(position, balanceIndependentAmountPercent);
		return MathUtils.negate(MathUtils.abs(MathUtils.getPercentageOf(balanceIndependentAmountPercent, position.getRemainingCostBasis(), true)));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If an IA percent is not provided, look it up from the security.
	 */
	protected BigDecimal getBalanceIndependentAmountPercent(AccountingPosition position, BigDecimal balanceIndependentAmountPercent) {
		if (balanceIndependentAmountPercent == null) {
			balanceIndependentAmountPercent = (BigDecimal) getInvestmentSecurityUtilHandler().getCustomColumnValue(position.getInvestmentSecurity(), InvestmentSecurity.INDEPENDENT_AMOUNT_PERCENT_CUSTOM_COLUMN);
		}
		return balanceIndependentAmountPercent;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
