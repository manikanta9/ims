package com.clifton.collateral.balance.rebuild;


import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralBalanceRebuildService</code> contains methods for rebuilding {@link CollateralBalance} records
 *
 * @author stevenf
 */
public interface CollateralBalanceRebuildService {


	/**
	 * Rebuilds CollateralBalances using the settings in the rebuild command.
	 */
	@ModelAttribute("result")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status rebuildCollateralBalance(final CollateralBalanceRebuildCommand command);


	/**
	 * Finds the correct collateral balance processor and uses it to round the amount correctly.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = CollateralBalance.class)
	public BigDecimal getCollateralTransferAmountRounded(String collateralTypeName, int holdingAccountId, BigDecimal amount, Date balanceDate);


	@DoNotAddRequestMapping
	public InvestmentAccount getCollateralAccount(CollateralType collateralType, InvestmentAccount holdingAccount, Date balanceDate);


	@DoNotAddRequestMapping
	public CollateralBalanceRebuildProcessor<?> getCollateralBalanceRebuildProcessor(CollateralType collateralType, InvestmentAccount holdingAccount, InvestmentAccount collateralAccount);
}
