package com.clifton.collateral.balance.booking;


import com.clifton.accounting.gl.booking.processor.SameRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralBalanceBookingProcessor</code> handles the booking process for CollateralBalances.
 *
 * @author mwacker
 */
public class CollateralBalanceBookingProcessor extends SameRulesBookingProcessor<CollateralBalance> {

	public static final String JOURNAL_NAME = "Collateral Journal";

	private CollateralBalanceService collateralBalanceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public CollateralBalance getBookableEntity(Serializable id) {
		return getCollateralBalanceService().getCollateralBalance((Integer) id);
	}


	@Override
	public List<CollateralBalance> getUnbookedEntityList() {
		// return unbooked with non 0 transfer amount
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBooked(false);
		searchForm.setBookable(true);
		searchForm.addSearchRestriction(new SearchRestriction("transferAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		return getCollateralBalanceService().getCollateralBalanceList(searchForm);
	}


	@Override
	public SimpleBookingSession<CollateralBalance> newBookingSession(AccountingJournal journal, CollateralBalance collateralBalance) {
		if (collateralBalance == null) {
			collateralBalance = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(collateralBalance, "Cannot find collateral balance with id = " + journal.getFkFieldId());
		}
		ValidationUtils.assertTrue(isBookableCollateralType(collateralBalance.getCollateralType()), "Collateral Balance with ID = " + journal.getFkFieldId() + " is a ["
				+ collateralBalance.getCollateralType().getName() + "] balance.  Cannot process this balance from Non OTC booking processor.");
		return new SimpleBookingSession<>(journal, collateralBalance);
	}


	@Override
	public CollateralBalance markBooked(CollateralBalance balance) {
		ValidationUtils.assertNull(balance.getBookingDate(), "Cannot book Collateral Balance with id = " + balance.getId() + " because it already has bookingDate set to " + balance.getBookingDate());

		balance.setBookingDate(new Date());
		return getCollateralBalanceService().saveCollateralBalance(balance);
	}


	@Override
	public CollateralBalance markUnbooked(int balanceId) {
		CollateralBalance balance = getBookableEntity(balanceId);
		balance.setBookingDate(null);
		return getCollateralBalanceService().saveCollateralBalance(balance);
	}


	@Override
	public void deleteSourceEntity(int balanceId) {
		throw new IllegalArgumentException("Cannot delete daily collateral balance entries from unbooking process: " + balanceId);
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	/**
	 * Currently - Non OTC is the only supported Collateral Type for Booking
	 */
	private boolean isBookableCollateralType(CollateralType type) {
		return type.isBookable();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}
}
