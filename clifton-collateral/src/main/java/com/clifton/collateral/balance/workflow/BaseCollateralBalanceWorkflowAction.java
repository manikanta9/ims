package com.clifton.collateral.balance.workflow;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>CollateralBalanceCollateralTransferWorkflowAction</code> class is a workflow transition action
 * that creates collateral transfers and books them as well.
 * <p/>
 *
 * @author stevenf
 */
public abstract class BaseCollateralBalanceWorkflowAction implements WorkflowTransitionActionHandler<CollateralBalance> {

	private CollateralBalancePledgedService collateralBalancePledgedService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralBalance processAction(CollateralBalance collateralBalance, WorkflowTransition transition) {
		processCollateralBalanceWorkflowAction(collateralBalance);
		return collateralBalance;
	}


	public abstract void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance);

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalancePledgedService getCollateralBalancePledgedService() {
		return this.collateralBalancePledgedService;
	}


	public void setCollateralBalancePledgedService(CollateralBalancePledgedService collateralBalancePledgedService) {
		this.collateralBalancePledgedService = collateralBalancePledgedService;
	}
}
