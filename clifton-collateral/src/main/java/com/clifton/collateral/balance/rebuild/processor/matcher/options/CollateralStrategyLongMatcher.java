package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.matcher.CollateralStrategyMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralStrategyLongMatcher implements CollateralStrategyMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategyLongMatcher longMatcher = new CollateralStrategyLongMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyLongPositionList(List<CollateralOptionsStrategyPosition> positionInfoList, Date positionDate) {
		return longMatcher.getCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), positionDate);
	}


	@Override
	public Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		LocalDate nineMonthsOut = DateUtils.asLocalDate(positionDate).plus(9, ChronoUnit.MONTHS);
		Map<AggregateCollateralStrategyOptionMatcher.TYPE, Map<Boolean, List<CollateralOptionsStrategyPosition>>> groupings = positionInfoList.stream()
				.filter(p -> InvestmentUtils.isOption(p.getInvestmentSecurity()))
				.filter(p -> MathUtils.isPositive(p.getRemainingQuantity()))
				.collect(
						Collectors.groupingBy(
								p -> AggregateCollateralStrategyOptionMatcher.TYPE.find(p.getOptionType()),
								Collectors.groupingBy(
										p -> DateUtils.asLocalDate(p.getInvestmentSecurity().getEndDate()).isAfter(nineMonthsOut)
								)
						)
				);
		for (Map.Entry<AggregateCollateralStrategyOptionMatcher.TYPE, Map<Boolean, List<CollateralOptionsStrategyPosition>>> entry : groupings.entrySet()) {
			if (entry.getKey() == AggregateCollateralStrategyOptionMatcher.TYPE.CALL) {
				Map<Boolean, List<CollateralOptionsStrategyPosition>> callMap = entry.getValue();
				if (callMap.containsKey(Boolean.FALSE)) {
					results.put(CollateralOptionsStrategyCalculators.LONG_CALL_UNDER_9_MONTHS, callMap.get(Boolean.FALSE));
				}
				if (callMap.containsKey(Boolean.TRUE)) {
					results.put(CollateralOptionsStrategyCalculators.LONG_CALL_OVER_9_MONTHS, callMap.get(Boolean.TRUE));
				}
			}
			else {
				Map<Boolean, List<CollateralOptionsStrategyPosition>> putMap = entry.getValue();
				if (putMap.containsKey(Boolean.FALSE)) {
					results.put(CollateralOptionsStrategyCalculators.LONG_PUT_UNDER_9_MONTHS, putMap.get(Boolean.FALSE));
				}
				if (putMap.containsKey(Boolean.TRUE)) {
					results.put(CollateralOptionsStrategyCalculators.LONG_PUT_OVER_9_MONTHS, putMap.get(Boolean.TRUE));
				}
			}
		}

		// update group IDs
		results.forEach((key, value) -> CollectionUtils.getStream(value).forEach(p -> p.setPositionGroupId(key.getTradeStrategyName())));

		// remove matches from the position list
		List<CollateralOptionsStrategyPosition> removals = results.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		positionInfoList.removeAll(removals);

		return results;
	}
}
