package com.clifton.collateral.balance.m2m;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * The <code>CollateralBalanceM2MService</code> defines methods used to look up M2M object with collateral balance data.
 *
 * @author mwacker
 */
public interface CollateralBalanceM2MService {

	@SecureMethod(dtoClass = AccountingM2MDaily.class)
	public List<AccountingM2MDailyExtended> getCollateralBalanceM2MDailyExtendedList(AccountingM2MDailyExtendedSearchForm searchForm);
}
