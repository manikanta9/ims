package com.clifton.collateral.balance.workflow.futures;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceFuturesRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Primary Context for the {@Link CollateralBalancePledgedTransactionsFuturesWorkflowAction} initializes data for easy access and
 * computation within the action.
 *
 * @author jonathanr
 */
public class CollateralBalancePledgedTransactionsFuturesWorkflowContext {

	private static final BigDecimal PERCENT_CONVERSION_MULTIPLICAND = new BigDecimal(".01");

	private final CollateralBalance collateralBalance;
	private final CollateralBalanceFuturesRebuildProcessor<?> processor;
	private final String errorMsgPrefix;

	private Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> eligibleCollateralMap;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianCollateral;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> idealCollateral = new LinkedHashMap<>();
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn;


	public CollateralBalancePledgedTransactionsFuturesWorkflowContext(CollateralBalance collateralBalance, CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalance = collateralBalance;
		this.processor = getRebuildProcessor(collateralBalance, collateralBalanceRebuildService);
		this.errorMsgPrefix = "Error processing collateral for " + collateralBalance.getLabel() + ": ";
		ValidationUtils.assertNotNull(getProcessor().getMinimumCashTransferAmount(), this.getErrorMsgPrefix() + "No Minimum Cash Transfer amount configured for collateralBalance.");
		ValidationUtils.assertNotNull(getProcessor().getCashTransferIncrementAmount(), this.getErrorMsgPrefix() + "No Cash Transfer Increment amount configured for collateralBalance.");
		ValidationUtils.assertNotNull(getProcessor().getMinimumPositionTransferAmount(), this.getErrorMsgPrefix() + "No Minimum Position Transfer amount configured for collateralBalance.");
		ValidationUtils.assertNotNull(getProcessor().getMinimumTransferQuantity(), this.getErrorMsgPrefix() + "No Minimum Transfer quantity configured for collateralBalance.");
		ValidationUtils.assertNotNull(getProcessor().getTargetCollateralBufferPercent(), this.getErrorMsgPrefix() + "No Target Collateral Buffer Percentage configured for collateralBalance.");
	}


	private CollateralBalanceFuturesRebuildProcessor<?> getRebuildProcessor(CollateralBalance collateralBalance, CollateralBalanceRebuildService collateralBalanceRebuildService) {
		CollateralBalanceRebuildProcessor<?> rebuildProcessor = collateralBalanceRebuildService.getCollateralBalanceRebuildProcessor(collateralBalance.getCollateralType(), collateralBalance.getHoldingInvestmentAccount(), collateralBalance.getCollateralInvestmentAccount());
		ValidationUtils.assertTrue(rebuildProcessor instanceof CollateralBalanceFuturesRebuildProcessor, this.getErrorMsgPrefix() + "An instance of CollateralBalanceFuturesRebuildProcessor is required!");
		assert rebuildProcessor instanceof CollateralBalanceFuturesRebuildProcessor<?>;
		return (CollateralBalanceFuturesRebuildProcessor<?>) rebuildProcessor;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                   Delegate Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMinimumCashTransferAmount() {
		return getProcessor().getMinimumCashTransferAmount();
	}


	public BigDecimal getCashTransferIncrementAmount() {
		return getProcessor().getCashTransferIncrementAmount();
	}


	public BigDecimal getMinimumPositionTransferAmount() {
		return getProcessor().getMinimumPositionTransferAmount();
	}


	public BigDecimal getMinimumTransferQuantity() {
		return getProcessor().getMinimumTransferQuantity();
	}


	public List<Short> getInvestmentInstrumentHierarchyIds() {
		return getProcessor().getInvestmentInstrumentHierarchyIds();
	}


	public boolean isManualAccount() {
		return getProcessor().isManualAccount();
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                    Computed Getters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTargetTransferAmount() {
		return MathUtils.getPercentageOf(MathUtils.add(BigDecimal.ONE, getTargetCollateralBufferPercent()), getCollateralBalance().getExternalCollateralRequirement());
	}


	public BigDecimal getTargetCollateralBufferPercent() {
		return MathUtils.multiply(getProcessor().getTargetCollateralBufferPercent(), PERCENT_CONVERSION_MULTIPLICAND);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                  Getters And Setters                   //////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalance getCollateralBalance() {
		return this.collateralBalance;
	}


	public CollateralBalanceFuturesRebuildProcessor<?> getProcessor() {
		return this.processor;
	}


	public Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> getEligibleCollateralMap() {
		return this.eligibleCollateralMap;
	}


	public void setEligibleCollateralMap(Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> eligibleCollateralMap) {
		this.eligibleCollateralMap = eligibleCollateralMap;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getBrokerHeldCollateral() {
		return this.brokerHeldCollateral;
	}


	public void setBrokerHeldCollateral(Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral) {
		this.brokerHeldCollateral = brokerHeldCollateral;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getCustodianCollateral() {
		return this.custodianCollateral;
	}


	public void setCustodianCollateral(Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianCollateral) {
		this.custodianCollateral = custodianCollateral;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getIdealCollateral() {
		return this.idealCollateral;
	}


	public void setIdealCollateral(Map<InvestmentSecurity, FuturesCollateralPositionPledge> idealCollateral) {
		this.idealCollateral = idealCollateral;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getCollateralPost() {
		return this.collateralPost;
	}


	public void setCollateralPost(Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost) {
		this.collateralPost = collateralPost;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getCollateralReturn() {
		return this.collateralReturn;
	}


	public void setCollateralReturn(Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn) {
		this.collateralReturn = collateralReturn;
	}


	public String getErrorMsgPrefix() {
		return this.errorMsgPrefix;
	}
}
