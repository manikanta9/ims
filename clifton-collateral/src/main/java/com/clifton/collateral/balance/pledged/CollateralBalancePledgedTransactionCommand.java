package com.clifton.collateral.balance.pledged;

import com.clifton.collateral.balance.pledged.search.CollateralBalancePledgedTransactionSearchForm;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * The <code>CollateralBalancePledgedCommand</code> is used to provide preview and generation functionality for
 * options collateral.
 *
 * @author stevenf
 */
public class CollateralBalancePledgedTransactionCommand extends CollateralBalancePledgedTransactionSearchForm {

	//Only used when uploading pledged transactions
	private MultipartFile file;

	//Used in tests only at this time - allows transfers to be automatically booked and posted after creation.
	private boolean bookAndPost;

	//Specifies that the command is being executed automatically (e.g. by a workflow transition action)
	private boolean automatic;

	//Designates if pledged transactions should be generated based on required collateral for daily positions
	//or if existing pledged transactions should simply be queried for.
	private boolean generatePledgedCollateral;
	//Used when creating transfers to determine the direction of the transfer
	private boolean postCollateral;

	private Integer collateralBalanceId;
	private List<CollateralBalancePledgedTransaction> collateralBalancePledgedTransactionList;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public boolean isBookAndPost() {
		return this.bookAndPost;
	}


	public void setBookAndPost(boolean bookAndPost) {
		this.bookAndPost = bookAndPost;
	}


	public boolean isAutomatic() {
		return this.automatic;
	}


	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}


	public boolean isGeneratePledgedCollateral() {
		return this.generatePledgedCollateral;
	}


	public void setGeneratePledgedCollateral(boolean generatePledgedCollateral) {
		this.generatePledgedCollateral = generatePledgedCollateral;
	}


	public boolean isPostCollateral() {
		return this.postCollateral;
	}


	public void setPostCollateral(boolean postCollateral) {
		this.postCollateral = postCollateral;
	}


	public Integer getCollateralBalanceId() {
		return this.collateralBalanceId;
	}


	public void setCollateralBalanceId(Integer collateralBalanceId) {
		this.collateralBalanceId = collateralBalanceId;
	}


	public List<CollateralBalancePledgedTransaction> getCollateralBalancePledgedTransactionList() {
		return this.collateralBalancePledgedTransactionList;
	}


	public void setCollateralBalancePledgedTransactionList(List<CollateralBalancePledgedTransaction> collateralBalancePledgedTransactionList) {
		this.collateralBalancePledgedTransactionList = collateralBalancePledgedTransactionList;
	}
}
