package com.clifton.collateral.balance.rebuild;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalanceDetail;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>CollateralBalanceRebuildCommand</code> holds all objects needed to rebuild a CollateralBalance entry for a specific holding account.
 *
 * @author stevenf
 */
public class CollateralBalanceRebuildCommand {

	private Date balanceDate;

	private boolean skipExisting;
	private boolean synchronous;
	/**
	 * By default, the Status object will be returned and will contain all errors, if any.
	 * Set this option to true, in order to throw an exception if an error was detected.
	 */
	private boolean throwExceptionOnError;

	private Short collateralTypeId;
	private String collateralTypeName;

	private Integer asynchronousRebuildDelay;
	private Integer businessClientId;
	private Integer holdingAccountId;
	private Short holdingAccountTypeId;
	private Integer issuingCompanyId;

	private Short[] excludeHoldingAccountTypeIds;

	private BusinessCompany issuingCompany;
	private CollateralType collateralType;
	private InvestmentAccount holdingAccount;

	// Used for currency specific
	private InvestmentSecurity currency;
	Map<InvestmentSecurity, List<AccountingPosition>> currencyPositionMap = null;

	private List<AccountingPosition> positionList;
	private List<CollateralPosition> collateralPositionList;
	private List<CollateralPosition> endingCollateralPositionList;
	private List<CollateralBalanceDetail> detailList;

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CollateralBalanceRebuildCommand ofSynchronousWithThrow() {
		CollateralBalanceRebuildCommand result = new CollateralBalanceRebuildCommand();
		result.setSynchronous(true);
		result.setThrowExceptionOnError(true);
		return result;
	}


	public CollateralBalanceSearchForm getCollateralBalanceSearchForm() {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(getBalanceDate());
		searchForm.setBusinessClientId(getBusinessClientId());
		searchForm.setHoldingInvestmentAccountId(getHoldingAccountId());
		searchForm.setHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
		searchForm.setIssuingCompanyId(getIssuingCompanyId());
		searchForm.setInvestmentAccountGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		return searchForm;
	}


	public String getRunId() {
		String runId = "";
		if (getBusinessClientId() != null) {
			runId = "CLIENT_" + getBusinessClientId();
		}
		else if (getHoldingAccountId() != null) {
			runId = "HOLDER_" + getHoldingAccountId();
		}
		else if (getIssuingCompanyId() != null) {
			runId = "ISSUER_" + getIssuingCompanyId();
		}
		if (getCollateralType() != null) {
			runId += "_" + getCollateralType().getName() + "_";
		}
		else {
			runId += "_ALL_";
		}
		runId += DateUtils.fromDate(getBalanceDate());
		return runId;
	}


	/**
	 * Creates a map of currency to positions so we have full breakdown
	 */
	public void setupLocalCurrencySupport() {
		this.currencyPositionMap = new HashMap<>();
		for (AccountingPosition pos : CollectionUtils.getIterable(this.positionList)) {
			InvestmentSecurity cur = pos.getInvestmentSecurity().getInstrument().getTradingCurrency();
			if (this.currencyPositionMap.containsKey(cur)) {
				this.currencyPositionMap.get(cur).add(pos);
			}
			else {
				this.currencyPositionMap.put(cur, CollectionUtils.createList(pos));
			}
		}
	}


	public Set<InvestmentSecurity> getCurrencyList() {
		if (this.currencyPositionMap == null) {
			return null;
		}
		return this.currencyPositionMap.keySet();
	}


	/**
	 * Returns currency specific (if applies) position list
	 */
	public List<AccountingPosition> getPositionList() {
		// No Currency - Return all
		if (getCurrency() == null || this.currencyPositionMap == null) {
			return this.positionList;
		}
		// Otherwise filter on ccy
		return this.currencyPositionMap.get(getCurrency());
	}


	/**
	 * Returns currency specific (if applies) position list
	 */
	public List<CollateralPosition> getCollateralPositionList() {
		// No Currency - Return all
		if (getCurrency() == null || this.currencyPositionMap == null) {
			return this.collateralPositionList;
		}
		// Otherwise filter on ccy
		return BeanUtils.filter(this.collateralPositionList, collateralPosition -> collateralPosition.getSecurity().getInstrument().getTradingCurrency(), getCurrency());
	}


	public List<CollateralPosition> getEndingCollateralPositionList() {
		// No Currency - Return all
		if (getCurrency() == null || this.currencyPositionMap == null) {
			return this.endingCollateralPositionList;
		}
		// Otherwise filter on ccy
		return BeanUtils.filter(this.endingCollateralPositionList, collateralPosition -> collateralPosition.getSecurity().getInstrument().getTradingCurrency(), getCurrency());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getCurrency() {
		return this.currency;
	}


	public void setCurrency(InvestmentSecurity currency) {
		this.currency = currency;
	}


	public void setPositionList(List<AccountingPosition> positionList) {
		this.positionList = positionList;
	}


	public void setCollateralPositionList(List<CollateralPosition> collateralPositionList) {
		this.collateralPositionList = collateralPositionList;
	}


	public List<CollateralBalanceDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<CollateralBalanceDetail> detailList) {
		this.detailList = detailList;
	}


	public void setEndingCollateralPositionList(List<CollateralPosition> endingCollateralPositionList) {
		this.endingCollateralPositionList = endingCollateralPositionList;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public boolean isSkipExisting() {
		return this.skipExisting;
	}


	public void setSkipExisting(Boolean skipExisting) {
		this.skipExisting = skipExisting;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(Boolean synchronous) {
		this.synchronous = synchronous;
	}


	public boolean isThrowExceptionOnError() {
		return this.throwExceptionOnError;
	}


	public void setThrowExceptionOnError(boolean throwExceptionOnError) {
		this.throwExceptionOnError = throwExceptionOnError;
	}


	public Integer getAsynchronousRebuildDelay() {
		return this.asynchronousRebuildDelay != null ? this.asynchronousRebuildDelay : 0;
	}


	public void setAsynchronousRebuildDelay(Integer asynchronousRebuildDelay) {
		this.asynchronousRebuildDelay = asynchronousRebuildDelay;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public Short getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(Short collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public String getCollateralTypeName() {
		return this.collateralTypeName;
	}


	public void setCollateralTypeName(String collateralTypeName) {
		this.collateralTypeName = collateralTypeName;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Short getHoldingAccountTypeId() {
		if (this.holdingAccountTypeId == null) {
			if (this.holdingAccount != null) {
				this.holdingAccountTypeId = this.holdingAccount.getType().getId();
			}
		}
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Short[] getExcludeHoldingAccountTypeIds() {
		return this.excludeHoldingAccountTypeIds;
	}


	public void setExcludeHoldingAccountTypeIds(Short[] excludeHoldingAccountTypeIds) {
		this.excludeHoldingAccountTypeIds = excludeHoldingAccountTypeIds;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}


	public BusinessCompany getIssuingCompany() {
		return this.issuingCompany;
	}


	public void setIssuingCompany(BusinessCompany issuingCompany) {
		this.issuingCompany = issuingCompany;
		if (issuingCompany != null) {
			this.issuingCompanyId = issuingCompany.getId();
		}
	}


	public CollateralType getCollateralType() {
		return this.collateralType;
	}


	public void setCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
		if (collateralType != null) {
			this.collateralTypeId = collateralType.getId();
		}
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	@ValueChangingSetter
	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
		if (holdingAccount != null) {
			this.holdingAccountId = holdingAccount.getId();
			this.holdingAccountTypeId = holdingAccount.getType().getId();
		}
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
