package com.clifton.collateral.balance.rule;

import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.beans.BeanUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>CollateralBalanceClientCounterpartyCollateralRuleEvaluator</code> is a rule evaluator that generates a violation if both client
 * and counterparty have posted collateral on the same date.
 *
 * @author jonathanr
 */
public class CollateralBalanceClientCounterpartyCollateralRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {

	private CollateralService collateralService;
	private CalendarBusinessDayService calendarBusinessDayService;


	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance collateralBalance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(collateralBalance.getHoldingInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			AccountingPositionCommand accountingPositionCommand = new AccountingPositionCommand();
			accountingPositionCommand.setHoldingInvestmentAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
			accountingPositionCommand.setTransactionDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(collateralBalance.getBalanceDate()), 1));
			List<CollateralPosition> collateralPositionList = getCollateralService().getCollateralPositionList(accountingPositionCommand);
			if (isClientAndCounterpartyPosted(collateralPositionList)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, collateralBalance.getId()));
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private boolean isClientAndCounterpartyPosted(List<CollateralPosition> collateralPositionList) {
		if (collateralPositionList.size() > 1) {
			boolean ourAccount = false;
			boolean notOurAccount = false;
			for (CollateralPosition collateralPosition : collateralPositionList) {
				if (collateralPosition.getAccountingAccount().isNotOurAccount()) {
					notOurAccount = true;
				}
				else {
					ourAccount = true;
				}
			}
			return ourAccount && notOurAccount;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
