package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralStrategyCoveredMatcher extends AggregateCollateralStrategyOptionMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategyCoveredMatcher coveredMatcher = new CollateralStrategyCoveredMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyCoveredCallPositionList(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		return coveredMatcher.getCollateralStrategyPositionMap(positionInfoList, collateralPositionList, positionDate);
	}


	@Override
	public Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, Date positionDate) {
		PositionMutations positionMutations = new PositionMutations();
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = doGetCollateralStrategyPositionMap(positionInfoList, collateralPositionList, positionMutations);
		// update group IDs
		results.forEach((key, value) -> CollectionUtils.getStream(value).forEach(p -> p.setPositionGroupId(key.getTradeStrategyName())));

		// remove matches from the position list
		List<CollateralOptionsStrategyPosition> removals = results.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		positionInfoList.removeAll(removals);
		return results;
	}


	@Override
	protected Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> doGetCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, PositionMutations positionMutations) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		// are there any collateral positions
		if (!CollectionUtils.isEmpty(collateralPositionList)) {

			// get long positions for physical securities.
			Map<InvestmentSecurity, List<CollateralPosition>> physicalCollateralPositions = CollectionUtils.asNonNullList(collateralPositionList).stream()
					// only physical securities
					.filter(p -> p.getSecurity().getInstrument().getHierarchy().getInvestmentType().isPhysicalSecurity())
					// long positions
					.filter(p -> MathUtils.isPositive(p.getQuantity()))
					.collect(Collectors.groupingBy(CollateralPosition::getSecurity));

			if (!physicalCollateralPositions.isEmpty()) {
				// option, short and underlying equity has a collateral position.
				Map<InvestmentSecurity, List<CollateralOptionsStrategyPosition>> positionMap = CollectionUtils.buildStream(positionInfoList)
						.filter(p -> InvestmentUtils.isOption(p.getInvestmentSecurity()))
						.filter(p -> physicalCollateralPositions.containsKey(p.getInvestmentSecurity().getUnderlyingSecurity()))
						.filter(p -> MathUtils.isNegative(p.getRemainingQuantity()))
						// Group by underlying security.
						.collect(Collectors.groupingBy(p -> p.getInvestmentSecurity().getUnderlyingSecurity()));

				if (!positionMap.isEmpty()) {
					for (Map.Entry<InvestmentSecurity, List<CollateralOptionsStrategyPosition>> entry : positionMap.entrySet()) {
						for (CollateralPosition collateralPosition : physicalCollateralPositions.get(entry.getKey())) {
							// physical position quantities are long (positive); negate for matching negative option quantities.
							CollateralOptionsStrategyPosition matchingPosition = getMatchingPositionList(positionMutations, entry.getValue()
									, MathUtils.divide(collateralPosition.getQuantity(), new BigDecimal("100")).negate());
							if (matchingPosition != null) {
								List<CollateralOptionsStrategyPosition> list = results.computeIfAbsent(CollateralOptionsStrategyCalculators.COVERED_CALL, k -> new ArrayList<>());
								list.add(matchingPosition);
							}
						}
					}
				}
			}
		}

		return results;
	}
}
