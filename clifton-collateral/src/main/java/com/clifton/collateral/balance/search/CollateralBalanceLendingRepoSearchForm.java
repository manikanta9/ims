package com.clifton.collateral.balance.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;


/**
 * The <code>CollateralBalanceRepoDetailSearchForm</code> ...
 *
 * @author manderson
 */
public class CollateralBalanceLendingRepoSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "collateralBalance.id")
	private Integer collateralBalanceId;


	@SearchField(searchField = "repoIdentifier")
	private Integer repoIdentifier;

	@SearchField
	private BigDecimal price;

	@SearchField
	private BigDecimal marketValue;

	@SearchField(searchField = "marketValue,exchangeRateToBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_DIVIDE)
	private BigDecimal marketValueLocal;

	@SearchField
	private BigDecimal netCash;

	@SearchField(searchField = "netCash,exchangeRateToBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_DIVIDE)
	private BigDecimal netCashLocal;


	@SearchField
	private BigDecimal accruedInterest;

	@SearchField(searchField = "accruedInterest,exchangeRateToBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_DIVIDE)
	private BigDecimal accruedInterestLocal;

	@SearchField
	private BigDecimal exchangeRateToBase;

	@SearchField
	private BigDecimal indexRatio;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getCollateralBalanceId() {
		return this.collateralBalanceId;
	}


	public void setCollateralBalanceId(Integer collateralBalanceId) {
		this.collateralBalanceId = collateralBalanceId;
	}


	public Integer getRepoIdentifier() {
		return this.repoIdentifier;
	}


	public void setRepoIdentifier(Integer repoIdentifier) {
		this.repoIdentifier = repoIdentifier;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getMarketValueLocal() {
		return this.marketValueLocal;
	}


	public void setMarketValueLocal(BigDecimal marketValueLocal) {
		this.marketValueLocal = marketValueLocal;
	}


	public BigDecimal getNetCash() {
		return this.netCash;
	}


	public void setNetCash(BigDecimal netCash) {
		this.netCash = netCash;
	}


	public BigDecimal getNetCashLocal() {
		return this.netCashLocal;
	}


	public void setNetCashLocal(BigDecimal netCashLocal) {
		this.netCashLocal = netCashLocal;
	}


	public BigDecimal getAccruedInterest() {
		return this.accruedInterest;
	}


	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}


	public BigDecimal getAccruedInterestLocal() {
		return this.accruedInterestLocal;
	}


	public void setAccruedInterestLocal(BigDecimal accruedInterestLocal) {
		this.accruedInterestLocal = accruedInterestLocal;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}
}
