package com.clifton.collateral.balance.rebuild.processor.command;

import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;

import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralStrategyCommandHandler</code> is used to provide the initialization of a collateral type
 * specific strategy command.  The handler allows for injection of services such as marketDataRetriever that can
 * then be used to compute specific values such as strikePrice that are used in collateral calculations by the
 * <code>CollateralStrategyCalculator</code> implementations
 *
 * @author StevenF
 */
public interface CollateralStrategyCommandHandler<T extends CollateralStrategyCommand<R>, R extends CollateralBalanceRebuildProcessor> {

	public T populateCommand(R rebuildProcessor, List<AccountingPositionInfo> positionInfoList, Date positionDate);


	public CollateralOptionsStrategyPosition createCollateralOptionsStrategyPosition(AccountingPositionInfo positionInfo, Date positionDate);
}
