package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.balance.rebuild.processor.calculator.options.CollateralOptionsStrategyCalculators;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySpreadPosition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
public class CollateralStrategySpreadMatcher extends AggregateCollateralStrategyOptionMatcher<CollateralOptionsStrategyPosition> {

	private static final CollateralStrategySpreadMatcher spreadMatcher = new CollateralStrategySpreadMatcher();


	public static Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> getCollateralStrategySpreadPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, Date positionDate) {
		return spreadMatcher.getCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), positionDate);
	}


	@Override
	protected Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> doGetCollateralStrategyPositionMap(List<CollateralOptionsStrategyPosition> positionInfoList, List<CollateralPosition> collateralPositionList, PositionMutations positionMutations) {
		Map<CollateralOptionsStrategyCalculators, List<CollateralOptionsStrategyPosition>> results = new EnumMap<>(CollateralOptionsStrategyCalculators.class);

		Map<InstrumentSpreadGrouping, Map<HOLDING, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>>> spreadMap = getGroupedPositionsMap(positionInfoList);
		for (Map<HOLDING, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>> candidate : spreadMap.values()) {
			// strangle must have LONG and SHORT of the same instrument put / call and expiry
			if (candidate.containsKey(HOLDING.SHORT) && candidate.containsKey(HOLDING.LONG)) {
				Map<PositionGrouping, BigDecimal> shortQuantities = candidate.get(HOLDING.SHORT).entrySet().stream()
						.collect(Collectors.toMap
								(
										Map.Entry::getKey,
										e -> e.getValue().stream().map(CollateralOptionsStrategyPosition::getRemainingQuantity).reduce(BigDecimal.ZERO, BigDecimal::add)
								)
						);
				Map<PositionGrouping, BigDecimal> longQuantities = candidate.get(HOLDING.LONG).entrySet().stream()
						.collect(Collectors.toMap
								(
										Map.Entry::getKey,
										e -> e.getValue().stream().map(CollateralOptionsStrategyPosition::getRemainingQuantity).reduce(BigDecimal.ZERO, BigDecimal::add)
								)
						);
				final Optional<Map.Entry<PositionGrouping, BigDecimal>> maximumShort = getMaximumPositionGroupingByQuantity(shortQuantities);
				final Optional<Map.Entry<PositionGrouping, BigDecimal>> maximumLong = getMaximumPositionGroupingByQuantity(longQuantities);

				if (maximumShort.isPresent() && maximumLong.isPresent()) {
					int comparison = maximumShort.get().getValue().abs().compareTo(maximumLong.get().getValue().abs());
					CollateralOptionsStrategySpreadPosition spreadPosition;
					if (comparison == 0) {
						// short = longs
						BigDecimal quantity = maximumLong.get().getValue();

						// negate the long quantity when finding the short positions
						CollateralOptionsStrategyPosition shortPosition = getMatchingPositionList(positionMutations, candidate.get(HOLDING.SHORT).get(maximumShort.get().getKey()), quantity.negate());
						AssertUtils.assertNotNull(shortPosition, "Could not determine short position");

						CollateralOptionsStrategyPosition longPosition = getMatchingPositionList(positionMutations, candidate.get(HOLDING.LONG).get(maximumLong.get().getKey()), quantity);
						AssertUtils.assertNotNull(longPosition, "Could not determine long position");

						spreadPosition = new CollateralOptionsStrategySpreadPosition(longPosition, shortPosition);
					}
					else if (comparison > 0) {
						// shorts > longs - longs quantity
						BigDecimal quantity = maximumLong.get().getValue();
						CollateralOptionsStrategyPosition longPosition = getMatchingPositionList(positionMutations, candidate.get(HOLDING.LONG).get(maximumLong.get().getKey()), quantity);
						AssertUtils.assertNotNull(longPosition, "Could not determine long position");

						List<CollateralOptionsStrategyPosition> positionList = candidate.get(HOLDING.SHORT).values().stream()
								.flatMap(List::stream)
								.collect(Collectors.toList());
						CollateralOptionsStrategyPosition shortPosition = getMatchingPositionList(positionMutations, positionList, quantity);
						AssertUtils.assertNotNull(shortPosition, "Could not determine short position");

						spreadPosition = new CollateralOptionsStrategySpreadPosition(longPosition, shortPosition);
					}
					else {
						// longs > shorts - short quantity
						BigDecimal quantity = maximumShort.get().getValue();
						CollateralOptionsStrategyPosition shortPosition = getMatchingPositionList(positionMutations, candidate.get(HOLDING.SHORT).get(maximumShort.get().getKey()), quantity);
						AssertUtils.assertNotNull(shortPosition, "Could not determine short position");

						List<CollateralOptionsStrategyPosition> positionList = candidate.get(HOLDING.LONG).values().stream()
								.flatMap(List::stream)
								.collect(Collectors.toList());
						CollateralOptionsStrategyPosition longPosition = getMatchingPositionList(positionMutations, positionList, quantity);
						AssertUtils.assertNotNull(longPosition, "Could not determine long position");

						spreadPosition = new CollateralOptionsStrategySpreadPosition(longPosition, shortPosition);
					}
					// the types are the same for long / short strategy positions.
					if (InvestmentSecurityOptionTypes.PUT == spreadPosition.getOptionType()) {
						int strikePriceComparison = spreadPosition.getLongStrategyPosition().getStrikePrice().compareTo(spreadPosition.getShortStrategyPosition().getStrikePrice());
						int optionPriceComparison = spreadPosition.getLongStrategyPosition().getOptionPrice().compareTo(spreadPosition.getShortStrategyPosition().getOptionPrice());
						CollateralOptionsStrategyCalculators calculators = (strikePriceComparison < 0 && optionPriceComparison < 0)
								? CollateralOptionsStrategyCalculators.SHORT_PUT_SPREAD : CollateralOptionsStrategyCalculators.LONG_PUT_SPREAD;
						List<CollateralOptionsStrategyPosition> list = results.computeIfAbsent(calculators, k -> new ArrayList<>());
						list.add(spreadPosition);
					}
					else {
						int strikePriceComparison = spreadPosition.getLongStrategyPosition().getStrikePrice().compareTo(spreadPosition.getShortStrategyPosition().getStrikePrice());
						int optionPriceComparison = spreadPosition.getLongStrategyPosition().getOptionPrice().compareTo(spreadPosition.getShortStrategyPosition().getOptionPrice());
						CollateralOptionsStrategyCalculators calculators = (strikePriceComparison < 0 && optionPriceComparison < 0)
								? CollateralOptionsStrategyCalculators.SHORT_CALL_SPREAD : CollateralOptionsStrategyCalculators.LONG_CALL_SPREAD;
						List<CollateralOptionsStrategyPosition> list = results.computeIfAbsent(calculators, k -> new ArrayList<>());
						list.add(spreadPosition);
					}
				}
			}
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<InstrumentSpreadGrouping, Map<HOLDING, Map<PositionGrouping, List<CollateralOptionsStrategyPosition>>>> getGroupedPositionsMap(List<CollateralOptionsStrategyPosition> positionInfoList) {
		return CollectionUtils.buildStream(CollectionUtils.asNonNullList(positionInfoList))
				// include only options
				.filter(p -> InvestmentUtils.isSecurityOfType(p.getInvestmentSecurity(), InvestmentType.OPTIONS))
				.collect(
						// group primarily on instrument PUT CALL and expiry
						Collectors.groupingBy(
								t -> new InstrumentSpreadGrouping(
										t.getInvestmentSecurity().getInstrument().getId(),
										DateUtils.asLocalDate(t.getInvestmentSecurity().getEndDate()),
										TYPE.find(t.getOptionType())
								),
								// group same instruments and expiry further by long / short
								Collectors.groupingBy(
										t -> HOLDING.find(t.getRemainingQuantity()),
										// group same instruments expiry and type further by strike price, option price and underlying price
										Collectors.groupingBy(
												t -> new PositionGrouping(
														t.getStrikePrice(),
														t.getUnderlyingPrice(),
														t.getOptionPrice()
												)
										)
								)
						)
				);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * group by: put / call, instrument and expiry
	 */
	protected static class InstrumentSpreadGrouping {

		private final long instrumentId;
		private final LocalDate expiry;
		private final TYPE type;


		public InstrumentSpreadGrouping(long instrumentId, LocalDate expiry, TYPE type) {
			this.instrumentId = instrumentId;
			this.expiry = expiry;
			this.type = type;
		}


		@SuppressWarnings("unused")
		public long getInstrumentId() {
			return this.instrumentId;
		}


		public LocalDate getExpiry() {
			return this.expiry;
		}


		public TYPE getType() {
			return this.type;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			InstrumentSpreadGrouping that = (InstrumentSpreadGrouping) o;
			return this.instrumentId == that.instrumentId &&
					Objects.equals(this.expiry, that.expiry) &&
					this.type == that.type;
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.instrumentId, this.expiry);
		}


		@Override
		public String toString() {
			return "InstrumentGrouping{" +
					"instrumentId=" + this.instrumentId +
					", expiry=" + this.expiry +
					", type=" + this.type +
					'}';
		}
	}
}
