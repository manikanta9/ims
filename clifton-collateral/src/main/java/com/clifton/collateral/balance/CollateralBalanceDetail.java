package com.clifton.collateral.balance;


import com.clifton.core.beans.IdentityObject;

import java.math.BigDecimal;


/**
 * The <code>CollateralBalanceDetail</code> is an interface that can be implemented to contain the details
 * of the CollateralBalance.
 * <p/>
 * Currently used for REPO Collateral where the REPO Positions are linked to the CollateralBalance along with their breakdown of the balance
 *
 * @author manderson
 */
public interface CollateralBalanceDetail extends IdentityObject {

	public CollateralBalance getCollateralBalance();


	public void setCollateralBalance(CollateralBalance collateralBalance);


	public BigDecimal getAccruedInterest();


	public BigDecimal getMarketValue();


	public BigDecimal getOriginalMarketValue();


	public BigDecimal getNetCash();


	public BigDecimal getOriginalNetCash();
}
