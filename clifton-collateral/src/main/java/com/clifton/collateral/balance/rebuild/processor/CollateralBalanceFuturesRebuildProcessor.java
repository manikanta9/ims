package com.clifton.collateral.balance.rebuild.processor;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>CollateralBalanceFuturesRebuildProcessor</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralBalanceFuturesRebuildProcessor<S extends CollateralStrategyPosition> extends BaseCollateralBalanceRebuildProcessor<S> {

	private boolean manualAccount;
	private BigDecimal minimumCashTransferAmount;
	private BigDecimal cashTransferIncrementAmount;
	private BigDecimal targetCollateralBufferPercent;

	private BigDecimal minimumPositionTransferAmount;
	private BigDecimal minimumTransferQuantity;


	private List<Short> investmentInstrumentHierarchyIds;


	/////////////////////////////////////////////////////////////////
	////////                Rebuild Methods                 /////////
	/////////////////////////////////////////////////////////////////


	@Override
	public void applyCollateralTypeOverrides(CollateralBalance balance) {
		// Non OTC - Set Client Account & Collateral Account where possible & Calculate Required Broker Collateral
		balance.setClientInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), balance.getBalanceDate(), true, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME));
		balance.setCollateralInvestmentAccount(getCollateralBalanceService().getCollateralBalanceRelatedAccountByPurposes(balance.getHoldingInvestmentAccount(), balance.getBalanceDate(), false, false, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME));
	}


	/**
	 * Also sets required collateral
	 */
	@Override
	protected void applyAccountingPositionsToCollateralBalance(CollateralBalance balance, CollateralBalanceRebuildCommand command) {
		// get Positions Market Value
		BigDecimal positionsMarketValue = BigDecimal.ZERO;
		BigDecimal requiredInitial = BigDecimal.ZERO;

		for (AccountingPosition position : CollectionUtils.getIterable(command.getPositionList())) {
			requiredInitial = MathUtils.add(requiredInitial, getAccountingPositionHandler().getAccountingPositionRequiredCollateral(position, balance.getBalanceDate()));

			InvestmentSecurity sec = position.getInvestmentSecurity();
			if (sec.getInstrument().getHierarchy().isCollateralUsed()) {
				BigDecimal marketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(position, balance.getBalanceDate());
				positionsMarketValue = MathUtils.add(positionsMarketValue, marketValue);
			}
		}
		balance.setPositionsMarketValue(positionsMarketValue);
		balance.setCollateralRequirement(requiredInitial);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters And Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isManualAccount() {
		return this.manualAccount;
	}


	public void setManualAccount(boolean manualAccount) {
		this.manualAccount = manualAccount;
	}


	public BigDecimal getMinimumCashTransferAmount() {
		return this.minimumCashTransferAmount;
	}


	public void setMinimumCashTransferAmount(BigDecimal minimumCashTransferAmount) {
		this.minimumCashTransferAmount = minimumCashTransferAmount;
	}


	public BigDecimal getCashTransferIncrementAmount() {
		return this.cashTransferIncrementAmount;
	}


	public void setCashTransferIncrementAmount(BigDecimal cashTransferIncrementAmount) {
		this.cashTransferIncrementAmount = cashTransferIncrementAmount;
	}


	public BigDecimal getTargetCollateralBufferPercent() {
		return this.targetCollateralBufferPercent;
	}


	public void setTargetCollateralBufferPercent(BigDecimal targetCollateralBufferPercent) {
		this.targetCollateralBufferPercent = targetCollateralBufferPercent;
	}


	public BigDecimal getMinimumPositionTransferAmount() {
		return this.minimumPositionTransferAmount;
	}


	public void setMinimumPositionTransferAmount(BigDecimal minimumPositionTransferAmount) {
		this.minimumPositionTransferAmount = minimumPositionTransferAmount;
	}


	public BigDecimal getMinimumTransferQuantity() {
		return this.minimumTransferQuantity;
	}


	public void setMinimumTransferQuantity(BigDecimal minimumTransferQuantity) {
		this.minimumTransferQuantity = minimumTransferQuantity;
	}


	public List<Short> getInvestmentInstrumentHierarchyIds() {
		return this.investmentInstrumentHierarchyIds;
	}


	public void setInvestmentInstrumentHierarchyIds(List<Short> investmentInstrumentHierarchyIds) {
		this.investmentInstrumentHierarchyIds = investmentInstrumentHierarchyIds;
	}
}
