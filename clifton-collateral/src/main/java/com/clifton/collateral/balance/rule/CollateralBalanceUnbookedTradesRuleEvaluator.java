package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.ArrayList;
import java.util.List;


/**
 * This <code>CollateralBalanceUnbookedTradesRuleEvaluator</code> is a rule evaluator that generates a violation when any unbooked Trades are found
 * relating to the holding account of an OTC collateral balance.
 *
 * @author jonathanr
 */
public class CollateralBalanceUnbookedTradesRuleEvaluator extends BaseRuleEvaluator<CollateralBalance, CollateralBalanceRuleEvaluatorContext> {


	private TradeService tradeService;


	@Override
	public List<RuleViolation> evaluateRule(CollateralBalance collateralBalance, RuleConfig ruleConfig, CollateralBalanceRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(collateralBalance.getHoldingInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			TradeSearchForm tradeSearchForm = new TradeSearchForm();
			tradeSearchForm.setHoldingInvestmentAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
			tradeSearchForm.setTradeDateLessThanOrEquals(collateralBalance.getBalanceDate());
			tradeSearchForm.setExcludeWorkflowStateNames((new String[]{"Booked", "Cancelled"}));
			List<Trade> tradeList = getTradeService().getTradeList(tradeSearchForm);
			for (Trade trade : CollectionUtils.getIterable(tradeList)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, collateralBalance.getId(), trade.getId(), null));
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
