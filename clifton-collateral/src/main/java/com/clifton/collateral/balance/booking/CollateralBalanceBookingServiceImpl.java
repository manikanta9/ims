package com.clifton.collateral.balance.booking;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.booking.AccountingPositionTransferBookingProcessor;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.status.RuleViolationStatus;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>CollateralBalanceBookingServiceImpl</code> class provides basic implementation of CollateralBalanceBookingService interface.
 *
 * @author stevenf
 */
@Service
public class CollateralBalanceBookingServiceImpl implements CollateralBalanceBookingService {

	private AccountingBookingService<CollateralBalance> accountingBookingService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private CollateralBalanceService collateralBalanceService;
	private CollateralService collateralService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int bookAndPostCollateralBalanceList(CollateralBalanceSearchForm searchForm) {
		// TODO: Update to keep a status object updated and return it to the screen rather than just a count

		ValidationUtils.assertNotNull(searchForm.getCollateralTypeName(), "Collateral Type is required to perform book and post action.");

		int count = 0;
		//Check if the collateralType supports booking
		if (getCollateralService().getCollateralTypeByName(searchForm.getCollateralTypeName()).isBookable()) {
			// Applies to unbooked balances only
			searchForm.setBooked(false);
			//Only process items without ignored or no violations
			searchForm.setViolationStatusNames(new String[]{RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED.name(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS.name()});

			List<CollateralBalance> balanceList = getCollateralBalanceService().getCollateralBalanceList(searchForm);
			for (CollateralBalance entry : CollectionUtils.getIterable(balanceList)) {
				if (entry.isBookable()) {
					if (bookAndPostCollateralBalance(entry)) {
						count++;
					}
				}
			}
		}
		return count;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean bookAndPostCollateralBalance(CollateralBalance balance) {
		// applies to only those with a non null, non zero transfer amount
		// NOTE: May want to move to generic method of all types will be using this value???
		boolean result = false;
		if (!MathUtils.isNullOrZero(balance.getTransferAmount())) {
			if (getAccountingBookingService().bookAccountingJournal(CollateralBalanceBookingProcessor.JOURNAL_NAME, balance.getId(), true) != null) {
				result = true;
			}
		}
		// book and post any linked transfers
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setSourceFkFieldId(balance.getId());
		searchForm.setSourceSystemTableName(CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME);
		searchForm.setJournalNotBooked(true);
		List<AccountingPositionTransfer> transferList = getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);
		for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(transferList)) {
			if (transfer != null && transfer.getBookingDate() == null) {
				AccountingJournal journal = getAccountingBookingService().bookAccountingJournal(AccountingPositionTransferBookingProcessor.JOURNAL_NAME, transfer.getId(), true);
				result = journal != null;
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<CollateralBalance> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<CollateralBalance> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}
}
