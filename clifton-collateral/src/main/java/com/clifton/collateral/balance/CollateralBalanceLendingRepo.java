
package com.clifton.collateral.balance;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.lending.api.repo.Repo;

import java.math.BigDecimal;


/**
 * The <code>CollateralBalanceRepoDetail</code> defines the REPO detail for a CollateralBalance record
 *
 * @author manderson
 */
public class CollateralBalanceLendingRepo extends BaseSimpleEntity<Integer> implements CollateralBalanceDetail {

	private CollateralBalance collateralBalance;


	private Integer repoIdentifier;

	/**
	 * This object is hydrated separately based on the repoIdentifier value
	 */
	@NonPersistentField
	private Repo repo;


	/**
	 * The following fields are "copies" from the Lending Repo however using
	 * current values - i.e. price on the CollateralBalanceDate, MarketValue of the Repo on the BalanceDate, etc.
	 */
	private BigDecimal price;
	/**
	 * The collateral position market value.
	 * <p/>
	 * NOTE:  For inflation adjusted securities, this value is calculated using the next days index ratio.
	 */
	private BigDecimal marketValue;

	private BigDecimal netCash;

	private BigDecimal accruedInterest;

	private BigDecimal exchangeRateToBase;

	/**
	 * The index ratio for the collateral position.
	 * <p/>
	 * NOTE:  This is next days index ratio.
	 */
	private BigDecimal indexRatio;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Need to keep repoIdentifier and repo in sync
	 * So when setting repo object, make sure repoIdentifier is updated
	 **/
	@ValueChangingSetter
	public void setRepo(Repo repo) {
		this.repo = repo;
		if (!isSameRepoIdentifier(repo, this.repoIdentifier)) {
			this.repoIdentifier = (repo == null ? null : repo.getId());
		}
	}


	public void setRepoIdentifier(Integer repoIdentifier) {
		this.repoIdentifier = repoIdentifier;
		if (!isSameRepoIdentifier(this.repo, repoIdentifier) && this.repo != null) {
			this.repo = null;
		}
	}


	private boolean isSameRepoIdentifier(Repo repo, Integer repoIdentifier) {
		Integer repoId = (repo == null ? null : repo.getId());
		return MathUtils.isEqual(repoId, repoIdentifier);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getOriginalNetCashLocal() {
		if (getRepo() != null) {
			return getRepo().getNetCash();
		}
		return null;
	}


	@Override
	public BigDecimal getOriginalNetCash() {
		return MathUtils.multiply(getExchangeRateToBase(), getOriginalNetCashLocal());
	}


	public BigDecimal getOriginalMarketValueLocal() {
		if (getRepo() != null) {
			return getRepo().getMarketValue();
		}
		return null;
	}


	@Override
	public BigDecimal getOriginalMarketValue() {
		return MathUtils.multiply(getExchangeRateToBase(), getOriginalMarketValueLocal());
	}


	public BigDecimal getMarketValueLocal() {
		return MathUtils.divide(getMarketValue(), getExchangeRateToBase());
	}


	public BigDecimal getNetCashLocal() {
		return MathUtils.divide(getNetCash(), getExchangeRateToBase());
	}


	public BigDecimal getRepoInterestAmount() {
		if (getRepo() != null) {
			return MathUtils.multiply(getRepo().getInterestAmount(), getExchangeRateToBase());
		}
		return null;
	}


	public BigDecimal getAccruedInterestLocal() {
		return MathUtils.divide(getAccruedInterest(), getExchangeRateToBase());
	}


	public BigDecimal getExposure() {
		return MathUtils.subtract(MathUtils.subtract(getNetCash(), getAccruedInterest()), getOriginalNetCash());
	}


	public BigDecimal getExposureLocal() {
		return MathUtils.divide(getExposure(), getExchangeRateToBase());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralBalance getCollateralBalance() {
		return this.collateralBalance;
	}


	@Override
	public void setCollateralBalance(CollateralBalance collateralBalance) {
		this.collateralBalance = collateralBalance;
	}


	public Integer getRepoIdentifier() {
		return this.repoIdentifier;
	}


	public Repo getRepo() {
		return this.repo;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	@Override
	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	@Override
	public BigDecimal getNetCash() {
		return this.netCash;
	}


	public void setNetCash(BigDecimal netCash) {
		this.netCash = netCash;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	@Override
	public BigDecimal getAccruedInterest() {
		return this.accruedInterest;
	}


	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}
}
