package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * @author StevenF
 */
public class CollateralOptionsStrategyZeroCalculator implements CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> {

	@Override
	public Map<Long, BigDecimal> calculateCollateral(CollateralOptionsStrategyCommand command) {
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();
		for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(command.getPositionList())) {
			collateralRequirementMap.put(strategyPosition.getAccountingTransactionId(), calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition));
		}
		return collateralRequirementMap;
	}


	@Override
	public BigDecimal calculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition) {
		return BigDecimal.ZERO;
	}


	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		BigDecimal requiredCollateral = calculateCollateral(processor, strategyPosition);
		strategyPosition.setRequiredCollateral(requiredCollateral);
		if (showExplanation) {
			strategyPosition.appendToExplanation(() -> "Zero Calculator");
		}
	}
}
