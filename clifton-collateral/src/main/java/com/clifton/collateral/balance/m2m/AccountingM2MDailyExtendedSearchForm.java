package com.clifton.collateral.balance.m2m;


import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;


/**
 * The <code>AccountingM2MDailySearchForm</code> class defines search configuration for AccountingM2MDaily objects.
 *
 * @author vgomelsky
 */
public class AccountingM2MDailyExtendedSearchForm extends AccountingM2MDailySearchForm {

	/**
	 * The accountingM2MDaily used to lookup the holding account to filter.
	 */
	// Custom Search Filter
	private Integer accountingM2MDailyId;


	public Integer getAccountingM2MDailyId() {
		return this.accountingM2MDailyId;
	}


	public void setAccountingM2MDailyId(Integer accountingM2MDailyId) {
		this.accountingM2MDailyId = accountingM2MDailyId;
	}
}
