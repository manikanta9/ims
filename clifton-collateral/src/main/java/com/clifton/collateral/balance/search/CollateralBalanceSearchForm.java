package com.clifton.collateral.balance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>CollateralBalanceSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralBalanceSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	/**
	 * Select all where id = summaryId or parent.id = summaryId
	 */
	// Custom Search Field
	private Integer summaryId;

	/**
	 * Select all where the holding account is in the provided group or groups.
	 */
	// Custom Search Field
	private Integer accountGroupId;
	private Integer[] accountGroupIds;

	/**
	 * Select all where the holding account is NOT in the provided group or groups.
	 */
	// Custom Search Field
	private Integer excludeAccountGroupId;
	private Integer[] excludeAccountGroupIds;


	/**
	 * Select all with specified holding account group alias.
	 */
	// Custom Search Field
	private String accountGroupAlias;

	// Custom Search Field
	private String holdingInvestmentAccountLabelOrGroupAlias;

	/**
	 * The resulting list will have the holdingInvestmentAccountGroup property populated.
	 */
	// Custom Search Field
	private Boolean populateHoldingInvestmentAccountGroup;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean parentIsNull;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean parentIsNotNull;

	@SearchField
	private Boolean parentCollateralBalance;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "businessClient.id", sortField = "businessClient.name")
	private Integer businessClientId;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer issuingCompanyId;

	@SearchField(searchFieldPath = "holdingInvestmentAccount.issuingCompany", searchField = "name")
	private String issuingCompanyName;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchField = "number", searchFieldPath = "holdingInvestmentAccount")
	private String holdingInvestmentAccountNumber;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "type.id", sortField = "type.name")
	private Short holdingInvestmentAccountTypeId;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "number,name", sortField = "number")
	private String holdingInvestmentAccountLabel;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "businessContract.id", sortField = "name")
	private Integer businessContractId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchFieldPath = "clientInvestmentAccount", searchField = "number,name", sortField = "number")
	private String clientInvestmentAccountLabel;

	@SearchField(searchFieldPath = "collateralInvestmentAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer collateralCompanyId;

	@SearchField(searchFieldPath = "collateralInvestmentAccount.issuingCompany", searchField = "name")
	private String collateralCompanyName;

	@SearchField(searchField = "collateralInvestmentAccount.id", sortField = "collateralInvestmentAccount.number")
	private Integer collateralInvestmentAccountId;

	@SearchField(searchFieldPath = "collateralInvestmentAccount", searchField = "number,name", sortField = "number")
	private String collateralInvestmentAccountLabel;

	@SearchField(searchFieldPath = "collateralInvestmentAccount", searchField = "type.id", sortField = "type.name")
	private Short collateralInvestmentAccountTypeId;

	@SearchField(searchField = "collateralCurrency.id", sortField = "collateralCurrency.symbol")
	private Integer collateralCurrencyId;

	// Custom Search Filter - If no currency on Collateral record, uses Holding Account's Base CCY
	private Integer coalesceCollateralHoldingAccountCurrencyId;

	@SearchField
	private BigDecimal transferAmount;

	@SearchField
	private Date balanceDate;

	@SearchField(searchField = "collateralType.id")
	private Short collateralTypeId;

	@SearchField(searchField = "collateralType.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] collateralTypeIds;

	@SearchField(searchFieldPath = "collateralType", searchField = "name")
	private String collateralTypeName;

	// custom restriction: collateral type allows booking and parent is not set
	private Boolean bookable;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean booked;

	@SearchField
	private Date bookingDate;

	@SearchField
	private BigDecimal positionsMarketValue;
	@SearchField
	private BigDecimal postedCollateralValue;
	@SearchField
	private BigDecimal postedCounterpartyCollateralValue;

	@SearchField
	private BigDecimal postedCollateralHaircutValue;
	@SearchField
	private BigDecimal postedCounterpartyCollateralHaircutValue;

	// Custom Search Field (postedCounterpartCollateralHaircutValue - collateralRequirement)
	private BigDecimal excessCounterpartyCollateral;

	// Custom Search Field (externalCollateralAmount - externalCollateralRequirement)
	private BigDecimal externalExcessCollateral;

	@SearchField
	private BigDecimal thresholdAmount;
	@SearchField
	private BigDecimal thresholdCounterpartyAmount;
	@SearchField
	private BigDecimal minTransferAmount;
	@SearchField
	private BigDecimal minTransferCounterpartyAmount;
	@SearchField
	private BigDecimal collateralChangeAmount;
	@SearchField
	private BigDecimal counterpartyCollateralChangeAmount;

	@SearchField
	private BigDecimal counterpartyCollateralCallAmount;

	@SearchField
	private BigDecimal externalCollateralAmount;
	@SearchField
	private BigDecimal collateralRequirement;

	@SearchField
	private BigDecimal externalCollateralRequirement;

	// custom search filter
	private Boolean holdingAccountIsHold;

	@SearchField(searchField = "violationStatus.id")
	private Short violationStatusId;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;


	/**
	 * Custom search filter that will exclude accounts with 0 both Client and Counterparty collateral change amount.
	 */
	private Boolean excludeAccountsWithoutCollateralChange;

	/**
	 * Custom search filter that will exclude accounts with 0 for market value, client/counterparty posted collateral and client/counterparty collateral change.
	 */
	private Boolean excludeAccountsWithoutPositions;

	/**
	 * Custom search filter that works similar to excludeAccountsWithoutPositions but also checks Required Collateral Values
	 */
	private Boolean excludeAccountsWithoutPositionsAndNoRequiredCollateral;

	// Custom Search Filter
	//This is used to include accounts in groups for collateral balance rebuild when the account is of a type
	// that is not mapped to by the CollateralTypes (e.g. an account group that has an OTC_ISDA and a Custodian account.
	// Only the OTC_ISDA account is found when searching by collateralType.holdingInvestmentAccountTypeID.
	private String investmentAccountGroupTypeName;


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSummaryId() {
		return this.summaryId;
	}


	public void setSummaryId(Integer summaryId) {
		this.summaryId = summaryId;
	}


	public Integer getAccountGroupId() {
		return this.accountGroupId;
	}


	public void setAccountGroupId(Integer accountGroupId) {
		this.accountGroupId = accountGroupId;
	}


	public Integer[] getAccountGroupIds() {
		return this.accountGroupIds;
	}


	public void setAccountGroupIds(Integer[] accountGroupIds) {
		this.accountGroupIds = accountGroupIds;
	}


	public Integer getExcludeAccountGroupId() {
		return this.excludeAccountGroupId;
	}


	public void setExcludeAccountGroupId(Integer excludeAccountGroupId) {
		this.excludeAccountGroupId = excludeAccountGroupId;
	}


	public Integer[] getExcludeAccountGroupIds() {
		return this.excludeAccountGroupIds;
	}


	public void setExcludeAccountGroupIds(Integer[] excludeAccountGroupIds) {
		this.excludeAccountGroupIds = excludeAccountGroupIds;
	}


	public String getAccountGroupAlias() {
		return this.accountGroupAlias;
	}


	public void setAccountGroupAlias(String accountGroupAlias) {
		this.accountGroupAlias = accountGroupAlias;
	}


	public String getHoldingInvestmentAccountLabelOrGroupAlias() {
		return this.holdingInvestmentAccountLabelOrGroupAlias;
	}


	public void setHoldingInvestmentAccountLabelOrGroupAlias(String holdingInvestmentAccountLabelOrGroupAlias) {
		this.holdingInvestmentAccountLabelOrGroupAlias = holdingInvestmentAccountLabelOrGroupAlias;
	}


	public Boolean getPopulateHoldingInvestmentAccountGroup() {
		return this.populateHoldingInvestmentAccountGroup;
	}


	public void setPopulateHoldingInvestmentAccountGroup(Boolean populateHoldingInvestmentAccountGroup) {
		this.populateHoldingInvestmentAccountGroup = populateHoldingInvestmentAccountGroup;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Boolean getParentIsNull() {
		return this.parentIsNull;
	}


	public void setParentIsNull(Boolean parentIsNull) {
		this.parentIsNull = parentIsNull;
	}


	public Boolean getParentIsNotNull() {
		return this.parentIsNotNull;
	}


	public void setParentIsNotNull(Boolean parentIsNotNull) {
		this.parentIsNotNull = parentIsNotNull;
	}


	public Boolean getParentCollateralBalance() {
		return this.parentCollateralBalance;
	}


	public void setParentCollateralBalance(Boolean parentCollateralBalance) {
		this.parentCollateralBalance = parentCollateralBalance;
	}


	public Boolean getBookable() {
		return this.bookable;
	}


	public void setBookable(Boolean bookable) {
		this.bookable = bookable;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}


	public String getIssuingCompanyName() {
		return this.issuingCompanyName;
	}


	public void setIssuingCompanyName(String issuingCompanyName) {
		this.issuingCompanyName = issuingCompanyName;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public Short getHoldingInvestmentAccountTypeId() {
		return this.holdingInvestmentAccountTypeId;
	}


	public void setHoldingInvestmentAccountTypeId(Short holdingInvestmentAccountTypeId) {
		this.holdingInvestmentAccountTypeId = holdingInvestmentAccountTypeId;
	}


	public String getHoldingInvestmentAccountLabel() {
		return this.holdingInvestmentAccountLabel;
	}


	public void setHoldingInvestmentAccountLabel(String holdingInvestmentAccountLabel) {
		this.holdingInvestmentAccountLabel = holdingInvestmentAccountLabel;
	}


	public Integer getBusinessContractId() {
		return this.businessContractId;
	}


	public void setBusinessContractId(Integer businessContractId) {
		this.businessContractId = businessContractId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public String getClientInvestmentAccountLabel() {
		return this.clientInvestmentAccountLabel;
	}


	public void setClientInvestmentAccountLabel(String clientInvestmentAccountLabel) {
		this.clientInvestmentAccountLabel = clientInvestmentAccountLabel;
	}


	public Integer getCollateralCompanyId() {
		return this.collateralCompanyId;
	}


	public void setCollateralCompanyId(Integer collateralCompanyId) {
		this.collateralCompanyId = collateralCompanyId;
	}


	public String getCollateralCompanyName() {
		return this.collateralCompanyName;
	}


	public void setCollateralCompanyName(String collateralCompanyName) {
		this.collateralCompanyName = collateralCompanyName;
	}


	public Integer getCollateralInvestmentAccountId() {
		return this.collateralInvestmentAccountId;
	}


	public void setCollateralInvestmentAccountId(Integer collateralInvestmentAccountId) {
		this.collateralInvestmentAccountId = collateralInvestmentAccountId;
	}


	public String getCollateralInvestmentAccountLabel() {
		return this.collateralInvestmentAccountLabel;
	}


	public void setCollateralInvestmentAccountLabel(String collateralInvestmentAccountLabel) {
		this.collateralInvestmentAccountLabel = collateralInvestmentAccountLabel;
	}


	public Short getCollateralInvestmentAccountTypeId() {
		return this.collateralInvestmentAccountTypeId;
	}


	public void setCollateralInvestmentAccountTypeId(Short collateralInvestmentAccountTypeId) {
		this.collateralInvestmentAccountTypeId = collateralInvestmentAccountTypeId;
	}


	public Integer getCollateralCurrencyId() {
		return this.collateralCurrencyId;
	}


	public void setCollateralCurrencyId(Integer collateralCurrencyId) {
		this.collateralCurrencyId = collateralCurrencyId;
	}


	public Integer getCoalesceCollateralHoldingAccountCurrencyId() {
		return this.coalesceCollateralHoldingAccountCurrencyId;
	}


	public void setCoalesceCollateralHoldingAccountCurrencyId(Integer coalesceCollateralHoldingAccountCurrencyId) {
		this.coalesceCollateralHoldingAccountCurrencyId = coalesceCollateralHoldingAccountCurrencyId;
	}


	public BigDecimal getTransferAmount() {
		return this.transferAmount;
	}


	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Short getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(Short collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public Short[] getCollateralTypeIds() {
		return this.collateralTypeIds;
	}


	public void setCollateralTypeIds(Short[] collateralTypeIds) {
		this.collateralTypeIds = collateralTypeIds;
	}


	public String getCollateralTypeName() {
		return this.collateralTypeName;
	}


	public void setCollateralTypeName(String collateralTypeName) {
		this.collateralTypeName = collateralTypeName;
	}


	public Boolean getBooked() {
		return this.booked;
	}


	public void setBooked(Boolean booked) {
		this.booked = booked;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public BigDecimal getPositionsMarketValue() {
		return this.positionsMarketValue;
	}


	public void setPositionsMarketValue(BigDecimal positionsMarketValue) {
		this.positionsMarketValue = positionsMarketValue;
	}


	public BigDecimal getPostedCollateralValue() {
		return this.postedCollateralValue;
	}


	public void setPostedCollateralValue(BigDecimal postedCollateralValue) {
		this.postedCollateralValue = postedCollateralValue;
	}


	public BigDecimal getPostedCounterpartyCollateralValue() {
		return this.postedCounterpartyCollateralValue;
	}


	public void setPostedCounterpartyCollateralValue(BigDecimal postedCounterpartyCollateralValue) {
		this.postedCounterpartyCollateralValue = postedCounterpartyCollateralValue;
	}


	public BigDecimal getPostedCollateralHaircutValue() {
		return this.postedCollateralHaircutValue;
	}


	public void setPostedCollateralHaircutValue(BigDecimal postedCollateralHaircutValue) {
		this.postedCollateralHaircutValue = postedCollateralHaircutValue;
	}


	public BigDecimal getPostedCounterpartyCollateralHaircutValue() {
		return this.postedCounterpartyCollateralHaircutValue;
	}


	public void setPostedCounterpartyCollateralHaircutValue(BigDecimal postedCounterpartyCollateralHaircutValue) {
		this.postedCounterpartyCollateralHaircutValue = postedCounterpartyCollateralHaircutValue;
	}


	public BigDecimal getExcessCounterpartyCollateral() {
		return this.excessCounterpartyCollateral;
	}


	public void setExcessCounterpartyCollateral(BigDecimal excessCounterpartyCollateral) {
		this.excessCounterpartyCollateral = excessCounterpartyCollateral;
	}


	public BigDecimal getExternalExcessCollateral() {
		return this.externalExcessCollateral;
	}


	public void setExternalExcessCollateral(BigDecimal externalExcessCollateral) {
		this.externalExcessCollateral = externalExcessCollateral;
	}


	public BigDecimal getThresholdAmount() {
		return this.thresholdAmount;
	}


	public void setThresholdAmount(BigDecimal thresholdAmount) {
		this.thresholdAmount = thresholdAmount;
	}


	public BigDecimal getThresholdCounterpartyAmount() {
		return this.thresholdCounterpartyAmount;
	}


	public void setThresholdCounterpartyAmount(BigDecimal thresholdCounterpartyAmount) {
		this.thresholdCounterpartyAmount = thresholdCounterpartyAmount;
	}


	public BigDecimal getMinTransferAmount() {
		return this.minTransferAmount;
	}


	public void setMinTransferAmount(BigDecimal minTransferAmount) {
		this.minTransferAmount = minTransferAmount;
	}


	public BigDecimal getMinTransferCounterpartyAmount() {
		return this.minTransferCounterpartyAmount;
	}


	public void setMinTransferCounterpartyAmount(BigDecimal minTransferCounterpartyAmount) {
		this.minTransferCounterpartyAmount = minTransferCounterpartyAmount;
	}


	public BigDecimal getCollateralChangeAmount() {
		return this.collateralChangeAmount;
	}


	public void setCollateralChangeAmount(BigDecimal collateralChangeAmount) {
		this.collateralChangeAmount = collateralChangeAmount;
	}


	public BigDecimal getCounterpartyCollateralChangeAmount() {
		return this.counterpartyCollateralChangeAmount;
	}


	public void setCounterpartyCollateralChangeAmount(BigDecimal counterpartyCollateralChangeAmount) {
		this.counterpartyCollateralChangeAmount = counterpartyCollateralChangeAmount;
	}


	public BigDecimal getCounterpartyCollateralCallAmount() {
		return this.counterpartyCollateralCallAmount;
	}


	public void setCounterpartyCollateralCallAmount(BigDecimal counterpartyCollateralCallAmount) {
		this.counterpartyCollateralCallAmount = counterpartyCollateralCallAmount;
	}


	public BigDecimal getExternalCollateralAmount() {
		return this.externalCollateralAmount;
	}


	public void setExternalCollateralAmount(BigDecimal externalCollateralAmount) {
		this.externalCollateralAmount = externalCollateralAmount;
	}


	public BigDecimal getCollateralRequirement() {
		return this.collateralRequirement;
	}


	public void setCollateralRequirement(BigDecimal collateralRequirement) {
		this.collateralRequirement = collateralRequirement;
	}


	public BigDecimal getExternalCollateralRequirement() {
		return this.externalCollateralRequirement;
	}


	public void setExternalCollateralRequirement(BigDecimal externalCollateralRequirement) {
		this.externalCollateralRequirement = externalCollateralRequirement;
	}


	public Boolean getHoldingAccountIsHold() {
		return this.holdingAccountIsHold;
	}


	public void setHoldingAccountIsHold(Boolean holdingAccountIsHold) {
		this.holdingAccountIsHold = holdingAccountIsHold;
	}


	public Short getViolationStatusId() {
		return this.violationStatusId;
	}


	public void setViolationStatusId(Short violationStatusId) {
		this.violationStatusId = violationStatusId;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public Boolean getExcludeAccountsWithoutCollateralChange() {
		return this.excludeAccountsWithoutCollateralChange;
	}


	public void setExcludeAccountsWithoutCollateralChange(Boolean excludeAccountsWithoutCollateralChange) {
		this.excludeAccountsWithoutCollateralChange = excludeAccountsWithoutCollateralChange;
	}


	public Boolean getExcludeAccountsWithoutPositions() {
		return this.excludeAccountsWithoutPositions;
	}


	public void setExcludeAccountsWithoutPositions(Boolean excludeAccountsWithoutPositions) {
		this.excludeAccountsWithoutPositions = excludeAccountsWithoutPositions;
	}


	public Boolean getExcludeAccountsWithoutPositionsAndNoRequiredCollateral() {
		return this.excludeAccountsWithoutPositionsAndNoRequiredCollateral;
	}


	public void setExcludeAccountsWithoutPositionsAndNoRequiredCollateral(Boolean excludeAccountsWithoutPositionsAndNoRequiredCollateral) {
		this.excludeAccountsWithoutPositionsAndNoRequiredCollateral = excludeAccountsWithoutPositionsAndNoRequiredCollateral;
	}


	public String getInvestmentAccountGroupTypeName() {
		return this.investmentAccountGroupTypeName;
	}


	public void setInvestmentAccountGroupTypeName(String investmentAccountGroupTypeName) {
		this.investmentAccountGroupTypeName = investmentAccountGroupTypeName;
	}
}
