package com.clifton.collateral.balance.m2m;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;


/**
 * The <code>AccountingM2MDaily</code> class represents daily Mark To Market by holding account.
 * All amounts are in holding account's base currency.
 *
 * @author vgomelsky
 */
@NonPersistentObject
public class AccountingM2MDailyExtended extends AccountingM2MDaily {

	private BigDecimal collateralTransferAmount;


	public BigDecimal getCollateralTransferAmount() {
		return this.collateralTransferAmount;
	}


	public void setCollateralTransferAmount(BigDecimal collateralTransferAmount) {
		this.collateralTransferAmount = collateralTransferAmount;
	}
}
