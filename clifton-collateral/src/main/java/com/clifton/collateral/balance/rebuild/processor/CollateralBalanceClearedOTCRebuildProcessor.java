package com.clifton.collateral.balance.rebuild.processor;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;


/**
 * Rebuild Processor for Cleared OTC Balances. Extends the Futures Rebuild Processor because at this time we are treating Futures and Cleared OTC the same.
 *
 * @author theodorez
 */
public class CollateralBalanceClearedOTCRebuildProcessor<S extends CollateralStrategyPosition> extends CollateralBalanceFuturesRebuildProcessor<S> {

	@Override
	protected void applyAccountingPositionsToCollateralBalance(CollateralBalance balance, CollateralBalanceRebuildCommand command) {
		super.applyAccountingPositionsToCollateralBalance(balance, command);
		balance.setCollateralRequirement(balance.getExternalCollateralRequirement());
	}
}
