package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;


/**
 * @author mwacker
 */
public interface CollateralBalanceRuleEvaluatorHandler {

	/**
	 * Execute any collateral balance rules for the specific balance object.
	 * <p>
	 * NOTE: This is executed by 'CollateralBalanceObserver' class in method 'beforeMethodCallImpl'
	 */
	public void executeCollateralBalanceRules(CollateralBalance balance);
}
