package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.BaseCollateralStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyStranglePosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;


/*
 *
 * Short Strangle (Equity)
 * Initial margin requirement:
 * short call(s) or short put(s) requirement, whichever is greater, plus the option proceeds of the other side
 * proceeds from sale of entire strangle may be applied to initial margin requirement
 * after position is established, ongoing maintenance margin requirement applies, and an increase (or decrease) in the margin required is possible
 *
 * Short Strangle (Index)
 * Initial margin requirement:
 * short call(s) or short put(s) requirement, whichever is greater, plus the option proceeds of the other side
 * proceeds from sale of entire strangle may be applied to initial margin requirement
 * after position is established, ongoing maintenance margin requirement applies, and an increase (or decrease) in the margin required is possible
 *<p>
 *
 * @author StevenF
 */
public class CollateralOptionsStrategyShortStrangleCalculator extends CollateralOptionsStrategyShortCallCalculator {

	@Override
	public Map<Long, BigDecimal> calculateCollateral(CollateralOptionsStrategyCommand command) {
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();
		Map<String, List<CollateralOptionsStrategyPosition>> stranglePositionMap = BeanUtils.getBeansMap(command.getPositionList(), BaseCollateralStrategyPosition::getPositionGroupId);
		for (List<CollateralOptionsStrategyPosition> strategyPositions : stranglePositionMap.values()) {
			BigDecimal callRequirement = null;
			BigDecimal putRequirement = null;
			CollateralOptionsStrategyPosition callStrategyPosition = null;
			CollateralOptionsStrategyPosition putStrategyPosition = null;
			//If both positions have the same quantity, then process normally
			if (strategyPositions.stream().map(CollateralOptionsStrategyPosition::getRemainingQuantity).distinct().count() == 1) {
				for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(strategyPositions)) {
					if (InvestmentSecurityOptionTypes.PUT == strategyPosition.getOptionType()) {
						putRequirement = CollateralOptionsStrategyCalculators.SHORT_PUT.getTradeStrategyCalculator().calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition);
						putRequirement = MathUtils.add(putRequirement, strategyPosition.getOptionProceeds());
						putStrategyPosition = strategyPosition;
						collateralRequirementMap.put(putStrategyPosition.getAccountingTransactionId(), putRequirement);
					}
					else {
						callRequirement = CollateralOptionsStrategyCalculators.SHORT_CALL.getTradeStrategyCalculator().calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition);
						callRequirement = MathUtils.add(callRequirement, strategyPosition.getOptionProceeds());
						callStrategyPosition = strategyPosition;
						collateralRequirementMap.put(callStrategyPosition.getAccountingTransactionId(), callRequirement);
					}
				}
				if (putStrategyPosition != null && MathUtils.isGreaterThan(callRequirement, putRequirement)) {
					collateralRequirementMap.put(putStrategyPosition.getAccountingTransactionId(), putStrategyPosition.getOptionProceeds());
				}
				else if (callStrategyPosition != null) {
					collateralRequirementMap.put(callStrategyPosition.getAccountingTransactionId(), callStrategyPosition.getOptionProceeds());
				}
			}
			else {
				//Find the 'matching' quantity value that should be used for computing the positions as a strangle.
				BigDecimal strategyQuantity = CollectionUtils.getFirstElementStrict(strategyPositions).getRemainingQuantity();
				for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(strategyPositions)) {
					if (MathUtils.isGreaterThan(MathUtils.abs(strategyQuantity), MathUtils.abs(strategyPosition.getRemainingQuantity()))) {
						strategyQuantity = strategyPosition.getRemainingQuantity();
					}
				}
				//Now find the position with the quantity that does not match the strategy quantity and create a new position
				//with the quantity difference and update the original position so it can be computed as a strangle with the other.
				CollateralOptionsStrategyPosition mismatchedPosition = null;
				for (int i = 0; i < CollectionUtils.getSize(strategyPositions); i++) {
					CollateralOptionsStrategyPosition strategyPosition = strategyPositions.get(i);
					if (MathUtils.isLessThan(MathUtils.abs(strategyQuantity), MathUtils.abs(strategyPosition.getRemainingQuantity()))) {
						BigDecimal mismatchedPositionQuantity = MathUtils.subtract(strategyPosition.getRemainingQuantity(), strategyQuantity);
						mismatchedPosition = new CollateralOptionsStrategyPosition(strategyPosition, mismatchedPositionQuantity);
						CollateralOptionsStrategyPosition updatedStrategyPosition = new CollateralOptionsStrategyPosition(strategyPosition, strategyQuantity);
						strategyPositions.set(i, updatedStrategyPosition);
					}
				}
				if (mismatchedPosition != null) {
					//Compute the strangle positions
					CollateralOptionsStrategyCommand mismatchedQuantityCommand = new CollateralOptionsStrategyCommand();
					mismatchedQuantityCommand.setCollateralBalanceRebuildProcessor(command.getCollateralBalanceRebuildProcessor());
					mismatchedQuantityCommand.setPositionList(strategyPositions);
					collateralRequirementMap.putAll(calculateCollateral(mismatchedQuantityCommand));
					//Now compute the mismatched quantity position separately and add to the collateral of the other part of it's
					//quantity that was computed as a strangle.
					BigDecimal requiredCollateral;
					if (InvestmentSecurityOptionTypes.PUT == mismatchedPosition.getOptionType()) {
						requiredCollateral = CollateralOptionsStrategyCalculators.SHORT_PUT.getTradeStrategyCalculator().calculateCollateral(command.getCollateralBalanceRebuildProcessor(), mismatchedPosition);
					}
					else {
						requiredCollateral = CollateralOptionsStrategyCalculators.SHORT_CALL.getTradeStrategyCalculator().calculateCollateral(command.getCollateralBalanceRebuildProcessor(), mismatchedPosition);
					}
					Long transactionId = mismatchedPosition.getAccountingTransactionId();
					collateralRequirementMap.put(transactionId, MathUtils.add(collateralRequirementMap.get(transactionId), requiredCollateral));
				}
			}
		}
		return collateralRequirementMap;
	}


	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal amount = doPopulateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(amount);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal doPopulateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getPutRequirementType())
				&& strategyPosition instanceof CollateralOptionsStrategyStranglePosition) {
			CollateralOptionsStrategyStranglePosition stranglePosition = (CollateralOptionsStrategyStranglePosition) strategyPosition;

			BigDecimal callAmount = MathUtils.add(CollateralOptionsStrategyCalculators.SHORT_CALL.getTradeStrategyCalculator().calculateCollateral(processor, stranglePosition.getCallStrategyPosition())
					, stranglePosition.getCallStrategyPosition().getOptionProceeds());
			BigDecimal putAmount = MathUtils.add(CollateralOptionsStrategyCalculators.SHORT_PUT.getTradeStrategyCalculator().calculateCollateral(processor, stranglePosition.getPutStrategyPosition())
					, stranglePosition.getPutStrategyPosition().getOptionProceeds());

			appender.accept(() -> "Short Strangle");
			if (callAmount.compareTo(putAmount) >= 0) {
				BigDecimal putProceeds = stranglePosition.getPutStrategyPosition().getOptionProceeds();
				BigDecimal callProceeds = stranglePosition.getCallStrategyPosition().getOptionProceeds();
				BigDecimal marginAmount = callAmount.subtract(callProceeds);

				appender.accept(() -> "Margin requirement for short calls is greater than and applies");
				appender.accept(() -> String.format("[%s] > [%s]", CoreMathUtils.formatNumber(callAmount, null), CoreMathUtils.formatNumber(putAmount, null)));
				appender.accept(() -> String.format("Proceeds from sale of short puts are added:  [%s]", CoreMathUtils.formatNumber(putProceeds, null)));
				appender.accept(() -> String.format("Proceeds from sale of short calls are subtracted:  [%s]", CoreMathUtils.formatNumber(callProceeds, null)));
				appender.accept(() -> "Positions:");
				appender.accept(strategyPosition::buildPositionExplanation);
				appender.accept(() -> "Initial Margin");
				appender.accept(() -> String.format("Margin Requirement for short put(s) [%s]", CoreMathUtils.formatNumber(marginAmount.add(putProceeds), null)));
				appender.accept(() -> String.format("Proceeds from sale of short put(s) [%s]", CoreMathUtils.formatNumber(putProceeds, null)));
				appender.accept(() -> String.format("Margin Call:  [%s]", CoreMathUtils.formatNumber(marginAmount, null)));

				// distribute strangle total to individual constituent put and call
				stranglePosition.getPutStrategyPosition().setRequiredCollateral(putProceeds.negate());
				stranglePosition.getCallStrategyPosition().setRequiredCollateral(callAmount.subtract(callProceeds).add(putProceeds));

				return InvestmentCalculatorUtils.roundLocalAmount(marginAmount, strategyPosition.getInvestmentSecurity());
			}
			else {
				BigDecimal callProceeds = stranglePosition.getCallStrategyPosition().getOptionProceeds();
				BigDecimal putProceeds = stranglePosition.getPutStrategyPosition().getOptionProceeds();
				BigDecimal marginAmount = putAmount.subtract(putProceeds);

				appender.accept(() -> "Margin requirement for short puts is greater than and applies");
				appender.accept(() -> String.format("[%s] > [%s]", CoreMathUtils.formatNumber(putAmount, null), CoreMathUtils.formatNumber(callAmount, null)));
				appender.accept(() -> String.format("Proceeds from sale of short calls are added:  [%s]", CoreMathUtils.formatNumber(callProceeds, null)));
				appender.accept(() -> String.format("Proceeds from sale of short puts are subtracted:  [%s]", CoreMathUtils.formatNumber(putProceeds, null)));
				appender.accept(() -> "Positions:");
				appender.accept(strategyPosition::buildPositionExplanation);
				appender.accept(() -> "Initial Margin");
				appender.accept(() -> String.format("Margin Requirement for short call(s) [%s]", CoreMathUtils.formatNumber(marginAmount.add(callProceeds), null)));
				appender.accept(() -> String.format("Proceeds from sale of short call(s) [%s]", CoreMathUtils.formatNumber(callProceeds, null)));
				appender.accept(() -> String.format("Margin Call:  [%s]", CoreMathUtils.formatNumber(marginAmount, null)));

				// distribute strangle total to individual constituent put and call
				stranglePosition.getPutStrategyPosition().setRequiredCollateral(putAmount.subtract(putProceeds).add(callProceeds));
				stranglePosition.getCallStrategyPosition().setRequiredCollateral(callProceeds.negate());

				return InvestmentCalculatorUtils.roundLocalAmount(marginAmount, strategyPosition.getInvestmentSecurity());
			}
		}
		return BigDecimal.ZERO;
	}
}

