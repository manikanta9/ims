package com.clifton.collateral.balance.rebuild.processor.position.options;

import java.util.List;


/**
 * @author terrys
 */
public interface CollateralOptionsStrategyAggregatePosition {

	public List<CollateralOptionsStrategyPosition> getCompositeStrategyPositionList();
}
