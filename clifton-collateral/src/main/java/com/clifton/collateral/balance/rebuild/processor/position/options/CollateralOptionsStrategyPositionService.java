package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.Date;
import java.util.List;


/**
 * @author terrys
 */
public interface CollateralOptionsStrategyPositionService {

	@SecureMethod(dtoClass = AccountingPositionDaily.class)
	public List<CollateralOptionsStrategyPosition> getCollateralOptionsStrategyPositionList(String collateralTypeName, int holdingAccountId, Date positionDate, boolean showExplanation);
}
