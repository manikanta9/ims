package com.clifton.collateral.balance;

/**
 * @author terrys
 */
public enum CollateralBalanceCategories {

	DEFAULT(CollateralBalance.class),
	OPTIONS_MARGIN(OptionsMarginCollateralBalance.class),
	OPTIONS_ESCROW(OptionsEscrowCollateralBalance.class),
	OTC_COLLATERAL(OTCCollateralBalance.class);

	private final Class<? extends CollateralBalance> implementationClass;


	CollateralBalanceCategories(Class<? extends CollateralBalance> implementationClass) {
		this.implementationClass = implementationClass;
	}


	public Class<? extends CollateralBalance> getImplementationClass() {
		return this.implementationClass;
	}
}
