package com.clifton.collateral.balance.simplejournal;

import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * The <code>CollateralBalanceSimpleJournalService</code> ...
 *
 * @author stevenf
 */
public interface CollateralBalanceSimpleJournalService {

	@ModelAttribute("data")
	@SecureMethod(dtoClass = AccountingSimpleJournal.class)
	@RequestMapping("collateralCashSimpleJournalEntriesGenerate")
	public Status generateCashCollateralSimpleJournalEntries(Date tradeDate);
}
