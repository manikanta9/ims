package com.clifton.collateral.balance.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.collateral.balance.CollateralBalance;

import java.math.BigDecimal;


/**
 * The {@link CollateralIndependentAmountCalculator} is a {@link SystemBeanGroup} interface which provides methods to calculate Independent Amount used for collateral.
 *
 * @author mwacker
 */
public interface CollateralIndependentAmountCalculator {

	public BigDecimal calculateIndependentAmount(CollateralBalance balance, AccountingPosition position, BigDecimal balanceIndependentAmountPercent);
}
