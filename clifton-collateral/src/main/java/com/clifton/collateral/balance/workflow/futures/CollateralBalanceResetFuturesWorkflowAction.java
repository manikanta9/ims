package com.clifton.collateral.balance.workflow.futures;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;
import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * clears the collateral balance of its transfer amount field and saves it.
 *
 * @author jrichter
 */
public class CollateralBalanceResetFuturesWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	private AccountingJournalService accountingJournalService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingPostingService accountingPostingService;


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setSourceSystemTableNameEquals("CollateralBalance");
		searchForm.setSourceFkFieldId(collateralBalance.getId());
		List<AccountingPositionTransfer> positionTransferList = getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);
		for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(positionTransferList)) {
			if (transfer.getBookingDate() != null) {
				AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySource("AccountingPositionTransfer", transfer.getId());

				if (journal.getJournalStatus().isPosted()) {
					getAccountingPostingService().unpostAccountingJournal(journal.getId());
				}
			}
			getAccountingPositionTransferService().deleteAccountingPositionTransfer(transfer.getId());
		}
		AccountingJournalSearchForm accountingJournalSearchForm = new AccountingJournalSearchForm();
		accountingJournalSearchForm.setFkFieldId(collateralBalance.getId());
		accountingJournalSearchForm.setSystemTableName("CollateralBalance");
		List<AccountingJournal> balanceJournalList = getAccountingJournalService().getAccountingJournalList(accountingJournalSearchForm);
		for (AccountingJournal journal : CollectionUtils.getIterable(balanceJournalList)) {
			if (journal.getJournalStatus().isPosted()) {
				getAccountingPostingService().unpostAccountingJournal(journal.getId());
			}
		}

		collateralBalance.setTransferAmount(null);
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                Getters And Setters                     //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}
}
