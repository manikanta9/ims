package com.clifton.collateral.balance.rebuild.processor.command.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.CollateralStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;

import java.util.ArrayList;
import java.util.List;


/**
 * This is an Options specific implementation of the CollateralStrategyCommand
 * that provides the ability to add a position list which is passed to the calculators
 *
 * @author StevenF
 */
public class CollateralOptionsStrategyCommand implements CollateralStrategyCommand<CollateralBalanceOptionsRebuildProcessor> {

	private List<CollateralOptionsStrategyPosition> positionList;
	private CollateralBalanceOptionsRebuildProcessor rebuildProcessor;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralBalanceOptionsRebuildProcessor getCollateralBalanceRebuildProcessor() {
		return this.rebuildProcessor;
	}


	public void setCollateralBalanceRebuildProcessor(CollateralBalanceOptionsRebuildProcessor rebuildProcessor) {
		this.rebuildProcessor = rebuildProcessor;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<CollateralOptionsStrategyPosition> getPositionList() {
		if (this.positionList == null) {
			this.positionList = new ArrayList<>();
		}
		return this.positionList;
	}


	public void setPositionList(List<CollateralOptionsStrategyPosition> positionList) {
		this.positionList = positionList;
	}
}
