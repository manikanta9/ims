package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.calculator.CollateralStrategyCalculator;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * Covered Call
 * Initial margin requirement:
 * no margin required on short call(s)
 * 50% requirement on long stock (in a margin account)
 * proceeds received from sale of call(s) may be applied to the initial margin requirement on underlying
 * after position is established, ongoing maintenance margin requirement applies, and an increase (or decrease) in the margin required is possible
 * mark-to-market price of the stock may not exceed call strike price
 *
 * @author terrys
 */
public class CollateralOptionStrategyCoveredCallCalculator implements CollateralStrategyCalculator<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor, CollateralOptionsStrategyPosition> {

	private static final BigDecimal MarginPercentage = new BigDecimal(".50");


	@Override
	public Map<Long, BigDecimal> calculateCollateral(CollateralOptionsStrategyCommand command) {
		Map<Long, BigDecimal> collateralRequirementMap = new HashMap<>();
		for (CollateralOptionsStrategyPosition strategyPosition : CollectionUtils.getIterable(command.getPositionList())) {
			collateralRequirementMap.put(strategyPosition.getAccountingTransactionId(), calculateCollateral(command.getCollateralBalanceRebuildProcessor(), strategyPosition));
		}
		return collateralRequirementMap;
	}


	@Override
	public BigDecimal calculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition) {
		return doCalculateCollateral(processor, strategyPosition, s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s)));
	}


	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal requiredCollateral = doCalculateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(requiredCollateral);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal doCalculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getCallRequirementType())) {
			appender.accept(() -> "Covered Call");
			if (MathUtils.isPositive(strategyPosition.getRemainingQuantity())) {
				appender.accept(() -> "No margin required on long call(s)");
				return BigDecimal.ZERO;
			}
			BigDecimal underlyingRequirement = strategyPosition.getUnderlyingPrice().multiply(MarginPercentage).multiply(strategyPosition.getRemainingQuantity()).multiply(new BigDecimal("100")).abs();
			BigDecimal marginAmount = MathUtils.subtract(strategyPosition.getOptionProceeds(), underlyingRequirement).abs();

			appender.accept(() -> String.format("[%s] percent of the underlying price * 100 * remaining quantity.", CoreMathUtils.formatNumber(MarginPercentage, null)));
			appender.accept(() -> String.format("Underlying price [%s].", CoreMathUtils.formatNumber(strategyPosition.getUnderlyingPrice(), null)));
			appender.accept(() -> String.format("Remaining Quantity [%s].", CoreMathUtils.formatNumber(strategyPosition.getRemainingQuantity(), null)));
			appender.accept(() -> String.format("Underlying Requirement [%s].", CoreMathUtils.formatNumber(underlyingRequirement, null)));
			appender.accept(() -> String.format("Option Proceeds [%s].", CoreMathUtils.formatNumber(strategyPosition.getOptionProceeds(), null)));
			appender.accept(() -> "Positions:");
			appender.accept(strategyPosition::buildPositionExplanation);
			appender.accept(() -> "Option Proceeds minus underlying requirement.");
			appender.accept(() -> String.format("Margin requirement: [%s]", CoreMathUtils.formatNumber(marginAmount, null)));

			return InvestmentCalculatorUtils.roundLocalAmount(marginAmount, strategyPosition.getInvestmentSecurity());
		}
		return BigDecimal.ZERO;
	}
}
