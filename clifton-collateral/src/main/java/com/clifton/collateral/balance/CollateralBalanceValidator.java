package com.clifton.collateral.balance;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>CollateralBalanceValidator</code> validated booked entities are not deleted
 * and values are never changed.  In order for values to change, the balance would be unbooked
 * and then values can be updated.  For now, just checking change amounts, balance date, holding account
 * since that's what's used for booking.
 *
 * @author Mary Anderson
 */
@Component
public class CollateralBalanceValidator extends SelfRegisteringDaoValidator<CollateralBalance> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	public void validate(CollateralBalance bean, DaoEventTypes config) throws ValidationException {
		CollateralBalance originalBean = getOriginalBean(bean);
		if (originalBean != null && originalBean.isBooked()) {
			if (config.isDelete()) {
				throw new ValidationException("Cannot delete collateral balance as it's already been booked.");
			}
			ValidationUtils.assertTrue((DateUtils.compare(originalBean.getBalanceDate(), bean.getBalanceDate(), false) == 0), "Balance date cannot be changed for a booked balance.");
			ValidationUtils.assertTrue(MathUtils.isEqual(originalBean.getTransferAmount(), bean.getTransferAmount()), "Transfer amount cannot be changed for a booked balance.");
			ValidationUtils.assertTrue(originalBean.getHoldingInvestmentAccount().equals(bean.getHoldingInvestmentAccount()), "Holding Account cannot be changed for a booked balance.");
		}
	}
}
