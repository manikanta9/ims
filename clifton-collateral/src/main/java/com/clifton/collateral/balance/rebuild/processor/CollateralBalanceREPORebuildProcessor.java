package com.clifton.collateral.balance.rebuild.processor;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.business.contract.BusinessContract;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceDetail;
import com.clifton.collateral.balance.CollateralBalanceLendingRepo;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.lending.api.repo.LendingRepoApiService;
import com.clifton.lending.api.repo.Repo;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CollateralBalanceREPORebuildProcessor</code> ...
 *
 * @author Mary Anderson
 */
public class CollateralBalanceREPORebuildProcessor<S extends CollateralStrategyPosition> extends BaseCollateralBalanceRebuildProcessor<S> {

	private static final String LENDING_REPO_TABLE_NAME = "LendingRepo";

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	private LendingRepoApiService lendingRepoApiService;
	private MarketDataRetriever marketDataRetriever;
	private MarketDataService marketDataService;
	private MarketDataSourceService marketDataSourceService;

	/////////////////////////////////////////////////////////////////
	////////                Rebuild Methods                 /////////
	/////////////////////////////////////////////////////////////////


	/**
	 * Filters on only those positions that originated from REPOS
	 * AND that mature AFTER next business day.
	 * This will enable us to exclude Overnight REPOS and anything that will be matured on the next day anyway
	 * since CollateralBalances are for the previous business day
	 */
	@Override
	protected void appendAccountingPositionList(CollateralBalanceRebuildCommand command) {
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
				.onPositionTransactionDate(command.getBalanceDate())
				.forHoldingAccount(command.getHoldingAccountId())
		);
		if (!CollectionUtils.isEmpty(positionList)) {
			// Need to filter out currency positions opened because of the repo
			positionList = BeanUtils.filter(positionList, position -> {
				if (position != null && position.getOpeningTransaction() != null && position.getOpeningTransaction().getSystemTable() != null) {
					return StringUtils.isEqual(LENDING_REPO_TABLE_NAME, position.getOpeningTransaction().getSystemTable().getName());
				}
				return false;
			});

			List<AccountingPosition> filteredPositionList = new ArrayList<>();
			Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(command.getBalanceDate()), 1);
			for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
				if (position.getOpeningTransaction().getParentTransaction() != null) {
					continue;
				}
				if (LENDING_REPO_TABLE_NAME.equals(position.getOpeningTransaction().getSystemTable() == null ? null : position.getOpeningTransaction().getSystemTable().getName())) {
					Repo repo = getLendingRepoApiService().getRepo(position.getOpeningTransaction().getFkFieldId());
					// NOTE: maturityDate FIELD ON SCREEN IS THE "ACTION DATE"
					// maturitySettlementDate FIELD ON SCREEN IS THE MATURITY DATE
					// Need to use the maturitySettlementDate since that is really the "Maturity Date"
					if (repo.getMaturitySettlementDate() == null || DateUtils.compare(repo.getMaturitySettlementDate(), nextBusinessDay, false) > 0) {
						filteredPositionList.add(position);
					}
				}
			}
			positionList = filteredPositionList;
		}
		command.setPositionList(positionList);
	}


	@Override
	protected void appendCollateralPositionList(CollateralBalanceRebuildCommand command) {
		super.appendCollateralPositionList(command);

		Map<Integer, CollateralBalanceDetail> detailMap = new HashMap<>();
		String dataSourceName = getMarketDataSourceService().getMarketDataSourceName(command.getIssuingCompany());
		Map<String, BigDecimal> fxRateMap = new HashMap<>();
		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(command.getBalanceDate()), 1);

		// Create CollateralBalanceRepoDetail for all applicable Repos
		for (AccountingPosition position : CollectionUtils.getIterable(command.getPositionList())) {
			Repo repo = getLendingRepoApiService().getRepo(position.getOpeningTransaction().getFkFieldId()); // Position List was already filtered on the correct table
			appendCollateralBalanceRepoDetail(command.getBalanceDate(), repo, position, detailMap, dataSourceName, fxRateMap, nextBusinessDay);
		}

		List<CollateralBalanceDetail> detailList = new ArrayList<>();
		for (CollateralBalanceDetail detail : CollectionUtils.getIterable(detailMap.values())) {
			detailList.add(detail);
		}
		command.setDetailList(detailList);
	}


	private void appendCollateralBalanceRepoDetail(Date balanceDate, Repo repo, AccountingPosition position, Map<Integer, CollateralBalanceDetail> detailMap, String datasourceName, Map<String, BigDecimal> fxRateMap, Date nextBusinessDay) {
		CollateralBalanceLendingRepo detail = null;
		if (detailMap.containsKey(repo.getId())) {
			detail = (CollateralBalanceLendingRepo) detailMap.get(repo.getId());
		}
		// Setup the Repo Detail
		if (detail == null) {
			detail = new CollateralBalanceLendingRepo();
			detail.setRepo(repo);

			// Constants
			// Get FX Rate to use for comparing current vs. original which was in local currency
			String ccy = repo.getSecurity().getCurrencyDenominationSymbol();
			BigDecimal exchangeRate = fxRateMap.get(ccy);
			if (exchangeRate == null) {
				exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(datasourceName, ccy, repo.getClientAccount().getBaseCurrencySymbol(), balanceDate).flexibleLookup().noExceptionIfRateIsMissing());
				fxRateMap.put(ccy, exchangeRate);
			}
			detail.setExchangeRateToBase(exchangeRate);
			detail.setIndexRatio(getMarketDataService().getMarketDataIndexRatioForSecurity(position.getInvestmentSecurity().getId(), nextBusinessDay, true));
			detail.setPrice(getMarketDataRetriever().getPrice(position.getInvestmentSecurity(), balanceDate, true, null));
			// May need to change this, but I believe these values are in local currency, so when adding to the detail list it is converted to base using current exchange rate
			detail.setAccruedInterest(getLendingRepoApiService().getRepoAccruedInterest(repo.getId(), balanceDate));

			// Aggregates
			detail.setMarketValue(BigDecimal.ZERO);
			detail.setNetCash(BigDecimal.ZERO);
		}
		// Append aggregates
		BigDecimal baseMarketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(position, balanceDate, detail.getIndexRatio());
		BigDecimal baseNetCash = MathUtils.getValueAfterHaircut(baseMarketValue, MathUtils.subtract(MathUtils.BIG_DECIMAL_ONE_HUNDRED, repo.getHaircutPercent()), true);

		detail.setMarketValue(MathUtils.add(detail.getMarketValue(), baseMarketValue));
		detail.setNetCash(MathUtils.add(detail.getNetCash(), baseNetCash));
		detailMap.put(repo.getId(), detail);
	}


	@Override
	protected void applyAccountingPositionsToCollateralBalance(CollateralBalance balance, CollateralBalanceRebuildCommand command) {
		balance.setDetailList(command.getDetailList());
		balance.setPositionsMarketValue(CoreMathUtils.sumProperty(command.getDetailList(), CollateralBalanceDetail::getOriginalMarketValue));
		balance.setCollateralRequirement(CoreMathUtils.sumProperty(command.getDetailList(), CollateralBalanceDetail::getOriginalNetCash));
	}


	@Override
	protected BigDecimal getPostedCollateral(CollateralBalanceRebuildCommand command, boolean baseCurrency) {
		BigDecimal posted = super.getPostedCollateral(command, baseCurrency);
		// Add in CollateralBalanceRepoDetail MarketValue
		posted = MathUtils.add(posted, CoreMathUtils.sumProperty(command.getDetailList(), CollateralBalanceDetail::getMarketValue));
		return posted;
	}


	@Override
	protected BigDecimal getPostedCollateralAdjusted(CollateralBalanceRebuildCommand command, boolean baseCurrency) {
		BigDecimal postedAdjusted = super.getPostedCollateralAdjusted(command, baseCurrency);
		// Add in CollateralBalanceRepoDetail NetCash and Interest
		postedAdjusted = MathUtils.add(postedAdjusted, CoreMathUtils.sumProperty(command.getDetailList(), CollateralBalanceDetail::getNetCash));
		postedAdjusted = MathUtils.add(postedAdjusted, CoreMathUtils.sumProperty(command.getDetailList(), CollateralBalanceDetail::getAccruedInterest));
		return postedAdjusted;
	}


	@Override
	public void applyCollateralTypeOverrides(CollateralBalance balance) {
		BigDecimal roundingValue = null;

		if (balance.getHoldingInvestmentAccount().getBusinessContract() != null) {
			roundingValue = (BigDecimal) getBusinessContractCustomFieldValue(balance.getHoldingInvestmentAccount().getBusinessContract(), BusinessContract.CONTRACT_ROUNDING_CUSTOM_COLUMN);
		}
		setRoundedCollateralChangeValues(balance, roundingValue);
	}


	private void setRoundedCollateralChangeValues(CollateralBalance balance, BigDecimal roundingValue) {
		BigDecimal changeValue = BigDecimal.ZERO;

		// Value = Original Net Cash - Current Net Cash
		BigDecimal value = balance.getExcessCollateral();

		// If there is Posted Collateral - Calculate Change Value
		if (!MathUtils.isNullOrZero(balance.getPostedCollateralHaircutValue())) {
			// If Threshold
			if (!MathUtils.isNullOrZero(balance.getThresholdAmount())) {
				// If amount is less than threshold - set changeValue = 0 - nothing to transfer
				// Otherwise changeValue = value (Net Cash Difference);
				if (!MathUtils.isLessThan(MathUtils.abs(value), balance.getThresholdAmount())) {
					changeValue = value;
				}
			}
			// No threshold - changeValue = value (Net Cash Difference)
			else {
				changeValue = value;
			}

			// If No Min Transfer - value is the change amount.  If change is less than min transfer, change is zero
			if (!MathUtils.isNullOrZero(balance.getMinTransferAmount())) {
				if (MathUtils.isLessThan(MathUtils.abs(changeValue), balance.getMinTransferAmount())) {
					changeValue = BigDecimal.ZERO;
				}
			}
		}
		balance.setCollateralChangeAmount(roundCollateralChangeAmount(changeValue, roundingValue));
	}


	///////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods          //////////////
	///////////////////////////////////////////////////////////////////////////


	public LendingRepoApiService getLendingRepoApiService() {
		return this.lendingRepoApiService;
	}


	public void setLendingRepoApiService(LendingRepoApiService lendingRepoApiService) {
		this.lendingRepoApiService = lendingRepoApiService;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
