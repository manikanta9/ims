package com.clifton.collateral.balance;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.collateral.CollateralType;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.WorkflowConfig;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>CollateralBalance</code> class represents daily collateral balance for a specific holding account.
 *
 * @author vgomelsky
 */
@WorkflowConfig(required = false, workflowBeanPropertyName = "collateralType.workflow")
public class CollateralBalance extends HierarchicalEntity<CollateralBalance, Integer> implements RuleViolationAware, BookableEntity, WorkflowAware {

	public static final String COLLATERAL_BALANCE_TABLE_NAME = "CollateralBalance";
	public static final String RULE_CATEGORY_COLLATERAL_BALANCE = "Collateral Balance Rules";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CollateralType collateralType;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;
	private RuleViolationStatus violationStatus;

	// where positions are as opposed to collateral which could be in a different account
	private InvestmentAccount holdingInvestmentAccount;
	// this balance is group parent meaning that it summarizes multiple child balances
	// the holding account of this balance will be the primary account in the account group
	private boolean parentCollateralBalance;
	// transient field used for displaying the account group alias on the UI
	@NonPersistentField
	private InvestmentAccountGroup holdingInvestmentAccountGroup;

	private InvestmentSecurity collateralCurrency;
	private Date balanceDate;

	private String note;

	/**
	 * All amounts are negative (client pays) and are in base currency of the holding investment account.
	 * Only positions that require collateral where the holding investment account allows independent amounts are included.
	 */
	private BigDecimal independentAmount;

	/**
	 * Exposure/MTM. All amounts are in base currency of the holding investment account.
	 * Only positions that require collateral are included. For REPO Collateral these are all REPO positions.
	 */
	private BigDecimal positionsMarketValue;

	// includes collateral
	private BigDecimal totalCashValue;

	// Posted Collateral & Haircut Posted Collateral. REPO: Client "Net Cash"
	private BigDecimal cashCollateralValue;
	private BigDecimal postedCollateralValue; // includes both Cash and Positions: Positions Collateral = postedCollateralValue - cashCollateralValue
	private BigDecimal postedCollateralHaircutValue;
	private BigDecimal endingPostedCollateralValue; // currently only used for OTC (includes transfers linked to this balance)

	// Posted Counterparty (or "Broker" for Non-OTC Collateral) Collateral/Haircut Collateral
	// REPO: Counterparty "Net Cash"
	private BigDecimal postedCounterpartyCollateralValue;
	private BigDecimal postedCounterpartyCollateralHaircutValue;
	private BigDecimal endingPostedCounterpartyCollateralValue; // includes transfers linked to this balance

	// Pulled from the Contract on the Holding Account
	// OTC (ISDA) and REPO (Repurchase Agreement)
	private BigDecimal thresholdAmount;
	private BigDecimal contractRounding;
	private BigDecimal thresholdCounterpartyAmount;
	private BigDecimal minTransferAmount;
	private BigDecimal minTransferCounterpartyAmount;

	// OTC Collateral Only: Client Projected Change In Collateral
	// required collateral transfer in order to get back under (threshold + min transfer amount)
	private BigDecimal collateralChangeAmount;

	// OTC Collateral Only: Counterparty Projected Change In Collateral
	// required collateral transfer in order to get back under (threshold + min transfer amount)
	private BigDecimal counterpartyCollateralChangeAmount;

	/**
	 * When applicable, enter collateral Call Amount made by the Counterparty.
	 */
	private BigDecimal counterpartyCollateralCallAmount;

	// Futures Collateral: In cases where there is only one - the Client Account related to the holding account
	// TODO NOT SURE IF THIS WILL APPLY TO OTC OR REPO COLLATERAL AS WELL
	private InvestmentAccount clientInvestmentAccount;

	// Futures Collateral: The "custodian" related account - collateral transfers are made from/to
	// TODO NOT SURE IF THIS WILL APPLY TO OTC OR REPO COLLATERAL AS WELL
	private InvestmentAccount collateralInvestmentAccount;

	// Futures Collateral: Externally Loaded from Broker Posted Collateral
	private BigDecimal externalCollateralAmount;

	// Futures Collateral: Externally Loaded from Broker Required Collateral (sum of initial margins for positions including cross-margining rules)
	private BigDecimal externalCollateralRequirement;

	// Options Margin Collateral: Externally Loaded from Broker Posted Collateral.  Provides an override for the calculated amount.
	private BigDecimal externalExcessCollateral;

	// Futures Collateral: IMS Calculated Required Collateral
	// OTC: N/A
	// REPO: Current "Net Cash" - i.e. Required Amount using current market value
	// For REPOS this value is calculated as Current Market Value for Each Position - Haircut defined on the LendingRepoDetailDefinition for the position (linked by Source Table/ID of LendingRepoDetail/LendingRepoDetailID)
	private BigDecimal collateralRequirement; // Calculated Required Collateral Amount

	private boolean reconciled;

	// Booking Fields
	// Currently Booking is only supported for Non-OTC Collateral

	// Amount to transfer
	// Futures: : Either hand entered or generated by the system depending on Our Posted/Required or Broker Posted/Required
	// or any combo of those fields.  User can select the combo they want.
	// Value is also rounded to the value from Holding Account Custom Fields - Collateral Transfer Rounding (For Non-OTC)
	// If used for OTC and/or REPO Rounding is used from the Contract
	// ***On the UI the transfer amount shown is the sum of the transfer amount and the securities transfer amount
	// ***Hover over the column to see that individual value breakdown.
	private BigDecimal transferAmount;

	// Amount of security positions to transfer
	// Futures: : Either hand entered or generated by the system depending on Our Posted/Required or Broker Posted/Required
	// or any combo of those fields.  User can select the combo they want.
	// Value is also rounded to the value from Holding Account Custom Fields - Collateral Transfer Rounding (For Non-OTC)
	// If used for OTC and/or REPO Rounding is used from the Contract
	private BigDecimal securitiesTransferAmount;

	// Market value of the position or cash being posted as collateral.
	// Balance Date + 1 on 'Accounting Transactions'.
	private BigDecimal agreedMovementAmount;

	// Once the transfer amount is booked, this date is set - Records are Read Only when booking date is set
	private Date bookingDate;

	/**
	 * Currently only used for REPO Collateral - Contains the breakdown of the balance record for each REPO position
	 */
	private List<CollateralBalanceDetail> detailList;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String lbl = DateUtils.fromDateShort(getBalanceDate()) + ": ";
		if (getHoldingInvestmentAccount() != null) {
			lbl += getHoldingInvestmentAccount().getLabel();
		}
		if (getCollateralType() != null) {
			lbl += " (" + getCollateralType().getName() + ")";
		}
		if (getCollateralCurrency() != null) {
			lbl += " - " + getCollateralCurrency().getSymbol();
		}
		return lbl;
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount());
	}


	/**
	 * Indicates if this CollateralBalance entry is bookable.
	 */
	public boolean isBookable() {
		return getParent() == null;
	}


	public boolean isBooked() {
		return getBookingDate() != null;
	}


	public BigDecimal getCoalesceExternalPostedCounterpartyCollateralHaircutValue() {
		if (!MathUtils.isNullOrZero(getExternalCollateralAmount())) {
			return getExternalCollateralAmount();
		}
		return getPostedCounterpartyCollateralHaircutValue();
	}


	public BigDecimal getCoalesceExternalCounterpartyCollateralRequirement() {
		if (!MathUtils.isNullOrZero(getExternalCollateralRequirement())) {
			return getExternalCollateralRequirement();
		}
		return getCollateralRequirement();
	}


	public BigDecimal getExcessCollateral() {
		return MathUtils.subtract(getPostedCollateralHaircutValue(), getCollateralRequirement());
	}


	public BigDecimal getExcessCollateralPercentage() {
		return CoreMathUtils.getPercentValue(getExcessCollateral(), getCollateralRequirement());
	}


	public BigDecimal getExcessCounterpartyCollateral() {
		return MathUtils.subtract(getPostedCounterpartyCollateralHaircutValue(), getCollateralRequirement());
	}


	public BigDecimal getExcessCounterpartyCollateralPercentage() {
		return CoreMathUtils.getPercentValue(getPostedCounterpartyCollateralHaircutValue(), getCollateralRequirement());
	}


	public BigDecimal getExternalExcessCollateral() {
		return ObjectUtils.coalesce(this.externalExcessCollateral, MathUtils.subtract(getExternalCollateralAmount(), getExternalCollateralRequirement()));
	}


	public void setExternalExcessCollateral(BigDecimal externalExcessCollateral) {
		this.externalExcessCollateral = externalExcessCollateral;
	}


	public BigDecimal getExternalExcessCollateralPercentage() {
		return CoreMathUtils.getPercentValue(getExternalExcessCollateral(), getExternalCollateralRequirement());
	}


	/**
	 * Returns true if holding investment account is a hold account (no daily m2m transfers as long as the balance is positive).
	 */
	public boolean isHoldAccount() {
		if (this.holdingInvestmentAccount != null) {
			InvestmentAccountType type = this.holdingInvestmentAccount.getType();
			if (type != null && type.getName() != null && type.getName().contains(AccountingM2MDaily.HOLD_ACCOUNT_PATTERN)) {
				return true;
			}
		}
		return false;
	}


	public BigDecimal getSecuritiesCollateralValue() {
		BigDecimal securitiesCollateralValue = MathUtils.subtract(getPostedCollateralValue(), getCashCollateralValue());
		return securitiesCollateralValue != null ? securitiesCollateralValue : BigDecimal.ZERO;
	}


	/**
	 * Returns Client or Counterparty Threshold Amount depending on which one should be used.
	 */
	public BigDecimal getCalculatedThresholdAmount() {
		if (isInClientFavor()) {
			return getThresholdCounterpartyAmount();
		}
		return getThresholdAmount();
	}


	/**
	 * The total amount of Collateral required (prior to Rounding & Minimum Transfer Amount being applied).
	 */
	public BigDecimal getCalculatedTotalRequirement() {
		BigDecimal result = CoreMathUtils.sum(getPositionsMarketValue(), getIndependentAmount());
		if (MathUtils.isLessThanOrEqual(MathUtils.abs(result), getCalculatedThresholdAmount())) {
			// have not crossed the threshold: no transfer
			return BigDecimal.ZERO;
		}

		if (isInClientFavor()) {
			// positive result: the client gets paid
			return MathUtils.subtract(result, getCalculatedThresholdAmount());
		}

		// negative result: the client pays
		return MathUtils.add(result, getCalculatedThresholdAmount());
	}


	/**
	 * Total Requirement + Client Posted Collateral - Counterparty Posted Collateral
	 */
	public BigDecimal getCalculatedNetRequirement() {
		return MathUtils.subtract(CoreMathUtils.sum(getCalculatedTotalRequirement(), getPostedCollateralHaircutValue()), getPostedCounterpartyCollateralHaircutValue());
	}


	public boolean isInClientFavor() {
		return MathUtils.isGreaterThan(CoreMathUtils.sum(getPositionsMarketValue(), getIndependentAmount()), java.math.BigDecimal.ZERO);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public BigDecimal getIndependentAmount() {
		return this.independentAmount;
	}


	public void setIndependentAmount(BigDecimal independentAmount) {
		this.independentAmount = independentAmount;
	}


	public BigDecimal getPositionsMarketValue() {
		return this.positionsMarketValue;
	}


	public void setPositionsMarketValue(BigDecimal positionsMarketValue) {
		this.positionsMarketValue = positionsMarketValue;
	}


	public BigDecimal getTotalCashValue() {
		return this.totalCashValue;
	}


	public void setTotalCashValue(BigDecimal totalCashValue) {
		this.totalCashValue = totalCashValue;
	}


	public BigDecimal getCashCollateralValue() {
		return this.cashCollateralValue;
	}


	public void setCashCollateralValue(BigDecimal cashCollateralValue) {
		this.cashCollateralValue = cashCollateralValue;
	}


	public BigDecimal getPostedCollateralValue() {
		return this.postedCollateralValue;
	}


	public void setPostedCollateralValue(BigDecimal postedCollateralValue) {
		this.postedCollateralValue = postedCollateralValue;
	}


	public BigDecimal getPostedCollateralHaircutValue() {
		return this.postedCollateralHaircutValue;
	}


	public void setPostedCollateralHaircutValue(BigDecimal postedCollateralHaircutValue) {
		this.postedCollateralHaircutValue = postedCollateralHaircutValue;
	}


	public BigDecimal getThresholdAmount() {
		return this.thresholdAmount;
	}


	public void setThresholdAmount(BigDecimal thresholdAmount) {
		this.thresholdAmount = thresholdAmount;
	}


	public BigDecimal getThresholdCounterpartyAmount() {
		return this.thresholdCounterpartyAmount;
	}


	public void setThresholdCounterpartyAmount(BigDecimal thresholdCounterpartyAmount) {
		this.thresholdCounterpartyAmount = thresholdCounterpartyAmount;
	}


	public BigDecimal getMinTransferAmount() {
		return this.minTransferAmount;
	}


	public void setMinTransferAmount(BigDecimal minTransferAmount) {
		this.minTransferAmount = minTransferAmount;
	}


	public BigDecimal getMinTransferCounterpartyAmount() {
		return this.minTransferCounterpartyAmount;
	}


	public void setMinTransferCounterpartyAmount(BigDecimal minTransferCounterpartyAmount) {
		this.minTransferCounterpartyAmount = minTransferCounterpartyAmount;
	}


	public BigDecimal getCollateralChangeAmount() {
		return this.collateralChangeAmount;
	}


	public void setCollateralChangeAmount(BigDecimal collateralChangeAmount) {
		this.collateralChangeAmount = collateralChangeAmount;
	}


	public BigDecimal getPostedCounterpartyCollateralValue() {
		return this.postedCounterpartyCollateralValue;
	}


	public void setPostedCounterpartyCollateralValue(BigDecimal postedCounterpartyCollateralValue) {
		this.postedCounterpartyCollateralValue = postedCounterpartyCollateralValue;
	}


	public BigDecimal getPostedCounterpartyCollateralHaircutValue() {
		return this.postedCounterpartyCollateralHaircutValue;
	}


	public void setPostedCounterpartyCollateralHaircutValue(BigDecimal postedCounterpartyCollateralHaircutValue) {
		this.postedCounterpartyCollateralHaircutValue = postedCounterpartyCollateralHaircutValue;
	}


	public BigDecimal getEndingPostedCounterpartyCollateralValue() {
		return this.endingPostedCounterpartyCollateralValue;
	}


	public void setEndingPostedCounterpartyCollateralValue(BigDecimal endingPostedCounterpartyCollateralValue) {
		this.endingPostedCounterpartyCollateralValue = endingPostedCounterpartyCollateralValue;
	}


	public BigDecimal getCounterpartyCollateralChangeAmount() {
		return this.counterpartyCollateralChangeAmount;
	}


	public void setCounterpartyCollateralChangeAmount(BigDecimal counterpartyCollateralChangeAmount) {
		this.counterpartyCollateralChangeAmount = counterpartyCollateralChangeAmount;
	}


	public BigDecimal getCounterpartyCollateralCallAmount() {
		return this.counterpartyCollateralCallAmount;
	}


	public void setCounterpartyCollateralCallAmount(BigDecimal counterpartyCollateralCallAmount) {
		this.counterpartyCollateralCallAmount = counterpartyCollateralCallAmount;
	}


	public BigDecimal getExternalCollateralAmount() {
		return this.externalCollateralAmount;
	}


	public void setExternalCollateralAmount(BigDecimal externalCollateralAmount) {
		this.externalCollateralAmount = externalCollateralAmount;
	}


	public BigDecimal getCollateralRequirement() {
		return this.collateralRequirement;
	}


	public void setCollateralRequirement(BigDecimal collateralRequirement) {
		this.collateralRequirement = collateralRequirement;
	}


	public BigDecimal getExternalCollateralRequirement() {
		return this.externalCollateralRequirement;
	}


	public void setExternalCollateralRequirement(BigDecimal externalCollateralRequirement) {
		this.externalCollateralRequirement = externalCollateralRequirement;
	}


	public BigDecimal getTransferAmount() {
		return this.transferAmount;
	}


	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}


	public BigDecimal getSecuritiesTransferAmount() {
		return this.securitiesTransferAmount;
	}


	public void setSecuritiesTransferAmount(BigDecimal securitiesTransferAmount) {
		this.securitiesTransferAmount = securitiesTransferAmount;
	}


	public BigDecimal getAgreedMovementAmount() {
		return this.agreedMovementAmount;
	}


	public void setAgreedMovementAmount(BigDecimal agreedMovementAmount) {
		this.agreedMovementAmount = agreedMovementAmount;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public InvestmentSecurity getCollateralCurrency() {
		return this.collateralCurrency;
	}


	public void setCollateralCurrency(InvestmentSecurity collateralCurrency) {
		this.collateralCurrency = collateralCurrency;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getCollateralInvestmentAccount() {
		return this.collateralInvestmentAccount;
	}


	public void setCollateralInvestmentAccount(InvestmentAccount collateralInvestmentAccount) {
		this.collateralInvestmentAccount = collateralInvestmentAccount;
	}


	public CollateralType getCollateralType() {
		return this.collateralType;
	}


	public void setCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
	}


	public List<CollateralBalanceDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<CollateralBalanceDetail> detailList) {
		this.detailList = detailList;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public BigDecimal getEndingPostedCollateralValue() {
		return this.endingPostedCollateralValue;
	}


	public void setEndingPostedCollateralValue(BigDecimal endingPostedCollateralValue) {
		this.endingPostedCollateralValue = endingPostedCollateralValue;
	}


	public BigDecimal getContractRounding() {
		return this.contractRounding;
	}


	public void setContractRounding(BigDecimal contractRounding) {
		this.contractRounding = contractRounding;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public boolean isParentCollateralBalance() {
		return this.parentCollateralBalance;
	}


	public void setParentCollateralBalance(boolean parentCollateralBalance) {
		this.parentCollateralBalance = parentCollateralBalance;
	}


	public InvestmentAccountGroup getHoldingInvestmentAccountGroup() {
		return this.holdingInvestmentAccountGroup;
	}


	public void setHoldingInvestmentAccountGroup(InvestmentAccountGroup holdingInvestmentAccountGroup) {
		this.holdingInvestmentAccountGroup = holdingInvestmentAccountGroup;
	}
}
