package com.clifton.collateral.balance.workflow.futures;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosing;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingCommand;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingSecurityQuantity;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingService;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.workflow.BaseCollateralBalanceWorkflowAction;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>CollateralBalancePledgedTransactionsFuturesWorkflowAction</code> is a workflow action that calculates/predicts a
 * collateral movement. The movement is then saved. For Cash movements the cash value is saved on the collateral balance
 * it self. For position movements a position transfer is created.
 *
 * @author jrichter
 */
public class CollateralBalancePledgedTransactionsFuturesWorkflowAction extends BaseCollateralBalanceWorkflowAction {

	private CollateralBalanceRebuildService collateralBalanceRebuildService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private AccountingTransactionService accountingTransactionService;
	private SystemSchemaService systemSchemaService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentSetupService investmentSetupService;
	private AccountingValuationService accountingValuationService;
	private AccountingPositionClosingService accountingPositionClosingService;
	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	//////////                  Business methods                      //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processCollateralBalanceWorkflowAction(CollateralBalance collateralBalance) {
		CollateralBalancePledgedTransactionsFuturesWorkflowContext context = new CollateralBalancePledgedTransactionsFuturesWorkflowContext(collateralBalance, getCollateralBalanceRebuildService());
		if (isCollateralProcessingNeeded(context)) {
			context.setBrokerHeldCollateral(buildFuturesCollateralPositionMap(context, true));
			context.setCustodianCollateral(buildFuturesCollateralPositionMap(context, false));
			processCollateral(context);
			pledgeCollateral(context);
		}
	}


	private boolean isCollateralProcessingNeeded(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		CollateralBalance collateralBalance = context.getCollateralBalance();
		if (context.isManualAccount() || collateralBalance.isHoldAccount() || MathUtils.isNullOrZero(collateralBalance.getExternalCollateralRequirement())) {
			return false;
		}
		//If user manually created a position transfer throw a validation exception to avoid potential conflict.
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setSourceSystemTableName("CollateralBalance");
		searchForm.setSourceFkFieldId(collateralBalance.getId());
		ValidationUtils.assertEmpty(getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm), context.getErrorMsgPrefix() + "Existing accounting transfers for current balance. Workflow cannot evaluate a collateral balance that has any transfers.");
		return true;
	}


	private void processCollateral(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> prioritizedAvailableCollateral = prioritizeAvailableCollateral(context);
		processCollateral(context, prioritizedAvailableCollateral);
		context.setCollateralPost(CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(context.getIdealCollateral(), context.getBrokerHeldCollateral()));
		context.setCollateralReturn(CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(context.getBrokerHeldCollateral(), context.getIdealCollateral()));
	}


	/**
	 * Recursive method used to continually add collateral to the 'IdealCollateral' map until it's collateral value exceeds the target transfer amount.
	 *
	 * @param context
	 * @param prioritizedAvailableCollateral
	 */
	private void processCollateral(CollateralBalancePledgedTransactionsFuturesWorkflowContext context, Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> prioritizedAvailableCollateral) {
		Iterator<InvestmentInstrumentHierarchy> iterator = prioritizedAvailableCollateral.keySet().iterator();
		if (iterator.hasNext()) {
			InvestmentInstrumentHierarchy hierarchy = iterator.next();
			List<FuturesCollateralPositionPledge> positionList = prioritizedAvailableCollateral.remove(hierarchy);
			CollectionUtils.sort(positionList, new FuturesCollateralPositionPledgeComparator(context.getBrokerHeldCollateral()));
			for (FuturesCollateralPositionPledge positionPledge : CollectionUtils.getIterable(positionList)) {
				BigDecimal remainingTransferAmount = MathUtils.subtract(context.getTargetTransferAmount(), CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(context.getIdealCollateral()));
				if (aggregatePositionToIdealCollateral(hierarchy, remainingTransferAmount, positionPledge, context)) {
					return;
				}
			}
			processCollateral(context, prioritizedAvailableCollateral);
		}
	}


	/**
	 * Decides the appropriate amount of cash or a given security to add to the 'idealCollateralMap' then returns a boolean indicating weather or not the new addition covers the
	 * target transfer amount.
	 */
	private boolean aggregatePositionToIdealCollateral(InvestmentInstrumentHierarchy hierarchy, BigDecimal remainingTransferAmount, FuturesCollateralPositionPledge positionPledge, CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		InvestmentSecurity investmentSecurity = positionPledge.getInvestmentSecurity();
		BigDecimal marketValueForOverrideQuantity;
		BigDecimal transferQuantity;
		boolean covered;
		if (hierarchy.isCurrency()) {
			covered = MathUtils.isLessThan(remainingTransferAmount, positionPledge.getQuantity());
			transferQuantity = covered ? remainingTransferAmount : positionPledge.getQuantity();
			marketValueForOverrideQuantity = transferQuantity;
		}
		else {
			BigDecimal calculatedTransferQuantity = calculateTransferQuantity(remainingTransferAmount, positionPledge.getQuantity(), positionPledge.getCollateralValue(), context);
			transferQuantity = MathUtils.isLessThan(calculatedTransferQuantity, positionPledge.getQuantity()) ? calculatedTransferQuantity : positionPledge.getQuantity();
			//We dont call 'getAccountingPositionClosingLotMarketValue' because it requires having the quantity in the provided account to get an accurate value
			// we dont know which account has the ideal value to evaluate at this point. Rounding market values should not cause issues given the minimum transfer quantity.
			marketValueForOverrideQuantity = MathUtils.multiply(transferQuantity, MathUtils.divide(positionPledge.getMarketValue(), positionPledge.getQuantity()));
			covered = MathUtils.isEqual(transferQuantity, calculatedTransferQuantity);
		}
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(new FuturesCollateralPositionPledge(investmentSecurity, transferQuantity, marketValueForOverrideQuantity, positionPledge.getHaircutValue()), context.getIdealCollateral());
		return covered;
	}


	/**
	 * This method decodes the final cash value to post/return. This needs to be done after calculating ideal because the cash rules are different
	 * depending on weather it is being posted or returned. For example posted cash must be moved in increments of the configured Cash Transfer Increment Amount
	 * however there is an exception when returning all cash.
	 *
	 * @param context
	 */
	private void pledgeCashTransfer(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		BigDecimal cashValue = CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(context.getCollateralPost());
		boolean isCashReturn = false;
		if (MathUtils.isNullOrZero(cashValue)) {
			isCashReturn = true;
			cashValue = CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(context.getCollateralReturn());
		}
		if (MathUtils.isNullOrZero(cashValue)) {
			return;
		}
		BigDecimal accountAvailableCash = isCashReturn ? CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(context.getBrokerHeldCollateral()) : CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(context.getCustodianCollateral());

		if (!MathUtils.isEqual(BigDecimal.ZERO, context.getCashTransferIncrementAmount())) {
			//round
			cashValue = MathUtils.divideToIntegralValue(cashValue, context.getCashTransferIncrementAmount());
			if (!isCashReturn) {
				//Round up for post: down for return
				cashValue = MathUtils.add(cashValue, BigDecimal.ONE);
			}
			cashValue = MathUtils.multiply(cashValue, context.getCashTransferIncrementAmount());
		}
		if (isCashReturn) {
			if (MathUtils.isLessThan(cashValue, context.getMinimumCashTransferAmount())) {
				cashValue = BigDecimal.ZERO;
			}
			else if (MathUtils.isGreaterThan(cashValue, accountAvailableCash)) {
				cashValue = accountAvailableCash;
			}
			context.getCollateralBalance().setTransferAmount(cashValue);
		}
		else {
			if (MathUtils.isGreaterThan(cashValue, accountAvailableCash)) {
				ValidationUtils.fail(context.getErrorMsgPrefix() + "While attempting to post cash: The available cash was insufficient to cover the desired amount.");
			}
			else if (MathUtils.isLessThan(cashValue, context.getMinimumCashTransferAmount())) {
				cashValue = context.getMinimumCashTransferAmount();
			}
			context.getCollateralBalance().setTransferAmount(MathUtils.negate(cashValue));
		}
	}


	private BigDecimal calculateTransferQuantity(BigDecimal desiredTransferAmount, BigDecimal availableQuantity, BigDecimal positionValue, CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		BigDecimal transferQuantity;
		//Using basic cross multiply and divide technique to get quantity close to the transfer amount.
		transferQuantity = MathUtils.divide(MathUtils.multiply(desiredTransferAmount, availableQuantity), positionValue);
		//Translate the transfer quantity to a multiple of the minimum transfer quantity.
		BigDecimal minimumTransferQuantityQuantity = MathUtils.divideToIntegralValue(transferQuantity, context.getMinimumTransferQuantity());
		transferQuantity = MathUtils.multiply(minimumTransferQuantityQuantity, context.getMinimumTransferQuantity());

		return transferQuantity;
	}


	/**
	 * this method organizes all collateral under the configured hierarchy configuration then returns the mapping.
	 */
	private Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> prioritizeAvailableCollateral(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> availableCollateral = new LinkedHashMap<>();
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateMaps(availableCollateral, context.getBrokerHeldCollateral());
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateMaps(availableCollateral, context.getCustodianCollateral());
		Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> eligibleCollateralMap = context.getEligibleCollateralMap();
		// ensures map is created once and allows us to safely mock this method for unit tests.
		if (CollectionUtils.isEmpty(eligibleCollateralMap)) {
			//Most other maps can be of any map type but this one must be A LinkedHashMap
			eligibleCollateralMap = new LinkedHashMap<>();
			List<InvestmentInstrumentHierarchy> priorityList = getInvestmentSetupService().getInvestmentInstrumentHierarchyListByIds(context.getInvestmentInstrumentHierarchyIds());
			for (InvestmentInstrumentHierarchy instrumentHierarchy : CollectionUtils.getIterable(priorityList)) {
				if (!eligibleCollateralMap.containsKey(instrumentHierarchy)) {
					eligibleCollateralMap.put(instrumentHierarchy, new ArrayList<>());
				}
				for (FuturesCollateralPositionPledge positionPledge : CollectionUtils.getIterable(availableCollateral.values())) {
					if (positionPledge.getInvestmentSecurity().getInstrument().getHierarchy().isSelfOrAncestor(instrumentHierarchy)) {
						eligibleCollateralMap.get(instrumentHierarchy).add(positionPledge);
					}
				}
			}
		}
		return eligibleCollateralMap;
	}


	/**
	 * Builds the transfers and or updates the collateral balance cash transfer values.
	 *
	 * @param context
	 */
	private void pledgeCollateral(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		pledgeCashTransfer(context);
		if (!MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralReturn()))) {
			Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> returnMap = CollateralBalanceFuturesCollateralWorkflowUtils.getPositionPledgedListByHierarchyMap(context.getCollateralReturn());
			for (List<FuturesCollateralPositionPledge> pledgeList : returnMap.values()) {
				AccountingPositionTransfer returnTransfer = buildPositionTransfer(context, pledgeList, false);
				getAccountingPositionTransferService().saveAccountingPositionTransfer(returnTransfer);
			}
		}
		if (!MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralPost()))) {
			Map<InvestmentInstrumentHierarchy, List<FuturesCollateralPositionPledge>> postMap = CollateralBalanceFuturesCollateralWorkflowUtils.getPositionPledgedListByHierarchyMap(context.getCollateralPost());
			for (List<FuturesCollateralPositionPledge> pledgeList : postMap.values()) {
				AccountingPositionTransfer postTransfer = buildPositionTransfer(context, pledgeList, true);
				getAccountingPositionTransferService().saveAccountingPositionTransfer(postTransfer);
			}
		}
		validateTransfers(context);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                   Calculated Getters                   //////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingBalanceValue> getAvailableCollateralList(CollateralBalance collateralBalance, boolean holdingAccount) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());
		searchForm.setHoldingInvestmentAccountId(holdingAccount ? collateralBalance.getHoldingInvestmentAccount().getId() : collateralBalance.getCollateralInvestmentAccount().getId());
		searchForm.setCollateralAccountingAccount(holdingAccount);
		searchForm.setPositionAccountingAccount(true);
		searchForm.setReceivableAccountingAccount(false);
		searchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.EXCLUDE_RECEIVABLE_COLLATERAL);
		searchForm.setTransactionDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(collateralBalance.getBalanceDate()), 1));
		return getAccountingValuationService().getAccountingBalanceValueList(searchForm);
	}


	/**
	 * looks up the and aggregates the cash available for the either the collateral balance holding account or the collateral account indicated by the holdingAccount parameter.
	 * Then we convert this into a <code>FuturesCollateralPositionPledge</code>
	 */
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> getMaximumTransferableCashValue(CollateralBalancePledgedTransactionsFuturesWorkflowContext context, Map<InvestmentSecurity, FuturesCollateralPositionPledge> map, boolean holdingAccount) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setTransactionDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(context.getCollateralBalance().getBalanceDate()), 1));
		searchForm.setClientInvestmentAccountId(context.getCollateralBalance().getClientInvestmentAccount().getId());
		searchForm.setReadUncommittedRequested(true);
		InvestmentAccount investmentAccount;
		//the 'CASH' designation here returns only Securities with a GL Account of Cash not Currency so we should be ok to sum these.
		//This should also allow us to use the 'isCurrency' flag on the 'InvestmentSecurity' objects.
		if (holdingAccount) {
			investmentAccount = context.getCollateralBalance().getHoldingInvestmentAccount();
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);
		}
		else {
			investmentAccount = context.getCollateralBalance().getCollateralInvestmentAccount();
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL);
		}
		searchForm.setHoldingInvestmentAccountId(investmentAccount.getId());
		List<AccountingBalanceValue> balanceList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		BigDecimal totalCash = BigDecimal.ZERO;
		for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(balanceList)) {
			totalCash = MathUtils.add(totalCash, accountingBalanceValue.getBaseMarketValue());
			CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(new FuturesCollateralPositionPledge(accountingBalanceValue.getInvestmentSecurity(), accountingBalanceValue.getBaseMarketValue(), accountingBalanceValue.getBaseMarketValue(), BigDecimal.ZERO), map);
		}
		ValidationUtils.assertFalse(MathUtils.isLessThan(totalCash, BigDecimal.ZERO), context.getErrorMsgPrefix() + "" + investmentAccount.getLabel() + " holds " + CoreMathUtils.formatNumberMoney(totalCash) + " available cash which is less than the allowed 0.00");
		return map;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                       Validation                       //////////
	////////////////////////////////////////////////////////////////////////////


	private void validateTransfers(CollateralBalancePledgedTransactionsFuturesWorkflowContext context) {
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(context.getCollateralPost())) && MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValueIncludingCash(context.getCollateralReturn())),
				context.getErrorMsgPrefix() + "Found nothing to pledge. The collateral balance may be improperly configured or there may be nothing in the accounts.");
		if (!MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralPost()))) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralPost()), context.getMinimumPositionTransferAmount()),
					context.getErrorMsgPrefix() + "The Market value of the post position transfer created was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralPost())) + ", which is less than the minimum transfer amount of " + CoreMathUtils.formatNumberMoney(context.getMinimumPositionTransferAmount()) + ". This could be because of a cash transfer or a configuration error.");
		}
		if (!MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralReturn()))) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralReturn()), context.getMinimumPositionTransferAmount()),
					context.getErrorMsgPrefix() + "The Market value of the return position transfer created was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(context.getCollateralReturn())) + ", which is less than the minimum transfer amount of " + CoreMathUtils.formatNumberMoney(context.getMinimumPositionTransferAmount()) + ". This could be because of a cash transfer or a configuration error.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                        Builders                        //////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPositionTransfer buildPositionTransfer(CollateralBalancePledgedTransactionsFuturesWorkflowContext context, List<FuturesCollateralPositionPledge> futuresCollateralPositionPledgeList, boolean post) {
		AccountingPositionTransferType transferType = getAccountingPositionTransferService().getAccountingPositionTransferTypeByName(post ? AccountingPositionTransferType.COLLATERAL_TRANSFER_POST : AccountingPositionTransferType.COLLATERAL_TRANSFER_RETURN);
		AccountingPositionTransfer transfer = buildAccountingPositionTransfer(context, transferType);
		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		for (FuturesCollateralPositionPledge pledge : futuresCollateralPositionPledgeList) {
			if (!pledge.getInvestmentSecurity().isCurrency()) {
				InvestmentAccount holdingAccount = post ? context.getCollateralBalance().getCollateralInvestmentAccount() : context.getCollateralBalance().getHoldingInvestmentAccount();
				AccountingPositionClosingCommand cmd = buildAccountingPositionClosingCommand(holdingAccount, pledge.getInvestmentSecurity(), pledge.getQuantity(), context.getCollateralBalance());
				detailList.addAll(buildAccountingPositionTransferDetail(getAccountingPositionClosingService().getAccountingPositionClosingLotList(cmd)));
			}
		}
		transfer.setDetailList(detailList);
		return transfer;
	}


	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> buildFuturesCollateralPositionMap(CollateralBalancePledgedTransactionsFuturesWorkflowContext context, boolean holdingAccount) {
		List<AccountingBalanceValue> accountingBalanceValueList = getAvailableCollateralList(context.getCollateralBalance(), holdingAccount);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> futuresCollateralPositionMap = new LinkedHashMap<>();
		getMaximumTransferableCashValue(context, futuresCollateralPositionMap, holdingAccount);
		for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(accountingBalanceValueList)) {
			BigDecimal positionValue = getAccountingPositionClosingService().getAccountingPositionClosingLotMarketValue(buildAccountingPositionClosingCommand(accountingBalanceValue.getHoldingInvestmentAccount(), accountingBalanceValue.getInvestmentSecurity(), accountingBalanceValue.getQuantity(), context.getCollateralBalance()));
			BigDecimal haircutValue = getCollateralHaircutDefinitionService().getCollateralHaircutValue(context.getCollateralBalance().getHoldingInvestmentAccount(), accountingBalanceValue.getInvestmentSecurity(), false, context.getCollateralBalance().getBalanceDate());
			FuturesCollateralPositionPledge positionPledge = new FuturesCollateralPositionPledge(accountingBalanceValue.getInvestmentSecurity(), accountingBalanceValue.getQuantity(), positionValue, haircutValue);
			CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(positionPledge, futuresCollateralPositionMap);
		}
		return futuresCollateralPositionMap;
	}


	private List<AccountingPositionTransferDetail> buildAccountingPositionTransferDetail(List<AccountingPositionClosing> positionList) {
		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		for (AccountingPositionClosing position : CollectionUtils.getIterable(positionList)) {
			AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
			transferDetail.setSecurity(position.getInvestmentSecurity());
			transferDetail.setQuantity(position.getRemainingQuantity());
			transferDetail.setOriginalPositionOpenDate(position.getOriginalTransactionDate());
			transferDetail.setExchangeRateToBase(position.getExchangeRateToBase());
			transferDetail.setCostPrice(position.getPrice());
			transferDetail.setTransferPrice(position.getPrice());
			transferDetail.setPositionCostBasis(position.getRemainingCostBasis());
			transferDetail.setExistingPosition(getAccountingTransactionService().getAccountingTransaction(position.getId()));
			detailList.add(transferDetail);
		}
		return detailList;
	}


	private AccountingPositionTransfer buildAccountingPositionTransfer(CollateralBalancePledgedTransactionsFuturesWorkflowContext context, AccountingPositionTransferType transferType) {
		boolean isReturn = StringUtils.isEqual(transferType.getName(), AccountingPositionTransferType.COLLATERAL_TRANSFER_RETURN);
		AccountingPositionTransfer accountingPositionTransfer = new AccountingPositionTransfer();
		// com.clifton.accounting.positiontransfer.booking.AccountingPositionTransferBookingRule.getJournalDetailAccountingAccount
		accountingPositionTransfer.setCollateralTransfer(transferType.isCollateralPosting());
		accountingPositionTransfer.setUseOriginalTransactionDate(true);
		accountingPositionTransfer.setFromClientInvestmentAccount(context.getCollateralBalance().getClientInvestmentAccount());
		accountingPositionTransfer.setToClientInvestmentAccount(context.getCollateralBalance().getClientInvestmentAccount());
		accountingPositionTransfer.setFromHoldingInvestmentAccount(isReturn ? context.getCollateralBalance().getHoldingInvestmentAccount() : context.getCollateralBalance().getCollateralInvestmentAccount());
		accountingPositionTransfer.setToHoldingInvestmentAccount(isReturn ? context.getCollateralBalance().getCollateralInvestmentAccount() : context.getCollateralBalance().getHoldingInvestmentAccount());
		accountingPositionTransfer.setNote((isReturn ? "Return" : "Post") + " of Client Owned Collateral from Collateral Balance Date [" + DateUtils.fromDate(context.getCollateralBalance().getBalanceDate()) + "]");
		accountingPositionTransfer.setSourceFkFieldId(context.getCollateralBalance().getId());
		accountingPositionTransfer.setSourceSystemTable(getSystemSchemaService().getSystemTableByName(CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME));
		accountingPositionTransfer.setType(transferType);
		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(context.getCollateralBalance().getBalanceDate()), 1);
		accountingPositionTransfer.setTransactionDate(nextBusinessDay);
		accountingPositionTransfer.setSettlementDate(nextBusinessDay);

		return accountingPositionTransfer;
	}


	private AccountingPositionClosingCommand buildAccountingPositionClosingCommand(InvestmentAccount holdingAccount, InvestmentSecurity investmentSecurity, BigDecimal overrideQuantity, CollateralBalance collateralBalance) {
		AccountingPositionClosingCommand accountingPositionClosingCommand = new AccountingPositionClosingCommand();
		accountingPositionClosingCommand.setClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());
		accountingPositionClosingCommand.setHoldingInvestmentAccountId(holdingAccount.getId());
		accountingPositionClosingCommand.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(collateralBalance.getBalanceDate()), 1));
		accountingPositionClosingCommand.setCollateral(MathUtils.isEqual(collateralBalance.getHoldingInvestmentAccount().getId(), holdingAccount.getId()));
		accountingPositionClosingCommand.setInvestmentSecurityId(investmentSecurity.getId());
		AccountingPositionClosingSecurityQuantity accountingPositionClosingSecurityQuantity = new AccountingPositionClosingSecurityQuantity();
		accountingPositionClosingSecurityQuantity.setInvestmentSecurityId(investmentSecurity.getId());
		accountingPositionClosingSecurityQuantity.setQuantity(overrideQuantity);
		List<AccountingPositionClosingSecurityQuantity> accountingPositionClosingSecurityQuantities = new ArrayList<>();
		accountingPositionClosingSecurityQuantities.add(accountingPositionClosingSecurityQuantity);
		accountingPositionClosingCommand.setSecurityQuantityList(accountingPositionClosingSecurityQuantities);
		accountingPositionClosingCommand.setOrder(MathUtils.isEqual(collateralBalance.getHoldingInvestmentAccount().getId(), holdingAccount.getId()) ? AccountingPositionOrders.FIFO : AccountingPositionOrders.LIFO);
		return accountingPositionClosingCommand;
	}


////////////////////////////////////////////////////////////////////////////
//////////             Custom Comparator                          //////////
////////////////////////////////////////////////////////////////////////////


	private class FuturesCollateralPositionPledgeComparator implements Comparator<FuturesCollateralPositionPledge> {

		private final Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral;


		@Override
		public int compare(FuturesCollateralPositionPledge o1, FuturesCollateralPositionPledge o2) {

			if (DateUtils.compare(o2.getInvestmentSecurity().getEndDate(), o1.getInvestmentSecurity().getEndDate(), false) == 0) {
				if (MathUtils.compare(o1.getHaircutValue(), o2.getHaircutValue()) == 0) {
					// prioritize investmentSecurities that are already being used.
					boolean o1Posted = this.brokerHeldCollateral.containsKey(o1.getInvestmentSecurity());
					boolean o2Posted = this.brokerHeldCollateral.containsKey(o2.getInvestmentSecurity());
					if (o1Posted == o2Posted) {
						//Biggest Quantity first
						return MathUtils.compare(o2.getQuantity(), o1.getQuantity());
					}
					else if (o1Posted) {
						return -1;
					}
					else {
						return 1;
					}
				}
				//Lowest Haircut First
				return MathUtils.compare(o1.getHaircutValue(), o2.getHaircutValue());
			}
			//Furthest out endDate first
			return DateUtils.compare(o2.getInvestmentSecurity().getEndDate(), o1.getInvestmentSecurity().getEndDate(), false);
		}


		public FuturesCollateralPositionPledgeComparator(Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral) {
			this.brokerHeldCollateral = brokerHeldCollateral;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                   Getters And Setters                  //////////
	////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public AccountingPositionClosingService getAccountingPositionClosingService() {
		return this.accountingPositionClosingService;
	}


	public void setAccountingPositionClosingService(AccountingPositionClosingService accountingPositionClosingService) {
		this.accountingPositionClosingService = accountingPositionClosingService;
	}


	public CollateralHaircutDefinitionService getCollateralHaircutDefinitionService() {
		return this.collateralHaircutDefinitionService;
	}


	public void setCollateralHaircutDefinitionService(CollateralHaircutDefinitionService collateralHaircutDefinitionService) {
		this.collateralHaircutDefinitionService = collateralHaircutDefinitionService;
	}
}
