package com.clifton.collateral.balance.rebuild.processor;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.business.contract.BusinessContract;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceFactory;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.calculator.BaseCollateralIndependentAmountCalculator;
import com.clifton.collateral.balance.calculator.CollateralIndependentAmountCalculator;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.workflow.assignment.WorkflowAssignmentService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>BaseCollateralBalanceRebuildProcessor</code> ...
 *
 * @author Mary Anderson
 */
public abstract class BaseCollateralBalanceRebuildProcessor<S extends CollateralStrategyPosition> implements CollateralBalanceRebuildProcessor<S> {

	private CollateralType collateralType;

	private AccountingPositionHandler accountingPositionHandler;
	private AccountingPositionService accountingPositionService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private CollateralBalanceService collateralBalanceService;
	private CollateralService collateralService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemColumnValueHandler systemColumnValueHandler;
	private SystemBeanService systemBeanService;

	private WorkflowAssignmentService workflowAssignmentService;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	public abstract void applyCollateralTypeOverrides(CollateralBalance balance);

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionDaily> appendAccountingPositionDaily(InvestmentAccount holdingAccount, Date balanceDate, List<AccountingPositionDaily> positionDailyList) {
		return positionDailyList;
	}


	/**
	 * Looks up holding account...first checks contract clause (if contract exists) otherwise checks holding account custom column value.
	 */
	@Override
	public BigDecimal getCollateralTransferAmountRounded(int holdingInvestmentAccountId, BigDecimal amount) {
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(holdingInvestmentAccountId);
		BigDecimal roundingValue = getCollateralTransferRoundingValue(holdingAccount);
		return roundCollateralChangeAmount(amount, roundingValue);
	}

	/////////////////////////////////////////////////////////////////
	////////                Rebuild Methods                 /////////
	/////////////////////////////////////////////////////////////////


	@Override
	public boolean processAccount(CollateralBalanceRebuildCommand rebuildCommand) {
		return true;
	}


	@Override
	public boolean rebuildCollateralBalance(CollateralBalanceRebuildCommand command, List<CollateralBalance> existingList) {
		ValidationUtils.assertNotNull(command.getHoldingAccount(), "CollateralBalanceRebuildCommand object is missing a holding account.");
		InvestmentAccount holdingAccount = command.getHoldingAccount();

		// Positions for the Holding Account
		appendAccountingPositionList(command);

		// Collateral Positions for the Holding Account
		appendCollateralPositionList(command);

		// Collateral Positions for the Holding Account
		appendCollateralEndingPositionList(command);

		boolean localCollateral = isCollateralInLocalCurrency(holdingAccount);
		// Local currency - one record per currency in the account
		if (localCollateral) {
			// Setup config object to split positions by currency and determine all currencies that apply
			command.setupLocalCurrencySupport();
			boolean result = false;
			for (InvestmentSecurity currency : CollectionUtils.getIterable(command.getCurrencyList())) {
				CollateralBalance existing = CollectionUtils.getOnlyElement(BeanUtils.filter(existingList, CollateralBalance::getCollateralCurrency, currency));
				// Set the current currency on the config
				command.setCurrency(currency);
				boolean thisResult = rebuildCollateralBalanceForAccountAndCurrency(command, existing);
				if (thisResult) {
					result = thisResult;
				}
			}
			return result;
		}
		// don't rebuild balances on the group-level collateral balance entity, it is the sum of the holding account specific collateral balance entities.
		final List<CollateralBalance> collateralBalances = CollectionUtils.getStream(existingList).filter(cb -> !cb.isParentCollateralBalance()).collect(Collectors.toList());
		// Not local currency - passes null for currency security
		return rebuildCollateralBalanceForAccountAndCurrency(command, CollectionUtils.getFirstElement(collateralBalances));
	}


	@Override
	public List<S> calculateRequiredCollateralList(List<? extends AccountingPositionInfo> accountingPositionDailyList, List<CollateralPosition> collateralPositionList, Date positionDate, InvestmentAccount holdingAccount, boolean showExplanation) {
		return Collections.emptyList();
	}


	private boolean rebuildCollateralBalanceForAccountAndCurrency(CollateralBalanceRebuildCommand command, CollateralBalance existing) {
		boolean baseCurrency = (command.getCurrency() == null || command.getCurrency().equals(command.getHoldingAccount().getBaseCurrency()));
		// set to existing; if null, then create a new balance and set initial values.
		CollateralBalance balance;
		if (existing != null) {
			if (command.isSkipExisting()) {
				return false;
			}
			balance = getCollateralBalanceService().getCollateralBalance(existing.getId());
		}
		else {
			balance = CollateralBalanceFactory.createCollateralBalance(getCollateralType());
			balance.setBalanceDate(command.getBalanceDate());
			balance.setHoldingInvestmentAccount(command.getHoldingAccount());
			balance.setCollateralCurrency(command.getCurrency());
		}
		//We always reset the collateral type because they could change (e.g. switching Options Escrow to Options Margin) so we want to ensure it is properly updated.
		balance.setCollateralType(getCollateralType());

		// For those holding accounts with a contract (OTC & REPO holding accounts would have them, copies contract clauses as Thresholds, Min Transfer Amounts, etc.)
		// For Non OTC the holding account doesn't have a contract so wouldn't do anything for those cases
		populateContractFieldsOnCollateralBalance(balance);

		applyAccountingPositionsToCollateralBalance(balance, command);

		BigDecimal postedCollateral = getPostedCollateral(command, baseCurrency);
		BigDecimal endingPostedCollateral = getEndingPostedCollateral(command, baseCurrency);
		BigDecimal postedCollateralAdjusted = getPostedCollateralAdjusted(command, baseCurrency);

		// Negative Cash Collateral balance means Counterparty posted collateral to the client account
		if (MathUtils.isGreaterThan(postedCollateral, BigDecimal.ZERO)) {
			balance.setPostedCounterpartyCollateralValue(BigDecimal.ZERO);
			balance.setPostedCounterpartyCollateralHaircutValue(BigDecimal.ZERO);

			balance.setPostedCollateralValue(postedCollateral);
			balance.setPostedCollateralHaircutValue(postedCollateralAdjusted);
		}
		else {
			// Make Values Positive
			balance.setPostedCounterpartyCollateralValue(MathUtils.abs(postedCollateral));
			balance.setPostedCounterpartyCollateralHaircutValue(MathUtils.abs(postedCollateralAdjusted));

			balance.setPostedCollateralValue(BigDecimal.ZERO);
			balance.setPostedCollateralHaircutValue(BigDecimal.ZERO);
		}

		if (MathUtils.isGreaterThan(endingPostedCollateral, BigDecimal.ZERO)) {
			balance.setEndingPostedCounterpartyCollateralValue(BigDecimal.ZERO);
			balance.setEndingPostedCollateralValue(endingPostedCollateral);
		}
		else {
			// Make Values Positive
			balance.setEndingPostedCounterpartyCollateralValue(MathUtils.abs(endingPostedCollateral));
			balance.setEndingPostedCollateralValue(BigDecimal.ZERO);
		}

		balance.setCashCollateralValue(getCashCollateralValue(command));

		applyCollateralTypeOverrides(balance);
		//save the balance so an ID is populated for new entries, but do not trigger rules via the observers yet
		CollateralBalance collateralBalancePopulated = DaoUtils.executeWithAllObserversDisabled(() -> getCollateralBalanceService().saveCollateralBalance(balance));

		//build group account balance or update balance with previous note
		collateralBalancePopulated = applyParentChildCollateralBalanceAndNote(collateralBalancePopulated);

		//Reconcile the internal collateral balance values with external.
		//Commenting out for now until reconciliation functionality is complete.
		//reconcileCollateralBalance(collateralBalancePopulated);

		//save the balance to trigger Collateral Balance Rules to run.
		collateralBalancePopulated = getCollateralBalanceService().saveCollateralBalance(collateralBalancePopulated);

		// reset the workflow for Collateral Types that have it
		if (collateralBalancePopulated.getCollateralType().getWorkflow() != null) {
			getWorkflowAssignmentService().reassignWorkflow("CollateralBalance", collateralBalancePopulated.getId());
		}
		return true;
	}


	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	/**
	 * Calculates the independentAmount and positionsMarketValue for the supplied positions and applied them to the CollateralBalance.
	 */
	protected void applyAccountingPositionsToCollateralBalance(CollateralBalance balance, CollateralBalanceRebuildCommand command) {
		// get Positions Market Value
		boolean independentAmountAllowed = BooleanUtils.isTrue(getBusinessContractCustomFieldValue(balance.getHoldingInvestmentAccount().getBusinessContract(), BusinessContract.INDEPENDENT_AMOUNT_ALLOWED_CUSTOM_COLUMN));
		BigDecimal independentAmountValue = BigDecimal.ZERO;
		BigDecimal independentAmountPercent = (BigDecimal) getBusinessContractCustomFieldValue(balance.getHoldingInvestmentAccount().getBusinessContract(), BusinessContract.INDEPENDENT_AMOUNT_PERCENT_CUSTOM_COLUMN);
		BigDecimal positionsMarketValue = BigDecimal.ZERO;
		CollateralIndependentAmountCalculator amountCalculator = getIndependentAmountCalculator(balance);
		FxRateLookupCache fxRateLookupCache = new FxRateLookupCache(getMarketDataExchangeRatesApiService());
		for (AccountingPosition position : CollectionUtils.getIterable(command.getPositionList())) {
			InvestmentSecurity security = position.getInvestmentSecurity();
			if (security.getInstrument().getHierarchy().isCollateralUsed()) {
				BigDecimal marketValue = getBaseMarketValue(balance, position, fxRateLookupCache, command.getCollateralType().isUseSettlementDate(), false);
				positionsMarketValue = MathUtils.add(positionsMarketValue, marketValue);
				if (independentAmountAllowed) {
					independentAmountValue = MathUtils.add(independentAmountValue, amountCalculator.calculateIndependentAmount(balance, position, independentAmountPercent));
				}
			}
		}
		balance.setIndependentAmount(independentAmountValue);
		balance.setPositionsMarketValue(positionsMarketValue);
	}


	/**
	 * Convert market values to the base currency.  Market values from grouped accounts can be an accumulation of positions with different currencies, convert all market
	 * values to the same currency as the primary group member.
	 */
	private BigDecimal adjustToBaseCurrency(CollateralBalance balance, AccountingPosition position, BigDecimal marketValue, FxRateLookupCache cache) {
		InvestmentSecurity baseCurrency = getBaseAccountCurrency(balance);
		InvestmentSecurity currency = InvestmentUtils.getClientAccountBaseCurrency(position);
		if (!currency.getId().equals(baseCurrency.getId())) {
			if (!MathUtils.isZeroOrOne(marketValue)) {
				HoldingAccount holdingAccount = position.getHoldingInvestmentAccount().toHoldingAccount();
				BigDecimal fxRate = cache.getExchangeRate(holdingAccount, currency.getSymbol(), baseCurrency.getSymbol(), balance.getBalanceDate(), true);

				return MathUtils.multiply(marketValue, fxRate, marketValue.scale());
			}
		}
		return marketValue;
	}


	private CollateralIndependentAmountCalculator getIndependentAmountCalculator(CollateralBalance balance) {
		Integer independentAmountCalculatorBeanId = (Integer) getBusinessContractCustomFieldValue(balance.getHoldingInvestmentAccount().getBusinessContract(), BusinessContract.INDEPENDENT_AMOUNT_CALCULATOR_CUSTOM_COLUMN);
		if (independentAmountCalculatorBeanId != null) {
			return (CollateralIndependentAmountCalculator) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(independentAmountCalculatorBeanId));
		}
		return new BaseCollateralIndependentAmountCalculator(getInvestmentSecurityUtilHandler());
	}


	/**
	 * Calculate the market value from the provided Collateral Balance.
	 * Allows derived classes to change the market value calculations.
	 */
	protected BigDecimal getBaseMarketValue(CollateralBalance balance, AccountingPosition position, FxRateLookupCache cache, boolean useSettlementDate, boolean includeAccrualLegPayments) {
		BigDecimal marketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(position, balance.getBalanceDate(), useSettlementDate, null, includeAccrualLegPayments);
		marketValue = adjustToBaseCurrency(balance, position, marketValue, cache);
		return marketValue;
	}


	/**
	 * Calculates the market value of the posted collateral be summarizing the marketValue (baseCurrency == true) or marketValueLocal (baseCurrency == false) depending on the
	 * baseCurrency flag.
	 */
	protected BigDecimal getPostedCollateral(CollateralBalanceRebuildCommand command, boolean baseCurrency) {
		return CoreMathUtils.sumProperty(command.getCollateralPositionList(),
				baseCurrency ? CollateralPosition::getCollateralMarketValue : CollateralPosition::getCollateralMarketValueLocal);
	}


	/**
	 * Calculates the adjusted market value of the posted collateral be summarizing the marketValueAdjusted (baseCurrency == true) or marketValueLocalAdjusted (baseCurrency ==
	 * false) depending on the baseCurrency flag.
	 */
	protected BigDecimal getPostedCollateralAdjusted(CollateralBalanceRebuildCommand command, boolean baseCurrency) {
		return CoreMathUtils.sumProperty(command.getCollateralPositionList(), baseCurrency ? CollateralPosition::getCollateralMarketValueAdjusted : CollateralPosition::getCollateralMarketValueLocalAdjusted);
	}


	/**
	 * Helper method for rounding collateral change amount.
	 */
	protected BigDecimal roundCollateralChangeAmount(BigDecimal amount, BigDecimal roundingValue) {
		// If either number to round or rounding is null or 0, then no rounding needs to take place
		if (MathUtils.isNullOrZero(amount) || MathUtils.isNullOrZero(roundingValue)) {
			return amount;
		}

		// Divide Amount By Rounding Value
		BigDecimal roundedAmount = MathUtils.round(MathUtils.divide(amount, roundingValue), 0, BigDecimal.ROUND_HALF_UP);
		return MathUtils.multiply(roundingValue, roundedAmount);
	}


	/**
	 * Return the collateral positions for a specific date, holding account and position list.
	 * <p>
	 * NOTE: The resulting positions are as of the settlement date.
	 */
	protected List<CollateralPosition> getCollateralPositionList(CollateralBalanceRebuildCommand command, Date balanceDate, List<AccountingPosition> positionList) {
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionSettlementDate(balanceDate);
		positionCommand.setHoldingInvestmentAccountId(command.getHoldingAccountId());
		return getCollateralService().getCollateralPositionListUsingPositions(positionCommand, positionList);
	}


	/**
	 * Adds the collateral positions the rebuild command.
	 */
	protected void appendCollateralPositionList(CollateralBalanceRebuildCommand command) {
		command.setCollateralPositionList(getCollateralPositionList(command, command.getBalanceDate(), command.getPositionList()));
	}


	/**
	 * Adds the accounting positions the rebuild command.
	 */
	protected void appendAccountingPositionList(CollateralBalanceRebuildCommand command) {
		command.setPositionList(
				getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingPositionCommand
						.onPositionSettlementDate(command.getBalanceDate())
						.forHoldingAccount(command.getHoldingAccountId())
				)
		);
	}


	/**
	 * Adds the ending collateral positions the rebuild command.
	 * <p>
	 * NOTE: This is only implemented by processors that need the ending positions.
	 */
	protected void appendCollateralEndingPositionList(CollateralBalanceRebuildCommand config) {
		// do nothing for the default implementation
	}


	/**
	 * For the provided positions, update the FX Rate based on the conversion from the investment security's trading
	 * currency to the provided currency.
	 * The base currency is based on logic contained in {@link com.clifton.accounting.gl.position.daily.AccountingPositionDailyServiceImpl#generateAccountingPositionDailyList}
	 */
	protected void updatePositionFxRate(List<AccountingPositionDaily> positionDailyList, InvestmentSecurity toCurrency) {
		FxRateLookupCache cache = new FxRateLookupCache(getMarketDataExchangeRatesApiService());
		for (AccountingPositionDaily accountingPositionDaily : positionDailyList) {
			// see: AccountingPositionDailyServiceImpl#generateAccountingPositionDailyList
			String fromCCY = accountingPositionDaily.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol();
			String toCCY = toCurrency.getSymbol();
			BigDecimal fxRate = BigDecimal.ONE;
			if (!fromCCY.equals(toCCY)) {
				HoldingAccount holdingAccount = accountingPositionDaily.getAccountingTransaction().getHoldingInvestmentAccount().toHoldingAccount();
				fxRate = cache.getExchangeRate(holdingAccount, fromCCY, toCCY, accountingPositionDaily.getPositionDate(), true);
			}
			accountingPositionDaily.setMarketFxRate(fxRate);
		}
	}


	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	/**
	 * Determine the base currency.  By default the base currency is the holding account's base currency.
	 * In some cases, positions can be grouped from different holding accounts and currencies,
	 * this returns the currency of the primary group member.
	 */
	private InvestmentSecurity getBaseAccountCurrency(CollateralBalance collateralBalance) {
		InvestmentSecurity results = collateralBalance.getHoldingInvestmentAccount().getBaseCurrency();

		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		searchForm.setAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
		List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);

		InvestmentAccountGroupAccount accountGroupAccount = CollectionUtils.getOnlyElement(groupAccountList);
		if (accountGroupAccount != null) {
			InvestmentAccountGroupAccount primaryAccountGroupAccount = getInvestmentAccountGroupService().getPrimaryInvestmentAccountGroupAccountByGroup(accountGroupAccount.getReferenceOne().getId());
			if (primaryAccountGroupAccount != null) {
				results = primaryAccountGroupAccount.getReferenceTwo().getBaseCurrency();
			}
		}
		return results;
	}


	private CollateralBalance getPreviousCollateralBalance(CollateralBalance balance) {
		//Purposely get the previous weekday rather than business day.
		//This is so notes added to balances on Holidays are not missed.
		Date previousDate = DateUtils.getPreviousWeekday(balance.getBalanceDate());
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(previousDate);
		searchForm.setHoldingInvestmentAccountId(balance.getHoldingInvestmentAccount().getId());
		if (balance.isParentCollateralBalance()) {
			searchForm.setParentCollateralBalance(balance.isParentCollateralBalance());
		}
		if (balance.getCollateralCurrency() != null) {
			searchForm.setCollateralCurrencyId(balance.getCollateralCurrency().getId());
		}
		List<CollateralBalance> balanceList = getCollateralBalanceService().getCollateralBalanceList(searchForm);
		if (!CollectionUtils.isEmpty(balanceList)) {
			if (balanceList.size() == 1) {
				return CollectionUtils.getOnlyElement(balanceList);
			}
			LogUtils.warn(getClass(), "Cannot copy CollateralBalance note because multiple balances exist for [" + balance.getHoldingInvestmentAccount() + "] on balance date [" + previousDate + "].");
		}
		return null;
	}


	private CollateralBalance applyParentChildCollateralBalanceAndNote(CollateralBalance balance) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setAccountId(balance.getHoldingInvestmentAccount().getId());
		searchForm.setGroupTypeName(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		InvestmentAccountGroupAccount accountGroupAccount = CollectionUtils.getFirstElement(getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm));
		if (accountGroupAccount != null) {
			List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(
					new InvestmentAccountGroupAccountSearchForm(accountGroupAccount.getReferenceOne().getId()));
			InvestmentAccountGroupAccount primary = CollectionUtils.getOnlyElement(BeanUtils.filter(groupAccountList, InvestmentAccountGroupAccount::isPrimary));
			ValidationUtils.assertNotNull(primary, "Cannot set CollateralBalance parent using investment group [" + accountGroupAccount.getReferenceOne().getName() + "] because no primary account is set.");

			CollateralBalance groupBalance = getGroupCollateralBalance(balance.getBalanceDate(), primary.getReferenceTwo().getId(),
					balance.getCollateralCurrency() == null ? null : balance.getCollateralCurrency().getId());

			List<CollateralBalance> balanceList = getGroupCollateralBalanceList(balance.getBalanceDate(), primary.getReferenceOne().getId());

			//TODO - issue with existing balance not being recreated - can cause illegitimate discrepancies when positions change.
			if (groupBalance == null) {
				groupBalance = BeanUtils.cloneBean(balance, false, false);
				//Clear the workflow state before the clone so object starts fresh.
				groupBalance.setWorkflowState(null);
				groupBalance.setWorkflowStatus(null);
				groupBalance.setHoldingInvestmentAccount(primary.getReferenceTwo());
				groupBalance.setParentCollateralBalance(true);
				groupBalance.setExternalCollateralAmount(null);
				groupBalance.setExternalCollateralRequirement(null);
				groupBalance.setParent(null);
			}

			groupBalance.setPositionsMarketValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getPositionsMarketValue));
			groupBalance.setPostedCollateralValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getPostedCollateralValue));
			groupBalance.setPostedCounterpartyCollateralValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getPostedCounterpartyCollateralValue));
			groupBalance.setPostedCollateralHaircutValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getPostedCollateralHaircutValue));
			groupBalance.setPostedCounterpartyCollateralHaircutValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getPostedCounterpartyCollateralHaircutValue));
			groupBalance.setCollateralChangeAmount(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getCollateralChangeAmount));
			groupBalance.setCounterpartyCollateralChangeAmount(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getCounterpartyCollateralChangeAmount));
			groupBalance.setCollateralRequirement(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getCollateralRequirement));
			groupBalance.setTransferAmount(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getTransferAmount));
			groupBalance.setSecuritiesTransferAmount(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getSecuritiesTransferAmount));
			groupBalance.setCashCollateralValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getCashCollateralValue));
			groupBalance.setEndingPostedCollateralValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getEndingPostedCollateralValue));
			groupBalance.setEndingPostedCounterpartyCollateralValue(CoreMathUtils.sumProperty(balanceList, CollateralBalance::getEndingPostedCounterpartyCollateralValue));

			// get the note from the previous balance
			CollateralBalance previousBalance = getPreviousCollateralBalance(groupBalance);
			if (previousBalance != null) {
				setBalanceNote(groupBalance, previousBalance.getNote());
			}
			applyCollateralTypeOverrides(groupBalance);

			final CollateralBalance finalBalance = groupBalance;
			//save the balance so an ID is populated for new entries, but do not trigger rules via the observers yet
			groupBalance = DaoUtils.executeWithAllObserversDisabled(() -> getCollateralBalanceService().saveCollateralBalance(finalBalance));

			for (CollateralBalance childBalance : CollectionUtils.getIterable(balanceList)) {
				if (!groupBalance.equals(childBalance.getParent())) {
					childBalance.setParent(groupBalance);
					// get the note from the previous balance
					CollateralBalance pb = getPreviousCollateralBalance(childBalance);
					if (pb != null) {
						setBalanceNote(childBalance, pb.getNote());
					}
					//save the child balance to set it's parent, but do not trigger rules via observers yet
					DaoUtils.executeWithAllObserversDisabled(() -> getCollateralBalanceService().saveCollateralBalance(childBalance));
				}
			}
			return groupBalance;
		}
		else {
			CollateralBalance previousBalance = getPreviousCollateralBalance(balance);
			if ((previousBalance != null) && !StringUtils.isEmpty(previousBalance.getNote())) {
				setBalanceNote(balance, previousBalance.getNote());
			}
			return balance;
		}
	}


	private void setBalanceNote(CollateralBalance balance, String note) {
		if (copyPreviousCollateralBalanceNote()) {
			balance.setNote(note);
		}
	}


	//Allows collateral type specific rebuild processors to override copy note functionality
	public boolean copyPreviousCollateralBalanceNote() {
		return true;
	}


	private List<CollateralBalance> getGroupCollateralBalanceList(Date balanceDate, int groupId) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(balanceDate);
		searchForm.setAccountGroupId(groupId);
		searchForm.setParentCollateralBalance(false);
		return getCollateralBalanceService().getCollateralBalanceList(searchForm);
	}


	private CollateralBalance getGroupCollateralBalance(Date balanceDate, int primaryAccountId, Integer currencyId) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(balanceDate);
		searchForm.setHoldingInvestmentAccountId(primaryAccountId);
		searchForm.setParentCollateralBalance(true);
		searchForm.setCollateralCurrencyId(currencyId);
		return CollectionUtils.getOnlyElement(getCollateralBalanceService().getCollateralBalanceList(searchForm));
	}


	private BigDecimal getCashCollateralValue(CollateralBalanceRebuildCommand command) {
		BigDecimal cashCollateral = BigDecimal.ZERO;
		for (CollateralPosition position : command.getCollateralPositionList()) {
			if (position.getAccountingAccount().isCash()) {
				cashCollateral = MathUtils.add(cashCollateral, position.getCollateralMarketValue());
			}
		}
		return cashCollateral;
	}


	private BigDecimal getEndingPostedCollateral(CollateralBalanceRebuildCommand command, boolean baseCurrency) {
		return CoreMathUtils.sumProperty(command.getEndingCollateralPositionList(), baseCurrency ? CollateralPosition::getCollateralMarketValue : CollateralPosition::getCollateralMarketValueLocal);
	}


	private void populateContractFieldsOnCollateralBalance(CollateralBalance balance) {
		BusinessContract contract = balance.getHoldingInvestmentAccount().getBusinessContract();
		if (contract != null) {
			// Copy Thresholds & Minimum Transfer Amounts from ISDA
			// Note: That each individual value lookup is cached, so for now doing one column at a time instead of getting the
			// full list and then filtering through the list.  May want to try the list lookup if this is slow.
			balance.setThresholdAmount((BigDecimal) getBusinessContractCustomFieldValue(contract, BusinessContract.CONTRACT_COLLATERAL_THRESHOLD_CUSTOM_COLUMN));
			balance.setThresholdCounterpartyAmount((BigDecimal) getBusinessContractCustomFieldValue(contract, BusinessContract.CONTRACT_COUNTERPARTY_COLLATERAL_THRESHOLD_CUSTOM_COLUMN));
			balance.setMinTransferAmount((BigDecimal) getBusinessContractCustomFieldValue(contract, BusinessContract.CONTRACT_MIN_TRANSFER_AMOUNT_CUSTOM_COLUMN));
			balance.setMinTransferCounterpartyAmount((BigDecimal) getBusinessContractCustomFieldValue(contract, BusinessContract.CONTRACT_COUNTERPARTY_MIN_TRANSFER_AMOUNT_CUSTOM_COLUMN));
			balance.setContractRounding(getCollateralTransferRoundingValue(balance.getHoldingInvestmentAccount()));
		}
	}


	private BigDecimal getCollateralTransferRoundingValue(InvestmentAccount holdingAccount) {
		BigDecimal roundingValue = null;
		if (holdingAccount != null && holdingAccount.getBusinessContract() != null) {
			roundingValue = (BigDecimal) getBusinessContractCustomFieldValue(holdingAccount.getBusinessContract(), BusinessContract.CONTRACT_ROUNDING_CUSTOM_COLUMN);
		}
		if (roundingValue == null) {
			Integer value = (Integer) getHoldingInvestmentAccountCustomFieldValue(holdingAccount, InvestmentAccount.HOLDING_ACCOUNT_COLLATERAL_TRANSFER_ROUNDING);
			if (value != null) {
				roundingValue = BigDecimal.valueOf(value);
			}
		}
		return roundingValue;
	}


	/**
	 * Returns true if collateral for the specified account should be performed in local currency of each position: separate collateral balance per currency denomination of
	 * holdings.
	 */
	private boolean isCollateralInLocalCurrency(InvestmentAccount account) {
		// CURRENTLY ONLY APPLIES TO NON OTC ACCOUNTS:
		if (!getCollateralType().isCollateralInLocalCurrencySupported()) {
			return false;
		}
		// account that uses CAD and USD collateral: 157-85098
		return BooleanUtils.isTrue(getHoldingInvestmentAccountCustomFieldValue(account, InvestmentAccount.HOLDING_ACCOUNT_M2M_LOCAL_CURRENCY));
	}


	@Override
	public List<AccountingPositionDaily> calculateRequiredCollateralForAccountingPositionDailyList(List<AccountingPositionDaily> accountingPositionDailyList, Date date) {
		return accountingPositionDailyList;
	}

	/////////////////////////////////////////////////////////////////
	//////////        Custom Field Lookup Methods         ///////////
	/////////////////////////////////////////////////////////////////


	protected Object getHoldingInvestmentAccountCustomFieldValue(InvestmentAccount account, String fieldName) {
		return getCustomFieldValue(account, InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME, fieldName);
	}


	protected Object getBusinessContractCustomFieldValue(BusinessContract contract, String fieldName) {
		return getCustomFieldValue(contract, BusinessContract.CONTRACT_CLAUSE_COLUMN_GROUP_NAME, fieldName);
	}


	private Object getCustomFieldValue(SystemColumnCustomAware bean, String groupName, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(bean, groupName, fieldName, true);
	}


	///////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods          //////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public CollateralType getCollateralType() {
		return this.collateralType;
	}


	@Override
	public void setCollateralType(CollateralType collateralType) {
		this.collateralType = collateralType;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public WorkflowAssignmentService getWorkflowAssignmentService() {
		return this.workflowAssignmentService;
	}


	public void setWorkflowAssignmentService(WorkflowAssignmentService workflowAssignmentService) {
		this.workflowAssignmentService = workflowAssignmentService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
