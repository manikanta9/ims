package com.clifton.collateral.balance;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import javax.persistence.Table;
import java.math.BigDecimal;


/**
 * @author terrys
 */
@Table(name = "CollateralBalance")
public class OptionsMarginCollateralBalance extends CollateralBalance {

	////////////////////////////////////////////////////////////////////////////
	//////////      Options Collateral Computed Getter Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getOptionsCollateralRequirement() {
		if (InvestmentAccountType.PORTFOLIO_MARGIN.equals(getHoldingInvestmentAccount().getType().getName())) {
			return getCollateralRequirement();
		}
		else {
			return MathUtils.add(getCollateralRequirement(), getPostedHaircutAmount());
		}
	}


	public BigDecimal getPostedHaircutAmount() {
		return MathUtils.subtract(getPostedCollateralValue(), getPostedCollateralHaircutValue());
	}


	/**
	 * The positions market value (minus the amount for long options position value adjusted based on time until expiration) plus cash and collateral cash.
	 */
	public BigDecimal getOptionsPositionsMarketValue() {
		return MathUtils.add(getPositionsMarketValue(), getTotalCashValue());
	}


	public BigDecimal getExcessOptionsEquityCollateral() {
		return MathUtils.subtract(getOptionsPositionsMarketValue(), getOptionsCollateralRequirement());
	}


	public BigDecimal getExcessOptionsEquityCollateralPercentage() {
		return MathUtils.isNullOrZero(getOptionsCollateralRequirement()) ? BigDecimal.ZERO :
				MathUtils.round(MathUtils.divide(getExcessOptionsEquityCollateral(), getOptionsCollateralRequirement()).multiply(new BigDecimal("100")), 2);
	}


	/**
	 * If Deficit, post additional to 107.5% of requirement.
	 * If Excess and over 115.0% collateralized, pull back 7.5% to reach 107.5% collateralized.
	 * If Excess and requirement = 0, pull back all collateral.
	 */
	public BigDecimal getOptionsTargetMovement() {
		BigDecimal difference = getExcessOptionsEquityCollateral();
		// negative - need more collateral
		if (MathUtils.isNegative(difference)) {
			// post to meet 107.5% of requirement
			return InvestmentCalculatorUtils.roundBaseAmount(MathUtils.multiply(getOptionsCollateralRequirement(), new BigDecimal(".075")).add(difference.abs()), getClientInvestmentAccount());
		}
		else {
			// positive - requirement is zero
			if (MathUtils.isNullOrZero(getOptionsCollateralRequirement())) {
				// pull back all collateral positions
				return getOptionsCollateralRequirement().negate();
			}
			// if the amount of collateral is over 115% of the requirement
			else if (difference.compareTo(MathUtils.multiply(getOptionsCollateralRequirement(), new BigDecimal(".15"))) > 0) {
				// pull back to 107.5% of requirement
				return InvestmentCalculatorUtils.roundBaseAmount(MathUtils.multiply(getOptionsCollateralRequirement(), new BigDecimal(".075")).subtract(difference), getClientInvestmentAccount());
			}
		}
		return BigDecimal.ZERO;
	}


	public BigDecimal getOptionsTargetMovementPercentage() {
		BigDecimal requirement = getOptionsCollateralRequirement();
		BigDecimal targetMovement = getOptionsTargetMovement();
		return CoreMathUtils.getPercentValue(targetMovement, requirement, true);
	}
}
