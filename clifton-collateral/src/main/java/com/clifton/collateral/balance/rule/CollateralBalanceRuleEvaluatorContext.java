package com.clifton.collateral.balance.rule;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * Used by all Collateral Balance Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 *
 * @author manderson
 */
public class CollateralBalanceRuleEvaluatorContext extends BaseRuleEvaluatorContext {

	Map<Integer, BigDecimal> collateralBalanceThresholdRangeMap = new HashMap<>();


	public BigDecimal getCollateralBalanceThresholdRangeValue(CollateralBalance balance) {
		//Compute value as whole (hundreds) percentage.
		return getCollateralBalanceThresholdRangeMap().computeIfAbsent(balance.getId(),
				b -> CoreMathUtils.getPercentValue(getExternalExcessCollateral(balance), balance.getExternalCollateralRequirement(), true));
	}


	public BigDecimal getExternalExcessCollateral(CollateralBalance balance) {
		//Compute the externalExcessCollateralPercentage manually so the transfer amount to be applied tomorrow can be
		//included in today's value to ensure the transfer amount brings the excess collateral percentage within the required threshold.
		BigDecimal transferAmount = MathUtils.add(balance.getTransferAmount(), balance.getSecuritiesTransferAmount());
		return MathUtils.subtract(MathUtils.add(MathUtils.negate(transferAmount),
				balance.getExternalCollateralAmount()), balance.getExternalCollateralRequirement());
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Integer, BigDecimal> getCollateralBalanceThresholdRangeMap() {
		return this.collateralBalanceThresholdRangeMap;
	}


	public void setCollateralBalanceThresholdRangeMap(Map<Integer, BigDecimal> collateralBalanceThresholdRangeMap) {
		this.collateralBalanceThresholdRangeMap = collateralBalanceThresholdRangeMap;
	}
}
