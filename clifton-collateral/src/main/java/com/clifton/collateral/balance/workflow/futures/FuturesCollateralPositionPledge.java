package com.clifton.collateral.balance.workflow.futures;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>FuturesCollateralPositionPledge</code> class is used to store relevant information for a collateral position
 * to avoid having to do multiple lookups for the information. It is backed by the <code>AccountingBalanceValue</code>
 * object and used the wraps the data stored there unless an overridden value is available.
 *
 * @author jonathanr
 */
@NonPersistentObject
public class FuturesCollateralPositionPledge {

	private InvestmentSecurity investmentSecurity;
	private BigDecimal quantity;
	private BigDecimal marketValue;
	private BigDecimal haircutValue;


	public FuturesCollateralPositionPledge(InvestmentSecurity investmentSecurity, BigDecimal quantity, BigDecimal marketValue, BigDecimal haircutValue) {
		this.investmentSecurity = investmentSecurity;
		this.quantity = quantity;
		this.marketValue = marketValue;
		this.haircutValue = haircutValue;
	}


	public BigDecimal getCollateralValue() {
		return MathUtils.getValueAfterHaircut(getMarketValue(), this.haircutValue, false);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                 Getters And Setters                    //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getHaircutValue() {
		return this.haircutValue;
	}


	public void setHaircutValue(BigDecimal haircutValue) {
		this.haircutValue = haircutValue;
	}
}
