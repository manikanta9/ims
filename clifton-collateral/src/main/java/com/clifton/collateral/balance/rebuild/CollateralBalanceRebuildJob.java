package com.clifton.collateral.balance.rebuild;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>CollateralBalanceRebuildJob</code> class is a batch job that rebuilds (Today - daysBack) collateral balances for specified parameters
 *
 * @author Mary Anderson
 */
public class CollateralBalanceRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private CollateralService collateralService;
	private CollateralBalanceRebuildService collateralBalanceRebuildService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final Integer DEFAULT_DAYS_BACK = 1;

	/**
	 * Rebuild balance data that for the past x days
	 */
	private Integer weekdaysBack = DEFAULT_DAYS_BACK;

	private boolean skipExisting;

	private Short collateralTypeId;
	//Allows for exclusion by type (e.g. exclude Margin accounts for options during 4 PM rebuild job.
	private Short[] excludeHoldingAccountTypeIds;

	private StatusHolderObject<Status> statusHolderObject;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();
		Date date = DateUtils.addWeekDays(new Date(), Math.negateExact(this.weekdaysBack));
		CollateralBalanceRebuildCommand rebuildCommand = new CollateralBalanceRebuildCommand();
		rebuildCommand.setBalanceDate(date);
		rebuildCommand.setSkipExisting(isSkipExisting());
		rebuildCommand.setSynchronous(true);
		rebuildCommand.setStatus(status);
		if (getCollateralTypeId() != null) {
			rebuildCommand.setCollateralType(getCollateralService().getCollateralType(getCollateralTypeId()));
		}
		if (getExcludeHoldingAccountTypeIds() != null) {
			rebuildCommand.setExcludeHoldingAccountTypeIds(getExcludeHoldingAccountTypeIds());
		}
		status = getCollateralBalanceRebuildService().rebuildCollateralBalance(rebuildCommand);

		return status;
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////////             Getter and Setter Methods            ////////////// 
	////////////////////////////////////////////////////////////////////////////


	public Short getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(Short collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public Short[] getExcludeHoldingAccountTypeIds() {
		return this.excludeHoldingAccountTypeIds;
	}


	public void setExcludeHoldingAccountTypeIds(Short[] excludeHoldingAccountTypeIds) {
		this.excludeHoldingAccountTypeIds = excludeHoldingAccountTypeIds;
	}


	public boolean isSkipExisting() {
		return this.skipExisting;
	}


	public void setSkipExisting(boolean skipExisting) {
		this.skipExisting = skipExisting;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public Integer getWeekdaysBack() {
		return this.weekdaysBack;
	}


	public void setWeekdaysBack(Integer weekdaysBack) {
		this.weekdaysBack = weekdaysBack;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}
}
