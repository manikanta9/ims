package com.clifton.collateral.balance.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contract.BusinessContract;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The {@link AdeptFundsCollateralIndependentAmountCalculator} class a custom independent amount calculation for Adept.  It will calculate IA in the usual fashion for all securities except
 * Forwards.
 * <p>
 * <p>
 * Due to specific language in the CSA for the Adept funds, our collateral calculations for OTC Collateral for Adept are not calculated correctly in IMS. Right now, we are performing these calculations in a spreadsheet to ensure we are making/agreeing to accurate call information.
 * <p>
 * <p>
 * An example of the correct way to calculate the requirement is as follows:
 * <p>
 * <p>
 * 1. Determine which currency is notional 1 & 2, based on alphabetical order. Example, currency pair USD/GBP, notional 1 would be GBP and notional 2 USD.
 * 2. Multiply notional 2 amount by IA %, listed within the clauses. In Adept’s case, this is 3%.
 * 3. Convert output from step #2 to USD (per CSA).
 * 4. Convert output from step #3 to GBP (per CSA).
 * 5. Amount calculated based on step #3 is what should show in the Independent amount for the Adept funds in the OTC collateral screen.
 * <p>
 * <p>
 * See JIRA <a href="https://jira.paraport.com/browse/COLLATERAL-132">COLLATERAL-132</a> for more details.
 *
 * @author mwacker
 */
public class AdeptFundsCollateralIndependentAmountCalculator extends BaseCollateralIndependentAmountCalculator {

	private BusinessCompanyService businessCompanyService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataSourceService marketDataSourceService;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculateIndependentAmount(CollateralBalance balance, AccountingPosition position, BigDecimal balanceIndependentAmountPercent) {
		balanceIndependentAmountPercent = getBalanceIndependentAmountPercent(position, balanceIndependentAmountPercent);
		InvestmentSecurity security = position.getInvestmentSecurity();
		if (!InvestmentUtils.isSecurityOfType(security, InvestmentType.FORWARDS)) {
			return super.calculateIndependentAmount(balance, position, balanceIndependentAmountPercent);
		}

		InvestmentSecurity usd = getInvestmentInstrumentService().getInvestmentSecurityBySymbol("USD", true);
		InvestmentSecurity underlyingCcy = security.getUnderlyingSecurity();

		// sort the currencies alphabetically
		List<InvestmentSecurity> ccyList = BeanUtils.sortWithFunction(CollectionUtils.createList(security.getUnderlyingSecurity(), security.getInstrument().getTradingCurrency()), InvestmentSecurity::getSymbol, true);
		InvestmentSecurity notional2Ccy = ccyList.get(1);

		// get the exchange rates
		String fxDataSource = getExchangeRateDataSourceName(balance, position);
		BigDecimal notional2ToUSDFx = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(fxDataSource, notional2Ccy.getSymbol(), usd.getSymbol(), balance.getBalanceDate()));
		BigDecimal usdToBase = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(fxDataSource, usd.getSymbol(), position.getClientInvestmentAccount().getBaseCurrency().getSymbol(), balance.getBalanceDate()));

		// calculate the exposure in the notional 2 currency
		BigDecimal exposure;
		if (notional2Ccy.equals(underlyingCcy)) {
			exposure = MathUtils.abs(position.getQuantity());
		}
		else {
			exposure = MathUtils.abs(position.getPositionCostBasis());
		}

		// apply the independent amount percent, then convert to USD and back to client base
		BigDecimal independentAmount = MathUtils.getPercentageOf(balanceIndependentAmountPercent, exposure, true);
		independentAmount = MathUtils.multiply(MathUtils.multiply(independentAmount, notional2ToUSDFx, 0), usdToBase, 0);

		return independentAmount.negate();
	}


	private String getExchangeRateDataSourceName(CollateralBalance balance, AccountingPosition position) {
		MarketDataSource result = null;
		Integer businessCompanyId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(balance.getHoldingInvestmentAccount().getBusinessContract(),
				BusinessContract.CONTRACT_CLAUSE_COLUMN_GROUP_NAME, BusinessContract.INDEPENDENT_AMOUNT_FX_RATE_PROVIDER, true);
		if (businessCompanyId != null) {
			BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(businessCompanyId);
			AssertUtils.assertNotNull(businessCompany, "Business Company could not be found [%s]", businessCompanyId);
			result = getMarketDataSourceService().getMarketDataSourceByCompany(businessCompany);
		}
		// no [Independent Amount FX Rate Provider] is specified
		if (result == null) {
			// check for issuing company name equal to the data source name.
			return getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForHoldingAccount(position.getHoldingInvestmentAccount().toHoldingAccount(), false);
		}
		return result.getName();
	}

	///////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods           //////////////
	///////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
