package com.clifton.collateral.balance.pledged;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The <code>CollateralBalancePledgedCollateralValue</code> is used to dynamically track pledged and remaining quantities for
 * collateral securities.  It keeps track of what securities have had this collateralSecurity pledged to it as well as what
 * quantity.  It allows for quantities to be posted and returned without the need to actually create and book the transfers first.
 *
 * @author StevenF
 */
public class CollateralBalancePledgedCollateralValue {

	private boolean cashCollateral;

	private BigDecimal originalQuantity;
	private BigDecimal pledgedQuantity;
	private BigDecimal remainingQuantity;

	private InvestmentSecurity collateralSecurity;
	private Map<Integer, BigDecimal> originalPositionSecurityQuantityMap;
	private Map<Integer, BigDecimal> pledgedPositionSecurityQuantityMap;

	////////////////////////////////////////////////////////////////////////////////
	////////////                 Convenience Methods                  //////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean hasCollateralMovements() {
		//If the original and remaining values are not the same, then collateral moved; if they are we need to do additional checks
		if (!MathUtils.isNullOrZero(getPledgedTransactionQuantity())) {
			return true;
		}
		//Varying sizes of original and pledged positions indicate collateral movements
		if (getOriginalPositionSecurityQuantityMap().size() != getPledgedPositionSecurityQuantityMap().size()) {
			return true;
		}
		//Now iterate positions and check for changes if the previous two checks failed to find movements
		for (Integer positionSecurityId : CollectionUtils.getIterable(getPositionSecurityIdSet())) {
			if (!MathUtils.isNullOrZero(getPledgedTransactionPositionSecurityQuantity(positionSecurityId))) {
				return true;
			}
		}
		return false;
	}


	public Set<Integer> getPositionSecurityIdSet() {
		Set<Integer> positionSecurityIdSet = new HashSet<>(getOriginalPositionSecurityQuantityMap().keySet());
		positionSecurityIdSet.addAll(getPledgedPositionSecurityQuantityMap().keySet());
		return positionSecurityIdSet;
	}


	public BigDecimal getTotalQuantity() {
		return MathUtils.add(getRemainingQuantity(), getPledgedQuantity());
	}

	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPledgedQuantity(InvestmentSecurity positionSecurity) {
		return getPledgedPositionSecurityQuantityMap().get(positionSecurity.getId());
	}


	public BigDecimal postCollateral(Integer positionSecurityId, BigDecimal collateralQuantity) {
		ValidationUtils.assertNotNull(positionSecurityId, "Position Security must be specified!");
		ValidationUtils.assertTrue(MathUtils.isGreaterThan(collateralQuantity, BigDecimal.ZERO), "Quantity must be positive! [SecurityID: " + positionSecurityId + "]");
		BigDecimal computedPledgedQuantity = MathUtils.add(getPledgedQuantity(), collateralQuantity);
		BigDecimal computedRemainingQuantity = MathUtils.subtract(getRemainingQuantity(), collateralQuantity);
		//For posting - if a manual entry is entered (such as 10, and the rounding value is 1000) then it is possible that
		//the quantity of 10 will not affect the overall requiredCollateral significantly enough to change the rounded pledged number
		//This can result in over-pledging during a post - so we automatically adjust the value to only pledge was is available
		//This makes the validation in the validateAndUpdateQuantities somewhat of a moot point during posts.
		if (MathUtils.isLessThan(computedRemainingQuantity, BigDecimal.ZERO)) {
			collateralQuantity = MathUtils.add(collateralQuantity, computedRemainingQuantity);
			computedPledgedQuantity = MathUtils.add(computedPledgedQuantity, computedRemainingQuantity);
			computedRemainingQuantity = BigDecimal.ZERO;
		}
		validateAndUpdateQuantities(computedPledgedQuantity, computedRemainingQuantity);

		//Update the position quantity
		BigDecimal computedPositionQuantity = MathUtils.add(getPledgedPositionSecurityQuantityMap().get(positionSecurityId), collateralQuantity);
		getPledgedPositionSecurityQuantityMap().put(positionSecurityId, computedPositionQuantity);
		return collateralQuantity;
	}


	public void returnAllCollateral() {
		returnCollateralForPosition(null, getPledgedQuantity());
	}


	public void returnCollateral(BigDecimal collateralQuantity) {
		returnCollateralForPosition(null, collateralQuantity);
	}


	public BigDecimal returnCollateralForPosition(Integer positionSecurityId, BigDecimal collateralQuantity) {
		BigDecimal quantityToReturn = BigDecimal.ZERO;
		ValidationUtils.assertTrue(MathUtils.isGreaterThan(collateralQuantity, BigDecimal.ZERO), "Quantity must be positive! [SecurityID: " + positionSecurityId + "]");
		for (Map.Entry<Integer, BigDecimal> pledgedPositionSecurityQuantityEntry : CollectionUtils.getIterable(getPledgedPositionSecurityQuantityMap().entrySet())) {
			if (positionSecurityId == null || pledgedPositionSecurityQuantityEntry.getKey().equals(positionSecurityId)) {
				if (MathUtils.isGreaterThanOrEqual(collateralQuantity, BigDecimal.ZERO)) {
					BigDecimal pledgedPositionQuantity = pledgedPositionSecurityQuantityEntry.getValue();

					quantityToReturn = collateralQuantity;
					if (MathUtils.isGreaterThan(quantityToReturn, pledgedPositionQuantity)) {
						quantityToReturn = pledgedPositionQuantity;
					}
					if (MathUtils.isGreaterThan(quantityToReturn, BigDecimal.ZERO)) {
						BigDecimal computedPledgedQuantity = MathUtils.subtract(getPledgedQuantity(), quantityToReturn);
						BigDecimal computedRemainingQuantity = MathUtils.add(getRemainingQuantity(), quantityToReturn);
						validateAndUpdateQuantities(computedPledgedQuantity, computedRemainingQuantity);
						//Update the position quantity
						pledgedPositionSecurityQuantityEntry.setValue(MathUtils.subtract(pledgedPositionQuantity, quantityToReturn));
					}
				}
			}
		}
		return quantityToReturn;
	}


	public BigDecimal getPledgedTransactionQuantity() {
		return MathUtils.subtract(getOriginalQuantity(), getRemainingQuantity());
	}


	//We reverse the order here to deal with transaction quantities properly
	//(e.g. if we started with 20, and posted 10, then we get 30 - 20 = 10 for the transaction change)
	//(e.g. if we started with 20, and returned 10, then we get 10 - 20 = -10 for the transaction change)
	public BigDecimal getPledgedTransactionPositionSecurityQuantity(Integer positionSecurityId) {
		return MathUtils.subtract(getPledgedPositionSecurityQuantityMap().get(positionSecurityId), getOriginalPositionSecurityQuantityMap().get(positionSecurityId));
	}


	private void validateAndUpdateQuantities(BigDecimal computedPledgedQuantity, BigDecimal computedRemainingQuantity) {
		ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(computedPledgedQuantity, BigDecimal.ZERO), "Pledged Quantity [" + computedPledgedQuantity + "] must be greater than or equal to 0");
		ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(computedRemainingQuantity, BigDecimal.ZERO), "Remaining Quantity [" + computedRemainingQuantity + "] must be greater than or equal to 0");
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(MathUtils.add(computedPledgedQuantity, computedRemainingQuantity), getTotalQuantity()), "Pledged Quantity [" + computedPledgedQuantity + "] and Remaining Quantity [" + computedRemainingQuantity + "] must not be greater than the Total Quantity!");
		setPledgedQuantity(computedPledgedQuantity);
		setRemainingQuantity(computedRemainingQuantity);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getOriginalQuantity() {
		return this.originalQuantity;
	}


	public void setOriginalQuantity(BigDecimal originalQuantity) {
		this.originalQuantity = originalQuantity;
	}


	public BigDecimal getPledgedQuantity() {
		return this.pledgedQuantity;
	}


	public void setPledgedQuantity(BigDecimal pledgedQuantity) {
		this.pledgedQuantity = pledgedQuantity;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public boolean isCashCollateral() {
		return this.cashCollateral;
	}


	public void setCashCollateral(boolean cashCollateral) {
		this.cashCollateral = cashCollateral;
	}


	public InvestmentSecurity getCollateralSecurity() {
		return this.collateralSecurity;
	}


	public void setCollateralSecurity(InvestmentSecurity collateralSecurity) {
		this.collateralSecurity = collateralSecurity;
	}


	@ValueIgnoringGetter
	public Map<Integer, BigDecimal> getOriginalPositionSecurityQuantityMap() {
		if (this.originalPositionSecurityQuantityMap == null) {
			this.originalPositionSecurityQuantityMap = new HashMap<>();
		}
		return this.originalPositionSecurityQuantityMap;
	}


	public void setOriginalPositionSecurityQuantityMap(Map<Integer, BigDecimal> originalPositionSecurityQuantityMap) {
		this.originalPositionSecurityQuantityMap = originalPositionSecurityQuantityMap;
	}


	@ValueIgnoringGetter
	public Map<Integer, BigDecimal> getPledgedPositionSecurityQuantityMap() {
		if (this.pledgedPositionSecurityQuantityMap == null) {
			this.pledgedPositionSecurityQuantityMap = new HashMap<>();
		}
		return this.pledgedPositionSecurityQuantityMap;
	}


	public void setPledgedPositionSecurityQuantityMap(Map<Integer, BigDecimal> pledgedPositionSecurityQuantityMap) {
		this.pledgedPositionSecurityQuantityMap = pledgedPositionSecurityQuantityMap;
	}
}
