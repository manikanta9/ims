package com.clifton.collateral.balance.rebuild.processor.command.options;

import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.CollateralStrategyCommandHandler;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CollateralOptionsStrategyCommandHandler</code> is used to populate all necessary values in the
 * <code>CollateralOptionsStrategyCommand</code> object.  This includes the computed prices and values for
 * each position that will be processed later by a strategy calculator.
 *
 * @author StevenF
 */
@Component
public class CollateralOptionsStrategyCommandHandler implements CollateralStrategyCommandHandler<CollateralOptionsStrategyCommand, CollateralBalanceOptionsRebuildProcessor> {

	private MarketDataRetriever marketDataRetriever;


	@Override
	public CollateralOptionsStrategyCommand populateCommand(CollateralBalanceOptionsRebuildProcessor rebuildProcessor, List<AccountingPositionInfo> positionInfoList, Date positionDate) {
		CollateralOptionsStrategyCommand command = new CollateralOptionsStrategyCommand();
		command.setCollateralBalanceRebuildProcessor(rebuildProcessor);

		for (AccountingPositionInfo positionInfo : CollectionUtils.getIterable(positionInfoList)) {
			CollateralOptionsStrategyPosition strategyPosition = createCollateralOptionsStrategyPosition(positionInfo, positionDate);
			command.getPositionList().add(strategyPosition);
		}
		return command;
	}


	@Override
	public CollateralOptionsStrategyPosition createCollateralOptionsStrategyPosition(AccountingPositionInfo positionInfo, Date positionDate) {

		//Populate required values for collateral calculation - some values aren't needed for some calculators of won't populate for some securities.
		//So we log as an info message and ignore.
		BigDecimal strikeValue = BigDecimal.ZERO;
		try {
			strikeValue = MathUtils.abs(MathUtils.multiply(positionInfo.getRemainingQuantity(), MathUtils.multiply(positionInfo.getInvestmentSecurity().getOptionStrikePrice(), positionInfo.getInvestmentSecurity().getPriceMultiplier())));
		}
		catch (Exception e) {
			LogUtils.info(CollateralOptionsStrategyCommandHandler.class, "Failed to set strike price and/or value; defaulting to 0.", e);
		}

		BigDecimal optionPrice = BigDecimal.ZERO;
		BigDecimal optionProceeds = BigDecimal.ZERO;
		try {
			optionPrice = getMarketDataRetriever().getPrice(positionInfo.getInvestmentSecurity(), positionDate, true, "Failed to locate closing price for security: " + positionInfo.getInvestmentSecurity().getSymbol());
			optionProceeds = MathUtils.abs(MathUtils.multiply(positionInfo.getRemainingQuantity(), MathUtils.multiply(optionPrice, positionInfo.getInvestmentSecurity().getPriceMultiplier())));
		}
		catch (Exception e) {
			LogUtils.info(CollateralOptionsStrategyCommandHandler.class, "Failed to set option price and/or value; defaulting to 0.", e);
		}

		BigDecimal underlyingPrice = BigDecimal.ZERO;
		BigDecimal underlyingValue = BigDecimal.ZERO;
		try {
			if (positionInfo.getInvestmentSecurity().getUnderlyingSecurity() != null) {
				underlyingPrice = getMarketDataRetriever().getPrice(positionInfo.getInvestmentSecurity().getUnderlyingSecurity(), positionDate, true, "Failed to locate closing price for underlying security: " + positionInfo.getInvestmentSecurity().getUnderlyingSecurity().getSymbol());
				underlyingValue = MathUtils.abs(MathUtils.multiply(positionInfo.getRemainingQuantity(), MathUtils.multiply(underlyingPrice, positionInfo.getInvestmentSecurity().getPriceMultiplier())));
			}
		}
		catch (Exception e) {
			LogUtils.info(CollateralOptionsStrategyCommandHandler.class, "Failed to set underlying price and/or value; defaulting to 0.", e);
		}
		return new CollateralOptionsStrategyPosition(positionDate, positionInfo.getInvestmentSecurity(), positionInfo.getInvestmentSecurity().getPriceMultiplier(),
				positionInfo.getRemainingQuantity(), positionInfo.getPositionGroupId(), strikeValue, underlyingPrice, underlyingValue,
				optionPrice, optionProceeds, positionInfo.getAccountingTransactionId());
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
