package com.clifton.collateral.balance.instruction;


import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemSchemaService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseCollateralBalanceInstructionSelector</code> ...
 *
 * @author manderson
 */
public abstract class BaseCollateralBalanceInstructionSelector extends BaseInvestmentInstructionSelector<CollateralBalance> {

	private AccountingPositionTransferService accountingPositionTransferService;

	private CollateralBalanceService collateralBalanceService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private SystemSchemaService systemSchemaService;

	///////////////////////////////////////////////////

	private Integer holdingAccountId;

	private Integer holdingAccountIssuerId;

	private Short holdingAccountTypeId;

	private Boolean holdingAccountIsHold;

	private List<Integer> includeHoldingAccountGroupIds;

	private List<Integer> excludeHoldingAccountGroupIds;

	private Integer clientAccountId;

	private Integer collateralAccountId;

	private Short collateralAccountTypeId;

	private Integer collateralAccountIssuerId;

	private Integer collateralCurrencyId;

	private Short collateralTypeId;

	/**
	 * Note: Used for Cash Collateral Only
	 */
	private boolean includeZeroTransferAmount;


	////////////////////////////////////////////////////


	@Override
	public String getInstructionItemLabel(CollateralBalance entity) {
		return entity.getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, CollateralBalance entity) {
		if (category.isCash()) {
			return entity.getBookingDate() != null;
		}
		else {
			List<AccountingPositionTransfer> transferList = getPositionTransferListForCollateralBalance(entity.getId());
			if (!CollectionUtils.isEmpty(transferList)) {
				for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(transferList)) {
					// If At least one unbooked return false;
					if (transfer.getBookingDate() == null) {
						return false;
					}
				}
				// return true
				return true;
			}
		}
		return false;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, CollateralBalance entity) {
		selection.setClientAccount(entity.getClientInvestmentAccount());
		selection.setHoldingAccount(entity.getHoldingInvestmentAccount());
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));
		if (selection.getCategory().isCash()) {
			selection.setSecurity(entity.getCollateralCurrency() == null ? entity.getHoldingInvestmentAccount().getBaseCurrency() : entity.getCollateralCurrency());
			selection.setDescription("Transfer Amount: " + CoreMathUtils.formatNumberMoney(entity.getTransferAmount() == null ? BigDecimal.ZERO : entity.getTransferAmount()));
		}
		else {
			List<AccountingPositionTransfer> transferList = getPositionTransferListForCollateralBalance(entity.getId());
			InvestmentSecurity security = null;
			boolean first = true;

			StringBuilder sb = new StringBuilder(16);
			for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(transferList)) {
				// Pull Again
				transfer = getAccountingPositionTransferService().getAccountingPositionTransfer(transfer.getId());
				StringBuilder transferSb = new StringBuilder(16);
				Map<InvestmentSecurity, List<AccountingPositionTransferDetail>> detailMap = BeanUtils.getBeansMap(transfer.getDetailList(), AccountingPositionTransferDetail::getSecurity);
				for (Map.Entry<InvestmentSecurity, List<AccountingPositionTransferDetail>> secEntry : detailMap.entrySet()) {
					InvestmentSecurity sec = secEntry.getKey();
					if (first) {
						security = sec;
						first = false;
					}
					else if (security != null && !security.equals(sec)) {
						security = null;
					}
					BigDecimal total = CoreMathUtils.sumProperty(secEntry.getValue(), AccountingPositionTransferDetail::getQuantity);
					if (transferSb.length() > 0) {
						transferSb.append(", ");
					}
					transferSb.append(CoreMathUtils.formatNumberDecimal(total));
					transferSb.append(" of ");
					transferSb.append(sec.getSymbol());
				}
				if (sb.length() > 0) {
					sb.append(", ");
				}

				sb.append("Transfer ");
				sb.append(transferSb.toString());
				sb.append("<br/>&nbsp;&nbsp;&nbsp;");
				if (transfer.getFromHoldingInvestmentAccount() != null) {
					sb.append(" From ");
					sb.append(transfer.getFromHoldingInvestmentAccount().getNumber());
				}
				if (transfer.getToHoldingInvestmentAccount() != null) {
					sb.append(" To ");
					sb.append(transfer.getToHoldingInvestmentAccount().getNumber());
				}
				sb.append("<br/>");
			}
			selection.setDescription(sb.toString());
			selection.setSecurity(security);
		}
	}


	// Note - this is a duplicate look up since we've already looked up items during the retrieval process
	// Used for UI display where the list is already filtered down, or for missing instructions list
	private List<AccountingPositionTransfer> getPositionTransferListForCollateralBalance(int balanceId) {
		AccountingPositionTransferSearchForm positionTransferSearchForm = new AccountingPositionTransferSearchForm();
		positionTransferSearchForm.setSourceSystemTableId(getSystemSchemaService().getSystemTableByName("CollateralBalance").getId());
		positionTransferSearchForm.setSourceFkFieldId(balanceId);
		return getAccountingPositionTransferService().getAccountingPositionTransferList(positionTransferSearchForm);
	}

	////////////////////////////////////////////////////


	@Override
	public List<CollateralBalance> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(date);
		// Only include primary accounts
		searchForm.setParentIsNull(true);
		searchForm.setCollateralTypeId(getCollateralTypeId());
		searchForm.setClientInvestmentAccountId(getClientAccountId());
		searchForm.setCoalesceCollateralHoldingAccountCurrencyId(getCollateralCurrencyId());
		searchForm.setAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
		searchForm.setExcludeAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));

		// If Cash: Only include those with an actual Transfer Amount (Unless specified to Include 0 Amounts)
		// Note: Check Definition Exists so when looking for "missing" (no definition) we always see all
		if (category.isCash() && (definition != null && !isIncludeZeroTransferAmount())) {
			searchForm.addSearchRestriction(new SearchRestriction("transferAmount", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		}
		// Otherwise (Position) - See below: Need to check for only those with a position transfer - filtered after list is initial filtered

		// Specific Holding Account
		if (getHoldingAccountId() != null) {
			searchForm.setHoldingInvestmentAccountId(getHoldingAccountId());
		}
		// Or Issuer and/or Type
		else {
			searchForm.setIssuingCompanyId(getHoldingAccountIssuerId());
			searchForm.setHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
			searchForm.setHoldingAccountIsHold(getHoldingAccountIsHold());
		}
		// Specific Collateral Account
		if (getCollateralAccountId() != null) {
			searchForm.setCollateralInvestmentAccountId(getCollateralAccountId());
		}
		// Or Issuer and/or Type
		else {
			searchForm.setCollateralCompanyId(getCollateralAccountIssuerId());
			searchForm.setCollateralInvestmentAccountTypeId(getCollateralAccountTypeId());
		}
		searchForm.setExcludeAccountsWithoutPositionsAndNoRequiredCollateral(true);

		List<CollateralBalance> balanceList = getCollateralBalanceService().getCollateralBalanceList(searchForm);

		// If checking for position transfer - find only those with a position transfer linked to the collateral Balance record
		if (!CollectionUtils.isEmpty(balanceList) && !category.isCash()) {
			AccountingPositionTransferSearchForm positionTransferSearchForm = new AccountingPositionTransferSearchForm();
			positionTransferSearchForm.setSourceSystemTableId(getSystemSchemaService().getSystemTableByName("CollateralBalance").getId());
			Object[] ids = BeanUtils.getPropertyValues(balanceList, "id");
			positionTransferSearchForm.setSourceFkFieldIdList(ArrayUtils.toIntegerArray(ids));
			List<AccountingPositionTransfer> transferList = getAccountingPositionTransferService().getAccountingPositionTransferList(positionTransferSearchForm);

			Integer[] transferSourceFkFieldIds = BeanUtils.getPropertyValuesUniqueExcludeNull(transferList, AccountingPositionTransfer::getSourceFkFieldId, Integer.class);
			return BeanUtils.filter(balanceList, collateralBalance -> ArrayUtils.contains(transferSourceFkFieldIds, collateralBalance.getId()));
		}

		return balanceList;
	}


	protected InvestmentAccount getCoalesceCollateralCustodianAccountForBalance(CollateralBalance balance) {
		// Return Collateral Account if Account Is Already Defined on the Balance Record
		if (balance.getCollateralInvestmentAccount() != null) {
			return balance.getCollateralInvestmentAccount();
		}

		// Per Eric:
		// 1. Check for related account with Purpose Collateral, if fail
		// 2. Check for related account with Purpose Mark to Market

		List<InvestmentAccount> collateralAccountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(balance.getHoldingInvestmentAccount().getId(), null,
				InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME, balance.getBalanceDate());
		List<InvestmentAccount> m2mAccountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(balance.getHoldingInvestmentAccount().getId(), null,
				InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME, balance.getBalanceDate());

		String errorMsg = "Unable to generate instructions for collateral balance record [" + balance.getLabel() + "]: ";
		// there can be up to 2 "Collateral" or 2 "Mark to Market" links: one to custodian holding account and one to our account
		InvestmentAccount collateralCustodialAccount = null;
		for (InvestmentAccount account : CollectionUtils.getIterable(collateralAccountList)) {
			if (!account.getType().isOurAccount()) {
				ValidationUtils.assertNull(collateralCustodialAccount, errorMsg + "Cannot have more than 1 NOT 'our' account for 'Collateral' purpose for: "
						+ balance.getHoldingInvestmentAccount().getLabel());
				collateralCustodialAccount = account;
			}
		}

		InvestmentAccount markCustodialAccount = null;
		for (InvestmentAccount account : CollectionUtils.getIterable(m2mAccountList)) {
			if (!account.getType().isOurAccount()) {
				ValidationUtils.assertNull(markCustodialAccount, errorMsg + "Cannot have more than 1 NOT 'our' account for 'Mark to Market' purpose for: "
						+ balance.getHoldingInvestmentAccount().getLabel());
				markCustodialAccount = account;
			}
		}

		if (collateralCustodialAccount == null) {
			collateralCustodialAccount = markCustodialAccount;
		}
		ValidationUtils.assertNotNull(collateralCustodialAccount, errorMsg + "Cannot find related account for 'Collateral' or 'Mark to Market' purpose for: "
				+ balance.getHoldingInvestmentAccount().getLabel());
		return collateralCustodialAccount;
	}


	////////////////////////////////////////////////////
	////           Getter & Setter Methods          ////
	////////////////////////////////////////////////////


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public Integer getCollateralAccountId() {
		return this.collateralAccountId;
	}


	public void setCollateralAccountId(Integer collateralAccountId) {
		this.collateralAccountId = collateralAccountId;
	}


	public Short getCollateralAccountTypeId() {
		return this.collateralAccountTypeId;
	}


	public void setCollateralAccountTypeId(Short collateralAccountTypeId) {
		this.collateralAccountTypeId = collateralAccountTypeId;
	}


	public Integer getCollateralAccountIssuerId() {
		return this.collateralAccountIssuerId;
	}


	public void setCollateralAccountIssuerId(Integer collateralAccountIssuerId) {
		this.collateralAccountIssuerId = collateralAccountIssuerId;
	}


	public Integer getCollateralCurrencyId() {
		return this.collateralCurrencyId;
	}


	public void setCollateralCurrencyId(Integer collateralCurrencyId) {
		this.collateralCurrencyId = collateralCurrencyId;
	}


	public Short getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(Short collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public Boolean getHoldingAccountIsHold() {
		return this.holdingAccountIsHold;
	}


	public void setHoldingAccountIsHold(Boolean holdingAccountIsHold) {
		this.holdingAccountIsHold = holdingAccountIsHold;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public List<Integer> getIncludeHoldingAccountGroupIds() {
		return this.includeHoldingAccountGroupIds;
	}


	public void setIncludeHoldingAccountGroupIds(List<Integer> includeHoldingAccountGroupIds) {
		this.includeHoldingAccountGroupIds = includeHoldingAccountGroupIds;
	}


	public List<Integer> getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(List<Integer> excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}


	public boolean isIncludeZeroTransferAmount() {
		return this.includeZeroTransferAmount;
	}


	public void setIncludeZeroTransferAmount(boolean includeZeroTransferAmount) {
		this.includeZeroTransferAmount = includeZeroTransferAmount;
	}
}
