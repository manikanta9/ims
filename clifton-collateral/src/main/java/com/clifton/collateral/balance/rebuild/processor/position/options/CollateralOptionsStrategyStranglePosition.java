package com.clifton.collateral.balance.rebuild.processor.position.options;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author terrys
 */
public class CollateralOptionsStrategyStranglePosition extends CollateralOptionsStrategyPosition implements CollateralOptionsStrategyAggregatePosition {

	private final CollateralOptionsStrategyPosition putStrategyPosition;
	private final CollateralOptionsStrategyPosition callStrategyPosition;

	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyStranglePosition(CollateralOptionsStrategyPosition putStrategyPosition, CollateralOptionsStrategyPosition callStrategyPosition) {
		// select the put, no reason
		super(putStrategyPosition);

		this.putStrategyPosition = putStrategyPosition;
		this.callStrategyPosition = callStrategyPosition;

		this.putStrategyPosition.setParentStrategyPosition(this);
		this.callStrategyPosition.setParentStrategyPosition(this);
		this.putStrategyPosition.setExplanationBuilder(this.getExplanationBuilder());
		this.callStrategyPosition.setExplanationBuilder(this.getExplanationBuilder());
		this.setAccountingTransactionId(null);

		AssertUtils.assertEquals(0, this.putStrategyPosition.getRemainingQuantity().compareTo(this.callStrategyPosition.getRemainingQuantity()),
				String.format("Strangles must have equal put [%s] and call [%s] quantities", this.putStrategyPosition.getRemainingQuantity(),
						this.callStrategyPosition.getRemainingQuantity()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String buildPositionExplanation() {
		StringBuilder results = new StringBuilder();

		results.append("Put Position: \n");
		results.append(getPutStrategyPosition().buildPositionExplanation()).append('\n');
		results.append("Call Position: \n");
		results.append(getCallStrategyPosition().buildPositionExplanation());

		return results.toString();
	}


	@Override
	@ValueChangingSetter
	public void setRequiredCollateral(BigDecimal requiredCollateral) {
		super.setRequiredCollateral(requiredCollateral);
		AssertUtils.assertTrue(MathUtils.isEqual(getRequiredCollateral(), this.putStrategyPosition.getRequiredCollateral().add(this.callStrategyPosition.getRequiredCollateral())),
				() -> String.format("Expected the put [%s] and call [%s] positions required collateral to equal the strangle collateral [%s].",
						this.putStrategyPosition.getRequiredCollateral(), this.callStrategyPosition.getRequiredCollateral(), this.getRequiredCollateral()));
	}


	@Override
	@ValueChangingSetter
	public void setFxRateToBase(BigDecimal fxRateToBase) {
		super.setFxRateToBase(fxRateToBase);
		this.putStrategyPosition.setFxRateToBase(fxRateToBase);
		this.callStrategyPosition.setFxRateToBase(fxRateToBase);
	}


	@Override
	@ValueChangingSetter
	public void setPositionGroupId(String positionGroupId) {
		super.setPositionGroupId(positionGroupId);
		this.putStrategyPosition.setPositionGroupId(positionGroupId);
		this.callStrategyPosition.setPositionGroupId(positionGroupId);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralOptionsStrategyPosition getPutStrategyPosition() {
		return this.putStrategyPosition;
	}


	public CollateralOptionsStrategyPosition getCallStrategyPosition() {
		return this.callStrategyPosition;
	}


	@Override
	public List<CollateralOptionsStrategyPosition> getCompositeStrategyPositionList() {
		List<CollateralOptionsStrategyPosition> removals = new ArrayList<>();
		List<CollateralOptionsStrategyPosition> results = Stream.of(this.putStrategyPosition, this.callStrategyPosition)
				.flatMap(p ->
						{
							if (p instanceof CollateralOptionsStrategyJoinedPosition) {
								removals.add(p);
								return ((CollateralOptionsStrategyJoinedPosition) p).getCompositeStrategyPositionList().stream();
							}
							else {
								return Stream.of(p);
							}
						}
				)
				.collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(removals)) {
			results.removeAll(removals);
		}
		return results;
	}
}
