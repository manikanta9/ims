package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySpreadPosition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * Long Put Spread
 * Initial margin requirement:
 * no margin required on short put(s)
 * long put(s) must be paid for in full
 * proceeds received from sale of short put(s) may be applied to the cost of the long put(s)
 * the short put(s) may expire before the long put(s) and not affect margin requirement
 */
public class CollateralOptionsStrategyLongPutSpreadCalculator extends CollateralOptionsStrategySpreadCalculator {

	@Override
	public void populateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, boolean showExplanation) {
		Consumer<Supplier<String>> appender = showExplanation ? strategyPosition::appendToExplanation : s -> LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), s));
		BigDecimal amount = doCalculateCollateral(processor, strategyPosition, appender);
		strategyPosition.setRequiredCollateral(amount);
	}


	@Override
	protected BigDecimal doCalculateCollateral(CollateralBalanceOptionsRebuildProcessor processor, CollateralOptionsStrategyPosition strategyPosition, Consumer<Supplier<String>> appender) {
		if (CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN.equals(processor.getPutRequirementType())
				&& strategyPosition instanceof CollateralOptionsStrategySpreadPosition) {

			CollateralOptionsStrategySpreadPosition spreadPosition = (CollateralOptionsStrategySpreadPosition) strategyPosition;

			BigDecimal marginAmount = spreadPosition.getLongStrategyPosition().getOptionProceeds();

			appender.accept(() -> "Long Put Spread");
			appender.accept(() -> String.format("Margin Requirement for long puts [%s]", CoreMathUtils.formatNumber(marginAmount, null)));
			appender.accept(() -> "Positions:");
			appender.accept(strategyPosition::buildPositionExplanation);
			appender.accept(() -> String.format("Margin requirement: [%s]", CoreMathUtils.formatNumber(marginAmount, null)));

			// distribute strangle total to individual constituent long and short
			spreadPosition.getLongStrategyPosition().setRequiredCollateral(marginAmount);
			spreadPosition.getShortStrategyPosition().setRequiredCollateral(BigDecimal.ZERO);

			return InvestmentCalculatorUtils.roundLocalAmount(marginAmount, strategyPosition.getInvestmentSecurity());
		}
		return BigDecimal.ZERO;
	}
}
