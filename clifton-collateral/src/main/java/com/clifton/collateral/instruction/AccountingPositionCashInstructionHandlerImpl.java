package com.clifton.collateral.instruction;

import com.clifton.accounting.m2m.instruction.handler.AbstractAccountingPositionInstructionHandler;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.AbstractTransferMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;


@Component
public class AccountingPositionCashInstructionHandlerImpl<I extends Instruction> extends AbstractAccountingPositionInstructionHandler<I> {

	private CollateralBalanceService collateralBalanceService;

	/////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accepts(AccountingPositionTransfer accountingPositionTransfer) {
		return accountingPositionTransfer.getType().isCash();
	}


	@Override
	public InstructionMessage generateInstructionMessage(Instruction instruction, AccountingPositionTransferDetail positionTransferDetail) {
		AccountingPositionTransfer positionTransfer = positionTransferDetail.getPositionTransfer();
		AssertUtils.assertTrue(positionTransfer.getType().isCash(), "The position transfer type [%s] is not supported.", positionTransfer.getType().getName());

		return generate(instruction, positionTransferDetail.getPositionTransfer(), positionTransferDetail);
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private AbstractTransferMessage generate(Instruction instruction, AccountingPositionTransfer positionTransfer, AccountingPositionTransferDetail positionTransferDetail) {
		AssertUtils.assertTrue(positionTransfer.getType().isCash(), "The position transfer type [%s] is not supported.", positionTransfer.getType().getName());

		AbstractTransferMessage results = isSending(positionTransfer.getType()) ? generateNoticeToSend(instruction, positionTransfer)
				: generateNoticeToReceive(instruction, positionTransfer);
		results.setSenderBIC(getSenderBic());
		results.setTransactionReferenceNumber(getTransactionReferenceNumber(instruction, positionTransfer, positionTransferDetail));
		results.setAmount(MathUtils.abs(positionTransferDetail.getPositionCostBasis()));
		results.setCurrency(positionTransferDetail.getSecurity().getSymbol());
		results.setValueDate(positionTransfer.getSettlementDate());
		results.setMessagePurpose(getMessagePurpose(positionTransfer));

		return results;
	}


	private GeneralFinancialInstitutionTransferMessage generateNoticeToSend(Instruction instruction, AccountingPositionTransfer positionTransfer) {
		GeneralFinancialInstitutionTransferMessage transferMessage = new GeneralFinancialInstitutionTransferMessage();

		Optional.ofNullable(positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode()).ifPresent(bic -> {
			transferMessage.setCustodyBIC(bic);
			transferMessage.setReceiverBIC(bic);
		});
		ValidationUtils.assertNotNull(transferMessage.getReceiverBIC(), "[%s] must have a BIC for the SWIFT receiver field value.",
				positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany().getLabel());
		transferMessage.setCustodyAccountNumber(positionTransfer.getFromHoldingInvestmentAccount().getNumber());

		Optional.ofNullable(positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode()).ifPresent(transferMessage::setBeneficiaryBIC);

		if (instruction instanceof InvestmentInstructionItem) {
			InvestmentInstructionItem investmentInstructionItem = (InvestmentInstructionItem) instruction;
			if (Objects.nonNull(investmentInstructionItem.getInstruction().getDefinition().getDeliveryType())) {
				InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(
						instruction,
						positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany(),
						positionTransfer.getToClientInvestmentAccount().getBaseCurrency());
				Optional.ofNullable(deliveryInstruction).map(InvestmentInstructionDelivery::getDeliveryCompany).map(BusinessCompany::getBusinessIdentifierCode)
						.ifPresent(transferMessage::setIntermediaryBIC);
				Optional.ofNullable(deliveryInstruction).map(InvestmentInstructionDelivery::getDeliveryAccountNumber).ifPresent(transferMessage::setIntermediaryAccountNumber);
			}
			else {
				BusinessCompany intermediary = getBusinessCompanyUtilHandler().getIntermediaryCustodian(positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany());
				transferMessage.setIntermediaryBIC(intermediary.getBusinessIdentifierCode());
				transferMessage.setIntermediaryAccountNumber(getBusinessCompanyUtilHandler().getIntermediaryCustodianAccountNumber(positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany()));
			}
		}

		return transferMessage;
	}


	private NoticeToReceiveTransferMessage generateNoticeToReceive(Instruction instruction, AccountingPositionTransfer positionTransfer) {
		NoticeToReceiveTransferMessage transferMessage = new NoticeToReceiveTransferMessage();

		Optional.ofNullable(positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode()).ifPresent(transferMessage::setReceiverBIC);
		ValidationUtils.assertNotNull(transferMessage.getReceiverBIC(), "[%s] must have a BIC for the SWIFT receiver field value.",
				positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany().getLabel());

		Optional.ofNullable(positionTransfer.getToHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode()).ifPresent(transferMessage::setOrderingBIC);
		transferMessage.setReceiverAccountNumber(positionTransfer.getToHoldingInvestmentAccount().getNumber());

		if (instruction instanceof InvestmentInstructionItem) {
			InvestmentInstructionItem investmentInstructionItem = (InvestmentInstructionItem) instruction;
			if (Objects.nonNull(investmentInstructionItem.getInstruction().getDefinition().getDeliveryType())) {
				InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction,
						positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany(),
						positionTransfer.getFromClientInvestmentAccount().getBaseCurrency());
				Optional.ofNullable(deliveryInstruction).map(InvestmentInstructionDelivery::getDeliveryCompany).map(BusinessCompany::getBusinessIdentifierCode)
						.ifPresent(transferMessage::setIntermediaryBIC);
			}
			else {
				BusinessCompany intermediary = getBusinessCompanyUtilHandler().getIntermediaryCustodian(positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany());
				transferMessage.setIntermediaryBIC(intermediary.getBusinessIdentifierCode());
			}
		}

		return transferMessage;
	}


	private TransferMessagePurposes getMessagePurpose(AccountingPositionTransfer positionTransfer) {
		TransferMessagePurposes purpose = getCollateralBalancePurpose(positionTransfer);
		if (purpose == null) {
			return positionTransfer.getType().isCounterpartyCollateral() ? TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT_ISDA
					: TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT;
		}
		else {
			return purpose;
		}
	}


	private TransferMessagePurposes getCollateralBalancePurpose(AccountingPositionTransfer positionTransfer) {
		if (positionTransfer.getType().isCollateral() && positionTransfer.getSourceSystemTable() != null
				&& CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME.equals(positionTransfer.getSourceSystemTable().getName())) {
			CollateralBalance collateralBalance = getCollateralBalanceService().getCollateralBalance(positionTransfer.getSourceFkFieldId());
			if (collateralBalance != null) {
				switch (collateralBalance.getCollateralType().getName()) {
					case CollateralType.COLLATERAL_TYPE_FUTURES:
						return TransferMessagePurposes.INITIAL_MARGIN;
					case CollateralType.COLLATERAL_TYPE_CLEARED_OTC:
						return positionTransfer.getType().isCash() ? TransferMessagePurposes.INITIAL_MARGIN_OTC : TransferMessagePurposes.DAILY_MARGIN_OTC;
					case CollateralType.COLLATERAL_TYPE_REPO:
						return positionTransfer.getType().isCounterpartyCollateral() ? TransferMessagePurposes.COLLATERAL_REPO_BROKER : TransferMessagePurposes.COLLATERAL_REPO_CLIENT;
					case CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN:
						return TransferMessagePurposes.COLLATERAL_LISTED_OPTIONS;
					case CollateralType.COLLATERAL_TYPE_OTC:
					default:
						return positionTransfer.getType().isCounterpartyCollateral() ? TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT_ISDA
								: TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT;
				}
			}
		}
		return null;
	}


	private boolean isSending(AccountingPositionTransferType transferType) {
		return (transferType.isCollateralPosting() && !transferType.isCounterpartyCollateral())
				|| (!transferType.isCollateralPosting() && transferType.isCounterpartyCollateral());
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}
}
