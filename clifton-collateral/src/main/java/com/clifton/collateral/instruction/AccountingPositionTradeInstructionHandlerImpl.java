package com.clifton.collateral.instruction;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.m2m.instruction.handler.AbstractAccountingPositionInstructionHandler;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.transfers.AbstractFreeAgainstPayment;
import com.clifton.instruction.messages.transfers.DeliverFreeAgainstPayment;
import com.clifton.instruction.messages.transfers.ExposureTypes;
import com.clifton.instruction.messages.transfers.ReceiveFreeAgainstPayment;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class AccountingPositionTradeInstructionHandlerImpl<I extends Instruction> extends AbstractAccountingPositionInstructionHandler<I> {

	private static final List<String> AcceptedIsinTypes = Arrays.asList(
			InvestmentType.BONDS,
			InvestmentType.FUNDS,
			InvestmentType.NOTES,
			InvestmentType.STOCKS
	);

	private static final Predicate<InvestmentSecurity> UseIsin = s -> Optional.ofNullable(s)
			.map(InvestmentSecurity::getInstrument)
			.map(InvestmentInstrument::getHierarchy)
			.map(InvestmentInstrumentHierarchy::getInvestmentType)
			.map(InvestmentType::getName)
			.filter(AcceptedIsinTypes::contains)
			.isPresent();

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private CollateralBalanceService collateralBalanceService;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accepts(AccountingPositionTransfer accountingPositionTransfer) {
		return !accountingPositionTransfer.getType().isCash();
	}


	@Override
	public InstructionMessage generateInstructionMessage(Instruction instruction, AccountingPositionTransferDetail positionTransferDetail) {
		AccountingPositionTransfer positionTransfer = positionTransferDetail.getPositionTransfer();
		AssertUtils.assertFalse(positionTransfer.getType().isCash(), "The position transfer type [%s] is not supported.", positionTransfer.getType().getName());

		InvestmentAccount custodianAccount = getCustodianAccount(positionTransfer);
		boolean isSending = isSending(positionTransfer);
		AbstractFreeAgainstPayment transferMessage = isSending ? new ReceiveFreeAgainstPayment() : new DeliverFreeAgainstPayment();

		transferMessage.setSenderBIC(getSenderBic());

		Optional.of(custodianAccount)
				.map(InvestmentAccount::getNumber)
				.ifPresent(transferMessage::setCustodyAccountNumber);
		Optional.of(custodianAccount)
				.map(InvestmentAccount::getIssuingCompany)
				.map(BusinessCompany::getBusinessIdentifierCode)
				.ifPresent(transferMessage::setReceiverBIC);

		transferMessage.setTransactionReferenceNumber(getTransactionReferenceNumber(instruction, positionTransfer, positionTransferDetail));
		transferMessage.setMessageFunctions(MessageFunctions.NEW_MESSAGE);
		transferMessage.setSettlementDate(positionTransfer.getSettlementDate());
		transferMessage.setTradeDate(positionTransfer.getSettlementDate());
		populateFromSecurity(transferMessage, positionTransferDetail.getSecurity());

		Optional.ofNullable(positionTransferDetail.getExistingPosition()).map(AccountingTransaction::isOpening).ifPresent(transferMessage::setOpen);

		Optional.of(positionTransferDetail).map(AccountingPositionTransferDetail::getQuantity).filter(b -> BigDecimal.ZERO.compareTo(b) != 0)
				.ifPresent(transferMessage::setQuantity);
		Optional.of(positionTransferDetail).map(AccountingPositionTransferDetail::getOriginalFace).filter(b -> BigDecimal.ZERO.compareTo(b) != 0)
				.ifPresent(transferMessage::setOriginalFaceAmount);

		String placeOfSettlementBic = getInvestmentSecurityUtilHandler().getPlaceOfSettlementStrict(positionTransferDetail.getSecurity());
		transferMessage.setPlaceOfSettlement(placeOfSettlementBic);
		transferMessage.setNetSettlementAmount(BigDecimal.ZERO);

		populateAgents(transferMessage, positionTransfer, instruction);

		Optional.ofNullable(getSettlementTransactionType(positionTransfer)).ifPresent(transferMessage::setSettlementTransactionType);
		Optional.ofNullable(getExposureType(transferMessage, positionTransfer)).ifPresent(transferMessage::setExposureType);

		return transferMessage;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private void populateFromSecurity(AbstractTradeMessage transferMessage, InvestmentSecurity investmentSecurity) {
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentUtils::getSecurityExchange)
				.map(InvestmentExchange::getMarketIdentifierCode)
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setSecurityExchangeCode);
		Optional.ofNullable(investmentSecurity)
				.filter(UseIsin)
				.map(InvestmentSecurity::getIsin)
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setIsin);
		Optional.ofNullable(investmentSecurity)
				.filter(s -> !UseIsin.test(s) || StringUtils.isEmpty(s.getIsin()))
				.map(InvestmentSecurity::getSymbol)
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setSecuritySymbol);
		Optional.ofNullable(investmentSecurity)
				.filter(s -> !StringUtils.isEmpty(s.getSymbol()))
				.map(InvestmentSecurity::getDescription)
				.map(d -> StringUtils.removeAll(d, "\\$"))
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setSecurityDescription);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getInstrument)
				.map(i -> this.getInvestmentSecurityUtilHandler().getISITCCodeStrict(i))
				.filter(c -> ISITCCodes.NONE != c)
				.ifPresent(transferMessage::setIsitcCode);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getOptionStyle)
				.map(OptionStyleTypes::toString)
				.map(TRIM_TO_NULL)
				.map(String::toUpperCase)
				.map(OptionStyleTypes::valueOf)
				.ifPresent(transferMessage::setOptionStyle);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getOptionType)
				.map(InvestmentSecurityOptionTypes::toString)
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setOptionType);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getOptionStrikePrice)
				.filter(b -> BigDecimal.ZERO.compareTo(b) != 0)
				.ifPresent(transferMessage::setStrikePrice);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getInstrument)
				.map(InvestmentInstrument::getTradingCurrency)
				.map(InvestmentSecurity::getSymbol)
				.map(TRIM_TO_NULL)
				.ifPresent(transferMessage::setSecurityCurrency);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getEndDate)
				.ifPresent(transferMessage::setSecurityExpirationDate);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getEndDate)
				.ifPresent(transferMessage::setMaturityDate);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getStartDate)
				.ifPresent(transferMessage::setIssueDate);
		Optional.ofNullable(investmentSecurity)
				.map(s -> this.getInvestmentSecurityUtilHandler().getInterestRate(s))
				.filter(b -> BigDecimal.ZERO.compareTo(b) != 0)
				.ifPresent(transferMessage::setInterestRate);
		Optional.ofNullable(investmentSecurity)
				.map(InvestmentSecurity::getPriceMultiplier)
				.filter(b -> BigDecimal.ZERO.compareTo(b) != 0)
				.ifPresent(transferMessage::setPriceMultiplier);
	}


	private void populateAgents(AbstractTradeMessage transferMessage, AccountingPositionTransfer positionTransfer, Instruction instruction) {
		InvestmentAccount clearingHoldingAccount = getClearingHoldingAccount(positionTransfer);
		InvestmentInstructionDelivery deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction, clearingHoldingAccount.getIssuingCompany(), clearingHoldingAccount.getBaseCurrency());
		Optional.ofNullable(deliveryInstruction)
				.map(InvestmentInstructionDelivery::getDeliveryABA)
				.map(aba -> new PartyIdentifier(aba, MessagePartyIdentifierTypes.USFW))
				.ifPresent(transferMessage::setClearingBrokerIdentifier);

		InvestmentAccount executingHoldingAccount = getExecutingHoldingAccount(positionTransfer);
		deliveryInstruction = getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(instruction, executingHoldingAccount.getIssuingCompany(), executingHoldingAccount.getBaseCurrency());
		Optional.ofNullable(deliveryInstruction)
				.map(InvestmentInstructionDelivery::getDeliveryABA)
				.map(aba -> new PartyIdentifier(aba, MessagePartyIdentifierTypes.USFW))
				.ifPresent(transferMessage::setExecutingBrokerIdentifier);
	}


	private InvestmentAccount getCustodianAccount(AccountingPositionTransfer positionTransfer) {
		List<InvestmentAccount> custodianAccounts = Stream.of(
				positionTransfer.getFromHoldingInvestmentAccount(),
				positionTransfer.getToHoldingInvestmentAccount()
		).filter(Objects::nonNull)
				.filter(a -> Objects.equals(Optional.of(a).map(InvestmentAccount::getType).map(InvestmentAccountType::getName).orElse(null), InvestmentAccountType.CUSTODIAN))
				.collect(Collectors.toList());
		ValidationUtils.assertEquals(1, custodianAccounts.size(), () -> String.format("Expected one custodian account on the accounting position transfer but encountered %s",
				custodianAccounts.isEmpty() ? "[0]" : custodianAccounts.stream().map(InvestmentAccount::getName).collect(Collectors.toList())));
		return custodianAccounts.stream().findFirst().orElse(null);
	}


	private SettlementTransactionTypes getSettlementTransactionType(AccountingPositionTransfer positionTransfer) {
		AccountingPositionTransferType transferType = positionTransfer.getType();
		if (transferType.isCollateral()) {
			return transferType.isCounterpartyCollateral() ? SettlementTransactionTypes.COLLATERAL_IN : SettlementTransactionTypes.COLLATERAL_OUT;
		}
		return SettlementTransactionTypes.TRADE;
	}


	private ExposureTypes getExposureType(AbstractTradeMessage transferMessage, AccountingPositionTransfer positionTransfer) {
		if (positionTransfer.getType().isCollateral() && positionTransfer.getSourceSystemTable() != null
				&& CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME.equals(positionTransfer.getSourceSystemTable().getName())) {
			CollateralBalance collateralBalance = getCollateralBalanceService().getCollateralBalance(positionTransfer.getSourceFkFieldId());
			if (collateralBalance != null) {
				switch (collateralBalance.getCollateralType().getName()) {
					case CollateralType.COLLATERAL_TYPE_FUTURES:
						return ExposureTypes.FUTURES;
					case CollateralType.COLLATERAL_TYPE_OTC:
						return ExposureTypes.OTC_DERIVATIVES;
					case CollateralType.COLLATERAL_TYPE_REPO:
						return InvestmentSecurityOptionTypes.CALL.toString().equalsIgnoreCase(transferMessage.getOptionType()) ? ExposureTypes.REPURCHASE_AGREEMENT
								: ExposureTypes.REVERSE_REPURCHASE_AGREEMENT;
					case CollateralType.COLLATERAL_TYPE_OPTIONS_ESCROW:
					case CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN:
						return ExposureTypes.EQUITY_OPTION;
					case CollateralType.COLLATERAL_TYPE_CLEARED_OTC:
					default:
						return ExposureTypes.OTC_INITIAL_MARGIN;
				}
			}
		}
		return null;
	}


	private boolean isSending(AccountingPositionTransfer positionTransfer) {
		return (Objects.equals(Optional.ofNullable(positionTransfer.getFromHoldingInvestmentAccount())
				.map(InvestmentAccount::getType).map(InvestmentAccountType::getName).orElse(null), InvestmentAccountType.CUSTODIAN));
	}


	private InvestmentAccount getExecutingHoldingAccount(AccountingPositionTransfer positionTransfer) {
		return isSending(positionTransfer) ? positionTransfer.getToHoldingInvestmentAccount() : positionTransfer.getFromHoldingInvestmentAccount();
	}


	private InvestmentAccount getClearingHoldingAccount(AccountingPositionTransfer positionTransfer) {
		return isSending(positionTransfer) ? positionTransfer.getFromHoldingInvestmentAccount() : positionTransfer.getToHoldingInvestmentAccount();
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}
}
