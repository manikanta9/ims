package com.clifton.collateral;


import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceCategories;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.Workflow;


/**
 * The <code>CollateralType</code> defines the type of collateral for a {@link CollateralBalance} record.
 * The type drives how the collateral balance is processed and where the data pulls from in the system.
 *
 * @author Mary Anderson
 */
@CacheByName
public class CollateralType extends NamedEntity<Short> {

	public static final String COLLATERAL_TYPE_CLEARED_OTC = "Cleared OTC Collateral";
	public static final String COLLATERAL_TYPE_FUTURES = "Futures Collateral";
	public static final String COLLATERAL_TYPE_OPTIONS_ESCROW = "Options Escrow Collateral";
	public static final String COLLATERAL_TYPE_OPTIONS_MARGIN = "Options Margin Collateral";
	public static final String COLLATERAL_TYPE_OTC = "OTC Collateral";
	public static final String COLLATERAL_TYPE_REPO = "REPO Collateral";

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	private boolean bookable;
	private boolean collateralInLocalCurrencySupported;

	/**
	 * OTC Collateral transfers are T+1 so when rebuilding the balance date we are subtracting 2 days
	 * For OTC Cleared, Futures and REPO Collateral, transfers are T+0 so when rebuilding the balance date we are subtracting 1 day
	 * <p>
	 * From Scott VanSickle:
	 * For example, today (4/19) we would be looking at Balance Date 4/18 on the OTC Collateral screen.
	 * For Collateral movements, we will book T+1 = 4/20, and then want our 4/18 OTC Collateral screen to update
	 * Technically triggering a rebuild for REPO when the transfer is for the next day changes nothing for the current day;
	 * However, we still rebuild the balance in case changes in the future to the UI would end up showing values affected by the rebuild.
	 */
	private int transferDateOffset;

	/**
	 * Collateral balances are limited to the following holding accounts.  The group is usually setup to be automatically rebuilt
	 * based on specific holding account type(s).
	 */
	private InvestmentAccountGroup accountGroup;

	/**
	 * Optional Workflow that can be used for reconciliation/affirmation/confirmation process
	 * Allowed to select workflow where there exists at least 1 security in the associated InvestmentGroup where there is a TradeType flagged as IsSingleFillTrade = 1 for its investment type.
	 */
	private Workflow workflow;

	private SystemTable detailTable;

	/**
	 * Defines the SystemBeanType of beans that can be used for Collateral Configuration for this collateral type.
	 * Used for UI functionality and server-side validation.
	 */
	private SystemBeanType systemBeanType;


	/**
	 * Discriminator column for selecting {@link CollateralBalance} derived entities
	 */
	private CollateralBalanceCategories category;


	/**
	 * Use Settlement date rather than transaction date.
	 */
	private boolean useSettlementDate;


	/**
	 * Allow event-driven collateral balance rebuilds.
	 * Temporary:  this can be removed when stale entity issues are resolved
	 */
	private boolean allowRebuildOnTransferEvent;

	////////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceCategories getCategory() {
		return this.category == null ? CollateralBalanceCategories.DEFAULT : this.category;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getTransferDateOffset() {
		return this.transferDateOffset;
	}


	public void setTransferDateOffset(int transferDateOffset) {
		this.transferDateOffset = transferDateOffset;
	}


	public boolean isBookable() {
		return this.bookable;
	}


	public void setBookable(boolean bookable) {
		this.bookable = bookable;
	}


	public boolean isCollateralInLocalCurrencySupported() {
		return this.collateralInLocalCurrencySupported;
	}


	public void setCollateralInLocalCurrencySupported(boolean collateralInLocalCurrencySupported) {
		this.collateralInLocalCurrencySupported = collateralInLocalCurrencySupported;
	}


	public InvestmentAccountGroup getAccountGroup() {
		return this.accountGroup;
	}


	public void setAccountGroup(InvestmentAccountGroup accountGroup) {
		this.accountGroup = accountGroup;
	}


	public SystemTable getDetailTable() {
		return this.detailTable;
	}


	public void setDetailTable(SystemTable detailTable) {
		this.detailTable = detailTable;
	}


	public SystemBeanType getSystemBeanType() {
		return this.systemBeanType;
	}


	public void setSystemBeanType(SystemBeanType systemBeanType) {
		this.systemBeanType = systemBeanType;
	}


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public void setCategory(CollateralBalanceCategories category) {
		this.category = category;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}


	public boolean isAllowRebuildOnTransferEvent() {
		return this.allowRebuildOnTransferEvent;
	}


	public void setAllowRebuildOnTransferEvent(boolean allowRebuildOnTransferEvent) {
		this.allowRebuildOnTransferEvent = allowRebuildOnTransferEvent;
	}
}
