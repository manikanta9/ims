package com.clifton.collateral;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CollateralProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "collateral";
	}


	@Override
	protected void configureApprovedPackageNames(@SuppressWarnings("unused") Set<String> approvedList) {
		approvedList.add("haircut");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> excludedServiceMethodNames = CollectionUtils.createHashSet(
				"getCollateralBalancePledgedBalanceList",
				"getCollateralBalanceLendingRepoList",
				"deleteCollateralBalance",
				"getCollateralBalance"
		);
		if (excludedServiceMethodNames.contains(method.getName())) {
			return true;
		}
		return super.isMethodSkipped(method);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveCollateralBalancePledgedTransactionListByCommand");
		return ignoredVoidSaveMethodSet;
	}
}
