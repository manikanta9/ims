package com.clifton.collateral.rules;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.collateral.CollateralPosition;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.rule.CollateralBalanceClientCounterpartyCollateralRuleEvaluator;
import com.clifton.collateral.balance.rule.CollateralBalancePricingMissingForSecurityRuleEvaluator;
import com.clifton.collateral.balance.rule.CollateralBalanceRuleEvaluatorContext;
import com.clifton.collateral.balance.rule.CollateralBalanceUnbookedCollateralTransfersRuleEvaluator;
import com.clifton.collateral.balance.rule.CollateralBalanceUnbookedEventsRuleEvaluator;
import com.clifton.collateral.balance.rule.CollateralBalanceUnbookedTradesRuleEvaluator;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author jonathanr
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CollateralBalanceRuleTests {

	@Mock
	private RuleViolationService ruleViolationService;
	@Mock
	private CollateralBalanceService collateralBalanceService;
	@Mock
	private RuleConfig ruleConfig;
	@Mock
	private EntityConfig entityConfig;
	@Mock
	private CollateralBalanceRuleEvaluatorContext collateralBalanceRuleEvaluatorContext;

	private CollateralBalance collateralBalance;


	public static CalendarBusinessDayService newCalendarBusinessDayService() {
		CalendarBusinessDayService result = Mockito.mock(CalendarBusinessDayService.class);

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.getDaysDifference((Date) args[1], (Date) args[0]);
		}).when(result).getBusinessDaysBetween(ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), (Integer) args[2]);
		}).when(result).getBusinessDayFrom(ArgumentMatchers.any(CalendarBusinessDayCommand.class), ArgumentMatchers.anyInt());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), (Integer) args[1]);
		}).when(result).getBusinessDayFrom(ArgumentMatchers.any(CalendarBusinessDayCommand.class), ArgumentMatchers.anyInt());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), -1);
		}).when(result).getPreviousBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class));

		Mockito.when(result.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenReturn(true);
		return result;
	}


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		CollateralBalance collateralBalance = Mockito.mock(CollateralBalance.class);
		Mockito.when(collateralBalance.getId()).thenReturn(12345);
		Mockito.when(collateralBalance.getHoldingInvestmentAccount()).thenReturn(InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount());
		Mockito.when(collateralBalance.getBalanceDate()).thenReturn(DateUtils.toDate("07/10/2019"));
		Mockito.when(collateralBalance.getCollateralType()).thenReturn(Mockito.mock(CollateralType.class));
		Mockito.when(collateralBalance.getCollateralType().getName()).thenReturn("Mock Collateral Type");
		this.collateralBalance = collateralBalance;
		Mockito.when(this.entityConfig.isExcluded()).thenReturn(false);
		Mockito.when(this.ruleConfig.getEntityConfig(Mockito.any())).thenReturn(this.entityConfig);
		Mockito.when(this.ruleViolationService.createRuleViolationWithCause(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenAnswer(invocation -> {
					RuleViolation violation = new RuleViolation();
					violation.setLinkedFkFieldId(invocation.getArgument(1));
					violation.setCauseFkFieldId(invocation.getArgument(2));
					violation.setCauseTable(invocation.getArgument(3));
					return violation;
				});
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                    Test Methods                        //////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void CollateralBalancePricingMissingForSecurityRuleEvaluatorTest() {
		CollateralBalancePricingMissingForSecurityRuleEvaluator evaluator = new CollateralBalancePricingMissingForSecurityRuleEvaluator();

		MarketDataRetriever marketDataRetriever = Mockito.mock(MarketDataRetriever.class);
		AccountingPositionDaily positionDaily = new AccountingPositionDaily();
		positionDaily.setRemainingCostBasisLocal(BigDecimal.ONE);
		positionDaily.setRemainingQuantity(BigDecimal.TEN);
		AccountingTransaction accountingTransaction = new AccountingTransaction();
		accountingTransaction.setInvestmentSecurity(InvestmentSecurityBuilder.newUSD());
		positionDaily.setAccountingTransaction(accountingTransaction);
		Mockito.when(this.collateralBalanceService.getCollateralAccountingPositionDailyLiveList(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(Collections.singletonList(positionDaily));
		Mockito.when(marketDataRetriever.getPriceDate(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyString())).thenReturn(null);
		evaluator.setCollateralBalanceService(this.collateralBalanceService);
		evaluator.setMarketDataRetriever(marketDataRetriever);
		evaluator.setRuleViolationService(this.ruleViolationService);

		List<RuleViolation> ruleViolationList = evaluator.evaluateRule(this.collateralBalance, this.ruleConfig, this.collateralBalanceRuleEvaluatorContext);
		Assertions.assertFalse(ruleViolationList.isEmpty(), "Rule Violation not found when expected");
	}


	@Test
	public void CollateralBalanceClientAndCounterpartyCollateralRuleEvaluatorTest() {
		CollateralBalanceClientCounterpartyCollateralRuleEvaluator evaluator = new CollateralBalanceClientCounterpartyCollateralRuleEvaluator();

		CollateralService collateralService = Mockito.mock(CollateralService.class);
		CollateralPosition testCollateralPosition1 = new CollateralPosition();
		AccountingAccount testAccountingAccount1 = new AccountingAccount();
		testAccountingAccount1.setNotOurAccount(true);
		testCollateralPosition1.setAccountingAccount(testAccountingAccount1);
		CollateralPosition testCollateralPosition2 = new CollateralPosition();
		AccountingAccount testAccountingAccount2 = new AccountingAccount();
		testAccountingAccount2.setNotOurAccount(false);
		testCollateralPosition2.setAccountingAccount(testAccountingAccount2);
		Mockito.when(collateralService.getCollateralPositionList(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(Arrays.asList(testCollateralPosition1, testCollateralPosition2));
		evaluator.setCollateralService(collateralService);
		evaluator.setCalendarBusinessDayService(newCalendarBusinessDayService());
		evaluator.setRuleViolationService(this.ruleViolationService);

		List<RuleViolation> ruleViolationList = evaluator.evaluateRule(this.collateralBalance, this.ruleConfig, this.collateralBalanceRuleEvaluatorContext);
		Assertions.assertFalse(ruleViolationList.isEmpty(), "Rule Violation not found when expected");
	}


	@Test
	public void CollateralBalanceUnbookedCollateralTransfersRuleEvaluatorTest() {
		CollateralBalanceUnbookedCollateralTransfersRuleEvaluator evaluator = new CollateralBalanceUnbookedCollateralTransfersRuleEvaluator();
		AccountingPositionTransferService accountingPositionTransferService = Mockito.mock(AccountingPositionTransferService.class);
		CollateralBalanceService collateralBalanceService = Mockito.mock(CollateralBalanceService.class);
		Mockito.when(collateralBalanceService.getCollateralBalanceList(ArgumentMatchers.any(CollateralBalanceSearchForm.class))).thenReturn(Collections.singletonList(this.collateralBalance));
		Mockito.when(accountingPositionTransferService.getAccountingPositionTransferList(ArgumentMatchers.any(AccountingPositionTransferSearchForm.class))).thenReturn(Collections.singletonList(Mockito.mock(AccountingPositionTransfer.class)));
		evaluator.setRuleViolationService(this.ruleViolationService);
		evaluator.setCollateralBalanceService(collateralBalanceService);
		evaluator.setAccountingPositionTransferService(accountingPositionTransferService);
		List<RuleViolation> ruleViolationList = evaluator.evaluateRule(this.collateralBalance, this.ruleConfig, this.collateralBalanceRuleEvaluatorContext);
		Assertions.assertFalse(ruleViolationList.isEmpty(), "Rule Violation not found when expected");
	}


	@Test
	public void CollateralBalanceUnbookedEventsRuleEvaluatorTest() {
		CollateralBalanceUnbookedEventsRuleEvaluator evaluator = new CollateralBalanceUnbookedEventsRuleEvaluator();
		AccountingEventJournalService accountingEventJournalService = Mockito.mock(AccountingEventJournalService.class);
		AccountingEventJournalDetail accountingEventJournalDetail = Mockito.mock(AccountingEventJournalDetail.class);
		AccountingEventJournal accountingEventJournal = Mockito.mock(AccountingEventJournal.class);
		InvestmentSecurityEvent investmentSecurityEvent = Mockito.mock(InvestmentSecurityEvent.class);
		Mockito.when(investmentSecurityEvent.getId()).thenReturn(1);
		Mockito.when(accountingEventJournal.getSecurityEvent()).thenReturn(investmentSecurityEvent);
		Mockito.when(accountingEventJournalDetail.getJournal()).thenReturn(accountingEventJournal);
		Mockito.when(accountingEventJournalService.getAccountingEventJournalDetailList(ArgumentMatchers.any(AccountingEventJournalDetailSearchForm.class))).thenReturn(Collections.singletonList(accountingEventJournalDetail));
		evaluator.setAccountingEventJournalService(accountingEventJournalService);
		evaluator.setRuleViolationService(this.ruleViolationService);
		List<RuleViolation> ruleViolationList = evaluator.evaluateRule(this.collateralBalance, this.ruleConfig, this.collateralBalanceRuleEvaluatorContext);
		Assertions.assertFalse(ruleViolationList.isEmpty(), "Rule Violation not found when expected");
	}


	@Test
	public void CollateralBalanceUnbookedTradesRuleEvaluatorTest() {
		CollateralBalanceUnbookedTradesRuleEvaluator evaluator = new CollateralBalanceUnbookedTradesRuleEvaluator();
		TradeService tradeService = Mockito.mock(TradeService.class);
		Mockito.when(tradeService.getTradeList(ArgumentMatchers.any(TradeSearchForm.class))).thenReturn(Collections.singletonList(Mockito.mock(Trade.class)));
		evaluator.setTradeService(tradeService);
		evaluator.setRuleViolationService(this.ruleViolationService);
		List<RuleViolation> ruleViolationList = evaluator.evaluateRule(this.collateralBalance, this.ruleConfig, this.collateralBalanceRuleEvaluatorContext);
		Assertions.assertFalse(ruleViolationList.isEmpty(), "Rule Violation not found when expected");
	}
}
