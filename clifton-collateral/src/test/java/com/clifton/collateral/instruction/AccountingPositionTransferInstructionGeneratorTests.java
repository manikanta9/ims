package com.clifton.collateral.instruction;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.m2m.instruction.AccountingPositionTransferInstructionGenerator;
import com.clifton.accounting.m2m.instruction.handler.AccountingPositionInstructionHandler;
import com.clifton.accounting.m2m.instruction.handler.AccountingPositionInstructionHandlerLocator;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.account.InvestmentAccountTypeBuilder;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@ExtendWith(MockitoExtension.class)
public class AccountingPositionTransferInstructionGeneratorTests<I extends Instruction> {

	public static final BiFunction<Field, Object, Object> GetFieldValue = (f, o) -> {
		try {
			return f.get(o);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException("Could net fetch field value " + f, e);
		}
	};

	@InjectMocks
	public AccountingPositionTransferInstructionGenerator accountingPositionTransferInstructionGenerator = new AccountingPositionTransferInstructionGenerator();

	@Mock
	public InstructionCancellationHandler instructionCancellationHandler;

	@Mock
	public AccountingPositionTransfer accountingPositionTransfer;
	@Mock
	public InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;
	@Mock
	public InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	@Mock
	public AccountingPositionTransferDetail accountingPositionTransferDetail;
	@Mock
	public InvestmentInstructionItem investmentInstructionItem;
	@Mock
	public AccountingTransaction accountingTransaction;
	@Mock
	public AccountingPositionTransferType accountingPositionTransferType;
	@Mock
	public InvestmentInstructionDeliveryService investmentInstructionDeliveryService;
	@Mock
	public AccountingPositionTransferService accountingPositionTransferService;
	@Mock
	public BusinessCompanyUtilHandler businessCompanyUtilHandler;
	@Mock
	public InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;
	@Mock
	private CollateralBalanceService collateralBalanceService;
	@InjectMocks
	private AccountingPositionTradeInstructionHandlerImpl<I> accountingPositionTradeInstructionHandler;
	@InjectMocks
	private AccountingPositionCashInstructionHandlerImpl<I> accountingPositionCashInstructionHandler;


	@BeforeEach
	public void beforeTest() {
		TestAccountingPositionInstructionHandlerLocator testAccountingPositionInstructionHandlerLocator = new TestAccountingPositionInstructionHandlerLocator(
				this.accountingPositionCashInstructionHandler,
				this.accountingPositionTradeInstructionHandler
		);
		this.accountingPositionTransferInstructionGenerator.setAccountingPositionInstructionHandlerLocator(testAccountingPositionInstructionHandlerLocator);
		this.accountingPositionTradeInstructionHandler.setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);
	}


	@SuppressWarnings("ResultOfMethodCallIgnored")
	@Test
	public void generateMessageTest() {
		Mockito.doReturn(55).when(this.investmentInstructionItem).getFkFieldId();
		Mockito.doReturn(this.accountingPositionTransfer).when(this.accountingPositionTransferService).getAccountingPositionTransfer(55);
		Mockito.doReturn(AccountingPositionTransferType.BETWEEN_TRANSFER).when(this.accountingPositionTransferType).getName();
		Mockito.doReturn(this.accountingPositionTransferType).when(this.accountingPositionTransfer).getType();
		Mockito.doReturn(Collections.singletonList(this.accountingPositionTransferDetail)).when(this.accountingPositionTransfer).getDetailList();
		Mockito.doReturn(this.accountingPositionTransfer).when(this.accountingPositionTransferDetail).getPositionTransfer();
		Mockito.doReturn("PARAMETRICBIC").when(this.businessCompanyUtilHandler).getParametricBusinessIdentifierCode();
		InvestmentAccount fromHoldingAccount = InvestmentAccountBuilder
				.createEmptyInvestmentAccount()
				.withNumber("11-123456")
				.withType(InvestmentAccountTypeBuilder.createCustodian().toInvestmentAccountType())
				.withIssuingCompany(BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany())
				.withBaseCurrency(InvestmentSecurityBuilder.newUSD())
				.toInvestmentAccount();
		fromHoldingAccount.getIssuingCompany().setBusinessIdentifierCode("HOLDINGBIC");
		Mockito.doReturn(fromHoldingAccount)
				.when(this.accountingPositionTransfer).getFromHoldingInvestmentAccount();
		InvestmentAccount toHoldingAccount = InvestmentAccountBuilder
				.createEmptyInvestmentAccount()
				.withType(InvestmentAccountTypeBuilder.createBroker().toInvestmentAccountType())
				.withIssuingCompany(BusinessCompanyBuilder.createTestingCompany2().toBusinessCompany())
				.withBaseCurrency(InvestmentSecurityBuilder.newUSD())
				.toInvestmentAccount();
		Mockito.doReturn(toHoldingAccount)
				.when(this.accountingPositionTransfer).getToHoldingInvestmentAccount();
		Mockito.doReturn(DateUtils.toDate("12/13/2012")).when(this.accountingPositionTransfer).getSettlementDate();
		Mockito.doReturn("IMS123II;IMS456APT;IMS789PTD")
				.when(this.instructionUtilHandler).getTransactionReferenceNumber(ArgumentMatchers.argThat(l ->
				Arrays.asList(this.investmentInstructionItem, this.accountingPositionTransfer, this.accountingPositionTransferDetail).containsAll(l)));
		InvestmentSecurity investmentSecurity = InvestmentSecurityBuilder.newBond("TEST").toInvestmentSecurity();
		investmentSecurity.setOptionType(InvestmentSecurityOptionTypes.PUT);
		investmentSecurity.setOptionStrikePrice(new BigDecimal("34.56"));
		investmentSecurity.setOptionStyle(OptionStyleTypes.BERMUDAN);
		Mockito.doReturn(investmentSecurity).when(this.accountingPositionTransferDetail).getSecurity();
		Mockito.doReturn(new BigDecimal("21.2")).when(this.accountingPositionTransferDetail).getOriginalFace();
		Mockito.doReturn(new BigDecimal("11.3")).when(this.accountingPositionTransferDetail).getQuantity();
		Mockito.doReturn(ISITCCodes.DISC).when(this.investmentSecurityUtilHandler).getISITCCodeStrict(investmentSecurity.getInstrument());
		Mockito.doReturn("DTC").when(this.investmentSecurityUtilHandler).getPlaceOfSettlementStrict(investmentSecurity);
		Mockito.doReturn(new BigDecimal("78.99")).when(this.investmentSecurityUtilHandler).getInterestRate(investmentSecurity);
		Mockito.doReturn(this.accountingTransaction).when(this.accountingPositionTransferDetail).getExistingPosition();
		Mockito.doReturn(true).when(this.accountingTransaction).isOpening();
		InvestmentInstructionDelivery clearingInvestmentInstructionDelivery = new InvestmentInstructionDelivery();
		clearingInvestmentInstructionDelivery.setDeliveryABA("milli");
		InvestmentInstructionDelivery executingInvestmentInstructionDelivery = new InvestmentInstructionDelivery();
		executingInvestmentInstructionDelivery.setDeliveryABA("vanilli");
		Mockito.doAnswer(i -> {
			Object[] arguments = i.getArguments();
			if (Objects.equals(arguments[1], fromHoldingAccount.getIssuingCompany())) {
				MatcherAssert.assertThat(fromHoldingAccount.getBaseCurrency(), IsEqual.equalTo(arguments[2]));
				return clearingInvestmentInstructionDelivery;
			}
			else if (Objects.equals(arguments[1], toHoldingAccount.getIssuingCompany())) {
				MatcherAssert.assertThat(toHoldingAccount.getBaseCurrency(), IsEqual.equalTo(arguments[2]));
				return executingInvestmentInstructionDelivery;
			}
			else {
				Assertions.fail();
				return null;
			}
		}).when(this.investmentInstructionDeliveryUtilHandler).getInvestmentInstructionDelivery(
				ArgumentMatchers.same(this.investmentInstructionItem),
				ArgumentMatchers.any(BusinessCompany.class),
				ArgumentMatchers.any(InvestmentSecurity.class)
		);

		List<InstructionMessage> messageList = this.accountingPositionTransferInstructionGenerator.generateInstructionMessageList(this.investmentInstructionItem);
		Assertions.assertFalse(messageList.isEmpty());
		Assertions.assertEquals(1, messageList.size());

		InstructionMessage instructionMessage = messageList.get(0);

		Map<String, Object> objectMap = BeanUtils.describe(instructionMessage)
				.entrySet()
				.stream()
				.filter(e -> Objects.nonNull(e.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		Map<String, Object> expectedMap = Stream.of(
				new AbstractMap.SimpleImmutableEntry<>("interestRate", new BigDecimal("78.99")),
				new AbstractMap.SimpleImmutableEntry<>("quantity", new BigDecimal("11.3")),
				new AbstractMap.SimpleImmutableEntry<>("transactionReferenceNumber", "IMS123II;IMS456APT;IMS789PTD"),
				new AbstractMap.SimpleImmutableEntry<>("buy", false),
				new AbstractMap.SimpleImmutableEntry<>("optionStyle", OptionStyleTypes.BERMUDAN),
				new AbstractMap.SimpleImmutableEntry<>("isitcCode", ISITCCodes.DISC),
				new AbstractMap.SimpleImmutableEntry<>("tradeDate", DateUtils.toDate("12/13/2012")),
				new AbstractMap.SimpleImmutableEntry<>("settlementDate", DateUtils.toDate("12/13/2012")),
				new AbstractMap.SimpleImmutableEntry<>("priceMultiplier", new BigDecimal("0.01")),
				new AbstractMap.SimpleImmutableEntry<>("optionType", "PUT"),
				new AbstractMap.SimpleImmutableEntry<>("securitySymbol", "TEST"),
				new AbstractMap.SimpleImmutableEntry<>("placeOfSettlement", "DTC"),
				new AbstractMap.SimpleImmutableEntry<>("originalFaceAmount", new BigDecimal("21.2")),
				new AbstractMap.SimpleImmutableEntry<>("securityCurrency", "USD"),
				new AbstractMap.SimpleImmutableEntry<>("messageFunctions", MessageFunctions.NEW_MESSAGE),
				new AbstractMap.SimpleImmutableEntry<>("custodyAccountNumber", "11-123456"),
				new AbstractMap.SimpleImmutableEntry<>("strikePrice", new BigDecimal("34.56")),
				new AbstractMap.SimpleImmutableEntry<>("open", true),
				new AbstractMap.SimpleImmutableEntry<>("clearingBrokerIdentifier", new PartyIdentifier("milli", MessagePartyIdentifierTypes.USFW)),
				new AbstractMap.SimpleImmutableEntry<>("executingBrokerIdentifier", new PartyIdentifier("vanilli", MessagePartyIdentifierTypes.USFW)),
				new AbstractMap.SimpleImmutableEntry<>("receiverBIC", "HOLDINGBIC"),
				new AbstractMap.SimpleImmutableEntry<>("senderBIC", "PARAMETRICBIC"),
				new AbstractMap.SimpleImmutableEntry<>("repurchaseAgreement", false),
				new AbstractMap.SimpleImmutableEntry<>("netSettlementAmount", BigDecimal.ZERO),
				new AbstractMap.SimpleImmutableEntry<>("settlementTransactionType", SettlementTransactionTypes.TRADE)

		).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		Set<String> expectedMissing = new HashSet<>(objectMap.keySet());
		expectedMissing.removeAll(expectedMap.keySet());
		Set<String> objectMissing = new HashSet<>(expectedMap.keySet());
		objectMissing.removeAll(objectMap.keySet());
		expectedMissing.addAll(objectMissing);

		Assertions.assertEquals(expectedMap.size(), objectMap.size(), expectedMissing.toString());
		Assertions.assertTrue(objectMap.keySet().containsAll(expectedMap.keySet()));
		Map<String, Object> copiedMap = new HashMap<>(expectedMap);
		copiedMap.values().removeAll(objectMap.values());
		Assertions.assertTrue(objectMap.values().containsAll(expectedMap.values()), copiedMap.toString());
	}


	private static class TestAccountingPositionInstructionHandlerLocator extends AccountingPositionInstructionHandlerLocator {

		private List<AccountingPositionInstructionHandler> handlers;


		TestAccountingPositionInstructionHandlerLocator(AccountingPositionInstructionHandler... handlerArray) {
			this.handlers = Arrays.stream(handlerArray).collect(Collectors.toList());
		}


		@Override
		protected List<AccountingPositionInstructionHandler> initialize() {
			return this.handlers;
		}
	}
}
