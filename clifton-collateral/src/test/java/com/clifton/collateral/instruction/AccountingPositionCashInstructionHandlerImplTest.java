package com.clifton.collateral.instruction;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetailBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferTypeBuilder;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionBuilder;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionItemBuilder;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBuilder;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryTypeBuilder;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategoryBuilder;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinitionBuilder;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusBuilder;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.report.definition.ReportBuilder;
import com.clifton.rule.violation.status.RuleViolationStatusBuilder;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
public class AccountingPositionCashInstructionHandlerImplTest<I extends Instruction> {

	@Mock
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;

	@Mock
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	@Mock
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;

	@Mock
	private CollateralBalanceService collateralBalanceService;

	@InjectMocks
	private AccountingPositionCashInstructionHandlerImpl<I> accountingPositionCashInstructionHandler;


	@Test
	public void clientCashCollateralTransferPost() {
		InvestmentInstruction investmentInstruction = InvestmentInstructionBuilder.createEmpty()
				.withId(57649)
				.withDefinition(InvestmentInstructionDefinitionBuilder.createEmpty()
						.withId(211)
						.withCategory(InvestmentInstructionCategoryBuilder.createEmpty()
								.withId((short) 10)
								.withTable(SystemTableBuilder.createAccountingPositionTransfer().toSystemTable())
								.withName("Cash Position Transfer Counterparty")
								.withDescription("Cash Position Transfer Counterparty Description")
								.withDateLabel("Transfer Date")
								.asCash()
								.toInvestmentInstructionCategory())
						.withName("OTC Cash Collateral - Bank of America N.A.")
						.withWorkflowState(WorkflowStateBuilder.createActiveInstruction().toWorkflowState())
						.withReport(ReportBuilder.createGenericTrade().toReport())
						.withDeliveryType(InvestmentInstructionDeliveryTypeBuilder.createCashCollateralBilateralOTC().toInvestmentInstructionDeliveryType())
						.toInvestmentInstructionDefinition()
				)
				.withRecipientCompany(BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany())
				.withStatus(InvestmentInstructionStatusBuilder.createCompleted().toInvestmentInstructionStatus())
				.withInstructionDate(DateUtils.toDate("06/18/2018"))
				.toInvestmentInstruction();
		InvestmentInstructionItem investmentInstructionItem = InvestmentInstructionItemBuilder.createEmpty()
				.withId(822826)
				.withInstruction(investmentInstruction)
				.withFkFieldId(23546)
				.withStatus(InvestmentInstructionStatusBuilder.createCompleted().toInvestmentInstructionStatus())
				.toInvestmentInstructionItem();

		AccountingPositionTransfer accountingPositionTransfer =
				AccountingPositionTransferBuilder.createEmpty()
						.withId(23546)
						.withType(AccountingPositionTransferTypeBuilder.createClientCashCollateralTransferPost().toAccountingPositionTransferType())
						.withViolationStatus(RuleViolationStatusBuilder.createProcessedSuccess().toRuleViolationStatus())
						.withFromClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount())
						.withFromHoldingInvestmentAccount(InvestmentAccountBuilder.createTestAccount1()
								.withIssuingCompany(BusinessCompanyBuilder.createBankOfAmericaCorp()
										.withBusinessIdentifierCode("111111")
										.toBusinessCompany()
								)
								.withNumber("aaaaaaaaa")
								.toInvestmentAccount())
						.withToClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount())
						.withToHoldingInvestmentAccount(InvestmentAccountBuilder.createTestAccount2()
								.withIssuingCompany(BusinessCompanyBuilder.createJPMorganChaseCompany()
										.withBusinessIdentifierCode("22222222")
										.toBusinessCompany()
								)
								.withNumber("bbbbbbbbbb")
								.toInvestmentAccount())
						.asUseOriginalTransactionDate()
						.withTransactionDate(DateUtils.toDate("06/18/2018"))
						.withSettlementDate(DateUtils.toDate("06/18/2018"))
						.withBookingDate(DateUtils.toDate("06/18/2018"))
						.withSourceSystemTable(SystemTableBuilder.createCollateralBalance().toSystemTable())
						.withSourceFkFieldId(1225991)
						.toAccountPositionTransfer();

		AccountingPositionTransferDetail detail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withId(98089)
				.withPositionTransfer(accountingPositionTransfer)
				.withSecurity(InvestmentSecurityBuilder.newUSD())
				.withExchangeRateToBase(BigDecimal.ONE)
				.withPositionCostBasis(new BigDecimal("1160000.00"))
				.withOriginalPositionOpenDate(DateUtils.toDate("06/18/2018"))
				.toAccountingPositionTransferDetail();
		InvestmentInstructionDelivery delivery = InvestmentInstructionDeliveryBuilder.createEmpty()
				.withDeliveryCompany(BusinessCompanyBuilder.createEmptyBusinessCompany().withBusinessIdentifierCode("Intermediary Code").toBusinessCompany())
				.withDeliveryAccountNumber("Delivery Account Number")
				.toInvestmentInstructionDelivery();

		Mockito.when(this.businessCompanyUtilHandler.getParametricBusinessIdentifierCode()).thenReturn("PARAMETRIC");
		Mockito.when(this.instructionUtilHandler.getTransactionReferenceNumber(Arrays.asList(investmentInstructionItem, accountingPositionTransfer, detail))).thenReturn("Reference Number");
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.same(investmentInstructionItem),
				ArgumentMatchers.same(accountingPositionTransfer.getToHoldingInvestmentAccount().getIssuingCompany()),
				ArgumentMatchers.same(accountingPositionTransfer.getToClientInvestmentAccount().getBaseCurrency())))
				.thenReturn(delivery);

		InstructionMessage instructionMessage = this.accountingPositionCashInstructionHandler.generateInstructionMessage(investmentInstructionItem, detail);

		Map<String, Object> properties = BeanUtils.describe(instructionMessage);
		MatcherAssert.assertThat(instructionMessage.getClass(), IsEqual.equalTo(GeneralFinancialInstitutionTransferMessage.class));
		MatcherAssert.assertThat(properties.get("senderBIC"), IsEqual.equalTo("PARAMETRIC"));
		MatcherAssert.assertThat(properties.get("receiverBIC"), IsEqual.equalTo("111111"));
		MatcherAssert.assertThat(properties.get("transactionReferenceNumber"), IsEqual.equalTo("Reference Number"));
		MatcherAssert.assertThat(properties.get("messagePurpose"), IsEqual.equalTo(TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT));
		MatcherAssert.assertThat(DateUtils.fromDate((Date) properties.get("valueDate"), DateUtils.DATE_FORMAT_INPUT), IsEqual.equalTo("06/18/2018"));
		MatcherAssert.assertThat(properties.get("amount"), IsEqual.equalTo(new BigDecimal("1160000.00")));
		MatcherAssert.assertThat(properties.get("currency"), IsEqual.equalTo("USD"));

		MatcherAssert.assertThat(properties.get("custodyAccountNumber"), IsEqual.equalTo("aaaaaaaaa"));
		MatcherAssert.assertThat(properties.get("custodyBIC"), IsEqual.equalTo("111111"));
		MatcherAssert.assertThat(properties.get("beneficiaryBIC"), IsEqual.equalTo("22222222"));
		Assertions.assertNull(properties.get("beneficiaryAccountNumber"));
		MatcherAssert.assertThat(properties.get("intermediaryBIC"), IsEqual.equalTo("Intermediary Code"));
		MatcherAssert.assertThat(properties.get("intermediaryAccountNumber"), IsEqual.equalTo("Delivery Account Number"));
	}


	@Test
	public void clientCashCollateralTransferReturn() {
		InvestmentInstruction investmentInstruction = InvestmentInstructionBuilder.createEmpty()
				.withId(57648)
				.withDefinition(InvestmentInstructionDefinitionBuilder.createEmpty()
						.withId(271)
						.withCategory(InvestmentInstructionCategoryBuilder.createEmpty()
								.withId((short) 10)
								.withTable(SystemTableBuilder.createAccountingPositionTransfer().toSystemTable())
								.withName("Cash Position Transfer Counterparty")
								.withDescription("Cash Position Transfer Counterparty Description")
								.withDateLabel("Transfer Date")
								.asCash()
								.toInvestmentInstructionCategory())
						.withName("OTC Cash Collateral - BNP Paribas - WARF Only")
						.withWorkflowState(WorkflowStateBuilder.createActiveInstruction().toWorkflowState())
						.withReport(ReportBuilder.createGenericTrade().toReport())
						.withDeliveryType(InvestmentInstructionDeliveryTypeBuilder.createCashCollateralBilateralOTC().toInvestmentInstructionDeliveryType())
						.toInvestmentInstructionDefinition()
				)
				.withRecipientCompany(BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany())
				.withStatus(InvestmentInstructionStatusBuilder.createCompleted().toInvestmentInstructionStatus())
				.withInstructionDate(DateUtils.toDate("06/18/2018"))
				.toInvestmentInstruction();
		InvestmentInstructionItem investmentInstructionItem = InvestmentInstructionItemBuilder.createEmpty()
				.withId(822819)
				.withInstruction(investmentInstruction)
				.withFkFieldId(23508)
				.withStatus(InvestmentInstructionStatusBuilder.createCompleted().toInvestmentInstructionStatus())
				.toInvestmentInstructionItem();

		AccountingPositionTransfer accountingPositionTransfer =
				AccountingPositionTransferBuilder.createEmpty()
						.withId(23508)
						.withType(AccountingPositionTransferTypeBuilder.createClientCashCollateralTransferReturn().toAccountingPositionTransferType())
						.withViolationStatus(RuleViolationStatusBuilder.createProcessedSuccess().toRuleViolationStatus())
						.withFromClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount())
						.withFromHoldingInvestmentAccount(InvestmentAccountBuilder.createTestAccount1()
								.withIssuingCompany(BusinessCompanyBuilder.createBankOfAmericaCorp()
										.withBusinessIdentifierCode("111111")
										.toBusinessCompany()
								)
								.withNumber("aaaaaaaaa")
								.toInvestmentAccount())
						.withToClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount())
						.withToHoldingInvestmentAccount(InvestmentAccountBuilder.createTestAccount2()
								.withIssuingCompany(BusinessCompanyBuilder.createJPMorganChaseCompany()
										.withBusinessIdentifierCode("22222222")
										.toBusinessCompany()
								)
								.withNumber("bbbbbbbbbb")
								.toInvestmentAccount())
						.asUseOriginalTransactionDate()
						.withTransactionDate(DateUtils.toDate("06/18/2018"))
						.withSettlementDate(DateUtils.toDate("06/18/2018"))
						.withBookingDate(DateUtils.toDate("06/18/2018"))
						.withSourceSystemTable(SystemTableBuilder.createCollateralBalance().toSystemTable())
						.withSourceFkFieldId(1226083)
						.toAccountPositionTransfer();

		AccountingPositionTransferDetail detail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withId(97500)
				.withPositionTransfer(accountingPositionTransfer)
				.withSecurity(InvestmentSecurityBuilder.newUSD())
				.withExchangeRateToBase(BigDecimal.ONE)
				.withPositionCostBasis(new BigDecimal("1290000.00"))
				.withOriginalPositionOpenDate(DateUtils.toDate("06/18/2018"))
				.toAccountingPositionTransferDetail();
		InvestmentInstructionDelivery delivery = InvestmentInstructionDeliveryBuilder.createEmpty()
				.withDeliveryCompany(BusinessCompanyBuilder.createEmptyBusinessCompany().withBusinessIdentifierCode("Intermediary Code").toBusinessCompany())
				.withDeliveryAccountNumber("Delivery Account Number")
				.toInvestmentInstructionDelivery();

		Mockito.when(this.businessCompanyUtilHandler.getParametricBusinessIdentifierCode()).thenReturn("PARAMETRIC");
		Mockito.when(this.instructionUtilHandler.getTransactionReferenceNumber(Arrays.asList(investmentInstructionItem, accountingPositionTransfer, detail))).thenReturn("Reference Number");
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.same(investmentInstructionItem),
				ArgumentMatchers.same(accountingPositionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany()),
				ArgumentMatchers.same(accountingPositionTransfer.getFromClientInvestmentAccount().getBaseCurrency())))
				.thenReturn(delivery);

		InstructionMessage instructionMessage = this.accountingPositionCashInstructionHandler.generateInstructionMessage(investmentInstructionItem, detail);

		Map<String, Object> properties = BeanUtils.describe(instructionMessage);
		MatcherAssert.assertThat(instructionMessage.getClass(), IsEqual.equalTo(NoticeToReceiveTransferMessage.class));
		MatcherAssert.assertThat(properties.get("senderBIC"), IsEqual.equalTo("PARAMETRIC"));
		MatcherAssert.assertThat(properties.get("transactionReferenceNumber"), IsEqual.equalTo("Reference Number"));
		MatcherAssert.assertThat(properties.get("messagePurpose"), IsEqual.equalTo(TransferMessagePurposes.COLLATERAL_SWAP_PAYMENT));
		MatcherAssert.assertThat(DateUtils.fromDate((Date) properties.get("valueDate"), DateUtils.DATE_FORMAT_INPUT), IsEqual.equalTo("06/18/2018"));
		MatcherAssert.assertThat(properties.get("amount"), IsEqual.equalTo(new BigDecimal("1290000.00")));
		MatcherAssert.assertThat(properties.get("currency"), IsEqual.equalTo("USD"));

		MatcherAssert.assertThat(properties.get("receiverAccountNumber"), IsEqual.equalTo("bbbbbbbbbb"));
		MatcherAssert.assertThat(properties.get("orderingBIC"), IsEqual.equalTo("22222222"));
		MatcherAssert.assertThat(properties.get("intermediaryBIC"), IsEqual.equalTo("Intermediary Code"));
	}
}
