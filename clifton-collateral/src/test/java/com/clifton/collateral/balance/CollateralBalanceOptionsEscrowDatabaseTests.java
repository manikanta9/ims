package com.clifton.collateral.balance;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralInMemoryDatabaseContext;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedService;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransaction;
import com.clifton.collateral.balance.pledged.CollateralBalancePledgedTransactionCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.TestDataUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This tests the full Collateral Options Escrow process from pledging, to creating transfers, booking them,
 * rebuilding the collateral balances and validating the results.
 *
 * @author stevenf
 */
@CollateralInMemoryDatabaseContext
public class CollateralBalanceOptionsEscrowDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private AccountingBookingService<AccountingPositionTransfer> accountingBookingService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CollateralBalanceRebuildService collateralBalanceRebuildService;

	@Resource
	private CollateralBalanceService collateralBalanceService;

	@Resource
	private CollateralService collateralService;

	@Resource
	private CollateralBalancePledgedService collateralBalancePledgedService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = getSecurityUserService().getSecurityUser(userTemplate.getId());
		if (user == null) {
			getSecurityUserService().saveSecurityUser(userTemplate);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Command Methods   ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOptionsEscrowCollateralManualProcess() {
		for (String date : COLLATERAL_BALANCE_DATES) {
			processOptionsEscrowCollateralBalanceManually(date);
		}
		//Rebuild one more day and validate
		getCollateralBalance("08/11/2017");
	}


	@Test
	@Disabled
	// FIXME - WorkflowManagerObserver is interfering
	public void testOptionsEscrowCollateralWorkflowProcess() {
		for (String date : COLLATERAL_BALANCE_DATES) {
			processOptionsEscrowCollateralBalanceWorkflow(date);
		}
		//Rebuild one more day and validate
		getCollateralBalance("08/11/2017");
	}


	private CollateralBalance processOptionsEscrowCollateralBalanceManually(String date) {
		CollateralBalance collateralBalance = getCollateralBalance(date);

		//Generate pledged collateral
		CollateralBalancePledgedTransactionCommand generatePledgedTransactionsCommand = new CollateralBalancePledgedTransactionCommand();
		generatePledgedTransactionsCommand.setGeneratePledgedCollateral(true);
		generatePledgedTransactionsCommand.setCollateralBalanceId(collateralBalance.getId());
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedService().getCollateralBalancePledgedTransactionListByCommand(generatePledgedTransactionsCommand);
		if (!CollectionUtils.isEmpty(pledgedTransactionList)) {
			pledgedTransactionList.sort(Comparator.comparing(CollateralBalancePledgedTransaction::getPostedCollateralQuantity));
		}

		//Validate pledged collateral and prep command for saving
		String[] expectedPledgedTransactions = EXPECTED_PLEDGED_TRANSACTIONS_MAP.get(date);
		List<CollateralBalancePledgedTransactionCommand> pledgedTransactionCommandList = new ArrayList<>();
		for (int i = 0; i < pledgedTransactionList.size(); i++) {
			CollateralBalancePledgedTransaction pledgedTransaction = pledgedTransactionList.get(i);
			Assertions.assertEquals(expectedPledgedTransactions[i], TestDataUtils.toStringFormatted(pledgedTransaction, EXPECTED_PLEDGED_TRANSACTIONS_FIELD_PATHS));
			CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
			command.setPostedDate(pledgedTransaction.getPostedDate());
			command.setHoldingInvestmentAccountId(pledgedTransaction.getHoldingInvestmentAccount().getId());
			command.setPositionInvestmentSecurityId(pledgedTransaction.getPositionInvestmentSecurity().getId());
			command.setPostedCollateralInvestmentSecurityId(pledgedTransaction.getPostedCollateralInvestmentSecurity().getId());
			command.setPostedCollateralQuantity(pledgedTransaction.getPostedCollateralQuantity());
			pledgedTransactionCommandList.add(command);
		}

		//Save pledged collateral
		BeanListCommand<CollateralBalancePledgedTransactionCommand> savePledgedTransactionsCommand = new BeanListCommand<>();
		savePledgedTransactionsCommand.setBeanList(pledgedTransactionCommandList);
		getCollateralBalancePledgedService().saveCollateralBalancePledgedTransactionListByCommand(collateralBalance.getId(), savePledgedTransactionsCommand);

		//Generate position transfers returns from pledged collateral and automatically BookAndPost)
		CollateralBalancePledgedTransactionCommand generatePositionTransferCommand = new CollateralBalancePledgedTransactionCommand();
		generatePositionTransferCommand.setBookAndPost(true);
		generatePositionTransferCommand.setPostCollateral(false);
		generatePositionTransferCommand.setCollateralBalanceId(collateralBalance.getId());
		getCollateralBalancePledgedService().createCollateralBalancePositionTransfers(generatePositionTransferCommand);

		//Generate position transfers posts from pledged collateral and automatically BookAndPost)
		generatePositionTransferCommand = new CollateralBalancePledgedTransactionCommand();
		generatePositionTransferCommand.setBookAndPost(true);
		generatePositionTransferCommand.setPostCollateral(true);
		generatePositionTransferCommand.setCollateralBalanceId(collateralBalance.getId());
		getCollateralBalancePledgedService().createCollateralBalancePositionTransfers(generatePositionTransferCommand);

		//Find and validate booked position transfers
		AccountingPositionTransferSearchForm positionTransferSearchForm = new AccountingPositionTransferSearchForm();
		positionTransferSearchForm.setJournalBooked(true);
		positionTransferSearchForm.setCollateralTransfer(true);
		positionTransferSearchForm.setTransactionDate(collateralBalance.getBalanceDate());
		positionTransferSearchForm.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(collateralBalance.getBalanceDate()), collateralBalance.getCollateralType().getTransferDateOffset()));
		positionTransferSearchForm.setFromClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());
		positionTransferSearchForm.setToClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());

		List<AccountingPositionTransfer> positionTransferList = getAccountingPositionTransferService().getAccountingPositionTransferList(positionTransferSearchForm);
		Assertions.assertEquals((int) EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP.get(date), CollectionUtils.getSize(positionTransferList), "Expected " + EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP.get(date) + " transfer(s) for balance date " + date + " but found " + CollectionUtils.getSize(positionTransferList));
		return collateralBalance;
	}


	private CollateralBalance processOptionsEscrowCollateralBalanceWorkflow(String date) {
		CollateralBalance collateralBalance = getCollateralBalance(date);
		collateralBalance = (CollateralBalance) transitionIfPresent("Pledge Collateral Transactions", CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		ValidationUtils.assertEquals("Collateral Pledged", collateralBalance.getWorkflowState().getName(), "Failed to transition to 'Collateral Pledged'");
		collateralBalance = (CollateralBalance) transitionIfPresent("Create and Book Return Collateral Transfers", CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		ValidationUtils.assertEquals("Collateral Returned", collateralBalance.getWorkflowState().getName(), "Failed to transition to 'Collateral Returned'");
		collateralBalance = (CollateralBalance) transitionIfPresent("Create and Book Post Collateral Transfers", CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		ValidationUtils.assertEquals("Collateral Posted", collateralBalance.getWorkflowState().getName(), "Failed to transition to 'Collateral Posted'");
		collateralBalance = (CollateralBalance) transitionIfPresent("Instructions Sent", CollateralBalance.COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		ValidationUtils.assertEquals("Complete", collateralBalance.getWorkflowState().getName(), "Failed to transition to 'Complete'");
		validateCollateralBalance(collateralBalance);
		return collateralBalance;
	}


	private void validateCollateralBalance(CollateralBalance collateralBalance) {
		String date = DateUtils.fromDate(collateralBalance.getBalanceDate(), DateUtils.DATE_FORMAT_INPUT);
		String[] expectedPledgedTransactions = EXPECTED_PLEDGED_TRANSACTIONS_MAP.get(date);
		CollateralBalancePledgedTransactionCommand command = new CollateralBalancePledgedTransactionCommand();
		command.setGeneratePledgedCollateral(false);
		command.setCollateralBalanceId(collateralBalance.getId());
		List<CollateralBalancePledgedTransaction> pledgedTransactionList = getCollateralBalancePledgedService().getCollateralBalancePledgedTransactionListByCommand(command);
		for (int i = 0; i < pledgedTransactionList.size(); i++) {
			CollateralBalancePledgedTransaction pledgedTransaction = pledgedTransactionList.get(i);
			Assertions.assertEquals(expectedPledgedTransactions[i], TestDataUtils.toStringFormatted(pledgedTransaction, EXPECTED_PLEDGED_TRANSACTIONS_FIELD_PATHS));
		}
		validatePositionTransfers(collateralBalance);
	}


	private void validatePositionTransfers(CollateralBalance collateralBalance) {
		String date = DateUtils.fromDate(collateralBalance.getBalanceDate(), DateUtils.DATE_FORMAT_INPUT);
		//Find and validate booked position transfers
		AccountingPositionTransferSearchForm positionTransferSearchForm = new AccountingPositionTransferSearchForm();
		positionTransferSearchForm.setJournalBooked(true);
		positionTransferSearchForm.setCollateralTransfer(true);
		positionTransferSearchForm.setTransactionDate(collateralBalance.getBalanceDate());
		positionTransferSearchForm.setSettlementDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(collateralBalance.getBalanceDate()), collateralBalance.getCollateralType().getTransferDateOffset()));
		positionTransferSearchForm.setFromClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());
		positionTransferSearchForm.setToClientInvestmentAccountId(collateralBalance.getClientInvestmentAccount().getId());

		List<AccountingPositionTransfer> positionTransferList = getAccountingPositionTransferService().getAccountingPositionTransferList(positionTransferSearchForm);
		Assertions.assertEquals((int) EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP.get(date), CollectionUtils.getSize(positionTransferList), "Expected " + EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP.get(date) + " transfer(s) for balance date " + date + " but found " + CollectionUtils.getSize(positionTransferList));
	}


	private CollateralBalance getCollateralBalance(String date) {
		Date balanceDate = DateUtils.toDate(date);
		//Rebuild collateral balance
		CollateralBalanceRebuildCommand rebuildCommand = CollateralBalanceRebuildCommand.ofSynchronousWithThrow();
		rebuildCommand.setBalanceDate(balanceDate);
		rebuildCommand.setCollateralType(getCollateralService().getCollateralTypeByName(CollateralType.COLLATERAL_TYPE_OPTIONS_ESCROW));
		getCollateralBalanceRebuildService().rebuildCollateralBalance(rebuildCommand);

		//Get CollateralBalance using date
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(balanceDate);
		CollateralBalance collateralBalance = CollectionUtils.getFirstElementStrict(getCollateralBalanceService().getCollateralBalanceList(searchForm));

		//Validate posted collateral
		Assertions.assertTrue(MathUtils.isEqual(EXPECTED_POSTED_COLLATERAL_MAP.get(date), collateralBalance.getPostedCollateralHaircutValue()), "Expected " + EXPECTED_POSTED_COLLATERAL_MAP.get(date) + " posted collateral for balance date " + date + " but found " + collateralBalance.getPostedCollateralHaircutValue());
		return collateralBalance;
	}


	private WorkflowAware transitionIfPresent(String transitionName, String tableName, Integer id) {
		List<WorkflowTransition> transitions = getWorkflowTransitionService().getWorkflowTransitionNextListByEntity(tableName, MathUtils.getNumberAsLong(id));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitions)) {
			if (transition.getName().equalsIgnoreCase(transitionName)) {
				return getWorkflowTransitionService().executeWorkflowTransitionByTransition(tableName, id, transition.getId());
			}
		}
		throw new ValidationException("Failed to transition to state: " + transitionName);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                      Test Data                      //////////////
	////////////////////////////////////////////////////////////////////////////////

	public static final List<String> COLLATERAL_BALANCE_DATES =
			Arrays.asList("08/07/2017", "08/08/2017", "08/09/2017", "08/10/2017");

	private static final String[] EXPECTED_PLEDGED_TRANSACTIONS_FIELD_PATHS = new String[]{"postedDate", "positionInvestmentSecurity.label", "postedCollateralInvestmentSecurity.label", "postedCollateralQuantity", "postedCollateralMarketValue"};

	private static final String[] EXPECTED_PLEDGED_TRANSACTIONS_08_07_17 = new String[]{
			"2017-08-07 00:00:00, SPXW US 08/09/17 C2480 [INACTIVE], VOO (Vanguard S&P 500 ETF), 6,811, 1,550,387.93",
			"2017-08-07 00:00:00, SPXW US 08/09/17 C2480 [INACTIVE], IVV (iShares Core S&P 500 ETF), 14,667, 3,659,709.84",
			"2017-08-07 00:00:00, SPXW US 09/05/17 C2510 [INACTIVE], IVV (iShares Core S&P 500 ETF), 19,886, 4,961,954.72",
			"2017-08-07 00:00:00, SPXW US 08/16/17 C2505 [INACTIVE], IVV (iShares Core S&P 500 ETF), 19,886, 4,961,954.72",
			"2017-08-07 00:00:00, SPXW US 08/18/17 C2505 [INACTIVE], IVV (iShares Core S&P 500 ETF), 19,886, 4,961,954.72",
			"2017-08-07 00:00:00, SPXW US 08/14/17 C2490 [INACTIVE], VOO (Vanguard S&P 500 ETF), 22,888, 5,209,995.44",
			"2017-08-07 00:00:00, SPXW US 08/30/17 C2505 [INACTIVE], VOO (Vanguard S&P 500 ETF), 22,888, 5,209,995.44",
			"2017-08-07 00:00:00, SPXW US 08/11/17 P2410 [INACTIVE], 912828D49 (T 0 7/8 08/15/17) [INACTIVE], 23,000, 23,001.796875",
			"2017-08-07 00:00:00, SPXW US 08/11/17 C2490 [INACTIVE], VOO (Vanguard S&P 500 ETF), 23,978, 5,458,112.14",
			"2017-08-07 00:00:00, SPXW US 08/25/17 C2500 [INACTIVE], VOO (Vanguard S&P 500 ETF), 25,068, 5,706,228.84",
			"2017-08-07 00:00:00, SPXW US 08/28/17 C2505 [INACTIVE], VOO (Vanguard S&P 500 ETF), 25,068, 5,706,228.84",
			"2017-08-07 00:00:00, SPXW US 08/21/17 C2500 [INACTIVE], VOO (Vanguard S&P 500 ETF), 25,068, 5,706,228.84",
			"2017-08-07 00:00:00, SPXW US 08/23/17 C2510 [INACTIVE], VOO (Vanguard S&P 500 ETF), 26,158, 5,954,345.54",
			"2017-08-07 00:00:00, SPXW US 09/01/17 C2505 [INACTIVE], VOO (Vanguard S&P 500 ETF), 26,158, 5,954,345.54",
			"2017-08-07 00:00:00, SPXW US 08/28/17 P2420 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 498,000, 496,891.171875",
			"2017-08-07 00:00:00, SPXW US 08/16/17 P2425 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 866,000, 864,071.796875",
			"2017-08-07 00:00:00, SPXW US 08/16/17 P2425 [INACTIVE], 912828UZ1 (T 0 5/8 04/30/18) [INACTIVE], 1,081,000, 1,076,523.984375",
			"2017-08-07 00:00:00, SPXW US 08/28/17 P2420 [INACTIVE], 912828TW0 (T 0 3/4 10/31/17) [INACTIVE], 1,199,000, 1,198,110.1171875",
			"2017-08-07 00:00:00, SPXW US 08/25/17 P2420 [INACTIVE], 912828D49 (T 0 7/8 08/15/17) [INACTIVE], 1,453,000, 1,453,113.515625",
			"2017-08-07 00:00:00, SPXW US 08/11/17 P2410 [INACTIVE], 912828TW0 (T 0 3/4 10/31/17) [INACTIVE], 1,666,000, 1,664,763.515625",
			"2017-08-07 00:00:00, SPXW US 08/09/17 P2390 [INACTIVE], 912828D49 (T 0 7/8 08/15/17) [INACTIVE], 1,674,000, 1,674,130.78125",
			"2017-08-07 00:00:00, SPXW US 08/14/17 P2410 [INACTIVE], 912828TW0 (T 0 3/4 10/31/17) [INACTIVE], 1,689,000, 1,687,746.4453125",
			"2017-08-07 00:00:00, SPXW US 08/18/17 P2420 [INACTIVE], 912828TW0 (T 0 3/4 10/31/17) [INACTIVE], 1,696,000, 1,694,741.25",
			"2017-08-07 00:00:00, SPXW US 09/01/17 P2420 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 1,699,000, 1,695,217.0703125",
			"2017-08-07 00:00:00, SPXW US 08/30/17 P2420 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 1,699,000, 1,695,217.0703125",
			"2017-08-07 00:00:00, SPXW US 09/05/17 P2425 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 1,702,000, 1,698,210.390625",
			"2017-08-07 00:00:00, SPXW US 08/23/17 P2430 [INACTIVE], 912828UR9 (T 0 3/4 02/28/18) [INACTIVE], 1,706,000, 1,702,201.484375",
			"2017-08-07 00:00:00, SPXW US 08/21/17 P2425 [INACTIVE], 912828UZ1 (T 0 5/8 04/30/18) [INACTIVE], 2,192,000, 2,182,923.75"
	};


	private static final String[] EXPECTED_PLEDGED_TRANSACTIONS_08_08_17 = new String[]{};
	private static final String[] EXPECTED_PLEDGED_TRANSACTIONS_08_09_17 = new String[]{
			"2017-08-09 00:00:00, SPXW US 08/09/17 P2390 [INACTIVE], 912828D49 (T 0 7/8 08/15/17) [INACTIVE], -1,674,000, -1,674,130.78125",
			"2017-08-09 00:00:00, SPXW US 08/09/17 C2480 [INACTIVE], IVV (iShares Core S&P 500 ETF), -14,667, -3,650,909.64",
			"2017-08-09 00:00:00, SPXW US 08/09/17 C2480 [INACTIVE], VOO (Vanguard S&P 500 ETF), -6,811, -1,546,437.55",
			"2017-08-09 00:00:00, SPXW US 09/06/17 C2505 [INACTIVE], VOO (Vanguard S&P 500 ETF), 6,803, 1,544,621.15",
			"2017-08-09 00:00:00, SPXW US 09/06/17 P2405 [INACTIVE], 912828UZ1 (T 0 5/8 04/30/18) [INACTIVE], 10,000, 9,959.375",
			"2017-08-09 00:00:00, SPXW US 09/06/17 C2505 [INACTIVE], IVV (iShares Core S&P 500 ETF), 14,667, 3,650,909.64",
			"2017-08-09 00:00:00, SPXW US 09/06/17 P2405 [INACTIVE], 912828D49 (T 0 7/8 08/15/17) [INACTIVE], 1,674,000, 1,674,130.78125"
	};
	private static final String[] EXPECTED_PLEDGED_TRANSACTIONS_08_10_17 = new String[]{};

	private static final Map<String, String[]> EXPECTED_PLEDGED_TRANSACTIONS_MAP;


	static {
		Map<String, String[]> map = new HashMap<>();
		map.put("08/07/2017", EXPECTED_PLEDGED_TRANSACTIONS_08_07_17);
		map.put("08/08/2017", EXPECTED_PLEDGED_TRANSACTIONS_08_08_17);
		map.put("08/09/2017", EXPECTED_PLEDGED_TRANSACTIONS_08_09_17);
		map.put("08/10/2017", EXPECTED_PLEDGED_TRANSACTIONS_08_10_17);
		EXPECTED_PLEDGED_TRANSACTIONS_MAP = Collections.unmodifiableMap(map);
	}


	private static final Map<String, BigDecimal> EXPECTED_POSTED_COLLATERAL_MAP;


	static {
		Map<String, BigDecimal> map = new HashMap<>();
		map.put("08/07/2017", new BigDecimal("0"));
		map.put("08/08/2017", new BigDecimal("85676077.96"));
		map.put("08/09/2017", new BigDecimal("85647043.80"));
		map.put("08/10/2017", new BigDecimal("84762784.86"));
		map.put("08/11/2017", new BigDecimal("84843687.21"));
		EXPECTED_POSTED_COLLATERAL_MAP = Collections.unmodifiableMap(map);
	}


	private static final Map<String, Integer> EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP;


	static {
		Map<String, Integer> map = new HashMap<>();
		map.put("08/07/2017", 6);
		map.put("08/08/2017", 0);
		map.put("08/09/2017", 7);
		map.put("08/10/2017", 0);
		EXPECTED_POSITION_TRANSFER_LIST_SIZE_MAP = Collections.unmodifiableMap(map);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<AccountingPositionTransfer> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingPositionTransfer> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CollateralBalanceRebuildService getCollateralBalanceRebuildService() {
		return this.collateralBalanceRebuildService;
	}


	public void setCollateralBalanceRebuildService(CollateralBalanceRebuildService collateralBalanceRebuildService) {
		this.collateralBalanceRebuildService = collateralBalanceRebuildService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CollateralBalancePledgedService getCollateralBalancePledgedService() {
		return this.collateralBalancePledgedService;
	}


	public void setCollateralBalancePledgedService(CollateralBalancePledgedService collateralBalancePledgedService) {
		this.collateralBalancePledgedService = collateralBalancePledgedService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
