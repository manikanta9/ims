package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyJoinedPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySpreadPosition;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentTypeSubTypeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;


/**
 * @author terrys
 */
public class CollateralOptionsStrategyShortPutSpreadCalculatorTest {

	private CollateralOptionsStrategyShortPutSpreadCalculator calculator = new CollateralOptionsStrategyShortPutSpreadCalculator();
	private CollateralBalanceOptionsRebuildProcessor rebuildProcessor = new CollateralBalanceOptionsRebuildProcessor();
	private CollateralOptionsStrategyCommand command = new CollateralOptionsStrategyCommand();

	private Date positionDate = DateUtils.toDate("03/21/2019");
	private InvestmentInstrument investmentInstrument;


	@BeforeEach
	public void before() {
		this.rebuildProcessor.setCallRequirementType(CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN);
		this.rebuildProcessor.setPutRequirementType(CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN);
		this.rebuildProcessor.setIndexMarginRequirementPercentage(new BigDecimal(".15"));
		this.command.setCollateralBalanceRebuildProcessor(this.rebuildProcessor);
		this.investmentInstrument = InvestmentInstrumentBuilder
				.createEmptyInvestmentInstrument()
				.withHierarchy(
						InvestmentInstrumentHierarchyBuilder
								.createEmptyInvestmentInstrumentHierarchy()
								.withInvestmentTypeSubType(
										InvestmentTypeSubTypeBuilder.createIndices()
												.withName("Options On Indices")
												.toInvestmentTypeSubType()
								)
								.toInvestmentInstrumentHierarchy()
				)
				.withPriceMultiplier(new BigDecimal("100"))
				.toInvestmentInstrument();
	}


	@Test
	public void requiredCollateralIndexTest() {
		BigDecimal underlyingPrice = new BigDecimal("128.83");
		BigDecimal underlyingValue = underlyingPrice.multiply(new BigDecimal("100")).multiply(new BigDecimal("26"));
		CollateralOptionsStrategyJoinedPosition longJoinedPosition = new CollateralOptionsStrategyJoinedPosition(
				Arrays.asList(
						new CollateralOptionsStrategyPosition(
								this.positionDate,
								InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
										.withId(1)
										.withInstrument(this.investmentInstrument)
										.withName("P1475")
										.withOptionType(InvestmentSecurityOptionTypes.PUT)
										.withOptionStrikePrice(new BigDecimal("1475"))
										.build(),
								new BigDecimal("100"),
								new BigDecimal("6"),
								"buy",
								new BigDecimal("1475").multiply(new BigDecimal("100")).multiply(new BigDecimal("6")),
								underlyingPrice,
								underlyingValue,
								new BigDecimal(".58"),
								new BigDecimal(".58").multiply(new BigDecimal("100")).multiply(new BigDecimal("6")),
								0L),
						new CollateralOptionsStrategyPosition(
								this.positionDate,
								InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
										.withId(1)
										.withInstrument(this.investmentInstrument)
										.withName("P1475")
										.withOptionType(InvestmentSecurityOptionTypes.PUT)
										.withOptionStrikePrice(new BigDecimal("1475"))
										.build(),
								new BigDecimal("100"),
								new BigDecimal("20"),
								"buy",
								new BigDecimal("1475").multiply(new BigDecimal("100")).multiply(new BigDecimal("20")),
								underlyingPrice,
								underlyingValue,
								new BigDecimal(".58"),
								new BigDecimal(".58").multiply(new BigDecimal("100")).multiply(new BigDecimal("20")),
								1L
						)
				),
				new BigDecimal("26")
		);
		CollateralOptionsStrategyPosition shortStrategyPosition = new CollateralOptionsStrategyPosition(
				this.positionDate,
				InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
						.withId(1)
						.withInstrument(this.investmentInstrument)
						.withName("P1770")
						.withOptionType(InvestmentSecurityOptionTypes.PUT)
						.withOptionStrikePrice(new BigDecimal("1770"))
						.build(),
				new BigDecimal("100"),
				new BigDecimal("-26"),
				"sell",
				new BigDecimal("1770").multiply(new BigDecimal("100")).multiply(new BigDecimal("26")),
				underlyingPrice,
				underlyingValue,
				new BigDecimal("7.61"),
				new BigDecimal("7.61").multiply(new BigDecimal("100")).multiply(new BigDecimal("26")),
				2L
		);
		CollateralOptionsStrategySpreadPosition strategySpreadPosition = new CollateralOptionsStrategySpreadPosition(longJoinedPosition, shortStrategyPosition);
		this.calculator.populateCollateral(this.rebuildProcessor, strategySpreadPosition, false);
		Assertions.assertEquals(0, new BigDecimal("768508").compareTo(strategySpreadPosition.getRequiredCollateral()));
	}
}
