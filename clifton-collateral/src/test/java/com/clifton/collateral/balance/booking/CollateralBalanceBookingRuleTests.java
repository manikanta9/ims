package com.clifton.collateral.balance.booking;


import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@Transactional
public class CollateralBalanceBookingRuleTests extends BaseInMemoryDatabaseTests {

	@Resource
	private CollateralBalanceService collateralBalanceService;

	@Resource
	private CollateralBalanceBookingRule collateralBalanceBookingRule;

	@Resource
	private CollateralBalanceBookingProcessor collateralBalanceBookingProcessor;


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testCollateralBalanceBookAndPost() {
		AccountingJournal journal = new AccountingJournal();
		CollateralBalance collateralBalance = getCollateralBalanceService().getCollateralBalance(967943);
		collateralBalance.setTransferAmount(new BigDecimal(50000));
		BookingSession<CollateralBalance> bookingSession = getCollateralBalanceBookingProcessor().newBookingSession(journal, collateralBalance);
		getCollateralBalanceBookingRule().setCalendarBusinessDayService(newCalendarBusinessDayService());
		getCollateralBalanceBookingRule().applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(2, journalDetailList.size());
		Assertions.assertEquals("-10	null	051200	50001255	Cash Collateral	GBP			-50,000	06/13/2017	1	-50,000	0	06/13/2017	06/13/2017	Cash Collateral Transfer", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	051200	522452	Cash	GBP			50,000	06/13/2017	1	50,000	0	06/13/2017	06/13/2017	Cash Collateral Transfer", journalDetailList.get(1).toStringFormatted(false));
	}


	public static CalendarBusinessDayService newCalendarBusinessDayService() {
		CalendarBusinessDayService result = Mockito.mock(CalendarBusinessDayService.class);

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.getDaysDifference((Date) args[1], (Date) args[0]);
		}).when(result).getBusinessDaysBetween(ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), (Integer) args[2]);
		}).when(result).getBusinessDayFrom(ArgumentMatchers.any(CalendarBusinessDayCommand.class), ArgumentMatchers.anyInt());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), (Integer) args[1]);
		}).when(result).getBusinessDayFrom(ArgumentMatchers.any(CalendarBusinessDayCommand.class), ArgumentMatchers.anyInt());

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return DateUtils.addDays(((CalendarBusinessDayCommand) args[0]).getDate(), -1);
		}).when(result).getPreviousBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class));

		Mockito.when(result.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenReturn(true);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CollateralBalanceBookingRule getCollateralBalanceBookingRule() {
		return this.collateralBalanceBookingRule;
	}


	public void setCollateralBalanceBookingRule(CollateralBalanceBookingRule collateralBalanceBookingRule) {
		this.collateralBalanceBookingRule = collateralBalanceBookingRule;
	}


	public CollateralBalanceBookingProcessor getCollateralBalanceBookingProcessor() {
		return this.collateralBalanceBookingProcessor;
	}


	public void setCollateralBalanceBookingProcessor(CollateralBalanceBookingProcessor collateralBalanceBookingProcessor) {
		this.collateralBalanceBookingProcessor = collateralBalanceBookingProcessor;
	}
}
