package com.clifton.collateral.balance.rebuild.processor.matcher;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommandHandler;
import com.clifton.collateral.balance.rebuild.processor.matcher.options.CollateralStrategyStrangleMatcher;
import com.clifton.collateral.balance.rebuild.processor.position.BaseCollateralStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyJoinedPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyStranglePosition;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CollateralStrategyStrangleMatcherTests extends BaseCollateralStrategyMatcherTests {

	@Resource
	private AccountingPositionDailyService accountingPositionDailyService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private CollateralOptionsStrategyCommandHandler collateralOptionsStrategyCommandHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testStrangleMatches() {
		Date positionDate = DateUtils.toDate("03/21/2019");

		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setNumber("237-70639-2-0");
		List<InvestmentAccount> holdingAccountList = this.investmentAccountService.getInvestmentAccountList(searchForm);
		Assertions.assertEquals(1, holdingAccountList.size());

		AccountingPositionDailySearchForm positionDailySearchForm = new AccountingPositionDailySearchForm();
		positionDailySearchForm.setHoldingInvestmentAccountId(holdingAccountList.get(0).getId());
		positionDailySearchForm.setPositionDate(positionDate);
		positionDailySearchForm.setCollateralAccountingAccount(false);
		List<AccountingPositionDaily> positionDailyList = this.accountingPositionDailyService.getAccountingPositionDailyList(positionDailySearchForm);

		Assertions.assertFalse(positionDailyList.isEmpty());

		List<CollateralOptionsStrategyPosition> collateralStrategyPositionList = positionDailyList.stream()
				.map(p -> this.collateralOptionsStrategyCommandHandler.createCollateralOptionsStrategyPosition(p, positionDate))
				.collect(Collectors.toList());

		Map<InvestmentInstrument, Totals> beforeTotals = collateralStrategyPositionList.stream()
				.map(Totals::new)
				.collect(Collectors.toMap(Totals::getInvestmentInstrument, Function.identity(), Totals::new));

		List<CollateralOptionsStrategyPosition> stranglePositions = CollateralStrategyStrangleMatcher
				.getCollateralStrategyStranglePositionList(collateralStrategyPositionList, positionDate).values().stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
		Assertions.assertFalse(stranglePositions.isEmpty());

		Map<InvestmentInstrument, Totals> afterTotals = Stream.concat(stranglePositions.stream()
						.map(CollateralOptionsStrategyStranglePosition.class::cast)
						.flatMap(s -> Stream.of(s.getCallStrategyPosition(), s.getPutStrategyPosition())),
				collateralStrategyPositionList.stream())
				.map(Totals::new)
				.collect(Collectors.toMap(Totals::getInvestmentInstrument, Function.identity(), Totals::new)
				);

		Assertions.assertTrue(beforeTotals.keySet().containsAll(afterTotals.keySet()));
		Assertions.assertTrue(afterTotals.keySet().containsAll(beforeTotals.keySet()));

		// make sure quantities before / after do not change
		Map<InvestmentInstrument, List<Totals>> differences = new HashMap<>();
		for (Map.Entry<InvestmentInstrument, Totals> entry : beforeTotals.entrySet()) {
			Totals before = entry.getValue();
			Totals after = afterTotals.get(entry.getKey());
			if (!Objects.equals(before, after)) {
				List<Totals> totals = differences.computeIfAbsent(entry.getKey(), k -> new ArrayList<>());
				totals.add(before);
				totals.add(after);
			}
		}
		Assertions.assertTrue(differences.isEmpty(), differences.toString());

		// assertions on strangle requirements
		for (CollateralOptionsStrategyPosition position : stranglePositions) {
			Assertions.assertTrue(position instanceof CollateralOptionsStrategyStranglePosition);
			CollateralOptionsStrategyStranglePosition stranglePosition = (CollateralOptionsStrategyStranglePosition) position;

			CollateralOptionsStrategyPosition putPosition = stranglePosition.getPutStrategyPosition();
			Assertions.assertEquals(putPosition.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName(), InvestmentType.OPTIONS, "Must be an Option");
			Assertions.assertEquals(putPosition.getOptionType(), InvestmentSecurityOptionTypes.PUT, "One side of strangle is a PUT and the other a CALL");

			CollateralOptionsStrategyPosition callPosition = stranglePosition.getCallStrategyPosition();
			Assertions.assertEquals(callPosition.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName(), InvestmentType.OPTIONS, "Must be an Option");
			Assertions.assertEquals(callPosition.getOptionType(), InvestmentSecurityOptionTypes.CALL, "One side of strangle is a PUT and the other a CALL");

			Assertions.assertEquals(putPosition.getInvestmentSecurity().getInstrument(), callPosition.getInvestmentSecurity().getInstrument(), "must be same instrument");
			Assertions.assertEquals(putPosition.getInvestmentSecurity().getEndDate(), callPosition.getInvestmentSecurity().getEndDate(), "must be same expiry");
			Assertions.assertEquals(0, putPosition.getRemainingQuantity().compareTo(callPosition.getRemainingQuantity()), "must be same quantities");
		}

		// look for a specific example
		List<CollateralOptionsStrategyPosition> c2865 = collateralStrategyPositionList.stream()
				.filter(s -> "SPXW US 03/29/19 C2865".equals(s.getInvestmentSecurity().getSymbol()))
				.collect(Collectors.toList());
		Assertions.assertEquals(1, c2865.size());
		Assertions.assertEquals(0, new BigDecimal("-12").compareTo(c2865.get(0).getRemainingQuantity()));
		List<CollateralOptionsStrategyStranglePosition> strangleExample = stranglePositions.stream()
				.filter(s -> "SPXW US 03/29/19 C2865".equals(s.getInvestmentSecurity().getSymbol()) || "SPXW US 03/29/19 P2715".equals(s.getInvestmentSecurity().getSymbol()))
				.filter(s -> s instanceof CollateralOptionsStrategyStranglePosition)
				.map(CollateralOptionsStrategyStranglePosition.class::cast)
				.collect(Collectors.toList());
		Assertions.assertEquals(1, strangleExample.size());
		CollateralOptionsStrategyStranglePosition stranglePosition = strangleExample.get(0);
		Assertions.assertEquals(0, new BigDecimal("-125").compareTo(stranglePosition.getRemainingQuantity()));

		Assertions.assertEquals("SPXW US 03/29/19 P2715", stranglePosition.getPutStrategyPosition().getInvestmentSecurity().getSymbol());
		Assertions.assertEquals(0, new BigDecimal("-125").compareTo(stranglePosition.getPutStrategyPosition().getRemainingQuantity()));

		Assertions.assertEquals("SPXW US 03/29/19 C2865", stranglePosition.getCallStrategyPosition().getInvestmentSecurity().getSymbol());
		Assertions.assertEquals(0, new BigDecimal("-125").compareTo(stranglePosition.getCallStrategyPosition().getRemainingQuantity()));

		CollateralOptionsStrategyJoinedPosition callStrategyPosition = (CollateralOptionsStrategyJoinedPosition) stranglePosition.getCallStrategyPosition();
		Assertions.assertEquals(2, callStrategyPosition.getCompositeStrategyPositionList().size());
		List<BigDecimal> quantities = callStrategyPosition.getCompositeStrategyPositionList()
				.stream()
				.map(BaseCollateralStrategyPosition::getRemainingQuantity)
				.filter(q -> q.compareTo(new BigDecimal("-117")) == 0)
				.filter(q -> q.compareTo(new BigDecimal("-8")) == 0)
				.collect(Collectors.toList());
		Assertions.assertTrue(quantities.isEmpty());
	}

	////////////////////////////////////////////////////////////////////////////////

	private static class Totals {

		private final InvestmentInstrument investmentInstrument;
		private final BigDecimal strikeValue;
		private final BigDecimal underlyingValue;
		private final BigDecimal remainingQuantity;
		private final BigDecimal optionProceeds;


		public Totals(InvestmentInstrument investmentInstrument, BigDecimal strikeValue, BigDecimal underlyingValue, BigDecimal remainingQuantity, BigDecimal optionProceeds) {
			this.investmentInstrument = investmentInstrument;
			this.strikeValue = strikeValue;
			this.underlyingValue = underlyingValue;
			this.remainingQuantity = remainingQuantity;
			this.optionProceeds = optionProceeds;
		}


		public Totals(CollateralOptionsStrategyPosition position) {
			this(position.getInvestmentSecurity().getInstrument(), position.getStrikeValue(), position.getUnderlyingValue(), position.getRemainingQuantity(), position.getOptionProceeds());
		}


		public Totals(Totals a, Totals b) {
			this.investmentInstrument = a.investmentInstrument;
			this.strikeValue = a.strikeValue.add(b.strikeValue);
			this.underlyingValue = a.underlyingValue.add(b.underlyingValue);
			this.remainingQuantity = a.remainingQuantity.add(b.remainingQuantity);
			this.optionProceeds = a.optionProceeds.add(b.optionProceeds);
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			Totals totals = (Totals) o;
			return Objects.equals(this.investmentInstrument, totals.investmentInstrument) &&
					MathUtils.isEqual(this.strikeValue, totals.strikeValue) &&
					MathUtils.isEqual(this.underlyingValue, totals.underlyingValue) &&
					MathUtils.isEqual(this.remainingQuantity, totals.remainingQuantity) &&
					MathUtils.isEqual(this.optionProceeds, totals.optionProceeds);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.investmentInstrument, this.strikeValue, this.underlyingValue, this.remainingQuantity, this.optionProceeds);
		}


		@Override
		public String toString() {
			return "{" +
					"investmentInstrument=" + this.investmentInstrument +
					", strikeValue=" + this.strikeValue +
					", underlyingValue=" + this.underlyingValue +
					", remainingQuantity=" + this.remainingQuantity +
					", optionProceeds=" + this.optionProceeds +
					'}';
		}


		public InvestmentInstrument getInvestmentInstrument() {
			return this.investmentInstrument;
		}
	}
}
