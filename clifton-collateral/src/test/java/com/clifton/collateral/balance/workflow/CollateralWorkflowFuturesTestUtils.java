package com.clifton.collateral.balance.workflow;

import com.clifton.accounting.gl.position.closing.AccountingPositionClosing;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingCommand;
import com.clifton.accounting.gl.position.closing.AccountingPositionClosingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.balance.workflow.futures.CollateralBalancePledgedTransactionsFuturesWorkflowAction;
import com.clifton.collateral.balance.workflow.futures.CollateralBalancePledgedTransactionsFuturesWorkflowContext;
import com.clifton.collateral.balance.workflow.futures.FuturesCollateralPositionPledge;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author jonathanr
 */
public class CollateralWorkflowFuturesTestUtils {


	////////////////////////////////////////////////////////////////////////////
	//////////                  Mock Builder Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public static CollateralBalancePledgedTransactionsFuturesWorkflowContext buildDefaultSpyContext(Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld, Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld, BigDecimal requiredCollateralAmount, BigDecimal bufferPercent) {
		InvestmentAccount clientAccount = Mockito.mock(InvestmentAccount.class);
		Mockito.when(clientAccount.getId()).thenReturn(1);
		InvestmentAccount brokerAccount = Mockito.mock(InvestmentAccount.class);
		Mockito.when(brokerAccount.getId()).thenReturn(100);
		InvestmentAccount collateralAccount = Mockito.mock(InvestmentAccount.class);
		Mockito.when(collateralAccount.getId()).thenReturn(200);
		return SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder.aCollateralBalancePledgedTransactionsFuturesWorkflowContext()
				.withClientInvestmentAccount(clientAccount)
				.withCustodianInvestmentAccount(collateralAccount)
				.withBrokerInvestmentAccount(brokerAccount)
				.withBrokerHeldCollateral(brokerHeld)
				.withCustodianHeldCollateral(custodianHeld)
				.withMinimumCashTransferAmount(new BigDecimal(10_000))
				.withCashTransferIncrementAmount(new BigDecimal(1_000))
				.withMinimumPositionTransferAmount(new BigDecimal(20_000))
				.withMinimumTransferQuantity(new BigDecimal(1_000))
				.withRequiredCollateralAmount(requiredCollateralAmount)
				.withTargetCollateralBufferPercent(bufferPercent)
				.build();
	}


	public static InvestmentInstrumentHierarchy mockInvestmentInstrumentHierarchy(boolean isCash) {
		InvestmentInstrumentHierarchy hierarchy = Mockito.mock(InvestmentInstrumentHierarchy.class);
		Mockito.when(hierarchy.isCurrency()).thenReturn(isCash);
		return hierarchy;
	}


	public static void mockWorkflowServices(CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction, CollateralBalancePledgedTransactionsFuturesWorkflowContext context, List<InvestmentInstrumentHierarchy> investmentInstrumentHierarchyList) {
		AccountingPositionClosingService accountingPositionClosingService = Mockito.mock(AccountingPositionClosingService.class);
		Mockito.when(accountingPositionClosingService.getAccountingPositionClosingLotMarketValue(Mockito.any())).thenAnswer(invocation -> {
			AccountingPositionClosingCommand cmd = invocation.getArgument(0);
			List<FuturesCollateralPositionPledge> pledgeList = new ArrayList<>(context.getBrokerHeldCollateral().values());
			pledgeList.addAll(context.getCustodianCollateral().values());
			for (FuturesCollateralPositionPledge pledge : pledgeList) {
				if (MathUtils.isEqual(pledge.getInvestmentSecurity().getId(), cmd.getInvestmentSecurityId())) {
					BigDecimal quantity = cmd.getSecurityQuantityList().get(0).getQuantity();
					return MathUtils.multiply(MathUtils.divide(pledge.getMarketValue(), pledge.getQuantity()), quantity);
				}
			}
			return null;
		});

		ArrayList<AccountingPositionClosing> AccountingPositionClosingList = new ArrayList<>();
		Mockito.when(accountingPositionClosingService.getAccountingPositionClosingLotList(Mockito.any())).thenReturn(AccountingPositionClosingList);
		CollateralHaircutDefinitionService collateralHaircutDefinitionService = Mockito.mock(CollateralHaircutDefinitionService.class);
		Mockito.when(collateralHaircutDefinitionService.getCollateralHaircutValue(Mockito.any(InvestmentAccount.class), Mockito.any(InvestmentSecurity.class), Mockito.eq(false), Mockito.any())).thenAnswer(invocation -> {
			InvestmentAccount account = invocation.getArgument(0);
			InvestmentSecurity investmentSecurity = invocation.getArgument(1);
			List<FuturesCollateralPositionPledge> pledgeList;
			if (MathUtils.isEqual(account.getId(), context.getCollateralBalance().getHoldingInvestmentAccount().getId())) {
				pledgeList = new ArrayList<>(context.getBrokerHeldCollateral().values());
			}
			else {
				pledgeList = new ArrayList<>(context.getCustodianCollateral().values());
			}

			for (FuturesCollateralPositionPledge pledge : pledgeList) {
				if (MathUtils.isEqual(pledge.getInvestmentSecurity().getId(), investmentSecurity.getId())) {
					return pledge.getHaircutValue();
				}
			}
			return null;
		});

		AccountingPositionTransferService accountingPositionTransferService = Mockito.mock(AccountingPositionTransferService.class);
		Mockito.when(accountingPositionTransferService.getAccountingPositionTransferTypeByName(Mockito.anyString())).thenReturn(Mockito.mock(AccountingPositionTransferType.class));

		SystemSchemaService systemSchemaService = Mockito.mock(SystemSchemaService.class);
		Mockito.when(systemSchemaService.getSystemTableByName("CollateralBalance")).thenReturn(Mockito.mock(SystemTable.class));
		CalendarBusinessDayService calendarBusinessDayService = Mockito.mock(CalendarBusinessDayService.class);
		Mockito.when(calendarBusinessDayService.getBusinessDayFrom(Mockito.any(), Mockito.anyInt())).thenReturn(new Date());
		InvestmentSetupService investmentSetupService = Mockito.mock(InvestmentSetupService.class);
		Mockito.when(investmentSetupService.getInvestmentInstrumentHierarchyListByIds(Mockito.any())).thenReturn(investmentInstrumentHierarchyList);

		workflowAction.setAccountingPositionClosingService(accountingPositionClosingService);
		workflowAction.setAccountingPositionTransferService(accountingPositionTransferService);
		workflowAction.setCollateralHaircutDefinitionService(collateralHaircutDefinitionService);
		workflowAction.setSystemSchemaService(systemSchemaService);
		workflowAction.setCalendarBusinessDayService(calendarBusinessDayService);
		workflowAction.setInvestmentSetupService(investmentSetupService);
	}


	public static InvestmentSecurity mockInvestmentSecurity(int id, InvestmentInstrumentHierarchy parentHierarchy) {
		return mockInvestmentSecurity(id, parentHierarchy, DateUtils.addMonths(new Date(), 1));
	}


	public static InvestmentSecurity mockInvestmentSecurity(int id, InvestmentInstrumentHierarchy parentHierarchy, Date date) {
		InvestmentSecurity investmentSecurity = Mockito.mock(InvestmentSecurity.class);
		InvestmentInstrument investmentInstrument = Mockito.mock(InvestmentInstrument.class);
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = Mockito.mock(InvestmentInstrumentHierarchy.class);
		Mockito.when(investmentInstrumentHierarchy.getParent()).thenReturn(parentHierarchy);
		Mockito.when(investmentInstrumentHierarchy.isSelfOrAncestor(Mockito.notNull())).thenCallRealMethod();
		Mockito.when(investmentInstrument.getHierarchy()).thenReturn(investmentInstrumentHierarchy);
		Mockito.when(investmentSecurity.getInstrument()).thenReturn(investmentInstrument);
		Mockito.when(investmentSecurity.getId()).thenReturn(id);
		Mockito.when(investmentSecurity.getEndDate()).thenReturn(date);
		if (parentHierarchy != null && parentHierarchy.isCurrency()) {
			Mockito.when(investmentInstrumentHierarchy.isCurrency()).thenReturn(true);
			Mockito.when(investmentSecurity.isCurrency()).thenReturn(true);
		}
		else {
			Mockito.when(investmentSecurity.isCurrency()).thenReturn(false);
		}
		return investmentSecurity;
	}
}
