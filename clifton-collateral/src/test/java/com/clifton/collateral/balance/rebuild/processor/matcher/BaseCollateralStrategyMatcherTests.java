package com.clifton.collateral.balance.rebuild.processor.matcher;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;


@ContextConfiguration
@Transactional
public abstract class BaseCollateralStrategyMatcherTests extends BaseInMemoryDatabaseTests {

	@Override
	protected Class<?> getTestClass() {
		return BaseCollateralStrategyMatcherTests.class;
	}
}
