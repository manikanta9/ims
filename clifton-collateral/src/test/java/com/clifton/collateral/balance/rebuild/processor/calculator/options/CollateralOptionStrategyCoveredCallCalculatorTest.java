package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.command.options.CollateralOptionsStrategyCommand;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubTypeBuilder;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author terrys
 */
public class CollateralOptionStrategyCoveredCallCalculatorTest {

	private CollateralOptionStrategyCoveredCallCalculator calculator = new CollateralOptionStrategyCoveredCallCalculator();
	private CollateralBalanceOptionsRebuildProcessor rebuildProcessor = new CollateralBalanceOptionsRebuildProcessor();
	private CollateralOptionsStrategyCommand command = new CollateralOptionsStrategyCommand();


	@BeforeEach
	public void before() {
		this.rebuildProcessor.setCallRequirementType(CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN);
		this.rebuildProcessor.setIndexMarginRequirementPercentage(new BigDecimal(".15"));
		this.command.setCollateralBalanceRebuildProcessor(this.rebuildProcessor);
	}


	@Test
	public void requiredCollateralEquityLongTest() {
		List<CollateralOptionsStrategyPosition> positionList = new ArrayList<>();
		AccountingJournalDetailDefinition detailDefinition = new AccountingTransaction();
		detailDefinition.setInvestmentSecurity(InvestmentSecurityBuilder
				.ofType("SPXW US 08/21/17 C2500", InvestmentType.INDEX)
				.withInstrument(InvestmentInstrumentBuilder
						.createEmptyInvestmentInstrument()
						.withHierarchy(
								InvestmentInstrumentHierarchyBuilder
										.createEmptyInvestmentInstrumentHierarchy()
										.withInvestmentTypeSubType(
												InvestmentTypeSubTypeBuilder.createIndices()
														.withName("Options On Indices")
														.toInvestmentTypeSubType()
										)
										.toInvestmentInstrumentHierarchy()
						)
						.withPriceMultiplier(new BigDecimal("100"))
						.toInvestmentInstrument()
				)
				.build()
		);
		AccountingPosition accountingPosition = AccountingPosition.forOpeningTransaction(detailDefinition);
		AccountingPositionDaily positionInfo = AccountingPositionDaily.ofAccountingTransaction(accountingPosition);
		positionInfo.setRemainingQuantity(new BigDecimal("4"));
		CollateralOptionsStrategyPosition position = new CollateralOptionsStrategyPosition(positionInfo,
				BigDecimal.ZERO,
				new BigDecimal("2480.91"),
				new BigDecimal("992364"),
				new BigDecimal("7"),
				new BigDecimal("2800"),
				0L
		);
		positionList.add(position);
		this.command.setPositionList(positionList);
		Map<Long, BigDecimal> results = this.calculator.calculateCollateral(this.command);
		Assertions.assertFalse(results.isEmpty());
		Assertions.assertEquals(1, results.entrySet().size());
		Assertions.assertEquals(0, BigDecimal.ZERO.compareTo(results.values().iterator().next()), "0 versus " + results.values().iterator().next().toPlainString());
	}


	@Test
	public void requiredCollateralEquityShortTest() {
		List<CollateralOptionsStrategyPosition> positionList = new ArrayList<>();
		AccountingJournalDetailDefinition detailDefinition = new AccountingTransaction();
		detailDefinition.setInvestmentSecurity(InvestmentSecurityBuilder
				.ofType("SPXW US 08/21/17 C2500", InvestmentType.INDEX)
				.withInstrument(InvestmentInstrumentBuilder
						.createEmptyInvestmentInstrument()
						.withHierarchy(
								InvestmentInstrumentHierarchyBuilder
										.createEmptyInvestmentInstrumentHierarchy()
										.withInvestmentTypeSubType(
												InvestmentTypeSubTypeBuilder.createIndices()
														.withName("Options On Indices")
														.toInvestmentTypeSubType()
										)
										.toInvestmentInstrumentHierarchy()
						)
						.withPriceMultiplier(new BigDecimal("100"))
						.toInvestmentInstrument()
				)
				.build()
		);
		AccountingPosition accountingPosition = AccountingPosition.forOpeningTransaction(detailDefinition);
		AccountingPositionDaily positionInfo = AccountingPositionDaily.ofAccountingTransaction(accountingPosition);
		positionInfo.setRemainingQuantity(new BigDecimal("-4"));
		CollateralOptionsStrategyPosition position = new CollateralOptionsStrategyPosition(positionInfo,
				BigDecimal.ZERO,
				new BigDecimal("2480.91"),
				new BigDecimal("992364"),
				new BigDecimal("7"),
				new BigDecimal("2800"),
				0L
		);
		positionList.add(position);
		this.command.setPositionList(positionList);
		Map<Long, BigDecimal> results = this.calculator.calculateCollateral(this.command);
		Assertions.assertFalse(results.isEmpty());
		Assertions.assertEquals(1, results.entrySet().size());
		BigDecimal expected = MathUtils.multiply(new BigDecimal("992364"), new BigDecimal(".50")).subtract(new BigDecimal("2800"));
		Assertions.assertEquals(0, expected.compareTo(results.values().iterator().next()), expected.toPlainString() + " versus " + results.values().iterator().next().toPlainString());
	}
}
