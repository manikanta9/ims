package com.clifton.collateral.balance;

import com.clifton.collateral.CollateralTypeBuilder;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class OptionsMarginCollateralBalanceTests {

	@Test
	public void targetMovementNeedMore() {
		OptionsMarginCollateralBalance balance = (OptionsMarginCollateralBalance) CollateralBalanceBuilder.aCollateralBalance(
				CollateralTypeBuilder.aCollateralType()
						.withCategory(CollateralBalanceCategories.OPTIONS_MARGIN)
						.build())
				.withPositionsMarketValue(new BigDecimal("9000"))
				.withCollateralRequirement(new BigDecimal("10704"))
				.withPostedCollateralValue(new BigDecimal("0"))
				.withPostedCollateralHaircutValue(new BigDecimal("0"))
				.withClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount())
				.withHoldingInvestmentAccount((InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount()))
				.build();
		Assertions.assertEquals(0, new BigDecimal("10704").compareTo(balance.getOptionsCollateralRequirement()));
		Assertions.assertEquals(0, new BigDecimal("9000").compareTo(balance.getOptionsPositionsMarketValue()));
		BigDecimal targetMovement = balance.getOptionsTargetMovement();
		Assertions.assertTrue(MathUtils.isPositive(targetMovement));

		BigDecimal overPledged = MathUtils.multiply(new BigDecimal(".075"), balance.getOptionsCollateralRequirement(), 2).add(balance.getOptionsCollateralRequirement())
				.add(MathUtils.subtract(balance.getOptionsCollateralRequirement(), balance.getOptionsPositionsMarketValue()));
		Assertions.assertEquals(0, overPledged.compareTo(targetMovement.add(balance.getOptionsCollateralRequirement())));
	}


	@Test
	public void targetMovementNoRequirement() {
		OptionsMarginCollateralBalance balance = (OptionsMarginCollateralBalance) CollateralBalanceBuilder.aCollateralBalance(
				CollateralTypeBuilder.aCollateralType()
						.withCategory(CollateralBalanceCategories.OPTIONS_MARGIN)
						.build())
				.withPositionsMarketValue(new BigDecimal("100000"))
				.withCollateralRequirement(new BigDecimal("0"))
				.withPostedCollateralValue(new BigDecimal("0"))
				.withPostedCollateralHaircutValue(new BigDecimal("0"))
				.withHoldingInvestmentAccount((InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount()))
				.build();
		Assertions.assertEquals(0, new BigDecimal("0").compareTo(balance.getOptionsCollateralRequirement()));
		Assertions.assertEquals(0, new BigDecimal("100000").compareTo(balance.getOptionsPositionsMarketValue()));
		BigDecimal targetMovement = balance.getOptionsTargetMovement();
		Assertions.assertTrue(MathUtils.isNullOrZero(targetMovement));
		// over pledged with no requirement, return all posted collateral

		Assertions.assertEquals(0, targetMovement.compareTo(balance.getPostedCollateralHaircutValue().negate()));
	}


	@Test
	public void targetMovementOverPledgedOverThreshold() {
		OptionsMarginCollateralBalance balance = (OptionsMarginCollateralBalance) CollateralBalanceBuilder.aCollateralBalance(
				CollateralTypeBuilder.aCollateralType()
						.withCategory(CollateralBalanceCategories.OPTIONS_MARGIN)
						.build())
				.withPositionsMarketValue(new BigDecimal("900000"))
				.withCollateralRequirement(new BigDecimal("200000"))
				.withPostedCollateralValue(new BigDecimal("100000"))
				.withPostedCollateralHaircutValue(new BigDecimal("10000"))
				.withClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount())
				.withHoldingInvestmentAccount((InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount()))
				.build();
		Assertions.assertEquals(0, new BigDecimal("290000").compareTo(balance.getOptionsCollateralRequirement()));
		Assertions.assertEquals(0, new BigDecimal("610000").compareTo(balance.getExcessOptionsEquityCollateral()));
		BigDecimal targetMovement = balance.getOptionsTargetMovement();
		Assertions.assertTrue(MathUtils.isNegative(targetMovement));
		// over pledged
		Assertions.assertEquals(1, balance.getExcessOptionsEquityCollateral().compareTo(balance.getOptionsCollateralRequirement().multiply(new BigDecimal(".15"))));
		Assertions.assertEquals(0, targetMovement.add(balance.getOptionsPositionsMarketValue()).compareTo(
				balance.getOptionsCollateralRequirement().multiply(new BigDecimal(".075")).add(balance.getOptionsCollateralRequirement())));
	}


	@Test
	public void targetMovementOverPledgedUnderThreshold() {
		OptionsMarginCollateralBalance balance = (OptionsMarginCollateralBalance) CollateralBalanceBuilder.aCollateralBalance(
				CollateralTypeBuilder.aCollateralType()
						.withCategory(CollateralBalanceCategories.OPTIONS_MARGIN)
						.build())
				.withPositionsMarketValue(new BigDecimal("333500"))
				.withCollateralRequirement(new BigDecimal("200000"))
				.withPostedCollateralValue(new BigDecimal("100000"))
				.withPostedCollateralHaircutValue(new BigDecimal("10000"))
				.withHoldingInvestmentAccount((InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount()))
				.build();
		Assertions.assertEquals(0, new BigDecimal("290000").compareTo(balance.getOptionsCollateralRequirement()));
		Assertions.assertEquals(0, new BigDecimal("43500.00").compareTo(balance.getExcessOptionsEquityCollateral()));
		BigDecimal targetMovement = balance.getOptionsTargetMovement();
		Assertions.assertTrue(MathUtils.isNullOrZero(targetMovement));
		// over pledged
		Assertions.assertEquals(0, balance.getExcessOptionsEquityCollateral().compareTo(balance.getOptionsCollateralRequirement().multiply(new BigDecimal(".15"))));
		Assertions.assertEquals(0, targetMovement.compareTo(BigDecimal.ZERO));
	}
}
