package com.clifton.collateral.balance.workflow;

import com.clifton.collateral.balance.workflow.futures.CollateralBalanceFuturesCollateralWorkflowUtils;
import com.clifton.collateral.balance.workflow.futures.CollateralBalancePledgedTransactionsFuturesWorkflowAction;
import com.clifton.collateral.balance.workflow.futures.CollateralBalancePledgedTransactionsFuturesWorkflowContext;
import com.clifton.collateral.balance.workflow.futures.FuturesCollateralPositionPledge;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author jonathanr
 */

public class CollateralBalancePledgedTransactionsFuturesWorkflowActionTests {


	////////////////////////////////////////////////////////////////////////////
	//////////                          TESTS                         //////////
	////////////////////////////////////////////////////////////////////////////
	@Test
	public void calculateQuantityTest() {
		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext context = Mockito.mock(CollateralBalancePledgedTransactionsFuturesWorkflowContext.class);
		Mockito.when(context.getMinimumTransferQuantity()).thenReturn(new BigDecimal(1_000));

		// = case
		BigDecimal computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("100000"), new BigDecimal("100000"), new BigDecimal("100000"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 100_000), "Transfer Quantity Expected value was 100,000 but found " + computedQuantity.toPlainString());

		// < case
		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("50000"), new BigDecimal("100000"), new BigDecimal("100000"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 50_000), "Transfer Quantity Expected value was 50,000 but found " + computedQuantity.toPlainString());

		// > case
		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("200000"), new BigDecimal("100000"), new BigDecimal("100000"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 200_000), "Transfer Quantity Expected value was 200,000 but found " + computedQuantity.toPlainString());

		// Test MinimumTransferAmount works. 11527 is prime number so must round
		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("11527"), new BigDecimal("11527"), new BigDecimal("11527"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 11_000), "Transfer Quantity Expected value was 11,000 but found " + computedQuantity.toPlainString());

		// Test MinimumTransferAmount post vs return works. 11527 is prime number so must round
		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("11527"), new BigDecimal("11527"), new BigDecimal("11527"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 11_000), "Transfer Quantity Expected value was 12,000 but found " + computedQuantity.toPlainString());

		// verify position value is affects quantity as expected

		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("100000"), new BigDecimal("100000"), new BigDecimal("200000"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 50_000), "Transfer Quantity Expected value was 50,000 but found " + computedQuantity.toPlainString());
		computedQuantity = ReflectionTestUtils.invokeMethod(workflowAction, "calculateTransferQuantity", new BigDecimal("100000"), new BigDecimal("100000"), new BigDecimal("50000"), context);
		Assertions.assertNotNull(computedQuantity, "the method 'calculateTransferQuantity()' was expected to return a value but returned null ");
		Assertions.assertTrue(MathUtils.isEqual(computedQuantity, 200_000), "Transfer Quantity Expected value was 200,000 but found " + computedQuantity.toPlainString());
	}


	@Test
	public void postPositionTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);
		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addCustodianPosition("Security1", 120_000, 120_000);
		testSetup.addCustodianPosition("Security2", 10_000, 10_000);
		testSetup.addCommonHierarchyAncestor("Security1", "Security2");
		testSetup.addCustodianPosition("Security3", 10_000, 10_000);
		testSetup.setUpHierarchyPriority("Security1", "Security3", FuturesCollateralTestSetupUtil.CASH_SECURITY_NAME);
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = testSetup.getBuiltCustodianMap();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = new LinkedHashMap<>();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test Return is empty
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralReturn)));

		//Test no cash posted
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)), "Expected Posted Cash to be 0 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)));

		//Test number of posted entities
		Assertions.assertNotNull(collateralPost.values());
		Assertions.assertEquals(1, collateralPost.values().size(), "Expected 1 position pledged but found " + collateralPost.values().size());

		FuturesCollateralPositionPledge security1Pledge = collateralPost.get(testSetup.getMockedSecurityByName("Security1"));
		//Test correct security posted
		Assertions.assertNotNull(security1Pledge, "Expected Security1 pledged");
		//Test correct quantity posted
		Assertions.assertTrue(MathUtils.isEqual(105_000, security1Pledge.getQuantity()), "Expected quantity for security 1 to be 105,000 but was :" + CoreMathUtils.formatNumberInteger(security1Pledge.getQuantity()));

		//Test total collateral value = required + buffer
		Assertions.assertTrue(MathUtils.isEqual(105_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)), "Expected total position collateral value of 105,000.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)));
	}


	@Test
	public void postMultiplePositionsTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);
		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addCustodianPosition("Security1", 200_000, 100_000, ".5");
		testSetup.addCustodianPosition("Security2", 60_000, 60_000);
		testSetup.addCommonHierarchyAncestor("Security1", "Security2");
		testSetup.addCustodianPosition("Security3", 10_000, 10_000);
		testSetup.setUpHierarchyPriority("Security1", "Security3", FuturesCollateralTestSetupUtil.CASH_SECURITY_NAME);
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = testSetup.getBuiltCustodianMap();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = new LinkedHashMap<>();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test Return is empty
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralReturn)));

		//Test no cash posted
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)), "Expected Posted Cash to be 0 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)));

		//Test number of posted entities
		Assertions.assertNotNull(collateralPost.values());
		Assertions.assertEquals(2, collateralPost.values().size(), "Expected 2 position pledged but found " + collateralPost.values().size());
		FuturesCollateralPositionPledge security1 = collateralPost.get(testSetup.getMockedSecurityByName("Security1"));
		FuturesCollateralPositionPledge security2 = collateralPost.get(testSetup.getMockedSecurityByName("Security2"));

		//Test correct securities posted
		Assertions.assertNotNull(security1, "Expected security 1 pledged");
		Assertions.assertNotNull(security2, "Expected security 2 pledged");
		//Test correct quantities posted
		Assertions.assertTrue(MathUtils.isEqual(180_000, security1.getQuantity()), "Expected quantity for security 1 to be 180,000 but was :" + CoreMathUtils.formatNumberInteger(security1.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(60_000, security2.getQuantity()), "Expected quantity for security 2 to be 60,000 but was :" + CoreMathUtils.formatNumberInteger(security2.getQuantity()));

		//Test total collateral value = required + buffer
		Assertions.assertTrue(MathUtils.isEqual(105_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)), "Expected total position collateral value of 105,000.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)));
	}


	@Test
	public void postMultiplePositionsAndCashTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);
		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addCustodianPosition("Security1", 100_000, 10_000, ".5");
		testSetup.addCustodianPosition("Security2", 60_000, 60_000);
		testSetup.addCommonHierarchyAncestor("Security1", "Security2");
		testSetup.addCustodianPosition("Security3", 10_000, 10_000);
		testSetup.addCustodianHeldCash(80_000);
		testSetup.setUpHierarchyPriority("Security1", "Security3", FuturesCollateralTestSetupUtil.CASH_SECURITY_NAME);
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = testSetup.getBuiltCustodianMap();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = new LinkedHashMap<>();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test Return is empty
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralReturn)));

		//Test cash posted
		Assertions.assertTrue(MathUtils.isEqual(30_000, CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)), "Expected Cash to be $30,000 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)));

		//Test number of posted entities
		Assertions.assertNotNull(collateralPost.values());
		Assertions.assertEquals(4, collateralPost.values().size(), "Expected 4 position pledged but found " + collateralPost.values().size());

		FuturesCollateralPositionPledge security1 = collateralPost.get(testSetup.getMockedSecurityByName("Security1"));
		FuturesCollateralPositionPledge security2 = collateralPost.get(testSetup.getMockedSecurityByName("Security2"));
		FuturesCollateralPositionPledge security3 = collateralPost.get(testSetup.getMockedSecurityByName("Security3"));

		//Test correct securities posted
		Assertions.assertNotNull(security1, "Expected security 1 pledged");
		Assertions.assertNotNull(security2, "Expected security 2 pledged");
		Assertions.assertNotNull(security3, "Expected security 3 pledged");
		//Test correct quantities posted
		Assertions.assertTrue(MathUtils.isEqual(100_000, security1.getQuantity()), "Expected quantity for security 1 to be 100,000 but was :" + CoreMathUtils.formatNumberInteger(security1.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(60_000, security2.getQuantity()), "Expected quantity for security 2 to be 60,000 but was :" + CoreMathUtils.formatNumberInteger(security2.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(10_000, security3.getQuantity()), "Expected quantity for security 3 to be 10,000 but was :" + CoreMathUtils.formatNumberInteger(security3.getQuantity()));

		//Test total collateral value = required + buffer
		Assertions.assertTrue(MathUtils.isEqual(105_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)), "Expected total position collateral value of 105,000.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralPost)));
	}


	@Test
	public void returnPositionsTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);

		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addBrokerPosition("Security1", 200_000, 200_000);
		testSetup.addBrokerPosition("Security2", 60_000, 60_000);
		testSetup.addCommonHierarchyAncestor("Security1", "Security2");
		testSetup.addBrokerPosition("Security3", 10_000, 10_000);
		testSetup.setUpHierarchyPriority("Security1", "Security3");
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = testSetup.getBuiltBrokerMap();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test Post is empty
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValueIncludingCash(collateralPost)));

		//Test no cash returned
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn)), "Expected Cash to be 0 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn)));

		//Test number of returned entities
		Assertions.assertNotNull(collateralReturn.values());
		Assertions.assertEquals(3, collateralReturn.values().size(), "Expected 3 position pledged but found " + collateralReturn.values().size());

		FuturesCollateralPositionPledge security1 = collateralReturn.get(testSetup.getMockedSecurityByName("Security1"));
		FuturesCollateralPositionPledge security2 = collateralReturn.get(testSetup.getMockedSecurityByName("Security2"));
		FuturesCollateralPositionPledge security3 = collateralReturn.get(testSetup.getMockedSecurityByName("Security3"));

		//Test correct securities returned
		Assertions.assertNotNull(security1, "Expected security 1 pledged");
		Assertions.assertNotNull(security2, "Expected security 2 pledged");
		Assertions.assertNotNull(security3, "Expected security 3 pledged");
		//Test correct quantities returned
		Assertions.assertTrue(MathUtils.isEqual(95_000, security1.getQuantity()), "Expected quantity for security 1 to be 95,000 but was :" + CoreMathUtils.formatNumberInteger(security1.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(60_000, security2.getQuantity()), "Expected quantity for security 2 to be 60,000 but was :" + CoreMathUtils.formatNumberInteger(security2.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(10_000, security3.getQuantity()), "Expected quantity for security 3 to be 10,000 but was :" + CoreMathUtils.formatNumberInteger(security3.getQuantity()));

		//Test total collateral value = Excess - (required + buffer)
		Assertions.assertTrue(MathUtils.isEqual(165_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralReturn)), "Expected total position collateral value of 165,000.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralReturn)));
	}


	@Test
	public void returnCashHaircutTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);

		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addBrokerPosition("Security1", 200_000, 200_000, "0.5");
		testSetup.addBrokerHeldCash(100_000);
		testSetup.setUpHierarchyPriority("Security1", FuturesCollateralTestSetupUtil.CASH_SECURITY_NAME);
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = testSetup.getBuiltBrokerMap();
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test Post is empty
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValueIncludingCash(collateralPost)));

		//Test cash returned
		Assertions.assertTrue(MathUtils.isEqual(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn), 95_000), "Expected Cash to be 95,000.00 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn)));

		//Test number of returned entities
		Assertions.assertNotNull(collateralReturn.values());
		Assertions.assertTrue(MathUtils.isEqual(1, collateralReturn.size()), "Expected 1 position pledged but found " + collateralReturn.values().size());

		//Test no securities returned
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(collateralReturn)), "Expected total position market value of 0.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(collateralReturn)));
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValue(collateralReturn)), "Expected total position collateral value of 0.00 but was: " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValue(collateralReturn)));
	}


	@Test
	public void returnAndReplacePositionsTest() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = new BigDecimal(5);

		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		testSetup.addCustodianPosition("Security1", 200_000, 200_000);
		testSetup.addCustodianHeldCash(100_000);
		testSetup.addBrokerPosition("Security2", 60_000, 60_000);
		testSetup.addBrokerPosition("Security3", 10_000, 10_000);
		testSetup.addCommonHierarchyAncestor("Security1", "Security2");
		testSetup.addBrokerHeldCash(50_000);
		testSetup.setUpHierarchyPriority("Security1", "Security3", FuturesCollateralTestSetupUtil.CASH_SECURITY_NAME);
		testSetup.generateMockedHoldings();

		for (InvestmentInstrumentHierarchy h : testSetup.getMockedHierarchyList()) {
			System.out.println(h.isCurrency());
		}

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = testSetup.getBuiltCustodianMap();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = testSetup.getBuiltBrokerMap();

		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralReturn = spyContext.getCollateralReturn();

		//Test no cash posted
		Assertions.assertTrue(MathUtils.isNullOrZero(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)), "Expected Cash post to be 0 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralPost)));
		//Test cash returned
		Assertions.assertTrue(MathUtils.isEqual(50_000, CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn)), "Expected Cash return to be 50,000.00 but was : " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralReturn)));

		FuturesCollateralPositionPledge postSecurity1 = collateralPost.get(testSetup.getMockedSecurityByName("Security1"));
		FuturesCollateralPositionPledge returnedSecurity3 = collateralReturn.get(testSetup.getMockedSecurityByName("Security3"));
		//Test number of posted entities
		Assertions.assertNotNull(collateralPost.values());
		Assertions.assertEquals(1, collateralPost.values().size(), "Expected 1 position pledged but found " + collateralPost.values().size());
		//Test number of returned entities
		Assertions.assertNotNull(collateralReturn.values());
		Assertions.assertEquals(2, collateralReturn.values().size(), "Expected 2 positions returned but found " + collateralReturn.values().size());

		//Test correct securities posted
		Assertions.assertNotNull(postSecurity1, "Expected security 1 pledged");
		//Test correct quantities are posted
		Assertions.assertTrue(MathUtils.isEqual(45_000, postSecurity1.getQuantity()), "Expected quantity for security 1 to be 45,000 but was :" + CoreMathUtils.formatNumberInteger(postSecurity1.getQuantity()));

		//Test correct securities returned
		Assertions.assertNotNull(returnedSecurity3, "Expected security 3 pledged");
		//Test correct quantities are returned
		Assertions.assertTrue(MathUtils.isEqual(10_000, returnedSecurity3.getQuantity()), "Expected quantity for security 3 to be 10,000 but was :" + CoreMathUtils.formatNumberInteger(returnedSecurity3.getQuantity()));
	}


	@Test
	public void prioritizationTests() {
		//Define Parameters
		BigDecimal requiredAmount = new BigDecimal(100_000);
		BigDecimal targetBuffer = BigDecimal.ZERO;

		Date monthAfterNext = DateUtils.addMonths(new Date(), 2);
		FuturesCollateralTestSetupUtil testSetup = new FuturesCollateralTestSetupUtil();
		//Security0 has lowest priority.
		testSetup.addCustodianPosition("Security0", 100_000, 102_000, "0.02");
		//Security1 4th highest priority because of quantity.
		testSetup.addCustodianPosition("Security1", 200_000, 102_000, "0.02");
		//Security2 3rd highest priority because pledge exists.
		testSetup.addCustodianPosition("Security2", 100_000, 102_000, "0.02");
		//Security3 has lower haircut value should be 2nd highest priority.
		testSetup.addCustodianPosition("Security3", 100_000, 101_000, "0.01");
		//Security4 has furthest out maturity date should have top priority.
		testSetup.addCustodianPosition("Security4", 100_000, 102_000, "0.02");

		testSetup.addBrokerPosition("Security2", 5_000, 5_000, "0.02");
		testSetup.overrideDefaultMaturityDate("Security4", monthAfterNext);
		//To test priority everything must fall under the same hierarchy
		testSetup.addCommonHierarchyAncestor("Security0", "Security1", "Security2", "Security3", "Security4");
		testSetup.generateMockedHoldings();

		//Set Holdings
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeld = testSetup.getBuiltCustodianMap();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeld = testSetup.getBuiltBrokerMap();

		//Set Holdings
		CollateralBalancePledgedTransactionsFuturesWorkflowContext spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);

		CollateralBalancePledgedTransactionsFuturesWorkflowAction workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralPost = spyContext.getCollateralPost();

		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security4")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security3")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security2")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security1")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security0")));


		//reset Holdings
		collateralPost.clear();
		requiredAmount = new BigDecimal(200_000);
		spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);
		workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		collateralPost = spyContext.getCollateralPost();

		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security4")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security3")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security2")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security1")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security0")));

		//reset Holdings
		requiredAmount = new BigDecimal(300_000);
		collateralPost.clear();
		spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);
		workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		collateralPost = spyContext.getCollateralPost();
		System.out.println(collateralPost.get(testSetup.getMockedSecurityByName("Security4")));
		System.out.println(collateralPost.get(testSetup.getMockedSecurityByName("Security3")));
		System.out.println(collateralPost.get(testSetup.getMockedSecurityByName("Security2")));
		System.out.println(collateralPost.get(testSetup.getMockedSecurityByName("Security1")));
		System.out.println(collateralPost.get(testSetup.getMockedSecurityByName("Security0")));

		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security4")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security3")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security2")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security1")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security0")));
		//Set Holdings
		requiredAmount = new BigDecimal(400_000);
		collateralPost.clear();
		spyContext = CollateralWorkflowFuturesTestUtils.buildDefaultSpyContext(brokerHeld, custodianHeld, requiredAmount, targetBuffer);
		workflowAction = new CollateralBalancePledgedTransactionsFuturesWorkflowAction();
		CollateralWorkflowFuturesTestUtils.mockWorkflowServices(workflowAction, spyContext, testSetup.getMockedHierarchyList());
		ReflectionTestUtils.invokeMethod(workflowAction, "processCollateral", spyContext);
		collateralPost = spyContext.getCollateralPost();

		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security4")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security3")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security2")));
		Assertions.assertNotNull(collateralPost.get(testSetup.getMockedSecurityByName("Security1")));
		Assertions.assertNull(collateralPost.get(testSetup.getMockedSecurityByName("Security0")));
	}
}
