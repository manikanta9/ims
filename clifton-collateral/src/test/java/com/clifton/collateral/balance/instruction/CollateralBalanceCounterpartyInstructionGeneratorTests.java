package com.clifton.collateral.balance.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CollateralBalanceCounterpartyInstructionGeneratorTests {

	@Resource
	private ReadOnlyDAO<CollateralBalance> collateralBalanceDAO;

	@Resource
	private ReadOnlyDAO<BusinessCompany> businessCompanyDAO;

	@Resource
	private InstructionGenerator<InstructionMessage, CollateralBalance, InstructionDeliveryFieldCommand> collateralBalanceCounterpartyInstructionGenerator;

	@Mock
	private SystemSchemaUtilHandler systemSchemaUtilHandler;

	@Mock
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;


	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.systemSchemaUtilHandler.getEntityUniqueId(ArgumentMatchers.any())).thenReturn("IMS42COL");

		((CollateralBalanceCounterpartyInstructionGenerator) this.collateralBalanceCounterpartyInstructionGenerator).setSystemSchemaUtilHandler(this.systemSchemaUtilHandler);
		((CollateralBalanceCounterpartyInstructionGenerator) this.collateralBalanceCounterpartyInstructionGenerator).setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);
	}


	@Test
	public void testGeneralFinancialInstitutionTransferMessage() {
		CollateralBalance balance = getBalanceObject(50);
		BusinessCompany goldman = this.businessCompanyDAO.findByPrimaryKey(2);
		Assertions.assertNotNull(goldman);
		MatcherAssert.assertThat(goldman.getName(), IsEqual.equalTo("Goldman Sachs"));

		InvestmentInstructionItem investmentInstructionItem = buildInvestmentInstructionItem(balance.getId(), SystemTableBuilder.createCollateralBalance().toSystemTable());
		InvestmentInstructionDelivery investmentInstructionDelivery = new InvestmentInstructionDelivery();
		investmentInstructionDelivery.setId(1);
		investmentInstructionDelivery.setDeliveryAccountName("001820451");
		BusinessCompany businessCompany = new BusinessCompany();
		businessCompany.setBusinessIdentifierCode("MRMDUS30");
		investmentInstructionDelivery.setDeliveryCompany(businessCompany);
		investmentInstructionDelivery.setDeliveryAccountNumber("001820451");
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.eq(investmentInstructionItem),
				ArgumentMatchers.eq(balance.getHoldingInvestmentAccount().getIssuingCompany()),
				ArgumentMatchers.eq(balance.getCollateralCurrency())))
				.thenReturn(investmentInstructionDelivery);
		InstructionMessage message = this.collateralBalanceCounterpartyInstructionGenerator.generateInstructionMessageList(investmentInstructionItem).get(0);

		Assertions.assertTrue(message instanceof GeneralFinancialInstitutionTransferMessage);

		GeneralFinancialInstitutionTransferMessage transferMessage = (GeneralFinancialInstitutionTransferMessage) message;
		Assertions.assertEquals("PPSCUS66", transferMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());
		Assertions.assertEquals("USD", transferMessage.getCurrency());
		Assertions.assertEquals(DateUtils.fromDate(transferMessage.getValueDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("1247357.72"), "#,###.###############"), CoreMathUtils.formatNumber(transferMessage.getAmount(), "#,###.###############"));

		Assertions.assertEquals("NT-1", transferMessage.getCustodyAccountNumber());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getCustodyBIC());

		Assertions.assertEquals("Gold-1", transferMessage.getBeneficiaryAccountNumber());
		Assertions.assertEquals("GOLDUS33", transferMessage.getBeneficiaryBIC());

		Assertions.assertEquals("001820451", transferMessage.getIntermediaryAccountNumber());
		Assertions.assertEquals("MRMDUS30", transferMessage.getIntermediaryBIC());
	}


	@Test
	public void testNoticeToReceiveTransferMessageMessage() {
		CollateralBalance balance = getBalanceObject(51);
		BusinessCompany goldman = this.businessCompanyDAO.findByPrimaryKey(2);
		Assertions.assertNotNull(goldman);
		MatcherAssert.assertThat(goldman.getName(), IsEqual.equalTo("Goldman Sachs"));

		InvestmentInstructionItem investmentInstructionItem = buildInvestmentInstructionItem(balance.getId(), SystemTableBuilder.createCollateralBalance().toSystemTable());
		InvestmentInstructionDelivery investmentInstructionDelivery = new InvestmentInstructionDelivery();
		investmentInstructionDelivery.setId(1);
		investmentInstructionDelivery.setDeliveryAccountName("001820451");
		BusinessCompany businessCompany = new BusinessCompany();
		businessCompany.setBusinessIdentifierCode("MRMDUS30");
		investmentInstructionDelivery.setDeliveryCompany(businessCompany);
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.eq(investmentInstructionItem),
				ArgumentMatchers.eq(balance.getHoldingInvestmentAccount().getIssuingCompany()),
				ArgumentMatchers.eq(balance.getCollateralCurrency())))
				.thenReturn(investmentInstructionDelivery);
		InstructionMessage message = this.collateralBalanceCounterpartyInstructionGenerator.generateInstructionMessageList(investmentInstructionItem).get(0);

		Assertions.assertTrue(message instanceof NoticeToReceiveTransferMessage);

		NoticeToReceiveTransferMessage transferMessage = (NoticeToReceiveTransferMessage) message;
		Assertions.assertEquals("PPSCUS66", transferMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());
		Assertions.assertEquals("USD", transferMessage.getCurrency());
		Assertions.assertEquals(DateUtils.fromDate(transferMessage.getValueDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("12500000.75"), "#,###.###############"), CoreMathUtils.formatNumber(transferMessage.getAmount(), "#,###.###############"));

		Assertions.assertEquals("NT-1", transferMessage.getReceiverAccountNumber());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());

		Assertions.assertEquals("GOLDUS33", transferMessage.getOrderingBIC());
		Assertions.assertEquals("MRMDUS30", transferMessage.getIntermediaryBIC());
	}


	private CollateralBalance getBalanceObject(int id) {
		return this.collateralBalanceDAO.findByPrimaryKey(id);
	}


	private InvestmentInstructionItem buildInvestmentInstructionItem(int fkFieldId, SystemTable systemTable) {
		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = new InvestmentInstructionDeliveryType();
		investmentInstructionDeliveryType.setId((short) 1);

		InvestmentInstructionCategory category = new InvestmentInstructionCategory();
		category.setTable(systemTable);

		InvestmentInstructionDefinition definition = new InvestmentInstructionDefinition();
		definition.setCategory(category);
		definition.setDeliveryType(investmentInstructionDeliveryType);

		InvestmentInstruction investmentInstruction = new InvestmentInstruction();
		investmentInstruction.setDefinition(definition);

		InvestmentInstructionItem investmentInstructionItem = new InvestmentInstructionItem();
		investmentInstructionItem.setId(1);
		investmentInstructionItem.setFkFieldId(fkFieldId);
		investmentInstructionItem.setInstruction(investmentInstruction);

		return investmentInstructionItem;
	}
}
