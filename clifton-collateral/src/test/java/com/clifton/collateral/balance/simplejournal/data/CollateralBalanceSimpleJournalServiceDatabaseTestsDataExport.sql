SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId
FROM TradeFill
WHERE TradeFillID IN (866069, 866117, 2465248, 2506408)
UNION
SELECT 'InvestmentAccountRelationshipPurpose' AS entityTableName, InvestmentAccountRelationshipPurposeID AS entityId
FROM InvestmentAccountRelationshipPurpose
WHERE InvestmentAccountRelationshipPurposeID IN (2, 4, 5, 6, 7, 8, 10, 11, 17, 20, 23, 30)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId
FROM InvestmentAccountRelationShip
WHERE MainAccountID IN (2481, 281, 5879)
UNION
SELECT 'InvestmentAccountRelationshipMapping' AS entityTableName, InvestmentAccountRelationshipMappingID AS entityId
FROM InvestmentAccountRelationShipMapping
WHERE (MainAccountTypeID = 1 AND RelatedAccountTypeID IN (1, 2, 6, 20))
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId
FROM InvestmentAccount
WHERE AccountNumber IN ('124100', '052-BAGXD', '052-87296', '157-63164')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId
FROM AccountingTransaction
WHERE (ClientInvestmentAccountID = 2481 AND TransactionDate = '2017-05-04')
   OR (HoldingInvestmentAccountID = 842 AND AccountingAccountID IN (751, 804, 805, 806, 807))
   OR (HoldingInvestmentAccountID = 5880 AND AccountingAccountID IN (751, 804, 805, 806, 807))
UNION
SELECT 'AccountingSimpleJournalType' AS entityTableName, AccountingSimpleJournalTypeID AS entityId
FROM AccountingSimpleJournalType
UNION
SELECT 'MarketDataSource' AS entityTableName, MarketDataSourceID AS entityId
FROM MarketDataSource
