package com.clifton.collateral.balance.workflow;

import com.clifton.collateral.balance.workflow.futures.CollateralBalanceFuturesCollateralWorkflowUtils;
import com.clifton.collateral.balance.workflow.futures.FuturesCollateralPositionPledge;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * @author jonathanr
 */
public class CollateralBalanceFuturesCollateralWorkflowUtilsTests {


	private List<InvestmentInstrumentHierarchy> hierarchyList;
	private FuturesCollateralPositionPledge pledge1;
	private FuturesCollateralPositionPledge pledge2;
	private FuturesCollateralPositionPledge pledge3;
	private FuturesCollateralPositionPledge cashPledge;


	@BeforeEach
	private void init() {
		InvestmentInstrumentHierarchy parentHierarchy1 = Mockito.mock(InvestmentInstrumentHierarchy.class);
		InvestmentInstrumentHierarchy parentHierarchy2 = Mockito.mock(InvestmentInstrumentHierarchy.class);
		InvestmentInstrumentHierarchy cashHierarchy = Mockito.mock(InvestmentInstrumentHierarchy.class);
		Mockito.when(cashHierarchy.isCurrency()).thenReturn(true);
		List<InvestmentInstrumentHierarchy> investmentInstrumentHierarchyList = new ArrayList<>();
		investmentInstrumentHierarchyList.add(parentHierarchy1);
		investmentInstrumentHierarchyList.add(parentHierarchy2);
		investmentInstrumentHierarchyList.add(cashHierarchy);

		this.hierarchyList = investmentInstrumentHierarchyList;
		InvestmentAccount clientAccount = Mockito.mock(InvestmentAccount.class);
		Mockito.when(clientAccount.getId()).thenReturn(1);

		InvestmentAccount collateralAccount = Mockito.mock(InvestmentAccount.class);
		Mockito.when(collateralAccount.getId()).thenReturn(100);

		InvestmentSecurity investmentSecurity1 = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(1, this.hierarchyList.get(0));
		InvestmentSecurity investmentSecurity2 = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(2, this.hierarchyList.get(0));
		InvestmentSecurity investmentSecurity3 = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(3, this.hierarchyList.get(1));
		InvestmentSecurity cashInvestmentSecurity = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(5, this.hierarchyList.get(2));


		this.pledge1 = new FuturesCollateralPositionPledge(investmentSecurity1, new BigDecimal(1_000), new BigDecimal(1_000), BigDecimal.ZERO);
		this.pledge2 = new FuturesCollateralPositionPledge(investmentSecurity2, new BigDecimal(1_000), new BigDecimal(2_000), BigDecimal.ZERO);
		this.pledge3 = new FuturesCollateralPositionPledge(investmentSecurity3, new BigDecimal(3_000), new BigDecimal(4_000), new BigDecimal(".5"));
		this.cashPledge = new FuturesCollateralPositionPledge(cashInvestmentSecurity, new BigDecimal(5_000), new BigDecimal(5_000), BigDecimal.ZERO);
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getDefaultCollateralMap() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = new HashMap<>();
		collateralMap.put(this.pledge1.getInvestmentSecurity(), this.pledge1);
		collateralMap.put(this.pledge2.getInvestmentSecurity(), this.pledge2);
		collateralMap.put(this.pledge3.getInvestmentSecurity(), this.pledge3);
		collateralMap.put(this.cashPledge.getInvestmentSecurity(), this.cashPledge);
		return collateralMap;
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetCashValueTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getCashValue method
		Assertions.assertTrue(MathUtils.isEqual(5_000, CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralMap)), "Expected Total cash to be $5,000 but was " + CollateralBalanceFuturesCollateralWorkflowUtils.getCashValue(collateralMap));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetTotalPositionQuantityTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getTotalPositionQuantity method
		Assertions.assertTrue(MathUtils.isEqual(10_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionQuantity(collateralMap)), "Expected Total Quantity of 10,000 but was " + CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionQuantity(collateralMap));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetTotalPositionMarketValueTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getTotalPositionMarketValue method
		Assertions.assertTrue(MathUtils.isEqual(7_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(collateralMap)), "Expected Total Position Market Value of $7000 but was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValue(collateralMap)));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetTotalPositionMarketValueIncludingCashTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getTotalPositionMarketValueIncludingCash method
		Assertions.assertTrue(MathUtils.isEqual(12_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValueIncludingCash(collateralMap)), "Expected total position Market Value with cash to be $12,000 but was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionMarketValueIncludingCash(collateralMap)));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetTotalPositionCollateralValueTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getTotalPositionCollateralValue method
		Assertions.assertTrue(MathUtils.isEqual(5_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValue(collateralMap)), "Expected Total Collateral Value to be $5,000 but was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValue(collateralMap)));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetTotalPositionCollateralValueIncludingCashTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		//Test getTotalPositionCollateralValueIncludingCash method
		Assertions.assertTrue(MathUtils.isEqual(10_000, CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralMap)), "Expected Total Collateral Value with cash to be $10,000 but was " + CoreMathUtils.formatNumberMoney(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(collateralMap)));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsAggregateAndPutTest() {
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> collateralMap = getDefaultCollateralMap();
		InvestmentSecurity investmentSecurity4 = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(4, this.hierarchyList.get(1));
		FuturesCollateralPositionPledge pledge4 = new FuturesCollateralPositionPledge(investmentSecurity4, new BigDecimal(10_000), new BigDecimal(10_000), new BigDecimal(".5"));

		//Test aggregateAndPut new position
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(pledge4, collateralMap);
		Assertions.assertTrue(MathUtils.isEqual(5, collateralMap.size()), "Expected Collateral Map size to be 5 but was " + collateralMap.size());
		Assertions.assertNotNull(collateralMap.get(investmentSecurity4), "Expected Collateral map to have entry for key investmentSecurity4 but was null");
		Assertions.assertTrue(MathUtils.isEqual(10_000, collateralMap.get(investmentSecurity4).getMarketValue()), "Expected aggregated pledge4 market value to be $10,000 but was " + CoreMathUtils.formatNumberMoney(collateralMap.get(investmentSecurity4).getMarketValue()));

		//Test aggregateAndPut existing
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(pledge4, collateralMap);
		Assertions.assertTrue(MathUtils.isEqual(5, collateralMap.size()), "Expected Collateral Map size to be 5 but was " + collateralMap.size());
		Assertions.assertNotNull(collateralMap.get(investmentSecurity4), "Expected Collateral map to have entry for key investmentSecurity4 but was null");
		Assertions.assertTrue(MathUtils.isEqual(20_000, collateralMap.get(investmentSecurity4).getMarketValue()), "Expected aggregated pledge4 market value to be $20,000 but was " + CoreMathUtils.formatNumberMoney(collateralMap.get(investmentSecurity4).getMarketValue()));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsAggregateMapsTest() {

		// aggregateMaps and diffMap tests setup
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapA = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapB = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapC = new LinkedHashMap<>();
		mapA.put(this.pledge1.getInvestmentSecurity(), this.pledge1); //In A not B
		mapA.put(this.pledge2.getInvestmentSecurity(), this.pledge2); // In A and B
		mapB.put(this.pledge2.getInvestmentSecurity(), this.pledge2); // In A and B
		mapB.put(this.pledge3.getInvestmentSecurity(), this.pledge3); // In B not A

		//Trivial case for aggregateMaps
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateMaps(mapC, mapA);
		Assertions.assertNotNull(mapC.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNotNull(mapC.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapC.get(this.pledge1.getInvestmentSecurity()).getQuantity()), "Expected pledge1 in mapC to have quantity 1_000 but was " + mapC.get(this.pledge1.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapC.get(this.pledge1.getInvestmentSecurity()).getMarketValue()), "Expected pledge1 in mapC to have Market value $1,000 but was " + CoreMathUtils.formatNumberMoney(mapC.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapC.get(this.pledge2.getInvestmentSecurity()).getQuantity()), "Expected pledge2 in mapC to have quantity 1_000 but was " + mapC.get(this.pledge2.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(2_000, mapC.get(this.pledge2.getInvestmentSecurity()).getMarketValue()), "Expected pledge2 in mapC to have Market value $2,000 but was " + CoreMathUtils.formatNumberMoney(mapC.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));


		//Basic aggregateMaps test
		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateMaps(mapB, mapA);
		Assertions.assertNotNull(mapB.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapB.get(this.pledge1.getInvestmentSecurity()).getMarketValue()), "Expected Market value of $1,000 but was " + CoreMathUtils.formatNumberMoney(mapB.get(this.pledge1.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertTrue(MathUtils.isEqual(4_000, mapB.get(this.pledge2.getInvestmentSecurity()).getMarketValue()), "Expected Market value of $4,000 but was " + CoreMathUtils.formatNumberMoney(mapB.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertTrue(MathUtils.isEqual(4_000, mapB.get(this.pledge3.getInvestmentSecurity()).getMarketValue()), "Expected Market value of $4000 but was " + CoreMathUtils.formatNumberMoney(mapB.get(this.pledge3.getInvestmentSecurity()).getMarketValue()));

		//Ensure mapA is unchanged
		Assertions.assertNotNull(mapA.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNotNull(mapA.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA.get(this.pledge1.getInvestmentSecurity()).getQuantity()), "Expected pledge1 in mapA to have quantity 1_000 but was " + mapA.get(this.pledge1.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA.get(this.pledge1.getInvestmentSecurity()).getMarketValue()), "Expected pledge1 in mapA to have Market value $1,000 but was " + CoreMathUtils.formatNumberMoney(mapA.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA.get(this.pledge2.getInvestmentSecurity()).getQuantity()), "Expected pledge2 in mapA to have quantity 1_000 but was " + mapA.get(this.pledge2.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(2_000, mapA.get(this.pledge2.getInvestmentSecurity()).getMarketValue()), "Expected pledge2 in mapA to have Market value $2,000 but was " + CoreMathUtils.formatNumberMoney(mapA.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));
	}


	@Test
	public void collateralBalanceFuturesCollateralWorkflowUtilsGetPositiveDiffMapTests() {
		FuturesCollateralPositionPledge pledge2b = new FuturesCollateralPositionPledge(this.pledge2.getInvestmentSecurity(), new BigDecimal(2_000), new BigDecimal(4_000), BigDecimal.ZERO);


		// aggregateMaps and diffMap tests setup
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapA = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapB = new LinkedHashMap<>();
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> emptyMap = new LinkedHashMap<>();
		mapA.put(this.pledge1.getInvestmentSecurity(), this.pledge1); //In A not B
		mapA.put(this.pledge2.getInvestmentSecurity(), this.pledge2); // In A and B
		mapB.put(this.pledge2.getInvestmentSecurity(), this.pledge2); // In A and B
		mapB.put(this.pledge3.getInvestmentSecurity(), this.pledge3); // In B not A

		//DIFF TEST
		//Trivial case dif between empty map and map a should be an empty map because all values would be negative (greater than case)
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> emptyMap_MapA_diff = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(emptyMap, mapA);
		Assertions.assertNull(emptyMap_MapA_diff.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNull(emptyMap_MapA_diff.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertNull(emptyMap_MapA_diff.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertEquals(0, emptyMap_MapA_diff.values().size());


		//Trivial case dif between mapA and an empty map should be an exact copy of mapA (less than case)
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapA_emptyMap_diff = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(mapA, emptyMap);
		Assertions.assertNotNull(mapA_emptyMap_diff.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA_emptyMap_diff.get(this.pledge1.getInvestmentSecurity()).getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA_emptyMap_diff.get(this.pledge1.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertNotNull(mapA_emptyMap_diff.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, mapA_emptyMap_diff.get(this.pledge2.getInvestmentSecurity()).getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(2_000, mapA_emptyMap_diff.get(this.pledge2.getInvestmentSecurity()).getMarketValue()));
		Assertions.assertNull(mapA_emptyMap_diff.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertEquals(2, mapA_emptyMap_diff.values().size());

		//Trivial case diff between mapA and mapA_emptyMap_diff which is now an exact copy of mapA (equals case)
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> mapA_mapACopy_Diff = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(mapA_emptyMap_diff, mapA);
		Assertions.assertNull(mapA_mapACopy_Diff.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNull(mapA_mapACopy_Diff.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertNull(mapA_mapACopy_Diff.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertEquals(0, mapA_mapACopy_Diff.values().size());


		//Advanced cases
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> a_b_DiffMap = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(mapA, mapB);
		Assertions.assertNotNull(a_b_DiffMap.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNull(a_b_DiffMap.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertNull(a_b_DiffMap.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, a_b_DiffMap.get(this.pledge1.getInvestmentSecurity()).getQuantity()), "Expected 1,000 quantity but was " + a_b_DiffMap.get(this.pledge1.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(1_000, a_b_DiffMap.get(this.pledge1.getInvestmentSecurity()).getMarketValue()), "Expected 1,000 Market value but was " + a_b_DiffMap.get(this.pledge1.getInvestmentSecurity()).getMarketValue());
		Assertions.assertEquals(1, a_b_DiffMap.values().size());


		Map<InvestmentSecurity, FuturesCollateralPositionPledge> b_a_DiffMap = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(mapB, mapA);
		Assertions.assertNull(b_a_DiffMap.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNull(b_a_DiffMap.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertNotNull(b_a_DiffMap.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(3_000, b_a_DiffMap.get(this.pledge3.getInvestmentSecurity()).getQuantity()), "Expected 3,000 quantity but was " + b_a_DiffMap.get(this.pledge3.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(4_000, b_a_DiffMap.get(this.pledge3.getInvestmentSecurity()).getMarketValue()), "Expected 4,000 Market value but was " + b_a_DiffMap.get(this.pledge3.getInvestmentSecurity()).getMarketValue());
		Assertions.assertEquals(1, b_a_DiffMap.values().size());


		CollateralBalanceFuturesCollateralWorkflowUtils.aggregateAndPut(pledge2b, mapA);
		Map<InvestmentSecurity, FuturesCollateralPositionPledge> augA_b_diffMap = CollateralBalanceFuturesCollateralWorkflowUtils.getPositiveDifferenceMap(mapA, mapB);
		Assertions.assertNotNull(augA_b_diffMap.get(this.pledge1.getInvestmentSecurity()));
		Assertions.assertNotNull(augA_b_diffMap.get(this.pledge2.getInvestmentSecurity()));
		Assertions.assertNull(augA_b_diffMap.get(this.pledge3.getInvestmentSecurity()));
		Assertions.assertTrue(MathUtils.isEqual(1_000, augA_b_diffMap.get(this.pledge1.getInvestmentSecurity()).getQuantity()), "Expected 1,000 quantity but was " + augA_b_diffMap.get(this.pledge1.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(1_000, augA_b_diffMap.get(this.pledge1.getInvestmentSecurity()).getMarketValue()), "Expected 1,000 Market value but was " + augA_b_diffMap.get(this.pledge1.getInvestmentSecurity()).getMarketValue());
		Assertions.assertTrue(MathUtils.isEqual(2_000, augA_b_diffMap.get(this.pledge2.getInvestmentSecurity()).getQuantity()), "Expected 2,000 quantity but was " + augA_b_diffMap.get(this.pledge2.getInvestmentSecurity()).getQuantity());
		Assertions.assertTrue(MathUtils.isEqual(4_000, augA_b_diffMap.get(this.pledge2.getInvestmentSecurity()).getMarketValue()), "Expected 4,000 Market value but was " + augA_b_diffMap.get(this.pledge2.getInvestmentSecurity()).getMarketValue());
		Assertions.assertEquals(2, augA_b_diffMap.values().size());
	}
}
