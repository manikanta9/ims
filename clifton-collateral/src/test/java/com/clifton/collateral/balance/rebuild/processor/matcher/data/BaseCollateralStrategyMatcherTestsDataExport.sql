SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId
FROM InvestmentAccount
WHERE AccountNumber = '237-70639-2-0'
UNION
-- Accounting Position Daily was moved to Data Additional since it gets wiped after 1 year
-- SELECT 'AccountingPositionDaily' AS entityTableName, AccountingPositionDailyID AS entityId
SELECT 'AccountingTransaction', AccountingTransactionID
FROM AccountingTransaction
WHERE TransactionDate <= '03/21/2019'
  AND ClientInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '800025')
  	 AND AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccount WHERE IsCollateral = 0)
	 AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '237-70639-2-0')
UNION
SELECT 'SystemColumnGroup' AS entityTableName, SystemColumnGroupID AS entityId
FROM SystemColumnGroup
WHERE ColumnGroupName = 'Security Custom Fields'
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId
FROM SystemColumnValue
WHERE FKFieldID IN (SELECT InvestmentSecurityID
					FROM AccountingTransaction
					WHERE AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccount WHERE IsCollateral = 0)
					  AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '237-70639-2-0'))
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId
FROM MarketDataValue
WHERE MeasureDate = '03/21/2019'
  AND MarketDataFieldID IN
	  (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName IN ('Last Trade Price', 'Mid Price', 'Forward Price', 'Last Price - Trade Data Field', ''))
  AND (InvestmentSecurityID IN (SELECT InvestmentSecurityID
								FROM AccountingTransaction
								WHERE AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccount WHERE IsCollateral = 0)
								  AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '237-70639-2-0'))
	OR InvestmentSecurityID IN (SELECT i.UnderlyingSecurityID
								FROM AccountingTransaction
									 INNER JOIN InvestmentSecurity i ON AccountingTransaction.InvestmentSecurityID = i.InvestmentSecurityID
								WHERE AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccount WHERE IsCollateral = 0)
								  AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '237-70639-2-0'))
	OR InvestmentSecurityID IN (SELECT i.InvestmentInstrumentID
								FROM AccountingTransaction
									 INNER JOIN InvestmentSecurity i ON AccountingTransaction.InvestmentSecurityID = i.InvestmentSecurityID
								WHERE AccountingAccountID IN (SELECT AccountingAccountID FROM AccountingAccount WHERE IsCollateral = 0)
								  AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '237-70639-2-0')))
UNION
SELECT 'MarketDataFieldMapping' AS entityTableName, MarketDataFieldMappingID AS entityId
FROM MarketDataFieldMapping
UNION
SELECT 'MarketDataPriceFieldMapping' AS entityTableName, MarketDataPriceFieldMappingID AS entityId
FROM MarketDataPriceFieldMapping
UNION
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId
FROM CalendarDay
WHERE CalendarYearID = 2019
  AND CalendarMonthID in (2, 3);
