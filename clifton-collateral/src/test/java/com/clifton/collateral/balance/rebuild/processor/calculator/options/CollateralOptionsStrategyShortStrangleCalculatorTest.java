package com.clifton.collateral.balance.rebuild.processor.calculator.options;

import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceOptionsRebuildProcessor;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyJoinedPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySplitPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyStranglePosition;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentTypeSubTypeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;


/**
 * @author terrys
 */
public class CollateralOptionsStrategyShortStrangleCalculatorTest {

	private CollateralOptionsStrategyShortStrangleCalculator calculator = new CollateralOptionsStrategyShortStrangleCalculator();
	private CollateralBalanceOptionsRebuildProcessor rebuildProcessor = new CollateralBalanceOptionsRebuildProcessor();


	@BeforeEach
	public void before() {
		this.rebuildProcessor.setCallRequirementType(CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN);
		this.rebuildProcessor.setPutRequirementType(CollateralBalanceOptionsRebuildProcessor.COLLATERAL_MARGIN);
		this.rebuildProcessor.setIndexMarginRequirementPercentage(new BigDecimal(".15"));
	}


	@Test
	public void calculateCollateralTest() {
		Date positionDate = DateUtils.toDate("03/21/2019");
		InvestmentInstrument investmentInstrument = InvestmentInstrumentBuilder
				.createEmptyInvestmentInstrument()
				.withHierarchy(
						InvestmentInstrumentHierarchyBuilder
								.createEmptyInvestmentInstrumentHierarchy()
								.withInvestmentTypeSubType(
										InvestmentTypeSubTypeBuilder.createIndices()
												.withName("Options On Indices")
												.toInvestmentTypeSubType()
								)
								.toInvestmentInstrumentHierarchy()
				)
				.withPriceMultiplier(new BigDecimal("100"))
				.toInvestmentInstrument();
		InvestmentSecurity c2817 = InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
				.withId(1)
				.withInstrument(investmentInstrument)
				.withName("c2817")
				.withOptionType(InvestmentSecurityOptionTypes.CALL)
				.withOptionStrikePrice(new BigDecimal("2865"))
				.build();
		CollateralOptionsStrategyPosition c2864_117 = new CollateralOptionsStrategyPosition(
				positionDate,
				c2817,
				new BigDecimal("100"),
				new BigDecimal("-117"),
				"no group",
				new BigDecimal("2865").multiply(new BigDecimal("100").multiply(new BigDecimal("117"))),
				new BigDecimal("2854.88"),
				new BigDecimal("2854.88").multiply(new BigDecimal("100").multiply(new BigDecimal("117"))),
				new BigDecimal("16.3"),
				new BigDecimal("16.3").multiply(new BigDecimal("100").multiply(new BigDecimal("117"))),
				1L
		);
		CollateralOptionsStrategyPosition c2864_20 = new CollateralOptionsStrategyPosition(c2864_117, new BigDecimal("-20"));
		InvestmentSecurity p2715 = InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
				.withId(2)
				.withName("p2715")
				.withInstrument(investmentInstrument)
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.withOptionStrikePrice(new BigDecimal("2715"))
				.build();
		CollateralOptionsStrategyPosition p2715_125 = new CollateralOptionsStrategyPosition(
				positionDate,
				p2715,
				new BigDecimal("100"),
				new BigDecimal("-125"),
				"no group",
				new BigDecimal("2715").multiply(new BigDecimal("100").multiply(new BigDecimal("125"))),
				new BigDecimal("2854.88"),
				new BigDecimal("2854.88").multiply(new BigDecimal("100").multiply(new BigDecimal("125"))),
				new BigDecimal("1"),
				new BigDecimal("1").multiply(new BigDecimal("100").multiply(new BigDecimal("125"))),
				2L
		);
		CollateralOptionsStrategySplitPosition strategySplitPosition = new CollateralOptionsStrategySplitPosition(c2864_20, new BigDecimal("-8"));
		CollateralOptionsStrategyJoinedPosition collateralOptionsStrategyJoinedPosition = new CollateralOptionsStrategyJoinedPosition(Arrays.asList(c2864_117, strategySplitPosition), new BigDecimal("-125"));

		CollateralOptionsStrategyStranglePosition stranglePosition = new CollateralOptionsStrategyStranglePosition(p2715_125, collateralOptionsStrategyJoinedPosition);
		this.calculator.populateCollateral(this.rebuildProcessor, stranglePosition, false);

		BigDecimal callProceeds = new BigDecimal("125").multiply(new BigDecimal("100")).multiply(c2864_117.getOptionPrice());
		CollateralOptionsStrategyShortCallCalculator shortCallCalculator = new CollateralOptionsStrategyShortCallCalculator();
		BigDecimal shortAmount = shortCallCalculator.calculateCollateral(this.rebuildProcessor, collateralOptionsStrategyJoinedPosition);
		Assertions.assertEquals(0, shortAmount.compareTo(stranglePosition.getRequiredCollateral()),
				String.format("calculated amount [%s] does not equal calculator amount [%s]."
						, CoreMathUtils.formatNumber(shortAmount, null)
						, CoreMathUtils.formatNumber(stranglePosition.getRequiredCollateral(), null)));
	}
}
