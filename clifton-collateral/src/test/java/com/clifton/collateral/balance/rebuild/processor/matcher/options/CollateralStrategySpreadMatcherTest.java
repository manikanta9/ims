package com.clifton.collateral.balance.rebuild.processor.matcher.options;

import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategySpreadPosition;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeBuilder;
import com.clifton.investment.setup.InvestmentTypeSubTypeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class CollateralStrategySpreadMatcherTest {

	private CollateralStrategySpreadMatcher spreadMatcher = new CollateralStrategySpreadMatcher();

	private Date positionDate = DateUtils.toDate("03/21/2020");

	private InvestmentInstrumentHierarchy hierarchy = InvestmentInstrumentHierarchyBuilder.createEmptyInvestmentInstrumentHierarchy()
			.withId((short) 1)
			.withInvestmentType(InvestmentTypeBuilder.forInvestmentType(InvestmentType.OPTIONS).toInvestmentType())
			.withInvestmentTypeSubType(InvestmentTypeSubTypeBuilder.createOptionsOnIndices().toInvestmentTypeSubType())
			.toInvestmentInstrumentHierarchy();

	private InvestmentInstrument investmentInstrument = InvestmentInstrumentBuilder.createEmptyInvestmentInstrument()
			.withId(1)
			.withName("MSCI EAFE Index FLEX Options")
			.withHierarchy(this.hierarchy)
			.toInvestmentInstrument();

	private InvestmentSecurity investmentSecurity_P1475;
	private InvestmentSecurity investmentSecurity_P1770;

	private BigDecimal priceMultiplier = new BigDecimal("100");

	private AggregateCollateralStrategyOptionMatcher.PositionMutations positionMutations = new AggregateCollateralStrategyOptionMatcher.PositionMutations();


	@BeforeEach
	public void before() {
		this.investmentSecurity_P1475 = InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
				.withId(1)
				.withSymbol("MXEA FLEX 09/20/19 P1475.00")
				.withEndDate(DateUtils.toDate("09/20/2019"))
				.withInstrument(this.investmentInstrument)
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.withOptionStrikePrice(new BigDecimal("1475"))
				.toInvestmentSecurity();
		this.investmentSecurity_P1770 = InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
				.withId(1)
				.withSymbol("MXEA FLEX 09/20/19 P1770.00")
				.withEndDate(DateUtils.toDate("09/20/2019"))
				.withInstrument(this.investmentInstrument)
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.withOptionStrikePrice(new BigDecimal("1770"))
				.toInvestmentSecurity();
	}


	@Test
	public void doGetCollateralStrategyPositionListTest() {
		List<CollateralOptionsStrategyPosition> positionInfoList = Arrays.asList(
				new CollateralOptionsStrategyPosition(
						this.positionDate,
						this.investmentSecurity_P1475,
						this.priceMultiplier,
						new BigDecimal("-26"),
						"group",
						new BigDecimal("1475").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						new BigDecimal("1863.85"),
						new BigDecimal("1863.85").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						new BigDecimal(".5852"),
						new BigDecimal(".5852").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						1L
				),
				new CollateralOptionsStrategyPosition(
						this.positionDate,
						this.investmentSecurity_P1770,
						this.priceMultiplier,
						new BigDecimal("26"),
						"group",
						new BigDecimal("1770").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						new BigDecimal("1863.85"),
						new BigDecimal("1863.85").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						new BigDecimal("7.61291"),
						new BigDecimal("7.61291").multiply(this.priceMultiplier).multiply(new BigDecimal("26")),
						2L
				)
		);
		List<CollateralOptionsStrategyPosition> matches = this.spreadMatcher.doGetCollateralStrategyPositionMap(positionInfoList, Collections.emptyList(), this.positionMutations).values().stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
		Assertions.assertFalse(matches.isEmpty());
		Assertions.assertEquals(1, matches.size());
		CollateralOptionsStrategyPosition position = matches.get(0);
		Assertions.assertTrue(position instanceof CollateralOptionsStrategySpreadPosition);
		CollateralOptionsStrategySpreadPosition spreadPosition = (CollateralOptionsStrategySpreadPosition) position;
		Assertions.assertEquals(positionInfoList.get(0), spreadPosition.getShortStrategyPosition());
		Assertions.assertEquals(positionInfoList.get(1), spreadPosition.getLongStrategyPosition());
	}
}
