SELECT 'AccountingTransactionType' AS entityTableName, AccountingTransactionTypeID AS entityId FROM AccountingTransactionType
UNION
SELECT 'CollateralBalance' AS entityTableName, CollateralBalanceID AS entityId FROM CollateralBalance WHERE BalanceDate = '08/07/2017' and CollateralBalanceID IN (Select CollateralBalanceID FROM CollateralBalance cb WHERE cb.ClientInvestmentAccountID IN (Select InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '663620') AND cb.HoldingInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '768-70113-1-7'))
UNION
SELECT 'CollateralType' AS entityTableName, CollateralTypeID AS entityId FROM CollateralType WHERE CollateralTypeName = 'Options Collateral'
UNION
SELECT 'CollateralConfiguration' AS entityTableName, CollateralConfigurationID AS entityId FROM CollateralConfiguration
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2332 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017')
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId FROM MarketDataValue WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND MeasureDate > '08/06/2017' AND MeasureDate < '08/15/2017')
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId FROM MarketDataValue WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2332 AND MeasureDate > '08/06/2017' AND MeasureDate < '08/15/2017')
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId FROM MarketDataValue WHERE InvestmentSecurityID IN (SELECT UnderlyingSecurityID FROM InvestmentSecurity WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND MeasureDate > '08/06/2017' AND MeasureDate < '08/15/2017'))
UNION
SELECT 'MarketDataPriceFieldMapping' AS entityTableName, MarketDataPriceFieldMappingID AS entityId FROM MarketDataPriceFieldMapping
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Last Trade Price')
	  and scv.FKFieldID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017'))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Last Trade Price')
	  and scv.FKFieldID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2332 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017'))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and  (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Last Trade Price')
	  and scv.FKFieldID IN (SELECT UnderlyingSecurityID FROM InvestmentSecurity WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017')))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Strike Price')
	  and scv.FKFieldID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017'))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Strike Price')
	  and scv.FKFieldID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2332 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017'))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv, SystemColumn sc, SystemColumnGroup scg, SystemTable st
WHERE scv.SystemColumnID = sc.SystemColumnID AND sc.SystemColumnGroupID = scg.SystemColumnGroupID AND scg.ColumnGroupName = 'Security Custom Fields' and sc.SystemTableID = st.SystemTableID AND st.TableName = 'InvestmentSecurity' and  (sc.ColumnName = 'Put or Call' or sc.ColumnName = 'Strike Price')
	  and scv.FKFieldID IN (SELECT UnderlyingSecurityID FROM InvestmentSecurity WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM AccountingTransaction WHERE HoldingInvestmentAccountID = 2330 AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE AccountingAccountTypeID IN (1, 2) and TransactionDate <= '08/14/2017')))
UNION
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = (Select CalendarYearID = 2017)
UNION
SELECT 'CalendarHolidayDay' AS entityTableName, CalendarHolidayDayID AS entityId FROM CalendarHolidayDay WHERE CalendarDayID IN (SELECT CalendarDayID FROM CalendarDay WHERE CalendarYearID = (Select CalendarYearID = 2017))
UNION
SELECT 'AccountingAccount' AS entityTableName, AccountingAccountID AS entityId FROM AccountingAccount
UNION
SELECT 'AccountingPositionTransferType' AS entityTableName, AccountingPositionTransferTypeID AS entityId FROM AccountingPositionTransferType
UNION
SELECT 'RuleCategory' AS entityTableName, RuleCategoryID AS entityId FROM RuleCategory WHERE CategoryName = 'Transfer Rules'
UNION
SELECT 'RuleViolationStatus' AS entityTableName, RuleViolationStatusID AS entityId FROM RuleViolationStatus
UNION
SELECT 'AccountingJournalType' AS entityTableName, AccountingJournalTypeID AS entityId FROM AccountingJournalType WHERE JournalTypeName = 'Transfer Journal'
UNION
SELECT 'AccountingJournalStatus' AS entityTableName, AccountingJournalStatusID AS entityId FROM AccountingJournalStatus
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountId IN (2272,2330,2331)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE InvestmentAccountRelationshipID = 3629
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE InvestmentAccountRelationshipID = 13104
UNION
SELECT 'InvestmentAccountGroup' AS entityTableName, InvestmentAccountGroupID AS entityId FROM InvestmentAccountGroup iag, InvestmentAccountGroupType iagt WHERE iag.InvestmentAccountGroupTypeID = iagt.InvestmentAccountGroupTypeID AND iagt.AccountGroupTypeName = 'Collateral Type Accounts'
UNION
SELECT 'InvestmentAccountGroupAccount' AS entityTableName, InvestmentAccountGroupAccountID AS entityId FROM InvestmentAccountGroupAccount WHERE InvestmentAccountID IN (2330)
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityId FROM InvestmentSecurityEventType
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityId FROM SystemBeanProperty WHERE SystemBeanID IN (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName LIKE 'CollateralType%')
UNION
SELECT 'WorkflowAssignment' AS entityTableName, WorkflowAssignmentID AS entityId FROM WorkflowAssignment WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow')
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityId FROM WorkflowTransition WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow'))
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityId FROM WorkflowTransition WHERE EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow'))
UNION
SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityId FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow')))
UNION
SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityId FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow')))
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityId FROM SystemBeanProperty WHERE SystemBeanID IN (SELECT ActionSystemBeanID FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow'))))
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityId FROM SystemBeanProperty WHERE SystemBeanID IN (SELECT ActionSystemBeanID FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Options Escrow Collateral Workflow'))))
UNION
SELECT 'TradeType' AS entityTableName, TradeTypeID AS entityId FROM TradeType WHERE TradeTypeName IN ('Bonds', 'Funds', 'Options')
UNION
SELECT 'MarketDataSource' AS entityTableName, MarketDataSourceID AS entityId FROM MarketDataSource WHERE DataSourceName = 'Morgan Stanley & Co. Inc.';
