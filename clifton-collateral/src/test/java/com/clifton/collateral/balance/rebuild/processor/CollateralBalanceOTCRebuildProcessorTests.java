package com.clifton.collateral.balance.rebuild.processor;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.OTCCollateralBalance;
import com.clifton.collateral.balance.rebuild.processor.position.CollateralStrategyPosition;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>CollateralBalanceOTCRebuildProcessorTests</code> ...
 *
 * @author Mary Anderson
 */

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CollateralBalanceOTCRebuildProcessorTests<S extends CollateralStrategyPosition> {

	@Resource
	private ReadOnlyDAO<CollateralBalance> collateralBalanceDAO;

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;

	private CollateralBalanceOTCRebuildProcessor<S> processor;


	@BeforeEach
	public void setup() {
		if (this.processor == null) {
			CollateralBalanceOTCRebuildProcessor<S> rebuildProcessor = new CollateralBalanceOTCRebuildProcessor<>();
			rebuildProcessor.setSystemColumnValueHandler(this.systemColumnValueHandler);
			this.processor = rebuildProcessor;
		}
	}


	@Test
	public void testCalculateCollateralCollateralCrossesZero() {
		OTCCollateralBalance testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(100);
		this.processor.setRoundedCollateralChangeValues(testBalance, new BigDecimal("50000"));

		assertValueEquals(testBalance, "Calculated Net Requirement", new BigDecimal("-804912.01"), testBalance.getCalculatedNetRequirement());
		assertValueEquals(testBalance, "Calculated Change Amount", new BigDecimal("-800000"), testBalance.getCalculatedChangeAmount());

		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("270000"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", new BigDecimal("-530000"), testBalance.getCounterpartyCollateralChangeAmount());


		testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(101);
		this.processor.setRoundedCollateralChangeValues(testBalance, new BigDecimal("50000"));

		assertValueEquals(testBalance, "Calculated Net Requirement", new BigDecimal("1520454.24"), testBalance.getCalculatedNetRequirement());
		assertValueEquals(testBalance, "Calculated Change Amount", new BigDecimal("1520454.24"), testBalance.getCalculatedChangeAmount());

		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("-1520454.24"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", BigDecimal.ZERO, testBalance.getCounterpartyCollateralChangeAmount());


		testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(102);
		this.processor.setRoundedCollateralChangeValues(testBalance, new BigDecimal("50000"));

		assertValueEquals(testBalance, "Calculated Net Requirement", new BigDecimal("805457.01"), testBalance.getCalculatedNetRequirement());
		assertValueEquals(testBalance, "Calculated Change Amount", new BigDecimal("800000"), testBalance.getCalculatedChangeAmount());

		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("-530545"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", new BigDecimal("269455"), testBalance.getCounterpartyCollateralChangeAmount());


		testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(104);
		this.processor.setRoundedCollateralChangeValues(testBalance, new BigDecimal("10000"));

		assertValueEquals(testBalance, "Calculated Net Requirement", new BigDecimal("-203187.00"), testBalance.getCalculatedNetRequirement());
		assertValueEquals(testBalance, "Calculated Change Amount", new BigDecimal("-8967.19"), testBalance.getCalculatedChangeAmount());

		assertValueEquals(testBalance, "Client Collateral Change Amount", BigDecimal.ZERO, testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", new BigDecimal("-8967.19"), testBalance.getCounterpartyCollateralChangeAmount());
	}


	@Test
	public void testCalculateCollateralCollateralRoundingTest() {
		OTCCollateralBalance testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(103);
		this.processor.setRoundedCollateralChangeValues(testBalance, new BigDecimal("10000"));

		assertValueEquals(testBalance, "Calculated Net Requirement", new BigDecimal("-26717.20"), testBalance.getCalculatedNetRequirement());
		assertValueEquals(testBalance, "Calculated Change Amount", new BigDecimal("-30000"), testBalance.getCalculatedChangeAmount());

		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("30000"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", BigDecimal.ZERO, testBalance.getCounterpartyCollateralChangeAmount());
	}


	private void assertValueEquals(CollateralBalance balance, String valueName, BigDecimal expected, BigDecimal actual) {
		Assertions.assertTrue(MathUtils.isEqual(expected, actual), "Balance ID " + balance.getId() + ": " + valueName + " [" + CoreMathUtils.formatNumberMoney(actual) + "] doesn't equal expected value of ["
				+ CoreMathUtils.formatNumberMoney(expected) + "]");
	}


	@Test
	public void testCalculateCollateralChangeAmountReturnAndPost() {
		OTCCollateralBalance testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(40);
		this.processor.setRoundedCollateralChangeValues(testBalance, BigDecimal.valueOf(100000));

		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("3300000"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", new BigDecimal("-11200000"), testBalance.getCounterpartyCollateralChangeAmount());

		testBalance = (OTCCollateralBalance) this.collateralBalanceDAO.findByPrimaryKey(41);
		this.processor.setRoundedCollateralChangeValues(testBalance, BigDecimal.valueOf(100000));


		assertValueEquals(testBalance, "Client Collateral Change Amount", new BigDecimal("-11200000"), testBalance.getCollateralChangeAmount());
		assertValueEquals(testBalance, "Counterparty Change Amount", new BigDecimal("3300000"), testBalance.getCounterpartyCollateralChangeAmount());
	}


	@Test
	public void testCalculateCollateralChangeAmount() {

		List<CollateralBalance> testList = getCollateralBalanceList();

		for (CollateralBalance b : CollectionUtils.getIterable(testList)) {
			BigDecimal clientChange = b.getCollateralChangeAmount();
			BigDecimal counterpartyChange = b.getCounterpartyCollateralChangeAmount();

			this.processor.setRoundedCollateralChangeValues(b, BigDecimal.ZERO);

			Assertions.assertTrue(MathUtils.isEqual(clientChange, b.getCollateralChangeAmount()), "Balance ID " + b.getId() + ": Client Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCollateralChangeAmount()) + "] doesn't equal expected value of ["
					+ CoreMathUtils.formatNumberMoney(clientChange) + "]");
			Assertions.assertTrue(MathUtils.isEqual(counterpartyChange, b.getCounterpartyCollateralChangeAmount()),
					"Balance ID " + b.getId() + ": Counterparty Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCounterpartyCollateralChangeAmount())
							+ "] doesn't equal expected value of [" + CoreMathUtils.formatNumberMoney(counterpartyChange) + "]");
		}
	}


	@Test
	public void testCalculateCollateralChangeAmountRoundingToNearestFiveThousand() {
		List<CollateralBalance> testList = getCollateralBalanceList();

		for (CollateralBalance b : CollectionUtils.getIterable(testList)) {
			BigDecimal clientChange = b.getCollateralChangeAmount();
			BigDecimal counterpartyChange = b.getCounterpartyCollateralChangeAmount();

			// Manually Change the Values to what we expect
			if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(490125))) {
				clientChange = BigDecimal.valueOf(490000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(996379))) {
				clientChange = BigDecimal.valueOf(995000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-634375))) {
				clientChange = BigDecimal.valueOf(-635000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(4000))) {
				clientChange = BigDecimal.valueOf(5000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-885483.61))) {
				clientChange = BigDecimal.valueOf(-885000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-479642.28))) {
				clientChange = BigDecimal.valueOf(-480000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-271000.00))) {
				clientChange = BigDecimal.valueOf(-270000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-10000.00))) {
				clientChange = BigDecimal.valueOf(-10000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-7983628.59))) {
				clientChange = BigDecimal.valueOf(-7985000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-9000.00))) {
				clientChange = BigDecimal.valueOf(-9000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-1470577.00))) {
				clientChange = BigDecimal.valueOf(-1470000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(5690298.37))) {
				clientChange = BigDecimal.valueOf(5690000);
			}

			if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(0.00))) {
				counterpartyChange = BigDecimal.valueOf(0.00);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(490125))) {
				counterpartyChange = BigDecimal.valueOf(490000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(4000))) {
				counterpartyChange = BigDecimal.valueOf(5000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(-407717))) {
				counterpartyChange = BigDecimal.valueOf(-410000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(1085934.92))) {
				counterpartyChange = BigDecimal.valueOf(1085000);
			}

			this.processor.setRoundedCollateralChangeValues(b, BigDecimal.valueOf(5000));

			Assertions.assertTrue(MathUtils.isEqual(clientChange, MathUtils.round(b.getCollateralChangeAmount(), 0)), "Balance ID " + b.getId() + ": Client Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCollateralChangeAmount()) + "] doesn't equal expected value of ["
					+ CoreMathUtils.formatNumberMoney(clientChange) + "]");
			Assertions.assertTrue(MathUtils.isEqual(counterpartyChange, MathUtils.round(b.getCounterpartyCollateralChangeAmount(), 0)),
					"Balance ID " + b.getId() + ": Counterparty Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCounterpartyCollateralChangeAmount())
							+ "] doesn't equal expected value of [" + CoreMathUtils.formatNumberMoney(counterpartyChange) + "]");
		}
	}


	@Test
	public void testCalculateCollateralChangeAmountRoundingToNearestTenThousand() {
		List<CollateralBalance> testList = getCollateralBalanceList();

		for (CollateralBalance b : CollectionUtils.getIterable(testList)) {
			BigDecimal clientChange = b.getCollateralChangeAmount();
			BigDecimal counterpartyChange = b.getCounterpartyCollateralChangeAmount();

			// Manually Change the Values to what we expect
			if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(490125))) {
				clientChange = BigDecimal.valueOf(490000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(996379))) {
				clientChange = BigDecimal.valueOf(1000000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-634375))) {
				clientChange = BigDecimal.valueOf(-630000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(4000))) {
				clientChange = BigDecimal.valueOf(0);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-885483.61))) {
				clientChange = BigDecimal.valueOf(-890000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-479642.28))) {
				clientChange = BigDecimal.valueOf(-480000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-271000.00))) {
				clientChange = BigDecimal.valueOf(-270000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-10000.00))) {
				clientChange = BigDecimal.valueOf(-10000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-7983628.59))) {
				clientChange = BigDecimal.valueOf(-7980000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-9000.00))) {
				clientChange = BigDecimal.valueOf(-9000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-1470577.00))) {
				clientChange = BigDecimal.valueOf(-1470000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(5690298.37))) {
				clientChange = BigDecimal.valueOf(5690000);
			}

			if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(0.00))) {
				counterpartyChange = BigDecimal.valueOf(0.00);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(490125))) {
				counterpartyChange = BigDecimal.valueOf(490000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(-407717))) {
				counterpartyChange = BigDecimal.valueOf(-410000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(4000))) {
				counterpartyChange = BigDecimal.valueOf(0);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(1085934.92))) {
				counterpartyChange = BigDecimal.valueOf(1090000);
			}

			this.processor.setRoundedCollateralChangeValues(b, BigDecimal.valueOf(10000));

			Assertions.assertTrue(MathUtils.isEqual(clientChange, MathUtils.round(b.getCollateralChangeAmount(), 0)), "Balance ID " + b.getId() + ": Client Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCollateralChangeAmount()) + "] doesn't equal expected value of ["
					+ CoreMathUtils.formatNumberMoney(clientChange) + "]");
			Assertions.assertTrue(MathUtils.isEqual(counterpartyChange, MathUtils.round(b.getCounterpartyCollateralChangeAmount(), 0)),
					"Balance ID " + b.getId() + ": Counterparty Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCounterpartyCollateralChangeAmount())
							+ "] doesn't equal expected value of [" + CoreMathUtils.formatNumberMoney(counterpartyChange) + "]");
		}
	}


	@Test
	public void testCalculateCollateralChangeAmountRoundingToNearestFiftyThousand() {
		List<CollateralBalance> testList = getCollateralBalanceList();

		for (CollateralBalance b : CollectionUtils.getIterable(testList)) {
			BigDecimal clientChange = b.getCollateralChangeAmount();
			BigDecimal counterpartyChange = b.getCounterpartyCollateralChangeAmount();

			// Manually Change the Values to what we expect
			if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(490125))) {
				clientChange = BigDecimal.valueOf(500000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(996379))) {
				clientChange = BigDecimal.valueOf(1000000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-634375))) {
				clientChange = BigDecimal.valueOf(-650000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(4000))) {
				clientChange = BigDecimal.valueOf(0);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-885483.61))) {
				clientChange = BigDecimal.valueOf(-900000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-479642.28))) {
				clientChange = BigDecimal.valueOf(-500000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-271000.00))) {
				clientChange = BigDecimal.valueOf(-250000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-7983628.59))) {
				clientChange = BigDecimal.valueOf(-8000000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-9000.00))) {
				clientChange = BigDecimal.valueOf(0);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(-1470577.00))) {
				clientChange = BigDecimal.valueOf(-1450000);
			}
			else if (MathUtils.isEqual(clientChange, BigDecimal.valueOf(5690298.37))) {
				clientChange = BigDecimal.valueOf(5710000);
			}

			if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(0.00))) {
				counterpartyChange = BigDecimal.valueOf(0.00);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(490125))) {
				counterpartyChange = BigDecimal.valueOf(500000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(-407717))) {
				counterpartyChange = BigDecimal.valueOf(-400000);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(4000))) {
				counterpartyChange = BigDecimal.valueOf(0);
			}
			else if (MathUtils.isEqual(counterpartyChange, BigDecimal.valueOf(1085934.92))) {
				counterpartyChange = BigDecimal.valueOf(1090000);
			}

			this.processor.setRoundedCollateralChangeValues(b, BigDecimal.valueOf(50000));

			Assertions.assertTrue(MathUtils.isEqual(clientChange, MathUtils.round(b.getCollateralChangeAmount(), 0)), "Balance ID " + b.getId() + ": Client Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCollateralChangeAmount()) + "] doesn't equal expected value of ["
					+ CoreMathUtils.formatNumberMoney(clientChange) + "]");
			Assertions.assertTrue(MathUtils.isEqual(counterpartyChange, MathUtils.round(b.getCounterpartyCollateralChangeAmount(), 0)),
					"Balance ID " + b.getId() + ": Counterparty Collateral Change Amount [" + CoreMathUtils.formatNumberMoney(b.getCounterpartyCollateralChangeAmount())
							+ "] doesn't equal expected value of [" + CoreMathUtils.formatNumberMoney(counterpartyChange) + "]");
		}
	}


	private List<CollateralBalance> getCollateralBalanceList() {
		List<CollateralBalance> result = new ArrayList<>();
		List<CollateralBalance> testList = this.collateralBalanceDAO.findAll();
		for (CollateralBalance balance : CollectionUtils.getIterable(testList)) {
			if (balance.getId() != 40 && balance.getId() != 41 && balance.getId() < 100) {
				result.add(balance);
			}
		}
		return result;
	}
}
