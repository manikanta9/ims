package com.clifton.collateral.balance;

import com.clifton.collateral.CollateralInMemoryDatabaseContext;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This tests the Collateral Options Margin rebuild process and validated expected required collateral
 *
 * @author stevenf
 */
@CollateralInMemoryDatabaseContext
public class CollateralBalanceOptionsMarginDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private CollateralBalanceRebuildService collateralBalanceRebuildService;

	@Resource
	private CollateralBalanceService collateralBalanceService;

	@Resource
	private CollateralService collateralService;


	////////////////////////////////////////////////////////////////////////////
	////////   Collateral Balance Pledged Transaction Command Methods   ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCollateralBalanceOptionsMargin() {
		for (String date : COLLATERAL_BALANCE_DATES) {
			processCollateralBalance(date);
		}
	}


	private void processCollateralBalance(String date) {
		Date balanceDate = DateUtils.toDate(date);
		//Rebuild collateral balance
		CollateralBalanceRebuildCommand rebuildCommand = CollateralBalanceRebuildCommand.ofSynchronousWithThrow();
		rebuildCommand.setBalanceDate(balanceDate);
		rebuildCommand.setCollateralType(this.collateralService.getCollateralTypeByName(CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN));
		this.collateralBalanceRebuildService.rebuildCollateralBalance(rebuildCommand);

		//Get CollateralBalance using date
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setBalanceDate(balanceDate);
		searchForm.setCollateralTypeName(CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN);
		CollateralBalance collateralBalance = CollectionUtils.getFirstElementStrict(this.collateralBalanceService.getCollateralBalanceList(searchForm));

		//Validate posted collateral
		Assertions.assertTrue(MathUtils.isEqual(collateralBalance.getCollateralRequirement(), EXPECTED_REQUIRED_COLLATERAL_MAP.get(date)), "Expected " + EXPECTED_REQUIRED_COLLATERAL_MAP.get(date) + " required collateral for balance date " + date + " but found " + collateralBalance.getCollateralRequirement());
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                      Test Data                      //////////////
	////////////////////////////////////////////////////////////////////////////////

	public static final List<String> COLLATERAL_BALANCE_DATES =
			Arrays.asList("08/07/2017", "08/08/2017", "08/09/2017");

	private static final Map<String, BigDecimal> EXPECTED_REQUIRED_COLLATERAL_MAP;


	static {
		Map<String, BigDecimal> map = new HashMap<>();
		map.put("08/07/2017", new BigDecimal("1933209.75"));
		map.put("08/08/2017", new BigDecimal("1896885.00"));
		map.put("08/09/2017", new BigDecimal("1881372.50"));
		EXPECTED_REQUIRED_COLLATERAL_MAP = Collections.unmodifiableMap(map);
	}
}
