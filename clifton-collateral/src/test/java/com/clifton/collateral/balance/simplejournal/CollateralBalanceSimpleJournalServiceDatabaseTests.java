package com.clifton.collateral.balance.simplejournal;


import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalService;
import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalSearchForm;
import com.clifton.collateral.CollateralInMemoryDatabaseContext;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@CollateralInMemoryDatabaseContext
public class CollateralBalanceSimpleJournalServiceDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	CollateralBalanceSimpleJournalService collateralBalanceSimpleJournalService;

	@Resource
	AccountingSimpleJournalService accountingSimpleJournalService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCollateralBalanceSimpleJournalGenerateCashTransactions_TwoBuyTradesMultipleFills() {

		Date tradeDate = DateUtils.toDate("05/03/2017");
		Status status = this.collateralBalanceSimpleJournalService.generateCashCollateralSimpleJournalEntries(tradeDate);
		Assertions.assertEquals("Processed 2 trade(s) out of 2.", status.getMessage());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());

		AccountingSimpleJournalSearchForm searchForm = new AccountingSimpleJournalSearchForm();
		searchForm.setJournalTypeId((short) 4);
		searchForm.setClientInvestmentAccountId(2481);
		searchForm.setTransactionDate(DateUtils.toDate("05/04/2017"));

		List<AccountingSimpleJournal> simpleJournalList = this.accountingSimpleJournalService.getAccountingSimpleJournalList(searchForm);
		Assertions.assertNotNull(simpleJournalList);
		Assertions.assertEquals(1, simpleJournalList.size());

		AccountingSimpleJournal simpleJournal = simpleJournalList.get(0);
		Assertions.assertEquals("{id=1, label=Cash Collateral Distribution of 5148.50000000000000000000 for 124100: Gainesville}", simpleJournal.toString());
	}


	@Test
	public void testBuyTradeGenerateSimpleJournals() {

		Date tradeDate = DateUtils.toDate("04/01/2020");
		Status status = this.collateralBalanceSimpleJournalService.generateCashCollateralSimpleJournalEntries(tradeDate);
		Assertions.assertEquals("Processed 1 trade(s) out of 1.", status.getMessage());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());

		AccountingSimpleJournalSearchForm searchForm = new AccountingSimpleJournalSearchForm();
		searchForm.setClientInvestmentAccount("456050");
		searchForm.setTransactionDate(DateUtils.toDate("04/02/2020"));
		List<AccountingSimpleJournal> simpleJournalList = this.accountingSimpleJournalService.getAccountingSimpleJournalList(searchForm);

		Assertions.assertEquals(2, simpleJournalList.size());

		for (AccountingSimpleJournal simpleJournal : simpleJournalList) {
			if ("Cash Collateral Contribution".equals(simpleJournal.getJournalType().getName())) {
				Assertions.assertTrue(MathUtils.isEqual(simpleJournal.getAmount(), 285_000), "expected 285,000 but was " + simpleJournal.getAmount());
			}
			else if ("Cash Transfer".equals(simpleJournal.getJournalType().getName())) {
				Assertions.assertTrue(MathUtils.isEqual(simpleJournal.getAmount(), 3362.77d), "expected 3,362.77 but was " + simpleJournal.getAmount());
			}
			else {
				throw new RuntimeException("Unexpected journal type found.");
			}
		}
	}


	@Test
	public void testSellTradeGenerateSimpleJournals() {

		Date tradeDate = DateUtils.toDate("05/15/2020");
		Status status = this.collateralBalanceSimpleJournalService.generateCashCollateralSimpleJournalEntries(tradeDate);

		Assertions.assertEquals("Processed 1 trade(s) out of 1.", status.getMessage());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());

		AccountingSimpleJournalSearchForm searchForm = new AccountingSimpleJournalSearchForm();
		searchForm.setClientInvestmentAccount("227003");
		//15th is a friday
		searchForm.setTransactionDate(DateUtils.toDate("05/18/2020"));
		List<AccountingSimpleJournal> simpleJournalList = this.accountingSimpleJournalService.getAccountingSimpleJournalList(searchForm);

		Assertions.assertEquals(2, simpleJournalList.size());

		for (AccountingSimpleJournal simpleJournal : simpleJournalList) {
			if ("Cash Collateral Distribution".equals(simpleJournal.getJournalType().getName())) {
				Assertions.assertTrue(MathUtils.isEqual(simpleJournal.getAmount(), 1_474_433.42d), "expected 1,474,433.42 but was " + simpleJournal.getAmount());
			}
			else if ("Cash Transfer".equals(simpleJournal.getJournalType().getName())) {
				Assertions.assertTrue(MathUtils.isEqual(simpleJournal.getAmount(), 2_869.66d), "expected 2,869.66 but was " + simpleJournal.getAmount());
			}
			else {
				throw new RuntimeException("Unexpected journal type found.");
			}
		}
	}
}
