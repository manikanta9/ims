package com.clifton.collateral.balance.workflow;

import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.collateral.balance.rebuild.processor.CollateralBalanceFuturesRebuildProcessor;
import com.clifton.collateral.balance.workflow.futures.CollateralBalanceFuturesCollateralWorkflowUtils;
import com.clifton.collateral.balance.workflow.futures.CollateralBalancePledgedTransactionsFuturesWorkflowContext;
import com.clifton.collateral.balance.workflow.futures.FuturesCollateralPositionPledge;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * @author jonathanr
 */
public final class SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder {

	private BigDecimal minimumCashTransferAmount;
	private BigDecimal cashTransferIncrementAmount;
	private BigDecimal targetCollateralBufferPercent;
	private BigDecimal minimumPositionTransferAmount;
	private BigDecimal minimumTransferQuantity;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount brokerInvestmentAccount;
	private InvestmentAccount collateralInvestmentAccount;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral;
	private Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianHeldCollateral;
	private BigDecimal requiredCollateralAmount;


	private SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder() {
	}


	public static SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder aCollateralBalancePledgedTransactionsFuturesWorkflowContext() {
		return new SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder();
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withMinimumCashTransferAmount(BigDecimal minimumCashTransferAmount) {
		this.minimumCashTransferAmount = minimumCashTransferAmount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withCashTransferIncrementAmount(BigDecimal cashTransferIncrementAmount) {
		this.cashTransferIncrementAmount = cashTransferIncrementAmount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withTargetCollateralBufferPercent(BigDecimal targetCollateralBufferPercent) {
		this.targetCollateralBufferPercent = targetCollateralBufferPercent;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withMinimumPositionTransferAmount(BigDecimal minimumPositionTransferAmount) {
		this.minimumPositionTransferAmount = minimumPositionTransferAmount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withMinimumTransferQuantity(BigDecimal minimumTransferQuantity) {
		this.minimumTransferQuantity = minimumTransferQuantity;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withClientInvestmentAccount(InvestmentAccount investmentAccount) {
		this.clientInvestmentAccount = investmentAccount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withBrokerInvestmentAccount(InvestmentAccount investmentAccount) {
		this.brokerInvestmentAccount = investmentAccount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withCustodianInvestmentAccount(InvestmentAccount investmentAccount) {
		this.collateralInvestmentAccount = investmentAccount;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withBrokerHeldCollateral(Map<InvestmentSecurity, FuturesCollateralPositionPledge> brokerHeldCollateral) {
		this.brokerHeldCollateral = brokerHeldCollateral;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withCustodianHeldCollateral(Map<InvestmentSecurity, FuturesCollateralPositionPledge> custodianCollateral) {
		this.custodianHeldCollateral = custodianCollateral;
		return this;
	}


	public SpyCollateralBalancePledgedTransactionsFuturesWorkflowContextBuilder withRequiredCollateralAmount(BigDecimal requiredCollateralAmount) {
		this.requiredCollateralAmount = requiredCollateralAmount;
		return this;
	}


	public CollateralBalancePledgedTransactionsFuturesWorkflowContext build() {

		//processor mock
		CollateralBalanceFuturesRebuildProcessor processor = Mockito.mock(CollateralBalanceFuturesRebuildProcessor.class);
		Mockito.when(processor.getTargetCollateralBufferPercent()).thenReturn(this.targetCollateralBufferPercent);
		Mockito.when(processor.getMinimumCashTransferAmount()).thenReturn(this.minimumCashTransferAmount);
		Mockito.when(processor.getCashTransferIncrementAmount()).thenReturn(this.cashTransferIncrementAmount);
		Mockito.when(processor.getMinimumPositionTransferAmount()).thenReturn(this.minimumPositionTransferAmount);
		Mockito.when(processor.getMinimumTransferQuantity()).thenReturn(this.minimumTransferQuantity);
		Mockito.when(processor.isManualAccount()).thenReturn(false);


		//rebuild Service mock
		CollateralBalanceRebuildService collateralBalanceRebuildService = Mockito.mock(CollateralBalanceRebuildService.class);
		Mockito.when(collateralBalanceRebuildService.getCollateralBalanceRebuildProcessor(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(processor);

		//Collateral Balance mock

		CollateralBalance collateralBalance = Mockito.spy(new CollateralBalance());
		// Mockito.doReturn(this.clientInvestmentAccount).when(collateralBalance.getClientInvestmentAccount());
		// Mockito.doReturn(this.brokerInvestmentAccount).when(collateralBalance.getHoldingInvestmentAccount());
		// Mockito.doReturn(this.collateralInvestmentAccount).when(collateralBalance.getCollateralInvestmentAccount());
		// Mockito.doReturn(this.requiredCollateralAmount).when(collateralBalance.getExternalCollateralRequirement());
		// Mockito.doReturn(MathUtils.subtract(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(this.brokerHeldCollateral), this.requiredCollateralAmount)).when(collateralBalance.getExternalExcessCollateral());
		// Mockito.doReturn(collateralBalance.getExternalExcessCollateralPercentage()).when(CoreMathUtils.getPercentValue(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(this.brokerHeldCollateral), this.requiredCollateralAmount));
		// Mockito.doReturn(new Date()).when(collateralBalance.getBookingDate());
		Mockito.when(collateralBalance.getClientInvestmentAccount()).thenReturn(this.clientInvestmentAccount);
		Mockito.when(collateralBalance.getHoldingInvestmentAccount()).thenReturn(this.brokerInvestmentAccount);
		Mockito.when(collateralBalance.getCollateralInvestmentAccount()).thenReturn(this.collateralInvestmentAccount);
		Mockito.when(collateralBalance.getExternalCollateralRequirement()).thenReturn(this.requiredCollateralAmount);
		Mockito.when(collateralBalance.getExternalExcessCollateral()).thenReturn(MathUtils.subtract(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(this.brokerHeldCollateral), this.requiredCollateralAmount));
		Mockito.when(collateralBalance.getExternalExcessCollateralPercentage()).thenReturn(CoreMathUtils.getPercentValue(CollateralBalanceFuturesCollateralWorkflowUtils.getTotalPositionCollateralValueIncludingCash(this.brokerHeldCollateral), this.requiredCollateralAmount));
		Mockito.when(collateralBalance.getBookingDate()).thenReturn(new Date());

		// Context mocks
		CollateralBalancePledgedTransactionsFuturesWorkflowContext collateralBalancePledgedTransactionsFuturesWorkflowContext = new CollateralBalancePledgedTransactionsFuturesWorkflowContext(collateralBalance, collateralBalanceRebuildService);
		collateralBalancePledgedTransactionsFuturesWorkflowContext.setBrokerHeldCollateral(this.brokerHeldCollateral);
		collateralBalancePledgedTransactionsFuturesWorkflowContext.setCustodianCollateral(this.custodianHeldCollateral);

		return Mockito.spy(collateralBalancePledgedTransactionsFuturesWorkflowContext);
	}
}
