package com.clifton.collateral.balance.workflow;

import com.clifton.collateral.balance.workflow.futures.FuturesCollateralPositionPledge;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author jonathanr
 */
public class FuturesCollateralTestSetupUtil {

	private int securityIdCount = 0;
	private int cashHierarchyId = -1;
	public static final String CASH_SECURITY_NAME = "~$CASH$~";
	private final Map<String, Integer> nameIdMap = new HashMap<>();
	private final Map<Integer, Integer> securityIdToHierarchyIdMap = new HashMap<>();
	private final Map<Integer, PledgeHolderObject> brokerPositionsHolderMap = new HashMap<>();
	private final Map<Integer, PledgeHolderObject> custodianPositionsHolderMap = new HashMap<>();
	private final Map<InvestmentSecurity, FuturesCollateralPositionPledge> builtBrokerMap = new HashMap<>();
	private final Map<InvestmentSecurity, FuturesCollateralPositionPledge> builtCustodianMap = new HashMap<>();
	private final Map<Integer, InvestmentInstrumentHierarchy> mockedHierarchyMap = new HashMap<>();
	private final Map<Integer, InvestmentSecurity> idMockedInvestmentSecurityMap = new HashMap<>();
	private final List<String> postPriorityList = new ArrayList<>();

	private boolean controlledState = true;


	public void addBrokerHeldCash(int cashValue) {
		addCashPosition(new BigDecimal(cashValue), true);
	}


	public void addBrokerHeldCash(BigDecimal cashValue) {
		addCashPosition(cashValue, true);
	}


	public void addCustodianHeldCash(int cashValue) {
		addCustodianHeldCash(new BigDecimal(cashValue));
	}


	public void addCustodianHeldCash(BigDecimal cashValue) {
		addCashPosition(cashValue, false);
	}


	private void addCashPosition(BigDecimal cashValue, boolean broker) {
		addPosition(CASH_SECURITY_NAME, cashValue, cashValue, BigDecimal.ZERO, broker);
	}


	public void addBrokerPosition(String name, int quantity, int totalMarketValue) {
		addBrokerPosition(name, quantity, totalMarketValue, BigDecimal.ZERO);
	}


	public void addBrokerPosition(String name, int quantity, int totalMarketValue, BigDecimal haircutValue) {
		addBrokerPosition(name, new BigDecimal(quantity), new BigDecimal(totalMarketValue), haircutValue);
	}


	public void addBrokerPosition(String name, int quantity, int totalMarketValue, String haircutValue) {
		addBrokerPosition(name, new BigDecimal(quantity), new BigDecimal(totalMarketValue), new BigDecimal(haircutValue));
	}


	public void addBrokerPosition(String name, BigDecimal quantity, BigDecimal totalMarketValue, BigDecimal haircutValue) {
		addPosition(name, quantity, totalMarketValue, haircutValue, true);
	}


	public void addCustodianPosition(String name, int quantity, int totalMarketValue) {
		addCustodianPosition(name, quantity, totalMarketValue, BigDecimal.ZERO);
	}


	public void addCustodianPosition(String name, int quantity, int totalMarketValue, BigDecimal haircutValue) {
		addCustodianPosition(name, new BigDecimal(quantity), new BigDecimal(totalMarketValue), haircutValue);
	}


	public void addCustodianPosition(String name, int quantity, int totalMarketValue, String haircutValue) {
		addCustodianPosition(name, new BigDecimal(quantity), new BigDecimal(totalMarketValue), new BigDecimal(haircutValue));
	}


	public void addCustodianPosition(String name, BigDecimal quantity, BigDecimal totalMarketValue, BigDecimal haircutValue) {
		addPosition(name, quantity, totalMarketValue, haircutValue, false);
	}


	private void addPosition(String name, BigDecimal quantity, BigDecimal totalMarketValue, BigDecimal haircutValue, boolean broker) {

		Integer securityId;
		if (!this.nameIdMap.containsKey(name)) {
			this.postPriorityList.add(name);
			securityId = getNewSecurityId();
			this.nameIdMap.put(name, securityId);
			if (CASH_SECURITY_NAME.equals(name)) {
				this.cashHierarchyId = securityId;
				this.securityIdToHierarchyIdMap.put(securityId, this.cashHierarchyId);
			}
			else {
				this.securityIdToHierarchyIdMap.put(securityId, securityId);
			}
		}
		else {
			securityId = this.nameIdMap.get(name);
		}

		PledgeHolderObject pledge = new PledgeHolderObject(quantity, totalMarketValue, haircutValue, new Date());
		if (broker) {
			this.brokerPositionsHolderMap.put(securityId, pledge);
		}
		else {
			this.custodianPositionsHolderMap.put(securityId, pledge);
		}
	}


	public void addCommonHierarchyAncestor(String... securityName) {
		int newHierarchyId = getNewSecurityId();
		for (String s : securityName) {
			this.securityIdToHierarchyIdMap.put(this.nameIdMap.get(s), newHierarchyId);
		}
	}


	public void overrideDefaultMaturityDate(String securityName, Date date) {
		PledgeHolderObject brokerPosition = this.brokerPositionsHolderMap.get(this.nameIdMap.get(securityName));
		PledgeHolderObject custodianPosition = this.custodianPositionsHolderMap.get(this.nameIdMap.get(securityName));
		if (brokerPosition != null) {
			brokerPosition.date = date;
		}
		if (custodianPosition != null) {
			custodianPosition.date = date;
		}
	}


	public void generateMockedHoldings() {
		if (!this.controlledState) {
			throw new RuntimeException("generateMockHoldings can only be called once create a new object");
		}
		this.controlledState = false;
		Map<Integer, Set<Integer>> hierarchyMap = new HashMap<>();
		for (String name : CollectionUtils.getIterable(this.nameIdMap.keySet())) {

			Integer id = this.nameIdMap.get(name);
			Integer hierarchyId = this.securityIdToHierarchyIdMap.get(id);
			if (hierarchyMap.containsKey(hierarchyId)) {
				hierarchyMap.get(hierarchyId).add(id);
			}
			else {
				Set<Integer> securityIdSet = new HashSet<>();
				securityIdSet.add(id);
				hierarchyMap.put(hierarchyId, securityIdSet);
			}
		}
		for (Integer hierarchyId : hierarchyMap.keySet()) {
			InvestmentInstrumentHierarchy hierarchy;
			if (hierarchyId == this.cashHierarchyId) {
				hierarchy = CollateralWorkflowFuturesTestUtils.mockInvestmentInstrumentHierarchy(true);
			}
			else {
				hierarchy = CollateralWorkflowFuturesTestUtils.mockInvestmentInstrumentHierarchy(false);
			}

			for (Integer id : hierarchyMap.get(hierarchyId)) {
				this.mockedHierarchyMap.put(id, hierarchy);
				PledgeHolderObject brokerPosition = this.brokerPositionsHolderMap.get(id);
				PledgeHolderObject custodianPosition = this.custodianPositionsHolderMap.get(id);

				InvestmentSecurity mockedSecurity = CollateralWorkflowFuturesTestUtils.mockInvestmentSecurity(id, hierarchy, brokerPosition != null ? brokerPosition.date : custodianPosition.date);
				if (brokerPosition != null) {
					this.builtBrokerMap.put(mockedSecurity, new FuturesCollateralPositionPledge(mockedSecurity, brokerPosition.quantity, brokerPosition.totalMarketValue, brokerPosition.hairCutValue));
				}
				if (custodianPosition != null) {
					this.builtCustodianMap.put(mockedSecurity, new FuturesCollateralPositionPledge(mockedSecurity, custodianPosition.quantity, custodianPosition.totalMarketValue, custodianPosition.hairCutValue));
				}
				this.idMockedInvestmentSecurityMap.put(id, mockedSecurity);
			}
		}
	}


	public void setUpHierarchyPriority(String... securityName) {
		this.postPriorityList.addAll(Arrays.asList(securityName));
	}


	private int getNewSecurityId() {
		return this.securityIdCount++;
	}


	public InvestmentSecurity getMockedSecurityByName(String name) {
		return this.getIdMockedInvestmentSecurityMap().get(this.nameIdMap.get(name));
	}


	private class PledgeHolderObject {

		public BigDecimal quantity;
		public BigDecimal totalMarketValue;
		public BigDecimal hairCutValue;
		public Date date;


		public PledgeHolderObject(BigDecimal quantity, BigDecimal totalMarketValue, BigDecimal hairCutValue, Date date) {
			this.quantity = quantity;
			this.totalMarketValue = totalMarketValue;
			this.hairCutValue = hairCutValue;
			this.date = date;
		}
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getBuiltBrokerMap() {
		if (this.controlledState) {
			throw new RuntimeException("you must call generateMockedHoldings before calling this method");
		}
		return this.builtBrokerMap;
	}


	public Map<InvestmentSecurity, FuturesCollateralPositionPledge> getBuiltCustodianMap() {
		if (this.controlledState) {
			throw new RuntimeException("you must call generateMockedHoldings before calling this method");
		}
		return this.builtCustodianMap;
	}


	public List<InvestmentInstrumentHierarchy> getMockedHierarchyList() {
		if (this.controlledState) {
			throw new RuntimeException("you must call generateMockedHoldings before calling this method");
		}
		ArrayList<InvestmentInstrumentHierarchy> orderedHierarchyList = new ArrayList<>();

		for (String priorityName : CollectionUtils.getIterable(this.postPriorityList)) {
			InvestmentInstrumentHierarchy hierarchy = this.mockedHierarchyMap.get(this.nameIdMap.get(priorityName));
			if (!orderedHierarchyList.contains(hierarchy)) {
				orderedHierarchyList.add(hierarchy);
			}
		}
		return orderedHierarchyList;
	}


	public Map<Integer, InvestmentSecurity> getIdMockedInvestmentSecurityMap() {
		if (this.controlledState) {
			throw new RuntimeException("you must call generateMockedHoldings before calling this method");
		}
		return this.idMockedInvestmentSecurityMap;
	}
}
