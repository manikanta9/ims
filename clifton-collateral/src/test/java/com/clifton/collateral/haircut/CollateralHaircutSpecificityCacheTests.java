package com.clifton.collateral.haircut;

import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.haircut.cache.CollateralHaircutDefinitionSpecificityCacheImpl;
import com.clifton.collateral.haircut.cache.CollateralHaircutDefinitionTargetHolder;
import com.clifton.core.cache.specificity.SpecificityTestObjectFactory;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class CollateralHaircutSpecificityCacheTests {

	private static List<CollateralHaircutDefinition> haircutDefinitionList;

	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;

	private CollateralHaircutDefinitionSpecificityCacheImpl cache;

	@Mock
	private DaoLocator daoLocator;

	@Mock
	private XmlReadOnlyDAO<CollateralHaircutDefinition> collateralHaircutDefinitionDAO;

	private static final CollateralHaircutDefinition HAIRCUT_DEFINITION_FUTURES;
	private static final CollateralHaircutDefinition HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND;
	private static final CollateralHaircutDefinition HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO;
	private static final CollateralHaircut HAIRCUT_0_1;
	private static final CollateralHaircut HAIRCUT_1_2;
	private static final CollateralHaircut HAIRCUT_2_5;
	private static final CollateralHaircut HAIRCUT_5_10;
	private static final CollateralHaircut HAIRCUT_10_30;

	private static final BusinessCompany HOLDING_COMPANY_BNP;
	private static final InvestmentAccount HOLDING_ACCOUNT_WARF;

	private static final InvestmentInstrumentHierarchy HIERARCHY_FUTURES;
	private static final InvestmentInstrumentHierarchy HIERARCHY_BONDS_CENT_GOV;
	private static final InvestmentInstrumentHierarchy HIERARCHY_BONDS_GOV_SPO;

	private static final InvestmentInstrument INSTRUMENT_FUTURES;
	private static final InvestmentInstrument INSTRUMENT_BONDS_CENT_GOV;
	private static final InvestmentInstrument INSTRUMENT_BONDS_GOV_SPO;

	private static final InvestmentType INVESTMENT_TYPE_BONDS;
	private static final InvestmentType INVESTMENT_TYPE_FUTURES;

	private static final InvestmentTypeSubType INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS;
	private static final InvestmentTypeSubType INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS;
	private static final InvestmentTypeSubType2 INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT;


	static {
		HOLDING_ACCOUNT_WARF = new InvestmentAccount();
		HOLDING_ACCOUNT_WARF.setId(0);
		HOLDING_ACCOUNT_WARF.setName("WARF-BNPP:WARF-ORC-BNPP");

		HOLDING_COMPANY_BNP = new BusinessCompany();
		HOLDING_COMPANY_BNP.setId(0);
		HOLDING_COMPANY_BNP.setName("BNP Paribas Securities Corp.");

		INVESTMENT_TYPE_BONDS = new InvestmentType();
		INVESTMENT_TYPE_BONDS.setId((short) 0);
		INVESTMENT_TYPE_BONDS.setName("Bonds");

		INVESTMENT_TYPE_FUTURES = new InvestmentType();
		INVESTMENT_TYPE_FUTURES.setId((short) 0);
		INVESTMENT_TYPE_FUTURES.setName("Futures");

		INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS = new InvestmentTypeSubType();
		INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS.setId((short) 0);
		INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS.setName("Government Bonds");
		INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS = new InvestmentTypeSubType();
		INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS.setId((short) 1);
		INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS.setName("Central Government Bonds");

		INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT = new InvestmentTypeSubType2();
		INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT.setId((short) 0);
		INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT.setName("Government Agency and Sponsored Entities");

		HIERARCHY_FUTURES = new InvestmentInstrumentHierarchy();
		HIERARCHY_FUTURES.setInvestmentType(INVESTMENT_TYPE_FUTURES);
		HIERARCHY_BONDS_CENT_GOV = new InvestmentInstrumentHierarchy();
		HIERARCHY_BONDS_CENT_GOV.setInvestmentType(INVESTMENT_TYPE_BONDS);
		HIERARCHY_BONDS_CENT_GOV.setInvestmentTypeSubType(INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS);
		HIERARCHY_BONDS_GOV_SPO = new InvestmentInstrumentHierarchy();
		HIERARCHY_BONDS_GOV_SPO.setInvestmentType(INVESTMENT_TYPE_BONDS);
		HIERARCHY_BONDS_GOV_SPO.setInvestmentTypeSubType(INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS);
		HIERARCHY_BONDS_GOV_SPO.setInvestmentTypeSubType2(INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT);

		INSTRUMENT_FUTURES = new InvestmentInstrument();
		INSTRUMENT_FUTURES.setHierarchy(HIERARCHY_FUTURES);
		INSTRUMENT_BONDS_CENT_GOV = new InvestmentInstrument();
		INSTRUMENT_BONDS_CENT_GOV.setHierarchy(HIERARCHY_BONDS_CENT_GOV);
		INSTRUMENT_BONDS_GOV_SPO = new InvestmentInstrument();
		INSTRUMENT_BONDS_GOV_SPO.setHierarchy(HIERARCHY_BONDS_GOV_SPO);

		HAIRCUT_0_1 = new CollateralHaircut();
		HAIRCUT_0_1.setStartYearsToMaturity(0);
		HAIRCUT_0_1.setEndYearsToMaturity(1);
		HAIRCUT_0_1.setHaircutPercent(BigDecimal.valueOf(0));
		HAIRCUT_0_1.setClientHaircut(true);
		HAIRCUT_0_1.setCounterpartyHaircut(true);

		HAIRCUT_1_2 = new CollateralHaircut();
		HAIRCUT_1_2.setStartYearsToMaturity(1);
		HAIRCUT_1_2.setEndYearsToMaturity(2);
		HAIRCUT_1_2.setHaircutPercent(BigDecimal.valueOf(.02));
		HAIRCUT_1_2.setClientHaircut(true);
		HAIRCUT_1_2.setCounterpartyHaircut(true);

		HAIRCUT_2_5 = new CollateralHaircut();
		HAIRCUT_2_5.setStartYearsToMaturity(2);
		HAIRCUT_2_5.setEndYearsToMaturity(5);
		HAIRCUT_2_5.setHaircutPercent(BigDecimal.valueOf(.03));
		HAIRCUT_2_5.setClientHaircut(true);
		HAIRCUT_2_5.setCounterpartyHaircut(true);

		HAIRCUT_5_10 = new CollateralHaircut();
		HAIRCUT_5_10.setStartYearsToMaturity(5);
		HAIRCUT_5_10.setEndYearsToMaturity(10);
		HAIRCUT_5_10.setHaircutPercent(BigDecimal.valueOf(.05));
		HAIRCUT_5_10.setClientHaircut(true);
		HAIRCUT_5_10.setCounterpartyHaircut(true);

		HAIRCUT_10_30 = new CollateralHaircut();
		HAIRCUT_10_30.setStartYearsToMaturity(10);
		HAIRCUT_10_30.setEndYearsToMaturity(30);
		HAIRCUT_10_30.setHaircutPercent(BigDecimal.valueOf(.08));
		HAIRCUT_10_30.setClientHaircut(true);
		HAIRCUT_10_30.setCounterpartyHaircut(true);

		List<CollateralHaircut> forwardsCollateralHaircutList = new ArrayList<>();
		forwardsCollateralHaircutList.add(HAIRCUT_0_1);
		forwardsCollateralHaircutList.add(HAIRCUT_1_2);

		HAIRCUT_DEFINITION_FUTURES = new CollateralHaircutDefinition();
		HAIRCUT_DEFINITION_FUTURES.setId(0);
		HAIRCUT_DEFINITION_FUTURES.setHoldingAccount(HOLDING_ACCOUNT_WARF);
		HAIRCUT_DEFINITION_FUTURES.setHoldingCompany(HOLDING_COMPANY_BNP);
		HAIRCUT_DEFINITION_FUTURES.setInvestmentType(INVESTMENT_TYPE_FUTURES);
		HAIRCUT_DEFINITION_FUTURES.setCollateralHaircutList(forwardsCollateralHaircutList);

		List<CollateralHaircut> centGovBondCollateralHaircutList = new ArrayList<>();
		centGovBondCollateralHaircutList.add(HAIRCUT_0_1);
		centGovBondCollateralHaircutList.add(HAIRCUT_1_2);
		centGovBondCollateralHaircutList.add(HAIRCUT_2_5);
		centGovBondCollateralHaircutList.add(HAIRCUT_5_10);

		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND = new CollateralHaircutDefinition();
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setId(1);
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setHoldingAccount(HOLDING_ACCOUNT_WARF);
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setHoldingCompany(HOLDING_COMPANY_BNP);
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setInvestmentType(INVESTMENT_TYPE_BONDS);
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setInvestmentTypeSubType(INVESTMENT_TYPE_SUB_TYPE_CENT_GOV_BONDS);
		HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND.setCollateralHaircutList(centGovBondCollateralHaircutList);

		List<CollateralHaircut> spoCentGovBondCollateralHaircutList = new ArrayList<>();
		spoCentGovBondCollateralHaircutList.add(HAIRCUT_0_1);
		spoCentGovBondCollateralHaircutList.add(HAIRCUT_1_2);
		spoCentGovBondCollateralHaircutList.add(HAIRCUT_5_10);
		spoCentGovBondCollateralHaircutList.add(HAIRCUT_10_30);

		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO = new CollateralHaircutDefinition();
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setId(2);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setHoldingAccount(HOLDING_ACCOUNT_WARF);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setHoldingCompany(HOLDING_COMPANY_BNP);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setInvestmentType(INVESTMENT_TYPE_BONDS);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setInvestmentTypeSubType(INVESTMENT_TYPE_SUB_TYPE_GOV_BONDS);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setInvestmentTypeSubType2(INVESTMENT_TYPE_SUB_TYPE_2_GOV_AGENCY_SPO_ENT);
		HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO.setCollateralHaircutList(spoCentGovBondCollateralHaircutList);

		haircutDefinitionList = new ArrayList<>();
		haircutDefinitionList.add(HAIRCUT_DEFINITION_FUTURES);
		haircutDefinitionList.add(HAIRCUT_DEFINITION_BOND_CENT_GOV_BOND);
		haircutDefinitionList.add(HAIRCUT_DEFINITION_BOND_GOV_BOND_SPO);
	}


	@SuppressWarnings("unchecked")
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		//init cache
		this.cache = new CollateralHaircutDefinitionSpecificityCacheImpl();

		//init definition service and set the cache.
		this.collateralHaircutDefinitionService = Mockito.spy(new CollateralHaircutDefinitionServiceImpl());
		((CollateralHaircutDefinitionServiceImpl) this.collateralHaircutDefinitionService).setCollateralHaircutDefinitionSpecificityCache(this.cache);

		//init the rest of the necessary cache objects including setting definition service to mocked one from above.
		this.cache.setCollateralHaircutDefinitionService(this.collateralHaircutDefinitionService);
		this.cache.setDaoLocator(this.daoLocator);
		this.cache.setCacheHandler(SpecificityTestObjectFactory.getCacheHandlerForSpecificityCache(CollateralHaircutDefinitionTargetHolder.class));

		//Mock specific services to return mocked list of definitions.
		Mockito.doReturn(this.collateralHaircutDefinitionDAO).when(this.daoLocator).locate(ArgumentMatchers.any(Class.class));
		Mockito.doReturn(haircutDefinitionList).when(this.collateralHaircutDefinitionService).getCollateralHaircutDefinitionList(ArgumentMatchers.any());
		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			int id = (Integer) args[0];
			for (CollateralHaircutDefinition haircutDefinition : haircutDefinitionList) {
				if (id == haircutDefinition.getId()) {
					return haircutDefinition;
				}
			}
			return null;
		}).when(this.collateralHaircutDefinitionService).getCollateralHaircutDefinition(ArgumentMatchers.anyInt());
	}


	@Test
	public void testGetMostSpecificResult() {
		InvestmentSecurity security = new InvestmentSecurity();
		security.setSymbol("FUTURE");
		security.setInstrument(INSTRUMENT_FUTURES);

		security.setEndDate(DateUtils.addDays(new Date(), 250));
		CollateralHaircut collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_0_1, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_1_2, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 1000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertNull(collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 2500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertNull(collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 5000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertNull(collateralHaircut);


		security.setSymbol("CENT_GOV");
		security.setInstrument(INSTRUMENT_BONDS_CENT_GOV);

		security.setEndDate(DateUtils.addDays(new Date(), 250));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_0_1, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_1_2, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 1000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_2_5, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 2500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_5_10, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 5000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertNull(collateralHaircut);


		security.setSymbol("GOV_SPO");
		security.setInstrument(INSTRUMENT_BONDS_GOV_SPO);

		security.setEndDate(DateUtils.addDays(new Date(), 250));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_0_1, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_1_2, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 1000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertNull(collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 2500));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_5_10, collateralHaircut);
		security.setEndDate(DateUtils.addDays(new Date(), 5000));
		collateralHaircut = this.collateralHaircutDefinitionService.getCollateralHaircut(HOLDING_ACCOUNT_WARF, security, true, new Date());
		Assertions.assertEquals(HAIRCUT_10_30, collateralHaircut);
	}
}
