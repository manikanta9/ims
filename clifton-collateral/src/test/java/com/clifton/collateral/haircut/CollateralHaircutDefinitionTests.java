package com.clifton.collateral.haircut;

import com.clifton.collateral.haircut.search.CollateralHaircutDefinitionSearchForm;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.system.schema.column.SystemColumnService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author stevenf
 */
@ContextConfiguration
@Transactional
public class CollateralHaircutDefinitionTests extends BaseInMemoryDatabaseTests {

	@Resource
	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;

	@Resource
	private SystemColumnService systemColumnService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeTest() {
		// Needed in order to load CCY for the account (new security) - which checks for any required custom fields
		Mockito.when(this.systemColumnService.getSystemColumnCustomListForGroupAndLinkedValue(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyBoolean())).thenReturn(null);
	}


	@Test
	public void testGetCollateralHaircutDefinitionList() {
		List<CollateralHaircutDefinition> collateralHaircutDefinitionList = this.collateralHaircutDefinitionService.getCollateralHaircutDefinitionList(new CollateralHaircutDefinitionSearchForm());
		Assertions.assertEquals(collateralHaircutDefinitionList.size(), 1);
	}
}
