package com.clifton.collateral;


import com.clifton.collateral.haircut.CollateralHaircut;
import com.clifton.collateral.haircut.CollateralHaircutDefinition;
import com.clifton.collateral.haircut.CollateralHaircutDefinitionService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CollateralServiceImplTests {

	@Resource
	private CollateralHaircutDefinitionService collateralHaircutDefinitionService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentGroupService investmentGroupService;


	////////////////////////////////////////////////////////////
	// Save Validation Tests
	////////////////////////////////////////////////////////////


	@Test
	public void testSaveCollateralHaircutNoTypeOrHierarchyOrGroup() {
		TestUtils.expectException(ValidationException.class, () -> {
			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "Investment Type or Hierarchy must be selected");
	}


	@Test
	public void testSaveCollateralHaircutBothTypeAndHierarchy() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("9")));
			bean.setInvestmentType(this.investmentSetupService.getInvestmentType(MathUtils.SHORT_ONE));
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "Investment Type or Hierarchy must be selected, but not more than one.");
	}


	@Test
	public void testSaveCollateralHaircutOutOfRangeLow() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(-.5));
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("11")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "Haircut Value [-0.5] is out of range.  Please enter a value between 0.00 and 1.00");
	}


	@Test
	public void testSaveCollateralHaircutOutOfRangeHigh() {
		TestUtils.expectException(ValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(1.0300));
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("11")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "Haircut Value [1.03] is out of range.  Please enter a value between 0.00 and 1.00");
	}


	@Test
	public void testSaveCollateralHaircutBadHierarchy() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(0.5));
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(MathUtils.SHORT_ONE));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "The Hierarchy selected [Bond] does not allow assignments.  Please select a hierarchy that allows assignments only.");
	}


	@Test
	public void testSaveCollateralHaircutNegativeStartYears() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(0.5));
			haircut.setStartYearsToMaturity(-21);
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("9")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "Start Days To Maturity entered [-7665] cannot be negative.  Please enter a value 0 or greater.");
	}


	@Test
	public void testSaveCollateralHaircutNegativeEndYears() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(0.5));
			haircut.setEndYearsToMaturity(-21);
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("9")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "End Days To Maturity entered [-7665] cannot be negative.  Please enter a value 0 or greater.");
	}


	@Test
	public void testSaveCollateralHaircutNegativeStartGreaterThanEnd() {
		TestUtils.expectException(ValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setHaircutPercent(BigDecimal.valueOf(0.5));
			haircut.setEndYearsToMaturity(10);
			haircut.setStartYearsToMaturity(20);
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("9")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "End Days To Maturity entered [3650] must be larger than Start Days To Maturity [7300]");
	}


	@Test
	public void testSaveCollateralHaircutOverlappingExistsStartEnd() {
		TestUtils.expectException(ValidationException.class, () -> {
			List<CollateralHaircut> haircutList = new ArrayList<>();
			CollateralHaircut haircut = new CollateralHaircut();
			haircut.setStartYearsToMaturity(1);
			haircut.setEndYearsToMaturity(3);
			haircut.setHaircutPercent(BigDecimal.valueOf(0.05));
			haircutList.add(haircut);
			haircut = new CollateralHaircut();
			haircut.setStartYearsToMaturity(0);
			haircut.setEndYearsToMaturity(2);
			haircut.setHaircutPercent(BigDecimal.valueOf(0.08));
			haircutList.add(haircut);

			CollateralHaircutDefinition bean = new CollateralHaircutDefinition();
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(Short.parseShort("9")));
			bean.setCollateralHaircutList(haircutList);
			this.collateralHaircutDefinitionService.saveCollateralHaircutDefinition(bean);
		}, "[Haircut Percent: 8%; Start Year: 0; End Year: 2] overlaps with [Haircut Percent: 5%; Start Year: 1; End Year: 3] in this definition.");
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////
	// Collateral Haircut Value tests


	private void setupTests() {
		// Set End Dates so can test DaysToMaturity
		Date today = new Date();
		today = DateUtils.clearTime(today);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(6);
		security.setEndDate(DateUtils.addYears(today, 2));
		this.investmentInstrumentService.saveInvestmentSecurity(security);

		security = this.investmentInstrumentService.getInvestmentSecurity(7);
		security.setEndDate(DateUtils.addDays(today, -10));
		this.investmentInstrumentService.saveInvestmentSecurity(security);

		security = this.investmentInstrumentService.getInvestmentSecurity(1);
		security.setEndDate(DateUtils.addDays(today, 300));
		this.investmentInstrumentService.saveInvestmentSecurity(security);

		security = this.investmentInstrumentService.getInvestmentSecurity(8);
		security.setEndDate(DateUtils.addYears(today, 2));
		this.investmentInstrumentService.saveInvestmentSecurity(security);

		Mockito.when(this.investmentGroupService.getInvestmentGroupListForSecurity(ArgumentMatchers.anyInt())).thenReturn(null);
	}


	@Test
	public void testGetCollateralHaircut() {
		int goldmanAccount = 2;
		int jpMorganAccount = 4;

		setupTests();

		// Security [1], Hierarchy [Bond / Asset Back Securities-4], Type [Bond-1]
		// Security [2], Hierarchy [Stock / Common Stocks-6], Type [Stock-2]
		// Security [3], Hierarchy [Future / Fixed Income Futures-9], Type [NULL]
		// Security [4], Hierarchy [Currency-10], Type [NULL]
		// Security [5], Hierarchy [Future / Fixed Income Futures-9], Type [NULL]
		// Security [6], Hierarchy [Future / Fixed Income Futures-9], Type [NULL]
		// Security [7], Hierarchy [Bond / Taxable Bonds-5], Type [Bond-1]

		// TEST VALUE FROM ACTUAL HAIRCUT OBJECTS
		// Use Goldman Bond Type Override (Only One defined, so Days To Maturity is ignored)
		CollateralHaircut haircut = this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 1, false, new Date());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.37), haircut.getHaircutPercent()));
		// Use Bond Type 2 years out
		haircut = this.collateralHaircutDefinitionService.getCollateralHaircut(1, 1, false, new Date());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.40), haircut.getHaircutPercent()));
		// Use Bond Type 2 years out
		haircut = this.collateralHaircutDefinitionService.getCollateralHaircut(1, 8, false, new Date());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.47), haircut.getHaircutPercent()));
		// No Stock Haircuts Defined
		Assertions.assertNull(this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 2, false, new Date()));
		// Use Goldman Fixed Income Future Hierarchy Override (Only One defined, so Days To Maturity is ignored)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.77), this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 3, false, new Date()).getHaircutPercent()));
		// No Currency Haircuts Defined
		Assertions.assertNull(this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 4, false, new Date()));
		// Use Goldman Fixed Income Future Hierarchy Override (Only One defined, so Days To Maturity is ignored)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.77), this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 5, false, new Date()).getHaircutPercent()));
		// Use Goldman Fixed Income Future Hierarchy Override (Only One defined, so Days To Maturity is ignored)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.77), this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 6, false, new Date()).getHaircutPercent()));
		// Use Goldman Bond Type Override (Only One defined, so Days To Maturity is ignored)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.37), this.collateralHaircutDefinitionService.getCollateralHaircut(goldmanAccount, 7, false, new Date()).getHaircutPercent()));
		// TEST HAIRCUT VALUES
		// Use JP Morgan Bond Type Override (No Security End Date, so uses last one)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.50), this.collateralHaircutDefinitionService.getCollateralHaircutValue(jpMorganAccount, 8, false, new Date())));
		// No Stock Haircuts Defined
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, this.collateralHaircutDefinitionService.getCollateralHaircutValue(jpMorganAccount, 2, false, new Date())));

//		// Use Default Fixed Income Future Hierarchy (No Security End Date, so last one)
		haircut = this.collateralHaircutDefinitionService.getCollateralHaircut(jpMorganAccount, 3, false, new Date());
		Assertions.assertEquals((Integer) 4, haircut.getId());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.80), haircut.getHaircutPercent()));
//		// No Currency Haircuts Defined
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, this.collateralHaircutDefinitionService.getCollateralHaircutValue(jpMorganAccount, 4, false, new Date())));
//		// Use Default Fixed Income Future Hierarchy (No Security End Date, so last one)
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.80), this.collateralHaircutDefinitionService.getCollateralHaircutValue(jpMorganAccount, 5, false, new Date())));

		// Use Default Fixed Income Future Hierarchy (Security End Date = 365 days out, so use 180 Haircut Override (Closest One))
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.80), this.collateralHaircutDefinitionService.getCollateralHaircutValue(jpMorganAccount, 6, false, new Date())));

		// Use JP Morgan Bond Type Override (Security End Date = 10 days ago (negative value), so considers daystomaturity = 0 (Closest One))
		haircut = this.collateralHaircutDefinitionService.getCollateralHaircut(jpMorganAccount, 7, false, new Date());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.25), haircut.getHaircutPercent()));
	}
}
