package com.clifton.oms.tests.workflow;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>WorkflowTransitioner</code> provides convenience methods for transitioning through workflows.
 *
 * @author jgommels
 */
@Component
public class WorkflowTransitioner {

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;


	/**
	 * Transitions to the given transition name for the given table name and ID.
	 *
	 * @param transitionName the name of the transition to transition to
	 * @param tableName      the name of the table to transition for
	 * @param id             the id of the row in the table
	 * @return whether or not the transition occurred
	 */
	public boolean transitionIfPresent(String transitionName, String tableName, Long id) {
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(tableName, id);

		for (WorkflowTransition transition : CollectionUtils.getIterable(transitions)) {
			if (transition.getName().equalsIgnoreCase(transitionName)) {
				this.workflowTransitionService.executeWorkflowTransitionByTransition(tableName, id, transition.getId());
				return true;
			}
		}

		return false;
	}


	/**
	 * Shortcut transition that can be used by tests that will look for a particular transition with name and end state, and if doesn't exists
	 * will create it.  This is useful for tests as a shortcut to get through various states so we can get to the end state and also not require some particular
	 * conditions that are necessary along the way that for testing purposes, our end goal doesn't need all of that validated.
	 * <p>
	 * Use transitionIfPresent method to go through real transitions
	 */
	public boolean transitionForTests(String transitionName, String endStateName, String tableName, Long id) {
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(tableName, id);
		WorkflowState startState = null;
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitions)) {
			// "no" transition is returned as an option where start state is null and end state = current state
			if (transition.getStartWorkflowState() == null) {
				startState = transition.getEndWorkflowState();
			}
			if (transition.getName().equalsIgnoreCase(transitionName)) {
				this.workflowTransitionService.executeWorkflowTransitionByTransition(tableName, id, transition.getId());
				return true;
			}
		}

		AssertUtils.assertNotNull(startState, "Start state is null");
		if (!endStateName.equals(startState.getName())) {
			// Didn't find it - create the transition
			WorkflowTransition transition = new WorkflowTransition();
			transition.setName(transitionName);
			transition.setStartWorkflowState(startState);
			transition.setEndWorkflowState(this.workflowDefinitionService.getWorkflowStateByName(startState.getWorkflow().getId(), endStateName));
			this.workflowTransitionService.saveWorkflowTransition(transition);

			this.workflowTransitionService.executeWorkflowTransitionByTransition(tableName, id, transition.getId());
			return true;
		}

		return false;
	}
}
