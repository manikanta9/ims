package com.clifton.oms.tests.order.management.setup.rule.execution;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.oms.tests.BaseOmsIntegrationTest;
import com.clifton.oms.tests.order.shared.OrderSharedUtils;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionCommand;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.management.setup.rule.execution.search.OrderManagementRuleExecutingBrokerCompanySearchForm;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class OrderManagementRuleExecutingBrokerTests extends BaseOmsIntegrationTest {

	@Resource
	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final String CITI = OrderSharedUtils.EXECUTING_BROKER_CITI_COMPANY_NAME;
	private static final String BBH = OrderSharedUtils.EXECUTING_BROKER_BBH_COMPANY_NAME;
	private static final String JPM = OrderSharedUtils.EXECUTING_BROKER_JPM_COMPANY_NAME;
	private static final String STATE_STREET = OrderSharedUtils.EXECUTING_BROKER_STATE_STREET_COMPANY_NAME;
	private static final String PERSHING = OrderSharedUtils.EXECUTING_BROKER_PERSHING_COMPANY_NAME;
	public static final String BNYM = OrderSharedUtils.EXECUTING_BROKER_BNYM_COMPANY_NAME;
	public static final String NORTHERN_TRUST = OrderSharedUtils.EXECUTING_BROKER_NORTHERN_TRUST_COMPANY_NAME;
	public static final String RBC = OrderSharedUtils.EXECUTING_BROKER_RBC_COMPANY_NAME;
	public static final String BNP = OrderSharedUtils.EXECUTING_BROKER_BNP_COMPANY_NAME;
	public static final String FIDELITY = OrderSharedUtils.EXECUTING_BROKER_FIDELITY_COMPANY_NAME;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Confirms data from Seattle trading spreadsheet that was generated using Sharepoint rules
	 */
	@Test
	public void testCurrencyBrokerExecutionRules() {
		validateExecutingBrokerForCurrencyResult("JEFGBL", "AUD", "USD", BBH, CITI, JPM, STATE_STREET);
		validateExecutingBrokerForCurrencyResult("JEFGBL", "USD", "AUD", BBH, CITI, JPM, STATE_STREET);

		validateExecutingBrokerForCurrencyResult("ZHUGBL", "AUD", "USD", PERSHING);

		validateExecutingBrokerForCurrencyResult("3C11INTL", "CHF", "USD", BNYM, STATE_STREET, CITI);

		validateExecutingBrokerForCurrencyResult("EVSTRINT", "EUR", "USD", BNYM, CITI, JPM, NORTHERN_TRUST, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("STRINTL", "EUR", "USD", BNYM, CITI, JPM, NORTHERN_TRUST, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("UCITDIV", "EUR", "USD", BNYM, CITI, NORTHERN_TRUST, STATE_STREET, BBH);

		validateExecutingBrokerForCurrencyResult("3C11INTL", "GBP", "USD", BNYM, STATE_STREET, CITI);

		validateExecutingBrokerForCurrencyResult("JEFGBL", "HKD", "USD", CITI, JPM, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("PEMCOM", "HKD", "USD", BNYM, CITI, JPM, NORTHERN_TRUST, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("PIMFEM", "HKD", "USD", BNYM, CITI, JPM, NORTHERN_TRUST, STATE_STREET, BBH);
		//validateExecutingBrokerForCurrencyPreviewResult("SAFINTL", "HKD", BNYM, CITI, NORTHERN_TRUST, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("UCITDIV", "HKD", "USD", BNYM, CITI, NORTHERN_TRUST, STATE_STREET, BBH);

		validateExecutingBrokerForCurrencyResult("CAINTL", "JPY", "USD", PERSHING);
		validateExecutingBrokerForCurrencyResult("EAGGBL", "JPY", "USD", PERSHING);
		validateExecutingBrokerForCurrencyResult("JEFGBL", "JPY", "USD", CITI, JPM, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("LGSGLI", "JPY", "USD", BNYM, CITI, JPM, NORTHERN_TRUST, BBH, STATE_STREET);
		validateExecutingBrokerForCurrencyResult("LGSUPGE", "JPY", "USD", BNYM, CITI, NORTHERN_TRUST, STATE_STREET, BBH);
		validateExecutingBrokerForCurrencyResult("QANTDB", "JPY", "USD", BNYM, JPM, NORTHERN_TRUST, STATE_STREET, BBH, CITI);
		validateExecutingBrokerForCurrencyResult("QANTDB", "AUD", "USD", BNYM, JPM, NORTHERN_TRUST, STATE_STREET, BBH, CITI);
		// Settlement of BHD - only State Street is allowed
		validateExecutingBrokerForCurrencyResult("QANTDB", "JPY", "BHD", STATE_STREET);
		validateExecutingBrokerForCurrencyResult("ZHUGBL", "JPY", "USD", PERSHING);
		validateExecutingBrokerForCurrencyResult("ZHUGBL", "USD", "JPY", PERSHING);

		validateExecutingBrokerForCurrencyResult("3C11INTL", "SEK", "USD", BNYM, STATE_STREET, CITI);

		// Required to by BNYM, but BNYM won't execute... NO LONGER VALID TEST, WAS SWITCHED TO REQUIRE 11A
		// validateExecutingBrokerForCurrencyResult("3c11emfd", "BHD", "USD");
		// validateExecutingBrokerForCurrencyResult("3c11emfd", "USD", "BHD");

		// No executing brokers available - none required and none allowed
		validateExecutingBrokerForCurrencyResult("TAXEFFEM", "COP", "USD");
		validateExecutingBrokerForCurrencyResult("TAXEFFEM", "USD", "COP");

		// Street FX
		validateExecutingBrokerForCurrencyResult("pimemetf", "KRW", "USD", "Street FX");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateExecutingBrokerForCurrencyResult(String accountShortName, String givenCurrencyCode, String settlementCurrencyCode, String... validExecutingBrokerCompanyNames) {
		OrderManagementRuleExecutionCommand command = new OrderManagementRuleExecutionCommand();
		OrderAccount orderAccount = this.orderSharedUtils.getClientAccountByShortName(accountShortName);
		ValidationUtils.assertTrue(orderAccount.isHoldingAccount(), "In order to preview rules for a single account it must be both a client and holding account.");
		command.setClientAccountId(orderAccount.getId());
		command.setHoldingAccountId(orderAccount.getId());
		command.setOrderTypeId(this.orderSetupUtils.getOrderTypeCurrency(false).getId());
		command.setOrderSecurityId(this.orderSharedUtils.getOrderSecurityByTicker(givenCurrencyCode, true).getId());
		command.setSettlementCurrencyId(this.orderSharedUtils.getOrderSecurityByTicker(settlementCurrencyCode, true).getId());
		command.setTradeDate(new Date());

		OrderManagementRuleExecutingBrokerCompanySearchForm searchForm = new OrderManagementRuleExecutingBrokerCompanySearchForm();
		searchForm.setCommand(command);

		List<OrderCompany> companyList = this.orderManagementRuleExecutionService.getOrderManagementRuleExecutingBrokerCompanyList(searchForm);

		if (ArrayUtils.isEmpty(validExecutingBrokerCompanyNames)) {
			ValidationUtils.assertEmpty(companyList, () -> String.format("Did not expect any valid executing brokers to be returned for %s  and %s but preview returned: %s", accountShortName, givenCurrencyCode, StringUtils.collectionToCommaDelimitedString(companyList, OrderCompany::getName)));
			return;
		}
		boolean misMatch = validExecutingBrokerCompanyNames.length != CollectionUtils.getSize(companyList);
		if (!misMatch) {
			for (OrderCompany orderCompany : CollectionUtils.getIterable(companyList)) {
				if (!StringUtils.equalsAnyIgnoreCase(orderCompany.getName(), validExecutingBrokerCompanyNames)) {
					misMatch = true;
					break;
				}
			}
		}
		ValidationUtils.assertFalse(misMatch, () -> String.format("Expected valid executing brokers to be [%s] for %s and %s but preview returned: %s", StringUtils.collectionToCommaDelimitedString(CollectionUtils.sort(Arrays.asList(validExecutingBrokerCompanyNames.clone()))), accountShortName, givenCurrencyCode, StringUtils.collectionToCommaDelimitedString(BeanUtils.sortWithFunction(companyList, OrderCompany::getName, true), OrderCompany::getName)));
	}
}
