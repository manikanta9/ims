package com.clifton.oms.tests.system.bean;

import com.clifton.oms.tests.spring.OmsIntegrationConfiguration;
import com.clifton.test.system.bean.BaseSystemBeanTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {OmsIntegrationConfiguration.class})
public class OmsSystemBeanTests extends BaseSystemBeanTests {

	// NOTHING HERE
}
