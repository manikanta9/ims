package com.clifton.oms.tests;

import com.clifton.core.context.ContextHandler;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListEntity;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.test.protocol.ImsProtocolClientConfiguration;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.apache.http.NameValuePair;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>IntegrationTestConfiguration</code> houses configuration defaults for how the core clifton-test project should act
 * in regards to handling various aspects of integration tests for IMS.
 * <p>
 * These defaults revolve around default properties to save and exclude along with various adapter overrides.  It also allows specifying various
 * additional parameters to add to requests for specific URLs.
 *
 * @author apopp
 */
public class OmsIntegrationTestConfiguration implements ImsProtocolClientConfiguration {

	private static final Set<Class<?>> EXCLUDED_CLASSES = CollectionUtils.createHashSet(MultipartFile.class, SystemListItem.class, SystemColumn.class);
	private static final Map<Class<?>, Set<String>> EXCLUDED_PROPERTIES_MAP = new HashMap<>();
	private static final Map<Class<?>, Set<String>> SAVE_NULL_PROPERTIES_MAP = new HashMap<>();
	private static final Map<Class<?>, Set<String>> NESTED_SAVE_PROPERTIES_MAP = new HashMap<>();
	private static final Set<Class<?>> OVERRIDE_CLASSES_ONLY_SEND_ID_WHEN_PRESENT = new HashSet<>();
	private static final Map<Class<?>, JsonDeserializer<?>> TYPE_ADAPTER_MAP = new HashMap<>();


	static {
		EXCLUDED_PROPERTIES_MAP.put(SystemBeanProperty.class, CollectionUtils.createHashSet("parameter", "multipleValuesAllowed"));
		EXCLUDED_PROPERTIES_MAP.put(SystemBeanPropertyType.class, CollectionUtils.createHashSet("systemUserInterfaceValueConfig", "usedByLabel", "userInterfaceConfig", "virtualProperty"));
	}


	static {
		SAVE_NULL_PROPERTIES_MAP.put(SystemTable.class, CollectionUtils.createHashSet("maxQuerySizeLimit"));
	}


	static {
		NESTED_SAVE_PROPERTIES_MAP.put(SecurityPermissionAssignment.class, CollectionUtils.createHashSet("securityPermission"));
	}


	static {
		TYPE_ADAPTER_MAP.put(Class.class, new ClassTypeAdapter());
		TYPE_ADAPTER_MAP.put(SystemList.class, new SystemListAdapter());
		TYPE_ADAPTER_MAP.put(SystemColumn.class, new SystemColumnAdapter());
		TYPE_ADAPTER_MAP.put(CustomJsonString.class, new CustomJsonStringAdapter());
	}


	/**
	 * Use {@link ThreadLocalContextHandler} that can be used by IMS Integration
	 * Test Framework to support concurrent test execution.
	 */
	private ContextHandler contextHandler = new ThreadLocalContextHandler();


	@Override
	public Map<Class<?>, Set<String>> getNestedSavePropertiesMap() {
		return NESTED_SAVE_PROPERTIES_MAP;
	}


	@Override
	public Set<Class<?>> getOverrideClassesOnlySendIdWhenPresent() {
		return OVERRIDE_CLASSES_ONLY_SEND_ID_WHEN_PRESENT;
	}


	@Override
	public Map<Class<?>, Set<String>> getExcludedPropertiesMap() {
		return EXCLUDED_PROPERTIES_MAP;
	}


	@Override
	public Map<Class<?>, Set<String>> getSaveNullPropertiesMap() {
		return SAVE_NULL_PROPERTIES_MAP;
	}


	@Override
	public Set<Class<?>> getExcludedClasses() {
		return EXCLUDED_CLASSES;
	}


	@Override
	public Map<Class<?>, JsonDeserializer<?>> getTypeAdapterMap() {
		return TYPE_ADAPTER_MAP;
	}


	@Override
	public void addParameterOverridesForPath(String path, List<NameValuePair> params) {
		// nothing here
	}


	@Override
	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	/**
	 * The <code>ClassTypeAdapter</code> is a JsonDeserializer that instructs Gson to deserialize a generic Class<?> object
	 *
	 * @author stevenf
	 */
	static class ClassTypeAdapter implements JsonDeserializer<Class<?>> {

		@Override
		public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			try {
				return Class.forName(json.getAsString());
			}
			catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
	}

	static class CustomJsonStringAdapter implements JsonDeserializer<CustomJsonString> {

		@Override
		public CustomJsonString deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return new CustomJsonString(json.toString());
		}
	}


	/**
	 * The <code>SystemListAdapter</code> is a JsonDeserializer that instructs Gson to deserialize SystemList to SystemListEntity.
	 * This is needed because SystemList is an abstract class and thus cannot be instantiated.
	 *
	 * @author jgommels
	 */
	static class SystemListAdapter implements JsonDeserializer<SystemList> {

		@Override
		public SystemList deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, SystemListEntity.class);
		}
	}

	static class SystemColumnAdapter implements JsonDeserializer<SystemColumn> {

		@Override
		public SystemColumn deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, SystemColumnCustom.class);
		}
	}
}
