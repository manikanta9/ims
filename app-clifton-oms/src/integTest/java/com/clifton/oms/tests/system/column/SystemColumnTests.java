package com.clifton.oms.tests.system.column;

import com.clifton.oms.tests.spring.OmsIntegrationConfiguration;
import com.clifton.test.system.column.BaseSystemColumnTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {OmsIntegrationConfiguration.class})
public class SystemColumnTests extends BaseSystemColumnTests {

	// NOTHING HERE
}
