package com.clifton.oms.tests.system.search;

import com.amazonaws.util.StringUtils;
import com.clifton.core.dataaccess.search.form.BaseSearchForm;
import com.clifton.test.system.search.BaseSystemSearchFormTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/oms/tests/core/dataaccess/SessionFactoryTests-context.xml")
@ExtendWith(SpringExtension.class)
public class OmsSystemSearchFormTests extends BaseSystemSearchFormTests {

	@Override
	protected boolean isIgnoreSearchFormDtoValidation(Class<? extends BaseSearchForm> clazz) {
		return StringUtils.beginsWithIgnoreCase(clazz.getName(), "com.clifton.fix");
	}
}
