package com.clifton.oms.tests.rule.order.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.oms.tests.order.shared.OrderSharedUtils;
import com.clifton.oms.tests.rule.RuleTests;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.OrderManagementRuleType;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleTypeSearchForm;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public class OrderAllocationRuleTests extends RuleTests {

	private static final String ORDER_MANAGEMENT_RULE_DEFINITION_NAME = "Order Management Rule Evaluator";


	@Resource
	private OrderManagementRuleService orderManagementRuleService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Order Allocation Rules";
	}


	@Override
	public String getTableName() {
		return OrderAllocation.TABLE_NAME;
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return CollectionUtils.createHashSet("testOrderManagementRuleEvaluatorRule", // Skip the main rule since we run each rule type individually
				"testOrderManagementRuleEvaluator_SecurityAllowedtoTradeRule",
				"testOrderManagementRuleEvaluator_SecurityProhibitedfromTradingRule"
		);
	}


	@Override
	protected List<String> getRuleDefinitionNameList() {
		List<String> ruleDefinitionNameList = super.getRuleDefinitionNameList();

		OrderManagementRuleTypeSearchForm orderManagementRuleTypeSearchForm = new OrderManagementRuleTypeSearchForm();
		orderManagementRuleTypeSearchForm.setRollup(false);
		List<OrderManagementRuleType> ruleTypeList = this.orderManagementRuleService.getOrderManagementRuleTypeList(orderManagementRuleTypeSearchForm);

		CollectionUtils.getStream(ruleTypeList).forEach(ruleType -> {
			ruleDefinitionNameList.add(ORDER_MANAGEMENT_RULE_DEFINITION_NAME + "_" + ruleType.getName());
		});
		return ruleDefinitionNameList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTradeDateIsAValidBusinessDayRule() {
		// Currencies do not trade on an exchange, so any trade date is considered to be valid.  What matters for these is the settlement date
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("1397567.7741"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), "Trade Date Is A Validate Business Day");
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testTradeDateisinthefutureCurrencyRule() {
		String ruleDefinitionName = "Trade Date Is In The Future (Currency)";
		Date currentDateWithoutTime = DateUtils.clearTime(new Date());
		OrderAllocation orderAllocation = this.orderBaseUtils.
				saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.addWeekDays(currentDateWithoutTime, 2), DateUtils.addWeekDays(currentDateWithoutTime, 3));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Trade Date is more than one day in the future");
		cancelOrderAllocation(orderAllocation);

		// One day in future is fine for CCY
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.addDays(currentDateWithoutTime, 1), DateUtils.addWeekDays(currentDateWithoutTime, 1));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testTradeDateisinthefutureNotCurrencyRule() {
		String ruleDefinitionName = "Trade Date Is In The Future (Not Currency)";
		Date currentDateWithoutTime = DateUtils.clearTime(new Date());
		OrderAllocation orderAllocation = this.orderBaseUtils.
				saveStockOrderAllocationForExchange("revpintl", true, "LN", new BigDecimal("50000"), DateUtils.addWeekDays(currentDateWithoutTime, 1));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Trade Date is in the future");
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testSettlementDateIsAValidBusinessDayRule() {
		String ruleDefinitionName = "Settlement Date Is A Valid Business Day";
		// AUD Holiday
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("04/05/2021"), DateUtils.toDate("04/05/2021"));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Settlement Date*.*is not a valid business day for security AUD and settlement currency USD*.*");
		cancelOrderAllocation(orderAllocation);

		// USD Holiday
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/12/2021"), DateUtils.toDate("02/15/2021"));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Settlement Date*.*is not a valid business day for security AUD and settlement currency USD*.*");
		cancelOrderAllocation(orderAllocation);

		// Weekend
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/25/2021"), DateUtils.toDate("02/28/2021"));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Settlement Date*.*is not a valid business day for security AUD and settlement currency USD*.*");
		cancelOrderAllocation(orderAllocation);

		// Before Trade Date
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/25/2021"), DateUtils.toDate("02/24/2021"));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Settlement Date*.*is not a valid business day for security AUD and settlement currency USD*.*Settlement Date cannot be before Trade Date*.*");
		cancelOrderAllocation(orderAllocation);

		// Good Date
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testSettlementDateismorethan2businessdaysfromTradeDateRule() {
		String ruleDefinitionName = "Settlement Date Is More Than 2 Business Days from Trade Date";

		// 2 Days out (OK)
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/08/2021"), DateUtils.toDate("02/10/2021"));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation);

		// 2 Days out over the weekend (OK)
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/05/2021"), DateUtils.toDate("02/09/2021"));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation);

		// 3 Days out (violation)
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), DateUtils.toDate("02/08/2021"), DateUtils.toDate("02/11/2021"));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Settlement Date of 02/11/2021 is more than 2 business days from Trade Date. Currency orders with late settlements may be treated as a Currency Forward order allocation.");
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testDuplicateOrderAllocationRule() {
		String ruleDefinitionName = "Duplicate Order Allocation";

		//New Order Allocation-save-Okay-No Violation
		OrderAllocation orderAllocation1 = this.orderBaseUtils.saveCurrencyOrderAllocation("lozfou", true, "AUD", "USD", new BigDecimal("40000"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation1.getId(), ruleDefinitionName);

		//Duplicate OrderAllocation-create a Violation

		OrderAllocation orderAllocation2 = this.orderBaseUtils.saveCurrencyOrderAllocation("lozfou", true, "AUD", "USD", new BigDecimal("40000"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation2.getId(), ruleDefinitionName, ".*Potential Duplicate Order Allocation Found for Account / Security / Trade Date.*");

		cancelOrderAllocation(orderAllocation1);
		cancelOrderAllocation(orderAllocation2);

		//create new Order allocation-3
		//New Order Allocation-save-Okay-No Violation
		OrderAllocation orderAllocation3 = this.orderBaseUtils.saveCurrencyOrderAllocation("lozfou", true, "AUD", "USD", new BigDecimal("40000"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation3.getId(), ruleDefinitionName);
		// CANCEL ORDER ALLOCATION 3 (SO TEST IS RE-RUNNABLE)
		cancelOrderAllocation(orderAllocation3);
	}


	@Test
	public void testValidateAccountWorkflowStateRule() {
		// terminated account
		String ruleDefinitionName = "Validate Account Workflow State";
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("82ahintl", true, "AUD", "USD", new BigDecimal("40000"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, ".*Account.*");
		cancelOrderAllocation(orderAllocation);

		//Active account
		OrderAllocation orderAllocation1 = this.orderBaseUtils.saveCurrencyOrderAllocation("3c11intl", true, "AUD", "USD", new BigDecimal("40000"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation1.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation1);
	}


	@Test
	public void testStreetFXSettlementDateConfirmationViolationRule() {

		//SHOULD THROW VIOLATION
		String ruleDefinitionName = "Street FX Settlement Date Confirmation Violation";
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("pimemetf", true, "TWD", "USD", new BigDecimal("3000"), new Date(), DateUtils.addWeekDays(new Date(), 1), OrderSharedUtils.EXECUTING_BROKER_STREET_FX);
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ruleDefinitionName, "Street FX Order: Please double check Settlement Dates. Confirm settlement dates by ignoring this violation");
		cancelOrderAllocation(orderAllocation);

		//NO VIOLATION
		OrderAllocation orderAllocation1 = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("davintl", true, "USD", "aed", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_PERSHING_COMPANY_NAME);
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation1.getId(), ruleDefinitionName);
		cancelOrderAllocation(orderAllocation1);
	}
	////////////////////////////////////////////////////////////////////////////
	////////            Order Management Rule Tests                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderManagementRuleEvaluator_CurrencyProhibitedRulesRule() {
		// By Default accounts are prohibited from CCY trading
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("bluflo", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Currency Pair AED.* / USD.*is not allowed for this account.*", true);

		// Account override supersedes Global Prohibition
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationNotExists(orderAllocation, true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_CurrencyRulesRule() {
		// By Default accounts are prohibited from CCY trading
		// Account override supersedes Global Prohibition
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationNotExists(orderAllocation, true);

		// Pershing is technically a required broker for pershing accounts and AED, but AED is not an approved currency
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("davintl", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_PERSHING_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Currency Pair AED.* / USD.*is not allowed for this account.*", true);

		// Switch Security and Settlement Currency - should still restrict AED
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("davintl", true, "USD", "aed", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_PERSHING_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Currency Pair USD.* / AED.*is not allowed for this account.*", true);

		// 11A Rule Checks
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("brandgl", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationExists(orderAllocation, ".*AED.*11A.*", true);

		// Switch USD/AED - should still catch it
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("brandgl", true, "USD", "AED", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationExists(orderAllocation, ".*AED.*11A.*", true);

		// TODO ADD AN 11 A ORDER TYPE AND VALIDATE IT PASSES
	}


	@Test
	public void testOrderManagementRuleEvaluator_ExecutingBrokerRequiredNotApprovedtoTradeRule() {
		// Pershing Required but not approved - no other brokers available
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("MACGBL", true, "CZK", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationExists(orderAllocation, ".*There are no available executing brokers that apply to this Order Allocation.*", true);

		// Try and set the EB - should still fail because not approved
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("MACGBL", true, "CZK", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_PERSHING_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Pershing.* is not an allowed executing broker for this account and currency.*", true);

		// Try another broker
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("MACGBL", true, "CZK", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_JPM_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*JPMorgan.* is not an allowed executing broker for this account and currency.*", true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_ExecutingBrokerAllowedtoTradeRule() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("calintl", true, "NOK", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_JPM_COMPANY_NAME);
		validateOrderManagementRuleViolationNotExists(orderAllocation, true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_ExecutingBrokerRequiredtoTradeRule() {
		// BNYM IS BOTH REQUIRED AND PROHIBITED - FAIL
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("3c11emfd", true, "BHD", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_BNYM_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Bank of New York Mellon is not an allowed executing broker for this account and currency", true);

		// BNYM is required - fail
		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("3c11emfd", true, "BHD", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_CITI_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Citibank NA is not an allowed executing broker for this account and currency", true);

		// Rules were updated to 11A in these cases.  Need to find a different example if possible
		// No executing brokers available - fail
		//orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("3c11emfd", true, "BHD", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		//validateOrderManagementRuleViolationExists(orderAllocation, , ".*There are no available executing brokers that apply to this Order Allocation.*");
	}


	@Test
	public void testOrderManagementRuleEvaluator_ExecutingBrokerProhibitAllfromTradingRule() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("trbgbl", true, "AUD", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_CITI_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Citibank NA is not an allowed executing broker for this account and currency", true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_ExecutingBrokerProhibitedfromTradingRule() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocationWithExecutingBroker("davintl", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"), OrderSharedUtils.EXECUTING_BROKER_CITI_COMPANY_NAME);
		validateOrderManagementRuleViolationExists(orderAllocation, ".*Citibank NA is not an allowed executing broker for this account and currency", true);

		orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("bluflo", true, "AED", "USD", new BigDecimal("10000"), DateUtils.toDate("03/01/2021"), DateUtils.toDate("03/02/2021"));
		validateOrderManagementRuleViolationExists(orderAllocation, ".*There are no available executing brokers that apply to this Order Allocation.*", true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_RestrictedSecurityAllowedtoTradeRule() {
		// Account 3c3emfd is allowed to trade stocks in Chile
		OrderAllocation orderAllocation = this.orderBaseUtils.saveStockOrderAllocationForExchange("3c3emfd", true, "CC", new BigDecimal("15000"), DateUtils.clearTime(new Date()));
		validateOrderManagementRuleViolationNotExists(orderAllocation, true);
	}


	@Test
	public void testOrderManagementRuleEvaluator_RestrictedSecurityProhibitedfromTradingRule() {
		// Account 3c11intl is NOT allowed to trade stocks in Chile
		OrderAllocation orderAllocation = this.orderBaseUtils.saveStockOrderAllocationForExchange("3c11intl", true, "CC", new BigDecimal("15000"), DateUtils.clearTime(new Date()));
		validateOrderManagementRuleViolationExists(orderAllocation, "This account is prohibited from trading in Chile", true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private void validateOrderManagementRuleViolationExists(OrderAllocation orderAllocation, String expectedMessageRegex, boolean cancelOrderAllocation) {
		validateRuleViolationExistsForEntityAndRuleDefinition(orderAllocation.getId(), ORDER_MANAGEMENT_RULE_DEFINITION_NAME, expectedMessageRegex);
		if (cancelOrderAllocation) {
			cancelOrderAllocation(orderAllocation);
		}
	}


	private void validateOrderManagementRuleViolationNotExists(OrderAllocation orderAllocation, boolean cancelOrderAllocation) {
		validateNoRuleViolationsForEntityAndRuleDefinition(orderAllocation.getId(), ORDER_MANAGEMENT_RULE_DEFINITION_NAME);
		if (cancelOrderAllocation) {
			cancelOrderAllocation(orderAllocation);
		}
	}
}
