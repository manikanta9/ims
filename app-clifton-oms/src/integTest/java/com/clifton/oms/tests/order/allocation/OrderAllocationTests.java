package com.clifton.oms.tests.order.allocation;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.oms.tests.BaseOmsIntegrationTest;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author AbhinayaM
 */

public class OrderAllocationTests extends BaseOmsIntegrationTest {

	@Resource
	private OrderAllocationService orderAllocationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Test case to prove successful rounding up of the currency
	 */
	@Test
	public void testOrderAllocation_AccountingNotionalRounding() {
		//case-1 saving an OrderAllocation for DKK for 1397567.7777
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "DKK", "USD", new BigDecimal("1397567.7777"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		Assertions.assertNotNull(orderAllocation);
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(1397567.78), orderAllocation.getAccountingNotional()));
		cancelOrderAllocation(orderAllocation);

		//case-2 saving OrderAllocation for Japanese YEN order
		OrderAllocation orderAllocation1 = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1397567.7777"), new Date(), DateUtils.addWeekDays(new Date(), 1));
		Assertions.assertNotNull(orderAllocation1);
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(1397568), orderAllocation1.getAccountingNotional()));
		cancelOrderAllocation(orderAllocation1);
	}


	@Test
	public void testOrderAllocation_ExchangeRateCurrencyConvention() {
		// Regardless if we have an order allocation that goes from JPY -> USD or USD to JPY the exchange rate should remain the same.
		OrderAllocation orderAllocation1 = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("05/10/2021"), null);
		OrderAllocation orderAllocation2 = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "USD", "JPY", new BigDecimal("1000"), DateUtils.toDate("05/10/2021"), null);

		Assertions.assertEquals(orderAllocation1.getExchangeRateToSettlementCurrency(), orderAllocation2.getExchangeRateToSettlementCurrency());

		cancelOrderAllocation(orderAllocation1);
		cancelOrderAllocation(orderAllocation2);
	}


	@Test
	public void testOrderAllocation_ExecutingBrokerForCurrency11A() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrency11AOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1397567.7777"), new Date(), DateUtils.addWeekDays(new Date(), 1), "");
		Assertions.assertNotNull(orderAllocation);
		Assertions.assertEquals(orderAllocation.getHoldingAccount().getIssuingCompany(), orderAllocation.getExecutingBrokerCompany());
		cancelOrderAllocation(orderAllocation);
	}


	@Test
	public void testOrderAllocation_Duplicate_Cancel_ClearViolationCode() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("50000"), new Date(), null);
		OrderAllocation orderAllocation2 = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "AUD", "USD", new BigDecimal("75000"), new Date(), null);

		Assertions.assertEquals("D", orderAllocation2.getRuleViolationCodes());
		// Cancel the first order allocation
		cancelOrderAllocation(orderAllocation);
		// Re-Validate the Second Order Allocation
		this.workflowTransitioner.transitionIfPresent("Validate", OrderAllocation.TABLE_NAME, orderAllocation2.getId());
		orderAllocation2 = this.orderAllocationService.getOrderAllocation(orderAllocation2.getId());

		Assertions.assertNull(orderAllocation2.getRuleViolationCodes());

		// Clean up - cancel the order allocation
		cancelOrderAllocation(orderAllocation2);
	}
}
