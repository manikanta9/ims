package com.clifton.oms.tests.order.shared;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSecurityExchange;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import com.clifton.order.shared.search.OrderCompanySearchForm;
import com.clifton.order.shared.search.OrderSecuritySearchForm;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class OrderSharedUtils {

	@Resource
	private OrderSharedService orderSharedService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static final String EXECUTING_BROKER_CITI_COMPANY_NAME = "Citibank NA";
	public static final String EXECUTING_BROKER_BBH_COMPANY_NAME = "Brown Brothers Harriman & Co";
	public static final String EXECUTING_BROKER_JPM_COMPANY_NAME = "JPMorgan Chase Bank NA";
	public static final String EXECUTING_BROKER_STATE_STREET_COMPANY_NAME = "State Street Bank";
	public static final String EXECUTING_BROKER_PERSHING_COMPANY_NAME = "Pershing Advisor Solutions LLC";
	public static final String EXECUTING_BROKER_BNYM_COMPANY_NAME = "Bank of New York Mellon";
	public static final String EXECUTING_BROKER_NORTHERN_TRUST_COMPANY_NAME = "Northern Trust Securities, Inc.";
	public static final String EXECUTING_BROKER_RBC_COMPANY_NAME = "RBC Capital Markets LLC";
	public static final String EXECUTING_BROKER_BNP_COMPANY_NAME = "BNP Paribas Securities Services";
	public static final String EXECUTING_BROKER_FIDELITY_COMPANY_NAME = "Fidelity Brokerage Services LLC";
	public static final String EXECUTING_BROKER_STREET_FX = "Street FX";
	public static final String ORDER_DESTINATION_STREET_FX = "Street FX";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderCompany getOrderCompanyByName(String companyName) {
		OrderCompanySearchForm searchForm = new OrderCompanySearchForm();
		searchForm.setNameEquals(companyName);
		return CollectionUtils.getFirstElementStrict(this.orderSharedService.getOrderCompanyList(searchForm), "Cannot find any company with name: " + companyName, "Found multiple companies with name: " + companyName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAccount getClientAccountByShortName(String accountShortName) {
		OrderAccountSearchForm searchForm = new OrderAccountSearchForm();
		searchForm.setAccountShortNameEquals(accountShortName);
		searchForm.setClientAccount(true);
		return CollectionUtils.getFirstElementStrict(this.orderSharedService.getOrderAccountList(searchForm), "Cannot find any client account with short name: " + accountShortName, "Found multiple client accounts with short name: " + accountShortName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderSecurity getOrderSecurityByTicker(String ticker, boolean currency) {
		if (currency) {
			return this.orderSharedService.getOrderSecurityForCurrencyTicker(ticker);
		}
		OrderSecuritySearchForm securitySearchForm = new OrderSecuritySearchForm();
		securitySearchForm.setTickerEquals(ticker);
		// Add Security Type NOT Currency?
		return CollectionUtils.getFirstElementStrict(this.orderSharedService.getOrderSecurityList(securitySearchForm), "Cannot find any security with ticker: " + ticker, "Found multiple securities with ticker: " + ticker);
	}


	/**
	 * Returns one security for the given exchange
	 * Can be used to just pull any security for an exchange to test the rules against that exchange or country, but you don't care which specific security it is.
	 * Will throw an exception if cannot find any security for the exchange and active options
	 */
	public OrderSecurity getOrderSecurityAnyForExchange(String exchangeCode, boolean activeOnly) {
		OrderSecurity security = CollectionUtils.getOnlyElement(getOrderSecurityListForExchangeImpl(exchangeCode, activeOnly, 1));
		ValidationUtils.assertNotNull(security, "Cannot find any securities for exchange " + exchangeCode + (activeOnly ? " and active only." : ""));
		return security;
	}


	private List<OrderSecurity> getOrderSecurityListForExchangeImpl(String exchangeCode, boolean activeOnly, Integer maxResults) {
		OrderSecurityExchange exchange = this.orderSharedService.getOrderSecurityExchangeForExchangeCode(exchangeCode);
		ValidationUtils.assertNotNull(exchange, "Cannot find Exchange with Exchange Code " + exchangeCode);

		OrderSecuritySearchForm orderSecuritySearchForm = new OrderSecuritySearchForm();
		orderSecuritySearchForm.setPrimaryExchangeId(exchange.getId());
		if (activeOnly) {
			orderSecuritySearchForm.setActive(true);
		}
		if (maxResults != null) {
			orderSecuritySearchForm.setLimit(maxResults);
		}
		return this.orderSharedService.getOrderSecurityList(orderSecuritySearchForm);
	}
}
