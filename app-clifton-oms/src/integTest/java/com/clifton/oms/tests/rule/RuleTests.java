package com.clifton.oms.tests.rule;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.oms.tests.BaseOmsIntegrationTest;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public abstract class RuleTests extends BaseOmsIntegrationTest {


	@Resource
	public CalendarBusinessDayService calendarBusinessDayService;
	@Resource
	public RuleDefinitionService ruleDefinitionService;
	@Resource
	public RuleEvaluatorService ruleEvaluatorService;
	@Resource
	public RuleViolationService ruleViolationService;
	@Resource
	public SystemRequestStatsService systemRequestStatsService;


	public abstract String getCategoryName();


	public abstract String getTableName();


	// Defaults to Null to Apply All Rules
	public Boolean getPreProcess() {
		return null;
	}


	public abstract Set<String> getExcludedTestMethodSet();


	public static final Map<String, String> expectedViolationNoteMap = new HashMap<>();
	public static final Map<String, Throwable> testExceptionsMap = new HashMap<>();

	public static final Set<String> excludedTestMethodSet = new HashSet<>();
	public static final Set<Method> testMethodSet = new HashSet<>();

	public static List<RuleViolation> ruleViolationList = new ArrayList<>();


	/**
	 * Returns the list of rule definition names, so that we can override
	 * and add more to the list if needed.
	 * For example - Order Allocation Order Management Rules, since one rule in Rule Engine runs all rules in OMS Rules,
	 * we add each OrderManagementRuleType name to the list for better coverage
	 */
	protected List<String> getRuleDefinitionNameList() {
		RuleDefinitionSearchForm ruleDefinitionSearchForm = new RuleDefinitionSearchForm();
		ruleDefinitionSearchForm.setManual(false);
		ruleDefinitionSearchForm.setInactive(false);
		ruleDefinitionSearchForm.setPreProcess(getPreProcess());
		ruleDefinitionSearchForm.setRuleCategoryName(getCategoryName());
		return Arrays.stream(BeanUtils.getPropertyValues(this.ruleDefinitionService.getRuleDefinitionList(ruleDefinitionSearchForm), RuleDefinition::getName, String.class)).collect(Collectors.toList());
	}


	@BeforeEach
	public void initializeTest() {
		if (testMethodSet.isEmpty()) {
			// initialize only once per test class
			Set<String> excludeTestMethods = getExcludedTestMethodSet();
			if (excludeTestMethods == null) {
				excludeTestMethods = new HashSet<>();
			}
			excludeTestMethods.add("testGetRuleCategoryByName");
			excludeTestMethods.add("testGetRuleDefinitionList");
			excludeTestMethods.add("testAllActiveRulesHaveTests");
			excludeTestMethods.add("testRuleTestsExistForRulesThatDoNotApply");
			excludeTestMethods.forEach(methodName -> excludedTestMethodSet.add(methodName.toLowerCase()));
			for (Method method : this.getClass().getMethods()) {
				if (AnnotationUtils.isAnnotationPresent(method, Test.class) && !excludedTestMethodSet.contains(method.getName().toLowerCase())) {
					testMethodSet.add(method);
				}
			}
		}
	}


	@AfterAll
	public static void clearTestDataStructures() {
		// clean up after each test class executes so the data structures are not polluted with data for the next test class
		excludedTestMethodSet.clear();
		testMethodSet.clear();
		expectedViolationNoteMap.clear();
		testExceptionsMap.clear();
	}


	@Test
	public void testGetRuleCategoryByName() {

		Assertions.assertEquals(getCategoryName(), this.ruleDefinitionService.getRuleCategoryByName(getCategoryName()).getName());
	}


	@Test
	public void testGetRuleDefinitionList() {
		RuleDefinitionSearchForm ruleDefinitionSearchForm = new RuleDefinitionSearchForm();
		ruleDefinitionSearchForm.setRuleCategoryName(getCategoryName());
		Assertions.assertNotNull(CollectionUtils.getFirstElement(getRuleDefinitionNameList()));
	}


	@Test
	public void testAllActiveRulesHaveTests() {
		Set<String> methodSet = new HashSet<>();
		for (Method method : testMethodSet) {
			methodSet.add(method.getName().toLowerCase());
		}

		List<String> missingTests = new ArrayList<>();
		for (String ruleDefinitionName : CollectionUtils.getIterable(getRuleDefinitionNameList())) {
			String testMethodName = "test" + ruleDefinitionName.replaceAll("\\s", "").replaceAll("\\W", "") + "Rule";
			if (!methodSet.contains(testMethodName.toLowerCase()) && !excludedTestMethodSet.contains(testMethodName.toLowerCase())) {
				missingTests.add(testMethodName);
			}
		}
		Collections.sort(missingTests);
		Assertions.assertEquals(0, missingTests.size(), "\n\nTests do not exist for:\n\n" + String.join("\n", missingTests) + "\n\n");
	}


	@Test
	public void testRuleTestsExistForRulesThatDoNotApply() {
		Set<String> definitionSet = new HashSet<>();
		for (String ruleDefinitionName : CollectionUtils.getIterable(getRuleDefinitionNameList())) {
			definitionSet.add(("test" + ruleDefinitionName.replaceAll("\\s", "").replaceAll("\\W", "") + "Rule").toLowerCase());
		}

		List<String> superfluousTests = new ArrayList<>();
		for (Method method : testMethodSet) {
			String testMethodName = method.getName();
			if (!definitionSet.contains(testMethodName.toLowerCase()) && !excludedTestMethodSet.contains(testMethodName.toLowerCase())) {
				superfluousTests.add(testMethodName);
			}
		}
		Collections.sort(superfluousTests);
		Assertions.assertEquals(0, superfluousTests.size(), "\n\nTests should not exist for:\n\n" + String.join("\n", superfluousTests) + "\n\n");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////             Test Assertion Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	protected void validateTestMethodResult(String methodName) {
		boolean foundViolationNote = false;
		Throwable exception = testExceptionsMap.get(methodName);
		if (exception == null) {
			String expectedViolationNote = expectedViolationNoteMap.get(methodName);
			StringBuilder failureMessage = new StringBuilder("Failed to find expected Violation: '" + expectedViolationNote + "' in ruleViolationList: \n");

			Pattern pattern = Pattern.compile(expectedViolationNote);
			for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
				Matcher matcher = pattern.matcher(ruleViolation.getViolationNote());
				if (ruleViolation.getViolationNote().equals(expectedViolationNote) || matcher.find()) {
					foundViolationNote = true;
				}
				failureMessage.append("\t").append(ruleViolation.getId()).append(" - ").append(ruleViolation.getViolationNote()).append("\n");
			}
			Assertions.assertTrue(foundViolationNote, failureMessage.toString());
		}
		else {
			exception.printStackTrace();
			Assertions.fail("Exception thrown by test method; see stack trace.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	protected List<RuleViolation> getRuleViolationListForEntity(Number fkFieldId) {
		return getRuleViolationListForEntityAndRuleDefinition(fkFieldId, null);
	}


	protected List<RuleViolation> getRuleViolationListForEntityAndRuleDefinition(Number fkFieldId, String ruleDefinitionName) {
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		searchForm.setLinkedTableName(getTableName());
		searchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(fkFieldId));
		searchForm.setRuleDefinitionName(ruleDefinitionName);
		return this.ruleViolationService.getRuleViolationList(searchForm);
	}


	protected void validateNoRuleViolationsForEntityAndRuleDefinition(Number fkFieldId, String ruleDefinitionName) {
		List<RuleViolation> violationList = getRuleViolationListForEntityAndRuleDefinition(fkFieldId, ruleDefinitionName);
		if (!CollectionUtils.isEmpty(violationList)) {
			Assertions.fail("Did not expected any rule violations for [" + ruleDefinitionName + "], but found: " + StringUtils.collectionToCommaDelimitedString(violationList, RuleViolation::getViolationNote));
		}
	}


	protected void validateRuleViolationExistsForEntityAndRuleDefinition(Number fkFieldId, String ruleDefinitionName, String expectedMessageRegex) {
		List<RuleViolation> violationList = getRuleViolationListForEntityAndRuleDefinition(fkFieldId, ruleDefinitionName);
		if (CollectionUtils.isEmpty(violationList)) {
			Assertions.fail("Expected Rule Violation for [" + ruleDefinitionName + "], but none were found.");
		}
		for (RuleViolation violation : violationList) {
			if (violation.getViolationNote().matches(expectedMessageRegex)) {
				return;
			}
		}
		Assertions.fail("Did not find expected Rule Violation for [" + ruleDefinitionName + "] that matched regex [" + expectedMessageRegex + "].  The following violations were found: " + StringUtils.collectionToCommaDelimitedString(violationList, RuleViolation::getViolationNote));
	}
}
