package com.clifton.oms.tests.security;


import com.clifton.oms.tests.spring.OmsIntegrationConfiguration;
import com.clifton.test.security.BaseUrlSecurityTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * The <code>OmsUrlSecurityTests</code> checks each URL is able to determine a way to lookup security
 * If @SecureMethod annotation is associated with the Method, will consider it to be configured.  Will only actually attempt URLs that do not have explicit annotations
 *
 * @author manderson
 */
@ContextConfiguration(classes = {OmsIntegrationConfiguration.class})
public class OmsUrlSecurityTests extends BaseUrlSecurityTests {

	// Temporarily excluding the Open API methods - these are being changed and security is being added.


	@Override
	protected boolean isExcludeClass(Class<?> clz) {
		return clz.getName().endsWith("ApiImpl");
	}
}
