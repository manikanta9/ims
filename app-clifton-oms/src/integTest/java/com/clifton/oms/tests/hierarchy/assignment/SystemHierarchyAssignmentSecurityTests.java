package com.clifton.oms.tests.hierarchy.assignment;


import com.clifton.oms.tests.BaseOmsIntegrationTest;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.system.group.SystemGroup;
import com.clifton.system.group.SystemGroupService;
import com.clifton.system.group.search.SystemGroupDefinitionSearchForm;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignment;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.annotation.Resource;


/**
 * {@link SystemHierarchyAssignmentSecurityTests} tests the dynamic security for {@link SystemHierarchyAssignmentService}
 * <p>
 * NOTE: Uses Trading group as example for tests - these users have write access to the Order Account Tags resource, but not the SystemGroup resource (SystemGroup is used
 * because the saveOrderAccount method does not have a request mapping)
 * <p>
 * {@link TestInstance} annotation is used to allow non-static @BeforeAll and @AfterAll methods for setting up/cleaning up test data.
 *
 * @author lnaylor
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SystemHierarchyAssignmentSecurityTests extends BaseOmsIntegrationTest {

	@Resource
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	@Resource
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SecurityAuthorizationSetupService securityAuthorizationSetupService;

	@Resource
	private SystemGroupService systemGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public void initData() {
		switchToAdminUser();

		saveTestSystemGroup();

		saveTestHierarchyCategoryWithSecurityOverride();
		saveTestHierarchyAssignmentWithSecurityOverride();
		saveTestHierarchiesWithSecurityOverride();

		saveTestHierarchyCategoryNoSecurityOverride();
		saveTestHierarchyAssignmentNoSecurityOverride();
		saveTestHierarchiesNoSecurityOverride();
	}


	@AfterAll
	public void cleanUp() {
		switchToAdminUser();

		deleteTestHierarchiesWithSecurityOverride();
		deleteTestHierarchyAssignmentWithSecurityOverride();
		deleteTestHierarchyCategoryWithSecurityOverride();

		deleteTestHierarchiesNoSecurityOverride();
		deleteTestHierarchyAssignmentNoSecurityOverride();
		deleteTestHierarchyCategoryNoSecurityOverride();

		deleteTestSystemGroup();
	}


	@AfterEach
	public void cleanUpSystemHierarchyLinks() {
		switchToAdminUser();
		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		getSystemHierarchyAssignmentService().deleteSystemHierarchyLinkList("SystemGroup", systemGroup.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSystemHierarchyLinkList_AccessAllowed_SecurityOverride() {
		switchToTraderUser();
		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("SECURITY_OVERRIDE_CATEGORY", "SECURITY_OVERRIDE_HIERARCHY");
		//test save
		Assertions.assertDoesNotThrow(() -> getSystemHierarchyAssignmentService().saveSystemHierarchyLinkList("SystemGroup", systemGroup.getId(), hierarchy.getId().toString(), "SECURITY_OVERRIDE_CATEGORY"));

		//test delete
		Assertions.assertDoesNotThrow(() -> getSystemHierarchyAssignmentService().deleteSystemHierarchyLinkList("SystemGroup", systemGroup.getId()));
	}


	@Test
	public void testSystemHierarchyLinkList_AccessDenied_NoCategoryGiven() {
		switchToTraderUser();
		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "TABLE_RESOURCE_HIERARCHY");

		//test save
		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().saveSystemHierarchyLinkList("SystemGroup", systemGroup.getId(), hierarchy.getId().toString(), ""));
	}


	@Test
	public void testDeleteSystemHierarchyLinkList_AccessDenied() {
		switchToAdminUser();
		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "TABLE_RESOURCE_HIERARCHY");
		getSystemHierarchyAssignmentService().saveSystemHierarchyLinkList("SystemGroup", systemGroup.getId(), hierarchy.getId().toString(), "TABLE_RESOURCE_CATEGORY");

		switchToTraderUser();
		//test delete
		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().deleteSystemHierarchyLinkList("SystemGroup", systemGroup.getId()));
	}


	@Test
	public void testSystemHierarchyLink_AccessAllowed_SecurityOverride() {
		switchToAdminUser();

		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		SystemHierarchy hierarchyWithOverride = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("SECURITY_OVERRIDE_CATEGORY", "SECURITY_OVERRIDE_HIERARCHY");
		SystemHierarchy newHierarchyWithOverride = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("SECURITY_OVERRIDE_CATEGORY", "NEW_HIERARCHY_SECURITY_OVERRIDE");

		switchToTraderUser();

		//test save
		Assertions.assertDoesNotThrow(() -> getSystemHierarchyAssignmentService().saveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyWithOverride.getId()));

		//test move
		Assertions.assertDoesNotThrow(() -> getSystemHierarchyAssignmentService().moveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyWithOverride.getId(), newHierarchyWithOverride.getId()));
	}


	@Test
	public void testSystemHierarchyLink_AccessDenied() {
		switchToAdminUser();

		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		SystemHierarchy hierarchyNoOverride = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "TABLE_RESOURCE_HIERARCHY");
		SystemHierarchy newHierarchyNoOverride = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "NEW_HIERARCHY_NO_OVERRIDE");

		switchToTraderUser();
		//test save
		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().saveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyNoOverride.getId()));

		switchToAdminUser();
		getSystemHierarchyAssignmentService().saveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyNoOverride.getId());

		switchToTraderUser();
		//test move
		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().moveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyNoOverride.getId(), newHierarchyNoOverride.getId()));

		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().moveSystemHierarchyLink("SystemGroup", systemGroup.getId(), hierarchyNoOverride.getId(), null));

		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().moveSystemHierarchyLink("SystemGroup", systemGroup.getId(), null, newHierarchyNoOverride.getId()));

		Assertions.assertThrows(ImsErrorResponseException.class, () -> getSystemHierarchyAssignmentService().moveSystemHierarchyLink("SystemGroup", systemGroup.getId(), null, null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void saveTestSystemGroup() {
		try {
			SystemGroup systemGroup = new SystemGroup();
			systemGroup.setName("Test");
			systemGroup.setSystemGroupDefinition(getSystemGroupService().getSystemGroupDefinitionList(new SystemGroupDefinitionSearchForm()).get(0));
			getSystemGroupService().saveSystemGroup(systemGroup);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; status already exists
		}
	}


	private void deleteTestSystemGroup() {
		SystemGroup systemGroup = getSystemGroupService().getSystemGroupByName("Test");
		if (systemGroup != null) {
			getSystemGroupService().deleteSystemGroup(systemGroup.getId());
		}
	}


	private void saveTestHierarchyCategoryWithSecurityOverride() {
		try {
			SystemHierarchyCategory category = new SystemHierarchyCategory();
			category.setName("SECURITY_OVERRIDE_CATEGORY");
			category.setDescription("Test category to be used with a security override");
			getSystemHierarchyDefinitionService().saveSystemHierarchyCategory(category);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category already exists
		}
	}


	private void saveTestHierarchyCategoryNoSecurityOverride() {
		try {
			SystemHierarchyCategory category = new SystemHierarchyCategory();
			category.setName("TABLE_RESOURCE_CATEGORY");
			category.setDescription("Test category to be used without a security override; table will be used to find security resource");
			getSystemHierarchyDefinitionService().saveSystemHierarchyCategory(category);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category already exists
		}
	}


	private void deleteTestHierarchyCategoryWithSecurityOverride() {
		try {
			SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("SECURITY_OVERRIDE_CATEGORY");
			getSystemHierarchyDefinitionService().deleteSystemHierarchyCategory(category.getId());
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category doesn't exist
		}
	}


	private void deleteTestHierarchyCategoryNoSecurityOverride() {
		try {
			SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("TABLE_RESOURCE_CATEGORY");
			getSystemHierarchyDefinitionService().deleteSystemHierarchyCategory(category.getId());
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category doesn't exist
		}
	}


	private void saveTestHierarchyAssignmentWithSecurityOverride() {
		try {
			SystemHierarchyAssignment assignment = new SystemHierarchyAssignment();
			assignment.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("SECURITY_OVERRIDE_CATEGORY"));
			assignment.setTable(getSystemSchemaService().getSystemTableByName("SystemGroup"));
			assignment.setSecurityResource(getSecurityAuthorizationSetupService().getSecurityResourceByName("Order Account Tags"));
			assignment.setMultipleValuesAllowed(true);

			getSystemHierarchyAssignmentService().saveSystemHierarchyAssignment(assignment);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; assignment already exists
		}
	}


	private void saveTestHierarchyAssignmentNoSecurityOverride() {
		try {
			SystemHierarchyAssignment assignment = new SystemHierarchyAssignment();
			assignment.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("TABLE_RESOURCE_CATEGORY"));
			assignment.setTable(getSystemSchemaService().getSystemTableByName("SystemGroup"));
			assignment.setMultipleValuesAllowed(true);

			getSystemHierarchyAssignmentService().saveSystemHierarchyAssignment(assignment);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; assignment already exists
		}
	}


	private void deleteTestHierarchyAssignmentWithSecurityOverride() {
		try {
			SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("SECURITY_OVERRIDE_CATEGORY");
			SystemHierarchyAssignment assignment = getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentByTableAndCategory("SystemGroup", category.getId());
			if (assignment != null) {
				getSystemHierarchyAssignmentService().deleteSystemHierarchyAssignment(assignment.getId());
			}
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category and assignment do not exist
		}
	}


	private void deleteTestHierarchyAssignmentNoSecurityOverride() {
		try {
			SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("TABLE_RESOURCE_CATEGORY");
			SystemHierarchyAssignment assignment = getSystemHierarchyAssignmentService().getSystemHierarchyAssignmentByTableAndCategory("SystemGroup", category.getId());
			if (assignment != null) {
				getSystemHierarchyAssignmentService().deleteSystemHierarchyAssignment(assignment.getId());
			}
		}
		catch (ImsErrorResponseException e) {
			//do nothing; category and assignment do not exist
		}
	}


	private void saveTestHierarchiesWithSecurityOverride() {
		try {
			SystemHierarchy hierarchy = new SystemHierarchy();
			hierarchy.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("SECURITY_OVERRIDE_CATEGORY"));
			hierarchy.setName("SECURITY_OVERRIDE_HIERARCHY");
			getSystemHierarchyDefinitionService().saveSystemHierarchy(hierarchy);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; hierarchy already exists
		}

		try {
			SystemHierarchy newHierarchy = new SystemHierarchy();
			newHierarchy.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("SECURITY_OVERRIDE_CATEGORY"));
			newHierarchy.setName("NEW_HIERARCHY_SECURITY_OVERRIDE");
			getSystemHierarchyDefinitionService().saveSystemHierarchy(newHierarchy);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; hierarchy already exists
		}
	}


	private void saveTestHierarchiesNoSecurityOverride() {
		try {
			SystemHierarchy hierarchy = new SystemHierarchy();
			hierarchy.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("TABLE_RESOURCE_CATEGORY"));
			hierarchy.setName("TABLE_RESOURCE_HIERARCHY");
			getSystemHierarchyDefinitionService().saveSystemHierarchy(hierarchy);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; hierarchy already exists
		}

		try {
			SystemHierarchy newHierarchy = new SystemHierarchy();
			newHierarchy.setCategory(getSystemHierarchyDefinitionService().getSystemHierarchyCategoryByName("TABLE_RESOURCE_CATEGORY"));
			newHierarchy.setName("NEW_HIERARCHY_NO_OVERRIDE");
			getSystemHierarchyDefinitionService().saveSystemHierarchy(newHierarchy);
		}
		catch (ImsErrorResponseException e) {
			//do nothing; hierarchy already exists
		}
	}


	private void deleteTestHierarchiesWithSecurityOverride() {
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("SECURITY_OVERRIDE_CATEGORY", "SECURITY_OVERRIDE_HIERARCHY");
		if (hierarchy != null) {
			getSystemHierarchyDefinitionService().deleteSystemHierarchy(hierarchy.getId());
		}

		hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("SECURITY_OVERRIDE_CATEGORY", "NEW_HIERARCHY_SECURITY_OVERRIDE");
		if (hierarchy != null) {
			getSystemHierarchyDefinitionService().deleteSystemHierarchy(hierarchy.getId());
		}
	}


	private void deleteTestHierarchiesNoSecurityOverride() {
		SystemHierarchy hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "TABLE_RESOURCE_HIERARCHY");
		if (hierarchy != null) {
			getSystemHierarchyDefinitionService().deleteSystemHierarchy(hierarchy.getId());
		}

		hierarchy = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded("TABLE_RESOURCE_CATEGORY", "NEW_HIERARCHY_NO_OVERRIDE");
		if (hierarchy != null) {
			getSystemHierarchyDefinitionService().deleteSystemHierarchy(hierarchy.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SecurityAuthorizationSetupService getSecurityAuthorizationSetupService() {
		return this.securityAuthorizationSetupService;
	}


	public void setSecurityAuthorizationSetupService(SecurityAuthorizationSetupService securityAuthorizationSetupService) {
		this.securityAuthorizationSetupService = securityAuthorizationSetupService;
	}


	public SystemGroupService getSystemGroupService() {
		return this.systemGroupService;
	}


	public void setSystemGroupService(SystemGroupService systemGroupService) {
		this.systemGroupService = systemGroupService;
	}
}
