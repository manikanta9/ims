package com.clifton.oms.tests.order.allocation.workflow;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.oms.tests.BaseOmsIntegrationTest;
import com.clifton.oms.tests.workflow.WorkflowTransitioner;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.OrderManagementActivityService;
import com.clifton.order.management.activity.OrderManagementDateRevisionTypes;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class OrderAllocationWorkflowTests extends BaseOmsIntegrationTest {

	@Resource
	private OrderAllocationService orderAllocationService;

	@Resource
	private OrderManagementActivityService orderManagementActivityService;

	@Resource
	private RuleViolationService ruleViolationService;

	@Resource
	private WorkflowTransitioner workflowTransitioner;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSuccessfulOrderAllocation_Pending_Approved() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
		try {
			orderAllocation = transitionApprove(orderAllocation, false, true);
			Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
		}
		finally {
			cancelOrderAllocation(orderAllocation);
		}
	}


	@Test
	public void testSuccessfulOrderAllocation_Pending_ReviseDates_StayApproved() {
		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
		try {
			orderAllocation = transitionApprove(orderAllocation, false, true);
			Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
			orderAllocation = reviseDatesWithCommand(orderAllocation, OrderManagementDateRevisionTypes.RECALCULATE, null, OrderManagementDateRevisionTypes.SAME_DAY, null, true);
			Assertions.assertTrue(DateUtils.isEqualWithoutTime(new Date(), orderAllocation.getTradeDate()));
			// Same day should be equal unless it's not a valid settlement date (i.e. 7/19 for JPY is not valid).
			Assertions.assertTrue(DateUtils.isDateAfterOrEqual(orderAllocation.getSettlementDate(), DateUtils.clearTime(new Date())));
			Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
		}
		finally {
			cancelOrderAllocation(orderAllocation);
		}
	}


	@Test
	public void testSuccessfulOrderAllocation_Pending_ReviseDates_Invalid_ReviseDates_Approved() {

		OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
		try {
			orderAllocation = transitionApprove(orderAllocation, false, true);
			Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
			Date newTradeDate = DateUtils.clearTime(DateUtils.addWeekDays(new Date(), 3));
			orderAllocation = reviseDates(orderAllocation, newTradeDate, newTradeDate);
			Assertions.assertTrue(DateUtils.isEqualWithoutTime(newTradeDate, orderAllocation.getTradeDate()));
			Assertions.assertTrue(DateUtils.isEqualWithoutTime(newTradeDate, orderAllocation.getSettlementDate()));
			Assertions.assertEquals("Invalid", orderAllocation.getWorkflowState().getName());

			newTradeDate = DateUtils.clearTime(new Date());
			orderAllocation = reviseDatesWithCommand(orderAllocation, OrderManagementDateRevisionTypes.OVERRIDE, newTradeDate, OrderManagementDateRevisionTypes.OVERRIDE, newTradeDate, true);
			Assertions.assertTrue(DateUtils.isEqualWithoutTime(newTradeDate, orderAllocation.getTradeDate()));
			// Same day should be equal unless it's not a valid settlement date (i.e. 7/19 for JPY is not valid).
			Assertions.assertTrue(DateUtils.isDateAfterOrEqual(orderAllocation.getSettlementDate(), newTradeDate));
			Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
		}
		finally {
			cancelOrderAllocation(orderAllocation);
		}
	}


	@Test
	public void testSuccessfulOrderAllocation_PendingOpsApproval_Pending_Approved() {
		try {
			this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
			OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
			try {
				orderAllocation = transitionApprove(orderAllocation, false, true);
				Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
			}
			finally {
				cancelOrderAllocation(orderAllocation);
			}
		}
		finally {
			this.userUtils.removeUserFromSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
		}
	}


	@Test
	public void testSuccessfulOrderAllocation_PendingOpsApproval_Pending_Approved_ReturnToPM() {
		try {
			this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
			OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
			try {
				orderAllocation = transitionApprove(orderAllocation, false, true);
				Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
				this.workflowTransitioner.transitionIfPresent("Return Order (PM)", OrderAllocation.TABLE_NAME, orderAllocation.getId());
				// Should not auto approve
				Assertions.assertEquals("Pending", this.orderAllocationService.getOrderAllocation(orderAllocation.getId()).getWorkflowState().getName());
			}
			finally {
				cancelOrderAllocation(orderAllocation);
			}
		}
		finally {
			this.userUtils.removeUserFromSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
		}
	}


	@Test
	public void testSuccessfulOrderAllocation_PendingOpsApproval_Pending_Approved_Reject() {
		try {
			this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
			OrderAllocation orderAllocation = this.orderBaseUtils.saveCurrencyOrderAllocation("revpintl", true, "JPY", "USD", new BigDecimal("1000000"), DateUtils.toDate("07/06/2021"), null);
			try {
				orderAllocation = transitionApprove(orderAllocation, false, true);
				Assertions.assertEquals("Approved", orderAllocation.getWorkflowState().getName());
				this.workflowTransitioner.transitionIfPresent("Reject", OrderAllocation.TABLE_NAME, orderAllocation.getId());
				Assertions.assertEquals("Rejected", this.orderAllocationService.getOrderAllocation(orderAllocation.getId()).getWorkflowState().getName());
			}
			finally {
				cancelOrderAllocation(orderAllocation);
			}
		}
		finally {
			this.userUtils.removeUserFromSecurityGroup(ImsTestProperties.TEST_USER_ADMIN, "Operations");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderAllocation transitionApprove(OrderAllocation orderAllocation, boolean opsOnly, boolean ignoreAllViolations) {
		try {
			if (ignoreAllViolations && StringUtils.isEqualIgnoreCase("Invalid", orderAllocation.getWorkflowState().getName())) {
				RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
				ruleViolationSearchForm.setLinkedTableName(OrderAllocation.TABLE_NAME);
				ruleViolationSearchForm.setLinkedFkFieldId(orderAllocation.getId());
				List<RuleViolation> ruleViolationList = this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm);
				if (!CollectionUtils.isEmpty(ruleViolationList)) {
					for (RuleViolation violation : CollectionUtils.getIterable(ruleViolationList)) {
						if (violation.getRuleAssignment().getRuleDefinition().isIgnorable()) {
							violation.setIgnored(true);
							if (violation.getRuleAssignment().getRuleDefinition().isIgnoreNoteRequired()) {
								violation.setIgnoreNote("Testing - Ignore Violations");
							}
							this.ruleViolationService.saveRuleViolation(violation);
						}
					}
					if (this.workflowTransitioner.transitionIfPresent("Validate", OrderAllocation.TABLE_NAME, orderAllocation.getId())) {
						orderAllocation = this.orderAllocationService.getOrderAllocation(orderAllocation.getId());
					}
				}
			}

			if (StringUtils.isEqualIgnoreCase("Pending Ops Approval", orderAllocation.getWorkflowState().getName())) {
				this.switchToOpsUser();
				if (this.workflowTransitioner.transitionIfPresent("Approve (Ops)", OrderAllocation.TABLE_NAME, orderAllocation.getId())) {
					orderAllocation = this.orderAllocationService.getOrderAllocation(orderAllocation.getId());
				}
			}
			if (!opsOnly && StringUtils.isEqualIgnoreCase("Pending", orderAllocation.getWorkflowState().getName())) {
				this.switchToPortfolioManagerUser();
				if (this.workflowTransitioner.transitionIfPresent("Approve", OrderAllocation.TABLE_NAME, orderAllocation.getId())) {
					orderAllocation = this.orderAllocationService.getOrderAllocation(orderAllocation.getId());
				}
			}
		}
		finally {
			this.switchToAdminUser();
		}
		return orderAllocation;
	}


	private OrderAllocation reviseDatesWithCommand(OrderAllocation orderAllocation, OrderManagementDateRevisionTypes tradeDateAdjustment, Date tradeDateOverride, OrderManagementDateRevisionTypes settlementDateAdjustment, Date settlementDateOverride, boolean autoAdjustForBusinessDays) {
		OrderManagementActivityCommand activityCommand = new OrderManagementActivityCommand();
		activityCommand.setOrderAllocationIds(new Long[]{orderAllocation.getId()});
		activityCommand.setReviseDates(true);
		activityCommand.setTradeDateAdjustment(tradeDateAdjustment);
		activityCommand.setOverrideTradeDate(tradeDateOverride);
		activityCommand.setSettlementDateAdjustment(settlementDateAdjustment);
		activityCommand.setOverrideSettlementDate(settlementDateOverride);
		activityCommand.setAutoAdjustReviseDatesForBusinessDays(autoAdjustForBusinessDays);
		this.orderManagementActivityService.processOrderManagementReviseDatesForOrderAllocations(activityCommand);

		return this.orderAllocationService.getOrderAllocation(orderAllocation.getId());
	}


	private OrderAllocation reviseDates(OrderAllocation orderAllocation, Date newTradeDate, Date newSettlementDate) {
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(OrderAllocation.TABLE_NAME, orderAllocation.getId());

		WorkflowTransition reviseDates = null;
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitions)) {
			if ("Revise Dates".equalsIgnoreCase(transition.getName())) {
				reviseDates = transition;
				break;
			}
		}
		Assertions.assertNotNull(reviseDates, "Cannot find Revise Dates transition from " + orderAllocation.getWorkflowState().getName());
		return this.orderAllocationService.saveOrderAllocationWithRevisedDates(orderAllocation.getId(), ObjectUtils.coalesce(newTradeDate, orderAllocation.getTradeDate()), ObjectUtils.coalesce(newSettlementDate, orderAllocation.getSettlementDate()), reviseDates.getId(), true);
	}
}
