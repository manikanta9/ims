package com.clifton.oms.tests.order.setup;

import com.clifton.order.setup.OrderOpenCloseType;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@Component
public class OrderSetupUtils {


	@Resource
	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderType getOrderTypeCurrency(boolean use11a) {
		return this.orderSetupService.getOrderTypeByName("Currency" + (use11a ? " 11A" : ""));
	}


	public OrderType getOrderTypeStocks() {
		return this.orderSetupService.getOrderTypeByName("Stocks");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderOpenCloseType getOrderOpenCloseTypeBuy() {
		return this.orderSetupService.getOrderOpenCloseTypeByName("Buy");
	}


	public OrderOpenCloseType getOrderOpenCloseTypeSell() {
		return this.orderSetupService.getOrderOpenCloseTypeByName("Sell");
	}
}
