package com.clifton.oms.tests.order.base;

import com.clifton.core.util.StringUtils;
import com.clifton.oms.tests.order.setup.OrderSetupUtils;
import com.clifton.oms.tests.order.shared.OrderSharedUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.setup.OrderOpenCloseType;
import com.clifton.order.shared.OrderSecurity;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author AbhinayaM
 */

@Component
public class OrderBaseUtils {

	@Resource
	private OrderAllocationService orderAllocationService;

	@Resource
	private OrderSharedUtils orderSharedUtils;

	@Resource
	private OrderSetupUtils orderSetupUtils;

	////////////////////////////////////////////////////////////////////////////
	////////            Currency Order Allocations                      ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocation saveCurrencyOrderAllocationWithExecutingBroker(String accountShortName, boolean buy, String givenCurrencyCode, String settlementCurrencyCode, BigDecimal givenAmount, Date tradeDate, Date settlementDate, String executingBrokerCompanyName) {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setOrderType(this.orderSetupUtils.getOrderTypeCurrency(false));
		return saveCurrencyOrderAllocationImpl(orderAllocation, accountShortName, buy, givenCurrencyCode, settlementCurrencyCode, givenAmount, tradeDate, settlementDate, executingBrokerCompanyName);
	}


	public OrderAllocation saveCurrencyOrderAllocation(String accountShortName, boolean buy, String givenCurrencyCode, String settlementCurrencyCode, BigDecimal givenAmount, Date tradeDate, Date settlementDate) {
		return saveCurrencyOrderAllocationWithExecutingBroker(accountShortName, buy, givenCurrencyCode, settlementCurrencyCode, givenAmount, tradeDate, settlementDate, null);
	}


	public OrderAllocation saveCurrency11AOrderAllocation(String accountShortName, boolean buy, String givenCurrencyCode, String settlementCurrencyCode, BigDecimal givenAmount, Date tradeDate, Date settlementDate, String executingBrokerCompanyName) {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setOrderType(this.orderSetupUtils.getOrderTypeCurrency(true));
		return saveCurrencyOrderAllocationImpl(orderAllocation, accountShortName, buy, givenCurrencyCode, settlementCurrencyCode, givenAmount, tradeDate, settlementDate, executingBrokerCompanyName);
	}


	private OrderAllocation saveCurrencyOrderAllocationImpl(OrderAllocation orderAllocation, String accountShortName, boolean buy, String givenCurrencyCode, String settlementCurrencyCode, BigDecimal givenAmount, Date tradeDate, Date settlementDate, String executingBrokerCompanyName) {
		orderAllocation.setClientAccount(this.orderSharedUtils.getClientAccountByShortName(accountShortName));
		orderAllocation.setOpenCloseType(getOpenCloseType(buy));
		orderAllocation.setSecurity(this.orderSharedUtils.getOrderSecurityByTicker(givenCurrencyCode, true));
		orderAllocation.setSettlementCurrency(this.orderSharedUtils.getOrderSecurityByTicker(settlementCurrencyCode, true));
		orderAllocation.setTradeDate(tradeDate);
		orderAllocation.setSettlementDate(settlementDate);
		orderAllocation.setExchangeRateToSettlementCurrency(BigDecimal.ONE);
		orderAllocation.setAccountingNotional(givenAmount);
		if (!StringUtils.isEmpty(executingBrokerCompanyName)) {
			orderAllocation.setExecutingBrokerCompany(this.orderSharedUtils.getOrderCompanyByName(executingBrokerCompanyName));
		}

		return saveOrderAllocationImpl(orderAllocation);
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Stock Order Allocations                      ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocation saveStockOrderAllocationForExchange(String accountShortName, boolean buy, String exchangeCode, BigDecimal quantity, Date tradeDate) {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setOrderType(this.orderSetupUtils.getOrderTypeStocks());
		orderAllocation.setClientAccount(this.orderSharedUtils.getClientAccountByShortName(accountShortName));
		orderAllocation.setOpenCloseType(getOpenCloseType(buy));
		OrderSecurity orderSecurity = this.orderSharedUtils.getOrderSecurityAnyForExchange(exchangeCode, true);
		orderAllocation.setSecurity(orderSecurity);
		orderAllocation.setSettlementCurrency(this.orderSharedUtils.getOrderSecurityByTicker(orderSecurity.getCurrencyCode(), true));
		orderAllocation.setQuantityIntended(quantity);
		orderAllocation.setTradeDate(tradeDate);
		return saveOrderAllocationImpl(orderAllocation);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	private OrderAllocation saveOrderAllocationImpl(OrderAllocation orderAllocation) {
		if (orderAllocation.getHoldingAccount() == null && orderAllocation.getClientAccount().isHoldingAccount()) {
			orderAllocation.setHoldingAccount(orderAllocation.getClientAccount());
		}
		return this.orderAllocationService.saveOrderAllocation(orderAllocation);
	}


	private OrderOpenCloseType getOpenCloseType(boolean buy) {
		return buy ? this.orderSetupUtils.getOrderOpenCloseTypeBuy() : this.orderSetupUtils.getOrderOpenCloseTypeSell();
	}
}


