package com.clifton.oms.tests.system.query;

import com.clifton.core.util.date.DateUtils;
import com.clifton.oms.tests.spring.OmsIntegrationConfiguration;
import com.clifton.test.system.query.BaseSystemQueryServiceTests;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {OmsIntegrationConfiguration.class})
public class OmsSystemQueryServiceTests extends BaseSystemQueryServiceTests {


	static {
		addQueryParameterValueOverride("Seattle Account Data for Import", "lastUpdatedAfterDate", DateUtils.fromDateShort(new Date()));
	}
}
