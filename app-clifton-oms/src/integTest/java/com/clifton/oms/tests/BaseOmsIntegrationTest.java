package com.clifton.oms.tests;

import com.clifton.oms.tests.order.base.OrderBaseUtils;
import com.clifton.oms.tests.order.setup.OrderSetupUtils;
import com.clifton.oms.tests.order.shared.OrderSharedUtils;
import com.clifton.oms.tests.spring.OmsIntegrationConfiguration;
import com.clifton.oms.tests.workflow.WorkflowTransitioner;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.test.util.templates.ImsTestProperties;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;


@ContextConfiguration(classes = {OmsIntegrationConfiguration.class})
public abstract class BaseOmsIntegrationTest extends BaseIntegrationTest {

	@Resource
	protected OrderSetupUtils orderSetupUtils;

	@Resource
	protected OrderSharedUtils orderSharedUtils;

	@Resource
	protected OrderBaseUtils orderBaseUtils;

	@Resource
	protected WorkflowTransitioner workflowTransitioner;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ops is Test User 2 (see after restore sql in OMS)
	 */
	public void switchToOpsUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD);
	}


	/**
	 * PM is Test User 3 (see after restore sql in OMS)
	 */
	public void switchToPortfolioManagerUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_3, ImsTestProperties.TEST_USER_3_PASSWORD);
	}


	public void switchToTraderUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_TRADER_USER, ImsTestProperties.TEST_TRADER_USER_PASSWORD);
	}


	public void switchToAdminUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void cancelOrderAllocation(OrderAllocation orderAllocation) {
		this.workflowTransitioner.transitionForTests("Cancel", "Canceled", OrderAllocation.TABLE_NAME, orderAllocation.getId());
	}
}
