package com.clifton.oms.app;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.system.validation.DataAccessExceptionConverter;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OmsProjectBasicTests extends BasicProjectTests {

	@Resource
	private DataAccessExceptionConverter dataAccessExceptionConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectPrefix() {
		return "oms-app";
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.boot.test.context.SpringBootTest");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Api");
		basicProjectAllowedSuffixNamesList.add("ApiClient");
		return basicProjectAllowedSuffixNamesList;
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.servlet.http.HttpServletRequest");
		imports.add("org.springframework.beans.factory.BeanFactory");
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.http.converter.");
		imports.add("org.springframework.security.oauth2.http.converter.");
		imports.add("org.springframework.web.");
		imports.add("org.springframework.boot.actuate.autoconfigure.");
		imports.add("org.springframework.boot.autoconfigure.");
		imports.add("org.springframework.boot.builder.SpringApplicationBuilder");
		imports.add("org.springframework.context.annotation.");
	}


	@Override
	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("dataAccessExceptionConverter", this.dataAccessExceptionConverter);
	}
}
