-- WARNING: This file gets executed on PROD. It is run immediately before DB migration is executed (dbMigrateRestore & dbMigrate). The only current use case is to rename migration files.

IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'MigrationModuleVersion')
	BEGIN
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0009-ims-shared-lookup-data-queries.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0001-ims-shared-lookup-data-queries.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0010-ims-companies-query.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0002-ims-companies-query.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0011-ims-securities-query.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0003-ims-securities-query.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0012-seattle-accounts-query.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0004-seattle-accounts-query.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0013-load-data-action.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0005-load-data-action.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0014-order-destinations.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0007-order-destinations.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0015-order-rules.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0008-order-rules.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0015-sharepoint-rules.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0008-sharepoint-rules.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0016-ims-exchange-rate-lookup.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0009-ims-exchange-rate-lookup.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0017-pershing-execution-upload-converter.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0010-pershing-execution-upload-converter.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0018-pershing-fx-execution-file.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0011-pershing-fx-execution-file.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0019-apx-trade-posting-file.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0012-apx-trade-posting-file.xml'
		UPDATE env.MigrationModuleVersion
		SET MigrationModuleName = 'app-clifton-oms',
			MigrationPath       = 'app-clifton-oms-01/0020-order-account-tags.xml'
		WHERE MigrationPath = 'app-clifton-oms-test-data-01/0013-order-account-tags.xml'

		DELETE FROM env.MigrationModuleVersion WHERE MigrationModuleName = 'app-clifton-oms-test-data'

	END
