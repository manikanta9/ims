-- Temporary to remove existing dupes until OMS-105 is deployed to Prod
DELETE a
FROM OrderAccount a
WHERE EXISTS(
			  SELECT *
			  FROM OrderAccount a2
			  WHERE a.OrderAccountID > a2.OrderAccountID
				AND a.MasterIdentifier = a2.MasterIdentifier
		  )
DELETE c
FROM OrderCompany c
WHERE EXISTS(
			  SELECT *
			  FROM OrderCompany c2
			  WHERE c.OrderCompanyID > c2.OrderCompanyID
				AND c.MasterIdentifier = c2.MasterIdentifier
		  )


