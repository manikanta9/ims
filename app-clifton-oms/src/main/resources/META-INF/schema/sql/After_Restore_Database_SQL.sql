IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';


-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group (groups may not be consistent across applications)
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Ops Test User', 'Operations|'
UNION
-- PW: password3
SELECT 'imstestuser3', 'PM Test User', 'Portfolio Managers|'
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', NULL
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', NULL
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trading|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', NULL

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)

INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================


UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';


-- DISABLE ALL WORKFLOW TASK DEFINITIONS LOCAL ONLY
IF ('@application.environment.level@' = 'LOCAL')
	BEGIN
		UPDATE WorkflowTaskDefinition
		SET IsActive = 0
	END
	-- ON DEV, DISABLE THE WORKFLOW AND SECURITY WORKFLOW TASK DEFINITIONS SO WE CAN ADD ADDITIONAL TEST TRANSITIONS AND MODIFY TEST USER ACCESS WITHOUT TASKS
ELSE
	IF ('@application.environment.level@' = 'DEV')
		BEGIN
			UPDATE WorkflowTaskDefinition
			SET IsActive = 0
			WHERE DefinitionName IN ('Workflow Review', 'Security User Access Change', 'Security Group Access Change')
		END


-- DISABLE ALL BATCH JOBS
UPDATE BatchJob
SET IsEnabled = 0

-- Override Mapping Values for FX Connect testing in QA/UAT
UPDATE e
SET MappingValue =
		CASE
			WHEN e.FkFieldLabel = 'State Street Bank' THEN 'aptestbank'
			WHEN e.FkFieldLabel = 'Bank of New York Mellon' THEN 'aptestbanka'
			WHEN e.FkFieldLabel = 'Brown Brothers Harriman & Co' THEN 'demobank1'
			WHEN e.FkFieldLabel = 'Northern Trust Securities, Inc.' THEN 'demobank2'
			ELSE e.MappingValue
		END
FROM SystemFieldMappingEntry e
	 INNER JOIN SystemFieldMapping m ON e.SystemFieldMappingID = m.SystemFieldMappingID
WHERE m.MappingName = 'FX Connect Broker Mappings'


-- Allow Traders to Approve their own Order Allocations (in QA)

DECLARE @BeanID INT
SELECT @BeanID = (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Current User Is In Group Trading')


INSERT INTO SystemConditionEntry (ParentSystemConditionEntryID, SystemBeanID, ConditionEntryType, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT e.SystemConditionEntryID, @BeanID, 'CONDITION', 0, GETDATE(), 0, GETDATE()
FROM SystemCondition c
	 INNER JOIN SystemConditionEntry e ON c.SystemConditionEntryID = e.SystemConditionEntryID
WHERE SystemConditionName = 'PM Approve Order Allocation Condition'
  AND NOT EXISTS(SELECT * FROM SystemConditionEntry WHERE ParentSystemConditionEntryID = e.SystemConditionEntryID AND SystemBeanID = @BeanID)

-- Disable all Batch Sending (update to do nothing)
DECLARE @DefaultSendingBeanName VARCHAR(50) = 'Do Nothing Order Batch Sender (Success)'
DECLARE @DefaultSendingBeanID   INT
SELECT @DefaultSendingBeanID = SystemBeanID
FROM SystemBean
WHERE SystemBeanName = @DefaultSendingBeanName

UPDATE OrderDestination
SET CustomColumns = JSON_MODIFY(JSON_MODIFY(CustomColumns, '$."Batch Sending System Bean".value', @DefaultSendingBeanID), '$."Batch Sending System Bean".text',
								@DefaultSendingBeanName)
FROM OrderDestination
WHERE JSON_VALUE(CustomColumns, '$."Batch Sending System Bean".text') <> @DefaultSendingBeanName


-- Even though we switch these to do nothing, put in the UAT drive in case we need to switch to it for tests
DECLARE @BatchSenderBeanGroupID INT
SELECT @BatchSenderBeanGroupID = SystemBeanGroupID
FROM SystemBeanGroup
WHERE SystemBeanGroupName = 'Order Batch Sender'

UPDATE p
SET Value = '\\paraport.com\Departments\UAT\Trading\manual posts for Ops\manual Advent',
	Text  = '\\paraport.com\Departments\UAT\Trading\manual posts for Ops\manual Advent'
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
	 INNER JOIN SystemBeanType bt ON b.SystemBeanTypeID = bt.SystemBeanTypeID
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
WHERE bt.SystemBeanGroupID = @BatchSenderBeanGroupID
  AND pt.SystemBeanPropertyTypeName = 'Destination Network Path'
  AND b.SystemBeanName = 'APX Manual Post for Ops Network File Share'

-- Street FX Confirmation File
-- NDM01 = prod, NDM-2 = UAT
UPDATE p
SET Value = '.csv.NDM02'
  , TEXT  = '.csv.NDM02'
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
	 INNER JOIN SystemBeanType bt ON b.SystemBeanTypeID = bt.SystemBeanTypeID
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
	 INNER JOIN SystemBeanGroup bg ON bt.SystemBeanGroupID = bg.SystemBeanGroupID
WHERE bt.SystemBeanTypeName = 'Basic Order Batch DataTable to File Converter'
  AND b.SystemBeanName = 'Street FX GTSS File Converter'
  AND pt.SystemBeanPropertyTypeName = 'Override Extension'


-- Add Test to Email Subject and Body
UPDATE p
SET Value = 'TESTING: ' + Value,
	Text  = 'TESTING: ' + Text
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
	 INNER JOIN SystemBeanType bt ON b.SystemBeanTypeID = bt.SystemBeanTypeID
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
WHERE bt.SystemBeanGroupID = @BatchSenderBeanGroupID
  AND pt.SystemBeanPropertyTypeName = 'Subject Template'

UPDATE p
SET Value = '<br><br><div style="text-align: left;">Warning: This email was sent from a testing environment</div>' + Value,
	Text  = '<br><br><div style="text-align: left;">Warning: This email was sent from a testing environment</div>' + Text
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
	 INNER JOIN SystemBeanType bt ON b.SystemBeanTypeID = bt.SystemBeanTypeID
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
WHERE bt.SystemBeanGroupID = @BatchSenderBeanGroupID
  AND pt.SystemBeanPropertyTypeName = 'Body Template'

-- Don't actual send emails out to external parties from a test environment
UPDATE p
SET Value = 'noreply@paraport.com',
	Text  = 'noreply@paraport.com'
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
	 INNER JOIN SystemBeanType bt ON b.SystemBeanTypeID = bt.SystemBeanTypeID
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
WHERE bt.SystemBeanGroupID = @BatchSenderBeanGroupID
  AND pt.SystemBeanPropertyTypeName = 'To Email Address(es)'


-- Obfuscate Account/Client Names
UPDATE a
SET AccountName = SUBSTRING(AccountName, 0, 4) + 'XXXXXXXX'
FROM OrderAccount a


UPDATE c
SET CompanyName = a.AccountName + ' (' + a.AccountShortName + ')'
FROM OrderAccount a
	 INNER JOIN OrderCompany c ON a.ClientCompanyID = c.OrderCompanyID
