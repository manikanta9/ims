// disable accidental Browser back button or refresh for non-local instances
TCG.enableConfirmBeforeLeavingWindow();


/*
 * Apply constants
 */
TCG.ApplicationName = TCG.PageConfiguration.applicationName;
TCG.ExternalApplicationNames = TCG.PageConfiguration.externalApplicationNames;
TCG.websocket.enabled = TCG.PageConfiguration.websocket.enabled;
TCG.websocket.endpoint = TCG.PageConfiguration.websocket.endpoint;

/*
 * Initialize application
 */
TCG.app.app = new TCG.app.Application({
	getTopBar: function() {
		return {
			html: '<div class="x-panel-header">' +
					'<table cellspacing="0" cellpadding="0" width="100%" class="app-header">' +
					'<tr>' +
					'<td width="23" qtip="Parametric Order Management System"><img src="core/images/icons/parametric.png" height="16" border="0" /></td>' +
					'<td style="FONT-WEIGHT: bold; COLOR: #646464; FONT-FAMILY: Arial;">OMS Application</td>' +
					'<td align="right" id="welcomeMessage">&nbsp;</td>' +
					'</tr>' +
					'</table>' +
					'</div>'
		};
	},
	getBody: function() {
		let suffix = '';
		const instanceText = TCG.PageConfiguration.instance.text;
		if (TCG.isNotBlank(instanceText)) {
			suffix = `<br /><br /><br /><div style="WIDTH: 100%; TEXT-ALIGN: center; COLOR: ${TCG.PageConfiguration.instance.color}; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: ${TCG.PageConfiguration.instance.size};">${instanceText}</div>`;
		}
		return {
			html: TCG.trimWhitespace(`
				<div style="WIDTH: 100%; TEXT-ALIGN: center; PADDING-TOP: 50px">
					<div><img src="core/images/logo/Parametric-120px-height.png" border="0" /></div>
					<div style="PADDING-TOP: 20px; COLOR: #646464; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 20pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">Order Management System</div>
					</div>
					<div style="COLOR: #34abdb; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 16pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">${TCG.PageConfiguration.instance.version}</div>
					</div>
					${suffix}
				</div>
			`)
		};
	},
	getMenuBar: function() {
		const mb = new Ext.Toolbar({height: 40});
		mb.render(Ext.getBody());

		mb.add(
				{
					id: 'myNotifications',
					iconCls: 'flag-red',
					text: '(0)',
					tooltip: 'View My Notifications<br />(count includes notifications of Medium and higher priority)',
					hidden: true,
					handler: function() {
						TCG.createComponent('Clifton.notification.NotificationListWindow');
					}
				}, '-',
				{
					text: 'Orders',
					menu: {
						items: [
							{
								text: 'Order Allocation Entry',
								iconCls: 'shopping-cart',
								menu: {
									items: [
										{
											text: 'Currency Order',
											iconCls: 'currency',
											handler: function() {
												TCG.createComponent('Clifton.order.allocation.OrderAllocationWindow', {orderType: 'Currency'});
											}
										},
										{
											text: 'Stock Order',
											iconCls: 'shopping-cart',
											handler: function() {
												TCG.createComponent('Clifton.order.allocation.OrderAllocationWindow', {orderType: 'Stocks'});
											}
										}
									]
								}
							},
							{
								text: 'Order Allocation Import',
								iconCls: 'shopping-cart',
								menu: {
									items: [
										{
											text: 'Currency Order Import',
											iconCls: 'currency',
											handler: function() {
												TCG.createComponent('Clifton.order.allocation.upload.CurrencyOrderAllocationUploadWindow');
											}
										},
										{
											text: 'Stock Order Import',
											iconCls: 'shopping-cart',
											handler: function() {
												TCG.createComponent('Clifton.order.allocation.upload.StockOrderAllocationUploadWindow');
											}
										}
									]
								}
							},
							'-',
							{
								text: 'Trading Blotter',
								iconCls: 'blotter',
								handler: function() {
									TCG.createComponent('Clifton.order.management.blotter.OrderTradingBlotterWindow');
								}
							},
							{
								text: 'PM Blotter',
								iconCls: 'blotter',
								handler: function() {
									TCG.createComponent('Clifton.order.management.blotter.OrderPortfolioManagerBlotterWindow');
								}
							},
							{
								text: 'Operations Blotter',
								iconCls: 'blotter',
								handler: function() {
									TCG.createComponent('Clifton.order.management.blotter.OrderOperationsBlotterWindow');
								}
							}
							, '-',
							{
								text: 'Order History',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.order.OrderHistoryWindow');
								}
							},
							{
								text: 'Order Batches',
								iconCls: 'list',
								handler: function() {
									TCG.createComponent('Clifton.order.batch.OrderBatchListWindow');
								}
							}, '-', {
								text: 'Settings',
								iconCls: 'config',
								menu: {
									items: [
										{
											text: 'Order Destinations',
											iconCls: 'repo',
											handler: function() {
												TCG.createComponent('Clifton.order.management.setup.destination.DestinationSetupWindow');
											}
										},
										{
											text: 'System Field Mappings',
											iconCls: 'tools',
											handler: function() {
												TCG.createComponent('Clifton.system.fieldmapping.SystemFieldMappingSetupWindow');
											}
										},
										{
											text: 'Order Rules',
											iconCls: 'rule',
											handler: function() {
												TCG.createComponent('Clifton.order.management.setup.rule.RuleSetupWindow');
											}
										},
										{
											text: 'Order Setup',
											iconCls: 'tools',
											handler: function() {
												TCG.createComponent('Clifton.order.setup.OrderSetupWindow');
											}
										}
									]
								}
							}
						]
					}
				}, '-',

				{
					text: 'Data',
					menu: {
						items: [{
							text: 'Query Exports',
							iconCls: 'excel',
							handler: function() {
								TCG.createComponent('Clifton.system.query.QueryExportListWindow');
							}
						}, '-', {
							text: 'Companies',
							iconCls: 'business',
							handler: function() {
								TCG.createComponent('Clifton.order.shared.company.CompanyListWindow');
							}
						}, {
							text: 'Accounts',
							iconCls: 'account',
							handler: function() {
								TCG.createComponent('Clifton.order.shared.account.AccountListWindow');
							}
						}, {
							text: 'Securities',
							iconCls: 'stock-chart',
							handler: function() {
								TCG.createComponent('Clifton.order.shared.security.SecurityListWindow');
							}
						}, '-', {
							text: 'OMS Dashboard',
							iconCls: 'chart-bar',
							handler: function() {
								TCG.createComponent('Clifton.order.OmsDashboardWindow');
							}
						}, {
							text: 'Data Sync',
							iconCls: 'run',
							tooltip: 'Run on demand synchronization to update data from master systems.',
							handler: function() {
								TCG.createComponent('Clifton.order.shared.OrderSharedDataAdministrationWindow');
							}
						}]
					}
				}, '-',

				{
					text: 'Tools',
					menu: {
						items: [
							{
								text: 'System Uploads',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.system.upload.UploadWindow');
								}
							},
							{
								text: 'Administration',
								menu: {
									items: [
										{
											text: 'Batch Jobs',
											iconCls: 'clock',
											handler: function() {
												TCG.createComponent('Clifton.batch.job.JobSetupWindow');
											}
										},
										{
											text: 'Document Management',
											iconCls: 'book',
											handler: function() {
												TCG.createComponent('Clifton.document.setup.DocumentSetupWindow');
											}
										},
										{
											text: 'Security',
											iconCls: 'lock',
											handler: function() {
												TCG.createComponent('Clifton.security.SecuritySetupWindow');
											}
										},
										{
											text: 'Schema',
											iconCls: 'grid',
											handler: function() {
												TCG.createComponent('Clifton.system.schema.SchemaSetupWindow');
											}
										},
										{
											text: 'System Beans',
											iconCls: 'objects',
											handler: function() {
												TCG.createComponent('Clifton.system.bean.BeanSetupWindow');
											}
										},
										{
											text: 'System Conditions',
											iconCls: 'question',
											handler: function() {
												TCG.createComponent('Clifton.system.condition.ConditionSetupWindow');
											}
										}, '-',
										{
											text: 'Audit Trail',
											iconCls: 'pencil',
											handler: function() {
												TCG.createComponent('Clifton.system.audit.AuditSetupWindow');
											}
										},
										{
											text: 'Centralized Logging',
											iconCls: 'explorer',
											handler: function() {
												const config = {
													defaultApplicationFilter: 'OMS',
													graylogUrl: `${TCG.PageConfiguration.graylog.apiScheme}://${TCG.PageConfiguration.graylog.apiHost}`
												};
												TCG.createComponent('Clifton.core.logging.LogMessageListWindow', config);
											}
										},
										{
											text: 'System Ops Views',
											iconCls: 'doctor',
											handler: function() {
												TCG.createComponent('Clifton.core.stats.StatsListWindow');
											}
										},
										{
											text: 'System Access Stats',
											iconCls: 'timer',
											handler: function() {
												TCG.createComponent('Clifton.system.statistic.StatisticListWindow');
											}
										}
									]
								}
							},
							{
								text: 'Setup',
								menu: {
									items: [
										{
											text: 'Calendars',
											iconCls: 'calendar',
											handler: function() {
												TCG.createComponent('Clifton.calendar.CalendarSetupWindow');
											}
										}, {
											text: 'Calendar Schedules',
											iconCls: 'schedule',
											handler: function() {
												TCG.createComponent('Clifton.calendar.schedule.ScheduleSetupWindow');
											}
										},
										{
											text: 'Hierarchies',
											iconCls: 'hierarchy',
											handler: function() {
												TCG.createComponent('Clifton.system.hierarchy.HierarchySetupWindow');
											}
										}, {
											text: 'Lists and Queries',
											iconCls: 'list',
											handler: function() {
												TCG.createComponent('Clifton.system.list.ListSetupWindow');
											}
										}, {
											text: 'Notes',
											iconCls: 'pencil',
											handler: function() {
												TCG.createComponent('Clifton.system.note.NoteSetupWindow');
											}
										}, {
											text: 'Notifications',
											iconCls: 'flag-red',
											handler: function() {
												TCG.createComponent('Clifton.notification.definition.DefinitionSetupWindow');
											}
										}, {
											text: 'Priorities',
											iconCls: 'priority',
											handler: function() {
												TCG.createComponent('Clifton.system.priority.PrioritySetupWindow');
											}
										}, {
											text: 'Rules',
											iconCls: 'rule',
											handler: function() {
												TCG.createComponent('Clifton.rule.setup.RuleSetupWindow');
											}
										}, {
											text: 'System Groups',
											iconCls: 'grouping',
											handler: function() {
												TCG.createComponent('Clifton.system.group.GroupSetupWindow');
											}
										},
										{
											text: 'Workflow',
											iconCls: 'workflow',
											handler: function() {
												TCG.createComponent('Clifton.workflow.WorkflowSetupWindow');
											}
										}, {
											text: 'Workflow Tasks',
											iconCls: 'task',
											handler: function() {
												TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
											}
										}
									]
								}
							},
							{
								text: 'String Differences',
								iconCls: 'diff',
								handler: function() {
									new TCG.app.OKCancelWindow({
										title: 'String Differences Window',
										iconCls: 'diff',
										id: 'stringDiffWindow',
										enableShowInfo: false,
										closeWindowTrail: true,
										okButtonText: 'Compare',
										okButtonTooltip: 'Compare String 1 to String 2 and highlight differences.',
										width: 750,
										height: 450,

										items: [{
											xtype: 'formpanel',
											instructions: 'Use this window to compare 2 strings and highlight differences. Enter or copy/paste value for each string and click "Compare" button.',
											labelWidth: 70,
											items: [
												{fieldLabel: 'String 1', name: 'string1', xtype: 'textarea', height: 150},
												{fieldLabel: 'String 2', name: 'string2', xtype: 'textarea', height: 150}
											]
										}],
										saveWindow: function() {
											const fp = this.getMainFormPanel();
											TCG.showDifferences(fp.getFormValue('string1'), fp.getFormValue('string2'));
										}
									});
								}
							},
							{
								text: 'User Tools',
								iconCls: 'tools',
								menu: {
									items: [{
										text: 'Change Password',
										iconCls: 'key',
										handler: function() {
											TCG.showInfo('System authentication is integrated with company\'s Active Directory.<br/><br/>To change password, press &lt;Ctrl&gt; + &lt;Alt&gt; + &lt;Delete&gt; while logged in with your corporate Windows account. Click the [Change Password...] button and follow instructions for password change.', 'Password Change Instructions');
										}
									}, {
										text: 'My Notifications',
										iconCls: 'flag-red',
										handler: function() {
											TCG.createComponent('Clifton.notification.NotificationListWindow');
										}
									}, {
										text: 'User Preferences',
										disabled: true,
										iconCls: 'config',
										handler: menuSelect
									}]
								}
							}
						]
					}
				},
				'-',

				new TCG.app.WindowMenu(this),
				'-',
				{
					text: 'Help',
					menu: {
						items: [
							TCG.app.ContactSupportMenu, '-', {
								text: 'OMS Documentation',
								iconCls: 'help',
								handler: function() {
									TCG.openWIKI('IT/OMS+Documentation', 'OMS Documentation');
								}
							}, {
								text: 'API Documentation',
								iconCls: 'pencil',
								handler: function() {
									new TCG.app.URLWindow({
										id: 'apiDocumentationWindow',
										title: 'API Documentation',
										width: 1200,
										height: 800,
										iconCls: 'pencil',
										url: `./webjars/swagger-ui/index.html?url=${encodeURIComponent(window.location.href)}openapi.yaml`
									});
								}
							}, '-', {
								text: 'System Health Check',
								iconCls: 'verify',
								handler: function() {
									new TCG.app.URLWindow({
										id: 'healthCheckWindow',
										title: 'System Health Check',
										width: 1000,
										height: 600,
										iconCls: 'verify',
										url: 'health-status?enableTimeIntervals=false'
									});
								}
							}
						]
					}
				},
				'-',
				{
					text: 'Logout',
					id: 'logoutMenu',
					handler: () => TCG.data.AuthHandler.logout()
				}
		);

		return mb;
	}
});


TCG.app.app.addListener('afterload', function() {
	// easy way to distinguish environments
	const theme = TCG.PageConfiguration.instance.theme;
	if (TCG.isNotBlank(theme)) {
		Ext.util.CSS.swapStyleSheet('theme', theme);
	}

	if (TCG.isTrue(TCG.getQueryParams()['maximize'])) {
		TCG.app.Window.prototype.maximized = true;
	}

	// When clicking the welcome message, open the Workflow task window
	Ext.fly('welcomeMessage').addListener('click', function() {
		TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
	});

	TCG.data.AuthHandler.initLogin('securityUserCurrent.json', user => {
		TCG.setCurrentUserIsAdmin(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Administrators'));
		TCG.setCurrentUserIsDeveloper(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Software Developers'));
		// load controls specified by createComponents URL parameter.  URL can be follow by an ID e.i. createComponents=Clifton.workflow.WorkflowWindow:workflowWindowID
		const componentStr = Ext.urlDecode(window.location.search.substring(1)).createComponents;
		if (componentStr) {
			const components = componentStr.split(',');
			for (const component of components) {
				if (component.indexOf(':') > 0) {
					const items = component.split(':');
					TCG.createComponent(items[0], {id: items[1]});
				}
				else {
					TCG.createComponent(component);
				}
			}
		}

		const loadWindowClass = TCG.getQueryParams()['loadWindowClass'];
		if (TCG.isTrue(loadWindowClass)) {
			const loadWindowParams = TCG.getQueryParams()['loadWindowParams'];
			TCG.createComponent(loadWindowClass, {params: Ext.decode(loadWindowParams)});
		}

		// Pre-Load/Cache all users (used for Created By/Updated By columns) - only gets id and label
		const loader = new TCG.data.JsonLoader(Ext.apply({
			onLoad: (records, conf) => {
				if (records && records.length > 0) {
					for (const record of records) {
						TCG.CacheUtils.put('DATA_CACHE', record, `security.user.${record.id}_Application=Internal`);
					}
				}
			}
		}));
		loader.load('securityUserList.json?requestedPropertiesRoot=data&requestedProperties=id|label');
	});
});

// Override printer path for WebJars compatibility
Ext.override(Ext.ux.Printer.BaseRenderer, {
	stylesheetPath: [
		`${TCG.ExtRootUrl}/resources/css/ext-all.css`,
		`${TCG.ExtRootUrl}/ux/css/ux-all.css`,
		`webjars/extjs-printer/print.css`,
		'core/css/application.css'
	]
});


function menuSelect(a) {
	TCG.showError('Menu item "' + a.text + '" is currently under construction', 'Under Construction');
}
