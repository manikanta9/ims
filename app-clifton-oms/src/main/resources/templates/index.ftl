<#-- @ftlvariable name="environment" type="org.springframework.core.env.Environment" -->
<?xml version="1.0" encoding="UTF-8" ?>
<#assign extJsRootUrl = environment.getRequiredProperty("application.extjs.rootUrl")>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>OMS Application - Parametric</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="SHORTCUT ICON" href="core/images/favicon.ico"/>

	<!-- Ideally, we use the webjar version, but with bootstrap 3, there is no easy way to override the CSS we need -->
	<!--<link rel="stylesheet" href="webjars/bootstrap/3.3.7/dist/css/bootstrap.min.css" id="bt-theme">-->
	<!--<link rel="stylesheet" href="css/bootstrap.css" id="ims-theme">-->

	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/gridfilters/css/GridFilters.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/gridfilters/css/RangeMenu.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/resources/css/ext-all.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/css/ux-all.css"/>
	<link rel="stylesheet" type="text/css" href="core/css/application.css" media="screen"/>

	<!-- Used for JSON migration functionality -->
	<link rel="stylesheet" type="text/css" href="webjars/jsoneditor/dist/jsoneditor.css"/>
	<link rel="stylesheet" type="text/css" href="core/css/jsoneditor-ims-theme.css"/>

	<script type="text/javascript" src="webjars/stomp__stompjs/lib/stomp.min.js"></script>
	<script type="text/javascript" src="webjars/sockjs-client/dist/sockjs.min.js"></script>

	<script type="text/javascript" src="${extJsRootUrl}/adapter/ext/ext-base-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ext-all-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ux/ux-all-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ux/FieldReplicator.js"></script>
	<script type="text/javascript" src="webjars/extjs-printer/Printer-all.js"></script>

	<!-- Used for JSON migration functionality -->
	<script type="text/javascript" src="webjars/jsoneditor/dist/jsoneditor.min.js"></script>

	<!-- Used for differences in audit trail values -->
	<script type="text/javascript" src="webjars/google-diff-match-patch/diff_match_patch.js"></script>

	<!-- Used for Charting -->
	<script type="text/javascript" src="webjars/moment/2.24.0/min/moment.min.js"></script>
	<script type="text/javascript" src="webjars/chart.js/2.7.3/dist/Chart.min.js"></script>
	<script type="text/javascript" src="webjars/chartjs-plugin-datalabels/0.7.0/dist/chartjs-plugin-datalabels.min.js"></script>


	<!-- Page configuration -->
	<script type="text/javascript">
		Ext.ns('TCG');
		TCG.ExtRootUrl = '${extJsRootUrl}';
		TCG.environmentLevel = '${environment.getRequiredProperty("application.environment.level")}';
		TCG.PageConfiguration = {
			applicationName: '${environment.getRequiredProperty("application.name")}'.toLowerCase(),
			externalApplicationNames: '${environment.getRequiredProperty("application.external.names")}'
					.toLocaleLowerCase()
					.split(',')
					.filter(name => !!name), // Remove empty values
			instance: {
				text: '${environment.getRequiredProperty("application.instance.text")}',
				color: '${environment.getRequiredProperty("application.instance.text.color")}',
				size: '${environment.getRequiredProperty("application.instance.text.size")}',
				version: '${environment.getRequiredProperty("application.instance.version")}',
				theme: '${environment.getRequiredProperty("application.instance.theme")}'
			},
			graylog: {
				apiScheme: '${environment.getRequiredProperty("graylog.api.scheme")}',
				apiHost: '${environment.getRequiredProperty("graylog.api.host")}'
			},
			websocket: {
				enabled: '${environment.getRequiredProperty("application.websocket.enabled")}' === 'true',
				// Use relative path for non-root contexts; path must be normalized (remove redundant slashes) to avoid Spring Security constraints on missing URI path elements
				endpoint: './${environment.getRequiredProperty("application.websocket.servlet.path")}/${environment.getRequiredProperty("application.websocket.stomp.path")}'.replace(/\/+(?=\/)|\/+$/g, '')
			}
		};
	</script>

	<!-- Internal Core Libraries -->
	<script type="text/javascript" src="core/js/tcg.js"></script>
	<script type="text/javascript" src="core/js/tcg-app.js"></script>
	<script type="text/javascript" src="core/js/tcg-cache.js"></script>
	<script type="text/javascript" src="core/js/tcg-data.js"></script>
	<script type="text/javascript" src="core/js/tcg-file.js"></script>
	<script type="text/javascript" src="core/js/tcg-form.js"></script>
	<script type="text/javascript" src="core/js/tcg-grid.js"></script>
	<script type="text/javascript" src="core/js/tcg-gridfilters.js"></script>
	<script type="text/javascript" src="core/js/tcg-gridsummary.js"></script>
	<script type="text/javascript" src="core/js/tcg-toolbar.js"></script>
	<script type="text/javascript" src="core/js/tcg-tree.js"></script>

	<!-- Internal Project libraries -->
	<script type="text/javascript" src="core/core-shared.js"></script>
	<script type="text/javascript" src="websocket/websocket-shared.js"></script>
	<script type="text/javascript" src="websocket/websocket-alert.js"></script>
	<script type="text/javascript" src="system/system-shared.js"></script>
	<script type="text/javascript" src="system/system-field-mappings-shared.js"></script>
	<script type="text/javascript" src="system/system-group-shared.js"></script>
	<script type="text/javascript" src="system/system-priority-shared.js"></script>
	<script type="text/javascript" src="system/system-note-shared.js"></script>
	<script type="text/javascript" src="system/system-statistic-shared.js"></script>
	<script type="text/javascript" src="system/system-query-shared.js"></script>
	<script type="text/javascript" src="security/security-shared.js"></script>
	<script type="text/javascript" src="calendar/calendar-shared.js"></script>
	<script type="text/javascript" src="calendar/calendar-schedule-shared.js"></script>
	<script type="text/javascript" src="batch/batch-shared.js"></script>
	<script type="text/javascript" src="document/document-shared.js"></script>
	<script type="text/javascript" src="notification/notification-shared.js"></script>
	<script type="text/javascript" src="workflow/workflow-shared.js"></script>
	<script type="text/javascript" src="rule/rule-shared.js"></script>

	<script type="text/javascript" src="fix/fix-shared.js"></script>

	<script type="text/javascript" src="order/order-shared-shared.js"></script>
	<script type="text/javascript" src="order/order-setup-shared.js"></script>
	<script type="text/javascript" src="order/order-base-shared.js"></script>
	<script type="text/javascript" src="order/order-batch-shared.js"></script>
	<script type="text/javascript" src="order/order-fix-shared.js"></script>
	<script type="text/javascript" src="order/order-management-shared.js"></script>

	<!-- Application Bootstrap -->
	<script type="text/javascript" src="js/app-oms-bootstrap.js"></script>

</head>

<body>
<div id="officePluginHolder"></div>
</body>
</html>
