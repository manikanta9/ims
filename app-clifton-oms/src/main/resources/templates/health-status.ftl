<#-- @ftlvariable name="applicationContext" type="org.springframework.context.ApplicationContext" -->
<#-- @ftlvariable name="handler" type="com.clifton.oms.app.handler.OmsHealthStatusHandler" -->
<#-- @ftlvariable name="request" type="javax.servlet.http.HttpServletRequest" -->
<#assign handler = applicationContext.getBean("omsHealthStatusHandler")>
<#assign parameters = handler.parseParameters(request)>
<body style="FONT-FAMILY: arial,serif; FONT-SIZE: 12px;">
${handler.impersonateUserPage()}
<b>SYSTEM INFORMATION</b>
<br/>Environment = ${handler.getEnvironment()}
<br/>
<br/><b>PAGE PARAMETERS</b>
<br/>maxTimeAllowed = ${(parameters.getMaxTimeAllowed())!"null"}
<br/>nameOfTestToRun = ${(parameters.getTestToRun())!"null"}
<br/>enableTimeIntervals = ${parameters.isEnableIntervals()?c}
<br/>
<br/><b>TESTS</b><br/>
<#assign initTime = .now?long>
<!-- Database maintenance tasks window: 11:00 PM - 12:00 AM -->
${handler.executeDbTest("ParametricOMS DB", parameters, 500, ["11:00 PM - 12:00 AM"])}
${handler.executeExternalDbTest("External DBs", parameters, 500, ["11:00 PM - 12:00 AM"])}
${handler.executeFixOauthTest("FIX OAuth", parameters, 500, [])}
${handler.executeDocumentManagementTest("Document Management", parameters, 1000, [])}
${handler.executeBatchJobStatusTest("Batch Job Status", parameters, 3000, [])}
${handler.executeNotificationStatusTest("Notification Status", parameters, 3000, [])}

<#assign elapsedTime = .now?long - initTime>
<br/><b>PROCESSING TIME: ${elapsedTime} milliseconds</b>
</body>
