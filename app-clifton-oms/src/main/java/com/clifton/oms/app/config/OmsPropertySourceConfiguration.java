package com.clifton.oms.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * @author lnaylor
 */
@Configuration
@PropertySource("classpath:/META-INF/app-clifton-oms.properties")
public class OmsPropertySourceConfiguration {
}
