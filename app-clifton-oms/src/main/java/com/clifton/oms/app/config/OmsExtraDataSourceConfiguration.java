package com.clifton.oms.app.config;

import com.clifton.core.context.spring.DelimitedXmlBeanDefinitionReader;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


/**
 * @author lnaylor
 */
@Configuration
@ImportResource(value = "${dataSource.extraDataSourcePaths}", reader = DelimitedXmlBeanDefinitionReader.class)
public class OmsExtraDataSourceConfiguration {
}
