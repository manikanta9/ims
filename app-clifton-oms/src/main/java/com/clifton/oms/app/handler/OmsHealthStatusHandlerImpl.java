package com.clifton.oms.app.handler;

import com.clifton.batch.job.BatchJob;
import com.clifton.batch.runner.BatchRunnerService;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.health.HealthCheckHandler;
import com.clifton.core.health.HealthCheckParameters;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.fix.session.FixSessionService;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.runner.NotificationBatchRunnerService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.security.user.search.SecurityUserSearchForm;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemSchemaService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;


/**
 * Handler for HealthCheck template view, resolved by Freemarker View Resolver. This component will resolve the test execution.
 *
 * @author manderson
 */
@Component
public class OmsHealthStatusHandlerImpl implements OmsHealthStatusHandler, ApplicationContextAware {

	private ApplicationContext applicationContext;

	private BatchRunnerService batchRunnerService;

	private ContextHandler contextHandler;

	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;

	private DocumentManagementService documentManagementService;

	private FixSessionService fixSessionService;

	private HealthCheckHandler healthCheckHandler;

	private NotificationBatchRunnerService notificationBatchRunnerService;

	private SecurityUserService securityUserService;

	private SystemSchemaService systemSchemaService;

	@Value("${application.environment.level:prod}")
	private String environment;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void impersonateUserPage() {
		SecurityUser pageUser = new SecurityUser();
		pageUser.setId((short) 999);
		pageUser.setUserName(SecurityUser.SYSTEM_USER);
		getContextHandler().setBean(Context.USER_BEAN_NAME, pageUser);
	}


	@Override
	public HealthCheckParameters parseParameters(HttpServletRequest request) {
		return getHealthCheckHandler().getHealthCheckPageParameters(request);
	}


	/*
	 * ParametricOMS database access: systemuser should never be deleted
	 */
	@Override
	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			SecurityUserSearchForm securityUserSearchForm = new SecurityUserSearchForm();
			securityUserSearchForm.setLimit(1);
			securityUserSearchForm.setUserNameEquals(SecurityUser.SYSTEM_USER);
			SecurityUser user = CollectionUtils.getFirstElementStrict(getSecurityUserService().getSecurityUserList(securityUserSearchForm));
			ValidationUtils.assertNotNull(user, "Cannot find security user: " + SecurityUser.SYSTEM_USER);
		});
		return writer.toString();
	}


	/**
	 * IMS & Union Databases
	 */
	@Override
	public String executeExternalDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			List<SystemDataSource> dataSourceList = getSystemSchemaService().getSystemDataSourceNotDefaultList();

			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Data Source: %s, Status: %s";
			StringBuilder successStringBuilder = new StringBuilder();
			StringBuilder errorStringBuilder = new StringBuilder();

			if (CollectionUtils.isEmpty(dataSourceList)) {
				successStringBuilder.append("<br />").append("No External DataSources to test");
			}
			else {
				for (SystemDataSource dataSource : dataSourceList) {
					try {
						getDataTableRetrievalHandlerLocator().locate(dataSource.getName()).findDataTable("SELECT 1");
						successStringBuilder.append("<br />").append(String.format(messageTemplate, dataSource.getName(), "OK"));
					}
					catch (Exception e) {
						errorStringBuilder.append("<br />").append(String.format(messageTemplate, dataSource.getName(), ExceptionUtils.getOriginalMessage(e)));
					}
				}
				if (errorStringBuilder.length() > 0) {
					throw new RuntimeException("Invalid Data Sources/Unable to Connect: " + errorStringBuilder);
				}
			}
			return successStringBuilder.toString();
		});
		return writer.toString();
	}


	@Override
	public String executeFixOauthTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			if (!BooleanUtils.isTrue(getFixSessionService().isFixSessionServiceAlive())) {
				throw new RuntimeException("Error occurred making call to the Fix with OAuth. Review the logs for further details");
			}
		});
		return writer.toString();
	}


	@Override
	public String executeBatchJobStatusTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Batch Job Name: %s, ID: %d";
			List<BatchJob> invalidBatchJobList = getBatchRunnerService().getBatchJobListInvalid();
			if (CollectionUtils.isEmpty(invalidBatchJobList)) {
				return;
			}
			invalidBatchJobList.sort(Comparator.comparing(BaseSimpleEntity::getId));
			StringBuilder stringBuilder = new StringBuilder("Batch Jobs with status 'RUNNING' that are not currently running:");
			for (BatchJob invalidBatchJob : invalidBatchJobList) {
				stringBuilder.append("<br />").append(String.format(messageTemplate, invalidBatchJob.getName(), invalidBatchJob.getId()));
			}
			throw new RuntimeException(stringBuilder.toString());
		});
		return writer.toString();
	}


	@Override
	public String executeDocumentManagementTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			// Doesn't need to return anything - just checking connection
			String testTableName = "SystemNote";
			getDocumentManagementService().getDocumentRecordVersionList(testTableName, 1);
		});
		return writer.toString();
	}


	@Override
	public String executeNotificationStatusTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Notification Definition Name: %s, ID: %d";
			List<NotificationDefinitionBatch> invalidNotificationDefinitionBatchList = getNotificationBatchRunnerService().getNotificationDefinitionBatchListInvalid();
			if (CollectionUtils.isEmpty(invalidNotificationDefinitionBatchList)) {
				return;
			}
			invalidNotificationDefinitionBatchList.sort(Comparator.comparing(BaseSimpleEntity::getId));
			StringBuilder stringBuilder = new StringBuilder("Notifications with status RUNNING that are not currently running:");
			for (NotificationDefinitionBatch invalidNotification : CollectionUtils.getIterable(invalidNotificationDefinitionBatchList)) {
				stringBuilder.append("<br />").append(String.format(messageTemplate, invalidNotification.getName(), invalidNotification.getId()));
			}
			throw new RuntimeException(stringBuilder.toString());
		});
		return writer.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public BatchRunnerService getBatchRunnerService() {
		return this.batchRunnerService;
	}


	public void setBatchRunnerService(BatchRunnerService batchRunnerService) {
		this.batchRunnerService = batchRunnerService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public HealthCheckHandler getHealthCheckHandler() {
		return this.healthCheckHandler;
	}


	public void setHealthCheckHandler(HealthCheckHandler healthCheckHandler) {
		this.healthCheckHandler = healthCheckHandler;
	}


	public NotificationBatchRunnerService getNotificationBatchRunnerService() {
		return this.notificationBatchRunnerService;
	}


	public void setNotificationBatchRunnerService(NotificationBatchRunnerService notificationBatchRunnerService) {
		this.notificationBatchRunnerService = notificationBatchRunnerService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	@Override
	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}
}
