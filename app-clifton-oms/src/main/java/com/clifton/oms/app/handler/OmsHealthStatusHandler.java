package com.clifton.oms.app.handler;

import com.clifton.core.health.HealthCheckParameters;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Handler to render the static templates for OMS Application. Should be implemented in order to handle a HealthCheck page template, and resolve all back-end behaviour related to
 * test execution.
 *
 * @author manderson
 */
public interface OmsHealthStatusHandler {

	public HealthCheckParameters parseParameters(HttpServletRequest request);


	public void impersonateUserPage();


	public String getEnvironment();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);


	public String executeExternalDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);


	public String executeFixOauthTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);


	public String executeBatchJobStatusTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);


	public String executeDocumentManagementTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeMillis, List<String> disabledIntervals);


	public String executeNotificationStatusTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);
}
