import com.clifton.gradle.plugin.artifact.CliftonSchemaPlugin
import com.clifton.gradle.plugin.artifact.MigrationExec
import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	// Used by Crowd
	runtimeOnly("org.codehaus.xfire:xfire-aegis:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// WebJars
	// implementation("org.webjars:extjs:3.3.0") // TODO: Switch to official webjars distribution
	implementation("sencha:extjs:3.3.0:clifton-webjar")
	implementation("sencha:extjs-printer:1.0.0:webjar")
	implementation("org.webjars.npm:jsoneditor:5.5.7")
	implementation("org.webjars:google-diff-match-patch:20121119-1")

	// Charting JS library
	implementation("org.webjars.npm:chart.js:2.7.3")
	implementation("org.webjars.npm:chartjs-plugin-datalabels:0.7.0")
	implementation("org.webjars.npm:moment:2.24.0")

	implementation("org.webjars:webjars-locator:0.30")

	implementation("org.springframework.boot:spring-boot-starter-actuator") {
		exclude(module = "spring-boot-starter-logging")
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	javascript(project(":clifton-core"))
	javascript(project(":clifton-security"))
	javascript(project(":clifton-system"))
	javascript(project(":clifton-system:clifton-system-priority"))
	javascript(project(":clifton-system:clifton-system-field-mapping"))
	javascript(project(":clifton-system:clifton-system-group"))
	javascript(project(":clifton-system:clifton-system-note"))
	javascript(project(":clifton-system:clifton-system-statistic"))
	javascript(project(":clifton-system:clifton-system-query"))
	javascript(project(":clifton-calendar"))
	javascript(project(":clifton-calendar:clifton-calendar-schedule"))
	javascript(project(":clifton-batch"))
	javascript(project(":clifton-document"))
	javascript(project(":clifton-notification"))
	javascript(project(":clifton-rule"))
	javascript(project(":clifton-workflow"))
	javascript(project(":clifton-websocket"))

	javascript(project(":clifton-fix"))

	javascript(project(":clifton-order:clifton-order-shared"))
	javascript(project(":clifton-order:clifton-order-setup"))
	javascript(project(":clifton-order:clifton-order-base"))
	javascript(project(":clifton-order:clifton-order-fix"))
	javascript(project(":clifton-order:clifton-order-batch"))
	javascript(project(":clifton-order:clifton-order-management"))


	implementation(project(":clifton-core"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-application"))
	implementation(project(":clifton-security"))
	implementation(project(":clifton-system"))
	implementation(project(":clifton-system:clifton-system-priority"))
	implementation(project(":clifton-system:clifton-system-field-mapping"))
	implementation(project(":clifton-system:clifton-system-group"))
	implementation(project(":clifton-system:clifton-system-note"))
	implementation(project(":clifton-system:clifton-system-statistic"))
	implementation(project(":clifton-system:clifton-system-query"))
	implementation(project(":clifton-calendar"))
	implementation(project(":clifton-calendar:clifton-calendar-schedule"))
	implementation(project(":clifton-batch"))
	implementation(project(":clifton-document"))
	implementation(project(":clifton-notification"))
	implementation(project(":clifton-rule"))
	implementation(project(":clifton-workflow"))
	implementation(project(":clifton-websocket"))

	implementation(project(":clifton-fix")) { usingVariant("api") }


	implementation(project(":clifton-order:clifton-order-shared"))
	implementation(project(":clifton-order:clifton-order-setup"))
	implementation(project(":clifton-order:clifton-order-base"))
	implementation(project(":clifton-order:clifton-order-fix"))
	implementation(project(":clifton-order:clifton-order-batch"))
	implementation(project(":clifton-order:clifton-order-management"))
	implementation(project(":clifton-order:clifton-order-api")) { usingVariant("documentation") }
	implementation(project(":clifton-order:clifton-order-position"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-application")))

	////////////////////////////////////////////////////////////////////////////
	////////            Integration Test Dependencies                   ////////
	////////////////////////////////////////////////////////////////////////////

	integTestImplementation(testFixtures(project(":clifton-application")))
	integTestImplementation(testFixtures(project(":clifton-test")))
	// Assertions for smtp
	integTestImplementation("org.hamcrest:hamcrest-core")
	//JSOUP is an excellent library for parsing HTML in java
	integTestImplementation("org.jsoup:jsoup:1.7.2")
}

/*
 * Enforce database restore order when running parallel restores. Some databases reference other databases (such as the IMS database) during the restore process and will fail if
 * executed while any of the referenced databases are in a restoring state. This only affects builds where multiple database restores are simultaneously requested.
 */
tasks.named(CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME, MigrationExec::class) { mustRunAfter(":app-clifton-ims:${CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME}") }
