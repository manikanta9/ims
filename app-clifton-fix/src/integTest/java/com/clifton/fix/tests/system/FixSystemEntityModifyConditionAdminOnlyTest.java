package com.clifton.fix.tests.system;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import com.clifton.fix.app.FixApplication;
import com.clifton.test.SystemEntityModifyConditionAdminOnlyTest;
import com.clifton.test.spring.NoopLifecycleProcessor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.LifecycleProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestPropertySource;


/**
 * The {@link FixSystemEntityModifyConditionAdminOnlyTest} class implements the {@link SystemEntityModifyConditionAdminOnlyTest} test type for execution within the current
 * environment.
 *
 * @author ignacioa
 */
@SpringBootTest
@ContextHierarchy({
		@ContextConfiguration(name = "root", classes = SpringLoadTimeWeavingConfiguration.class),
		@ContextConfiguration(name = "child", classes = FixApplication.class),
		// @ContextConfiguration(name = "child", classes = FixSystemEntityModifyConditionAdminOnlyTest.TestConfiguration.class)
})
@TestPropertySource
@Import(FixSystemEntityModifyConditionAdminOnlyTest.TestConfiguration.class)
public class FixSystemEntityModifyConditionAdminOnlyTest extends SystemEntityModifyConditionAdminOnlyTest {

	@Configuration
	@ExcludeFromComponentScan
	public static class TestConfiguration {

		// Simplify start-up process by disabling lifecycle beans
		@Bean(AbstractApplicationContext.LIFECYCLE_PROCESSOR_BEAN_NAME)
		public LifecycleProcessor lifecycleProcessor() {
			return new NoopLifecycleProcessor();
		}
	}
}
