package com.clifton.fix.tests.spring;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.context.FilteredComponentScan;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.fix.tests.FixIntegrationTestConfiguration;
import com.clifton.test.protocol.ImsProtocolClientConfiguration;
import com.clifton.test.spring.ImsProtocolClientSpringConfiguration;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import javax.sql.DataSource;


@Configuration
@ExcludeFromComponentScan
@Import({FixIntegrationConfiguration.FixIntegrationDataSourceConfiguration.class, FixIntegrationConfiguration.FixIntegrationClientConfiguration.class})
@PropertySource({"classpath:META-INF/integration-tests.properties", "classpath:META-INF/integration-tests-overrides.properties"})
@FilteredComponentScan(basePackages = {"com.clifton.test", "com.clifton.fix.tests", "com.clifton.core.json.custom", "com.clifton.core.messaging"})
public class FixIntegrationConfiguration {

	@Configuration
	@ExcludeFromComponentScan
	public static class FixIntegrationDataSourceConfiguration implements EnvironmentAware {

		@Resource
		private Environment environment;


		@Bean
		public DataSource dataSource() {
			org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
			dataSource.setUrl(getEnvironment().getProperty("dataSource.url"));
			dataSource.setDriverClassName(getEnvironment().getProperty("dataSource.driverClassName"));
			dataSource.setUsername(getEnvironment().getProperty("dataSource.username"));
			dataSource.setPassword(getEnvironment().getProperty("dataSource.password"));
			return dataSource;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public Environment getEnvironment() {
			return this.environment;
		}


		@Override
		public void setEnvironment(Environment environment) {
			this.environment = environment;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	@ExcludeFromComponentScan
	@Import(ImsProtocolClientSpringConfiguration.class)
	public static class FixIntegrationClientConfiguration {

		@Bean // Used for CustomJsonObject serialization
		public JsonHandler<?> jsonHandler() {
			return new JacksonHandlerImpl();
		}


		@Bean
		public ImsProtocolClientConfiguration imsProtocolClientConfiguration() {
			return new FixIntegrationTestConfiguration();
		}
	}
}
