package com.clifton.fix.tests.fixsim.api;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.fixsim.model.OrderSingleIn;
import com.clifton.fix.fixsim.model.OrderSingleOut;
import com.clifton.fix.fixsim.model.SessionInfo;
import com.clifton.fix.fixsim.model.UpdateSessionInfo;
import com.clifton.fix.util.FixSimApiHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.util.List;


/**
 * This tests the basic functionality of the FixSimApiHandler.
 *
 * @author davidi
 */
public class FixSimApiHandlerTests {

	private final static String KEY = "4cc8ee20-83c8-4f9e-a01f-152683726b78";
	private final static String INSTANCE = "PARAMETRIC_DEV";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSendRawMessages_AS_STRING() {
		final String msgData1 = "8=FIX.4.4\u00019=0278\u000135=D\u000134=36\u000149=CLFTPARAEMSX\u000150=test_user_1\u000152=20180419-03:41:07.353\u000156=BLPTEST\u000111=O_291432_20180418\u000121=3\u000122=A\u000138=17.00000000\u000140=1\u000148=C K18 Comdty\u000154=1\u000155=C K18\u000159=0\u000160=20180419-03:41:07.050\u000164=20180618\u000175=20180618\u0001461=FUT\u000178=1\u000179=052-BAJ44\u000180=17.00000000\u000158=Integration Test Trade\u0001167=FUT\u000176=GSFT\u000110=212\u0001\n";
		final String msgData2 = "8=FIX.4.4\u00019=0278\u000135=D\u000134=36\u000149=CLFTPARAEMSX\u000150=test_user_1\u000152=20180419-03:41:07.353\u000156=BLPTEST\u000111=O_291432_20180418\u000121=3\u000122=A\u000138=20.00000000\u000140=1\u000148=C K18 Comdty\u000154=1\u000155=C K18\u000159=0\u000160=20180419-03:41:07.050\u000164=20180618\u000175=20180618\u0001461=FUT\u000178=1\u000179=052-BAJ44\u000180=17.00000000\u000158=Integration Test Trade\u0001167=FUT\u000176=GSFT\u000110=212\u0001\n";

		List<String> messageList = CollectionUtils.createList(msgData1, msgData2);

		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_004";

		try {
			createFixSimSession(sessionName, "CC", "DD");

			// Send order data to FixSim Session
			fixSimApiHandler.sendRawMessages(messageList, INSTANCE, sessionName);

			// Verify messages sent to FixSim
			List<OrderSingleOut> orderSingleOutList = fixSimApiHandler.getFixSimOrdersOutForSession(INSTANCE, sessionName);
			Assertions.assertEquals(2, CollectionUtils.getSize(orderSingleOutList));
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	@Test
	public void testSendRawMessages_FROM_FILE() {
		final String dataSourcePath = new ClassPathResource("com/clifton/fix/tests/fixsim/api/data/fixmessages_001.txt").getPath();
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_005";

		try {
			createFixSimSession(sessionName, "AA", "BB");

			// Send order data to FixSim Session
			fixSimApiHandler.sendRawMessages(dataSourcePath, INSTANCE, sessionName);

			// Check messages sent to FixSim
			List<OrderSingleOut> orderSingleOutList = fixSimApiHandler.getFixSimOrdersOutForSession(INSTANCE, sessionName);
			Assertions.assertEquals(2, CollectionUtils.getSize(orderSingleOutList));
			List<OrderSingleIn> orderSingleInList = fixSimApiHandler.getFixSimOrdersInForSession(INSTANCE, sessionName, null);
			Assertions.assertEquals(0, CollectionUtils.getSize(orderSingleInList));

			// Verify messages can be cleared via API
			fixSimApiHandler.deleteFixSimOrdersOutForSession(INSTANCE, sessionName);
			orderSingleOutList = fixSimApiHandler.getFixSimOrdersOutForSession(INSTANCE, sessionName);
			Assertions.assertEquals(0, CollectionUtils.getSize(orderSingleOutList));
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	@Test
	public void testCreateFixSimSession_withDelete() {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_001";
		try {
			SessionInfo sessionInfo = new SessionInfo();
			sessionInfo.setSessionName(sessionName);
			sessionInfo.setFiXVersion("FIX.4.4");
			sessionInfo.setSenderCompId("CSENDER1");
			sessionInfo.setTargetCompId("CTARGET1");

			// Create and verify session got stored
			fixSimApiHandler.createFixSimSession(INSTANCE, sessionInfo);
			SessionInfo retrievedSessionInfo = fixSimApiHandler.getFixSimSession(INSTANCE, sessionName);
			Assertions.assertNotNull(retrievedSessionInfo.getSessionName(), "Cannot find newly-created FixSim session.");
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	@Test
	public void testReCreateFixSimSession_withDelete() {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_002";
		try {
			SessionInfo sessionInfo = new SessionInfo();
			sessionInfo.setSessionName(sessionName);
			sessionInfo.setFiXVersion("FIX.4.4");
			sessionInfo.setSenderCompId("SND2");
			sessionInfo.setTargetCompId("TRG2");

			// Create and verify session got stored
			fixSimApiHandler.createFixSimSession(INSTANCE, sessionInfo);
			SessionInfo retrievedSessionInfo = fixSimApiHandler.updateFixSimSessionSequenceNumbers(INSTANCE, sessionName, 1000, 1001);
			Assertions.assertNotNull(retrievedSessionInfo.getSessionName(), "Cannot find newly-created FixSim session.");
			Assertions.assertEquals(1000, retrievedSessionInfo.getSendingSeqNum());
			Assertions.assertEquals(1001, retrievedSessionInfo.getRcvdSeqNum());

			// Re-Create session, verify data is the samve except for sequence numbers, which should have been reset.
			retrievedSessionInfo = fixSimApiHandler.reCreateFixSimSession(INSTANCE, sessionName);
			Assertions.assertNotNull(retrievedSessionInfo, "Cannot find FixSim session.");
			Assertions.assertEquals(sessionInfo.getSessionName(), retrievedSessionInfo.getSessionName(), "Session name was altered.");
			Assertions.assertEquals(sessionInfo.getFiXVersion(), retrievedSessionInfo.getFiXVersion(), "Fix version was altered.");
			Assertions.assertEquals(sessionInfo.getSenderCompId(), retrievedSessionInfo.getSenderCompId(), "SenderCompId was altered.");
			Assertions.assertEquals(sessionInfo.getTargetCompId(), retrievedSessionInfo.getTargetCompId(), "TargetCompId was altered.");

			// Verify process reset the sequence numbers
			Assertions.assertEquals(1, retrievedSessionInfo.getSendingSeqNum());
			Assertions.assertEquals(1, retrievedSessionInfo.getRcvdSeqNum());
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	@Test
	public void testUpdateFixSimSession() {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_003";
		try {
			SessionInfo sessionInfo = new SessionInfo();
			sessionInfo.setSessionName(sessionName);
			sessionInfo.setFiXVersion("FIX.4.4");
			sessionInfo.setSenderCompId("CSENDER3");
			sessionInfo.setTargetCompId("CTARGET3");

			// Create and verify session got stored
			SessionInfo retrievedSessionInfo = fixSimApiHandler.createFixSimSession(INSTANCE, sessionInfo);
			Assertions.assertNotNull(retrievedSessionInfo.getSessionName(), "Cannot find newly-created FixSim session.");

			// Update data in session
			final String fixVersion = "FIX.4.2";
			UpdateSessionInfo data = new UpdateSessionInfo();
			data.setFiXVersion(fixVersion);
			retrievedSessionInfo = fixSimApiHandler.updateFixSimSession(INSTANCE, sessionName, data);

			// Verify session got updated
			Assertions.assertNotNull(retrievedSessionInfo.getSessionName(), "Cannot find newly-created FixSim session.");
			Assertions.assertEquals(fixVersion, retrievedSessionInfo.getFiXVersion());
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	@Test
	public void testGetFixSimSessionList() {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);

		List<SessionInfo> resultList = fixSimApiHandler.getFixSimSessionList(INSTANCE);
		Assertions.assertFalse(resultList.isEmpty());
		CollectionUtils.anyMatch(resultList, x -> "FIXSIM IOI".equals(x.getSessionName()));
		CollectionUtils.anyMatch(resultList, x -> "Bloomberg EMSX".equals(x.getSessionName()));
		CollectionUtils.anyMatch(resultList, x -> "Bloomberg EMSX Allocation".equals(x.getSessionName()));
	}


	@Test
	public void testGetFixSimSession() {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		final String sessionName = "TEST_SESSION_006";

		try {
			createFixSimSession(sessionName, "TS6a", "TS6b");
			// Explicit call to getFixSimSession()
			SessionInfo sessionInfo = fixSimApiHandler.getFixSimSession(INSTANCE, sessionName);
			// Verify session got updated
			Assertions.assertNotNull(sessionInfo.getSessionName(), "Cannot find newly-created FixSim session.");
			Assertions.assertEquals("TEST_SESSION_006_TS6a", sessionInfo.getSenderCompId());
			Assertions.assertEquals("TEST_SESSION_006_TS6b", sessionInfo.getTargetCompId());
		}
		finally {
			deleteFixSession(sessionName);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Create and return FixSim Session for testing purposes.
	 */
	private SessionInfo createFixSimSession(String sessionName, String senderCompIdSuffix, String targetCompIdSuffix) {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);

		SessionInfo sessionInfo = new SessionInfo();
		sessionInfo.setSessionName(sessionName);
		sessionInfo.setFiXVersion("FIX.4.4");
		sessionInfo.setSenderCompId(sessionName + "_" + senderCompIdSuffix);
		sessionInfo.setTargetCompId(sessionName + "_" + targetCompIdSuffix);
		return fixSimApiHandler.createFixSimSession(INSTANCE, sessionInfo);
	}


	/**
	 * Delete Fixsim Session and verify it got deleted successfully
	 */
	private void deleteFixSession(String sessionName) {
		FixSimApiHandler fixSimApiHandler = new FixSimApiHandler();
		fixSimApiHandler.setApiKey(KEY);
		fixSimApiHandler.deleteFixSimSession(INSTANCE, sessionName);
		SessionInfo retrievedSessionInfo = fixSimApiHandler.getFixSimSession(INSTANCE, sessionName);
		Assertions.assertNull(retrievedSessionInfo.getSessionName(), "Failed to delete FixSim session.");
	}
}
