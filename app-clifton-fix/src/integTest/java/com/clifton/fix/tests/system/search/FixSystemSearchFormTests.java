package com.clifton.fix.tests.system.search;

import com.clifton.test.system.search.BaseSystemSearchFormTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/fix/tests/core/dataaccess/SessionFactoryTests-context.xml")
@ExtendWith(SpringExtension.class)
public class FixSystemSearchFormTests extends BaseSystemSearchFormTests {

	// NOTHING HERE
}
