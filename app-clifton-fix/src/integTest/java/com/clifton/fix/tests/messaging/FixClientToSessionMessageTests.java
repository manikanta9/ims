package com.clifton.fix.tests.messaging;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.context.FilteredComponentScan;
import com.clifton.core.messaging.AsynchronousMessagingTestProcessor;
import com.clifton.core.messaging.jms.JmsQueueService;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.JmsAsynchronousErrorMessage;
import com.clifton.fix.ioi.FixIOIEntity;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.tests.BaseFixIntegrationTest;
import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.fix.util.FixSimApiHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_CLASS;


/**
 * These tests send FixEntityMessagingMessage to FIX Sessions and use the FIXSIM API to test the acquisition of IOI and Drop Copies.
 *
 * <code>@DirtiesContext</code> is used to clear the context set up in {@link com.clifton.test.BaseIntegrationTest} after these tests are run in order to allow subsequent tests
 * to be properly initialized. This will destroy persistent listeners (such as JMS consumers) so that subsequent tests do not try to use beans created here alongside newly
 * instantiated ones.
 *
 * @author syedz
 */

@ContextConfiguration(classes = {FixIntegrationConfiguration.class, FixClientToSessionMessageTests.FixClientToSessionMessageTestsConfiguration.class})
@DirtiesContext(classMode = AFTER_CLASS)
public class FixClientToSessionMessageTests extends BaseFixIntegrationTest {

	@Resource
	private AsynchronousMessagingTestProcessor<FixEntityMessagingMessage> fixTestProcessor;
	@Resource
	private AsynchronousMessageHandler testFixMessagingHandler;
	@Resource
	private FixSimApiHandler fixSimApiHandler;
	@Resource
	private JmsQueueService<JmsAsynchronousErrorMessage> fixJmsQueueService;


	private static final String FIX_IOI_FILE = "com/clifton/fix/tests/messaging/testdata/FixClientToSessionMessage_IOI.txt";
	private static final String FIX_DROPCOPY_FILE = "com/clifton/fix/tests/messaging/testdata/FixClientToSessionMessage_DropCopy.txt";
	private static final String FIXSIM_INSTANCE = "PARAMETRIC_DEV";
	private static final String FIXSIM_SESSION = "PARAMETRIC1";
	@Value("${fix.queue.response}")
	private String incomingQueue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void purge() {
		//clearing out the JMS Queue and the processor response queue
		getFixJmsQueueService().purgeMessages(this.incomingQueue);
		getFixTestProcessor().getResponses().clear();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSendOrder() {
		FixOrder order = new FixOrder();
		order.setId((long) 1);
		order.setAccount("ACT-" + 1);
		order.setOrderType(OrderTypes.MARKET);
		order.setOrderQty(new BigDecimal(10));
		order.setSymbol("GOOG");
		order.setHandlingInstructions(HandlingInstructions.AUTOMATIC);

		order.setSide(OrderSides.BUY);
		order.setTransactionTime(new Date());
		order.setTimeInForce(TimeInForceTypes.GTC);
		order.setClientOrderStringId("ORD-" + 1);

		// Deprecated
		order.setFixBeginString("FIX.4.4");
		order.setFixSenderCompId("PARAMETRIC1");
		order.setFixTargetCompId("FIXSIM");

		order.setSenderUserName("fixsimTest");
		order.setFixSenderSubId("sender_sub_id");

		order.setFixDestinationName("FIXSIM");
		Assertions.assertNotNull(order, "Expected an Order for the trade.");
		getFixSimApiHandler().sendFixEntity(order, getTestFixMessagingHandler());

		FixExecutionReport fixExecutionReport = getFixResponse(FixExecutionReport.class);
		Assertions.assertNotNull(fixExecutionReport, "Failed to get an Execution Report.");
		MatcherAssert.assertThat(fixExecutionReport.getOrderStatus(), IsEqual.equalTo(OrderStatuses.NEW));
		Assertions.assertEquals("GOOG", fixExecutionReport.getSymbol(), "Order symbol is incorrect.");
		Assertions.assertEquals(0, fixExecutionReport.getOrderQuantity().compareTo(new BigDecimal(10)), "Order quantity is incorrect.");
	}


	@Test
	public void testIOI() {
		getFixSimApiHandler().sendRawMessages(FIX_IOI_FILE, FIXSIM_INSTANCE, FIXSIM_SESSION);

		FixIOIEntity fixIOIEntity = getFixResponse(FixIOIEntity.class);
		Assertions.assertNotNull(fixIOIEntity, "Failed to get a Fix IOI Entity.");
		MatcherAssert.assertThat(fixIOIEntity.getIoiTransactionType(), IsEqual.equalTo("N"));
		Assertions.assertEquals("MSFT", fixIOIEntity.getSymbol(), "IOI symbol is incorrect.");
		Assertions.assertEquals("5", fixIOIEntity.getIoiQuantity(), "IOI quantity is incorrect.");
	}


	@Test
	public void testDropCopy() {
		getFixSimApiHandler().sendRawMessages(FIX_DROPCOPY_FILE, FIXSIM_INSTANCE, FIXSIM_SESSION);

		FixExecutionReport fixExecutionReport = getFixResponse(FixExecutionReport.class);
		Assertions.assertNotNull(fixExecutionReport, "Failed to get an Execution Report Object for the Drop Copy.");
		MatcherAssert.assertThat(fixExecutionReport.getOrderStatus(), IsEqual.equalTo(OrderStatuses.NEW));
		Assertions.assertEquals("GOOG", fixExecutionReport.getSymbol(), "Order symbol is incorrect.");
		Assertions.assertEquals(0, fixExecutionReport.getOrderQuantity().compareTo(new BigDecimal(10)), "Order quantity is incorrect.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	private <T extends FixEntity> T getFixResponse(Class<T> clazz) {
		FixEntityMessagingMessage fixResponse = getFixTestProcessor().getNextMessage();
		Assertions.assertNotNull(fixResponse, "No response received.");
		Assertions.assertSame(fixResponse.getFixEntity().getClass(), clazz);
		FixEntity fixEntity = fixResponse.getFixEntity();
		return (T) fixEntity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This configuration class is used to add the FixSimApiHandlerTests-context.xml to the main class <code>ContextConfiguration</code> annotation, since this
	 * annotation does not support mixing class-based and path-based resources.
	 *
	 * <code>@FilteredComponentScan</code> supplied with the packages is used to consume the Jackson module for FIX to make deserialization work with the tests.
	 */
	@Configuration
	@FilteredComponentScan(basePackages = {"com.clifton.core.messaging.jms.config", "com.clifton.fix.messaging.json"})
	@ImportResource("classpath:com/clifton/fix/tests/messaging/FixClientToSessionMessageTests-context.xml")
	@ExcludeFromComponentScan
	public static class FixClientToSessionMessageTestsConfiguration {
		//Nothing here
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessagingTestProcessor<FixEntityMessagingMessage> getFixTestProcessor() {
		return this.fixTestProcessor;
	}


	public void setFixTestProcessor(AsynchronousMessagingTestProcessor<FixEntityMessagingMessage> fixTestProcessor) {
		this.fixTestProcessor = fixTestProcessor;
	}


	public AsynchronousMessageHandler getTestFixMessagingHandler() {
		return this.testFixMessagingHandler;
	}


	public void setTestFixMessagingHandler(AsynchronousMessageHandler testFixMessagingHandler) {
		this.testFixMessagingHandler = testFixMessagingHandler;
	}


	public FixSimApiHandler getFixSimApiHandler() {
		return this.fixSimApiHandler;
	}


	public void setFixSimApiHandler(FixSimApiHandler fixSimApiHandler) {
		this.fixSimApiHandler = fixSimApiHandler;
	}


	public JmsQueueService<JmsAsynchronousErrorMessage> getFixJmsQueueService() {
		return this.fixJmsQueueService;
	}


	public void setFixJmsQueueService(JmsQueueService<JmsAsynchronousErrorMessage> fixJmsQueueService) {
		this.fixJmsQueueService = fixJmsQueueService;
	}
}
