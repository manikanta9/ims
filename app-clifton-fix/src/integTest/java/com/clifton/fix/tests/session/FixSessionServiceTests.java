package com.clifton.fix.tests.session;


import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.tests.BaseFixIntegrationTest;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * @author manderson
 */
public class FixSessionServiceTests extends BaseFixIntegrationTest {


	@Resource
	private FixSessionService fixSessionService;


	/**
	 * Example test to confirm connectivity to FIX application
	 **/
	@Test
	public void testGetFixSessionDefinition() {
		FixSessionDefinition fixSessionDefinition = this.fixSessionService.getFixSessionDefinition((short) 4);
		ValidationUtils.assertNotNull(fixSessionDefinition, "Missing FIX Session Definition with ID 4");
		ValidationUtils.assertEquals("Default Session", fixSessionDefinition.getName(), "Expected FIX Session Definition with ID 4 to be the Default Session");
	}

}
