package com.clifton.fix.tests.system.bean;

import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.test.system.bean.BaseSystemBeanTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {FixIntegrationConfiguration.class})
public class FixSystemBeanTests extends BaseSystemBeanTests {

	// NOTHING HERE
}
