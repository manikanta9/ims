package com.clifton.fix.tests.system.query;

import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.test.system.query.BaseSystemQueryServiceTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {FixIntegrationConfiguration.class})
public class FixSystemQueryServiceTests extends BaseSystemQueryServiceTests {

	// nothing here
}
