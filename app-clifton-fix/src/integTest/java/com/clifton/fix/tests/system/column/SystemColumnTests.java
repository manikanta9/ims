package com.clifton.fix.tests.system.column;

import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.test.system.column.BaseSystemColumnTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {FixIntegrationConfiguration.class})
public class SystemColumnTests extends BaseSystemColumnTests {

	// NOTHING HERE
}
