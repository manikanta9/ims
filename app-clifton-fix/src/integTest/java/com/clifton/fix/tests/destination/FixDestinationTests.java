package com.clifton.fix.tests.destination;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.session.search.FixSessionDefinitionSearchForm;
import com.clifton.fix.tests.BaseFixIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@link FixDestinationTests} type implements tests to verify general {@link FixDestination} characteristics.
 *
 * @author MikeH
 */
public class FixDestinationTests extends BaseFixIntegrationTest {

	@Resource
	private FixSessionService fixSessionService;
	@Resource
	private FixDestinationService fixDestinationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAllFixSessionsHaveDefaultDestination() {
		// Verify that all session definitions have a matching default destination
		List<FixSessionDefinition> definitionList = getFixSessionService().getFixSessionDefinitionList(new FixSessionDefinitionSearchForm());
		List<String> skippedDefinitionNameList = getSkippedSessionDefinitionNameList();
		List<FixSessionDefinition> definitionMissingDestinationList = definitionList.stream()
				.filter(definition -> !skippedDefinitionNameList.contains(definition.getName()))
				.filter(definition -> getFixDestinationService().getFixDestinationBySessionDefinition(definition.getId()) == null)
				.collect(Collectors.toList());
		if (!definitionMissingDestinationList.isEmpty()) {
			Assertions.fail(definitionMissingDestinationList.stream()
					.map(NamedEntityWithoutLabel::getName)
					.collect(Collectors.joining("\n- ", "Default destinations are missing for the following session definitions:\n- ", StringUtils.EMPTY_STRING)));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> getSkippedSessionDefinitionNameList() {
		return Arrays.asList(
				"Default Session",
				"Default Session_DEV",
				"Default Session_QA",
				"FIXSIM Session",
				"FIXSIM Session_DEV",
				"FIXSIM Session_QA",
				"IOI Stress Test (FIXSIM)",
				"IOI Stress Test (FIXSIM)_DEV",
				"IOI Stress Test (FIXSIM)_QA",
				"Test Initiator1",
				"Test Initiator2",
				"Test Initiator3"
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixDestinationService getFixDestinationService() {
		return this.fixDestinationService;
	}


	public void setFixDestinationService(FixDestinationService fixDestinationService) {
		this.fixDestinationService = fixDestinationService;
	}
}
