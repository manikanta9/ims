package com.clifton.fix.tests.security;


import com.clifton.core.context.spring.UrlDefinitionDescriptor;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.test.security.BaseUrlSecurityTests;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>FixUrlSecurityTests</code> checks each URL is able to determine a way to lookup security
 * If @SecureMethod annotation is associated with the Method, will consider it to be configured.  Will only actually attempt URLs that do not have explicit annotations
 *
 * @author manderson
 */
@ContextConfiguration(classes = {FixIntegrationConfiguration.class})
public class FixUrlSecurityTests extends BaseUrlSecurityTests {


	/**
	 * No users or groups have beyond READ access to Fix Messaging security resource.
	 * These are likely to be more administrative functions (or could be done with read access).
	 * If this test generates results then security should be reviewed...
	 */
	@Test
	public void testUrlSecurity_RequiresFixMessagingWriteAccess() {
		// Start with admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		List<UrlDefinitionDescriptor> urlList = getApplicationContextService().getContextUrlMappingDefinitionList();

		// Switch to Non Admin User Without Any Security Permissions
		this.userUtils.switchUser(ImsTestProperties.TEST_NO_PERMISSION_USER, ImsTestProperties.TEST_NO_PERMISSION_USER_PASSWORD);

		List<String> errors = new ArrayList<>();

		for (UrlDefinitionDescriptor urlDefinition : CollectionUtils.getIterable(urlList)) {
			String error = executeUrl(urlDefinition);
			if (!StringUtils.isEmpty(error)) {
				// Only include if NOT READ and for FIX Messaging Security Resource
				if (!error.contains("[READ]") && error.contains("[FIX Messaging]")) {
					errors.add(error);
				}
			}
		}

		if (CollectionUtils.getSize(errors) > 0) {
			Collections.sort(errors);
			throw new RuntimeException("URLS requiring more than READ access to FIX Messaging: "
					+ StringUtils.NEW_LINE + StringUtils.collectionToDelimitedString(errors, StringUtils.NEW_LINE));
		}
	}
}
