package com.clifton.fix.tests.load;

import quickfix.Application;
import quickfix.ConfigError;
import quickfix.DefaultMessageFactory;
import quickfix.FileLogFactory;
import quickfix.FileStoreFactory;
import quickfix.MessageFactory;
import quickfix.SessionSettings;
import quickfix.SocketAcceptor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;


/**
 * The <code>TestTradeAppExecutorApp</code> mimics 3 session acceptors that we can set our FIX app to connect to for heartbeats or simple testing (bulk loading of orders)
 * This DOES NOT actually run as a test, but after restoring fix with running migration.data.modules=app-clifton-fix-load-test=0
 * You can right click and just run this class and FIX sessions will connect and heartbeat.  3 sessions, the first 2 reset sequence #'s on log ons, the 3rd does not.
 * They all use different heart beat intervals (10s, 20s, 30s) so that sequence numbers shouldn't be the same for all the sessions.
 * <p>
 * Note: Only intended to be used for local debugging
 *
 * @author manderson
 */
public class TestTradeAppExecutorApp {

	private static final String settings = "#default settings for sessions\n" +
			"[DEFAULT]    # ——-> This will applies to all sessions\n" +
			"ConnectionType=acceptor\n" +
			"ResetOnLogon=Y\n" +
			"FileLogPath=C:/logs/fix/acceptor\n" +
			"LogonTimeout=6000\n" +
			"ReconnectInterval=6030\n" +
			"[SESSION] #A single session\n" +
			"BeginString=FIX.4.4\n" +
			"SenderCompID=local-test-acceptor[counter]\n" +
			"TargetCompID=local-test-initiator[counter]\n" +
			"StartDay=sunday\n" +
			"EndDay=saturday\n" +
			"StartTime=00:00:00\n" +
			"EndTime=00:00:00\n" +
			"HeartbeatInterval=[counter]0\n" +
			"CheckLatency=N\n" +
			"SocketAcceptPort=1200[counter]\n" +
			"SocketConnectHost=127.0.0.1\n" +
			"UseDataDictionary=Y\n" +
			"DataDictionary=C:/work/workspace/clifton-fix/subprojects/clifton-fix-quickfix/src/main/resources/META-INF/schema/quickfix/FIX44.xml\n" +
			"FileStorePath=C:/logs/fix/acceptor/ICE";


	/**
	 * The main method.
	 *
	 * @param args the args
	 */
	public static void main(String[] args) {
		try {
			SocketAcceptor socketAcceptor1 = createSocketAcceptor(settings.replace("[counter]", "1"));
			SocketAcceptor socketAcceptor2 = createSocketAcceptor(settings.replace("[counter]", "2"));
			SocketAcceptor socketAcceptor3 = createSocketAcceptor(settings.replace("[counter]", "3").replace("ResetOnLogon=Y", "ResetOnLogon=N"));
			socketAcceptor1.start();
			socketAcceptor2.start();
			socketAcceptor3.start();
		}
		catch (Throwable e) {
			e.printStackTrace();
		}
	}


	private static SocketAcceptor createSocketAcceptor(String settings) throws ConfigError {
		InputStream in = new ByteArrayInputStream(settings.getBytes());
		SessionSettings executorSettings = new SessionSettings(in);
		Application application = new TestTradeAppExecutor();
		FileStoreFactory fileStoreFactory = new FileStoreFactory(executorSettings);
		MessageFactory messageFactory = new DefaultMessageFactory();
		FileLogFactory fileLogFactory = new FileLogFactory(executorSettings);
		return new SocketAcceptor(application, fileStoreFactory, executorSettings, fileLogFactory, messageFactory);
	}
}
