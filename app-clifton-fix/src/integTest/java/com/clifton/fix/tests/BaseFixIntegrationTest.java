package com.clifton.fix.tests;

import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import com.clifton.test.BaseIntegrationTest;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(classes = {FixIntegrationConfiguration.class})
public abstract class BaseFixIntegrationTest extends BaseIntegrationTest {

	// NOTHING HERE (YET)
}
