package com.clifton.fix.tests.load;

import quickfix.Application;
import quickfix.DoNotSend;
import quickfix.FieldNotFound;
import quickfix.IncorrectDataFormat;
import quickfix.IncorrectTagValue;
import quickfix.LogUtil;
import quickfix.Message;
import quickfix.MessageCracker;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.UnsupportedMessageType;
import quickfix.field.AvgPx;
import quickfix.field.CumQty;
import quickfix.field.ExecID;
import quickfix.field.ExecType;
import quickfix.field.LastPx;
import quickfix.field.LeavesQty;
import quickfix.field.OrdStatus;
import quickfix.field.OrderID;
import quickfix.fix44.NewOrderSingle;


/**
 * The <code>TestTradeAppExecutor</code> is a mimic of an acceptor for Fix Sessions.
 * Mainly just prints messages and responds with simple Execution Report for a New Order Single (for load testing).
 * See {@link TestTradeAppExecutorApp} for additional details.
 *
 * @author manderson
 */
public class TestTradeAppExecutor extends MessageCracker implements Application {

	@Override
	public void onCreate(SessionID sessionId) {
		System.out.println("Executor Session Created with SessionID = " + sessionId);
	}


	@Override
	public void onLogon(SessionID sessionId) {
		System.out.println("Executor Session. Logon with SessionID = " + sessionId);
	}


	@Override
	public void onLogout(SessionID sessionId) {
		System.out.println("Executor Session. Logout with SessionID = " + sessionId);
	}


	@Override
	public void toAdmin(Message message, SessionID sessionId) {
		System.out.println("TO ADMIN: Executor SessionID = " + sessionId + " Message (" + message.getClass() + "): " + message);
	}


	@Override
	public void fromAdmin(Message message, SessionID sessionId) {
		System.out.println("FROM ADMIN: Executor SessionID = " + sessionId + " Message (" + message.getClass() + "): " + message);
	}


	@Override
	public void toApp(Message message, SessionID sessionId) throws DoNotSend {
		System.out.println("TO APP: Executor SessionID = " + sessionId + " Message (" + message.getClass() + "): " + message);
	}


	@Override
	public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
		System.out.println("FROM APP: Executor SessionID = " + sessionId + " Message (" + message.getClass() + "): " + message);
		crack(message, sessionId);
	}


	public void onMessage(NewOrderSingle order, SessionID sessionID) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
		System.out.println("###NewOrder Received:" + order.toString());
		System.out.println("###Symbol" + order.getSymbol().toString());
		System.out.println("###Side" + order.getSide().toString());
		System.out.println("###Type" + order.getOrdType().toString());
		System.out.println("###TransactionTime" + order.getTransactTime().toString());

		sendMessageToClient(order, sessionID);
	}


	public void sendMessageToClient(NewOrderSingle order, SessionID sessionID) {
		try {
			quickfix.fix44.ExecutionReport accept = new quickfix.fix44.ExecutionReport(new OrderID("133456"), new ExecID("789"),
					new ExecType(ExecType.NEW), new OrdStatus(OrdStatus.NEW), order.getSide(), new LeavesQty(0), new CumQty(order.getOrderQty().getValue()), new AvgPx(0));
			accept.set(order.getClOrdID());
			accept.set(order.getSymbol());
			accept.set(new LastPx(0));
			System.out.println("###Sending Order Acceptance:" + accept + "sessionID:" + sessionID.toString());
			Session.sendToTarget(accept, sessionID);
		}
		catch (Exception e) {
			LogUtil.logThrowable(sessionID, e.getMessage(), e);
		}
	}
}
