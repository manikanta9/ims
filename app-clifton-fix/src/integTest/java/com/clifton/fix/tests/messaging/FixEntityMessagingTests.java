package com.clifton.fix.tests.messaging;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.context.FilteredComponentScan;
import com.clifton.core.messaging.AsynchronousMessagingTestProcessor;
import com.clifton.core.messaging.jms.JmsQueueService;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandlerImpl;
import com.clifton.core.messaging.jms.asynchronous.JmsAsynchronousErrorMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.order.FixDontKnowTrade;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixMessageTextEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.tests.BaseFixIntegrationTest;
import com.clifton.fix.tests.spring.FixIntegrationConfiguration;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_CLASS;


/**
 * These tests send a FixEntityMessagingMessage to FIX with different types of FixEntity objects and checks
 * that FIX was able to receive and deserialize the message.
 *
 * @author lnaylor
 */

@ContextConfiguration(classes = {FixIntegrationConfiguration.class, FixEntityMessagingTests.FixEntityMessagingTestsConfiguration.class})
@PropertySource({"classpath:META-INF/integration-tests.properties"})
@PropertySource({"classpath:META-INF/integration-tests-overrides.properties"})
@EnabledIf(expression = "#{environment['fix.jms.host'] == 'activemq.dev-msp.paraport.com'}", loadContext = true)
@DirtiesContext(classMode = AFTER_CLASS)
class FixEntityMessagingTests extends BaseFixIntegrationTest {


	@Resource
	private AsynchronousMessagingTestProcessor<JmsAsynchronousErrorMessage> fixEntityMessagingTestProcessor;
	@Resource
	private AsynchronousMessageHandler testFixMessagingHandler;
	@Resource
	private JmsQueueService<JmsAsynchronousErrorMessage> jmsQueueService;

	private static final String FIX_ORDER_MESSAGE_STRING = "8=FIX.4.4\u00019=132\u000135=D\u000149=CLIFTON\u000150=fixsimTest\u000156=GSFUT\u00011=ACT-1\u000111=ORD-1\u000121=1\u000138=10.00000000\u000140=1\u000148=GOOG\u000154=1\u000155=GOOG\u000159=1\u000160=20210204-06:00:00.000\u000110=207\u0001";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void init() {
		//clear out error messages
		getJmsQueueService().purgeMessages(((AsynchronousMessageHandlerImpl) getTestFixMessagingHandler()).getErrorQueue());
	}


	@Test
	public void testSendFixTextEntityWithSessionInfo() {
		FixOrder order = createFixOrder();
		setCommonFixEntitySessionFields(order);
		FixMessageTextEntity textEntity = new FixMessageTextEntity();
		textEntity.setMessageText(FIX_ORDER_MESSAGE_STRING);
		textEntity.setFixSessionQualifier(order.getFixSessionQualifier());
		textEntity.setFixDestinationName(order.getFixDestinationName());
		this.send(textEntity);
	}


	@Test
	public void testSendFixTextEntityWithDestinationName() {
		FixOrder order = createFixOrder();
		setFixEntityDestinationName(order);
		FixMessageTextEntity textEntity = new FixMessageTextEntity();
		textEntity.setMessageText(FIX_ORDER_MESSAGE_STRING);
		textEntity.setFixSessionQualifier(order.getFixSessionQualifier());
		textEntity.setFixDestinationName(order.getFixDestinationName());
		this.send(textEntity);
	}


	@Test
	public void testSendFixOrderWithSessionInfo() {
		FixOrder order = createFixOrder();
		setCommonFixEntitySessionFields(order);
		this.send(order);
	}


	@Test
	public void testSendFixOrderWithDestinationName() {
		FixOrder order = createFixOrder();
		setFixEntityDestinationName(order);
		this.send(order);
	}


	@Test
	public void testSendFixExecutionReportWithSessionInfo() {
		FixExecutionReport fixExecutionReport = (FixExecutionReport) setCommonFixEntitySessionFields(new FixExecutionReport());
		this.send(fixExecutionReport);
	}


	@Test
	public void testSendFixExecutionReportWithDestinationName() {
		FixExecutionReport fixExecutionReport = (FixExecutionReport) setFixEntityDestinationName(new FixExecutionReport());
		this.send(fixExecutionReport);
	}


	@Test
	public void testSendFixOrderCancelReplaceRequestWithSessionInfo() {
		FixOrderCancelReplaceRequest fixOrderCancelReplaceRequest = (FixOrderCancelReplaceRequest) setCommonFixEntitySessionFields(createFixOrderCancelReplaceRequest());
		this.send(fixOrderCancelReplaceRequest);
	}


	@Test
	public void testSendFixOrderCancelReplaceRequestWithDestinationName() {
		FixOrderCancelReplaceRequest fixOrderCancelReplaceRequest = (FixOrderCancelReplaceRequest) setFixEntityDestinationName(createFixOrderCancelReplaceRequest());
		this.send(fixOrderCancelReplaceRequest);
	}


	@Test
	public void testSendFixDontKnowTradeWithSessionInfo() {
		FixDontKnowTrade fixDontKnowTrade = (FixDontKnowTrade) setCommonFixEntitySessionFields(createFixDontKnowTrade());
		this.send(fixDontKnowTrade);
	}


	@Test
	public void testSendFixDontKnowTradeWithDestinationName() {
		FixDontKnowTrade fixDontKnowTrade = (FixDontKnowTrade) setFixEntityDestinationName(createFixDontKnowTrade());
		this.send(fixDontKnowTrade);
	}


	@Test
	public void testSendFixAllocationInstructionWithSessionInfo() {
		FixAllocationInstruction fixAllocationInstruction = (FixAllocationInstruction) setCommonFixEntitySessionFields(createFixAllocationInstruction());
		this.send(fixAllocationInstruction);
	}


	@Test
	public void testSendFixAllocationInstructionWithDestinationName() {
		FixAllocationInstruction fixAllocationInstruction = (FixAllocationInstruction) setFixEntityDestinationName(createFixAllocationInstruction());
		this.send(fixAllocationInstruction);
	}


	@Test
	public void testSendFixOrderCancelRequestWithSessionInfo() {
		FixOrderCancelRequest fixOrderCancelRequest = (FixOrderCancelRequest) setCommonFixEntitySessionFields(createFixOrderCancelRequest());
		this.send(fixOrderCancelRequest);
	}


	@Test
	public void testSendFixOrderCancelRequestWithDestinationName() {
		FixOrderCancelRequest fixOrderCancelRequest = (FixOrderCancelRequest) setFixEntityDestinationName(createFixOrderCancelRequest());
		this.send(fixOrderCancelRequest);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends a <code>FixEntityMessagingMessage</code> with the given <code>FixEntity</code> to FIX. Because there is no valid session, FIX should
	 * place a message on the error queue, which will be picked up by the <code>AsynchronousMessagingTestProcessor</code> and checked to make sure the
	 * error message is as expected.
	 */
	public <T extends FixEntity> void send(T entity) {
		Assertions.assertNotNull(getTestFixMessagingHandler(), "Expected an Entity service for the trade.");

		FixEntityMessagingMessage message = new FixEntityMessagingMessage();
		message.setFixEntity(entity);

		getTestFixMessagingHandler().send(message);

		JmsAsynchronousErrorMessage errorMessage = getFixEntityMessagingTestProcessor().getNextMessage();
		Assertions.assertNotNull(errorMessage, "No message was received. The server may have not successfully processed the outgoing message.");
		MatcherAssert.assertThat(errorMessage.getError().getMessage(), Matchers.startsWith("No session found for message ["));
	}


	private FixEntity setCommonFixEntitySessionFields(FixEntity entity) {
		entity.setFixBeginString("FIX.4.4");
		entity.setFixSenderCompId("CLIFTON");
		entity.setFixTargetCompId("GSFUT");
		return entity;
	}


	private FixEntity setFixEntityDestinationName(FixEntity entity) {
		entity.setFixDestinationName("Goldman Sachs");
		return entity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixOrder createFixOrder() {
		FixOrder order = new FixOrder();
		order.setId((long) 1);
		order.setAccount("ACT-" + 1);
		order.setOrderType(OrderTypes.MARKET);
		order.setOrderQty(new BigDecimal(10));
		order.setSymbol("GOOG");
		order.setHandlingInstructions(HandlingInstructions.AUTOMATIC);

		order.setSide(OrderSides.BUY);
		order.setTransactionTime(new Date());
		order.setTimeInForce(TimeInForceTypes.GTC);
		order.setClientOrderStringId("ORD-" + 1);

		order.setSenderUserName("user_name");
		order.setFixSenderSubId("sender_sub_id");

		Assertions.assertNotNull(order, "Expected an Order for the trade.");
		return order;
	}


	private FixOrderCancelReplaceRequest createFixOrderCancelReplaceRequest() {
		FixOrderCancelReplaceRequest fixOrderCancelReplaceRequest = new FixOrderCancelReplaceRequest();
		fixOrderCancelReplaceRequest.setOrderType(OrderTypes.MARKET);
		fixOrderCancelReplaceRequest.setOrderQty(new BigDecimal(10));
		fixOrderCancelReplaceRequest.setSymbol("GOOG");
		fixOrderCancelReplaceRequest.setHandlingInstructions(HandlingInstructions.AUTOMATIC);

		fixOrderCancelReplaceRequest.setSide(OrderSides.BUY);
		fixOrderCancelReplaceRequest.setTransactionTime(new Date());
		fixOrderCancelReplaceRequest.setTimeInForce(TimeInForceTypes.GTC);
		fixOrderCancelReplaceRequest.setClientOrderStringId("ORD-" + 1);
		fixOrderCancelReplaceRequest.setSenderUserName("user_name");
		fixOrderCancelReplaceRequest.setFixSenderSubId("sender_sub_id");
		return fixOrderCancelReplaceRequest;
	}


	private FixDontKnowTrade createFixDontKnowTrade() {
		FixDontKnowTrade fixDontKnowTrade = new FixDontKnowTrade();
		fixDontKnowTrade.setOrderId("ORDER_ID");
		fixDontKnowTrade.setExecutionId("EXECUTION_ID");
		fixDontKnowTrade.setSymbol("GOOG");
		fixDontKnowTrade.setSecurityIDSource(SecurityIDSources.COMMON);
		fixDontKnowTrade.setSecurityType(SecurityTypes.NO_SECURITY_TYPE);
		fixDontKnowTrade.setSide(OrderSides.BUY);
		return fixDontKnowTrade;
	}


	private FixAllocationInstruction createFixAllocationInstruction() {
		FixAllocationInstruction fixAllocationInstruction = new FixAllocationInstruction();
		fixAllocationInstruction.setAllocationTransactionType(AllocationTransactionTypes.CANCEL);

		List<FixExecutionReport> fixExecutionReportList = new ArrayList<>();
		FixExecutionReport fixExecutionReport = new FixExecutionReport();
		fixExecutionReport.setExecutionType(ExecutionTypes.CANCELED);
		fixExecutionReportList.add(fixExecutionReport);

		List<FixOrder> fixOrderList = new ArrayList<>();
		FixOrder fixOrder = new FixOrder();
		fixOrder.setFixExecutionReportList(fixExecutionReportList);
		fixOrderList.add(fixOrder);

		fixAllocationInstruction.setFixOrderList(fixOrderList);
		return fixAllocationInstruction;
	}


	private FixOrderCancelRequest createFixOrderCancelRequest() {
		FixOrderCancelRequest fixOrderCancelRequest = new FixOrderCancelRequest();
		fixOrderCancelRequest.setClientOrderStringId("1");
		fixOrderCancelRequest.setHandlingInstructions(HandlingInstructions.AUTOMATIC);
		return fixOrderCancelRequest;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * This configuration class is used to add the FixSimApiHandlerTests-context.xml to the main class <code>ContextConfiguration</code> annotation, since this
	 * annotation does not support mixing class-based and path-based resources.
	 */
	@Configuration
	@ImportResource("classpath:com/clifton/fix/tests/messaging/FixEntityMessagingTests-context.xml")
	@FilteredComponentScan(basePackages = {"com.clifton.core.messaging.jms.config", "com.clifton.fix.messaging.json"})
	@ExcludeFromComponentScan
	public static class FixEntityMessagingTestsConfiguration {
		//Nothing here
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessagingTestProcessor<JmsAsynchronousErrorMessage> getFixEntityMessagingTestProcessor() {
		return this.fixEntityMessagingTestProcessor;
	}


	public void setFixEntityMessagingTestProcessor(AsynchronousMessagingTestProcessor<JmsAsynchronousErrorMessage> fixEntityMessagingTestProcessor) {
		this.fixEntityMessagingTestProcessor = fixEntityMessagingTestProcessor;
	}


	public AsynchronousMessageHandler getTestFixMessagingHandler() {
		return this.testFixMessagingHandler;
	}


	public void setTestFixMessagingHandler(AsynchronousMessageHandler testFixMessagingHandler) {
		this.testFixMessagingHandler = testFixMessagingHandler;
	}


	public JmsQueueService<JmsAsynchronousErrorMessage> getJmsQueueService() {
		return this.jmsQueueService;
	}


	public void setJmsQueueService(JmsQueueService<JmsAsynchronousErrorMessage> jmsQueueService) {
		this.jmsQueueService = jmsQueueService;
	}
}
