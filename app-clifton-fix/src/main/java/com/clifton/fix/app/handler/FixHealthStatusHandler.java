package com.clifton.fix.app.handler;

import com.clifton.core.health.HealthCheckParameters;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Handler to render the static templates for Fix Application. Should be implemented in order to handle a HealthCheck page template, and resolve all back-end behaviour related to
 * test execution.
 *
 * @author ChristianM.
 */
public interface FixHealthStatusHandler {

	public HealthCheckParameters parseParameters(HttpServletRequest request);


	public void impersonateUserPage();


	public String getEnvironment();


	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);


	public String executeHeartbeatTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals);
}
