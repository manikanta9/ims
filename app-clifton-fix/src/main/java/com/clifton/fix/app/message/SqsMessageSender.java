package com.clifton.fix.app.message;

import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class is an implementation of {@link MessageSender} to handle the messages targeted to AWS SQS. It uses the Asynchronous AWS SQS
 * client.
 *
 * @author vanand
 */
public class SqsMessageSender implements MessageSender {

	private final AsyncHandler<SendMessageRequest, SendMessageResult> responseHandler = new LoggingAwsAsyncHandler();

	private AmazonSQSAsync amazonSqsAsync;
	/**
	 * The queue used to send messages.
	 */
	private String outgoingQueue;
	private ObjectMapper objectMapper;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void send(AsynchronousMessage message) {
		try {
			String payload = getObjectMapper().writeValueAsString(message);
			getAmazonSqsAsync().sendMessageAsync(getOutgoingQueue(), payload, getResponseHandler());
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occured while sending the message to SQS. Queue: [%s]", getOutgoingQueue()), e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final class LoggingAwsAsyncHandler implements AsyncHandler<SendMessageRequest, SendMessageResult> {

		@Override
		public void onSuccess(SendMessageRequest request, SendMessageResult sendMessageResult) {
			LogUtils.info(LogCommand.ofMessage(getClass(), "Message sent to SQS successfully. Queue [", request.getQueueUrl(), "]. Message ID: [", sendMessageResult.getMessageId(), "]. Body length: [", request.getMessageBody().length(), "]."));
		}


		@Override
		public void onError(Exception exception) {
			LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), exception, "An error occured while sending the message to SQS. Queue: [", getOutgoingQueue(), "]."));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AsyncHandler<SendMessageRequest, SendMessageResult> getResponseHandler() {
		return this.responseHandler;
	}


	public AmazonSQSAsync getAmazonSqsAsync() {
		return this.amazonSqsAsync;
	}


	public void setAmazonSqsAsync(AmazonSQSAsync amazonSqsAsync) {
		this.amazonSqsAsync = amazonSqsAsync;
	}


	public String getOutgoingQueue() {
		return this.outgoingQueue;
	}


	public void setOutgoingQueue(String outgoingQueue) {
		this.outgoingQueue = outgoingQueue;
	}


	public ObjectMapper getObjectMapper() {
		return this.objectMapper;
	}


	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
}
