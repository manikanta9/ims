package com.clifton.fix.app.handler;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.health.HealthCheckHandler;
import com.clifton.core.health.HealthCheckParameters;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageEntryHeartbeatStatus;
import com.clifton.fix.message.FixMessageEntryHeartbeatStatusCommand;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.util.List;


/**
 * Handler for HealthCheck template view, resolved by Freemarker View Resolver. This component will resolve the test execution.
 *
 * @author ChristianM.
 */
@Component
public class FixHealthStatusHandlerImpl implements FixHealthStatusHandler, ApplicationContextAware {

	private ApplicationContext applicationContext;

	private ContextHandler contextHandler;

	private HealthCheckHandler healthCheckHandler;

	private FixMessageEntryService fixMessageEntryService;

	private SecurityUserService securityUserService;

	@Value("${application.environment.level:prod}")
	private String environment;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void impersonateUserPage() {
		SecurityUser pageUser = new SecurityUser();
		pageUser.setId((short) 999);
		pageUser.setUserName(SecurityUser.SYSTEM_USER);
		getContextHandler().setBean(Context.USER_BEAN_NAME, pageUser);
	}


	@Override
	public HealthCheckParameters parseParameters(HttpServletRequest request) {
		return this.healthCheckHandler.getHealthCheckPageParameters(request);
	}


	/*
	 * CliftonFix database access: systemuser should never be deleted
	 */
	@Override
	public String executeDbTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			SecurityUser user = getSecurityUserService().getSecurityUserByName(SecurityUser.SYSTEM_USER);
			ValidationUtils.assertNotNull(user, "Cannot find security user: " + SecurityUser.SYSTEM_USER);
		});
		return writer.toString();
	}


	/*
	 * FIX heartbeat check
	 */
	@Override
	public String executeHeartbeatTest(String testName, HealthCheckParameters healthCheckParameters, long maxAllowedTimeInMillis, List<String> disabledIntervals) {
		final StringWriter writer = new StringWriter();

		getHealthCheckHandler().runTest(testName, maxAllowedTimeInMillis, disabledIntervals, writer, healthCheckParameters, () -> {
			final short maximumAllowedHeartbeatIntervalMinutes = 5;
			List<FixMessageEntryHeartbeatStatus> lastHeartbeatList;
			FixMessageEntryHeartbeatStatusCommand fixMessageEntryHeartbeatStatusCommand = new FixMessageEntryHeartbeatStatusCommand();
			fixMessageEntryHeartbeatStatusCommand.setActiveSessionsOnly(true);
			fixMessageEntryHeartbeatStatusCommand.setReturnWarningsOnly(false);
			fixMessageEntryHeartbeatStatusCommand.setMaximumMinutesSinceLastHeartbeatWarn(maximumAllowedHeartbeatIntervalMinutes);

			try {
				lastHeartbeatList = getFixMessageEntryService().getFixMessageEntryLastHeartbeatBySession(fixMessageEntryHeartbeatStatusCommand);
			}
			catch (Throwable e) {
				if (e.getMessage().contains("null")) {
					throw new RuntimeException("Failed to get a list of FIX heartbeats.  Most likely cause is that there is no FIX server running.");
				}
				throw new RuntimeException("Failed to get a list of FIX heartbeats.", e);
			}

			StringBuilder heartbeatMsgBuilder = new StringBuilder();
			StringBuilder errorMessageBuilder = new StringBuilder();
			for (FixMessageEntryHeartbeatStatus heartbeatStatus : CollectionUtils.getIterable(lastHeartbeatList)) {
				if (heartbeatStatus.isFailed()) {
					errorMessageBuilder.append(heartbeatStatus.getStatusMessage()).append("<br/>");
				}
				else {
					heartbeatMsgBuilder.append("<br/>").append("&nbsp;&nbsp;").append(heartbeatStatus.getStatusMessage());
				}
			}
			String errorMessage = errorMessageBuilder.toString();
			ValidationUtils.assertTrue(StringUtils.isEmpty(errorMessage), errorMessage);

			return heartbeatMsgBuilder.toString();
		});
		return writer.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public HealthCheckHandler getHealthCheckHandler() {
		return this.healthCheckHandler;
	}


	public void setHealthCheckHandler(HealthCheckHandler healthCheckHandler) {
		this.healthCheckHandler = healthCheckHandler;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	@Override
	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}
}
