package com.clifton.fix.app.message;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSenderContext;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSenderProvider;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.credential.SecurityCredential;
import com.clifton.security.credential.SecurityCredentialService;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class is an implementation of {@link MessageSenderProvider} to handle the messages targeted to AWS SQS. It creates own  Asynchronous AWS SQS
 * client.
 *
 * @author vanand
 */
public class SqsMessageSenderProvider implements MessageSenderProvider {

	private String awsRegionName;
	private Short awsUserId;

	private SecurityCredentialService securityCredentialService;
	private SecuritySecretService securitySecretService;
	private ObjectMapper jmsObjectMapper;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MessageSender getMessageSender(MessageSenderContext messageSenderContext) {
		SqsMessageSender asynchronousSqsMessageSender = new SqsMessageSender();
		ValidationUtils.assertNotNull(getAwsUserId(), "AWS User is required.");
		SecurityCredential securityCredential = getSecurityCredentialService().getSecurityCredential(getAwsUserId());
		ValidationUtils.assertNotNull(securityCredential, "Cannot find security credential for ID " + getAwsUserId());
		String accessKey = securityCredential.getUserName();
		String secretKey = decrypt(securityCredential);
		AmazonSQSAsync amazonSQSAsync = AmazonSQSAsyncClientBuilder
				.standard()
				.withRegion(getAwsRegionName())
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.build();
		asynchronousSqsMessageSender.setAmazonSqsAsync(amazonSQSAsync);
		asynchronousSqsMessageSender.setOutgoingQueue(messageSenderContext.getOutgoingQueue());
		asynchronousSqsMessageSender.setObjectMapper(getJmsObjectMapper());
		return asynchronousSqsMessageSender;
	}


	/**
	 * This method decrypt the secret Key saved in the DB and returns the secret String
	 */
	private String decrypt(SecurityCredential securityCredential) {
		SecuritySecret securitySecret = securityCredential.getPasswordSecret();
		securitySecret = getSecuritySecretService().decryptSecuritySecret(securitySecret);
		return securitySecret.getSecretString();
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public String getAwsRegionName() {
		return this.awsRegionName;
	}


	public void setAwsRegionName(String awsRegionName) {
		this.awsRegionName = awsRegionName;
	}


	public Short getAwsUserId() {
		return this.awsUserId;
	}


	public void setAwsUserId(Short awsUserId) {
		this.awsUserId = awsUserId;
	}


	public SecurityCredentialService getSecurityCredentialService() {
		return this.securityCredentialService;
	}


	public void setSecurityCredentialService(SecurityCredentialService securityCredentialService) {
		this.securityCredentialService = securityCredentialService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public ObjectMapper getJmsObjectMapper() {
		return this.jmsObjectMapper;
	}


	public void setJmsObjectMapper(ObjectMapper jmsObjectMapper) {
		this.jmsObjectMapper = jmsObjectMapper;
	}
}
