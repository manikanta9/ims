-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|'
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|'
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|'

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================

-- Need to add an alias for Test User 1
DECLARE @TestUserID INT
SELECT @TestUserID = SecurityUserID
FROM SecurityUser
WHERE SecurityUserName = 'imstestuser1'

INSERT INTO SecuritySystemUser (SecuritySystemID, SecurityUserID, UserName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT SecuritySystemID, @TestUserID, 'imstestuser1', 0, GETDATE(), 0, GETDATE()
FROM SecuritySystem s
WHERE NOT EXISTS(SELECT * FROM SecuritySystemUser ss WHERE ss.SecurityUserID = @TestUserID AND ss.SecuritySystemID = s.SecuritySystemID)

UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';


-- Reset definitions to use QA Settings
UPDATE FixSessionDefinition
SET TargetCompId      = 'HTGM_QA_IOI',
	SocketConnectHost = '38.65.218.78',
	SocketConnectPort = '8205'
WHERE SessionDefinitionName = 'HTGM IOI'
UPDATE FixSessionDefinition
SET TargetCompId      = 'HTGM_QA_ORD',
	SocketConnectHost = '38.65.218.78',
	SocketConnectPort = '8206'
WHERE SessionDefinitionName = 'HTGM Order Entry'
UPDATE FixSessionDefinition
SET TargetCompId      = 'INCAUAT',
	TargetSubId       = NULL,
	SenderCompId      = 'PARAMETRICUAT',
	SocketConnectHost = '198.178.38.56',
	SocketConnectPort = '32206'
WHERE SessionDefinitionName = 'Newport EMS'
UPDATE FixSessionDefinition
SET SenderCompId      = 'PARAHYB',
	TargetCompId      = 'BLP_BETA',
	SocketConnectHost = '69.191.198.34',
	SocketConnectPort = '8228'
WHERE SessionDefinitionName = 'Bloomberg EMSX Allocation'

UPDATE FixSessionDefinition
SET TargetCompId      = 'MA',
	TargetSubId       = NULL,
	SenderCompId      = 'PARAPORTSTP',
	SocketConnectHost = '127.0.0.1',
	SocketConnectPort = '9002'
WHERE SessionDefinitionName = 'MarketAxess STP'

UPDATE FixSessionDefinition
SET SenderCompId      = 'MAP_PARA_BETA',
	TargetCompId      = 'MAP_BLP_BETA',
	SocketConnectHost = '160.43.172.32'
WHERE SessionDefinitionName = 'Bloomberg (PPA New York Fixed Income New)'


UPDATE FixSessionDefinition
SET SenderCompId = 'PARAPORT_CONTRI_UAT',
	TargetCompId = 'JPM_CONTRI_UAT'
WHERE SessionDefinitionName = 'JPM IOI'

UPDATE FixSessionDefinition
SET SenderCompId = 'PARAPORT_OE_UAT',
	TargetCompId = 'JPM_OE_UAT'
WHERE SessionDefinitionName = 'JPM Orders'

UPDATE FixSessionDefinitionSetting
SET SettingValue ='5W062sowFUHAC3JsjpQpUA'
WHERE FixSessionDefinitionID = (SELECT FixSessionDefinitionID FROM FixSessionDefinition WHERE SessionDefinitionName = 'Bloomberg (PPA New York Fixed Income New)')
  AND SettingName = 'SocketKeyStorePassword'

DELETE
FROM FixSessionDefinitionSetting
WHERE FixSessionDefinitionID = (SELECT FixSessionDefinitionID FROM FixSessionDefinition WHERE SessionDefinitionName = 'Newport EMS')

UPDATE FixSessionDefinition
SET SocketConnectHost = '38.160.71.97'
  , SenderCompId      = 'PRMETRIC'
WHERE SessionDefinitionName = 'Street FX'

DECLARE @TagModifierSystemBeanID     INTEGER = (SELECT FixTagModifierSystemBeanID
												FROM FixDestinationTagModifier fdtm
													 LEFT JOIN SystemBean sb ON fdtm.FixTagModifierSystemBeanID = sb.SystemBeanID
												WHERE fdtm.FixDestinationID = (SELECT FixDestinationID FROM FixDestination WHERE DestinationName = 'Street FX')
												  AND sb.SystemBeanName = 'Street FX Deliver to CompID (tag 128)')
DECLARE @TagModifierSystemBeanTypeID INTEGER = (SELECT SystemBeanTypeID
												FROM SystemBean
												WHERE SystemBeanID = @TagModifierSystemBeanID)
DECLARE @TagModifierPropertyID       INTEGER = (SELECT SystemBeanPropertyID
												FROM SystemBeanProperty
												WHERE SystemBeanID = @TagModifierSystemBeanID
												  AND SystemBeanPropertyTypeID = (SELECT SystemBeanPropertyTypeID
																				  FROM SystemBeanPropertyType
																				  WHERE SystemBeanPropertyTypeName = 'Field Value'
																					AND SystemBeanTypeID = @TagModifierSystemBeanTypeID))
UPDATE SystemBeanProperty
SET Text  = 'FXSISUAT2',
	Value = 'FXSISUAT2'
WHERE SystemBeanPropertyID = @TagModifierPropertyID

UPDATE FixSessionDefinition
SET ServerEnvironment = '@fix.session.serverEnvironment@'

UPDATE FixSourceSystem
SET SourceSystemTag = SourceSystemTag + '_@application.environment.level@'

UPDATE BatchJob
SET IsEnabled = 0


-- FX CONNECT (SEATTLE)
DECLARE @SystemID INT
SELECT @SystemID = SecuritySystemID
FROM SecuritySystem
WHERE SystemName = 'FX Connect (Seattle)'

INSERT INTO SecuritySystemUser (SecuritySystemID, SecurityUserID, UserName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT @SystemID
	 , u.SecurityUserID
	 , 'bos.parametric.mjackson'
	 , 0
	 , GETDATE()
	 , 0
	 , GETDATE()
FROM SecurityUser u
WHERE u.SecurityUserName = 'MasonJ'
  AND NOT EXISTS(SELECT * FROM SecuritySystemUser ss WHERE ss.SecurityUserID = u.SecurityUserID AND ss.SecuritySystemID = @SystemID)

UPDATE ssu
SET UserName =
		CASE
			WHEN u.SecurityUserName = 'MeganF' THEN 'bos.parametric.mfiorito'
			WHEN u.SecurityUserName = 'LeeT' THEN 'bos.parametric.lthacker'
			ELSE ssu.UserName
		END
FROM SecuritySystemUser ssu
	 INNER JOIN SecurityUser u ON ssu.SecurityUserID = u.SecurityUserID
WHERE ssu.SecuritySystemID = @SystemID


/*
Testing sessions for FixSim.  These are based on the production sessions, but may contain suffixes in SessionDefinition naming conventions, SenderCompID
TargetCompID to meet uniqueness requirements for DB and FixSim uniqueness constraints. Suffix _DEV idendifies sessions created for local dev environments,
and _QA designates QA tests sessions.

Author:  David Irizarry
*/

DECLARE @session_suffix         VARCHAR(5) = '_QA'
DECLARE @default_session        VARCHAR(50) = 'Default Session'
DECLARE @default_session_fixsim VARCHAR(50) = CONCAT(@default_session, @session_suffix)
DECLARE @fixsim_ip              VARCHAR(50) = 'app.fixsim.com'
DECLARE @fixsim_port            VARCHAR(50) = '15101'
DECLARE @connectivity_type_id   VARCHAR(50) = (SELECT SystemListItemID
											   FROM SystemListItem
											   WHERE SystemListID = (SELECT SystemListID FROM SystemList WHERE SystemListName = 'FIX Connectivity Types')
												 AND SystemListItemValue = 'N/A')

BEGIN TRANSACTION

-- Generate FixSessionDefinitions
INSERT INTO FixSessionDefinition ( ParentFixSessionDefinitionID
								 , SessionDefinitionName
								 , SessionDefinitionDescription
								 , ConnectivityTypeID
								 , SaveSequenceNumberBatchSize
								 , ServerEnvironment
								 , BeginString
								 , SenderCompId
								 , SenderSubId
								 , SenderLocId
								 , TargetCompId
								 , TargetSubId
								 , TargetLocId
								 , SessionQualifier
								 , DefaultApplVerID
								 , ConnectionType
								 , MessageQueue
								 , UnknownMessageFixSourceSystemID
								 , TimeZone
								 , StartWeekday
								 , StartTime
								 , EndWeekday
								 , EndTime
								 , HeartbeatInterval
								 , ReconnectInterval
								 , SocketConnectHost
								 , SocketConnectPort
								 , PathToExecutable
								 , ExecutableArgument
								 , IsCreateSession
								 , IsInformational
								 , IsDisabled
								 , CreateUserID
								 , CreateDate
								 , UpdateUserID
								 , UpdateDate
								 , Label
								 , NonPersistentMessageCacheSize)

SELECT ParentFixSessionDefinitionID
	 , CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix)
	 , SessionDefinitionDescription
	 , @connectivity_type_id
	 , SaveSequenceNumberBatchSize
	 , ServerEnvironment
	 , BeginString
	 , CONCAT(fsd.SenderCompId, @session_suffix)
	 , SenderSubId
	 , SenderLocId
	 , CONCAT(fsd.TargetCompId, @session_suffix)
	 , TargetSubId
	 , TargetLocId
	 , SessionQualifier
	 , DefaultApplVerID
	 , ConnectionType
	 , MessageQueue
	 , UnknownMessageFixSourceSystemID
	 , TimeZone
	 , StartWeekday
	 , StartTime
	 , EndWeekday
	 , EndTime
	 , HeartbeatInterval
	 , ReconnectInterval
	 , @fixsim_ip
	 , @fixsim_port
	 , NULL
	 , NULL
	 , IsCreateSession
	 , IsInformational
	 , IsDisabled
	 , 0
	 , GETDATE()
	 , 0
	 , GETDATE()
	 , CONCAT(SUBSTRING(fsd.Label, 1, 45), @session_suffix)
	 , NonPersistentMessageCacheSize
FROM FixSessionDefinition fsd

UPDATE FixSessionDefinition
SET ParentFixSessionDefinitionID = (SELECT FixSessionDefinitionID FROM FixSessionDefinition WHERE SessionDefinitionName = @default_session_fixsim)
WHERE ParentFixSessionDefinitionID IS NOT NULL
  AND SessionDefinitionName LIKE (CONCAT('%', @session_suffix))
UPDATE FixSessionDefinition
SET SenderCompId = NULL
WHERE SessionDefinitionName = @default_session_fixsim;

-- Generate Fix Session Definition Settings
INSERT INTO FixSessionDefinitionSetting (FixSessionDefinitionID, SettingName, SettingValue, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT (SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix)),
	   ds.SettingName,
	   ds.SettingValue,
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM FixSessionDefinitionSetting ds
	 LEFT JOIN FixSessionDefinition fsd ON ds.FixSessionDefinitionID = fsd.FixSessionDefinitionID
WHERE ds.SettingName NOT IN ('SocketUseSSL', 'SocketKeyStorePassword', 'SocketKeyStore', 'EnabledProtocols', 'ResetOnLogon')
  AND EXISTS(SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix))

-- Apply ResetOnLogon to all FixSim test sessions
INSERT INTO FixSessionDefinitionSetting (FixSessionDefinitionID, SettingName, SettingValue, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT FixSessionDefinitionID,
	   'ResetOnLogon',
	   'Y',
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM FixSessionDefinition
WHERE SessionDefinitionName LIKE ('%' + @session_suffix)
  AND SessionDefinitionName NOT LIKE ('Default Session_%')

-- Generate Fix Sessions
INSERT INTO FixSession (FixSessionDefinitionID, CreationDateAndTime, IncomingSequenceNumber, OutgoingSequenceNumber, isActiveSession)
SELECT (SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix)),
	   GETDATE(),
	   1,
	   1,
	   isActiveSession
FROM FixSession fs
	 LEFT JOIN FixSessionDefinition fsd ON fs.FixSessionDefinitionID = fsd.FixSessionDefinitionID
WHERE EXISTS(SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix))

-- Add generated FixSessionDefinition entries to FixDestination table
INSERT INTO FixDestination (FixSessionDefinitionID, DestinationName, DestinationDescription, IsDefaultDestination, CreateUserID, CreateDate, UpdateUserID, UpdateDate,
							SecuritySystemID)
SELECT (SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix)),
	   CONCAT(SUBSTRING(fd.DestinationName, 1, 45), @session_suffix),
	   DestinationDescription,
	   IsDefaultDestination,
	   0,
	   GETDATE(),
	   0,
	   GETDATE(),
	   SecuritySystemID
FROM FixDestination fd
	 LEFT JOIN FixSessionDefinition fsd ON fd.FixSessionDefinitionID = fsd.FixSessionDefinitionID
WHERE EXISTS(SELECT FixSessionDefinitionID FROM FixSessionDefinition sd WHERE sd.SessionDefinitionName = CONCAT(SUBSTRING(fsd.SessionDefinitionName, 1, 45), @session_suffix))

-- Link existing FixTagModifiers to New FixDestinations
INSERT INTO FixDestinationTagModifier (FixDestinationID, FixTagModifierSystemBeanID, TagModifierOrder, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT (SELECT FixDestinationID FROM FixDestination fd WHERE fd.DestinationName = CONCAT(SUBSTRING(fd2.DestinationName, 1, 45), @session_suffix)),
	   FixTagModifierSystemBeanID,
	   TagModifierOrder,
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM FixDestinationTagModifier fdtm
	 LEFT JOIN FixDestination fd2 ON fdtm.FixDestinationID = fd2.FixDestinationID
WHERE EXISTS(SELECT FixDestinationID FROM FixDestination fd WHERE fd.DestinationName = CONCAT(SUBSTRING(fd2.DestinationName, 1, 45), @session_suffix))

-- Apply filters to FixTagModifiers for the FixDestinations
INSERT INTO FixDestinationTagModifierFilter (FixDestinationTagModifierID, FixTagModifierFilterID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT (SELECT FixDestinationTagModifierID
		FROM FixDestinationTagModifier fdtm
			 LEFT JOIN FixDestination fd ON fdtm.FixDestinationID = fd.FixDestinationID
			 LEFT JOIN SystemBean tm ON fdtm.FixTagModifierSystemBeanID = tm.SystemBeanID
		WHERE CONCAT(SUBSTRING(fd2.DestinationName, 1, 45), @session_suffix) = fd.DestinationName
		  AND tm.SystemBeanID = tm2.SystemBeanID),
	   FixTagModifierFilterID,
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM FixDestinationTagModifierFilter tmf
	 LEFT JOIN FixDestinationTagModifier fdtm2 ON tmf.FixDestinationTagModifierID = fdtm2.FixDestinationTagModifierID
	 LEFT JOIN FixDestination fd2 ON fdtm2.FixDestinationID = fd2.FixDestinationID
	 LEFT JOIN SystemBean tm2 ON fdtm2.FixTagModifierSystemBeanID = tm2.SystemBeanID


-- Additional adjustments
UPDATE FixSessionDefinition
SET StartTime = '00:00:00 US/Central',
	EndTime   = '23:59:00 US/Central'
WHERE SessionDefinitionName LIKE ('%' + @session_suffix)

-- Add HeartbeatTag to Sessions used with FixSim
INSERT INTO FixSessionDefinitionSetting (FixSessionDefinitionID, SettingName, SettingValue, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT FixSessionDefinitionID,
	   'HeartbeatTag',
	   '553=Username: @user.name@. Message group: @fix.messageGroup@.',
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM FixSessionDefinition
WHERE SessionDefinitionName LIKE ('%' + @session_suffix)
  AND SessionDefinitionName NOT LIKE ('Default Session_%')

-- Enable sessions so they can be started when integration test starts the Fix Engine, as these will need to connect to FIXSim for testing. Applies to DEV environment only.
-- Enable sessions so they can be started when integration test starts the Fix Engine, as these will need to connect to FIXSim for testing. Applies to DEV environment only.
IF '@application.environment.level@' = 'DEV'
	BEGIN
		UPDATE FixSessionDefinition
		SET IsDisabled = 0
		WHERE SessionDefinitionName LIKE ('%' + @session_suffix)
	END

-- Enable sessions so they can be started when integration test starts the Fix Engine, as these will need to connect to FIXSim for testing. Applies to DEV environment only.
IF '@application.environment.level@' = 'DEV'
	BEGIN
		UPDATE FixSessionDefinition
		SET IsDisabled = 0
		WHERE SessionDefinitionName LIKE ('%' + @session_suffix)
	END

COMMIT

-- Replace IAM SQS user:
-- - Match QA private key (encrypted value manually pulled from QA)
-- - Use Non-Prod credentials
--
-- Retrieval query (source: https://newbedev.com/sql-sql-server-convert-varbinary-to-base64-string-code-example):
--  SELECT sc.SecurityCredentialName,
--  	   (SELECT ss.SecretValue '*' FOR XML PATH(''), BINARY BASE64)          SecretValueBase64,
--  	   (SELECT ss.EncryptionKey '*' FOR XML PATH(''), BINARY BASE64)        EncryptionKeyBase64,
--  	   (SELECT ss.InitializationVector '*' FOR XML PATH(''), BINARY BASE64) InitializationVectorBase64
--  FROM SecurityCredential sc
--  		 JOIN SecuritySecret ss ON sc.PasswordSecretID = ss.SecuritySecretID
--  WHERE sc.SecurityCredentialName = 'AWS SQS User - Investment Systems'
UPDATE ss
SET ss.SecretValue          = cast('R+5wTw0wNCrkd9fePKZtcNr6aVNu/ojlzvWw4i88VkMd7k4oEMTstjoyf0Ie7HkiDsluxm3HxQA=' AS XML).value('.', 'varbinary(max)'),
	ss.EncryptionKey        = cast('mmWNYd95uIofzeYv3AZmCiU4QQrpnsIUQUwxpMnGYhk=' AS XML).value('.', 'varbinary(max)'),
	ss.InitializationVector = cast('X+beq0jrR2yY+CF+' AS XML).value('.', 'varbinary(max)')
FROM SecurityCredential sc
		 JOIN SecuritySecret ss ON sc.PasswordSecretID = ss.SecuritySecretID
WHERE sc.SecurityCredentialName = 'AWS SQS User - Investment Systems';

UPDATE sc
SET sc.UserName = 'AKIASQSWKEI7XJM2BWU5'
FROM SecurityCredential sc
WHERE sc.SecurityCredentialName = 'AWS SQS User - Investment Systems';
