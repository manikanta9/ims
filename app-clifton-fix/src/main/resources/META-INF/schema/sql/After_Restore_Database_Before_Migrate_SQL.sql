IF 'OFF' = (SELECT snapshot_isolation_state_desc
			FROM sys.databases
			WHERE name = 'CliftonFIX')
	BEGIN
		ALTER DATABASE CliftonFIX SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
		ALTER DATABASE CliftonFIX SET ALLOW_SNAPSHOT_ISOLATION ON;
		ALTER DATABASE CliftonFIX SET READ_COMMITTED_SNAPSHOT ON;
		ALTER DATABASE CliftonFIX SET MULTI_USER;
	END


UPDATE FixSession
SET IncomingSequenceNumber = 1,
	OutgoingSequenceNumber = 1;

DELETE
FROM FixMessageStore;

DELETE
FROM FixSessionDefinitionSetting
WHERE SettingName IN ('SocketConnectHost1', 'SocketConnectPort1');

UPDATE FixSessionDefinitionSetting
SET SettingValue = 'app-clifton-fix/build/dist/logs'
WHERE SettingName = 'FileLogPath';

DECLARE @FixSessionDefinitionOverrides TABLE (
	SessionDefinitionName VARCHAR(50) COLLATE LATIN1_GENERAL_100_CI_AS_SC_UTF8,
	SocketConnectHost     NVARCHAR(50),
	SocketConnectPort     NVARCHAR(50),
	StartTime             NVARCHAR(50),
	EndTime               NVARCHAR(50),
	TargetCompID          NVARCHAR(50),
	SenderCompID          NVARCHAR(50)
)
INSERT INTO @FixSessionDefinitionOverrides
SELECT 'Goldman Sachs', '155.195.43.140', '41811', '16:33:00 US/Central', '16:30:00 US/Central', NULL, NULL
UNION
SELECT 'REDI Portfolio Trader', '192.214.97.195', '41811', '16:33:00 US/Central', '16:30:00 US/Central', NULL, NULL
UNION
SELECT 'REDI Multi-Broker', '192.214.97.195', '41811', '16:33:00 US/Central', '16:30:00 US/Central', NULL, NULL
UNION
SELECT 'FX Connect', NULL, NULL, '06:00:00 US/Central', '21:00:00 US/Central', 'FXCCXUAT', NULL
UNION
SELECT 'FX Connect (Seattle)', NULL, NULL, NULL, NULL, 'FXCCXUAT', NULL
UNION
SELECT 'Bloomberg (Parametric Minneapolis Fixed Income)', '69.191.198.32', '8228', '22:03:00 US/Central', '22:00:00 US/Central', NULL, NULL
UNION
SELECT 'Bloomberg EMSX', '69.191.198.34', '8228', '22:03:00 US/Central', '22:00:00 US/Central', 'BLPTEST', NULL
UNION
SELECT 'Bloomberg FX', '205.216.112.15', '20535', '19:03:00 US/Central', '19:00:00 US/Central', NULL, NULL
UNION
SELECT 'CITI CARE', '199.67.139.10', '9400', '19:03:00 US/Central', '19:00:00 US/Central', 'CITICFOXU', 'PPAMPLSU'
UNION
SELECT 'ATR Connection (Morgan Stanley)', '198.80.148.193', '7002', '21:05:00 US/Central', '20:59:00 US/Central', 'ATR', 'PARAPORT'
UNION
SELECT 'WEX (Wolverine Execution)', '216.200.23.44', '7554', '06:00:00 US/Central', '21:00:00 US/Central', 'WEXOEUAT', 'PPAMPLSUAT'
UNION
SELECT 'Bloomberg EMSX Allocation', '69.191.198.34', '8228', '22:03:00 US/Central', '22:00:00 US/Central', 'BLP_BETA', 'PARAHYB'
UNION
SELECT 'HTGM IOI', '38.65.218.78', '8205', '05:00:00 US/Central', '16:59:00 US/Central', 'HTGM_QA_IOI', 'PARAMETRIC'
UNION
SELECT 'HTGM Order Entry', '38.65.218.78', '8206', '05:00:00 US/Central', '16:59:00 US/Central', 'HTGM_QA_ORD', 'PARAMETRIC'
UNION
SELECT 'Newport EMS', '198.178.38.56', '32206', '13:00:00 US/Central', '19:25:00 US/Central', 'INCAUAT', 'PARAMETRICUAT'
UNION
SELECT 'MarketAxess STP', NULL, NULL, '06:00:00 US/Central', '21:00:00 US/Central', 'MA', 'PARAPORTSTP'

UPDATE fsd
SET SocketConnectHost = COALESCE(o.SocketConnectHost, fsd.SocketConnectHost)
  , SocketConnectPort = COALESCE(o.SocketConnectPort, fsd.SocketConnectPort)
  , StartTime         = COALESCE(o.StartTime, fsd.StartTime)
  , EndTime           = COALESCE(o.EndTime, fsd.EndTime)
  , TargetCompID      = COALESCE(o.TargetCompID, fsd.TargetCompID)
  , SenderCompID      = COALESCE(o.SenderCompID, fsd.SenderCompId)
FROM @FixSessionDefinitionOverrides o
	 INNER JOIN FixSessionDefinition fsd ON o.SessionDefinitionName = fsd.SessionDefinitionName


UPDATE s
SET SettingValue = '69.191.230.32'
FROM FixSessionDefinitionSetting s
	 INNER JOIN FixSessionDefinition d ON d.FixSessionDefinitionID = s.FixSessionDefinitionID
WHERE d.SessionDefinitionName = 'Bloomberg (Parametric Minneapolis Fixed Income)'
  AND SettingName = 'SocketConnectHost1'

UPDATE s
SET SettingValue='BxdwZKZkmg0Q9uKlFoiaSg'
FROM FixSessionDefinitionSetting s
	 INNER JOIN FixSessionDefinition d ON d.FixSessionDefinitionID = s.FixSessionDefinitionID
WHERE d.SessionDefinitionName = 'Bloomberg (Parametric Minneapolis Fixed Income)'
  AND SettingName = 'SocketKeyStorePassword'

UPDATE s
SET SettingValue='u4NM8TzIq66sFKiTfGndBw'
FROM FixSessionDefinitionSetting s
	 INNER JOIN FixSessionDefinition d ON d.FixSessionDefinitionID = s.FixSessionDefinitionID
WHERE d.SessionDefinitionName = 'Bloomberg EMSX Allocation'
  AND SettingName = 'SocketKeyStorePassword'

UPDATE s
SET SettingValue='jhaKbrZ36YBnhmh9bgEVmw'
FROM FixSessionDefinitionSetting s
	 INNER JOIN FixSessionDefinition d ON d.FixSessionDefinitionID = s.FixSessionDefinitionID
WHERE d.SessionDefinitionName = 'Bloomberg EMSX'
  AND SettingName = 'SocketKeyStorePassword'


DELETE
FROM s
FROM FixSessionDefinitionSetting s
	 INNER JOIN FixSessionDefinition d ON d.FixSessionDefinitionID = s.FixSessionDefinitionID
WHERE d.SessionDefinitionName IN ('Bloomberg FX', 'CITI CARE')


UPDATE FixSessionDefinition
SET IsDisabled = 1
WHERE SessionDefinitionName <> 'Default Session'

-- TODO THIS RELIES ON IMS AND WE SHOULD GET RID OF IT, FOR NOW ADDING A DB CHECK
DECLARE @MaxOrderId INT
IF EXISTS(SELECT *
		  FROM master.dbo.sysdatabases
		  WHERE name = 'CliftonIMS')
	BEGIN
		SET @MaxOrderId = (
							  SELECT MAX(ExternalIdentifier)
							  FROM (
									   SELECT MAX(ExternalIdentifier) AS ExternalIdentifier
									   FROM CliftonIMS.dbo.TradeOrder
									   UNION ALL
									   SELECT MAX(ExternalIdentifier)
									   FROM CliftonIMS.dbo.TradeOrderAllocation
									   UNION ALL
									   SELECT MAX(FixIdentifierID)
									   FROM FixIdentifier
								   ) x
						  ) + 1
	END
ELSE
	BEGIN
		SET @MaxOrderId = (
							  SELECT MAX(FixIdentifierID)
							  FROM FixIdentifier
						  ) + 1

	END
DBCC CHECKIDENT (FixIdentifier, RESEED, @MaxOrderId)
