<?xml version="1.0" encoding="UTF-8"?>
<beans default-lazy-init="true" default-autowire="byName"
       xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
		http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/context
		http://www.springframework.org/schema/context/spring-context.xsd
		http://www.springframework.org/schema/util
		http://www.springframework.org/schema/util/spring-util.xsd">

	<!-- Load properties file for variable replacement -->
	<context:property-placeholder location="classpath:/META-INF/app-clifton-fix.properties" />

	<!-- Import migration-context-core then override beans -->
	<import resource="classpath:/META-INF/schema/migration-context-core.xml" />


	<!-- ******************************************************************* -->
	<!-- ** MIGRATION CONTEXT CORE OVERRIDES BEGIN                        ** -->
	<!-- ******************************************************************* -->
	<bean id="migrationSchemaSQLConverter" class="com.clifton.core.dataaccess.migrate.converter.MSSQLSchemaSQLConverter">
		<property name="sqlReplacementString" value="${migration-sql-replacements}" />
		<property name="ddlUpdateMigrationConverter">
			<ref bean="migrationSchemaDDLUpdateConverter" />
		</property>
	</bean>
	<!-- ******************************************************************* -->
	<!-- ** MIGRATION CONTEXT CORE OVERRIDES END                        ** -->
	<!-- ******************************************************************* -->


	<!-- ******************************************************************* -->
	<!-- ** MIGRATION CONTEXT BEAN DEFINITIONS BEGIN                      ** -->
	<!-- ******************************************************************* -->
	<bean id="migrationExecutionHandler" class="com.clifton.core.dataaccess.migrate.execution.MigrationExecutionHandlerImpl">
		<property name="migrationExecutionActionHandler" ref="migrationExecutionActionHandler" />
		<property name="migrationExecutionDatabaseHandler" ref="migrationExecutionDatabaseHandler" />
		<property name="migrationFileLocator" ref="migrationScanFileLocator" />
		<property name="moduleListString" value="${migration.modules}${migration.data.modules}" />
		<property name="migrationDefinitionReader" ref="migrationXmlDefinitionReader" />
		<property name="migrationSQLConverterList">
			<util:list>
				<ref bean="migrationSchemaPreDDLConverter" />
				<ref bean="migrationSchemaDDLConverter" />
				<ref bean="migrationSchemaMetaDataConverter" />
				<ref bean="migrationSchemaDataConverter" />
				<ref bean="migrationSchemaDataActionConverter" />
				<ref bean="migrationSchemaSQLConverter" />
			</util:list>
		</property>
		<property name="transactionManager" ref="transactionManager" />
	</bean>

	<bean id="migrationExecutionActionHandler" class="com.clifton.core.dataaccess.migrate.execution.action.MigrationExecutionActionHandlerImpl">
		<property name="jsonHandler" ref="migrationJsonHandler" />
		<property name="migrationExecutionDatabaseHandler" ref="migrationExecutionDatabaseHandler" />
		<property name="migrationUser">
			<bean class="com.clifton.security.user.SecurityUser">
				<property name="id" value="1" />
				<property name="userName" value="systemUser" />
				<property name="integrationPseudonym" value="systemUser" />
			</bean>
		</property>
	</bean>

	<bean id="migrationExecutionDatabaseHandler" class="com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionMSSQLDatabaseHandlerImpl">
		<property name="migrationJdbcTemplate" ref="migrationJdbcTemplate" />
	</bean>

	<bean id="dataSource" parent="dataSourceTemplate" autowire="no">
		<property name="driverClassName" value="${dataSource.driverClassName}" />
		<property name="url" value="${dataSource.url}" />
		<property name="username" value="${migration.username}" />
		<property name="password" value="${migration.password}" />
	</bean>

	<!-- Wrap data source in a lazy connection proxy so a connection is only established when needed. Use caches, when available, with no connection. -->
	<bean id="lazyConnectionDataSource" class="org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy">
		<property name="targetDataSource" ref="dataSource" />
	</bean>

	<bean id="masterDataSource" parent="dataSourceTemplate" autowire="no">
		<property name="driverClassName" value="${dataSource.driverClassName}" />
		<property name="url" value="${masterDataSource.url}" />
		<property name="username" value="${migration.username}" />
		<property name="password" value="${migration.password}" />
	</bean>

	<bean id="migrationJdbcTemplate" class="com.clifton.core.dataaccess.migrate.MigrationJdbcTemplate">
		<property name="migrationDatabaseName" value="${dataSource.databaseName}" />
		<property name="restoreFromFileLocation" value="${dataSource.restoreFromFileLocation}" />
		<property name="restoreFromOriginalDatabaseName" value="${dataSource.restoreFromOriginalDatabaseName}" />
		<property name="mdfFileLocation" value="${dataSource.mdfFileLocation}" />
		<property name="logFileLocation" value="${dataSource.logFileLocation}" />
		<property name="backupToFileLocation" value="${dataSource.backupToFileLocation}" />
		<constructor-arg ref="dataSource" />
		<constructor-arg ref="masterDataSource" />
	</bean>

	<!-- Transaction Handling -->
	<bean id="transactionManager" class="org.springframework.orm.hibernate5.HibernateTransactionManager">
		<property name="sessionFactory" ref="sessionFactory" />
		<property name="dataSource" ref="dataSource" />
		<property name="defaultTimeout" value="90" />
		<qualifier value="transactionManager" />
	</bean>

	<bean id="sessionFactory" class="org.springframework.orm.hibernate5.LocalSessionFactoryBean">
		<property name="dataSource" ref="dataSource" />
		<!-- enable reusing the same cache region factory bean: declaring this bean removes the need to use hibernate's "hibernate.cache.region.factory_class" property -->
		<property name="cacheRegionFactory" ref="cacheRegionFactory" />
		<property name="hibernateProperties">
			<props>
				<prop key="hibernate.dialect">${hibernate.dialect}</prop>
				<prop key="hibernate.show_sql">${hibernate.show_sql}</prop>
				<prop key="hibernate.format_sql">${hibernate.format_sql}</prop>
				<prop key="hibernate.generate_statistics">${hibernate.generate_statistics}</prop>
				<prop key="hibernate.cache.cache_manager_factory_bean_name">cacheManagerFactory</prop>
				<prop key="hibernate.entity_dirtiness_strategy">com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy</prop>
				<prop key="hibernate.ejb.metamodel.population">disabled</prop>
				<prop key="hibernate.cache.ehcache.missing_cache_strategy">create</prop>
				<!-- use: SCOPE_IDENTITY() -->
			</props>
		</property>
		<property name="mappingResources" ref="hibernateMappingResources" />
	</bean>
	<!-- ******************************************************************* -->
	<!-- ** MIGRATION CONTEXT BEAN DEFINITIONS END                        ** -->
	<!-- ******************************************************************* -->


	<!-- ******************************************************************* -->
	<!-- ** DATA ACCESS DEFINITIONS BEGIN                                 ** -->
	<!-- ******************************************************************* -->
	<util:list id="hibernateMappingResources">
		<value>META-INF/schema/schema-hibernate-core.xml</value>
		<value>META-INF/schema/schema-hibernate-security.xml</value>
		<value>META-INF/schema/schema-hibernate-system.xml</value>
		<value>META-INF/schema/schema-hibernate-system-note.xml</value>
		<value>META-INF/schema/schema-hibernate-system-priority.xml</value>
		<value>META-INF/schema/schema-hibernate-system-query.xml</value>
		<value>META-INF/schema/schema-hibernate-system-statistic.xml</value>
		<value>META-INF/schema/schema-hibernate-system-statistic-queries.xml</value>
		<value>META-INF/schema/schema-hibernate-calendar.xml</value>
		<value>META-INF/schema/schema-hibernate-calendar-schedule.xml</value>
		<value>META-INF/schema/schema-hibernate-batch.xml</value>
		<value>META-INF/schema/schema-hibernate-workflow.xml</value>
		<value>META-INF/schema/schema-hibernate-fix.xml</value>
	</util:list>


	<util:list id="appSchemaDependencyList">
		<value>security</value>
		<value>system</value>
		<value>system-note</value>
		<value>system-priority</value>
		<value>system-query</value>
		<value>system-statistic</value>
		<value>calendar</value>
		<value>calendar-schedule</value>
		<value>batch</value>
		<value>workflow</value>
		<value>fix</value>
	</util:list>


	<!-- AUTO-DISCOVERS DAO's FROM HIBERNATE MAPPING FILES AND CREATES CORRESPONDING DAO BEANS -->
	<bean id="fixDaoRegistrator" class="com.clifton.core.dataaccess.dao.hibernate.HibernateDAOFactoryPostProcessor" depends-on="cacheRegionFactory">
		<property name="schemaNameList" ref="appSchemaDependencyList" />
	</bean>

	<!-- ****************************************************************************
		INITIALIZE CONTEXT
		Scan for components and services, but exclude based on annotations and assignableFrom
		By excluding the applicationListeners from the scan we disable the registrators defined in clifton-core:
			daoObserverRegistrator
			hierarchicalObjectObserverRegistrator
		Which in turn disables validation observers as well as systemAuditRegistrator, etc.
		I explicitly declare the base-package for individual modules so that I don't have to load dw, fix, and portal.
		If I load those then you will get warnings during migrations regarding the DAOs that can't be found
		because I exclude those projects from the DAO Registrator above.
	******************************************************************************* -->
	<context:component-scan
			base-package="com.clifton.core,com.clifton.security,com.clifton.system,com.clifton.calendar,com.clifton.batch,com.clifton.workflow,com.clifton.fix"
			name-generator="com.clifton.core.context.spring.AnnotationUsingOurConventionBeanNameGenerator">
		<context:exclude-filter type="annotation" expression="com.clifton.core.context.ExcludeFromComponentScan" />
		<context:exclude-filter type="assignable" expression="org.springframework.context.ApplicationListener" />
		<context:exclude-filter type="annotation" expression="org.springframework.boot.SpringBootConfiguration" />
		<context:exclude-filter type="annotation" expression="org.springframework.context.annotation.Configuration" />
	</context:component-scan>


	<!-- ****************************************************************************
		IMPORT DEPENDENT CONTEXTS
		I load only the context files I need to in order to register necessary DAOs.
		A warning is output by migration if a DAO can not be mapped properly and
		then that project context file should be included if migrating that type of
		object is necessary.
	******************************************************************************* -->
	<import resource="classpath:META-INF/spring/clifton-security-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-system-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-system-note-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-system-priority-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-system-query-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-system-statistic-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-security-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-calendar-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-batch-context.xml" />
	<import resource="classpath:META-INF/spring/clifton-workflow-context.xml" />
	<!-- ******************************************************************* -->
	<!-- ** DATA ACCESS DEFINITIONS END                                   ** -->
	<!-- ******************************************************************* -->
</beans>
