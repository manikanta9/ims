// disable accidental Browser back button or refresh for non-local instances
TCG.enableConfirmBeforeLeavingWindow();


/*
 * Apply constants
 */
TCG.ApplicationName = TCG.PageConfiguration.applicationName;
TCG.ExternalApplicationNames = TCG.PageConfiguration.externalApplicationNames;
TCG.websocket.enabled = TCG.PageConfiguration.websocket.enabled;
TCG.websocket.endpoint = TCG.PageConfiguration.websocket.endpoint;

/*
 * Initialize application
 */
TCG.app.app = new TCG.app.Application({
	getTopBar: function() {
		return {
			html: '<div class="x-panel-header">' +
				'<table cellspacing="0" cellpadding="0" width="100%" class="app-header">' +
				'<tr>' +
				'<td width="23" qtip="Minneapolis Investment Center - FIX Application"><img src="core/images/icons/parametric.png" height="16" border="0" /></td>' +
				'<td style="FONT-WEIGHT: bold; COLOR: #646464; FONT-FAMILY: Arial;">FIX Application</td>' +
				'<td align="right" id="welcomeMessage">&nbsp;</td>' +
				'</tr>' +
				'</table>' +
				'</div>'
		};
	},
	getBody: function() {
		let suffix = '';
		const instanceText = TCG.PageConfiguration.instance.text;
		if (TCG.isNotBlank(instanceText)) {
			suffix = `<br /><br /><br /><div style="WIDTH: 100%; TEXT-ALIGN: center; COLOR: ${TCG.PageConfiguration.instance.color}; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: ${TCG.PageConfiguration.instance.size};">${instanceText}</div>`;
		}
		return {
			html: TCG.trimWhitespace(`
				<div style="WIDTH: 100%; TEXT-ALIGN: center; PADDING-TOP: 50px">
					<div><img src="core/images/logo/Parametric-120px-height.png" border="0" /></div>
					<div style="PADDING-TOP: 20px; COLOR: #646464; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 20pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">FIX Application</div>
					</div>
					<div style="COLOR: #34abdb; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 16pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">${TCG.PageConfiguration.instance.version}</div>
					</div>
					${suffix}
				</div>
			`)
		};
	},
	getMenuBar: function() {
		const mb = new Ext.Toolbar({height: 40});
		mb.render(Ext.getBody());

		mb.add(
			{
				text: 'FIX Messages',
				handler: function() {
					TCG.createComponent('Clifton.fix.FixMessageListWindow');
				}
			}, '-',
			{
				text: 'FIX Setup',
				handler: function() {
					TCG.createComponent('Clifton.fix.FixSetupWindow');
				}
			}, '-', {
				text: 'Data',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Query Exports',
							iconCls: 'excel',
							handler: function() {
								TCG.createComponent('Clifton.system.query.QueryExportListWindow');
							}
						},
						{
							text: 'FIX Dashboard',
							iconCls: 'chart-bar',
							handler: function() {
								TCG.createComponent('Clifton.fix.FixDashboardWindow');
							}
						}
					]
				})
			}, '-',
			{
				text: 'Tools',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'System Uploads',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.system.upload.UploadWindow');
						}
					}, {
						text: 'Administration',
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'Batch Jobs',
									iconCls: 'clock',
									handler: function() {
										TCG.createComponent('Clifton.batch.job.JobSetupWindow');
									}
								},
								{
									text: 'Security',
									iconCls: 'lock',
									handler: function() {
										TCG.createComponent('Clifton.security.SecuritySetupWindow');
									}
								},
								{
									text: 'Schema',
									iconCls: 'grid',
									handler: function() {
										TCG.createComponent('Clifton.system.schema.SchemaSetupWindow');
									}
								}, {
									text: 'System Beans',
									iconCls: 'objects',
									handler: function() {
										TCG.createComponent('Clifton.system.bean.BeanSetupWindow');
									}
								}, {
									text: 'System Conditions',
									iconCls: 'question',
									handler: function() {
										TCG.createComponent('Clifton.system.condition.ConditionSetupWindow');
									}
								}, '-', {
									text: 'Audit Trail',
									iconCls: 'pencil',
									handler: function() {
										TCG.createComponent('Clifton.system.audit.AuditSetupWindow');
									}
								}, {
									text: 'Centralized Logging',
									iconCls: 'explorer',
									handler: function() {
										const config = {
											defaultApplicationFilter: 'FIX',
											graylogUrl: `${TCG.PageConfiguration.graylog.apiScheme}://${TCG.PageConfiguration.graylog.apiHost}`
										};
										TCG.createComponent('Clifton.core.logging.LogMessageListWindow', config);
									}
								}, {
									text: 'System Ops Views',
									iconCls: 'doctor',
									handler: function() {
										TCG.createComponent('Clifton.core.stats.StatsListWindow');
									}
								}, {
									text: 'System Access Stats',
									iconCls: 'timer',
									handler: function() {
										TCG.createComponent('Clifton.system.statistic.StatisticListWindow');
									}
								}
							]
						})
					}, {
						text: 'Setup',
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'Calendars',
									handler: function() {
										TCG.createComponent('Clifton.calendar.CalendarSetupWindow');
									},
									iconCls: 'calendar'
								}, {
									text: 'Calendar Schedules',
									handler: function() {
										TCG.createComponent('Clifton.calendar.schedule.ScheduleSetupWindow');
									},
									iconCls: 'schedule'
								},
								{
									text: 'Hierarchies',
									handler: function() {
										TCG.createComponent('Clifton.system.hierarchy.HierarchySetupWindow');
									},
									iconCls: 'hierarchy'
								}, {
									text: 'Lists and Queries',
									iconCls: 'list',
									handler: function() {
										TCG.createComponent('Clifton.system.list.ListSetupWindow');
									}
								}, {
									text: 'Notes',
									iconCls: 'pencil',
									handler: function() {
										TCG.createComponent('Clifton.system.note.NoteSetupWindow');
									}
								}, {
									text: 'Priorities',
									iconCls: 'priority',
									handler: function() {
										TCG.createComponent('Clifton.system.priority.PrioritySetupWindow');
									}
								}, {
									text: 'Workflow',
									iconCls: 'workflow',
									handler: function() {
										TCG.createComponent('Clifton.workflow.WorkflowSetupWindow');
									}
								}, {
									text: 'Workflow Tasks',
									iconCls: 'task',
									handler: function() {
										TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
									}
								}]
						})
					}, {
						text: 'String Differences',
						iconCls: 'diff',
						handler: function() {
							new TCG.app.OKCancelWindow({
								title: 'String Differences Window',
								iconCls: 'diff',
								id: 'stringDiffWindow',
								enableShowInfo: false,
								closeWindowTrail: true,
								okButtonText: 'Compare',
								okButtonTooltip: 'Compare String 1 to String 2 and highlight differences.',
								width: 750,
								height: 450,

								items: [{
									xtype: 'formpanel',
									instructions: 'Use this window to compare 2 strings and highlight differences. Enter or copy/paste value for each string and click "Compare" button.',
									labelWidth: 70,
									items: [
										{fieldLabel: 'String 1', name: 'string1', xtype: 'textarea', height: 150},
										{fieldLabel: 'String 2', name: 'string2', xtype: 'textarea', height: 150}
									]
								}],
								saveWindow: function() {
									const fp = this.getMainFormPanel();
									TCG.showDifferences(fp.getFormValue('string1'), fp.getFormValue('string2'));
								}
							});
						}
					}, {
						text: 'User Tools',
						iconCls: 'tools',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Change Password',
								iconCls: 'key',
								handler: function() {
									TCG.showInfo('System authentication is integrated with company\'s Active Directory.<br/><br/>To change password, press &lt;Ctrl&gt; + &lt;Alt&gt; + &lt;Delete&gt; while logged in with your corporate Windows account. Click the [Change Password...] button and follow instructions for password change.', 'Password Change Instructions');
								}
							}, {
								text: 'User Preferences',
								disabled: true,
								iconCls: 'config',
								handler: menuSelect
							}]
						})
					}]
				})
			},
			'-',
			new TCG.app.WindowMenu(this),
			'-',
			{
				text: 'Help',
				menu: {
					items: [
						TCG.app.ContactSupportMenu, '-', {
							text: 'FIX Documentation',
							iconCls: 'help',
							handler: function() {
								TCG.openWIKI('IT/FIX+Integration', 'FIX Documentation');
							}
						}, {
							text: 'API Documentation',
							iconCls: 'pencil',
							handler: function() {
								new TCG.app.URLWindow({
									id: 'apiDocumentationWindow',
									title: 'API Documentation',
									width: 1200,
									height: 800,
									iconCls: 'pencil',
									url: `./webjars/swagger-ui/index.html?url=${encodeURIComponent(window.location.href)}openapi.yaml`
								});
							}
						}, '-', {
							text: 'System Health Check',
							iconCls: 'verify',
							handler: function() {
								new TCG.app.URLWindow({
									id: 'healthCheckWindow',
									title: 'System Health Check',
									width: 1000,
									height: 600,
									iconCls: 'verify',
									url: 'health-status?enableTimeIntervals=false'
								});
							}
						}
					]
				}
			},
			'-',
			{
				text: 'Logout',
				id: 'logoutMenu',
				handler: () => TCG.data.AuthHandler.logout()
			}
		);

		return mb;
	}
});


TCG.app.app.addListener('afterload', function() {
	// easy way to distinguish environments
	const theme = TCG.PageConfiguration.instance.theme;
	if (TCG.isNotBlank(theme)) {
		Ext.util.CSS.swapStyleSheet('theme', theme);
	}

	if (TCG.isTrue(TCG.getQueryParams()['maximize'])) {
		TCG.app.Window.prototype.maximized = true;
	}

	// When clicking the welcome message, open the Workflow task window
	Ext.fly('welcomeMessage').addListener('click', function() {
		TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
	});

	TCG.data.AuthHandler.initLogin('securityUserCurrent.json', user => {
		TCG.setCurrentUserIsAdmin(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Administrators'));
		TCG.setCurrentUserIsDeveloper(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Software Developers'));
		// load controls specified by createComponents URL parameter.  URL can be follow by an ID e.i. createComponents=Clifton.workflow.WorkflowWindow:workflowWindowID
		const componentStr = Ext.urlDecode(window.location.search.substring(1)).createComponents;
		if (componentStr) {
			const components = componentStr.split(',');
			for (const component of components) {
				if (component.indexOf(':') > 0) {
					const items = component.split(':');
					TCG.createComponent(items[0], {id: items[1]});
				}
				else {
					TCG.createComponent(component);
				}
			}
		}

		const loadWindowClass = TCG.getQueryParams()['loadWindowClass'];
		if (TCG.isTrue(loadWindowClass)) {
			const loadWindowParams = TCG.getQueryParams()['loadWindowParams'];
			TCG.createComponent(loadWindowClass, {params: Ext.decode(loadWindowParams)});
		}

		// Pre-Load/Cache all users (used for Created By/Updated By columns) - only gets id and label
		const loader = new TCG.data.JsonLoader(Ext.apply({
			onLoad: (records, conf) => {
				if (records && records.length > 0) {
					for (const record of records) {
						TCG.CacheUtils.put('DATA_CACHE', record, `security.user.${record.id}_Application=Internal`);
					}
				}
			}
		}));
		loader.load('securityUserList.json?requestedPropertiesRoot=data&requestedProperties=id|label');
	});
});

// Override printer path for WebJars compatibility
Ext.override(Ext.ux.Printer.BaseRenderer, {
	stylesheetPath: [
		`${TCG.ExtRootUrl}/resources/css/ext-all.css`,
		`${TCG.ExtRootUrl}/ux/css/ux-all.css`,
		`webjars/extjs-printer/print.css`,
		'core/css/application.css'
	]
});


function menuSelect(a) {
	TCG.showError('Menu item "' + a.text + '" is currently under construction', 'Under Construction');
}
