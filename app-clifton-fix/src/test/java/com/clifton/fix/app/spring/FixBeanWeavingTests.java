package com.clifton.fix.app.spring;

import com.clifton.core.context.spring.AbstractBeanWeavingTests;
import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import com.clifton.fix.app.FixApplication;
import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestPropertySource;


/**
 * The {@link FixBeanWeavingTests} attempts to validate that all types are properly woven during context initialization.
 *
 * @author manderson
 */
@SpringBootTest
//Used to fix issues that arise from the missing compile-time property replacements during CI builds
@TestPropertySource(locations = {"/com/clifton/fix/app/spring/fix-bean-weaving-tests.properties", "/crowd-test.properties", "/spring-boot-test.properties"})
@ContextHierarchy({
		@ContextConfiguration(name = "root", classes = SpringLoadTimeWeavingConfiguration.class),
		@ContextConfiguration(name = "child", locations = "FixBeanWeavingTests-context.xml"),
		@ContextConfiguration(name = "child", classes = FixApplication.class)
})
@Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
public class FixBeanWeavingTests extends AbstractBeanWeavingTests {

	public FixBeanWeavingTests(ApplicationContext applicationContext) {
		super(applicationContext);
	}
}
