package com.clifton.fix.app;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.system.validation.DataAccessExceptionConverter;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FixAppProjectBasicTests extends BasicProjectTests {

	@Resource
	private DataAccessExceptionConverter dataAccessExceptionConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectPrefix() {
		return "fix-app";
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.boot.test.context.SpringBootTest");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.amazon.sqs.");
		imports.add("com.amazonaws.");
		imports.add("javax.servlet.http.HttpServletRequest");
		imports.add("org.springframework.beans.");
		imports.add("org.springframework.boot.actuate.autoconfigure.");
		imports.add("org.springframework.boot.autoconfigure.");
		imports.add("org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration");
		imports.add("org.springframework.boot.builder.SpringApplicationBuilder");
		imports.add("org.springframework.context.annotation.");
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.http.converter.");
		imports.add("org.springframework.security.oauth2.http.converter.");
		imports.add("org.springframework.web.");
	}


	@Override
	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("dataAccessExceptionConverter", this.dataAccessExceptionConverter);
	}
}
