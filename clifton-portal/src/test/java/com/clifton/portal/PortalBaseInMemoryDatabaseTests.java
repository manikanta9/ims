package com.clifton.portal;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserRole;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.mockito.Mockito;

import javax.annotation.Resource;


/**
 * @author manderson
 */
public abstract class PortalBaseInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortalSecurityUserService portalSecurityUserService;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Override User for Migrations
	 */
	@Override
	protected IdentityObject getUser() {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setId((short) 0);
		PortalSecurityUserRole role = new PortalSecurityUserRole();
		role.setAdministrator(true);
		user.setUserRole(role);
		return user;
	}


	protected void setRunAsUser(String roleName) {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUsername("TestUser");
		user.setUserRole(this.portalSecurityUserService.getPortalSecurityUserRoleByName(roleName));
		setRunAsUser(user);
	}


	protected void setRunAsUser(PortalSecurityUser user) {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	/**
	 * @param atLeastOnce - if true then uses atLeastOnce, otherwise expected never called
	 */
	protected void verifyScheduledRunner(String runnerType, String runnerId, boolean atLeastOnce) {
		Mockito.verify(this.runnerHandler, atLeastOnce ? Mockito.atLeastOnce() : Mockito.never()).rescheduleRunner(Mockito.argThat(arg -> StringUtils.isEqualIgnoreCase(runnerType, arg.getType()) && StringUtils.isEqualIgnoreCase(runnerId, arg.getTypeId())));
	}
}
