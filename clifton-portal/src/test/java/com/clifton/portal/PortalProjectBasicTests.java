package com.clifton.portal;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureClass;
import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalProjectBasicTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "portal";
	}


	@Override
	public List<Class<?>> getAdditionalRestrictedServiceMethodAnnotationList() {
		return Collections.singletonList(PortalSecureMethod.class);
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.beans.BeanInfo");
		imports.add("java.beans.Introspector");

		imports.add("java.net.URLEncoder");

		imports.add("javax.mail.internet");

		imports.add("javax.servlet");

		imports.add("java.sql.Timestamp");

		imports.add("org.apache.http");

		imports.add("org.springframework.beans.factory.config.AutowireCapableBeanFactory");
		imports.add("org.springframework.jdbc.core.ResultSetExtractor");

		imports.add("org.springframework.security.authentication");
		imports.add("org.springframework.security.crypto");
		imports.add("org.springframework.security.core");
		imports.add("org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler");
		imports.add("org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler");

		imports.add("org.springframework.web.context.WebApplicationContext");
		imports.add("org.springframework.web.method.HandlerMethod");
		imports.add("org.springframework.web.servlet");
		imports.add("java.security");

		imports.add("javax.mail.util.ByteArrayDataSource");
		imports.add("javax.activation.DataHandler");
		imports.add("java.net.URLConnection");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> result = super.getAllowedContextManagedBeanSuffixNames();
		// Portal Password Encoder
		result.add("Encoder");
		return result;
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("entity");
		approvedList.add("feedback");
		approvedList.add("page");
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipProperties) {
		skipProperties.add("PortalFileExtended.id");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		String methodName = method.getName();

		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("savePortalTrackingEventType");
		skipMethods.add("getPortalTrackingEventExtended");
		skipMethods.add("savePortalEntity");
		skipMethods.add("savePortalFile");
		skipMethods.add("deletePortalFile");
		skipMethods.add("getPortalFileExtendedList");
		skipMethods.add("getPortalSecurityUser");
		skipMethods.add("savePortalSecurityUserAssignment");
		skipMethods.add("deletePortalSecurityUserAssignment");
		skipMethods.add("savePortalSecurityUserGroup");
		skipMethods.add("deletePortalSecurityGroup");
		skipMethods.add("deletePortalSecurityUserGroup");
		return skipMethods.contains(methodName);
	}


	/**
	 * For every method that begins with save confirms that return is NOT void
	 */
	// @Override
	@Test
	public void testSaveMethodsAreNotVoid1() throws Exception {
		Map<String, Object> controllerMap = getApplicationContext().getBeansWithAnnotation(Controller.class);
		controllerMap.putAll(getApplicationContext().getBeansWithAnnotation(Service.class));

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectPrefix())) {
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());

				// public and non-abstract methods only
				BeanInfo beanInfo = Introspector.getBeanInfo(clz);

				for (Method method : clz.getMethods()) {
					// ignore Object methods
					if (MethodUtils.isMethodInClass(Object.class, method)) {
						continue;
					}
					if (processMethod(beanInfo, method)) {
						Method interfaceMethod = MethodUtils.getInterfaceMethod(method);
						if (interfaceMethod != null) {
							if (method.getName().startsWith("save") && Void.TYPE.equals(method.getReturnType())) {
								if (!getIgnoreVoidSaveMethodSet().contains(method.getName())) {
									results.append(interfaceMethod.toString()).append(StringUtils.NEW_LINE);
								}
							}
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "Void Save Methods found for the following method(s):" + StringUtils.NEW_LINE + results.toString());
	}


	/**
	 * For every controller/service method, checks if PortalSecureMethod annotation is missing
	 * The default is Portal Admin, however to be safe, better to confirm mapping exists
	 */
	@Test
	public void testPortalSecureMethodAnnotationMissing() throws Exception {
		Map<String, Object> controllerMap = getApplicationContext().getBeansWithAnnotation(Controller.class);
		controllerMap.putAll(getApplicationContext().getBeansWithAnnotation(Service.class));

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectPrefix())) {
				// These objects already have Request Mapping Annotation that we add by default
				// So we want only what's manually entered - so instead get the class file
				// and check annotations there
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());
				if (AnnotationUtils.isAnnotationPresent(clz, SecureClass.class)) {
					continue;
				}

				// public and non-abstract methods only
				BeanInfo beanInfo = Introspector.getBeanInfo(clz);

				for (Method method : clz.getMethods()) {
					// ignore Object methods
					if (MethodUtils.isMethodInClass(Object.class, method)) {
						continue;
					}
					if (AnnotationUtils.isAnnotationPresent(method, DoNotAddRequestMapping.class)) {
						continue;
					}
					if (processMethod(beanInfo, method)) {
						if (!AnnotationUtils.isAnnotationPresent(method, PortalSecureMethod.class)) {
							results.append(method.toString()).append(StringUtils.NEW_LINE);
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "PortalSecureMethod annotation missing found for the following method(s):" + StringUtils.NEW_LINE + results.toString());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		// Not a Real Object - contains a list that is saved
		ignoredVoidSaveMethodSet.add("savePortalFeedbackResultEntry");
		// Updates tracking, not a real object
		ignoredVoidSaveMethodSet.add("savePortalNotificationAcknowledgement");
		ignoredVoidSaveMethodSet.add("savePortalNotificationAcknowledgementAdmin");
		// List Save
		ignoredVoidSaveMethodSet.add("savePortalTrackingEventList");
		return ignoredVoidSaveMethodSet;
	}
}
