package com.clifton.portal.cache.runner;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalCacheRunnerProviderTests {


	@Resource
	private PortalCacheRunnerProvider portalCacheRunnerProvider1; // One run at 4 am

	@Resource
	private PortalCacheRunnerProvider portalCacheRunnerProvider2; // One run at 4 am and one run at 4pm

	@Resource
	private PortalCacheRunnerProvider portalCacheRunnerProvider3; // No runs


	@Test
	public void testPortalCacheRunnerProvider1() {
		Date startDate = DateUtils.toDate("05/01/2017 3:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 4:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalCacheRunnerProvider1.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists("05/01/2017 4:00 AM", runners);

		startDate = DateUtils.toDate("05/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider1.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled on between 8 and 9 am");

		startDate = DateUtils.toDate("05/01/2017 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 5:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider1.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled on between 4 and 5 pm");
	}


	@Test
	public void testPortalCacheRunnerProvider2() {
		Date startDate = DateUtils.toDate("05/01/2017 3:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 4:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalCacheRunnerProvider2.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists("05/01/2017 4:00 AM", runners);

		startDate = DateUtils.toDate("05/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider2.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled on between 8 and 9 am");

		startDate = DateUtils.toDate("05/01/2017 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 5:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider2.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists("05/01/2017 4:00 PM", runners);

		startDate = DateUtils.toDate("05/01/2017 4:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider2.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists("05/01/2017 4:00 AM", runners);
		validateRunnerExists("05/01/2017 4:00 PM", runners);
	}


	@Test
	public void testPortalCacheRunnerProvider3() {
		Date startDate = DateUtils.toDate("05/01/2017 3:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 4:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalCacheRunnerProvider3.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled");

		startDate = DateUtils.toDate("05/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider3.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled");

		startDate = DateUtils.toDate("05/01/2017 4:00 PM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 5:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalCacheRunnerProvider3.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateRunnerExists(String expectedDateWithTime, List<Runner> runners) {
		String expected = "PORTAL_CACHE_LOAD-ALL-" + expectedDateWithTime;
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			String runnerString = runner.getType() + "-" + runner.getTypeId() + "-" + DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT);
			if (StringUtils.isEqual(expected, runnerString)) {
				return;
			}
		}
		Assertions.fail("Did not find expected runner: " + expected);
	}
}
