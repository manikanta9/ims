package com.clifton.portal.entity.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.PortalEntityServiceImpl;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalEntityUploadServiceImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortalEntityUploadService portalEntityUploadService;

	@Resource
	private PortalEntitySetupService portalEntitySetupService;

	@Resource
	private PortalEntityServiceImpl portalEntityService;

	@Resource
	private PortalEmailService portalEmailService;

	@Resource
	private PortalSecurityUserService portalSecurityUserService;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private RunnerHandler runnerHandler;

	private static final int ALL_ENTITIES_SHEET_INDEX = 0;
	private static final int CLIENT_RELATIONSHIPS_SUCCESS_SHEET_INDEX = 1;
	private static final int CLIENTS_SHEET_INDEX = 2;
	private static final int CLIENT_ACCOUNTS_SHEET_INDEX = 3;
	private static final int CLIENT_ACCOUNTS_SHEET_INDEX_WITH_VIEW_TYPE_POPULATED = 12;
	private static final int CLIENT_ACCOUNTS_SHEET_INDEX_WITH_TERMINATION_DATE_POPULATED = 13;
	private static final int CONTACTS_SHEET_INDEX = 4;
	private static final int TEAMS_SHEET_INDEX = 5;
	private static final int CLIENT_RELATIONSHIPS_MISSING_RM_SHEET_INDEX = 6;
	private static final int CLIENT_RELATIONSHIPS_UPDATES_SHEET_INDEX = 7;

	private static final int ROLLUP_PARENT_ENTITIES_SHEET_INDEX = 8;
	private static final int ROLLUP_SUCCESS_SHEET_INDEX = 9;
	private static final int ROLLUP_UPDATES_SHEET_INDEX = 10;
	private static final int ROLLUP_ERRORS_SHEET_INDEX = 11;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testImport_AdminOnlySourceSystem() {
		TestUtils.expectException(ValidationException.class,
				() -> {
					PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(ALL_ENTITIES_SHEET_INDEX);
					uploadCommand.setSourceSystem(getSourceSystem(false));
					this.portalEntityUploadService.uploadPortalEntityUploadFile(uploadCommand);
				},
				"Only Administrators can upload portal entities for source system IMS");
	}


	@Test
	public void testImportClientRelationships() {
		Date now = new Date();
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CLIENT_RELATIONSHIPS_SUCCESS_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 0);
		validatePortalEntityUpdateCount(2, now);
		validateSkippedStatusDetails(status, null);
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Client Relationship (Organization): Successful: 2, Deleted: 0, Skipped: 0 Failed: 0"));

		// Confirm the entity we intend to delete exists before running the update
		PortalEntity portalEntity = getPortalEntity(true, "Client Relationship (Organization)", 12, false, true);
		Assertions.assertNotNull(portalEntity, "Expected Portal Entity for Test System, Client Relationship Source FKFieldID to exist.");

		// Run the Update Upload - should have 2 updates - 1 delete, 1 insert, 1 update
		// The deleted is actually removed since nothing references it, so confirm deletion
		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(CLIENT_RELATIONSHIPS_UPDATES_SHEET_INDEX);
		uploadCommand.setDeleteEntitiesMissingFromFile(true);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validateSuccessStatusDetails(status, 1, CollectionUtils.createList("Client Relationship (Organization): Successful: 2, Deleted: 1, Skipped: 0 Failed: 0"));
		validatePortalEntityUpdateCount(2, now);

		portalEntity = getPortalEntity(true, "Client Relationship (Organization)", 12, false, false);
		Assertions.assertNull(portalEntity, "Expected Portal Entity for Test System, Client Relationship Source FKFieldID to be deleted.");

		// Run the Update Upload again - should have 0 updates because no changes
		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(CLIENT_RELATIONSHIPS_UPDATES_SHEET_INDEX);
		uploadCommand.setDeleteEntitiesMissingFromFile(true);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validateSuccessStatusDetails(status, 0, CollectionUtils.createList("Client Relationship (Organization): Successful: 0, Deleted: 0, Skipped: 2 Failed: 0"));
		validatePortalEntityUpdateCount(0, now);
	}


	@Test
	public void testImportClientRelationships_Missing_RM_Contact() {
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CLIENT_RELATIONSHIPS_MISSING_RM_SHEET_INDEX);
		uploadCommand.setSourceSystem(getSourceSystem(true));
		Status status = uploadPortalEntityUpload(uploadCommand, 1);
		validateErrorStatusDetails(status, CollectionUtils.createList("Cannot update file postings for [Relationship Manager: John Smith]. Missing Contact from the system."));
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Client Relationship (Organization): Successful: 2, Deleted: 0, Skipped: 0 Failed: 0", "Created new Portal File for Relationship Manager and entity Contact: Jeremy Smith", "Successfully inserted/updated/deleted 2 Relationship Manager postings."));
	}


	@Test
	public void testImportClientAccounts() {
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 2);
		validateErrorStatusDetails(status, CollectionUtils.createList("Cannot insert/update Client Account - Client Account - 1110 (00011.1A) because parent entity doesn't exist and is not in the upload file.", "Cannot insert/update Client Account - Client Account - 1111 (00011.2A) because parent entity doesn't exist and is not in the upload file."));
	}


	@Test
	public void testImportClientAccounts_MissingViewType() {
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX, null);
		Status status = uploadPortalEntityUpload(uploadCommand, 3);
		validateErrorStatusDetails(status, CollectionUtils.createList("Cannot insert/update Client Account - Client Account - 1110 (00011.1A) because parent entity doesn't exist and is not in the upload file.", "Cannot insert/update Client Account - Client Account - 1111 (00011.2A) because parent entity doesn't exist and is not in the upload file.", "Error saving Portal Entity: Client Account: nmemfd: New Mexico PERA: Entity View Type is required for entity type Client Account"));

		uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX_WITH_VIEW_TYPE_POPULATED, null);
		status = uploadPortalEntityUpload(uploadCommand, 2);
		validateErrorStatusDetails(status, CollectionUtils.createList("Cannot insert/update Client Account - Client Account - 1110 (00011.1A) because parent entity doesn't exist and is not in the upload file.", "Cannot insert/update Client Account - Client Account - 1111 (00011.2A) because parent entity doesn't exist and is not in the upload file."));
	}


	@Test
	public void testImportClientAccounts_ViewTypeInFile() {
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX_WITH_VIEW_TYPE_POPULATED, null);
		Status status = uploadPortalEntityUpload(uploadCommand, 2);
		validateErrorStatusDetails(status, CollectionUtils.createList("Cannot insert/update Client Account - Client Account - 1110 (00011.1A) because parent entity doesn't exist and is not in the upload file.", "Cannot insert/update Client Account - Client Account - 1111 (00011.2A) because parent entity doesn't exist and is not in the upload file."));
	}


	@Test
	public void testImportClientAccounts_TerminationDate() {
		// Import All Entities First
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(ALL_ENTITIES_SHEET_INDEX);
		uploadCommand.setSourceSystem(getSourceSystem(true));
		uploadPortalEntityUpload(uploadCommand, 0);

		Date now = new Date();

		// Then Upload the Termination Sheet
		uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX_WITH_TERMINATION_DATE_POPULATED, null);
		uploadPortalEntityUpload(uploadCommand, 0);

		List<PortalEntity> updatedEntityList = validatePortalEntityUpdateCount(3, now);
		for (PortalEntity portalEntity : updatedEntityList) {
			if ("nmemfd".equals(portalEntity.getName())) {
				Assertions.assertEquals("05/01/2019", DateUtils.fromDateShort(portalEntity.getTerminationDate()));
				// Explicitly Provided End Date
				Assertions.assertEquals("07/01/2019", DateUtils.fromDateShort(portalEntity.getEndDate()));
			}
			else {
				// Should update the client account and the client since only account for that client
				Assertions.assertEquals("05/01/2019", DateUtils.fromDateShort(portalEntity.getTerminationDate()));
				// System Generated End Date
				Assertions.assertEquals(DateUtils.fromDateShort(DateUtils.addDays(portalEntity.getTerminationDate(), this.portalEntityService.getDefaultDaysForTerminationPeriod())), DateUtils.fromDateShort(portalEntity.getEndDate()));
			}
		}

		// Re-Upload the Client File - Should NOT overwrite the end date since the account is still set
		now = new Date();

		uploadCommand = setupPortalEntityUploadCommand(CLIENTS_SHEET_INDEX, null);
		uploadPortalEntityUpload(uploadCommand, 0);

		// one client updated, but term / end date are still the same (verified below)
		validatePortalEntityUpdateCount(1, now);

		// Go through the original list and confirm term/end dates are the same as they were...
		for (PortalEntity portalEntity : updatedEntityList) {
			PortalEntity portalEntityLatest = this.portalEntityService.getPortalEntity(portalEntity.getId());
			if ("nmemfd".equals(portalEntity.getName())) {
				Assertions.assertEquals("05/01/2019", DateUtils.fromDateShort(portalEntity.getTerminationDate()));
				// Explicitly Provided End Date
				Assertions.assertEquals("07/01/2019", DateUtils.fromDateShort(portalEntity.getEndDate()));
			}
			else {
				// Should update the client account and the client since only account for that client
				Assertions.assertEquals("05/01/2019", DateUtils.fromDateShort(portalEntity.getTerminationDate()));
				// System Generated End Date
				Assertions.assertEquals(DateUtils.fromDateShort(DateUtils.addDays(portalEntity.getTerminationDate(), this.portalEntityService.getDefaultDaysForTerminationPeriod())), DateUtils.fromDateShort(portalEntity.getEndDate()));
			}
		}
	}


	@Test
	public void testImportContacts() {
		Date now = new Date();
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CONTACTS_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 0);
		validateSkippedStatusDetails(status, CollectionUtils.createList("Skipping Contact: Jeremy Smith because it is associated with another source system [IMS] with a higher priority."));
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Contact: Successful: 1, Deleted: 0, Skipped: 0 Failed: 0"));

		// SHOULD UPDATE ONLY 1 - JEREMY SMITH ALREADY EXISTS FROM IMS
		PortalEntity updatedEntity = CollectionUtils.getFirstElementStrict(validatePortalEntityUpdateCount(1, now));
		ValidationUtils.assertEquals("John Smith", updatedEntity.getLabel(), "Expected John Smith Contact to be the only entity updated.");
		List<PortalEntityFieldValue> fieldValueList = this.portalEntityService.getPortalEntityFieldValueListByEntity(updatedEntity.getId());
		verifyScheduledRunner("PORTAL_FILE_TEMPLATE_GENERATOR_FOR_SOURCE", "ENTITY_" + updatedEntity.getId(), true);
		Assertions.assertEquals("John Smith is a made up person., johnsmith@paraport.com, 123-789-8702, CFA, Made Up Person", StringUtils.collectionToCommaDelimitedString(BeanUtils.sortWithFunction(fieldValueList, fieldValue -> fieldValue.getPortalEntityFieldType().getName(), true), PortalEntityFieldValue::getValue));
	}


	@Test
	public void testImportAll() {
		Date now = new Date();
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(ALL_ENTITIES_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 0);
		// 12 Rows in the File - one is trying to update for IMS it is ignored, the other is a Contact that already exists from IMS, so it is also ignored
		validatePortalEntityUpdateCount(10, now);
		validateSkippedStatusDetails(status, CollectionUtils.createList("Skipping Contact: Jeremy Smith because it is associated with another source system [IMS] with a higher priority."));
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Client Relationship (Organization): Successful: 2, Deleted: 0, Skipped: 0 Failed: 0", "Client (Investing Entity): Successful: 3, Deleted: 0, Skipped: 0 Failed: 0", "Client Account: Successful: 3, Deleted: 0, Skipped: 0 Failed: 0", "Contact: Successful: 1, Deleted: 0, Skipped: 0 Failed: 0", "Investment Team: Successful: 1, Deleted: 0, Skipped: 0 Failed: 0", "Created new Portal File for Relationship Manager and entity Contact: Jeremy Smith", "Created new Portal File for Relationship Manager and entity Contact: John Smith", "Successfully inserted/updated/deleted 3 Relationship Manager postings.", "Created new Portal File for Meet Your Team and entity Investment Team: PPA Institutional", "Successfully inserted/updated/deleted 2 Meet Your Team postings."));

		// Validate File Postings:
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 11, CollectionUtils.createList("Jeremy Smith"));
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 12, CollectionUtils.createList("John Smith"));

		// Validate IMS File Postings were NOT affected
		validateRelationshipManagerPostings(false, "Client Relationship (Organization)", 208, CollectionUtils.createList("Jason Chalmers"));
		validateRelationshipManagerPostings(false, "Client Relationship (Organization)", 381, CollectionUtils.createList("Justin Henne", "Daniel Ryan"));

		verifyScheduledRunner("PORTAL_SECURITY_USER_ASSIGNMENT_REBUILD", "ALL", true);
	}


	@Test
	public void testImportAll_Separately() {
		// Separate Files - Order is Important because of dependencies

		// Contacts and Teams First (used for File Posting)
		Date now = new Date();
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(CONTACTS_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 0);
		validatePortalEntityUpdateCount(1, now);

		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(TEAMS_SHEET_INDEX);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validatePortalEntityUpdateCount(1, now);

		// Client Relationships
		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(CLIENT_RELATIONSHIPS_MISSING_RM_SHEET_INDEX); // OK BECAUSE MISSING RM WAS ADDED IN ABOVE CONTACTS
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Client Relationship (Organization): Successful: 2, Deleted: 0, Skipped: 0 Failed: 0", "Created new Portal File for Relationship Manager and entity Contact: John Smith", "Created new Portal File for Relationship Manager and entity Contact: Jeremy Smith", "Successfully inserted/updated/deleted 3 Relationship Manager postings."));
		// File Postings so File Category Rebuilds must be triggered (RMs so will trigger for all) security is affected as well
		verifyScheduledRunner("PORTAL_SECURITY_USER_ASSIGNMENT_REBUILD", "ALL", true);
		verifyScheduledRunner("PORTAL_FILE_CATEGORY_PORTAL_ENTITY_MAPPING_REBUILD", "ALL", true);


		// Validate File Postings:
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 11, CollectionUtils.createList("Jeremy Smith"));
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 12, CollectionUtils.createList("John Smith"));


		// Clients
		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(CLIENTS_SHEET_INDEX);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validatePortalEntityUpdateCount(3, now);

		// Client Accounts
		now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(CLIENT_ACCOUNTS_SHEET_INDEX);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		validateSuccessStatusDetails(status, null, CollectionUtils.createList("Client Account: Successful: 3, Deleted: 0, Skipped: 0 Failed: 0", "Successfully inserted/updated/deleted 5 Relationship Manager postings."));

		// Validate File Postings for Client Accounts didn't break RMs for Client Relationships
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 11, CollectionUtils.createList("Jeremy Smith"));
		validateRelationshipManagerPostings(true, "Client Relationship (Organization)", 12, CollectionUtils.createList("John Smith"));

		validateRelationshipManagerPostings(true, "Client Account", 8000, CollectionUtils.createList("Justin Henne", "Daniel Ryan"));

		validateRelationshipManagerPostings(false, "Client Relationship (Organization)", 381, CollectionUtils.createList("Justin Henne", "Daniel Ryan"));
	}


	@Test
	public void testImportAll_Rollups() {
		// ALL ENTITIES
		PortalEntityUploadCommand uploadCommand = setupPortalEntityUploadCommand(ALL_ENTITIES_SHEET_INDEX);
		Status status = uploadPortalEntityUpload(uploadCommand, 0);

		// ROLLUP PARENT ENTITIES
		Date now = new Date();
		uploadCommand = setupPortalEntityUploadCommand(ROLLUP_PARENT_ENTITIES_SHEET_INDEX);
		status = uploadPortalEntityUpload(uploadCommand, 0);
		List<PortalEntity> rollupParentEntityList = validatePortalEntityUpdateCount(3, now);


		// ROLLUPS
		PortalEntityRollupUploadCommand rollupUploadCommand = setupPortalEntityRollupUploadCommand(ROLLUP_SUCCESS_SHEET_INDEX);
		status = uploadPortalEntityRollupUpload(rollupUploadCommand, 0);
		Assertions.assertEquals("Portal Entity Rollup Upload Completed. Parent Entity Count affected 2, rollups affected 3", status.getMessage());
		PortalEntity rollup1 = validateRollupChildrenList(rollupParentEntityList, 1, CollectionUtils.createList(1110, 1111));
		PortalEntity rollup2 = validateRollupChildrenList(rollupParentEntityList, 2, CollectionUtils.createList(8000));
		PortalEntity rollup3 = validateRollupChildrenList(rollupParentEntityList, 3, null);

		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup1.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup2.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup3.getId(), false);


		rollupUploadCommand = setupPortalEntityRollupUploadCommand(ROLLUP_UPDATES_SHEET_INDEX);
		status = uploadPortalEntityRollupUpload(rollupUploadCommand, 0);
		Assertions.assertEquals("Portal Entity Rollup Upload Completed. Parent Entity Count affected 2, rollups affected 5", status.getMessage());
		validateRollupChildrenList(rollupParentEntityList, 1, CollectionUtils.createList(1110, 1111, 8000));
		validateRollupChildrenList(rollupParentEntityList, 2, CollectionUtils.createList(8000, 1110));
		validateRollupChildrenList(rollupParentEntityList, 3, null);

		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup1.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup2.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup3.getId(), false);


		rollupUploadCommand = setupPortalEntityRollupUploadCommand(ROLLUP_UPDATES_SHEET_INDEX);
		rollupUploadCommand.setDeleteChildEntitiesMissingFromFile(true);
		status = uploadPortalEntityRollupUpload(rollupUploadCommand, 0);
		Assertions.assertEquals("Portal Entity Rollup Upload Completed. Parent Entity Count affected 2, rollups affected 3", status.getMessage());
		validateRollupChildrenList(rollupParentEntityList, 1, CollectionUtils.createList(1111, 8000));
		validateRollupChildrenList(rollupParentEntityList, 2, CollectionUtils.createList(1110));
		validateRollupChildrenList(rollupParentEntityList, 3, null);

		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup1.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup2.getId(), true);
		verifyScheduledRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", "ENTITY_" + rollup3.getId(), false);


		// ERROR FILE
		rollupUploadCommand = setupPortalEntityRollupUploadCommand(ROLLUP_ERRORS_SHEET_INDEX);
		status = uploadPortalEntityRollupUpload(rollupUploadCommand, 2);
		validateErrorStatusDetails(status, CollectionUtils.createList("Error processing upload for rollup: Cannot find child entity in the system for Client Account - Client Account - 998", "Error processing upload for rollup: Cannot find parent entity in the system for Postable Entity - Commingled Vehicle - 5"));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalEntityUploadCommand setupPortalEntityUploadCommand(int sheetIndex) {
		return setupPortalEntityUploadCommand(sheetIndex, PortalEntityViewType.ENTITY_VIEW_TYPE_INSTITUTIONAL);
	}


	private PortalEntityUploadCommand setupPortalEntityUploadCommand(int sheetIndex, String entityViewTypeName) {
		setRunAsUser("PPA Administrator");
		PortalEntityUploadCommand uploadCommand = new PortalEntityUploadCommand();
		uploadCommand.setSourceSystem(getSourceSystem(true));
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/portal/entity/upload/PortalEntityUploadTest.xls"));
		uploadCommand.setSheetIndex(sheetIndex);
		if (!StringUtils.isEmpty(entityViewTypeName)) {
			uploadCommand.setEntityViewType(this.portalEntitySetupService.getPortalEntityViewTypeByName(entityViewTypeName));
		}
		return uploadCommand;
	}


	private Status uploadPortalEntityUpload(PortalEntityUploadCommand uploadCommand, int expectedErrorCount) {
		Status status = this.portalEntityUploadService.uploadPortalEntityUploadFile(uploadCommand);
		/**
		 System.out.println(status.getMessage());
		 for (StatusDetail detail : CollectionUtils.getIterable(status.getDetailList())) {
		 System.out.println(detail.getCategory() + ": " + detail.getNote());
		 }
		 **/
		Assertions.assertEquals(expectedErrorCount, status.getErrorCount());
		return status;
	}


	private PortalEntityRollupUploadCommand setupPortalEntityRollupUploadCommand(int sheetIndex) {
		setRunAsUser("PPA Administrator");
		PortalEntityRollupUploadCommand uploadCommand = new PortalEntityRollupUploadCommand();
		uploadCommand.setSourceSystem(getSourceSystem(true));
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/portal/entity/upload/PortalEntityUploadTest.xls"));
		uploadCommand.setSheetIndex(sheetIndex);
		return uploadCommand;
	}


	private Status uploadPortalEntityRollupUpload(PortalEntityRollupUploadCommand uploadCommand, int expectedErrorCount) {
		Status status = this.portalEntityUploadService.uploadPortalEntityRollupUploadFile(uploadCommand);
		/**
		 System.out.println(status.getMessage());
		 for (StatusDetail detail : CollectionUtils.getIterable(status.getDetailList())) {
		 System.out.println(detail.getCategory() + ": " + detail.getNote());
		 }
		 **/
		Assertions.assertEquals(expectedErrorCount, status.getErrorCount());
		return status;
	}


	/**
	 * @param atLeastOnce - if true then uses atLeastOnce, otherwise expected never called
	 */
	protected void verifyScheduledRunner(String runnerType, String runnerId, boolean atLeastOnce) {
		Mockito.verify(this.runnerHandler, atLeastOnce ? Mockito.atLeastOnce() : Mockito.never()).rescheduleRunner(Mockito.argThat(arg -> StringUtils.isEqualIgnoreCase(runnerType, arg.getType()) && StringUtils.isEqualIgnoreCase(runnerId, arg.getTypeId())));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalEntitySourceSystem getSourceSystem(boolean testSystem) {
		return this.portalEntitySetupService.getPortalEntitySourceSystemByName(getSourceSystemName(testSystem));
	}


	private String getSourceSystemName(boolean testSystem) {
		return (testSystem ? "Test System" : "IMS");
	}


	private PortalEntity getPortalEntity(boolean testSystem, String sourceSectionName, Integer sourceFkFieldId, boolean populateFieldValues, boolean exceptionIfMissing) {
		PortalEntitySourceSection sourceSection = this.portalEntitySetupService.getPortalEntitySourceSectionByName(getSourceSystemName(testSystem), sourceSectionName);
		Assertions.assertNotNull(sourceSection, "Cannot find source section with name " + sourceSectionName + " and system " + getSourceSystemName(testSystem));
		PortalEntity portalEntity = this.portalEntityService.getPortalEntityBySourceSection(sourceSection.getId(), sourceFkFieldId, populateFieldValues);
		if (exceptionIfMissing) {
			Assertions.assertNotNull(portalEntity, "Cannot find portal entity for source system " + getSourceSystemName(testSystem) + ", section name " + sourceSectionName + ", and source fk field id " + sourceFkFieldId);
		}
		return portalEntity;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalEntity> validatePortalEntityUpdateCount(int expectedUpdateCount, Date updateDateOnOrAfter) {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setIncludeDeleted(true);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, updateDateOnOrAfter));
		List<PortalEntity> updateList = this.portalEntityService.getPortalEntityList(searchForm);
		//System.out.println(StringUtils.collectionToCommaDelimitedString(updateList, PortalEntity::getEntityLabelWithSourceSection));
		int actualUpdateCount = CollectionUtils.getSize(updateList);
		ValidationUtils.assertEquals(expectedUpdateCount, actualUpdateCount, "Expected " + expectedUpdateCount + " portal entities to be updated, but " + actualUpdateCount + " portal entities were updated.");
		return updateList;
	}


	private PortalEntity validateRollupChildrenList(List<PortalEntity> rollupParentList, int parentFkFieldId, List<Integer> childFkFieldIds) {
		PortalEntity portalEntity = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(rollupParentList, PortalEntity::getSourceFkFieldId, parentFkFieldId));
		List<PortalEntityRollup> rollupList = this.portalEntityService.getPortalEntityRollupListByParent(portalEntity.getId());
		Assertions.assertEquals(CollectionUtils.getSize(childFkFieldIds), CollectionUtils.getSize(rollupList));
		for (PortalEntityRollup rollup : CollectionUtils.getIterable(rollupList)) {
			Assertions.assertTrue(childFkFieldIds.contains(rollup.getReferenceTwo().getSourceFkFieldId()), "Did not expect child entity " + rollup.getReferenceTwo().getEntityLabelWithSourceSection() + " under " + rollup.getReferenceOne().getEntityLabelWithSourceSection());
		}
		return portalEntity;
	}


	private void validateSuccessStatusDetails(Status status, Integer deleteCount, List<String> messages) {
		if (deleteCount != null) {
			messages.add("Successfully deleted [" + deleteCount + "] portal entities.");
		}
		messages.add("Successfully processed all user assignments for terminated portal entities.");
		validateStatusDetails(status, StatusDetail.CATEGORY_SUCCESS, messages);
	}


	private void validateSkippedStatusDetails(Status status, List<String> messages) {
		validateStatusDetails(status, StatusDetail.CATEGORY_SKIPPED, messages);
	}


	private void validateErrorStatusDetails(Status status, List<String> messages) {
		validateStatusDetails(status, StatusDetail.CATEGORY_ERROR, messages);
	}


	private void validateStatusDetails(Status status, String category, List<String> messages) {
		List<StatusDetail> detailList = status.getDetailListForCategory(category);

		for (StatusDetail statusDetail : CollectionUtils.getIterable(detailList)) {
			//System.out.println(statusDetail.getNote());
			Assertions.assertTrue(messages.contains(statusDetail.getNote()), "Did not expect message: " + statusDetail.getNote());
		}
		Assertions.assertEquals(CollectionUtils.getSize(messages), CollectionUtils.getSize(detailList));
	}


	private void validateRelationshipManagerPostings(boolean testSystem, String sourceSectionName, Integer sourceFKFieldId, List<String> relationshipManagerNames) {
		PortalEntity portalEntity = getPortalEntity(testSystem, sourceSectionName, sourceFKFieldId, false, true);
		List<PortalEntity> relationshipManagers = this.portalEmailService.getRelationshipManagerList(portalEntity.getId());
		if (CollectionUtils.isEmpty(relationshipManagerNames)) {
			AssertUtils.assertEmpty(relationshipManagers, "Did not expect any relationship managers to be set for portal entity " + portalEntity.getEntityLabelWithSourceSection());
		}
		else {
			Collections.sort(relationshipManagerNames);
			//Assertions.assertEquals(CollectionUtils.getSize(relationshipManagerNames), CollectionUtils.getSize(relationshipManagers));
			String actualRelationshipManagerNames = StringUtils.collectionToCommaDelimitedString(BeanUtils.sortWithFunction(relationshipManagers, PortalEntity::getLabel, true), PortalEntity::getLabel);
			String expectedRelationshipManagerNames = StringUtils.collectionToCommaDelimitedString(relationshipManagerNames);
			Assertions.assertEquals(expectedRelationshipManagerNames, actualRelationshipManagerNames, "Did not find expected relationship managers for portal entity " + portalEntity.getEntityLabelWithSourceSection());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void setRunAsUser(String roleName) {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUsername("TestUser");
		user.setUserRole(this.portalSecurityUserService.getPortalSecurityUserRoleByName(roleName));
		setRunAsUser(user);
	}


	private void setRunAsUser(PortalSecurityUser user) {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}
}
