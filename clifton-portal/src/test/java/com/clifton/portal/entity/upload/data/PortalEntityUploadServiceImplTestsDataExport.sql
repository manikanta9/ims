SELECT 'PortalSecurityUserRole' AS EntityTableName, PortalSecurityUserRoleID AS EntityID FROM PortalSecurityUserRole
UNION
SELECT 'PortalSecurityUser', PortalSecurityUserID FROM PortalSecurityUser WHERE SecurityUserName = 'systemuser'
UNION
SELECT 'PortalEntitySourceSection', PortalEntitySourceSectionID FROM PortalEntitySourceSection
UNION
SELECT 'PortalEntityFieldType',	PortalEntityFieldTypeID FROM PortalEntityFieldType
UNION
-- KEEP ALL CONTACTS & TEAMS
SELECT 'PortalEntity',	PortalEntityID FROM PortalEntity WHERE PortalEntitySourceSectionID IN (17, 18) AND EndDate IS NULL
UNION
SELECT 'PortalEntityFieldValue', PortalEntityFieldValueID FROM PortalEntityFieldValue v INNER JOIN PortalEntity e ON v.PortalEntityID = e.PortalEntityID WHERE e.PortalEntitySourceSectionID IN (17,18) AND EndDate IS NULL
-- KEEP A FEW IMS CLIENT ACCOUNTS TO CONFIRM UPLOADS WON'T OVERWRITE IMS DATA AND TO CONFIRM OVERLAPS
-- PERA of New Mexico Shared Client nmemfd will be skipped here because we'll add it from the test system
-- and University Of Arkansas
UNION
SELECT 'PortalEntity', PortalEntityID FROM PortalEntity
WHERE PortalEntityID IN (SELECT PortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6
							   AND SourceFKFieldID IN (499, 3389, 4935, 5272, 2107)
						 UNION
						 SELECT ParentPortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6 AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND ParentPortalEntityID IS NOT NULL
						 UNION
						 SELECT pe.ParentPortalEntityID
						 FROM PortalEntity e INNER JOIN PortalEntity pe ON e.ParentPortalEntityID = pe.PortalEntityID
						 WHERE e.PortalEntitySourceSectionID = 6 AND e.SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND pe.ParentPortalEntityID IS NOT NULL)
UNION
SELECT 'PortalEntityFieldValue', PortalEntityFieldValueID FROM PortalEntityFieldValue
WHERE PortalEntityID IN (SELECT PortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6
							   AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107)
						 UNION
						 SELECT ParentPortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6 AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND ParentPortalEntityID IS NOT NULL
						 UNION
						 SELECT pe.ParentPortalEntityID
						 FROM PortalEntity e INNER JOIN PortalEntity pe ON e.ParentPortalEntityID = pe.PortalEntityID
						 WHERE e.PortalEntitySourceSectionID = 6 AND e.SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND pe.ParentPortalEntityID IS NOT NULL)

UNION
SELECT 'PortalFileAssignment', PortalFileAssignmentID FROM PortalFileAssignment
WHERE PortalEntityID IN (SELECT PortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6
							   AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107)
						 UNION
						 SELECT ParentPortalEntityID
						 FROM PortalEntity
						 WHERE PortalEntitySourceSectionID = 6 AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND ParentPortalEntityID IS NOT NULL
						 UNION
						 SELECT pe.ParentPortalEntityID
						 FROM PortalEntity e INNER JOIN PortalEntity pe ON e.ParentPortalEntityID = pe.PortalEntityID
						 WHERE e.PortalEntitySourceSectionID = 6 AND e.SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND pe.ParentPortalEntityID IS NOT NULL)
UNION
SELECT 'PortalFile', PortalFileID FROM PortalFile
WHERE PostPortalEntityID IN (SELECT PortalEntityID
							 FROM PortalEntity
							 WHERE PortalEntitySourceSectionID = 6
								   AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107)
							 UNION
							 SELECT ParentPortalEntityID
							 FROM PortalEntity
							 WHERE PortalEntitySourceSectionID = 6 AND SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND ParentPortalEntityID IS NOT NULL
							 UNION
							 SELECT pe.ParentPortalEntityID
							 FROM PortalEntity e INNER JOIN PortalEntity pe ON e.ParentPortalEntityID = pe.PortalEntityID
							 WHERE e.PortalEntitySourceSectionID = 6 AND e.SourceFKFieldID IN (4949, 3389, 4935, 5272, 2107) AND pe.ParentPortalEntityID IS NOT NULL);


