SELECT 'PortalNotificationDefinition' AS EntityTableName, PortalNotificationDefinitionID AS EntityID
FROM PortalNotificationDefinition
WHERE DefinitionName = 'Portal Admin: Confirm Electronic Delivery (Institutional Only)'
UNION
SELECT 'PortalTrackingEventType', PortalTrackingEventTypeID
FROM PortalTrackingEventType
UNION
-- This is just a small selection for testing
SELECT 'PortalSecurityUserAssignment', PortalSecurityUserAssignmentID
FROM PortalSecurityUserAssignment
WHERE PortalEntityID IN (272, 314, 6438, 6847, 6890, 9486, 9513, 9933, 10468, 10781, 11097)
UNION
SELECT 'PortalSecurityUser', PortalSecurityUserID
FROM PortalSecurityUser
WHERE SecurityUserName = 'systemuser'
