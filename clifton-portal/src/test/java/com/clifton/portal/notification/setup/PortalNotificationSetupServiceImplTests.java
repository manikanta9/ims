package com.clifton.portal.notification.setup;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.function.Function;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalNotificationSetupServiceImplTests extends BaseInMemoryDatabaseTests {


	@Resource
	private PortalNotificationSetupService portalNotificationSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////       Portal Notification Definition Validation            ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidNotificationDefinitionSetup_InvalidRecurrence() {
		PortalNotificationDefinition definition = populatePortalNotificationDefinition("Files Posted - Invalid Recurrence", PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED);
		savePortalNotificationDefinition(definition, "Notification Recurrence is required for notification definitions that are not triggered by an event.");

		definition.setNotificationRecurrence(PortalNotificationRecurrences.MONTHS);
		savePortalNotificationDefinition(definition, "Notification Recurrence Interval is required for recurrence MONTHS");

		definition.setNotificationRecurrenceInterval((short) -1);
		savePortalNotificationDefinition(definition, "Notification Recurrence Interval must be a positive number");

		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		definition.setNotificationRecurrenceInterval((short) 1);
		savePortalNotificationDefinition(definition, "Notification Recurrence Interval is not supported for recurrence ONCE");


		definition = populatePortalNotificationDefinition("Forgot Password - Invalid Recurrence", PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_FORGOT_PASSWORD);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.MONTHS);
		savePortalNotificationDefinition(definition, "Notification Recurrence is not supported for notifications triggered by an event.");
	}


	@Test
	public void testInvalidNotificationDefinitionSetup_InvalidFileApprovedDaysBack() {
		PortalNotificationDefinition definition = populatePortalNotificationDefinition("Welcome Reminder - Invalid", PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME_REMINDER);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		definition.setFileApprovedDaysBack((short) 5);
		savePortalNotificationDefinition(definition, "File Approved Days Back is not supported for notification definition type Portal User: Welcome Reminder");

		definition = populatePortalNotificationDefinition("Files Posted - Invalid", PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		definition.setFileApprovedDaysBack((short) -5);
		savePortalNotificationDefinition(definition, "File Approved Days Back must be greater than zero.");


		definition.setFileApprovedDaysBack(null);
		savePortalNotificationDefinition(definition, "Files posted notification requires file approval days back to be set.");
	}


	@Test
	public void testInvalidManagementFeeInvoiceDefinitionSetup() {
		PortalNotificationDefinition definition = populatePortalNotificationDefinition("Invoice Paid", PortalNotificationDefinitionType.TYPE_NAME_INVOICE_PAID);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		savePortalNotificationDefinition(definition, "Invoice paid notification requires recurrence and recurrence interval to be set in order to find status change within given time period.");

		definition.setNotificationRecurrence(PortalNotificationRecurrences.DAYS_ONCE);
		savePortalNotificationDefinition(definition, "Notification Recurrence Interval is required for recurrence DAYS_ONCE");

		definition = populatePortalNotificationDefinition("Invoice Overdue", PortalNotificationDefinitionType.TYPE_NAME_INVOICE_OVERDUE);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		savePortalNotificationDefinition(definition, "Invoice overdue notification requires recurrence and recurrence interval to be set in order to find status change before given time period, i.e. over 60 days ago.");

		definition.setNotificationRecurrence(PortalNotificationRecurrences.DAYS_ONCE);
		savePortalNotificationDefinition(definition, "Notification Recurrence Interval is required for recurrence DAYS_ONCE");
	}


	@Test
	public void testInvalidNotificationDefinitionSetup_InvalidSchedule() {
		PortalNotificationDefinition definition = populatePortalNotificationDefinition("Files Posted - Invalid Schedule", PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.ONCE);
		definition.setEndTime(TimeUtils.toTime("6:00 AM"));

		savePortalNotificationDefinition(definition, "End Time is not allowed without a start time entered.");

		definition.setStartTime(TimeUtils.toTime("11:00 AM"));
		savePortalNotificationDefinition(definition, "End Time must be after Start Time");

		definition.setEndTime(null);
		definition.setRunFrequencyInMinutes(-50);
		savePortalNotificationDefinition(definition, "Run Frequency in minutes must be greater than or equal to 5.");

		definition.setRunFrequencyInMinutes(3);
		savePortalNotificationDefinition(definition, "Run Frequency in minutes must be greater than or equal to 5.");


		definition = populatePortalNotificationDefinition("Forgot Password - Invalid Recurrence", PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_FORGOT_PASSWORD);
		definition.setStartTime(TimeUtils.toTime("11:00 AM"));
		savePortalNotificationDefinition(definition, "Schedule Start time is not allowed for notifications triggered by events.");

		definition.setStartTime(null);
		definition.setEndTime(TimeUtils.toTime("11:00 AM"));
		savePortalNotificationDefinition(definition, "Schedule End time is not allowed for notifications triggered by events.");

		definition.setEndTime(null);
		definition.setRunFrequencyInMinutes(5);
		savePortalNotificationDefinition(definition, "Schedule Run Frequency in minutes is not allowed for notifications triggered by events.");
	}


	@Test
	public void testInvalidNotificationDefinitionSetup_InvalidPopUp() {
		PortalNotificationDefinition definition = populatePortalNotificationDefinition("Portal Files Pending - Invalid", PortalNotificationDefinitionType.TYPE_NAME_PORTAL_FILES_PENDING);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.NONE);
		definition.setPopUpNotification(true);
		savePortalNotificationDefinition(definition, "Pop-Up Notifications are not supported for internal notifications.");

		definition = populatePortalNotificationDefinition("Portal Files Posted - Invalid", PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED);
		definition.setNotificationRecurrence(PortalNotificationRecurrences.NONE);
		definition.setPopUpNotification(true);

		savePortalNotificationDefinition(definition, "Acknowledgement Type is required for Pop-Up Notifications");

		PortalNotificationAcknowledgementType acknowledgementType = populatePortalNotificationAcknowledgementType("Test Acknowledgement", "Confirm", null, null);
		acknowledgementType.setConfirmEntity(true);
		acknowledgementType = savePortalNotificationAcknowledgementType(acknowledgementType, null);

		definition.setAcknowledgementType(acknowledgementType);
		savePortalNotificationDefinition(definition, "Pop Ups with Confirm Entity Option on is not supported for definition type Portal File Posted");

		acknowledgementType.setConfirmEntity(false);
		savePortalNotificationAcknowledgementType(acknowledgementType, null);

		savePortalNotificationDefinition(definition, "Pop Up Notifications cannot use a NONE recurrence as users may end up with duplicate pop ups.");
	}


	////////////////////////////////////////////////////////////////////////////
	///////     Portal Notification Acknowledgement Type Validation     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidAcknowledgementType() {
		PortalNotificationAcknowledgementType acknowledgementType = populatePortalNotificationAcknowledgementType("Test", null, null, null);
		savePortalNotificationAcknowledgementType(acknowledgementType, "At least one of Confirm, Decline, or Cancel option is required.");

		acknowledgementType.setConfirmButtonLabel("Confirm");
		acknowledgementType.setCancelAcknowledgement(true);
		savePortalNotificationAcknowledgementType(acknowledgementType, "Cancel Acknowledgement can only be used if Cancel option is used.");

		acknowledgementType.setCancelButtonLabel("Cancel");
		acknowledgementType.setConfirmButtonLabel(null);
		acknowledgementType.setConfirmEntity(true);
		savePortalNotificationAcknowledgementType(acknowledgementType, "Confirm Entity can only be used if Confirm option is used.");
	}


	@Test
	public void testAcknowledgementTypeChange_Used() {
		// Validate Notification Definition is Active:
		// Note: This is the same name that is in the SQL file for the data
		PortalNotificationDefinition definition = this.portalNotificationSetupService.getPortalNotificationDefinitionByName("Portal Admin: Confirm Electronic Delivery (Institutional Only)");
		ValidationUtils.assertNotNull(definition, "Missing Portal Notification Definition with name: Portal Admin: Confirm Electronic Delivery (Institutional Only).  Confirm value in database");

		this.portalNotificationSetupService.setPortalNotificationDefinitionActive(definition.getId(), true);

		// Now Attempt to change confirm entity property on the acknowledgement type
		PortalNotificationAcknowledgementType acknowledgementType = definition.getAcknowledgementType();
		acknowledgementType.setConfirmEntity(false);

		// NOTE: I think maybe we should restrict this completely, but we don't have enough use cases, so for now an ignorable warning will be used.
		savePortalNotificationAcknowledgementType(acknowledgementType, "This acknowledgement type is used by active notification definition(s): Portal Admin: Confirm Electronic Delivery (Institutional Only).  Are you sure you want to change the options for the acknowledgement type?  Historical Notifications and Acknowledgements will NOT be affected (i.e. If cancel didn't mean acknowledgement before, but now does.  Historical cancels will still NOT be acknowledged.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortalNotificationDefinition populatePortalNotificationDefinition(String name, String typeName) {
		PortalNotificationDefinition definition = new PortalNotificationDefinition();
		definition.setDefinitionType(this.portalNotificationSetupService.getPortalNotificationDefinitionTypeByName(typeName));
		definition.setName(name);
		definition.setDescription(name);
		definition.setEmailSubjectTemplate("test subject");
		definition.setEmailBodyTemplate("test message");
		return definition;
	}


	private PortalNotificationDefinition savePortalNotificationDefinition(PortalNotificationDefinition notificationDefinition, String errorMessage) {
		return validateSave(notificationDefinition, errorMessage, bean -> this.portalNotificationSetupService.savePortalNotificationDefinition(bean));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortalNotificationAcknowledgementType populatePortalNotificationAcknowledgementType(String name, String confirmButtonLabel, String declineButtonLabel, String cancelButtonLabel) {
		PortalNotificationAcknowledgementType acknowledgementType = new PortalNotificationAcknowledgementType();
		acknowledgementType.setName(name);
		acknowledgementType.setDescription(name);
		acknowledgementType.setConfirmButtonLabel(confirmButtonLabel);
		acknowledgementType.setConfirmButtonTooltip(confirmButtonLabel);
		acknowledgementType.setDeclineButtonLabel(declineButtonLabel);
		acknowledgementType.setDeclineButtonTooltip(declineButtonLabel);
		acknowledgementType.setCancelButtonLabel(cancelButtonLabel);
		acknowledgementType.setCancelButtonTooltip(cancelButtonLabel);
		return acknowledgementType;
	}


	private PortalNotificationAcknowledgementType savePortalNotificationAcknowledgementType(PortalNotificationAcknowledgementType acknowledgementType, String errorMessage) {
		return validateSave(acknowledgementType, errorMessage, bean -> this.portalNotificationSetupService.savePortalNotificationAcknowledgementType(bean, false));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T> T validateSave(T bean, String errorMessage, Function<T, T> saveFunction) {
		boolean error = false;
		try {
			bean = saveFunction.apply(bean);
		}
		catch (ValidationException e) {
			error = true;
			if (StringUtils.isEmpty(errorMessage)) {
				Assertions.fail("Expected no error on save, but received: " + e.getMessage());
			}
			else {
				Assertions.assertEquals(errorMessage, e.getMessage());
			}
		}
		if (!error && !StringUtils.isEmpty(errorMessage)) {
			Assertions.fail("Did not get expected error message: " + errorMessage);
		}
		return bean;
	}
}
