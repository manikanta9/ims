package com.clifton.portal.notification.acknowledgement;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationSearchForm;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommandTypes;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorService;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildService;
import com.clifton.portal.tracking.PortalTrackingEventExtended;
import com.clifton.portal.tracking.PortalTrackingEventTypes;
import com.clifton.portal.tracking.PortalTrackingService;
import com.clifton.portal.tracking.search.PortalTrackingEventExtendedSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalNotificationAcknowledgementServiceImplTests extends BaseInMemoryDatabaseTests {


	@Resource
	private PortalNotificationService portalNotificationService;

	@Resource
	private PortalNotificationSetupService portalNotificationSetupService;

	@Resource
	private PortalNotificationGeneratorService portalNotificationGeneratorService;

	@Resource
	private PortalNotificationAcknowledgementService portalNotificationAcknowledgementService;

	@Resource
	private PortalSecurityUserService portalSecurityUserService;

	@Resource
	private PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService;

	@Resource
	private PortalTrackingService portalTrackingService;

	@Resource
	private ContextHandler contextHandler;


	private static final short SYSTEM_USER_ID = 1;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortalPopUpNotificationAcknowledgements() {
		setRunAsUser("Administrator");

		// Need to Build up the Expanded Table for Entity Population
		this.portalSecurityUserAssignmentExpandedRebuildService.rebuildPortalSecurityUserAssignmentExpanded();

		// Activate the Notification
		PortalNotificationDefinition definition = this.portalNotificationSetupService.getPortalNotificationDefinitionByName("Portal Admin: Confirm Electronic Delivery (Institutional Only)");
		if (!definition.isActive()) {
			this.portalNotificationSetupService.setPortalNotificationDefinitionActive(definition.getId(), true);
		}

		// Generate the Notification
		PortalNotificationGeneratorCommand portalNotificationGeneratorCommand = new PortalNotificationGeneratorCommand();
		portalNotificationGeneratorCommand.setCommandType(PortalNotificationGeneratorCommandTypes.SEND);
		portalNotificationGeneratorCommand.setPortalNotificationDefinition(definition);

		this.portalNotificationGeneratorService.generatePortalNotificationList(portalNotificationGeneratorCommand);


		List<PortalNotification> notificationList = getPortalNotificationList(definition.getId(), null, null);
		Assertions.assertEquals(26, CollectionUtils.getSize(notificationList), "Expected 26 Notifications to be generated");

		PortalSecurityUser securityUser = this.portalSecurityUserService.getPortalSecurityUser((short) 4198); // Cynthia.Thatcher@ambeacon.com

		List<PortalNotification> userNotificationList = getPortalNotificationList(definition.getId(), securityUser.getId(), null);
		Assertions.assertEquals(1, CollectionUtils.getSize(userNotificationList), "Expected only ONE Notifications for user " + securityUser.getUserName() + " to  be generated");
		PortalNotification userNotification = userNotificationList.get(0);

		// At this time, users cannot decline
		setRunAsUser(securityUser);
		savePortalNotificationAcknowledgement(userNotification.getId(), false, false, "Decline is not a supported acknowledgement");

		// Cancel - OK, but does NOT acknowledge
		savePortalNotificationAcknowledgement(userNotification.getId(), null, false, null);
		// Make sure tracking event exists
		validatePortalTrackingEventsForNotification(userNotification.getId(), null, securityUser.getId(), null, 1);
		// Confirm the Notification is NOT acknowledged
		userNotification = this.portalNotificationService.getPortalNotification(userNotification.getId());
		Assertions.assertFalse(userNotification.isAcknowledged());

		// Cancel it again
		savePortalNotificationAcknowledgement(userNotification.getId(), null, false, null);
		validatePortalTrackingEventsForNotification(userNotification.getId(), null, securityUser.getId(), null, 2);

		// Now accept it
		savePortalNotificationAcknowledgement(userNotification.getId(), true, false, null);

		// Should have 2, because 2 Portal Entities Apply and it's Confirm Entity
		validatePortalTrackingEventsForNotification(userNotification.getId(), true, securityUser.getId(), null, 2);

		// Confirm the Notification is acknowledged
		userNotification = this.portalNotificationService.getPortalNotification(userNotification.getId());
		Assertions.assertTrue(userNotification.isAcknowledged());

		// That acceptance should have also triggered a bunch of acceptances, only spot checking 1
		setRunAsUser("Administrator");

		// 4199 Patrick.Sporl@ambeacon.com
		userNotificationList = getPortalNotificationList(definition.getId(), (short) 4199, true);
		Assertions.assertEquals(1, CollectionUtils.getSize(userNotificationList), "Expected notification for another user to be automatically acknowledged");

		validatePortalTrackingEventsForNotification(userNotificationList.get(0).getId(), true, SYSTEM_USER_ID, null, 2); // 2 Entities


		// Total Acknowledged Notifications = 7
		List<PortalNotification> acknowledgedNotificationList = getPortalNotificationList(definition.getId(), null, true);
		Assertions.assertEquals(7, CollectionUtils.getSize(acknowledgedNotificationList), "Expected total acknowledged notifications to be 7");


		// Decline (as an admin) for another user
		// Need to run as a real user
		setRunAsUser(this.portalSecurityUserService.getPortalSecurityUser(SYSTEM_USER_ID));
		securityUser = this.portalSecurityUserService.getPortalSecurityUser((short) 4488); // lkrussell@wilmingtontrust.com

		userNotificationList = getPortalNotificationList(definition.getId(), securityUser.getId(), null);
		Assertions.assertEquals(1, CollectionUtils.getSize(userNotificationList), "Expected only ONE Notifications for user " + securityUser.getUserName() + " to  be generated");
		userNotification = userNotificationList.get(0);

		// At this time, users cannot decline, but admins can
		savePortalNotificationAcknowledgement(userNotification.getId(), false, true, null);
		// Should still create the tracking for the Portal Entities
		validatePortalTrackingEventsForNotification(userNotification.getId(), false, SYSTEM_USER_ID, 9933, 1);
		// Notification should be acknowledged
		userNotification = this.portalNotificationService.getPortalNotification(userNotification.getId());
		Assertions.assertTrue(userNotification.isAcknowledged());

		// Second user for this same portal entity should NOT be affected
		securityUser = this.portalSecurityUserService.getPortalSecurityUser((short) 4489); // jmcnichol@wilmingtontrust.com
		userNotificationList = getPortalNotificationList(definition.getId(), securityUser.getId(), false);
		Assertions.assertEquals(1, CollectionUtils.getSize(userNotificationList), "Expected notification for another user to remain un-acknowledged");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<PortalNotification> getPortalNotificationList(short definitionId, Short userId, Boolean acknowledged) {
		PortalNotificationSearchForm notificationSearchForm = new PortalNotificationSearchForm();
		notificationSearchForm.setNotificationDefinitionId(definitionId);
		notificationSearchForm.setRecipientUserId(userId);
		notificationSearchForm.setAcknowledged(acknowledged);
		return this.portalNotificationService.getPortalNotificationList(notificationSearchForm);
	}


	private void validatePortalTrackingEventsForNotification(int notificationId, Boolean acknowledgement, Short userId, Integer portalEntityId, int count) {
		PortalTrackingEventExtendedSearchForm searchForm = new PortalTrackingEventExtendedSearchForm();
		searchForm.setTrackingEventTypeNameEquals(PortalTrackingEventTypes.NOTIFICATION_ACKNOWLEDGEMENT.getName());
		searchForm.setSourceFkFieldId(notificationId);
		searchForm.setPortalSecurityUserId(userId);
		if (BooleanUtils.isTrue(acknowledgement)) {
			searchForm.setDescription("true");
		}
		else if (BooleanUtils.isFalse(acknowledgement)) {
			searchForm.setDescription("false");
		}
		searchForm.setPortalEntityId(portalEntityId);


		List<PortalTrackingEventExtended> eventList = this.portalTrackingService.getPortalTrackingEventExtendedList(searchForm, false);
		if (acknowledgement == null) {
			eventList = CollectionUtils.getFiltered(eventList, event -> StringUtils.isEmpty(event.getDescription()));
		}
		Assertions.assertEquals(count, CollectionUtils.getSize(eventList), "Did not find expected notification acknowledgement tracking events.");
	}


	private void savePortalNotificationAcknowledgement(int notificationId, Boolean acknowledgement, boolean admin, String errorMessage) {
		boolean error = false;
		try {
			if (admin) {
				this.portalNotificationAcknowledgementService.savePortalNotificationAcknowledgementAdmin(notificationId, acknowledgement);
			}
			else {
				this.portalNotificationAcknowledgementService.savePortalNotificationAcknowledgement(notificationId, acknowledgement);
			}
		}
		catch (ValidationException e) {
			error = true;
			if (StringUtils.isEmpty(errorMessage)) {
				Assertions.fail("Expected no error on save, but received: " + e.getMessage());
			}
			else {
				Assertions.assertEquals(errorMessage, e.getMessage());
			}
		}
		if (!error && !StringUtils.isEmpty(errorMessage)) {
			Assertions.fail("Did not get expected error message: " + errorMessage);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setRunAsUser(String roleName) {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUsername("TestUser");
		user.setUserRole(this.portalSecurityUserService.getPortalSecurityUserRoleByName(roleName));
		setRunAsUser(user);
	}


	private void setRunAsUser(PortalSecurityUser user) {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}
}
