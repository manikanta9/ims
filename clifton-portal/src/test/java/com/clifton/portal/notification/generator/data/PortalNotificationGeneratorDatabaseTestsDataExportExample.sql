/** NOTE THIS IS COMMENTED OUT AND THE FILE IS RENAMED BECAUSE IT SHOULDN'T BE REGENERATED AUTOMATICALLY DATA WAS SPECIFICALLY PULLED AND MANIPULATED FOR THIS TEST AND GENERATED IN THE DATA.XML FILE **/
/**
SELECT 'PortalSecurityUserRole' AS EntityTableName, PortalSecurityUserRoleID AS EntityID FROM PortalSecurityUserRole
UNION
SELECT 'PortalNotificationDefinition', PortalNotificationDefinitionID FROM PortalNotificationDefinition
UNION
SELECT 'PortalFile', PortalFileID FROM PortalFile WHERE PortalFileID IN (42893,353248,574041,574043,574428,573836,573549,572843,554710,555071,554970,561519,561518,565351,566836,4,5,13)
UNION
SELECT 'PortalSecurityUserAssignment', PortalSecurityUserAssignmentID FROM PortalSecurityUserAssignment a INNER JOIN PortalSecurityUser u ON a.PortalSecurityUserID = u.PortalSecurityUserID WHERE u.UserEmailAddress = 'testPortalAdmin_WCRA@paraport.com'
UNION
SELECT 'PortalSecurityUser', PortalSecurityUserID FROM PortalSecurityUser WHERE SecurityUserName = 'SystemUser'
UNION
SELECT 'PortalEntity', PortalEntityID FROM PortalEntity WHERE PortalEntityID in (2964,3527)
UNION
SELECT 'PortalEntityFieldValue', PortalEntityFieldValueID FROM PortalEntityFieldValue WHERE PortalEntityID in (2964,3527);
**/


/**  GLOBAL FILE SQL TEST

  SELECT 'PortalNotificationDefinition' AS EntityTableName, PortalNotificationDefinitionID AS EntityID
FROM PortalNotificationDefinition
WHERE DefinitionName = 'Client Notice'
UNION
SELECT 'PortalFile', PortalFileID
FROM PortalFile
WHERE PortalFileID = 1224651 -- Client Notice file
union
   SELECT 'PortalSecurityUserAssignmentResource',PortalSecurityUserAssignmentResourceID FROM PortalSecurityUserAssignmentResource
 WHERE PortalSecurityUserAssignmentID IN (779,1523,1529,1533,1537,1541)

 */


