package com.clifton.portal.notification.cache;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationPortalFile;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalNotificationLastCacheTests {

	@Resource
	private PortalNotificationLastCacheImpl<?> portalNotificationLastCache;

	@Resource
	private PortalNotificationService portalNotificationService;

	@Resource
	private AdvancedUpdatableDAO<PortalNotification, Criteria> portalNotificationDAO;

	@Resource
	private UpdatableDAO<PortalNotificationPortalFile> portalNotificationPortalFileDAO;

	@Resource
	private PortalNotificationSetupService portalNotificationSetupService;

	@Resource
	private ReadOnlyDAO<PortalSecurityUser> portalSecurityUserDAO;

	@Resource
	private PortalFileService portalFileService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final short DEFINITION_TYPE_FILE_POSTED = 1;
	private static final short DEFINITION_TYPE_WELCOME_REMINDER = 2;

	private static final short DEFINITION_FILE_POSTED_DAILY = 1;
	private static final short DEFINITION_FILE_POSTED_QUARTER_HOURLY = 2;
	private static final short DEFINITION_WELCOME_REMINDER = 3;

	private static final short TEST_USER_1 = 1;
	private static final short TEST_USER_2 = 2;
	private static final short TEST_USER_3 = 3;

	private static final int TEST_FILE_1 = 1;
	private static final int TEST_FILE_2 = 2;
	private static final int TEST_FILE_3 = 3;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void beforeEach() {
		// Need to reset otherwise the verify will continue to add on for each test within this class
		Mockito.reset(this.portalNotificationLastCache);
	}

	////////////////////////////////////////////////////////////////////////////
	////////             TEST FOR NOTIFICATIONS - NO FILES              ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCacheLookup_NoResults() {
		// Look it up - should do database look up in the cache
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_1, null);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");
		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_1, null);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");

		// Total: One Database Lookup
		verifyCacheLookup_DatabaseRetrieval(1);
	}


	@Test
	public void testCacheLookup_Results() {
		// Look it up - should do database look up in the cache
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_2, null);
		// Context has a record of a welcome email reminder set to user 2
		Assertions.assertNotNull(notification, "Expected a notification to be returned");
		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_2, null);
		Assertions.assertNotNull(notification, "Expected a notification to be returned");

		// Total: One Database Lookup
		verifyCacheLookup_DatabaseRetrieval(1);
	}


	@Test
	public void testCacheLookup_NoResults_InsertNew_Results() {
		// Look it up - should do database look up in the cache
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_3, null);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");

		// Now it's in the cache so we want to clear it on insert
		PortalNotification newNotification = new PortalNotification();
		newNotification.setRecipientUser(this.portalSecurityUserDAO.findByPrimaryKey(TEST_USER_3));
		newNotification.setNotificationDefinition(this.portalNotificationSetupService.getPortalNotificationDefinition(DEFINITION_WELCOME_REMINDER));
		newNotification.setSentDate(new Date());
		newNotification = this.portalNotificationDAO.save(newNotification);

		// Look it up again - database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_3, null);
		Assertions.assertEquals(newNotification, notification);

		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_WELCOME_REMINDER, TEST_USER_3, null);
		Assertions.assertEquals(newNotification, notification);

		// Total: Two Database Lookups
		verifyCacheLookup_DatabaseRetrieval(2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////               TEST FOR NOTIFICATIONS - FILES               ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCacheLookup_WithFile_NoResults() {
		// Look it up - should do database look up in the cache
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_2, TEST_FILE_1);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");
		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_2, TEST_FILE_1);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");

		// Total: One Database Lookup
		verifyCacheLookup_DatabaseRetrieval(1);
	}


	@Test
	public void testCacheLookup_WithFile_Results() {
		// Look it up - should do database look up in the cache
		// Context has a record of a file posted email sent to user 1 for test files 1 and 2
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_1, TEST_FILE_2);
		Assertions.assertNotNull(notification, "Expected a notification to be returned");
		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_1, TEST_FILE_2);
		Assertions.assertNotNull(notification, "Expected a notification to be returned");

		// Total: One Database Lookup
		verifyCacheLookup_DatabaseRetrieval(1);
	}


	@Test
	public void testCacheLookup_WithFile_NoResults_InsertNew_Results() {
		// Look it up - should do database look up in the cache
		PortalNotification notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_3, TEST_FILE_3);
		Assertions.assertNull(notification, "Did not expect a notification to be returned");

		// Now it's in the cache so we want to clear it on insert
		PortalNotification newNotification = new PortalNotification();
		newNotification.setRecipientUser(this.portalSecurityUserDAO.findByPrimaryKey(TEST_USER_3));
		newNotification.setNotificationDefinition(this.portalNotificationSetupService.getPortalNotificationDefinition(DEFINITION_FILE_POSTED_QUARTER_HOURLY));
		newNotification.setSentDate(new Date());
		newNotification = this.portalNotificationDAO.save(newNotification);

		PortalNotificationPortalFile notificationPortalFile1 = new PortalNotificationPortalFile();
		notificationPortalFile1.setReferenceOne(newNotification);
		notificationPortalFile1.setReferenceTwo(this.portalFileService.getPortalFile(TEST_FILE_1));
		this.portalNotificationPortalFileDAO.save(notificationPortalFile1);

		PortalNotificationPortalFile notificationPortalFile3 = new PortalNotificationPortalFile();
		notificationPortalFile3.setReferenceOne(newNotification);
		notificationPortalFile3.setReferenceTwo(this.portalFileService.getPortalFile(TEST_FILE_3));
		this.portalNotificationPortalFileDAO.save(notificationPortalFile3);

		// Look it up again - database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_3, TEST_FILE_3);
		Assertions.assertEquals(newNotification, notification);

		// Look it up again - no database lookup
		notification = this.portalNotificationLastCache.getPortalNotificationLastForUser(this.portalNotificationService, DEFINITION_TYPE_FILE_POSTED, TEST_USER_3, TEST_FILE_3);
		Assertions.assertEquals(newNotification, notification);

		// Total: Two Database Lookups
		verifyCacheLookup_DatabaseRetrieval(2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void verifyCacheLookup_DatabaseRetrieval(int numberOfTimes) {
		Mockito.verify(this.portalNotificationLastCache, Mockito.times(numberOfTimes)).lookupPortalNotificationLast(ArgumentMatchers.any(PortalNotificationService.class), ArgumentMatchers.anyShort(), ArgumentMatchers.anyShort(), ArgumentMatchers.nullable(Integer.class));
	}
}
