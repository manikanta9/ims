package com.clifton.portal.notification.generator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMappingService;
import com.clifton.portal.file.setup.security.PortalFileCategorySecurityResourceExpandedRebuildService;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionSearchForm;
import com.clifton.portal.notification.subscription.PortalNotificationSubscription;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionEntry;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalNotificationGeneratorDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortalNotificationSetupService portalNotificationSetupService;

	@Resource
	private PortalNotificationGeneratorService portalNotificationGeneratorService;

	@Resource
	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;

	@Resource
	private PortalSecurityUserService portalSecurityUserService;

	@Resource
	private PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService;

	@Resource
	private PortalFileCategorySecurityResourceExpandedRebuildService portalFileCategorySecurityResourceExpandedRebuildService;

	@Resource
	private PortalFileService portalFileService;

	@Resource
	private PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService;


	@Resource
	private ContextHandler contextHandler;


	private static final String TEST_USER_NAME = "testPortalAdmin_WCRA@paraport.com";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortalFilesPendingNotificationGenerator() {
		setRunAsUser("PPA Administrator");

		// By default, the notification doesn't have a filter on the date
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_FILES_PENDING, "Portal Files Pending"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));

		// File Should be addressed to Team Implementation
		PortalNotification notification = notificationList.get(0);
		// NOTE: NEED TO ADD FILE ASSIGNMENT IN ORDER FOR THIS TO WORK, UNTIL THAT IS FIXED EMAIL WILL GO TO INSTITUTIONAL CENTER
		// Assertions.assertEquals("TeamImplementation@paraport.com", notification.getRecipientEmailAddress());
		Assertions.assertEquals("InstitutionalCenter@paraport.com", notification.getRecipientEmailAddress());
	}


	@Test
	public void testPortalFilesPosted_QuarterHourlyNotificationGenerator_NoSubscriptions() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, false);

		// Should Return No Results as well because files were approved over a day ago
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED, "Portal Files Posted (Quarter-Hourly)"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));

		// Changing Run Date to 9/11/2017 at 4pm
		command.setRunDate(DateUtils.toDate("09/01/2017 4:00 pm", DateUtils.DATE_FORMAT_SHORT));
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));
	}


	@Test
	public void testPortalFilesPosted_QuarterHourlyNotificationGenerator() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, true);

		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		searchForm.setApproved(true);

		PortalFileSearchForm portalFileSearchForm = new PortalFileSearchForm();
		portalFileSearchForm.setApproved(true);

		// Should Return No Results as well because files were approved over a day ago
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED, "Portal Files Posted (Quarter-Hourly)"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));

		// Changing Run Date to 9/11/2017 at 4pm - should return 1 file because 1 DTR was approved that day (the others don't use this notification definition)
		command.setRunDate(DateUtils.toDate("09/11/2017 4:00 pm", DateUtils.DATE_FORMAT_SHORT));
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));

		// Send Them
		command.setCommandType(PortalNotificationGeneratorCommandTypes.SEND);
		this.portalNotificationGeneratorService.generatePortalNotificationList(command);

		// Preview Again - No Results Now
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));
	}


	@Test
	public void testPortalFilesPosted_DailyNotificationGenerator() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, true);

		// Should Return No Results as well because files were approved over a day ago
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED, "Portal Files Posted (Quarter-Hourly)"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));

		// Changing Run Date to 8/22/2017 at 8pm - should return 2 files because 2 ADV forms - the other is an invoice and get's it's own definition
		command.setRunDate(DateUtils.toDate("08/22/2017 8:00 pm"));
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
	}


	@Test
	public void testPortalFilesPosted_InvoiceNotificationGenerator() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, true);

		// Should Return No Results as well because files were approved over a day ago
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_INVOICE_POSTED, "Management Fee Invoice Posted"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));

		// Changing Run Date to 8/25/2017 at 8pm - should return 1 invoice
		command.setRunDate(DateUtils.toDate("08/25/2017 8:00 pm"));
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
	}


	@Test
	public void testPortalFilesPosted_InvoicePaidGenerator() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, true);

		// Should Return No Results as well because files were approved over a day ago
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_INVOICE_PAID, "Management Fee Invoice Paid"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList));

		// Changing Run Date to 8/25/2017 at 8pm - should return 1 invoice - invoice was paid on 8/24
		command.setRunDate(DateUtils.toDate("08/25/2017 8:00 pm"));
		notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
	}


	@Test
	public void testPortalFilesPosted_GlobalFile() {
		setRunAsUser("PPA Administrator");
		switchNotificationEnabled(TEST_USER_NAME, true);

		// Should Return All Active Users as Results - REQUIRED subscription and a global file
		PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
		command.setRunDate(DateUtils.toDate("08/25/2021 5:00 am"));
		command.setCommandType(PortalNotificationGeneratorCommandTypes.PREVIEW);
		command.setPortalNotificationDefinition(getPortalNotificationDefinition(PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED, "Client Notice"));

		List<PortalNotification> notificationList = this.portalNotificationGeneratorService.previewPortalNotificationList(command);

		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalNotificationDefinition getPortalNotificationDefinition(String definitionTypeName, String definitionName) {
		PortalNotificationDefinition definition = CollectionUtils.getFirstElementStrict(BeanUtils.filter(this.portalNotificationSetupService.getPortalNotificationDefinitionList(new PortalNotificationDefinitionSearchForm()), PortalNotificationDefinition::getName, definitionName));
		if (!definition.isActive()) {
			definition.setActive(true);
			this.portalNotificationSetupService.setPortalNotificationDefinitionActive(definition.getId(), true);
		}
		return definition;
	}


	private void switchNotificationEnabled(String userName, boolean enabled) {
		processPortalRebuilds();
		PortalSecurityUser user = this.portalSecurityUserService.getPortalSecurityUserByUserName(userName, false);
		List<PortalSecurityUserAssignment> assignmentList = this.portalSecurityUserService.getPortalSecurityUserAssignmentListForUser(user.getId());

		for (PortalSecurityUserAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			PortalNotificationSubscriptionEntry entry = this.portalNotificationSubscriptionService.getPortalNotificationSubscriptionEntry(assignment.getSecurityUser().getId(), assignment.getPortalEntity().getId());
			entry.setSecurityUser(user);
			entry.setPortalEntity(assignment.getPortalEntity());
			for (PortalNotificationSubscription subscription : entry.getSubscriptionList()) {
				subscription.setNotificationEnabled(enabled);
			}
			this.portalNotificationSubscriptionService.savePortalNotificationSubscriptionEntry(entry);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void setRunAsUser(String roleName) {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUsername("TestUser");
		user.setUserRole(this.portalSecurityUserService.getPortalSecurityUserRoleByName(roleName));
		setRunAsUser(user);
	}


	private void setRunAsUser(PortalSecurityUser user) {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	private void processPortalRebuilds() {
		// Admin - Portal Files - File Categories: Rebuild Entity Mapping
		this.portalFileCategoryPortalEntityMappingService.rebuildPortalFileCategoryPortalEntityMappingList();
		// Admin - Portal Files - File Categories: Rebuild Security Data
		this.portalFileCategorySecurityResourceExpandedRebuildService.rebuildPortalFileCategorySecurityResourceExpanded();
		// Admin - Security: Rebuild User Data
		this.portalSecurityUserAssignmentExpandedRebuildService.rebuildPortalSecurityUserAssignmentExpanded();
		// Admin - Notification Setup - Subscriptions: Rebuild User Subscriptions
		this.portalNotificationSubscriptionService.rebuildPortalNotificationSubscriptionList();
	}
}
