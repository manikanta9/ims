SELECT 'PortalNotificationDefinitionType' AS EntityTableName, PortalNotificationDefinitionTypeID AS EntityID
FROM PortalNotificationDefinitionType
UNION
SELECT 'PortalNotificationDefinition' AS EntityTableName, PortalNotificationDefinitionID AS EntityID
FROM PortalNotificationDefinition
WHERE DefinitionName = 'Portal Admin: Confirm Electronic Delivery (Institutional Only)'
