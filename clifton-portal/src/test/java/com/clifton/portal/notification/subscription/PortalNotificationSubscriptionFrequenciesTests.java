package com.clifton.portal.notification.subscription;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.holiday.PortalHoliday;
import com.clifton.portal.holiday.PortalHolidayService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalNotificationSubscriptionFrequenciesTests {

	@Resource
	private PortalHolidayService portalHolidayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFirstBusinessDayOfWeek() {
		PortalNotificationSubscriptionFrequencies frequency = PortalNotificationSubscriptionFrequencies.WEEKLY_FIRST_BUSINESS_DAY;

		validateFrequencyValidForDate(frequency, "01/01/2017", false); // Sunday
		validateFrequencyValidForDate(frequency, "01/02/2017", false); // Holiday
		validateFrequencyValidForDate(frequency, "01/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/01/2017", false); // Saturday
		validateFrequencyValidForDate(frequency, "07/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/10/2017", true);
		validateFrequencyValidForDate(frequency, "09/01/2017", false); // Friday
		validateFrequencyValidForDate(frequency, "09/04/2017", false); // Holiday

		// NO HOLIDAYS DEFINED FOR 2018
		validateFrequencyValidForDate(frequency, "01/01/2018", true);
		// Add Holiday
		PortalHoliday holiday = addHolidayForDate("01/01/2018");
		validateFrequencyValidForDate(frequency, "01/01/2018", false);
		// Remove Holiday
		this.portalHolidayService.deletePortalHolidayList(CollectionUtils.createList(holiday));
		validateFrequencyValidForDate(frequency, "01/01/2018", true);
	}


	@Test
	public void testFirstBusinessDayOfMonth() {
		PortalNotificationSubscriptionFrequencies frequency = PortalNotificationSubscriptionFrequencies.MONTHLY_FIRST_BUSINESS_DAY;

		validateFrequencyValidForDate(frequency, "01/01/2017", false); // Sunday
		validateFrequencyValidForDate(frequency, "01/02/2017", false); // Holiday
		validateFrequencyValidForDate(frequency, "01/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/01/2017", false); // Saturday
		validateFrequencyValidForDate(frequency, "07/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/10/2017", false); // After Start of Month
		validateFrequencyValidForDate(frequency, "09/01/2017", true);
		validateFrequencyValidForDate(frequency, "09/04/2017", false); // After Start of Month

		validateFrequencyValidForDate(frequency, "01/01/2018", true); // NO HOLIDAYS DEFINED FOR 2018
	}


	@Test
	public void testFirstBusinessDayOfQuarter() {
		PortalNotificationSubscriptionFrequencies frequency = PortalNotificationSubscriptionFrequencies.QUARTERLY_FIRST_BUSINESS_DAY;

		validateFrequencyValidForDate(frequency, "01/01/2017", false); // Sunday
		validateFrequencyValidForDate(frequency, "01/02/2017", false); // Holiday
		validateFrequencyValidForDate(frequency, "01/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/01/2017", false); // Saturday
		validateFrequencyValidForDate(frequency, "07/03/2017", true);
		validateFrequencyValidForDate(frequency, "07/10/2017", false); // After Start of Quarter
		validateFrequencyValidForDate(frequency, "08/01/2017", false); // After Start of Quarter
		validateFrequencyValidForDate(frequency, "09/01/2017", false); // After Start of Quarter
		validateFrequencyValidForDate(frequency, "09/04/2017", false); // After Start of Quarter
		validateFrequencyValidForDate(frequency, "10/01/2017", false); // Sunday
		validateFrequencyValidForDate(frequency, "10/02/2017", true);

		validateFrequencyValidForDate(frequency, "01/01/2018", true); // NO HOLIDAYS DEFINED FOR 2018
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalHoliday addHolidayForDate(String dateString) {
		PortalHoliday holiday = new PortalHoliday();
		holiday.setHolidayDate(DateUtils.toDate(dateString));
		return this.portalHolidayService.savePortalHolidayList(CollectionUtils.createList(holiday)).get(0);
	}


	private void validateFrequencyValidForDate(PortalNotificationSubscriptionFrequencies frequency, String dateString, boolean expected) {
		Date date = DateUtils.toDate(dateString);
		boolean result = frequency.isFrequencyValidForDate(date, this.portalHolidayService);
		ValidationUtils.assertEquals(expected, result, "Expected [" + dateString + "] to return " + Boolean.toString(expected) + " for frequency [" + frequency.getLabel() + "], but it returned " + Boolean.toString(result));
	}
}
