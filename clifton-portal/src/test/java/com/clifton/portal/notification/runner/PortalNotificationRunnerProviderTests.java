package com.clifton.portal.notification.runner;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalNotificationRunnerProviderTests {

	@Resource
	private PortalNotificationRunnerProvider portalNotificationRunnerProvider;

	private static final short NOTIFICATION_DEFINITION_FILE_POSTED_DAILY = 1; // Runs once at 8pm
	private static final short NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY = 2; // Runs every 15 minutes between 5 am and 8 pm
	private static final short NOTIFICATION_DEFINITION_WELCOME_REMINDER = 3; // Runs once at 7 am


	@Test
	public void testPortalNotificationRunnerProvider_OneActiveWithMultipleResults() {
		Date startDate = DateUtils.toDate("05/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:00 AM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:15 AM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:30 AM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:45 AM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 9:00 AM", runners);
		Assertions.assertEquals(5, runners.size());
	}


	@Test
	public void testPortalNotificationRunnerProvider_MultipleActiveWithMultipleResults() {
		Date startDate = DateUtils.toDate("05/01/2017 8:00 PM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 9:00 PM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:00 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_DAILY, "05/01/2017 8:00 PM", runners);
		Assertions.assertEquals(2, runners.size());

		startDate = DateUtils.toDate("05/01/2017 7:00 PM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("05/01/2017 9:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 7:00 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 7:15 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 7:30 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 7:45 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_QUARTER_HOURLY, "05/01/2017 8:00 PM", runners);
		validateRunnerExists(NOTIFICATION_DEFINITION_FILE_POSTED_DAILY, "05/01/2017 8:00 PM", runners);
		Assertions.assertEquals(6, runners.size());
	}


	@Test
	public void testPortalNotificationRunnerProvider_MultipleActiveWithMultipleResults_SkipOnWeekends() {
		Date startDate = DateUtils.toDate("04/30/2017 8:00 PM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("04/30/2017 9:00 PM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled on Sunday");

		startDate = DateUtils.toDate("04/30/2017 7:00 PM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("04/30/2017 9:00 PM", DateUtils.DATE_FORMAT_SHORT);
		runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled on Sunday");
	}


	@Test
	public void testPortalNotificationRunnerProvider_NoneActiveBeforeStartTime() {
		Date startDate = DateUtils.toDate("05/01/2017 2:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 4:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled between 2 am and 4 am");
	}


	@Test
	public void testPortalNotificationRunnerProvider_NoneActiveAfterEndTime() {
		Date startDate = DateUtils.toDate("05/01/2017 10:00 PM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 11:00 PM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertTrue(CollectionUtils.isEmpty(runners), "Did not expect any runners to be scheduled between 10 PM and 11 PM");
	}


	@Test
	public void testPortalNotificationRunnerProvider_WelcomeReminder() {
		Date startDate = DateUtils.toDate("05/01/2017 7:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("05/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.portalNotificationRunnerProvider.getOccurrencesBetween(startDate, endDate);
		validateRunnerExists(NOTIFICATION_DEFINITION_WELCOME_REMINDER, "05/01/2017 7:00 AM", runners);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateRunnerExists(short definitionId, String expectedDateWithTime, List<Runner> runners) {
		String expected = "PORTAL_NOTIFICATION_DEFINITION-" + definitionId + "-" + expectedDateWithTime;
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			String runnerString = runner.getType() + "-" + runner.getTypeId() + "-" + DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT);
			if (StringUtils.isEqual(expected, runnerString)) {
				return;
			}
		}
		Assertions.fail("Did not find expected runner: " + expected);
	}
}
