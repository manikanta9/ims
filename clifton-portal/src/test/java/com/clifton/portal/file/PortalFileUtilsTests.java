package com.clifton.portal.file;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;


/**
 * @author manderson
 */
public class PortalFileUtilsTests {


	@Test
	public void testGeneratePortalFileNameAndRelativePath() {
		MultipartFile testFile = new MultipartFileImpl("src/test/java/com/clifton/portal/file/Test Document.doc");

		PortalFileCategory category = getPortalFileCategory("Documentation", "Legal Documentation", "Investment Guidelines");
		PortalFile portalFile = new PortalFile();
		portalFile.setCreateDate(new Date());
		portalFile.setFileCategory(category);
		Assertions.assertEquals("Documentation" + File.separator + "Legal Documentation" + File.separator + "Investment Guidelines" + File.separator, category.getFolderNameExpanded());

		// Manual:
		validateExpectedFileNameAndRelativePath(portalFile, testFile, FileUtils.combinePath("Manual Files", category.getFolderNameExpanded()), "Test Document", "doc");
		portalFile.setDisplayName("My Test File");
		validateExpectedFileNameAndRelativePath(portalFile, testFile, FileUtils.combinePath("Manual Files", category.getFolderNameExpanded()), "Test Document", "doc");

		// Global
		portalFile.setGlobalFile(true);
		validateExpectedFileNameAndRelativePath(portalFile, testFile, FileUtils.combinePath("Global Files", category.getFolderNameExpanded()), "Test Document", "doc");

		// Client Relationship
		portalFile.setGlobalFile(false);
		PortalEntity clientRelationshipTestEntity = getPortalEntity("Test Relationship 1");
		portalFile.setPostPortalEntity(clientRelationshipTestEntity);
		validateExpectedFileNameAndRelativePath(portalFile, testFile, FileUtils.combinePath("Test Relationship 1", category.getFolderNameExpanded()), "Test Document", "doc");

		PortalEntity clientTestEntity = getPortalEntity("Test Client A");
		clientTestEntity.setParentPortalEntity(clientRelationshipTestEntity);
		portalFile.setPostPortalEntity(clientTestEntity);
		validateExpectedFileNameAndRelativePath(portalFile, testFile, FileUtils.combinePaths("Test Relationship 1", "Test Client A", category.getFolderNameExpanded()), "Test Document", "doc");
	}


	@Test
	public void testGetReportDateFromFileName() {
		// Specific date
		Assertions.assertEquals(DateUtils.toDate("09/15/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016.09.15.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/22/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016 09 22.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("09/27/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016-09-27.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("09/22/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.09.22.2016.pdf", null));

		Assertions.assertEquals(DateUtils.toDate("09/15/2016"), PortalFileUtils.getReportDateFromFileName("2016.09.15.040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/22/2016"), PortalFileUtils.getReportDateFromFileName("2016 09 22 040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("09/27/2016"), PortalFileUtils.getReportDateFromFileName("2016-09-27-040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("09/22/2016"), PortalFileUtils.getReportDateFromFileName("09.22.2016.040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));

		// Specific Month
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016.09.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016 09.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.2016-09.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.09-2016.pdf", new StringBuilder()));

		Assertions.assertEquals(DateUtils.toDate("09/30/2017"), PortalFileUtils.getReportDateFromFileName("Sep17 - Advance Publications, Inc. Master Retirement Trust.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2017"), PortalFileUtils.getReportDateFromFileName("Sep17 - The Robert D. Kern and Patricia E. Kern 1992 Revocable Trust.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2017"), PortalFileUtils.getReportDateFromFileName("September 17 - Advance Publications, Inc. Master Retirement Trust.pdf", null));

		Assertions.assertEquals(DateUtils.toDate("10/31/2017"), PortalFileUtils.getReportDateFromFileName("Advance Publications, Inc. Master Retirement Trust - Oct 2017.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("10/31/2017"), PortalFileUtils.getReportDateFromFileName("Advance Publications, Inc. Master Retirement Trust - October 2017.pdf", null));

		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("2016.09.040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("2016 09 040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("2016-09-040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("09-2016-040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));


		// Specific Quarter
		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.Q3 2016.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("12/31/2016"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.4Q 2016.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("03/31/2017"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.1Q 2017.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("06/30/2017"), PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.Q2 2017.pdf", null));

		Assertions.assertEquals(DateUtils.toDate("09/30/2016"), PortalFileUtils.getReportDateFromFileName("Q3 2016 040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("12/31/2016"), PortalFileUtils.getReportDateFromFileName("4Q.2016.040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
		Assertions.assertEquals(DateUtils.toDate("03/31/2017"), PortalFileUtils.getReportDateFromFileName("2017-Q1 040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", new StringBuilder()));
		Assertions.assertEquals(DateUtils.toDate("06/30/2017"), PortalFileUtils.getReportDateFromFileName("2017 Q2 040001.1998 Frank Batten, Jr. Trust-ETFs.pdf", null));
	}


	@Test
	public void testGetPortalEntitySearchPatternFromFileName() {
		StringBuilder dateRegex = new StringBuilder(25);
		String categoryName = null;
		PortalFileUtils.getReportDateFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.Q3 2016.pdf", dateRegex);
		Assertions.assertEquals("040001", PortalFileUtils.getPortalEntitySearchPatternFromFileName("040001.1998 Frank Batten, Jr. Trust-ETFs.Q3 2016.pdf", dateRegex.toString(), categoryName));

		Assertions.assertEquals("050950", PortalFileUtils.getPortalEntitySearchPatternFromFileName("050950 ABC-NABET Retirement Trust Fund-Target Allocation.2016.09.pdf", null, categoryName));

		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("1998 Frank Batten, Jr. Trust-ETFs.Q3 2016.pdf", dateRegex);
		Assertions.assertEquals("1998", PortalFileUtils.getPortalEntitySearchPatternFromFileName("1998 Frank Batten, Jr. Trust-ETFs.Q3 2016.pdf", dateRegex.toString(), categoryName));

		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("Battelle Account In-2011-12-22_08_47_11.700 - Copy.pdf", dateRegex);
		Assertions.assertEquals("Battelle", PortalFileUtils.getPortalEntitySearchPatternFromFileName("Battelle Account In-2011-12-22_08_47_11.700 - Copy.pdf", dateRegex.toString(), categoryName));

		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("2016.09.Employee Retirement Income Plan of MMM Co.pdf", dateRegex);
		Assertions.assertEquals("Employee", PortalFileUtils.getPortalEntitySearchPatternFromFileName("2016.09.Employee Retirement Income Plan of MMM Co.pdf", dateRegex.toString(), categoryName));

		Assertions.assertEquals("Pepperdine", PortalFileUtils.getPortalEntitySearchPatternFromFileName("Pepperdine University-Treasuries Performance.pdf", null, categoryName));

		categoryName = "Reconciliation of Market Value";
		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("Reconciliation of Market Value.2017.06.nmemfd.pdf", dateRegex);
		Assertions.assertEquals("nmemfd", PortalFileUtils.getPortalEntitySearchPatternFromFileName("Reconciliation of Market Value.2017.06.nmemfd.pdf", dateRegex.toString(), categoryName));

		categoryName = "Private Funds Statement";
		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("Sep17 - ACT, Inc..pdf", dateRegex);
		Assertions.assertEquals("ACT", PortalFileUtils.getPortalEntitySearchPatternFromFileName("Sep17 - ACT, Inc..pdf", dateRegex.toString(), categoryName));

		categoryName = "K-1 Form";
		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("800020-19_NECA - IBEW PENSION TRUST FUND.pdf", dateRegex);
		Assertions.assertEquals("800020-19", PortalFileUtils.getPortalEntitySearchPatternFromFileName("800020-19_NECA - IBEW PENSION TRUST FUND.pdf", dateRegex.toString(), categoryName));

		categoryName = "Performance Reports";
		dateRegex = new StringBuilder(25);
		PortalFileUtils.getReportDateFromFileName("Sep17 - The Robert D. Kern and Patricia E. Kern 1992 Revocable Trust.pdf", dateRegex);
		Assertions.assertEquals("Robert", PortalFileUtils.getPortalEntitySearchPatternFromFileName("Sep17 - The Robert D. Kern and Patricia E. Kern 1992 Revocable Trust.pdf", dateRegex.toString(), categoryName));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateExpectedFileNameAndRelativePath(PortalFile portalFile, MultipartFile multipartFile, String expectedRelativePath, String expectedFileNameWithoutExtension, String fileExtension) {
		// yyyy-MM-dd_HH_mm_ss.SSS
		String expectedFileNameRegex = expectedFileNameWithoutExtension + "_" + DateUtils.fromDate(new Date(), "yyyy-MM-dd_") + "[\\d\\d]*_[\\d\\d]*_[\\d\\d]*.[\\d\\d\\d]*." + fileExtension + "$";
		String relativePath = PortalFileUtils.generatePortalFileRelativePath(portalFile);
		Assertions.assertEquals(expectedRelativePath, relativePath);
		String fileName = PortalFileUtils.generatePortalFileName(portalFile, multipartFile.getOriginalFilename(), false);
		Assertions.assertTrue(fileName.matches("^" + expectedFileNameRegex));
		Assertions.assertTrue(PortalFileUtils.generatePortalFileName(portalFile, multipartFile.getOriginalFilename(), true).matches("^" + expectedRelativePath.replace("\\", "\\\\") + expectedFileNameRegex));
	}


	private PortalEntity getPortalEntity(String entityName) {
		PortalEntity portalEntity = new PortalEntity();
		portalEntity.setName(entityName);
		portalEntity.setFolderName(entityName);
		return portalEntity;
	}


	private PortalFileCategory getPortalFileCategory(String categoryName, String categorySubsetName, String fileGroupName) {
		PortalFileCategory category = new PortalFileCategory();
		category.setFileCategoryType(PortalFileCategoryTypes.CATEGORY);
		category.setName(categoryName);
		category.setFolderName(categoryName);

		PortalFileCategory categorySubset = new PortalFileCategory();
		categorySubset.setFileCategoryType(PortalFileCategoryTypes.CATEGORY_SUBSET);
		categorySubset.setName(categorySubsetName);
		categorySubset.setFolderName(categorySubsetName);
		categorySubset.setParent(category);

		PortalFileCategory fileGroup = new PortalFileCategory();
		fileGroup.setFileCategoryType(PortalFileCategoryTypes.FILE_GROUP);
		fileGroup.setName(fileGroupName);
		fileGroup.setFolderName(fileGroupName);
		fileGroup.setParent(categorySubset);

		return fileGroup;
	}
}
