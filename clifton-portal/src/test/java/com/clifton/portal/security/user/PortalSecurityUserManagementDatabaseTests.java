package com.clifton.portal.security.user;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.PortalBaseInMemoryDatabaseTests;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Tests external user management
 *
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalSecurityUserManagementDatabaseTests extends PortalBaseInMemoryDatabaseTests {

	@Resource
	private PortalEntityService portalEntityService;

	@Resource
	private ReadOnlyDAO<PortalEntity> portalEntityDAO;

	@Resource
	private PortalFileSetupService portalFileSetupService;

	@Resource
	private PortalFileService portalFileService;

	@Resource
	private PortalSecurityResourceService portalSecurityResourceService;

	@Resource
	private PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler;

	@Resource
	private PortalSecurityUserManagementService portalSecurityUserManagementService;

	@Resource
	private PortalSecurityUserServiceImpl portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateUserAndAssignment() {
		setRunAsUser("PPA Administrator");

		PortalSecurityUserRole clientAdminRole = this.portalSecurityUserService.getPortalSecurityUserRoleByName("Portal Administrator");
		PortalSecurityUserRole clientUserRole = this.portalSecurityUserService.getPortalSecurityUserRoleByName("Client User");

		PortalEntity entity = getPortalEntity("BusinessClientRelationship", "AkzoNobel, Inc.");

		// Create a User
		PortalSecurityUser adminUser = createUser("admin@email.com", true);
		Assertions.assertEquals(clientUserRole, adminUser.getUserRole());

		// Add an Assignment
		PortalSecurityUserAssignment adminAssignment = createAssignment(adminUser, entity, null, false);
		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, "Assignment Role is required");

		// Make this user a Portal Administrator
		adminAssignment.setAssignmentRole(clientAdminRole);
		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, null); // No error this time

		// No Files Posted Yet So no Resources Available
		Assertions.assertTrue(CollectionUtils.isEmpty(adminAssignment.getUserAssignmentResourceList()), "Resource List should be empty, because there are no files posted yet.");

		// Check Expanded Table - Should have 4 entries with no resources - one for the organization group, one for the client relationship, one for the client, and one for the client account
		List<PortalSecurityUserAssignmentExpanded> expandedList = this.portalSecurityUserAssignmentExpandedHandler.getPortalSecurityUserAssignmentExpandedListForUser(adminUser.getId());
		Assertions.assertEquals(4, CollectionUtils.getSize(expandedList));

		for (PortalSecurityUserAssignmentExpanded expanded : CollectionUtils.getIterable(expandedList)) {
			Assertions.assertNull(expanded.getPortalSecurityResourceId());
		}

		// Post an Approved File - Switch to Admin to Prevent Permission Error
		setRunAsUser("Administrator");
		PortalFile portalFile = createPortalFile(entity, "Investment Guidelines", true, true);
		// Switch Back
		setRunAsUser("PPA Administrator");

		// Create another Test User, This one a regular Client User
		PortalSecurityUser user = createUser("test@email.com", true);
		Assertions.assertEquals(clientUserRole, user.getUserRole());
		// Add an Assignment
		PortalSecurityUserAssignment assignment = createAssignment(user, entity, clientUserRole, true);
		// One File Posted - Should have in the list, but be off because not a portal admin
		Assertions.assertEquals(1, CollectionUtils.getSize(assignment.getUserAssignmentResourceList()));
		// Guidelines are security resource defaulted ON
		Assertions.assertTrue(assignment.getUserAssignmentResourceList().get(0).isAccessAllowed());

		// Turn defaulting permission off for Investment Guidelines
		PortalSecurityResource resource = this.portalSecurityResourceService.getPortalSecurityResourceByName("Documentation");
		resource.setDoNotDefaultPortalEntitySpecificAccess(true);
		this.portalSecurityResourceService.savePortalSecurityResource(resource);

		// Create another Test User, This one a regular Client User
		PortalSecurityUser user2 = createUser("test2@email.com", true);
		Assertions.assertEquals(clientUserRole, user.getUserRole());
		// Add an Assignment
		PortalSecurityUserAssignment assignment2 = createAssignment(user2, entity, clientUserRole, true);
		// One File Posted - Should have in the list, but be off because not a portal admin and defaulted to off
		Assertions.assertEquals(1, CollectionUtils.getSize(assignment.getUserAssignmentResourceList()));
		// Guidelines are security resource defaulted OFF
		Assertions.assertFalse(assignment2.getUserAssignmentResourceList().get(0).isAccessAllowed());

		// Check Expanded Table - Should have 4 entries with 1 resource - one for the organization group, one for the client relationship, one for the client, and one for the client account
		expandedList = this.portalSecurityUserAssignmentExpandedHandler.getPortalSecurityUserAssignmentExpandedListForUser(user2.getId());
		Assertions.assertEquals(4, CollectionUtils.getSize(expandedList));

		for (PortalSecurityUserAssignmentExpanded expanded : CollectionUtils.getIterable(expandedList)) {
			Assertions.assertNull(expanded.getPortalSecurityResourceId());
		}

		// Pull the assignment again, this time the new security resource should be populated and set by default
		adminAssignment = this.portalSecurityUserService.getPortalSecurityUserAssignment(adminAssignment.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(adminAssignment.getUserAssignmentResourceList()));
		Assertions.assertTrue(adminAssignment.getUserAssignmentResourceList().get(0).isAccessAllowed());

		// Save the assignment to trigger the expanded table rebuild
		// NOTE HOW DO WE TRIGGER EXPANDED TABLE BY DEFAULT WHEN NEW FILE IS POSTED...DO WE NEED THIS?  DO WE JUST REBUILD ALL ONCE A DAY?
		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, null);

		// Pull the Admin User Expanded - They should now have access because they have access by default
		expandedList = this.portalSecurityUserAssignmentExpandedHandler.getPortalSecurityUserAssignmentExpandedListForUser(adminUser.getId());
		Assertions.assertEquals(4, CollectionUtils.getSize(expandedList));


		for (PortalSecurityUserAssignmentExpanded expanded : CollectionUtils.getIterable(expandedList)) {
			// At the top level - Organization Group level - the user does not have resource permission
			if (expanded.getPortalEntityId().equals(entity.getRootParent().getId())) {
				Assertions.assertNull(expanded.getPortalSecurityResourceId());
			}
			else {
				// All other levels they do
				Assertions.assertEquals(portalFile.getFileCategory().getSecurityResource().getId(), expanded.getPortalSecurityResourceId());
			}
		}

		// Change the admin to a client user and remove security
		adminAssignment.setAssignmentRole(clientUserRole);
		adminAssignment.getUserAssignmentResourceList().get(0).setAccessAllowed(false);

		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, null);

		// Switch them back to a portal admin
		adminAssignment.setAssignmentRole(clientAdminRole);
		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, null);

		// The admin should have their access back
		Assertions.assertTrue(adminAssignment.getUserAssignmentResourceList().get(0).isAccessAllowed());

		// Now run as the Portal Administrator
		setRunAsUser(adminUser);

		// As the admin user, try to change myself to a Client User
		adminAssignment.setAssignmentRole(clientUserRole);
		// These tests appear not to really support "originalBean" so the error message is different than expected, but it does catch this as an error
		adminAssignment = savePortalSecurityUserAssignment(adminAssignment, "You cannot remove security administrative functions for yourself to Organization: AkzoNobel, Inc.");
		adminAssignment.setAssignmentRole(clientAdminRole);

		// As a Portal Admin - Create a new Client user
		PortalSecurityUser user3 = createUser("test3@email.com", true);
		// Add an Assignment
		PortalSecurityUserAssignment assignment3 = createAssignment(user3, entity, clientUserRole, true);
		// Pull expanded list - should have 4
		expandedList = this.portalSecurityUserAssignmentExpandedHandler.getPortalSecurityUserAssignmentExpandedListForUser(user3.getId());
		Assertions.assertEquals(4, CollectionUtils.getSize(expandedList));
		// Delete the assignment
		this.portalSecurityUserService.deletePortalSecurityUserAssignment(assignment3.getId());
		// Expanded List should now be empty
		expandedList = this.portalSecurityUserAssignmentExpandedHandler.getPortalSecurityUserAssignmentExpandedListForUser(user3.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(expandedList));

		// As a Portal Admin - Create a new Client User, but try to assign them to a Portal Entity outside your control
		PortalSecurityUser user4 = createUser("test4@email.com", true);
		// Add an Assignment
		// Running as Portal Admin - Can't see the Aon Relationship....
		PortalEntity entity2 = getPortalEntity("BusinessClientRelationship", "Aon Corporation - Hewitt Risk Management Services Limited");
		Assertions.assertNull(entity2, "Should not have returned a portal entity, because there are no assignments that will allow that entity to be visible to current user.");
		// Get it using DAO
		entity2 = getPortalEntityUsingDao("BusinessClientRelationship", "Aon Hewitt Investment Company");
		PortalSecurityUserAssignment assignment4 = createAssignment(user4, entity2, clientUserRole, false);
		assignment4 = savePortalSecurityUserAssignment(assignment4, "You do not have access to security administrative functions for portal entity " + BeanUtils.getBeanIdentity(entity2));

		// Now make that the client under the client relationship
		assignment4.setPortalEntity(getPortalEntity("BusinessClient", "AkzoNobel, Inc. North American Pension Committee"));
		assignment4 = savePortalSecurityUserAssignment(assignment4, null);

		Assertions.assertFalse(assignment4.isDisabled());

		// Disable the assignment
		assignment4.setDisabledDate(new Date());
		assignment4.setDisabledReason("Left Organization");

		assignment4 = savePortalSecurityUserAssignment(assignment4, null);
		Assertions.assertTrue(assignment4.isDisabled());

		// Update Custodian Role to Limit to Investment Reporting Only
		setRunAsUser("PPA Administrator");
		PortalSecurityUserRole custodianRole = this.portalSecurityUserService.getPortalSecurityUserRoleByName("Custodian");
		custodianRole = this.portalSecurityUserService.getPortalSecurityUserRoleExpanded(custodianRole.getId());
		for (PortalSecurityUserRoleResourceMapping resourceMapping : CollectionUtils.getIterable(custodianRole.getRoleResourceMappingList())) {
			if (resourceMapping.getSecurityResource().getName().equalsIgnoreCase("Investment Reporting") || (resourceMapping.getSecurityResource().getParent() != null && resourceMapping.getSecurityResource().getParent().getName().equalsIgnoreCase("Investment Reporting"))) {
				resourceMapping.setAccessAllowed(true);
			}
		}
		custodianRole.setApplyRoleResourceMapping(true);
		this.portalSecurityUserService.savePortalSecurityUserRoleExpanded(custodianRole);

		// Create User 4 as Custodian Role
		// As a Portal Admin - Create a new Client user
		PortalSecurityUser user5 = createUser("test5@email.com", true);
		// Add an Assignment
		PortalSecurityUserAssignment assignment5 = createAssignment(user5, entity, custodianRole, true);

		// There should be no resource available for the custodian since nothing is posted to Investment Reporting
		AssertUtils.assertEmpty(assignment5.getUserAssignmentResourceList(), "Custodian Assignment resource list should be empty");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalSecurityUser createUser(String userName, boolean save) {
		PortalSecurityUser user = new PortalSecurityUser();
		user.setUsername(userName);
		user.setFirstName("Test");
		user.setLastName("Test");
		user.setCompanyName("ABC Company");
		user.setPhoneNumber("203-123-1234");
		user.setTitle("Test User");
		// Don't set a role, the user role should populated by default to Client User
		if (save) {
			this.portalSecurityUserManagementService.savePortalSecurityUser(user);
		}
		return user;
	}


	private PortalSecurityUserAssignment createAssignment(PortalSecurityUser user, PortalEntity entity, PortalSecurityUserRole assignmentRole, boolean save) {
		PortalSecurityUserAssignment assignment = new PortalSecurityUserAssignment();
		assignment.setSecurityUser(user);
		assignment.setPortalEntity(entity);
		assignment.setAssignmentRole(assignmentRole);
		if (save) {
			assignment = savePortalSecurityUserAssignment(assignment, null);
		}
		return assignment;
	}


	private PortalFile createPortalFile(PortalEntity postEntity, String fileGroupName, boolean approved, boolean save) {
		PortalFile portalFile = new PortalFile();
		portalFile.setApproved(approved);
		portalFile.setPostPortalEntity(postEntity);
		portalFile.setDisplayName("Test " + fileGroupName);
		portalFile.setReportDate(DateUtils.toDate("05/12/2017"));
		portalFile.setFileCategory(this.portalFileSetupService.getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, fileGroupName));
		if (save) {
			portalFile = this.portalFileService.savePortalFile(portalFile);
		}
		return portalFile;
	}


	private PortalEntity getPortalEntity(String tableName, String name) {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setSecurableEntity(true);
		searchForm.setSourceSystemTableNameEquals(tableName);
		searchForm.setNameEquals(name);
		List<PortalEntity> result = this.portalEntityService.getPortalEntityList(searchForm);
		return CollectionUtils.getFirstElement(result);
	}


	// The search form will limit based on users permissions, so need this if we want to try to get around that...
	private PortalEntity getPortalEntityUsingDao(String tableName, String name) {
		List<PortalEntity> result = this.portalEntityDAO.findAll();
		for (PortalEntity entity : CollectionUtils.getIterable(result)) {
			if (entity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
				if (entity.getName().equalsIgnoreCase(name)) {
					if (entity.getEntitySourceSection().getSourceSystemTableName().equalsIgnoreCase(tableName)) {
						return entity;
					}
				}
			}
		}
		return null;
	}


	private PortalSecurityUserAssignment savePortalSecurityUserAssignment(PortalSecurityUserAssignment assignment, String errorMessage) {
		boolean fail = false;
		try {
			assignment = this.portalSecurityUserService.savePortalSecurityUserAssignment(assignment);
		}
		catch (Exception e) {
			fail = true;
			e.printStackTrace();
			Assertions.assertFalse(StringUtils.isEmpty(errorMessage), "Did not expect exception on save of assignment, but received: [" + ExceptionUtils.getOriginalMessage(e) + "]");
			Assertions.assertEquals(errorMessage, ExceptionUtils.getOriginalMessage(e));
		}
		if (!fail) {
			Assertions.assertTrue(StringUtils.isEmpty(errorMessage), "Expected exception on save of assignment, but didn't get one.");
		}
		return assignment;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserServiceImpl getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}
}
