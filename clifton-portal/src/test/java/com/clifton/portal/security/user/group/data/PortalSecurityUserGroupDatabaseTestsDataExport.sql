-- LOAD ALL INTERNAL USERS SO CAN CREATE GROUPS, USER GROUPS AND GROUP RESOURCES
SELECT 'PortalSecurityUser' "EntityTableName", u.PortalSecurityUserID "EntityID"
FROM PortalSecurityUser u
	 INNER JOIN PortalSecurityUserRole r ON u.PortalSecurityUserRoleID = r.PortalSecurityUserRoleID
WHERE r.IsPortalEntitySpecific = 0

UNION ALL

SELECT 'PortalSecurityResource', PortalSecurityResourceID
FROM PortalSecurityResource;
