package com.clifton.portal.security.user.group;

import com.clifton.portal.PortalBaseInMemoryDatabaseTests;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;


/**
 * Tests Internal User Management
 *
 * @author manderson
 */
@ContextConfiguration(locations = "classpath:com/clifton/portal/PortalDatabaseTests-context.xml")
public class PortalSecurityUserGroupDatabaseTests extends PortalBaseInMemoryDatabaseTests {

	@Resource
	private PortalSecurityUserGroupService portalSecurityUserGroupService;

	@Resource
	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGroupPermissions() {
		setRunAsUser("PPA Administrator");

		// Create a new Accounting Group
		PortalSecurityGroup accountingGroup = new PortalSecurityGroup();
		accountingGroup.setName("Accounting Test Group");
		accountingGroup.setDescription("Accounting Test Group");
		accountingGroup = this.portalSecurityUserGroupService.savePortalSecurityGroup(accountingGroup, false);

		// The group now has the resource list populated
		Set<String> accountingResources = new HashSet<>();
		accountingResources.add("Accounting Reporting");
		accountingResources.add("Accounting Reconciliation Data");
		accountingResources.add("Investment Statement");

		for (PortalSecurityGroupResource resource : accountingGroup.getResourceList()) {
			resource.setAccessAllowed(accountingResources.contains(resource.getSecurityResource().getName()));
		}
		accountingGroup = this.portalSecurityUserGroupService.savePortalSecurityGroup(accountingGroup, true);
		verifyScheduledRunner("PORTAL_SECURITY_GROUP_REBUILD", "GROUP_" + accountingGroup.getId(), true);


		// Create a new Performance Group
		PortalSecurityGroup performanceGroup = new PortalSecurityGroup();
		performanceGroup.setName("Performance Test Group");
		performanceGroup.setDescription("Performance Test Group");
		performanceGroup = this.portalSecurityUserGroupService.savePortalSecurityGroup(performanceGroup, false);


		Set<String> performanceResources = new HashSet<>();
		performanceResources.add("Performance Detail");
		performanceResources.add("Performance Reports");

		for (PortalSecurityGroupResource resource : performanceGroup.getResourceList()) {
			resource.setAccessAllowed(performanceResources.contains(resource.getSecurityResource().getName()));
		}
		performanceGroup = this.portalSecurityUserGroupService.savePortalSecurityGroup(performanceGroup, true);
		verifyScheduledRunner("PORTAL_SECURITY_GROUP_REBUILD", "GROUP_" + performanceGroup.getId(), true);


		// Validate Jane has no access to anything
		PortalSecurityUser jane = this.portalSecurityUserService.getPortalSecurityUserByUserName("jhenning", false);
		Assertions.assertNotNull(jane, "Missing Jane as a User (username: jhenning)");

		// Pull by ID so resource list is populated
		jane = this.portalSecurityUserService.getPortalSecurityUser(jane.getId());

		for (PortalSecurityUserAssignmentResource userResource : jane.getUserAssignmentResourceList()) {
			Assertions.assertFalse(userResource.isAccessAllowed());
		}

		// Add Jane to Accounting Test Group
		PortalSecurityUserGroup accountingUserGroup = new PortalSecurityUserGroup();
		accountingUserGroup.setSecurityGroup(accountingGroup);
		accountingUserGroup.setSecurityUser(jane);
		this.portalSecurityUserGroupService.savePortalSecurityUserGroup(accountingUserGroup);

		// Pull Jane again - permissions should be updated
		jane = this.portalSecurityUserService.getPortalSecurityUser(jane.getId());

		for (PortalSecurityUserAssignmentResource userResource : jane.getUserAssignmentResourceList()) {
			if (accountingResources.contains(userResource.getSecurityResource().getName())) {
				Assertions.assertTrue(userResource.isAccessAllowed());
			}
			else {
				Assertions.assertFalse(userResource.isAccessAllowed());
			}
		}

		// Add Jane to Performance Group
		PortalSecurityUserGroup performanceUserGroup = new PortalSecurityUserGroup();
		performanceUserGroup.setSecurityGroup(performanceGroup);
		performanceUserGroup.setSecurityUser(jane);
		this.portalSecurityUserGroupService.savePortalSecurityUserGroup(performanceUserGroup);

		// Now Jane should have access to Both Accounting and Performance Resources
		jane = this.portalSecurityUserService.getPortalSecurityUser(jane.getId());

		for (PortalSecurityUserAssignmentResource userResource : jane.getUserAssignmentResourceList()) {
			if (accountingResources.contains(userResource.getSecurityResource().getName()) || performanceResources.contains(userResource.getSecurityResource().getName())) {
				Assertions.assertTrue(userResource.isAccessAllowed());
			}
			else {
				Assertions.assertFalse(userResource.isAccessAllowed());
			}
		}

		// Remove Jane from Accounting Group
		this.portalSecurityUserGroupService.deletePortalSecurityUserGroup(accountingUserGroup.getId());

		// Now Jane should only have access to Performance Resources
		jane = this.portalSecurityUserService.getPortalSecurityUser(jane.getId());

		for (PortalSecurityUserAssignmentResource userResource : jane.getUserAssignmentResourceList()) {
			if (performanceResources.contains(userResource.getSecurityResource().getName())) {
				Assertions.assertTrue(userResource.isAccessAllowed());
			}
			else {
				Assertions.assertFalse(userResource.isAccessAllowed());
			}
		}
	}
}
