package com.clifton.portal.security.user;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalSecurityUserServiceImplTests {

	@Resource
	private PortalSecurityUserServiceImpl portalSecurityUserService;

	@Resource
	private PortalSecurityUserManagementServiceImpl portalSecurityUserManagementService;


	@Test
	public void testPortalSecurityUserName() {
		validatePortalSecurityUserNameValid("test", "Supplied User Name [test] is not a valid e-mail address.");
		validatePortalSecurityUserNameValid("test@company", "Supplied User Name [test@company] is not a valid e-mail address.");
		validatePortalSecurityUserNameValid("test@company.com", null);

		validatePortalSecurityUserNameValid("test@gmail.com", null);
		validatePortalSecurityUserNameValid("test@yahoo.com", null);
		validatePortalSecurityUserNameValid("test@outlook.com", null);
	}


	@Test
	public void testPortalSecurityUserPassword() {
		validatePortalSecurityUserPassword("test@company.com", null, "test", "Password cannot contain your user name.");
		validatePortalSecurityUserPassword("test@company.com", null, "test@company.com", "Password cannot contain your user name.");
		validatePortalSecurityUserPassword("test@company.com", null, "moc.ynapmoc@tset", "Password matches 2 of 4 character rules, but 3 are required. Missing: [Password must contain at least 1 digit characters, and/or Password must contain at least 1 uppercase characters].", "Password contains the user id 'test@company.com' in reverse.");
		validatePortalSecurityUserPassword("test@company.com", null, "moC.ynapmoc@tset1", "Password contains the user id 'test@company.com' in reverse.");
		validatePortalSecurityUserPassword("test@company.com", null, "test123", "Password contains the illegal QWERTY sequence: 123.", "Password contains the illegal username sequence: test.", "Password contains the illegal numerical sequence: 123.", "Password matches 2 of 4 character rules, but 3 are required. Missing: [Password must contain at least 1 special characters, and/or Password must contain at least 1 uppercase characters].", "Password must be at least 8 characters in length.");
		validatePortalSecurityUserPassword("test@company.com", null, "aBcDeF123456", "Password contains the illegal QWERTY sequences: 123, 234, 345, 456.", "Password contains the illegal numerical sequences: 123, 234, 345, 456.", "Password contains the illegal alphabetical sequences: aBc, BcD, cDe, DeF.");
		validatePortalSecurityUserPassword("test@company.com", null, "company@185", "Password contains the illegal username sequences: comp, ompa, mpan, pany.");

		validatePortalSecurityUserPassword("test@company.com", null, "password1!", "Password contains the illegal word: password.");
		validatePortalSecurityUserPassword("test@company.com", null, "Passwordtest1!", "Password contains the illegal word: Password.");

		// Cannot Repeat the same character more than 3 times
		validatePortalSecurityUserPassword("test@company.com", null, "Testing1111!", "Password cannot contain the same character more than 3 times in succession.  Found the following violation: 1111.");
		validatePortalSecurityUserPassword("test@company.com", null, "Testing111!");
		validatePortalSecurityUserPassword("test@company.com", null, "Teeeesting135!", "Password cannot contain the same character more than 3 times in succession.  Found the following violation: eeee.");
		validatePortalSecurityUserPassword("test@company.com", null, "Teeesting126!");
		validatePortalSecurityUserPassword("test@company.com", null, "Testing124!!!!", "Password cannot contain the same character more than 3 times in succession.  Found the following violation: !!!!.");

		// History Check
		String encodedPassword = validatePortalSecurityUserPassword("test@company.com", null, "MySecretPa$$word5");

		validatePortalSecurityUserPassword("test@company.com", encodedPassword, "MySecretPa$$word5", "Password cannot be the same as the previous password.");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validatePortalSecurityUserNameValid(String userName, String errorMessage) {
		boolean error = false;
		try {
			this.portalSecurityUserService.validatePortalSecurityUserName(userName);
		}
		catch (ValidationException e) {
			error = true;
			if (StringUtils.isEmpty(errorMessage)) {
				Assertions.fail("Expected user name [" + userName + "] to be valid, but it wasn't: " + e.getMessage());
			}
			else {
				Assertions.assertEquals(errorMessage, e.getMessage());
			}
		}
		if (!error && !StringUtils.isEmpty(errorMessage)) {
			Assertions.fail("Did not get expected error message: " + errorMessage);
		}
	}


	protected String validatePortalSecurityUserPassword(String username, String oldEncodedPassword, String newPassword, String... errorMessages) {
		boolean error = false;
		try {
			return this.portalSecurityUserManagementService.validateAndEncodePassword(username, oldEncodedPassword, newPassword);
		}
		catch (ValidationException e) {
			error = true;
			if (errorMessages.length == 0) {
				Assertions.fail("Expected password [" + newPassword + "] to be valid, but it wasn't: " + e.getMessage());
			}
			else {
				for (String errorMessage : errorMessages) {
					Assertions.assertTrue(e.getMessage().contains(errorMessage), "Missing expected error message: " + errorMessage + ". Actual error message: " + e.getMessage());
				}
			}
		}
		finally {
			if (!error && errorMessages.length > 0) {
				Assertions.fail("Did not get expected error message(s): " + ArrayUtils.toString(errorMessages));
			}
		}
		return null;
	}
}
