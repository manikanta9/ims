-- LOAD ALL VIEW TYPES: Must be added via sql, not actions because you can't insert new view types via service/dao saves
SELECT 'PortalEntityViewType' "EntityTableName", PortalEntityViewTypeID "EntityID"
FROM PortalEntityViewType
UNION
SELECT 'PortalNotificationDefinitionType', PortalNotificationDefinitionTypeID
FROM PortalNotificationDefinitionType
WHERE DefinitionTypeName = 'Portal File Posted';
