package com.clifton.portal.tracking.audit;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.PortalTestObjectFactory;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalTrackingAuditUtilsTests {

	/**
	 * Used to test/confirm Do Not Audit fields
	 */
	@Test
	public void testPortalTrackingAuditPortalSecurityUser() {
		PortalSecurityUser user = PortalTestObjectFactory.newPortalSecurityUser_External();
		user.setPassword("TestPassword");
		user.setPasswordUpdateDate(new Date());

		// Insert
		validateAuditTrailDescription("John Smith", DaoEventTypes.INSERT, user, null);

		PortalSecurityUser updateUser = BeanUtils.cloneBean(user, false, true);
		updateUser.setLastName("Doe");
		updateUser.setPassword("Test New Password");
		updateUser.setPasswordUpdateDate(new Date());

		// Update
		validateAuditTrailDescription("John Doe lastName: [Smith] to [Doe]", DaoEventTypes.UPDATE, updateUser, user);

		// Delete
		validateAuditTrailDescription("John Doe lastName: [Doe] companyName: [Test Company] title: [Test User] emailAddress: [johnsmith@email.com] firstName: [John] userRole: [{id=null,label=Client User}] username: [johnsmith@email.com]", DaoEventTypes.DELETE, updateUser, null);
	}


	/**
	 * Used to test/confirm Do Not Audit FROM value fields
	 */
	@Test
	public void testPortalTrackingAuditPortalFile() {
		PortalFile portalFile = PortalTestObjectFactory.newPortalFile();

		// Note: We Don't audit inserts for Portal Files

		PortalFile updateFile = BeanUtils.cloneBean(portalFile, false, true);
		updateFile.setApproved(true);
		updateFile.setApprovalDate(DateUtils.toDate("09/01/2017 8:00 AM", DateUtils.DATE_FORMAT_SHORT));
		updateFile.setApprovedByUserId(MathUtils.SHORT_ONE);

		// Update - Nothing because we don't audit from null for approval date and approved by user id and we don't audit approved from false = true
		validateNoAuditTrailDescription(DaoEventTypes.UPDATE, updateFile, portalFile);

		PortalFile updateFile2 = BeanUtils.cloneBean(updateFile, false, true);

		// Date with Time Comparison Tests
		updateFile2.setApprovalDate(DateUtils.addMilliseconds(updateFile.getApprovalDate(), 5));
		// Nothing Audited because we ignore milliseconds in time comparison
		validateNoAuditTrailDescription(DaoEventTypes.UPDATE, updateFile2, updateFile);

		// Clear Time
		updateFile2.setApprovalDate(DateUtils.clearTime(updateFile.getApprovalDate()));
		// Nothing Audited because if One value doesn't have time, then we compare without time
		validateNoAuditTrailDescription(DaoEventTypes.UPDATE, updateFile2, updateFile);


		// Clear Approval Fields
		updateFile2.setApproved(false);
		updateFile2.setApprovalDate(null);
		updateFile2.setApprovedByUserId(null);

		validateAuditTrailDescription("Test File 09/01/2017 approvalDate: [2017-09-01 08:00:00] to [null] approved: [true] to [false] approvedByUserId: [1] to [null]", DaoEventTypes.UPDATE, updateFile2, updateFile);

		// Delete
		validateAuditTrailDescription("Test File 09/01/2017 publishDate: [2017-09-05] reportDate: [2017-09-01] fileCategory: [{id=null,label=Daily Tracking Report}]", DaoEventTypes.DELETE, updateFile2, null);
	}


	/**
	 * Used to test Always Audit fields
	 */
	@Test
	public void testPortalTrackingAuditPortalSecurityUserAssignmentResource() {
		PortalSecurityUserAssignmentResource assignmentResource = PortalTestObjectFactory.newPortalSecurityUserAssignmentResource();

		// Insert
		validateAuditTrailDescription("John Smith - Relationship 1 - Daily Tracking Report accessAllowed: [true]", DaoEventTypes.INSERT, assignmentResource, null);

		PortalSecurityUserAssignmentResource updateAssignmentResource = BeanUtils.cloneBean(assignmentResource, false, true);
		updateAssignmentResource.setAccessAllowed(false);

		// Update
		validateAuditTrailDescription("John Smith - Relationship 1 - Daily Tracking Report accessAllowed: [true] to [false]", DaoEventTypes.UPDATE, updateAssignmentResource, assignmentResource);

		// Delete
		validateAuditTrailDescription("John Smith - Relationship 1 - Daily Tracking Report securityResource: [{id=null,label=Daily Tracking Report}] securityUserAssignment: [{id=null, label=John Smith - Relationship 1}] accessAllowed: [false]", DaoEventTypes.DELETE, updateAssignmentResource, null);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private <T> void validateNoAuditTrailDescription(DaoEventTypes eventType, T bean, T originalBean) {
		validateAuditTrailDescription(null, eventType, bean, originalBean);
	}


	private <T> void validateAuditTrailDescription(String expectedDescription, DaoEventTypes eventType, T bean, T originalBean) {
		String result = PortalTrackingAuditUtils.generatePortalTrackingAuditEventDescription(eventType, bean, originalBean, PortalTrackingAuditUtils.getPortalTrackingAuditColumnConfigMap(bean.getClass()));
		// Replace New Lines with blanks to make comparison easier
		if (result != null) {
			result = result.replace(StringUtils.NEW_LINE, " ");
			result = result.trim();
		}
		//
		Assertions.assertEquals(expectedDescription, result);
	}
}
