package com.clifton.portal.tracking.ip;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortalTrackingIpServiceImplTests {

	@Resource
	private String excludeIpRangeRegEx;

	private Pattern excludeIpPattern = null;


	@Test
	public void testExcludeIpAddress() {
		//  Internal IP so should exclude
		validateExcludeIpAddress("10.54.59.120", true);
		validateExcludeIpAddress("10.68.200.218", true);

		// External IP
		validateExcludeIpAddress("23.25.149.149", false);
		validateExcludeIpAddress("216.30.186.210", false);

		// Internal Reverse Proxy that happens locally
		validateExcludeIpAddress("0:0:0:0:0:0:0:1", true);
	}


	private void validateExcludeIpAddress(String ip, boolean expectExcluded) {
		if (this.excludeIpPattern == null) {
			this.excludeIpPattern = Pattern.compile(this.excludeIpRangeRegEx);
		}

		Matcher matcher = this.excludeIpPattern.matcher(ip);
		boolean excluded = matcher.find();
		if (expectExcluded) {
			Assertions.assertTrue(excluded, "Expected IP Address [" + ip + "] to be an IP that is excluded but it was not.");
		}
		else {
			Assertions.assertFalse(excluded, "Expected IP Address [" + ip + "] to be an IP that is NOT excluded but it was.");
		}
	}
}
