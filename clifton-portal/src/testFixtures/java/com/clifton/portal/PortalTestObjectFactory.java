package com.clifton.portal;

import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserRole;


/**
 * @author manderson
 */
public class PortalTestObjectFactory {

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityUser newPortalSecurityUser_External() {
		PortalSecurityUser securityUser = new PortalSecurityUser();
		securityUser.setFirstName("John");
		securityUser.setLastName("Smith");
		securityUser.setEmailAddress("johnsmith@email.com");
		securityUser.setUsername(securityUser.getEmailAddress());
		securityUser.setTitle("Test User");
		securityUser.setCompanyName("Test Company");
		securityUser.setUserRole(PortalTestObjectFactory.newPortalSecurityUserRole_ClientUser());
		return securityUser;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityUserAssignment newPortalSecurityUserAssignment() {
		PortalSecurityUser securityUser = newPortalSecurityUser_External();
		PortalEntity portalEntity = newPortalEntity_ClientRelationship();
		PortalSecurityUserAssignment assignment = new PortalSecurityUserAssignment();
		assignment.setPortalEntity(portalEntity);
		assignment.setSecurityUser(securityUser);
		assignment.setAssignmentRole(newPortalSecurityUserRole_ClientUser());
		return assignment;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityUserAssignmentResource newPortalSecurityUserAssignmentResource() {
		PortalSecurityUserAssignmentResource assignmentResource = new PortalSecurityUserAssignmentResource();
		PortalSecurityUserAssignment assignment = newPortalSecurityUserAssignment();
		assignmentResource.setAccessAllowed(true);
		assignmentResource.setSecurityUserAssignment(assignment);
		assignmentResource.setSecurityResource(newPortalSecurityResource());
		return assignmentResource;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityUserRole newPortalSecurityUserRole_ClientUser() {
		PortalSecurityUserRole userRole = new PortalSecurityUserRole();
		userRole.setName("Client User");
		userRole.setPortalEntitySpecific(true);
		return userRole;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalFile newPortalFile() {
		PortalFile portalFile = new PortalFile();
		portalFile.setDisplayName("Test File 09/01/2017");
		portalFile.setFileCategory(newPortalFileCategory());
		portalFile.setReportDate(DateUtils.toDate("09/01/2017"));
		portalFile.setPublishDate(DateUtils.toDate("09/05/2017"));
		return portalFile;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalFileCategory newPortalFileCategory() {
		PortalFileCategory parent = newPortalFileCategoryImpl(PortalFileCategoryTypes.CATEGORY_SUBSET, "Investments", newPortalFileCategoryImpl(PortalFileCategoryTypes.CATEGORY, "Reporting", null));
		PortalFileCategory fileCategory = newPortalFileCategoryImpl(PortalFileCategoryTypes.FILE_GROUP, "Daily Tracking Report", parent);
		fileCategory.setPortalFileFrequency(PortalFileFrequencies.DAILY);
		return fileCategory;
	}


	private static PortalFileCategory newPortalFileCategoryImpl(PortalFileCategoryTypes categoryType, String name, PortalFileCategory parent) {
		PortalFileCategory fileCategory = new PortalFileCategory();
		fileCategory.setFileCategoryType(categoryType);
		fileCategory.setName(name);
		fileCategory.setParent(parent);
		return fileCategory;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityResource newPortalSecurityResource() {
		PortalSecurityResource resource = new PortalSecurityResource();
		resource.setName("Daily Tracking Report");
		return resource;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalEntity newPortalEntity_ClientRelationship() {
		PortalEntity portalEntity = new PortalEntity();
		portalEntity.setName("Relationship 1");
		portalEntity.setEntitySourceSection(newPortalEntitySourceSection_ClientRelationship());
		return portalEntity;
	}


	private static PortalEntitySourceSection newPortalEntitySourceSection_ClientRelationship() {
		PortalEntitySourceSection sourceSection = new PortalEntitySourceSection();
		PortalEntityType entityType = new PortalEntityType();
		entityType.setName("Client Relationship");
		entityType.setSecurableEntity(true);
		sourceSection.setEntityType(entityType);
		return sourceSection;
	}
}
