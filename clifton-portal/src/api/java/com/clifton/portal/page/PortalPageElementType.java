package com.clifton.portal.page;

import com.clifton.core.beans.NamedEntity;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The {@link PortalPageElementType} type defines a backing field for which {@link PortalPageElement} entities can be created. This field represents the placeholder in the UI for
 * which applicable elements will be used. Examples of types include email address fields, message placeholders, and other various text holders.
 *
 * @author mikeh
 * @see PortalPageElement
 */
@PortalTrackingAuditAware
public class PortalPageElementType extends NamedEntity<Short> {

	/**
	 * The key representing the placeholder. These are named with a similar convention to Java packages. For example, a note on the "User Management" &rarr; "Edit User" page might
	 * have the key <tt>admin.user.add-edit.role-note</tt>.
	 */
	private String key;
	/**
	 * If {@code true}, multiple elements of this type may be applied by the UI simultaneously. Otherwise, only a single element may apply at a time. This field should only be
	 * marked as {@code true} if the UI is capable of applying multiple elements at a time. The value of this field affects on-save and on-retrieve validation.
	 */
	private boolean allowMultipleElementsOfType;
	/**
	 * If {@code true}, this type must have a global element. This ensures that at least one element will always apply for this type. This flag should be enabled for types for
	 * which the UI requires a value, either explicitly (such that it cannot execute without a value) or implicitly (such that the UI does not make sense without a value).
	 */
	private boolean requireGlobalElement;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getKey() {
		return this.key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public boolean isAllowMultipleElementsOfType() {
		return this.allowMultipleElementsOfType;
	}


	public void setAllowMultipleElementsOfType(boolean allowMultipleElementsOfType) {
		this.allowMultipleElementsOfType = allowMultipleElementsOfType;
	}


	public boolean isRequireGlobalElement() {
		return this.requireGlobalElement;
	}


	public void setRequireGlobalElement(boolean requireGlobalElement) {
		this.requireGlobalElement = requireGlobalElement;
	}
}
