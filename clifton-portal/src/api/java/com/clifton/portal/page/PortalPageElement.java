package com.clifton.portal.page;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The {@link PortalPageElement} type defines a page element to be used by the UI. These elements have values which are typically displayed on the page, such as email addresses,
 * messages, and text, but can also be used for variable data that is used on the page elsewhere, such as email addresses to use when generating an email pop-up.
 * <p>
 * These page elements are configured with specificity parameters on {@link PortalPageElementType}, {@link PortalEntityViewType}, and {@link #priority}, such that elements can be
 * selected based on the {@link PortalEntityViewType view types} available for the current user. All elements at the most specific level for each element type will apply for each
 * user. For example, a user with assignments providing both retail and institutional view types may have multiple elements for a single element type, some for the retail
 * view type and some for the institutional view type. In these cases, the {@link #priority} field may be used to deduce a single resulting element type, if desired.
 *
 * @author mikeh
 * @see PortalPageElementType
 * @see PortalPageElementService
 */
@PortalTrackingAuditAware
public class PortalPageElement extends BaseEntity<Integer> {

	// Element properties
	/**
	 * The element type. This represents the field for which this element is a value.
	 */
	private PortalPageElementType type;
	/**
	 * The value to be used by the UI when this element applies.
	 */
	private String value;
	/**
	 * The order to use when multiple elements of the same type apply.
	 */
	private Integer order;

	// Specificity properties
	/**
	 * The view type to which this element applies. This element will only apply to a user if the user has assignments to entities with this view type.
	 */
	private PortalEntityViewType viewType;
	/**
	 * The priority value to use to resolve conflicts between multiple elements which may apply for a given {@link #type} for the querying user. All elements with the maximum
	 * priority value will be returned to the client.
	 */
	private Integer priority;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementType getType() {
		return this.type;
	}


	public void setType(PortalPageElementType type) {
		this.type = type;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public PortalEntityViewType getViewType() {
		return this.viewType;
	}


	public void setViewType(PortalEntityViewType viewType) {
		this.viewType = viewType;
	}


	public Integer getPriority() {
		return this.priority;
	}


	public void setPriority(Integer priority) {
		this.priority = priority;
	}
}
