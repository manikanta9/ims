package com.clifton.portal.page;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The {@link PortalPageElementTypeSearchForm} is the search form for querying {@link PortalPageElementType} entities.
 *
 * @author mikeh
 */
public class PortalPageElementTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,key,description")
	private String searchPattern;
	@SearchField
	private Short id;
	@SearchField
	private String name;
	@SearchField
	private String description;
	@SearchField
	private String key;
	@SearchField
	private Boolean allowMultipleElementsOfType;
	@SearchField
	private Boolean requireGlobalElement;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getKey() {
		return this.key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public Boolean getAllowMultipleElementsOfType() {
		return this.allowMultipleElementsOfType;
	}


	public void setAllowMultipleElementsOfType(Boolean allowMultipleElementsOfType) {
		this.allowMultipleElementsOfType = allowMultipleElementsOfType;
	}


	public Boolean getRequireGlobalElement() {
		return this.requireGlobalElement;
	}


	public void setRequireGlobalElement(Boolean requireGlobalElement) {
		this.requireGlobalElement = requireGlobalElement;
	}
}
