package com.clifton.portal.page;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The {@link PortalPageElementSearchForm} is the search form DTO for {@link PortalPageElement} queries.
 *
 * @author mikeh
 */
public class PortalPageElementSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "type.name,type.key,viewType.name,value")
	private String searchPattern;
	@SearchField
	private Integer id;
	@SearchField(searchField = "type.id")
	private Short typeId;
	@SearchField(searchField = "type.name")
	private String typeName;
	@SearchField(searchField = "type.key")
	private String typeKey;
	@SearchField(searchField = "type.allowMultipleElementsOfType")
	private Boolean typeIsAllowMultipleElementsOfType;
	@SearchField
	private String value;
	@SearchField
	private Integer order;
	@SearchField(searchField = "viewType.id")
	private Short viewTypeId;
	@SearchField(searchField = "viewType.name")
	private String viewTypeName;
	@SearchField(searchField = "viewType.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean viewTypeNotNull;
	@SearchField
	private Integer priority;

	// Custom search fields
	/**
	 * If set, queries will filter the resulting set of elements to the most specific set of elements applicable to the given user ID.
	 */
	private Short viewAsUserId;
	/**
	 * If set, queries will filter the resulting set of elements to the most specific set of elements applicable to the given entity ID.
	 */
	private Integer viewAsEntityId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getTypeKey() {
		return this.typeKey;
	}


	public void setTypeKey(String typeKey) {
		this.typeKey = typeKey;
	}


	public Boolean getTypeIsAllowMultipleElementsOfType() {
		return this.typeIsAllowMultipleElementsOfType;
	}


	public void setTypeIsAllowMultipleElementsOfType(Boolean typeIsAllowMultipleElementsOfType) {
		this.typeIsAllowMultipleElementsOfType = typeIsAllowMultipleElementsOfType;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Short getViewTypeId() {
		return this.viewTypeId;
	}


	public void setViewTypeId(Short viewTypeId) {
		this.viewTypeId = viewTypeId;
	}


	public String getViewTypeName() {
		return this.viewTypeName;
	}


	public void setViewTypeName(String viewTypeName) {
		this.viewTypeName = viewTypeName;
	}


	public Boolean getViewTypeNotNull() {
		return this.viewTypeNotNull;
	}


	public void setViewTypeNotNull(Boolean viewTypeNotNull) {
		this.viewTypeNotNull = viewTypeNotNull;
	}


	public Integer getPriority() {
		return this.priority;
	}


	public void setPriority(Integer priority) {
		this.priority = priority;
	}


	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}
}
