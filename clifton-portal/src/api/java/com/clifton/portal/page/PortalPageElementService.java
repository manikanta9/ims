package com.clifton.portal.page;

import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * The {@link PortalPageElementService} declares the service methods used for working with {@link PortalPageElement} and {@link PortalPageElementType} entities.
 *
 * @author mikeh
 */
public interface PortalPageElementService {

	////////////////////////////////////////////////////////////////////////////
	////////            PortalPageElement Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalPageElement getPortalPageElement(int id);


	@PortalSecureMethod
	public List<PortalPageElement> getPortalPageElementList(PortalPageElementSearchForm searchForm);


	/**
	 * Gets the list of default {@link PortalPageElement} entities. These are the entities that would apply for a user with no view types or for a user who is not logged in.
	 */
	@PortalSecureMethod(disableSecurity = true)
	public List<PortalPageElement> getPortalPageElementListDefault();


	/**
	 * Gets the list of {@link PortalPageElement} entities which apply to the given user.
	 */
	@PortalSecureMethod
	public List<PortalPageElement> getPortalPageElementListForUser(short userId);


	/**
	 * Gets the list of {@link PortalPageElement} entities which apply to the given entity.
	 */
	@PortalSecureMethod
	public List<PortalPageElement> getPortalPageElementListForEntity(int entityId);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalPageElement savePortalPageElement(PortalPageElement element);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalPageElement(int id);


	////////////////////////////////////////////////////////////////////////////
	////////            PortalPageElementType Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalPageElementType getPortalPageElementType(short id);


	@PortalSecureMethod
	public List<PortalPageElementType> getPortalPageElementTypeList(PortalPageElementTypeSearchForm searchForm);


	@PortalSecureMethod(adminSecurity = true)
	public PortalPageElementType savePortalPageElementType(PortalPageElementType type);


	@PortalSecureMethod(adminSecurity = true)
	public void deletePortalPageElementType(short id);
}
