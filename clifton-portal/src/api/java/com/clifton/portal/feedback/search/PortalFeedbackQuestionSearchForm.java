package com.clifton.portal.feedback.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalFeedbackQuestionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,questionText")
	private String searchPattern;

	// Used for sorting
	@SearchField(searchField = "parent.displayOrder,displayOrder", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_PADDING)
	private Short orderExpanded;


	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean rootOnly;

	@SearchField
	private Boolean disabled;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getOrderExpanded() {
		return this.orderExpanded;
	}


	public void setOrderExpanded(Short orderExpanded) {
		this.orderExpanded = orderExpanded;
	}


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
