package com.clifton.portal.feedback;

import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;

import java.util.List;


/**
 * The <code>PortalFeedbackQuestion</code> is a specific question that we are asking clients about
 * If parent is empty then it's a new section of questions, where if parent is not empty then it's a follow up to the parent question (usually free text)
 *
 * @author manderson
 */
public class PortalFeedbackQuestion extends NamedHierarchicalEntity<PortalFeedbackQuestion, Integer> {


	private Short displayOrder;

	/**
	 * The display text for the question
	 */
	private String questionText;

	/**
	 * If there are selectable options for the answer (radio box), otherwise free text
	 */
	private boolean selectableOption;


	/**
	 * Will allow us to change questions around without losing history of existing questions.
	 */
	private boolean disabled;


	private List<PortalFeedbackQuestionOption> optionList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getMaxDepth() {
		return 2;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}


	public boolean isSelectableOption() {
		return this.selectableOption;
	}


	public void setSelectableOption(boolean selectableOption) {
		this.selectableOption = selectableOption;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public String getQuestionText() {
		return this.questionText;
	}


	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}


	public List<PortalFeedbackQuestionOption> getOptionList() {
		return this.optionList;
	}


	public void setOptionList(List<PortalFeedbackQuestionOption> optionList) {
		this.optionList = optionList;
	}
}
