package com.clifton.portal.feedback;

import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.util.List;


/**
 * The <code>PortalFeedbackResultEntry</code> is a virtual DTO that encapsulates the list of {@link PortalFeedbackResult}s
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortalFeedbackResultEntry {

	private List<PortalFeedbackResult> resultList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PortalFeedbackResult> getResultList() {
		return this.resultList;
	}


	public void setResultList(List<PortalFeedbackResult> resultList) {
		this.resultList = resultList;
	}
}
