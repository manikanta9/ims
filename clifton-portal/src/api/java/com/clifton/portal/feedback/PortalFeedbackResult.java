package com.clifton.portal.feedback;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.Date;


/**
 * The <code>PortalFeedbackResult</code> contains a specific user's answer to a feedback question at a given time
 *
 * @author manderson
 */
public class PortalFeedbackResult extends BaseSimpleEntity<Integer> {

	private PortalFeedbackQuestion feedbackQuestion;

	private String resultText;

	/**
	 * User that entered the feedback
	 */
	private PortalSecurityUser securityUser;

	/**
	 * Datetime when the feedback was entered
	 */
	private Date resultDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFeedbackQuestion getFeedbackQuestion() {
		return this.feedbackQuestion;
	}


	public void setFeedbackQuestion(PortalFeedbackQuestion feedbackQuestion) {
		this.feedbackQuestion = feedbackQuestion;
	}


	public String getResultText() {
		return this.resultText;
	}


	public void setResultText(String resultText) {
		this.resultText = resultText;
	}


	public PortalSecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(PortalSecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public Date getResultDate() {
		return this.resultDate;
	}


	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}
}
