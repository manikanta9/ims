package com.clifton.portal.feedback;

import com.clifton.portal.feedback.search.PortalFeedbackQuestionSearchForm;
import com.clifton.portal.feedback.search.PortalFeedbackResultSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalFeedbackService {

	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal Feedback Question Methods           ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalFeedbackQuestion getPortalFeedbackQuestion(int id);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalFeedbackQuestion savePortalFeedbackQuestion(PortalFeedbackQuestion bean);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalFeedbackQuestion(int id);


	@PortalSecureMethod
	public List<PortalFeedbackQuestion> getPortalFeedbackQuestionList(PortalFeedbackQuestionSearchForm searchForm, boolean populateOptions);

	////////////////////////////////////////////////////////////////////////////////
	///////////              Portal Feedback Result Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalFeedbackResult getPortalFeedbackResult(int id);


	@PortalSecureMethod
	public List<PortalFeedbackResult> getPortalFeedbackResultList(PortalFeedbackResultSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////
	////////////              Portal Feedback Entry Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public void savePortalFeedbackResultEntry(PortalFeedbackResultEntry bean);
}
