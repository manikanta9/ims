package com.clifton.portal.feedback.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalFeedbackResultSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "feedbackQuestion.id")
	private Integer feedbackQuestionId;

	@SearchField(searchFieldPath = "feedbackQuestion", searchField = "name")
	private String feedbackQuestionName;

	// Used for sorting
	@SearchField(searchFieldPath = "feedbackQuestion", searchField = "parent.displayOrder,displayOrder", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_PADDING)
	private Short orderExpanded;

	@SearchField(searchFieldPath = "feedbackQuestion", searchField = "questionText")
	private String feedbackQuestionText;

	@SearchField
	private Date resultDate;

	@SearchField
	private String resultText;

	@SearchField(searchFieldPath = "securityUser", searchField = "username,firstName,lastName,title,phoneNumber,companyName")
	private String userSearchPattern;

	@SearchField(searchFieldPath = "securityUser", searchField = "username")
	private String username;

	@SearchField(searchFieldPath = "securityUser", searchField = "title")
	private String userTitle;

	@SearchField(searchFieldPath = "securityUser", searchField = "phoneNumber")
	private String userPhoneNumber;

	@SearchField(searchFieldPath = "securityUser", searchField = "companyName")
	private String userCompanyName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getFeedbackQuestionId() {
		return this.feedbackQuestionId;
	}


	public void setFeedbackQuestionId(Integer feedbackQuestionId) {
		this.feedbackQuestionId = feedbackQuestionId;
	}


	public String getFeedbackQuestionName() {
		return this.feedbackQuestionName;
	}


	public void setFeedbackQuestionName(String feedbackQuestionName) {
		this.feedbackQuestionName = feedbackQuestionName;
	}


	public String getFeedbackQuestionText() {
		return this.feedbackQuestionText;
	}


	public void setFeedbackQuestionText(String feedbackQuestionText) {
		this.feedbackQuestionText = feedbackQuestionText;
	}


	public Date getResultDate() {
		return this.resultDate;
	}


	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}


	public String getResultText() {
		return this.resultText;
	}


	public void setResultText(String resultText) {
		this.resultText = resultText;
	}


	public Short getOrderExpanded() {
		return this.orderExpanded;
	}


	public void setOrderExpanded(Short orderExpanded) {
		this.orderExpanded = orderExpanded;
	}


	public String getUserSearchPattern() {
		return this.userSearchPattern;
	}


	public void setUserSearchPattern(String userSearchPattern) {
		this.userSearchPattern = userSearchPattern;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getUserTitle() {
		return this.userTitle;
	}


	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}


	public String getUserPhoneNumber() {
		return this.userPhoneNumber;
	}


	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}


	public String getUserCompanyName() {
		return this.userCompanyName;
	}


	public void setUserCompanyName(String userCompanyName) {
		this.userCompanyName = userCompanyName;
	}
}
