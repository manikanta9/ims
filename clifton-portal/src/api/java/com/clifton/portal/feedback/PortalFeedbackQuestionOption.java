package com.clifton.portal.feedback;

import com.clifton.core.beans.NamedEntity;


/**
 * The <code>PortalFeedbackQuestionOption</code> contains a list of answer options that can be used to answer a specific question
 *
 * @author manderson
 */
public class PortalFeedbackQuestionOption extends NamedEntity<Integer> {

	private PortalFeedbackQuestion feedbackQuestion;

	private Short displayOrder;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFeedbackQuestion getFeedbackQuestion() {
		return this.feedbackQuestion;
	}


	public void setFeedbackQuestion(PortalFeedbackQuestion feedbackQuestion) {
		this.feedbackQuestion = feedbackQuestion;
	}


	public Short getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
