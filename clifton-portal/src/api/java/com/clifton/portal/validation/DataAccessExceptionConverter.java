package com.clifton.portal.validation;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.validation.BaseDataAccessExceptionConverter;
import com.clifton.portal.security.user.PortalSecurityUserService;


/**
 * @author manderson
 */
public class DataAccessExceptionConverter extends BaseDataAccessExceptionConverter {


	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public LabeledObject getSecurityUser(short id) {
		return getPortalSecurityUserService().getPortalSecurityUser(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
