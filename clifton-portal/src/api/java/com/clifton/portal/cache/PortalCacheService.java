package com.clifton.portal.cache;

import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>PortalCacheService</code> is used to pre-load caches for the portal that are frequently used to help with performance of requests
 *
 * @author manderson
 */
public interface PortalCacheService {


	/**
	 * Pre-loads all frequently used caches.  Can optionally NOT clear the cache prior to loading all
	 */
	@RequestMapping("portalCacheReload")
	@PortalSecureMethod(adminSecurity = true)
	public void reloadPortalCache(boolean doNotClearCache);
}
