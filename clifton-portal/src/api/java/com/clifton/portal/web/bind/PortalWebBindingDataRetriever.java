package com.clifton.portal.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.web.bind.WebBindingDataRetriever;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;


/**
 * @author vgomelsky
 */
public class PortalWebBindingDataRetriever implements WebBindingDataRetriever {

	private PortalWebBindingDataRetrieverService portalWebBindingDataRetrieverService;

	private int order;
	private Pattern requestUriPattern;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	@Override
	public boolean isApplicableForRequest(WebRequest request) {
		if (this.requestUriPattern == null) {
			return true;
		}
		String uri = request.getDescription(false);
		return this.requestUriPattern.matcher(uri).matches();
	}


	@Override
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId) {
		return getPortalWebBindingDataRetrieverService().getPortalObject(entityType.getName(), (Number) entityId);
	}


	@Override
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType) {
		return getPortalWebBindingDataRetrieverService().getPortalObjectColumnList(entityType.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalWebBindingDataRetrieverService getPortalWebBindingDataRetrieverService() {
		return this.portalWebBindingDataRetrieverService;
	}


	public void setPortalWebBindingDataRetrieverService(PortalWebBindingDataRetrieverService portalWebBindingDataRetrieverService) {
		this.portalWebBindingDataRetrieverService = portalWebBindingDataRetrieverService;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public Pattern getRequestUriPattern() {
		return this.requestUriPattern;
	}


	public void setRequestUriPattern(Pattern requestUriPattern) {
		this.requestUriPattern = requestUriPattern;
	}
}
