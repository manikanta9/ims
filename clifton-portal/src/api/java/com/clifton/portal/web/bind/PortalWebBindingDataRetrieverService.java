package com.clifton.portal.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * The PortalWebBindingDataRetrieverService interface defines methods that external callers
 * need to do proper binding and validation of portal objects.
 *
 * @author vgomelsky
 */
public interface PortalWebBindingDataRetrieverService {

	/**
	 * Returns the DTO of the specified type for the specified primary key.
	 */
	@PortalSecureMethod
	public <T extends IdentityObject> T getPortalObject(String objectClassName, Number objectId);


	/**
	 * Returns Column meta-data for each column of the specified DTO type.
	 */
	@PortalSecureMethod
	public List<Column> getPortalObjectColumnList(String objectClassName);
}
