package com.clifton.portal.file;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.PortalEntityViewTypeAware;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortalFile</code> is something that is posted to the portal.  This is usually an actual file, but could be other things such as
 * free text, contacts/team information, etc.
 * <p>
 * A portal file represents one item that is posted, and then that one file can be assigned to multiple entities so we don't store multiple copies of the same file
 * For example, in IMS a file is posted to all investors of a Fund - there is one file and and it's assigned to all of the accounts (or clients) invested in that fund
 *
 * @author manderson
 */
@PortalTrackingAuditAware(insert = false)
public class PortalFile extends BaseEntity<Integer> implements LabeledObject, PortalEntityViewTypeAware {

	private PortalFileCategory fileCategory;

	/**
	 * If different than the default frequency on the file category
	 */
	private PortalFileFrequencies fileFrequency;

	/**
	 * The entity the file was posted to
	 * Use PortalFileAssignment when needed to assign the file to multiple entities
	 */
	private PortalEntity postPortalEntity;

	/**
	 * When we post something to all entities - i.e. payment instructions for invoices that is standard text and posted to everyone
	 * Can be limited by view type - i.e. Global, but only for Institutional entities
	 */
	private boolean globalFile;

	/**
	 * Only used when globalFile = true to limit global files to specific view type
	 */
	private PortalEntityViewType entityViewType;

	// Note: This is not associated with a specific PortalEntity because we aren't going to create a row in that table for everything
	// i.e. we won't be creating a row for every portfolio run, but we still want that association here.

	private PortalEntitySourceSection entitySourceSection;

	private Integer sourceFkFieldId;

	/**
	 * Populated as the current user with fileNameAndPath is populated with the new file
	 * Usually this would match the createUserId, however when a file is replaced it will be the user that replaced the file
	 * Used for dual approval as the approver cannot be the same person that created the file
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private Short fileCreateUserId;

	/**
	 * Required for file types that require a document associated with it
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private String fileNameAndPath;

	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private Long fileSize;

	/**
	 * This would be the file name, or other text for display when not a file
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private String displayName;

	/**
	 * This can contain text for display - for example, Payment Instructions
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private String displayText;


	private Date reportDate;

	/**
	 * When the file should stop displaying by default (as opposed to historical searches)
	 * "reportDate" is the start date.  System defaults this based on file frequency, or optional override on the category
	 */
	private Date endDate;

	/**
	 * Date with time - When the file was published
	 * Useful for notifications and comparing last notification sent with what was published after that.
	 */
	private Date publishDate;

	@PortalTrackingAuditColumnConfig(doNotAuditUpdateFromValue = "false")
	private boolean approved;

	@PortalTrackingAuditColumnConfig(doNotAuditUpdateFromNull = true)
	private Date approvalDate;

	@PortalTrackingAuditColumnConfig(doNotAuditUpdateFromNull = true)
	private Short approvedByUserId;


	/**
	 * Used only when uploading file for a PortalFile
	 * When uploading files, we allow selecting multiple rollup entities
	 * so that a file can be uploaded once, but it still creates a new PortalFile object for each entity as the postEntity
	 */
	@NonPersistentField
	private List<PortalEntity> postEntityList;

	/**
	 * Used only when uploading file for a PortalFile
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private MultipartFile file;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getDisplayName();
	}


	public String getFileCategoryNameExpandedWithoutRoot() {
		if (getFileCategory() != null) {
			if (getFileCategory().getParent() != null) {
				return getFileCategory().getParent().getName() + " / " + getFileCategory().getName();
			}
			return getFileCategory().getName();
		}
		return null;
	}


	public String getEntitySourceLabel() {
		if (getEntitySourceSection() != null) {
			return getEntitySourceSection().getLabel() + ": " + getSourceFkFieldId();
		}
		return null;
	}


	/**
	 * Returns the file type (extension) from file name and path
	 * ONLY if the file type supports downloads
	 */
	public String getDocumentFileType() {
		if (!StringUtils.isEmpty(getFileNameAndPath())) {
			return FileUtils.getFileExtension(getFileNameAndPath());
		}
		return null;
	}


	public String getFileName() {
		if (!StringUtils.isEmpty(getFileNameAndPath())) {
			return FileUtils.getFileName(getFileNameAndPath());
		}
		return null;
	}


	public PortalFileFrequencies getCoalescePortalFileFrequency() {
		return ObjectUtils.coalesce(getFileFrequency(), (getFileCategory() != null ? getFileCategory().getPortalFileFrequency() : null));
	}


	public String getReportDateFormatted() {
		PortalFileFrequencies frequency = getCoalescePortalFileFrequency();
		if (frequency != null) {
			return frequency.getReportDateFormatted(getReportDate());
		}
		// Default to DAILY? SHOULD REQUIRE ON PORTAL FILE IF NOT DEFINED ON THE CATEGORY?
		return PortalFileFrequencies.DAILY.getReportDateFormatted(getReportDate());
	}


	/**
	 * This is the report date to use within the file name.  i.e. yyyy.MM.dd
	 */
	public String getReportDateFileFormatted() {
		PortalFileFrequencies frequency = getCoalescePortalFileFrequency();
		if (frequency != null) {
			return frequency.getReportDateFileFormatted(getReportDate());
		}
		// Default to DAILY? SHOULD REQUIRE ON PORTAL FILE IF NOT DEFINED ON THE CATEGORY?
		return PortalFileFrequencies.DAILY.getReportDateFileFormatted(getReportDate());
	}


	public Short getCoalesceFileCreateUserId() {
		return ObjectUtils.coalesce(getFileCreateUserId(), getCreateUserId());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntity getPostPortalEntity() {
		return this.postPortalEntity;
	}


	public void setPostPortalEntity(PortalEntity postPortalEntity) {
		this.postPortalEntity = postPortalEntity;
	}


	public PortalFileCategory getFileCategory() {
		return this.fileCategory;
	}


	public void setFileCategory(PortalFileCategory fileCategory) {
		this.fileCategory = fileCategory;
	}


	public PortalEntitySourceSection getEntitySourceSection() {
		return this.entitySourceSection;
	}


	public void setEntitySourceSection(PortalEntitySourceSection entitySourceSection) {
		this.entitySourceSection = entitySourceSection;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public String getFileNameAndPath() {
		return this.fileNameAndPath;
	}


	public void setFileNameAndPath(String fileNameAndPath) {
		this.fileNameAndPath = fileNameAndPath;
	}


	public Long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getDisplayText() {
		return this.displayText;
	}


	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getPublishDate() {
		return this.publishDate;
	}


	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}


	public boolean isApproved() {
		return this.approved;
	}


	public void setApproved(boolean approved) {
		this.approved = approved;
	}


	public Date getApprovalDate() {
		return this.approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public Short getApprovedByUserId() {
		return this.approvedByUserId;
	}


	public void setApprovedByUserId(Short approvedByUserId) {
		this.approvedByUserId = approvedByUserId;
	}


	public Date getReportDate() {
		return this.reportDate;
	}


	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}


	public boolean isGlobalFile() {
		return this.globalFile;
	}


	public void setGlobalFile(boolean globalFile) {
		this.globalFile = globalFile;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public PortalFileFrequencies getFileFrequency() {
		return this.fileFrequency;
	}


	public void setFileFrequency(PortalFileFrequencies fileFrequency) {
		this.fileFrequency = fileFrequency;
	}


	public Short getFileCreateUserId() {
		return this.fileCreateUserId;
	}


	public void setFileCreateUserId(Short fileCreateUserId) {
		this.fileCreateUserId = fileCreateUserId;
	}


	@Override
	public PortalEntityViewType getEntityViewType() {
		return this.entityViewType;
	}


	public void setEntityViewType(PortalEntityViewType entityViewType) {
		this.entityViewType = entityViewType;
	}


	public List<PortalEntity> getPostEntityList() {
		return this.postEntityList;
	}


	public void setPostEntityList(List<PortalEntity> postEntityList) {
		this.postEntityList = postEntityList;
	}
}
