package com.clifton.portal.file;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.template.PortalFileTemplate;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public interface PortalFileService {

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public String getPortalFileRootDirectory();

	////////////////////////////////////////////////////////////////////////////////
	////////////                  Portal File Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalFile getPortalFile(int id);


	@PortalSecureMethod
	public List<PortalFile> getPortalFileList(PortalFileSearchForm searchForm);


	/**
	 * Limits the file searches for those only where the current user has write access
	 * i.e. What are my files pending approval
	 */
	@PortalSecureMethod
	public List<PortalFile> getPortalFileListWithEditAccess(final PortalFileSearchForm searchForm, String supportTeamEmail);


	/**
	 * Returns the files associated with a given source entity.  Should usually only be one file per source
	 * but there could be multiple.  For example, one Invoice could have a source file of the Invoice PDF and the supporting documents
	 */
	@PortalSecureMethod
	public List<PortalFile> getPortalFileListForSourceEntity(String sourceSystemName, String sourceTableName, Integer sourceFkFieldId);


	/**
	 * Returns the list of SourceFKFieldIds for Portal Files for the following source system name and source table name
	 * Admin only and can be used by external system to update flag if files are posted or not.  Returns Integer list to reduce the amount of data returned
	 * Optional minCreateDate can be used to also reduce the amount of data returned, as on a daily basis we may only check back a year or so, but we could recheck all
	 */
	@PortalSecureMethod(adminSecurity = true)
	public Set<Integer> getPortalFileSourceFkFieldIdList(String sourceSystemName, String sourceTableName, Date minReportDate, boolean approvedOnly);


	@PortalSecureMethod
	public PortalFile savePortalFile(PortalFile portalFile);


	/**
	 * Internal call from upload service or external service that first validates the portal file,
	 * then saves the real file to the correct location and updates the file properties on the portal file
	 * and finally saved the portal file.  (One save)
	 */
	@DoNotAddRequestMapping
	public PortalFile savePortalFileNew(PortalFile portalFile, File file, String originalFileName, FileDuplicateActions fileDuplicateAction);


	/**
	 * Manual Portal File and Portal File Assignments save
	 * Used for system "posted" files - like Contacts and Teams
	 * If NO assignments, then the portal file will be deleted
	 */
	@PortalSecureMethod(adminSecurity = true)
	public PortalFile savePortalFileManual(PortalFile portalFile, List<PortalFileAssignment> assignmentList);


	/**
	 * Ability to move a portal file to a new category
	 */
	@PortalSecureMethod
	public void movePortalFileList(Integer[] portalFileIds, short portalFileCategoryId);


	/**
	 * Ability to approve (or un-approve) portal files
	 */
	@PortalSecureMethod
	public void approvePortalFileList(Integer[] portalFileIds, boolean approve);


	@PortalSecureMethod
	public void deletePortalFile(int id);


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Template Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the PortalFileTemplate, which includes the newly generated Display Name and Display Text for the file
	 * This is used to preview changes for a specific file and DOES NOT save any change in the database
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalFileTemplate getPortalFileTemplateForPortalFile(int portalFileId);


	/**
	 * Updates the selected files with the newly generated display name and display text.
	 * Note: Does not execute the save if there are no changes to display name or display text
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public void generatePortalFileTemplatesForPortalFiles(Integer[] portalFileIds);


	/**
	 * Called by PortalEntityObserver and PortalEntityFieldValueObserver to rebuild templates when the source entity was updated
	 */
	@DoNotAddRequestMapping
	public void generatePortalFileTemplatesForPortalFilesBySource(Integer portalEntitySourceSectionId, Integer sourceFkFieldId);


	/**
	 * Updates ALL files under the selected category with the newly generated display name and display text.
	 * Note: Does not execute the save if there are no changes to display name or display text
	 */
	// Admin only to run for entire category.  For large categories it might be easier to do via SQL on the backend.
	@PortalSecureMethod(adminSecurity = true)
	public void generatePortalFileTemplatesForPortalFileCategory(short fileCategoryId);


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Download Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Download a Portal File:
	 * Note: viewAsUserId is used only if the current user is not "portalEntitySpecific" and can be used to "view as" a user to validate/confirm permissions
	 */
	@PortalSecureMethod
	public FileWrapper downloadPortalFile(int portalFileId, Short viewAsUserId);


	/**
	 * Download multiple Portal Files into 1 file
	 * Note: all files must be PDF (as we can only concatenate PDF files)
	 * Note: viewAsUserId is used only if the current user is not "portalEntitySpecific" and can be used to "view as" a user to validate/confirm permissions
	 *
	 * @param zipFiles - indicates to create a zip file of all of the files, if false will concatenate them into one PDF (which requires all selections to also be PDF)
	 */
	@PortalSecureMethod
	public FileWrapper downloadPortalFileList(Integer[] portalFileIds, Short viewAsUserId, boolean zipFiles);


	////////////////////////////////////////////////////////////////////////////////
	//////////               Portal File Extended Methods                 //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalFileExtended> getPortalFileExtendedList(PortalFileExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////
	/////////                 Portal File Entity Methods                   /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Should be used rarely - currently will be used by Client UI to get Relationship Manager and Team Email information
	 * Note: Will filter out duplications, so if user can see 5 relationships and they all are the same relationship manager, that person will be displayed only once
	 */
	@PortalSecureMethod
	public List<PortalEntity> getPortalFileSourceEntityList(PortalFileExtendedSearchForm searchForm);


	/**
	 * Used for selecting View As Entity logic for a given file
	 * Returns the list of securable portal entities and their parents for view as functionality that have the file assigned to them
	 */
	@PortalSecureMethod
	public List<PortalEntity> getPortalFileSecurableEntityListForFile(int portalFileId);


	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal File Assignment Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalFileAssignment> getPortalFileAssignmentListByFile(int portalFileId);


	@PortalSecureMethod
	public PortalFileAssignment savePortalFileAssignmentExclude(int portalFileAssignmentId, boolean exclude);


	/**
	 * For given portal file will update the manually maintained file assignments to only include the assignmentPortalEntityList for the given source system
	 * Will NOT change assignments for entities from other systems.
	 * Called by IMS batch job or entity uploads ONLY
	 * IF sourceSectionSet is populated, will apply only to assignments for the sourceSystem and given list of sections, else all.
	 * This allows uploads different sections entities in separate files without changing others. i.e. RM override at Client Account Level
	 */
	@PortalSecureMethod(adminSecurity = true)
	public void updatePortalFileManualAssignments(PortalFile portalFile, PortalEntitySourceSystem sourceSystem, Set<PortalEntitySourceSection> sourceSectionSet, List<PortalEntity> assignedPortalEntityList);


	/**
	 * Schedules the rebuild to occur 10 seconds in the future, so multiple rollups for the same post entity are applied at the same time
	 * <p>
	 * Called from the PortalFileAssignmentPortalEntityRollupObserver when rollups are updated and file assignments need to be rebuilt
	 */
	@DoNotAddRequestMapping
	public void schedulePortalFileAssignmentListForPostEntityRebuild(PortalEntity postEntity);


	////////////////////////////////////////////////////////////////////////////////
	//////////                   Portal File Security                     //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of users that have access to the file.
	 * Required parameter on the search form is the "userRolePortalEntitySpecific" property
	 * If userRolePortalEntitySpecific = false, then would be internal PPA employees that have access to upload, edit the file
	 * Otherwise, client users that have access to download the file
	 *
	 * @param assignedPortalEntityId (optional) is used for files that can apply to multiple entities to limit to that specific assignment (used by notifications only)
	 */
	@PortalSecureMethod
	public List<PortalSecurityUser> getPortalFileSecurityUserList(int portalFileId, PortalSecurityUserSearchForm searchForm, Integer assignedPortalEntityId);
}
