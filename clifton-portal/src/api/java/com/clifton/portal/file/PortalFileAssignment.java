package com.clifton.portal.file;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.portal.entity.PortalEntity;


/**
 * The <code>PortalFileAssignment</code> is used to assign a portal file to a specific portal entity.
 * Example, posting a Daily Tracking Report to an Account, posting a Fund Report to an Account Group
 * <p>
 * Outside of manually managed assignments (i.e. Contacts) the file assignments are rebuilt based on the {@link com.clifton.portal.entity.PortalEntityRollup} children under the parent entity
 *
 * @author manderson
 */
public class PortalFileAssignment extends ManyToManyEntity<PortalFile, PortalEntity, Integer> {


	/**
	 * For file assignments that are automatically managed, users will still have the ability to manually exclude a specific file assignment
	 * Example: A large client group, with one client that has it's own set of Guidelines.  This allows the client group to still contain that specific client
	 * but for guidelines we can exclude the group level posted guidelines and post only the client specific guidelines file.
	 */
	private boolean excluded;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isExcluded() {
		return this.excluded;
	}


	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}
}
