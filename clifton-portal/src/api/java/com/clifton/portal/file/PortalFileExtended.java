package com.clifton.portal.file;

import com.clifton.portal.entity.PortalEntity;


/**
 * @author manderson
 */
public class PortalFileExtended extends PortalFile {

	/**
	 * Unique identifier - since each file can have many assignments (needed for UI to display each separately)
	 */
	private String uuid;

	/**
	 * Can't use "id" here - UI is merging records together by id
	 */
	private Integer portalFileId;

	/**
	 * The "postPortalEntity" from the file if it's a securable entity or from the assignments if more than one
	 */
	private PortalEntity assignedPortalEntity;

	/**
	 * Based on the assignedPortalEntity - if there is a field value for service it is populated here
	 * Would only apply to client accounts at this time.
	 */
	private String serviceName;

	/**
	 * Not always populated.
	 * This is set during notification generation for files to give users better names of files that we are notifying them about.
	 */
	private String downloadDisplayName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFileCategoryWithAssignmentLabel() {
		if (getAssignedPortalEntity() != null) {
			if (getFileCategory() != null) {
				return getAssignedPortalEntity().getLabel() + ": " + getFileCategory().getName();
			}
			return getAssignedPortalEntity().getLabel();
		}
		if (getFileCategory() != null) {
			return getFileCategory().getName();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getId() {
		return getPortalFileId();
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public Integer getPortalFileId() {
		return this.portalFileId;
	}


	public void setPortalFileId(Integer portalFileId) {
		this.portalFileId = portalFileId;
	}


	public PortalEntity getAssignedPortalEntity() {
		return this.assignedPortalEntity;
	}


	public void setAssignedPortalEntity(PortalEntity assignedPortalEntity) {
		this.assignedPortalEntity = assignedPortalEntity;
	}


	public String getServiceName() {
		return this.serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getDownloadDisplayName() {
		return this.downloadDisplayName;
	}


	public void setDownloadDisplayName(String downloadDisplayName) {
		this.downloadDisplayName = downloadDisplayName;
	}
}
