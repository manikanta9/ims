package com.clifton.portal.file.upload;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


/**
 * The <code>PortalFileUploadPendingCommand</code> is used for the bulk drag/drop feature which calls this upload command for each file that was dropped.
 * <p>
 * Note: Because of the way the ExternalServiceInvocationHandler works for multipart files (can only be a property on an argument, not the argument itself) need to us a command object
 *
 * @author manderson
 */
public class PortalFileUploadPendingCommand {

	/**
	 * Optional - If not set the system will try to determine based on the file names
	 */
	private Short portalFileCategoryId;


	/**
	 * Optional - If set, will use the given date for the report date instead of trying to determine it in the existing file name
	 */
	private Date reportDateOverride;

	/**
	 * File that we are uploading
	 */
	private MultipartFile file;


	/**
	 * Used to determine what to do if the file already exists (Skip, Replace, Error Out)
	 */
	private PortalFileUploadPendingFileActions fileAction;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPortalFileCategoryId() {
		return this.portalFileCategoryId;
	}


	public void setPortalFileCategoryId(Short portalFileCategoryId) {
		this.portalFileCategoryId = portalFileCategoryId;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public PortalFileUploadPendingFileActions getFileAction() {
		return this.fileAction;
	}


	public void setFileAction(PortalFileUploadPendingFileActions fileAction) {
		this.fileAction = fileAction;
	}


	public Date getReportDateOverride() {
		return this.reportDateOverride;
	}


	public void setReportDateOverride(Date reportDateOverride) {
		this.reportDateOverride = reportDateOverride;
	}
}
