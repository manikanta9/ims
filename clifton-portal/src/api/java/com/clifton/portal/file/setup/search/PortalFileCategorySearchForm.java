package com.clifton.portal.file.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionOptions;
import com.clifton.portal.security.search.PortalEntitySpecificAwareSearchForm;


/**
 * @author manderson
 */
public class PortalFileCategorySearchForm extends BaseAuditableEntitySearchForm implements PortalEntitySpecificAwareSearchForm {

	// Same as nameExpanded
	@SearchField(searchField = "parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String searchPattern;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String categoryNameEquals;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean rootOnly;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	@SearchField(searchField = "parent.id,parent.parent.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short parentIdExpanded;

	// Max Levels Deep is Currently 3
	@SearchField(searchField = "parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	// Used for sorting
	@SearchField(searchField = "parent.parent.categoryOrder,parent.categoryOrder,categoryOrder", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_PADDING)
	private Short orderExpanded;

	@SearchField(searchField = "categoryOrder")
	private Short categoryOrder;

	// Custom search field - does a where exists on the {@link PortalFileCategorySecurityResourceExpanded}
	private Short securityResourceId;

	// Custom search field - does a where exists on the {@link PortalFileCategorySecurityResourceExpanded}
	private Short[] securityResourceIds;

	@SearchField
	private PortalFileCategoryTypes fileCategoryType;

	@SearchField
	private PortalFileFrequencies portalFileFrequency;

	@SearchField
	private String folderName;

	@SearchField
	private String categoryEmailAddress;

	@SearchField(searchField = "categoryEmailAddress", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean categoryEmailAddressPopulated;

	@SearchField(searchField = "notificationDefinition.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean notificationUsed;

	@SearchField(searchField = "notificationDefinition.id", sortField = "notificationDefinition.name")
	private Short notificationDefinitionId;

	@SearchField(searchField = "subscriptionForFileCategory", searchFieldPath = "notificationDefinition.definitionType")
	private Boolean notificationSubscriptionForFileCategory;

	@SearchField
	private PortalNotificationSubscriptionOptions notificationSubscriptionOption;

	@SearchField
	private Boolean approvalRequired;

	@SearchField
	private Boolean systemDefined;


	@SearchField
	private String displayNameTemplate;

	@SearchField
	private String fileNameTemplate;

	@SearchField
	private String displayTextTemplate;

	@SearchField(searchField = "postEntityType.id", sortField = "postEntityType.name")
	private Short postEntityTypeId;

	@SearchField(searchField = "sourceEntityType.id", sortField = "sourceEntityType.name")
	private Short sourceEntityTypeId;


	// Custom Search Field that Checks the File Category Count Cache and only returns those categories with files
	// Populates IDs filter above
	private Boolean limitToCategoriesWithAvailableFiles;

	// Custom Search Field that Checks the File Category Count Cache and only returns those categories with APPROVED files
	// Populates IDs filter above
	private Boolean limitToCategoriesWithAvailableApprovedFiles;

	// Custom Search Field ONLY used when limitToCategoriesWithAvailableFiles is True.  Uses the given entity to find file counts for.
	// Populates IDs filter above
	private Integer limitToCategoriesForAssignedPortalEntityId;

	// Custom Search Field ONLY used when limitToCategoriesForAssignedPortalEntityId is True.  Excludes files posted to parent entities (Notifications)
	private Boolean excludeParentsOfAssignedPortalEntityId;

	// Custom Search Field for Viewing As User
	private Short viewAsUserId;

	// Custom Search Field for Viewing As Entity
	private Integer viewAsEntityId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}


	public Short getOrderExpanded() {
		return this.orderExpanded;
	}


	public void setOrderExpanded(Short orderExpanded) {
		this.orderExpanded = orderExpanded;
	}


	public String getCategoryNameEquals() {
		return this.categoryNameEquals;
	}


	public void setCategoryNameEquals(String categoryNameEquals) {
		this.categoryNameEquals = categoryNameEquals;
	}


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public Short getParentIdExpanded() {
		return this.parentIdExpanded;
	}


	public void setParentIdExpanded(Short parentIdExpanded) {
		this.parentIdExpanded = parentIdExpanded;
	}


	public Short getCategoryOrder() {
		return this.categoryOrder;
	}


	public void setCategoryOrder(Short categoryOrder) {
		this.categoryOrder = categoryOrder;
	}


	public Boolean getLimitToCategoriesWithAvailableFiles() {
		return this.limitToCategoriesWithAvailableFiles;
	}


	public void setLimitToCategoriesWithAvailableFiles(Boolean limitToCategoriesWithAvailableFiles) {
		this.limitToCategoriesWithAvailableFiles = limitToCategoriesWithAvailableFiles;
	}


	public Boolean getLimitToCategoriesWithAvailableApprovedFiles() {
		return this.limitToCategoriesWithAvailableApprovedFiles;
	}


	public void setLimitToCategoriesWithAvailableApprovedFiles(Boolean limitToCategoriesWithAvailableApprovedFiles) {
		this.limitToCategoriesWithAvailableApprovedFiles = limitToCategoriesWithAvailableApprovedFiles;
	}


	public Integer getLimitToCategoriesForAssignedPortalEntityId() {
		return this.limitToCategoriesForAssignedPortalEntityId;
	}


	public void setLimitToCategoriesForAssignedPortalEntityId(Integer limitToCategoriesForAssignedPortalEntityId) {
		this.limitToCategoriesForAssignedPortalEntityId = limitToCategoriesForAssignedPortalEntityId;
	}


	@Override
	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	@Override
	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	public PortalFileCategoryTypes getFileCategoryType() {
		return this.fileCategoryType;
	}


	public void setFileCategoryType(PortalFileCategoryTypes fileCategoryType) {
		this.fileCategoryType = fileCategoryType;
	}


	public PortalFileFrequencies getPortalFileFrequency() {
		return this.portalFileFrequency;
	}


	public void setPortalFileFrequency(PortalFileFrequencies portalFileFrequency) {
		this.portalFileFrequency = portalFileFrequency;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Short securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public String getFolderName() {
		return this.folderName;
	}


	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public String getCategoryEmailAddress() {
		return this.categoryEmailAddress;
	}


	public void setCategoryEmailAddress(String categoryEmailAddress) {
		this.categoryEmailAddress = categoryEmailAddress;
	}


	public Boolean getNotificationUsed() {
		return this.notificationUsed;
	}


	public void setNotificationUsed(Boolean notificationUsed) {
		this.notificationUsed = notificationUsed;
	}


	public Short getNotificationDefinitionId() {
		return this.notificationDefinitionId;
	}


	public void setNotificationDefinitionId(Short notificationDefinitionId) {
		this.notificationDefinitionId = notificationDefinitionId;
	}


	public Boolean getApprovalRequired() {
		return this.approvalRequired;
	}


	public void setApprovalRequired(Boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}


	public Short[] getSecurityResourceIds() {
		return this.securityResourceIds;
	}


	public void setSecurityResourceIds(Short[] securityResourceIds) {
		this.securityResourceIds = securityResourceIds;
	}


	public String getDisplayNameTemplate() {
		return this.displayNameTemplate;
	}


	public void setDisplayNameTemplate(String displayNameTemplate) {
		this.displayNameTemplate = displayNameTemplate;
	}


	public String getDisplayTextTemplate() {
		return this.displayTextTemplate;
	}


	public void setDisplayTextTemplate(String displayTextTemplate) {
		this.displayTextTemplate = displayTextTemplate;
	}


	public Short getSourceEntityTypeId() {
		return this.sourceEntityTypeId;
	}


	public void setSourceEntityTypeId(Short sourceEntityTypeId) {
		this.sourceEntityTypeId = sourceEntityTypeId;
	}


	public Short getPostEntityTypeId() {
		return this.postEntityTypeId;
	}


	public void setPostEntityTypeId(Short postEntityTypeId) {
		this.postEntityTypeId = postEntityTypeId;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public String getFileNameTemplate() {
		return this.fileNameTemplate;
	}


	public void setFileNameTemplate(String fileNameTemplate) {
		this.fileNameTemplate = fileNameTemplate;
	}


	public Boolean getNotificationSubscriptionForFileCategory() {
		return this.notificationSubscriptionForFileCategory;
	}


	public void setNotificationSubscriptionForFileCategory(Boolean notificationSubscriptionForFileCategory) {
		this.notificationSubscriptionForFileCategory = notificationSubscriptionForFileCategory;
	}


	public PortalNotificationSubscriptionOptions getNotificationSubscriptionOption() {
		return this.notificationSubscriptionOption;
	}


	public void setNotificationSubscriptionOption(PortalNotificationSubscriptionOptions notificationSubscriptionOption) {
		this.notificationSubscriptionOption = notificationSubscriptionOption;
	}


	public Boolean getCategoryEmailAddressPopulated() {
		return this.categoryEmailAddressPopulated;
	}


	public void setCategoryEmailAddressPopulated(Boolean categoryEmailAddressPopulated) {
		this.categoryEmailAddressPopulated = categoryEmailAddressPopulated;
	}


	public Boolean getExcludeParentsOfAssignedPortalEntityId() {
		return this.excludeParentsOfAssignedPortalEntityId;
	}


	public void setExcludeParentsOfAssignedPortalEntityId(Boolean excludeParentsOfAssignedPortalEntityId) {
		this.excludeParentsOfAssignedPortalEntityId = excludeParentsOfAssignedPortalEntityId;
	}


	@Override
	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	@Override
	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}
}
