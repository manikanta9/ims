package com.clifton.portal.file.setup;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionFrequencies;

import java.util.Date;
import java.util.Set;


/**
 * The <code>PortalFileFrequencies</code> enum defines frequencies for files.  A file category (file group level) can have a default frequency
 * and each file can override that frequency.
 * <p>
 * Frequencies are used to determine report dates, date formatting, and default file displays.
 * WARNING: Frequency Options should be kept in sync with portal-api-shared.js Clifton.portal.notification.subscription.GetSubscriptionFrequenciesForFileFrequency AND Client UI
 *
 * @author manderson
 */
public enum PortalFileFrequencies {

	// No Specific Frequency, this is used for files for documentation (i.e. IMA) where a file is available until someone explicitly un-approves and posts a new file
	FINAL(null, null),
	// File is expected for each weekday and/or business day. Default Display is for 7 calendar days
	DAILY(7, CollectionUtils.createHashSet(PortalNotificationSubscriptionFrequencies.WEEKLY_FIRST_BUSINESS_DAY, PortalNotificationSubscriptionFrequencies.MONTHLY_FIRST_BUSINESS_DAY, PortalNotificationSubscriptionFrequencies.QUARTERLY_FIRST_BUSINESS_DAY)),
	// File is expected once a week, but not on any specific date.  Default display is for 31 days (4 weeks + 3 days to get full month of posted files)
	WEEKLY(28, null), // No Specific Date Formatting Overrides - There is no specific day of the week, so just use the date as entered
	// File is expected for each month end. Default display is for 105 days (Approx 3 months + 15 days)
	MONTHLY(105, null) {
		@Override
		public String getReportDateFormatted(Date reportDate) {
			// i.e. January 2017
			return DateUtils.fromDate(reportDate, "MMMM yyyy");
		}


		@Override
		public String getReportDateFileFormatted(Date reportDate) {
			return DateUtils.fromDate(reportDate, "yyyy.MM");
		}


		@Override
		public Date getReportDateForDate(Date date) {
			return DateUtils.getLastDayOfMonth(date);
		}
	},
	// File is expected for each month end, but is a QTD file. Default display is for 105 days (Approx 3 months + 15 days)
	QUARTER_TO_DATE(105, null) {
		@Override
		public String getReportDateFormatted(Date reportDate) {
			// i.e. January 2017 QTD
			return DateUtils.fromDate(reportDate, "MMMM yyyy") + " QTD";
		}


		@Override
		public String getReportDateFileFormatted(Date reportDate) {
			return DateUtils.fromDate(reportDate, "yyyy.MM") + " QTD";
		}


		@Override
		public Date getReportDateForDate(Date date) {
			return DateUtils.getLastDayOfMonth(date);
		}
	},
	// File is expected for each quarter end. Default display is for 395 days (Approx 1 Year + 30 days)
	QUARTERLY(395, null) {
		// i.e. 4Q 2016
		@Override
		public String getReportDateFormatted(Date reportDate) {
			return DateUtils.getQuarter(reportDate) + "Q " + DateUtils.fromDate(reportDate, "yyyy");
		}


		@Override
		public String getReportDateFileFormatted(Date reportDate) {
			return DateUtils.fromDate(reportDate, "yyyy.MM");
		}


		@Override
		public Date getReportDateForDate(Date date) {
			return DateUtils.getLastDayOfQuarter(date);
		}
	},
	// File is expected for once a year (no specific date). Default display is for 1,490 days (Approx 4 Years + 30 days)
	ANNUALLY(1490, null) {
		// i.e. 2016
		@Override
		public String getReportDateFormatted(Date reportDate) {
			return DateUtils.fromDate(reportDate, "yyyy");
		}


		@Override
		public String getReportDateFileFormatted(Date reportDate) {
			return DateUtils.fromDate(reportDate, "yyyy");
		}


		@Override
		public Date getReportDateForDate(Date date) {
			return date;
		}
	};


	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////

	private final Integer displayDays;

	/**
	 * If not just all, these are optional frequencies, currently only used for daily.
	 * For example, for daily files, only send notifications for files posted on the first business day of the week
	 */
	private final Set<PortalNotificationSubscriptionFrequencies> allowedSubscriptionFrequencies;


	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	PortalFileFrequencies(Integer displayDays, Set<PortalNotificationSubscriptionFrequencies> allowedSubscriptionFrequencies) {
		this.displayDays = displayDays;
		this.allowedSubscriptionFrequencies = allowedSubscriptionFrequencies;
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public String getReportDateFormatted(Date reportDate) {
		return DateUtils.fromDateShort(reportDate);
	}


	// Returns the Report Date to use in the File Name - i.e. YYYY.MM, or YYYY.MM.dd
	public String getReportDateFileFormatted(Date reportDate) {
		return DateUtils.fromDate(reportDate, "yyyy.MM.dd");
	}


	public Date getReportDateForDate(Date date) {
		return date;
	}


	public Date getDefaultDisplayEndDate(Date reportDate) {
		if (getDisplayDays() == null) {
			return null;
		}
		return DateUtils.addDays(reportDate, getDisplayDays());
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public Integer getDisplayDays() {
		return this.displayDays;
	}


	public Set<PortalNotificationSubscriptionFrequencies> getAllowedSubscriptionFrequencies() {
		return this.allowedSubscriptionFrequencies;
	}
}
