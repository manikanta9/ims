package com.clifton.portal.file;

import com.clifton.core.beans.IdentityObject;


/**
 * The <code>PortalFileAware</code> interface is used to note classes that are aware of portal files (and whether or not those files have at least one APPROVED file posted to the portal)
 *
 * @author manderson
 */
public interface PortalFileAware extends IdentityObject {


	public void setPostedToPortal(boolean postedToPortal);


	public boolean isPostedToPortal();
}
