package com.clifton.portal.file.setup.mapping;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.compare.CompareUtils;

import java.util.Date;


/**
 * The <code>PortalFileCategoryPortalEntityMapping</code> tracks for an entity (null for All or Global) and category if files exist (i.e. this object exists) and if at least one file is approved
 * This is used to determine which entities and which categories have files that are available
 * <p>
 * This is a table, so we can easily filter results in various searches (i.e. exclude entities without files posted), categories with files posted for entity etc. at the database level
 *
 * @author manderson
 */
public class PortalFileCategoryPortalEntityMapping extends BaseSimpleEntity<Integer> {

	/**
	 * Can be null for All Entity Files or Global (All vs. Global indicated by global flag)
	 */
	private Integer portalEntityId;
	private short portalFileCategoryId;

	/**
	 * Used for global files
	 */
	private boolean global;

	/**
	 * Used for global files that are limited to a specific view type
	 * i.e. Global only for Institutional users
	 */
	private Short portalEntityViewTypeId;

	/**
	 * If they have at least one approved file
	 */
	private boolean approved;


	/**
	 * Approval date of first approved file
	 * used to determine when a new file category became available
	 */
	private Date firstApprovalDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategoryPortalEntityMapping() {
		super();
	}


	public PortalFileCategoryPortalEntityMapping(Integer portalEntityId, boolean global, Short portalEntityViewTypeId, short portalFileCategoryId, boolean approved, Date firstApprovalDate) {
		this();
		this.portalEntityId = portalEntityId;
		this.global = global;
		this.portalEntityViewTypeId = portalEntityViewTypeId;
		this.portalFileCategoryId = portalFileCategoryId;
		this.approved = approved;
		// First Approval date should only apply if it's approved
		if (approved) {
			this.firstApprovalDate = firstApprovalDate;
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	public static PortalFileCategoryPortalEntityMapping ofEntityMapping(Integer portalEntityId, short portalFileCategoryId, boolean approved, Date firstApprovalDate) {
		return new PortalFileCategoryPortalEntityMapping(portalEntityId, false, null, portalFileCategoryId, approved, firstApprovalDate);
	}


	public static PortalFileCategoryPortalEntityMapping ofAllMapping(short portalFileCategoryId, boolean approved, Date firstApprovalDate) {
		return new PortalFileCategoryPortalEntityMapping(null, false, null, portalFileCategoryId, approved, firstApprovalDate);
	}


	public static PortalFileCategoryPortalEntityMapping ofGlobalMapping(Short portalEntityViewTypeId, short portalFileCategoryId, boolean approved, Date firstApprovalDate) {
		return new PortalFileCategoryPortalEntityMapping(null, true, portalEntityViewTypeId, portalFileCategoryId, approved, firstApprovalDate);
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PortalFileCategoryPortalEntityMapping)) {
			return false;
		}
		PortalFileCategoryPortalEntityMapping mappingObject = (PortalFileCategoryPortalEntityMapping) obj;
		if (CompareUtils.isEqual(getPortalEntityId(), mappingObject.getPortalEntityId())) {
			if (CompareUtils.isEqual(getPortalFileCategoryId(), mappingObject.getPortalFileCategoryId())) {
				return true;
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}


	public short getPortalFileCategoryId() {
		return this.portalFileCategoryId;
	}


	public void setPortalFileCategoryId(short portalFileCategoryId) {
		this.portalFileCategoryId = portalFileCategoryId;
	}


	public boolean isGlobal() {
		return this.global;
	}


	public void setGlobal(boolean global) {
		this.global = global;
	}


	public Short getPortalEntityViewTypeId() {
		return this.portalEntityViewTypeId;
	}


	public void setPortalEntityViewTypeId(Short portalEntityViewTypeId) {
		this.portalEntityViewTypeId = portalEntityViewTypeId;
	}


	public boolean isApproved() {
		return this.approved;
	}


	public void setApproved(boolean approved) {
		this.approved = approved;
	}


	public Date getFirstApprovalDate() {
		return this.firstApprovalDate;
	}


	public void setFirstApprovalDate(Date firstApprovalDate) {
		this.firstApprovalDate = firstApprovalDate;
	}
}
