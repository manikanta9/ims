package com.clifton.portal.file.template;

/**
 * The <code>PortalFileTemplate</code> are the properties for the file that can be re-generated based on the category templates
 * <p>
 * This can be used to generate the new properties values and compare if necessary to perform an update on the Portal file
 *
 * @author manderson
 */
public class PortalFileTemplate {

	private String displayName;

	private String displayText;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getDisplayText() {
		return this.displayText;
	}


	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
}
