package com.clifton.portal.file.setup.security;

import com.clifton.portal.security.authorization.PortalSecureMethod;


/**
 * The <code>PortalFileSecurityResourceExpandedHandler</code> handles the rebuild of the PortalFileSecurityResourceExpanded table
 * This is triggered to any update to a PortalFileCategory which is rare or can be rebuild on demand
 *
 * @author manderson
 */
public interface PortalFileCategorySecurityResourceExpandedRebuildService {


	@PortalSecureMethod(portalAdminSecurity = true)
	public void rebuildPortalFileCategorySecurityResourceExpanded();
}
