package com.clifton.portal.file;

import com.clifton.core.util.validation.ValidationException;


/**
 * @author manderson
 */
public class PortalFileDuplicateException extends ValidationException {

	private final boolean replaceAllowed;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * This constructor is necessary in order to properly deserialize the exception with Jackson when it is thrown by an external service call.
	 */
	public PortalFileDuplicateException(String message) {
		super(message);
		this.replaceAllowed = false;
	}


	public PortalFileDuplicateException(String message, boolean replaceAllowed) {
		super(message);
		this.replaceAllowed = replaceAllowed;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isReplaceAllowed() {
		return this.replaceAllowed;
	}
}
