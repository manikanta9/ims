package com.clifton.portal.file.setup;

/**
 * @author manderson
 */
public enum PortalFileCategoryTypes {

	CATEGORY(null), // Grouping of category subsets. Provided across the top of the client portal as the main click-able pages of the portal.
	CATEGORY_SUBSET(CATEGORY), // Grouping of category files. Provided on the portal once clicked into a category page.
	FILE_GROUP(CATEGORY_SUBSET, true);  // Documents provided to the client, specific to a category subset. Security is set at this specific level


	private final PortalFileCategoryTypes parentCategoryType;

	private final boolean securityResourceRequired;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalFileCategoryTypes(PortalFileCategoryTypes parentCategoryType) {
		this(parentCategoryType, false);
	}


	PortalFileCategoryTypes(PortalFileCategoryTypes parentCategoryType, boolean securityResourceRequired) {
		this.parentCategoryType = parentCategoryType;
		this.securityResourceRequired = securityResourceRequired;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategoryTypes getParentCategoryType() {
		return this.parentCategoryType;
	}


	public boolean isSecurityResourceRequired() {
		return this.securityResourceRequired;
	}
}
