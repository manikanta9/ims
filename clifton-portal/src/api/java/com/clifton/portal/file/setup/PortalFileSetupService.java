package com.clifton.portal.file.setup;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalFileSetupService {


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Category Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalFileCategory getPortalFileCategory(short id);


	@PortalSecureMethod
	public PortalFileCategory getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes categoryType, String categoryName);


	@PortalSecureMethod
	public List<PortalFileCategory> getPortalFileCategoryList(PortalFileCategorySearchForm searchForm);


	/**
	 * Used for upload feature where the category list is limited to those categories under category type PortalFileCategoryTypes.FILE_GROUP
	 * and only those the user has access to post files to
	 * Uses PatternSearchForm because the full list is cached, and then the user can optionally limit based on name
	 *
	 * @param categoryName     - Can optionally be used to limit the list to those under a main tab.  For example, contract posting in IMS will limit to Documentation tab
	 * @param postEntityTypeId - Can optionally be used to limit the type of entity the category is for.  Currently used for moving files between categories to ensure the move applies to the same level of entity
	 * @param includeParents   - Can optionally be used for cases where the user can select a parent category (i.e. Invoices, and see all the file groups under that selection)
	 */
	@PortalSecureMethod
	public List<PortalFileCategory> getPortalFileCategoryForUploadList(PortalFileCategorySearchForm searchForm, String categoryName, Short postEntityTypeId, boolean includeParents);



	@DoNotAddRequestMapping
	public Short[] getPortalFileGroupIdsForPortalFileCategory(short portalFileCategoryId);


	/**
	 * Custom method that returns an array of the category ids that includes and falls under the root parent "Contact Us"
	 * This is a specialized method that stores the result in the cache for quick retrievals as it is used frequently when filtering
	 * portal entities that have files posted (which we want to exclude Contact Us) categories for
	 */
	@DoNotAddRequestMapping
	public Short[] getPortalFileCategoryIdsForContactUsCategories();


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalFileCategory savePortalFileCategory(PortalFileCategory bean);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalFileCategory(short id);
}
