package com.clifton.portal.file.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileFrequencies;

import java.util.Date;


/**
 * A portal file import is a file from the input directory that is available for automatic uploading.
 * <p>
 * We can attempt to match the report date and post entity based on the file name, otherwise users can manually input this data and import
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortalFileUploadPending {

	/**
	 * Used when overriding the report date from the bulk screen instead of supplying it in the file name as is
	 * We append _REPORT_DATE_yyyy-MM-dd to the file name so we can pull it out for property mapping
	 */
	public static final String REPORT_DATE_OVERRIDE_PREFIX = "_REPORT_DATE_";
	public static final String REPORT_DATE_OVERRIDE_EXPRESSION = "(" + REPORT_DATE_OVERRIDE_PREFIX + "[\\d\\d\\d\\d-\\d\\d-\\d\\d]+)";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// File Properties
	private String filePath;
	private String fileName;
	private Long fileSize;
	private Date modifiedDate;


	// Mapping Properties - where the file goes and what entity it will post to
	private PortalFileFrequencies fileFrequency;
	private Date reportDate;
	private Date publishDate;
	private PortalFileCategory fileCategory;
	private PortalEntity postPortalEntity;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * When we override the report date from the screen we append it to the file name
	 * Remove it for the user so they don't see that part of the file name, we only need it for mapping properties
	 */
	public String getFileNameFriendly() {
		return StringUtils.removeAll(getFileName(), REPORT_DATE_OVERRIDE_EXPRESSION);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFilePath() {
		return this.filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}


	public Date getModifiedDate() {
		return this.modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public Date getReportDate() {
		return this.reportDate;
	}


	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}


	public PortalFileCategory getFileCategory() {
		return this.fileCategory;
	}


	public void setFileCategory(PortalFileCategory fileCategory) {
		this.fileCategory = fileCategory;
	}


	public PortalEntity getPostPortalEntity() {
		return this.postPortalEntity;
	}


	public void setPostPortalEntity(PortalEntity postPortalEntity) {
		this.postPortalEntity = postPortalEntity;
	}


	public Date getPublishDate() {
		return this.publishDate;
	}


	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}


	public PortalFileFrequencies getFileFrequency() {
		return this.fileFrequency;
	}


	public void setFileFrequency(PortalFileFrequencies fileFrequency) {
		this.fileFrequency = fileFrequency;
	}
}
