package com.clifton.portal.file.setup;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionOptions;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;

import java.io.File;
import java.util.Collections;


/**
 * The <code>PortalFileCategory</code> drives the menu items, layout and sections
 * <p>
 * It supports a maximum of three levels deep where the top level is the main tab/menu item
 * and the second level is the section of that page, and the third level is specific categories that {@link com.clifton.portal.file.PortalFile} is posted to
 *
 * @author manderson
 */
@PortalTrackingAuditAware
public class PortalFileCategory extends NamedHierarchicalEntity<PortalFileCategory, Short> implements LabeledObject {


	public static final String CATEGORY_NAME_CONTACT_US = "Contact Us";
	public static final String CATEGORY_NAME_RELATIONSHIP_MANAGER = "Relationship Manager";
	public static final String CATEGORY_NAME_MEET_YOUR_TEAM = "Meet Your Team";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Security associated with the category
	 */
	private PortalSecurityResource securityResource;

	private PortalFileCategoryTypes fileCategoryType;

	/**
	 * The Freemarker syntax to generate the display name for the individual file
	 * If blank, the user is required to enter the display name manually which defaults to the category name
	 * Can contain source information (like for contacts), "file" is passed in the context
	 */
	private String displayNameTemplate;

	/**
	 * The Freemarker syntax to generate the file name for the individual file when downloaded
	 * If blank, the display name will be used
	 */
	private String fileNameTemplate;

	/**
	 * The Freemarker syntax to generate the display text for the individual file
	 * Can contain source information (like for contacts), "file" is passed in the context
	 */
	private String displayTextTemplate;

	/**
	 * If defined the converter for the fileTemplate will be passed the "sourceEntity" in the context
	 * and expects entities of that type because of specific field values
	 * i.e. the Contact
	 * and each "fieldValue" for the sourceEntity will be added with the bean key as the field type name (replacing spaces with _)
	 * i.e. "Phone Number" would be Phone_Number
	 * <p>
	 * If not defined, then none or any source entity can be selected
	 */
	private PortalEntityType sourceEntityType;

	/**
	 * When defined, files of this type are posted to entities that fall under this type.
	 * i.e. Client Relationships, Clients, Client Accounts
	 * This helps with manual posting of files to drive how the user should select what they are posting too
	 * as well as finding entities missing files in specific categories.
	 */
	private PortalEntityType postEntityType;


	private PortalNotificationDefinition notificationDefinition;

	/**
	 * If PortalNotificationDefinition is set and it's definition type allows subscriptions then this value will control the default value or required value.
	 */
	private PortalNotificationSubscriptionOptions notificationSubscriptionOption;

	/**
	 * Default frequency for files in this category.
	 */
	private PortalFileFrequencies portalFileFrequency;

	private String folderName;

	private Short categoryOrder;

	/**
	 * If true, then the name of the category cannot be changed
	 */
	private boolean systemDefined;

	/**
	 * Alias email address utilized for contacting specific internal departments or groups.
	 */
	private String categoryEmailAddress;


	/**
	 * If false, will automatically set the file to approved when it's posted (inserted) into this table
	 * Note: Even if files don't require the explicit approval, they can still be "un-approved" manually
	 */
	private boolean approvalRequired;

	/**
	 * Calendar days
	 * By Default, these will be blank and determined by the file frequency, however
	 * some cases they will want to change this to be less or more depending on when those specific file types are usually posted by
	 */
	private Short defaultDaysToDisplay;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getMaxDepth() {
		return 3;
	}


	public String getFolderNameExpanded() {
		String parentFolderName = "";
		if (getParent() != null) {
			parentFolderName = getParent().getFolderNameExpanded();
		}
		if (StringUtils.isEmpty(getFolderName())) {
			return parentFolderName;
		}
		return parentFolderName + getFolderName() + File.separator;
	}


	public String getLabelWithLevels() {
		return String.join("", Collections.nCopies(getLevel() - 1, "&nbsp;&nbsp;&nbsp;")) + getLabel();
	}


	public String getCoalesceCategoryEmailAddress() {
		if (!StringUtils.isEmpty(getCategoryEmailAddress())) {
			return getCategoryEmailAddress();
		}
		if (getParent() != null) {
			return getParent().getCoalesceCategoryEmailAddress();
		}
		return null;
	}


	public boolean isNotificationUsed() {
		return getNotificationDefinition() != null;
	}


	public boolean isNotificationSubscriptionRequired() {
		if (getNotificationSubscriptionOption() != null) {
			return getNotificationSubscriptionOption().isRequired();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(PortalSecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public PortalFileCategoryTypes getFileCategoryType() {
		return this.fileCategoryType;
	}


	public void setFileCategoryType(PortalFileCategoryTypes fileCategoryType) {
		this.fileCategoryType = fileCategoryType;
	}


	public Short getCategoryOrder() {
		return this.categoryOrder;
	}


	public void setCategoryOrder(Short categoryOrder) {
		this.categoryOrder = categoryOrder;
	}


	public String getCategoryEmailAddress() {
		return this.categoryEmailAddress;
	}


	public void setCategoryEmailAddress(String categoryEmailAddress) {
		this.categoryEmailAddress = categoryEmailAddress;
	}


	public boolean isApprovalRequired() {
		return this.approvalRequired;
	}


	public void setApprovalRequired(boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}


	public String getFolderName() {
		return this.folderName;
	}


	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public PortalFileFrequencies getPortalFileFrequency() {
		return this.portalFileFrequency;
	}


	public void setPortalFileFrequency(PortalFileFrequencies portalFileFrequency) {
		this.portalFileFrequency = portalFileFrequency;
	}


	public String getDisplayNameTemplate() {
		return this.displayNameTemplate;
	}


	public void setDisplayNameTemplate(String displayNameTemplate) {
		this.displayNameTemplate = displayNameTemplate;
	}


	public String getDisplayTextTemplate() {
		return this.displayTextTemplate;
	}


	public void setDisplayTextTemplate(String displayTextTemplate) {
		this.displayTextTemplate = displayTextTemplate;
	}


	public PortalEntityType getSourceEntityType() {
		return this.sourceEntityType;
	}


	public void setSourceEntityType(PortalEntityType sourceEntityType) {
		this.sourceEntityType = sourceEntityType;
	}


	public PortalEntityType getPostEntityType() {
		return this.postEntityType;
	}


	public void setPostEntityType(PortalEntityType postEntityType) {
		this.postEntityType = postEntityType;
	}


	public Short getDefaultDaysToDisplay() {
		return this.defaultDaysToDisplay;
	}


	public void setDefaultDaysToDisplay(Short defaultDaysToDisplay) {
		this.defaultDaysToDisplay = defaultDaysToDisplay;
	}


	public String getFileNameTemplate() {
		return this.fileNameTemplate;
	}


	public void setFileNameTemplate(String fileNameTemplate) {
		this.fileNameTemplate = fileNameTemplate;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public PortalNotificationDefinition getNotificationDefinition() {
		return this.notificationDefinition;
	}


	public void setNotificationDefinition(PortalNotificationDefinition notificationDefinition) {
		this.notificationDefinition = notificationDefinition;
	}


	public PortalNotificationSubscriptionOptions getNotificationSubscriptionOption() {
		return this.notificationSubscriptionOption;
	}


	public void setNotificationSubscriptionOption(PortalNotificationSubscriptionOptions notificationSubscriptionOption) {
		this.notificationSubscriptionOption = notificationSubscriptionOption;
	}
}
