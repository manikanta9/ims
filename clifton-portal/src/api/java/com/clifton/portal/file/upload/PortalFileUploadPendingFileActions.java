package com.clifton.portal.file.upload;

/**
 * The <code>PortalFileUploadPendingFileActions</code> is used for drag and drop feature to determine how to handle existing files - skip, replace, or error out
 * <p>
 * TODO REPLACE WITH FileDuplicateActions
 *
 * @author manderson
 */
public enum PortalFileUploadPendingFileActions {

	SKIP(false, false), // Leave Existing File, Skip New File
	REPLACE(true, false), // Replace Existing File
	ERROR(false, true); // Throw Error

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final boolean replaceFile;
	private final boolean error;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalFileUploadPendingFileActions(boolean replaceFile, boolean error) {
		this.replaceFile = replaceFile;
		this.error = error;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isReplaceFile() {
		return this.replaceFile;
	}


	public boolean isError() {
		return this.error;
	}
}
