package com.clifton.portal.file.setup.mapping;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author manderson
 */
public interface PortalFileCategoryPortalEntityMappingService {

	/**
	 * Schedules the rebuild to occur 30 seconds in the future,
	 * and will also trigger security updates if there is a change.
	 * <p>
	 * Called from the PortalFileObserver, as well as PortalFileService when PortalFileAssignments are directly updated
	 */
	@DoNotAddRequestMapping
	public void schedulePortalFileCategoryPortalEntityMappingListRebuild();


	/**
	 * Returns true if a change was saved
	 */
	@ResponseBody
	@ModelAttribute("data")
	@PortalSecureMethod(portalAdminSecurity = true)
	public boolean rebuildPortalFileCategoryPortalEntityMappingList();
}
