package com.clifton.portal.file.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.ArrayUtils;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalFileSearchForm extends BaseAuditableEntitySearchForm {

	// Explicitly the top level category name - used to load all files under a specific "tab"
	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent.parent", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String categoryName;

	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent", searchField = "parent.id")
	private Short categoryId;

	// Explicitly the 2nd level category name - used to load all files under a specific sub category
	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String categorySubsetName;

	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory", searchField = "parent.id")
	private Short categorySubsetId;

	// Explicitly the top level category name - used to load all files NOT IN a specific "tab" i.e. Contacts
	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent.parent", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeCategoryName;

	// Explicitly the top level category name - used to load all files NOT IN MULTIPLE specific "tab" i.e. Contacts and Help Documents
	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent.parent", searchField = "name", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeCategoryNames;

	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent", searchField = "parent.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeCategoryId;

	// Exclude subcategories
	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeCategorySubsetName;

	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory.parent", searchField = "name", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeCategorySubsetNames;

	// Note: Search will convert this to FILE_GROUP IDS for performance improvement
	@SearchField(searchFieldPath = "fileCategory", searchField = "parent.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeCategorySubsetId;

	// Explicitly the file group level category name - used for unique cases like Relationship Manager
	@SearchField(searchFieldPath = "fileCategory", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	// Note: Search will convert this to ID value for performance improvement
	private String fileGroupName;

	// Note: Converts to fileGroupId or fileGroupIds
	@SearchField(searchField = "fileCategory.id,fileCategory.parent.id,fileCategory.parent.parent.id", comparisonConditions = ComparisonConditions.EQUALS, leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short fileCategoryIdExpanded;

	@SearchField(searchField = "fileCategory.id")
	private Short fileGroupId;

	@SearchField(searchField = "fileCategory.id")
	private Short[] fileGroupIds;

	@SearchField(searchField = "fileCategory.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeFileGroupIds;

	@SearchField(searchField = "reportDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL)
	private Date minReportDate;

	@SearchField(searchField = "reportDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL)
	private Date maxReportDate;

	@SearchField(searchField = "reportDate")
	private Date reportDate;

	@SearchField
	private String displayName;

	@SearchField(searchField = "displayName", comparisonConditions = ComparisonConditions.EQUALS)
	private String displayNameEquals;

	@SearchField
	private String displayText;

	// Custom Search Field - Works similar to Active where current date is between Report Date and End Date on the File
	private Boolean defaultDisplay;

	@SearchField(searchField = "publishDate")
	private Date publishDate;

	@SearchField(searchField = "postPortalEntity.id")
	private Integer postPortalEntityId;

	@SearchField(searchField = "postPortalEntity.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean postPortalEntityIdPopulated;

	@SearchField(searchFieldPath = "postPortalEntity", searchField = "name,label,description")
	private String postPortalEntityLabel;

	// Used for sorting
	@SearchField(searchField = "fileCategory.parent.parent.categoryOrder,fileCategory.parent.categoryOrder,fileCategory.categoryOrder", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_PADDING)
	private Short fileCategoryOrderExpanded;

	@SearchField(searchField = "fileCategory.categoryOrder", leftJoin = true)
	private Short fileCategoryOrder;

	@SearchField(searchFieldPath = "fileCategory", searchField = "notificationDefinition.id")
	private Short notificationDefinitionId;

	@SearchField(searchFieldPath = "fileCategory", searchField = "notificationDefinition.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean notificationUsed;

	@SearchField(searchFieldPath = "fileCategory", searchField = "securityResource.id", sortField = "securityResource.name")
	private Short securityResourceId;

	@SearchField
	private Boolean approved;

	@SearchField
	private Short approvedByUserId;

	@SearchField(dateFieldIncludesTime = true)
	private Date approvalDate;

	@SearchField(searchField = "entitySourceSection.id")
	private Integer entitySourceSectionId;

	@SearchField(searchField = "entitySourceSection.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean entitySourceSectionIdPopulated;

	@SearchField(searchFieldPath = "entitySourceSection.sourceSystem", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String entitySourceSystemNameEquals;


	@SearchField(searchFieldPath = "entitySourceSection", searchField = "sourceSystemTableName", comparisonConditions = ComparisonConditions.EQUALS)
	private String entitySourceTableNameEquals;

	@SearchField
	private Integer sourceFkFieldId;

	@SearchField(searchField = "sourceFkFieldId")
	private Integer[] sourceFkFieldIds;

	@SearchField(searchField = "sourceFkFieldId", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean sourceFkFieldIdPopulated;

	@SearchField(searchField = "fileCreateUserId,createUserId", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Short coalesceFileCreateUserId;

	@SearchField(searchField = "fileNameAndPath", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean fileNameAndPathPopulated;

	@SearchField(searchField = "fileNameAndPath", comparisonConditions = ComparisonConditions.ENDS_WITH)
	private String fileType;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public void addToFileGroupIds(Short[] addFileGroupIds) {
		setFileGroupIds(ArrayUtils.addAll(getFileGroupIds(), addFileGroupIds));
	}


	public void addToExcludeFileGroupIds(Short[] addExcludeFileGroupIds) {
		setExcludeFileGroupIds(ArrayUtils.addAll(getExcludeFileGroupIds(), addExcludeFileGroupIds));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public String getCategorySubsetName() {
		return this.categorySubsetName;
	}


	public void setCategorySubsetName(String categorySubsetName) {
		this.categorySubsetName = categorySubsetName;
	}


	public Short getCategorySubsetId() {
		return this.categorySubsetId;
	}


	public void setCategorySubsetId(Short categorySubsetId) {
		this.categorySubsetId = categorySubsetId;
	}


	public String getExcludeCategoryName() {
		return this.excludeCategoryName;
	}


	public void setExcludeCategoryName(String excludeCategoryName) {
		this.excludeCategoryName = excludeCategoryName;
	}


	public String[] getExcludeCategoryNames() {
		return this.excludeCategoryNames;
	}


	public void setExcludeCategoryNames(String[] excludeCategoryNames) {
		this.excludeCategoryNames = excludeCategoryNames;
	}


	public Short getExcludeCategoryId() {
		return this.excludeCategoryId;
	}


	public void setExcludeCategoryId(Short excludeCategoryId) {
		this.excludeCategoryId = excludeCategoryId;
	}


	public String getExcludeCategorySubsetName() {
		return this.excludeCategorySubsetName;
	}


	public void setExcludeCategorySubsetName(String excludeCategorySubsetName) {
		this.excludeCategorySubsetName = excludeCategorySubsetName;
	}


	public String[] getExcludeCategorySubsetNames() {
		return this.excludeCategorySubsetNames;
	}


	public void setExcludeCategorySubsetNames(String[] excludeCategorySubsetNames) {
		this.excludeCategorySubsetNames = excludeCategorySubsetNames;
	}


	public Short getExcludeCategorySubsetId() {
		return this.excludeCategorySubsetId;
	}


	public void setExcludeCategorySubsetId(Short excludeCategorySubsetId) {
		this.excludeCategorySubsetId = excludeCategorySubsetId;
	}


	public String getFileGroupName() {
		return this.fileGroupName;
	}


	public void setFileGroupName(String fileGroupName) {
		this.fileGroupName = fileGroupName;
	}


	public Short getFileCategoryIdExpanded() {
		return this.fileCategoryIdExpanded;
	}


	public void setFileCategoryIdExpanded(Short fileCategoryIdExpanded) {
		this.fileCategoryIdExpanded = fileCategoryIdExpanded;
	}


	public Short getFileGroupId() {
		return this.fileGroupId;
	}


	public void setFileGroupId(Short fileGroupId) {
		this.fileGroupId = fileGroupId;
	}


	public Short[] getFileGroupIds() {
		return this.fileGroupIds;
	}


	public void setFileGroupIds(Short[] fileGroupIds) {
		this.fileGroupIds = fileGroupIds;
	}


	public Date getMinReportDate() {
		return this.minReportDate;
	}


	public void setMinReportDate(Date minReportDate) {
		this.minReportDate = minReportDate;
	}


	public Date getMaxReportDate() {
		return this.maxReportDate;
	}


	public void setMaxReportDate(Date maxReportDate) {
		this.maxReportDate = maxReportDate;
	}


	public Date getReportDate() {
		return this.reportDate;
	}


	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getDisplayNameEquals() {
		return this.displayNameEquals;
	}


	public void setDisplayNameEquals(String displayNameEquals) {
		this.displayNameEquals = displayNameEquals;
	}


	public String getDisplayText() {
		return this.displayText;
	}


	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}


	public Boolean getDefaultDisplay() {
		return this.defaultDisplay;
	}


	public void setDefaultDisplay(Boolean defaultDisplay) {
		this.defaultDisplay = defaultDisplay;
	}


	public Date getPublishDate() {
		return this.publishDate;
	}


	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}


	public Integer getPostPortalEntityId() {
		return this.postPortalEntityId;
	}


	public void setPostPortalEntityId(Integer postPortalEntityId) {
		this.postPortalEntityId = postPortalEntityId;
	}


	public Boolean getPostPortalEntityIdPopulated() {
		return this.postPortalEntityIdPopulated;
	}


	public void setPostPortalEntityIdPopulated(Boolean postPortalEntityIdPopulated) {
		this.postPortalEntityIdPopulated = postPortalEntityIdPopulated;
	}


	public String getPostPortalEntityLabel() {
		return this.postPortalEntityLabel;
	}


	public void setPostPortalEntityLabel(String postPortalEntityLabel) {
		this.postPortalEntityLabel = postPortalEntityLabel;
	}


	public Short getFileCategoryOrderExpanded() {
		return this.fileCategoryOrderExpanded;
	}


	public void setFileCategoryOrderExpanded(Short fileCategoryOrderExpanded) {
		this.fileCategoryOrderExpanded = fileCategoryOrderExpanded;
	}


	public Short getFileCategoryOrder() {
		return this.fileCategoryOrder;
	}


	public void setFileCategoryOrder(Short fileCategoryOrder) {
		this.fileCategoryOrder = fileCategoryOrder;
	}


	public Short getNotificationDefinitionId() {
		return this.notificationDefinitionId;
	}


	public void setNotificationDefinitionId(Short notificationDefinitionId) {
		this.notificationDefinitionId = notificationDefinitionId;
	}


	public Boolean getNotificationUsed() {
		return this.notificationUsed;
	}


	public void setNotificationUsed(Boolean notificationUsed) {
		this.notificationUsed = notificationUsed;
	}


	public Short getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Short securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public Boolean getApproved() {
		return this.approved;
	}


	public void setApproved(Boolean approved) {
		this.approved = approved;
	}


	public Short getApprovedByUserId() {
		return this.approvedByUserId;
	}


	public void setApprovedByUserId(Short approvedByUserId) {
		this.approvedByUserId = approvedByUserId;
	}


	public Date getApprovalDate() {
		return this.approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public Integer getEntitySourceSectionId() {
		return this.entitySourceSectionId;
	}


	public void setEntitySourceSectionId(Integer entitySourceSectionId) {
		this.entitySourceSectionId = entitySourceSectionId;
	}


	public Boolean getEntitySourceSectionIdPopulated() {
		return this.entitySourceSectionIdPopulated;
	}


	public void setEntitySourceSectionIdPopulated(Boolean entitySourceSectionIdPopulated) {
		this.entitySourceSectionIdPopulated = entitySourceSectionIdPopulated;
	}


	public String getEntitySourceSystemNameEquals() {
		return this.entitySourceSystemNameEquals;
	}


	public void setEntitySourceSystemNameEquals(String entitySourceSystemNameEquals) {
		this.entitySourceSystemNameEquals = entitySourceSystemNameEquals;
	}


	public String getEntitySourceTableNameEquals() {
		return this.entitySourceTableNameEquals;
	}


	public void setEntitySourceTableNameEquals(String entitySourceTableNameEquals) {
		this.entitySourceTableNameEquals = entitySourceTableNameEquals;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Integer[] getSourceFkFieldIds() {
		return this.sourceFkFieldIds;
	}


	public void setSourceFkFieldIds(Integer[] sourceFkFieldIds) {
		this.sourceFkFieldIds = sourceFkFieldIds;
	}


	public Boolean getSourceFkFieldIdPopulated() {
		return this.sourceFkFieldIdPopulated;
	}


	public void setSourceFkFieldIdPopulated(Boolean sourceFkFieldIdPopulated) {
		this.sourceFkFieldIdPopulated = sourceFkFieldIdPopulated;
	}


	public Short getCoalesceFileCreateUserId() {
		return this.coalesceFileCreateUserId;
	}


	public void setCoalesceFileCreateUserId(Short coalesceFileCreateUserId) {
		this.coalesceFileCreateUserId = coalesceFileCreateUserId;
	}


	public Boolean getFileNameAndPathPopulated() {
		return this.fileNameAndPathPopulated;
	}


	public void setFileNameAndPathPopulated(Boolean fileNameAndPathPopulated) {
		this.fileNameAndPathPopulated = fileNameAndPathPopulated;
	}


	public String getFileType() {
		return this.fileType;
	}


	public void setFileType(String fileType) {
		this.fileType = fileType;
	}


	public Short[] getExcludeFileGroupIds() {
		return this.excludeFileGroupIds;
	}


	public void setExcludeFileGroupIds(Short[] excludeFileGroupIds) {
		this.excludeFileGroupIds = excludeFileGroupIds;
	}
}
