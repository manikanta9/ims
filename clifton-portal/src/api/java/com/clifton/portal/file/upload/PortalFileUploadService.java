package com.clifton.portal.file.upload;

import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalFileUploadService {

	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Upload Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalFile uploadPortalFile(PortalFile portalFile, FileDuplicateActions fileDuplicateAction);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalFile uploadPortalFileAllowGlobal(PortalFile portalFile, FileDuplicateActions fileDuplicateAction);


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Portal File Upload Pending             ///////////////
	///////     File Share Feature: Read, Drag and Drop and Remove Files     ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of files found in the pending upload directory as a list of {@PortalFileUploadPending} objects
	 * with properties (category, report date, post entity, etc.) mapped if possible
	 * The list of returned files is limited to those with no category, or those categories that the current user has access to post to
	 */
	@PortalSecureMethod
	public List<PortalFileUploadPending> getPortalFileUploadPendingList(Short fileCategoryId);


	/**
	 * Used for individual errors to be able to replace the file if an error is found because the file already exists
	 */
	@PortalSecureMethod
	public void uploadPortalFileUploadPendingReplace(PortalFileUploadPendingCommand uploadPendingCommand);


	/**
	 * Drag and Drop feature from UI to save a file to the pending upload directory.  On saves, users can optionally
	 * select the category, which is used for mapping file properties, otherwise category is attempted to be determined based on the file name
	 */
	@PortalSecureMethod
	public void uploadPortalFileUploadPending(PortalFileUploadPendingCommand uploadPendingCommand);


	/**
	 * Remove a file from the pending upload directory
	 */
	@PortalSecureMethod
	public void deletePortalFileUploadPending(String filePath, String fileName);


	/**
	 * Save pending upload file as a real {@link PortalFile}
	 */
	@PortalSecureMethod
	public PortalFile savePortalFileUploadPending(PortalFileUploadPending bean, FileDuplicateActions fileDuplicateAction);
}
