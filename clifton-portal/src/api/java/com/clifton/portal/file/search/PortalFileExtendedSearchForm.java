package com.clifton.portal.file.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.portal.security.search.PortalEntitySpecificAwareSearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalFileExtendedSearchForm extends PortalFileSearchForm implements PortalEntitySpecificAwareSearchForm {

	// Custom Search Field
	private Integer assignedPortalEntityId;

	// Used for sorting
	@SearchField(searchFieldPath = "assignedPortalEntity", searchField = "name")
	private String assignedPortalEntityName;

	@SearchField(searchFieldPath = "assignedPortalEntity", searchField = "label")
	private String assignedPortalEntityLabel;

	// Custom search field that when true will filter out inactive assigned entities - i.e. Client has 5 accounts, 2 are terminated, no longer show those documents
	private Boolean activeAssignedPortalEntity;

	@SearchField
	private String serviceName;

	// Custom Search Fields - Currently Used for Notifications to find files with given source entity field values
	// i.e. find invoices with Status = "Presented for Payment"
	// Since we use extended search for notifications, these fields are only available here
	private String sourceFieldTypeName;
	private String sourceFieldValue;
	private Date sourceFieldValueUpdateDateMin; // Field Value Update Date >= given date
	private Date sourceFieldValueUpdateDateMax; // Field Value Update Date <= given date


	// Custom Search Field for Viewing As User
	private Short viewAsUserId;

	// Custom Search Field for Viewing As Entity
	private Integer viewAsEntityId;

	// Custom Search where PortalFileCategoryPortalEntityMapping.FirstApprovalDate = Approval Date
	private Boolean firstApprovedFileForEntity;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getAssignedPortalEntityId() {
		return this.assignedPortalEntityId;
	}


	public void setAssignedPortalEntityId(Integer assignedPortalEntityId) {
		this.assignedPortalEntityId = assignedPortalEntityId;
	}


	public String getAssignedPortalEntityName() {
		return this.assignedPortalEntityName;
	}


	public void setAssignedPortalEntityName(String assignedPortalEntityName) {
		this.assignedPortalEntityName = assignedPortalEntityName;
	}


	@Override
	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	@Override
	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	public String getServiceName() {
		return this.serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getSourceFieldTypeName() {
		return this.sourceFieldTypeName;
	}


	public void setSourceFieldTypeName(String sourceFieldTypeName) {
		this.sourceFieldTypeName = sourceFieldTypeName;
	}


	public String getSourceFieldValue() {
		return this.sourceFieldValue;
	}


	public void setSourceFieldValue(String sourceFieldValue) {
		this.sourceFieldValue = sourceFieldValue;
	}


	public Date getSourceFieldValueUpdateDateMin() {
		return this.sourceFieldValueUpdateDateMin;
	}


	public void setSourceFieldValueUpdateDateMin(Date sourceFieldValueUpdateDateMin) {
		this.sourceFieldValueUpdateDateMin = sourceFieldValueUpdateDateMin;
	}


	public Date getSourceFieldValueUpdateDateMax() {
		return this.sourceFieldValueUpdateDateMax;
	}


	public void setSourceFieldValueUpdateDateMax(Date sourceFieldValueUpdateDateMax) {
		this.sourceFieldValueUpdateDateMax = sourceFieldValueUpdateDateMax;
	}


	public Boolean getActiveAssignedPortalEntity() {
		return this.activeAssignedPortalEntity;
	}


	public void setActiveAssignedPortalEntity(Boolean activeAssignedPortalEntity) {
		this.activeAssignedPortalEntity = activeAssignedPortalEntity;
	}


	public String getAssignedPortalEntityLabel() {
		return this.assignedPortalEntityLabel;
	}


	public void setAssignedPortalEntityLabel(String assignedPortalEntityLabel) {
		this.assignedPortalEntityLabel = assignedPortalEntityLabel;
	}


	@Override
	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	@Override
	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}


	public Boolean getFirstApprovedFileForEntity() {
		return this.firstApprovedFileForEntity;
	}


	public void setFirstApprovedFileForEntity(Boolean firstApprovedFileForEntity) {
		this.firstApprovedFileForEntity = firstApprovedFileForEntity;
	}
}
