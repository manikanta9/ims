package com.clifton.portal.holiday;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface PortalHolidayService {


	@PortalSecureMethod
	public List<PortalHoliday> getPortalHolidayList(PortalHolidaySearchForm searchForm);


	@PortalSecureMethod(adminSecurity = true)
	public List<PortalHoliday> savePortalHolidayList(List<PortalHoliday> holidayList);


	@PortalSecureMethod(adminSecurity = true)
	public void deletePortalHolidayList(List<PortalHoliday> holidayList);


	////////////////////////////////////////////////////////////////////////////////
	/////////////             Business Day Helper Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if date parameter is the first business day as of the fromDate
	 * <p>
	 * A business day is any weekday (i.e. excludes all weekends) that isn't in the Portal Holiday table
	 */
	@DoNotAddRequestMapping
	public boolean isFirstBusinessDaySinceFromDate(Date fromDate, Date date);


	/**
	 * Used for daily files only to default the report date to the previous business day
	 *
	 * @param date - optional, if blank uses today
	 */
	@ResponseBody
	@PortalSecureMethod
	public Date getPortalHolidayPreviousBusinessDay(Date date);
}
