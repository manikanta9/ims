package com.clifton.portal.holiday;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;

import java.util.Date;


/**
 * The <code>PortalHoliday</code> class contains dates that are considered to be holidays
 * This data is currently only updated from a batch job in IMS that pushes the holiday dates from our default calendar
 *
 * @author manderson
 */
@PortalTrackingAuditAware(insert = false)
public class PortalHoliday extends BaseEntity<Integer> {


	private Date holidayDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getHolidayDate() {
		return this.holidayDate;
	}


	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}
}
