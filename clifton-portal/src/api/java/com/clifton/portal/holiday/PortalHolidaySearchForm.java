package com.clifton.portal.holiday;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalHolidaySearchForm extends BaseAuditableEntitySearchForm {


	@SearchField
	private Date holidayDate;


	@SearchField(searchField = "holidayDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date startDate;

	@SearchField(searchField = "holidayDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getHolidayDate() {
		return this.holidayDate;
	}


	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
