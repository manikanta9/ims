package com.clifton.portal.notification.setup;

import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.function.BiFunction;


/**
 * The <code>PortalNotificationRecurrences</code> defines how often a notification is sent to a user.
 * For example - NONE means no recurrence - user gets results as often as the notification runs
 * ONCE means one recurrence the user gets one notification, if it's a file posted notification, it would be once per user/per file
 * If it is DAYS_ONCE, the user would get the notification after XX days, but only once.  If it's DAYS, the user would get the notification after every XX days continually
 * <p>
 * If using DAYS or DAYS_ONCE and interval is 1 will use addWeekDays method, otherwise will use addDays method.
 *
 * @author manderson
 */
public enum PortalNotificationRecurrences {

	NONE(false, false, null),
	ONCE(false, true, null),
	DAYS_ONCE(true, true, (date, integer) -> (integer == 1) ? DateUtils.addWeekDays(date, integer) : DateUtils.addDays(date, integer)),
	DAYS(true, false, (date, integer) -> (integer == 1) ? DateUtils.addWeekDays(date, integer) : DateUtils.addDays(date, integer)),
	MONTHS_ONCE(true, true, DateUtils::addMonths),
	MONTHS(true, false, DateUtils::addMonths),
	YEARS_ONCE(true, true, DateUtils::addYears),
	YEARS(true, false, DateUtils::addYears);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private final boolean recurrenceIntervalSupported;
	private final boolean oneNotificationPerRecipient;

	private final BiFunction<Date, Integer, Date> recurrenceCalculationFunction;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalNotificationRecurrences(boolean recurrenceIntervalSupported, boolean oneNotificationPerRecipient, BiFunction<Date, Integer, Date> recurrenceCalculationFunction) {
		this.recurrenceIntervalSupported = recurrenceIntervalSupported;
		this.oneNotificationPerRecipient = oneNotificationPerRecipient;
		this.recurrenceCalculationFunction = recurrenceCalculationFunction;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getRecurrenceFromDate(Date date, Short interval, boolean previous) {
		if (this.recurrenceCalculationFunction == null) {
			return null;
		}
		return this.recurrenceCalculationFunction.apply(date, previous ? -interval : interval);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRecurrenceIntervalSupported() {
		return this.recurrenceIntervalSupported;
	}


	public boolean isOneNotificationPerRecipient() {
		return this.oneNotificationPerRecipient;
	}
}
