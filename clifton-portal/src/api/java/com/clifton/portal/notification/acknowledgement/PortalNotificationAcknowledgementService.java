package com.clifton.portal.notification.acknowledgement;

import com.clifton.portal.security.authorization.PortalSecureMethod;


/**
 * The <code>PortalNotificationAcknowledgementService</code> is used to acknowledge pop up notifications
 * Acknowledgements are tracked via {@link com.clifton.portal.tracking.PortalTrackingEvent} table
 *
 * @author manderson
 */
public interface PortalNotificationAcknowledgementService {


	/**
	 * The acknowledgement can be true, false, or NULL as we track Positive, Negative, and "no" responses
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void savePortalNotificationAcknowledgement(int portalNotificationId, Boolean acknowledgement);


	/**
	 * Admin save - does not validate the acknowledgement type is allowed
	 * This allows clients only to accept, but internally we can still track declines and get the notification out of the user's bucket
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void savePortalNotificationAcknowledgementAdmin(int portalNotificationId, Boolean acknowledgement);
}
