package com.clifton.portal.notification;

import com.clifton.core.beans.ManyToManySimpleEntity;
import com.clifton.portal.file.PortalFile;


/**
 * The <code>PortalNotificationPortalFile</code> is used to associate a notification with a particular list of files (if applies)
 * This allows use to know if a user was sent a notification or not to confirm 1. what file notifications are being sent out as
 * well as 2. to prevent sending the same file notification twice.
 *
 * @author manderson
 */
public class PortalNotificationPortalFile extends ManyToManySimpleEntity<PortalNotification, PortalFile, Integer> {

}
