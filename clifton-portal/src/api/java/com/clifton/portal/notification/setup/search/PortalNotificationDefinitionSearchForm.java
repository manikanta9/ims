package com.clifton.portal.notification.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalNotificationDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,emailSubjectTemplate,emailBodyTemplate")
	private String searchPattern;

	@SearchField(searchField = "definitionType.id")
	private Short definitionTypeId;

	@SearchField(searchFieldPath = "definitionType", searchField = "triggeredByEvent")
	private Boolean triggeredByEvent;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean active;

	@SearchField
	private Boolean popUpNotification;

	@SearchField(searchField = "acknowledgementType.id")
	private Short acknowledgementTypeId;

	@SearchField(searchFieldPath = "definitionType", searchField = "subscriptionAllowed")
	private Boolean subscriptionAllowed;

	@SearchField(searchFieldPath = "definitionType", searchField = "subscriptionAllowed")
	private Boolean subscriptionForFileCategory;

	@SearchField(searchFieldPath = "definitionType", searchField = "fileNotification")
	private Boolean fileNotification;

	@SearchField(searchFieldPath = "definitionType", searchField = "internalNotification")
	private Boolean internalNotification;

	@SearchField
	private Short fileApprovedDaysBack;

	@SearchField
	private String emailSubjectTemplate;

	@SearchField
	private String emailBodyTemplate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Boolean getSubscriptionAllowed() {
		return this.subscriptionAllowed;
	}


	public void setSubscriptionAllowed(Boolean subscriptionAllowed) {
		this.subscriptionAllowed = subscriptionAllowed;
	}


	public Boolean getSubscriptionForFileCategory() {
		return this.subscriptionForFileCategory;
	}


	public void setSubscriptionForFileCategory(Boolean subscriptionForFileCategory) {
		this.subscriptionForFileCategory = subscriptionForFileCategory;
	}


	public String getEmailSubjectTemplate() {
		return this.emailSubjectTemplate;
	}


	public void setEmailSubjectTemplate(String emailSubjectTemplate) {
		this.emailSubjectTemplate = emailSubjectTemplate;
	}


	public String getEmailBodyTemplate() {
		return this.emailBodyTemplate;
	}


	public void setEmailBodyTemplate(String emailBodyTemplate) {
		this.emailBodyTemplate = emailBodyTemplate;
	}


	public Short getDefinitionTypeId() {
		return this.definitionTypeId;
	}


	public void setDefinitionTypeId(Short definitionTypeId) {
		this.definitionTypeId = definitionTypeId;
	}


	public Short getFileApprovedDaysBack() {
		return this.fileApprovedDaysBack;
	}


	public void setFileApprovedDaysBack(Short fileApprovedDaysBack) {
		this.fileApprovedDaysBack = fileApprovedDaysBack;
	}


	public Boolean getTriggeredByEvent() {
		return this.triggeredByEvent;
	}


	public void setTriggeredByEvent(Boolean triggeredByEvent) {
		this.triggeredByEvent = triggeredByEvent;
	}


	public Boolean getFileNotification() {
		return this.fileNotification;
	}


	public void setFileNotification(Boolean fileNotification) {
		this.fileNotification = fileNotification;
	}


	public Boolean getInternalNotification() {
		return this.internalNotification;
	}


	public void setInternalNotification(Boolean internalNotification) {
		this.internalNotification = internalNotification;
	}


	public Boolean getPopUpNotification() {
		return this.popUpNotification;
	}


	public void setPopUpNotification(Boolean popUpNotification) {
		this.popUpNotification = popUpNotification;
	}


	public Short getAcknowledgementTypeId() {
		return this.acknowledgementTypeId;
	}


	public void setAcknowledgementTypeId(Short acknowledgementTypeId) {
		this.acknowledgementTypeId = acknowledgementTypeId;
	}
}
