package com.clifton.portal.notification;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalNotificationService {


	////////////////////////////////////////////////////////////////////////////////
	///////////               Portal Notification Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalNotification getPortalNotification(int id);


	@DoNotAddRequestMapping
	public PortalNotification getPortalNotificationLastForUser(short notificationDefinitionTypeId, short securityUserId, Integer portalFileId);


	@PortalSecureMethod
	public List<PortalNotification> getPortalNotificationList(PortalNotificationSearchForm searchForm);


	@DoNotAddRequestMapping
	public PortalNotification savePortalNotification(PortalNotification portalNotification);


	@DoNotAddRequestMapping
	public PortalNotification savePortalNotificationAcknowledged(PortalNotification portalNotification);


	////////////////////////////////////////////////////////////////////////////////
	/////////          Portal Notification Portal File Methods            //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalNotificationPortalFile> getPortalNotificationPortalFileListForNotification(int portalNotificationId);


	/**
	 * Called prior to deleting a portal file - removes references in notifications for that file so it can be properly deleted
	 */
	@DoNotAddRequestMapping
	public void deletePortalNotificationPortalFileListForFile(int portalFileId);
}
