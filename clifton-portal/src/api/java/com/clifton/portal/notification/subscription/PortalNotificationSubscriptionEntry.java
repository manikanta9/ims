package com.clifton.portal.notification.subscription;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.List;


/**
 * The <code>PortalNotificationSubscriptionEntry</code> is a virtual dto class that is used to bulk enter user notification subscriptions
 * for a particular user and entity
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortalNotificationSubscriptionEntry {

	private PortalSecurityUser securityUser;

	private PortalEntity portalEntity;

	private List<PortalNotificationSubscription> subscriptionList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(PortalSecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public PortalEntity getPortalEntity() {
		return this.portalEntity;
	}


	public void setPortalEntity(PortalEntity portalEntity) {
		this.portalEntity = portalEntity;
	}


	public List<PortalNotificationSubscription> getSubscriptionList() {
		return this.subscriptionList;
	}


	public void setSubscriptionList(List<PortalNotificationSubscription> subscriptionList) {
		this.subscriptionList = subscriptionList;
	}
}
