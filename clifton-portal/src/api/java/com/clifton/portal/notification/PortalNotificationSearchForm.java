package com.clifton.portal.notification;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalNotificationSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "notificationDefinition.id")
	private Short notificationDefinitionId;

	@SearchField(searchFieldPath = "notificationDefinition", searchField = "definitionType.id")
	private Short notificationDefinitionTypeId;

	@SearchField(searchFieldPath = "notificationDefinition.definitionType", searchField = "internalNotification")
	private Boolean internalNotification;

	@SearchField(searchFieldPath = "notificationDefinition", searchField = "popUpNotification")
	private Boolean popUpNotification;

	@SearchField(searchField = "recipientUser.id")
	private Short recipientUserId;

	@SearchField(searchField = "recipientUser.id")
	private Short[] recipientUserIds;

	@SearchField(searchField = "recipientEmailAddress")
	private String recipientEmailAddress;

	@SearchField(searchField = "recipientEmailAddress,recipientUser.emailAddress,recipientUser.username,recipientUser.firstName,recipientUser.lastName", leftJoin = true)
	private String recipientSearchPattern;

	// Custom Search Field - where exists clause
	private Integer portalFileId;

	@SearchField
	private String subject;

	@SearchField
	private String text;

	@SearchField
	private Date sentDate;

	@SearchField
	private Boolean acknowledged;


	@SearchField
	private String errorMessage;

	@SearchField(searchField = "errorMessage", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean error;

	@SearchField(searchField = "parent", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean resend;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getNotificationDefinitionId() {
		return this.notificationDefinitionId;
	}


	public void setNotificationDefinitionId(Short notificationDefinitionId) {
		this.notificationDefinitionId = notificationDefinitionId;
	}


	public Short getNotificationDefinitionTypeId() {
		return this.notificationDefinitionTypeId;
	}


	public void setNotificationDefinitionTypeId(Short notificationDefinitionTypeId) {
		this.notificationDefinitionTypeId = notificationDefinitionTypeId;
	}


	public Short getRecipientUserId() {
		return this.recipientUserId;
	}


	public void setRecipientUserId(Short recipientUserId) {
		this.recipientUserId = recipientUserId;
	}


	public Short[] getRecipientUserIds() {
		return this.recipientUserIds;
	}


	public void setRecipientUserIds(Short[] recipientUserIds) {
		this.recipientUserIds = recipientUserIds;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getSentDate() {
		return this.sentDate;
	}


	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}


	public Integer getPortalFileId() {
		return this.portalFileId;
	}


	public void setPortalFileId(Integer portalFileId) {
		this.portalFileId = portalFileId;
	}


	public String getErrorMessage() {
		return this.errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public Boolean getInternalNotification() {
		return this.internalNotification;
	}


	public void setInternalNotification(Boolean internalNotification) {
		this.internalNotification = internalNotification;
	}


	public Boolean getError() {
		return this.error;
	}


	public void setError(Boolean error) {
		this.error = error;
	}


	public String getRecipientEmailAddress() {
		return this.recipientEmailAddress;
	}


	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}


	public String getRecipientSearchPattern() {
		return this.recipientSearchPattern;
	}


	public void setRecipientSearchPattern(String recipientSearchPattern) {
		this.recipientSearchPattern = recipientSearchPattern;
	}


	public Boolean getResend() {
		return this.resend;
	}


	public void setResend(Boolean resend) {
		this.resend = resend;
	}


	public Boolean getPopUpNotification() {
		return this.popUpNotification;
	}


	public void setPopUpNotification(Boolean popUpNotification) {
		this.popUpNotification = popUpNotification;
	}


	public Boolean getAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}
}
