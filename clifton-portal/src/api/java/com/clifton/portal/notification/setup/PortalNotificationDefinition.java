package com.clifton.portal.notification.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.Time;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The <code>PortalNotificationDefinition</code> defines for a specific type of notification, how it's run, how often the user receives the notification
 * and the email subject and text included.
 *
 * @author manderson
 */
@PortalTrackingAuditAware(insert = false)
public class PortalNotificationDefinition extends NamedEntity<Short> {

	/**
	 * The type of the notification definition
	 * i.e. file posted, invoice paid, user locked out
	 */
	private PortalNotificationDefinitionType definitionType;

	/**
	 * If true, then the notification is not an email that is sent out, but a pop up upon client successful sign on
	 */
	private boolean popUpNotification;


	/**
	 * If popUpNotification is true, an acknowledgement type is required.
	 */
	private PortalNotificationAcknowledgementType acknowledgementType;


	/**
	 * How often this notification sends information
	 * For example, a file posted notification has no recurrence because it is sent once per user/file
	 * Annual user reviews would be YEARS recurrence with recurrence value of 1
	 * There is also an option to send one after x time, but only once. So, after 60 days send once - but not every 60 days.
	 */
	private PortalNotificationRecurrences notificationRecurrence;

	/**
	 * i.e. = 1 with notificationRecurrence = YEARS for every one year
	 */
	private Short notificationRecurrenceInterval;

	/**
	 * If type.fileNotification = true, then if entered, this limit the files in the notification for approved date filter
	 * For example a user turns on and off notifications, we don't want to send them notifications for old files
	 * just recent ones.  In some cases this may be blank, i.e. overdue or paid invoice, because we don't care about when the file was approved just when the status changed
	 * which is determined by the recurrence
	 */
	private Short fileApprovedDaysBack;

	/**
	 * startTime and endTime are saved as integers see {@link Time}.
	 * If definition is active, these are the times it runs
	 */
	private Time startTime;

	private Time endTime;

	/**
	 * How often in minutes between scheduled runs
	 * Leave blank to only run once a day
	 */
	private Integer runFrequencyInMinutes;

	/**
	 * If the definition is active.  Only one active definition for each type that does not use "subscriptionForFileCategory"
	 */
	private boolean active;

	/**
	 * If true and a user is in the termination period (between termination date and end date for ALL of their entity assignments)
	 * Then we won't send them this notification.  For example, we don't want to ask them to review their contact information or user's security if they are terminating their relationship with us
	 * and their portal access is going away.
	 */
	private boolean skipPendingTermination;

	/**
	 * Freemarker template for the email subject of the notification
	 * Values available to the template are based on the type of the notification
	 */
	private String emailSubjectTemplate;

	/**
	 * Freemarker template for the email body of the notification
	 * Values available to the template are based on the type of the notification
	 */
	private String emailBodyTemplate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationDefinitionType getDefinitionType() {
		return this.definitionType;
	}


	public void setDefinitionType(PortalNotificationDefinitionType definitionType) {
		this.definitionType = definitionType;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public boolean isSkipPendingTermination() {
		return this.skipPendingTermination;
	}


	public void setSkipPendingTermination(boolean skipPendingTermination) {
		this.skipPendingTermination = skipPendingTermination;
	}


	public String getEmailSubjectTemplate() {
		return this.emailSubjectTemplate;
	}


	public void setEmailSubjectTemplate(String emailSubjectTemplate) {
		this.emailSubjectTemplate = emailSubjectTemplate;
	}


	public String getEmailBodyTemplate() {
		return this.emailBodyTemplate;
	}


	public void setEmailBodyTemplate(String emailBodyTemplate) {
		this.emailBodyTemplate = emailBodyTemplate;
	}


	public PortalNotificationRecurrences getNotificationRecurrence() {
		return this.notificationRecurrence;
	}


	public void setNotificationRecurrence(PortalNotificationRecurrences notificationRecurrence) {
		this.notificationRecurrence = notificationRecurrence;
	}


	public Short getNotificationRecurrenceInterval() {
		return this.notificationRecurrenceInterval;
	}


	public void setNotificationRecurrenceInterval(Short notificationRecurrenceInterval) {
		this.notificationRecurrenceInterval = notificationRecurrenceInterval;
	}


	public Time getStartTime() {
		return this.startTime;
	}


	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}


	public Time getEndTime() {
		return this.endTime;
	}


	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}


	public Integer getRunFrequencyInMinutes() {
		return this.runFrequencyInMinutes;
	}


	public void setRunFrequencyInMinutes(Integer runFrequencyInMinutes) {
		this.runFrequencyInMinutes = runFrequencyInMinutes;
	}


	public Short getFileApprovedDaysBack() {
		return this.fileApprovedDaysBack;
	}


	public void setFileApprovedDaysBack(Short fileApprovedDaysBack) {
		this.fileApprovedDaysBack = fileApprovedDaysBack;
	}


	public boolean isPopUpNotification() {
		return this.popUpNotification;
	}


	public void setPopUpNotification(boolean popUpNotification) {
		this.popUpNotification = popUpNotification;
	}


	public PortalNotificationAcknowledgementType getAcknowledgementType() {
		return this.acknowledgementType;
	}


	public void setAcknowledgementType(PortalNotificationAcknowledgementType acknowledgementType) {
		this.acknowledgementType = acknowledgementType;
	}
}
