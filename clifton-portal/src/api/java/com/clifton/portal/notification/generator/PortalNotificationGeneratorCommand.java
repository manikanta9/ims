package com.clifton.portal.notification.generator;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.notification.event.PortalNotificationEvent;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.Date;


/**
 * The <code>PortalNotificationGeneratorCommand</code> command is used to generate or preview notifications
 *
 * @author manderson
 */
@NonPersistentObject
public class PortalNotificationGeneratorCommand {


	/**
	 * One is Required - Definition to generate Notifications for
	 */
	private Short portalNotificationDefinitionId;
	private PortalNotificationDefinition portalNotificationDefinition;

	/**
	 * Usually blank, which uses "today" - no time
	 * Tests can set the date so we can mimic runs for specific dates to ensure retrieved data is correct
	 */
	private Date runDate;

	/*
	 * When generating notifications for a definition triggered by an event, this would be populated/required
	 * Otherwise, this isn't used
	 */
	private PortalNotificationEvent portalNotificationEvent;


	/**
	 * Used to determine if we are just previewing, sending a preview, really sending, etc.
	 */
	private PortalNotificationGeneratorCommandTypes commandType;


	/**
	 * Used for preview functionality to be able to edit and preview without saving changes.
	 */
	private String subjectTemplate;

	private String bodyTemplate;

	/**
	 * Optional user parameter. Added for test notifications
	 */
	private PortalSecurityUser recipientUser;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getRunDateWithTime() {
		Date date = this.runDate;
		if (date == null) {
			date = new Date();
		}
		return date;
	}


	public Date getRunDateWithoutTime() {
		return DateUtils.clearTime(getRunDateWithTime());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPortalNotificationDefinitionId() {
		return this.portalNotificationDefinitionId;
	}


	public void setPortalNotificationDefinitionId(Short portalNotificationDefinitionId) {
		this.portalNotificationDefinitionId = portalNotificationDefinitionId;
	}


	public PortalNotificationDefinition getPortalNotificationDefinition() {
		return this.portalNotificationDefinition;
	}


	public void setPortalNotificationDefinition(PortalNotificationDefinition portalNotificationDefinition) {
		this.portalNotificationDefinition = portalNotificationDefinition;
	}


	public PortalNotificationEvent getPortalNotificationEvent() {
		return this.portalNotificationEvent;
	}


	public void setPortalNotificationEvent(PortalNotificationEvent portalNotificationEvent) {
		this.portalNotificationEvent = portalNotificationEvent;
	}


	public PortalNotificationGeneratorCommandTypes getCommandType() {
		return this.commandType;
	}


	public void setCommandType(PortalNotificationGeneratorCommandTypes commandType) {
		this.commandType = commandType;
	}


	public String getSubjectTemplate() {
		return this.subjectTemplate;
	}


	public void setSubjectTemplate(String subjectTemplate) {
		this.subjectTemplate = subjectTemplate;
	}


	public String getBodyTemplate() {
		return this.bodyTemplate;
	}


	public void setBodyTemplate(String bodyTemplate) {
		this.bodyTemplate = bodyTemplate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public PortalSecurityUser getRecipientUser() {
		return this.recipientUser;
	}


	public void setRecipientUser(PortalSecurityUser recipientUser) {
		this.recipientUser = recipientUser;
	}
}
