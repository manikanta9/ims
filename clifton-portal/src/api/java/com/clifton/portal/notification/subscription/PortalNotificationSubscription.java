package com.clifton.portal.notification.subscription;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;


/**
 * The <code>PortalNotificationSubscription</code> defines a users subscription to a notification for a particular assignment.
 * Subscriptions can be applied for a specific definition, if allowed and not defined at the file category level, otherwise the subscription is
 * for a file category that uses a definition subscribed to at the file category level.
 *
 * @author manderson
 */
@PortalTrackingAuditAware
public class PortalNotificationSubscription extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * The user assignment this notification is for.  A user
	 * can have assignments to multiple relationships, and each notification can
	 * be specific per assignment.
	 */
	private PortalSecurityUserAssignment userAssignment;

	/**
	 * This could be the entity on the assignment or a child entity
	 * so users can have notifications at specific levels
	 */
	private PortalEntity portalEntity;

	/**
	 * The notification definition this subscription is for.  This is only populated
	 * for notification definitions that are not flagged as "subscriptionForFileCategory"
	 */
	private PortalNotificationDefinitionType notificationDefinitionType;


	/**
	 * The file category this this subscription is for.  This applies to any notification definition
	 * where "subscriptionForFileCategory" is true
	 */
	private PortalFileCategory fileCategory;

	/**
	 * Note: By Default notifications are off - users must chose to turn them on
	 */
	@PortalTrackingAuditColumnConfig(alwaysAuditDelete = true, alwaysAuditInsert = true)
	private boolean notificationEnabled;


	/**
	 * Optional frequency that can be used to limit the notifications sent.  If blank, user gets all
	 * For example: Daily Files posted, but user only receives notifications for files posted on the first business day of the week
	 */
	@PortalTrackingAuditColumnConfig(alwaysAuditDelete = true, alwaysAuditInsert = true)
	private PortalNotificationSubscriptionFrequencies subscriptionFrequency;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getUserAssignment() != null) {
			return getUserAssignment().getLabel() + " - " + getCoalesceDefinitionTypeFileCategoryName();
		}
		return getCoalesceDefinitionTypeFileCategoryName();
	}


	public String getCoalesceDefinitionTypeFileCategoryName() {
		if (getNotificationDefinitionType() != null) {
			return getNotificationDefinitionType().getName();
		}
		if (getFileCategory() != null) {
			return getFileCategory().getName();
		}
		return null;
	}


	public String getNotificationGroupingLabel() {
		if (getNotificationDefinitionType() != null) {
			return getNotificationDefinitionType().getName();
		}
		if (getFileCategory() != null) {
			return getFileCategory().getRootParent().getLabel();
		}
		return null;
	}


	public String getSubscriptionFrequencyLabel() {
		if (this.subscriptionFrequency != null) {
			return this.subscriptionFrequency.getLabel();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserAssignment getUserAssignment() {
		return this.userAssignment;
	}


	public void setUserAssignment(PortalSecurityUserAssignment userAssignment) {
		this.userAssignment = userAssignment;
	}


	public PortalNotificationDefinitionType getNotificationDefinitionType() {
		return this.notificationDefinitionType;
	}


	public void setNotificationDefinitionType(PortalNotificationDefinitionType notificationDefinitionType) {
		this.notificationDefinitionType = notificationDefinitionType;
	}


	public PortalFileCategory getFileCategory() {
		return this.fileCategory;
	}


	public void setFileCategory(PortalFileCategory fileCategory) {
		this.fileCategory = fileCategory;
	}


	public boolean isNotificationEnabled() {
		return this.notificationEnabled;
	}


	public void setNotificationEnabled(boolean notificationEnabled) {
		this.notificationEnabled = notificationEnabled;
	}


	public PortalEntity getPortalEntity() {
		return this.portalEntity;
	}


	public void setPortalEntity(PortalEntity portalEntity) {
		this.portalEntity = portalEntity;
	}


	public PortalNotificationSubscriptionFrequencies getSubscriptionFrequency() {
		return this.subscriptionFrequency;
	}


	public void setSubscriptionFrequency(PortalNotificationSubscriptionFrequencies subscriptionFrequency) {
		this.subscriptionFrequency = subscriptionFrequency;
	}
}
