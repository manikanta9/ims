package com.clifton.portal.notification.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The <code>PortalNotificationAcknowledgementType</code> class is used by pop up notification definitions
 * to define how the acknowledgements are handled and how the buttons appear.
 */
@PortalTrackingAuditAware(insert = false)
public class PortalNotificationAcknowledgementType extends NamedEntity<Short> {

	/**
	 * If populated the confirm button is available to the user
	 */
	private String confirmButtonLabel;
	private String confirmButtonTooltip;

	/**
	 * If populated the decline button is available to the user
	 */
	private String declineButtonLabel;
	private String declineButtonTooltip;

	/**
	 * If populated the cancel button is available to the user
	 */
	private String cancelButtonLabel;
	private String cancelButtonTooltip;

	/**
	 * Defines if by clicking the cancel button, it's still considered an acknowledgement.  Otherwise, it acts more as a defer and the user will receive the popup again until they click the confirm or decline button
	 */
	private boolean cancelAcknowledgement;

	/**
	 * If true, and a user "confirms" acknowledgement of the notification, then that confirmation should be used to "silently" confirm the nofication to other notifications for the same entity.
	 */
	private boolean confirmEntity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isConfirmUsed() {
		return !StringUtils.isEmpty(getConfirmButtonLabel());
	}


	public boolean isDeclineUsed() {
		return !StringUtils.isEmpty(getDeclineButtonLabel());
	}


	public boolean isCancelUsed() {
		return !StringUtils.isEmpty(getCancelButtonLabel());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getConfirmButtonLabel() {
		return this.confirmButtonLabel;
	}


	public void setConfirmButtonLabel(String confirmButtonLabel) {
		this.confirmButtonLabel = confirmButtonLabel;
	}


	public String getConfirmButtonTooltip() {
		return this.confirmButtonTooltip;
	}


	public void setConfirmButtonTooltip(String confirmButtonTooltip) {
		this.confirmButtonTooltip = confirmButtonTooltip;
	}


	public String getDeclineButtonLabel() {
		return this.declineButtonLabel;
	}


	public void setDeclineButtonLabel(String declineButtonLabel) {
		this.declineButtonLabel = declineButtonLabel;
	}


	public String getDeclineButtonTooltip() {
		return this.declineButtonTooltip;
	}


	public void setDeclineButtonTooltip(String declineButtonTooltip) {
		this.declineButtonTooltip = declineButtonTooltip;
	}


	public String getCancelButtonLabel() {
		return this.cancelButtonLabel;
	}


	public void setCancelButtonLabel(String cancelButtonLabel) {
		this.cancelButtonLabel = cancelButtonLabel;
	}


	public String getCancelButtonTooltip() {
		return this.cancelButtonTooltip;
	}


	public void setCancelButtonTooltip(String cancelButtonTooltip) {
		this.cancelButtonTooltip = cancelButtonTooltip;
	}


	public boolean isCancelAcknowledgement() {
		return this.cancelAcknowledgement;
	}


	public void setCancelAcknowledgement(boolean cancelAcknowledgement) {
		this.cancelAcknowledgement = cancelAcknowledgement;
	}


	public boolean isConfirmEntity() {
		return this.confirmEntity;
	}


	public void setConfirmEntity(boolean confirmEntity) {
		this.confirmEntity = confirmEntity;
	}
}
