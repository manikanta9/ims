package com.clifton.portal.notification.runner;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommandTypes;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorService;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;

import java.util.Date;


/**
 * The <code>PortalNotificationRunner</code> class represents a Runner for a specific run of a PortalNotificationDefinition.
 * <p>
 */
public class PortalNotificationRunner extends AbstractStatusAwareRunner {

	public static final String RUNNER_TYPE = "PORTAL_NOTIFICATION_DEFINITION";

	////////////////////////////////////////////////////////////////////////////

	private final short portalNotificationDefinitionId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortalNotificationGeneratorService portalNotificationGeneratorService;
	private PortalNotificationSetupService portalNotificationSetupService;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PortalNotificationRunner(Date runDate, short portalNotificationDefinitionId) {
		super(RUNNER_TYPE, Integer.toString(portalNotificationDefinitionId), runDate);
		this.portalNotificationDefinitionId = portalNotificationDefinitionId;
	}


	@Override
	public void run() {
		getStatus().setMessage("Started on " + DateUtils.fromDate(new Date()));

		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> {
			PortalNotificationDefinition notificationDefinition = getPortalNotificationSetupService().getPortalNotificationDefinition(this.portalNotificationDefinitionId);
			ValidationUtils.assertTrue(notificationDefinition.isActive(), "Cannot run notification definition [" + notificationDefinition.getLabel() + "] because it is not active as of [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "].");

			PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
			command.setPortalNotificationDefinition(notificationDefinition);
			command.setCommandType(PortalNotificationGeneratorCommandTypes.SEND);
			getPortalNotificationGeneratorService().generatePortalNotificationList(command);
		});
		getStatus().setMessage("Finished on " + DateUtils.fromDate(new Date()));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationGeneratorService getPortalNotificationGeneratorService() {
		return this.portalNotificationGeneratorService;
	}


	public void setPortalNotificationGeneratorService(PortalNotificationGeneratorService portalNotificationGeneratorService) {
		this.portalNotificationGeneratorService = portalNotificationGeneratorService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}
}
