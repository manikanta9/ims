package com.clifton.portal.notification;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PortalNotification extends HierarchicalEntity<PortalNotification, Integer> implements LabeledObject {

	private PortalNotificationDefinition notificationDefinition;

	private PortalSecurityUser recipientUser;

	/**
	 * Can only be used if the {@link PortalNotificationDefinitionType} IsInternalNotification = true
	 * Used when the email doesn't go to a particular user recipient but either a group of individuals or mailing list.
	 * i.e. Feedback entered is sent at the end of the day for any feedback entered that day to InstitutionalCenter@paraport.com
	 */
	private String recipientEmailAddress;

	private String subject;

	private String text;

	/**
	 * Date with time with the notification was sent
	 */
	private Date sentDate;

	/**
	 * If email does not successfully send, this would have the error message with why
	 */
	private String errorMessage;


	private boolean acknowledged;


	private List<PortalNotificationPortalFile> fileList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isError() {
		return !StringUtils.isEmpty(getErrorMessage());
	}


	public boolean isResend() {
		return this.getParent() != null;
	}


	public String getCoalesceRecipientLabel() {
		if (!StringUtils.isEmpty(getRecipientEmailAddress())) {
			return getRecipientEmailAddress();
		}
		if (getRecipientUser() != null) {
			return getRecipientUser().getLabelLong();
		}
		return null;
	}


	@Override
	public String getLabel() {
		return getNotificationDefinition().getLabel() + " - " + getCoalesceRecipientLabel() + (getSentDate() == null ? "" : " - " + DateUtils.fromDateShort(getSentDate()));
	}


	/**
	 * Only "popup" notifications are acknowledgeable (i.e. Notification Definition has acknowledgement type selected)
	 */
	public boolean isAcknowledgeable() {
		if (getNotificationDefinition() != null) {
			return getNotificationDefinition().getAcknowledgementType() != null;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationDefinition getNotificationDefinition() {
		return this.notificationDefinition;
	}


	public void setNotificationDefinition(PortalNotificationDefinition notificationDefinition) {
		this.notificationDefinition = notificationDefinition;
	}


	public PortalSecurityUser getRecipientUser() {
		return this.recipientUser;
	}


	public void setRecipientUser(PortalSecurityUser recipientUser) {
		this.recipientUser = recipientUser;
	}


	public String getRecipientEmailAddress() {
		return this.recipientEmailAddress;
	}


	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getSentDate() {
		return this.sentDate;
	}


	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}


	public String getErrorMessage() {
		return this.errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public boolean isAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(boolean acknowledged) {
		this.acknowledged = acknowledged;
	}


	public List<PortalNotificationPortalFile> getFileList() {
		return this.fileList;
	}


	public void setFileList(List<PortalNotificationPortalFile> fileList) {
		this.fileList = fileList;
	}
}
