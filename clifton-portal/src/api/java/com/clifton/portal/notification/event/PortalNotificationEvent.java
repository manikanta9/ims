package com.clifton.portal.notification.event;

import com.clifton.core.util.event.Event;
import com.clifton.core.util.event.EventObject;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.Map;


/**
 * The <code>PortalNotificationEvent</code> represents an {@link Event} that is raised during specific actions that should generate a notification/email to be sent
 * i.e. Forgot My Password, User Locked Out
 *
 * @author manderson
 */
public class PortalNotificationEvent extends EventObject<PortalSecurityUser, String> {

	public static final String EVENT_NAME = "PORTAL_NOTIFICATION_EVENT";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final String notificationDefinitionTypeName;

	private final Map<String, Object> templateContextBeanMap;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationEvent(String notificationDefinitionTypeName, PortalSecurityUser securityUser) {
		this(notificationDefinitionTypeName, securityUser, null);
	}


	public PortalNotificationEvent(String notificationDefinitionTypeName, PortalSecurityUser securityUser, Map<String, Object> templateContextBeanMap) {
		super(EVENT_NAME);
		setTarget(securityUser);
		this.notificationDefinitionTypeName = notificationDefinitionTypeName;
		this.templateContextBeanMap = templateContextBeanMap;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getNotificationDefinitionTypeName() {
		return this.notificationDefinitionTypeName;
	}


	public Map<String, Object> getTemplateContextBeanMap() {
		return this.templateContextBeanMap;
	}
}
