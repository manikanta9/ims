package com.clifton.portal.notification.setup;

import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.portal.notification.setup.search.PortalNotificationAcknowledgementTypeSearchForm;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionSearchForm;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionTypeSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalNotificationSetupService {

	////////////////////////////////////////////////////////////////////////////////
	////////         Portal Notification Definition Type Methods            ////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalNotificationDefinitionType getPortalNotificationDefinitionType(short id);


	@PortalSecureMethod
	public PortalNotificationDefinitionType getPortalNotificationDefinitionTypeByName(String name);


	@PortalSecureMethod
	public List<PortalNotificationDefinitionType> getPortalNotificationDefinitionTypeList(PortalNotificationDefinitionTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////
	/////////          Portal Notification Definition Methods              /////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalNotificationDefinition getPortalNotificationDefinition(short id);


	@PortalSecureMethod
	public PortalNotificationDefinition getPortalNotificationDefinitionByName(String name);


	@PortalSecureMethod
	public List<PortalNotificationDefinition> getPortalNotificationDefinitionList(PortalNotificationDefinitionSearchForm searchForm);


	@PortalSecureMethod // Anyone can edit an inactive definition, or name/description of an active definition
	public PortalNotificationDefinition savePortalNotificationDefinition(PortalNotificationDefinition bean);


	@PortalSecureMethod(portalAdminSecurity = true) // Only portal admins or admins can change the active flag on a definition
	public void setPortalNotificationDefinitionActive(short id, boolean active);


	@PortalSecureMethod
	public void copyPortalNotificationDefinition(short id, String name);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalNotificationDefinition(short id);


	////////////////////////////////////////////////////////////////////////////////
	///////      Portal Notification Acknowledgement Type Methods            ///////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalNotificationAcknowledgementType getPortalNotificationAcknowledgementType(short id);


	@PortalSecureMethod
	public List<PortalNotificationAcknowledgementType> getPortalNotificationAcknowledgementTypeList(PortalNotificationAcknowledgementTypeSearchForm searchForm);


	@PortalSecureMethod(portalAdminSecurity = true)
	@UserIgnorableValidation
	public PortalNotificationAcknowledgementType savePortalNotificationAcknowledgementType(PortalNotificationAcknowledgementType bean, boolean ignoreValidation);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalNotificationAcknowledgementType copyPortalNotificationAcknowledgementType(short id, String name);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalNotificationAcknowledgementType(short id);
}
