package com.clifton.portal.notification.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.portal.security.resource.PortalSecurityResource;


/**
 * The <code>PortalNotificationDefinitionType</code> defines a type of notification.  Each type determines how the data is retrieved and what values are available
 * for the email templates.
 * <p>
 *
 * @author manderson
 */
@CacheByName
public class PortalNotificationDefinitionType extends NamedEntity<Short> {

	// EXTERNAL NOTIFICATIONS
	public static final String TYPE_NAME_PORTAL_USER_WELCOME = "Portal User: Welcome";
	public static final String TYPE_NAME_PORTAL_USER_WELCOME_REMINDER = "Portal User: Welcome Reminder";
	public static final String TYPE_NAME_PORTAL_USER_FORGOT_PASSWORD = "Portal User: Forgot Password";
	public static final String TYPE_NAME_PORTAL_USER_CONFIRM_RESET_PASSWORD = "Portal User: Confirm Password Reset";
	public static final String TYPE_NAME_PORTAL_USER_REVIEW_CONTACT_INFO = "Portal User: Review Contact Information";
	public static final String TYPE_NAME_PORTAL_USER_LOCKED = "Portal User: Account Locked";
	public static final String TYPE_NAME_PORTAL_ADMIN_REVIEW_USERS = "Portal Administrator: Review Users";
	public static final String TYPE_NAME_PORTAL_ADMIN_INSTITUTIONAL = "Portal Administrator: Institutional Only";
	public static final String TYPE_NAME_INVOICE_OVERDUE = "Management Fee Invoice Overdue";
	public static final String TYPE_NAME_INVOICE_PAID = "Management Fee Invoice Paid";
	public static final String TYPE_NAME_INVOICE_POSTED = "Management Fee Invoice Posted";
	public static final String TYPE_NAME_FILE_POSTED = "Portal File Posted";
	public static final String TYPE_NAME_NEW_FILE_CATEGORY_AVAILABLE = "Portal Administrator: New File Category Available";

	// INTERNAL NOTIFICATIONS
	public static final String TYPE_NAME_PORTAL_FEEDBACK_ENTRY = "Portal Feedback Entry";
	public static final String TYPE_NAME_PORTAL_FILES_PENDING = "Portal Files Pending";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Defines whether this notification is sent to external/client users or internally
	 * Internal notifications may not be associated with a particular recipient email and could potentially go to multiple or team/mailing lists
	 */
	private boolean internalNotification;

	/**
	 * Defines whether or not a user can subscribe to the notification or not.
	 * i.e. Notify me when files are posted or when payment is applied to invoice - yes you can subscribe.
	 * Forgot Password, User is Locked Out, etc. type notifications cannot be opted out of
	 */
	private boolean subscriptionAllowed;

	/**
	 * If false, then the user subscribes directly to this notification
	 * If true, then the user subscribes through portal file categories (i.e. I want to be notified of Investment Reports being posted, but not Documentation)
	 * Any type that does not use subscriptionForFileCategory can only have one active definition at a time.
	 */
	private boolean subscriptionForFileCategory;

	/**
	 * If true, then the notification applies to files (creates entries in PortalNotificationPortalFile) table
	 * and also allows the definition to enter fileApprovedDaysBack
	 * This will limit the files in the notification - for example a user turns on and off notifications, we don't want to send them notifications for old files
	 * just recent ones.  In some cases this may be blank, i.e. overdue or paid invoice, because we don't care about when the file was approved just when the status changed
	 * which is determined by the recurrence
	 */
	private boolean fileNotification;


	/**
	 * If subscription is not for a specific file category, can optionally limit it to a security resource.  If it is for a file category, can then limit the file categories available to use definitions of this type.
	 * i.e. Invoice Paid notification should only be allowed to go to those that have access to Invoices
	 */
	private PortalSecurityResource securityResource;


	/**
	 * If true, then it's triggered by an event raised with the same name as the definition type.
	 * Otherwise, it uses frequency/schedule options
	 */
	private boolean triggeredByEvent;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isInternalNotification() {
		return this.internalNotification;
	}


	public void setInternalNotification(boolean internalNotification) {
		this.internalNotification = internalNotification;
	}


	public boolean isSubscriptionAllowed() {
		return this.subscriptionAllowed;
	}


	public void setSubscriptionAllowed(boolean subscriptionAllowed) {
		this.subscriptionAllowed = subscriptionAllowed;
	}


	public boolean isSubscriptionForFileCategory() {
		return this.subscriptionForFileCategory;
	}


	public void setSubscriptionForFileCategory(boolean subscriptionForFileCategory) {
		this.subscriptionForFileCategory = subscriptionForFileCategory;
	}


	public PortalSecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(PortalSecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public boolean isTriggeredByEvent() {
		return this.triggeredByEvent;
	}


	public void setTriggeredByEvent(boolean triggeredByEvent) {
		this.triggeredByEvent = triggeredByEvent;
	}


	public boolean isFileNotification() {
		return this.fileNotification;
	}


	public void setFileNotification(boolean fileNotification) {
		this.fileNotification = fileNotification;
	}
}
