package com.clifton.portal.notification.subscription;

import com.clifton.core.util.AssertUtils;


/**
 * The class <code>{@link PortalNotificationSubscriptionOptions}</code> enum defines how subscriptions are managed - i.e. defaulted on or off or required to be on
 *
 * @author manderson
 */
public enum PortalNotificationSubscriptionOptions {

	DEFAULT_OFF(false),
	DEFAULT_ON(true),
	REQUIRED_ON(true, true);

	/**
	 * Defaults to On or Off
	 */
	private final boolean defaultValue;

	/**
	 * If set, required to be on (Default must also be true)
	 */
	private final boolean required;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalNotificationSubscriptionOptions(boolean defaultValue) {
		this(defaultValue, false);
	}


	PortalNotificationSubscriptionOptions(boolean defaultValue, boolean required) {
		if (required) {
			AssertUtils.assertTrue(defaultValue, "If required, then default value must be true");
		}
		this.defaultValue = defaultValue;
		this.required = required;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isDefaultValue() {
		return this.defaultValue;
	}


	public boolean isRequired() {
		return this.required;
	}
}
