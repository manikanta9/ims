package com.clifton.portal.notification.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalNotificationDefinitionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;


	@SearchField
	private Boolean subscriptionAllowed;

	@SearchField
	private Boolean subscriptionForFileCategory;

	@SearchField
	private Boolean fileNotification;

	@SearchField(searchField = "securityResource.id")
	private Short securityResourceId;

	@SearchField(searchField = "securityResource.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean securityResourceIdIsNull;

	@SearchField(searchField = "securityResource.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Short[] securityResourceIdsOrNull;

	@SearchField
	private Boolean triggeredByEvent;

	@SearchField
	private Boolean internalNotification;

	// Custom Search Field if the Type has at least one active definition
	private Boolean activeDefinition;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getSubscriptionAllowed() {
		return this.subscriptionAllowed;
	}


	public void setSubscriptionAllowed(Boolean subscriptionAllowed) {
		this.subscriptionAllowed = subscriptionAllowed;
	}


	public Boolean getSubscriptionForFileCategory() {
		return this.subscriptionForFileCategory;
	}


	public void setSubscriptionForFileCategory(Boolean subscriptionForFileCategory) {
		this.subscriptionForFileCategory = subscriptionForFileCategory;
	}


	public Short getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Short securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public Boolean getTriggeredByEvent() {
		return this.triggeredByEvent;
	}


	public void setTriggeredByEvent(Boolean triggeredByEvent) {
		this.triggeredByEvent = triggeredByEvent;
	}


	public Boolean getActiveDefinition() {
		return this.activeDefinition;
	}


	public void setActiveDefinition(Boolean activeDefinition) {
		this.activeDefinition = activeDefinition;
	}


	public Short[] getSecurityResourceIdsOrNull() {
		return this.securityResourceIdsOrNull;
	}


	public void setSecurityResourceIdsOrNull(Short[] securityResourceIdsOrNull) {
		this.securityResourceIdsOrNull = securityResourceIdsOrNull;
	}


	public Boolean getFileNotification() {
		return this.fileNotification;
	}


	public void setFileNotification(Boolean fileNotification) {
		this.fileNotification = fileNotification;
	}


	public Boolean getInternalNotification() {
		return this.internalNotification;
	}


	public void setInternalNotification(Boolean internalNotification) {
		this.internalNotification = internalNotification;
	}


	public Boolean getSecurityResourceIdIsNull() {
		return this.securityResourceIdIsNull;
	}


	public void setSecurityResourceIdIsNull(Boolean securityResourceIdIsNull) {
		this.securityResourceIdIsNull = securityResourceIdIsNull;
	}
}
