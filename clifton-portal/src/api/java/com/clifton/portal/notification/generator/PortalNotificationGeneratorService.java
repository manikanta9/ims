package com.clifton.portal.notification.generator;

import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalNotificationGeneratorService {


	////////////////////////////////////////////////////////////////////////////////
	//////////          Portal Notification Generation Methods            //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalNotification> previewPortalNotificationList(PortalNotificationGeneratorCommand command);


	@ResponseBody
	@ModelAttribute("data")
	@PortalSecureMethod(portalAdminSecurity = true)
	public String generatePortalNotificationList(PortalNotificationGeneratorCommand command);


	@ResponseBody
	@ModelAttribute("data")
	@PortalSecureMethod(portalAdminSecurity = true)
	public String generatePortalNotificationResend(PortalNotificationGeneratorCommand command, Integer notificationId);


	@ResponseBody
	@ModelAttribute("data")
	@PortalSecureMethod(portalAdminSecurity = true)
	public String generatePortalTestNotification(PortalNotificationGeneratorCommand command);
}
