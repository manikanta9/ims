package com.clifton.portal.notification.subscription;

import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.holiday.PortalHolidayService;

import java.util.Date;


/**
 * The <code>PortalNotificationSubscriptionFrequencies</code> can be used to determine how often a user is notified of file postings.
 * These are currently only used for Daily file types so that users don't get notified each day a file is posted, but can be notified once a week, month, quarter
 *
 * @author manderson
 */
public enum PortalNotificationSubscriptionFrequencies {

	WEEKLY_FIRST_BUSINESS_DAY("First Business Day of the Week") {
		@Override
		public boolean isFrequencyValidForDate(Date date, PortalHolidayService portalHolidayService) {
			return portalHolidayService.isFirstBusinessDaySinceFromDate(DateUtils.getFirstDayOfWeek(date), date);
		}
	},

	MONTHLY_FIRST_BUSINESS_DAY("First Business Day of the Month") {
		@Override
		public boolean isFrequencyValidForDate(Date date, PortalHolidayService portalHolidayService) {
			return portalHolidayService.isFirstBusinessDaySinceFromDate(DateUtils.getFirstDayOfMonth(date), date);
		}
	},

	QUARTERLY_FIRST_BUSINESS_DAY("First Business Day of the Quarter") {
		@Override
		public boolean isFrequencyValidForDate(Date date, PortalHolidayService portalHolidayService) {
			return portalHolidayService.isFirstBusinessDaySinceFromDate(DateUtils.getFirstDayOfQuarter(date), date);
		}
	};

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final String label;


	private PortalNotificationSubscriptionFrequencies(String label) {
		this.label = label;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract boolean isFrequencyValidForDate(Date date, PortalHolidayService portalHolidayService);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}
}
