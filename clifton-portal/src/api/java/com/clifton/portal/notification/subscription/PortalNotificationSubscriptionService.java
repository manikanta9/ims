package com.clifton.portal.notification.subscription;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalNotificationSubscriptionService {

	////////////////////////////////////////////////////////////////////////////////
	///////////       Portal Notification Subscription Entry Methods      //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for subscription entry.  For the given user and entity will find the corresponding user assignment.
	 * Then, will return the list of subscriptions available for the entity.  By default all notifications are on, but if any were saved for the user
	 * this will return the most specific (by entity) list of subscriptions.  So, if viewing at the account level, but only relationship level was saved will use those on/off options.
	 * The children may have less options, but the higher levels will always include the children.
	 */
	@PortalSecureMethod
	public PortalNotificationSubscriptionEntry getPortalNotificationSubscriptionEntry(short securityUserId, int portalEntityId);


	/**
	 * Saves the list of subscriptions for the given user and entity.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalNotificationSubscriptionEntry savePortalNotificationSubscriptionEntry(PortalNotificationSubscriptionEntry bean);

	////////////////////////////////////////////////////////////////////////////////
	///////////         Portal Notification Subscription Methods        ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalNotificationSubscription> getPortalNotificationSubscriptionList(PortalNotificationSubscriptionSearchForm searchForm);


	/**
	 * Portal Admins can  manually rebuild for all users
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public void rebuildPortalNotificationSubscriptionList();


	/**
	 * Internal method - called from PortalFileCategoryObserver when a file category's subscriptions are changed to be required on
	 * Updates all existing subscriptions
	 */
	@DoNotAddRequestMapping
	public void requirePortalNotificationSubscriptionListForFileCategory(short portalFileCategoryId);


	/**
	 * When a security assignment is modified, the notifications for that assignment's user are automatically rebuilt.
	 * We have to do this at the user level as a user can have multiple assignments, and even if one is changed, the other may still apply
	 * i.e. Have access at Relationship Level, Separate access at client level, if client level is disabled, the relationship level applies to the notifications
	 * So, if a user is removed from having access, that notification subscription will be removed.
	 * <p>
	 * Portal Admins can also manually rebuild for a user
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public void rebuildPortalNotificationSubscriptionListForUser(short portalSecurityUserId);


	@DoNotAddRequestMapping
	public void deletePortalNotificationSubscriptionListForUserAssignment(int securityUserAssignmentId);


	/**
	 * Returns true if the user is subscribed to the given notification, based on type, user, and optional file extended
	 * file extended is used to find the assignment entity for the subscription (and file category if subscription is at that level)
	 * Checks most specific level up.  By default all notifications are off
	 * <p>
	 * If subscriptions aren't used for the definition type then returns true since users can't turn them off
	 */
	@DoNotAddRequestMapping
	public boolean isPortalNotificationSubscriptionEnabled(PortalNotificationDefinitionType definitionType, PortalSecurityUser securityUser, PortalFileExtended portalFileExtended);
}
