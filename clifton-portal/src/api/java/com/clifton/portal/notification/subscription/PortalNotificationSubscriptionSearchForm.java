package com.clifton.portal.notification.subscription;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalNotificationSubscriptionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchFieldPath = "userAssignment", searchField = "securityUser.id")
	private Short portalSecurityUserId;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "username,firstName,lastName,title,phoneNumber,companyName,emailAddress")
	private String userSearchPattern;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "username")
	private String username;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "emailAddress")
	private String emailAddress;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "title")
	private String userTitle;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "phoneNumber")
	private String userPhoneNumber;

	@SearchField(searchFieldPath = "userAssignment.securityUser", searchField = "companyName")
	private String userCompanyName;

	@SearchField(searchField = "userAssignment.id")
	private Integer portalSecurityUserAssignmentId;

	@SearchField(searchField = "portalEntity.id")
	private Integer portalEntityId;

	@SearchField(searchField = "notificationDefinitionType.id")
	private Short portalNotificationDefinitionTypeId;

	@SearchField(searchField = "fileCategory.id")
	private Short portalFileCategoryId;

	@SearchField(searchField = "fileCategory.id")
	private Short[] portalFileCategoryIds;

	@SearchField(searchField = "notificationDefinitionType.name,fileCategory.name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private String coalesceDefinitionTypeFileCategoryName;


	@SearchField
	private Boolean notificationEnabled;

	@SearchField
	private PortalNotificationSubscriptionFrequencies subscriptionFrequency;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPortalSecurityUserId() {
		return this.portalSecurityUserId;
	}


	public void setPortalSecurityUserId(Short portalSecurityUserId) {
		this.portalSecurityUserId = portalSecurityUserId;
	}


	public Integer getPortalSecurityUserAssignmentId() {
		return this.portalSecurityUserAssignmentId;
	}


	public void setPortalSecurityUserAssignmentId(Integer portalSecurityUserAssignmentId) {
		this.portalSecurityUserAssignmentId = portalSecurityUserAssignmentId;
	}


	public Short getPortalNotificationDefinitionTypeId() {
		return this.portalNotificationDefinitionTypeId;
	}


	public void setPortalNotificationDefinitionTypeId(Short portalNotificationDefinitionTypeId) {
		this.portalNotificationDefinitionTypeId = portalNotificationDefinitionTypeId;
	}


	public Short getPortalFileCategoryId() {
		return this.portalFileCategoryId;
	}


	public void setPortalFileCategoryId(Short portalFileCategoryId) {
		this.portalFileCategoryId = portalFileCategoryId;
	}


	public Short[] getPortalFileCategoryIds() {
		return this.portalFileCategoryIds;
	}


	public void setPortalFileCategoryIds(Short[] portalFileCategoryIds) {
		this.portalFileCategoryIds = portalFileCategoryIds;
	}


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}


	public String getUserSearchPattern() {
		return this.userSearchPattern;
	}


	public void setUserSearchPattern(String userSearchPattern) {
		this.userSearchPattern = userSearchPattern;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getUserTitle() {
		return this.userTitle;
	}


	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}


	public String getUserPhoneNumber() {
		return this.userPhoneNumber;
	}


	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}


	public String getUserCompanyName() {
		return this.userCompanyName;
	}


	public void setUserCompanyName(String userCompanyName) {
		this.userCompanyName = userCompanyName;
	}


	public String getCoalesceDefinitionTypeFileCategoryName() {
		return this.coalesceDefinitionTypeFileCategoryName;
	}


	public void setCoalesceDefinitionTypeFileCategoryName(String coalesceDefinitionTypeFileCategoryName) {
		this.coalesceDefinitionTypeFileCategoryName = coalesceDefinitionTypeFileCategoryName;
	}


	public Boolean getNotificationEnabled() {
		return this.notificationEnabled;
	}


	public void setNotificationEnabled(Boolean notificationEnabled) {
		this.notificationEnabled = notificationEnabled;
	}


	public PortalNotificationSubscriptionFrequencies getSubscriptionFrequency() {
		return this.subscriptionFrequency;
	}


	public void setSubscriptionFrequency(PortalNotificationSubscriptionFrequencies subscriptionFrequency) {
		this.subscriptionFrequency = subscriptionFrequency;
	}
}
