package com.clifton.portal.notification.generator;

/**
 * @author manderson
 */
public enum PortalNotificationGeneratorCommandTypes {

	PREVIEW(true, false, false, true), // GENERATES AND RETURNS A LIST OF NOTIFICATIONS THAT WOULD BE SENT IF IT WERE ACTIVE AND SENT NOW.  DOES NOT SEND ANY EMAILS
	PREVIEW_ONE_SAMPLE(true, true, false, true), // GENERATES A SAMPLE NOTIFICATION - SOMETIMES WITH FAKE DATA. USEFUL VALIDATE FREEMARKER SYNTAX
	SEND_PREVIEW(true, false, true, false), // SAME AS PREVIEW, BUT WILL SEND THE EMAILS TO THE CURRENT USER.  BECAUSE OF IMAGES IN THE EMAILS USEFUL TO CONFIRM THE EMAIL IS GENERATED CORRECTLY
	SEND_PREVIEW_ONE_SAMPLE(true, true, true, false), // SAME AS PREVIEW_ONE_SAMPLE, BUT WILL ALSO SEND THE EMAIL TO THE CURRENT USER. BECAUSE OF IMAGES IN THE EMAILS USEFUL TO CONFIRM THE EMAIL IS GENERATED CORRECTLY
	SEND(false, false, true, true); // GENERATE AND SEND THE EMAILS TO APPROPRIATE RECIPIENTS - NOTE: PopUps won't send, just generate

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final boolean preview;

	private final boolean previewOneSample;

	private final boolean send;

	private final boolean popUpNotificationSupported;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalNotificationGeneratorCommandTypes(boolean preview, boolean previewOneSample, boolean send, boolean popUpNotificationSupported) {
		this.preview = preview;
		this.previewOneSample = previewOneSample;
		this.send = send;
		this.popUpNotificationSupported = popUpNotificationSupported;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isPreview() {
		return this.preview;
	}


	public boolean isPreviewOneSample() {
		return this.previewOneSample;
	}


	public boolean isSend() {
		return this.send;
	}


	public boolean isPopUpNotificationSupported() {
		return this.popUpNotificationSupported;
	}
}
