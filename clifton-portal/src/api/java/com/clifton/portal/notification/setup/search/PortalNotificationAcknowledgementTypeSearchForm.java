package com.clifton.portal.notification.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalNotificationAcknowledgementTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "confirmButtonLabel", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean confirmUsed;

	@SearchField
	private String confirmButtonLabel;

	@SearchField
	private String confirmButtonTooltip;

	@SearchField(searchField = "declineButtonLabel", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean declineUsed;

	@SearchField
	private String declineButtonLabel;

	@SearchField
	private String declineButtonTooltip;


	@SearchField(searchField = "cancelButtonLabel", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean cancelUsed;


	@SearchField
	private String cancelButtonLabel;

	@SearchField
	private String cancelButtonTooltip;

	@SearchField
	private Boolean cancelAcknowledgement;

	@SearchField
	private Boolean confirmEntity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getConfirmUsed() {
		return this.confirmUsed;
	}


	public void setConfirmUsed(Boolean confirmUsed) {
		this.confirmUsed = confirmUsed;
	}


	public String getConfirmButtonLabel() {
		return this.confirmButtonLabel;
	}


	public void setConfirmButtonLabel(String confirmButtonLabel) {
		this.confirmButtonLabel = confirmButtonLabel;
	}


	public String getConfirmButtonTooltip() {
		return this.confirmButtonTooltip;
	}


	public void setConfirmButtonTooltip(String confirmButtonTooltip) {
		this.confirmButtonTooltip = confirmButtonTooltip;
	}


	public Boolean getDeclineUsed() {
		return this.declineUsed;
	}


	public void setDeclineUsed(Boolean declineUsed) {
		this.declineUsed = declineUsed;
	}


	public String getDeclineButtonLabel() {
		return this.declineButtonLabel;
	}


	public void setDeclineButtonLabel(String declineButtonLabel) {
		this.declineButtonLabel = declineButtonLabel;
	}


	public String getDeclineButtonTooltip() {
		return this.declineButtonTooltip;
	}


	public void setDeclineButtonTooltip(String declineButtonTooltip) {
		this.declineButtonTooltip = declineButtonTooltip;
	}


	public Boolean getCancelUsed() {
		return this.cancelUsed;
	}


	public void setCancelUsed(Boolean cancelUsed) {
		this.cancelUsed = cancelUsed;
	}


	public String getCancelButtonLabel() {
		return this.cancelButtonLabel;
	}


	public void setCancelButtonLabel(String cancelButtonLabel) {
		this.cancelButtonLabel = cancelButtonLabel;
	}


	public String getCancelButtonTooltip() {
		return this.cancelButtonTooltip;
	}


	public void setCancelButtonTooltip(String cancelButtonTooltip) {
		this.cancelButtonTooltip = cancelButtonTooltip;
	}


	public Boolean getCancelAcknowledgement() {
		return this.cancelAcknowledgement;
	}


	public void setCancelAcknowledgement(Boolean cancelAcknowledgement) {
		this.cancelAcknowledgement = cancelAcknowledgement;
	}


	public Boolean getConfirmEntity() {
		return this.confirmEntity;
	}


	public void setConfirmEntity(Boolean confirmEntity) {
		this.confirmEntity = confirmEntity;
	}
}
