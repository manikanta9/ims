package com.clifton.portal.security.resource;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The <code>PortalSecurityResource</code> defines security resources that are associated with various portal file categories
 * which allows individual client users to have access to specific client documents/information
 * <p>
 * This is currently only used on {@link com.clifton.portal.file.setup.PortalFileCategory}
 * and then for assigning security: {@link com.clifton.portal.security.user.PortalSecurityUserAssignmentResource}
 *
 * @author manderson
 */
@CacheByName
@PortalTrackingAuditAware
public class PortalSecurityResource extends NamedHierarchicalEntity<PortalSecurityResource, Short> implements LabeledObject {

	public static final String RESOURCE_ACCOUNTING_REPORTING = "Accounting Reporting";
	public static final String RESOURCE_INVOICES = "Invoices";
	public static final String RESOURCE_CONTACTS = "Contacts";
	public static final String RESOURCE_DOCUMENTATION = "Documentation";
	public static final String RESOURCE_HELP = "Help";
	public static final String RESOURCE_INVESTMENT_REPORTING = "Investment Reporting";
	public static final String RESOURCE_PRIVATE_FUNDS_DOCUMENTATION = "Private Funds Documentation";
	public static final String RESOURCE_PRIVATE_FUNDS_REPORTING = "Private Funds Reporting";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If system defined, then referenced by name and can't be changed
	 */
	private boolean systemDefined;


	/**
	 * By Default all portal entity specific users will be granted access to this
	 * security resource when the first file for the resource becomes available for their assignment.
	 * If this is checked, then the default access applies only to Portal Admins (who always get access to everything by default) - or if the user already get's access via parent security.
	 * Applies to security for portalEntitySpecific user roles ONLY (not internal users)
	 */
	private boolean doNotDefaultPortalEntitySpecificAccess;


	/**
	 * Support 2 levels deep
	 */
	@Override
	public Integer getMaxDepth() {
		return 2;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isDoNotDefaultPortalEntitySpecificAccess() {
		return this.doNotDefaultPortalEntitySpecificAccess;
	}


	public void setDoNotDefaultPortalEntitySpecificAccess(boolean doNotDefaultPortalEntitySpecificAccess) {
		this.doNotDefaultPortalEntitySpecificAccess = doNotDefaultPortalEntitySpecificAccess;
	}
}
