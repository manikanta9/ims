package com.clifton.portal.security.user;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author manderson
 */
public interface PortalSecurityUserManagementService {


	/**
	 * This is the save method to create new users, or edit existing users.
	 * This method can ONLY be used for users that do not have a source system defined.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUser savePortalSecurityUser(PortalSecurityUser bean);


	@DoNotAddRequestMapping
	public void incrementPortalSecurityUserInvalidLoginCount(String userName);


	@DoNotAddRequestMapping
	public void resetPortalSecurityUserInvalidLoginCount(String userName);


	/**
	 * Generates a random password reset string, and sends an email to the user with a link to reset their password.
	 */
	@DoNotAddRequestMapping
	public String requestPortalSecurityUserPasswordReset(String userName);


	/**
	 * Validates the passwordResetKey has not expires and updates the user password if it's valid.
	 */
	@DoNotAddRequestMapping
	public void resetPortalSecurityUserPassword(String passwordResetKey, String password);


	/**
	 * Optional manual step to send given user a welcome email.
	 * Will also generate a temporary password and include it in the email
	 * This cannot be used once the user has accepted the terms of use - at that point the welcome doesn't apply and we don't want to overwrite their password
	 * NOTE: Returns result of the event raising, not the randomPassword
	 * <p>
	 */
	@ResponseBody
	@ModelAttribute("data")
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	@RequestMapping("portalSecurityUserWelcomeNotificationSend")
	public String sendPortalSecurityUserWelcomeNotification(short portalSecurityUserId);


	/**
	 * This method can be used for any user account
	 * Note: The user making the change must have access to that role.
	 * i.e. a Client Admin cannot make themselves or any other user a real Administrator.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void updatePortalSecurityUserRole(short portalSecurityUserId, short portalSecurityUserRoleId);


	/**
	 * This is only called for the welcome emails.  As an extra precaution will confirm TOU haven't been accepted
	 * Will generate a random password - encode it and save it on the user
	 * returns the un-encoded password for use in the email notifications
	 */
	@DoNotAddRequestMapping
	public String resetPortalSecurityUserRandomPassword(PortalSecurityUser portalSecurityUser);


	/**
	 * This method can ONLY be used for users that do not have a source system defined.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void updatePortalSecurityUserPassword(short portalSecurityUserId, String password);


	/**
	 * This method can ONLY be used for users that do not have a source system defined.
	 */
	@PortalSecureMethod
	public void updatePortalSecurityCurrentUserPassword(String oldPassword, String password);


	/**
	 * This method can ONLY be used for users that do not have a source system defined.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void lockPortalSecurityUser(short portalSecurityUserId);


	/**
	 * This method can ONLY be used for users that do not have a source system defined.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void unlockPortalSecurityUser(short portalSecurityUserId);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	@RequestMapping("portalSecurityUserTermsOfUseAccept")
	public void acceptPortalSecurityUserTermsOfUse(PortalSecurityUser user);
}
