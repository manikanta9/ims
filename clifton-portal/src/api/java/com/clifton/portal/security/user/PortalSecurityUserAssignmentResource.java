package com.clifton.portal.security.user;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;

import java.util.List;


/**
 * @author manderson
 */
@PortalTrackingAuditAware
public class PortalSecurityUserAssignmentResource extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * Used for users under portal entity specific roles, there will be an assignment that defines which user AND entity this security applies to
	 */
	private PortalSecurityUserAssignment securityUserAssignment;

	/**
	 * Used for users under NON portal entity specific roles, who will NOT have a specific assignment.  This defines the overall security
	 * that applies for posting files.
	 */
	private PortalSecurityUser securityUser;


	private PortalSecurityResource securityResource;


	@PortalTrackingAuditColumnConfig(alwaysAuditInsert = true, alwaysAuditDelete = true)
	private boolean accessAllowed;

	/**
	 * Used in UI to display the file category names that apply to the selected resource
	 * These are different based on each assignment because each entity may utilize different file categories
	 */
	@NonPersistentField
	private List<PortalFileCategory> fileCategoryList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getSecurityUser() != null) {
			if (getSecurityResource() != null) {
				return getSecurityUser().getLabel() + " - " + getSecurityResource().getLabel();
			}
			return getSecurityUser().getLabel();
		}
		if (getSecurityUserAssignment() != null) {
			if (getSecurityResource() != null) {
				return getSecurityUserAssignment().getLabel() + " - " + getSecurityResource().getLabel();
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserAssignment getSecurityUserAssignment() {
		return this.securityUserAssignment;
	}


	public void setSecurityUserAssignment(PortalSecurityUserAssignment securityUserAssignment) {
		this.securityUserAssignment = securityUserAssignment;
	}


	public PortalSecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(PortalSecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public PortalSecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(PortalSecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public boolean isAccessAllowed() {
		return this.accessAllowed;
	}


	public void setAccessAllowed(boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}


	public List<PortalFileCategory> getFileCategoryList() {
		return this.fileCategoryList;
	}


	public void setFileCategoryList(List<PortalFileCategory> fileCategoryList) {
		this.fileCategoryList = fileCategoryList;
	}
}
