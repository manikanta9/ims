package com.clifton.portal.security.search;

/**
 * The <code>PortalEntitySpecificAwareSearchForm</code> is implemented by search forms where View As Support is needed.
 * View as Support can be applied by a user or entity
 *
 * @author manderson
 */
public interface PortalEntitySpecificAwareSearchForm {


	public Short getViewAsUserId();


	public void setViewAsUserId(Short viewAsUserId);


	public Integer getViewAsEntityId();


	public void setViewAsEntityId(Integer viewAsEntityId);
}
