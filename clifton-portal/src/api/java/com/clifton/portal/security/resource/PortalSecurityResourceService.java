package com.clifton.portal.security.resource;

import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalSecurityResourceService {


	////////////////////////////////////////////////////////////////////////////////
	//////////            Portal Security Resource Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalSecurityResource getPortalSecurityResource(short id);


	@PortalSecureMethod
	public PortalSecurityResource getPortalSecurityResourceByName(String name);


	@PortalSecureMethod
	public List<PortalSecurityResource> getPortalSecurityResourceList(PortalSecurityResourceSearchForm searchForm);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalSecurityResource savePortalSecurityResource(PortalSecurityResource bean);


	@PortalSecureMethod(portalAdminSecurity = true)
	public void deletePortalSecurityResource(short id);
}
