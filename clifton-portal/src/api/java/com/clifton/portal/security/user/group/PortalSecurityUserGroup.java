package com.clifton.portal.security.user.group;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portal.security.user.PortalSecurityUser;


/**
 * The <code>PortalSecurityUserGroup</code> defines a user's (internal only) membership in a group.  Any user in a group inherits all of the group's security resources.
 *
 * @author manderson
 */
public class PortalSecurityUserGroup extends BaseEntity<Integer> {


	private PortalSecurityGroup securityGroup;

	private PortalSecurityUser securityUser;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(PortalSecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}


	public PortalSecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(PortalSecurityUser securityUser) {
		this.securityUser = securityUser;
	}
}
