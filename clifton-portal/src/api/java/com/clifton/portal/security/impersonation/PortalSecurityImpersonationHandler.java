package com.clifton.portal.security.impersonation;

import java.util.function.Supplier;


/**
 * The PortalSecurityImpersonationHandler interface defines method that can be used to run as system user.
 * Run as functionality saves current user information before running the task and then restores it after.
 *
 * @author manderson
 */
public interface PortalSecurityImpersonationHandler {

	/**
	 * Executes the specified task as the System User
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public void runAsSystemUser(Runnable task);


	/**
	 * Executes the specified task as the System user
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public <T> T runAsSystemUserAndReturn(Supplier<T> task);
}
