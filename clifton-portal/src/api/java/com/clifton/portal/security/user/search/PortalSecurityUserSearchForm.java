package com.clifton.portal.security.user.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalSecurityUserSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	// Custom search field that looks for specific user ids or any user that is an admin
	private Short[] idsOrAdmin;


	@SearchField(searchField = "username,firstName,lastName,title,phoneNumber,companyName")
	private String searchPattern;

	@SearchField
	private String username;

	@SearchField
	private String firstName;

	@SearchField
	private String lastName;

	@SearchField
	private String title;

	@SearchField
	private String phoneNumber;

	@SearchField
	private String companyName;

	@SearchField
	private Date passwordUpdateDate;

	@SearchField
	private Short invalidLoginAttemptCount;

	@SearchField(searchField = "portalEntitySourceSystem.id", sortField = "portalEntitySourceSystem.name")
	private Short portalEntitySourceSystemId;

	@SearchField
	private Boolean accountLocked;

	@SearchField
	private Boolean disabled;

	@SearchField(searchField = "userRole.id", sortField = "userRole.name")
	private Short userRoleId;

	@SearchField(searchFieldPath = "userRole", searchField = "portalEntitySpecific")
	private Boolean userRolePortalEntitySpecific;

	@SearchField(searchFieldPath = "userRole", searchField = "portalSecurityAdministrator")
	private Boolean userRolePortalSecurityAdministrator;

	// Custom Search Field - where exists in assignment table
	private Boolean assignmentRolePortalSecurityAdministrator;

	// Custom Search Field where exists in assignment expanded table
	// Used ONLY to display list of users with access to a security resource for GLOBAL files
	private Short assignmentAccessForPortalSecurityResourceId;

	// Custom Search Field where exists in assignment table where user is assigned to an entity of the specified view type
	private Short assignmentPortalEntityViewTypeId;

	// Custom Search Field: Password Reset is Required if No Source System and (No Password Update Date or it's over a year since password has been updated)
	private Boolean passwordResetRequired;

	@SearchField
	private Date termsOfUseAcceptanceDate;

	@SearchField(searchField = "termsOfUseAcceptanceDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean termsOfUseAccepted;

	// Custom search field - does a where exists on the PortalSecurityUserAssignment (use only with userRolePortalEntitySpecific = true)
	// If true - returns users with at least one active assignment
	// If false - returns users with no active assignments
	private Boolean activeAssignmentsExist;

	@SearchField
	private String passwordResetKey;

	@SearchField(searchField = "passwordResetKeyExpirationDateAndTime", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date passwordResetKeyExpirationDateAndTimeGreaterThanOrEqual;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Short[] getIdsOrAdmin() {
		return this.idsOrAdmin;
	}


	public void setIdsOrAdmin(Short[] idsOrAdmin) {
		this.idsOrAdmin = idsOrAdmin;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Date getPasswordUpdateDate() {
		return this.passwordUpdateDate;
	}


	public void setPasswordUpdateDate(Date passwordUpdateDate) {
		this.passwordUpdateDate = passwordUpdateDate;
	}


	public Short getInvalidLoginAttemptCount() {
		return this.invalidLoginAttemptCount;
	}


	public void setInvalidLoginAttemptCount(Short invalidLoginAttemptCount) {
		this.invalidLoginAttemptCount = invalidLoginAttemptCount;
	}


	public Short getPortalEntitySourceSystemId() {
		return this.portalEntitySourceSystemId;
	}


	public void setPortalEntitySourceSystemId(Short portalEntitySourceSystemId) {
		this.portalEntitySourceSystemId = portalEntitySourceSystemId;
	}


	public Boolean getAccountLocked() {
		return this.accountLocked;
	}


	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Short getUserRoleId() {
		return this.userRoleId;
	}


	public void setUserRoleId(Short userRoleId) {
		this.userRoleId = userRoleId;
	}


	public Boolean getUserRolePortalEntitySpecific() {
		return this.userRolePortalEntitySpecific;
	}


	public void setUserRolePortalEntitySpecific(Boolean userRolePortalEntitySpecific) {
		this.userRolePortalEntitySpecific = userRolePortalEntitySpecific;
	}


	public Boolean getUserRolePortalSecurityAdministrator() {
		return this.userRolePortalSecurityAdministrator;
	}


	public void setUserRolePortalSecurityAdministrator(Boolean userRolePortalSecurityAdministrator) {
		this.userRolePortalSecurityAdministrator = userRolePortalSecurityAdministrator;
	}


	public Boolean getAssignmentRolePortalSecurityAdministrator() {
		return this.assignmentRolePortalSecurityAdministrator;
	}


	public void setAssignmentRolePortalSecurityAdministrator(Boolean assignmentRolePortalSecurityAdministrator) {
		this.assignmentRolePortalSecurityAdministrator = assignmentRolePortalSecurityAdministrator;
	}


	public Short getAssignmentAccessForPortalSecurityResourceId() {
		return this.assignmentAccessForPortalSecurityResourceId;
	}


	public void setAssignmentAccessForPortalSecurityResourceId(Short assignmentAccessForPortalSecurityResourceId) {
		this.assignmentAccessForPortalSecurityResourceId = assignmentAccessForPortalSecurityResourceId;
	}


	public Short getAssignmentPortalEntityViewTypeId() {
		return this.assignmentPortalEntityViewTypeId;
	}


	public void setAssignmentPortalEntityViewTypeId(Short assignmentPortalEntityViewTypeId) {
		this.assignmentPortalEntityViewTypeId = assignmentPortalEntityViewTypeId;
	}


	public Boolean getPasswordResetRequired() {
		return this.passwordResetRequired;
	}


	public void setPasswordResetRequired(Boolean passwordResetRequired) {
		this.passwordResetRequired = passwordResetRequired;
	}


	public Date getTermsOfUseAcceptanceDate() {
		return this.termsOfUseAcceptanceDate;
	}


	public void setTermsOfUseAcceptanceDate(Date termsOfUseAcceptanceDate) {
		this.termsOfUseAcceptanceDate = termsOfUseAcceptanceDate;
	}


	public Boolean getTermsOfUseAccepted() {
		return this.termsOfUseAccepted;
	}


	public void setTermsOfUseAccepted(Boolean termsOfUseAccepted) {
		this.termsOfUseAccepted = termsOfUseAccepted;
	}


	public Boolean getActiveAssignmentsExist() {
		return this.activeAssignmentsExist;
	}


	public void setActiveAssignmentsExist(Boolean activeAssignmentsExist) {
		this.activeAssignmentsExist = activeAssignmentsExist;
	}


	public String getPasswordResetKey() {
		return this.passwordResetKey;
	}


	public void setPasswordResetKey(String passwordResetKey) {
		this.passwordResetKey = passwordResetKey;
	}


	public Date getPasswordResetKeyExpirationDateAndTimeGreaterThanOrEqual() {
		return this.passwordResetKeyExpirationDateAndTimeGreaterThanOrEqual;
	}


	public void setPasswordResetKeyExpirationDateAndTimeGreaterThanOrEqual(Date passwordResetKeyExpirationDateAndTimeGreaterThanOrEqual) {
		this.passwordResetKeyExpirationDateAndTimeGreaterThanOrEqual = passwordResetKeyExpirationDateAndTimeGreaterThanOrEqual;
	}
}
