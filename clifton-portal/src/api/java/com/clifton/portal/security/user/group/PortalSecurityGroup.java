package com.clifton.portal.security.user.group;

import com.clifton.core.beans.NamedEntity;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;

import java.util.List;


/**
 * The <code>PortalSecurityGroup</code> class is used to group a list of users (internal users only) for the purposes of assigning security resources for which file categories they can post to.
 * Groups can either be manually created (in which the users can be manually added/removed from the group by portal security admins), OR maintained by an outside source system (IMS)
 * In cases where the group is from an outside source system, the group and it's members are fully maintained by that system and cannot be manually updated.  ONLY the security resources that group has access to can be
 * updated by portal security admins within the portal application
 * <p>
 * Example: Analysts Group from IMS - user membership is maintained by IMS, but within the Portal, security access for posting specific types of reports can be granted for any user that is in the analyst group.
 *
 * @author manderson
 */
public class PortalSecurityGroup extends NamedEntity<Short> {

	/**
	 * If this group is maintained by an outside system
	 */
	private PortalEntitySourceSystem sourceSystem;

	/**
	 * Id reference to the entity in the source system
	 */
	private Integer sourceFkFieldId;


	/**
	 * Defines access for the group
	 */
	private List<PortalSecurityGroupResource> resourceList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getSourceSystem() != null) {
			return getSourceSystem().getName() + ": " + getName();
		}
		return getName();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(PortalEntitySourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public List<PortalSecurityGroupResource> getResourceList() {
		return this.resourceList;
	}


	public void setResourceList(List<PortalSecurityGroupResource> resourceList) {
		this.resourceList = resourceList;
	}
}
