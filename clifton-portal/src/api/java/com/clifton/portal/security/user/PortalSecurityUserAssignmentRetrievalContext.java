package com.clifton.portal.security.user;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceSearchForm;
import com.clifton.portal.security.resource.PortalSecurityResourceService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalSecurityUserAssignmentRetrievalContext</code> object is used to hold common lookups results and helps with processing of user security rebuilds across users
 *
 * @author manderson
 */
public class PortalSecurityUserAssignmentRetrievalContext {

	/**
	 * For each entity, the file categories available that have approved files.
	 * For each user, this can then be easily filtered based on security resource access
	 */
	private Map<Integer, List<PortalFileCategory>> portalEntityFileCategoryListMap = new HashMap<>();


	/**
	 * Caches the list of all security resources, which can be easily filtered based on an array of ids
	 */
	private List<PortalSecurityResource> portalSecurityResourceList;


	/**
	 * Caches for an entity id, the direct children only of that entity
	 */
	private Map<Integer, List<PortalEntity>> portalEntityChildrenListMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PortalFileCategory> getPortalFileCategoryListForEntity(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler, PortalFileSetupService portalFileSetupService, int portalEntityId) {
		if (!this.portalEntityFileCategoryListMap.containsKey(portalEntityId)) {
			portalSecurityImpersonationHandler.runAsSystemUser(() -> {
				PortalFileCategorySearchForm fileCategorySearchForm = new PortalFileCategorySearchForm();
				fileCategorySearchForm.setLimitToCategoriesForAssignedPortalEntityId(portalEntityId);
				fileCategorySearchForm.setLimitToCategoriesWithAvailableApprovedFiles(true);
				fileCategorySearchForm.setFileCategoryType(PortalFileCategoryTypes.FILE_GROUP);
				fileCategorySearchForm.setOrderBy("id:asc"); // Order by ID to avoid extra joins in default sorting
				this.portalEntityFileCategoryListMap.put(portalEntityId, portalFileSetupService.getPortalFileCategoryList(fileCategorySearchForm));
			});
		}
		return this.portalEntityFileCategoryListMap.get(portalEntityId);
	}


	public List<PortalSecurityResource> getPortalSecurityResourceList(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler, PortalSecurityResourceService portalSecurityResourceService) {
		if (this.portalSecurityResourceList == null) {
			PortalSecurityResourceSearchForm securityResourceSearchForm = new PortalSecurityResourceSearchForm();
			portalSecurityImpersonationHandler.runAsSystemUser(() -> this.portalSecurityResourceList = portalSecurityResourceService.getPortalSecurityResourceList(securityResourceSearchForm));
		}
		return this.portalSecurityResourceList;
	}


	public List<PortalSecurityResource> getPortalSecurityResourceList(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler, PortalSecurityResourceService portalSecurityResourceService, Short[] securityResourceIds) {
		if (ArrayUtils.isEmpty(securityResourceIds)) {
			return null;
		}
		return BeanUtils.filter(getPortalSecurityResourceList(portalSecurityImpersonationHandler, portalSecurityResourceService), portalSecurityResource -> ArrayUtils.contains(securityResourceIds, portalSecurityResource.getId()));
	}


	public List<PortalEntity> getPortalEntityChildrenList(PortalEntityService portalEntityService, int portalEntityId) {
		if (!this.portalEntityChildrenListMap.containsKey(portalEntityId)) {
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setParentId(portalEntityId);
			this.portalEntityChildrenListMap.put(portalEntityId, portalEntityService.getPortalEntityList(searchForm));
		}
		return this.portalEntityChildrenListMap.get(portalEntityId);
	}
}
