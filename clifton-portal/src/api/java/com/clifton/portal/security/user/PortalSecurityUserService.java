package com.clifton.portal.security.user;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.form.BaseSortableSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserRoleSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalSecurityUserService {


	////////////////////////////////////////////////////////////////////////////////
	///////////            Portal Security User Role Methods            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalSecurityUserRole getPortalSecurityUserRole(short id);


	/**
	 * Used for Detail Role window which also populates the resource list for ALL resources so the user can chose limits.
	 */
	@PortalSecureMethod
	public PortalSecurityUserRole getPortalSecurityUserRoleExpanded(short id);


	@DoNotAddRequestMapping
	public PortalSecurityUserRole getPortalSecurityUserRoleByName(String name);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUserRole savePortalSecurityUserRole(PortalSecurityUserRole bean);


	/**
	 * Also saves the role resource mapping and returns the role which also populates the resource list for ALL resources so the user can chose limits.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUserRole savePortalSecurityUserRoleExpanded(PortalSecurityUserRole bean);


	@PortalSecureMethod
	public List<PortalSecurityUserRole> getPortalSecurityUserRoleList(PortalSecurityUserRoleSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////
	////////////              Portal Security User Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For non portal entity specific users, will also populate their PortalSecurityUserAssignmentResource list
	 */
	@PortalSecureMethod
	public PortalSecurityUser getPortalSecurityUser(short id);


	/**
	 * @param validateEmail is used when checking for an existing user of given username/email and not found to validate the supplied username is a valid and supported email
	 *                      address
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUser getPortalSecurityUserByUserName(String username, boolean validateEmail);


	/**
	 * Returns current {@link PortalSecurityUser} object for the given password reset key if it has not expired.
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUser getPortalSecurityUserByPasswordResetKey(String passwordResetKey);


	/**
	 * @param username      the username to check against
	 * @param entityId      the assignment id to validate.  If the assignment exists return false/error.  If the assignment does not exist return the user.
	 * @param validateEmail is used when checking for an existing user of given username/email and not found to validate the supplied username is a valid and supported email
	 *                      address
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUser getPortalSecurityUserByUserNameAndEntityValidateAssignment(String username, Integer entityId, boolean validateEmail);


	/**
	 * Returns current {@link PortalSecurityUser} object associated with current request (thread).
	 */
	@PortalSecureMethod(disableSecurity = true)
	public PortalSecurityUser getPortalSecurityUserCurrent();


	/**
	 * Returns true if the user has at least one assignment where they are a security admin
	 */
	@PortalSecureMethod
	public boolean isPortalSecurityUserSecurityAdmin(PortalSecurityUser securityUser);


	/**
	 * Returns true if the only active assignments the user has are pending termination
	 */
	@PortalSecureMethod
	public boolean isPortalSecurityUserPendingTermination(PortalSecurityUser securityUser);


	@PortalSecureMethod
	public List<PortalSecurityUser> getPortalSecurityUserList(PortalSecurityUserSearchForm searchForm, boolean populateResourcePermissionMap);


	/**
	 * Internal save - not exposed to UI - See {@link PortalSecurityUserManagementService for exposed save method}
	 */
	@DoNotAddRequestMapping
	public PortalSecurityUser savePortalSecurityUser(PortalSecurityUser bean);


	/**
	 * Called from IMS jobs to save user
	 */
	@PortalSecureMethod(adminSecurity = true)
	public PortalSecurityUser savePortalSecurityUserFromSourceSystem(PortalSecurityUser bean);


	////////////////////////////////////////////////////////////////////////////////
	/////////           Portal Security User Assignment Methods            /////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalSecurityUserAssignment getPortalSecurityUserAssignment(int id);


	/**
	 * Returns the assignment for the given user and entity - if checkParents is true then will continue up the entity tree until it finds one
	 * Otherwise will check for an assignment for that specific entity only
	 */
	@DoNotAddRequestMapping
	public PortalSecurityUserAssignment getPortalSecurityUserAssignmentByUserAndEntity(short userId, int entityId, boolean checkParents);


	@PortalSecureMethod
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentListForUser(short userId);


	/**
	 * Gets the list of active {@link PortalSecurityUserAssignment} assignments for which users are actively controlling the given user's access.
	 * <p>
	 * This method finds all {@link PortalSecurityUserAssignment} entities which allow a user to have administrative privileges over any of the entities for which the given user
	 * has an assignment. This effectively gets all users which control the given user's access.
	 *
	 * @param bean    the user for whom administrative assignments should be sought
	 * @param orderBy the {@link BaseSortableSearchForm#orderBy} parameter which should be used in the query
	 * @return the list of administrative assignments which control access for the given user's current assignments
	 */
	@PortalSecureMethod
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentAdministratorListForUser(PortalSecurityUser bean, String orderBy);


	@PortalSecureMethod
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentList(PortalSecurityUserAssignmentSearchForm searchForm, boolean populateResourcePermissionMap);


	@DoNotAddRequestMapping
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentListWithContext(PortalSecurityUserAssignmentSearchForm searchForm, boolean populateResourcePermissionMap, PortalSecurityUserAssignmentRetrievalContext retrievalContext);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUserAssignment savePortalSecurityUserAssignment(PortalSecurityUserAssignment bean);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUserAssignment savePortalSecurityUserAssignmentBulk(PortalSecurityUserAssignment userAssignment, Integer[] additionalEntities);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void deletePortalSecurityUserAssignment(int id);


	/**
	 * Goes through all active assignments, and if any of the entities have been terminated, the assignment is marked as disabled
	 * and the reason is "Terminated"  After all assignments are updated, reviews external users that do not have any active assignments anymore and disables them at the user level
	 * Note: Currently called during IMS batch jobs to clean up assignments after all entities have been updates
	 */
	@PortalSecureMethod(adminSecurity = true)
	public void processPortalSecurityUserAssignmentDisabledList();


	////////////////////////////////////////////////////////////////////////////////
	////////         Portal Security User Assignment Resource Methods       ////////
	////////                           OUR USERS                            ////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void rebuildPortalSecurityUserAssignmentResourceList();


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void rebuildPortalSecurityUserAssignmentResourceListForUser(short securityUserId);


	/**
	 * Called from observers only - rebuilds security for all users in a group when the group permissions are changed
	 */
	@DoNotAddRequestMapping
	public void rebuildPortalSecurityUserAssignmentResourceListForGroup(short securityGroupId);


	////////////////////////////////////////////////////////////////////////////////
	////////        Portal Security User Assignment Resource Methods         ///////
	////////                        EXTERNAL USERS                          ////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForAssignment(PortalSecurityUserAssignment bean, boolean expandSecurity, PortalSecurityUserAssignmentRetrievalContext retrievalContext);


	/**
	 * For an existing assignment and new role returns the list of permissions available.  Will merge existing list of permissions with the new list so we can retain ids and
	 * existing currently selected permissions (if still available). i.e. If a user was a Client User and is being changed to a Custodian, the new list will be limited, but if they
	 * had permissions for anything in that limited list before, it will remained on or off based on what the permissions were
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true, bypassWhenNonPortalEntitySpecific = true)
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForAssignmentAndRole(int userAssignmentId, short roleId);


	/**
	 * For a NEW assignment and role returns the list of permissions available given the portal entity id that would be used on the assignment when saved
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true, bypassWhenNonPortalEntitySpecific = true)
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForEntityAndRole(int portalEntityId, short roleId);
}
