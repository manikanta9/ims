package com.clifton.portal.security.user.group;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portal.security.resource.PortalSecurityResource;


/**
 * The <code>PortalSecurityGroupResource</code> defines the posting access for a portal security group and resource.
 * <p>
 * Portal Security admins are responsible for maintaining group resource access.
 *
 * @author manderson
 */
public class PortalSecurityGroupResource extends BaseEntity<Integer> {

	private PortalSecurityGroup securityGroup;

	private PortalSecurityResource securityResource;

	/**
	 * If a user is in multiple groups, they'll get access if at least one of those groups has access
	 */
	private boolean accessAllowed;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(PortalSecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}


	public PortalSecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(PortalSecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public boolean isAccessAllowed() {
		return this.accessAllowed;
	}


	public void setAccessAllowed(boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}
}
