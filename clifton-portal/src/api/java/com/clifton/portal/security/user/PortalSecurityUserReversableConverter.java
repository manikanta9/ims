package com.clifton.portal.security.user;


import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>PortalSecurityUserReversableConverter</code> class is a ReversableConverter for PortalSecurityUsers
 * <p>
 * It converts Short securityUserId to Display Name strings (Used for exporting grids)
 * and it converts String display name to security user id (Not currently used)
 *
 * @author manderson
 */
public class PortalSecurityUserReversableConverter implements ReversableConverter<String, Short> {

	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// Used when user id = 0 (usually a backend insert or update)
	private static final String DEFAULT_SYSTEM_USER = "System User";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: This is not currently used - Implementation attempts lookup by username/email
	 */
	@Override
	public Short convert(String from) {
		if (from == null) {
			return null;
		}
		if (DEFAULT_SYSTEM_USER.equals(from)) {
			return 0;
		}
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserByUserName(from, false);
		if (user == null) {
			throw new ValidationException("Unable to find portal security user [" + from + "] in the system.");
		}
		return user.getId();
	}


	@Override
	public String reverseConvert(Short to) {
		if (to == null) {
			return null;
		}
		if (MathUtils.isEqual(to, 0)) {
			return DEFAULT_SYSTEM_USER;
		}
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUser(to);
		if (user != null) {
			return user.getLabel();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
