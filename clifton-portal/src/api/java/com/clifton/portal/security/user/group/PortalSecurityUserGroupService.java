package com.clifton.portal.security.user.group;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.group.search.PortalSecurityGroupSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalSecurityUserGroupService {


	////////////////////////////////////////////////////////////////////////////////
	//////////               Portal Security Group Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalSecurityGroup getPortalSecurityGroup(short id);


	/**
	 * Also populates the resource list for the group
	 */
	@PortalSecureMethod
	public PortalSecurityGroup getPortalSecurityGroupExpanded(short id);


	@PortalSecureMethod
	public List<PortalSecurityGroup> getPortalSecurityGroupList(PortalSecurityGroupSearchForm searchForm);


	/**
	 * For groups NOT maintained by an outside source system will save the group and the resources associated with the group
	 * For groups maintained by an outside source system will allow with saveResourceListOnly flag on
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityGroup savePortalSecurityGroup(PortalSecurityGroup securityGroup, boolean saveResourceListOnly);


	/**
	 * When saving from an external system, save both the group and it's membership list
	 * This DOES NOT save resources for the group since that is expected to be maintained via portal only
	 */
	@PortalSecureMethod(adminSecurity = true)
	public PortalSecurityGroup savePortalSecurityGroupFromSourceSystem(PortalSecurityGroup securityGroup, List<PortalSecurityUser> userList);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void deletePortalSecurityGroup(short id);


	@PortalSecureMethod(adminSecurity = true)
	public void deletePortalSecurityGroupFromSourceSystem(short id);


	////////////////////////////////////////////////////////////////////////////////
	/////////             Portal Security User Group Methods            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupList();


	@PortalSecureMethod
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupListForUser(short userId);


	@PortalSecureMethod
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupListForGroup(short groupId);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalSecurityUserGroup savePortalSecurityUserGroup(PortalSecurityUserGroup bean);


	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public void deletePortalSecurityUserGroup(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////           Portal Security Group Resource Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<PortalSecurityGroupResource> getPortalSecurityGroupResourceListForGroup(short groupId, boolean expandSecurity);
}
