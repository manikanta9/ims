package com.clifton.portal.security.user.expanded;

import com.clifton.portal.security.authorization.PortalSecureMethod;


/**
 * The <code>PortalSecurityUserAssignmentExpandedRebuildService</code> handles rebuilding expanded assignments for users
 * that are portal entity specific.
 *
 * @author manderson
 */
public interface PortalSecurityUserAssignmentExpandedRebuildService {

	////////////////////////////////////////////////////////////////////////////////
	///////     Portal Security User Assignment Expanded Rebuild Methods     ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuild All Users
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public void rebuildPortalSecurityUserAssignmentExpanded();


	/**
	 * Rebuild Specific User
	 */
	@PortalSecureMethod(portalAdminSecurity = true)
	public void rebuildPortalSecurityUserAssignmentExpandedForUser(short userId);
}
