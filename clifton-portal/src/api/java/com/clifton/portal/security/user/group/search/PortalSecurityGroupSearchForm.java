package com.clifton.portal.security.user.group.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalSecurityGroupSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "name,description")
	private String searchPattern;


	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "sourceSystem.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean sourceSystemEmpty;

	@SearchField(searchField = "sourceSystem.id", sortField = "sourceSystem.name")
	private Short sourceSystemId;

	@SearchField
	private Integer sourceFkFieldId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getSourceSystemEmpty() {
		return this.sourceSystemEmpty;
	}


	public void setSourceSystemEmpty(Boolean sourceSystemEmpty) {
		this.sourceSystemEmpty = sourceSystemEmpty;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}
}
