package com.clifton.portal.security.search;

/**
 * @author manderson
 */
public interface PortalEntitySpecificSecurityAdminAwareSearchForm extends PortalEntitySpecificAwareSearchForm {


	// Applies that the user has at least one security resource configured.
	// If a user is assigned to the client, in some cases the entity drop down should still allow selecting the relationship (viewing docs)
	// But others (notifications) it shouldn't.  For those cases will confirm user has at least one security resource defined if this is true


	public Boolean getHasSecurityResourceAccess();


	public void setHasSecurityResourceAccess(Boolean hasSecurityResourceAccess);


	// Applies additional security for the VIEWING user (i.e. current user or viewAsUserId) if they are a security administrator for the entity


	public Boolean getViewSecurityAdminRole();


	public void setViewSecurityAdminRole(Boolean viewSecurityAdminRole);
}
