package com.clifton.portal.security.user;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalSecurityUserAssignment</code> is assign a specific user access to a specific entity (account, client, relationship)
 * Security Group can be left blank for client administrators as they have full access to the portal entity
 *
 * @author manderson
 */
@PortalTrackingAuditAware
public class PortalSecurityUserAssignment extends BaseEntity<Integer> implements LabeledObject {

	private PortalSecurityUser securityUser;

	private PortalEntity portalEntity;

	private PortalSecurityUserRole assignmentRole;

	private Date disabledDate;

	private String disabledReason;

	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private List<PortalSecurityUserAssignmentResource> userAssignmentResourceList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getSecurityUser() != null) {
			if (getPortalEntity() != null) {
				return getSecurityUser().getLabel() + " - " + getPortalEntity().getLabel();
			}
			return getSecurityUser().getLabel();
		}
		if (getPortalEntity() != null) {
			return getPortalEntity().getLabel();
		}
		return null;
	}


	public boolean isPendingTermination() {
		if (getPortalEntity() != null) {
			return !isDisabled() && getPortalEntity().isPendingTermination();
		}
		return false;
	}


	public boolean isDisabled() {
		return getDisabledDate() != null && DateUtils.isDateAfterOrEqual(new Date(), getDisabledDate());
	}


	public Map<Short, PortalSecurityUserAssignmentResource> getResourcePermissionMap() {
		return BeanUtils.getBeanMap(getUserAssignmentResourceList(), userAssignmentResource -> (userAssignmentResource != null && userAssignmentResource.getSecurityResource() != null ? userAssignmentResource.getSecurityResource().getId() : null));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(PortalSecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public PortalEntity getPortalEntity() {
		return this.portalEntity;
	}


	public void setPortalEntity(PortalEntity portalEntity) {
		this.portalEntity = portalEntity;
	}


	public List<PortalSecurityUserAssignmentResource> getUserAssignmentResourceList() {
		return this.userAssignmentResourceList;
	}


	public void setUserAssignmentResourceList(List<PortalSecurityUserAssignmentResource> userAssignmentResourceList) {
		this.userAssignmentResourceList = userAssignmentResourceList;
	}


	public PortalSecurityUserRole getAssignmentRole() {
		return this.assignmentRole;
	}


	public void setAssignmentRole(PortalSecurityUserRole assignmentRole) {
		this.assignmentRole = assignmentRole;
	}


	public Date getDisabledDate() {
		return this.disabledDate;
	}


	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}


	public String getDisabledReason() {
		return this.disabledReason;
	}


	public void setDisabledReason(String disabledReason) {
		this.disabledReason = disabledReason;
	}
}
