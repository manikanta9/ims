package com.clifton.portal.security.authorization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>PortalSecureMethod</code> annotation is used to control access to specific methods based on the users role.
 * <p>
 * By Default - no annotation on a method requires at least Portal Administrator access (shouldn't be allowed because there are tests confirm annotation is present)
 * Annotation without properties explicitly set defaults to non portal entity specific users (i.e. any internal user, not clients)
 *
 * @author manderson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface PortalSecureMethod {

	/**
	 * Setting this property to true, disables security check for this method and ignores all other settings.
	 */
	boolean disableSecurity() default false;


	/**
	 * Setting this property to true, will require the user to be Administrator.
	 */
	boolean adminSecurity() default false;


	/**
	 * Setting this property to true, will require the user to be Portal Administrator.
	 */
	boolean portalAdminSecurity() default false;


	/**
	 * Setting this property to true, will require the user to be Portal Security Administrator.
	 */
	boolean portalSecurityAdminSecurity() default false;


	/**
	 * Setting this to true will given internal (non portal entity specific users) access
	 * and will ignore other requirements like portalSecurityAdminSecurity
	 * This is currently used only by Client UI Controllers so our internal users have
	 * read access to the security admin methods (i.e. they can view the assignments those users have access to edit, but they can't edit it themselves)
	 */
	boolean bypassWhenNonPortalEntitySpecific() default false;


	/**
	 * Setting this property to true, will deny access to any user that Is Portal Entity Specific = True
	 * This is basically restricting the method to any user that isn't a client (all PPA/employees)
	 * This is only turned off for client portal requests
	 */
	boolean denyPortalEntitySpecific() default true;
}
