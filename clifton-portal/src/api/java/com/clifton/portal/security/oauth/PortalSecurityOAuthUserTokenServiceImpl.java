package com.clifton.portal.security.oauth;

import com.clifton.core.security.oauth.BaseSecurityOAuthUserTokenServiceImpl;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;


/**
 * @author theodorez
 */
public class PortalSecurityOAuthUserTokenServiceImpl extends BaseSecurityOAuthUserTokenServiceImpl<PortalSecurityUser> {


	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityUser getSecurityUserCurrent() {
		return getPortalSecurityUserService().getPortalSecurityUserCurrent();
	}


	@Override
	public PortalSecurityUser getSecurityUserByName(String userName) {
		return getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
