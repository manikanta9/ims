package com.clifton.portal.security.user;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.PortalEntityViewTypeAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;

import java.util.List;


/**
 * The <code>PortalSecurityUserRole</code> defines the role the user has in the portal and that role is used to determine
 * various access to the system.
 *
 * @author manderson
 */
@CacheByName
@PortalTrackingAuditAware
public class PortalSecurityUserRole extends NamedEntity<Short> implements LabeledObject, PortalEntityViewTypeAware {

	public static final String ROLE_PPA_INTERNAL_USER = "PPA Internal User";
	public static final String ROLE_CLIENT_USER = "Client User";

	/**
	 * Administrators have default access to everything
	 * This would include IT Admins
	 */
	private boolean administrator;

	/**
	 * Portal Administrators have additional write access to the portal setup
	 */
	private boolean portalAdministrator;

	/**
	 * Portal Security Administrators have additional write access to security settings
	 * IF, portalEntitySpecific = true, then they only have write access to security settings for users
	 * within their assigned entities
	 */
	private boolean portalSecurityAdministrator;


	/**
	 * Indicates that security for users in this role MUST be limited to specific portal entities
	 * i.e. Relationships, Clients, Accounts
	 */
	private boolean portalEntitySpecific;

	/**
	 * Indicates this role can only apply to the assignment, not the user.
	 * By Default, all external users will be Client Users and their assignment will have the specific role for that assignment
	 * An assignment can have any role that is portalEntitySpecific = true, but a user can't have an assignment that is assignmentRoleOnly = true
	 */
	private boolean assignmentRoleOnly;

	/**
	 * Only used when portalEntitySpecific = true to limit roles to specific view type = assigned entity's view type
	 * If blank, then applies to any view type
	 */
	private PortalEntityViewType entityViewType;


	/**
	 * If true, then at least one entry is expected in the roleResourceMappingList.
	 * This means the role has limitations on what resources are available for users/assignments in that role
	 * i.e. Custodians can only see Accounting Statements.
	 * Any resource that isn't defined in this list is considered NOT to be allowed.
	 */
	private boolean applyRoleResourceMapping;

	private List<PortalSecurityUserRoleResourceMapping> roleResourceMappingList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAdministrator() {
		return this.administrator;
	}


	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}


	public boolean isPortalAdministrator() {
		return this.portalAdministrator;
	}


	public void setPortalAdministrator(boolean portalAdministrator) {
		this.portalAdministrator = portalAdministrator;
	}


	public boolean isPortalSecurityAdministrator() {
		return this.portalSecurityAdministrator;
	}


	public void setPortalSecurityAdministrator(boolean portalSecurityAdministrator) {
		this.portalSecurityAdministrator = portalSecurityAdministrator;
	}


	public boolean isPortalEntitySpecific() {
		return this.portalEntitySpecific;
	}


	public void setPortalEntitySpecific(boolean portalEntitySpecific) {
		this.portalEntitySpecific = portalEntitySpecific;
	}


	public boolean isAssignmentRoleOnly() {
		return this.assignmentRoleOnly;
	}


	public void setAssignmentRoleOnly(boolean assignmentRoleOnly) {
		this.assignmentRoleOnly = assignmentRoleOnly;
	}


	@Override
	public PortalEntityViewType getEntityViewType() {
		return this.entityViewType;
	}


	public void setEntityViewType(PortalEntityViewType entityViewType) {
		this.entityViewType = entityViewType;
	}


	public boolean isApplyRoleResourceMapping() {
		return this.applyRoleResourceMapping;
	}


	public void setApplyRoleResourceMapping(boolean applyRoleResourceMapping) {
		this.applyRoleResourceMapping = applyRoleResourceMapping;
	}


	public List<PortalSecurityUserRoleResourceMapping> getRoleResourceMappingList() {
		return this.roleResourceMappingList;
	}


	public void setRoleResourceMappingList(List<PortalSecurityUserRoleResourceMapping> roleResourceMappingList) {
		this.roleResourceMappingList = roleResourceMappingList;
	}
}
