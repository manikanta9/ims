package com.clifton.portal.security.user;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.security.SecurityUser;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalSecurityUser</code> defines a user that can login to the client portal
 * You can configure which fields are removed from service requests in a custom appender so we are not sending data we don't need.
 * See com.clifton.portal.converter.json.appenders.PortalSecurityUserValueAppender as an example.
 *
 * @author manderson
 */
@PortalTrackingAuditAware
public class PortalSecurityUser extends BaseEntity<Short> implements LabeledObject, UserDetails, SecurityUser {

	private PortalSecurityUserRole userRole;

	/**
	 * For external users, the user name should be the users email address
	 */
	private String username;

	// Contact Information that we will store
	private String firstName;
	private String lastName;
	private String emailAddress; // For external users this will match the username
	private String title;
	private String companyName;
	private String phoneNumber;


	/**
	 * Encrypted password
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private String password;


	/**
	 * Date password was last updated, so we can enforce changes on an annual basis
	 * When blank, then the password was set by a security admin, and user would be required to reset their password after successful login
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private Date passwordUpdateDate;

	/**
	 * Tracks how many sequential invalid login attempts have been made
	 * After pre-defined amount - account is also locked
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private Short invalidLoginAttemptCount;

	/**
	 * If the user was created from an outside system, like IMS
	 * Passwords and related logic do not apply to these users since we use Crowd/external app for authentication
	 */
	private PortalEntitySourceSystem portalEntitySourceSystem;

	/**
	 * The user id from the source system - i.e. IMS SecurityUserID
	 */
	private Short sourceFKFieldId;


	/**
	 * Accounts are locked when too many incorrect login attempts are made or we can manually lock accounts.
	 * Users are prevented from accessing the system and the only way to re-gain access would be for a security admin to unlock the account
	 */
	private boolean accountLocked;

	/**
	 * We don't delete users so we can retain history, but when a client and/or specific user of that client
	 * is terminated we disable the login.
	 * <p>
	 * At the client level this is done via a batch job once all entities the user has access to have been ended?
	 */
	private boolean disabled;


	/**
	 * Will be required that all external users accept the terms of use. If this date is not set, when they login they'll be redirected to a screen with the terms of use that they
	 * must accept if they do not accept they won't be able to do anything in the portal (similar to requiring to reset password, the popup will keep showing and not let them get
	 * anywhere else)
	 */
	@PortalTrackingAuditColumnConfig(doNotAuditUpdateFromNull = true)
	private Date termsOfUseAcceptanceDate;


	/**
	 * Used for For PPA users (portal entity specific = false) - and defines what security resources they have access to post files to
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private List<PortalSecurityUserAssignmentResource> userAssignmentResourceList;


	/**
	 * An auto generated key that can be used to reset a users password.
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private String passwordResetKey;

	/**
	 * The time that the password reset key will expire.
	 */
	@PortalTrackingAuditColumnConfig(doNotAudit = true)
	private Date passwordResetKeyExpirationDateAndTime;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<Short, PortalSecurityUserAssignmentResource> getResourcePermissionMap() {
		return BeanUtils.getBeanMap(getUserAssignmentResourceList(), userAssignmentResource -> (userAssignmentResource != null && userAssignmentResource.getSecurityResource() != null ? userAssignmentResource.getSecurityResource().getId() : null));
	}


	@Override
	public String getUserName() {
		return getUsername();
	}


	@Override
	public String getLabel() {
		return this.firstName + " " + this.lastName;
	}


	public String getLabelLong() {
		return getLabel() + " (" + getUsername() + ")";
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}


	@Override
	public boolean isEnabled() {
		return !isDisabled();
	}


	@Override
	public boolean isAccountNonLocked() {
		return !isAccountLocked();
	}


	@Override
	public boolean isAccountNonExpired() {
		return !isAccountLocked();
	}


	@Override
	public boolean isCredentialsNonExpired() {
		// If password is expired, we still allow the user to authenticate
		// but there is a filter the will re-direct them to the change password window
		return true;
	}


	public boolean isPasswordResetRequired() {
		// Passwords are only used if No Source System
		if (getPortalEntitySourceSystem() == null) {
			// Internal Policy dictates quarterly, however for clients will make it annual
			return (getPasswordUpdateDate() == null || DateUtils.isDateAfter(new Date(), DateUtils.addYears(getPasswordUpdateDate(), 1)));
		}
		return false;
	}


	public boolean isTermsOfUseAcceptanceRequired() {
		// Terms of Use are only used if No Source System
		if (getPortalEntitySourceSystem() == null) {
			return getTermsOfUseAcceptanceDate() == null;
		}
		return false;
	}


	public boolean isTermsOfUseAccepted() {
		return getTermsOfUseAcceptanceDate() != null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserRole getUserRole() {
		return this.userRole;
	}


	public void setUserRole(PortalSecurityUserRole userRole) {
		this.userRole = userRole;
	}


	@Override
	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	@Override
	public String getPassword() {
		return this.password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public boolean isAccountLocked() {
		return this.accountLocked;
	}


	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public PortalEntitySourceSystem getPortalEntitySourceSystem() {
		return this.portalEntitySourceSystem;
	}


	public void setPortalEntitySourceSystem(PortalEntitySourceSystem portalEntitySourceSystem) {
		this.portalEntitySourceSystem = portalEntitySourceSystem;
	}


	public Short getSourceFKFieldId() {
		return this.sourceFKFieldId;
	}


	public void setSourceFKFieldId(Short sourceFKFieldId) {
		this.sourceFKFieldId = sourceFKFieldId;
	}


	public Date getPasswordUpdateDate() {
		return this.passwordUpdateDate;
	}


	public void setPasswordUpdateDate(Date passwordUpdateDate) {
		this.passwordUpdateDate = passwordUpdateDate;
	}


	public Short getInvalidLoginAttemptCount() {
		return this.invalidLoginAttemptCount;
	}


	public void setInvalidLoginAttemptCount(Short invalidLoginAttemptCount) {
		this.invalidLoginAttemptCount = invalidLoginAttemptCount;
	}


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public List<PortalSecurityUserAssignmentResource> getUserAssignmentResourceList() {
		return this.userAssignmentResourceList;
	}


	public void setUserAssignmentResourceList(List<PortalSecurityUserAssignmentResource> userAssignmentResourceList) {
		this.userAssignmentResourceList = userAssignmentResourceList;
	}


	@Override
	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public Date getTermsOfUseAcceptanceDate() {
		return this.termsOfUseAcceptanceDate;
	}


	public void setTermsOfUseAcceptanceDate(Date termsOfUseAcceptanceDate) {
		this.termsOfUseAcceptanceDate = termsOfUseAcceptanceDate;
	}


	public String getPasswordResetKey() {
		return this.passwordResetKey;
	}


	public void setPasswordResetKey(String passwordResetKey) {
		this.passwordResetKey = passwordResetKey;
	}


	public Date getPasswordResetKeyExpirationDateAndTime() {
		return this.passwordResetKeyExpirationDateAndTime;
	}


	public void setPasswordResetKeyExpirationDateAndTime(Date passwordResetKeyExpirationDateAndTime) {
		this.passwordResetKeyExpirationDateAndTime = passwordResetKeyExpirationDateAndTime;
	}
}
