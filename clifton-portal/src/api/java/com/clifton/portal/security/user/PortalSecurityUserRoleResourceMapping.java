package com.clifton.portal.security.user;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portal.security.resource.PortalSecurityResource;


/**
 * The <code>PortalSecurityUserRoleResourceMapping</code> table is used for roles that need to limit the resources
 * for a given {@link PortalSecurityUserRole}
 * <p>
 * Most cases, this is not used, so when it is the role itself would have IsRoleResourceMappingExists = true so the system knows to retrieve the list
 * and limit the user's assignment resources.   Currently, the only use case is the Custodian
 *
 * @author manderson
 */
public class PortalSecurityUserRoleResourceMapping extends BaseEntity<Short> {

	/**
	 * The user role this mapping is for
	 */
	private PortalSecurityUserRole securityUserRole;

	/**
	 * The resource that is allowed for this mapping
	 */
	private PortalSecurityResource securityResource;


	/**
	 * If access is allowed (If mapping doesn't exist for a resource and it's not mapped at a parent level, then it would not be allowed)
	 */
	private boolean accessAllowed;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserRole getSecurityUserRole() {
		return this.securityUserRole;
	}


	public void setSecurityUserRole(PortalSecurityUserRole securityUserRole) {
		this.securityUserRole = securityUserRole;
	}


	public PortalSecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(PortalSecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public boolean isAccessAllowed() {
		return this.accessAllowed;
	}


	public void setAccessAllowed(boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}
}
