package com.clifton.portal.security.user.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalSecurityUserRoleSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;


	@SearchField
	private Boolean administrator;

	@SearchField
	private Boolean portalAdministrator;

	@SearchField
	private Boolean portalSecurityAdministrator;

	@SearchField
	private Boolean portalEntitySpecific;

	@SearchField
	private Boolean assignmentRoleOnly;

	@SearchField(searchField = "entityViewType.id")
	private Short entityViewTypeId;

	@SearchField(searchField = "entityViewType.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short entityViewTypeIdOrNull;

	// Custom Search Field where role entity View type is null or role entity view type = Portal Entity View Type
	// Used to limit selections when creating/editing user assignments
	// Looks up the entity and then sets that view type as the entityViewTypeIdOrNull search form value
	private Integer assignmentForPortalEntityId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getAdministrator() {
		return this.administrator;
	}


	public void setAdministrator(Boolean administrator) {
		this.administrator = administrator;
	}


	public Boolean getPortalAdministrator() {
		return this.portalAdministrator;
	}


	public void setPortalAdministrator(Boolean portalAdministrator) {
		this.portalAdministrator = portalAdministrator;
	}


	public Boolean getPortalSecurityAdministrator() {
		return this.portalSecurityAdministrator;
	}


	public void setPortalSecurityAdministrator(Boolean portalSecurityAdministrator) {
		this.portalSecurityAdministrator = portalSecurityAdministrator;
	}


	public Boolean getPortalEntitySpecific() {
		return this.portalEntitySpecific;
	}


	public void setPortalEntitySpecific(Boolean portalEntitySpecific) {
		this.portalEntitySpecific = portalEntitySpecific;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public Boolean getAssignmentRoleOnly() {
		return this.assignmentRoleOnly;
	}


	public void setAssignmentRoleOnly(Boolean assignmentRoleOnly) {
		this.assignmentRoleOnly = assignmentRoleOnly;
	}


	public Short getEntityViewTypeId() {
		return this.entityViewTypeId;
	}


	public void setEntityViewTypeId(Short entityViewTypeId) {
		this.entityViewTypeId = entityViewTypeId;
	}


	public Short getEntityViewTypeIdOrNull() {
		return this.entityViewTypeIdOrNull;
	}


	public void setEntityViewTypeIdOrNull(Short entityViewTypeIdOrNull) {
		this.entityViewTypeIdOrNull = entityViewTypeIdOrNull;
	}


	public Integer getAssignmentForPortalEntityId() {
		return this.assignmentForPortalEntityId;
	}


	public void setAssignmentForPortalEntityId(Integer assignmentForPortalEntityId) {
		this.assignmentForPortalEntityId = assignmentForPortalEntityId;
	}
}
