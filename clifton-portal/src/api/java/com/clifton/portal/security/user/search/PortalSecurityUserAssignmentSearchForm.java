package com.clifton.portal.security.user.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.portal.security.search.PortalEntitySpecificSecurityAdminAwareSearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalSecurityUserAssignmentSearchForm extends BaseEntitySearchForm implements PortalEntitySpecificSecurityAdminAwareSearchForm {

	@SearchField(searchField = "portalEntity.label,assignmentRole.name,securityUser.username,securityUser.firstName,securityUser.lastName,securityUser.emailAddress")
	private String searchPattern;

	@SearchField(searchField = "securityUser.id")
	private Short portalSecurityUserId;

	@SearchField(searchField = "securityUser.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludePortalSecurityUserId;

	@SearchField(searchFieldPath = "securityUser", searchField = "username,firstName,lastName,title,phoneNumber,companyName,emailAddress")
	private String userSearchPattern;

	@SearchField(searchFieldPath = "securityUser", searchField = "lastName")
	private String userLastName;

	@SearchField(searchFieldPath = "securityUser", searchField = "firstName")
	private String userFirstName;

	@SearchField(searchFieldPath = "securityUser", searchField = "firstName,lastName", searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT)
	private String userFirstLastName;

	@SearchField(searchFieldPath = "securityUser", searchField = "username")
	private String username;

	@SearchField(searchFieldPath = "securityUser", searchField = "emailAddress")
	private String emailAddress;

	@SearchField(searchFieldPath = "securityUser", searchField = "title")
	private String userTitle;

	@SearchField(searchFieldPath = "securityUser", searchField = "phoneNumber")
	private String userPhoneNumber;

	@SearchField(searchFieldPath = "securityUser", searchField = "companyName")
	private String userCompanyName;

	@SearchField(searchField = "assignmentRole.id", sortField = "assignmentRole.name")
	private Short assignmentRoleId;

	@SearchField(searchFieldPath = "assignmentRole", searchField = "portalSecurityAdministrator")
	private Boolean assignmentRolePortalSecurityAdministrator;

	@SearchField(searchFieldPath = "portalEntity", searchField = "entityViewType.id")
	private Short entityViewTypeId;

	@SearchField(searchFieldPath = "portalEntity", searchField = "entityViewType.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short entityViewTypeIdNotEquals;

	@SearchField(searchField = "portalEntity.id")
	private Integer portalEntityId;

	@SearchField(searchField = "portalEntity.name")
	private String portalEntityName;

	@SearchField(searchField = "portalEntity.parentPortalEntity.parentPortalEntity.label,portalEntity.parentPortalEntity.label,portalEntity.label", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String portalEntityLabelExpanded;

	// Custom Search Field that Will Populate portalEntityIds with selected entity Id, all parent entity ids, and all children ids
	private Integer portalEntityIdOrRelatedEntity;

	// Custom Search Field that Will Populate portalEntityIds with selected entity Id, all parent entity ids, and all children ids
	private Integer[] portalEntityIdsOrRelatedEntities;

	@SearchField(searchField = "portalEntity.id")
	private Integer[] portalEntityIds;

	@SearchField(searchFieldPath = "portalEntity.entitySourceSection", searchField = "name")
	private String portalEntitySourceSection;

	// Custom Search Field for Viewing As User
	private Short viewAsUserId;

	// Custom Search Field for Viewing As Entity
	private Integer viewAsEntityId;

	// Applies additional security for the "VIEWING" user if they are a security administrator for the entity
	private Boolean viewSecurityAdminRole;

	// Applies additional security for the VIEWING user if they have at least one security resource mapped.
	private Boolean hasSecurityResourceAccess;

	@SearchField(searchFieldPath = "securityUser", searchField = "termsOfUseAcceptanceDate")
	private Date termsOfUseAcceptanceDate;

	@SearchField(searchFieldPath = "securityUser", searchField = "termsOfUseAcceptanceDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean termsOfUseAccepted;


	// Custom Search Field True if NOT Disabled and PortalEntity is Pending Termination - false if NOT true expression
	private Boolean pendingTermination;

	// Custom Search Field True if DisabledDate <= Today, False if Disabled Date is Null or After Today
	private Boolean disabled;

	@SearchField
	private Date disabledDate;

	@SearchField
	private String disabledReason;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getPortalSecurityUserId() {
		return this.portalSecurityUserId;
	}


	public void setPortalSecurityUserId(Short portalSecurityUserId) {
		this.portalSecurityUserId = portalSecurityUserId;
	}


	public Short getExcludePortalSecurityUserId() {
		return this.excludePortalSecurityUserId;
	}


	public void setExcludePortalSecurityUserId(Short excludePortalSecurityUserId) {
		this.excludePortalSecurityUserId = excludePortalSecurityUserId;
	}


	public String getUserSearchPattern() {
		return this.userSearchPattern;
	}


	public void setUserSearchPattern(String userSearchPattern) {
		this.userSearchPattern = userSearchPattern;
	}


	public String getUserLastName() {
		return this.userLastName;
	}


	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}


	public String getUserFirstName() {
		return this.userFirstName;
	}


	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}


	public String getUserFirstLastName() {
		return this.userFirstLastName;
	}


	public void setUserFirstLastName(String userFirstLastName) {
		this.userFirstLastName = userFirstLastName;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getUserTitle() {
		return this.userTitle;
	}


	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}


	public String getUserPhoneNumber() {
		return this.userPhoneNumber;
	}


	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}


	public String getUserCompanyName() {
		return this.userCompanyName;
	}


	public void setUserCompanyName(String userCompanyName) {
		this.userCompanyName = userCompanyName;
	}


	public Short getAssignmentRoleId() {
		return this.assignmentRoleId;
	}


	public void setAssignmentRoleId(Short assignmentRoleId) {
		this.assignmentRoleId = assignmentRoleId;
	}


	public Boolean getAssignmentRolePortalSecurityAdministrator() {
		return this.assignmentRolePortalSecurityAdministrator;
	}


	public void setAssignmentRolePortalSecurityAdministrator(Boolean assignmentRolePortalSecurityAdministrator) {
		this.assignmentRolePortalSecurityAdministrator = assignmentRolePortalSecurityAdministrator;
	}


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}


	public String getPortalEntityName() {
		return this.portalEntityName;
	}


	public void setPortalEntityName(String portalEntityName) {
		this.portalEntityName = portalEntityName;
	}


	public String getPortalEntityLabelExpanded() {
		return this.portalEntityLabelExpanded;
	}


	public void setPortalEntityLabelExpanded(String portalEntityLabelExpanded) {
		this.portalEntityLabelExpanded = portalEntityLabelExpanded;
	}


	public Integer getPortalEntityIdOrRelatedEntity() {
		return this.portalEntityIdOrRelatedEntity;
	}


	public void setPortalEntityIdOrRelatedEntity(Integer portalEntityIdOrRelatedEntity) {
		this.portalEntityIdOrRelatedEntity = portalEntityIdOrRelatedEntity;
	}


	public Integer[] getPortalEntityIdsOrRelatedEntities() {
		return this.portalEntityIdsOrRelatedEntities;
	}


	public void setPortalEntityIdsOrRelatedEntities(Integer[] portalEntityIdsOrRelatedEntities) {
		this.portalEntityIdsOrRelatedEntities = portalEntityIdsOrRelatedEntities;
	}


	public Integer[] getPortalEntityIds() {
		return this.portalEntityIds;
	}


	public void setPortalEntityIds(Integer[] portalEntityIds) {
		this.portalEntityIds = portalEntityIds;
	}


	public String getPortalEntitySourceSection() {
		return this.portalEntitySourceSection;
	}


	public void setPortalEntitySourceSection(String portalEntitySourceSection) {
		this.portalEntitySourceSection = portalEntitySourceSection;
	}


	@Override
	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	@Override
	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	@Override
	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	@Override
	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}


	@Override
	public Boolean getViewSecurityAdminRole() {
		return this.viewSecurityAdminRole;
	}


	@Override
	public void setViewSecurityAdminRole(Boolean viewSecurityAdminRole) {
		this.viewSecurityAdminRole = viewSecurityAdminRole;
	}


	@Override
	public Boolean getHasSecurityResourceAccess() {
		return this.hasSecurityResourceAccess;
	}


	@Override
	public void setHasSecurityResourceAccess(Boolean hasSecurityResourceAccess) {
		this.hasSecurityResourceAccess = hasSecurityResourceAccess;
	}


	public Date getTermsOfUseAcceptanceDate() {
		return this.termsOfUseAcceptanceDate;
	}


	public void setTermsOfUseAcceptanceDate(Date termsOfUseAcceptanceDate) {
		this.termsOfUseAcceptanceDate = termsOfUseAcceptanceDate;
	}


	public Boolean getTermsOfUseAccepted() {
		return this.termsOfUseAccepted;
	}


	public void setTermsOfUseAccepted(Boolean termsOfUseAccepted) {
		this.termsOfUseAccepted = termsOfUseAccepted;
	}


	public Boolean getPendingTermination() {
		return this.pendingTermination;
	}


	public void setPendingTermination(Boolean pendingTermination) {
		this.pendingTermination = pendingTermination;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Date getDisabledDate() {
		return this.disabledDate;
	}


	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}


	public String getDisabledReason() {
		return this.disabledReason;
	}


	public void setDisabledReason(String disabledReason) {
		this.disabledReason = disabledReason;
	}


	public Short getEntityViewTypeId() {
		return this.entityViewTypeId;
	}


	public void setEntityViewTypeId(Short entityViewTypeId) {
		this.entityViewTypeId = entityViewTypeId;
	}


	public Short getEntityViewTypeIdNotEquals() {
		return this.entityViewTypeIdNotEquals;
	}


	public void setEntityViewTypeIdNotEquals(Short entityViewTypeIdNotEquals) {
		this.entityViewTypeIdNotEquals = entityViewTypeIdNotEquals;
	}
}
