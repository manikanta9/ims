package com.clifton.portal.email;

import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PortalEmail</code> is a virtual object generated when a user needs to contact us.  Based on specified parameters
 * the To and CC are usually some combination of the team email, Relationship manager, and/or category email address.
 * <p>
 * The subject is attempted to also auto generated based on the current view parameters.
 * <p>
 * If the system cannot find a contact, the default contact email InstutionalCenter@paraport.com will be used
 *
 * @author manderson
 */
public class PortalEmail {

	private List<String> toAddressList = new ArrayList<>();

	private List<String> ccAddressList = new ArrayList<>();

	private String subject;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public void addEmailAddress(String email, boolean to) {
		if (to) {
			if (!this.toAddressList.contains(email)) {
				this.toAddressList.add(email);
			}
			if (this.ccAddressList.contains(email)) {
				this.ccAddressList.remove(email);
			}
		}
		else if (!this.toAddressList.contains(email) && !this.ccAddressList.contains(email)) {
			this.ccAddressList.add(email);
		}
	}


	public String getToAddress() {
		return StringUtils.collectionToDelimitedString(getToAddressList(), ";");
	}


	public String getCcAddress() {
		return StringUtils.collectionToDelimitedString(getCcAddressList(), ";");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<String> getToAddressList() {
		return this.toAddressList;
	}


	public void setToAddressList(List<String> toAddressList) {
		this.toAddressList = toAddressList;
	}


	public List<String> getCcAddressList() {
		return this.ccAddressList;
	}


	public void setCcAddressList(List<String> ccAddressList) {
		this.ccAddressList = ccAddressList;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}
}
