package com.clifton.portal.email;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * The <code>PortalEmailService</code> is used to generate an {@link PortalEmail} that is used by UI
 * to open user's email application/outlook to send an email to us.
 *
 * @author manderson
 */
public interface PortalEmailService {


	/**
	 * Returns the current default email address for the default view (Currently: InstitutionalCenter@paraport.com)
	 */
	@DoNotAddRequestMapping
	public String getPortalEmailAddressDefault();


	/**
	 * Returns the PortalEmail object populated with To and CC email addresses as well as a generated subject line
	 * This is used to popup an email to send to "us" when the client clicks contact us.
	 */
	@PortalSecureMethod
	public PortalEmail getPortalEmailForCategoryAndEntity(short categoryId, Integer portalEntityId);


	/**
	 * Used for internal emails (Files Pending Approval) to determine the recipient of the email for the given category and portal entity (where the file was posted to)
	 * We send these emails to mailing lists, so we expect one email address only to apply, i.e. DocumentationTeam@paraport.com, TeamPIOS@paraport.com, etc.
	 * There should be only one for each file
	 * Returns the getPortalEmailAddressDefault if none found
	 */
	@DoNotAddRequestMapping
	public List<String> getPortalSupportEmailAddressListForPortalFile(PortalFile portalFile);


	/**
	 * Used when getting the list of Relationship managers to dynamically add the teams that need to be cc'd when a user clicks on a specific relationship manager to email
	 * There usually is only be one returned, but could have multiple
	 */
	@DoNotAddRequestMapping
	public List<String> getPortalContactTeamEmailAddressListForPortalFile(PortalFileExtended portalFile);


	/**
	 * Returns the list of all emails used for Portal Support Teams.  This includes Portal File Category email addresses, Investment Teams, and Portal Entity Field Overrides
	 */
	@PortalSecureMethod
	public List<String> getPortalSupportTeamEmailList();


	@DoNotAddRequestMapping
	public List<PortalEntity> getRelationshipManagerList(int portalEntityId);
}
