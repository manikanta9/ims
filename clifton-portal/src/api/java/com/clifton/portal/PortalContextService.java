package com.clifton.portal;

import com.clifton.core.cache.CacheStats;
import com.clifton.core.context.spring.UrlDefinitionDescriptor;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.logging.configuration.LogAppender;
import com.clifton.core.logging.configuration.LogConfiguration;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.util.runner.config.RunnerConfig;
import com.clifton.core.util.runner.config.RunnerConfigSearchForm;
import com.clifton.core.web.stats.MemoryState;
import com.clifton.core.web.stats.SystemRequestStats;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>PortalContextService</code> interface exposes api methods to allow seeing data from context of the Portal app.
 * For example, seeing the URLs available to call, cache handlers, request stats, and runners
 *
 * @author manderson
 */
public interface PortalContextService {


	/**
	 * Returns a List of all URL mapping definitions identified by @RequestMapping.
	 */
	public List<UrlDefinitionDescriptor> getPortalContextUrlMappingDefinitionList();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	public String getPortalJsonMigrationAction(String entityTableName, Integer entityFkFieldId) throws Exception;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a Collection of all Portal SystemRequestStats objects.
	 */
	public List<SystemRequestStats> getPortalSystemRequestStatsList(AuditableEntitySearchForm searchForm);


	/**
	 * Removes request stats from PORTAL APP for the specified source and URI from the cache.
	 * Can be used to cleanup invalid data.
	 */
	public void deletePortalSystemRequestStats(String requestSource, String requestURI);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int setPortalSystemCacheStatsEnabled(String cacheName, boolean enableStatistics);


	public List<CacheStats> getPortalSystemCacheStats(boolean calculateSize);


	public void deletePortalSystemCache(String cacheName);


	public MemoryState getPortalSystemMemoryState();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	public List<RunnerConfig> getPortalRunnerConfigList(RunnerConfigSearchForm searchForm);


	@RequestMapping("portalRunnerControllerReschedule")
	public void rescheduleRunnersFromProviders();


	@RequestMapping("portalRunnerControllerRestart")
	public void restartPortalRunnerHandler();


	@RequestMapping("portalRunnerCompletedClear")
	public void clearCompletedPortalRunners();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalAdminSecurity = true)
	@RequestMapping("portalOauthClientDetail")
	public SecurityOAuthClientDetail getPortalClientDetailsFromFile(String id);


	@PortalSecureMethod(portalAdminSecurity = true)
	@RequestMapping("portalOauthClientDetailDelete")
	public SecurityOAuthClientDetail deletePortalClientDetailFromFiles(String id);


	@PortalSecureMethod(portalAdminSecurity = true)
	@RequestMapping("portalOauthClientDetailList")
	public List<SecurityOAuthClientDetail> getPortalClientDetailsList();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public LogConfiguration savePortalLogConfigurationLogAppenderLink(String loggerName, String appenderName);


	public LogConfiguration deletePortalLogConfigurationLogAppenderLink(String loggerName, String appenderName);


	public LogAppender getPortalLogAppender(String name);


	public List<LogAppender> getPortalLogAppenderList();


	public List<LogAppender> getPortalLogAppenderListForLogConfiguration(String loggerName);


	public LogConfiguration getPortalLogConfiguration(String name);


	public LogConfiguration savePortalLogConfiguration(LogConfiguration configuration);


	public LogConfiguration deletePortalLogConfiguration(String name);


	public List<LogConfiguration> getPortalLogConfigurationList();
}
