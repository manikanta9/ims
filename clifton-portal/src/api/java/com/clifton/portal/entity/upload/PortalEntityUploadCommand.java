package com.clifton.portal.entity.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.core.util.status.Status;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.entity.setup.PortalEntityViewType;

import java.util.Map;


/**
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortalEntityUploadCommand extends FileUploadCommand<PortalEntityUploadTemplate> {

	public static final String[] REQUIRED_PROPERTIES = {"portalEntityTypeName", "portalEntitySourceSectionName", "sourceFKFieldId", "entityName"};


	/**
	 * The source system for the upload. Some entities in the file may reference another source system, if so they can still be used but won't be modified
	 */
	private PortalEntitySourceSystem sourceSystem;

	/**
	 * If true, then the list in the upload file is considered to be a complete list and anything not in the file for any source section listed would be deleted
	 * If false, then anything not in the list won't be updated. i.e. Salesforce will stop sending terminated accounts after x days, however we don't want to attempt to delete them from the portal
	 */
	private boolean deleteEntitiesMissingFromFile;


	/**
	 * Default entity view type for the entities in the file (can also be supplied in the file)
	 * If NOT in the file, but set here, this value will only be set on securable entity types since that is the only time this is used.
	 */
	private PortalEntityViewType entityViewType;


	/**
	 * For the securable entities (Client Relationships, Clients, etc) this field is available.
	 * For cases like Salesforce the value is the same for all, so instead of including this in the file for each row allow defaulting the value here.
	 */
	private String portalContactTeamEmail;


	/**
	 * For the securable entities (Client Relationships, Clients, etc) this field is available.
	 * For cases like Salesforce the value is the same for all, so instead of including this in the file for each row allow defaulting the value here.
	 */
	private String portalSupportTeamEmail;


	/**
	 * Status object to track status and details of the upload that is performed.
	 */
	private Status status = Status.ofEmptyMessage();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<PortalEntityUploadTemplate> getUploadBeanClass() {
		return PortalEntityUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	@Override
	public void applyUnmappedColumnValues(PortalEntityUploadTemplate bean, Map<String, Object> unmappedColumnValueMap) {
		bean.setUnmappedColumnValueMap(unmappedColumnValueMap);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(PortalEntitySourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public PortalEntityViewType getEntityViewType() {
		return this.entityViewType;
	}


	public void setEntityViewType(PortalEntityViewType entityViewType) {
		this.entityViewType = entityViewType;
	}


	public boolean isDeleteEntitiesMissingFromFile() {
		return this.deleteEntitiesMissingFromFile;
	}


	public void setDeleteEntitiesMissingFromFile(boolean deleteEntitiesMissingFromFile) {
		this.deleteEntitiesMissingFromFile = deleteEntitiesMissingFromFile;
	}


	public String getPortalContactTeamEmail() {
		return this.portalContactTeamEmail;
	}


	public void setPortalContactTeamEmail(String portalContactTeamEmail) {
		this.portalContactTeamEmail = portalContactTeamEmail;
	}


	public String getPortalSupportTeamEmail() {
		return this.portalSupportTeamEmail;
	}


	public void setPortalSupportTeamEmail(String portalSupportTeamEmail) {
		this.portalSupportTeamEmail = portalSupportTeamEmail;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
