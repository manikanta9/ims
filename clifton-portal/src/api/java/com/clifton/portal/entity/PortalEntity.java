package com.clifton.portal.entity;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.PortalEntityViewTypeAware;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;

import java.io.File;
import java.util.Date;
import java.util.List;


/**
 * The <code>PortalEntity</code> represents a specific entity/row of data from the source system
 * i.e. Client Account 123, Invoice 987, Business Service: PIOS (Overlay)
 *
 * @author manderson
 */
@PortalTrackingAuditAware(insert = false)
public class PortalEntity extends NamedEntity<Integer> implements PortalEntityViewTypeAware {

	private PortalEntity parentPortalEntity;

	private PortalEntityViewType entityViewType;

	private PortalEntitySourceSection entitySourceSection;

	private Integer sourceFkFieldId;

	private Date startDate;

	private Date endDate;

	/**
	 * For securable entities the external systems will set the termination date
	 * The end date is usually set to a specified number of days (i.e. 120) after the termination date
	 * to allow clients to continue to view their documents for a period of time during the termination phase.
	 */
	private Date terminationDate;

	/**
	 * This is used by IMS batch jobs to flag an entity as deleted.
	 * Because of hierarchical structure or usage for security/posting files we can't
	 * always delete items immediately.  So, the batch jobs flag items as being deleted in the source system
	 * and then a clean up job will actually delete those that can be deleted because they aren't used.
	 * <p>
	 * By Default, deleted items are excluded from UI searches
	 */
	private boolean deleted;

	/**
	 * For Postable Entities - where the files are posted.
	 * If the entity has a parent, it is nested within the parent's folder
	 */
	private String folderName;


	private List<PortalEntityFieldValue> fieldValueList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	/**
	 * Entity is still active, but termination date is set
	 */
	public boolean isPendingTermination() {
		return isActive() && getTerminationDate() != null;
	}


	public String getEntityLabelWithSourceSection() {
		if (getEntitySourceSection() != null) {
			return getEntitySourceSection().getLabel() + ": " + getLabel();
		}
		return getLabel();
	}


	public String getLabelExpanded() {
		String parentLabel = "";
		if (getParentPortalEntity() != null) {
			parentLabel = getParentPortalEntity().getLabelExpanded();
		}
		if (StringUtils.isEmpty(getLabel())) {
			return parentLabel;
		}
		return parentLabel + getLabel() + NamedHierarchicalEntity.HIERARCHY_DELIMITER;
	}


	public PortalEntity getRootParent() {
		if (getParentPortalEntity() == null) {
			return this;
		}
		return getParentPortalEntity().getRootParent();
	}


	public Integer getLevel() {
		int level = 1;
		if (getParentPortalEntity() != null) {
			return getParentPortalEntity().getLevel() + 1;
		}
		return level;
	}


	public String getFolderNameExpanded() {
		String parentFolderName = "";
		if (getParentPortalEntity() != null) {
			parentFolderName = getParentPortalEntity().getFolderNameExpanded();
		}
		if (StringUtils.isEmpty(getFolderName())) {
			return parentFolderName;
		}
		return parentFolderName + getFolderName() + File.separator;
	}

	////////////////////////////////////////////////////////////////////////////////


	public PortalEntity getParentPortalEntity() {
		return this.parentPortalEntity;
	}


	public void setParentPortalEntity(PortalEntity parentPortalEntity) {
		this.parentPortalEntity = parentPortalEntity;
	}


	@Override
	public PortalEntityViewType getEntityViewType() {
		return this.entityViewType;
	}


	public void setEntityViewType(PortalEntityViewType entityViewType) {
		this.entityViewType = entityViewType;
	}


	public PortalEntitySourceSection getEntitySourceSection() {
		return this.entitySourceSection;
	}


	public void setEntitySourceSection(PortalEntitySourceSection entitySourceSection) {
		this.entitySourceSection = entitySourceSection;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getTerminationDate() {
		return this.terminationDate;
	}


	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}


	public List<PortalEntityFieldValue> getFieldValueList() {
		return this.fieldValueList;
	}


	public void setFieldValueList(List<PortalEntityFieldValue> fieldValueList) {
		this.fieldValueList = fieldValueList;
	}


	public String getFolderName() {
		return this.folderName;
	}


	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public boolean isDeleted() {
		return this.deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
