package com.clifton.portal.entity.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalEntityTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean postableEntity;

	@SearchField
	private Boolean securableEntity;

	@SearchField
	private Boolean fileSource;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getPostableEntity() {
		return this.postableEntity;
	}


	public void setPostableEntity(Boolean postableEntity) {
		this.postableEntity = postableEntity;
	}


	public Boolean getSecurableEntity() {
		return this.securableEntity;
	}


	public void setSecurableEntity(Boolean securableEntity) {
		this.securableEntity = securableEntity;
	}


	public Boolean getFileSource() {
		return this.fileSource;
	}


	public void setFileSource(Boolean fileSource) {
		this.fileSource = fileSource;
	}
}
