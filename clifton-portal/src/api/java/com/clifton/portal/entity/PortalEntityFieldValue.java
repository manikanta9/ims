package com.clifton.portal.entity;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The <code>PortalEntityFieldValue</code> defines the specific value for the field type and entity
 * i.e. Contact First Name = Mary, Invoice Status = Paid
 *
 * @author manderson
 */
@PortalTrackingAuditAware(insert = false)
public class PortalEntityFieldValue extends BaseEntity<Integer> implements LabeledObject {

	private PortalEntityFieldType portalEntityFieldType;

	private PortalEntity portalEntity;

	private String value;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder(16);
		if (getPortalEntity() != null) {
			label.append(getPortalEntity().getLabel());
			label.append(" - ");
		}
		if (getPortalEntityFieldType() != null) {
			label.append(getPortalEntityFieldType().getLabel());
		}
		return label.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityFieldType getPortalEntityFieldType() {
		return this.portalEntityFieldType;
	}


	public void setPortalEntityFieldType(PortalEntityFieldType portalEntityFieldType) {
		this.portalEntityFieldType = portalEntityFieldType;
	}


	public PortalEntity getPortalEntity() {
		return this.portalEntity;
	}


	public void setPortalEntity(PortalEntity portalEntity) {
		this.portalEntity = portalEntity;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}
}
