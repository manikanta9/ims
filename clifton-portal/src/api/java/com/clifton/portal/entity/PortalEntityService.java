package com.clifton.portal.entity;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface PortalEntityService {


	////////////////////////////////////////////////////////////////////////////////
	////////////                 Portal Entity Methods                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntity getPortalEntity(int id);


	@PortalSecureMethod
	public PortalEntity getPortalEntityBySourceSection(int portalEntitySourceSectionId, int sourceFkFieldId, boolean populateFieldValues);


	@PortalSecureMethod
	public PortalEntity getPortalEntityBySourceTable(String sourceSystemName, String sourceTableName, int sourceFkFieldId, boolean populateFieldValues);


	@PortalSecureMethod
	public List<PortalEntity> getPortalEntityList(PortalEntitySearchForm searchForm);


	/**
	 * Returns a list of Portal Entities related to the given entity.
	 *
	 * @param includeSelf     - Include PortalEntity with Id portalEntityId in the list
	 * @param includeParents  - Include all parents, grandparents, etc. in the list
	 * @param includeChildren - Include all children, grandchildren, etc. in the list
	 * @param activeOnly      - Limit to portal entities that are currently active
	 */
	@DoNotAddRequestMapping
	public List<PortalEntity> getRelatedPortalEntityList(int portalEntityId, boolean includeSelf, boolean includeParents, boolean includeChildren, boolean activeOnly);


	/**
	 * Also saves the list of {@link PortalEntityFieldValue}
	 * Note: This does a smart save for each bean to only set updateDate and save if there really was a change, either on the bean itself or any of it's field values
	 */
	@PortalSecureMethod
	public PortalEntity savePortalEntity(PortalEntity bean);


	/**
	 * Allows users to change the end date on a portal entity.  Useful for securable entities where we want to end sooner or later than the default logic
	 * based on termination date
	 */
	@PortalSecureMethod(portalSecurityAdminSecurity = true)
	public PortalEntity updatePortalEntityEndDate(int portalEntityId, Date endDate);


	@PortalSecureMethod(adminSecurity = true)
	public void deletePortalEntity(int id);


	/**
	 * Called after all entity updates are done for a source system.  At that time unneeded portal entities are marked for deletion,
	 * but they aren't actually deleted until the end because of parent/child/etc. type uses
	 */
	@PortalSecureMethod(adminSecurity = true)
	public Status deletePortalEntitiesMarkedForDeletion(String sourceSystemName, Status status);


	////////////////////////////////////////////////////////////////////////////////
	///////////             Portal Entity Field Value Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntityFieldValue getPortalEntityFieldValue(int id);


	@PortalSecureMethod
	public List<PortalEntityFieldValue> getPortalEntityFieldValueListByEntity(int portalEntityId);


	@DoNotAddRequestMapping
	public List<PortalEntityFieldValue> getPortalEntityFieldValueListByEntityTypeAndFieldName(String entityTypeName, String fieldName);


	////////////////////////////////////////////////////////////////////////////////
	///////////              Portal Entity Rollup Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalEntityRollup> getPortalEntityRollupListByParent(int portalEntityId);


	@PortalSecureMethod
	public List<PortalEntityRollup> getPortalEntityRollupListByChild(int portalEntityId);


	@PortalSecureMethod(adminSecurity = true)
	public PortalEntity savePortalEntityRollupListForParent(PortalEntity parentPortalEntity, List<PortalEntityRollup> rollupList);
}
