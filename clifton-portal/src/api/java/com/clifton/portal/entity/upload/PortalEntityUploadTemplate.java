package com.clifton.portal.entity.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PortalEntityUploadTemplate</code> is used for portal entity uploads.
 *
 * @author manderson
 */
@NonPersistentObject
public class PortalEntityUploadTemplate {

	private String parentPortalEntityTypeName;
	private String parentPortalEntitySourceSystemName;
	private String parentPortalEntitySourceSectionName;
	private Integer parentSourceFKFieldId;

	private String portalEntityViewTypeName;

	private String portalEntityTypeName;
	private String portalEntitySourceSystemName;
	private String portalEntitySourceSectionName;
	private Integer sourceFKFieldId;

	private String entityName;
	private String entityLabel;
	private String entityDescription;

	private Date startDate;
	private Date endDate;
	private Date terminationDate;

	// File Posting options where the posting is for an entity in the system
	private String relationshipManager;
	private String meetYourTeam;


	/**
	 * Used for custom support of field values, file posting (i.e. assigning RM to an entity), etc.
	 */
	private Map<String, Object> unmappedColumnValueMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isParentPopulated() {
		return !StringUtils.isEmpty(getParentPortalEntityTypeName());
	}


	public String getLabel() {
		return this.portalEntityTypeName + " - " + this.portalEntitySourceSectionName + " - " + this.sourceFKFieldId + " (" + StringUtils.coalesce(true, this.entityName, this.entityLabel) + ")";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getPortalEntityViewTypeName() {
		return this.portalEntityViewTypeName;
	}


	public void setPortalEntityViewTypeName(String portalEntityViewTypeName) {
		this.portalEntityViewTypeName = portalEntityViewTypeName;
	}


	public String getPortalEntityTypeName() {
		return this.portalEntityTypeName;
	}


	public void setPortalEntityTypeName(String portalEntityTypeName) {
		this.portalEntityTypeName = portalEntityTypeName;
	}


	public String getPortalEntitySourceSystemName() {
		return this.portalEntitySourceSystemName;
	}


	public void setPortalEntitySourceSystemName(String portalEntitySourceSystemName) {
		this.portalEntitySourceSystemName = portalEntitySourceSystemName;
	}


	public String getPortalEntitySourceSectionName() {
		return this.portalEntitySourceSectionName;
	}


	public void setPortalEntitySourceSectionName(String portalEntitySourceSectionName) {
		this.portalEntitySourceSectionName = portalEntitySourceSectionName;
	}


	public Integer getSourceFKFieldId() {
		return this.sourceFKFieldId;
	}


	public void setSourceFKFieldId(Integer sourceFKFieldId) {
		this.sourceFKFieldId = sourceFKFieldId;
	}


	public String getParentPortalEntityTypeName() {
		return this.parentPortalEntityTypeName;
	}


	public void setParentPortalEntityTypeName(String parentPortalEntityTypeName) {
		this.parentPortalEntityTypeName = parentPortalEntityTypeName;
	}


	public String getParentPortalEntitySourceSystemName() {
		return this.parentPortalEntitySourceSystemName;
	}


	public void setParentPortalEntitySourceSystemName(String parentPortalEntitySourceSystemName) {
		this.parentPortalEntitySourceSystemName = parentPortalEntitySourceSystemName;
	}


	public String getParentPortalEntitySourceSectionName() {
		return this.parentPortalEntitySourceSectionName;
	}


	public void setParentPortalEntitySourceSectionName(String parentPortalEntitySourceSectionName) {
		this.parentPortalEntitySourceSectionName = parentPortalEntitySourceSectionName;
	}


	public Integer getParentSourceFKFieldId() {
		return this.parentSourceFKFieldId;
	}


	public void setParentSourceFKFieldId(Integer parentSourceFKFieldId) {
		this.parentSourceFKFieldId = parentSourceFKFieldId;
	}


	public String getEntityName() {
		return this.entityName;
	}


	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}


	public String getEntityLabel() {
		return this.entityLabel;
	}


	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}


	public String getEntityDescription() {
		return this.entityDescription;
	}


	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getTerminationDate() {
		return this.terminationDate;
	}


	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}


	public String getRelationshipManager() {
		return this.relationshipManager;
	}


	public void setRelationshipManager(String relationshipManager) {
		this.relationshipManager = relationshipManager;
	}


	public String getMeetYourTeam() {
		return this.meetYourTeam;
	}


	public void setMeetYourTeam(String meetYourTeam) {
		this.meetYourTeam = meetYourTeam;
	}


	public Map<String, Object> getUnmappedColumnValueMap() {
		return this.unmappedColumnValueMap;
	}


	public void setUnmappedColumnValueMap(Map<String, Object> unmappedColumnValueMap) {
		this.unmappedColumnValueMap = unmappedColumnValueMap;
	}
}
