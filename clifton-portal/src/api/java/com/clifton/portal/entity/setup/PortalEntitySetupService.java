package com.clifton.portal.entity.setup;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.entity.setup.search.PortalEntitySourceSectionSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityTypeSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.portal.security.authorization.PortalSecureMethod;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalEntitySetupService {


	////////////////////////////////////////////////////////////////////////////////
	//////////              Portal Entity View Type Methods               //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntityViewType getPortalEntityViewType(short id);


	@PortalSecureMethod
	public PortalEntityViewType getPortalEntityViewTypeDefault();


	@PortalSecureMethod
	public PortalEntityViewType getPortalEntityViewTypeByName(String name);


	/**
	 * Return default if none can be used if we don't know what applies to a user but we need something to return
	 */
	@PortalSecureMethod
	public List<PortalEntityViewType> getPortalEntityViewTypeList(PortalEntityViewTypeSearchForm searchForm, boolean returnDefaultIfNone);


	/**
	 * Gets the list of {@link PortalEntityViewType} entities which apply to the user with the given ID.
	 */
	@PortalSecureMethod
	public List<PortalEntityViewType> getPortalEntityViewTypeListForUser(short userId);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalEntityViewType savePortalEntityViewType(PortalEntityViewType bean);


	////////////////////////////////////////////////////////////////////////////////
	//////////            Portal Entity Source System Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntitySourceSystem getPortalEntitySourceSystemByName(String name);


	@PortalSecureMethod
	public List<PortalEntitySourceSystem> getPortalEntitySourceSystemList();


	@DoNotAddRequestMapping
	public PortalEntitySourceSystem savePortalEntitySourceSystem(PortalEntitySourceSystem bean);


	////////////////////////////////////////////////////////////////////////////////
	/////////            Portal Entity Source Section Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntitySourceSection getPortalEntitySourceSection(int id);


	@PortalSecureMethod
	public PortalEntitySourceSection getPortalEntitySourceSectionByName(String sourceSystemName, String sourceSectionName);


	@PortalSecureMethod
	public List<PortalEntitySourceSection> getPortalEntitySourceSectionList(PortalEntitySourceSectionSearchForm searchForm);


	@PortalSecureMethod(portalAdminSecurity = true)
	public PortalEntitySourceSection savePortalEntitySourceSection(PortalEntitySourceSection bean);

	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal Entity Type Methods                  //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalEntityType getPortalEntityType(short id);


	@PortalSecureMethod
	public PortalEntityType getPortalEntityTypeByName(String name);


	@PortalSecureMethod
	public List<PortalEntityType> getPortalEntityTypeList(PortalEntityTypeSearchForm searchForm);


	@DoNotAddRequestMapping
	public PortalEntityType savePortalEntityType(PortalEntityType bean);

	////////////////////////////////////////////////////////////////////////////////
	/////////              Portal Entity Field Type Methods               //////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public List<PortalEntityFieldType> getPortalEntityFieldTypeListByEntityType(short portalEntityTypeId);


	@PortalSecureMethod
	public List<PortalEntityFieldType> getPortalEntityFieldTypeList();
}
