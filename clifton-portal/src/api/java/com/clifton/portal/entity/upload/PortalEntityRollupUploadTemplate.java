package com.clifton.portal.entity.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.util.Date;


/**
 * The <code>PortalEntityUploadTemplate</code> is used for portal entity uploads.
 *
 * @author manderson
 */
@NonPersistentObject
public class PortalEntityRollupUploadTemplate {


	private String parentPortalEntityTypeName;
	private String parentPortalEntitySourceSystemName;
	private String parentPortalEntitySourceSectionName;
	private Integer parentSourceFKFieldId;

	private String childPortalEntityTypeName;
	private String childPortalEntitySourceSystemName;
	private String childPortalEntitySourceSectionName;
	private Integer childSourceFKFieldId;

	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return "Rollup - Parent: " + getParentLabel() + ", Child: " + getChildLabel();
	}


	public String getParentLabel() {
		return this.parentPortalEntityTypeName + " - " + this.parentPortalEntitySourceSectionName + " - " + this.parentSourceFKFieldId;
	}


	public String getChildLabel() {
		return this.childPortalEntityTypeName + " - " + this.childPortalEntitySourceSectionName + " - " + this.childSourceFKFieldId;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getParentPortalEntityTypeName() {
		return this.parentPortalEntityTypeName;
	}


	public void setParentPortalEntityTypeName(String parentPortalEntityTypeName) {
		this.parentPortalEntityTypeName = parentPortalEntityTypeName;
	}


	public String getParentPortalEntitySourceSystemName() {
		return this.parentPortalEntitySourceSystemName;
	}


	public void setParentPortalEntitySourceSystemName(String parentPortalEntitySourceSystemName) {
		this.parentPortalEntitySourceSystemName = parentPortalEntitySourceSystemName;
	}


	public String getParentPortalEntitySourceSectionName() {
		return this.parentPortalEntitySourceSectionName;
	}


	public void setParentPortalEntitySourceSectionName(String parentPortalEntitySourceSectionName) {
		this.parentPortalEntitySourceSectionName = parentPortalEntitySourceSectionName;
	}


	public Integer getParentSourceFKFieldId() {
		return this.parentSourceFKFieldId;
	}


	public void setParentSourceFKFieldId(Integer parentSourceFKFieldId) {
		this.parentSourceFKFieldId = parentSourceFKFieldId;
	}


	public String getChildPortalEntityTypeName() {
		return this.childPortalEntityTypeName;
	}


	public void setChildPortalEntityTypeName(String childPortalEntityTypeName) {
		this.childPortalEntityTypeName = childPortalEntityTypeName;
	}


	public String getChildPortalEntitySourceSystemName() {
		return this.childPortalEntitySourceSystemName;
	}


	public void setChildPortalEntitySourceSystemName(String childPortalEntitySourceSystemName) {
		this.childPortalEntitySourceSystemName = childPortalEntitySourceSystemName;
	}


	public String getChildPortalEntitySourceSectionName() {
		return this.childPortalEntitySourceSectionName;
	}


	public void setChildPortalEntitySourceSectionName(String childPortalEntitySourceSectionName) {
		this.childPortalEntitySourceSectionName = childPortalEntitySourceSectionName;
	}


	public Integer getChildSourceFKFieldId() {
		return this.childSourceFKFieldId;
	}


	public void setChildSourceFKFieldId(Integer childSourceFKFieldId) {
		this.childSourceFKFieldId = childSourceFKFieldId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
