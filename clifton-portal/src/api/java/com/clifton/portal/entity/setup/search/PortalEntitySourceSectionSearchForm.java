package com.clifton.portal.entity.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PortalEntitySourceSectionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "sourceSystem.name,name,label,description")
	private String searchPattern;

	@SearchField
	private String label;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "sourceSystem.id", sortField = "sourceSystem.name")
	private Short sourceSystemId;

	@SearchField
	private String sourceSystemTableName;

	@SearchField(searchField = "entityType.id")
	private Short entityTypeId;

	@SearchField(searchField = "childEntityType.id")
	private Short childEntityTypeId;


	@SearchField(searchField = "childEntityType.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean childEntityTypeIdPopulated;

	@SearchField(searchFieldPath = "entityType")
	private Boolean fileSource;

	@SearchField(searchFieldPath = "entityType")
	private Boolean securableEntity;

	@SearchField(searchFieldPath = "entityType")
	private Boolean postableEntity;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getEntityTypeId() {
		return this.entityTypeId;
	}


	public void setEntityTypeId(Short entityTypeId) {
		this.entityTypeId = entityTypeId;
	}


	public Boolean getFileSource() {
		return this.fileSource;
	}


	public void setFileSource(Boolean fileSource) {
		this.fileSource = fileSource;
	}


	public Boolean getPostableEntity() {
		return this.postableEntity;
	}


	public void setPostableEntity(Boolean postableEntity) {
		this.postableEntity = postableEntity;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public Short getChildEntityTypeId() {
		return this.childEntityTypeId;
	}


	public void setChildEntityTypeId(Short childEntityTypeId) {
		this.childEntityTypeId = childEntityTypeId;
	}


	public Boolean getSecurableEntity() {
		return this.securableEntity;
	}


	public void setSecurableEntity(Boolean securableEntity) {
		this.securableEntity = securableEntity;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Boolean getChildEntityTypeIdPopulated() {
		return this.childEntityTypeIdPopulated;
	}


	public void setChildEntityTypeIdPopulated(Boolean childEntityTypeIdPopulated) {
		this.childEntityTypeIdPopulated = childEntityTypeIdPopulated;
	}
}
