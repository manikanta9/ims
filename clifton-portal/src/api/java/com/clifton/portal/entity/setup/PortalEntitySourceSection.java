package com.clifton.portal.entity.setup;

import com.clifton.core.beans.NamedEntity;


/**
 * The <code>PortalEntitySourceSection</code> represents a specific table in a {@link PortalEntitySourceSystem}
 *
 * @author manderson
 */
public class PortalEntitySourceSection extends NamedEntity<Integer> {

	private PortalEntitySourceSystem sourceSystem;

	private String sourceSystemTableName;

	private PortalEntityType entityType;

	/**
	 * Used only if entityType.postableEntity = true.
	 * When defined, indicates this entity is a rollup of child entities. (Has entries in PortalEntityRollup table where this entity = parent)
	 * i.e. Commingled Vehicle is a roll up of Investor Client Accounts
	 * This is used to determine when posting, if the file category posts to Client Accounts, then can be posted to these entities as well
	 */
	private PortalEntityType childEntityType;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getNameWithSourceSystem() {
		if (getSourceSystem() != null) {
			return getSourceSystem().getName() + ": " + getName();
		}
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(PortalEntitySourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public PortalEntityType getEntityType() {
		return this.entityType;
	}


	public void setEntityType(PortalEntityType entityType) {
		this.entityType = entityType;
	}


	public PortalEntityType getChildEntityType() {
		return this.childEntityType;
	}


	public void setChildEntityType(PortalEntityType childEntityType) {
		this.childEntityType = childEntityType;
	}
}
