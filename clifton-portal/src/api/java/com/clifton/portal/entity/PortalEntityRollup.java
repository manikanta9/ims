package com.clifton.portal.entity;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>PortalEntityRollup</code> is built to assign the relationships between portal entities
 * i.e. Accounts in an account group, Accounts of a given service, etc.
 *
 * @author manderson
 */
public class PortalEntityRollup extends ManyToManyEntity<PortalEntity, PortalEntity, Integer> {

	private Date startDate;

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
