package com.clifton.portal.entity.upload;

import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.core.util.status.Status;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;


/**
 * @author manderson
 */
public class PortalEntityRollupUploadCommand extends FileUploadCommand<PortalEntityRollupUploadTemplate> {

	public static final String[] REQUIRED_PROPERTIES = {"parentPortalEntityTypeName", "parentPortalEntitySourceSectionName", "parentSourceFKFieldId", "childPortalEntityTypeName", "childPortalEntitySourceSectionName", "childSourceFKFieldId"};


	/**
	 * The default source system for the upload. Some entities in the file may reference another source system, if so they can still be used but won't be modified
	 */
	private PortalEntitySourceSystem sourceSystem;

	/**
	 * If true, then the list in the upload file for each rollup is considered to be all encompassing - so any child not in the list will be removed as a child of the rollup.
	 * Any rollups NOT included in the file for the given source section will NOT be touched.
	 * If false, then any child not in the list won't be updated
	 */
	private boolean deleteChildEntitiesMissingFromFile;


	/**
	 * Status object to track status and details of the upload that is performed.
	 */
	private Status status = Status.ofEmptyMessage();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<PortalEntityRollupUploadTemplate> getUploadBeanClass() {
		return PortalEntityRollupUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(PortalEntitySourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public boolean isDeleteChildEntitiesMissingFromFile() {
		return this.deleteChildEntitiesMissingFromFile;
	}


	public void setDeleteChildEntitiesMissingFromFile(boolean deleteChildEntitiesMissingFromFile) {
		this.deleteChildEntitiesMissingFromFile = deleteChildEntitiesMissingFromFile;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
