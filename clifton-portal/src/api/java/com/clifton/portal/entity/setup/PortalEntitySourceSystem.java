package com.clifton.portal.entity.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>PortalEntitySourceSystem</code> defines a system that pushes data to the portal, i.e. IMS
 *
 * @author manderson
 */
@CacheByName
public class PortalEntitySourceSystem extends NamedEntity<Short> {

	/**
	 * When using multiple systems, there can be some shared information
	 * For cases where things are shared, the system with the highest priority is the one the entity belongs to
	 * The other systems can still reference that entity.
	 * Example: Client Relationship / Client / Client Account in IMS - Priority 1
	 * Same Client Relationship / Client in Salesforce, but additional client account to add to that client.
	 */
	private Short priorityOrder;


	/**
	 * IMS data comes directly from IMS through batch jobs which use an admin user. (i.e. administratorOnly)
	 * Salesforce data will come from upload files which won't be restricted to admins only. (Note: Users that upload must still be a Portal Admin)
	 * i.e. Role IsAdministrator = true vs. Role IsPortalAdministrator = true
	 * NOTE: Entities and related information cannot be edited via UI, only through API or uploads
	 */
	private boolean administratorOnly;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Short priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public boolean isAdministratorOnly() {
		return this.administratorOnly;
	}


	public void setAdministratorOnly(boolean administratorOnly) {
		this.administratorOnly = administratorOnly;
	}
}
