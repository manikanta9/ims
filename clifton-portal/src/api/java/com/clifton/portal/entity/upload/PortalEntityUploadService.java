package com.clifton.portal.entity.upload;

import com.clifton.core.util.status.Status;
import com.clifton.portal.security.authorization.PortalSecureMethod;


/**
 * The <code>PortalEntityUploadService</code> defines methods for uploading PortalEntity related changes
 *
 * @author manderson
 */
public interface PortalEntityUploadService {

	@PortalSecureMethod(portalAdminSecurity = true)
	public Status uploadPortalEntityUploadFile(PortalEntityUploadCommand uploadCommand);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod(portalAdminSecurity = true)
	public Status uploadPortalEntityRollupUploadFile(PortalEntityRollupUploadCommand uploadCommand);
}
