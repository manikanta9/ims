package com.clifton.portal.entity.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.portal.security.search.PortalEntitySpecificSecurityAdminAwareSearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalEntitySearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements PortalEntitySpecificSecurityAdminAwareSearchForm {


	@SearchField(searchField = "name,label,description")
	private String searchPattern;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "name,label", comparisonConditions = ComparisonConditions.EQUALS, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private String nameOrLabelEquals;

	@SearchField
	private Integer sourceFkFieldId;

	@SearchField(searchField = "parentPortalEntity", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean parentIsNull;

	@SearchField(searchField = "parentPortalEntity.id")
	private Integer parentId;

	@SearchField
	private Boolean deleted;

	// Custom search field that when set will allow the deleted filter to stand on its own.  Otherwise if NULL, will default deleted filter to false so by default we always exclude deleted entities
	private Boolean includeDeleted;

	@SearchField
	private Date terminationDate;

	// Custom Search Field: True: terminationDate IS NOT NULL and endDate >= today, False = NOT of true expression
	private Boolean pendingTermination;

	// Currently only goes up 3 levels because our max use case is 4 levels
	@SearchField(searchField = "parentPortalEntity.id,parentPortalEntity.parentPortalEntity.id,parentPortalEntity.parentPortalEntity.parentPortalEntity.id", leftJoin = true)
	private Integer parentIdExpanded;

	// Currently only goes up 3 levels because our max use case is 4 levels
	@SearchField(searchField = "parentPortalEntity.parentPortalEntity.parentPortalEntity.label,parentPortalEntity.parentPortalEntity.label,parentPortalEntity.label,label", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String labelExpanded;

	@SearchField(searchField = "entityViewType.id")
	private Short entityViewTypeId;

	@SearchField(searchField = "entitySourceSection.id")
	private Integer entitySourceSectionId;

	@SearchField(searchFieldPath = "entitySourceSection", searchField = "name")
	private String[] entitySourceSectionNames;

	@SearchField(searchFieldPath = "entitySourceSection", searchField = "sourceSystem.id")
	private Short sourceSystemId;

	@SearchField(searchFieldPath = "entitySourceSection", searchField = "sourceSystemTableName", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceSystemTableNameEquals;

	@SearchField(searchFieldPath = "entitySourceSection.sourceSystem", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceSystemNameEquals;

	@SearchField(searchFieldPath = "entitySourceSection.sourceSystem", searchField = "priorityOrder")
	private Short sourceSystemPriorityOrder;


	@SearchField(searchFieldPath = "entitySourceSection", searchField = "entityType.id")
	private Short entityTypeId;

	@SearchField(searchFieldPath = "entitySourceSection.entityType", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String entityTypeNameEquals;

	@SearchField(searchFieldPath = "entitySourceSection.entityType", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeEntityTypeName;

	@SearchField(searchFieldPath = "entitySourceSection", searchField = "entityType.id,childEntityType.id", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short entityTypeOrChildEntityTypeId;

	@SearchField(searchFieldPath = "entitySourceSection.entityType", searchField = "securableEntity")
	private Boolean securableEntity;

	/**
	 * Custom Search Field - also sets securableEntity = true (if not set)
	 * Valid options are 1 - 4 and build the filters based on the options set.
	 * 1 = Organization Group,
	 * 2 = Client Relationship, Sister Client Group, and Sister Client (if not part of a group)
	 * 3 = Sister Client (if part of a group) or Client
	 * 4 = Client Account
	 * Set once here, so any time we have to show the multiple levels we can use one search form parameter instead of building that for each combo
	 */
	private Short securableEntityLevel;

	/**
	 * Custom Search Field - also sets securableEntity = true (if not set)
	 * That filters based on at least one approved file posted to the entity (includes children)
	 */
	private Boolean securableEntityHasApprovedFilesPosted;


	@SearchField(searchFieldPath = "entitySourceSection.entityType", searchField = "postableEntity")
	private Boolean postableEntity;

	@SearchField(searchFieldPath = "entitySourceSection.entityType", searchField = "fileSource")
	private Boolean fileSource;


	// Custom Search Field for Viewing As User
	private Short viewAsUserId;

	// Custom Search Field for Viewing As Entity
	private Integer viewAsEntityId;

	// Applies additional security for the VIEWING user if they are a security administrator for the entity
	private Boolean viewSecurityAdminRole;

	// Applies additional security for the VIEWING user if they have at least one security resource mapped.
	private Boolean hasSecurityResourceAccess;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getParentIsNull() {
		return this.parentIsNull;
	}


	public void setParentIsNull(Boolean parentIsNull) {
		this.parentIsNull = parentIsNull;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Short getEntityViewTypeId() {
		return this.entityViewTypeId;
	}


	public void setEntityViewTypeId(Short entityViewTypeId) {
		this.entityViewTypeId = entityViewTypeId;
	}


	public Integer getEntitySourceSectionId() {
		return this.entitySourceSectionId;
	}


	public void setEntitySourceSectionId(Integer entitySourceSectionId) {
		this.entitySourceSectionId = entitySourceSectionId;
	}


	public Boolean getSecurableEntity() {
		return this.securableEntity;
	}


	public void setSecurableEntity(Boolean securableEntity) {
		this.securableEntity = securableEntity;
	}


	@Override
	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	@Override
	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	public Boolean getPostableEntity() {
		return this.postableEntity;
	}


	public void setPostableEntity(Boolean postableEntity) {
		this.postableEntity = postableEntity;
	}


	public Boolean getFileSource() {
		return this.fileSource;
	}


	public void setFileSource(Boolean fileSource) {
		this.fileSource = fileSource;
	}


	public Short getEntityTypeId() {
		return this.entityTypeId;
	}


	public void setEntityTypeId(Short entityTypeId) {
		this.entityTypeId = entityTypeId;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getNameOrLabelEquals() {
		return this.nameOrLabelEquals;
	}


	public void setNameOrLabelEquals(String nameOrLabelEquals) {
		this.nameOrLabelEquals = nameOrLabelEquals;
	}


	public Integer getParentIdExpanded() {
		return this.parentIdExpanded;
	}


	public void setParentIdExpanded(Integer parentIdExpanded) {
		this.parentIdExpanded = parentIdExpanded;
	}


	public Short getEntityTypeOrChildEntityTypeId() {
		return this.entityTypeOrChildEntityTypeId;
	}


	public void setEntityTypeOrChildEntityTypeId(Short entityTypeOrChildEntityTypeId) {
		this.entityTypeOrChildEntityTypeId = entityTypeOrChildEntityTypeId;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Boolean getIncludeDeleted() {
		return this.includeDeleted;
	}


	public void setIncludeDeleted(Boolean includeDeleted) {
		this.includeDeleted = includeDeleted;
	}


	public Date getTerminationDate() {
		return this.terminationDate;
	}


	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}


	public Boolean getPendingTermination() {
		return this.pendingTermination;
	}


	public void setPendingTermination(Boolean pendingTermination) {
		this.pendingTermination = pendingTermination;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public String getSourceSystemTableNameEquals() {
		return this.sourceSystemTableNameEquals;
	}


	public void setSourceSystemTableNameEquals(String sourceSystemTableNameEquals) {
		this.sourceSystemTableNameEquals = sourceSystemTableNameEquals;
	}


	public String getSourceSystemNameEquals() {
		return this.sourceSystemNameEquals;
	}


	public void setSourceSystemNameEquals(String sourceSystemNameEquals) {
		this.sourceSystemNameEquals = sourceSystemNameEquals;
	}


	public Short getSourceSystemPriorityOrder() {
		return this.sourceSystemPriorityOrder;
	}


	public void setSourceSystemPriorityOrder(Short sourceSystemPriorityOrder) {
		this.sourceSystemPriorityOrder = sourceSystemPriorityOrder;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public String getExcludeEntityTypeName() {
		return this.excludeEntityTypeName;
	}


	public void setExcludeEntityTypeName(String excludeEntityTypeName) {
		this.excludeEntityTypeName = excludeEntityTypeName;
	}


	public String getEntityTypeNameEquals() {
		return this.entityTypeNameEquals;
	}


	public void setEntityTypeNameEquals(String entityTypeNameEquals) {
		this.entityTypeNameEquals = entityTypeNameEquals;
	}


	public String[] getEntitySourceSectionNames() {
		return this.entitySourceSectionNames;
	}


	public void setEntitySourceSectionNames(String[] entitySourceSectionNames) {
		this.entitySourceSectionNames = entitySourceSectionNames;
	}


	public Short getSecurableEntityLevel() {
		return this.securableEntityLevel;
	}


	public void setSecurableEntityLevel(Short securableEntityLevel) {
		this.securableEntityLevel = securableEntityLevel;
	}


	@Override
	public Boolean getViewSecurityAdminRole() {
		return this.viewSecurityAdminRole;
	}


	@Override
	public void setViewSecurityAdminRole(Boolean viewSecurityAdminRole) {
		this.viewSecurityAdminRole = viewSecurityAdminRole;
	}


	@Override
	public Boolean getHasSecurityResourceAccess() {
		return this.hasSecurityResourceAccess;
	}


	@Override
	public void setHasSecurityResourceAccess(Boolean hasSecurityResourceAccess) {
		this.hasSecurityResourceAccess = hasSecurityResourceAccess;
	}


	public String getLabelExpanded() {
		return this.labelExpanded;
	}


	public void setLabelExpanded(String labelExpanded) {
		this.labelExpanded = labelExpanded;
	}


	public Boolean getSecurableEntityHasApprovedFilesPosted() {
		return this.securableEntityHasApprovedFilesPosted;
	}


	public void setSecurableEntityHasApprovedFilesPosted(Boolean securableEntityHasApprovedFilesPosted) {
		this.securableEntityHasApprovedFilesPosted = securableEntityHasApprovedFilesPosted;
	}


	@Override
	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	@Override
	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}
}
