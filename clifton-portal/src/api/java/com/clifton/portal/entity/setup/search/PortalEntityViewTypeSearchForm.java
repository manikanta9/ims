package com.clifton.portal.entity.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.portal.security.search.PortalEntitySpecificAwareSearchForm;


/**
 * @author manderson
 */
public class PortalEntityViewTypeSearchForm extends BaseAuditableEntitySearchForm implements PortalEntitySpecificAwareSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String defaultContactEmailAddress;


	@SearchField
	private Boolean defaultView;

	// Custom Search Field for Viewing As User
	private Short viewAsUserId;

	// Custom Search Field for Viewing As Entity
	private Integer viewAsEntityId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDefaultContactEmailAddress() {
		return this.defaultContactEmailAddress;
	}


	public void setDefaultContactEmailAddress(String defaultContactEmailAddress) {
		this.defaultContactEmailAddress = defaultContactEmailAddress;
	}


	public Boolean getDefaultView() {
		return this.defaultView;
	}


	public void setDefaultView(Boolean defaultView) {
		this.defaultView = defaultView;
	}


	@Override
	public Short getViewAsUserId() {
		return this.viewAsUserId;
	}


	@Override
	public void setViewAsUserId(Short viewAsUserId) {
		this.viewAsUserId = viewAsUserId;
	}


	@Override
	public Integer getViewAsEntityId() {
		return this.viewAsEntityId;
	}


	@Override
	public void setViewAsEntityId(Integer viewAsEntityId) {
		this.viewAsEntityId = viewAsEntityId;
	}
}
