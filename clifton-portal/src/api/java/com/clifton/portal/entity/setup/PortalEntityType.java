package com.clifton.portal.entity.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;

import java.util.List;


/**
 * The <code>PortalEntityType</code> defines the type of entity - i.e. Contact, File Source, Client, Post-able Entity
 * Each entity type can have specific fields that are associated with it.  For example, contacts have first name, last name, email
 * <p>
 *
 * @author manderson
 */
@CacheByName
public class PortalEntityType extends NamedEntity<Short> {

	public static final String ENTITY_TYPE_ORGANIZATION_GROUP = "Organization Group";
	public static final String ENTITY_TYPE_CLIENT_RELATIONSHIP = "Client Relationship";
	public static final String ENTITY_TYPE_CLIENT = "Client";
	public static final String ENTITY_TYPE_CLIENT_ACCOUNT = "Client Account";
	public static final String ENTITY_TYPE_TEAM = "Team";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * An entity type that can be posted to, like a Client, Account, Service
	 */
	private boolean postableEntity;

	/**
	 * A securable entity is a special type of postable entity that differentiates those that our customers log in/secure (Accounts) from other ways we post (Services)
	 */
	private boolean securableEntity;

	/**
	 * An entity type that can be referenced as the file source, like an Invoice, Portfolio Run, Contact
	 */
	private boolean fileSource;


	private List<PortalEntityFieldType> fieldTypeList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isPostableEntity() {
		return this.postableEntity;
	}


	public void setPostableEntity(boolean postableEntity) {
		this.postableEntity = postableEntity;
	}


	public boolean isSecurableEntity() {
		return this.securableEntity;
	}


	public void setSecurableEntity(boolean securableEntity) {
		this.securableEntity = securableEntity;
	}


	public boolean isFileSource() {
		return this.fileSource;
	}


	public void setFileSource(boolean fileSource) {
		this.fileSource = fileSource;
	}


	public List<PortalEntityFieldType> getFieldTypeList() {
		return this.fieldTypeList;
	}


	public void setFieldTypeList(List<PortalEntityFieldType> fieldTypeList) {
		this.fieldTypeList = fieldTypeList;
	}
}
