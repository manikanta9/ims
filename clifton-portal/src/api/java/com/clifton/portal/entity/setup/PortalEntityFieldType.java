package com.clifton.portal.entity.setup;

import com.clifton.core.beans.NamedEntity;


/**
 * The <code>PortalEntityFieldType</code> defines a specific field/attribute that is associated with the portal entity type
 * i.e. Contact has a field for First Name, a field for Last Name
 *
 * @author manderson
 */
public class PortalEntityFieldType extends NamedEntity<Short> {

	// Contact Field Types
	public static final String FIELD_TYPE_NAME_BIOGRAPHY = "Biography";
	public static final String FIELD_TYPE_NAME_PHONE_NUMBER = "Phone Number";
	public static final String FIELD_TYPE_NAME_PROFESSIONAL_DESIGNATIONS = "Professional Designations";
	public static final String FIELD_TYPE_TITLE = "Title";

	// Contact and Team Field Types
	public static final String FIELD_TYPE_NAME_EMAIL = "Email Address";
	// File Source with Status Field Types
	public static final String FIELD_TYPE_STATUS = "Status";
	// Client Account Field Types
	public static final String FIELD_TYPE_SERVICE_NAME = "Service";

	// Portal Team Email Overrides (Client Relationships and Clients)
	public static final String FIELD_TYPE_SUPPORT_TEAM_EMAIL = "Portal Support Team Email"; // Internal team responsible for posting all files under this client relationship - ignores file category emails and investment teams
	public static final String FIELD_TYPE_CONTACT_TEAM_EMAIL = "Portal Contact Team Email"; // Internal team responsible for receiving all Client "Contact Us" emails for all entities under this client relationship - ignores file category emails and investment teams


	////////////////////////////////////////////////////////////////////////////////


	private PortalEntityType portalEntityType;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityType getPortalEntityType() {
		return this.portalEntityType;
	}


	public void setPortalEntityType(PortalEntityType portalEntityType) {
		this.portalEntityType = portalEntityType;
	}
}
