package com.clifton.portal.entity.setup;

/**
 * The <code>PortalEntityViewTypeAware</code> interface defines classes that use PortalEntityViewType field which can be used to simplify and standardize utility methods
 *
 * @author manderson
 */
public interface PortalEntityViewTypeAware {


	public PortalEntityViewType getEntityViewType();
}
