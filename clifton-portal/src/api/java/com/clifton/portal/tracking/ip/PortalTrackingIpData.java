package com.clifton.portal.tracking.ip;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortalTrackingIpData</code> defines IP information that a user is logging in from
 *
 * @author manderson
 */
public class PortalTrackingIpData extends BaseSimpleEntity<Integer> {

	private String ipAddress;
	private String countryCode;
	private String region;
	private String city;
	private String postalCode;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private Integer metroCode;
	private Integer areaCode;
	private String isp;
	private String organization;

	private Date queryDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCityState() {
		if (StringUtils.isEmpty(getRegion())) {
			return getCity();
		}
		if (StringUtils.isEmpty(getCity())) {
			return getRegion();
		}
		return getCity() + ", " + getRegion();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getIpAddress() {
		return this.ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getCountryCode() {
		return this.countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getRegion() {
		return this.region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public String getCity() {
		return this.city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getPostalCode() {
		return this.postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public BigDecimal getLatitude() {
		return this.latitude;
	}


	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}


	public BigDecimal getLongitude() {
		return this.longitude;
	}


	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}


	public Integer getMetroCode() {
		return this.metroCode;
	}


	public void setMetroCode(Integer metroCode) {
		this.metroCode = metroCode;
	}


	public Integer getAreaCode() {
		return this.areaCode;
	}


	public void setAreaCode(Integer areaCode) {
		this.areaCode = areaCode;
	}


	public String getIsp() {
		return this.isp;
	}


	public void setIsp(String isp) {
		this.isp = isp;
	}


	public String getOrganization() {
		return this.organization;
	}


	public void setOrganization(String organization) {
		this.organization = organization;
	}


	public Date getQueryDate() {
		return this.queryDate;
	}


	public void setQueryDate(Date queryDate) {
		this.queryDate = queryDate;
	}
}
