package com.clifton.portal.tracking.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.tracking.PortalTrackingService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;


/**
 * @author manderson
 */
public class PortalTrackingEventSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final PortalTrackingEventSearchForm portalTrackingEventSearchForm;


	public PortalTrackingEventSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalTrackingService portalTrackingService) {
		// Need to configure search form after we update Names to Use Ids
		super(searchForm, true);

		this.portalTrackingEventSearchForm = (PortalTrackingEventSearchForm) searchForm;

		// Remove Joins to Type Table and use IDs
		if (!StringUtils.isEmpty(this.portalTrackingEventSearchForm.getTrackingEventTypeNameEquals())) {
			this.portalTrackingEventSearchForm.setTrackingEventTypeId(portalTrackingService.getPortalTrackingEventTypeByName(this.portalTrackingEventSearchForm.getTrackingEventTypeNameEquals()).getId());
			this.portalTrackingEventSearchForm.setTrackingEventTypeNameEquals(null);
		}
		if (this.portalTrackingEventSearchForm.getTrackingEventTypeNames() != null && this.portalTrackingEventSearchForm.getTrackingEventTypeNames().length > 0) {
			Short[] typeIds = new Short[this.portalTrackingEventSearchForm.getTrackingEventTypeNames().length];
			for (int i = 0; i < this.portalTrackingEventSearchForm.getTrackingEventTypeNames().length; i++) {
				typeIds[i] = portalTrackingService.getPortalTrackingEventTypeByName(this.portalTrackingEventSearchForm.getTrackingEventTypeNames()[i]).getId();
			}
			this.portalTrackingEventSearchForm.setTrackingEventTypeIds(typeIds);
			this.portalTrackingEventSearchForm.setTrackingEventTypeNames(null);
		}

		configureSearchForm();
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PortalTrackingEventSearchForm searchForm = this.portalTrackingEventSearchForm;
		boolean extendedSearch = searchForm instanceof PortalTrackingEventExtendedSearchForm;
		if (searchForm.getPortalEntityViewTypeId() != null) {
			Disjunction ors = Restrictions.disjunction();
			// Portal Entity ID is NULL & User Assignment Exists to Entity of Selected View Type
			DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignment.class, "a");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("portalEntity", "ape");
			sub.add(Restrictions.eq("ape.entityViewType.id", searchForm.getPortalEntityViewTypeId()));
			sub.add(Restrictions.eqProperty("securityUser.id", getPathAlias("portalSecurityUser", criteria) + ".id"));
			ors.add(Restrictions.and(Restrictions.isNull(extendedSearch ? "portalEntity.id" : "portalEntityId"), Subqueries.exists(sub)));


			// OR Portal Entity ID Is NOT NULL && Portal Entity ID View Type = Selected View Type
			// Must Use Exists because it's not a real FK unless in Extended View
			if (extendedSearch) {
				ors.add(Restrictions.and(Restrictions.isNotNull("portalEntity.id"), Restrictions.eq(getPathAlias("portalEntity.entityViewType", criteria, JoinType.LEFT_OUTER_JOIN) + ".id", searchForm.getPortalEntityViewTypeId())));
			}
			else {
				DetachedCriteria entitySub = DetachedCriteria.forClass(PortalEntity.class, "e");
				entitySub.setProjection(Projections.property("id"));
				entitySub.add(Restrictions.eq("entityViewType.id", searchForm.getPortalEntityViewTypeId()));
				entitySub.add(Restrictions.eqProperty("id", criteria.getAlias() + ".portalEntityId"));
				ors.add(Restrictions.and(Restrictions.isNotNull("portalEntityId"), Subqueries.exists(entitySub)));
			}
			criteria.add(ors);
		}
	}
}
