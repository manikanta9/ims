package com.clifton.portal.tracking.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>PortalTrackingAuditColumnConfig</code> allows specifying how a specific column should be audited if not following the default logic
 *
 * @author manderson
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PortalTrackingAuditColumnConfig {


	/**
	 * Ability to disable auditing for a specific field completely
	 */
	boolean doNotAudit() default false;


	/**
	 * Ability to turn off update audits from Null
	 */
	boolean doNotAuditUpdateFromNull() default false;


	/**
	 * Ability to turn off update audits from specified string representation of the value
	 * For example, don't audit from boolean false to boolean true (similar to not auditing from null but useful for booleans which are never null)
	 */
	String doNotAuditUpdateFromValue() default "";


	/**
	 * Ability to always audit a field during inserts.
	 * By default for inserts we audit that the insert occurred, but don't track the value of each field
	 */
	boolean alwaysAuditInsert() default false;


	/**
	 * Ability to always audit a field during deletes.
	 * By Default if the value is the default value (i.e. boolean false) then the value won't be audited
	 * in some cases, like user security, we always want to audit what the value is
	 * i.e. Can see that a user added a permission record - but looks like the value is always true, when instead they could be inserting a false (no access)
	 */
	boolean alwaysAuditDelete() default false;
}
