package com.clifton.portal.tracking;

/**
 * @author manderson
 */
public enum PortalTrackingEventTypes {

	AUDIT_TRAIL_INSERT("Audit Trail: Insert"),
	AUDIT_TRAIL_UPDATE("Audit Trail: Update"),
	AUDIT_TRAIL_DELETE("Audit Trail: Delete"),
	LOGIN("Login"),
	FILE_DOWNLOAD("File Download", true, "portalFileId", null, "viewAsUserId"),
	FILE_LIST_DOWNLOAD("File Download", true, null, "portalFileIds", "viewAsUserId,zipFiles"),
	FILE_LIST_VIEW("File List View", false, "fileCategoryIdExpanded", null, "categoryName,fileGroupName,minReportDate,maxReportDate,defaultDisplay,start,limit"),
	NOTIFICATION_ACKNOWLEDGEMENT("Portal Notification Acknowledgement");

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final String name;

	/**
	 * If true, then event can be generated from the web request event method annotation {@link PortalTrackingEventMethod}
	 */
	private final boolean webRequestEventMethodSupported;

	/**
	 * If the event type is associated with a source table, this would be the id of that entity, i.e. PortalFileID
	 */
	private final String webRequestFkFieldIdUrlParameter;

	/**
	 * If the event type is associated with a source table, and the property is an Integer[] i.e. PortalFileIDs
	 * creates unique events for each item the in array
	 */
	private final String webRequestFkFieldIdArrayUrlParameter;


	/**
	 * Requires that the fkFieldId is populated in order to save the event
	 * This is currently used for downloads, which should not track the download event if there was an error, but Client UI currently missing validation and it is possible to call the download method without passing any files
	 * so it's tracked as a download event without a file.
	 */
	private final boolean webRequestRequireFkFieldId;

	/**
	 * Comma delimited list of Url parameters to include in the event tracking description
	 * i.e. when view files for a category, the category name search, report date filters, and if they are looking at a specific portal entity
	 */
	private final String webRequestDescriptionUrlParameters;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PortalTrackingEventTypes(String name) {
		this(name, false, false, null, null, null);
	}


	PortalTrackingEventTypes(String name, boolean webRequestRequireFkFieldId, String webRequestFkFieldIdUrlParameter, String webRequestFkFieldIdArrayUrlParameter, String webRequestDescriptionUrlParameters) {
		this(name, webRequestRequireFkFieldId, true, webRequestFkFieldIdUrlParameter, webRequestFkFieldIdArrayUrlParameter, webRequestDescriptionUrlParameters);
	}


	PortalTrackingEventTypes(String name, boolean webRequestRequireFkFieldId, boolean webRequestEventMethodSupported, String webRequestFkFieldIdUrlParameter, String webRequestFkFieldIdArrayUrlParameter, String webRequestDescriptionUrlParameters) {
		this.name = name;
		this.webRequestEventMethodSupported = webRequestEventMethodSupported;
		this.webRequestRequireFkFieldId = webRequestRequireFkFieldId;
		this.webRequestFkFieldIdUrlParameter = webRequestFkFieldIdUrlParameter;
		this.webRequestFkFieldIdArrayUrlParameter = webRequestFkFieldIdArrayUrlParameter;
		this.webRequestDescriptionUrlParameters = webRequestDescriptionUrlParameters;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public boolean isWebRequestEventMethodSupported() {
		return this.webRequestEventMethodSupported;
	}


	public String getWebRequestFkFieldIdUrlParameter() {
		return this.webRequestFkFieldIdUrlParameter;
	}


	public String getWebRequestFkFieldIdArrayUrlParameter() {
		return this.webRequestFkFieldIdArrayUrlParameter;
	}


	public String getWebRequestDescriptionUrlParameters() {
		return this.webRequestDescriptionUrlParameters;
	}


	public boolean isWebRequestRequireFkFieldId() {
		return this.webRequestRequireFkFieldId;
	}
}
