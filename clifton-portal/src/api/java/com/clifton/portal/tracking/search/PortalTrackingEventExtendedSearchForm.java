package com.clifton.portal.tracking.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;


/**
 * @author manderson
 */
public class PortalTrackingEventExtendedSearchForm extends PortalTrackingEventSearchForm {


	@SearchField(searchField = "portalFileCategory.id,portalFileCategory.parent.id,portalFileCategory.parent.parent.id", comparisonConditions = ComparisonConditions.EQUALS, leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short fileCategoryIdExpanded;

	@SearchField(searchField = "portalEntity.id", leftJoin = true)
	private Integer portalEntityId;

	@SearchField(searchFieldPath = "portalEntity", searchField = "name", leftJoin = true)
	private String portalEntityName;

	@SearchField(searchField = "portalFile.id")
	private Integer portalFileId;

	@SearchField(searchField = "portalFile.id")
	private Integer[] portalFileIds;

	@SearchField(searchFieldPath = "portalFile", searchField = "displayName", leftJoin = true)
	private String fileDisplayName;

	@SearchField(searchField = "portalNotification.id")
	private Integer portalNotificationId;

	@SearchField(searchFieldPath = "portalNotification", searchField = "notificationDefinition.id")
	private Short portalNotificationDefinitionId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getFileCategoryIdExpanded() {
		return this.fileCategoryIdExpanded;
	}


	public void setFileCategoryIdExpanded(Short fileCategoryIdExpanded) {
		this.fileCategoryIdExpanded = fileCategoryIdExpanded;
	}


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}


	public String getPortalEntityName() {
		return this.portalEntityName;
	}


	public void setPortalEntityName(String portalEntityName) {
		this.portalEntityName = portalEntityName;
	}


	public String getFileDisplayName() {
		return this.fileDisplayName;
	}


	public void setFileDisplayName(String fileDisplayName) {
		this.fileDisplayName = fileDisplayName;
	}


	public Integer getPortalFileId() {
		return this.portalFileId;
	}


	public void setPortalFileId(Integer portalFileId) {
		this.portalFileId = portalFileId;
	}


	public Integer[] getPortalFileIds() {
		return this.portalFileIds;
	}


	public void setPortalFileIds(Integer[] portalFileIds) {
		this.portalFileIds = portalFileIds;
	}


	public Integer getPortalNotificationId() {
		return this.portalNotificationId;
	}


	public void setPortalNotificationId(Integer portalNotificationId) {
		this.portalNotificationId = portalNotificationId;
	}


	public Short getPortalNotificationDefinitionId() {
		return this.portalNotificationDefinitionId;
	}


	public void setPortalNotificationDefinitionId(Short portalNotificationDefinitionId) {
		this.portalNotificationDefinitionId = portalNotificationDefinitionId;
	}
}
