package com.clifton.portal.tracking.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author manderson
 */
public class PortalTrackingEventSearchForm extends BaseEntitySearchForm {


	@SearchField(searchField = "trackingEventType.id", sortField = "trackingEventType.name")
	private Short trackingEventTypeId;

	@SearchField(searchField = "trackingEventType.id", sortField = "trackingEventType.name")
	private Short[] trackingEventTypeIds;

	@SearchField(searchField = "trackingEventType.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String trackingEventTypeNameEquals;

	@SearchField(searchField = "trackingEventType.name", comparisonConditions = ComparisonConditions.IN)
	private String[] trackingEventTypeNames;

	@SearchField(searchField = "portalSecurityUser.id", sortField = "portalSecurityUser.username")
	private Short portalSecurityUserId;

	@SearchField(searchField = "portalSecurityUser.userRole.portalEntitySpecific")
	private Boolean portalSecurityUserRolePortalEntitySpecific;

	@SearchField(searchFieldPath = "portalSecurityUser", searchField = "username,firstName,lastName")
	private String portalSecurityUserName;

	@SearchField(searchFieldPath = "portalSecurityUser", searchField = "companyName")
	private String portalSecurityUserCompanyName;

	@SearchField(searchFieldPath = "portalSecurityUser", searchField = "title")
	private String portalSecurityUserTitle;

	@SearchField(searchFieldPath = "portalSecurityUser", searchField = "phoneNumber")
	private String portalSecurityUserPhoneNumber;

	@SearchField
	private Date eventDate;

	@SearchField
	private String description;


	@SearchField(searchField = "city,region", searchFieldPath = "portalTrackingIpData")
	private String cityState;

	@SearchField(searchField = "countryCode", searchFieldPath = "portalTrackingIpData")
	private String countryCode;


	@SearchField(searchField = "sourceTableName,trackingEventType.sourceTableName", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String sourceTableName;

	@SearchField(searchField = "sourceTableName,trackingEventType.sourceTableName", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceTableNameEquals;


	@SearchField
	private Integer sourceFkFieldId;


	// Custom Search Field
	// Portal Entity ID is NULL AND User Assignment Exists to Portal Entity of View Type
	// OR PortalEntity.PortalEntityViewTypeID = Selected Value
	private Short portalEntityViewTypeId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getTrackingEventTypeId() {
		return this.trackingEventTypeId;
	}


	public void setTrackingEventTypeId(Short trackingEventTypeId) {
		this.trackingEventTypeId = trackingEventTypeId;
	}


	public Short getPortalSecurityUserId() {
		return this.portalSecurityUserId;
	}


	public void setPortalSecurityUserId(Short portalSecurityUserId) {
		this.portalSecurityUserId = portalSecurityUserId;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCityState() {
		return this.cityState;
	}


	public void setCityState(String cityState) {
		this.cityState = cityState;
	}


	public String getCountryCode() {
		return this.countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public Boolean getPortalSecurityUserRolePortalEntitySpecific() {
		return this.portalSecurityUserRolePortalEntitySpecific;
	}


	public void setPortalSecurityUserRolePortalEntitySpecific(Boolean portalSecurityUserRolePortalEntitySpecific) {
		this.portalSecurityUserRolePortalEntitySpecific = portalSecurityUserRolePortalEntitySpecific;
	}


	public String getTrackingEventTypeNameEquals() {
		return this.trackingEventTypeNameEquals;
	}


	public void setTrackingEventTypeNameEquals(String trackingEventTypeNameEquals) {
		this.trackingEventTypeNameEquals = trackingEventTypeNameEquals;
	}


	public String getPortalSecurityUserName() {
		return this.portalSecurityUserName;
	}


	public void setPortalSecurityUserName(String portalSecurityUserName) {
		this.portalSecurityUserName = portalSecurityUserName;
	}


	public String getPortalSecurityUserCompanyName() {
		return this.portalSecurityUserCompanyName;
	}


	public void setPortalSecurityUserCompanyName(String portalSecurityUserCompanyName) {
		this.portalSecurityUserCompanyName = portalSecurityUserCompanyName;
	}


	public String getPortalSecurityUserTitle() {
		return this.portalSecurityUserTitle;
	}


	public void setPortalSecurityUserTitle(String portalSecurityUserTitle) {
		this.portalSecurityUserTitle = portalSecurityUserTitle;
	}


	public String getPortalSecurityUserPhoneNumber() {
		return this.portalSecurityUserPhoneNumber;
	}


	public void setPortalSecurityUserPhoneNumber(String portalSecurityUserPhoneNumber) {
		this.portalSecurityUserPhoneNumber = portalSecurityUserPhoneNumber;
	}


	public String getSourceTableName() {
		return this.sourceTableName;
	}


	public void setSourceTableName(String sourceTableName) {
		this.sourceTableName = sourceTableName;
	}


	public Short[] getTrackingEventTypeIds() {
		return this.trackingEventTypeIds;
	}


	public void setTrackingEventTypeIds(Short[] trackingEventTypeIds) {
		this.trackingEventTypeIds = trackingEventTypeIds;
	}


	public String[] getTrackingEventTypeNames() {
		return this.trackingEventTypeNames;
	}


	public void setTrackingEventTypeNames(String[] trackingEventTypeNames) {
		this.trackingEventTypeNames = trackingEventTypeNames;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public String getSourceTableNameEquals() {
		return this.sourceTableNameEquals;
	}


	public void setSourceTableNameEquals(String sourceTableNameEquals) {
		this.sourceTableNameEquals = sourceTableNameEquals;
	}


	public Short getPortalEntityViewTypeId() {
		return this.portalEntityViewTypeId;
	}


	public void setPortalEntityViewTypeId(Short portalEntityViewTypeId) {
		this.portalEntityViewTypeId = portalEntityViewTypeId;
	}
}
