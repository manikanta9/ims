package com.clifton.portal.tracking;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.notification.PortalNotification;


/**
 * @author manderson
 */
@NonPersistentObject
public class PortalTrackingEventExtended extends PortalTrackingEvent {

	/**
	 * The event only knows the id, so the extended object populates with the real PortalEntity object
	 */
	private PortalEntity portalEntity;

	/**
	 * File Downloads: Based on the event/event type, if source table name references PortalFile, this will be populated
	 * to use for searching, and viewing information about the file.
	 */
	private PortalFile portalFile;


	/**
	 * File Downloads/File List View: Based on the event/event type, if source table name references PortalFileCategory or PortalFile, this will be populated
	 * to use for searching, and viewing information about the file category
	 */
	private PortalFileCategory portalFileCategory;

	/**
	 * Portal Notification Acknowledgements
	 */
	private PortalNotification portalNotification;

	/**
	 * Currently Used for File Download viewing - Not saved - but determined on the fly what RMs apply to the entity (if set) or file post entity and adds their name(s).
	 * There would usually be only one, but some cases there could be multiple
	 */
	private String relationshipManagerNames;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntity getPortalEntity() {
		return this.portalEntity;
	}


	public void setPortalEntity(PortalEntity portalEntity) {
		this.portalEntity = portalEntity;
	}


	public PortalFile getPortalFile() {
		return this.portalFile;
	}


	public void setPortalFile(PortalFile portalFile) {
		this.portalFile = portalFile;
	}


	public PortalFileCategory getPortalFileCategory() {
		return this.portalFileCategory;
	}


	public void setPortalFileCategory(PortalFileCategory portalFileCategory) {
		this.portalFileCategory = portalFileCategory;
	}


	public PortalNotification getPortalNotification() {
		return this.portalNotification;
	}


	public void setPortalNotification(PortalNotification portalNotification) {
		this.portalNotification = portalNotification;
	}


	public String getRelationshipManagerNames() {
		return this.relationshipManagerNames;
	}


	public void setRelationshipManagerNames(String relationshipManagerNames) {
		this.relationshipManagerNames = relationshipManagerNames;
	}
}
