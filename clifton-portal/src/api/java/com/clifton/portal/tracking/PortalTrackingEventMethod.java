package com.clifton.portal.tracking;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>PortalTrackingEventMethod</code> annotation is used on specific methods to log the method call as an event
 * The specific properties for how it is logged is defined by the {@link PortalTrackingEventTypes} so they can be easily reused across methods (since we have internal and client urls)
 *
 * @author manderson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface PortalTrackingEventMethod {

	/**
	 * The event type this method applies to
	 * The event type has additional properties that define how the event is generated
	 */
	PortalTrackingEventTypes eventType();


	/**
	 * If we can associate the event with a particular entity (Client Relationship, Client, Account)
	 * This parameter is passed to most methods as in most cases we are looking at files for a specific entity or related to that entity
	 */
	String portalEntityIdUrlParameter() default "assignedPortalEntityId";
}
