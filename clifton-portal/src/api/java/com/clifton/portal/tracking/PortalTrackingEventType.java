package com.clifton.portal.tracking;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAware;


/**
 * The <code>PortalTrackingEventType</code> defines the type of event that is tracked
 * i.e. Login, File Download, Click, etc.
 *
 * @author manderson
 */
@CacheByName
@PortalTrackingAuditAware
public class PortalTrackingEventType extends NamedEntity<Short> {

	/**
	 * Used for "click" type events that are associated with a particular entity that is clicked
	 * i.e. File Download - what PortalFile was downloaded
	 */
	private String sourceTableName;

	/**
	 * For some event types we might want to disable tracking for certain event types, i.e. like logins
	 * for our internal employees (i.e. role = !portalEntitySpecific)
	 */
	private boolean excludeNonPortalEntitySpecific;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSourceTableName() {
		return this.sourceTableName;
	}


	public void setSourceTableName(String sourceTableName) {
		this.sourceTableName = sourceTableName;
	}


	public boolean isExcludeNonPortalEntitySpecific() {
		return this.excludeNonPortalEntitySpecific;
	}


	public void setExcludeNonPortalEntitySpecific(boolean excludeNonPortalEntitySpecific) {
		this.excludeNonPortalEntitySpecific = excludeNonPortalEntitySpecific;
	}
}
