package com.clifton.portal.tracking;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.tracking.search.PortalTrackingEventExtendedSearchForm;
import com.clifton.portal.tracking.search.PortalTrackingEventSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalTrackingService {

	@PortalSecureMethod
	public List<PortalTrackingEvent> getPortalTrackingEventList(PortalTrackingEventSearchForm searchForm);


	@PortalSecureMethod
	public List<PortalTrackingEvent> getPortalTrackingAuditTrailEventListForSource(String sourceTableName, int sourceFkFieldId);


	@DoNotAddRequestMapping
	public PortalTrackingEvent savePortalTrackingEvent(PortalTrackingEvent bean);


	@DoNotAddRequestMapping
	public void savePortalTrackingEventList(List<PortalTrackingEvent> beanList);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalTrackingEventExtended getPortalTrackingEventExtended(long id);


	/**
	 * @param populateRelationshipManagerNames - Used currently only for file downloads - populates the RM name(s) - usually only one on the extended object prior to returning
	 */
	@PortalSecureMethod
	public List<PortalTrackingEventExtended> getPortalTrackingEventExtendedList(PortalTrackingEventExtendedSearchForm searchForm, boolean populateRelationshipManagerNames);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@PortalSecureMethod
	public PortalTrackingEventType getPortalTrackingEventType(short id);


	@PortalSecureMethod
	public PortalTrackingEventType getPortalTrackingEventTypeByName(String name);


	@PortalSecureMethod
	public List<PortalTrackingEventType> getPortalTrackingEventTypeList();


	/**
	 * Allows updates only, and the only fields that can change are description, and excludeNonPortalEntitySpecific
	 */
	@PortalSecureMethod(adminSecurity = true)
	public PortalTrackingEventType savePortalTrackingEventType(PortalTrackingEventType bean);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
}
