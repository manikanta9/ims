package com.clifton.portal.tracking.ip;

/**
 * @author manderson
 */
public interface PortalTrackingIpService {


	/**
	 * Returns the most recent PortalTrackingIpData object for the given ip.  Will retrieve from the database first, if not found or the data in the database
	 * is over 6 months old, will re-query the ip data and create a new record in the database and return that.
	 */
	public PortalTrackingIpData getPortalTrackingIpServiceByIp(final String ip);
}
