package com.clifton.portal.tracking.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>PortalTrackingAuditAware</code> annotation can be used on DTOs where we want to track Insert, Update, Delete events.
 * By default, all events and properties are audited
 * <p>
 * On application start up, the {@link PortalTrackingAuditAwareObserverRegistrator} finds all {@link com.clifton.core.dataaccess.dao.UpdatableDAO}s
 * and if the DTO for that dao has this annotation, then a new instance of the {@link PortalTrackingAuditAwareObserver} is created with the specified ignoreProperties if any,
 * and that observer is registered for the selected events only.
 *
 * @author manderson
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PortalTrackingAuditAware {

	boolean insert() default true;


	boolean update() default true;


	boolean delete() default true;
}
