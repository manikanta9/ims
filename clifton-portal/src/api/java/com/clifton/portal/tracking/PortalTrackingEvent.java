package com.clifton.portal.tracking;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.tracking.ip.PortalTrackingIpData;

import java.util.Date;


/**
 * The <code>PortalTrackingEvent</code> defines a specific user event that occurred
 *
 * @author manderson
 */
public class PortalTrackingEvent extends BaseSimpleEntity<Long> {

	private PortalTrackingEventType trackingEventType;

	private PortalSecurityUser portalSecurityUser;

	private PortalTrackingIpData portalTrackingIpData;

	// Note: Date with Time
	private Date eventDate;

	/**
	 * Some events may need to specify a table for each event. i.e. Audit Trail
	 */
	private String sourceTableName;

	/**
	 * When the event/event type references a table, we enter the id of the entity here
	 */
	private Integer sourceFkFieldId;

	/**
	 * Some cases/events we can also track what entity the user was "viewing"
	 */
	private Integer portalEntityId;

	/**
	 * Populated with additional information - for example the Portal File name
	 * So if the source entity is deleted we have history of what it was?
	 */
	private String description;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCoalesceSourceTableName() {
		if (!StringUtils.isEmpty(getSourceTableName())) {
			return getSourceTableName();
		}
		if (getTrackingEventType() != null) {
			return getTrackingEventType().getSourceTableName();
		}
		return null;
	}


	public String getSourceEntityLabel() {
		if (StringUtils.isEmpty(getCoalesceSourceTableName()) && getSourceFkFieldId() == null) {
			return "";
		}
		return getCoalesceSourceTableName() + ": " + getSourceFkFieldId();
	}


	@Override
	public String toString() {
		return "PortalTrackingEvent [" + //
				"eventType=" + (this.trackingEventType == null ? null : this.trackingEventType.getName()) + //
				", user=" + (this.portalSecurityUser == null ? null : this.portalSecurityUser.getLabel()) + //
				", eventDate=" + DateUtils.fromDateSmart(this.eventDate) + //
				", table=" + getCoalesceSourceTableName() + //
				", sourceFkFieldId=" + this.sourceFkFieldId + //
				", portalEntityId=" + this.portalEntityId + //
				", description=" + this.description + "]";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalTrackingEventType getTrackingEventType() {
		return this.trackingEventType;
	}


	public void setTrackingEventType(PortalTrackingEventType trackingEventType) {
		this.trackingEventType = trackingEventType;
	}


	public PortalSecurityUser getPortalSecurityUser() {
		return this.portalSecurityUser;
	}


	public void setPortalSecurityUser(PortalSecurityUser portalSecurityUser) {
		this.portalSecurityUser = portalSecurityUser;
	}


	public PortalTrackingIpData getPortalTrackingIpData() {
		return this.portalTrackingIpData;
	}


	public void setPortalTrackingIpData(PortalTrackingIpData portalTrackingIpData) {
		this.portalTrackingIpData = portalTrackingIpData;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSourceTableName() {
		return this.sourceTableName;
	}


	public void setSourceTableName(String sourceTableName) {
		this.sourceTableName = sourceTableName;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}
}
