package com.clifton.portal.email.cache;

import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;

import java.util.List;


/**
 * The <code>PortalEmailTeamAddressListForEntityAndCategoryCache</code> caches the list of String values of email addresses
 * that apply to a specific entity (account, client, client relationship) and file category.  There are internal "Support" email addresses
 * and external "contact us" email addresses.  Most cases they are the same.
 *
 * @author manderson
 */
public interface PortalEmailTeamAddressListForEntityAndCategoryCache {


	public List<String> getPortalEmailTeamAddressListForEntityAndCategory(PortalEntity portalEntity, PortalFileCategory fileCategory, boolean internalSupport);
}
