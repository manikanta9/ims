package com.clifton.portal.email;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.email.cache.PortalEmailTeamAddressListForEntityAndCategoryCache;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileAssignment;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalEmailServiceImpl implements PortalEmailService {

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;

	private PortalFileService portalFileService; // Note: Circular Dependency
	private PortalFileSetupService portalFileSetupService;

	private PortalEmailTeamAddressListForEntityAndCategoryCache portalEmailTeamAddressListForEntityAndCategoryCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalEmailAddressDefault() {
		return getPortalEntitySetupService().getPortalEntityViewTypeDefault().getDefaultContactEmailAddress();
	}


	@Override
	public PortalEmail getPortalEmailForCategoryAndEntity(short categoryId, Integer portalEntityId) {
		PortalFileCategory category = getPortalFileSetupService().getPortalFileCategory(categoryId);

		PortalEmail email = new PortalEmail();

		// Would only be NULL for Global Files in which case just use the category email address
		PortalEntity portalEntity = null;
		if (portalEntityId != null) {
			portalEntity = getPortalEntityService().getPortalEntity(portalEntityId);

			// If we are on the "Contact Us" section - all emails go to RM and CC teams
			boolean relationshipManagerPrimaryContact = StringUtils.isEqual(PortalFileCategory.CATEGORY_NAME_CONTACT_US, category.getRootParent().getName());
			email.setSubject("RE: " + portalEntity.getEntityLabelWithSourceSection() + " - " + category.getName());

			// There is Usually only one, but there could be multiple
			List<PortalEntity> relationshipManagerList = getRelationshipManagerList(portalEntityId);
			if (!CollectionUtils.isEmpty(relationshipManagerList)) {
				for (PortalEntity relationshipManager : CollectionUtils.getIterable(relationshipManagerList)) {
					PortalEntityFieldValue emailAddress = CollectionUtils.getFirstElement(BeanUtils.filter(relationshipManager.getFieldValueList(), fieldValue -> StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL, fieldValue.getPortalEntityFieldType().getName())));
					if (emailAddress != null) {
						email.addEmailAddress(emailAddress.getValue(), relationshipManagerPrimaryContact);
					}
				}
			}
			else {
				relationshipManagerPrimaryContact = false; // If there is no RM, set the TO to the teams
			}
			List<String> teamEmailAddressList = getPortalEmailTeamAddressListForEntityAndCategoryCache().getPortalEmailTeamAddressListForEntityAndCategory(portalEntity, category, false);
			for (String emailAddress : CollectionUtils.getIterable(teamEmailAddressList)) {
				email.addEmailAddress(emailAddress, !relationshipManagerPrimaryContact);
			}
		}
		else {
			email.setSubject("RE: " + category.getName());
			if (!StringUtils.isEmpty(category.getCoalesceCategoryEmailAddress())) {
				email.addEmailAddress(category.getCoalesceCategoryEmailAddress(), true);
			}
		}

		// Finally if there is no "TO" determined - then use the entity's view type default, else system default
		if (CollectionUtils.isEmpty(email.getToAddressList())) {
			if (portalEntity != null && portalEntity.getEntityViewType() != null) {
				email.addEmailAddress(portalEntity.getEntityViewType().getDefaultContactEmailAddress(), true);
			}
			else {
				email.addEmailAddress(getPortalEmailAddressDefault(), true);
			}
		}
		return email;
	}


	@Override
	public List<String> getPortalSupportEmailAddressListForPortalFile(PortalFile portalFile) {
		List<String> teamEmailAddressList = null;
		PortalEntity postEntity = portalFile.getPostPortalEntity();
		if (!portalFile.isGlobalFile() && postEntity != null) { // If it's not a global file post entity would be defined
			// Posted to Specific Securable Entity
			if (postEntity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
				teamEmailAddressList = getPortalEmailTeamAddressListForEntityAndCategoryCache().getPortalEmailTeamAddressListForEntityAndCategory(postEntity, portalFile.getFileCategory(), true);
			}
			else if (postEntity.getEntitySourceSection().getChildEntityType() != null) { // It's a rollup - pull the values for the first assignment
				PortalFileAssignment fileAssignment = CollectionUtils.getFirstElement(getPortalFileService().getPortalFileAssignmentListByFile(portalFile.getId()));
				if (fileAssignment != null) {
					teamEmailAddressList = getPortalEmailTeamAddressListForEntityAndCategoryCache().getPortalEmailTeamAddressListForEntityAndCategory(fileAssignment.getReferenceTwo(), portalFile.getFileCategory(), true);
				}
			}
		}
		if (CollectionUtils.isEmpty(teamEmailAddressList)) {
			// If nothing there but file is posted to a specific view type use that
			if (portalFile.getEntityViewType() != null) {
				return CollectionUtils.createList(portalFile.getEntityViewType().getDefaultContactEmailAddress());
			}
			// Or if the post entity has a view type use that
			else if (postEntity != null && postEntity.getEntityViewType() != null) {
				return CollectionUtils.createList(postEntity.getEntityViewType().getDefaultContactEmailAddress());
			}

			// Else use the final default
			return CollectionUtils.createList(getPortalEmailAddressDefault());
		}
		return teamEmailAddressList;
	}


	@Override
	public List<String> getPortalContactTeamEmailAddressListForPortalFile(PortalFileExtended portalFile) {
		return getPortalEmailTeamAddressListForEntityAndCategoryCache().getPortalEmailTeamAddressListForEntityAndCategory(portalFile.getAssignedPortalEntity(), portalFile.getFileCategory(), false);
	}


	@Override
	public List<String> getPortalSupportTeamEmailList() {
		List<String> emailAddressList = new ArrayList<>();
		PortalFileCategorySearchForm fileCategorySearchForm = new PortalFileCategorySearchForm();
		fileCategorySearchForm.setCategoryEmailAddressPopulated(true);
		for (PortalFileCategory fileCategory : CollectionUtils.getIterable(getPortalFileSetupService().getPortalFileCategoryList(fileCategorySearchForm))) {
			if (!emailAddressList.contains(fileCategory.getCategoryEmailAddress())) {
				emailAddressList.add(fileCategory.getCategoryEmailAddress());
			}
		}

		// Add in all Investment Teams
		PortalEntitySearchForm teamSearchForm = new PortalEntitySearchForm();
		teamSearchForm.setEntityTypeId(getPortalEntitySetupService().getPortalEntityTypeByName(PortalEntityType.ENTITY_TYPE_TEAM).getId());
		List<PortalEntity> portalEntityList = getPortalEntityService().getPortalEntityList(teamSearchForm);
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			PortalEntityFieldValue emailAddress = CollectionUtils.getFirstElement(BeanUtils.filter(getPortalEntityService().getPortalEntityFieldValueListByEntity(portalEntity.getId()), fieldValue -> StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL, fieldValue.getPortalEntityFieldType().getName())));
			if (emailAddress != null && !emailAddressList.contains(emailAddress.getValue())) {
				emailAddressList.add(emailAddress.getValue());
			}
		}

		// Add In All Overrides
		List<PortalEntityFieldValue> overrideValues = getPortalEntityService().getPortalEntityFieldValueListByEntityTypeAndFieldName(PortalEntityType.ENTITY_TYPE_CLIENT_RELATIONSHIP, PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL);
		for (PortalEntityFieldValue overrideValue : CollectionUtils.getIterable(overrideValues)) {
			if (!emailAddressList.contains(overrideValue.getValue())) {
				emailAddressList.add(overrideValue.getValue());
			}
		}

		// Add in All View Type Emails
		for (PortalEntityViewType viewType : CollectionUtils.getIterable(getPortalEntitySetupService().getPortalEntityViewTypeList(new PortalEntityViewTypeSearchForm(), true))) {
			if (!emailAddressList.contains(viewType.getDefaultContactEmailAddress())) {
				emailAddressList.add(viewType.getDefaultContactEmailAddress());
			}
		}

		emailAddressList = CollectionUtils.sort(emailAddressList);
		return emailAddressList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalEntity> getRelationshipManagerList(int portalEntityId) {
		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		searchForm.setAssignedPortalEntityId(portalEntityId);
		searchForm.setFileGroupId(getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, PortalFileCategory.CATEGORY_NAME_RELATIONSHIP_MANAGER).getId());
		return getPortalFileService().getPortalFileSourceEntityList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalEmailTeamAddressListForEntityAndCategoryCache getPortalEmailTeamAddressListForEntityAndCategoryCache() {
		return this.portalEmailTeamAddressListForEntityAndCategoryCache;
	}


	public void setPortalEmailTeamAddressListForEntityAndCategoryCache(PortalEmailTeamAddressListForEntityAndCategoryCache portalEmailTeamAddressListForEntityAndCategoryCache) {
		this.portalEmailTeamAddressListForEntityAndCategoryCache = portalEmailTeamAddressListForEntityAndCategoryCache;
	}
}
