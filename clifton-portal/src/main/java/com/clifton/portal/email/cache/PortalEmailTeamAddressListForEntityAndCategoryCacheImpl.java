package com.clifton.portal.email.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileAssignment;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PortalEmailTeamAddressListForEntityAndCategoryCache</code> caches the list of String values of email addresses
 * that apply to a specific entity (account, client, client relationship) and file category.  There are internal "Support" email addresses
 * and external "contact us" email addresses.  Most cases they are the same.
 * <p>
 * The cache is cleared entirely when the following occurs (note these changes are rare and are currently only updated during a daily batch job from IMS)
 * 1.  PortalEntityFieldValue - If Entity Type = Investment Team and Field Type = Email Address
 * 2.  PortalEntityFieldValue - If Entity Type = Client Relationship and Field Type = Portal Contact Team Email or Portal Support Team Email
 * 3.  PortalFileCategory - If CategoryEmailAddress is updated
 * 4.  PortalFile and PortalFileAssignment - If file is under "Meet your Team" file category
 *
 * @author manderson
 */
@Component
public class PortalEmailTeamAddressListForEntityAndCategoryCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, List<String>>, PortalEmailTeamAddressListForEntityAndCategoryCache {

	private CacheHandler<String, List<String>> cacheHandler;

	private PortalEntityService portalEntityService;

	private PortalFileService portalFileService;
	private PortalFileSetupService portalFileSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getPortalEmailTeamAddressListForEntityAndCategory(PortalEntity portalEntity, PortalFileCategory fileCategory, boolean internalSupport) {
		String key = (portalEntity == null ? "NULL" : portalEntity.getId()) + "_" + fileCategory.getId() + "_" + internalSupport;
		List<String> emailAddressList = getCacheHandler().get(getCacheName(), key);
		if (emailAddressList == null) {
			emailAddressList = getPortalEmailTeamAddressListForEntityAndCategoryImpl(portalEntity, fileCategory, internalSupport);
			if (emailAddressList == null) {
				emailAddressList = new ArrayList<>();
			}
			getCacheHandler().put(getCacheName(), key, emailAddressList);
		}
		return emailAddressList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<String> getPortalEmailTeamAddressListForEntityAndCategoryImpl(PortalEntity portalEntity, PortalFileCategory portalFileCategory, boolean internalSupport) {
		// If there is an override - use it
		String contactOverride = getPortalContactOverrideEmailAddress(portalEntity, internalSupport);
		if (!StringUtils.isEmpty(contactOverride)) {
			return CollectionUtils.createList(contactOverride);
		}
		// Otherwise use the file category email addresses and investment teams
		else {
			String teamEmail = portalFileCategory.getCoalesceCategoryEmailAddress();
			if (!StringUtils.isEmpty(teamEmail)) {
				return CollectionUtils.createList(teamEmail);
			}
			// Investment Teams
			else {
				List<PortalEntity> teamList = getInvestmentTeamList(portalEntity);
				if (!CollectionUtils.isEmpty(teamList)) {
					List<String> emailList = new ArrayList<>();
					for (PortalEntity team : teamList) {
						PortalEntityFieldValue emailAddress = CollectionUtils.getFirstElement(BeanUtils.filter(team.getFieldValueList(), fieldValue -> StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL, fieldValue.getPortalEntityFieldType().getName())));
						if (emailAddress != null) {
							emailList.add(emailAddress.getValue());
						}
					}
					return emailList;
				}
			}
		}
		return null;
	}


	/**
	 * For the given portal entity - continues up the chain until we find one (usually top level except for OCIO cases) where the entity type = Client Relationship (Client Relationships and Sister Client Groups)
	 * if the override email is set for given option (internalSupport = Portal Support Team Email, else Portal Contact Team Email) will return that address
	 */
	private String getPortalContactOverrideEmailAddress(PortalEntity portalEntity, boolean internalSupport) {
		String fieldType = (internalSupport ? PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL : PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL);
		while (portalEntity != null) {
			PortalEntityFieldValue emailAddress = CollectionUtils.getFirstElement(BeanUtils.filter(getPortalEntityService().getPortalEntityFieldValueListByEntity(portalEntity.getId()), fieldValue -> StringUtils.isEqual(fieldType, fieldValue.getPortalEntityFieldType().getName())));
			if (emailAddress != null) {
				return emailAddress.getValue();
			}
			portalEntity = portalEntity.getParentPortalEntity();
		}
		return null;
	}


	private List<PortalEntity> getInvestmentTeamList(PortalEntity portalEntity) {
		if (portalEntity == null) {
			return null;
		}
		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		searchForm.setAssignedPortalEntityId(portalEntity.getId());
		searchForm.setFileGroupId(getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, PortalFileCategory.CATEGORY_NAME_MEET_YOUR_TEAM).getId());
		return getPortalFileService().getPortalFileSourceEntityList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                      Observer Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// The cache is cleared entirely when the following occurs (note these changes are rare and are currently only updated during a daily batch job from IMS)
			// 1.  PortalEntityFieldValue - If Entity Type = Investment Team and Field Type = Email Address
			// 2.  PortalEntityFieldValue - If Entity Type = Client Relationship and Field Type = Portal Contact Team Email or Portal Support Team Email
			if (bean instanceof PortalEntityFieldValue) {
				PortalEntityFieldType fieldType = ((PortalEntityFieldValue) bean).getPortalEntityFieldType();
				if (PortalEntityType.ENTITY_TYPE_TEAM.equals(fieldType.getPortalEntityType().getName()) && PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL.equals(fieldType.getName())) {
					getCacheHandler().clear(getCacheName());
				}
				if (PortalEntityType.ENTITY_TYPE_CLIENT_RELATIONSHIP.equals(fieldType.getPortalEntityType().getName()) && (PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL.equals(fieldType.getName()) || PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL.equals(fieldType.getName()))) {
					getCacheHandler().clear(getCacheName());
				}
			}
			// 3.  PortalFileCategory - If Insert/Delete or CategoryEmailAddress is updated
			else if (bean instanceof PortalFileCategory) {
				if (event.isInsert() || event.isDelete()) {
					getCacheHandler().clear(getCacheName());
				}
				else {
					PortalFileCategory originalBean = (PortalFileCategory) getOriginalBean(dao, bean);
					if (!StringUtils.isEqual(((PortalFileCategory) bean).getCategoryEmailAddress(), originalBean.getCategoryEmailAddress())) {
						getCacheHandler().clear(getCacheName());
					}
				}
			}
			// 4.  PortalFile and PortalFileAssignment - If file is under "Meet your Team" file category
			else if (bean instanceof PortalFile) {
				if (PortalFileCategory.CATEGORY_NAME_MEET_YOUR_TEAM.equals(((PortalFile) bean).getFileCategory().getName())) {
					getCacheHandler().clear(getCacheName());
				}
			}
			else if (bean instanceof PortalFileAssignment) {
				if (PortalFileCategory.CATEGORY_NAME_MEET_YOUR_TEAM.equals(((PortalFileAssignment) bean).getReferenceOne().getFileCategory().getName())) {
					getCacheHandler().clear(getCacheName());
				}
			}
			else {
				throw new IllegalStateException("PortalEmailTeamAddressListForEntityAndCategoryCacheImpl: No definition for clearing the cache on changes to Bean ["
						+ bean + "] of type " + dao.getConfiguration().getBeanClass().getName() + ".");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public CacheHandler<String, List<String>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, List<String>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
