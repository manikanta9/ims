package com.clifton.portal.entity.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalEntityUploadServiceImpl implements PortalEntityUploadService {


	private FileUploadHandler fileUploadHandler;

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;

	private PortalFileService portalFileService;
	private PortalFileSetupService portalFileSetupService;

	private PortalSecurityUserService portalSecurityUserService;
	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status uploadPortalEntityUploadFile(PortalEntityUploadCommand uploadCommand) {
		ValidationUtils.assertNotNull(uploadCommand.getSourceSystem(), "A source system is required to perform an upload");
		if (uploadCommand.getSourceSystem().isAdministratorOnly()) {
			PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
			ValidationUtils.assertNotNull(currentUser, "Current User is Missing");
			ValidationUtils.assertTrue(currentUser.getUserRole().isAdministrator(), "Only Administrators can upload portal entities for source system " + uploadCommand.getSourceSystem().getName());
		}

		// Put everything in a try/catch block to ensure we return the status object with the error
		try {
			// Convert File to List of Beans
			List<PortalEntityUploadTemplate> entityUploadList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
			PortalEntityUploadConfig uploadConfig = new PortalEntityUploadConfig(uploadCommand.getSourceSystem(), entityUploadList);

			// There are the only 2 categories now that are managed via manual relationships through the system - track them as we go through entities then "post" at the end
			Map<String, List<PortalEntity>> relationshipManagerPosting = new HashMap<>();
			Map<String, List<PortalEntity>> meetYourTeamPosting = new HashMap<>();

			populatePortalEntityMap(uploadCommand, uploadConfig, relationshipManagerPosting, meetYourTeamPosting);
			Set<PortalEntitySourceSection> sourceSectionListAffected = savePortalEntityMap(uploadCommand, uploadConfig);

			// Manual File Posting - Relationship Managers
			if (!CollectionUtils.isEmpty(relationshipManagerPosting)) {
				savePortalFilePosting(uploadCommand, PortalFileCategory.CATEGORY_NAME_RELATIONSHIP_MANAGER, relationshipManagerPosting, sourceSectionListAffected);
			}
			// Manual File Posting - Meet Your Team
			if (!CollectionUtils.isEmpty(meetYourTeamPosting)) {
				savePortalFilePosting(uploadCommand, PortalFileCategory.CATEGORY_NAME_MEET_YOUR_TEAM, meetYourTeamPosting, sourceSectionListAffected);
			}

			if (uploadCommand.isDeleteEntitiesMissingFromFile()) {
				Status deletedEntityStatus = getPortalEntityService().deletePortalEntitiesMarkedForDeletion(uploadCommand.getSourceSystem().getName(), null);
				uploadCommand.getStatus().addDetail(StatusDetail.CATEGORY_SUCCESS, deletedEntityStatus.getMessage());
				if (deletedEntityStatus.getErrorCount() > 0) {
					for (StatusDetail errorDetail : CollectionUtils.getIterable(deletedEntityStatus.getErrorList())) {
						uploadCommand.getStatus().addError(errorDetail.getNote());
					}
				}
			}

			// This is NOT source system specific, so IMS batch job would pick it up, however after updating portal entities should be run
			try {
				getPortalSecurityUserService().processPortalSecurityUserAssignmentDisabledList();
				uploadCommand.getStatus().addDetail(StatusDetail.CATEGORY_SUCCESS, "Successfully processed all user assignments for terminated portal entities.");
			}
			catch (Throwable e) {
				String message = "Error processing user assignments for terminated portal entities: " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				uploadCommand.getStatus().addError(message);
			}
		}
		catch (Throwable e) {
			String message = "Error processing upload " + ExceptionUtils.getOriginalMessage(e);
			LogUtils.errorOrInfo(getClass(), message, e);
			uploadCommand.getStatus().addError(message);
		}


		uploadCommand.getStatus().setMessageWithErrors("Portal Entity Upload Completed.", 20);
		return uploadCommand.getStatus();
	}


	private void populatePortalEntityMap(PortalEntityUploadCommand uploadCommand, PortalEntityUploadConfig uploadConfig, Map<String, List<PortalEntity>> relationshipManagerPosting, Map<String, List<PortalEntity>> meetYourTeamPosting) {
		// Convert Template Beans to Real Portal Entity Beans
		for (Map.Entry<String, PortalEntityUploadTemplate> entityUploadEntry : uploadConfig.getEntityUploadTemplateMap().entrySet()) {
			PortalEntityUploadTemplate entityUploadTemplate = entityUploadEntry.getValue();
			PortalEntity portalEntity = uploadConfig.getPortalEntityForUploadTemplate(entityUploadTemplate, getPortalEntitySetupService(), getPortalEntityService());
			// If returned Portal Entity is for another Source System, put it in the map, but don't make any edits
			if (portalEntity != null && !uploadCommand.getSourceSystem().equals(portalEntity.getEntitySourceSection().getSourceSystem())) {
				uploadCommand.getStatus().addSkipped("Skipping " + portalEntity.getEntityLabelWithSourceSection() + " because it is associated with another source system [" + portalEntity.getEntitySourceSection().getSourceSystem().getName() + "] with a higher priority.");
				uploadConfig.getEntityUploadMap().put(entityUploadEntry.getKey(), portalEntity);
				continue;
			}
			List<PortalEntityFieldValue> fieldValueList = null;
			if (portalEntity == null) {
				portalEntity = new PortalEntity();
				portalEntity.setEntitySourceSection(uploadConfig.getPortalEntitySourceSection(entityUploadTemplate.getPortalEntitySourceSystemName(), entityUploadTemplate.getPortalEntitySourceSectionName(), getPortalEntitySetupService()));
				portalEntity.setSourceFkFieldId(entityUploadTemplate.getSourceFKFieldId());
			}
			else {
				fieldValueList = getPortalEntityService().getPortalEntityFieldValueListByEntity(portalEntity.getId());
			}
			// NOTE: PARENT ENTITIES ARE APPLIED AFTER ALL ENTITIES ARE FIRST MAPPED
			portalEntity.setName(entityUploadTemplate.getEntityName());
			portalEntity.setLabel(StringUtils.coalesce(false, entityUploadTemplate.getEntityLabel(), entityUploadTemplate.getEntityName()));
			portalEntity.setDescription(entityUploadTemplate.getEntityDescription());
			portalEntity.setStartDate(entityUploadTemplate.getStartDate());
			portalEntity.setEndDate(entityUploadTemplate.getEndDate());
			portalEntity.setTerminationDate(entityUploadTemplate.getTerminationDate());

			// If set in the file, use that
			if (!StringUtils.isEmpty(entityUploadTemplate.getPortalEntityViewTypeName())) {
				portalEntity.setEntityViewType(getPortalEntitySetupService().getPortalEntityViewTypeByName(entityUploadTemplate.getPortalEntityViewTypeName()));
			}
			// Otherwise if set as a default value - only set if securable entity
			else if (uploadCommand.getEntityViewType() != null && portalEntity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
				portalEntity.setEntityViewType(uploadCommand.getEntityViewType());
			}

			List<PortalEntityFieldType> fieldTypeList = uploadConfig.getPortalEntityFieldTypeListByPortalEntityType(portalEntity.getEntitySourceSection().getEntityType().getId(), getPortalEntitySetupService());
			if (CollectionUtils.isEmpty(fieldTypeList)) {
				portalEntity.setFieldValueList(null);
			}
			else {
				portalEntity.setFieldValueList(new ArrayList<>());
				for (PortalEntityFieldType fieldType : fieldTypeList) {
					String fieldTypeKey = (portalEntity.getEntitySourceSection().getEntityType().getName() + "-" + fieldType.getName()).toLowerCase().trim();
					Object value = entityUploadTemplate.getUnmappedColumnValueMap().get(fieldTypeKey);
					// If value is null, try without the entity type
					if (value == null) {
						fieldTypeKey = fieldType.getName().toLowerCase().trim();
						value = entityUploadTemplate.getUnmappedColumnValueMap().get(fieldTypeKey);
					}
					// If Value is Missing but it is one of the defaults on the upload - set it
					// Otherwise if defaulted
					if (value == null && StringUtils.isEqualIgnoreCase("portal Contact Team Email", fieldTypeKey) && !StringUtils.isEmpty(uploadCommand.getPortalContactTeamEmail())) {
						value = uploadCommand.getPortalContactTeamEmail();
					}
					else if (value == null && StringUtils.isEqualIgnoreCase("portal Support Team Email", fieldTypeKey) && !StringUtils.isEmpty(uploadCommand.getPortalSupportTeamEmail())) {
						value = uploadCommand.getPortalSupportTeamEmail();
					}
					if (value != null) {
						// See if we already have a field value for it
						PortalEntityFieldValue fieldTypeValue = null;
						for (PortalEntityFieldValue fieldValue : CollectionUtils.getIterable(fieldValueList)) {
							if (fieldType.equals(fieldValue.getPortalEntityFieldType())) {
								fieldTypeValue = fieldValue;
								break;
							}
						}
						if (fieldTypeValue == null) {
							fieldTypeValue = new PortalEntityFieldValue();
							fieldTypeValue.setPortalEntity(portalEntity);
							fieldTypeValue.setPortalEntityFieldType(fieldType);
						}
						fieldTypeValue.setValue(value.toString());
						portalEntity.getFieldValueList().add(fieldTypeValue);
					}
				}
			}
			if (!StringUtils.isEmpty(entityUploadTemplate.getMeetYourTeam())) {
				if (!meetYourTeamPosting.containsKey(entityUploadTemplate.getMeetYourTeam())) {
					meetYourTeamPosting.put(entityUploadTemplate.getMeetYourTeam(), new ArrayList<>());
				}
				meetYourTeamPosting.get(entityUploadTemplate.getMeetYourTeam()).add(portalEntity);
			}
			if (!StringUtils.isEmpty(entityUploadTemplate.getRelationshipManager())) {
				if (!relationshipManagerPosting.containsKey(entityUploadTemplate.getRelationshipManager())) {
					relationshipManagerPosting.put(entityUploadTemplate.getRelationshipManager(), new ArrayList<>());
				}
				relationshipManagerPosting.get(entityUploadTemplate.getRelationshipManager()).add(portalEntity);
			}
			uploadConfig.setPortalEntityInMap(entityUploadEntry.getKey(), portalEntity);
		}
		// SET/CONFIRM PARENT SELECTIONS
		for (Map.Entry<String, PortalEntityUploadTemplate> entityUploadEntry : uploadConfig.getEntityUploadTemplateMap().entrySet()) {
			PortalEntity entity = uploadConfig.getEntityUploadMap().get(entityUploadEntry.getKey());
			// Only set parent if returned Portal Entity is for same Source System
			if (uploadCommand.getSourceSystem().equals(entity.getEntitySourceSection().getSourceSystem())) {
				if (entityUploadEntry.getValue().isParentPopulated()) {
					PortalEntity parentEntity = uploadConfig.getPortalEntityForParentUploadTemplate(entityUploadEntry.getValue(), getPortalEntitySetupService(), getPortalEntityService());
					if (parentEntity == null) {
						uploadCommand.getStatus().addError("Cannot insert/update " + entityUploadEntry.getValue().getLabel() + " because parent entity doesn't exist and is not in the upload file.");
						uploadConfig.getEntityUploadMap().remove(entityUploadEntry.getKey());
					}
					else {
						entity.setParentPortalEntity(parentEntity);
					}
				}
			}
		}
	}


	/**
	 * Saves the Portal Entities, Marks for Deletion which entities are no longer included.
	 * Returns the list of source sections that are affected so file postings won't remove RMs and Teams if those entities aren't included in the file
	 */
	private Set<PortalEntitySourceSection> savePortalEntityMap(PortalEntityUploadCommand uploadCommand, PortalEntityUploadConfig uploadConfig) {
		List<PortalEntity> entityList = new ArrayList<>(uploadConfig.getEntityUploadMap().values());
		// Filter Out Entities for Other Source Systems
		entityList = BeanUtils.filter(entityList, portalEntity -> uploadCommand.getSourceSystem().equals(portalEntity.getEntitySourceSection().getSourceSystem()));

		// Break Out By Source Section
		Map<PortalEntitySourceSection, List<PortalEntity>> sourceSectionListMap = BeanUtils.getBeansMap(entityList, PortalEntity::getEntitySourceSection);
		Map<Integer, List<PortalEntitySourceSection>> sourceSectionMaxLevelsDeepMap = new HashMap<>();

		int maxLevelsDeep = 1;
		for (Map.Entry<PortalEntitySourceSection, List<PortalEntity>> sourceSectionListEntry : sourceSectionListMap.entrySet()) {
			int levelsDeep = CoreMathUtils.getBeanWithMaxProperty(sourceSectionListEntry.getValue(), "level", true).getLevel();
			if (maxLevelsDeep < levelsDeep) {
				maxLevelsDeep = levelsDeep;
			}
			if (!sourceSectionMaxLevelsDeepMap.containsKey(levelsDeep)) {
				sourceSectionMaxLevelsDeepMap.put(levelsDeep, new ArrayList<>());
			}
			sourceSectionMaxLevelsDeepMap.get(levelsDeep).add(sourceSectionListEntry.getKey());
		}

		int levelsDeep = 1;
		while (levelsDeep <= maxLevelsDeep) {
			List<PortalEntitySourceSection> sourceSectionList = sourceSectionMaxLevelsDeepMap.get(levelsDeep);
			for (PortalEntitySourceSection sourceSection : CollectionUtils.getIterable(sourceSectionList)) {
				List<PortalEntity> saveList = sourceSectionListMap.get(sourceSection);
				savePortalEntityListForSourceSection(uploadCommand, sourceSection, saveList);
			}
			levelsDeep++;
		}
		return sourceSectionListMap.keySet();
	}


	private void savePortalEntityListForSourceSection(PortalEntityUploadCommand uploadCommand, PortalEntitySourceSection sourceSection, List<PortalEntity> portalEntityList) {
		int skipCount = 0;
		int failCount = 0;
		int successCount = 0;
		int deleteCount = 0;

		Date now = new Date();
		if (uploadCommand.isDeleteEntitiesMissingFromFile()) {
			// Anything that exists now, but isn't in the save list should be marked for deletion
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setEntitySourceSectionId(sourceSection.getId());
			List<PortalEntity> existingList = getPortalEntityService().getPortalEntityList(searchForm);
			for (PortalEntity portalEntity : CollectionUtils.getIterable(existingList)) {
				if (!portalEntityList.contains(portalEntity)) {
					if (portalEntity.isDeleted()) {
						skipCount++;
					}
					else {
						portalEntity.setDeleted(true);
						try {
							getPortalEntityService().savePortalEntity(portalEntity);
							deleteCount++;
						}
						catch (Throwable e) {
							failCount++;
							String message = "Error saving Portal Entity: " + portalEntity.getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
							LogUtils.errorOrInfo(getClass(), message, e);
							uploadCommand.getStatus().addError(message);
						}
					}
				}
			}
		}

		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			portalEntity.setDeleted(false);
			try {
				portalEntity = getPortalEntityService().savePortalEntity(portalEntity);
				if (DateUtils.isDateAfterOrEqual(portalEntity.getUpdateDate(), now)) {
					successCount++;
				}
				else {
					skipCount++;
					// Not adding to the details because it might be too many depending on the size of the file
				}
			}
			catch (Throwable e) {
				failCount++;
				String message = "Error saving Portal Entity: " + portalEntity.getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				uploadCommand.getStatus().addError(message);
			}
		}
		uploadCommand.getStatus().addDetail(StatusDetail.CATEGORY_SUCCESS, sourceSection.getName() + ": Successful: " + successCount + ", Deleted: " + deleteCount + ", Skipped: " + skipCount + " Failed: " + failCount);
	}


	private void savePortalFilePosting(PortalEntityUploadCommand uploadCommand, String fileCategoryName, Map<String, List<PortalEntity>> filePostingMap, Set<PortalEntitySourceSection> sourceSectionSetAffected) {
		PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, fileCategoryName);
		ValidationUtils.assertNotNull(fileCategory, "Cannot file File Category Group for name [" + fileCategoryName + "]");

		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setFileGroupId(fileCategory.getId());
		List<PortalFile> fileList = getPortalFileService().getPortalFileList(searchForm);

		Map<String, PortalFile> sourceEntityNamePortalFileMap = BeanUtils.getBeanMap(fileList, PortalFile::getEntitySourceLabel);
		Set<String> sourceEntityKeysUsed = new HashSet<>();

		int errorCount = 0;
		for (Map.Entry<String, List<PortalEntity>> filePostingMapEntry : filePostingMap.entrySet()) {
			// Find Source Entity
			PortalEntitySearchForm sourceEntitySearchForm = new PortalEntitySearchForm();
			sourceEntitySearchForm.setNameOrLabelEquals(filePostingMapEntry.getKey());
			sourceEntitySearchForm.setEntityTypeId(fileCategory.getSourceEntityType().getId());
			PortalEntity sourceEntity = CollectionUtils.getFirstElement(getPortalEntityService().getPortalEntityList(sourceEntitySearchForm));
			if (sourceEntity == null) {
				errorCount++;
				uploadCommand.getStatus().addError("Cannot update file postings for [" + fileCategoryName + ": " + filePostingMapEntry.getKey() + "]. Missing " + fileCategory.getSourceEntityType().getName() + " from the system.");
				continue;
			}
			// Find the Portal File for the Source Entity
			String sourceEntityKey = sourceEntity.getEntitySourceSection().getLabel() + ": " + sourceEntity.getSourceFkFieldId();
			PortalFile portalFile = sourceEntityNamePortalFileMap.get(sourceEntityKey);
			if (portalFile == null) {
				portalFile = new PortalFile();
				portalFile.setFileCategory(fileCategory);
				portalFile.setEntitySourceSection(sourceEntity.getEntitySourceSection());
				portalFile.setSourceFkFieldId(sourceEntity.getSourceFkFieldId());
				portalFile.setApproved(true);
				portalFile.setPublishDate(new Date());
				portalFile.setReportDate(new Date());
				uploadCommand.getStatus().addDetail(StatusDetail.CATEGORY_SUCCESS, "Created new Portal File for " + fileCategoryName + " and entity " + sourceEntity.getEntityLabelWithSourceSection());
			}
			sourceEntityKeysUsed.add(sourceEntityKey);

			// Remove any entity with a NULL id since it wasn't inserted in the original list (no need to add details since it's already in the status as an error or skip)
			List<PortalEntity> entityList = BeanUtils.filterNotNull(filePostingMapEntry.getValue(), PortalEntity::getId);
			updatePortalFileManualAssignments(portalFile, uploadCommand.getSourceSystem(), sourceSectionSetAffected, entityList);
		}

		// See if any keys are no longer used and remove their file assignments
		for (Map.Entry<String, PortalFile> sourceEntityPortalFileMapEntry : sourceEntityNamePortalFileMap.entrySet()) {
			if (!sourceEntityKeysUsed.contains(sourceEntityPortalFileMapEntry.getKey())) {
				updatePortalFileManualAssignments(sourceEntityPortalFileMapEntry.getValue(), uploadCommand.getSourceSystem(), sourceSectionSetAffected, null);
			}
		}
		uploadCommand.getStatus().addDetail(StatusDetail.CATEGORY_SUCCESS, "Successfully inserted/updated/deleted " + (sourceEntityNamePortalFileMap.size() - errorCount) + " " + fileCategoryName + " postings.");
	}


	/**
	 * The user may be a Portal Admin, not necessarily an admin.  These are manual file assignments from the source system, so we need to run as system user
	 */
	private void updatePortalFileManualAssignments(PortalFile portalFile, PortalEntitySourceSystem sourceSystem, Set<PortalEntitySourceSection> sourceSectionSet, List<PortalEntity> assignedPortalEntityList) {
		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> getPortalFileService().updatePortalFileManualAssignments(portalFile, sourceSystem, sourceSectionSet, assignedPortalEntityList));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status uploadPortalEntityRollupUploadFile(PortalEntityRollupUploadCommand uploadCommand) {
		ValidationUtils.assertNotNull(uploadCommand.getSourceSystem(), "A source system is required to perform an upload");
		if (uploadCommand.getSourceSystem().isAdministratorOnly()) {
			PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
			ValidationUtils.assertNotNull(currentUser, "Current User is Missing");
			ValidationUtils.assertTrue(currentUser.getUserRole().isAdministrator(), "Only Administrators can upload portal entities for source system " + uploadCommand.getSourceSystem().getName());
		}

		int parentCount = 0;
		int rollupCount = 0;

		// Put everything in a try/catch block to ensure we return the status object with the error
		try {
			// Convert File to List of Beans
			List<PortalEntityRollupUploadTemplate> entityUploadList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
			PortalEntityRollupUploadConfig uploadConfig = new PortalEntityRollupUploadConfig(uploadCommand.getSourceSystem(), entityUploadList);

			// These are the actual rollups that will be saved
			Map<PortalEntity, List<PortalEntityRollup>> entityRollupListMap = new HashMap<>();

			for (PortalEntityRollupUploadTemplate uploadTemplate : CollectionUtils.getIterable(entityUploadList)) {
				try {
					PortalEntity parentEntity = uploadConfig.getPortalEntityForRollupUploadTemplate(uploadTemplate, true, getPortalEntitySetupService(), getPortalEntityService());
					PortalEntity childEntity = uploadConfig.getPortalEntityForRollupUploadTemplate(uploadTemplate, false, getPortalEntitySetupService(), getPortalEntityService());
					if (!entityRollupListMap.containsKey(parentEntity)) {
						entityRollupListMap.put(parentEntity, new ArrayList<>());
					}
					PortalEntityRollup rollup = new PortalEntityRollup();
					rollup.setReferenceOne(parentEntity);
					rollup.setReferenceTwo(childEntity);
					rollup.setStartDate(uploadTemplate.getStartDate());
					rollup.setEndDate(uploadTemplate.getEndDate());
					entityRollupListMap.get(parentEntity).add(rollup);
				}
				catch (Throwable e) {
					String message = "Error processing upload for rollup: " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), message, e);
					uploadCommand.getStatus().addError(message);
				}
			}


			// Now go through the entity rollup list map - merge with actual data from the database and save
			for (Map.Entry<PortalEntity, List<PortalEntityRollup>> entityRollupListMapEntry : entityRollupListMap.entrySet()) {
				try {
					List<PortalEntityRollup> existingList = getPortalEntityService().getPortalEntityRollupListByParent(entityRollupListMapEntry.getKey().getId());
					List<PortalEntityRollup> newList = entityRollupListMapEntry.getValue();
					List<PortalEntityRollup> saveList = new ArrayList<>();

					for (PortalEntityRollup newRollup : CollectionUtils.getIterable(newList)) {
						boolean found = false;
						for (PortalEntityRollup existingRollup : CollectionUtils.getIterable(existingList)) {
							if (newRollup.getReferenceTwo().equals(existingRollup.getReferenceTwo())) {
								// The only thing that would change would be the start/end dates
								existingRollup.setStartDate(newRollup.getStartDate());
								existingRollup.setEndDate(newRollup.getEndDate());
								saveList.add(existingRollup);
								existingList.remove(existingRollup);

								found = true;
								break;
							}
						}
						if (!found) {
							saveList.add(newRollup);
						}
					}

					// If we aren't deleting missing from the file, then add whatever is still left in existing list into the save list
					if (!uploadCommand.isDeleteChildEntitiesMissingFromFile()) {
						for (PortalEntityRollup existingRollup : CollectionUtils.getIterable(existingList)) {
							System.out.println("adding " + existingRollup.getLabel());
							saveList.add(existingRollup);
						}
					}

					getPortalEntityService().savePortalEntityRollupListForParent(entityRollupListMapEntry.getKey(), saveList);
					parentCount++;
					rollupCount += CollectionUtils.getSize(saveList);
				}
				catch (Throwable e) {
					String message = "Error saving rollups for parent entity " + entityRollupListMapEntry.getKey().getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), message, e);
					uploadCommand.getStatus().addError(message);
				}
			}
		}
		catch (Throwable e) {
			String message = "Error processing upload " + ExceptionUtils.getOriginalMessage(e);
			LogUtils.errorOrInfo(getClass(), message, e);
			uploadCommand.getStatus().addError(message);
		}


		uploadCommand.getStatus().setMessageWithErrors("Portal Entity Rollup Upload Completed. Parent Entity Count affected " + parentCount + ", rollups affected " + rollupCount, 20);
		return uploadCommand.getStatus();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}
}
