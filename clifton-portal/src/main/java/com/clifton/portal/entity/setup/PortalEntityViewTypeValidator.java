package com.clifton.portal.entity.setup;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalEntityViewTypeValidator extends SelfRegisteringDaoValidator<PortalEntityViewType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortalEntityViewType bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("Creating new portal entity view types is not allowed.");
		}
		if (config.isDelete()) {
			throw new ValidationException("Deleting portal entity view types is not allowed.");
		}
		// NAME cannot be modified
		PortalEntityViewType originalBean = getOriginalBean(bean);
		ValidationUtils.assertTrue(StringUtils.isEqual(originalBean.getName(), bean.getName()), "You cannot change the name of portal entity view types.");

		// Default View - cannot be modified
		ValidationUtils.assertEquals(originalBean.isDefaultView(), bean.isDefaultView(), "You cannot change the default portal entity view type.");
	}
}
