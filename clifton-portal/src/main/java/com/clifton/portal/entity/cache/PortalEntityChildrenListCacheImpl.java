package com.clifton.portal.entity.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.portal.entity.PortalEntity;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>PortalEntityChildrenListCache</code> caches list of child portal entity ids for a given entity.  The list includes grand children, great grand children, etc.
 * <p>
 * The cache is cleared (in entirety) if there is a parent change (insert with a parent, delete with a parent, or parent update).  Because this only happens currently by
 * the daily batch job to insert these entities, we clear the full cache instead of going through each entity in the list and clearing their hierarchy.
 *
 * @author Mary Anderson
 */
@Component
public class PortalEntityChildrenListCacheImpl extends SelfRegisteringSimpleDaoCache<PortalEntity, Integer, Integer[]> implements PortalEntityChildrenListCache {


	@Override
	public Integer[] getPortalEntityChildrenIdList(int portalEntityId, ReadOnlyDAO<PortalEntity> portalEntityDAO) {
		Integer[] result = getCacheHandler().get(getCacheName(), portalEntityId);
		if (result == null) {
			Set<Integer> childrenIdList = new HashSet<>();
			addAllPortalEntityChildren(portalEntityId, childrenIdList, portalEntityDAO);
			if (CollectionUtils.isEmpty(childrenIdList)) {
				result = new Integer[0];
			}
			else {
				result = childrenIdList.toArray(new Integer[childrenIdList.size()]);
			}
			getCacheHandler().put(getCacheName(), portalEntityId, result);
		}
		return result;
	}


	protected void addAllPortalEntityChildren(Integer portalEntityId, Set<Integer> childrenIdList, ReadOnlyDAO<PortalEntity> portalEntityDAO) {
		List<PortalEntity> childrenList = portalEntityDAO.findByFields(new String[]{"parentPortalEntity.id", "deleted"}, new Object[]{portalEntityId, false});
		for (PortalEntity childEntity : CollectionUtils.getIterable(childrenList)) {
			childrenIdList.add(childEntity.getId());
			addAllPortalEntityChildren(childEntity.getId(), childrenIdList, portalEntityDAO);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                   Observer Methods                    //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<PortalEntity> dao, DaoEventTypes event, PortalEntity bean, Throwable e) {
		if (e == null) {
			boolean clear = false;
			if (event.isUpdate()) {
				PortalEntity originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CompareUtils.isEqual(originalBean.getParentPortalEntity(), bean.getParentPortalEntity())) {
					clear = true;
				}
			}
			else if (bean.getParentPortalEntity() != null) {
				clear = true;
			}
			if (clear) {
				getCacheHandler().clear(getCacheName());
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<PortalEntity> dao, DaoEventTypes event, PortalEntity bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}
}
