package com.clifton.portal.entity.setup.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.search.BasePortalEntitySpecificAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PortalEntityViewTypeSearchFormConfigurer extends BasePortalEntitySpecificAwareSearchFormConfigurer {


	public PortalEntityViewTypeSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);
	}


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId) {
		// View Types the User has an active assignment for
		DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignment.class, "a");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eq("securityUser.id", portalSecurityUserId));
		sub.add(Restrictions.or(Restrictions.isNull("disabledDate"), Restrictions.ge("disabledDate", DateUtils.clearTime(new Date()))));
		sub.createAlias("portalEntity", "ae");
		sub.add(Restrictions.eqProperty("ae.entityViewType.id", criteria.getAlias() + ".id"));
		criteria.add(Subqueries.exists(sub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		if (!CollectionUtils.isEmpty(viewAsEntityList)) {
			Short[] entityViewTypes = PortalUtils.getPortalEntityViewTypeIds(viewAsEntityList);
			if (!ArrayUtils.isEmpty(entityViewTypes)) {
				criteria.add(Restrictions.in("id", entityViewTypes));
			}
		}
	}
}
