package com.clifton.portal.entity.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.portal.entity.PortalEntity;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalEntityBySourceSectionCache</code> class caches PortalEntities based on source section and source FK Field Id
 *
 * @author manderson
 */
@Component
public class PortalEntityBySourceSectionCache extends SelfRegisteringCompositeKeyDaoCache<PortalEntity, Integer, Integer> {

	@Override
	protected String getBeanKey1Property() {
		return "entitySourceSection.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "sourceFkFieldId";
	}


	@Override
	protected Integer getBeanKey1Value(PortalEntity bean) {
		if (bean.getEntitySourceSection() != null) {
			return bean.getEntitySourceSection().getId();
		}
		return null;
	}


	@Override
	protected Integer getBeanKey2Value(PortalEntity bean) {
		return bean.getSourceFkFieldId();
	}
}
