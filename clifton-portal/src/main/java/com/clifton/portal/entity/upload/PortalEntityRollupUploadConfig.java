package com.clifton.portal.entity.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityRollupUploadConfig</code> is used to store information during the upload processing
 *
 * @author manderson
 */
@NonPersistentObject
public class PortalEntityRollupUploadConfig extends BasePortalEntityUploadConfig {

	/**
	 * These are the upload beans that are converted from the file and associated with the parent
	 * Key = Source System Name_Source Section Name_Source FKField ID
	 */
	private Map<String, List<PortalEntityRollupUploadTemplate>> entityRollupUploadTemplateListMap;

	/**
	 * These are the actual portal entities that represent each bean referenced in the upload file (parents and children) for easy retrieval
	 * Beans themselves are not updated, just referenced as parent or child in the rollup table.
	 */
	private Map<String, PortalEntity> entityMap;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityRollupUploadConfig(PortalEntitySourceSystem sourceSystem, List<PortalEntityRollupUploadTemplate> entityRollupUploadTemplateList) {
		super(sourceSystem);

		this.entityRollupUploadTemplateListMap = BeanUtils.getBeansMap(entityRollupUploadTemplateList, portalEntityRollupUploadTemplate -> StringUtils.coalesce(true, portalEntityRollupUploadTemplate.getParentPortalEntitySourceSystemName(), sourceSystem.getName()) + "_" + portalEntityRollupUploadTemplate.getParentPortalEntitySourceSectionName() + "_" + portalEntityRollupUploadTemplate.getParentSourceFKFieldId());
		this.entityMap = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, PortalEntity> getPortalEntityMap() {
		return getEntityMap();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntity getPortalEntityForRollupUploadTemplate(PortalEntityRollupUploadTemplate rollupUploadTemplate, boolean parent, PortalEntitySetupService portalEntitySetupService, PortalEntityService portalEntityService) {
		PortalEntity portalEntity = null;
		if (parent) {
			portalEntity = getPortalEntityImpl(rollupUploadTemplate.getParentPortalEntitySourceSystemName(), rollupUploadTemplate.getParentPortalEntitySourceSectionName(), rollupUploadTemplate.getParentSourceFKFieldId(), null, true, portalEntitySetupService, portalEntityService);
		}
		else {
			portalEntity = getPortalEntityImpl(rollupUploadTemplate.getChildPortalEntitySourceSystemName(), rollupUploadTemplate.getChildPortalEntitySourceSectionName(), rollupUploadTemplate.getChildSourceFKFieldId(), null, true, portalEntitySetupService, portalEntityService);
		}
		if (portalEntity == null) {
			throw new ValidationException("Cannot find " + (parent ? "parent" : "child") + " entity in the system for " + (parent ? rollupUploadTemplate.getParentLabel() : rollupUploadTemplate.getChildLabel()));
		}
		return portalEntity;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<String, List<PortalEntityRollupUploadTemplate>> getEntityRollupUploadTemplateListMap() {
		return this.entityRollupUploadTemplateListMap;
	}


	public void setEntityRollupUploadTemplateListMap(Map<String, List<PortalEntityRollupUploadTemplate>> entityRollupUploadTemplateListMap) {
		this.entityRollupUploadTemplateListMap = entityRollupUploadTemplateListMap;
	}


	public Map<String, PortalEntity> getEntityMap() {
		return this.entityMap;
	}


	public void setEntityMap(Map<String, PortalEntity> entityMap) {
		this.entityMap = entityMap;
	}
}
