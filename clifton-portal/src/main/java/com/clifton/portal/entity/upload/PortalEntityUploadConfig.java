package com.clifton.portal.entity.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityUploadConfig</code> is used to store information during the upload processing
 *
 * @author manderson
 */
@NonPersistentObject
public class PortalEntityUploadConfig extends BasePortalEntityUploadConfig {


	/**
	 * These are the upload beans that are converted from the file
	 * Key = Source System Name_Source Section Name_Source FKField ID
	 */
	private Map<String, PortalEntityUploadTemplate> entityUploadTemplateMap;

	/**
	 * These are the actual portal entities that represent each bean in the entity upload map
	 * that will be inserted / updated
	 * Uses same keys as entityUploadTemplateMap
	 */
	private Map<String, PortalEntity> entityUploadMap;


	private Map<Short, List<PortalEntityFieldType>> entityTypeFieldTypeListMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityUploadConfig(PortalEntitySourceSystem sourceSystem, List<PortalEntityUploadTemplate> entityUploadTemplateList) {
		super(sourceSystem);

		this.entityUploadTemplateMap = BeanUtils.getBeanMap(entityUploadTemplateList, portalEntityUploadTemplate -> StringUtils.coalesce(true, portalEntityUploadTemplate.getPortalEntitySourceSystemName(), sourceSystem.getName()) + "_" + portalEntityUploadTemplate.getPortalEntitySourceSectionName() + "_" + portalEntityUploadTemplate.getSourceFKFieldId());
		this.entityUploadMap = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, PortalEntity> getPortalEntityMap() {
		return this.entityUploadMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntity getPortalEntityForUploadTemplate(PortalEntityUploadTemplate uploadTemplate, PortalEntitySetupService portalEntitySetupService, PortalEntityService portalEntityService) {
		return getPortalEntityImpl(uploadTemplate.getPortalEntitySourceSystemName(), uploadTemplate.getPortalEntitySourceSectionName(), uploadTemplate.getSourceFKFieldId(), uploadTemplate.getEntityName(), false, portalEntitySetupService, portalEntityService);
	}


	public PortalEntity getPortalEntityForParentUploadTemplate(PortalEntityUploadTemplate uploadTemplate, PortalEntitySetupService portalEntitySetupService, PortalEntityService portalEntityService) {
		if (uploadTemplate.isParentPopulated()) {
			ValidationUtils.assertNotNull(uploadTemplate.getParentPortalEntitySourceSectionName(), uploadTemplate.getLabel() + ": If a parent is defined, it's source section name and source FK Field ID must also be defined");
			ValidationUtils.assertNotNull(uploadTemplate.getParentSourceFKFieldId(), uploadTemplate.getLabel() + ": If a parent is defined, it's source section name and source FK Field ID must also be defined");
			PortalEntity parentEntity = getPortalEntityImpl(uploadTemplate.getParentPortalEntitySourceSystemName(), uploadTemplate.getParentPortalEntitySourceSectionName(), uploadTemplate.getParentSourceFKFieldId(), null, true, portalEntitySetupService, portalEntityService);
			// Note: IF parent entity is null will add it to the error list
			return parentEntity;
		}
		return null;
	}


	public List<PortalEntityFieldType> getPortalEntityFieldTypeListByPortalEntityType(Short entityTypeId, PortalEntitySetupService portalEntitySetupService) {
		return CollectionUtils.getValue(this.entityTypeFieldTypeListMap, entityTypeId, () ->
				portalEntitySetupService.getPortalEntityFieldTypeListByEntityType(entityTypeId), ArrayList::new);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<String, PortalEntityUploadTemplate> getEntityUploadTemplateMap() {
		return this.entityUploadTemplateMap;
	}


	public Map<String, PortalEntity> getEntityUploadMap() {
		return this.entityUploadMap;
	}
}
