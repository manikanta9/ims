package com.clifton.portal.entity.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalEntitySourceSectionBySystemAndNameCache</code> caches a {@link PortalEntitySourceSection} by it's unique properties of
 * source system id and source section name
 *
 * @author manderson
 */
@Component
public class PortalEntitySourceSectionBySystemAndNameCache extends SelfRegisteringCompositeKeyDaoCache<PortalEntitySourceSection, Short, String> {

	@Override
	protected String getBeanKey1Property() {
		return "sourceSystem.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "name";
	}


	@Override
	protected Short getBeanKey1Value(PortalEntitySourceSection bean) {
		if (bean.getSourceSystem() != null) {
			return bean.getSourceSystem().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(PortalEntitySourceSection bean) {
		return bean.getName();
	}
}
