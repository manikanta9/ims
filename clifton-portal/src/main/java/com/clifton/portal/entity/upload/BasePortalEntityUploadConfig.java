package com.clifton.portal.entity.upload;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;

import java.util.HashMap;
import java.util.Map;


/**
 * The  <code>BasePortalEntityUploadConfig</code> class is a base class that contains common information/helper methods for processing portal entity and related (portal entity rollups) objects
 *
 * @author manderson
 */
public abstract class BasePortalEntityUploadConfig {

	/**
	 * This is the source system the file should be inserting/updating entities for
	 */
	private final PortalEntitySourceSystem portalEntitySourceSystem;


	/**
	 * Map of source system name_section name to the object in the database
	 */
	private Map<String, PortalEntitySourceSection> sourceSectionMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BasePortalEntityUploadConfig(PortalEntitySourceSystem sourceSystem) {
		super();
		this.portalEntitySourceSystem = sourceSystem;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract Map<String, PortalEntity> getPortalEntityMap();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected PortalEntity getPortalEntityImpl(String sourceSystemName, String sourceSectionName, Integer sourceFkFieldId, String entityName, boolean allowRetrievalFromMap, PortalEntitySetupService portalEntitySetupService, PortalEntityService portalEntityService) {
		if (allowRetrievalFromMap) {
			String key = getCoalescePortalEntitySourceSystemName(sourceSystemName) + "_" + sourceSectionName + "_" + sourceFkFieldId;
			if (getPortalEntityMap().containsKey(key)) {
				return getPortalEntityMap().get(key);
			}
		}
		PortalEntitySourceSection portalEntitySourceSection = getPortalEntitySourceSection(sourceSystemName, sourceSectionName, portalEntitySetupService);
		PortalEntity portalEntity = portalEntityService.getPortalEntityBySourceSection(portalEntitySourceSection.getId(), sourceFkFieldId, false);
		if (portalEntity == null && !StringUtils.isEmpty(entityName)) {
			// Try to Search By Name where source system is a higher priority before inserting a new
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setNameEquals(entityName);
			searchForm.setEntityTypeId(portalEntitySourceSection.getEntityType().getId());
			searchForm.addSearchRestriction(new SearchRestriction("sourceSystemPriorityOrder", ComparisonConditions.LESS_THAN, portalEntitySourceSection.getSourceSystem().getPriorityOrder()));
			searchForm.setOrderBy("sourceSystemPriorityOrder");
			portalEntity = CollectionUtils.getFirstElement(portalEntityService.getPortalEntityList(searchForm));
		}
		return portalEntity;
	}


	public void setPortalEntityInMap(String key, PortalEntity portalEntity) {
		getPortalEntityMap().put(key, portalEntity);
	}


	public PortalEntitySourceSection getPortalEntitySourceSection(String sourceSystemName, String sourceSectionName, PortalEntitySetupService portalEntitySetupService) {
		PortalEntitySourceSection sourceSection = CollectionUtils.getValue(this.sourceSectionMap, getCoalescePortalEntitySourceSystemName(sourceSystemName) + "_" + sourceSectionName, () -> portalEntitySetupService.getPortalEntitySourceSectionByName(getCoalescePortalEntitySourceSystemName(sourceSystemName), sourceSectionName));
		if (sourceSection == null) {
			throw new ValidationException("Cannot find source section with name " + sourceSectionName + " and source system " + sourceSystemName);
		}
		return sourceSection;
	}


	private String getCoalescePortalEntitySourceSystemName(String sourceSystemName) {
		return StringUtils.coalesce(true, sourceSystemName, this.portalEntitySourceSystem.getName());
	}
}
