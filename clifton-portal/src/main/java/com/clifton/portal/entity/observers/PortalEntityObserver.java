package com.clifton.portal.entity.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMappingService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>PortalEntityObserver</code> will apply end dates from children up to their parents.  End date is only set when it's set on all children.
 * <p>
 * WARNING: Currently the only parent/child relationships in the portal need this feature so it's applied for all.  If we have any cases where we don't want this
 * we can add a flag to the source section if that push up on the end date should be done.
 *
 * @author manderson
 */
@Component
public class PortalEntityObserver extends SelfRegisteringDaoObserver<PortalEntity> {


	private PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService;

	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<PortalEntity> dao, DaoEventTypes event, PortalEntity bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<PortalEntity> dao, DaoEventTypes event, PortalEntity bean, Throwable e) {
		Set<Integer> rebuildSecurityForEntityIds = new HashSet<>();
		if (event.isUpdate()) {
			PortalEntity originalBean = getOriginalBean(dao, bean);
			AssertUtils.assertNotNull(originalBean, "Original bean is null for bean " + bean.getName());
			if (!CompareUtils.isEqual(originalBean.getParentPortalEntity(), bean.getParentPortalEntity())) {
				if (originalBean.getParentPortalEntity() != null) {
					applyPortalEntityEndDateFromChildren(originalBean.getParentPortalEntity(), dao, true);
				}
				addPortalEntityAndParentIdsToRebuildSet(originalBean.getParentPortalEntity(), rebuildSecurityForEntityIds);
				addPortalEntityAndParentIdsToRebuildSet(bean.getParentPortalEntity(), rebuildSecurityForEntityIds);
			}
			// If original had an end date and end date changed...
			if (originalBean.getEndDate() != null && !CompareUtils.isEqual(originalBean.getEndDate(), bean.getEndDate())) {
				// If update has null end date - check children can happen from uploads where the end date is only set on the lowest level
				if (bean.getEndDate() == null) {
					applyPortalEntityEndDateFromChildren(bean, dao, false);
				}

				// and original end date had already passed and new end date is in the future, need to rebuild entity file category mapping
				if (DateUtils.isDateBefore(originalBean.getEndDate(), new Date(), false) && DateUtils.isDateAfterOrEqual(new Date(), bean.getEndDate())) {
					// Use Schedule method so multiple updates will lump together in one rebuild
					getPortalFileCategoryPortalEntityMappingService().schedulePortalFileCategoryPortalEntityMappingListRebuild();
				}
			}
		}
		if (bean.getParentPortalEntity() != null) {
			applyPortalEntityEndDateFromChildren(bean.getParentPortalEntity(), dao, true);
			if (event.isInsert()) {
				addPortalEntityAndParentIdsToRebuildSet(bean.getParentPortalEntity(), rebuildSecurityForEntityIds);
			}
		}

		// Rebuild Security Expanded if any changes
		if (!CollectionUtils.isEmpty(rebuildSecurityForEntityIds)) {
			getEventHandler().raiseEvent(PortalSecurityUserAssignmentExpandedRebuildEvent.ofPortalEntityChanges(rebuildSecurityForEntityIds.toArray(new Integer[0])));
		}
	}


	/**
	 * Traverse up the parent list and add the entity ids to the rebuilds for security
	 * This is updated when the parent changes on an existing bean or a new bean is added that has a parent
	 */
	private void addPortalEntityAndParentIdsToRebuildSet(PortalEntity portalEntity, Set<Integer> rebuildPortalEntityIdList) {
		if (portalEntity != null && portalEntity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
			rebuildPortalEntityIdList.add(portalEntity.getId());
			if (portalEntity.getParentPortalEntity() != null) {
				addPortalEntityAndParentIdsToRebuildSet(portalEntity.getParentPortalEntity(), rebuildPortalEntityIdList);
			}
		}
	}


	/**
	 * Rolls up termination date and end date from children where applies.
	 *
	 * @param saveUpdate - if false will just update the bean (applies when updating current bean) vs. actually saving
	 */
	protected void applyPortalEntityEndDateFromChildren(PortalEntity bean, ReadOnlyDAO<PortalEntity> dao, boolean saveUpdate) {
		List<PortalEntity> childrenList = dao.findByFields(new String[]{"parentPortalEntity.id", "deleted"}, new Object[]{bean.getId(), false});
		boolean update = false;

		Date terminationDate = null;
		for (PortalEntity childEntity : CollectionUtils.getIterable(childrenList)) {
			if (childEntity.getTerminationDate() == null) {
				terminationDate = null;
				break;
			}
			else if (terminationDate == null) {
				terminationDate = childEntity.getTerminationDate();
			}
			else if (DateUtils.isDateAfter(childEntity.getTerminationDate(), terminationDate)) {
				terminationDate = childEntity.getTerminationDate();
			}
		}
		if (!DateUtils.isEqualWithoutTime(terminationDate, bean.getTerminationDate())) {
			bean.setTerminationDate(terminationDate);
			update = true;
		}


		Date endDate = null;
		for (PortalEntity childEntity : CollectionUtils.getIterable(childrenList)) {
			if (childEntity.getEndDate() == null) {
				endDate = null;
				break;
			}
			else if (endDate == null) {
				endDate = childEntity.getEndDate();
			}
			else if (DateUtils.isDateAfter(childEntity.getEndDate(), endDate)) {
				endDate = childEntity.getEndDate();
			}
		}
		if (!DateUtils.isEqualWithoutTime(endDate, bean.getEndDate())) {
			bean.setEndDate(endDate);
			update = true;
		}

		if (update) {
			((UpdatableDAO<PortalEntity>) dao).save(bean);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategoryPortalEntityMappingService getPortalFileCategoryPortalEntityMappingService() {
		return this.portalFileCategoryPortalEntityMappingService;
	}


	public void setPortalFileCategoryPortalEntityMappingService(PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService) {
		this.portalFileCategoryPortalEntityMappingService = portalFileCategoryPortalEntityMappingService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
