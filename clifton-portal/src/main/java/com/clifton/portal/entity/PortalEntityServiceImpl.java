package com.clifton.portal.entity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoThreeKeyCache;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.cache.PortalEntityChildrenListCache;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.search.PortalEntitySearchFormConfigurer;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortalEntityServiceImpl implements PortalEntityService {


	private AdvancedUpdatableDAO<PortalEntity, Criteria> portalEntityDAO;
	private AdvancedUpdatableDAO<PortalEntityFieldValue, Criteria> portalEntityFieldValueDAO;
	private AdvancedUpdatableDAO<PortalEntityRollup, Criteria> portalEntityRollupDAO;


	private DaoCompositeKeyCache<PortalEntity, Integer, Integer> portalEntityBySourceSectionCache;
	private DaoThreeKeyCache<PortalEntity, Short, String, Integer> portalEntityBySourceTableCache;
	private DaoSingleKeyListCache<PortalEntityFieldValue, Integer> portalEntityFieldValueListByEntityCache;
	private PortalEntityChildrenListCache portalEntityChildrenListCache;

	private PortalEntitySetupService portalEntitySetupService;
	private PortalFileSetupService portalFileSetupService;
	private PortalSecurityResourceService portalSecurityResourceService;
	private PortalSecurityUserService portalSecurityUserService;

	/**
	 * Set in build properties, when termination date is set on a securable entity, but end date isn't
	 * The end date is auto set to x days after termination date.  This allows clients time to still receive/review reports following termination
	 */
	private int defaultDaysForTerminationPeriod = 150;


	////////////////////////////////////////////////////////////////////////////////
	////////////                 Portal Entity Methods                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntity getPortalEntity(int id) {
		return getPortalEntityDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalEntity getPortalEntityBySourceSection(int portalEntitySourceSectionId, int sourceFkFieldId, boolean populateFieldValues) {
		PortalEntity bean = getPortalEntityBySourceSectionCache().getBeanForKeyValues(getPortalEntityDAO(), portalEntitySourceSectionId, sourceFkFieldId);
		if (bean != null && populateFieldValues) {
			bean.setFieldValueList(getPortalEntityFieldValueListByEntity(bean.getId()));
		}
		return bean;
	}


	@Override
	public PortalEntity getPortalEntityBySourceTable(String sourceSystemName, String sourceTableName, int sourceFkFieldId, boolean populateFieldValues) {
		PortalEntity bean = getPortalEntityBySourceTableCache().getBeanForKeyValues(getPortalEntityDAO(), getPortalEntitySetupService().getPortalEntitySourceSystemByName(sourceSystemName).getId(), sourceTableName, sourceFkFieldId);
		if (bean != null && populateFieldValues) {
			bean.setFieldValueList(getPortalEntityFieldValueListByEntity(bean.getId()));
		}
		return bean;
	}


	@Override
	public List<PortalEntity> getPortalEntityList(PortalEntitySearchForm searchForm) {
		//  If Include Deleted filter is NOT set, deleted items will be excluded from the results
		if (searchForm.getIncludeDeleted() == null) {
			searchForm.setDeleted(false);
		}
		if (searchForm.getSecurableEntityLevel() != null || searchForm.getSecurableEntityHasApprovedFilesPosted() != null) {
			searchForm.setSecurableEntity(true);
			// NOTE: Criteria is built in the search form configurer
		}
		return getPortalEntityDAO().findBySearchCriteria(new PortalEntitySearchFormConfigurer(searchForm, getPortalSecurityUserService(), this, getPortalEntitySetupService(), getPortalFileSetupService(), getPortalSecurityResourceService()));
	}


	@Override
	public List<PortalEntity> getRelatedPortalEntityList(int portalEntityId, boolean includeSelf, boolean includeParents, boolean includeChildren, boolean activeOnly) {
		List<PortalEntity> entityList = new ArrayList<>();
		PortalEntity entity = getPortalEntity(portalEntityId);
		if (includeSelf) {
			entityList.add(entity);
		}
		if (includeParents) {
			while (entity.getParentPortalEntity() != null) {
				entityList.add(entity.getParentPortalEntity());
				entity = entity.getParentPortalEntity();
			}
		}
		if (includeChildren) {
			Integer[] childrenIds = getPortalEntityChildrenListCache().getPortalEntityChildrenIdList(portalEntityId, getPortalEntityDAO());
			if (!ArrayUtils.isEmpty(childrenIds)) {
				entityList.addAll(getPortalEntityDAO().findByPrimaryKeys(childrenIds));
			}
		}
		if (activeOnly) {
			entityList = BeanUtils.filter(entityList, PortalEntity::isActive);
		}
		return entityList;
	}


	/**
	 * This does a smart save for each bean to only set updateDate and save if there really was a change, either on the bean itself or any of it's field values
	 * Note: Portal Entities cannot be edited via UI and this can only be called from places like IMS that push their updates.
	 * Because of cases like invoices where whomever posts the invoice needs to be able to also create the portal entity and update the status field the security is open to all internal users
	 * If we ever open up edits on screen with stricter access, then should create a separate savePortalEntityFromSourceSystem method that has more open security for cases like invoices
	 */
	@Override
	@Transactional
	public PortalEntity savePortalEntity(PortalEntity bean) {
		boolean save = true;
		boolean useFolderName = bean.getEntitySourceSection().getEntityType().isPostableEntity();

		boolean securableEntity = bean.getEntitySourceSection().getEntityType().isSecurableEntity();
		if (securableEntity) {
			ValidationUtils.assertNotNull(bean.getEntityViewType(), "Entity View Type is required for entity type " + bean.getEntitySourceSection().getEntityType().getName());
			// If termination date is set, but end date isn't - set it to default days from termination
			if (bean.getTerminationDate() != null && bean.getEndDate() == null) {
				bean.setEndDate(DateUtils.addDays(bean.getTerminationDate(), getDefaultDaysForTerminationPeriod()));
			}
			// Else if End Date is set, but Term Date isn't - set it to End Date
			// Shouldn't really be necessary except during transition period as external systems (Salesforce) starts sending use Term date
			if (bean.getTerminationDate() == null && bean.getEndDate() != null) {
				bean.setTerminationDate(bean.getEndDate());
			}
		}
		else {
			ValidationUtils.assertNull(bean.getEntityViewType(), "Entity View Type is not allowed for entity type " + bean.getEntitySourceSection().getEntityType().getName());
			// Termination Date NEVER applies
			ValidationUtils.assertNull(bean.getTerminationDate(), "Termination Date does not apply to entity type " + bean.getEntitySourceSection().getEntityType().getName());
		}

		// If an entity is marked for deletion - remove the parent reference to make clean up easier
		// Or if the Parent is Marked as Deleted, remove the parent reference
		if (bean.isDeleted() || (bean.getParentPortalEntity() != null && bean.getParentPortalEntity().isDeleted())) {
			bean.setParentPortalEntity(null);
		}

		List<PortalEntityFieldValue> originalList = null;
		if (!bean.isNewBean()) {
			PortalEntity originalBean = getPortalEntity(bean.getId());
			originalList = getPortalEntityFieldValueListByEntity(bean.getId());
			// Do Not Allow Changing Folder Name -  Just set it back
			if (!StringUtils.isEmpty(originalBean.getFolderName())) {
				bean.setFolderName(originalBean.getFolderName());
			}
			if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(originalBean, bean, false, "fieldValueList"))) {
				save = false;
			}
		}

		if (useFolderName) {
			String folderName = bean.getFolderName();
			if (StringUtils.isEmpty(folderName)) {
				folderName = bean.getName();
				if (folderName.length() > 50) {
					folderName = folderName.substring(0, 50);
				}
			}
			folderName = FileUtils.replaceInvalidCharacters(folderName, "");
			if (!StringUtils.isEqual(folderName, bean.getFolderName())) {
				save = true;
				bean.setFolderName(folderName);
			}
		}
		else {
			if (!StringUtils.isEmpty(bean.getFolderName())) {
				save = true;
				bean.setFolderName(null);
			}
		}

		boolean saveList = !isPortalEntityFieldValueListsEqual(bean.getFieldValueList(), originalList);
		if (save || saveList) {
			getPortalEntityDAO().save(bean);
			if (saveList) {
				getPortalEntityFieldValueDAO().saveList(bean.getFieldValueList(), originalList);
			}
		}
		return bean;
	}


	/**
	 * Used to determine if the entity is really being updated.  If there is any difference if the field value lists then it returns false which
	 * indicates a save is necessary
	 */
	private boolean isPortalEntityFieldValueListsEqual(List<PortalEntityFieldValue> fieldValueList, List<PortalEntityFieldValue> originalFieldValueList) {
		if (CollectionUtils.isEmpty(fieldValueList) && CollectionUtils.isEmpty(originalFieldValueList)) {
			return true;
		}
		if (CollectionUtils.getSize(fieldValueList) != CollectionUtils.getSize(originalFieldValueList)) {
			return false;
		}
		for (PortalEntityFieldValue fieldValue : CollectionUtils.getIterable(fieldValueList)) {
			if (fieldValue.isNewBean()) {
				return false;
			}
			boolean found = false;
			for (PortalEntityFieldValue originalValue : CollectionUtils.getIterable(originalFieldValueList)) {
				if (fieldValue.equals(originalValue)) {
					found = true;
					if (!CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(fieldValue, originalValue, false))) {
						return false;
					}
				}
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}


	@Override
	@Transactional
	public PortalEntity updatePortalEntityEndDate(int portalEntityId, Date endDate) {
		PortalEntity portalEntity = getPortalEntity(portalEntityId);
		ValidationUtils.assertNotNull(portalEntity, "Portal Entity with ID " + portalEntityId + " is missing from the system.");
		ValidationUtils.assertTrue(portalEntity.getEntitySourceSection().getEntityType().isSecurableEntity(), "Updating end date is only supported for securable entities.");
		ValidationUtils.assertNotNull(portalEntity.getTerminationDate(), "You can only edit the end date of an entity if termination date is set.");
		// DO WE NEED TO FORCE SETTING END DATE ON THE LOWEST LEVEL?
		portalEntity.setEndDate(endDate);
		return getPortalEntityDAO().save(portalEntity);
	}


	@Override
	@Transactional
	public void deletePortalEntity(int id) {
		// Delete where referenced in rollup table
		getPortalEntityRollupDAO().deleteList(getPortalEntityRollupListByParent(id));
		getPortalEntityRollupDAO().deleteList(getPortalEntityRollupListByChild(id));
		// Delete Field Values
		getPortalEntityFieldValueDAO().deleteList(getPortalEntityFieldValueListByEntity(id));
		// Delete the Entity
		getPortalEntityDAO().delete(id);
	}


	@Override
	public Status deletePortalEntitiesMarkedForDeletion(String sourceSystemName, Status status) {
		if (status == null) {
			status = Status.ofEmptyMessage();
		}

		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setIncludeDeleted(true);
		searchForm.setDeleted(true);
		searchForm.setSourceSystemNameEquals(sourceSystemName);

		List<PortalEntity> entityList = getPortalEntityList(searchForm);
		int successCount = 0;
		for (PortalEntity portalEntity : CollectionUtils.getIterable(entityList)) {
			try {
				deletePortalEntity(portalEntity.getId());
				successCount++;
			}
			catch (Exception e) {
				status.addError(portalEntity.getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e));
			}
		}
		status.setMessage("Successfully deleted [" + successCount + "] portal entities.");
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////             Portal Entity Field Value Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntityFieldValue getPortalEntityFieldValue(int id) {
		return getPortalEntityFieldValueDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalEntityFieldValue> getPortalEntityFieldValueListByEntity(int portalEntityId) {
		return getPortalEntityFieldValueListByEntityCache().getBeanListForKeyValue(getPortalEntityFieldValueDAO(), portalEntityId);
	}


	@Override
	public List<PortalEntityFieldValue> getPortalEntityFieldValueListByEntityTypeAndFieldName(String entityTypeName, String fieldName) {
		PortalEntityType entityType = getPortalEntitySetupService().getPortalEntityTypeByName(entityTypeName);
		PortalEntityFieldType fieldType = CollectionUtils.getFirstElementStrict(BeanUtils.filter(getPortalEntitySetupService().getPortalEntityFieldTypeListByEntityType(entityType.getId()), portalEntityFieldType -> StringUtils.isEqual(fieldName, portalEntityFieldType.getName())));
		return getPortalEntityFieldValueDAO().findByField("portalEntityFieldType.id", fieldType.getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////              Portal Entity Rollup Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalEntityRollup> getPortalEntityRollupListByParent(int portalEntityId) {
		return getPortalEntityRollupDAO().findByField("referenceOne.id", portalEntityId);
	}


	@Override
	public List<PortalEntityRollup> getPortalEntityRollupListByChild(int portalEntityId) {
		return getPortalEntityRollupDAO().findByField("referenceTwo.id", portalEntityId);
	}


	@Override
	@Transactional
	public PortalEntity savePortalEntityRollupListForParent(PortalEntity parentPortalEntity, List<PortalEntityRollup> rollupList) {
		if (!CollectionUtils.isEmpty(rollupList)) {
			// Same entity can be a child more than once as long as there isn't an overlapping date range
			// For example Performance Composites can have accounts come on and off over time.
			Map<PortalEntity, List<PortalEntityRollup>> childEntityRollupMap = new HashMap<>();
			PortalEntityType childType = parentPortalEntity.getEntitySourceSection().getChildEntityType();
			if (childType == null) {
				throw new ValidationException("Portal Entity Source Section [" + parentPortalEntity.getEntitySourceSection().getName() + "] does not support children.");
			}
			for (PortalEntityRollup rollup : rollupList) {
				if (!childType.equals(rollup.getReferenceTwo().getEntitySourceSection().getEntityType())) {
					throw new ValidationException("Child Portal Entity [" + rollup.getReferenceTwo().getEntityLabelWithSourceSection() + "] is not a valid child.  Only [" + childType.getName() + "] entity types are allowed.");
				}
				rollup.setReferenceOne(parentPortalEntity);

				if (childEntityRollupMap.containsKey(rollup.getReferenceTwo())) {
					for (PortalEntityRollup childRollup : CollectionUtils.getIterable(childEntityRollupMap.get(rollup.getReferenceTwo()))) {
						if (DateUtils.isOverlapInDates(rollup.getStartDate(), rollup.getEndDate(), childRollup.getStartDate(), childRollup.getEndDate())) {
							throw new ValidationException("Parent Portal Entity " + parentPortalEntity.getLabel() + " has child portal entity " + rollup.getReferenceTwo().getLabel() + " multiple times with overlapping date ranges " + DateUtils.fromDateRange(rollup.getStartDate(), rollup.getEndDate(), true) + " and " + DateUtils.fromDateRange(childRollup.getStartDate(), childRollup.getEndDate(), true));
						}
					}
					childEntityRollupMap.get(rollup.getReferenceTwo()).add(rollup);
				}
				else {
					childEntityRollupMap.put(rollup.getReferenceTwo(), CollectionUtils.createList(rollup));
				}
			}
		}
		getPortalEntityRollupDAO().saveList(rollupList, getPortalEntityRollupListByParent(parentPortalEntity.getId()));
		return parentPortalEntity;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalEntity, Criteria> getPortalEntityDAO() {
		return this.portalEntityDAO;
	}


	public void setPortalEntityDAO(AdvancedUpdatableDAO<PortalEntity, Criteria> portalEntityDAO) {
		this.portalEntityDAO = portalEntityDAO;
	}


	public AdvancedUpdatableDAO<PortalEntityFieldValue, Criteria> getPortalEntityFieldValueDAO() {
		return this.portalEntityFieldValueDAO;
	}


	public void setPortalEntityFieldValueDAO(AdvancedUpdatableDAO<PortalEntityFieldValue, Criteria> portalEntityFieldValueDAO) {
		this.portalEntityFieldValueDAO = portalEntityFieldValueDAO;
	}


	public AdvancedUpdatableDAO<PortalEntityRollup, Criteria> getPortalEntityRollupDAO() {
		return this.portalEntityRollupDAO;
	}


	public void setPortalEntityRollupDAO(AdvancedUpdatableDAO<PortalEntityRollup, Criteria> portalEntityRollupDAO) {
		this.portalEntityRollupDAO = portalEntityRollupDAO;
	}


	public DaoCompositeKeyCache<PortalEntity, Integer, Integer> getPortalEntityBySourceSectionCache() {
		return this.portalEntityBySourceSectionCache;
	}


	public void setPortalEntityBySourceSectionCache(DaoCompositeKeyCache<PortalEntity, Integer, Integer> portalEntityBySourceSectionCache) {
		this.portalEntityBySourceSectionCache = portalEntityBySourceSectionCache;
	}


	public DaoThreeKeyCache<PortalEntity, Short, String, Integer> getPortalEntityBySourceTableCache() {
		return this.portalEntityBySourceTableCache;
	}


	public void setPortalEntityBySourceTableCache(DaoThreeKeyCache<PortalEntity, Short, String, Integer> portalEntityBySourceTableCache) {
		this.portalEntityBySourceTableCache = portalEntityBySourceTableCache;
	}


	public DaoSingleKeyListCache<PortalEntityFieldValue, Integer> getPortalEntityFieldValueListByEntityCache() {
		return this.portalEntityFieldValueListByEntityCache;
	}


	public void setPortalEntityFieldValueListByEntityCache(DaoSingleKeyListCache<PortalEntityFieldValue, Integer> portalEntityFieldValueListByEntityCache) {
		this.portalEntityFieldValueListByEntityCache = portalEntityFieldValueListByEntityCache;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalEntityChildrenListCache getPortalEntityChildrenListCache() {
		return this.portalEntityChildrenListCache;
	}


	public void setPortalEntityChildrenListCache(PortalEntityChildrenListCache portalEntityChildrenListCache) {
		this.portalEntityChildrenListCache = portalEntityChildrenListCache;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalSecurityResourceService getPortalSecurityResourceService() {
		return this.portalSecurityResourceService;
	}


	public void setPortalSecurityResourceService(PortalSecurityResourceService portalSecurityResourceService) {
		this.portalSecurityResourceService = portalSecurityResourceService;
	}


	public int getDefaultDaysForTerminationPeriod() {
		return this.defaultDaysForTerminationPeriod;
	}


	public void setDefaultDaysForTerminationPeriod(int defaultDaysForTerminationPeriod) {
		this.defaultDaysForTerminationPeriod = defaultDaysForTerminationPeriod;
	}
}
