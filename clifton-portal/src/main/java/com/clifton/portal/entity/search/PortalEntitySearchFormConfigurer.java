package com.clifton.portal.entity.search;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMapping;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.search.BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PortalEntitySearchFormConfigurer extends BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer {

	private final PortalEntitySetupService portalEntitySetupService;

	private final PortalFileSetupService portalFileSetupService;

	private final PortalSecurityResourceService portalSecurityResourceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService, PortalEntitySetupService portalEntitySetupService, PortalFileSetupService portalFileSetupService, PortalSecurityResourceService portalSecurityResourceService) {
		super(searchForm, portalSecurityUserService, portalEntityService);
		this.portalEntitySetupService = portalEntitySetupService;
		this.portalFileSetupService = portalFileSetupService;
		this.portalSecurityResourceService = portalSecurityResourceService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PortalEntitySearchForm searchForm = (PortalEntitySearchForm) getPortalEntitySpecificAwareSearchForm();
		Boolean pendingTermination = (Boolean) SearchUtils.getRestrictionValue(searchForm, "pendingTermination");
		if (pendingTermination != null) {
			LogicalExpression pendingTerminationExpression = Restrictions.and(Restrictions.isNotNull("terminationDate"), Restrictions.ge("endDate", DateUtils.clearTime(new Date())));
			criteria.add(BooleanUtils.isTrue(pendingTermination) ? pendingTerminationExpression : Restrictions.not(pendingTerminationExpression));
		}
		if (searchForm.getSecurableEntityLevel() != null) {
			switch (searchForm.getSecurableEntityLevel()) {
				case 1:
					criteria.add(Restrictions.eq(getPathAlias("entitySourceSection", criteria) + ".entityType.id", getPortalEntityTypeId(PortalEntityType.ENTITY_TYPE_ORGANIZATION_GROUP)));
					break;
				case 2:
					criteria.add(Restrictions.or(Restrictions.eq(getPathAlias("entitySourceSection", criteria) + ".entityType.id", getPortalEntityTypeId(PortalEntityType.ENTITY_TYPE_CLIENT_RELATIONSHIP)),
							Restrictions.and(Restrictions.eq(getPathAlias("entitySourceSection", criteria) + ".entityType.id", getPortalEntityTypeId(PortalEntityType.ENTITY_TYPE_CLIENT)), Restrictions.isNull("parentPortalEntity"))));
					break;
				case 3:
					criteria.add(Restrictions.and(Restrictions.eq(getPathAlias("entitySourceSection", criteria) + ".entityType.id", getPortalEntityTypeId(PortalEntityType.ENTITY_TYPE_CLIENT)), Restrictions.isNotNull("parentPortalEntity")));
					break;
				case 4:
					criteria.add(Restrictions.eq(getPathAlias("entitySourceSection", criteria) + ".entityType.id", getPortalEntityTypeId(PortalEntityType.ENTITY_TYPE_CLIENT_ACCOUNT)));
					break;
				default:
					throw new ValidationException("Invalid Securable Entity Level selected.  Valid Options are 1 - 4");
			}
		}
		if (searchForm.getSecurableEntityHasApprovedFilesPosted() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(PortalFileCategoryPortalEntityMapping.class, "fcm");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("portalEntityId", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("approved", true));
			// Need to Exclude all categories under "Contact Us" as many accounts are assigned teams but don't actually have anything else posted
			sub.add(Restrictions.not(Restrictions.in("portalFileCategoryId", getPortalFileSetupService().getPortalFileCategoryIdsForContactUsCategories())));
			criteria.add(Subqueries.exists(sub));
		}
	}


	private Short getPortalEntityTypeId(String entityTypeName) {
		return getPortalEntitySetupService().getPortalEntityTypeByName(entityTypeName).getId();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId, Boolean viewSecurityAdminRole, Boolean hasSecurityResourceAccess) {
		DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "se");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eq("portalSecurityUserId", portalSecurityUserId));
		if (BooleanUtils.isTrue(viewSecurityAdminRole)) {
			sub.add(Restrictions.eq("portalSecurityAdministrator", true));
		}
		if (BooleanUtils.isTrue(hasSecurityResourceAccess)) {
			// Additional Condition - Exclude ONLY Contact security - so if that security pushes up - but user doesn't really have any other access to the entity,
			// then consider it no access
			sub.add(Restrictions.isNotNull("portalSecurityResourceId"));
			sub.add(Restrictions.ne("portalSecurityResourceId", getPortalSecurityResourceService().getPortalSecurityResourceByName(PortalSecurityResource.RESOURCE_CONTACTS).getId()));
		}
		sub.add(Restrictions.disjunction(
				Restrictions.eq(getPathAlias("entitySourceSection.entityType", criteria) + ".securableEntity", false),
				Restrictions.eqProperty("portalEntityId", criteria.getAlias() + ".id")
		));
		criteria.add(Subqueries.exists(sub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		// This would only apply if called from client UI which means that only looking for securable entities
		criteria.add(Restrictions.in("id", BeanUtils.getBeanIdentityArray(viewAsEntityList, Integer.class)));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public PortalSecurityResourceService getPortalSecurityResourceService() {
		return this.portalSecurityResourceService;
	}
}
