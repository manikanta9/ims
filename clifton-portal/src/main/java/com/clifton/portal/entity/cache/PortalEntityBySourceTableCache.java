package com.clifton.portal.entity.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringThreeKeyDaoCache;
import com.clifton.portal.entity.PortalEntity;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalEntityBySourceTableCache</code> class caches PortalEntities based on key 1: Source System Name _ Source Table Name and key 2: source FK Field Id
 *
 * @author manderson
 */
@Component
public class PortalEntityBySourceTableCache extends SelfRegisteringThreeKeyDaoCache<PortalEntity, Short, String, Integer> {

	@Override
	protected String getBeanKey1Property() {
		return "entitySourceSection.sourceSystem.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "entitySourceSection.sourceSystemTableName";
	}


	@Override
	protected String getBeanKey3Property() {
		return "sourceFkFieldId";
	}


	@Override
	protected Short getBeanKey1Value(PortalEntity bean) {
		if (bean.getEntitySourceSection() != null && bean.getEntitySourceSection().getSourceSystem() != null) {
			return bean.getEntitySourceSection().getSourceSystem().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(PortalEntity bean) {
		if (bean.getEntitySourceSection() != null) {
			return bean.getEntitySourceSection().getSourceSystemTableName();
		}
		return null;
	}


	@Override
	protected Integer getBeanKey3Value(PortalEntity bean) {
		return bean.getSourceFkFieldId();
	}
}
