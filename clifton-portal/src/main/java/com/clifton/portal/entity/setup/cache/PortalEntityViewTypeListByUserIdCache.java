package com.clifton.portal.entity.setup.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCustomKeyDaoListCache;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


/**
 * The {@link PortalEntityViewTypeListByUserIdCache} type caches {@link PortalEntityViewType} entities by user ID. This is used to rapidly retrieve the list of view type IDs which
 * apply to any given user.
 *
 * @author mikeh
 */
@Component
public class PortalEntityViewTypeListByUserIdCache extends SelfRegisteringCustomKeyDaoListCache<Short, PortalEntityViewType> {

	@Override
	protected Class<PortalEntityViewType> getDtoClass() {
		return PortalEntityViewType.class;
	}


	@Override
	public List<Class<? extends IdentityObject>> getAdditionalDtoTypeList() {
		// Clear cache on assignment change and view type change (add/delete)
		return Arrays.asList(PortalSecurityUserAssignment.class, PortalEntityViewType.class);
	}
}
