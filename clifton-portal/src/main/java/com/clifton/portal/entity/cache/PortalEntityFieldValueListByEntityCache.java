package com.clifton.portal.entity.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.entity.PortalEntityFieldValue;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalEntityFieldValueListByEntityCache</code> caches the list of field values for a specified entity
 *
 * @author manderson
 */
@Component
public class PortalEntityFieldValueListByEntityCache extends SelfRegisteringSingleKeyDaoListCache<PortalEntityFieldValue, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "portalEntity.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortalEntityFieldValue bean) {
		if (bean.getPortalEntity() != null) {
			return bean.getPortalEntity().getId();
		}
		return null;
	}
}
