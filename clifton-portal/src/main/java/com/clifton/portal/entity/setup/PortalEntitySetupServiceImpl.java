package com.clifton.portal.entity.setup;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoCustomKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.search.PortalEntitySourceSectionSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityTypeSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalEntitySetupServiceImpl implements PortalEntitySetupService {

	public static final String CACHE_DEFAULT_VIEW = "PORTAL_ENTITY_VIEW_TYPE_DEFAULT";

	private AdvancedUpdatableDAO<PortalEntityViewType, Criteria> portalEntityViewTypeDAO;

	private UpdatableDAO<PortalEntitySourceSystem> portalEntitySourceSystemDAO;
	private AdvancedUpdatableDAO<PortalEntitySourceSection, Criteria> portalEntitySourceSectionDAO;

	private AdvancedUpdatableDAO<PortalEntityType, Criteria> portalEntityTypeDAO;
	private AdvancedUpdatableDAO<PortalEntityFieldType, Criteria> portalEntityFieldTypeDAO;

	private DaoNamedEntityCache<PortalEntityViewType> portalEntityViewTypeCache;
	private DaoNamedEntityCache<PortalEntityType> portalEntityTypeCache;
	private DaoNamedEntityCache<PortalEntitySourceSystem> portalEntitySourceSystemCache;
	private DaoCompositeKeyCache<PortalEntitySourceSection, Short, String> portalEntitySourceSectionBySystemAndNameCache;
	private DaoCustomKeyListCache<Short, PortalEntityViewType> portalEntityViewTypeListByUserIdCache;

	private CacheHandler<Object, PortalEntityViewType> cacheHandler;

	// Circular Dependencies...
	private PortalSecurityUserService portalSecurityUserService;
	private PortalEntityService portalEntityService;


	////////////////////////////////////////////////////////////////////////////////
	//////////              Portal Entity View Type Methods               //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntityViewType getPortalEntityViewType(short id) {
		return getPortalEntityViewTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalEntityViewType getPortalEntityViewTypeDefault() {
		PortalEntityViewType result = getPortalEntityViewTypeDefaultFromCache();
		if (result == null) {
			result = getPortalEntityViewTypeDAO().findOneByField("defaultView", true);
			ValidationUtils.assertNotNull(result, "Cannot find default view type. One view type must be marked as default.");
			setPortalEntityViewTypeDefaultInCache(result);
		}
		return result;
	}


	@Override
	public PortalEntityViewType getPortalEntityViewTypeByName(String name) {
		return getPortalEntityViewTypeCache().getBeanForKeyValueStrict(getPortalEntityViewTypeDAO(), name);
	}


	@Override
	public List<PortalEntityViewType> getPortalEntityViewTypeList(PortalEntityViewTypeSearchForm searchForm, boolean returnDefaultIfNone) {
		// If no sorting - return default view first
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("defaultView:desc");
		}
		List<PortalEntityViewType> result = getPortalEntityViewTypeDAO().findBySearchCriteria(new PortalEntityViewTypeSearchFormConfigurer(searchForm, getPortalSecurityUserService(), getPortalEntityService()));
		if (CollectionUtils.isEmpty(result) && returnDefaultIfNone) {
			return new PagingArrayList<>(CollectionUtils.createList(getPortalEntityViewTypeDefault()), 0, 1);
		}
		return result;
	}


	@Override
	public List<PortalEntityViewType> getPortalEntityViewTypeListForUser(short userId) {
		return getPortalEntityViewTypeListByUserIdCache().computeIfAbsent(userId, id -> {
			PortalEntityViewTypeSearchForm searchForm = new PortalEntityViewTypeSearchForm();
			searchForm.setViewAsUserId(userId);
			return getPortalEntityViewTypeList(searchForm, true);
		});
	}


	@Override
	public PortalEntityViewType savePortalEntityViewType(PortalEntityViewType bean) {
		return getPortalEntityViewTypeDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            Portal Entity Source System Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntitySourceSystem getPortalEntitySourceSystemByName(String name) {
		return getPortalEntitySourceSystemCache().getBeanForKeyValueStrict(getPortalEntitySourceSystemDAO(), name);
	}


	@Override
	public List<PortalEntitySourceSystem> getPortalEntitySourceSystemList() {
		return getPortalEntitySourceSystemDAO().findAll();
	}


	@Override
	public PortalEntitySourceSystem savePortalEntitySourceSystem(PortalEntitySourceSystem bean) {
		return getPortalEntitySourceSystemDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////            Portal Entity Source Section Methods             //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntitySourceSection getPortalEntitySourceSection(int id) {
		return getPortalEntitySourceSectionDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalEntitySourceSection getPortalEntitySourceSectionByName(String sourceSystemName, String sourceSectionName) {
		PortalEntitySourceSystem sourceSystem = getPortalEntitySourceSystemByName(sourceSystemName);
		return getPortalEntitySourceSectionBySystemAndNameCache().getBeanForKeyValues(getPortalEntitySourceSectionDAO(), sourceSystem.getId(), sourceSectionName);
	}


	@Override
	public List<PortalEntitySourceSection> getPortalEntitySourceSectionList(PortalEntitySourceSectionSearchForm searchForm) {
		return getPortalEntitySourceSectionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortalEntitySourceSection savePortalEntitySourceSection(PortalEntitySourceSection bean) {
		return getPortalEntitySourceSectionDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal Entity Type Methods                  //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalEntityType getPortalEntityType(short id) {
		return getPortalEntityTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalEntityType getPortalEntityTypeByName(String name) {
		return getPortalEntityTypeCache().getBeanForKeyValueStrict(getPortalEntityTypeDAO(), name);
	}


	@Override
	public List<PortalEntityType> getPortalEntityTypeList(PortalEntityTypeSearchForm searchForm) {
		return getPortalEntityTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortalEntityType savePortalEntityType(PortalEntityType bean) {
		return getPortalEntityTypeDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////              Portal Entity Field Type Methods               //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalEntityFieldType> getPortalEntityFieldTypeListByEntityType(short portalEntityTypeId) {
		return getPortalEntityFieldTypeDAO().findByField("portalEntityType.id", portalEntityTypeId);
	}


	@Override
	public List<PortalEntityFieldType> getPortalEntityFieldTypeList() {
		return getPortalEntityFieldTypeDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                  Cache Helper Methods                   //////////
	////////////////////////////////////////////////////////////////////////////


	private PortalEntityViewType getPortalEntityViewTypeDefaultFromCache() {
		return getCacheHandler().get(CACHE_DEFAULT_VIEW, true);
	}


	private void setPortalEntityViewTypeDefaultInCache(PortalEntityViewType entityViewType) {
		getCacheHandler().put(CACHE_DEFAULT_VIEW, true, entityViewType);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalEntityViewType, Criteria> getPortalEntityViewTypeDAO() {
		return this.portalEntityViewTypeDAO;
	}


	public void setPortalEntityViewTypeDAO(AdvancedUpdatableDAO<PortalEntityViewType, Criteria> portalEntityViewTypeDAO) {
		this.portalEntityViewTypeDAO = portalEntityViewTypeDAO;
	}


	public UpdatableDAO<PortalEntitySourceSystem> getPortalEntitySourceSystemDAO() {
		return this.portalEntitySourceSystemDAO;
	}


	public void setPortalEntitySourceSystemDAO(UpdatableDAO<PortalEntitySourceSystem> portalEntitySourceSystemDAO) {
		this.portalEntitySourceSystemDAO = portalEntitySourceSystemDAO;
	}


	public AdvancedUpdatableDAO<PortalEntitySourceSection, Criteria> getPortalEntitySourceSectionDAO() {
		return this.portalEntitySourceSectionDAO;
	}


	public void setPortalEntitySourceSectionDAO(AdvancedUpdatableDAO<PortalEntitySourceSection, Criteria> portalEntitySourceSectionDAO) {
		this.portalEntitySourceSectionDAO = portalEntitySourceSectionDAO;
	}


	public AdvancedUpdatableDAO<PortalEntityType, Criteria> getPortalEntityTypeDAO() {
		return this.portalEntityTypeDAO;
	}


	public void setPortalEntityTypeDAO(AdvancedUpdatableDAO<PortalEntityType, Criteria> portalEntityTypeDAO) {
		this.portalEntityTypeDAO = portalEntityTypeDAO;
	}


	public AdvancedUpdatableDAO<PortalEntityFieldType, Criteria> getPortalEntityFieldTypeDAO() {
		return this.portalEntityFieldTypeDAO;
	}


	public void setPortalEntityFieldTypeDAO(AdvancedUpdatableDAO<PortalEntityFieldType, Criteria> portalEntityFieldTypeDAO) {
		this.portalEntityFieldTypeDAO = portalEntityFieldTypeDAO;
	}


	public DaoNamedEntityCache<PortalEntityViewType> getPortalEntityViewTypeCache() {
		return this.portalEntityViewTypeCache;
	}


	public void setPortalEntityViewTypeCache(DaoNamedEntityCache<PortalEntityViewType> portalEntityViewTypeCache) {
		this.portalEntityViewTypeCache = portalEntityViewTypeCache;
	}


	public DaoNamedEntityCache<PortalEntityType> getPortalEntityTypeCache() {
		return this.portalEntityTypeCache;
	}


	public void setPortalEntityTypeCache(DaoNamedEntityCache<PortalEntityType> portalEntityTypeCache) {
		this.portalEntityTypeCache = portalEntityTypeCache;
	}


	public DaoNamedEntityCache<PortalEntitySourceSystem> getPortalEntitySourceSystemCache() {
		return this.portalEntitySourceSystemCache;
	}


	public void setPortalEntitySourceSystemCache(DaoNamedEntityCache<PortalEntitySourceSystem> portalEntitySourceSystemCache) {
		this.portalEntitySourceSystemCache = portalEntitySourceSystemCache;
	}


	public DaoCompositeKeyCache<PortalEntitySourceSection, Short, String> getPortalEntitySourceSectionBySystemAndNameCache() {
		return this.portalEntitySourceSectionBySystemAndNameCache;
	}


	public void setPortalEntitySourceSectionBySystemAndNameCache(DaoCompositeKeyCache<PortalEntitySourceSection, Short, String> portalEntitySourceSectionBySystemAndNameCache) {
		this.portalEntitySourceSectionBySystemAndNameCache = portalEntitySourceSectionBySystemAndNameCache;
	}


	public DaoCustomKeyListCache<Short, PortalEntityViewType> getPortalEntityViewTypeListByUserIdCache() {
		return this.portalEntityViewTypeListByUserIdCache;
	}


	public void setPortalEntityViewTypeListByUserIdCache(DaoCustomKeyListCache<Short, PortalEntityViewType> portalEntityViewTypeListByUserIdCache) {
		this.portalEntityViewTypeListByUserIdCache = portalEntityViewTypeListByUserIdCache;
	}


	public CacheHandler<Object, PortalEntityViewType> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, PortalEntityViewType> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}
}
