package com.clifton.portal.entity.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.portal.entity.PortalEntity;


/**
 * The <code>PortalEntityChildrenListCache</code> caches list of child portal entity ids for a given entity.  The list includes grand children, great grand children, etc.
 *
 * @author Mary Anderson
 */
public interface PortalEntityChildrenListCache {


	public Integer[] getPortalEntityChildrenIdList(int portalEntityId, ReadOnlyDAO<PortalEntity> portalEntityDAO);
}
