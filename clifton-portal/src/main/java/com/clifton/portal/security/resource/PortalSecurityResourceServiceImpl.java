package com.clifton.portal.security.resource;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalSecurityResourceServiceImpl implements PortalSecurityResourceService {

	private AdvancedUpdatableDAO<PortalSecurityResource, Criteria> portalSecurityResourceDAO;

	private DaoNamedEntityCache<PortalSecurityResource> portalSecurityResourceCache;

	private PortalEntityService portalEntityService;

	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	//////////            Portal Security Resource Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityResource getPortalSecurityResource(short id) {
		return getPortalSecurityResourceDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalSecurityResource getPortalSecurityResourceByName(String name) {
		return getPortalSecurityResourceCache().getBeanForKeyValue(getPortalSecurityResourceDAO(), name);
	}


	@Override
	public List<PortalSecurityResource> getPortalSecurityResourceList(PortalSecurityResourceSearchForm searchForm) {
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("nameExpanded:ASC");
		}
		return getPortalSecurityResourceDAO().findBySearchCriteria(new PortalSecurityResourceSearchFormConfigurer(searchForm, getPortalSecurityUserService(), getPortalEntityService()));
	}


	@Override
	public PortalSecurityResource savePortalSecurityResource(PortalSecurityResource bean) {
		return getPortalSecurityResourceDAO().save(bean);
	}


	@Override
	public void deletePortalSecurityResource(short id) {
		getPortalSecurityResourceDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalSecurityResource, Criteria> getPortalSecurityResourceDAO() {
		return this.portalSecurityResourceDAO;
	}


	public void setPortalSecurityResourceDAO(AdvancedUpdatableDAO<PortalSecurityResource, Criteria> portalSecurityResourceDAO) {
		this.portalSecurityResourceDAO = portalSecurityResourceDAO;
	}


	public DaoNamedEntityCache<PortalSecurityResource> getPortalSecurityResourceCache() {
		return this.portalSecurityResourceCache;
	}


	public void setPortalSecurityResourceCache(DaoNamedEntityCache<PortalSecurityResource> portalSecurityResourceCache) {
		this.portalSecurityResourceCache = portalSecurityResourceCache;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}
}
