package com.clifton.portal.security.user.expanded.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache</code> caches the list of PortalSecurityUserAssignmentExpanded rows
 * for a Portal Security Resource and Entity
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache extends SelfRegisteringCompositeKeyDaoListCache<PortalSecurityUserAssignmentExpanded, Short, Integer> {


	@Override
	protected String getBeanKey1Property() {
		return "portalSecurityResourceId";
	}


	@Override
	protected String getBeanKey2Property() {
		return "portalEntityId";
	}


	@Override
	protected Short getBeanKey1Value(PortalSecurityUserAssignmentExpanded bean) {
		return bean.getPortalSecurityResourceId();
	}


	@Override
	protected Integer getBeanKey2Value(PortalSecurityUserAssignmentExpanded bean) {
		return bean.getPortalEntityId();
	}
}
