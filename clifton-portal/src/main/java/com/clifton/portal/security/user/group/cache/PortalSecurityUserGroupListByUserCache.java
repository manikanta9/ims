package com.clifton.portal.security.user.group.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.group.PortalSecurityUserGroup;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalSecurityUserGroupListByUserCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserGroup, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityUser.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserGroup bean) {
		if (bean.getSecurityUser() != null) {
			return bean.getSecurityUser().getId();
		}
		return null;
	}
}
