package com.clifton.portal.security;


import com.clifton.portal.security.user.PortalSecurityUserManagementService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;


/**
 * @author manderson
 */
public class PortalUserDetailsAuthenticationProvider extends DaoAuthenticationProvider {

	private PortalSecurityUserManagementService portalSecurityUserManagementService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		try {
			Authentication auth = super.authenticate(authentication);
			// If we reach here, then authentication was successful so make sure invalid login attempts is reset
			getPortalSecurityUserManagementService().resetPortalSecurityUserInvalidLoginCount(authentication.getName());
			return auth;
		}
		catch (BadCredentialsException e) {
			// invalid login, update to invalid login attempts
			getPortalSecurityUserManagementService().incrementPortalSecurityUserInvalidLoginCount(authentication.getName());
			throw e;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserManagementService getPortalSecurityUserManagementService() {
		return this.portalSecurityUserManagementService;
	}


	public void setPortalSecurityUserManagementService(PortalSecurityUserManagementService portalSecurityUserManagementService) {
		this.portalSecurityUserManagementService = portalSecurityUserManagementService;
	}
}
