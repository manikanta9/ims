package com.clifton.portal.security.user.group.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.group.PortalSecurityGroupResource;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalSecurityGroupResourceListByGroupCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityGroupResource, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityGroup.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityGroupResource bean) {
		if (bean.getSecurityGroup() != null) {
			return bean.getSecurityGroup().getId();
		}
		return null;
	}
}
