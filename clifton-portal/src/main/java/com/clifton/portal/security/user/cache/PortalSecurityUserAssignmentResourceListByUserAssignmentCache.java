package com.clifton.portal.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentResourceListByUserAssignmentCache</code> cache is used for client (portal entity specific) users to cache their
 * access to security resources they have access to see for each assignment
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentResourceListByUserAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserAssignmentResource, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityUserAssignment.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortalSecurityUserAssignmentResource bean) {
		if (bean.getSecurityUserAssignment() != null) {
			return bean.getSecurityUserAssignment().getId();
		}
		return null;
	}
}
