package com.clifton.portal.security.user;

import com.clifton.core.security.password.SecurityPasswordValidator;
import com.clifton.core.security.password.passay.PassayPasswordValidator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.event.PortalNotificationEvent;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalSecurityUserManagementServiceImpl</code> contains methods for working with user objects
 * and changes to their accounts/passwords, etc.
 *
 * @author manderson
 */
@Service
public class PortalSecurityUserManagementServiceImpl implements PortalSecurityUserManagementService {

	private EventHandler eventHandler;

	private PasswordEncoder passwordEncoder;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;
	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityUser savePortalSecurityUser(PortalSecurityUser bean) {
		if (bean.isNewBean()) {
			if (bean.getUserRole() == null) {
				bean.setUserRole(getPortalSecurityUserService().getPortalSecurityUserRoleByName(PortalSecurityUserRole.ROLE_CLIENT_USER));
			}
			validatePortalSecurityUserSourceSystemNotSet(bean, "Manual User Entry");
			// The only fields that can be entered manually for NEW users are: user role, user name, first name, last name, title, phone number, and password
			bean.setEmailAddress(bean.getUserName());
			List<String> changedValues = CoreCompareUtils.getNoEqualPropertiesWithSetters(new PortalSecurityUser(), bean, false, "userRole", "username", "emailAddress", "firstName", "lastName", "title", "companyName", "phoneNumber", "password");
			ValidationUtils.assertEmpty(changedValues, "The only fields that can be set for new users are: Username/E-mail, User Role, First Name, Last Name, Title, Company, Phone Number, and Password. You cannot change the following: " + StringUtils.collectionToCommaDelimitedString(changedValues));
			// Users are not required to assign a password on creation of a user
			if (!StringUtils.isEmpty(bean.getPassword())) {
				bean.setPassword(validateAndEncodePassword(bean.getUsername(), null, bean.getPassword()));
			}
		}
		else {
			PortalSecurityUser originalBean = getPortalSecurityUserService().getPortalSecurityUser(bean.getId());
			validatePortalSecurityUserSourceSystemNotSet(originalBean, "User editing");
			ValidationUtils.assertNull(bean.getPortalEntitySourceSystem(), "You cannot change the source system for the user.");
			// Allow username change - confirm the change first
			if (!CompareUtils.isEqual(originalBean.getUsername(), bean.getUserName())) {
				PortalSecurityUser existingUser = getPortalSecurityUserService().getPortalSecurityUserByUserName(bean.getUsername(), true);
				// The email is already validated in the above call, so if existingUser is not null then a user already exists
				ValidationUtils.assertNull(existingUser, "A user already exists in the system with username [" + bean.getUserName() + "]");
				// If it's good, set the email to the username
				bean.setEmailAddress(bean.getUserName());
			}

			// The only fields that can be edited manually for EXISTING users are: first name, last name, title, phone number, however user name/email has already been validated above
			List<String> changedValues = CoreCompareUtils.getNoEqualPropertiesWithSetters(originalBean, bean, false, "username", "emailAddress", "firstName", "lastName", "title", "companyName", "phoneNumber");
			ValidationUtils.assertEmpty(changedValues, "The only fields that can be modified are: User Name/E-mail, First Name, Last Name, Title, Company, Phone Number, Disabled, and Disabled Reason. You cannot change the following: " + StringUtils.collectionToCommaDelimitedString(changedValues));
		}
		if (!StringUtils.isPhoneNumberValid(bean.getPhoneNumber(), true)) {
			throw new ValidationException("Phone Number [" + bean.getPhoneNumber() + "] is not valid. Please enter a valid phone number (minimum 10 digits).");
		}
		return savePortalSecurityUserImpl(bean, false);
	}


	@Override
	public void incrementPortalSecurityUserInvalidLoginCount(String userName) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
		// Only Track for Client Users
		if (portalSecurityUser != null && portalSecurityUser.getUserRole().isPortalEntitySpecific()) {
			short count = portalSecurityUser.getInvalidLoginAttemptCount() == null ? 0 : portalSecurityUser.getInvalidLoginAttemptCount();
			count++;
			portalSecurityUser.setInvalidLoginAttemptCount(count);
			savePortalSecurityUserImpl(portalSecurityUser, true);
		}
	}


	@Override
	public void resetPortalSecurityUserInvalidLoginCount(String userName) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
		if (portalSecurityUser != null && portalSecurityUser.getInvalidLoginAttemptCount() != null) {
			portalSecurityUser.setInvalidLoginAttemptCount(null);
			savePortalSecurityUserImpl(portalSecurityUser, true);
		}
	}


	@Override
	public String requestPortalSecurityUserPasswordReset(String userName) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
		ValidationUtils.assertNotNull(portalSecurityUser, "Cannot find user with user name [" + userName + "]");
		ValidationUtils.assertFalse(portalSecurityUser.isDisabled(), "Cannot find user with user name [" + userName + "]");

		byte[] random = new byte[125];
		SecureRandom sr = new SecureRandom();
		sr.nextBytes(random);


		portalSecurityUser.setPasswordResetKey(EncryptionUtils.byteArrayToHexString(random));
		portalSecurityUser.setPasswordResetKeyExpirationDateAndTime(DateUtils.addMinutes(new Date(), 120));

		portalSecurityUser = savePortalSecurityUserImpl(portalSecurityUser, true);

		// Raise the event with the un-encoded password for the notification generation
		Map<String, Object> templateContextBeanMap = new HashMap<>();
		templateContextBeanMap.put("passwordResetKey", portalSecurityUser.getPasswordResetKey());

		PortalNotificationEvent event = new PortalNotificationEvent(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_FORGOT_PASSWORD, portalSecurityUser, templateContextBeanMap);
		getEventHandler().raiseEvent(event);
		return event.getResult();
	}


	@Override
	public void resetPortalSecurityUserPassword(String passwordResetKey, String password) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserByPasswordResetKey(passwordResetKey);
		ValidationUtils.assertNotNull(user, "Password reset key has expired.  Please request a new one.");
		doUpdatePortalSecurityUserPassword(user.getId(), password, false);

		PortalNotificationEvent event = new PortalNotificationEvent(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_CONFIRM_RESET_PASSWORD, user);
		getEventHandler().raiseEvent(event);
	}


	@Override
	public String sendPortalSecurityUserWelcomeNotification(short portalSecurityUserId) {
		PortalSecurityUser portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUser(portalSecurityUserId);
		if (portalSecurityUser.getPortalEntitySourceSystem() == null && portalSecurityUser.getTermsOfUseAcceptanceDate() != null) {
			throw new ValidationException("Portal Security User [" + portalSecurityUser.getLabel() + "] has already accepted the terms of use.  If the user forgot their password they should use that link instead.");
		}
		return resetPortalSecurityUserPasswordImpl(portalSecurityUser, PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME);
	}


	@Override
	public String resetPortalSecurityUserRandomPassword(PortalSecurityUser portalSecurityUser) {
		if (portalSecurityUser.getPortalEntitySourceSystem() == null && portalSecurityUser.getTermsOfUseAcceptanceDate() != null) {
			throw new ValidationException("Portal Security User [" + portalSecurityUser.getLabel() + "] has already accepted the terms of use.  If the user forgot their password they should use that link instead.");
		}
		SecurityPasswordValidator passwordValidator = new PassayPasswordValidator();
		String randomPassword = passwordValidator.generateRandomPassword();
		// Don't want to validate the password against rules - used basic rules to generate random one
		// So just encode it and update the user
		String encodedPass = getPasswordEncoder().encode(randomPassword);
		portalSecurityUser.setPassword(encodedPass);
		portalSecurityUser.setPasswordUpdateDate(null); // Force user to reset password on their own successful login
		portalSecurityUser.setPasswordResetKeyExpirationDateAndTime(null);
		portalSecurityUser.setPasswordResetKey(null);
		savePortalSecurityUserImpl(portalSecurityUser, true);
		return randomPassword;
	}


	private String resetPortalSecurityUserPasswordImpl(PortalSecurityUser portalSecurityUser, String notificationDefinitionTypeName) {
		ValidationUtils.assertNull(portalSecurityUser.getPortalEntitySourceSystem(), "Can only reset passwords for External client users.");
		ValidationUtils.assertNotNull(notificationDefinitionTypeName, "Portal Notification Definition Type Name is required");


		// Raise the event with the un-encoded password for the notification generation
		Map<String, Object> templateContextBeanMap = new HashMap<>();
		templateContextBeanMap.put("newPassword", resetPortalSecurityUserRandomPassword(portalSecurityUser));
		// NOTE: I think we also need to raise the event with option to run as system user here???
		PortalNotificationEvent event = new PortalNotificationEvent(notificationDefinitionTypeName, portalSecurityUser, templateContextBeanMap);
		getEventHandler().raiseEvent(event);
		return event.getResult();
	}


	@Override
	public void updatePortalSecurityUserRole(short portalSecurityUserId, short portalSecurityUserRoleId) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUser(portalSecurityUserId);
		PortalSecurityUserRole userRole = getPortalSecurityUserService().getPortalSecurityUserRole(portalSecurityUserRoleId);
		ValidationUtils.assertNotEquals(user.getUserRole(), userRole, "User [" + user.getLabel() + "] is already assigned to role [" + userRole.getName() + "].");

		// If current user is Portal Entity Specific, then they can only assign roles that are also portal entity specific
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		if (!currentUser.getUserRole().isAdministrator()) {
			ValidationUtils.assertFalse(userRole.isAdministrator(), "Only Administrators can assign the administrator role.");
		}
		if (!currentUser.getUserRole().isPortalAdministrator()) {
			ValidationUtils.assertFalse(userRole.isPortalAdministrator(), "Only Portal Administrators can assign the Portal Administrator role.");
		}

		if (currentUser.getUserRole().isPortalEntitySpecific()) {
			ValidationUtils.assertTrue(userRole.isPortalEntitySpecific(), "You do not have permission to change user [" + user.getLabel() + "] to role [" + userRole.getName() + "]");
		}
		user.setUserRole(userRole);
		savePortalSecurityUserImpl(user, false);
	}


	@Override
	public void updatePortalSecurityUserPassword(short portalSecurityUserId, String password) {
		doUpdatePortalSecurityUserPassword(portalSecurityUserId, password, true);
	}


	private void doUpdatePortalSecurityUserPassword(short portalSecurityUserId, String password, boolean clearPasswordDate) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUser(portalSecurityUserId);
		validatePortalSecurityUserSourceSystemNotSet(user, "Password Resets");
		// Validate Not the Current User
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		if (currentUser.equals(user) && StringUtils.isEmpty(currentUser.getPasswordResetKey())) {
			throw new ValidationException("To change your own password, please use the Password Reset option under Settings.");
		}
		String encodedPass = validateAndEncodePassword(user.getUsername(), user.getPassword(), password);
		user.setPassword(encodedPass);
		user.setPasswordUpdateDate(clearPasswordDate ? null : new Date()); // Force user to reset password on their own successful login

		user.setPasswordResetKeyExpirationDateAndTime(null);
		user.setPasswordResetKey(null);

		savePortalSecurityUserImpl(user, false);
	}


	@Override
	public void updatePortalSecurityCurrentUserPassword(String oldPassword, String password) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		validatePortalSecurityUserSourceSystemNotSet(user, "Password Resets");
		// Validate Existing Password Matches
		if (!getPasswordEncoder().matches(oldPassword, user.getPassword())) {
			throw new ValidationException("You current password is invalid.  Please try again");
		}

		String encodedPass = validateAndEncodePassword(user.getUsername(), user.getPassword(), password);
		user.setPassword(encodedPass);
		user.setPasswordUpdateDate(new Date()); // Current User Changing their Own Password, so Set Update Date
		user.setPasswordResetKeyExpirationDateAndTime(null);
		user.setPasswordResetKey(null);

		savePortalSecurityUserImpl(user, false);

		PortalNotificationEvent event = new PortalNotificationEvent(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_CONFIRM_RESET_PASSWORD, user);
		getEventHandler().raiseEvent(event);
	}


	@Override
	public void lockPortalSecurityUser(short portalSecurityUserId) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUser(portalSecurityUserId);
		validatePortalSecurityUserSourceSystemNotSet(user, "Account Locking");

		if (user.isAccountLocked()) {
			throw new ValidationException("User [" + user.getLabel() + "] is already locked.");
		}
		user.setAccountLocked(true);
		savePortalSecurityUserImpl(user, false);
	}


	@Override
	public void unlockPortalSecurityUser(short portalSecurityUserId) {
		PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUser(portalSecurityUserId);
		validatePortalSecurityUserSourceSystemNotSet(user, "Account Unlocking");
		if (!user.isAccountLocked()) {
			throw new ValidationException("User [" + user.getLabel() + "] is not locked.");
		}
		user.setAccountLocked(false);
		// Clear Invalid Login Attempt Count
		user.setInvalidLoginAttemptCount(null);
		savePortalSecurityUserImpl(user, false);
	}


	@Override
	public void acceptPortalSecurityUserTermsOfUse(PortalSecurityUser user) {
		// If it's not blank - throw an exception or just ignore it?
		if (user.getTermsOfUseAcceptanceDate() == null) {
			user.setTermsOfUseAcceptanceDate(new Date());
			// Use Service Method to avoid additional validation
			savePortalSecurityUserImpl(user, false);

			// Trigger Welcome event on terms of use acceptance that will see if other users need to be sent the welcome emails
			getEventHandler().raiseEvent(PortalSecurityUserChangeEvent.ofTermsOfUseAcceptance(user));
		}
	}


	protected PortalSecurityUser savePortalSecurityUserImpl(PortalSecurityUser bean, boolean runAsSystemUser) {
		if (runAsSystemUser) {
			return getPortalSecurityImpersonationHandler().runAsSystemUserAndReturn(() -> getPortalSecurityUserService().savePortalSecurityUser(bean));
		}
		return getPortalSecurityUserService().savePortalSecurityUser(bean);
	}


	private void validatePortalSecurityUserSourceSystemNotSet(PortalSecurityUser user, String actionName) {
		if (user.getPortalEntitySourceSystem() != null) {
			throw new ValidationException(actionName + " is not supported for users created and maintained by source system [" + user.getPortalEntitySourceSystem().getName() + "].");
		}
	}


	protected String validateAndEncodePassword(String username, String oldEncodedPassword, String newPassword) {
		if (StringUtils.isEmpty(newPassword)) {
			throw new ValidationException("Password is required.");
		}
		// Password cannot be contained in the username
		if (StringUtils.isLikeIgnoringCase(username, newPassword)) {
			throw new ValidationException("Password cannot contain your user name.");
		}
		// Passwords cannot match previous password
		if (!StringUtils.isEmpty(oldEncodedPassword) && getPasswordEncoder().matches(newPassword, oldEncodedPassword)) {
			throw new ValidationException("Password cannot be the same as the previous password.");
		}

		SecurityPasswordValidator validator = new PassayPasswordValidator();
		validator.validatePassword(username, newPassword);
		return getPasswordEncoder().encode(newPassword);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PasswordEncoder getPasswordEncoder() {
		return this.passwordEncoder;
	}


	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}
}
