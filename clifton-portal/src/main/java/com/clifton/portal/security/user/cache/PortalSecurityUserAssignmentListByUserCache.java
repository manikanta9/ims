package com.clifton.portal.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentListByUserCache</code> cache is used for client (portal entity specific) users to cache their assignments
 * <p>
 * This is specifically used to see if the user is available as a Portal Administrator and has access to security/admin functions
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentListByUserCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserAssignment, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityUser.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserAssignment bean) {
		if (bean.getSecurityUser() != null) {
			return bean.getSecurityUser().getId();
		}
		return null;
	}
}
