package com.clifton.portal.security.password;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class PortalPasswordEncoder extends BCryptPasswordEncoder {

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		// Need to check for null because internal users that don't authenticate correctly against crowd will end up here and we won't have a password for them
		if (encodedPassword != null) {
			return super.matches(rawPassword, encodedPassword);
		}
		return false;
	}
}
