package com.clifton.portal.security.web.mvc;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.portal.security.authorization.PortalSecureMethod;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserRole;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.tracking.PortalTrackingEventMethod;
import com.clifton.portal.tracking.generator.PortalTrackingEventGenerator;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;


/**
 * The <code>PortalSecureHandlerInterceptorAdapter</code> verifies the user has access to the requested method.
 * <p>
 * Supports methods with {@link SecureMethod} annotation (limited to disabling and admin only) for core utility methods
 * And {@link PortalSecureMethod} annotation for Portal User Role checks.
 * <p>
 * If neither annotation exists then requires at least user is a "Portal Administrator"
 *
 * @author manderson
 */
public class PortalSecureHandlerInterceptorAdapter extends HandlerInterceptorAdapter {

	private ApplicationContextService applicationContextService;

	private PortalSecurityUserService portalSecurityUserService;
	private PortalTrackingEventGenerator portalTrackingEventGenerator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// Verifies the User Has access to the Request
		verifySecurityAccess(request, handler);
		return super.preHandle(request, response, handler);
	}


	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		// Log Tracking Events if no exception
		if (ex == null) {
			logPortalTrackingEvent(request, handler);
		}

		super.afterCompletion(request, response, handler, ex);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////                Security Methods                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Verifies that the user has rights to executed requested handler method.
	 */
	protected void verifySecurityAccess(HttpServletRequest request, Object handler) {
		Method handlerMethod = ((HandlerMethod) handler).getMethod();
		PortalSecureMethod portalSecureAnnotation = AnnotationUtils.getAnnotation(handlerMethod, PortalSecureMethod.class);
		SecureMethod secureAnnotation = AnnotationUtils.getAnnotation(handlerMethod, SecureMethod.class);
		if (isSecurityDisabled(portalSecureAnnotation, secureAnnotation)) {
			LogUtils.info(getClass(), "METHOD " + handlerMethod.getName() + " IS NOT SECURED");
			return;
		}

		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		if (currentUser == null || currentUser.isNewBean()) {
			throw new RuntimeException("You are either not logged in, or your user is not fully set up.");
		}
		PortalSecurityUserRole userRole = currentUser.getUserRole();
		// do not check security if current user is an admin
		if (userRole.isAdministrator()) {
			LogUtils.info(getClass(), "CURRENT USER IS AN ADMIN: UNRESTRICTED ACCESS TO EVERYTHING: " + request.getRequestURI());
			return;
		}

		String methodName = handlerMethod.getName();
		if (isSecurityAdmin(portalSecureAnnotation, secureAnnotation)) {
			// Admins will never get here
			throw new AccessDeniedException("Only Administrators can access: " + methodName);
		}
		if (portalSecureAnnotation != null) {
			// Bypass Security Option
			if (portalSecureAnnotation.bypassWhenNonPortalEntitySpecific() && !userRole.isPortalEntitySpecific()) {
				return;
			}
			if (portalSecureAnnotation.portalAdminSecurity() && !userRole.isPortalAdministrator()) {
				throw new AccessDeniedException("Only Portal Administrators can access: " + methodName);
			}
			if (portalSecureAnnotation.denyPortalEntitySpecific() && userRole.isPortalEntitySpecific()) {
				throw new AccessDeniedException("You do not have access to: " + methodName);
			}
			if (portalSecureAnnotation.portalSecurityAdminSecurity() && !getPortalSecurityUserService().isPortalSecurityUserSecurityAdmin(currentUser)) {
				throw new AccessDeniedException("Only Portal Security Administrators can access: " + methodName);
			}
		}
		else {
			// If no secure annotation - assume must be a Portal Administrator
			if (!userRole.isPortalAdministrator()) {
				throw new AccessDeniedException("Only Portal Administrators can access: " + methodName);
			}
		}
	}


	/**
	 * There are some core methods that can be used (getValidationMetaData) that use SecureMethod annotation for disabling security
	 * So, we can support disable on both annotations
	 */
	private boolean isSecurityDisabled(PortalSecureMethod portalSecureMethod, SecureMethod secureMethod) {
		if (portalSecureMethod != null) {
			return portalSecureMethod.disableSecurity();
		}
		if (secureMethod != null) {
			return secureMethod.disableSecurity();
		}
		return false;
	}


	/**
	 * There are some core methods that can be used that use SecureMethod annotation for admin only (request stats)
	 * So, we can support admin on both annotations
	 */
	private boolean isSecurityAdmin(PortalSecureMethod portalSecureMethod, SecureMethod secureMethod) {
		if (portalSecureMethod != null) {
			return portalSecureMethod.adminSecurity();
		}
		if (secureMethod != null) {
			return secureMethod.adminSecurity();
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////                Tracking Methods                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If method has annotation to track the request as an event, do so
	 */
	protected void logPortalTrackingEvent(HttpServletRequest request, Object handler) {
		Method handlerMethod = ((HandlerMethod) handler).getMethod();
		PortalTrackingEventMethod portalTrackingEventAnnotation = AnnotationUtils.getAnnotation(handlerMethod, PortalTrackingEventMethod.class);
		if (portalTrackingEventAnnotation != null) {
			if (!portalTrackingEventAnnotation.eventType().isWebRequestEventMethodSupported()) {
				// TODO ADD THIS AS A BASIC PROJECT TEST
				throw new IllegalStateException("Event Type " + portalTrackingEventAnnotation.eventType().name() + " is not supported through Web Request Event Method Annotations.");
			}
			getPortalTrackingEventGenerator().generatePortalTrackingWebRequestMethodEvent(portalTrackingEventAnnotation, request);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalTrackingEventGenerator getPortalTrackingEventGenerator() {
		return this.portalTrackingEventGenerator;
	}


	public void setPortalTrackingEventGenerator(PortalTrackingEventGenerator portalTrackingEventGenerator) {
		this.portalTrackingEventGenerator = portalTrackingEventGenerator;
	}
}
