package com.clifton.portal.security.user.expanded;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.core.util.MathUtils;


/**
 * The <code>PortalSecurityUserAssignmentExpanded</code> is an expanded version of the {@link PortalSecurityUserAssignment} and its {@link PortalSecurityUserAssignmentResource} list
 * (as will as the role level assignments where applied).
 * <p>
 * This is used for Database Level Joins Only and not used in code
 *
 * @author manderson
 */
public class PortalSecurityUserAssignmentExpanded extends BaseSimpleEntity<Integer> {

	private Short portalSecurityUserId;

	private Integer portalEntityId;

	/**
	 * Can be determined by the portal entity - added here to simplify queries
	 */
	private Short portalEntityViewTypeId;

	private Short portalSecurityResourceId;

	private boolean portalSecurityAdministrator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserAssignmentExpanded() {
		//
	}


	public PortalSecurityUserAssignmentExpanded(PortalSecurityUserAssignmentExpanded assignmentExpanded, Integer portalEntityId, Short portalEntityViewTypeId) {
		this.portalSecurityUserId = assignmentExpanded.getPortalSecurityUserId();
		this.portalEntityId = portalEntityId;
		this.portalEntityViewTypeId = portalEntityViewTypeId;
		this.portalSecurityResourceId = assignmentExpanded.getPortalSecurityResourceId();
		this.portalSecurityAdministrator = assignmentExpanded.isPortalSecurityAdministrator();
	}


	public PortalSecurityUserAssignmentExpanded(PortalSecurityUserAssignment assignment, PortalSecurityResource securityResource) {
		this.portalSecurityUserId = assignment.getSecurityUser().getId();
		this.portalEntityId = assignment.getPortalEntity().getId();
		this.portalEntityViewTypeId = PortalUtils.getPortalEntityViewTypeId(assignment.getPortalEntity());
		this.portalSecurityResourceId = (securityResource != null ? securityResource.getId() : null);
		this.portalSecurityAdministrator = assignment.getAssignmentRole().isPortalSecurityAdministrator();
	}


	public PortalSecurityUserAssignmentExpanded(PortalSecurityUser user, PortalSecurityResource securityResource) {
		this.portalSecurityUserId = user.getId();
		this.portalEntityId = null;
		this.portalSecurityResourceId = securityResource.getId();
	}


	@Override
	public boolean equals(Object o) {
		// Equal If all Same User / Entity / Security Resource
		if (!(o instanceof PortalSecurityUserAssignmentExpanded)) {
			return false;
		}
		PortalSecurityUserAssignmentExpanded oBean = (PortalSecurityUserAssignmentExpanded) o;
		if (MathUtils.isEqual(this.getPortalSecurityUserId(), oBean.getPortalSecurityUserId())) {
			if (MathUtils.isEqual(this.getPortalEntityId(), oBean.getPortalEntityId())) {
				if (MathUtils.isEqual(this.portalSecurityResourceId, oBean.getPortalSecurityResourceId())) {
					return true;
				}
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPortalSecurityUserId() {
		return this.portalSecurityUserId;
	}


	public void setPortalSecurityUserId(Short portalSecurityUserId) {
		this.portalSecurityUserId = portalSecurityUserId;
	}


	public Integer getPortalEntityId() {
		return this.portalEntityId;
	}


	public void setPortalEntityId(Integer portalEntityId) {
		this.portalEntityId = portalEntityId;
	}


	public Short getPortalSecurityResourceId() {
		return this.portalSecurityResourceId;
	}


	public void setPortalSecurityResourceId(Short portalSecurityResourceId) {
		this.portalSecurityResourceId = portalSecurityResourceId;
	}


	public boolean isPortalSecurityAdministrator() {
		return this.portalSecurityAdministrator;
	}


	public void setPortalSecurityAdministrator(boolean portalSecurityAdministrator) {
		this.portalSecurityAdministrator = portalSecurityAdministrator;
	}


	public Short getPortalEntityViewTypeId() {
		return this.portalEntityViewTypeId;
	}


	public void setPortalEntityViewTypeId(Short portalEntityViewTypeId) {
		this.portalEntityViewTypeId = portalEntityViewTypeId;
	}
}
