package com.clifton.portal.security.user.search;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.search.BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PortalSecurityUserAssignmentSearchFormConfigurer extends BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer {

	public PortalSecurityUserAssignmentSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);

		PortalSecurityUserAssignmentSearchForm assignmentSearchForm = (PortalSecurityUserAssignmentSearchForm) searchForm;


		Boolean disabled = (Boolean) SearchUtils.getRestrictionValue(searchForm, "disabled");
		if (disabled != null) {
			if (BooleanUtils.isTrue(disabled)) {
				assignmentSearchForm.addSearchRestriction(new SearchRestriction("disabledDate", ComparisonConditions.LESS_THAN_OR_EQUALS, new Date()));
			}
			else {
				assignmentSearchForm.addSearchRestriction(new SearchRestriction("disabledDate", ComparisonConditions.GREATER_THAN_OR_IS_NULL, new Date()));
			}
		}
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		Boolean pendingTermination = (Boolean) SearchUtils.getRestrictionValue(getSortableSearchForm(), "pendingTermination");
		if (pendingTermination != null) {
			String portalEntityAlias = getPathAlias("portalEntity", criteria);
			LogicalExpression pendingTerminationExpression = Restrictions.and(Restrictions.or(Restrictions.ge("disabledDate", DateUtils.clearTime(new Date())), Restrictions.isNull("disabledDate")), Restrictions.and(Restrictions.isNotNull(portalEntityAlias + ".terminationDate"), Restrictions.ge(portalEntityAlias + ".endDate", DateUtils.clearTime(new Date()))));
			criteria.add(BooleanUtils.isTrue(pendingTermination) ? pendingTerminationExpression : Restrictions.not(pendingTerminationExpression));
		}
	}


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId, Boolean viewSecurityAdminRole, Boolean hasSecurityResourceAccess) {
		DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "se");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eq("portalSecurityUserId", portalSecurityUserId));
		sub.add(Restrictions.eqProperty("portalEntityId", getPathAlias("portalEntity", criteria) + ".id"));
		if (BooleanUtils.isTrue(viewSecurityAdminRole)) {
			sub.add(Restrictions.eq("portalSecurityAdministrator", true));
		}
		if (BooleanUtils.isTrue(hasSecurityResourceAccess)) {
			sub.add(Restrictions.isNotNull("portalSecurityResourceId"));
		}
		criteria.add(Subqueries.exists(sub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		criteria.add(Restrictions.in("portalEntity.id", BeanUtils.getBeanIdentityArray(viewAsEntityList, Integer.class)));
	}
}
