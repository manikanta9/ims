package com.clifton.portal.security.web;

import com.clifton.portal.tracking.generator.PortalTrackingEventGenerator;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The <code>PortalAuthenticationSuccessHandler</code> extends the {@link SavedRequestAwareAuthenticationSuccessHandler}
 * but also logs the login event into the tracking table.
 *
 * @author manderson
 */
public class PortalAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private PortalTrackingEventGenerator portalTrackingEventGenerator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
		super.onAuthenticationSuccess(request, response, authentication);

		// Track the Login Event
		String username = authentication.getName();
		getPortalTrackingEventGenerator().generatePortalTrackingLoginEvent(request, username);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalTrackingEventGenerator getPortalTrackingEventGenerator() {
		return this.portalTrackingEventGenerator;
	}


	public void setPortalTrackingEventGenerator(PortalTrackingEventGenerator portalTrackingEventGenerator) {
		this.portalTrackingEventGenerator = portalTrackingEventGenerator;
	}
}
