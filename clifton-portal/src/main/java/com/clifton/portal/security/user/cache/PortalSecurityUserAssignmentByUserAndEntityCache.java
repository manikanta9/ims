package com.clifton.portal.security.user.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentByUserAndEntityCache</code> caches the {@link PortalSecurityUserAssignment} by the {@link PortalSecurityUser} and {@link PortalEntity}
 * These is UX so there can only ever be one.
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentByUserAndEntityCache extends SelfRegisteringCompositeKeyDaoCache<PortalSecurityUserAssignment, Short, Integer> {


	@Override
	protected String getBeanKey1Property() {
		return "securityUser.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "portalEntity.id";
	}


	@Override
	protected Short getBeanKey1Value(PortalSecurityUserAssignment bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getSecurityUser());
		}
		return null;
	}


	@Override
	protected Integer getBeanKey2Value(PortalSecurityUserAssignment bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getPortalEntity());
		}
		return null;
	}
}
