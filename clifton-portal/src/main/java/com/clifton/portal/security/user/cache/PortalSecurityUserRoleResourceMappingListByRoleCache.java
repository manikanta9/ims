package com.clifton.portal.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.PortalSecurityUserRoleResourceMapping;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentListByUserCache</code> cache is used for client (portal entity specific) users to cache their assignments
 * <p>
 * This is specifically used to see if the user is available as a Portal Administrator and has access to security/admin functions
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserRoleResourceMappingListByRoleCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserRoleResourceMapping, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityUserRole.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserRoleResourceMapping bean) {
		if (bean.getSecurityUserRole() != null) {
			return bean.getSecurityUserRole().getId();
		}
		return null;
	}
}
