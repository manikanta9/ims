package com.clifton.portal.security.user.expanded;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentExpandedHandlerImpl implements PortalSecurityUserAssignmentExpandedHandler {

	private UpdatableDAO<PortalSecurityUserAssignmentExpanded> portalSecurityUserAssignmentExpandedDAO;

	private DaoSingleKeyListCache<PortalSecurityUserAssignmentExpanded, Short> portalSecurityUserAssignmentExpandedByUserCache;

	private DaoCompositeKeyListCache<PortalSecurityUserAssignmentExpanded, Short, Integer> portalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalSecurityUserAssignmentExpanded> getPortalSecurityUserAssignmentExpandedListForUser(short userId) {
		return getPortalSecurityUserAssignmentExpandedByUserCache().getBeanListForKeyValue(getPortalSecurityUserAssignmentExpandedDAO(), userId);
	}


	@Override
	public List<PortalSecurityUserAssignmentExpanded> getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(short securityResourceId, Integer portalEntityId) {
		return getPortalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache().getBeanListForKeyValues(getPortalSecurityUserAssignmentExpandedDAO(), securityResourceId, portalEntityId);
	}


	@Override
	public void savePortalSecurityUserAssignmentExpandedList(List<PortalSecurityUserAssignmentExpanded> newList, List<PortalSecurityUserAssignmentExpanded> existingList) {
		// IF BOTH lists aren't empty, then need to go through them and properly set ids, etc for updates.
		if (!CollectionUtils.isEmpty(newList) && !CollectionUtils.isEmpty(existingList)) {
			for (PortalSecurityUserAssignmentExpanded newAssignment : CollectionUtils.getIterable(newList)) {
				for (PortalSecurityUserAssignmentExpanded existingAssignment : CollectionUtils.getIterable(existingList)) {
					// Call special overridden equals method to see if both entities are really equal
					if (newAssignment.equals(existingAssignment)) {
						// Set ID on the the new list, so updates
						newAssignment.setId(existingAssignment.getId());
					}
				}
			}
		}
		getPortalSecurityUserAssignmentExpandedDAO().saveList(newList, existingList);
	}


	@Override
	public void deletePortalSecurityUserAssignmentExpandedList(List<PortalSecurityUserAssignmentExpanded> deleteList) {
		getPortalSecurityUserAssignmentExpandedDAO().deleteList(deleteList);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<PortalSecurityUserAssignmentExpanded> getPortalSecurityUserAssignmentExpandedDAO() {
		return this.portalSecurityUserAssignmentExpandedDAO;
	}


	public void setPortalSecurityUserAssignmentExpandedDAO(UpdatableDAO<PortalSecurityUserAssignmentExpanded> portalSecurityUserAssignmentExpandedDAO) {
		this.portalSecurityUserAssignmentExpandedDAO = portalSecurityUserAssignmentExpandedDAO;
	}


	public DaoSingleKeyListCache<PortalSecurityUserAssignmentExpanded, Short> getPortalSecurityUserAssignmentExpandedByUserCache() {
		return this.portalSecurityUserAssignmentExpandedByUserCache;
	}


	public void setPortalSecurityUserAssignmentExpandedByUserCache(DaoSingleKeyListCache<PortalSecurityUserAssignmentExpanded, Short> portalSecurityUserAssignmentExpandedByUserCache) {
		this.portalSecurityUserAssignmentExpandedByUserCache = portalSecurityUserAssignmentExpandedByUserCache;
	}


	public DaoCompositeKeyListCache<PortalSecurityUserAssignmentExpanded, Short, Integer> getPortalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache() {
		return this.portalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache;
	}


	public void setPortalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache(DaoCompositeKeyListCache<PortalSecurityUserAssignmentExpanded, Short, Integer> portalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache) {
		this.portalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache = portalSecurityUserAssignmentExpandedBySecurityResourceAndEntityCache;
	}
}
