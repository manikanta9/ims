package com.clifton.portal.security.user.expanded;

import com.clifton.core.util.event.EventObject;
import com.clifton.portal.security.user.PortalSecurityUser;


/**
 * The <code>PortalSecurityUserAssignmentExpandedRebuildEvent</code> is raised on various changes that may need a security rebuild
 * Rebuilds can be triggered for:
 * 1. Single User
 * 2. All Users with Access to a Specified Entity (or multiple entities)
 * 3. All Users In A Specified Role
 * 4. All Users
 *
 * @author manderson
 */
public class PortalSecurityUserAssignmentExpandedRebuildEvent extends EventObject<PortalSecurityUser, Object> {

	public static final String EVENT_NAME = "PORTAL_SECURITY_USER_ASSIGNMENT_EXPANDED_REBUILD";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If set will only rebuild security for users with access to the specified portal entities
	 */
	private Integer[] portalEntityIds;

	private Short portalSecurityUserAssignmentRoleId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PortalSecurityUserAssignmentExpandedRebuildEvent ofPortalEntityChanges(Integer[] portalEntityIds) {
		return new PortalSecurityUserAssignmentExpandedRebuildEvent(null, portalEntityIds, null);
	}


	public static PortalSecurityUserAssignmentExpandedRebuildEvent ofPortalSecurityUserChange(PortalSecurityUser portalSecurityUser) {
		return new PortalSecurityUserAssignmentExpandedRebuildEvent(portalSecurityUser, null, null);
	}


	public static PortalSecurityUserAssignmentExpandedRebuildEvent ofPortalSecurityUserRoleResourceChange(Short portalSecurityUserAssignmentRoleId) {
		return new PortalSecurityUserAssignmentExpandedRebuildEvent(null, null, portalSecurityUserAssignmentRoleId);
	}


	private PortalSecurityUserAssignmentExpandedRebuildEvent(PortalSecurityUser portalSecurityUser, Integer[] portalEntityIds, Short portalSecurityUserAssignmentRoleId) {
		super();
		setEventName(EVENT_NAME);
		setTarget(portalSecurityUser);
		setPortalEntityIds(portalEntityIds);
		setPortalSecurityUserAssignmentRoleId(portalSecurityUserAssignmentRoleId);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer[] getPortalEntityIds() {
		return this.portalEntityIds;
	}


	public void setPortalEntityIds(Integer[] portalEntityIds) {
		this.portalEntityIds = portalEntityIds;
	}


	public Short getPortalSecurityUserAssignmentRoleId() {
		return this.portalSecurityUserAssignmentRoleId;
	}


	public void setPortalSecurityUserAssignmentRoleId(Short portalSecurityUserAssignmentRoleId) {
		this.portalSecurityUserAssignmentRoleId = portalSecurityUserAssignmentRoleId;
	}
}
