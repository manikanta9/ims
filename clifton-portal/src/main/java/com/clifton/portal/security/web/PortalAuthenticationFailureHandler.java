package com.clifton.portal.security.web;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * @author manderson
 */
public class PortalAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {

	// COPIED FROM import com.atlassian.crowd.integration.springsecurity.UsernameStoringAuthenticationFailureHandler;
	// SO WE CAN USE USER NAME AND EXCEPTION MAPPING
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		String username = request.getParameter("j_username");
		if (username != null) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", username);
			}
		}

		super.onAuthenticationFailure(request, response, exception);
	}
}
