package com.clifton.portal.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserByUsernameCache</code> class provides caching and retrieval from cache of PortalSecurityUser
 * objects by user name.
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserByUsernameCache extends SelfRegisteringSingleKeyDaoCache<PortalSecurityUser, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "username";
	}


	@Override
	protected String getBeanKeyValue(PortalSecurityUser bean) {
		return bean.getUsername();
	}
}


