package com.clifton.portal.security.user.expanded.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentExpandedByUserCache</code> caches the list of PortalSecurityUserAssignmentExpanded rows
 * for a Portal Security User
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentExpandedByUserCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserAssignmentExpanded, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "portalSecurityUserId";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserAssignmentExpanded bean) {
		return bean.getPortalSecurityUserId();
	}
}
