package com.clifton.portal.security.resource;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMapping;
import com.clifton.portal.security.search.BasePortalEntitySpecificAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.List;


/**
 * @author manderson
 */
public class PortalSecurityResourceSearchFormConfigurer extends BasePortalEntitySpecificAwareSearchFormConfigurer {

	public PortalSecurityResourceSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);
	}


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId) {
		// This isn't actually used by anywhere in client UI right now
		// Note: Currently only used in Portal Admin - Settings -> User Management
		DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "se");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eq("portalSecurityUserId", portalSecurityUserId));
		sub.add(Restrictions.eqProperty("portalSecurityResourceId", criteria.getAlias() + ".id"));
		criteria.add(Subqueries.exists(sub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		// This isn't actually used by anywhere in client UI right now
		// But it gets the categories for the entities and then filters based on the security resources for those categories
		DetachedCriteria categorySub = DetachedCriteria.forClass(PortalFileCategory.class, "fc");
		categorySub.setProjection(Projections.property("id"));
		categorySub.add(Restrictions.eqProperty("securityResource.id", criteria.getAlias() + ".id"));
		DetachedCriteria viewAsEntitySub = DetachedCriteria.forClass(PortalFileCategoryPortalEntityMapping.class, "fcm");
		viewAsEntitySub.setProjection(Projections.property("id"));
		viewAsEntitySub.add(Restrictions.eqProperty("portalFileCategoryId", categorySub.getAlias() + ".id"));
		viewAsEntitySub.add(Restrictions.eq("approved", true));

		LogicalExpression trueGlobalFileRestriction = Restrictions.and(Restrictions.eq("global", true), Restrictions.isNull("portalEntityViewTypeId"));
		if (CollectionUtils.isEmpty(viewAsEntityList)) {
			// No Entities, then only TRUE global files (no view type)
			viewAsEntitySub.add(trueGlobalFileRestriction);
		}
		else {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(trueGlobalFileRestriction);
			// Entity Specific File Restriction
			disjunction.add(Restrictions.in("portalEntityId", BeanUtils.getBeanIdentityArray(viewAsEntityList, Integer.class)));
			// Global Files limited by view type
			Short[] entityViewTypes = PortalUtils.getPortalEntityViewTypeIds(viewAsEntityList);
			if (!ArrayUtils.isEmpty(entityViewTypes)) {
				disjunction.add(Restrictions.and(
						Restrictions.eq("global", true),
						(entityViewTypes.length == 1 ? Restrictions.eq("portalEntityViewTypeId", entityViewTypes[0]) : Restrictions.in("portalEntityViewTypeId", entityViewTypes))));
			}
			viewAsEntitySub.add(disjunction);
		}
		categorySub.add(Subqueries.exists(viewAsEntitySub));
		criteria.add(Subqueries.exists(categorySub));
	}
}
