package com.clifton.portal.security.user.group.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.group.PortalSecurityUserGroup;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalSecurityUserGroupListByGroupCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserGroup, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityGroup.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserGroup bean) {
		if (bean.getSecurityGroup() != null) {
			return bean.getSecurityGroup().getId();
		}
		return null;
	}
}
