package com.clifton.portal.security.user.group.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.group.PortalSecurityGroupResource;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>PortalSecurityGroupResourceObserver</code> observes changes to group resources and rebuilds security for all users in that group on a change
 * Scheduled for 5 seconds in the future so multiple changes are applied at once.
 *
 * @author manderson
 */
@Component
public class PortalSecurityGroupResourceObserver extends SelfRegisteringDaoObserver<PortalSecurityGroupResource> {

	private PortalSecurityUserService portalSecurityUserService;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortalSecurityGroupResource> dao, DaoEventTypes event, PortalSecurityGroupResource bean, Throwable e) {
		// asynchronous support
		String runId = "GROUP_" + bean.getSecurityGroup().getId();
		final Date scheduledDate = DateUtils.addSeconds(new Date(), 5);

		Runner runner = new AbstractStatusAwareRunner("PORTAL_SECURITY_GROUP_REBUILD", runId, scheduledDate) {

			@Override
			public void run() {
				getPortalSecurityUserService().rebuildPortalSecurityUserAssignmentResourceListForGroup(bean.getSecurityGroup().getId());
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
