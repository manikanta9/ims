package com.clifton.portal.security;


import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class PortalUserDetailsServiceImpl implements UserDetailsService {

	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails details = getPortalSecurityUserService().getPortalSecurityUserByUserName(username, false);
		if (details == null) {
			throw new UsernameNotFoundException(username);
		}
		return details;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
