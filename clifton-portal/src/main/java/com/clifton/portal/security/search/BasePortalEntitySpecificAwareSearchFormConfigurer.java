package com.clifton.portal.security.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;

import java.util.List;


/**
 * @author manderson
 */
public abstract class BasePortalEntitySpecificAwareSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final PortalEntitySpecificAwareSearchForm portalEntitySpecificAwareSearchForm;

	private final PortalEntityService portalEntityService;

	private final PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BasePortalEntitySpecificAwareSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm);

		this.portalEntitySpecificAwareSearchForm = (PortalEntitySpecificAwareSearchForm) searchForm;
		this.portalSecurityUserService = portalSecurityUserService;
		this.portalEntityService = portalEntityService;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		if (currentUser == null) {
			configureCriteriaForUser(criteria, (short) 0); // No User - so No Access
		}
		else if (currentUser.getUserRole().isPortalEntitySpecific()) {
			configureCriteriaForUser(criteria, currentUser.getId());
		}
		else {
			// View As Functionality Would only pass one or the other, but for the search forms we'll support both
			if (getPortalEntitySpecificAwareSearchForm().getViewAsUserId() != null) {
				configureCriteriaForUser(criteria, getPortalEntitySpecificAwareSearchForm().getViewAsUserId());
			}
			if (getPortalEntitySpecificAwareSearchForm().getViewAsEntityId() != null) {
				// Get this entity id, all parents, and all children - active only
				List<PortalEntity> entityList = getPortalEntityService().getRelatedPortalEntityList(getPortalEntitySpecificAwareSearchForm().getViewAsEntityId(), true, true, true, true);
				configureCriteriaForViewAsEntity(criteria, entityList);
			}
		}
	}


	public abstract void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId);


	/**
	 * viewAsEntityId passed in the search form is already converted to the hierarchical list of portal entity ids
	 */
	public abstract void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public PortalEntitySpecificAwareSearchForm getPortalEntitySpecificAwareSearchForm() {
		return this.portalEntitySpecificAwareSearchForm;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}
}
