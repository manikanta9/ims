package com.clifton.portal.security.user;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreStringUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceSearchForm;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildEvent;
import com.clifton.portal.security.user.group.PortalSecurityGroupResource;
import com.clifton.portal.security.user.group.PortalSecurityUserGroup;
import com.clifton.portal.security.user.group.PortalSecurityUserGroupService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchFormConfigurer;
import com.clifton.portal.security.user.search.PortalSecurityUserRoleSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalSecurityUserServiceImpl implements PortalSecurityUserService {

	private AdvancedUpdatableDAO<PortalSecurityUser, Criteria> portalSecurityUserDAO;
	private AdvancedUpdatableDAO<PortalSecurityUserAssignment, Criteria> portalSecurityUserAssignmentDAO;
	private AdvancedUpdatableDAO<PortalSecurityUserAssignmentResource, Criteria> portalSecurityUserAssignmentResourceDAO;

	private AdvancedUpdatableDAO<PortalSecurityUserRole, Criteria> portalSecurityUserRoleDAO;
	private AdvancedUpdatableDAO<PortalSecurityUserRoleResourceMapping, Criteria> portalSecurityUserRoleResourceMappingDAO;

	private ContextHandler contextHandler;

	private DaoCompositeKeyCache<PortalSecurityUserAssignment, Short, Integer> portalSecurityUserAssignmentByUserAndEntityCache;
	private DaoNamedEntityCache<PortalSecurityUserRole> portalSecurityUserRoleCache;
	private DaoSingleKeyCache<PortalSecurityUser, String> portalSecurityUserByUsernameCache;
	private DaoSingleKeyListCache<PortalSecurityUserAssignment, Short> portalSecurityUserAssignmentListByUserCache;
	private DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Short> portalSecurityUserAssignmentResourceListByUserCache;
	private DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> portalSecurityUserAssignmentResourceListByUserAssignmentCache;
	private DaoSingleKeyListCache<PortalSecurityUserRoleResourceMapping, Short> portalSecurityUserRoleResourceMappingListByRoleCache;

	private PortalEntityService portalEntityService;
	// Circular Dependency
	private PortalFileSetupService portalFileSetupService;
	private PortalSecurityResourceService portalSecurityResourceService;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;
	private PortalSecurityUserGroupService portalSecurityUserGroupService;

	// Loaded From Properties File
	private Short portalMaxInvalidLoginCount;


	private EventHandler eventHandler;


	////////////////////////////////////////////////////////////////////////////////
	///////////            Portal Security User Role Methods            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityUserRole getPortalSecurityUserRole(short id) {
		return getPortalSecurityUserRoleDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalSecurityUserRole getPortalSecurityUserRoleExpanded(short id) {
		PortalSecurityUserRole role = getPortalSecurityUserRoleDAO().findByPrimaryKey(id);
		role.setRoleResourceMappingList(getPortalSecurityUserRoleResourceMappingListForRole(id, true));
		return role;
	}


	@Override
	public PortalSecurityUserRole getPortalSecurityUserRoleByName(String name) {
		return getPortalSecurityUserRoleCache().getBeanForKeyValueStrict(getPortalSecurityUserRoleDAO(), name);
	}


	@Override
	public PortalSecurityUserRole savePortalSecurityUserRole(PortalSecurityUserRole bean) {
		if (!bean.isNewBean()) {
			PortalSecurityUserRole originalBean = getPortalSecurityUserRole(bean.getId());
			ValidationUtils.assertTrue(bean.isAdministrator() == originalBean.isAdministrator(), "Administrator flag cannot be changed.");

			// If it's not an insert and assignment role only changes to true - need to confirm the change is OK
			if (!originalBean.isAssignmentRoleOnly() && bean.isAssignmentRoleOnly()) {
				if (!CollectionUtils.isEmpty(getPortalSecurityUserDAO().findByField("userRole.id", bean.getId()))) {
					throw new ValidationException("User Role [" + bean.getName() + "] cannot be changed to assignments only, because there are existing users under that role.");
				}
			}

			// If it's not an insert and view type changes (not blank now) - need to confirm the change is OK
			if (!CompareUtils.isEqual(originalBean.getEntityViewType(), bean.getEntityViewType()) && bean.getEntityViewType() != null) {
				PortalSecurityUserAssignmentSearchForm securityUserAssignmentSearchForm = new PortalSecurityUserAssignmentSearchForm();
				securityUserAssignmentSearchForm.setEntityViewTypeIdNotEquals(bean.getEntityViewType().getId());
				securityUserAssignmentSearchForm.setAssignmentRoleId(bean.getId());
				if (!CollectionUtils.isEmpty(getPortalSecurityUserAssignmentList(securityUserAssignmentSearchForm, false))) {
					throw new ValidationException("User Role [" + bean.getName() + "] cannot be changed to entity view type [" + bean.getEntityViewType().getName() + "], because there are existing user assignments under that role that do not apply to the view type.");
				}
			}
		}
		if (bean.isAssignmentRoleOnly()) {
			ValidationUtils.assertTrue(bean.isPortalEntitySpecific(), "Assignment Role Only applies only to Portal Entity Specific Roles.");
		}

		if (bean.getEntityViewType() != null) {
			ValidationUtils.assertTrue(bean.isPortalEntitySpecific(), "Entity View Type applies only to Portal Entity Specific Roles.");
		}

		return getPortalSecurityUserRoleDAO().save(bean);
	}


	@Override
	public PortalSecurityUserRole savePortalSecurityUserRoleExpanded(PortalSecurityUserRole bean) {
		bean = savePortalSecurityUserRoleExpandedImpl(bean);

		// WARNING: Must clear the role from the resource to prevent infinite loop
		// This must be done outside of the transaction to prevent hibernate from pushing the change
		for (PortalSecurityUserRoleResourceMapping resourceMapping : CollectionUtils.getIterable(bean.getRoleResourceMappingList())) {
			resourceMapping.setSecurityUserRole(null);
		}
		return bean;
	}


	@Transactional
	protected PortalSecurityUserRole savePortalSecurityUserRoleExpandedImpl(PortalSecurityUserRole bean) {
		List<PortalSecurityUserRoleResourceMapping> resourceMappingList = bean.getRoleResourceMappingList();
		bean.setRoleResourceMappingList(null);
		bean = savePortalSecurityUserRole(bean);
		bean.setRoleResourceMappingList(savePortalSecurityUserRoleResourceMappingListForRole(bean, resourceMappingList));
		return bean;
	}


	@Override
	public List<PortalSecurityUserRole> getPortalSecurityUserRoleList(PortalSecurityUserRoleSearchForm searchForm) {
		// Limit to current user being portal entity specific or not
		PortalSecurityUser currentUser = getPortalSecurityUserCurrent();
		if (currentUser == null || currentUser.getUserRole().isPortalEntitySpecific()) {
			searchForm.setPortalEntitySpecific(true);
		}

		if (searchForm.getAssignmentForPortalEntityId() != null) {
			PortalEntity portalEntity = getPortalEntityService().getPortalEntity(searchForm.getAssignmentForPortalEntityId());
			if (portalEntity.getEntityViewType() != null) {
				searchForm.setEntityViewTypeIdOrNull(portalEntity.getEntityViewType().getId());
			}
		}

		return getPortalSecurityUserRoleDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////////
	///////       Portal Security User Role Resource Mapping Methods       /////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalSecurityUserRoleResourceMapping> getPortalSecurityUserRoleResourceMappingListForRole(short roleId, boolean expandSecurity) {
		List<PortalSecurityUserRoleResourceMapping> mappingList = getPortalSecurityUserRoleResourceMappingListByRoleCache().getBeanListForKeyValue(getPortalSecurityUserRoleResourceMappingDAO(), roleId);
		if (expandSecurity) {
			short counter = 0;
			Map<Short, PortalSecurityUserRoleResourceMapping> resourceMap = BeanUtils.getBeanMap(mappingList, roleResourceMapping -> roleResourceMapping.getSecurityResource().getId());
			List<PortalSecurityResource> resourceList = getPortalSecurityResourceService().getPortalSecurityResourceList(new PortalSecurityResourceSearchForm());

			List<PortalSecurityUserRoleResourceMapping> newResourceMappingList = new ArrayList<>();
			for (PortalSecurityResource resource : CollectionUtils.getIterable(resourceList)) {
				PortalSecurityUserRoleResourceMapping roleResourceMapping = resourceMap.get(resource.getId());
				if (roleResourceMapping == null) {
					counter = (short) (counter - 10);
					roleResourceMapping = new PortalSecurityUserRoleResourceMapping();
					roleResourceMapping.setId(counter);
					roleResourceMapping.setSecurityResource(resource);
					roleResourceMapping.setAccessAllowed(false);
					// If parent is mapped - give it the parent access
					if (resource.getParent() != null && resourceMap.get(resource.getParent().getId()) != null) {
						roleResourceMapping.setAccessAllowed(resourceMap.get(resource.getParent().getId()).isAccessAllowed());
					}
				}
				newResourceMappingList.add(roleResourceMapping);
			}
			return newResourceMappingList;
		}
		return mappingList;
	}


	private List<PortalSecurityUserRoleResourceMapping> savePortalSecurityUserRoleResourceMappingListForRole(PortalSecurityUserRole role, List<PortalSecurityUserRoleResourceMapping> resourceMappingList) {
		List<PortalSecurityUserRoleResourceMapping> existingList = getPortalSecurityUserRoleResourceMappingListForRole(role.getId(), false);
		if (!role.isApplyRoleResourceMapping() || CollectionUtils.isEmpty(resourceMappingList)) {
			getPortalSecurityUserRoleResourceMappingDAO().deleteList(existingList);
		}
		else {
			List<PortalSecurityUserRoleResourceMapping> saveList = new ArrayList<>();
			Map<Short, PortalSecurityUserRoleResourceMapping> resourceMap = BeanUtils.getBeanMap(resourceMappingList, resourceMapping -> resourceMapping.getSecurityResource().getId());

			for (PortalSecurityUserRoleResourceMapping resourceMapping : CollectionUtils.getIterable(resourceMappingList)) {
				// If there is a parent - check it - if it's the same, we just save the parent level
				if (resourceMapping.getSecurityResource().getParent() != null) {
					PortalSecurityUserRoleResourceMapping parentResourceMapping = resourceMap.get(resourceMapping.getSecurityResource().getParent().getId());
					if (parentResourceMapping != null && parentResourceMapping.isAccessAllowed() == resourceMapping.isAccessAllowed()) {
						continue;
					}
					saveList.add(resourceMapping);
				}
				// Otherwise only save if true
				else if (resourceMapping.isAccessAllowed()) {
					saveList.add(resourceMapping);
				}
			}

			for (PortalSecurityUserRoleResourceMapping resourceMapping : CollectionUtils.getIterable(saveList)) {
				resourceMapping.setSecurityUserRole(role);
				// When we populate the default list we set ids to -10, -20, etc.  On save we need to clear that so that it's properly inserted.
				if (resourceMapping.getId() != null && resourceMapping.getId() <= 0) {
					resourceMapping.setId(null);
				}
			}
			getPortalSecurityUserRoleResourceMappingDAO().saveList(saveList, existingList);
			// Rebuild Security Expanded Table for All Users of Specified Role
			getEventHandler().raiseEvent(PortalSecurityUserAssignmentExpandedRebuildEvent.ofPortalSecurityUserRoleResourceChange(role.getId()));
		}
		return getPortalSecurityUserRoleResourceMappingListForRole(role.getId(), true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Portal Security User Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityUser getPortalSecurityUserByUserName(String username, boolean validateEmail) {
		PortalSecurityUser securityUser = getPortalSecurityUserByUsernameCache().getBeanForKeyValue(getPortalSecurityUserDAO(), username);
		if (securityUser == null && validateEmail) {
			validatePortalSecurityUserName(username);
		}
		return securityUser;
	}


	@Override
	public PortalSecurityUser getPortalSecurityUserByPasswordResetKey(String passwordResetKey) {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setPasswordResetKey(passwordResetKey);
		searchForm.setPasswordResetKeyExpirationDateAndTimeGreaterThanOrEqual(new Date());
		return CollectionUtils.getOnlyElement(getPortalSecurityUserDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm)));
	}


	@Override
	public PortalSecurityUser getPortalSecurityUserByUserNameAndEntityValidateAssignment(String username, Integer entityId, boolean validateEmail) {
		PortalSecurityUser securityUser = getPortalSecurityUserByUsernameCache().getBeanForKeyValue(getPortalSecurityUserDAO(), username);
		if (securityUser == null && validateEmail) {
			validatePortalSecurityUserName(username);
		}
		if (securityUser != null) {
			PortalSecurityUserAssignment securityUserAssignment = getPortalSecurityUserAssignmentByUserAndEntity(securityUser.getId(), entityId, false);
			if (securityUserAssignment != null && !securityUserAssignment.isDisabled()) {
				throw new ValidationException(String.format("User %s is already assigned to %s.", username, securityUserAssignment.getPortalEntity().getLabel()));
			}
		}

		return securityUser;
	}


	@Override
	public PortalSecurityUser getPortalSecurityUser(short id) {
		PortalSecurityUser user = getPortalSecurityUserDAO().findByPrimaryKey(id);
		if (user != null) {
			PortalSecurityUserAssignmentRetrievalContext retrievalContext = new PortalSecurityUserAssignmentRetrievalContext();
			user.setUserAssignmentResourceList(getPortalSecurityUserAssignmentResourceListForUser(user, true, retrievalContext));
		}
		return user;
	}


	protected void validatePortalSecurityUserName(String username) {
		if (!CoreStringUtils.isEmailValid(username)) {
			throw new ValidationException("Supplied User Name [" + username + "] is not a valid e-mail address.");
		}
	}


	@Override
	public PortalSecurityUser getPortalSecurityUserCurrent() {
		// the user is put in the Context by PortalSecurityUserManagementFilter
		return (PortalSecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
	}


	@Override
	public boolean isPortalSecurityUserSecurityAdmin(PortalSecurityUser securityUser) {
		if (securityUser.getUserRole().isPortalSecurityAdministrator()) {
			return true;
		}
		if (securityUser.getUserRole().isPortalEntitySpecific()) {
			List<PortalSecurityUserAssignment> assignmentList = getPortalSecurityUserAssignmentListForUser(securityUser.getId());
			for (PortalSecurityUserAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
				if (!assignment.isDisabled() && assignment.getAssignmentRole().isPortalSecurityAdministrator()) {
					return true;
				}
			}
		}
		return false;
	}


	@Override
	public boolean isPortalSecurityUserPendingTermination(PortalSecurityUser securityUser) {
		if (securityUser.getUserRole().isPortalEntitySpecific()) {
			List<PortalSecurityUserAssignment> assignmentList = getPortalSecurityUserAssignmentListForUser(securityUser.getId());
			for (PortalSecurityUserAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
				if (!assignment.isPendingTermination()) {
					return false;
				}
			}
			return true;
		}
		return false;
	}


	@Override
	public List<PortalSecurityUser> getPortalSecurityUserList(final PortalSecurityUserSearchForm searchForm, boolean populateResourcePermissionMap) {
		PortalSecurityUserAssignmentRetrievalContext retrievalContext = new PortalSecurityUserAssignmentRetrievalContext();

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// Custom Search Field: Password Reset is Required if No Source System and (No Password Update Date or it's over a year since password has been updated)
				Boolean passwordResetRequired = searchForm.getPasswordResetRequired();
				if (passwordResetRequired == null && searchForm.containsSearchRestriction("passwordResetRequired")) {
					String resetString = (String) searchForm.getSearchRestriction("passwordResetRequired").getValue();
					if (!StringUtils.isEmpty(resetString)) {
						passwordResetRequired = Boolean.parseBoolean(resetString);
					}
				}

				if (passwordResetRequired != null) {
					if (BooleanUtils.isTrue(passwordResetRequired)) {
						criteria.add(Restrictions.and(Restrictions.isNull("portalEntitySourceSystem.id"), Restrictions.or(Restrictions.isNull("passwordUpdateDate"), Restrictions.lt("passwordUpdateDate", DateUtils.addYears(new Date(), -1)))));
					}
					else {
						criteria.add(Restrictions.or(Restrictions.isNotNull("portalEntitySourceSystem.id"), Restrictions.ge("passwordUpdateDate", DateUtils.addYears(new Date(), -1))));
					}
				}

				Object[] idsOrAdmin = searchForm.getIdsOrAdmin();
				if (ArrayUtils.getLength(idsOrAdmin) > 0) {
					String roleAlias = getPathAlias("userRole", criteria);
					criteria.add(Restrictions.or(Restrictions.in("id", idsOrAdmin), Restrictions.eq(roleAlias + ".administrator", true)));
				}

				if (searchForm.getAssignmentRolePortalSecurityAdministrator() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignment.class, "a");
					sub.setProjection(Projections.property("id"));
					// add these to search sub properties
					sub.createAlias("assignmentRole", "ar");
					sub.add(Restrictions.eq("ar.portalSecurityAdministrator", searchForm.getAssignmentRolePortalSecurityAdministrator()));
					sub.add(Restrictions.eqProperty("securityUser.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}

				if (searchForm.getActiveAssignmentsExist() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignment.class, "a");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.or(Restrictions.isNull("disabledDate"), Restrictions.gt("disabledDate", DateUtils.clearTime(new Date()))));
					sub.add(Restrictions.eqProperty("securityUser.id", criteria.getAlias() + ".id"));
					if (BooleanUtils.isTrue(searchForm.getActiveAssignmentsExist())) {
						criteria.add(Subqueries.exists(sub));
					}
					else {
						criteria.add(Subqueries.notExists(sub));
					}
				}

				// Custom Search Field where exists in assignment expanded table
				// Used ONLY to display list of users with access to a security resource for GLOBAL files
				if (searchForm.getAssignmentAccessForPortalSecurityResourceId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "uae");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eq("portalSecurityResourceId", searchForm.getAssignmentAccessForPortalSecurityResourceId()));
					sub.add(Restrictions.eqProperty("portalSecurityUserId", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}


				// Custom Search Field where exists in assignment table where user is assigned to an entity of the specified view type
				if (searchForm.getAssignmentPortalEntityViewTypeId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignment.class, "a");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("portalEntity", "ape");
					sub.add(Restrictions.eq("ape.entityViewType.id", searchForm.getAssignmentPortalEntityViewTypeId()));
					sub.add(Restrictions.eqProperty("securityUser.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}
			}


			@Override
			protected String getOrderByFieldName(OrderByField field, Criteria criteria) {
				String result = field.getName();
				if ("passwordResetRequired".equals(result)) {
					return "passwordUpdateDate";
				}
				return super.getOrderByFieldName(field, criteria);
			}
		};
		List<PortalSecurityUser> result = getPortalSecurityUserDAO().findBySearchCriteria(searchConfig);
		if (populateResourcePermissionMap) {
			for (PortalSecurityUser user : CollectionUtils.getIterable(result)) {
				user.setUserAssignmentResourceList(getPortalSecurityUserAssignmentResourceListForUser(user, true, retrievalContext));
			}
		}
		return result;
	}


	@Override
	public PortalSecurityUser savePortalSecurityUser(PortalSecurityUser bean) {
		ValidationUtils.assertNotNull(bean.getUserRole(), "User Role is required.");
		ValidationUtils.assertFalse(bean.getUserRole().isAssignmentRoleOnly(), "User Role [" + bean.getUserRole().getName() + "] is only allowed on assignments, not users.");
		if (!bean.isAccountLocked() && MathUtils.isGreaterThanOrEqual(bean.getInvalidLoginAttemptCount(), getPortalMaxInvalidLoginCount())) {
			bean.setAccountLocked(true);
		}
		return getPortalSecurityUserDAO().save(bean);
	}


	/**
	 * Will be called from IMS Batch Job where run as user = systemuser who will be an admin in portal as well
	 */
	@Override
	public PortalSecurityUser savePortalSecurityUserFromSourceSystem(PortalSecurityUser bean) {
		ValidationUtils.assertNotNull(bean.getPortalEntitySourceSystem(), "Source System is required");
		ValidationUtils.assertNotNull(bean.getSourceFKFieldId(), "Source FK Field ID is required");
		return getPortalSecurityUserDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////           Portal Security User Assignment Methods            /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityUserAssignment getPortalSecurityUserAssignment(int id) {
		PortalSecurityUserAssignment bean = getPortalSecurityUserAssignmentDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setUserAssignmentResourceList(getPortalSecurityUserAssignmentResourceListForAssignment(bean, true, null));
		}
		return bean;
	}


	@Override
	public PortalSecurityUserAssignment getPortalSecurityUserAssignmentByUserAndEntity(short userId, int entityId, boolean checkParents) {
		if (checkParents) {
			return getMostSpecificPortalSecurityUserAssignmentByUserAndEntity(getPortalSecurityUser(userId), getPortalEntityService().getPortalEntity(entityId), false);
		}
		return getPortalSecurityUserAssignmentByUserAndEntityCache().getBeanForKeyValues(getPortalSecurityUserAssignmentDAO(), userId, entityId);
	}


	private PortalSecurityUserAssignment getMostSpecificPortalSecurityUserAssignmentByUserAndEntity(PortalSecurityUser securityUser, PortalEntity portalEntity, boolean securityAdministratorOnly) {
		PortalSecurityUserAssignment assignment = getPortalSecurityUserAssignmentByUserAndEntityCache().getBeanForKeyValues(getPortalSecurityUserAssignmentDAO(), securityUser.getId(), portalEntity.getId());
		if (assignment != null && !assignment.isDisabled() && (!securityAdministratorOnly || assignment.getAssignmentRole().isPortalSecurityAdministrator())) {
			return assignment;
		}
		if (portalEntity.getParentPortalEntity() != null) {
			return getMostSpecificPortalSecurityUserAssignmentByUserAndEntity(securityUser, portalEntity.getParentPortalEntity(), securityAdministratorOnly);
		}
		return null;
	}


	@Override
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentListForUser(short userId) {
		return getPortalSecurityUserAssignmentListByUserCache().getBeanListForKeyValue(getPortalSecurityUserAssignmentDAO(), userId);
	}


	@Override
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentAdministratorListForUser(PortalSecurityUser bean, String orderBy) {
		List<PortalSecurityUserAssignment> userAssignmentList = getPortalSecurityUserAssignmentListForUser(bean.getId());

		PortalSecurityUserAssignmentSearchForm adminAssignmentSearchForm = new PortalSecurityUserAssignmentSearchForm();
		Integer[] relatedEntityIds = BeanUtils.getBeanIdentityArray(CollectionUtils.getConverted(userAssignmentList, PortalSecurityUserAssignment::getPortalEntity), Integer.class);
		adminAssignmentSearchForm.setPortalEntityIdsOrRelatedEntities(relatedEntityIds);
		adminAssignmentSearchForm.setAssignmentRolePortalSecurityAdministrator(true);
		adminAssignmentSearchForm.setDisabled(false);
		adminAssignmentSearchForm.setOrderBy(orderBy);
		return getPortalSecurityUserAssignmentList(adminAssignmentSearchForm, false);
	}


	@Override
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentList(PortalSecurityUserAssignmentSearchForm searchForm, boolean populateResourcePermissionMap) {
		return getPortalSecurityUserAssignmentListWithContext(searchForm, populateResourcePermissionMap, new PortalSecurityUserAssignmentRetrievalContext());
	}


	@Override
	public List<PortalSecurityUserAssignment> getPortalSecurityUserAssignmentListWithContext(PortalSecurityUserAssignmentSearchForm searchForm, boolean populateResourcePermissionMap, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		Integer[] portalEntityIdOrRelatedEntityIds = {searchForm.getPortalEntityIdOrRelatedEntity()};
		Integer[] relatedEntityIds = ArrayUtils.getStream(searchForm.getPortalEntityIdsOrRelatedEntities(), portalEntityIdOrRelatedEntityIds)
				// Flatten to array of query parameter IDs
				.flatMap(ArrayUtils::getStream)
				.filter(Objects::nonNull)
				// Get related IDs
				.map(entityId -> getPortalEntityService().getRelatedPortalEntityList(entityId, true, true, true, false))
				// Flatten to list of related entity IDs
				.flatMap(CollectionUtils::getStream)
				.map(BaseSimpleEntity::getId)
				.toArray(Integer[]::new);
		if (!ArrayUtils.isEmpty(relatedEntityIds)) {
			searchForm.setPortalEntityIds(relatedEntityIds);
		}
		List<PortalSecurityUserAssignment> result = getPortalSecurityUserAssignmentDAO().findBySearchCriteria(new PortalSecurityUserAssignmentSearchFormConfigurer(searchForm, this, getPortalEntityService()));

		if (populateResourcePermissionMap) {
			for (PortalSecurityUserAssignment userAssignment : CollectionUtils.getIterable(result)) {
				userAssignment.setUserAssignmentResourceList(getPortalSecurityUserAssignmentResourceListForAssignment(userAssignment, true, retrievalContext));
			}
		}
		return result;
	}


	@Override
	public PortalSecurityUserAssignment savePortalSecurityUserAssignment(PortalSecurityUserAssignment bean) {
		savePortalSecurityUserAssignmentImpl(bean);

		// Rebuild Expanded and check if any Welcome Emails need to be sent
		getEventHandler().raiseEvent(PortalSecurityUserChangeEvent.ofAssignmentChange(bean));

		// Need to retrieve the object again to properly retrieve user overrides to default roles, etc.
		PortalSecurityUserAssignment result = getPortalSecurityUserAssignment(bean.getId());
		// WARNING: Must clear the assignment from the resource to prevent infinite loop
		// This must be done outside of the transaction to prevent hibernate from pushing the change
		for (PortalSecurityUserAssignmentResource assignmentResource : CollectionUtils.getIterable(result.getUserAssignmentResourceList())) {
			assignmentResource.setSecurityUserAssignment(null);
		}
		return result;
	}


	@Override
	public PortalSecurityUserAssignment savePortalSecurityUserAssignmentBulk(PortalSecurityUserAssignment userAssignment, Integer[] additionalEntities) {
		if (ArrayUtils.isEmpty(additionalEntities)) {
			return savePortalSecurityUserAssignment(userAssignment);
		}

		List<PortalSecurityUserAssignment> saveList = savePortalSecurityUserAssignmentBulkImpl(userAssignment, additionalEntities);

		// Rebuild Expanded and check if any Welcome Emails need to be sent - call once for the user and the list of assignments
		getEventHandler().raiseEvent(PortalSecurityUserChangeEvent.ofAssignmentListChange(userAssignment.getSecurityUser(), saveList));

		// Need to retrieve the object again to properly retrieve user overrides to default roles, etc. -  user the original one, not the one for the additional entities
		PortalSecurityUserAssignment result = getPortalSecurityUserAssignment(userAssignment.getId());
		// WARNING: Must clear the assignment from the resource to prevent infinite loop
		// This must be done outside of the transaction to prevent hibernate from pushing the change
		for (PortalSecurityUserAssignmentResource assignmentResource : CollectionUtils.getIterable(result.getUserAssignmentResourceList())) {
			assignmentResource.setSecurityUserAssignment(null);
		}
		return result;
	}


	@Transactional
	protected List<PortalSecurityUserAssignment> savePortalSecurityUserAssignmentBulkImpl(PortalSecurityUserAssignment userAssignment, Integer[] additionalEntities) {
		List<PortalSecurityUserAssignment> saveList = new ArrayList<>();
		Set<Integer> portalEntityIdSet = CollectionUtils.createHashSet(userAssignment.getPortalEntity().getId());

		for (Integer entityId : additionalEntities) {
			if (portalEntityIdSet.contains(entityId)) {
				throw new ValidationException("Please review your additional entities selections. Found duplicate selection for " + getPortalEntityService().getPortalEntity(entityId).getEntityLabelWithSourceSection());
			}
			portalEntityIdSet.add(entityId);
			PortalSecurityUserAssignment copyUserAssignment = BeanUtils.cloneBean(userAssignment, false, false);
			copyUserAssignment.setPortalEntity(getPortalEntityService().getPortalEntity(entityId));
			savePortalSecurityUserAssignmentImpl(copyUserAssignment);
			saveList.add(copyUserAssignment);
		}
		savePortalSecurityUserAssignmentImpl(userAssignment);
		saveList.add(userAssignment);
		return saveList;
	}


	@Transactional
	protected void savePortalSecurityUserAssignmentImpl(PortalSecurityUserAssignment assignment) {
		// Copy the New Resources if any to the original assignment so we can save them as well
		List<PortalSecurityUserAssignmentResource> resourceList = assignment.getUserAssignmentResourceList();
		assignment.setUserAssignmentResourceList(null);

		ValidationUtils.assertNotNull(assignment.getSecurityUser(), "Security User is required");
		ValidationUtils.assertNotNull(assignment.getPortalEntity(), "Portal Entity is required");
		ValidationUtils.assertTrue(assignment.getSecurityUser().getUserRole().isPortalEntitySpecific(), "User [" + assignment.getSecurityUser().getLabel() + "] is not available for assignments.");
		ValidationUtils.assertTrue(assignment.getPortalEntity().getEntitySourceSection().getEntityType().isSecurableEntity(), "Portal Entity [" + assignment.getPortalEntity().getEntityLabelWithSourceSection() + "] is not available for assignments");
		ValidationUtils.assertNotNull(assignment.getAssignmentRole(), "Assignment Role is required");
		ValidationUtils.assertTrue(assignment.getAssignmentRole().isPortalEntitySpecific(), "Assignment Role [" + assignment.getAssignmentRole().getName() + "] is not allowed on assignments.");
		if (assignment.getAssignmentRole().getEntityViewType() != null) {
			ValidationUtils.assertTrue(CompareUtils.isEqual(assignment.getPortalEntity().getEntityViewType(), assignment.getAssignmentRole().getEntityViewType()), "Assignment Role [" + assignment.getAssignmentRole().getName() + "] does not apply to [" + assignment.getPortalEntity().getEntityLabelWithSourceSection() + "].");
		}

		PortalSecurityUserAssignment originalAssignment = (assignment.isNewBean() ? null : getPortalSecurityUserAssignmentDAO().findByPrimaryKey(assignment.getId()));
		validatePortalSecurityUserAssignmentAccess(assignment);
		if (originalAssignment != null) {
			ValidationUtils.assertTrue(assignment.getSecurityUser().equals(originalAssignment.getSecurityUser()), "You cannot change the user for an assignment.");
			ValidationUtils.assertTrue(assignment.getPortalEntity().equals(originalAssignment.getPortalEntity()), "You cannot change the entity for an assignment.");
		}
		else {
			ValidationUtils.assertNull(assignment.getDisabledDate(), "You cannot add an assignment that is disabled.");

			// If it's an insert - see if there is existing assignment
			PortalSecurityUserAssignment sameAssignment = getPortalSecurityUserAssignmentByUserAndEntity(assignment.getSecurityUser().getId(), assignment.getPortalEntity().getId(), false);
			// If there is, but it's disabled - we are going to re-use that assignment
			if (sameAssignment != null) {
				if (sameAssignment.isDisabled()) {
					// Put new role on the existing assignment and clear disabled fields
					sameAssignment.setAssignmentRole(assignment.getAssignmentRole());
					sameAssignment.setDisabledDate(null);
					sameAssignment.setDisabledReason(null);
					assignment = sameAssignment;
				}
				else {
					throw new ValidationException("There already existing an assignment for user [" + assignment.getSecurityUser().getLabel() + "] and [" + assignment.getPortalEntity().getEntityLabelWithSourceSection() + "].");
				}
			}
		}
		// If the assignment is not disabled, but the user is - re-enable the user
		if (!assignment.isDisabled() && assignment.getSecurityUser().isDisabled()) {
			assignment.getSecurityUser().setDisabled(false);
			getPortalSecurityUserDAO().save(assignment.getSecurityUser());
		}
		assignment = getPortalSecurityUserAssignmentDAO().save(assignment);
		// Save the Assignment Resources - Note: for new assignments this may be empty which is fine (we just ignore the save step).  Existing assignments should always have the list populated
		// The new bean difference is because Client UI screen merged the resource list population in the initial creation of the assignment to skip an additional step for the user - portal admin still has it separate
		if (!assignment.isNewBean() || !CollectionUtils.isEmpty(resourceList)) {
			savePortalSecurityUserAssignmentResourceListForUserAssignment(assignment, resourceList);
		}
	}


	protected void validatePortalSecurityUserAssignmentAccess(PortalSecurityUserAssignment assignment) {
		PortalSecurityUser currentUser = getPortalSecurityUserCurrent();
		if (!currentUser.getUserRole().isPortalEntitySpecific()) {
			// Because of method level permissions, this should never happen
			if (!currentUser.getUserRole().isPortalSecurityAdministrator()) {
				throw new ValidationException("You do not have access to security administrative functions.");
			}
		}
		else {
			// Get the assignment for the portal entity
			PortalSecurityUserAssignment currentUserAssignment = getMostSpecificPortalSecurityUserAssignmentByUserAndEntity(currentUser, assignment.getPortalEntity(), true);
			if (currentUserAssignment == null) {
				throw new ValidationException("You do not have access to security administrative functions for portal entity " + assignment.getPortalEntity().getId());
			}
			// If the assignment is what is used for the current user to give them permissions to edit the assignment and they are removing that permission - deny
			if (assignment.equals(currentUserAssignment) && !assignment.getAssignmentRole().isPortalSecurityAdministrator()) {
				throw new ValidationException("You cannot remove security administrative functions for yourself to " + assignment.getPortalEntity().getEntityLabelWithSourceSection());
			}
		}
	}


	@Override
	public void deletePortalSecurityUserAssignment(int id) {
		PortalSecurityUserAssignment assignment = getPortalSecurityUserAssignment(id);
		if (assignment != null) {
			PortalSecurityUser user = assignment.getSecurityUser();

			deletePortalSecurityUserAssignmentImpl(id);

			// Rebuild Expanded for user this assignment was associated with
			getEventHandler().raiseEvent(PortalSecurityUserAssignmentExpandedRebuildEvent.ofPortalSecurityUserChange(user));
		}
	}


	@Transactional
	protected void deletePortalSecurityUserAssignmentImpl(int id) {
		getPortalSecurityUserAssignmentResourceDAO().deleteList(getPortalSecurityUserAssignmentResourceDAO().findByField("securityUserAssignment.id", id));
		getPortalSecurityUserAssignmentDAO().delete(id);
	}


	@Override
	public void processPortalSecurityUserAssignmentDisabledList() {
		PortalSecurityUserAssignmentSearchForm searchForm = new PortalSecurityUserAssignmentSearchForm();
		searchForm.setDisabled(false);
		List<PortalSecurityUserAssignment> assignmentList = getPortalSecurityUserAssignmentList(searchForm, false);
		for (PortalSecurityUserAssignment userAssignment : CollectionUtils.getIterable(assignmentList)) {
			// If Portal Entity is Deleted (Don't think that is possible to have an assignment and then delete an entity)
			// No Longer Active (terminated) - Note: Confirms that end date set as on-boarding it could be possible to post date the start???
			if (userAssignment.getPortalEntity().isDeleted() || (userAssignment.getPortalEntity().getEndDate() != null && !userAssignment.getPortalEntity().isActive())) {
				userAssignment.setDisabledDate(userAssignment.getPortalEntity().getEndDate());
				userAssignment.setDisabledReason("Terminated");
				savePortalSecurityUserAssignment(userAssignment);
				// NOTE: Saving the assignment will trigger expanded rebuild and notification subscriptions rebuild
				// And if it's the last assignment for the user, the user will also be automatically disabled
			}
		}

		// Now find all users that are not disabled, but don't have any active assignments and disable them
		PortalSecurityUserSearchForm userSearchForm = new PortalSecurityUserSearchForm();
		userSearchForm.setUserRolePortalEntitySpecific(true);
		userSearchForm.setDisabled(false);
		userSearchForm.setActiveAssignmentsExist(false);

		List<PortalSecurityUser> userList = getPortalSecurityUserList(userSearchForm, false);
		for (PortalSecurityUser user : CollectionUtils.getIterable(userList)) {
			user.setDisabled(true);
			savePortalSecurityUser(user);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////         Portal Security User Assignment Resource Methods       ////////
	////////                           OUR USERS                            ////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForUser(PortalSecurityUser securityUser, boolean expandSecurity, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		if (securityUser != null && !securityUser.getUserRole().isPortalEntitySpecific() && !securityUser.getUserRole().isAdministrator()) {
			List<PortalSecurityUserAssignmentResource> userResourceList = getPortalSecurityUserAssignmentResourceListByUserCache().getBeanListForKeyValue(getPortalSecurityUserAssignmentResourceDAO(), securityUser.getId());
			if (expandSecurity) {
				return getPortalSecurityUserAssignmentResourceListImpl(securityUser, null, userResourceList, retrievalContext);
			}
			return userResourceList;
		}
		return null;
	}


	@Override
	public void rebuildPortalSecurityUserAssignmentResourceList() {
		PortalSecurityUserSearchForm securityUserSearchForm = new PortalSecurityUserSearchForm();
		securityUserSearchForm.setUserRolePortalEntitySpecific(false);

		Map<Short, List<PortalSecurityGroupResource>> groupResourceListMap = new HashMap<>();
		List<PortalSecurityUserGroup> userGroupList = getPortalSecurityUserGroupService().getPortalSecurityUserGroupList();

		List<PortalSecurityUser> userList = getPortalSecurityUserList(securityUserSearchForm, true);
		for (PortalSecurityUser user : userList) {
			rebuildPortalSecurityUserResourceListForUserImpl(user, userGroupList, groupResourceListMap);
		}
	}


	@Override
	public void rebuildPortalSecurityUserAssignmentResourceListForUser(short securityUserId) {
		rebuildPortalSecurityUserResourceListForUserImpl(getPortalSecurityUser(securityUserId), getPortalSecurityUserGroupService().getPortalSecurityUserGroupListForUser(securityUserId), new HashMap<>());
	}


	@Override
	public void rebuildPortalSecurityUserAssignmentResourceListForGroup(short securityGroupId) {
		Map<Short, List<PortalSecurityGroupResource>> groupResourceListMap = new HashMap<>();
		List<PortalSecurityUserGroup> userGroupList = getPortalSecurityUserGroupService().getPortalSecurityUserGroupListForGroup(securityGroupId);
		for (PortalSecurityUserGroup userGroup : CollectionUtils.getIterable(userGroupList)) {
			rebuildPortalSecurityUserResourceListForUserImpl(getPortalSecurityUser(userGroup.getSecurityUser().getId()), userGroupList, groupResourceListMap);
		}
	}


	private void rebuildPortalSecurityUserResourceListForUserImpl(PortalSecurityUser securityUser, List<PortalSecurityUserGroup> userGroupList, Map<Short, List<PortalSecurityGroupResource>> groupResourceListMap) {
		if (securityUser.getUserRole().isPortalEntitySpecific()) {
			throw new ValidationException("You cannot change permissions at the user level for portal entity specific users.  Please open a specific assignment and edit permissions there.");
		}

		if (securityUser.getUserRole().isAdministrator() || securityUser.isDisabled()) {
			// If user is an admin or disabled, remove their permissions
			List<PortalSecurityUserAssignmentResource> userResourceList = getPortalSecurityUserAssignmentResourceListByUserCache().getBeanListForKeyValue(getPortalSecurityUserAssignmentResourceDAO(), securityUser.getId());
			getPortalSecurityUserAssignmentResourceDAO().deleteList(userResourceList);
			return;
		}


		List<PortalSecurityGroupResource> groupResourceList = new ArrayList<>();
		for (PortalSecurityUserGroup userGroup : CollectionUtils.getIterable(BeanUtils.filter(userGroupList, PortalSecurityUserGroup::getSecurityUser, securityUser))) {
			if (!groupResourceListMap.containsKey(userGroup.getSecurityGroup().getId())) {
				groupResourceListMap.put(userGroup.getSecurityGroup().getId(), getPortalSecurityUserGroupService().getPortalSecurityGroupResourceListForGroup(userGroup.getSecurityGroup().getId(), true));
			}
			groupResourceList.addAll(groupResourceListMap.get(userGroup.getSecurityGroup().getId()));
		}

		Map<Short, List<PortalSecurityGroupResource>> resourceGroupResourceListMap = BeanUtils.getBeansMap(groupResourceList, groupResource -> groupResource.getSecurityResource().getId());

		for (PortalSecurityUserAssignmentResource userAssignmentResource : securityUser.getUserAssignmentResourceList()) {
			userAssignmentResource.setAccessAllowed(isAccessAllowed(userAssignmentResource.getSecurityResource().getId(), resourceGroupResourceListMap));
		}
		savePortalSecurityUserAssignmentResourceListForUser(securityUser, securityUser.getUserAssignmentResourceList());
	}


	private boolean isAccessAllowed(short securityResourceId, Map<Short, List<PortalSecurityGroupResource>> resourceGroupResourceListMap) {
		List<PortalSecurityGroupResource> groupResourceList = resourceGroupResourceListMap.get(securityResourceId);
		return !CollectionUtils.isEmpty(BeanUtils.filter(groupResourceList, PortalSecurityGroupResource::isAccessAllowed));
	}


	private void savePortalSecurityUserAssignmentResourceListForUser(PortalSecurityUser securityUser, List<PortalSecurityUserAssignmentResource> userResourceList) {
		PortalSecurityUserAssignmentRetrievalContext retrievalContext = new PortalSecurityUserAssignmentRetrievalContext();
		List<PortalSecurityUserAssignmentResource> existingList = getPortalSecurityUserAssignmentResourceListForUser(securityUser, false, retrievalContext);
		List<PortalSecurityUserAssignmentResource> saveList = new ArrayList<>();
		Map<Short, PortalSecurityUserAssignmentResource> resourceMap = BeanUtils.getBeanMap(userResourceList, userAssignmentResource -> userAssignmentResource.getSecurityResource().getId());

		for (PortalSecurityUserAssignmentResource userAssignmentResource : CollectionUtils.getIterable(userResourceList)) {
			// If there is a parent - check it - if it's the same, we just save the parent level
			if (userAssignmentResource.getSecurityResource().getParent() != null) {
				PortalSecurityUserAssignmentResource parentUserAssignmentResource = resourceMap.get(userAssignmentResource.getSecurityResource().getParent().getId());
				if (parentUserAssignmentResource != null && parentUserAssignmentResource.isAccessAllowed() == userAssignmentResource.isAccessAllowed()) {
					continue;
				}
				saveList.add(userAssignmentResource);
			}
			// Otherwise only save if true
			else if (userAssignmentResource.isAccessAllowed()) {
				saveList.add(userAssignmentResource);
			}
		}

		for (PortalSecurityUserAssignmentResource userAssignmentResource : CollectionUtils.getIterable(saveList)) {
			userAssignmentResource.setSecurityUser(securityUser);
			// When we populate the default list we set ids to -10, -20, etc.  On save we need to clear that so that it's properly inserted.
			if (userAssignmentResource.getId() != null && userAssignmentResource.getId() <= 0) {
				userAssignmentResource.setId(null);
			}
		}
		getPortalSecurityUserAssignmentResourceDAO().saveList(saveList, existingList);

		// Rebuild Expanded
		getEventHandler().raiseEvent(PortalSecurityUserChangeEvent.ofSecurityResourceChange(securityUser));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////         Portal Security User Assignment Resource Methods       ////////
	////////                        EXTERNAL USERS                          ////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForAssignment(PortalSecurityUserAssignment bean, boolean expandSecurity, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		if (bean != null) {
			List<PortalSecurityUserAssignmentResource> userResourceList = (bean.isNewBean() ? null : getPortalSecurityUserAssignmentResourceListByUserAssignmentCache().getBeanListForKeyValue(getPortalSecurityUserAssignmentResourceDAO(), bean.getId()));
			if (expandSecurity) {
				return getPortalSecurityUserAssignmentResourceListImpl(bean.getSecurityUser(), bean, userResourceList, retrievalContext == null ? new PortalSecurityUserAssignmentRetrievalContext() : retrievalContext);
			}
			return userResourceList;
		}
		return null;
	}


	@Override
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForAssignmentAndRole(int userAssignmentId, short roleId) {
		PortalSecurityUserAssignment userAssignment = getPortalSecurityUserAssignment(userAssignmentId);
		userAssignment.setAssignmentRole(getPortalSecurityUserRole(roleId));
		return getPortalSecurityUserAssignmentResourceListForAssignment(userAssignment, true, null);
	}


	@Override
	public List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListForEntityAndRole(int portalEntityId, short roleId) {
		PortalSecurityUserAssignment userAssignment = new PortalSecurityUserAssignment();
		userAssignment.setAssignmentRole(getPortalSecurityUserRole(roleId));
		userAssignment.setPortalEntity(getPortalEntityService().getPortalEntity(portalEntityId));
		return getPortalSecurityUserAssignmentResourceListForAssignment(userAssignment, true, null);
	}


	private void savePortalSecurityUserAssignmentResourceListForUserAssignment(PortalSecurityUserAssignment assignment, List<PortalSecurityUserAssignmentResource> userAssignmentResourceList) {
		if (!assignment.getSecurityUser().getUserRole().isPortalEntitySpecific()) {
			throw new ValidationException("You cannot change permissions at the assignment level for non-portal entity specific users.  Please open the user and edit permissions there.");
		}
		List<PortalSecurityUserAssignmentResource> existingList = getPortalSecurityUserAssignmentResourceListForAssignment(assignment, false, null);
		List<PortalSecurityUserAssignmentResource> saveList = new ArrayList<>();
		Map<Short, PortalSecurityUserAssignmentResource> resourceMap = BeanUtils.getBeanMap(userAssignmentResourceList, userAssignmentResource -> userAssignmentResource.getSecurityResource().getId());

		for (PortalSecurityUserAssignmentResource userAssignmentResource : CollectionUtils.getIterable(userAssignmentResourceList)) {
			// When we populate the default list we set ids to -10, -20, etc.  On save we need to clear that so that it's properly inserted.
			if (userAssignmentResource.getId() != null && userAssignmentResource.getId() <= 0) {
				userAssignmentResource.setId(null);
			}
			if (userAssignmentResource.getId() == null) {
				// If it's an insert - check existing list first for that security resource to see if it's really an update
				// This occurs when re-enabling a previously disabled assignment.  The new resources are passed as inserts, but we really just want to update
				// the original existing list and delete/insert what is now different
				for (PortalSecurityUserAssignmentResource existing : CollectionUtils.getIterable(existingList)) {
					if (existing.getSecurityResource().equals(userAssignmentResource.getSecurityResource())) {
						existing.setAccessAllowed(userAssignmentResource.isAccessAllowed());
						userAssignmentResource = existing;
						break;
					}
				}
			}

			// If there is a parent - check it - if it's the same, we just save the parent level
			if (userAssignmentResource.getSecurityResource().getParent() != null) {
				PortalSecurityUserAssignmentResource parentUserAssignmentResource = resourceMap.get(userAssignmentResource.getSecurityResource().getParent().getId());
				if (parentUserAssignmentResource != null && parentUserAssignmentResource.isAccessAllowed() == userAssignmentResource.isAccessAllowed()) {
					continue;
				}
			}
			// Otherwise always save so the default on functionality doesn't override it
			userAssignmentResource.setSecurityUserAssignment(assignment);
			saveList.add(userAssignmentResource);
		}
		getPortalSecurityUserAssignmentResourceDAO().saveList(saveList, existingList);
	}


	private List<PortalSecurityUserAssignmentResource> getPortalSecurityUserAssignmentResourceListImpl(PortalSecurityUser securityUser, PortalSecurityUserAssignment assignment, List<PortalSecurityUserAssignmentResource> userResourceList, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		int counter = 0;
		Map<Short, PortalSecurityUserAssignmentResource> resourceMap = BeanUtils.getBeanMap(userResourceList, userAssignmentResource -> userAssignmentResource.getSecurityResource().getId());
		List<PortalFileCategory> categoryList = null;
		List<PortalSecurityResource> resourceList;

		// Limit Security Resources to those that the given assigned entity has approved files for
		if (assignment != null) {
			// Note: We Must Run the Search as System User because if the user takes permission off then they lose the ability to turn it back on as they no longer have access
			categoryList = retrievalContext.getPortalFileCategoryListForEntity(getPortalSecurityImpersonationHandler(), getPortalFileSetupService(), assignment.getPortalEntity().getId());
			if (CollectionUtils.isEmpty(categoryList)) {
				return null;
			}
			Short[] securityResourceIds = BeanUtils.getPropertyValues(categoryList, category -> category.getSecurityResource().getId(), Short.class);
			// Include parent security resources where not explicitly mapped
			securityResourceIds = ArrayUtils.addAll(securityResourceIds, BeanUtils.getPropertyValuesExcludeNull(categoryList, category -> (category.getSecurityResource().getParent() != null ? category.getSecurityResource().getParent().getId() : null), Short.class));
			resourceList = retrievalContext.getPortalSecurityResourceList(getPortalSecurityImpersonationHandler(), getPortalSecurityResourceService(), securityResourceIds);
		}
		else {
			resourceList = retrievalContext.getPortalSecurityResourceList(getPortalSecurityImpersonationHandler(), getPortalSecurityResourceService());
		}

		Map<Short, PortalSecurityUserRoleResourceMapping> roleResourceMap = null;
		PortalSecurityUserRole role = null;
		if (assignment != null) {
			role = assignment.getAssignmentRole();
		}
		else if (securityUser != null) {
			role = securityUser.getUserRole();
		}
		if (role != null && role.isApplyRoleResourceMapping()) {
			roleResourceMap = BeanUtils.getBeanMap(getPortalSecurityUserRoleResourceMappingListForRole(role.getId(), true), userRoleResourceMapping -> userRoleResourceMapping.getSecurityResource().getId());
		}


		List<PortalSecurityUserAssignmentResource> newUserResourceList = new ArrayList<>();
		for (PortalSecurityResource resource : CollectionUtils.getIterable(resourceList)) {
			// If role has specific resources limited, and doesn't allow that resource - skip it
			if (roleResourceMap != null) {
				PortalSecurityUserRoleResourceMapping roleResourceMapping = roleResourceMap.get(resource.getId());
				if (roleResourceMapping == null || !roleResourceMapping.isAccessAllowed()) {
					// Remove it from the user/assignment resource map
					resourceMap.remove(resource.getId());
					continue;
				}
			}
			PortalSecurityUserAssignmentResource userAssignmentResource = resourceMap.get(resource.getId());
			if (userAssignmentResource == null) {
				counter = counter - 10;
				userAssignmentResource = new PortalSecurityUserAssignmentResource();
				userAssignmentResource.setId(counter);
				userAssignmentResource.setSecurityResource(resource);
				userAssignmentResource.setAccessAllowed(false);
				// If parent is mapped - give it the parent access
				if (resource.getParent() != null && resourceMap.get(resource.getParent().getId()) != null) {
					userAssignmentResource.setAccessAllowed(resourceMap.get(resource.getParent().getId()).isAccessAllowed());
				}
				// Otherwise if for a specific entity and user is a security administrator - they get access by default
				// Or if it's for a new assignment - Give them access by default and users can deselect access where necessary
				// Or as long as security resource do not default access is UNCHECKED - give them access
				else if (assignment != null && (assignment.isNewBean() || assignment.getAssignmentRole().isPortalSecurityAdministrator() || !resource.isDoNotDefaultPortalEntitySpecificAccess())) {
					userAssignmentResource.setAccessAllowed(true);
				}
			}
			// If switching roles to a portal admin, turn security back on
			else if (assignment != null && assignment.getAssignmentRole().isPortalSecurityAdministrator()) {
				userAssignmentResource.setAccessAllowed(true);
			}
			userAssignmentResource.setFileCategoryList(BeanUtils.filter(categoryList, portalFileCategory -> portalFileCategory.getSecurityResource().getId(), resource.getId()));
			newUserResourceList.add(userAssignmentResource);
		}
		return newUserResourceList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalSecurityUser, Criteria> getPortalSecurityUserDAO() {
		return this.portalSecurityUserDAO;
	}


	public void setPortalSecurityUserDAO(AdvancedUpdatableDAO<PortalSecurityUser, Criteria> portalSecurityUserDAO) {
		this.portalSecurityUserDAO = portalSecurityUserDAO;
	}


	public AdvancedUpdatableDAO<PortalSecurityUserRole, Criteria> getPortalSecurityUserRoleDAO() {
		return this.portalSecurityUserRoleDAO;
	}


	public void setPortalSecurityUserRoleDAO(AdvancedUpdatableDAO<PortalSecurityUserRole, Criteria> portalSecurityUserRoleDAO) {
		this.portalSecurityUserRoleDAO = portalSecurityUserRoleDAO;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DaoSingleKeyCache<PortalSecurityUser, String> getPortalSecurityUserByUsernameCache() {
		return this.portalSecurityUserByUsernameCache;
	}


	public void setPortalSecurityUserByUsernameCache(DaoSingleKeyCache<PortalSecurityUser, String> portalSecurityUserByUsernameCache) {
		this.portalSecurityUserByUsernameCache = portalSecurityUserByUsernameCache;
	}


	public AdvancedUpdatableDAO<PortalSecurityUserAssignment, Criteria> getPortalSecurityUserAssignmentDAO() {
		return this.portalSecurityUserAssignmentDAO;
	}


	public void setPortalSecurityUserAssignmentDAO(AdvancedUpdatableDAO<PortalSecurityUserAssignment, Criteria> portalSecurityUserAssignmentDAO) {
		this.portalSecurityUserAssignmentDAO = portalSecurityUserAssignmentDAO;
	}


	public AdvancedUpdatableDAO<PortalSecurityUserAssignmentResource, Criteria> getPortalSecurityUserAssignmentResourceDAO() {
		return this.portalSecurityUserAssignmentResourceDAO;
	}


	public void setPortalSecurityUserAssignmentResourceDAO(AdvancedUpdatableDAO<PortalSecurityUserAssignmentResource, Criteria> portalSecurityUserAssignmentResourceDAO) {
		this.portalSecurityUserAssignmentResourceDAO = portalSecurityUserAssignmentResourceDAO;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public Short getPortalMaxInvalidLoginCount() {
		return this.portalMaxInvalidLoginCount;
	}


	public void setPortalMaxInvalidLoginCount(Short portalMaxInvalidLoginCount) {
		this.portalMaxInvalidLoginCount = portalMaxInvalidLoginCount;
	}


	public DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Short> getPortalSecurityUserAssignmentResourceListByUserCache() {
		return this.portalSecurityUserAssignmentResourceListByUserCache;
	}


	public void setPortalSecurityUserAssignmentResourceListByUserCache(DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Short> portalSecurityUserAssignmentResourceListByUserCache) {
		this.portalSecurityUserAssignmentResourceListByUserCache = portalSecurityUserAssignmentResourceListByUserCache;
	}


	public DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> getPortalSecurityUserAssignmentResourceListByUserAssignmentCache() {
		return this.portalSecurityUserAssignmentResourceListByUserAssignmentCache;
	}


	public void setPortalSecurityUserAssignmentResourceListByUserAssignmentCache(DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> portalSecurityUserAssignmentResourceListByUserAssignmentCache) {
		this.portalSecurityUserAssignmentResourceListByUserAssignmentCache = portalSecurityUserAssignmentResourceListByUserAssignmentCache;
	}


	public PortalSecurityResourceService getPortalSecurityResourceService() {
		return this.portalSecurityResourceService;
	}


	public void setPortalSecurityResourceService(PortalSecurityResourceService portalSecurityResourceService) {
		this.portalSecurityResourceService = portalSecurityResourceService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public DaoNamedEntityCache<PortalSecurityUserRole> getPortalSecurityUserRoleCache() {
		return this.portalSecurityUserRoleCache;
	}


	public void setPortalSecurityUserRoleCache(DaoNamedEntityCache<PortalSecurityUserRole> portalSecurityUserRoleCache) {
		this.portalSecurityUserRoleCache = portalSecurityUserRoleCache;
	}


	public DaoSingleKeyListCache<PortalSecurityUserAssignment, Short> getPortalSecurityUserAssignmentListByUserCache() {
		return this.portalSecurityUserAssignmentListByUserCache;
	}


	public void setPortalSecurityUserAssignmentListByUserCache(DaoSingleKeyListCache<PortalSecurityUserAssignment, Short> portalSecurityUserAssignmentListByUserCache) {
		this.portalSecurityUserAssignmentListByUserCache = portalSecurityUserAssignmentListByUserCache;
	}


	public DaoCompositeKeyCache<PortalSecurityUserAssignment, Short, Integer> getPortalSecurityUserAssignmentByUserAndEntityCache() {
		return this.portalSecurityUserAssignmentByUserAndEntityCache;
	}


	public void setPortalSecurityUserAssignmentByUserAndEntityCache(DaoCompositeKeyCache<PortalSecurityUserAssignment, Short, Integer> portalSecurityUserAssignmentByUserAndEntityCache) {
		this.portalSecurityUserAssignmentByUserAndEntityCache = portalSecurityUserAssignmentByUserAndEntityCache;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public AdvancedUpdatableDAO<PortalSecurityUserRoleResourceMapping, Criteria> getPortalSecurityUserRoleResourceMappingDAO() {
		return this.portalSecurityUserRoleResourceMappingDAO;
	}


	public void setPortalSecurityUserRoleResourceMappingDAO(AdvancedUpdatableDAO<PortalSecurityUserRoleResourceMapping, Criteria> portalSecurityUserRoleResourceMappingDAO) {
		this.portalSecurityUserRoleResourceMappingDAO = portalSecurityUserRoleResourceMappingDAO;
	}


	public DaoSingleKeyListCache<PortalSecurityUserRoleResourceMapping, Short> getPortalSecurityUserRoleResourceMappingListByRoleCache() {
		return this.portalSecurityUserRoleResourceMappingListByRoleCache;
	}


	public void setPortalSecurityUserRoleResourceMappingListByRoleCache(DaoSingleKeyListCache<PortalSecurityUserRoleResourceMapping, Short> portalSecurityUserRoleResourceMappingListByRoleCache) {
		this.portalSecurityUserRoleResourceMappingListByRoleCache = portalSecurityUserRoleResourceMappingListByRoleCache;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}


	public PortalSecurityUserGroupService getPortalSecurityUserGroupService() {
		return this.portalSecurityUserGroupService;
	}


	public void setPortalSecurityUserGroupService(PortalSecurityUserGroupService portalSecurityUserGroupService) {
		this.portalSecurityUserGroupService = portalSecurityUserGroupService;
	}
}
