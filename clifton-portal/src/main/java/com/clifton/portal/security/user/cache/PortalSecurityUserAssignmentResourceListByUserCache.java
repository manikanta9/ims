package com.clifton.portal.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentResourceListByUserCache</code> cache is used for internal (non portal entity specific) users to cache their
 * access to security resources they have access to upload to/edit, etc.
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentResourceListByUserCache extends SelfRegisteringSingleKeyDaoListCache<PortalSecurityUserAssignmentResource, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "securityUser.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalSecurityUserAssignmentResource bean) {
		if (bean.getSecurityUser() != null) {
			return bean.getSecurityUser().getId();
		}
		return null;
	}
}
