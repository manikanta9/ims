package com.clifton.portal.security.user;

import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Collections;
import java.util.List;


/**
 * The <code>PortalSecurityUserChangeEvent</code> is raised on various security related changes:
 * user assignment saves (inserts or role changes)
 * user assignment resource saves
 * user Terms of Use Acceptance
 * <p>
 * These are listened to by the {@link com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildEventListener} to rebuild expanded security table
 * and {@link com.clifton.portal.notification.event.PortalNotificationWelcomeEventListener} if applies to send out welcome emails to user(s)
 *
 * @author manderson
 */
public class PortalSecurityUserChangeEvent extends EventObject<PortalSecurityUser, String> {

	public static final String USER_ASSIGNMENT_CHANGE_EVENT_NAME = "PORTAL_SECURITY_USER_ASSIGNMENT_CHANGE";
	public static final String TERMS_OF_USE_ACCEPTANCE_EVENT_NAME = "PORTAL_SECURITY_USER_TERMS_OF_USE_ACCEPTANCE";
	public static final String USER_SECURITY_RESOURCE_CHANGE_EVENT_NAME = "PORTAL_SECURITY_USER_RESOURCE_CHANGE";


	/**
	 * If the event is associated with a particular assignment update
	 * Can hold more than one, however all assignments are  for the same user
	 */
	private List<PortalSecurityUserAssignment> userAssignmentList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuilds security expanded
	 * Then also triggers welcome email to see if user is eligible for welcome email, or if a portal admin who already accepted terms of use
	 * if there are other users eligible for welcome email
	 */
	public static PortalSecurityUserChangeEvent ofAssignmentChange(PortalSecurityUserAssignment assignment) {
		return ofCustomSecurityUserAssignmentEvent(assignment.getSecurityUser(), Collections.singletonList(assignment), USER_ASSIGNMENT_CHANGE_EVENT_NAME);
	}


	public static PortalSecurityUserChangeEvent ofAssignmentListChange(PortalSecurityUser user, List<PortalSecurityUserAssignment> assignmentList) {
		return ofCustomSecurityUserAssignmentEvent(user, assignmentList, USER_ASSIGNMENT_CHANGE_EVENT_NAME);
	}


	/**
	 * When a user accepts the Terms of Use
	 * Does not rebuild security but if the user is a portal admin will see if there are other users eligible for the welcome email
	 */
	public static PortalSecurityUserChangeEvent ofTermsOfUseAcceptance(PortalSecurityUser user) {
		return ofCustomSecurityUserEvent(user, TERMS_OF_USE_ACCEPTANCE_EVENT_NAME);
	}


	/**
	 * User change is when a user's security resources are updated
	 * Security expanded table is rebuilt for that user
	 * Welcome Emails are not applied
	 */
	public static PortalSecurityUserChangeEvent ofSecurityResourceChange(PortalSecurityUser user) {
		return ofCustomSecurityUserEvent(user, USER_SECURITY_RESOURCE_CHANGE_EVENT_NAME);
	}


	public static PortalSecurityUserChangeEvent ofCustomSecurityUserAssignmentEvent(PortalSecurityUser user, List<PortalSecurityUserAssignment> userAssignmentList, String eventName) {
		ValidationUtils.assertNotNull(user, "User is required");
		ValidationUtils.assertNotEmpty(userAssignmentList, "User Assignment is required for assignment events for PortalSecurityUserChangeEvent");
		PortalSecurityUserChangeEvent event = ofCustomSecurityUserEvent(user, eventName);
		event.setUserAssignmentList(userAssignmentList);
		return event;
	}


	public static PortalSecurityUserChangeEvent ofCustomSecurityUserEvent(PortalSecurityUser user, String eventName) {
		return new PortalSecurityUserChangeEvent(user, eventName);
	}


	private PortalSecurityUserChangeEvent(PortalSecurityUser user, String eventName) {
		super();
		ValidationUtils.assertNotNull(user, "User is required for PortalSecurityUserChangeEvent");
		ValidationUtils.assertNotNull(eventName, "Event Name is required for PortalSecurityUserChangeEvent");
		setTarget(user);
		setEventName(eventName);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PortalSecurityUserAssignment> getUserAssignmentList() {
		return this.userAssignmentList;
	}


	public void setUserAssignmentList(List<PortalSecurityUserAssignment> userAssignmentList) {
		this.userAssignmentList = userAssignmentList;
	}
}
