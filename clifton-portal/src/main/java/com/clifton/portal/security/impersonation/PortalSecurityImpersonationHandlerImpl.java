package com.clifton.portal.security.impersonation;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;


/**
 * @author manderson
 */
@Component
public class PortalSecurityImpersonationHandlerImpl implements PortalSecurityImpersonationHandler {

	private ContextHandler contextHandler;
	private PortalSecurityUserService portalSecurityUserService;

	private final static String SYSTEM_USER_NAME = "systemuser";


	@Override
	public void runAsSystemUser(Runnable task) {
		PortalSecurityUser systemUser = getSystemUser();

		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
		try {
			// set the Run As User as system user
			getContextHandler().setBean(Context.USER_BEAN_NAME, systemUser);

			task.run();
		}
		finally {
			// restore the current user
			if (currentUser == null) {
				getContextHandler().removeBean(Context.USER_BEAN_NAME);
			}
			else {
				getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
			}
		}
	}


	@Override
	public <T> T runAsSystemUserAndReturn(Supplier<T> task) {
		PortalSecurityUser systemUser = getSystemUser();

		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
		try {
			// set the Run As User as current user
			getContextHandler().setBean(Context.USER_BEAN_NAME, systemUser);

			return task.get();
		}
		finally {
			// restore the current user
			if (currentUser == null) {
				getContextHandler().removeBean(Context.USER_BEAN_NAME);
			}
			else {
				getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalSecurityUser getSystemUser() {
		// Note: This retrieval is cached
		PortalSecurityUser systemUser = getPortalSecurityUserService().getPortalSecurityUserByUserName(SYSTEM_USER_NAME, false);
		if (systemUser == null) {
			throw new ValidationException("Could not find System user with username " + SYSTEM_USER_NAME + " as an available user");
		}
		return systemUser;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
