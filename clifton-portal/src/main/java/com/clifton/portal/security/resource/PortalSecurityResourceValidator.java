package com.clifton.portal.security.resource;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalSecurityResourceValidator extends SelfRegisteringDaoValidator<PortalSecurityResource> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortalSecurityResource bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "You cannot delete a system defined security resource.");
		}
		else {
			if (config.isInsert()) {
				ValidationUtils.assertFalse(bean.isSystemDefined(), "You cannot add a system defined security resource.");
			}
			if (config.isUpdate()) {
				PortalSecurityResource originalBean = getOriginalBean(bean);
				if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
					throw new FieldValidationException("You cannot change system defined flag of an existing security resource.", "systemDefined");
				}
				if (originalBean.isSystemDefined()) {
					ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "You cannot change the name of a system defined security resource.", "name");
				}
			}
			if (bean.getLevel() > bean.getMaxDepth()) {
				throw new ValidationException("Security resources can be a max of [" + bean.getMaxDepth() + "] levels deep.  Security Resource " + bean.getName() + " is now " + bean.getLevel() + " levels deep.");
			}
		}
	}
}
