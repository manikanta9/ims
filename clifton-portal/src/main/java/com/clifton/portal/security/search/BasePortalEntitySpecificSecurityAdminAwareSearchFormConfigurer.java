package com.clifton.portal.security.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;


/**
 * @author manderson
 */
public abstract class BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer extends BasePortalEntitySpecificAwareSearchFormConfigurer {

	private final PortalEntitySpecificSecurityAdminAwareSearchForm portalEntitySpecificSecurityAdminAwareSearchForm;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BasePortalEntitySpecificSecurityAdminAwareSearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);

		this.portalEntitySpecificSecurityAdminAwareSearchForm = (PortalEntitySpecificSecurityAdminAwareSearchForm) searchForm;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId) {
		configureCriteriaForUser(criteria, portalSecurityUserId, this.portalEntitySpecificSecurityAdminAwareSearchForm.getViewSecurityAdminRole(), this.portalEntitySpecificSecurityAdminAwareSearchForm.getHasSecurityResourceAccess());
	}


	public abstract void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId, Boolean viewSecurityAdminRole, Boolean hasSecurityResourceAccess);
}
