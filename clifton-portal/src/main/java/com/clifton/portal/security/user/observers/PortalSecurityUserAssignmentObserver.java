package com.clifton.portal.security.user.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserAssignmentObserver</code> handles:
 * 1. Deleting notification subscriptions for an assignment before the assignment is deleted
 * 2. Rebuilding Notification Subscriptions on Inserts or Changes to User Assignments
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentObserver extends SelfRegisteringDaoObserver<PortalSecurityUserAssignment> {

	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortalSecurityUserAssignment> dao, DaoEventTypes event, PortalSecurityUserAssignment bean) {
		if (event.isDelete()) {
			getPortalNotificationSubscriptionService().deletePortalNotificationSubscriptionListForUserAssignment(bean.getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationSubscriptionService getPortalNotificationSubscriptionService() {
		return this.portalNotificationSubscriptionService;
	}


	public void setPortalNotificationSubscriptionService(PortalNotificationSubscriptionService portalNotificationSubscriptionService) {
		this.portalNotificationSubscriptionService = portalNotificationSubscriptionService;
	}
}
