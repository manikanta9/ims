package com.clifton.portal.security.web.filter;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.core.web.view.JsonView;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * The <code>PortalSecurityUserManagementFilter</code> class is a servlet filter that puts current {@link com.clifton.portal.security.user.PortalSecurityUser}
 * into request {@link Context} before its execution, then chains remaining filter and finally clears the user in the end.
 * <p>
 * Also does timing of execution.
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserManagementFilter implements Filter {

	private ContextHandler contextHandler;
	private HandlerExceptionResolver webExceptionResolver;
	private PortalSecurityUserService portalSecurityUserService;
	private SystemRequestStatsService systemRequestStatsService;
	private TimerHandler timerHandler;

	private JsonView webJsonView;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The url for the user to accept the terms of use.  Will be used to allow this to go through if terms of use is required for the user.
	 */
	private String termsOfUseAcceptanceUrl;

	/**
	 * If terms of use is required for the user, and the request isn't for the termsOfUseAcceptanceUrl redirect user to this location to read and accept the
	 * terms of use. NOTE: IF THIS IS BLANK THAN THE TERMS OF USE ACCEPTANCE IS NOT ENFORCED!
	 */
	private String termsOfUseAcceptanceRequiredRedirect;


	/**
	 * The url for the user to reset their password.  Will be used to allow this to go through if password reset is required for the user.
	 */
	private String passwordResetUrl;

	/**
	 * The url for the user to request a password reset.  Will be used to allow this to go through if password reset is required for the user.
	 */
	private String passwordResetRequestUrl;

	/**
	 * If password reset is required for the use, and the request isn't for the passwordResetUrl, redirect user to this location to change password
	 * NOTE: IF THIS IS BLANK THAN THE PASSWORD RESET IS NOT ENFORCED!
	 */
	private String passwordResetRedirect;

	/**
	 * The list of URLs that are allowed without authenticated access. If no authenticated session exists when accessing any of these URLs, then no principal user will be retrieved
	 * during request processing.
	 */
	private List<String> unsecuredUrlList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void init(FilterConfig config) {
		// do nothing
	}


	@Override
	public void destroy() {
		// do nothing
	}


	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException {
		getTimerHandler().startTimer();
		HttpServletRequest request = (HttpServletRequest) req;

		try {
			boolean stopFilterChain = false;
			PortalSecurityUser user = getPrincipalUser(request);
			if (user != null) {
				// 2. store current user in context
				getContextHandler().setBean(Context.USER_BEAN_NAME, user);

				// 3. Check if Terms of Use Acceptance Is Missing and Required
				if (!StringUtils.isEmpty(getTermsOfUseAcceptanceRequiredRedirect()) && user.isTermsOfUseAcceptanceRequired() && !StringUtils.isLikeIgnoringCase(request.getRequestURI(), getTermsOfUseAcceptanceUrl())) {
					LogUtils.warn(getClass(), "User [" + user.getUsername() + "] credentials have expired.");
					HttpServletResponse response = (HttpServletResponse) res;
					response.sendRedirect(getTermsOfUseAcceptanceRequiredRedirect());
					stopFilterChain = true;
				}

				// 4. Check if Password is Expired Only if Not Resetting It
				if (!StringUtils.isEmpty(getPasswordResetRedirect()) && user.isPasswordResetRequired() && !StringUtils.isLikeIgnoringCase(request.getRequestURI(), getPasswordResetUrl())) {
					LogUtils.warn(getClass(), "User [" + user.getUsername() + "] credentials have expired.");
					HttpServletResponse response = (HttpServletResponse) res;
					response.sendRedirect(getPasswordResetRedirect());
					stopFilterChain = true;
				}
			}
			if (!stopFilterChain) {
				//Execute everything else
				chain.doFilter(req, res);
			}
		}
		catch (Exception e) {
			try {
				// in case there's an exception, resolve it and render results
				HttpServletResponse response = (HttpServletResponse) res;
				ModelAndView modelAndView = getWebExceptionResolver().resolveException(request, response, null, e);
				AssertUtils.assertNotNull(modelAndView, "Exception resolver returned null ModelAndView");
				View view = modelAndView.getView();
				if (view == null) {
					view = getWebJsonView();
				}
				view.render(modelAndView.getModel(), request, response);
			}
			catch (Throwable innerException) {
				// do not throw exception if connection to the client was closed: there's nowhere to write response
				if (!ExceptionUtils.isConnectionBrokenException(innerException)) {
					// let's hope that this never happens
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "ORIGINAL: " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(e))));
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), innerException, () -> "INNER: " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(innerException))).withRequest(request));
					throw new ServletException("Error in generation of output for another error.", e);
				}
			}
		}
		finally {
			// 3. clear the user in the end
			Object user = getContextHandler().removeBean(Context.USER_BEAN_NAME);

			// clear full context so that the next request can have a clean start
			// NOTE: assumes this is the last part of request to be executed that may need the context
			getContextHandler().clear();

			// stop the timer and record stats
			getTimerHandler().stopTimer();
			getSystemRequestStatsService().saveSystemRequestStats(request, (HttpServletResponse) res, user, getTimerHandler().getDurationNano(), 0, 0);
			getTimerHandler().reset();
		}
	}


	protected PortalSecurityUser getPrincipalUser(HttpServletRequest request) {
		// 1. retrieve authenticated principal from security context
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication != null ? SecurityContextHolder.getContext().getAuthentication().getPrincipal() : null;
		if (principal instanceof PortalSecurityUser) {
			// Pull from Hibernate Cache to ensure getting latest up to date user data
			return getPortalSecurityUserService().getPortalSecurityUser(((PortalSecurityUser) principal).getId());
		}
		else if (principal instanceof User) {
			return getPortalSecurityUserService().getPortalSecurityUserByUserName(((User) principal).getUsername(), false);
		}
		else if (principal == null || principal instanceof String) {
			if (principal == null || "anonymousUser".equals(principal)) {
				return getAnonymousUser(request);
			}
			return getPortalSecurityUserService().getPortalSecurityUserByUserName((String) principal, false);
		}
		throw new RuntimeException("Cannot find authentication principal in security context.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalSecurityUser getAnonymousUser(HttpServletRequest request) {
		if (!StringUtils.isEmpty(getPasswordResetRequestUrl()) && StringUtils.isLikeIgnoringCase(request.getRequestURI(), getPasswordResetRequestUrl())) {
			String userName = request.getParameter("userName");
			PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, true);
			ValidationUtils.assertNotNull(user, "Cannot find user with user name [" + userName + "]");
			return user;
		}
		else if (!StringUtils.isEmpty(getPasswordResetUrl()) && StringUtils.isLikeIgnoringCase(request.getRequestURI(), getPasswordResetUrl())) {
			String passwordResetKey = request.getParameter("passwordResetKey");
			ValidationUtils.assertNotNull(passwordResetKey, "Invalid server request.");
			PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserByPasswordResetKey(passwordResetKey);
			ValidationUtils.assertNotNull(user, "Password reset key has expired.  Please request a new one.");
			return user;
		}
		else if (CollectionUtils.anyMatch(getUnsecuredUrlList(), url -> StringUtils.isLikeIgnoringCase(request.getRequestURI(), url))) {
			// Allow no user
			return null;
		}
		throw new RuntimeException("Cannot find authentication principal in security context.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@ValueIgnoringGetter
	public HandlerExceptionResolver getWebExceptionResolver() {
		return this.webExceptionResolver;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public void setWebExceptionResolver(HandlerExceptionResolver webExceptionResolver) {
		this.webExceptionResolver = webExceptionResolver;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}


	public JsonView getWebJsonView() {
		return this.webJsonView;
	}


	public void setWebJsonView(JsonView webJsonView) {
		this.webJsonView = webJsonView;
	}


	public String getTermsOfUseAcceptanceUrl() {
		return this.termsOfUseAcceptanceUrl;
	}


	public void setTermsOfUseAcceptanceUrl(String termsOfUseAcceptanceUrl) {
		this.termsOfUseAcceptanceUrl = termsOfUseAcceptanceUrl;
	}


	public String getTermsOfUseAcceptanceRequiredRedirect() {
		return this.termsOfUseAcceptanceRequiredRedirect;
	}


	public void setTermsOfUseAcceptanceRequiredRedirect(String termsOfUseAcceptanceRequiredRedirect) {
		this.termsOfUseAcceptanceRequiredRedirect = termsOfUseAcceptanceRequiredRedirect;
	}


	public String getPasswordResetUrl() {
		return this.passwordResetUrl;
	}


	public void setPasswordResetUrl(String passwordResetUrl) {
		this.passwordResetUrl = passwordResetUrl;
	}


	public String getPasswordResetRequestUrl() {
		return this.passwordResetRequestUrl;
	}


	public void setPasswordResetRequestUrl(String passwordResetRequestUrl) {
		this.passwordResetRequestUrl = passwordResetRequestUrl;
	}


	public String getPasswordResetRedirect() {
		return this.passwordResetRedirect;
	}


	public void setPasswordResetRedirect(String passwordResetRedirect) {
		this.passwordResetRedirect = passwordResetRedirect;
	}


	public List<String> getUnsecuredUrlList() {
		return this.unsecuredUrlList;
	}


	public void setUnsecuredUrlList(List<String> unsecuredUrlList) {
		this.unsecuredUrlList = unsecuredUrlList;
	}
}
