package com.clifton.portal.security.user.expanded;

import java.util.List;


/**
 * @author manderson
 */
public interface PortalSecurityUserAssignmentExpandedHandler {


	////////////////////////////////////////////////////////////////////////////////
	////////      Portal Security User Assignment Expanded DAO Methods      ////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PortalSecurityUserAssignmentExpanded> getPortalSecurityUserAssignmentExpandedListForUser(short userId);


	/**
	 * PortalEntityID can be null if we are looking for internal users with access to specified security resource
	 */
	public List<PortalSecurityUserAssignmentExpanded> getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(short securityResourceId, Integer portalEntityId);


	public void savePortalSecurityUserAssignmentExpandedList(List<PortalSecurityUserAssignmentExpanded> newList, List<PortalSecurityUserAssignmentExpanded> existingList);


	public void deletePortalSecurityUserAssignmentExpandedList(List<PortalSecurityUserAssignmentExpanded> deleteList);
}
