package com.clifton.portal.security.user.group;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.resource.PortalSecurityResourceSearchForm;
import com.clifton.portal.security.resource.PortalSecurityResourceService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.group.search.PortalSecurityGroupSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortalSecurityUserGroupServiceImpl implements PortalSecurityUserGroupService {


	private AdvancedUpdatableDAO<PortalSecurityGroup, Criteria> portalSecurityGroupDAO;
	private AdvancedUpdatableDAO<PortalSecurityUserGroup, Criteria> portalSecurityUserGroupDAO;
	private AdvancedUpdatableDAO<PortalSecurityGroupResource, Criteria> portalSecurityGroupResourceDAO;

	private PortalSecurityResourceService portalSecurityResourceService;

	private DaoSingleKeyListCache<PortalSecurityUserGroup, Short> portalSecurityUserGroupListByGroupCache;
	private DaoSingleKeyListCache<PortalSecurityUserGroup, Short> portalSecurityUserGroupListByUserCache;

	private DaoSingleKeyListCache<PortalSecurityGroupResource, Short> portalSecurityGroupResourceListByGroupCache;


	////////////////////////////////////////////////////////////////////////////////
	//////////               Portal Security Group Methods              ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalSecurityGroup getPortalSecurityGroup(short id) {
		return getPortalSecurityGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalSecurityGroup getPortalSecurityGroupExpanded(short id) {
		PortalSecurityGroup securityGroup = getPortalSecurityGroup(id);
		if (securityGroup != null) {
			securityGroup.setResourceList(getPortalSecurityGroupResourceListForGroup(id, true));
		}
		return securityGroup;
	}


	@Override
	public List<PortalSecurityGroup> getPortalSecurityGroupList(PortalSecurityGroupSearchForm searchForm) {
		return getPortalSecurityGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortalSecurityGroup savePortalSecurityGroup(PortalSecurityGroup securityGroup, boolean saveResourceListOnly) {
		List<PortalSecurityGroupResource> resourceList = securityGroup.getResourceList();

		if (!saveResourceListOnly) {
			validatePortalSecurityGroupSourceSystemNotSet(securityGroup, "Group edits");
			securityGroup = getPortalSecurityGroupDAO().save(securityGroup);
		}
		savePortalSecurityGroupResourceListImpl(securityGroup, resourceList);
		return getPortalSecurityGroupExpanded(securityGroup.getId());
	}


	@Transactional
	@Override
	public PortalSecurityGroup savePortalSecurityGroupFromSourceSystem(PortalSecurityGroup securityGroup, List<PortalSecurityUser> userList) {
		ValidationUtils.assertNotNull(securityGroup.getSourceSystem(), "Source System is required");
		ValidationUtils.assertNotNull(securityGroup.getSourceFkFieldId(), "Source FK Field ID is required");

		List<PortalSecurityUserGroup> existingList = securityGroup.isNewBean() ? null : getPortalSecurityUserGroupListForGroup(securityGroup.getId());
		Map<Short, PortalSecurityUserGroup> existingUserMap = BeanUtils.getBeanMap(existingList, userGroup -> userGroup.getSecurityUser().getId());
		List<PortalSecurityUserGroup> saveList = new ArrayList<>();

		for (PortalSecurityUser user : CollectionUtils.getIterable(userList)) {
			ValidationUtils.assertFalse(user.getUserRole().isPortalEntitySpecific(), "Invalid User Selected. Security Group User membership applies to internal users only.");
			if (existingUserMap.containsKey(user.getId())) {
				saveList.add(existingUserMap.get(user.getId()));
			}
			else {
				PortalSecurityUserGroup userGroup = new PortalSecurityUserGroup();
				userGroup.setSecurityUser(user);
				userGroup.setSecurityGroup(securityGroup);
				saveList.add(userGroup);
			}
		}

		securityGroup = getPortalSecurityGroupDAO().save(securityGroup);
		getPortalSecurityUserGroupDAO().saveList(saveList, existingList);
		return securityGroup;
	}


	@Override
	public void deletePortalSecurityGroup(short id) {
		validatePortalSecurityGroupSourceSystemNotSet(getPortalSecurityGroup(id), "Delete group");
		getPortalSecurityGroupDAO().delete(id);
	}


	@Override
	public void deletePortalSecurityGroupFromSourceSystem(short id) {
		getPortalSecurityUserGroupDAO().deleteList(getPortalSecurityUserGroupListForGroup(id));
		getPortalSecurityGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////


	private void validatePortalSecurityGroupSourceSystemNotSet(PortalSecurityGroup group, String actionName) {
		if (group.getSourceSystem() != null) {
			throw new ValidationException(actionName + " is not supported for groups created and maintained by source system [" + group.getSourceSystem().getName() + "].");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////             Portal Security User Group Methods            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupList() {
		return getPortalSecurityUserGroupDAO().findAll();
	}


	private PortalSecurityUserGroup getPortalSecurityUserGroup(short userId, short groupId) {
		return getPortalSecurityUserGroupDAO().findOneByFields(new String[]{"securityUser.id", "securityGroup.id"}, new Object[]{userId, groupId});
	}


	@Override
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupListForUser(short userId) {
		return getPortalSecurityUserGroupListByUserCache().getBeanListForKeyValue(getPortalSecurityUserGroupDAO(), userId);
	}


	@Override
	public List<PortalSecurityUserGroup> getPortalSecurityUserGroupListForGroup(short groupId) {
		return getPortalSecurityUserGroupListByGroupCache().getBeanListForKeyValue(getPortalSecurityUserGroupDAO(), groupId);
	}


	@Override
	public PortalSecurityUserGroup savePortalSecurityUserGroup(PortalSecurityUserGroup bean) {
		PortalSecurityUserGroup existingLink = getPortalSecurityUserGroup(bean.getSecurityUser().getId(), bean.getSecurityGroup().getId());
		ValidationUtils.assertNull(existingLink, "User / Group is already linked.");
		// Otherwise - can only use this method for groups not managed by an external source system
		validatePortalSecurityGroupSourceSystemNotSet(bean.getSecurityGroup(), "User membership edits");
		// And for internal users only
		ValidationUtils.assertFalse(bean.getSecurityUser().getUserRole().isPortalEntitySpecific(), "Invalid User Selected. Security Group User membership applies to internal users only.");
		return getPortalSecurityUserGroupDAO().save(bean);
	}


	@Override
	public void deletePortalSecurityUserGroup(int id) {
		PortalSecurityUserGroup existingLink = getPortalSecurityUserGroupDAO().findByPrimaryKey(id);
		ValidationUtils.assertNotNull(existingLink, "User / Group doesn't exist.");

		// Otherwise - can only use this method for groups not managed by an external source system
		PortalSecurityGroup group = getPortalSecurityGroup(existingLink.getSecurityGroup().getId());
		validatePortalSecurityGroupSourceSystemNotSet(group, "User membership edits");

		getPortalSecurityUserGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////           Portal Security Group Resource Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalSecurityGroupResource> getPortalSecurityGroupResourceListForGroup(short groupId, boolean expandSecurity) {
		List<PortalSecurityGroupResource> groupResourceList = getPortalSecurityGroupResourceListByGroupCache().getBeanListForKeyValue(getPortalSecurityGroupResourceDAO(), groupId);
		if (expandSecurity) {
			int counter = 0;
			Map<Short, PortalSecurityGroupResource> groupResourceMap = BeanUtils.getBeanMap(groupResourceList, groupResource -> groupResource.getSecurityResource().getId());
			List<PortalSecurityResource> resourceList = getPortalSecurityResourceService().getPortalSecurityResourceList(new PortalSecurityResourceSearchForm());

			List<PortalSecurityGroupResource> newGroupResourceList = new ArrayList<>();
			for (PortalSecurityResource resource : CollectionUtils.getIterable(resourceList)) {
				PortalSecurityGroupResource groupResource = groupResourceMap.get(resource.getId());
				if (groupResource == null) {
					counter = (counter - 10);
					groupResource = new PortalSecurityGroupResource();
					groupResource.setId(counter);
					groupResource.setSecurityResource(resource);
					groupResource.setAccessAllowed(false);
					// If parent is mapped - give it the parent access
					if (resource.getParent() != null && groupResourceMap.get(resource.getParent().getId()) != null) {
						groupResource.setAccessAllowed(groupResourceMap.get(resource.getParent().getId()).isAccessAllowed());
					}
				}
				newGroupResourceList.add(groupResource);
			}
			return newGroupResourceList;
		}
		return groupResourceList;
	}


	private void savePortalSecurityGroupResourceListImpl(PortalSecurityGroup securityGroup, List<PortalSecurityGroupResource> resourceList) {
		List<PortalSecurityGroupResource> existingList = getPortalSecurityGroupResourceListForGroup(securityGroup.getId(), false);
		List<PortalSecurityGroupResource> saveList = new ArrayList<>();
		Map<Short, PortalSecurityGroupResource> resourceMap = BeanUtils.getBeanMap(resourceList, groupResource -> groupResource.getSecurityResource().getId());

		for (PortalSecurityGroupResource groupResource : CollectionUtils.getIterable(resourceList)) {
			// If there is a parent - check it - if it's the same, we just save the parent level
			if (groupResource.getSecurityResource().getParent() != null) {
				PortalSecurityGroupResource parentGroupResource = resourceMap.get(groupResource.getSecurityResource().getParent().getId());
				if (parentGroupResource != null && parentGroupResource.isAccessAllowed() == groupResource.isAccessAllowed()) {
					continue;
				}
				saveList.add(groupResource);
			}
			// Otherwise only save if true
			else if (groupResource.isAccessAllowed()) {
				saveList.add(groupResource);
			}
		}

		for (PortalSecurityGroupResource groupResource : CollectionUtils.getIterable(saveList)) {
			groupResource.setSecurityGroup(securityGroup);
			// When we populate the default list we set ids to -10, -20, etc.  On save we need to clear that so that it's properly inserted.
			if (groupResource.getId() != null && groupResource.getId() <= 0) {
				groupResource.setId(null);
			}
		}
		getPortalSecurityGroupResourceDAO().saveList(saveList, existingList);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalSecurityGroup, Criteria> getPortalSecurityGroupDAO() {
		return this.portalSecurityGroupDAO;
	}


	public void setPortalSecurityGroupDAO(AdvancedUpdatableDAO<PortalSecurityGroup, Criteria> portalSecurityGroupDAO) {
		this.portalSecurityGroupDAO = portalSecurityGroupDAO;
	}


	public AdvancedUpdatableDAO<PortalSecurityUserGroup, Criteria> getPortalSecurityUserGroupDAO() {
		return this.portalSecurityUserGroupDAO;
	}


	public void setPortalSecurityUserGroupDAO(AdvancedUpdatableDAO<PortalSecurityUserGroup, Criteria> portalSecurityUserGroupDAO) {
		this.portalSecurityUserGroupDAO = portalSecurityUserGroupDAO;
	}


	public AdvancedUpdatableDAO<PortalSecurityGroupResource, Criteria> getPortalSecurityGroupResourceDAO() {
		return this.portalSecurityGroupResourceDAO;
	}


	public void setPortalSecurityGroupResourceDAO(AdvancedUpdatableDAO<PortalSecurityGroupResource, Criteria> portalSecurityGroupResourceDAO) {
		this.portalSecurityGroupResourceDAO = portalSecurityGroupResourceDAO;
	}


	public PortalSecurityResourceService getPortalSecurityResourceService() {
		return this.portalSecurityResourceService;
	}


	public void setPortalSecurityResourceService(PortalSecurityResourceService portalSecurityResourceService) {
		this.portalSecurityResourceService = portalSecurityResourceService;
	}


	public DaoSingleKeyListCache<PortalSecurityUserGroup, Short> getPortalSecurityUserGroupListByGroupCache() {
		return this.portalSecurityUserGroupListByGroupCache;
	}


	public void setPortalSecurityUserGroupListByGroupCache(DaoSingleKeyListCache<PortalSecurityUserGroup, Short> portalSecurityUserGroupListByGroupCache) {
		this.portalSecurityUserGroupListByGroupCache = portalSecurityUserGroupListByGroupCache;
	}


	public DaoSingleKeyListCache<PortalSecurityUserGroup, Short> getPortalSecurityUserGroupListByUserCache() {
		return this.portalSecurityUserGroupListByUserCache;
	}


	public void setPortalSecurityUserGroupListByUserCache(DaoSingleKeyListCache<PortalSecurityUserGroup, Short> portalSecurityUserGroupListByUserCache) {
		this.portalSecurityUserGroupListByUserCache = portalSecurityUserGroupListByUserCache;
	}


	public DaoSingleKeyListCache<PortalSecurityGroupResource, Short> getPortalSecurityGroupResourceListByGroupCache() {
		return this.portalSecurityGroupResourceListByGroupCache;
	}


	public void setPortalSecurityGroupResourceListByGroupCache(DaoSingleKeyListCache<PortalSecurityGroupResource, Short> portalSecurityGroupResourceListByGroupCache) {
		this.portalSecurityGroupResourceListByGroupCache = portalSecurityGroupResourceListByGroupCache;
	}
}
