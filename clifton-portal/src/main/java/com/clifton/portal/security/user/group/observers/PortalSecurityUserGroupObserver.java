package com.clifton.portal.security.user.group.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.group.PortalSecurityUserGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserGroupObserver</code> observes changes to user group memberships rebuilds security for the user impacted by the change
 * Changes to this table are either inserts or deletes, no updates
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserGroupObserver extends SelfRegisteringDaoObserver<PortalSecurityUserGroup> {

	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT_OR_DELETE;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortalSecurityUserGroup> dao, DaoEventTypes event, PortalSecurityUserGroup bean, Throwable e) {
		getPortalSecurityUserService().rebuildPortalSecurityUserAssignmentResourceListForUser(bean.getSecurityUser().getId());
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
