package com.clifton.portal.security.user.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.notification.event.PortalNotificationEvent;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalSecurityUserObserver</code> validates:
 * <p>
 * 1. Deletes Cannot occur.  Users can only be disabled.  There aren't any service methods that would allow this so it should never get here
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserObserver extends SelfRegisteringDaoObserver<PortalSecurityUser> {

	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortalSecurityUser> dao, DaoEventTypes event, PortalSecurityUser bean) {
		if (event.isDelete()) {
			throw new ValidationException("Users cannot be deleted.  Please disable the user instead.");
		}
		// Get Original Bean so We have it for after check
		else if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortalSecurityUser> dao, DaoEventTypes event, PortalSecurityUser bean, Throwable e) {
		if (e == null && event.isUpdate()) {
			PortalSecurityUser originalBean = getOriginalBean(dao, bean);
			if (!originalBean.isAccountLocked() && bean.isAccountLocked()) {
				// Raise Event that Will Send Email to User that is Locked Out
				getEventHandler().raiseEvent(new PortalNotificationEvent(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_LOCKED, bean));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}

