package com.clifton.portal.security.user.expanded;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentRetrievalContext;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortalSecurityUserAssignmentExpandedRebuildServiceImpl implements PortalSecurityUserAssignmentExpandedRebuildService {

	private PortalEntityService portalEntityService;
	private PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler;
	private PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////////
	///////     Portal Security User Assignment Expanded Rebuild Methods     ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebuildPortalSecurityUserAssignmentExpanded() {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		List<PortalSecurityUser> userList = getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false);
		PortalSecurityUserAssignmentRetrievalContext retrievalContext = new PortalSecurityUserAssignmentRetrievalContext();
		for (PortalSecurityUser user : CollectionUtils.getIterable(userList)) {
			rebuildPortalSecurityUserAssignmentExpandedForUserImpl(getPortalSecurityUserService().getPortalSecurityUser(user.getId()), retrievalContext);
		}
		LogUtils.info(getClass(), "rebuildPortalSecurityUserAssignmentExpanded for all users completed.");
	}


	@Override
	public void rebuildPortalSecurityUserAssignmentExpandedForUser(short userId) {
		rebuildPortalSecurityUserAssignmentExpandedForUserImpl(getPortalSecurityUserService().getPortalSecurityUser(userId), new PortalSecurityUserAssignmentRetrievalContext());
		LogUtils.info(getClass(), "rebuildPortalSecurityUserAssignmentExpandedForUser (" + userId + ") completed");
	}


	protected void rebuildPortalSecurityUserAssignmentExpandedForUserImpl(PortalSecurityUser user, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		// Get Existing Results
		List<PortalSecurityUserAssignmentExpanded> existingList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForUser(user.getId());
		// If User Is Disabled, Remove all Records
		if (user.isDisabled()) {
			getPortalSecurityUserAssignmentExpandedHandler().deletePortalSecurityUserAssignmentExpandedList(existingList);
		}
		else if (user.getUserRole().isPortalEntitySpecific()) {
			// Get List of All Assignments
			PortalSecurityUserAssignmentSearchForm searchForm = new PortalSecurityUserAssignmentSearchForm();
			searchForm.setPortalSecurityUserId(user.getId());
			// Exclude Disabled Assignments
			searchForm.setDisabled(false);
			Map<Integer, PortalSecurityUserAssignment> entityAssignmentMap = BeanUtils.getBeanMap(getPortalSecurityUserService().getPortalSecurityUserAssignmentListWithContext(searchForm, true, retrievalContext), assignment -> assignment.getPortalEntity().getId());
			Map<Integer, List<PortalSecurityUserAssignmentExpanded>> entityAssignmentExpandedMap = new HashMap<>();
			// This is for Contacts - unique case that can push UP - not just down.
			Map<Integer, PortalSecurityUserAssignmentExpanded> entityAssignmentContactSecurityMap = new HashMap<>();

			for (Map.Entry<Integer, PortalSecurityUserAssignment> integerPortalSecurityUserAssignmentEntry : entityAssignmentMap.entrySet()) {
				PortalEntity portalEntity = getPortalEntityService().getPortalEntity(integerPortalSecurityUserAssignmentEntry.getKey());
				// Only Include if it's active and not marked for deletion (can't think of how a deleted entity would get here)
				if (portalEntity.isActive() || !portalEntity.isDeleted()) {
					List<PortalSecurityUserAssignmentExpanded> entityList = new ArrayList<>();
					for (PortalSecurityUserAssignmentResource assignmentResource : CollectionUtils.getIterable(integerPortalSecurityUserAssignmentEntry.getValue().getUserAssignmentResourceList())) {
						if (assignmentResource.isAccessAllowed()) {
							PortalSecurityUserAssignmentExpanded expanded = new PortalSecurityUserAssignmentExpanded(integerPortalSecurityUserAssignmentEntry.getValue(), assignmentResource.getSecurityResource());
							entityList.add(expanded);
							if (PortalSecurityResource.RESOURCE_CONTACTS.equals(assignmentResource.getSecurityResource().getName())) {
								entityAssignmentContactSecurityMap.put(integerPortalSecurityUserAssignmentEntry.getKey(), expanded);
							}
						}
					}
					// If no resource permissions, still create the entry for the entity
					if (CollectionUtils.isEmpty(entityList)) {
						entityList.add(new PortalSecurityUserAssignmentExpanded(integerPortalSecurityUserAssignmentEntry.getValue(), null));
					}
					entityAssignmentExpandedMap.put(integerPortalSecurityUserAssignmentEntry.getKey(), entityList);
				}
			}

			// Now Get All Children
			for (Integer portalEntityId : entityAssignmentMap.keySet()) {
				addAllPortalEntityChildren(portalEntityId, entityAssignmentExpandedMap, retrievalContext);
			}

			// Add missing parent assignments with CONTACT security ONLY
			// i.e. given access to the Client level, but pick list we still need the organization available
			// Note: If the User has access to Contacts then will have access to Contacts at parent level as well
			for (Integer portalEntityId : entityAssignmentMap.keySet()) {
				PortalEntity portalEntity = getPortalEntityService().getPortalEntity(portalEntityId);
				if (portalEntity.getParentPortalEntity() != null && !entityAssignmentExpandedMap.containsKey(portalEntity.getParentPortalEntity().getId())) {
					PortalSecurityUserAssignmentExpanded parentExpanded = new PortalSecurityUserAssignmentExpanded();
					parentExpanded.setPortalSecurityUserId(user.getId());
					parentExpanded.setPortalEntityId(portalEntity.getParentPortalEntity().getId());
					// If contact security is allowed - push it up
					PortalSecurityUserAssignmentExpanded contactSecurityExpanded = entityAssignmentContactSecurityMap.get(portalEntityId);
					if (contactSecurityExpanded != null) {
						parentExpanded.setPortalSecurityResourceId(contactSecurityExpanded.getPortalSecurityResourceId());
					}
					entityAssignmentExpandedMap.put(portalEntity.getParentPortalEntity().getId(), CollectionUtils.createList(parentExpanded));
				}
			}

			// Merge Existing List with New List And Save
			getPortalSecurityUserAssignmentExpandedHandler().savePortalSecurityUserAssignmentExpandedList(CollectionUtils.combineCollectionOfCollections(entityAssignmentExpandedMap.values()), existingList);
		}
		else {
			// Global Permissions for Posting/editing files
			List<PortalSecurityUserAssignmentResource> userAssignmentResourceList = user.getUserAssignmentResourceList();
			List<PortalSecurityUserAssignmentExpanded> newList = new ArrayList<>();
			for (PortalSecurityUserAssignmentResource assignmentResource : CollectionUtils.getIterable(userAssignmentResourceList)) {
				if (assignmentResource.isAccessAllowed()) {
					newList.add(new PortalSecurityUserAssignmentExpanded(user, assignmentResource.getSecurityResource()));
				}
			}
			// Merge Existing List with New List And Save
			getPortalSecurityUserAssignmentExpandedHandler().savePortalSecurityUserAssignmentExpandedList(newList, existingList);
		}
	}


	protected void addAllPortalEntityChildren(Integer portalEntityId, Map<Integer, List<PortalSecurityUserAssignmentExpanded>> entityAssignmentExpandedMap, PortalSecurityUserAssignmentRetrievalContext retrievalContext) {
		List<PortalEntity> childrenList = retrievalContext.getPortalEntityChildrenList(getPortalEntityService(), portalEntityId);

		List<PortalSecurityUserAssignmentExpanded> parentEntityAssignmentList = entityAssignmentExpandedMap.get(portalEntityId);

		for (PortalEntity childEntity : CollectionUtils.getIterable(childrenList)) {
			if (childEntity.isActive() && !childEntity.isDeleted()) {
				// Nothing Specific Use the Parent
				if (!entityAssignmentExpandedMap.containsKey(childEntity.getId())) {
					List<PortalSecurityUserAssignmentExpanded> entityList = new ArrayList<>();

					for (PortalSecurityUserAssignmentExpanded entityAssignment : CollectionUtils.getIterable(parentEntityAssignmentList)) {
						entityList.add(new PortalSecurityUserAssignmentExpanded(entityAssignment, childEntity.getId(), PortalUtils.getPortalEntityViewTypeId(childEntity)));
					}
					entityAssignmentExpandedMap.put(childEntity.getId(), entityList);
				}
				addAllPortalEntityChildren(childEntity.getId(), entityAssignmentExpandedMap, retrievalContext);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalSecurityUserAssignmentExpandedHandler getPortalSecurityUserAssignmentExpandedHandler() {
		return this.portalSecurityUserAssignmentExpandedHandler;
	}


	public void setPortalSecurityUserAssignmentExpandedHandler(PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler) {
		this.portalSecurityUserAssignmentExpandedHandler = portalSecurityUserAssignmentExpandedHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
