package com.clifton.portal.security.user.observers;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentResourceValidator extends SelfRegisteringDaoValidator<PortalSecurityUserAssignmentResource> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortalSecurityUserAssignmentResource bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertMutuallyExclusive("A security resource must be associated with either a user or user assignment, but not both.", bean.getSecurityUser(), bean.getSecurityUserAssignment());
	}
}
