package com.clifton.portal.security.user.expanded;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserChangeEvent;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortalSecurityUserAssignmentExpandedRebuildEventListener</code> listens to events that cause assignment/resource changes
 * to a user and rebuilds the expanded table.
 * <p>
 * This also rebuilds notification subscriptions following the security expanded rebuild
 *
 * @author manderson
 */
@Component
public class PortalSecurityUserAssignmentExpandedRebuildEventListener extends BaseEventListener<Event<PortalSecurityUser, Object>> {

	private PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService;

	private PortalSecurityUserService portalSecurityUserService;

	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(PortalSecurityUserAssignmentExpandedRebuildEvent.EVENT_NAME, PortalSecurityUserChangeEvent.USER_ASSIGNMENT_CHANGE_EVENT_NAME, PortalSecurityUserChangeEvent.USER_SECURITY_RESOURCE_CHANGE_EVENT_NAME);
	}


	@Override
	public void onEvent(Event<PortalSecurityUser, Object> event) {
		PortalSecurityUser user = event.getTarget();
		if (user != null) {
			rebuildForPortalSecurityUser(user);
		}
		// If No Specific User - Rebuild All (unless specific event type that allows only running for users with access to an entity or specified role) - run asynchronously
		else {
			boolean rebuildAll = true;
			if (event instanceof PortalSecurityUserAssignmentExpandedRebuildEvent) {
				Integer[] portalEntityIds = ((PortalSecurityUserAssignmentExpandedRebuildEvent) event).getPortalEntityIds();
				Short portalSecurityUserAssignmentRoleId = ((PortalSecurityUserAssignmentExpandedRebuildEvent) event).getPortalSecurityUserAssignmentRoleId();
				if ((portalEntityIds != null && portalEntityIds.length > 0) || portalSecurityUserAssignmentRoleId != null) {
					rebuildAll = false;
					PortalSecurityUserAssignmentSearchForm securityUserAssignmentSearchForm = new PortalSecurityUserAssignmentSearchForm();
					securityUserAssignmentSearchForm.setDisabled(false);
					securityUserAssignmentSearchForm.setPortalEntityIds(portalEntityIds);
					securityUserAssignmentSearchForm.setAssignmentRoleId(portalSecurityUserAssignmentRoleId);
					PortalSecurityUser[] userList = BeanUtils.getPropertyValuesUniqueExcludeNull(getPortalSecurityUserService().getPortalSecurityUserAssignmentList(securityUserAssignmentSearchForm, false), PortalSecurityUserAssignment::getSecurityUser, PortalSecurityUser.class);
					if (userList.length > 0) {
						// Schedule a run for each user
						for (PortalSecurityUser portalSecurityUser : userList) {
							// asynchronous support - schedule a run for each user
							final Date now = DateUtils.addSeconds(new Date(), 10);

							Runner runner = new AbstractStatusAwareRunner("PORTAL_SECURITY_USER_ASSIGNMENT_REBUILD", "USER_" + portalSecurityUser.getId(), now) {

								@Override
								public void run() {
									rebuildForPortalSecurityUser(portalSecurityUser);
									getStatus().setMessage("Security and Notification Subscription Rebuild Completed for User " + portalSecurityUser.getLabelLong());
								}
							};
							getRunnerHandler().rescheduleRunner(runner);
						}
					}
				}
			}

			if (rebuildAll) {
				// asynchronous support - if needing to rebuild all - pushing back 30 seconds in case there is another one that would re-schedule this
				final Date scheduledDate = DateUtils.addSeconds(new Date(), 30);

				Runner runner = new AbstractStatusAwareRunner("PORTAL_SECURITY_USER_ASSIGNMENT_REBUILD", "ALL", scheduledDate) {

					@Override
					public void run() {
						long start = System.currentTimeMillis();
						getPortalSecurityUserAssignmentExpandedRebuildService().rebuildPortalSecurityUserAssignmentExpanded();
						long userRebuildEnd = System.currentTimeMillis();
						getPortalNotificationSubscriptionService().rebuildPortalNotificationSubscriptionList();
						getStatus().setMessage("Rebuild completed for all users: PortalSecurityUserAssignmentExpanded (" + DateUtils.getTimeDifference(userRebuildEnd - start, true) + " ) and PortalNotificationSubscription (" + DateUtils.getTimeDifference(System.currentTimeMillis() - userRebuildEnd, true) + ").");
					}
				};
				getRunnerHandler().rescheduleRunner(runner);
			}
		}
	}


	private void rebuildForPortalSecurityUser(PortalSecurityUser user) {
		getPortalSecurityUserAssignmentExpandedRebuildService().rebuildPortalSecurityUserAssignmentExpandedForUser(user.getId());
		getPortalNotificationSubscriptionService().rebuildPortalNotificationSubscriptionListForUser(user.getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserAssignmentExpandedRebuildService getPortalSecurityUserAssignmentExpandedRebuildService() {
		return this.portalSecurityUserAssignmentExpandedRebuildService;
	}


	public void setPortalSecurityUserAssignmentExpandedRebuildService(PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService) {
		this.portalSecurityUserAssignmentExpandedRebuildService = portalSecurityUserAssignmentExpandedRebuildService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalNotificationSubscriptionService getPortalNotificationSubscriptionService() {
		return this.portalNotificationSubscriptionService;
	}


	public void setPortalNotificationSubscriptionService(PortalNotificationSubscriptionService portalNotificationSubscriptionService) {
		this.portalNotificationSubscriptionService = portalNotificationSubscriptionService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
