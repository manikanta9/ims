package com.clifton.portal.feedback;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.feedback.search.PortalFeedbackQuestionSearchForm;
import com.clifton.portal.feedback.search.PortalFeedbackResultSearchForm;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalFeedbackServiceImpl implements PortalFeedbackService {

	private AdvancedUpdatableDAO<PortalFeedbackQuestion, Criteria> portalFeedbackQuestionDAO;
	private AdvancedUpdatableDAO<PortalFeedbackQuestionOption, Criteria> portalFeedbackQuestionOptionDAO;
	private AdvancedUpdatableDAO<PortalFeedbackResult, Criteria> portalFeedbackResultDAO;

	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal Feedback Question Methods           ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFeedbackQuestion getPortalFeedbackQuestion(int id) {
		PortalFeedbackQuestion bean = getPortalFeedbackQuestionDAO().findByPrimaryKey(id);
		populatePortalFeedbackQuestionOptions(bean);
		return bean;
	}


	@Override
	public List<PortalFeedbackQuestion> getPortalFeedbackQuestionList(PortalFeedbackQuestionSearchForm searchForm, boolean populateOptions) {
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("orderExpanded:asc");
		}
		List<PortalFeedbackQuestion> list = getPortalFeedbackQuestionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (populateOptions) {
			for (PortalFeedbackQuestion question : CollectionUtils.getIterable(list)) {
				populatePortalFeedbackQuestionOptions(question);
			}
		}
		return list;
	}


	private void populatePortalFeedbackQuestionOptions(PortalFeedbackQuestion question) {
		if (question != null && question.isSelectableOption()) {
			question.setOptionList(getPortalFeedbackQuestionOptionListForQuestion(question.getId()));
		}
	}


	@Override
	@Transactional
	public PortalFeedbackQuestion savePortalFeedbackQuestion(PortalFeedbackQuestion bean) {
		List<PortalFeedbackQuestionOption> originalOptionList = null;
		if (!bean.isNewBean()) {
			originalOptionList = getPortalFeedbackQuestionOptionListForQuestion(bean.getId());
		}

		if (bean.isSelectableOption()) {
			ValidationUtils.assertNotEmpty(bean.getOptionList(), "Options are required for selection type questions.");
			for (PortalFeedbackQuestionOption option : CollectionUtils.getIterable(bean.getOptionList())) {
				option.setFeedbackQuestion(bean);
			}
		}
		else {
			bean.setOptionList(null);
		}
		getPortalFeedbackQuestionDAO().save(bean);
		getPortalFeedbackQuestionOptionDAO().saveList(bean.getOptionList(), originalOptionList);
		return bean;
	}


	@Override
	@Transactional
	public void deletePortalFeedbackQuestion(int id) {
		// If any results - can't delete must disable
		PortalFeedbackResultSearchForm searchForm = new PortalFeedbackResultSearchForm();
		searchForm.setFeedbackQuestionId(id);
		if (!CollectionUtils.isEmpty(getPortalFeedbackResultList(searchForm))) {
			throw new ValidationException("This question has some results associated with it and cannot be deleted.  Please disable it instead.");
		}
		getPortalFeedbackQuestionOptionDAO().deleteList(getPortalFeedbackQuestionOptionListForQuestion(id));
		getPortalFeedbackQuestionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////           Portal Feedback Question Options Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalFeedbackQuestionOption> getPortalFeedbackQuestionOptionListForQuestion(int questionId) {
		return getPortalFeedbackQuestionOptionDAO().findByField("feedbackQuestion.id", questionId);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////              Portal Feedback Result Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFeedbackResult getPortalFeedbackResult(int id) {
		return getPortalFeedbackResultDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalFeedbackResult> getPortalFeedbackResultList(PortalFeedbackResultSearchForm searchForm) {
		// Default order to question order expanded
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("orderExpanded:asc");
		}
		return getPortalFeedbackResultDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////              Portal Feedback Entry Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void savePortalFeedbackResultEntry(PortalFeedbackResultEntry bean) {
		List<PortalFeedbackResult> saveList = BeanUtils.filter(bean.getResultList(), result -> result.getResultText() != null);
		if (!CollectionUtils.isEmpty(saveList)) {
			PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
			Date resultDate = new Date();
			for (PortalFeedbackResult result : saveList) {
				result.setSecurityUser(currentUser);
				result.setResultDate(resultDate);
			}
			getPortalFeedbackResultDAO().saveList(saveList);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalFeedbackQuestion, Criteria> getPortalFeedbackQuestionDAO() {
		return this.portalFeedbackQuestionDAO;
	}


	public void setPortalFeedbackQuestionDAO(AdvancedUpdatableDAO<PortalFeedbackQuestion, Criteria> portalFeedbackQuestionDAO) {
		this.portalFeedbackQuestionDAO = portalFeedbackQuestionDAO;
	}


	public AdvancedUpdatableDAO<PortalFeedbackQuestionOption, Criteria> getPortalFeedbackQuestionOptionDAO() {
		return this.portalFeedbackQuestionOptionDAO;
	}


	public void setPortalFeedbackQuestionOptionDAO(AdvancedUpdatableDAO<PortalFeedbackQuestionOption, Criteria> portalFeedbackQuestionOptionDAO) {
		this.portalFeedbackQuestionOptionDAO = portalFeedbackQuestionOptionDAO;
	}


	public AdvancedUpdatableDAO<PortalFeedbackResult, Criteria> getPortalFeedbackResultDAO() {
		return this.portalFeedbackResultDAO;
	}


	public void setPortalFeedbackResultDAO(AdvancedUpdatableDAO<PortalFeedbackResult, Criteria> portalFeedbackResultDAO) {
		this.portalFeedbackResultDAO = portalFeedbackResultDAO;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
