package com.clifton.portal.notification.generator.file;

import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileInvoicePostedNotificationGenerator extends BasePortalFileNotificationGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_INVOICE_POSTED;
	}


	// NO OTHER DIFFERENCES? OPTIONS THAT DIFFER ARE SET ON THE TYPE TABLE
}
