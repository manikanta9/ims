package com.clifton.portal.notification.generator.file;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileInvoiceOverdueNotificationGenerator extends BasePortalFileNotificationGenerator {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_INVOICE_OVERDUE;
	}


	@Override
	public void validatePortalNotificationDefinitionImpl(PortalNotificationDefinition portalNotificationDefinition) {
		String message = "Invoice overdue notification requires recurrence and recurrence interval to be set in order to find status change before given time period, i.e. over 60 days ago.";
		ValidationUtils.assertTrue(portalNotificationDefinition.getNotificationRecurrence() != null && portalNotificationDefinition.getNotificationRecurrence().isRecurrenceIntervalSupported(), message);
		ValidationUtils.assertNotNull(portalNotificationDefinition.getNotificationRecurrenceInterval(), message);
	}


	@Override
	protected void applyAdditionalPortalFileFilters(PortalNotificationGeneratorCommand command, PortalFileExtendedSearchForm searchForm) {
		searchForm.setSourceFieldTypeName(PortalEntityFieldType.FIELD_TYPE_STATUS);
		searchForm.setSourceFieldValue("Presented for Payment");

		if (command.getCommandType().isPreviewOneSample()) {
			searchForm.setOrderBy("publishDate:ASC");
			searchForm.setLimit(1);
		}
		else {
			searchForm.setSourceFieldValueUpdateDateMax(getPreviousRecurrence(command));
		}
	}
}
