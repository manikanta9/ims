package com.clifton.portal.notification.generator.user;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserManagementService;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalUserWelcomeReminderNotificationGenerator</code> re-sends the welcome email every two weeks since it was last sent
 * for users that haven't logged in and accepted the terms of use yet.
 *
 * @author manderson
 */
@Component
public class PortalUserWelcomeReminderNotificationGenerator extends BasePortalUserNotificationGenerator {

	private PortalSecurityUserManagementService portalSecurityUserManagementService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME_REMINDER;
	}


	@Override
	protected boolean isPortalAdministratorRecipientOnly() {
		return false;
	}


	@Override
	public boolean isTermsOfUseRequiredToSend() {
		return false;
	}


	@Override
	protected List<PortalSecurityUser> getPortalSecurityUserList(PortalNotificationGeneratorCommand command) {
		List<PortalSecurityUser> userList = super.getPortalSecurityUserList(command);
		// If no users or we are just previewing one sample - return the list as is
		if (CollectionUtils.isEmpty(userList) || command.getCommandType().isPreviewOneSample()) {
			return userList;
		}
		// Apply Additional filters on users that last received the Welcome Email before last occurrence
		// This needs to be handled separately because the Welcome Reminder email can handle spacing out between defined occurrences itself, but it doesn't check
		// other notification definitions - i.e. the original welcome email
		List<PortalSecurityUser> filteredList = new ArrayList<>();
		PortalNotificationDefinitionType welcomeDefinitionType = getPortalNotificationSetupService().getPortalNotificationDefinitionTypeByName(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME);
		Date startDate = getPreviousRecurrence(command);
		for (PortalSecurityUser user : userList) {
			PortalNotification lastWelcomeNotification = getPortalNotificationService().getPortalNotificationLastForUser(welcomeDefinitionType.getId(), user.getId(), null);
			// Don't send reminder if they haven't received original welcome email yet
			if (lastWelcomeNotification != null) {
				if (DateUtils.isDateBefore(lastWelcomeNotification.getSentDate(), startDate, false)) {
					filteredList.add(user);
				}
			}
		}
		return filteredList;
	}


	@Override
	protected void configurePortalSecurityUserSearchForm(PortalSecurityUserSearchForm searchForm, PortalNotificationGeneratorCommand command) {
		searchForm.setTermsOfUseAccepted(false);
		searchForm.setPasswordResetRequired(true); // Extra confirmation - can't do anything until TOU have been accepted, but they may be changed their password on initial log on but haven't click through the TOU yet to accept
	}


	@Override
	protected Map<String, Object> getNotificationTemplateContextBeansForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser) {
		Map<String, Object> templateContextBeanMap = new HashMap<>();
		if (command.getCommandType().isPreview()) {
			templateContextBeanMap.put("newPassword", "Password123");
		}
		else {
			// We need to generate a new password for the user and save it so we can include it in the email
			templateContextBeanMap.put("newPassword", getPortalSecurityUserManagementService().resetPortalSecurityUserRandomPassword(securityUser));
		}
		return templateContextBeanMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserManagementService getPortalSecurityUserManagementService() {
		return this.portalSecurityUserManagementService;
	}


	public void setPortalSecurityUserManagementService(PortalSecurityUserManagementService portalSecurityUserManagementService) {
		this.portalSecurityUserManagementService = portalSecurityUserManagementService;
	}
}
