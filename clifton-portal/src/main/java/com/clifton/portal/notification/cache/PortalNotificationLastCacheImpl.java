package com.clifton.portal.notification.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationPortalFile;
import com.clifton.portal.notification.PortalNotificationSearchForm;
import com.clifton.portal.notification.PortalNotificationService;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalNotificationLastCache</code> caches for a user and definition type (and optional file) the last notification that was sent
 * <p>
 *
 * @author manderson
 */
@Component
public class PortalNotificationLastCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, ObjectWrapper<Integer>>, PortalNotificationLastCache {

	private CacheHandler<String, ObjectWrapper<Integer>> cacheHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotification getPortalNotificationLastForUser(PortalNotificationService portalNotificationService, short notificationDefinitionTypeId, short securityUserId, Integer portalFileId) {
		String key = getCacheKey(notificationDefinitionTypeId, securityUserId, portalFileId);

		ObjectWrapper<Integer> id = getCacheHandler().get(getCacheName(), key);
		if (id != null) {
			if (!id.isPresent()) {
				// id is not null, but not present then there are NO notifications for the selected parameters, return null, don't look it up in the database again
				return null;
			}
			PortalNotification bean = portalNotificationService.getPortalNotification(id.getObject());
			// If bean is null, then transaction was rolled back, look it up again and reset the cache
			if (bean != null) {
				return bean;
			}
		}

		PortalNotification notification = lookupPortalNotificationLast(portalNotificationService, notificationDefinitionTypeId, securityUserId, portalFileId);
		id = (notification != null ? new ObjectWrapper<>(notification.getId()) : new ObjectWrapper<>());
		getCacheHandler().put(getCacheName(), key, id);
		return notification;
	}


	protected PortalNotification lookupPortalNotificationLast(PortalNotificationService portalNotificationService, short notificationDefinitionTypeId, short securityUserId, Integer portalFileId) {
		PortalNotificationSearchForm searchForm = new PortalNotificationSearchForm();
		searchForm.setRecipientUserId(securityUserId);
		searchForm.setNotificationDefinitionTypeId(notificationDefinitionTypeId);
		searchForm.setPortalFileId(portalFileId);
		searchForm.setOrderBy("sentDate:DESC");
		searchForm.setLimit(1);
		searchForm.setResend(false);
		return CollectionUtils.getFirstElement(portalNotificationService.getPortalNotificationList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                      Observer Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// Note: Key will return null if it's not some that applies to caching (i.e. the eventual re-send option or test email where we don't have a definition)
			if (bean instanceof PortalNotification) {
				if (!((PortalNotification) bean).isResend()) {
					String key = getCacheKey((PortalNotification) bean);
					if (key != null) {
						getCacheHandler().remove(getCacheName(), key);
					}
				}
			}
			else if (bean instanceof PortalNotificationPortalFile) {
				String key = getCacheKey((PortalNotificationPortalFile) bean);
				if (key != null) {
					getCacheHandler().remove(getCacheName(), key);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getCacheKey(short notificationDefinitionTypeId, short securityUserId, Integer portalFileId) {
		return notificationDefinitionTypeId + "_" + securityUserId + (portalFileId != null ? "_" + portalFileId : "");
	}


	private String getCacheKey(PortalNotification portalNotification) {
		if (portalNotification.getNotificationDefinition() == null) {
			return null;
		}
		if (portalNotification.getRecipientUser() == null) {
			return null;
		}
		return getCacheKey(portalNotification.getNotificationDefinition().getDefinitionType().getId(), portalNotification.getRecipientUser().getId(), null);
	}


	private String getCacheKey(PortalNotificationPortalFile portalNotificationPortalFile) {
		if (portalNotificationPortalFile.getReferenceOne().getNotificationDefinition() == null) {
			return null;
		}
		if (portalNotificationPortalFile.getReferenceOne().getRecipientUser() == null) {
			return null;
		}
		return getCacheKey(portalNotificationPortalFile.getReferenceOne().getNotificationDefinition().getDefinitionType().getId(), portalNotificationPortalFile.getReferenceOne().getRecipientUser().getId(), portalNotificationPortalFile.getReferenceTwo().getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public CacheHandler<String, ObjectWrapper<Integer>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, ObjectWrapper<Integer>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
