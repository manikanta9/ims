package com.clifton.portal.notification.generator.file;

import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFilePostedNotificationGenerator extends BasePortalFileNotificationGenerator {


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_FILE_POSTED;
	}
}
