package com.clifton.portal.notification.generator.feedback;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.feedback.PortalFeedbackResult;
import com.clifton.portal.feedback.PortalFeedbackResultEntry;
import com.clifton.portal.feedback.PortalFeedbackService;
import com.clifton.portal.feedback.search.PortalFeedbackResultSearchForm;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.BasePortalNotificationGenerator;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalNotificationFeedbackEntryNotificationGenerator</code> generates a notification to sent to the internal portal mailing list
 * when new feedback was entered.  Sent daily to bulk feedback into one email
 *
 * @author manderson
 */
@Component
public class PortalNotificationFeedbackEntryNotificationGenerator extends BasePortalNotificationGenerator {

	private PortalEmailService portalEmailService;
	private PortalFeedbackService portalFeedbackService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_FEEDBACK_ENTRY;
	}


	@Override
	public List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command) {
		List<PortalFeedbackResult> feedbackResultList = getPortalFeedbackResults(command, false);
		if (CollectionUtils.isEmpty(feedbackResultList) && command.getCommandType().isPreviewOneSample()) {
			feedbackResultList = getPortalFeedbackResults(command, true);
			if (CollectionUtils.isEmpty(feedbackResultList)) {
				throw new ValidationException("There are no feedback results in the system to generate a sample notification for.");
			}
		}
		if (CollectionUtils.isEmpty(feedbackResultList)) {
			return null;
		}
		Map<String, Object> templateContextMap = new HashMap<>();
		templateContextMap.put("feedbackEntryList", convertResultListToEntryList(feedbackResultList));
		PortalNotification notification = populatePortalNotificationForEmailAddress(command, getPortalEmailService().getPortalEmailAddressDefault(), templateContextMap);
		return CollectionUtils.createList(notification);
	}


	private List<PortalFeedbackResult> getPortalFeedbackResults(PortalNotificationGeneratorCommand command, boolean flexible) {
		PortalFeedbackResultSearchForm searchForm = new PortalFeedbackResultSearchForm();
		if (flexible) {
			searchForm.setLimit(10);
		}
		else {
			searchForm.addSearchRestriction("resultDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addWeekDays(command.getRunDateWithTime(), -1));
		}
		return getPortalFeedbackService().getPortalFeedbackResultList(searchForm);
	}


	/**
	 * Convert the list of results (question/answer/user/date) to an entry list which essentially groups the results by user and date
	 * so the notification can display the results per entry
	 */
	private List<PortalFeedbackResultEntry> convertResultListToEntryList(List<PortalFeedbackResult> resultList) {
		Map<String, List<PortalFeedbackResult>> entryMap = BeanUtils.getBeansMap(resultList, portalFeedbackResult -> portalFeedbackResult.getSecurityUser().getLabel() + "_" + DateUtils.fromDateSmart(portalFeedbackResult.getResultDate()));
		List<PortalFeedbackResultEntry> entryList = new ArrayList<>();
		for (List<PortalFeedbackResult> userResultList : entryMap.values()) {
			PortalFeedbackResultEntry entry = new PortalFeedbackResultEntry();
			entry.setResultList(userResultList);
			entryList.add(entry);
		}
		return entryList;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEmailService getPortalEmailService() {
		return this.portalEmailService;
	}


	public void setPortalEmailService(PortalEmailService portalEmailService) {
		this.portalEmailService = portalEmailService;
	}


	public PortalFeedbackService getPortalFeedbackService() {
		return this.portalFeedbackService;
	}


	public void setPortalFeedbackService(PortalFeedbackService portalFeedbackService) {
		this.portalFeedbackService = portalFeedbackService;
	}
}
