package com.clifton.portal.notification.event;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserChangeEvent;
import com.clifton.portal.security.user.PortalSecurityUserManagementService;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PortalNotificationWelcomeEventListener</code> listens for events that are triggered by assignment saves (inserts and assignment role changes)
 * and users accepting the terms of use and determines if that user, or users under that users control should receive the welcome email
 *
 * @author manderson
 */
@Component
public class PortalNotificationWelcomeEventListener extends BaseEventListener<PortalSecurityUserChangeEvent> {

	public static final String PORTAL_NOTIFICATION_WELCOME_EVENT_NAME = "PORTAL_NOTIFICATION_WELCOME_EVENT";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private PortalNotificationService portalNotificationService;
	private PortalNotificationSetupService portalNotificationSetupService;

	private PortalSecurityUserManagementService portalSecurityUserManagementService;
	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * When run for events that also listen for security expanded rebuild, this must run AFTER
	 */
	@Override
	@ValueIgnoringGetter
	public int getOrder() {
		return 200;
	}


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(PORTAL_NOTIFICATION_WELCOME_EVENT_NAME, PortalSecurityUserChangeEvent.TERMS_OF_USE_ACCEPTANCE_EVENT_NAME, PortalSecurityUserChangeEvent.USER_ASSIGNMENT_CHANGE_EVENT_NAME);
	}


	@Override
	public void onEvent(PortalSecurityUserChangeEvent event) {
		try {
			if (event.getTarget() == null) {
				throw new IllegalStateException("Invalid event triggered for Welcome Notification Event Listener.  User is missing.  Event Name is: " + event.getEventName());
			}

			PortalNotificationDefinitionType definitionType = getPortalNotificationSetupService().getPortalNotificationDefinitionTypeByName(PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME);
			StringBuilder results = new StringBuilder(50);
			// User Assignment Change
			if (!CollectionUtils.isEmpty(event.getUserAssignmentList())) {
				boolean triggered = false;
				for (PortalSecurityUserAssignment userAssignment : event.getUserAssignmentList()) {
					if (isPortalSecurityUserWelcomeEmailPermitted(userAssignment, definitionType, true)) {
						getPortalSecurityUserManagementService().sendPortalSecurityUserWelcomeNotification(event.getTarget().getId());
						results.append("Welcome Notification triggered for user [").append(event.getTarget().getLabel()).append("]").append(StringUtils.NEW_LINE);
						triggered = true;
						break;
					}
				}
				if (!triggered) {
					results.append("Welcome Notification skipped for user [").append(event.getTarget().getLabel()).append("]").append(StringUtils.NEW_LINE);
				}
			}

			// If the user is a portal security administrator and they have already accepted the TOU - check if anyone else needs to be sent the email
			if (getPortalSecurityUserService().isPortalSecurityUserSecurityAdmin(event.getTarget()) && event.getTarget().isTermsOfUseAccepted()) {
				PortalSecurityUserAssignmentSearchForm searchForm = new PortalSecurityUserAssignmentSearchForm();
				searchForm.setDisabled(false);
				searchForm.setViewAsUserId(event.getTarget().getId());
				searchForm.setViewSecurityAdminRole(true);
				// Exclude this user
				searchForm.setExcludePortalSecurityUserId(event.getTarget().getId());
				List<PortalSecurityUserAssignment> assignmentList = getPortalSecurityUserService().getPortalSecurityUserAssignmentList(searchForm, false);
				List<PortalSecurityUser> userList = new ArrayList<>();

				for (PortalSecurityUserAssignment userAssignment : CollectionUtils.getIterable(assignmentList)) {
					if (!userList.contains(userAssignment.getSecurityUser())) {
						userList.add(userAssignment.getSecurityUser());
						if (isPortalSecurityUserWelcomeEmailPermitted(userAssignment, definitionType, false)) {
							getPortalSecurityUserManagementService().sendPortalSecurityUserWelcomeNotification(userAssignment.getSecurityUser().getId());
							results.append("Welcome Notification triggered for user [").append(userAssignment.getSecurityUser().getLabel()).append("]").append(StringUtils.NEW_LINE);
						}
						else {
							results.append("Welcome Notification skipped for user [").append(userAssignment.getSecurityUser().getLabel()).append("]").append(StringUtils.NEW_LINE);
						}
					}
				}
			}
			event.setResult(results.toString());
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error determining welcome email notification recipients for " + ((!CollectionUtils.isEmpty(event.getUserAssignmentList())) ? "user assignment(s) save " + StringUtils.collectionToCommaDelimitedString(event.getUserAssignmentList(), PortalSecurityUserAssignment::getLabel) : "user TOU acceptance " + event.getTarget().getLabel()), e);
			event.setResult("There was an error processing your request." + ExceptionUtils.getOriginalMessage(e));
		}
	}


	/**
	 * Returns true if the given user has not accepted the terms of use, has not received the welcome email yet
	 */
	private boolean isPortalSecurityUserWelcomeEmailPermitted(PortalSecurityUserAssignment userAssignment, PortalNotificationDefinitionType definitionType, boolean validateSecurityAdmin) {
		PortalSecurityUser portalSecurityUser = userAssignment.getSecurityUser();
		// External User
		if (!portalSecurityUser.isDisabled() && portalSecurityUser.isTermsOfUseAcceptanceRequired()) {
			// That hasn't accepted terms of use and don't have a password defined
			if (portalSecurityUser.getTermsOfUseAcceptanceDate() == null && StringUtils.isEmpty(portalSecurityUser.getPassword())) {
				// And they haven't been sent the Welcome email yet...
				if (getPortalNotificationService().getPortalNotificationLastForUser(definitionType.getId(), portalSecurityUser.getId(), null) == null) {
					if (validateSecurityAdmin) {
						// And we have an assignment available and they are a security admin
						if (!userAssignment.isDisabled() && userAssignment.getAssignmentRole().isPortalSecurityAdministrator()) {
							return true;
						}
						// Else if they are a portal security admin on another assignment (don't think this could happen because the above would trigger first)
						if (getPortalSecurityUserService().isPortalSecurityUserSecurityAdmin(portalSecurityUser)) {
							return true;
						}
						// Or there is a Portal Security Admin that "manages" them that has accepted the terms of use
						PortalSecurityUserAssignmentSearchForm adminSearchForm = new PortalSecurityUserAssignmentSearchForm();
						adminSearchForm.setPortalEntityIdOrRelatedEntity(userAssignment.getPortalEntity().getId());
						adminSearchForm.setAssignmentRolePortalSecurityAdministrator(true);
						adminSearchForm.setDisabled(false);
						List<PortalSecurityUserAssignment> adminAssignmentList = getPortalSecurityUserService().getPortalSecurityUserAssignmentList(adminSearchForm, false);
						for (PortalSecurityUserAssignment adminAssignment : CollectionUtils.getIterable(adminAssignmentList)) {
							if (adminAssignment.getSecurityUser().getTermsOfUseAcceptanceDate() != null) {
								return true;
							}
						}
						return false;
					}
					// Optionally don't validate security admin - as if the event was triggered by a security admin accepting the terms of use then we know they are good to send
					else {
						return true;
					}
				}
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserManagementService getPortalSecurityUserManagementService() {
		return this.portalSecurityUserManagementService;
	}


	public void setPortalSecurityUserManagementService(PortalSecurityUserManagementService portalSecurityUserManagementService) {
		this.portalSecurityUserManagementService = portalSecurityUserManagementService;
	}


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}
}
