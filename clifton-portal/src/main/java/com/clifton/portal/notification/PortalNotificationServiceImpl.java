package com.clifton.portal.notification;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.notification.cache.PortalNotificationLastCache;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalNotificationServiceImpl implements PortalNotificationService {

	private AdvancedUpdatableDAO<PortalNotification, Criteria> portalNotificationDAO;
	private UpdatableDAO<PortalNotificationPortalFile> portalNotificationPortalFileDAO;

	private PortalNotificationLastCache portalNotificationLastCache;


	////////////////////////////////////////////////////////////////////////////////
	///////////               Portal Notification Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotification getPortalNotification(int id) {
		return getPortalNotificationDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalNotification getPortalNotificationLastForUser(short notificationDefinitionTypeId, short securityUserId, Integer portalFileId) {
		return getPortalNotificationLastCache().getPortalNotificationLastForUser(this, notificationDefinitionTypeId, securityUserId, portalFileId);
	}


	@Override
	public List<PortalNotification> getPortalNotificationList(final PortalNotificationSearchForm searchForm) {
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getPortalFileId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalNotificationPortalFile.class, "nf");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("referenceOne.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("referenceTwo.id", searchForm.getPortalFileId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
		return getPortalNotificationDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	@Transactional
	public PortalNotification savePortalNotification(PortalNotification portalNotification) {
		if (!portalNotification.isNewBean()) {
			throw new ValidationException("Portal notifications can only be inserted, not updated.");
		}
		List<PortalNotificationPortalFile> fileList = portalNotification.getFileList();
		for (PortalNotificationPortalFile file : CollectionUtils.getIterable(fileList)) {
			file.setReferenceOne(portalNotification);
		}
		portalNotification = getPortalNotificationDAO().save(portalNotification);
		getPortalNotificationPortalFileDAO().saveList(fileList);
		portalNotification.setFileList(fileList);
		return portalNotification;
	}


	@Override
	public PortalNotification savePortalNotificationAcknowledged(PortalNotification portalNotification) {
		if (portalNotification.isAcknowledgeable()) {
			portalNotification.setAcknowledged(true);
			return getPortalNotificationDAO().save(portalNotification);
		}
		return portalNotification;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////          Portal Notification Portal File Methods            //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalNotificationPortalFile> getPortalNotificationPortalFileListForNotification(int portalNotificationId) {
		return getPortalNotificationPortalFileDAO().findByField("referenceOne.id", portalNotificationId);
	}


	@Override
	public void deletePortalNotificationPortalFileListForFile(int portalFileId) {
		getPortalNotificationPortalFileDAO().deleteList(getPortalNotificationPortalFileDAO().findByField("referenceTwo.id", portalFileId));
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalNotification, Criteria> getPortalNotificationDAO() {
		return this.portalNotificationDAO;
	}


	public void setPortalNotificationDAO(AdvancedUpdatableDAO<PortalNotification, Criteria> portalNotificationDAO) {
		this.portalNotificationDAO = portalNotificationDAO;
	}


	public UpdatableDAO<PortalNotificationPortalFile> getPortalNotificationPortalFileDAO() {
		return this.portalNotificationPortalFileDAO;
	}


	public void setPortalNotificationPortalFileDAO(UpdatableDAO<PortalNotificationPortalFile> portalNotificationPortalFileDAO) {
		this.portalNotificationPortalFileDAO = portalNotificationPortalFileDAO;
	}


	public PortalNotificationLastCache getPortalNotificationLastCache() {
		return this.portalNotificationLastCache;
	}


	public void setPortalNotificationLastCache(PortalNotificationLastCache portalNotificationLastCache) {
		this.portalNotificationLastCache = portalNotificationLastCache;
	}
}
