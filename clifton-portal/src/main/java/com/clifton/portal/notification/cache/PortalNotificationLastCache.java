package com.clifton.portal.notification.cache;

import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationService;


/**
 * The <code>PortalNotificationLastCache</code> caches for a user and definition type (and optional file) the last notification that was sent
 *
 * @author manderson
 */
public interface PortalNotificationLastCache {


	public PortalNotification getPortalNotificationLastForUser(PortalNotificationService portalNotificationService, short notificationDefinitionTypeId, short securityUserId, Integer portalFileId);
}
