package com.clifton.portal.notification.acknowledgement;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationSearchForm;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.generator.PortalNotificationGenerator;
import com.clifton.portal.notification.generator.locator.PortalNotificationGeneratorLocator;
import com.clifton.portal.notification.setup.PortalNotificationAcknowledgementType;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.tracking.PortalTrackingEventExtended;
import com.clifton.portal.tracking.PortalTrackingService;
import com.clifton.portal.tracking.generator.PortalTrackingEventGenerator;
import com.clifton.portal.tracking.search.PortalTrackingEventExtendedSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
public class PortalNotificationAcknowledgementServiceImpl implements PortalNotificationAcknowledgementService {


	private PortalNotificationGeneratorLocator portalNotificationGeneratorLocator;

	private PortalNotificationService portalNotificationService;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;

	private PortalSecurityUserService portalSecurityUserService;

	private PortalTrackingEventGenerator portalTrackingEventGenerator;

	private PortalTrackingService portalTrackingService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void savePortalNotificationAcknowledgement(int portalNotificationId, Boolean acknowledgement) {
		savePortalNotificationAcknowledgementImpl(portalNotificationId, acknowledgement, false);
	}


	@Override
	public void savePortalNotificationAcknowledgementAdmin(int portalNotificationId, Boolean acknowledgement) {
		savePortalNotificationAcknowledgementImpl(portalNotificationId, acknowledgement, true);
	}


	private void savePortalNotificationAcknowledgementImpl(int portalNotificationId, Boolean acknowledgement, boolean adminOverride) {
		PortalNotification notification = getPortalNotificationService().getPortalNotification(portalNotificationId);
		if (notification.isAcknowledged()) {
			// Do Nothing or Throw Error?  If users share a login email address it's possible 2 could hit at the "same" time?
			return;
		}
		if (!notification.isAcknowledgeable()) {
			throw new ValidationException("Cannot acknowledge notification " + notification.getSubject() + " because it is not acknowledgeable.");
		}
		PortalNotificationAcknowledgementType acknowledgementType = notification.getNotificationDefinition().getAcknowledgementType();
		if (!adminOverride) {
			validateAcknowledgement(acknowledgementType, acknowledgement);
		}

		boolean notificationAcknowledged = adminOverride || (acknowledgement != null || acknowledgementType.isCancelAcknowledgement());
		Set<PortalEntity> portalEntitySet = null;
		if (acknowledgement != null && acknowledgementType.isConfirmEntity()) {
			// If tracking true or false and ack type is confirm entity, let's also track the decline entities (but not pass them on to the other users)
			portalEntitySet = getPortalEntitySetForNotification(notification);
		}

		savePortalNotificationAcknowledgementImpl(notification, acknowledgement, portalEntitySet, notificationAcknowledged);
		if (BooleanUtils.isTrue(acknowledgement) && !CollectionUtils.isEmpty(portalEntitySet)) {
			savePortalNotificationAcknowledgementForConfirmEntity(notification.getNotificationDefinition().getId(), portalEntitySet);
		}
	}


	@Transactional
	protected void savePortalNotificationAcknowledgementImpl(PortalNotification notification, Boolean acknowledgement, Set<PortalEntity> portalEntitySet, boolean notificationAcknowledged) {
		getPortalTrackingEventGenerator().generatePortalTrackingNotificationAcknowledgementEvent(notification.getId(), acknowledgement, portalEntitySet);
		if (notificationAcknowledged) {
			getPortalNotificationService().savePortalNotificationAcknowledged(notification);
		}
	}


	private void validateAcknowledgement(PortalNotificationAcknowledgementType acknowledgementType, Boolean acknowledgement) {
		if (acknowledgement == null) {
			ValidationUtils.assertTrue(acknowledgementType.isCancelUsed(), "Cancel is not a supported acknowledgement");
			return;
		}
		if (BooleanUtils.isTrue(acknowledgement)) {
			ValidationUtils.assertTrue(acknowledgementType.isConfirmUsed(), "Confirm is not a supported acknowledgement");
			return;
		}
		ValidationUtils.assertTrue(acknowledgementType.isDeclineUsed(), "Decline is not a supported acknowledgement");
	}


	private Set<PortalEntity> getPortalEntitySetForNotification(PortalNotification notification) {
		PortalNotificationGenerator generator = getPortalNotificationGeneratorLocator().locate(notification.getNotificationDefinition().getDefinitionType());
		return CollectionUtils.getStream(generator.getPortalEntityListForNotification(notification)).collect(Collectors.toSet());
	}


	private void savePortalNotificationAcknowledgementForConfirmEntity(short portalNotificationDefinitionId, Set<PortalEntity> portalEntitySet) {
		// Upon these acknowledgements, there may be more portal entities available to trigger
		Set<PortalEntity> newPortalEntitySet = new HashSet<>();

		// Run as system user so that it's obvious that the system set the acknowlegement, not an actual user but because of another user's response
		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> {
			// All Unacknowledged Notifications for All Users that have any access to any of the the portal entity ids
			PortalSecurityUserAssignmentSearchForm assignmentSearchForm = new PortalSecurityUserAssignmentSearchForm();
			assignmentSearchForm.setDisabled(false);
			assignmentSearchForm.setPortalEntityIdsOrRelatedEntities(BeanUtils.getBeanIdentityArray(portalEntitySet, Integer.class));
			List<PortalSecurityUser> userList = CollectionUtils.getStream(getPortalSecurityUserService().getPortalSecurityUserAssignmentList(assignmentSearchForm, false)).map(PortalSecurityUserAssignment::getSecurityUser).distinct().collect(Collectors.toList());

			PortalNotificationSearchForm notificationSearchForm = new PortalNotificationSearchForm();
			notificationSearchForm.setRecipientUserIds(BeanUtils.getBeanIdentityArray(userList, Short.class));
			notificationSearchForm.setNotificationDefinitionId(portalNotificationDefinitionId);
			notificationSearchForm.setAcknowledged(false);

			List<PortalNotification> notificationList = getPortalNotificationService().getPortalNotificationList(notificationSearchForm);
			if (!CollectionUtils.isEmpty(notificationList)) {
				Set<PortalEntity> confirmedPortalEntitySet = getPortalEntitySetConfirmedAcknowledgement(portalNotificationDefinitionId);


				// Then for each notification, get their list of Portal entities
				for (PortalNotification notification : CollectionUtils.getIterable(notificationList)) {
					Set<PortalEntity> notificationPortalEntitySet = getPortalEntitySetForNotification(notification);
					// If all have been acknowledged - acknowledge for the user

					// Check if itself was acknowledged OR a parent...
					boolean acknowledgeNotification = CollectionUtils.getStream(notificationPortalEntitySet).allMatch(notificationPortalEntity -> {
						if (confirmedPortalEntitySet.contains(notificationPortalEntity)) {
							return true;
						}
						PortalEntity parent = notificationPortalEntity.getParentPortalEntity();
						while (parent != null) {
							if (confirmedPortalEntitySet.contains(notificationPortalEntity)) {
								return true;
							}
							parent = parent.getParentPortalEntity();
						}
						return false;
					});

					if (acknowledgeNotification) {
						// Add all entities to the list that we might need to check for additional rounds
						newPortalEntitySet.addAll(notificationPortalEntitySet);
						// Save the notification and tracking
						savePortalNotificationAcknowledgementImpl(notification, true, notificationPortalEntitySet, true);
					}
				}
				// Remove the portal entities we just checked to see if there really are more to check
				newPortalEntitySet.removeAll(portalEntitySet);
			}
		});
		if (!CollectionUtils.isEmpty(newPortalEntitySet)) {
			savePortalNotificationAcknowledgementForConfirmEntity(portalNotificationDefinitionId, portalEntitySet);
		}
	}


	private Set<PortalEntity> getPortalEntitySetConfirmedAcknowledgement(short portalNotificationDefinitionId) {
		PortalTrackingEventExtendedSearchForm searchForm = new PortalTrackingEventExtendedSearchForm();
		searchForm.setPortalNotificationDefinitionId(portalNotificationDefinitionId);
		searchForm.setDescription("true");
		return CollectionUtils.getStream(getPortalTrackingService().getPortalTrackingEventExtendedList(searchForm, false)).map(PortalTrackingEventExtended::getPortalEntity).collect(Collectors.toSet());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortalNotificationGeneratorLocator getPortalNotificationGeneratorLocator() {
		return this.portalNotificationGeneratorLocator;
	}


	public void setPortalNotificationGeneratorLocator(PortalNotificationGeneratorLocator portalNotificationGeneratorLocator) {
		this.portalNotificationGeneratorLocator = portalNotificationGeneratorLocator;
	}


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalTrackingEventGenerator getPortalTrackingEventGenerator() {
		return this.portalTrackingEventGenerator;
	}


	public void setPortalTrackingEventGenerator(PortalTrackingEventGenerator portalTrackingEventGenerator) {
		this.portalTrackingEventGenerator = portalTrackingEventGenerator;
	}


	public PortalTrackingService getPortalTrackingService() {
		return this.portalTrackingService;
	}


	public void setPortalTrackingService(PortalTrackingService portalTrackingService) {
		this.portalTrackingService = portalTrackingService;
	}
}
