package com.clifton.portal.notification.generator.file;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileInvoicePaidNotificationGenerator extends PortalFilePostedNotificationGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_INVOICE_PAID;
	}


	@Override
	public void validatePortalNotificationDefinitionImpl(PortalNotificationDefinition portalNotificationDefinition) {
		String message = "Invoice paid notification requires recurrence and recurrence interval to be set in order to find status change within given time period.";
		ValidationUtils.assertTrue(portalNotificationDefinition.getNotificationRecurrence() != null && portalNotificationDefinition.getNotificationRecurrence().isRecurrenceIntervalSupported(), message);
		ValidationUtils.assertNotNull(portalNotificationDefinition.getNotificationRecurrenceInterval(), message);
	}


	@Override
	protected void applyAdditionalPortalFileFilters(PortalNotificationGeneratorCommand command, PortalFileExtendedSearchForm searchForm) {
		searchForm.setSourceFieldTypeName(PortalEntityFieldType.FIELD_TYPE_STATUS);
		searchForm.setSourceFieldValue("Paid");

		if (command.getCommandType().isPreviewOneSample()) {
			searchForm.setOrderBy("publishDate:DESC");
			searchForm.setLimit(1);
		}
		else {
			searchForm.setSourceFieldValueUpdateDateMin(getPreviousRecurrence(command));
		}
	}
}
