package com.clifton.portal.notification.generator.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortalUserConfirmPasswordResetNotificationGenerator</code> notification is sent when an event is triggered by a user that just successfully
 * reset their password
 *
 * @author manderson
 */
@Component
public class PortalUserConfirmPasswordResetNotificationGenerator extends BasePortalNotificationEventGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_CONFIRM_RESET_PASSWORD;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<PortalNotification> generatePortalNotificationListForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser) {
		return CollectionUtils.createList(populatePortalNotificationForUser(command, securityUser, null));
	}
}
