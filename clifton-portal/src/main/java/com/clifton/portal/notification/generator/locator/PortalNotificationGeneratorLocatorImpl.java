package com.clifton.portal.notification.generator.locator;

import com.clifton.core.util.AssertUtils;
import com.clifton.portal.notification.generator.PortalNotificationGenerator;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>PortalNotificationGeneratorLocatorImpl</code> class is responsible for locating {@link com.clifton.portal.notification.generator.PortalNotificationGenerator}
 * implementation beans for specific {@link com.clifton.portal.notification.setup.PortalNotificationDefinitionType}
 *
 * @author manderson
 */
@Component
public class PortalNotificationGeneratorLocatorImpl implements PortalNotificationGeneratorLocator, InitializingBean, ApplicationContextAware {


	/**
	 * The bean cache. This holds the beans indexed by locator key.
	 */
	private final Map<String, PortalNotificationGenerator> retrieverMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationGenerator locate(PortalNotificationDefinitionType definitionType) {
		AssertUtils.assertNotNull(definitionType, "The Portal Notification Definition Type cannot be null.");
		PortalNotificationGenerator generator = getRetrieverMap().get(definitionType.getName());
		AssertUtils.assertNotNull(generator, "Unable to locate the generator for the [%s] portal notification definition type.", definitionType.getName());
		return generator;
	}


	@Override
	public void afterPropertiesSet() {
		// Get beans of target type
		Map<String, PortalNotificationGenerator> beanMap = getApplicationContext().getBeansOfType(PortalNotificationGenerator.class);

		// Index and cache context-managed beans by type name
		for (Map.Entry<String, PortalNotificationGenerator> generatorBeanEntry : beanMap.entrySet()) {
			String generatorBeanName = generatorBeanEntry.getKey();
			PortalNotificationGenerator generatorBean = generatorBeanEntry.getValue();
			String definitionTypeName = generatorBean.getPortalNotificationGeneratorType();
			AssertUtils.assertFalse(getRetrieverMap().containsKey(definitionTypeName), "Cannot register '%s' as a generator for the portal notification definition type '%s' because this type already has a registered generator.", generatorBeanName, definitionTypeName);
			getRetrieverMap().put(definitionTypeName, generatorBean);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, PortalNotificationGenerator> getRetrieverMap() {
		return this.retrieverMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
