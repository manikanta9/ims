package com.clifton.portal.notification.subscription.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.notification.subscription.PortalNotificationSubscription;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalNotificationSubscriptionListByUserAssignmentCache</code> cache is used for client (portal entity specific) users to cache their
 * notification subscriptions for each assignment
 *
 * @author manderson
 */
@Component
public class PortalNotificationSubscriptionListByUserAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<PortalNotificationSubscription, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "userAssignment.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortalNotificationSubscription bean) {
		if (bean.getUserAssignment() != null) {
			return bean.getUserAssignment().getId();
		}
		return null;
	}
}
