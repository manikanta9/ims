package com.clifton.portal.notification.subscription;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionTypeSearchForm;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentRetrievalContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalNotificationSubscriptionRebuildContext</code> object is used to hold common lookups results and helps with processing of notification subscriptions across users
 *
 * @author manderson
 */
public class PortalNotificationSubscriptionRebuildContext {

	private PortalSecurityUserAssignmentRetrievalContext retrievalContext = new PortalSecurityUserAssignmentRetrievalContext();

	/**
	 * Full list of {@link PortalNotificationDefinitionType} objects that allow subscriptions, have an active definition, but do not use file category for subscriptions
	 * For each user, this can then be easily filtered based on security resource access
	 */
	private List<PortalNotificationDefinitionType> subscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions;


	/**
	 * For each entity, the file categories available that have approved files.
	 * For each user, this can then be easily filtered based on security resource access
	 */
	private Map<Integer, List<PortalFileCategory>> portalEntityFileCategoryListMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PortalNotificationDefinitionType> getSubscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions(PortalNotificationSetupService portalNotificationSetupService, Short[] securityResourceIds) {
		if (this.subscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions == null) {
			PortalNotificationDefinitionTypeSearchForm definitionTypeSearchForm = new PortalNotificationDefinitionTypeSearchForm();
			definitionTypeSearchForm.setActiveDefinition(true);
			definitionTypeSearchForm.setSubscriptionAllowed(true);
			definitionTypeSearchForm.setSubscriptionForFileCategory(false);

			this.subscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions = CollectionUtils.asNonNullList(portalNotificationSetupService.getPortalNotificationDefinitionTypeList(definitionTypeSearchForm));
		}
		return BeanUtils.filter(this.subscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions, portalNotificationDefinitionType -> portalNotificationDefinitionType.getSecurityResource() == null || ArrayUtils.contains(securityResourceIds, portalNotificationDefinitionType.getSecurityResource().getId()));
	}


	public List<PortalFileCategory> getPortalFileCategoryListForEntity(PortalFileSetupService portalFileSetupService, int portalEntityId, Short[] securityResourceIds) {
		if (!this.portalEntityFileCategoryListMap.containsKey(portalEntityId)) {
			PortalFileCategorySearchForm fileCategorySearchForm = new PortalFileCategorySearchForm();
			fileCategorySearchForm.setLimitToCategoriesForAssignedPortalEntityId(portalEntityId);
			fileCategorySearchForm.setExcludeParentsOfAssignedPortalEntityId(true);
			fileCategorySearchForm.setLimitToCategoriesWithAvailableApprovedFiles(true);
			fileCategorySearchForm.setFileCategoryType(PortalFileCategoryTypes.FILE_GROUP);
			fileCategorySearchForm.setOrderBy("id:asc"); // Avoid extra joins for default sorting
			this.portalEntityFileCategoryListMap.put(portalEntityId, portalFileSetupService.getPortalFileCategoryList(fileCategorySearchForm));
		}
		return BeanUtils.filter(this.portalEntityFileCategoryListMap.get(portalEntityId), portalFileCategory -> ArrayUtils.contains(securityResourceIds, portalFileCategory.getSecurityResource().getId()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserAssignmentRetrievalContext getRetrievalContext() {
		return this.retrievalContext;
	}
}
