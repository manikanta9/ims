package com.clifton.portal.notification.runner;


import com.clifton.core.context.ApplicationContextService;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class PortalNotificationRunnerFactoryImpl implements PortalNotificationRunnerFactory {

	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationRunner createPortalNotificationRunner(Date runDate, short definitionId) {
		PortalNotificationRunner runner = new PortalNotificationRunner(runDate, definitionId);
		getApplicationContextService().autowireBean(runner);
		return runner;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
