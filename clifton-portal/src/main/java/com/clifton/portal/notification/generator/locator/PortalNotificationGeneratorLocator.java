package com.clifton.portal.notification.generator.locator;

import com.clifton.portal.notification.generator.PortalNotificationGenerator;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;


/**
 * @author manderson
 */
public interface PortalNotificationGeneratorLocator {


	/**
	 * Returns PortalNotificationGenerator for the specified definition type
	 */
	public PortalNotificationGenerator locate(PortalNotificationDefinitionType definitionType);
}
