package com.clifton.portal.notification.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.notification.setup.search.PortalNotificationAcknowledgementTypeSearchForm;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionSearchForm;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionTypeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalNotificationSetupServiceImpl implements PortalNotificationSetupService {

	private AdvancedUpdatableDAO<PortalNotificationDefinition, Criteria> portalNotificationDefinitionDAO;
	private AdvancedUpdatableDAO<PortalNotificationDefinitionType, Criteria> portalNotificationDefinitionTypeDAO;
	private AdvancedUpdatableDAO<PortalNotificationAcknowledgementType, Criteria> portalNotificationAcknowledgementTypeDAO;

	private DaoNamedEntityCache<PortalNotificationDefinitionType> portalNotificationDefinitionTypeCache;

	private PortalFileSetupService portalFileSetupService;


	////////////////////////////////////////////////////////////////////////////////
	////////         Portal Notification Definition Type Methods            ////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationDefinitionType getPortalNotificationDefinitionType(short id) {
		return getPortalNotificationDefinitionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalNotificationDefinitionType getPortalNotificationDefinitionTypeByName(String name) {
		return getPortalNotificationDefinitionTypeCache().getBeanForKeyValueStrict(getPortalNotificationDefinitionTypeDAO(), name);
	}


	@Override
	public List<PortalNotificationDefinitionType> getPortalNotificationDefinitionTypeList(final PortalNotificationDefinitionTypeSearchForm searchForm) {
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveDefinition() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(PortalNotificationDefinition.class, "def");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("def.definitionType.id", criteria.getAlias() + ".id"));
					sub.add(Restrictions.eq("def.active", true));

					if (BooleanUtils.isTrue(searchForm.getActiveDefinition())) {
						criteria.add(Subqueries.exists(sub));
					}
					else {
						criteria.add(Subqueries.notExists(sub));
					}
				}
			}
		};
		return getPortalNotificationDefinitionTypeDAO().findBySearchCriteria(searchFormConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////          Portal Notification Definition Methods              /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationDefinition getPortalNotificationDefinition(short id) {
		return getPortalNotificationDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalNotificationDefinition getPortalNotificationDefinitionByName(String name) {
		return getPortalNotificationDefinitionDAO().findOneByField("name", name);
	}


	@Override
	public List<PortalNotificationDefinition> getPortalNotificationDefinitionList(PortalNotificationDefinitionSearchForm searchForm) {
		return getPortalNotificationDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * See {@link com.clifton.portal.notification.setup.validation.PortalNotificationDefinitionValidator for additional validation}
	 */
	@Override
	public PortalNotificationDefinition savePortalNotificationDefinition(PortalNotificationDefinition bean) {
		if (!bean.isNewBean()) {
			PortalNotificationDefinition originalBean = getPortalNotificationDefinition(bean.getId());
			if (bean.isActive() != originalBean.isActive()) {
				throw new ValidationException("You cannot change active flag for an existing portal notification definition here.  Please use the explicit activate/de-activate menu items.");
			}
			if (originalBean.isActive()) {
				List<String> changedValues = CoreCompareUtils.getNoEqualPropertiesWithSetters(bean, originalBean, false, "name", "description");
				if (!CollectionUtils.isEmpty(changedValues)) {
					throw new ValidationException("You can only change name and description of existing active portal notification definitions.");
				}
			}
		}
		else {
			// new definitions are always inactive
			bean.setActive(false);
		}
		return getPortalNotificationDefinitionDAO().save(bean);
	}


	/**
	 * When activating a definition, if type allows only one active (i.e. doesn't use file categories), then the existing active one will be de-activated
	 */
	@Override
	@Transactional
	public void setPortalNotificationDefinitionActive(short id, boolean active) {
		PortalNotificationDefinition definition = getPortalNotificationDefinition(id);
		if (definition.isActive() == active) {
			throw new ValidationException("Notification Definition [" + definition.getName() + "] is already " + (active ? "active." : "in-active."));
		}
		if (active && !definition.getDefinitionType().isSubscriptionForFileCategory()) {
			PortalNotificationDefinitionSearchForm searchForm = new PortalNotificationDefinitionSearchForm();
			searchForm.setDefinitionTypeId(definition.getDefinitionType().getId());
			searchForm.setActive(true);
			PortalNotificationDefinition existing = CollectionUtils.getOnlyElement(getPortalNotificationDefinitionList(searchForm));
			if (existing != null) {
				existing.setActive(false);
				getPortalNotificationDefinitionDAO().save(existing);
			}
		}
		if (!active) {
			PortalFileCategorySearchForm searchForm = new PortalFileCategorySearchForm();
			searchForm.setNotificationDefinitionId(id);
			List<PortalFileCategory> categoryList = getPortalFileSetupService().getPortalFileCategoryList(searchForm);
			if (!CollectionUtils.isEmpty(categoryList)) {
				throw new ValidationException("You cannot de-activate notification definition [" + definition.getName() + "] because it is being used by the following file categories: " + StringUtils.collectionToCommaDelimitedString(categoryList, PortalFileCategory::getName));
			}
		}
		definition.setActive(active);
		getPortalNotificationDefinitionDAO().save(definition);
	}


	@Override
	public void copyPortalNotificationDefinition(short id, String name) {
		PortalNotificationDefinition bean = getPortalNotificationDefinition(id);
		PortalNotificationDefinition copy = BeanUtils.cloneBean(bean, false, false);
		copy.setName(name);
		copy.setActive(false);
		savePortalNotificationDefinition(copy);
	}


	@Override
	public void deletePortalNotificationDefinition(short id) {
		getPortalNotificationDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////      Portal Notification Acknowledgement Type Methods            ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationAcknowledgementType getPortalNotificationAcknowledgementType(short id) {
		return getPortalNotificationAcknowledgementTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalNotificationAcknowledgementType> getPortalNotificationAcknowledgementTypeList(PortalNotificationAcknowledgementTypeSearchForm searchForm) {
		return getPortalNotificationAcknowledgementTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortalNotificationAcknowledgementType savePortalNotificationAcknowledgementType(PortalNotificationAcknowledgementType bean, boolean ignoreValidation) {
		// Simple Validation
		ValidationUtils.assertTrue(bean.isConfirmUsed() || bean.isDeclineUsed() || bean.isCancelUsed(), "At least one of Confirm, Decline, or Cancel option is required.");
		if (bean.isConfirmEntity()) {
			ValidationUtils.assertTrue(bean.isConfirmUsed(), "Confirm Entity can only be used if Confirm option is used.");
		}
		if (bean.isCancelAcknowledgement()) {
			ValidationUtils.assertTrue(bean.isCancelUsed(), "Cancel Acknowledgement can only be used if Cancel option is used.");
		}

		// If ANY active notification definitions use this acknowledgement type - add a user ignorable warning
		if (!bean.isNewBean() && !ignoreValidation) {
			PortalNotificationAcknowledgementType originalBean = getPortalNotificationAcknowledgementType(bean.getId());
			List<String> changedFields = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false);
			Set<String> checkFields = CollectionUtils.createHashSet("confirmUsed", "declineUsed", "cancelUsed", "confirmEntity", "cancelAcknowledgement");
			if (!CollectionUtils.isEmpty(changedFields) && CollectionUtils.getStream(changedFields).anyMatch(checkFields::contains)) {
				PortalNotificationDefinitionSearchForm searchForm = new PortalNotificationDefinitionSearchForm();
				searchForm.setActive(true);
				searchForm.setAcknowledgementTypeId(bean.getId());
				List<PortalNotificationDefinition> definitionList = getPortalNotificationDefinitionList(searchForm);
				if (!CollectionUtils.isEmpty(definitionList)) {
					throw new UserIgnorableValidationException("This acknowledgement type is used by active notification definition(s): " + StringUtils.collectionToCommaDelimitedString(definitionList, PortalNotificationDefinition::getName) + ".  Are you sure you want to change the options for the acknowledgement type?  Historical Notifications and Acknowledgements will NOT be affected (i.e. If cancel didn't mean acknowledgement before, but now does.  Historical cancels will still NOT be acknowledged.");
				}
			}
		}

		return getPortalNotificationAcknowledgementTypeDAO().save(bean);
	}


	@Override
	public PortalNotificationAcknowledgementType copyPortalNotificationAcknowledgementType(short id, String name) {
		PortalNotificationAcknowledgementType bean = getPortalNotificationAcknowledgementType(id);
		PortalNotificationAcknowledgementType copy = BeanUtils.cloneBean(bean, false, false);
		copy.setName(name);
		return savePortalNotificationAcknowledgementType(copy, false);
	}


	@Override
	public void deletePortalNotificationAcknowledgementType(short id) {
		getPortalNotificationAcknowledgementTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalNotificationDefinition, Criteria> getPortalNotificationDefinitionDAO() {
		return this.portalNotificationDefinitionDAO;
	}


	public void setPortalNotificationDefinitionDAO(AdvancedUpdatableDAO<PortalNotificationDefinition, Criteria> portalNotificationDefinitionDAO) {
		this.portalNotificationDefinitionDAO = portalNotificationDefinitionDAO;
	}


	public AdvancedUpdatableDAO<PortalNotificationDefinitionType, Criteria> getPortalNotificationDefinitionTypeDAO() {
		return this.portalNotificationDefinitionTypeDAO;
	}


	public void setPortalNotificationDefinitionTypeDAO(AdvancedUpdatableDAO<PortalNotificationDefinitionType, Criteria> portalNotificationDefinitionTypeDAO) {
		this.portalNotificationDefinitionTypeDAO = portalNotificationDefinitionTypeDAO;
	}


	public AdvancedUpdatableDAO<PortalNotificationAcknowledgementType, Criteria> getPortalNotificationAcknowledgementTypeDAO() {
		return this.portalNotificationAcknowledgementTypeDAO;
	}


	public void setPortalNotificationAcknowledgementTypeDAO(AdvancedUpdatableDAO<PortalNotificationAcknowledgementType, Criteria> portalNotificationAcknowledgementTypeDAO) {
		this.portalNotificationAcknowledgementTypeDAO = portalNotificationAcknowledgementTypeDAO;
	}


	public DaoNamedEntityCache<PortalNotificationDefinitionType> getPortalNotificationDefinitionTypeCache() {
		return this.portalNotificationDefinitionTypeCache;
	}


	public void setPortalNotificationDefinitionTypeCache(DaoNamedEntityCache<PortalNotificationDefinitionType> portalNotificationDefinitionTypeCache) {
		this.portalNotificationDefinitionTypeCache = portalNotificationDefinitionTypeCache;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
