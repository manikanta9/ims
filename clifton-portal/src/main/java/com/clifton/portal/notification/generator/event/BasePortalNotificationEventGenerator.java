package com.clifton.portal.notification.generator.event;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.BasePortalNotificationGenerator;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public abstract class BasePortalNotificationEventGenerator extends BasePortalNotificationGenerator {


	@Override
	public final List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command) {
		if (command.getCommandType().isPreview() && !command.getCommandType().isPreviewOneSample()) {
			throw new ValidationException("You cannot preview run results for a notification that is triggered by an event. Please use the view sample options.");
		}
		PortalSecurityUser securityUser;
		if (command.getCommandType().isPreviewOneSample()) {
			securityUser = getPortalSecurityUserForSample();
		}
		else {
			securityUser = command.getPortalNotificationEvent().getTarget();
			AssertUtils.assertNotNull(securityUser, "Missing security user this notification is for. The user should have been set by the event.");
		}
		return generatePortalNotificationListForUser(command, securityUser);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected abstract List<PortalNotification> generatePortalNotificationListForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected PortalSecurityUser getPortalSecurityUserForSample() {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setUserRolePortalEntitySpecific(true);
		searchForm.setDisabled(false);
		searchForm.setLimit(1);

		PortalSecurityUser securityUser = CollectionUtils.getFirstElement(getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false));
		ValidationUtils.assertNotNull(securityUser, "There are no Portal Users available to preview this notification for.");
		return securityUser;
	}
}
