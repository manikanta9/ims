package com.clifton.portal.notification.subscription.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.notification.subscription.PortalNotificationSubscription;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalNotificationSubscriptionListByUserCache</code> cache is used for client (portal entity specific) users to cache their
 * notification subscriptions for each user
 *
 * @author manderson
 */
@Component
public class PortalNotificationSubscriptionListByUserCache extends SelfRegisteringSingleKeyDaoListCache<PortalNotificationSubscription, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "userAssignment.securityUser.id";
	}


	@Override
	protected Short getBeanKeyValue(PortalNotificationSubscription bean) {
		if (bean.getUserAssignment() != null) {
			if (bean.getUserAssignment().getSecurityUser() != null) {
				return bean.getUserAssignment().getSecurityUser().getId();
			}
		}
		return null;
	}
}
