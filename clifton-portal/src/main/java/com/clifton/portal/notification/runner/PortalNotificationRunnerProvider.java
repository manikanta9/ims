package com.clifton.portal.notification.runner;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerProvider;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionSearchForm;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PortalNotificationRunnerProvider</code> class returns PortalNotificationDefinition Runner instances that are scheduled for the specified time
 * period.
 */
@Component
public class PortalNotificationRunnerProvider implements RunnerProvider<PortalNotificationDefinition> {

	private PortalNotificationSetupService portalNotificationSetupService;
	private PortalNotificationRunnerFactory portalNotificationRunnerFactory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		List<Runner> results = new ArrayList<>();

		PortalNotificationDefinitionSearchForm searchForm = new PortalNotificationDefinitionSearchForm();
		searchForm.setActive(true);
		searchForm.setTriggeredByEvent(false);
		List<PortalNotificationDefinition> activeDefinitions = getPortalNotificationSetupService().getPortalNotificationDefinitionList(searchForm);

		for (PortalNotificationDefinition definition : CollectionUtils.getIterable(activeDefinitions)) {
			List<Runner> newNotificationRunners = getOccurrencesBetweenForEntity(definition, startDateTime, endDateTime);

			if (!CollectionUtils.isEmpty(newNotificationRunners)) {
				results.addAll(newNotificationRunners);
			}
		}
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(PortalNotificationDefinition entity, Date runnerDate) {
		return getPortalNotificationRunnerFactory().createPortalNotificationRunner(runnerDate, entity.getId());
	}


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(PortalNotificationDefinition entity, Date startDateTime, Date endDateTime) {
		List<Runner> results = new ArrayList<>();

		try {
			List<Date> occurrences = getPortalNotificationDefinitionOccurrencesBetween(entity, startDateTime, endDateTime);
			for (Date date : CollectionUtils.getIterable(occurrences)) {
				Runner runner = createRunnerForEntityAndDate(entity, date);
				results.add(runner);
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to get schedule occurrences of " + entity.getClass() + " for [" + entity.getName() + "] between [" + DateUtils.fromDate(startDateTime) + "] and [" + DateUtils.fromDate(endDateTime) + "].", e);
		}

		return results;
	}


	protected List<Date> getPortalNotificationDefinitionOccurrencesBetween(PortalNotificationDefinition definition, Date startDateTime, Date endDateTime) {
		List<Date> result = new ArrayList<>();

		Time startTime = definition.getStartTime() == null ? new Time(0) : definition.getStartTime();
		Time endTime = definition.getEndTime() == null ? new Time((24 * 60 - 1) * 60 * 1000) : definition.getEndTime();

		// date is a business day and is between start and end date to calculate the next run
		Date date = DateUtils.clearTime(startDateTime);

		Date runDateTime = DateUtils.setTime(date, startTime);
		Date endRunDateTime = DateUtils.setTime(date, endTime);
		Integer runFrequencyInMinutes = definition.getRunFrequencyInMinutes();

		// As long as we are before end time, go through the run times
		while (DateUtils.compare(runDateTime, endDateTime, true) <= 0 && DateUtils.compare(runDateTime, endRunDateTime, true) <= 0) {
			// If we are before start time - move to next recurrence - if none return null
			if (DateUtils.compare(runDateTime, startDateTime, true) < 0) {
				if (runFrequencyInMinutes == null) {
					return null;
				}
				runDateTime = DateUtils.addMinutes(runDateTime, runFrequencyInMinutes);
			}
			else {
				// As long as not the weekend add it to the result
				if (!DateUtils.isWeekend(runDateTime)) {
					result.add(runDateTime);
				}
				// If ran only once - return the result
				if (runFrequencyInMinutes == null) {
					return result;
				}
				// Otherwise move on to next recurrence
				runDateTime = DateUtils.addMinutes(runDateTime, runFrequencyInMinutes);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}


	public PortalNotificationRunnerFactory getPortalNotificationRunnerFactory() {
		return this.portalNotificationRunnerFactory;
	}


	public void setPortalNotificationRunnerFactory(PortalNotificationRunnerFactory portalNotificationRunnerFactory) {
		this.portalNotificationRunnerFactory = portalNotificationRunnerFactory;
	}
}
