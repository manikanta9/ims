package com.clifton.portal.notification.generator;

import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;

import java.util.List;


/**
 * The <code>PortalNotificationGenerator</code> interfaces defines the methods to be implemented by each notification generator.
 * Each generator is associated with a particular {@link com.clifton.portal.notification.setup.PortalNotificationDefinitionType}
 *
 * @author manderson
 */
public interface PortalNotificationGenerator {


	/**
	 * Returns the notification definition type this generator is for
	 */
	public abstract String getPortalNotificationGeneratorType();


	/**
	 * Validates the properties for the definition for required fields.
	 * For example invoice overdue and paid notification requires a recurrence interval
	 */
	public void validatePortalNotificationDefinition(PortalNotificationDefinition portalNotificationDefinition);


	/**
	 * Creates a list of PortalNotifications based on command options
	 */
	public List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command);


	/**
	 * Returns true if popUpNotificationSupported = true and the notification can support tracking portal entities
	 * for the notification.
	 */
	public boolean isPopUpNotificationConfirmEntitySupported();


	/**
	 * Used for popUpNotificationConfirmEntitySupported = true to find the portal entities applicable to the notification
	 */
	public List<PortalEntity> getPortalEntityListForNotification(PortalNotification notification);
}
