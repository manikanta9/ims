package com.clifton.portal.notification.generator.user;

import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalAdministratorReviewUsersNotificationGenerator</code> generates notifications for all portal admin users
 * based on configured dates from when the user last received the notification, or was created (if never received)
 * that have at least one user under their "ownership"
 *
 * @author manderson
 */
@Component
public class PortalAdministratorReviewUsersNotificationGenerator extends BasePortalUserNotificationGenerator {


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_ADMIN_REVIEW_USERS;
	}


	@Override
	protected boolean isPortalAdministratorRecipientOnly() {
		return true;
	}
}
