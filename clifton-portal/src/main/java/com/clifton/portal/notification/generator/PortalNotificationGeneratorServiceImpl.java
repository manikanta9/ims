package com.clifton.portal.notification.generator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.MimeContentTypes;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.generator.locator.PortalNotificationGeneratorLocator;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalNotificationGeneratorServiceImpl implements PortalNotificationGeneratorService {

	private EmailHandler emailHandler;

	private PortalNotificationGeneratorLocator portalNotificationGeneratorLocator;
	private PortalNotificationService portalNotificationService;
	private PortalNotificationSetupService portalNotificationSetupService;

	private PortalSecurityUserService portalSecurityUserService;

	private RunnerHandler runnerHandler;

	/**
	 * Directory where the image files can be located Email Text with the following syntax "[IMAGE_" + fileNameWithoutExtension + "]" will be replaced with <img
	 * src="cid:FILE_NAME_WITHOUT_EXTENSION_HERE" /> and files in that directory that match by name will be add them as attachments to the email and inserted
	 * inline.
	 */
	private String emailImageDirectory;

	/**
	 * On our "DEMO" environment the emails could potentially send because it out in the DMZ
	 * For these cases the server is set to this value.  To prevent cluttering logs, when it's set don't even attempt to send and
	 * just set the error on the notification without logging
	 */
	private static final String INVALID_SMTP_HOST = "doesnotexist.paraport.com";

	////////////////////////////////////////////////////////////////////////////////
	//////////          Portal Notification Generation Methods            //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalNotification> previewPortalNotificationList(PortalNotificationGeneratorCommand command) {
		ValidationUtils.assertNotNull(command.getCommandType(), "Command Type is required");
		ValidationUtils.assertTrue(command.getCommandType().isPreview(), "Command Type [" + command.getCommandType().name() + "] is invalid.  Command type must be a Preview.");
		PortalNotificationGenerator generator = locatePortalNotificationGenerator(command);
		return doGeneratePortalNotificationList(generator, command);
	}


	@Override
	public String generatePortalNotificationResend(PortalNotificationGeneratorCommand command, Integer notificationId) {
		PortalNotification notification = getPortalNotificationService().getPortalNotification(notificationId);

		// You cannot resend a Pop Up Notification
		ValidationUtils.assertFalse(notification.getNotificationDefinition().isPopUpNotification(), "Re-Sending Pop-Up Notifications is not supported.");

		if (command.getCommandType().isSend()) {
			PortalSecurityUser securityUser = notification.getRecipientUser();
			if (securityUser != null) {
				ValidationUtils.assertFalse(notification.getNotificationDefinition().isSkipPendingTermination() && getPortalSecurityUserService().isPortalSecurityUserPendingTermination(securityUser), "User is pending termination!");
				ValidationUtils.assertFalse(securityUser.isDisabled(), "User is disabled!");
			}

			PortalNotification newNotification = BeanUtils.cloneBean(notification, false, false);
			newNotification.setErrorMessage(null);

			//Set notifications parent as the original notification
			newNotification.setParent(notification.getRootParent());
			sendNotificationWithHandling(command, newNotification);
			return "Notification is being generated";
		}
		return "";
	}


	@Override
	public String generatePortalNotificationList(PortalNotificationGeneratorCommand command) {
		ValidationUtils.assertNotNull(command.getCommandType(), "Command Type is required");
		ValidationUtils.assertFalse(command.getCommandType().isPreview(), "Command Type [" + command.getCommandType().name() + "] is invalid.  Command type must not be a Preview.");
		PortalNotificationGenerator generator = locatePortalNotificationGenerator(command);
		if (!command.getPortalNotificationDefinition().isActive()) {
			throw new ValidationException("Selected Definition [" + command.getPortalNotificationDefinition().getName() + "] is not currently active.  You cannot run a notification that is not active.  Please use preview option if you want to review results.");
		}

		final String runId = "DEF-" + command.getPortalNotificationDefinition().getId();
		final Date now = new Date();

		Runner runner = new AbstractStatusAwareRunner("NOTIFICATION-GENERATOR", runId, now) {

			@Override
			public void run() {
				try {
					List<PortalNotification> result = doGeneratePortalNotificationList(generator, command);
					getStatus().setMessage(CollectionUtils.getSize(result) + " Notifications generated.");
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.error(getClass(), "Error generating notification for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Notifications are being generated for selected definition.";
	}


	@Override
	public String generatePortalTestNotification(PortalNotificationGeneratorCommand command) {
		PortalNotification notification = new PortalNotification();
		if (command.getPortalNotificationDefinitionId() != null) {
			notification.setNotificationDefinition(getPortalNotificationSetupService().getPortalNotificationDefinition(command.getPortalNotificationDefinitionId()));
		}
		else {
			notification.setNotificationDefinition(getPortalNotificationSetupService().getPortalNotificationDefinitionByName("Test Email"));
		}

		// Recipient User not fully hydrated during request
		PortalSecurityUser recipientUser = getPortalSecurityUserService().getPortalSecurityUser(command.getRecipientUser().getId());

		notification.setRecipientUser(recipientUser);
		notification.setRecipientEmailAddress(recipientUser.getEmailAddress());
		notification.setSubject(command.getSubjectTemplate());
		notification.setText(command.getBodyTemplate());

		sendNotificationWithHandling(command, notification);
		return "Test Notification generated";
	}


	private List<PortalNotification> doGeneratePortalNotificationList(PortalNotificationGenerator generator, PortalNotificationGeneratorCommand command) {
		if (command.getPortalNotificationDefinition().isPopUpNotification() && !command.getCommandType().isPopUpNotificationSupported()) {
			throw new ValidationException("Pop-Up Notifications do not support " + command.getCommandType().name() + " commands.");
		}

		List<PortalNotification> notificationList = generator.generatePortalNotificationList(command);

		if (command.getCommandType().isSend()) {
			for (PortalNotification notification : CollectionUtils.getIterable(notificationList)) {
				sendNotificationWithHandling(command, notification);
				if (command.getCommandType().isPreview() && !StringUtils.isEmpty(notification.getErrorMessage())) {
					throw new ValidationException("Error sending email: " + notification.getErrorMessage());
				}
			}
		}
		return notificationList;
	}


	private void sendNotificationWithHandling(PortalNotificationGeneratorCommand command, PortalNotification notification) {
		try {
			notification.setSentDate(new Date());
			// Pop Ups Never Actually Send An Email
			if (!notification.getNotificationDefinition().isPopUpNotification()) {
				if (StringUtils.isEqualIgnoreCase(INVALID_SMTP_HOST, getEmailHandler().getSmtpHost())) {
					notification.setErrorMessage("No attempt made.  Invalid SMTP Host: " + getEmailHandler().getSmtpHost());
				}
				else {
					getEmailHandler().send(getEmailFromPortalNotification(command, notification));
				}
			}
		}
		catch (Throwable e) {
			String errorMessage = ExceptionUtils.getOriginalMessage(e);
			notification.setErrorMessage(errorMessage);
			LogUtils.errorOrInfo(getClass(), errorMessage, e);
		}
		finally {
			if (!command.getCommandType().isPreview()) {
				getPortalNotificationService().savePortalNotification(notification);
			}
		}
	}


	private Email getEmailFromPortalNotification(PortalNotificationGeneratorCommand command, PortalNotification notification) {
		String fromAddress = getEmailHandler().getDefaultFromAddress(); // No overrides?
		String[] toAddresses = notification.getRecipientUser() != null ? new String[]{notification.getRecipientUser().getEmailAddress()} : notification.getRecipientEmailAddress().split(";");
		// If sending a preview - send to the current user
		if (command.getCommandType().isPreview()) {
			toAddresses = new String[]{getPortalSecurityUserService().getPortalSecurityUserCurrent().getEmailAddress()};
		}
		String subject = notification.getSubject();
		MimeMultipart content = new MimeMultipart("mixed");

		try {
			MimeBodyPart textPart = new MimeBodyPart();
			String notificationText = notification.getText();
			Resource[] resources = new PathMatchingResourcePatternResolver().getResources("classpath*:" + getEmailImageDirectory() + "/*");
			for (Resource resource : resources) {
				ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(resource.getInputStream(), URLConnection.guessContentTypeFromName(resource.getFilename()));
				String fileName = FileUtils.getFileNameWithoutExtension(resource.getFilename());
				if (notificationText.contains("[IMAGE_" + fileName + "]")) {
					notificationText = notificationText.replace("[IMAGE_" + fileName + "]", "<img src=\"cid:" + fileName + "\" />");
					MimeBodyPart imagePart = new MimeBodyPart();
					imagePart.setDataHandler(new DataHandler(byteArrayDataSource));
					imagePart.setContentID("<" + fileName + ">");
					imagePart.setDisposition(MimeBodyPart.INLINE);
					content.addBodyPart(imagePart);
				}
			}

			textPart.setContent(notificationText, MimeContentTypes.TEXT_HTML.getValue());
			content.addBodyPart(textPart);
		}
		catch (Exception e) {
			throw new RuntimeException("Error building Email " + ExceptionUtils.getOriginalMessage(e), e);
		}
		MimeContentTypes contentType = MimeContentTypes.TEXT_HTML;
		return new Email(fromAddress, toAddresses, subject, content, contentType);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Generator Locator Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalNotificationGenerator locatePortalNotificationGenerator(PortalNotificationGeneratorCommand command) {
		if (command.getPortalNotificationDefinition() == null) {
			if (command.getPortalNotificationDefinitionId() != null) {
				command.setPortalNotificationDefinition(getPortalNotificationSetupService().getPortalNotificationDefinition(command.getPortalNotificationDefinitionId()));
			}
		}
		ValidationUtils.assertNotNull(command.getPortalNotificationDefinition(), "Portal Definition is required to locate the generator.");
		// Throws exception if missing
		return getPortalNotificationGeneratorLocator().locate(command.getPortalNotificationDefinition().getDefinitionType());
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationGeneratorLocator getPortalNotificationGeneratorLocator() {
		return this.portalNotificationGeneratorLocator;
	}


	public void setPortalNotificationGeneratorLocator(PortalNotificationGeneratorLocator portalNotificationGeneratorLocator) {
		this.portalNotificationGeneratorLocator = portalNotificationGeneratorLocator;
	}


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}


	public EmailHandler getEmailHandler() {
		return this.emailHandler;
	}


	public void setEmailHandler(EmailHandler emailHandler) {
		this.emailHandler = emailHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public String getEmailImageDirectory() {
		return this.emailImageDirectory;
	}


	public void setEmailImageDirectory(String emailImageDirectory) {
		this.emailImageDirectory = emailImageDirectory;
	}
}
