package com.clifton.portal.notification.generator.user;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>PortalAdministratorInstitutionalNotificationGenerator</code> generates notifications for all institutional portal admin users
 * Current Use Case is for a one-time pop up acknowledgable notification
 *
 * @author manderson
 */
@Component
public class PortalAdministratorInstitutionalNotificationGenerator extends BasePortalUserNotificationGenerator {


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_ADMIN_INSTITUTIONAL;
	}


	@Override
	protected boolean isPortalAdministratorRecipientOnly() {
		return true;
	}


	@Override
	protected void configurePortalSecurityUserSearchForm(PortalSecurityUserSearchForm searchForm, PortalNotificationGeneratorCommand command) {
		searchForm.setAssignmentPortalEntityViewTypeId(getPortalEntityViewType().getId());
	}


	@Override
	public boolean isPopUpNotificationConfirmEntitySupported() {
		return true;
	}


	@Override
	public List<PortalEntity> getPortalEntityListForNotification(PortalNotification notification) {
		PortalSecurityUserAssignmentSearchForm searchForm = new PortalSecurityUserAssignmentSearchForm();
		searchForm.setPortalSecurityUserId(notification.getRecipientUser().getId());
		searchForm.setAssignmentRolePortalSecurityAdministrator(true);
		searchForm.setDisabled(false);
		if (isPortalAdministratorRecipientOnly()) {
			searchForm.setAssignmentRolePortalSecurityAdministrator(true);
		}
		searchForm.setEntityViewTypeId(getPortalEntityViewType().getId());
		return CollectionUtils.getStream(getPortalSecurityUserService().getPortalSecurityUserAssignmentList(searchForm, false)).map(PortalSecurityUserAssignment::getPortalEntity).collect(Collectors.toList());
	}


	private PortalEntityViewType getPortalEntityViewType() {
		return getPortalEntitySetupService().getPortalEntityViewTypeByName(PortalEntityViewType.ENTITY_VIEW_TYPE_INSTITUTIONAL);
	}
}
