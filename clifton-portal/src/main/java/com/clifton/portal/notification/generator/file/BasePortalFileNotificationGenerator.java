package com.clifton.portal.notification.generator.file;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.template.PortalFileTemplateGenerator;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationPortalFile;
import com.clifton.portal.notification.generator.BasePortalNotificationGenerator;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public abstract class BasePortalFileNotificationGenerator extends BasePortalNotificationGenerator {

	private PortalFileService portalFileService;
	private PortalFileTemplateGenerator portalFileTemplateGenerator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validatePortalNotificationDefinitionImpl(PortalNotificationDefinition portalNotificationDefinition) {
		ValidationUtils.assertNotNull(portalNotificationDefinition.getFileApprovedDaysBack(), "Files posted notification requires file approval days back to be set.");
	}


	@Override
	public final List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command) {
		List<PortalFileExtended> portalFileExtendedList = getPortalFileExtendedList(command); // Files available to be sent

		Map<PortalSecurityUser, List<PortalFileExtended>> userFileListMap = new HashMap<>();
		// If previewing a sample, just create a test user object as the recipient and put the 5 examples in the context
		if (command.getCommandType().isPreviewOneSample()) {
			for (PortalFileExtended portalFileExtended : CollectionUtils.getIterable(portalFileExtendedList)) {
				portalFileExtended.setDownloadDisplayName(getPortalFileTemplateGenerator().generatePortalFileName(portalFileExtended));
			}
			PortalSecurityUser testUser = new PortalSecurityUser();
			testUser.setEmailAddress("testEmail@test.com");
			testUser.setLastName("User");
			testUser.setFirstName("Test");
			userFileListMap.put(testUser, portalFileExtendedList);
		}
		else {
			for (PortalFileExtended portalFileExtended : CollectionUtils.getIterable(portalFileExtendedList)) {
				boolean add = false;
				List<PortalSecurityUser> fileUserList = getPortalSecurityUserListForFile(portalFileExtended);
				for (PortalSecurityUser securityUser : CollectionUtils.getIterable(fileUserList)) {
					if (isNotificationSubscribedAndValidToSend(command, securityUser, portalFileExtended)) {
						add = true;
						if (!userFileListMap.containsKey(securityUser)) {
							userFileListMap.put(securityUser, CollectionUtils.createList(portalFileExtended));
						}
						else {
							userFileListMap.get(securityUser).add(portalFileExtended);
						}
					}
				}
				if (add) {
					portalFileExtended.setDownloadDisplayName(getPortalFileTemplateGenerator().generatePortalFileName(portalFileExtended));
				}
			}
		}
		List<PortalNotification> notificationList = new ArrayList<>();
		for (Map.Entry<PortalSecurityUser, List<PortalFileExtended>> portalSecurityUserListEntry : userFileListMap.entrySet()) {
			PortalNotification notification = generatePortalNotificationForUserAndFileList(command, portalSecurityUserListEntry.getKey(), portalSecurityUserListEntry.getValue());
			// Only Need One sample and do not need files
			if (command.getCommandType().isPreviewOneSample()) {
				return CollectionUtils.createList(notification);
			}
			notificationList.add(notification);
		}
		return notificationList;
	}


	protected PortalNotification generatePortalNotificationForUserAndFileList(PortalNotificationGeneratorCommand command, PortalSecurityUser portalSecurityUser, List<PortalFileExtended> portalFileList) {
		Map<String, Object> templateContextMap = new HashMap<>();
		templateContextMap.put("fileList", portalFileList);
		PortalNotification notification = populatePortalNotificationForUser(command, portalSecurityUser, templateContextMap);
		// Only Need One sample and do not need files
		if (command.getCommandType().isPreviewOneSample()) {
			return notification;
		}
		addPortalFileListToNotification(notification, portalFileList);
		return notification;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<PortalFileExtended> getPortalFileExtendedList(PortalNotificationGeneratorCommand command) {
		PortalFileExtendedSearchForm searchForm = getPortalFileSearchForm(command, false);
		List<PortalFileExtended> fileExtendedList = getPortalFileService().getPortalFileExtendedList(searchForm);
		if (CollectionUtils.isEmpty(fileExtendedList) && command.getCommandType().isPreviewOneSample()) {
			// If nothing (which is possible especially if it's inactive) allow more flexibility with which files
			// any file with notifications
			searchForm = getPortalFileSearchForm(command, true);
			fileExtendedList = getPortalFileService().getPortalFileExtendedList(searchForm);
		}
		return fileExtendedList;
	}


	/**
	 * Populates the search form with filters to get the results.  flexible is used for previous one sample only when there isn't any real data
	 * so we just attempt to put some files regardless of what definition they currently use to preview data for the sample
	 */
	protected PortalFileExtendedSearchForm getPortalFileSearchForm(PortalNotificationGeneratorCommand command, boolean flexible) {
		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		if (command.getPortalNotificationDefinition().getDefinitionType().isSubscriptionForFileCategory()) {
			if (flexible) {
				searchForm.setNotificationUsed(true);
			}
			else {
				searchForm.setNotificationDefinitionId(command.getPortalNotificationDefinition().getId());
			}
		}
		searchForm.setApproved(true);

		if (!flexible && command.getPortalNotificationDefinition().getFileApprovedDaysBack() != null) {
			Date date = command.getRunDateWithoutTime();
			// If only one day, use weekday
			if (command.getPortalNotificationDefinition().getFileApprovedDaysBack() == 1) {
				date = DateUtils.addWeekDays(date, -command.getPortalNotificationDefinition().getFileApprovedDaysBack());
			}
			else {
				date = DateUtils.addDays(date, -command.getPortalNotificationDefinition().getFileApprovedDaysBack());
			}
			searchForm.addSearchRestriction(new SearchRestriction("approvalDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, date));
		}

		if (command.getPortalNotificationDefinition().getDefinitionType().getSecurityResource() != null) {
			searchForm.setSecurityResourceId(command.getPortalNotificationDefinition().getDefinitionType().getSecurityResource().getId());
		}
		searchForm.setOrderBy("fileCategoryOrderExpanded#reportDate:DESC");
		applyAdditionalPortalFileFilters(command, searchForm);
		return searchForm;
	}


	protected void applyAdditionalPortalFileFilters(PortalNotificationGeneratorCommand command, PortalFileExtendedSearchForm searchForm) {
		if (command.getCommandType().isPreviewOneSample()) {
			searchForm.setOrderBy("publishDate:DESC");
			searchForm.setLimit(5);
		}
	}


	protected List<PortalSecurityUser> getPortalSecurityUserListForFile(PortalFileExtended portalFileExtended) {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setDisabled(false);
		searchForm.setUserRolePortalEntitySpecific(true);
		// If the file applies to a specific entity, let's limit the users to that entity as well
		return getPortalFileService().getPortalFileSecurityUserList(portalFileExtended.getPortalFileId(), searchForm, (portalFileExtended.getAssignedPortalEntity() != null ? portalFileExtended.getAssignedPortalEntity().getId() : null));
	}


	protected void addPortalFileListToNotification(PortalNotification notification, List<PortalFileExtended> fileExtendedList) {
		List<PortalNotificationPortalFile> portalNotificationFileList = new ArrayList<>();
		for (PortalFileExtended fileExtended : CollectionUtils.getIterable(fileExtendedList)) {
			PortalNotificationPortalFile notificationPortalFile = new PortalNotificationPortalFile();
			notificationPortalFile.setReferenceOne(notification);
			notificationPortalFile.setReferenceTwo(getPortalFileService().getPortalFile(fileExtended.getPortalFileId()));
			portalNotificationFileList.add(notificationPortalFile);
		}
		notification.setFileList(portalNotificationFileList);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileTemplateGenerator getPortalFileTemplateGenerator() {
		return this.portalFileTemplateGenerator;
	}


	public void setPortalFileTemplateGenerator(PortalFileTemplateGenerator portalFileTemplateGenerator) {
		this.portalFileTemplateGenerator = portalFileTemplateGenerator;
	}
}
