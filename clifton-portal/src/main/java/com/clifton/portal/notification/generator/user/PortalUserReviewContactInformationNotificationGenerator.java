package com.clifton.portal.notification.generator.user;

import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalUserReviewContactInformationNotificationGenerator extends BasePortalUserNotificationGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_REVIEW_CONTACT_INFO;
	}


	@Override
	protected boolean isPortalAdministratorRecipientOnly() {
		return false;
	}
}
