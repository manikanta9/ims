package com.clifton.portal.notification.generator.file;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalAdministratorNewFileCategoryAvailableNotificationGenerator</code> will generate a notification for the first approved file for a portal entity for portal administrators
 * Will also list all users under their control and if that user has access to the new file category.
 *
 * @author manderson
 */
@Component
public class PortalAdministratorNewFileCategoryAvailableNotificationGenerator extends BasePortalFileNotificationGenerator {


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_NEW_FILE_CATEGORY_AVAILABLE;
	}


	@Override
	protected PortalNotification generatePortalNotificationForUserAndFileList(PortalNotificationGeneratorCommand command, PortalSecurityUser portalSecurityUser, List<PortalFileExtended> portalFileList) {
		Map<String, Object> templateContextMap = new HashMap<>();
		PortalSecurityUserAssignmentSearchForm securityUserAssignmentSearchForm = new PortalSecurityUserAssignmentSearchForm();
		if (command.getCommandType().isPreviewOneSample()) {
			securityUserAssignmentSearchForm.setLimit(5);
			securityUserAssignmentSearchForm.setViewAsEntityId(portalFileList.get(0).getAssignedPortalEntity().getId());
		}
		else {
			securityUserAssignmentSearchForm.setViewAsUserId(portalSecurityUser.getId());
		}
		securityUserAssignmentSearchForm.setDisabled(false);
		List<PortalSecurityUserAssignment> portalSecurityUserAssignmentList = getPortalSecurityUserService().getPortalSecurityUserAssignmentList(securityUserAssignmentSearchForm, true);

		List<PortalFileCategory> categoryList = new ArrayList<>();
		Map<String, PortalFileCategoryPortalSecurityUserPermission> userPermissionMap = new HashMap<>();

		for (PortalFileExtended portalFileExtended : CollectionUtils.getIterable(portalFileList)) {
			if (!categoryList.contains(portalFileExtended.getFileCategory())) {
				categoryList.add(portalFileExtended.getFileCategory());
			}
			for (PortalSecurityUserAssignment userAssignment : CollectionUtils.getIterable(portalSecurityUserAssignmentList)) {
				if (isUserAssignmentApplyToEntity(userAssignment, portalFileExtended.getAssignedPortalEntity())) {
					for (PortalSecurityUserAssignmentResource userAssignmentResource : CollectionUtils.getIterable(userAssignment.getUserAssignmentResourceList())) {
						if (userAssignmentResource.getSecurityResource().equals(portalFileExtended.getFileCategory().getSecurityResource())) {
							PortalFileCategoryPortalSecurityUserPermission fileCategoryPortalSecurityUserPermission = new PortalFileCategoryPortalSecurityUserPermission(portalFileExtended.getFileCategory(), userAssignment, userAssignmentResource);
							String key = fileCategoryPortalSecurityUserPermission.getUniqueKey();
							if (!userPermissionMap.containsKey(key)) {
								userPermissionMap.put(key, fileCategoryPortalSecurityUserPermission);
							}
							userPermissionMap.get(key).addAssignedPortalEntity(portalFileExtended.getAssignedPortalEntity());
						}
					}
				}
			}
		}
		List<PortalFileCategoryPortalSecurityUserPermission> fileCategoryPermissionList = new ArrayList<>(userPermissionMap.values());
		fileCategoryPermissionList = BeanUtils.sortWithFunctions(fileCategoryPermissionList, CollectionUtils.createList(PortalFileCategoryPortalSecurityUserPermission::getCategoryName, fileCategoryUserPermission -> fileCategoryUserPermission.getPortalSecurityUserAssignmentResource().getSecurityUserAssignment().getSecurityUser().getLabel()), CollectionUtils.createList(true, true));
		templateContextMap.put("fileCategoryPermissionList", fileCategoryPermissionList);
		// Track the category list so we know if there is one or multiple for wording in the message
		templateContextMap.put("categoryList", categoryList);
		PortalNotification notification = populatePortalNotificationForUser(command, portalSecurityUser, templateContextMap);
		// Only Need One sample and do not need files
		if (command.getCommandType().isPreviewOneSample()) {
			return notification;
		}


		addPortalFileListToNotification(notification, portalFileList);
		return notification;
	}


	@Override
	protected void applyAdditionalPortalFileFilters(PortalNotificationGeneratorCommand command, PortalFileExtendedSearchForm searchForm) {
		super.applyAdditionalPortalFileFilters(command, searchForm);

		//exclude portal "contact us" files, usually when uploading new photos for relationship managers.
		searchForm.setExcludeCategoryName(PortalFileCategory.CATEGORY_NAME_CONTACT_US);

		// If viewing a sample, we will just pull any file, doesn't have to be first approved
		if (!command.getCommandType().isPreviewOneSample()) {
			searchForm.setFirstApprovedFileForEntity(true);
		}
	}


	@Override
	protected List<PortalSecurityUser> getPortalSecurityUserListForFile(PortalFileExtended portalFileExtended) {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setDisabled(false);
		searchForm.setAssignmentRolePortalSecurityAdministrator(true);
		searchForm.setUserRolePortalEntitySpecific(true);
		// If the file applies to a specific entity, let's limit the users to that entity as well
		return getPortalFileService().getPortalFileSecurityUserList(portalFileExtended.getPortalFileId(), searchForm, (portalFileExtended.getAssignedPortalEntity() != null ? portalFileExtended.getAssignedPortalEntity().getId() : null));
	}


	private boolean isUserAssignmentApplyToEntity(PortalSecurityUserAssignment userAssignment, PortalEntity portalEntity) {
		if (userAssignment.getPortalEntity().equals(portalEntity)) {
			return true;
		}
		if (portalEntity.getParentPortalEntity() != null) {
			return isUserAssignmentApplyToEntity(userAssignment, portalEntity.getParentPortalEntity());
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static class PortalFileCategoryPortalSecurityUserPermission {

		private final PortalFileCategory portalFileCategory;
		private final PortalSecurityUserAssignmentResource portalSecurityUserAssignmentResource;

		/**
		 * Files for this permission were posted to the following entities
		 */
		private List<PortalEntity> assignedEntityList;


		public PortalFileCategoryPortalSecurityUserPermission(PortalFileCategory portalFileCategory, PortalSecurityUserAssignment userAssignment, PortalSecurityUserAssignmentResource portalSecurityUserAssignmentResource) {
			this.portalFileCategory = portalFileCategory;
			// If it's not a saved resource, the assignment is missing
			if (portalSecurityUserAssignmentResource.getSecurityUserAssignment() == null) {
				portalSecurityUserAssignmentResource.setSecurityUserAssignment(userAssignment);
			}
			this.portalSecurityUserAssignmentResource = portalSecurityUserAssignmentResource;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public String getUniqueKey() {
			return getPortalFileCategory().getId() + "_" + getPortalSecurityUserAssignmentResource().getSecurityUserAssignment().getId();
		}


		public void addAssignedPortalEntity(PortalEntity portalEntity) {
			if (portalEntity != null) { // Global File ?? add user assigned entity?
				if (this.assignedEntityList == null) {
					this.assignedEntityList = new ArrayList<>();
				}
				if (!this.assignedEntityList.contains(portalEntity)) {
					this.assignedEntityList.add(portalEntity);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public String getCategoryName() {
			return getPortalFileCategory().getName();
		}


		public String getSecurityResourceName() {
			return getPortalFileCategory().getSecurityResource().getName();
		}


		public PortalEntity getSecurablePortalEntity() {
			return getPortalSecurityUserAssignmentResource().getSecurityUserAssignment().getPortalEntity();
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public PortalFileCategory getPortalFileCategory() {
			return this.portalFileCategory;
		}


		public PortalSecurityUserAssignmentResource getPortalSecurityUserAssignmentResource() {
			return this.portalSecurityUserAssignmentResource;
		}


		public List<PortalEntity> getAssignedEntityList() {
			return this.assignedEntityList;
		}


		public void setAssignedEntityList(List<PortalEntity> assignedEntityList) {
			this.assignedEntityList = assignedEntityList;
		}
	}
}
