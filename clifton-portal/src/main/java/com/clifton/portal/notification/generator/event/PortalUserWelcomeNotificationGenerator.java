package com.clifton.portal.notification.generator.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalUserWelcomeNotificationGenerator</code> notification is sent when a new user is set up
 * <p>
 * The event is sent to the user with the newly generated temporary password populated.
 *
 * @author manderson
 */
@Component
public class PortalUserWelcomeNotificationGenerator extends BasePortalNotificationEventGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_WELCOME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isTermsOfUseRequiredToSend() {
		// Applies to External Users Only (PortalSecurityUser.isTermsOfUseAcceptanceRequired)
		// By default all notifications to external users will only be sent if the user has accepted the terms of use
		// This will be turned off ONLY for the Welcome and Forgot Password emails
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<PortalNotification> generatePortalNotificationListForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser) {
		Map<String, Object> templateBeanMap = (command.getPortalNotificationEvent() != null ? command.getPortalNotificationEvent().getTemplateContextBeanMap() : null);
		if (templateBeanMap == null) {
			if (command.getCommandType().isPreview()) {
				templateBeanMap = new HashMap<>();
				templateBeanMap.put("newPassword", "Password123");
			}
			else {
				throw new ValidationException("Missing newPassword on event context.");
			}
		}
		return CollectionUtils.createList(populatePortalNotificationForUser(command, securityUser, templateBeanMap));
	}
}
