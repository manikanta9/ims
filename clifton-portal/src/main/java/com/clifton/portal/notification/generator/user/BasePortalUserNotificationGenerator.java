package com.clifton.portal.notification.generator.user;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.BasePortalNotificationGenerator;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public abstract class BasePortalUserNotificationGenerator extends BasePortalNotificationGenerator {


	@Override
	public final List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command) {

		List<PortalSecurityUser> securityUserList = getPortalSecurityUserList(command);
		if (command.getCommandType().isPreviewOneSample()) {
			ValidationUtils.assertNotEmpty(securityUserList, "There are no Portal Users available to preview this notification for.");
		}

		List<PortalNotification> notificationList = new ArrayList<>();
		for (PortalSecurityUser securityUser : CollectionUtils.getIterable(securityUserList)) {
			if (command.getCommandType().isPreviewOneSample() || isNotificationSubscribedAndValidToSend(command, securityUser, null)) {
				Map<String, Object> templateContextBeans = getNotificationTemplateContextBeansForUser(command, securityUser);
				notificationList.add(populatePortalNotificationForUser(command, securityUser, templateContextBeans));
			}
		}
		return notificationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected abstract boolean isPortalAdministratorRecipientOnly();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<PortalSecurityUser> getPortalSecurityUserList(PortalNotificationGeneratorCommand command) {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setUserRolePortalEntitySpecific(true);
		if (isPortalAdministratorRecipientOnly()) {
			searchForm.setAssignmentRolePortalSecurityAdministrator(true);
		}
		searchForm.setDisabled(false);
		configurePortalSecurityUserSearchForm(searchForm, command);

		if (command.getCommandType().isPreviewOneSample()) {
			searchForm.setLimit(1);
		}
		// If not a sample, limit results to those users created before previous recurrence
		else {
			Date startDate = getPreviousRecurrence(command);
			if (startDate != null) {
				searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.LESS_THAN, startDate));
			}
		}
		return getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false);
	}


	protected void configurePortalSecurityUserSearchForm(PortalSecurityUserSearchForm searchForm, PortalNotificationGeneratorCommand command) {
		// DEFAULT DO NOTHING
	}


	protected Map<String, Object> getNotificationTemplateContextBeansForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser) {
		return null;
	}
}
