package com.clifton.portal.notification.event;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommandTypes;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorService;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.notification.setup.search.PortalNotificationDefinitionSearchForm;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalNotificationEventListener</code> listens for notifications events to send emails
 *
 * @author manderson
 */
@Component
public class PortalNotificationEventListener extends BaseEventListener<PortalNotificationEvent> {

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalNotificationGeneratorService portalNotificationGeneratorService;
	private PortalNotificationSetupService portalNotificationSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return PortalNotificationEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(PortalNotificationEvent event) {
		try {
			PortalNotificationDefinitionSearchForm searchForm = new PortalNotificationDefinitionSearchForm();
			searchForm.setDefinitionTypeId(getPortalNotificationSetupService().getPortalNotificationDefinitionTypeByName(event.getNotificationDefinitionTypeName()).getId());
			searchForm.setActive(true);
			// There should be ONLY one
			PortalNotificationDefinition definition = CollectionUtils.getOnlyElement(getPortalNotificationSetupService().getPortalNotificationDefinitionList(searchForm));
			if (definition == null) {
				throw new ValidationException("There is no active definition for type [" + event.getNotificationDefinitionTypeName() + "].  No notifications will be generated.");
			}

			PortalNotificationGeneratorCommand command = new PortalNotificationGeneratorCommand();
			command.setCommandType(PortalNotificationGeneratorCommandTypes.SEND);
			command.setPortalNotificationEvent(event);
			command.setPortalNotificationDefinition(definition);

			getPortalNotificationGeneratorService().generatePortalNotificationList(command);
			event.setResult("Requested email has been sent.");
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error generating notification during event " + getEventName() + ", definition type " + event.getNotificationDefinitionTypeName() + " for user " + event.getTarget().getLabel(), e);
			event.setResult("There was an error processing your request." + ExceptionUtils.getOriginalMessage(e));
		}
	}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationGeneratorService getPortalNotificationGeneratorService() {
		return this.portalNotificationGeneratorService;
	}


	public void setPortalNotificationGeneratorService(PortalNotificationGeneratorService portalNotificationGeneratorService) {
		this.portalNotificationGeneratorService = portalNotificationGeneratorService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}
}
