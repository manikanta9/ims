package com.clifton.portal.notification.runner;


import java.util.Date;


/**
 * The <code>PortalNotificationRunnerFactory</code> interface defines methods for creating PortalNotificationRunner instances.
 */
public interface PortalNotificationRunnerFactory {

	/**
	 * Create a top level portal notification job runner.
	 */
	public PortalNotificationRunner createPortalNotificationRunner(Date runDate, short definitionId);
}
