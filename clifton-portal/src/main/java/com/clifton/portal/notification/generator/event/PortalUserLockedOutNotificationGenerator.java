package com.clifton.portal.notification.generator.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortalUserLockedOutNotificationGenerator</code> notification is sent to a user when there account has been locked based on an event is triggered by a user that just successfully
 * reset their password
 *
 * @author manderson
 */
@Component
public class PortalUserLockedOutNotificationGenerator extends BasePortalNotificationEventGenerator {

	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_USER_LOCKED;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<PortalNotification> generatePortalNotificationListForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser) {
		return CollectionUtils.createList(populatePortalNotificationForUser(command, securityUser, null));
	}
}
