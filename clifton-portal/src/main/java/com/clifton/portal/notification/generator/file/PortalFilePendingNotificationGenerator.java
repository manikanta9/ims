package com.clifton.portal.notification.generator.file;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationPortalFile;
import com.clifton.portal.notification.generator.BasePortalNotificationGenerator;
import com.clifton.portal.notification.generator.PortalNotificationGeneratorCommand;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalFilePendingNotificationGenerator</code> is an internal notification sent to the relevant support teams where there are files pending approval
 * or some action.
 * <p>
 * Note: Because this is internal notification, and not the same as notifying users when files are posted (which also looks at file assignments) it does not extend the BasePortalFileNotificationGenerator
 *
 * @author manderson
 */
@Component
public class PortalFilePendingNotificationGenerator extends BasePortalNotificationGenerator {

	private PortalEmailService portalEmailService;
	private PortalFileService portalFileService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalNotificationGeneratorType() {
		return PortalNotificationDefinitionType.TYPE_NAME_PORTAL_FILES_PENDING;
	}


	@Override
	public List<PortalNotification> generatePortalNotificationList(PortalNotificationGeneratorCommand command) {
		// This notification will link back to the Portal Admin UI File Management Window Pending Approval tab
		// Track the URL params used to generate the notification, so we can default them when we click the link from the email
		StringBuilder loadWindowParams = new StringBuilder(16);
		List<PortalFile> portalFileList = getPortalFileListPendingApproval(command, loadWindowParams, false);
		Map<String, List<PortalFile>> recipientPortalFileMap = new HashMap<>();

		List<Short> userIdList = new ArrayList<>();


		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			if (!userIdList.contains(portalFile.getCreateUserId())) {
				userIdList.add(portalFile.getCreateUserId());
			}
			List<String> recipientList = getPortalEmailService().getPortalSupportEmailAddressListForPortalFile(portalFile);
			for (String recipient : CollectionUtils.getIterable(recipientList)) {
				List<PortalFile> fileList = recipientPortalFileMap.get(recipient);
				if (fileList == null) {
					fileList = new ArrayList<>();
				}
				fileList.add(portalFile);
				recipientPortalFileMap.put(recipient, fileList);
			}
		}

		List<PortalNotification> portalNotificationList = new ArrayList<>();
		for (Map.Entry<String, List<PortalFile>> stringListEntry : recipientPortalFileMap.entrySet()) {
			String recipientLoadWindowParams = "{" + loadWindowParams.toString() + ",supportTeamEmail:\"" + stringListEntry.getKey() + "\"}";
			String loadWindowParamsEncoded;
			try {
				loadWindowParamsEncoded = URLEncoder.encode(recipientLoadWindowParams, "UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				throw new RuntimeException("Error encoding url params: " + recipientLoadWindowParams);
			}
			Map<String, Object> templateContextMap = new HashMap<>();
			templateContextMap.put("fileList", stringListEntry.getValue());
			templateContextMap.put("userList", getPortalSecurityUserList(userIdList));
			templateContextMap.put("loadWindowParams", loadWindowParamsEncoded);
			PortalNotification notification = populatePortalNotificationForEmailAddress(command, stringListEntry.getKey(), templateContextMap);
			addPortalFileListToNotification(notification, stringListEntry.getValue());
			portalNotificationList.add(notification);
			// If only previewing one sample - return after the first one generated
			if (command.getCommandType().isPreviewOneSample()) {
				return portalNotificationList;
			}
		}
		return portalNotificationList;
	}


	protected List<PortalFile> getPortalFileListPendingApproval(PortalNotificationGeneratorCommand command, StringBuilder urlParams, boolean flexible) {
		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setApproved(false);
		searchForm.setOrderBy("fileGroupId#reportDate:DESC");
		urlParams.append("orderBy:\"").append(searchForm.getOrderBy()).append("\"");
		if (flexible) {
			searchForm.setLimit(10);
			urlParams.append(",limit:10");
		}
		if (!flexible) {
			if (command.getPortalNotificationDefinition().getFileApprovedDaysBack() != null) {
				Date startDate;
				if (command.getPortalNotificationDefinition().getFileApprovedDaysBack() == 1) {
					startDate = DateUtils.addWeekDays(command.getRunDateWithoutTime(), -command.getPortalNotificationDefinition().getFileApprovedDaysBack());
				}
				else {
					startDate = DateUtils.addDays(command.getRunDateWithoutTime(), -command.getPortalNotificationDefinition().getFileApprovedDaysBack());
				}
				searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
				urlParams.append(",unapprovedMinCreateDate:\"").append(DateUtils.fromDate(startDate)).append("\"");
			}
		}
		List<PortalFile> result = getPortalFileService().getPortalFileList(searchForm);
		if (CollectionUtils.isEmpty(result) && !flexible && command.getCommandType().isPreviewOneSample()) {
			// If we are switching to flexible search - clear url params
			urlParams.delete(0, urlParams.length());
			return getPortalFileListPendingApproval(command, urlParams, true);
		}
		return result;
	}


	protected void addPortalFileListToNotification(PortalNotification notification, List<PortalFile> fileList) {
		List<PortalNotificationPortalFile> portalNotificationFileList = new ArrayList<>();
		for (PortalFile file : CollectionUtils.getIterable(fileList)) {
			PortalNotificationPortalFile notificationPortalFile = new PortalNotificationPortalFile();
			notificationPortalFile.setReferenceOne(notification);
			notificationPortalFile.setReferenceTwo(file);
			portalNotificationFileList.add(notificationPortalFile);
		}
		notification.setFileList(portalNotificationFileList);
	}


	private List<PortalSecurityUser> getPortalSecurityUserList(List<Short> userIds) {
		if (!CollectionUtils.isEmpty(userIds)) {
			PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
			searchForm.setIds(userIds.toArray(new Short[userIds.size()]));
			return getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEmailService getPortalEmailService() {
		return this.portalEmailService;
	}


	public void setPortalEmailService(PortalEmailService portalEmailService) {
		this.portalEmailService = portalEmailService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}
}
