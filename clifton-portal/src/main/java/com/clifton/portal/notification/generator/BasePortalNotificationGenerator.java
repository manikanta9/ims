package com.clifton.portal.notification.generator;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.notification.PortalNotification;
import com.clifton.portal.notification.PortalNotificationService;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.notification.setup.PortalNotificationRecurrences;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public abstract class BasePortalNotificationGenerator implements PortalNotificationGenerator {

	private PortalNotificationService portalNotificationService;
	private PortalNotificationSetupService portalNotificationSetupService;
	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;

	private PortalEntitySetupService portalEntitySetupService;

	private PortalSecurityUserService portalSecurityUserService;

	private TemplateConverter templateConverter;


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final void validatePortalNotificationDefinition(PortalNotificationDefinition portalNotificationDefinition) {
		// Base validation that applies to ALL
		if (portalNotificationDefinition.isPopUpNotification()) {
			if (portalNotificationDefinition.getAcknowledgementType().isConfirmEntity()) {
				ValidationUtils.assertTrue(isPopUpNotificationConfirmEntitySupported(), "Pop Ups with Confirm Entity Option on is not supported for definition type " + portalNotificationDefinition.getDefinitionType().getName());
				ValidationUtils.assertTrue(portalNotificationDefinition.getNotificationRecurrence() != null && portalNotificationDefinition.getNotificationRecurrence() == PortalNotificationRecurrences.ONCE, "Pop Up Notifications that Confirm at the Entity Level must use a Recurrence of ONCE to prevent duplicate notifications and cross acknowledgements.");
			}
			ValidationUtils.assertTrue(portalNotificationDefinition.getNotificationRecurrence() != null && portalNotificationDefinition.getNotificationRecurrence() != PortalNotificationRecurrences.NONE, "Pop Up Notifications cannot use a NONE recurrence as users may end up with duplicate pop ups.");
		}
		validatePortalNotificationDefinitionImpl(portalNotificationDefinition);
	}


	protected void validatePortalNotificationDefinitionImpl(PortalNotificationDefinition portalNotificationDefinition) {
		// Additional Validation for other notification specific properties can be overridden here
	}


	public boolean isTermsOfUseRequiredToSend() {
		// Applies to External Users Only (PortalSecurityUser.isTermsOfUseAcceptanceRequired)
		// By default all notifications to external users will only be sent if the user has accepted the terms of use
		// This will be turned off ONLY for the Welcome and Forgot Password emails
		return true;
	}


	@Override
	public boolean isPopUpNotificationConfirmEntitySupported() {
		return false;
	}


	@Override
	public List<PortalEntity> getPortalEntityListForNotification(PortalNotification notification) {
		// Needs to be implemented for isPopUpNotificationConfirmEntitySupported
		AssertUtils.assertFalse(isPopUpNotificationConfirmEntitySupported(), "In order to support pop up notification confirm entity, this method must be implemented.");
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////


	protected boolean isNotificationSubscribedAndValidToSend(PortalNotificationGeneratorCommand command, PortalSecurityUser securityUser, PortalFileExtended portalFileExtended) {
		PortalNotificationDefinitionType definitionType = command.getPortalNotificationDefinition().getDefinitionType();

		// If Terms of Use are required to send the email, and user is an external user - DO NOT send if the user has not yet accepted the terms of use
		if (isTermsOfUseRequiredToSend() && securityUser.isTermsOfUseAcceptanceRequired() && securityUser.getTermsOfUseAcceptanceDate() == null) {
			return false;
		}

		// User is pending termination and we don't want to send
		if (command.getPortalNotificationDefinition().isSkipPendingTermination() && getPortalSecurityUserService().isPortalSecurityUserPendingTermination(securityUser)) {
			return false;
		}

		// Note: When file is present it is already filtered on users that actually have access to the file
		boolean subscribed = getPortalNotificationSubscriptionService().isPortalNotificationSubscriptionEnabled(definitionType, securityUser, portalFileExtended);

		// If user is subscribed, then confirm notification hasn't already been sent (if not recurring) or is valid since last one sent
		boolean valid = true;
		if (subscribed) {
			if (!definitionType.isTriggeredByEvent()) {
				PortalNotification lastNotification = getPortalNotificationService().getPortalNotificationLastForUser(definitionType.getId(), securityUser.getId(), (portalFileExtended == null ? null : portalFileExtended.getPortalFileId()));
				if (lastNotification != null) {
					if (command.getPortalNotificationDefinition().getNotificationRecurrence().isOneNotificationPerRecipient()) {
						valid = false;
					}
					// Get Max Date of Last Recurrence
					Date startDate = getPreviousRecurrence(command);
					// If last notification was on or equal to the recurrence period start, then don't send it again
					if (DateUtils.isDateAfterOrEqual(lastNotification.getSentDate(), startDate)) {
						valid = false;
					}
				}
			}
		}
		return subscribed && valid;
	}

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates and returns {@link PortalNotification} with given user as recipient, given definition, and subject and text.
	 * Optionally pass in additional beans to include in the template config.  recipientUser is automatically added
	 */
	protected PortalNotification populatePortalNotificationForUser(PortalNotificationGeneratorCommand command, PortalSecurityUser recipient, Map<String, Object> templateContextBeans) {
		return populatePortalNotificationImpl(command, recipient, null, templateContextBeans);
	}


	protected PortalNotification populatePortalNotificationForEmailAddress(PortalNotificationGeneratorCommand command, String recipientEmailAddress, Map<String, Object> templateContextBeans) {
		return populatePortalNotificationImpl(command, null, recipientEmailAddress, templateContextBeans);
	}


	private PortalNotification populatePortalNotificationImpl(PortalNotificationGeneratorCommand command, PortalSecurityUser recipient, String recipientEmailAddress, Map<String, Object> templateContextBeans) {
		PortalNotification notification = new PortalNotification();
		notification.setNotificationDefinition(command.getPortalNotificationDefinition());
		notification.setRecipientUser(recipient);
		notification.setRecipientEmailAddress(recipientEmailAddress);

		String subjectTemplate = command.getPortalNotificationDefinition().getEmailSubjectTemplate();
		String bodyTemplate = command.getPortalNotificationDefinition().getEmailBodyTemplate();
		// Only if it's a Preview do we use the potential override
		if (command.getCommandType().isPreview()) {
			if (!StringUtils.isEmpty(command.getSubjectTemplate())) {
				subjectTemplate = command.getSubjectTemplate();
			}
			if (!StringUtils.isEmpty(command.getBodyTemplate())) {
				bodyTemplate = command.getBodyTemplate();
			}
		}
		if (templateContextBeans == null) {
			templateContextBeans = new HashMap<>();
		}
		if (recipient != null) {
			templateContextBeans.put("recipientUser", recipient);
			templateContextBeans.put("entityViewTypeList", getPortalEntityViewTypeListForRecipient(recipient.getId()));
		}
		else {
			templateContextBeans.put("recipientEmailAddress", recipientEmailAddress);
		}
		notification.setSubject(evaluateTemplate(subjectTemplate, templateContextBeans));
		notification.setText(evaluateTemplate(bodyTemplate, templateContextBeans));
		return notification;
	}


	private String evaluateTemplate(String templateString, Map<String, Object> templateContextBeans) {
		TemplateConfig templateConfig = new TemplateConfig(templateString);
		for (String beanName : CollectionUtils.getKeys(templateContextBeans)) {
			templateConfig.addBeanToContext(beanName, templateContextBeans.get(beanName));
		}
		return getTemplateConverter().convert(templateConfig);
	}

	////////////////////////////////////////////////////////////////////////////////


	protected Date getPreviousRecurrence(PortalNotificationGeneratorCommand command) {
		Date date = command.getRunDateWithoutTime();
		PortalNotificationRecurrences recurrence = command.getPortalNotificationDefinition().getNotificationRecurrence();
		if (recurrence == null) {
			return null;
		}
		return recurrence.getRecurrenceFromDate(date, command.getPortalNotificationDefinition().getNotificationRecurrenceInterval(), true);
	}

	////////////////////////////////////////////////////////////////////////////////


	protected List<PortalEntityViewType> getPortalEntityViewTypeListForRecipient(Short securityUserId) {
		// If previewing and not a real user then could be null - in this case just return the default view
		if (securityUserId == null) {
			return CollectionUtils.createList(getPortalEntitySetupService().getPortalEntityViewTypeDefault());
		}
		PortalEntityViewTypeSearchForm searchForm = new PortalEntityViewTypeSearchForm();
		searchForm.setViewAsUserId(securityUserId);
		return getPortalEntitySetupService().getPortalEntityViewTypeList(searchForm, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public PortalNotificationSubscriptionService getPortalNotificationSubscriptionService() {
		return this.portalNotificationSubscriptionService;
	}


	public void setPortalNotificationSubscriptionService(PortalNotificationSubscriptionService portalNotificationSubscriptionService) {
		this.portalNotificationSubscriptionService = portalNotificationSubscriptionService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}
}
