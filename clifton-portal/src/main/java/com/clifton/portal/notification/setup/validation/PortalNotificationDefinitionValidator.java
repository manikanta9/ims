package com.clifton.portal.notification.setup.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.notification.generator.PortalNotificationGenerator;
import com.clifton.portal.notification.generator.locator.PortalNotificationGeneratorLocator;
import com.clifton.portal.notification.setup.PortalNotificationDefinition;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalNotificationDefinitionValidator extends SelfRegisteringDaoValidator<PortalNotificationDefinition> {


	private PortalNotificationGeneratorLocator portalNotificationGeneratorLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortalNotificationDefinition bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertNotNull(bean.getDefinitionType(), "Notification Definition Type is required.");
		if (!bean.getDefinitionType().isTriggeredByEvent()) {
			ValidationUtils.assertNotNull(bean.getNotificationRecurrence(), "Notification Recurrence is required for notification definitions that are not triggered by an event.");
			if (bean.getNotificationRecurrence().isRecurrenceIntervalSupported()) {
				ValidationUtils.assertNotNull(bean.getNotificationRecurrenceInterval(), "Notification Recurrence Interval is required for recurrence " + bean.getNotificationRecurrence().name());
				ValidationUtils.assertTrue(MathUtils.isGreaterThan(bean.getNotificationRecurrenceInterval(), 0), "Notification Recurrence Interval must be a positive number");
			}
			else {
				ValidationUtils.assertNull(bean.getNotificationRecurrenceInterval(), "Notification Recurrence Interval is not supported for recurrence " + bean.getNotificationRecurrence().name());
			}
		}
		else {
			ValidationUtils.assertNull(bean.getNotificationRecurrence(), "Notification Recurrence is not supported for notifications triggered by an event.");
			ValidationUtils.assertNull(bean.getNotificationRecurrenceInterval(), "Notification Recurrence Interval is not supported for notifications triggered by an event.");
			ValidationUtils.assertNull(bean.getStartTime(), "Schedule Start time is not allowed for notifications triggered by events.");
			ValidationUtils.assertNull(bean.getEndTime(), "Schedule End time is not allowed for notifications triggered by events.");
			ValidationUtils.assertNull(bean.getRunFrequencyInMinutes(), "Schedule Run Frequency in minutes is not allowed for notifications triggered by events.");
		}

		if (!bean.getDefinitionType().isFileNotification()) {
			ValidationUtils.assertNull(bean.getFileApprovedDaysBack(), "File Approved Days Back is not supported for notification definition type " + bean.getDefinitionType().getName());
		}
		else if (bean.getFileApprovedDaysBack() != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(bean.getFileApprovedDaysBack(), 0), "File Approved Days Back must be greater than zero.");
		}

		if (bean.getStartTime() != null) {
			if (bean.getEndTime() != null) {
				ValidationUtils.assertTrue(bean.getEndTime().after(bean.getStartTime()), "End Time must be after Start Time");
			}
			if (bean.getRunFrequencyInMinutes() != null) {
				ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(bean.getRunFrequencyInMinutes(), 5), "Run Frequency in minutes must be greater than or equal to 5.");
			}
		}
		else {
			ValidationUtils.assertNull(bean.getEndTime(), "End Time is not allowed without a start time entered.");
			ValidationUtils.assertNull(bean.getRunFrequencyInMinutes(), "Run Frequency is not allowed without a start time entered.");
		}

		if (bean.isPopUpNotification()) {
			ValidationUtils.assertFalse(bean.getDefinitionType().isInternalNotification(), "Pop-Up Notifications are not supported for internal notifications.");
			ValidationUtils.assertNotNull(bean.getAcknowledgementType(), "Acknowledgement Type is required for Pop-Up Notifications");
		}
		else {
			ValidationUtils.assertNull(bean.getAcknowledgementType(), "Acknowledgement Type is not supported for E-Mail Notifications");
		}

		PortalNotificationGenerator notificationGenerator = getPortalNotificationGeneratorLocator().locate(bean.getDefinitionType());
		notificationGenerator.validatePortalNotificationDefinition(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortalNotificationGeneratorLocator getPortalNotificationGeneratorLocator() {
		return this.portalNotificationGeneratorLocator;
	}


	public void setPortalNotificationGeneratorLocator(PortalNotificationGeneratorLocator portalNotificationGeneratorLocator) {
		this.portalNotificationGeneratorLocator = portalNotificationGeneratorLocator;
	}
}
