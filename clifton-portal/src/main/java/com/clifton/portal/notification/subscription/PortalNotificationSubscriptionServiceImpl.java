package com.clifton.portal.notification.subscription;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.holiday.PortalHolidayService;
import com.clifton.portal.notification.setup.PortalNotificationDefinitionType;
import com.clifton.portal.notification.setup.PortalNotificationSetupService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserAssignmentSearchForm;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortalNotificationSubscriptionServiceImpl implements PortalNotificationSubscriptionService {


	private AdvancedUpdatableDAO<PortalNotificationSubscription, Criteria> portalNotificationSubscriptionDAO;

	private DaoSingleKeyListCache<PortalNotificationSubscription, Short> portalNotificationSubscriptionListByUserCache;

	private PortalEntityService portalEntityService;
	private PortalFileSetupService portalFileSetupService;
	private PortalHolidayService portalHolidayService;
	private PortalNotificationSetupService portalNotificationSetupService;
	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;
	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	///////////       Portal Notification Subscription Entry Methods      //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalNotificationSubscriptionEntry getPortalNotificationSubscriptionEntry(short securityUserId, int portalEntityId) {
		PortalSecurityUser securityUser = getPortalSecurityUserService().getPortalSecurityUser(securityUserId);
		ValidationUtils.assertTrue(securityUser.getUserRole().isPortalEntitySpecific(), "Notifications can only be defined for portal entity specific (client) users.");

		PortalNotificationSubscriptionEntry subscriptionEntry = new PortalNotificationSubscriptionEntry();
		subscriptionEntry.setSecurityUser(securityUser);
		PortalNotificationSubscriptionRebuildContext rebuildContext = new PortalNotificationSubscriptionRebuildContext();
		subscriptionEntry.setSubscriptionList(getPortalNotificationSubscriptionListForUserAndEntity(securityUserId, portalEntityId, false, false, rebuildContext));
		return subscriptionEntry;
	}


	@Override
	public PortalNotificationSubscriptionEntry savePortalNotificationSubscriptionEntry(PortalNotificationSubscriptionEntry bean) {
		PortalSecurityUser securityUser = bean.getSecurityUser();
		ValidationUtils.assertTrue(securityUser.getUserRole().isPortalEntitySpecific(), "Notifications can only be defined for portal entity specific (client) users.");

		for (PortalNotificationSubscription subscription : CollectionUtils.getIterable(bean.getSubscriptionList())) {
			// When we populate the default list we set ids to -10, -20, etc.  On save we need to clear that so that it's properly inserted.
			if (subscription.getId() != null && subscription.getId() <= 0) {
				subscription.setId(null);
			}
			if (!subscription.isNotificationEnabled()) {
				subscription.setSubscriptionFrequency(null);
			}
		}
		getPortalNotificationSubscriptionDAO().saveList(bean.getSubscriptionList());
		return getPortalNotificationSubscriptionEntry(bean.getSecurityUser().getId(), bean.getPortalEntity().getId());
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////         Portal Notification Subscription Methods        ////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalNotificationSubscription> getPortalNotificationSubscriptionListForUserAndEntity(short securityUserId, Integer portalEntityId, boolean returnNullIfDisabled, boolean useNullIdForNewBeans, PortalNotificationSubscriptionRebuildContext rebuildContext) {
		PortalSecurityUser securityUser = getPortalSecurityUserService().getPortalSecurityUser(securityUserId);
		ValidationUtils.assertTrue(securityUser.getUserRole().isPortalEntitySpecific(), "Notifications can only be defined for portal entity specific (client) users.");

		PortalEntity portalEntity = getPortalEntityService().getPortalEntity(portalEntityId);
		if (portalEntity == null || !portalEntity.isActive() || portalEntity.isDeleted()) {
			if (returnNullIfDisabled) {
				return null;
			}
			throw new ValidationException("Portal Entity " + portalEntityId + " is not currently active or available for selection.");
		}

		// Get the Most Specific Assignment That Applies
		PortalSecurityUserAssignment assignment = getPortalSecurityUserService().getPortalSecurityUserAssignmentByUserAndEntity(securityUserId, portalEntityId, true);
		if (assignment == null) {
			if (returnNullIfDisabled) {
				return null;
			}
			throw new ValidationException("Cannot find an assignment that allows user [" + securityUser.getLabel() + "] to access portal entity [" + portalEntity.getId() + "]");
		}
		// Get Most Specific List of Subscriptions
		List<PortalNotificationSubscription> subscriptionList = getPortalNotificationSubscriptionListForUserAndEntityImpl(securityUserId, portalEntity, true);

		return getPortalNotificationSubscriptionListImpl(assignment, portalEntity, subscriptionList, useNullIdForNewBeans, rebuildContext);
	}


	private List<PortalNotificationSubscription> getPortalNotificationSubscriptionListForUserAndEntityImpl(short securityUserId, PortalEntity portalEntity, boolean checkParents) {
		Map<PortalEntity, List<PortalNotificationSubscription>> entitySubscriptionListMap = BeanUtils.getBeansMap(getPortalNotificationSubscriptionListByUserCache().getBeanListForKeyValue(getPortalNotificationSubscriptionDAO(), securityUserId), PortalNotificationSubscription::getPortalEntity);
		if (entitySubscriptionListMap.containsKey(portalEntity)) {
			return entitySubscriptionListMap.get(portalEntity);
		}
		if (checkParents && portalEntity.getParentPortalEntity() != null) {
			return getPortalNotificationSubscriptionListForUserAndEntityImpl(securityUserId, portalEntity.getParentPortalEntity(), checkParents);
		}
		return null;
	}


	@Override
	public void requirePortalNotificationSubscriptionListForFileCategory(short portalFileCategoryId) {
		PortalNotificationSubscriptionSearchForm searchForm = new PortalNotificationSubscriptionSearchForm();
		searchForm.setPortalFileCategoryId(portalFileCategoryId);
		searchForm.setNotificationEnabled(false);
		List<PortalNotificationSubscription> subscriptionList = getPortalNotificationSubscriptionList(searchForm);
		if (!CollectionUtils.isEmpty(subscriptionList)) {
			subscriptionList.forEach(portalNotificationSubscription -> portalNotificationSubscription.setNotificationEnabled(true));
		}
		getPortalNotificationSubscriptionDAO().saveList(subscriptionList);
	}


	@Override
	public void rebuildPortalNotificationSubscriptionList() {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setUserRolePortalEntitySpecific(true);
		List<PortalSecurityUser> portalSecurityUserList = getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false);

		PortalNotificationSubscriptionRebuildContext rebuildContext = new PortalNotificationSubscriptionRebuildContext();

		for (PortalSecurityUser portalSecurityUser : CollectionUtils.getIterable(portalSecurityUserList)) {
			rebuildPortalNotificationSubscriptionListForUserImpl(portalSecurityUser.getId(), rebuildContext);
		}
	}


	@Override
	public void rebuildPortalNotificationSubscriptionListForUser(short portalSecurityUserId) {
		rebuildPortalNotificationSubscriptionListForUserImpl(portalSecurityUserId, new PortalNotificationSubscriptionRebuildContext());
	}


	private void rebuildPortalNotificationSubscriptionListForUserImpl(short portalSecurityUserId, PortalNotificationSubscriptionRebuildContext rebuildContext) {
		List<PortalNotificationSubscription> subscriptionList = getPortalNotificationSubscriptionListByUserCache().getBeanListForKeyValue(getPortalNotificationSubscriptionDAO(), portalSecurityUserId);
		Map<PortalEntity, List<PortalNotificationSubscription>> entitySubscriptionMap = BeanUtils.getBeansMap(subscriptionList, PortalNotificationSubscription::getPortalEntity);

		Map<PortalEntity, List<PortalNotificationSubscription>> newEntitySubscriptionMap = new HashMap<>();

		// Get the List Of Assignments
		PortalSecurityUserAssignmentSearchForm searchForm = new PortalSecurityUserAssignmentSearchForm();
		searchForm.setPortalSecurityUserId(portalSecurityUserId);
		List<PortalSecurityUserAssignment> userAssignmentList = getPortalSecurityUserService().getPortalSecurityUserAssignmentList(searchForm, false);

		// For each assignment, regenerate the list of subscriptions
		for (PortalSecurityUserAssignment userAssignment : CollectionUtils.getIterable(userAssignmentList)) {
			List<PortalNotificationSubscription> assignmentSubscriptionList = getPortalNotificationSubscriptionListForUserAndEntity(portalSecurityUserId, userAssignment.getPortalEntity().getId(), true, true, rebuildContext);
			newEntitySubscriptionMap.put(userAssignment.getPortalEntity(), assignmentSubscriptionList == null ? new ArrayList<>() : assignmentSubscriptionList);
		}

		// Then check if any other entities have subscriptions and regenerate them
		for (PortalEntity portalEntity : entitySubscriptionMap.keySet()) {
			if (!newEntitySubscriptionMap.containsKey(portalEntity)) {
				List<PortalNotificationSubscription> entitySubscriptionList = getPortalNotificationSubscriptionListForUserAndEntityImpl(portalSecurityUserId, portalEntity, true);
				newEntitySubscriptionMap.put(portalEntity, entitySubscriptionList == null ? new ArrayList<>() : entitySubscriptionList);
			}
		}

		// Now Save the New List - Save as system user so we know this was rebuilt by the system, not specifically edited by the current user
		List<PortalNotificationSubscription> newSubscriptionList = CollectionUtils.combineCollectionOfCollections(newEntitySubscriptionMap.values());
		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> getPortalNotificationSubscriptionDAO().saveList(newSubscriptionList, subscriptionList));
		LogUtils.info(getClass(), "rebuildPortalNotificationSubscriptionListForUser completed");
	}


	@Override
	public void deletePortalNotificationSubscriptionListForUserAssignment(int securityUserAssignmentId) {
		getPortalNotificationSubscriptionDAO().deleteList(getPortalNotificationSubscriptionDAO().findByField("userAssignment.id", securityUserAssignmentId));
	}


	@Override
	public List<PortalNotificationSubscription> getPortalNotificationSubscriptionList(PortalNotificationSubscriptionSearchForm searchForm) {
		return getPortalNotificationSubscriptionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * Returns true if the user is subscribed to the given notification OR if the notification doesn't support subscriptions
	 * By default notifications are off for subscribed notifications
	 */
	@Override
	public boolean isPortalNotificationSubscriptionEnabled(PortalNotificationDefinitionType definitionType, PortalSecurityUser securityUser, PortalFileExtended portalFileExtended) {
		if (definitionType.isSubscriptionAllowed()) {
			PortalNotificationSubscription subscription = null;
			ValidationUtils.assertNotNull(portalFileExtended, "Portal File Extended is required to determine if user is subscribed to notification.");
			PortalEntity assignedPortalEntity = portalFileExtended.getAssignedPortalEntity();
			// Get List of Subscriptions for the User from the Cache
			List<PortalNotificationSubscription> subscriptionList = getPortalNotificationSubscriptionListByUserCache().getBeanListForKeyValue(getPortalNotificationSubscriptionDAO(), securityUser.getId());
			if (!CollectionUtils.isEmpty(subscriptionList)) {
				String filterValue = (definitionType.isSubscriptionForFileCategory() ? portalFileExtended.getFileCategory().getName() : definitionType.getName());
				subscriptionList = BeanUtils.filter(subscriptionList, PortalNotificationSubscription::getCoalesceDefinitionTypeFileCategoryName, filterValue);
				// if not assigned entity, then it's a global file - so then find the first one that has the notification enabled
				if (assignedPortalEntity == null) {
					subscription = CollectionUtils.getFirstElement(BeanUtils.filter(subscriptionList, PortalNotificationSubscription::isNotificationEnabled));
				}
				else {
					// Find the Most Specific One
					subscription = getMostSpecificPortalNotificationSubscriptionFromListForEntity(subscriptionList, assignedPortalEntity);
				}
			}
			if (subscription != null && subscription.isNotificationEnabled()) {
				if (subscription.getSubscriptionFrequency() == null) {
					return true;
				}
				return subscription.getSubscriptionFrequency().isFrequencyValidForDate(portalFileExtended.getPublishDate(), getPortalHolidayService());
			}
			return false;
		}
		// No subscriptions allowed, then notifications are ON
		return true;
	}


	private PortalNotificationSubscription getMostSpecificPortalNotificationSubscriptionFromListForEntity(List<PortalNotificationSubscription> subscriptionList, PortalEntity portalEntity) {
		PortalNotificationSubscription subscription = CollectionUtils.getFirstElement(BeanUtils.filter(subscriptionList, PortalNotificationSubscription::getPortalEntity, portalEntity));
		if (subscription == null && portalEntity != null && portalEntity.getParentPortalEntity() != null) {
			return getMostSpecificPortalNotificationSubscriptionFromListForEntity(subscriptionList, portalEntity.getParentPortalEntity());
		}
		return subscription;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortalNotificationSubscription> getPortalNotificationSubscriptionListImpl(PortalSecurityUserAssignment assignment, PortalEntity portalEntity, List<PortalNotificationSubscription> subscriptionList, boolean useNullIdForNewBeans, PortalNotificationSubscriptionRebuildContext rebuildContext) {
		int counter = 0;
		Map<String, PortalNotificationSubscription> subscriptionMap = BeanUtils.getBeanMap(subscriptionList, PortalNotificationSubscription::getCoalesceDefinitionTypeFileCategoryName);

		List<PortalSecurityUserAssignmentResource> assignmentResourceList = getPortalSecurityUserService().getPortalSecurityUserAssignmentResourceListForAssignment(assignment, true, rebuildContext.getRetrievalContext());
		assignmentResourceList = BeanUtils.filter(assignmentResourceList, PortalSecurityUserAssignmentResource::isAccessAllowed, true);
		Short[] securityResourceIds = BeanUtils.getPropertyValues(assignmentResourceList, assignmentResource -> assignmentResource.getSecurityResource().getId(), Short.class);
		if (ArrayUtils.isEmpty(securityResourceIds)) {
			return null;
		}

		// Limit File Category Selections to those that the given assigned entity has approved files for and has notifications enabled
		// Full Category List - Need the full list for below when finding definition types that have notifications
		// At that point we still want to limit to those (if limited by security resource) only if the entity has a file posted under that security resource
		List<PortalFileCategory> fullCategoryList = rebuildContext.getPortalFileCategoryListForEntity(getPortalFileSetupService(), portalEntity.getId(), securityResourceIds);

		// Limit to Those that Support Notification Subscriptions on the Category
		List<PortalFileCategory> categoryList = BeanUtils.filter(fullCategoryList, portalFileCategory -> portalFileCategory.getNotificationDefinition() != null && portalFileCategory.getNotificationDefinition().getDefinitionType() != null && portalFileCategory.getNotificationDefinition().getDefinitionType().isSubscriptionForFileCategory());

		Short[] securityResourceIdsWithFiles = BeanUtils.getPropertyValuesExcludeNull(fullCategoryList, portalFileCategory -> portalFileCategory.getSecurityResource() == null ? null : portalFileCategory.getSecurityResource().getId(), Short.class);
		List<PortalNotificationDefinitionType> definitionTypeList = rebuildContext.getSubscriptionEligibleDefinitionTypesNotUsingFileCategorySubscriptions(getPortalNotificationSetupService(), securityResourceIdsWithFiles);

		List<PortalNotificationSubscription> newSubscriptionList = new ArrayList<>();
		// Go Through File Categories First
		for (PortalFileCategory fileCategory : CollectionUtils.getIterable(categoryList)) {
			PortalNotificationSubscription notificationSubscription = subscriptionMap.get(fileCategory.getName());
			if (notificationSubscription != null && fileCategory.isNotificationSubscriptionRequired() && !notificationSubscription.isNotificationEnabled()) {
				notificationSubscription.setNotificationEnabled(fileCategory.getNotificationSubscriptionOption().isDefaultValue());
			}
			if (notificationSubscription == null) {
				counter = counter - 10;
				notificationSubscription = new PortalNotificationSubscription();
				if (!useNullIdForNewBeans) {
					notificationSubscription.setId(counter);
				}
				notificationSubscription.setFileCategory(fileCategory);
				// Get Default or Required Value
				notificationSubscription.setNotificationEnabled(fileCategory.getNotificationSubscriptionOption() != null && fileCategory.getNotificationSubscriptionOption().isDefaultValue());
				notificationSubscription.setUserAssignment(assignment);
				notificationSubscription.setPortalEntity(portalEntity);
			}
			else if (!portalEntity.equals(notificationSubscription.getPortalEntity())) {
				counter = counter - 10;
				notificationSubscription = BeanUtils.cloneBean(notificationSubscription, false, false);
				if (!useNullIdForNewBeans) {
					notificationSubscription.setId(counter);
				}
				notificationSubscription.setUserAssignment(assignment);
				notificationSubscription.setPortalEntity(portalEntity);
			}
			newSubscriptionList.add(notificationSubscription);
		}

		// Then go through additional customizable notifications
		for (PortalNotificationDefinitionType definitionType : CollectionUtils.getIterable(definitionTypeList)) {
			PortalNotificationSubscription notificationSubscription = subscriptionMap.get(definitionType.getName());
			if (notificationSubscription == null) {
				counter = counter - 10;
				notificationSubscription = new PortalNotificationSubscription();
				if (!useNullIdForNewBeans) {
					notificationSubscription.setId(counter);
				}
				notificationSubscription.setNotificationDefinitionType(definitionType);
				// By Default - All notifications are off
				notificationSubscription.setNotificationEnabled(false);
				notificationSubscription.setUserAssignment(assignment);
				notificationSubscription.setPortalEntity(portalEntity);
			}
			else if (!portalEntity.equals(notificationSubscription.getPortalEntity())) {
				counter = counter - 10;
				notificationSubscription = BeanUtils.cloneBean(notificationSubscription, false, false);
				if (!useNullIdForNewBeans) {
					notificationSubscription.setId(counter);
				}
				notificationSubscription.setUserAssignment(assignment);
				notificationSubscription.setPortalEntity(portalEntity);
			}
			newSubscriptionList.add(notificationSubscription);
		}
		return newSubscriptionList;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalNotificationSubscription, Criteria> getPortalNotificationSubscriptionDAO() {
		return this.portalNotificationSubscriptionDAO;
	}


	public void setPortalNotificationSubscriptionDAO(AdvancedUpdatableDAO<PortalNotificationSubscription, Criteria> portalNotificationSubscriptionDAO) {
		this.portalNotificationSubscriptionDAO = portalNotificationSubscriptionDAO;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalNotificationSetupService getPortalNotificationSetupService() {
		return this.portalNotificationSetupService;
	}


	public void setPortalNotificationSetupService(PortalNotificationSetupService portalNotificationSetupService) {
		this.portalNotificationSetupService = portalNotificationSetupService;
	}


	public DaoSingleKeyListCache<PortalNotificationSubscription, Short> getPortalNotificationSubscriptionListByUserCache() {
		return this.portalNotificationSubscriptionListByUserCache;
	}


	public void setPortalNotificationSubscriptionListByUserCache(DaoSingleKeyListCache<PortalNotificationSubscription, Short> portalNotificationSubscriptionListByUserCache) {
		this.portalNotificationSubscriptionListByUserCache = portalNotificationSubscriptionListByUserCache;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}


	public PortalHolidayService getPortalHolidayService() {
		return this.portalHolidayService;
	}


	public void setPortalHolidayService(PortalHolidayService portalHolidayService) {
		this.portalHolidayService = portalHolidayService;
	}
}
