package com.clifton.portal.page;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.SimpleSpecificityScope;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.core.cache.specificity.key.AbstractSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;


/**
 * The {@link PortalPageElementSpecificityTargetHolder} type defines a target holder for {@link PortalPageElement} entities to be used when querying the {@link
 * PortalPageElementSpecificityCache}. This type encapsulates all data necessary to determine the applicability for the backing {@link PortalPageElement} against a given {@link
 * PortalPageElementSpecificitySource} as well as the data necessary to retrieve the backing {@link PortalPageElement}.
 *
 * @author mikeh
 */
public class PortalPageElementSpecificityTargetHolder implements SpecificityTargetHolder {

	// Specificity target holder fields
	private final String key;
	private final SpecificityScope scope;

	// Page element fields
	private final int pageElementId;
	private final Short viewTypeId;
	private final Integer priority;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementSpecificityTargetHolder(PortalPageElement pageElement, AbstractSpecificityKeyGenerator keyGenerator) {
		this.pageElementId = pageElement.getId();
		this.viewTypeId = BeanUtils.getBeanIdentity(pageElement.getViewType());
		this.priority = pageElement.getPriority();
		this.key = generateKey(pageElement, keyGenerator);
		this.scope = new SimpleSpecificityScope(pageElement.getType().getId(), true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String generateKey(PortalPageElement pageElement, AbstractSpecificityKeyGenerator keyGenerator) {
		/*
		 * We cannot simply include the view type as a parameter here, as the source may include any number of view types. Instead, we include a flag indicating whether a view type
		 * is attached and then execute after-the-fact filtering.
		 */
		return keyGenerator.generateKeyForTarget(SpecificityKeySource
				.builder(pageElement.getViewType() != null ? true : null, null)
				.build());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public SpecificityScope getScope() {
		return this.scope;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getPageElementId() {
		return this.pageElementId;
	}


	public Short getViewTypeId() {
		return this.viewTypeId;
	}


	public Integer getPriority() {
		return this.priority;
	}
}
