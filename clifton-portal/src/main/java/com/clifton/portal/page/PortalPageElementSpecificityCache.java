package com.clifton.portal.page;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


/**
 * The {@link PortalPageElementSpecificityCache} is the specificity cache for querying for {@link PortalPageElement} entities which apply to a given user or entity.
 * <p>
 * This cache finds all {@link PortalPageElement} entities which apply for a given set of {@link PortalEntityViewType view types}. The cache is configured to return all of the
 * <i>most specific</i> elements with equal specificity. I.e., when the same specificity applies to multiple elements of the same type, then all such elements will be returned.
 * <p>
 * Specificity is configured with two dimensions: Presence or absence of a {@link PortalEntityViewType view type} and {@link PortalPageElement#getPriority() priority level}.
 * Additionally, elements will only be considered during cache queries when the element view type is global (absent) or when the {@link PortalPageElement#getViewType() element view
 * type} exists in the passed-in list of view types.
 * <p>
 * Standard usage:
 * <code><pre>
 * List&lt;PortalEntityViewType&gt; viewTypeList = getPortalEntitySetupService().getPortalEntityViewTypeListForUser(userId);
 * List&lt;PortalPageElement&gt; elementList = getPortalPageElementSpecificityCache().getMostSpecificResultList(new PortalPageElementSpecificitySource(viewTypeList));
 * </pre></code>
 *
 * @author mikeh
 */
@Component
public class PortalPageElementSpecificityCache extends AbstractSpecificityCache<PortalPageElementSpecificitySource, PortalPageElement, PortalPageElementSpecificityTargetHolder> {

	private SpecificityCacheUpdater<PortalPageElementSpecificityTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(
			PortalPageElement.class, // New/removed page elements
			PortalPageElementType.class // New/removed page element types
	);
	private PortalPageElementService portalPageElementService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementSpecificityCache() {
		super(SpecificityCacheTypes.MATCHING_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean isMatch(PortalPageElementSpecificitySource source, PortalPageElementSpecificityTargetHolder holder) {
		// Only consider elements if they are global or if their view type exists in the source view type list
		return holder.getViewTypeId() == null // Global
				|| CollectionUtils.anyMatch(source.getViewTypeList(), viewType -> holder.getViewTypeId().equals(viewType.getId())); // Matched view type
	}


	@Override
	protected PortalPageElement getTarget(PortalPageElementSpecificityTargetHolder targetHolder) {
		return getPortalPageElementService().getPortalPageElement(targetHolder.getPageElementId());
	}


	@Override
	protected SpecificityKeySource getKeySource(PortalPageElementSpecificitySource source) {
		return source.getKeySource();
	}


	@Override
	protected Collection<PortalPageElementSpecificityTargetHolder> getTargetHolders() {
		List<PortalPageElement> pageElementList = getPortalPageElementService().getPortalPageElementList(new PortalPageElementSearchForm());
		return CollectionUtils.getConverted(pageElementList, pageElement -> new PortalPageElementSpecificityTargetHolder(pageElement, getKeyGenerator()));
	}


	@Override
	public List<PortalPageElement> getMostSpecificResultList(PortalPageElementSpecificitySource source) {
		List<PortalPageElement> mostSpecificResultList = super.getMostSpecificResultList(source);
		// Filter to applicable elements based on maximum numeric priorities
		return getMaxPriorityPageElementList(mostSpecificResultList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of {@link PortalPageElement} entities from the given list with the maximum priority.
	 *
	 * @param elementList the list of elements which should be filtered to the max-priority elements
	 * @return the list of max-priority elements
	 */
	private List<PortalPageElement> getMaxPriorityPageElementList(List<PortalPageElement> elementList) {
		Map<PortalPageElementType, List<PortalPageElement>> elementListByType = BeanUtils.getBeansMap(elementList, PortalPageElement::getType);
		List<PortalPageElement> filteredResultList = new ArrayList<>();
		for (Map.Entry<PortalPageElementType, List<PortalPageElement>> typeToElementListEntry : elementListByType.entrySet()) {
			// Find elements matching max priority
			PortalPageElementType elementType = typeToElementListEntry.getKey();
			List<PortalPageElement> elementListForType = typeToElementListEntry.getValue();
			Integer maxPriority = CoreMathUtils.maxNumeric(CollectionUtils.getConverted(elementListForType, PortalPageElement::getPriority));
			List<PortalPageElement> maxPriorityElementList = CollectionUtils.getFiltered(elementListForType, element -> CompareUtils.isEqual(maxPriority, element.getPriority()));
			// Resolve conflicts when multiple matched elements are not allowed
			maxPriorityElementList = getValidatedPageElementListForType(elementType, maxPriorityElementList);
			filteredResultList.addAll(maxPriorityElementList);
		}
		return filteredResultList;
	}


	/**
	 * Validates the given <code>elementList</code> for the provided <code>elementType</code> against the constraints set by the element type.
	 * <p>
	 * If the <code>elementList</code> contains multiple elements and the type does not allow {@link PortalPageElementType#isAllowMultipleElementsOfType() multiple simultaneous
	 * elements} then an error will be logged and the first element will be returned.
	 * <p>
	 * If the <code>elementList</code> is empty and the type requires a {@link PortalPageElementType#isRequireGlobalElement() global element} then an error will be logged and the
	 * original (empty) list will be returned.
	 *
	 * @param elementType the element type against which the list will be validated
	 * @param elementList the list of elements
	 * @return the original list with minor attempts to gracefully correct for validation issues
	 */
	private List<PortalPageElement> getValidatedPageElementListForType(PortalPageElementType elementType, List<PortalPageElement> elementList) {
		List<PortalPageElement> resultElementList = elementList;
		if (elementList.size() > 1 && !elementType.isAllowMultipleElementsOfType()) {
			// Validate singular result
			List<Integer> elementIdList = CollectionUtils.getConverted(elementList, BaseSimpleEntity::getId);
			PortalPageElement firstElement = elementList.stream()
					.min(Comparator.nullsLast(Comparator.comparing(PortalPageElement::getOrder)))
					.orElseThrow(() -> new IllegalStateException(String.format("Unexpected state. A first element could not be found for the elements of type [%s].", elementType.getName())));
			LogUtils.error(LogCommand.ofMessageSupplier(PortalPageElementSpecificityCache.class, () -> String.format("Multiple matching page elements were found for element type [%s]. Matching IDs: %s. The element type does not support displaying multiple values simultaneously. The first element (ID: [%d]) will be returned.", elementType.getName(), CollectionUtils.toString(elementIdList, 5), BeanUtils.getBeanIdentity(firstElement))));
			resultElementList = Collections.singletonList(firstElement);
		}
		else if (elementList.isEmpty() && elementType.isRequireGlobalElement()) {
			// Validate presence of global element
			LogUtils.error(LogCommand.ofMessageSupplier(PortalPageElementSpecificityCache.class, () -> String.format("No page elements were found for the element type [%s]. This type requires a global element.", elementType.getName())));
		}
		return resultElementList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SpecificityCacheUpdater<PortalPageElementSpecificityTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementService getPortalPageElementService() {
		return this.portalPageElementService;
	}


	public void setPortalPageElementService(PortalPageElementService portalPageElementService) {
		this.portalPageElementService = portalPageElementService;
	}
}
