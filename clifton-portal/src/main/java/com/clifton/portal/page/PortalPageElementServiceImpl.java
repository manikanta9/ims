package com.clifton.portal.page;

import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


/**
 * The {@link PortalPageElementServiceImpl} is the default implementation for the {@link PortalPageElementService} type.
 *
 * @author mikeh
 */
@Service
public class PortalPageElementServiceImpl implements PortalPageElementService {

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;

	private AdvancedUpdatableDAO<PortalPageElement, Criteria> portalPageElementDAO;
	private AdvancedUpdatableDAO<PortalPageElementType, Criteria> portalPageElementTypeDAO;
	private SpecificityCache<PortalPageElementSpecificitySource, PortalPageElement> portalPageElementSpecificityCache;


	////////////////////////////////////////////////////////////////////////////
	////////            PortalPageElement Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalPageElement getPortalPageElement(int id) {
		return getPortalPageElementDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalPageElement> getPortalPageElementList(PortalPageElementSearchForm searchForm) {
		List<PortalPageElement> elementList = getPortalPageElementDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (searchForm.getViewAsEntityId() != null) {
			elementList = CollectionUtils.getIntersection(elementList, getPortalPageElementListForEntity(searchForm.getViewAsEntityId()));
		}
		else if (searchForm.getViewAsUserId() != null) {
			elementList = CollectionUtils.getIntersection(elementList, getPortalPageElementListForUser(searchForm.getViewAsUserId()));
		}
		return CollectionUtils.toPagingArrayList(elementList, searchForm.getStart(), searchForm.getLimit());
	}


	@Override
	public List<PortalPageElement> getPortalPageElementListDefault() {
		return getPortalPageElementSpecificityCache().getMostSpecificResultList(new PortalPageElementSpecificitySource(Collections.emptyList()));
	}


	@Override
	public List<PortalPageElement> getPortalPageElementListForUser(short userId) {
		List<PortalEntityViewType> viewTypeList = getPortalEntitySetupService().getPortalEntityViewTypeListForUser(userId);
		return getPortalPageElementSpecificityCache().getMostSpecificResultList(new PortalPageElementSpecificitySource(viewTypeList));
	}


	@Override
	public List<PortalPageElement> getPortalPageElementListForEntity(int entityId) {
		PortalEntity entity = getPortalEntityService().getPortalEntity(entityId);
		ValidationUtils.assertNotNull(entity, () -> String.format("No portal entity was found for ID [%d].", entityId));
		return getPortalPageElementSpecificityCache().getMostSpecificResultList(new PortalPageElementSpecificitySource(Collections.singletonList(entity.getEntityViewType())));
	}


	@Override
	public PortalPageElement savePortalPageElement(PortalPageElement element) {
		return getPortalPageElementDAO().save(element);
	}


	@Override
	public void deletePortalPageElement(int id) {
		getPortalPageElementDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            PortalPageElementType Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalPageElementType getPortalPageElementType(short id) {
		return getPortalPageElementTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalPageElementType> getPortalPageElementTypeList(PortalPageElementTypeSearchForm searchForm) {
		return getPortalPageElementTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortalPageElementType savePortalPageElementType(PortalPageElementType type) {
		return getPortalPageElementTypeDAO().save(type);
	}


	@Override
	public void deletePortalPageElementType(short id) {
		getPortalPageElementTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public AdvancedUpdatableDAO<PortalPageElement, Criteria> getPortalPageElementDAO() {
		return this.portalPageElementDAO;
	}


	public void setPortalPageElementDAO(AdvancedUpdatableDAO<PortalPageElement, Criteria> portalPageElementDAO) {
		this.portalPageElementDAO = portalPageElementDAO;
	}


	public AdvancedUpdatableDAO<PortalPageElementType, Criteria> getPortalPageElementTypeDAO() {
		return this.portalPageElementTypeDAO;
	}


	public void setPortalPageElementTypeDAO(AdvancedUpdatableDAO<PortalPageElementType, Criteria> portalPageElementTypeDAO) {
		this.portalPageElementTypeDAO = portalPageElementTypeDAO;
	}


	public SpecificityCache<PortalPageElementSpecificitySource, PortalPageElement> getPortalPageElementSpecificityCache() {
		return this.portalPageElementSpecificityCache;
	}


	public void setPortalPageElementSpecificityCache(SpecificityCache<PortalPageElementSpecificitySource, PortalPageElement> portalPageElementSpecificityCache) {
		this.portalPageElementSpecificityCache = portalPageElementSpecificityCache;
	}
}
