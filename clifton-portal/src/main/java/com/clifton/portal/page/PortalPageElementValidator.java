package com.clifton.portal.page;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The {@link PortalPageElementValidator} is the validator for {@link PortalPageElement} entities. This validates that all elements are non-conflicting (in the case of a non-true
 * {@link PortalPageElementType#isAllowMultipleElementsOfType()} flag) and that a {@link PortalPageElementType#isRequireGlobalElement() global element} is present, if required.
 *
 * @author mikeh
 */
@Component
public class PortalPageElementValidator extends SelfRegisteringDaoValidator<PortalPageElement> {

	private PortalPageElementService portalPageElementService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortalPageElement bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			validateElementDelete(bean);
		}
		else {
			validateElementSave(bean);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateElementSave(PortalPageElement bean) {
		// Validate element is global if a global element is required
		if (bean.getViewType() != null && bean.getType().isRequireGlobalElement()) {
			PortalPageElementSearchForm searchForm = new PortalPageElementSearchForm();
			searchForm.setTypeId(bean.getType().getId());
			searchForm.setViewTypeNotNull(false);
			List<PortalPageElement> globalPageElementList = getPortalPageElementService().getPortalPageElementList(searchForm);
			globalPageElementList.remove(bean);
			ValidationUtils.assertNotEmpty(globalPageElementList, () -> String.format("The element type [%s] requires at least one global element. No other global elements exist. This element must be made a global element (i.e., have no configured view type).", bean.getType().getName()));
		}
		// Validate no duplicate elements when disallowed
		if (!bean.getType().isAllowMultipleElementsOfType()) {
			PortalPageElementSearchForm searchForm = new PortalPageElementSearchForm();
			searchForm.setTypeId(bean.getType().getId());
			searchForm.setPriority(bean.getPriority());
			searchForm.setViewTypeNotNull(bean.getViewType() != null);
			List<PortalPageElement> duplicatePageElementList = getPortalPageElementService().getPortalPageElementList(searchForm);
			duplicatePageElementList.remove(bean);
			ValidationUtils.assertEmpty(duplicatePageElementList, () -> String.format("The element type [%s] does not allow multiple elements of the same type to apply simultaneously. Other %s elements were found with identical priorities. Duplicate IDs: %s", bean.getType().getName(), bean.getViewType() == null ? "global" : "non-global", CollectionUtils.toString(CollectionUtils.getConverted(duplicatePageElementList, BaseSimpleEntity::getId), 5)));
		}
	}


	private void validateElementDelete(PortalPageElement bean) {
		// Validate global element not deleted if required
		if (bean.getViewType() == null && bean.getType().isRequireGlobalElement()) {
			PortalPageElementSearchForm searchForm = new PortalPageElementSearchForm();
			searchForm.setTypeId(bean.getType().getId());
			searchForm.setViewTypeNotNull(false);
			List<PortalPageElement> globalPageElementList = getPortalPageElementService().getPortalPageElementList(searchForm);
			globalPageElementList.remove(bean);
			ValidationUtils.assertNotEmpty(globalPageElementList, () -> String.format("The element type [%s] requires at least one global element. No other global elements exist.", bean.getType().getName()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementService getPortalPageElementService() {
		return this.portalPageElementService;
	}


	public void setPortalPageElementService(PortalPageElementService portalPageElementService) {
		this.portalPageElementService = portalPageElementService;
	}
}
