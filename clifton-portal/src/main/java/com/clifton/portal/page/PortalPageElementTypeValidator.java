package com.clifton.portal.page;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The {@link PortalPageElementTypeValidator} is the validator for {@link PortalPageElementType} entities. This validates a restrictive set of modifications, only allowing
 * modifications for a strict subset of fields.
 *
 * @author mikeh
 */
@Component
public class PortalPageElementTypeValidator extends SelfRegisteringDaoValidator<PortalPageElementType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortalPageElementType bean, DaoEventTypes config) throws ValidationException {
		// Allow no changes except for name and description
		if (config.isInsert()) {
			ValidationUtils.fail("Page element types may not be created.");
		}
		else if (config.isDelete()) {
			ValidationUtils.fail("Page element types may not be deleted.");
		}
		else if (config.isUpdate()) {
			PortalPageElementType originalBean = getOriginalBean(bean);
			CoreValidationUtils.assertAllowedModifiedFields(bean, originalBean, "name", "description");
		}
	}
}
