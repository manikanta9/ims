package com.clifton.portal.page;

import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.portal.entity.setup.PortalEntityViewType;

import java.util.List;


/**
 * The {@link PortalPageElementSpecificitySource} type defines the source data to be used when querying the {@link PortalPageElementSpecificityCache}. This type includes all
 * necessary information to determine applicable {@link PortalPageElement} entities.
 *
 * @author mikeh
 */
public class PortalPageElementSpecificitySource {

	private final List<PortalEntityViewType> viewTypeList;

	// Cache value at instantiation rather than generating each time this is used
	private final SpecificityKeySource keySource;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalPageElementSpecificitySource(List<PortalEntityViewType> viewTypeList) {
		this.viewTypeList = viewTypeList;
		this.keySource = generateKey(viewTypeList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SpecificityKeySource generateKey(List<PortalEntityViewType> viewTypeList) {
		return SpecificityKeySource
				.builder(!viewTypeList.isEmpty() ? true : null, null)
				.build();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<PortalEntityViewType> getViewTypeList() {
		return this.viewTypeList;
	}


	public SpecificityKeySource getKeySource() {
		return this.keySource;
	}
}
