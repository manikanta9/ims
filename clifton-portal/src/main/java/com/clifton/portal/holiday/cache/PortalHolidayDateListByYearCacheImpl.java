package com.clifton.portal.holiday.cache;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.holiday.PortalHoliday;
import com.clifton.portal.holiday.PortalHolidaySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The cache is cleared for the year in which the updated date is affected
 *
 * @author Mary Anderson
 */
@Component
public class PortalHolidayDateListByYearCacheImpl extends SelfRegisteringSimpleDaoCache<PortalHoliday, Integer, Set<Date>> implements PortalHolidayDateListByYearCache {


	@Override
	public Set<Date> getPortalHolidayDateListForYear(int year, AdvancedReadOnlyDAO<PortalHoliday, Criteria> portalHolidayDAO) {
		Set<Date> result = getCacheHandler().get(getCacheName(), year);
		if (result == null) {
			PortalHolidaySearchForm searchForm = new PortalHolidaySearchForm();
			searchForm.setStartDate(DateUtils.toDate("01/01/" + year));
			searchForm.setEndDate(DateUtils.toDate("12/31/" + year));
			List<PortalHoliday> holidayList = portalHolidayDAO.findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
			result = CollectionUtils.getStream(holidayList).map(PortalHoliday::getHolidayDate).collect(Collectors.toSet());
			getCacheHandler().put(getCacheName(), year, result);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                   Observer Methods                    //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT_OR_DELETE;
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<PortalHoliday> dao, DaoEventTypes event, PortalHoliday bean, Throwable e) {
		if (e == null) {
			// NOTE: WE DON'T DO UPDATES - JUST INSERTS AND DELETES
			getCacheHandler().remove(getCacheName(), DateUtils.getYear(bean.getHolidayDate()));
		}
	}
}
