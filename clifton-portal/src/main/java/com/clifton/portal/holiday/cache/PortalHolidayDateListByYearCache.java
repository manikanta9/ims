package com.clifton.portal.holiday.cache;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.portal.holiday.PortalHoliday;
import org.hibernate.Criteria;

import java.util.Date;
import java.util.Set;


/**
 * The <code>PortalHolidayDateListByYearCache</code> caches the list of dates during the year that are holidays.
 *
 * @author Mary Anderson
 */
public interface PortalHolidayDateListByYearCache {

	public Set<Date> getPortalHolidayDateListForYear(int year, AdvancedReadOnlyDAO<PortalHoliday, Criteria> portalHolidayDAO);
}
