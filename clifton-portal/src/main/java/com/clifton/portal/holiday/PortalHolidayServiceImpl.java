package com.clifton.portal.holiday;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.holiday.cache.PortalHolidayDateListByYearCache;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalHolidayServiceImpl implements PortalHolidayService {


	private AdvancedUpdatableDAO<PortalHoliday, Criteria> portalHolidayDAO;

	private PortalHolidayDateListByYearCache portalHolidayDateListByYearCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalHoliday> getPortalHolidayList(PortalHolidaySearchForm searchForm) {
		return getPortalHolidayDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<PortalHoliday> savePortalHolidayList(List<PortalHoliday> holidayList) {
		getPortalHolidayDAO().saveList(holidayList);
		return holidayList;
	}


	@Override
	public void deletePortalHolidayList(List<PortalHoliday> holidayList) {
		getPortalHolidayDAO().deleteList(holidayList);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////             Business Day Helper Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFirstBusinessDaySinceFromDate(Date fromDate, Date date) {
		Date nextBusinessDay = getSelfOrNextBusinessDay(fromDate);
		return DateUtils.isEqualWithoutTime(nextBusinessDay, date);
	}


	@Override
	public Date getPortalHolidayPreviousBusinessDay(Date date) {
		if (date == null) {
			date = DateUtils.clearTime(new Date());
		}
		// Move back to previous weekday
		date = DateUtils.getPreviousWeekday(date);
		// Return that date or previous business day of that date
		return getSelfOrPreviousBusinessDay(date);
	}


	/**
	 * If given date is a business day will return that date, otherwise will move backwards through weekdays until a non-holiday date is found
	 */
	private Date getSelfOrPreviousBusinessDay(Date date) {
		if (!isBusinessDay(date)) {
			// Move backward weekdays instead of calendar days because weekends are never business days
			return getSelfOrPreviousBusinessDay(DateUtils.getPreviousWeekday(date));
		}
		return date;
	}


	/**
	 * If given date is a business day will return that date, otherwise will move forward through weekdays until a non-holiday date is found
	 */
	private Date getSelfOrNextBusinessDay(Date date) {
		if (!isBusinessDay(date)) {
			// Move forward weekdays instead of calendar days because weekends are never business days
			return getSelfOrNextBusinessDay(DateUtils.getNextWeekday(date));
		}
		return date;
	}


	private boolean isBusinessDay(Date date) {
		if (DateUtils.isWeekend(date)) {
			return false;
		}
		if (getPortalHolidayDateListByYearCache().getPortalHolidayDateListForYear(DateUtils.getYear(date), getPortalHolidayDAO()).contains(date)) {
			return false;
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalHoliday, Criteria> getPortalHolidayDAO() {
		return this.portalHolidayDAO;
	}


	public void setPortalHolidayDAO(AdvancedUpdatableDAO<PortalHoliday, Criteria> portalHolidayDAO) {
		this.portalHolidayDAO = portalHolidayDAO;
	}


	public PortalHolidayDateListByYearCache getPortalHolidayDateListByYearCache() {
		return this.portalHolidayDateListByYearCache;
	}


	public void setPortalHolidayDateListByYearCache(PortalHolidayDateListByYearCache portalHolidayDateListByYearCache) {
		this.portalHolidayDateListByYearCache = portalHolidayDateListByYearCache;
	}
}
