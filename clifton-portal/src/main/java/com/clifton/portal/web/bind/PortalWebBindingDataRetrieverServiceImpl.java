package com.clifton.portal.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.CoreClassUtils;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class PortalWebBindingDataRetrieverServiceImpl implements PortalWebBindingDataRetrieverService {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public <T extends IdentityObject> T getPortalObject(String objectClassName, Number objectId) {
		Class<T> dtoClass = (Class<T>) CoreClassUtils.getClass(objectClassName);
		ReadOnlyDAO<T> dao = getDaoLocator().locate(dtoClass);
		return dao.findByPrimaryKey(objectId);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Column> getPortalObjectColumnList(String objectClassName) {
		Class<? extends IdentityObject> dtoClass = (Class<? extends IdentityObject>) CoreClassUtils.getClass(objectClassName);
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(dtoClass);
		return dao.getConfiguration().getColumnList();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
