package com.clifton.portal.tracking;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.tracking.search.PortalTrackingEventExtendedSearchForm;
import com.clifton.portal.tracking.search.PortalTrackingEventSearchForm;
import com.clifton.portal.tracking.search.PortalTrackingEventSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortalTrackingServiceImpl implements PortalTrackingService {

	private AdvancedUpdatableDAO<PortalTrackingEvent, Criteria> portalTrackingEventDAO;
	private AdvancedReadOnlyDAO<PortalTrackingEventExtended, Criteria> portalTrackingEventExtendedDAO;
	private AdvancedUpdatableDAO<PortalTrackingEventType, Criteria> portalTrackingEventTypeDAO;

	private DaoNamedEntityCache<PortalTrackingEventType> portalTrackingEventTypeCache;

	private PortalEmailService portalEmailService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalTrackingEvent> getPortalTrackingEventList(PortalTrackingEventSearchForm searchForm) {
		return getPortalTrackingEventDAO().findBySearchCriteria(new PortalTrackingEventSearchFormConfigurer(searchForm, this));
	}


	@Override
	public List<PortalTrackingEvent> getPortalTrackingAuditTrailEventListForSource(String sourceTableName, int sourceFkFieldId) {
		ValidationUtils.assertNotEmpty(sourceTableName, "Source Table Name is Required");
		PortalTrackingEventSearchForm searchForm = new PortalTrackingEventSearchForm();
		searchForm.setSourceTableNameEquals(sourceTableName);
		searchForm.setSourceFkFieldId(sourceFkFieldId);
		searchForm.setTrackingEventTypeNames(new String[]{PortalTrackingEventTypes.AUDIT_TRAIL_INSERT.getName(), PortalTrackingEventTypes.AUDIT_TRAIL_UPDATE.getName(), PortalTrackingEventTypes.AUDIT_TRAIL_DELETE.getName()});
		searchForm.setOrderBy("eventDate:DESC");
		return getPortalTrackingEventList(searchForm);
	}


	@Override
	public PortalTrackingEvent savePortalTrackingEvent(PortalTrackingEvent bean) {
		return getPortalTrackingEventDAO().save(bean);
	}


	@Override
	public void savePortalTrackingEventList(List<PortalTrackingEvent> beanList) {
		getPortalTrackingEventDAO().saveList(beanList);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalTrackingEventExtended getPortalTrackingEventExtended(long id) {
		return getPortalTrackingEventExtendedDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalTrackingEventExtended> getPortalTrackingEventExtendedList(PortalTrackingEventExtendedSearchForm searchForm, boolean populateRelationshipManagerNames) {
		List<PortalTrackingEventExtended> result = getPortalTrackingEventExtendedDAO().findBySearchCriteria(new PortalTrackingEventSearchFormConfigurer(searchForm, this));
		if (populateRelationshipManagerNames) {
			// Track the RMs for each entity so we don't have to re-query them
			Map<Integer, String> entityRelationshipManagerNamesMap = new HashMap<>();
			for (PortalTrackingEventExtended eventExtended : CollectionUtils.getIterable(result)) {
				setRelationshipManagerNamesForPortalTrackingEventExtended(eventExtended, entityRelationshipManagerNamesMap);
			}
		}
		return result;
	}


	private void setRelationshipManagerNamesForPortalTrackingEventExtended(PortalTrackingEventExtended eventExtended, Map<Integer, String> entityRelationshipManagerNamesMap) {
		Integer portalEntityId = (eventExtended.getPortalEntity() == null ? null : eventExtended.getPortalEntity().getId());
		if (portalEntityId == null) {
			portalEntityId = (eventExtended.getPortalFile() != null && eventExtended.getPortalFile().getPostPortalEntity() != null ? eventExtended.getPortalFile().getPostPortalEntity().getId() : null);
		}
		if (portalEntityId != null) {
			String relationshipManagerNames = entityRelationshipManagerNamesMap.get(portalEntityId);
			if (!entityRelationshipManagerNamesMap.containsKey(portalEntityId)) {
				List<PortalEntity> relationshipManagerList = getPortalEmailService().getRelationshipManagerList(portalEntityId);
				if (!CollectionUtils.isEmpty(relationshipManagerList)) {
					relationshipManagerNames = BeanUtils.getPropertyValues(relationshipManagerList, "label", ",");
				}
				entityRelationshipManagerNamesMap.put(portalEntityId, relationshipManagerNames);
			}
			eventExtended.setRelationshipManagerNames(entityRelationshipManagerNamesMap.get(portalEntityId));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalTrackingEventType getPortalTrackingEventType(short id) {
		return getPortalTrackingEventTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalTrackingEventType getPortalTrackingEventTypeByName(String name) {
		return getPortalTrackingEventTypeCache().getBeanForKeyValueStrict(getPortalTrackingEventTypeDAO(), name);
	}


	@Override
	public List<PortalTrackingEventType> getPortalTrackingEventTypeList() {
		return getPortalTrackingEventTypeDAO().findAll();
	}


	@Override
	public PortalTrackingEventType savePortalTrackingEventType(PortalTrackingEventType bean) {
		if (bean.isNewBean()) {
			throw new ValidationException("New Portal Tracking Event Types cannot be created manually.");
		}
		PortalTrackingEventType originalBean = getPortalTrackingEventType(bean.getId());
		List<String> changedProperties = CoreCompareUtils.getNoEqualPropertiesWithSetters(originalBean, bean, false, "description", "excludeNonPortalEntitySpecific");
		if (!CollectionUtils.isEmpty(changedProperties)) {
			throw new ValidationException("The following fields cannot be updated on existing event types: " + StringUtils.collectionToCommaDelimitedString(changedProperties));
		}
		return getPortalTrackingEventTypeDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalTrackingEvent, Criteria> getPortalTrackingEventDAO() {
		return this.portalTrackingEventDAO;
	}


	public void setPortalTrackingEventDAO(AdvancedUpdatableDAO<PortalTrackingEvent, Criteria> portalTrackingEventDAO) {
		this.portalTrackingEventDAO = portalTrackingEventDAO;
	}


	public AdvancedReadOnlyDAO<PortalTrackingEventExtended, Criteria> getPortalTrackingEventExtendedDAO() {
		return this.portalTrackingEventExtendedDAO;
	}


	public void setPortalTrackingEventExtendedDAO(AdvancedReadOnlyDAO<PortalTrackingEventExtended, Criteria> portalTrackingEventExtendedDAO) {
		this.portalTrackingEventExtendedDAO = portalTrackingEventExtendedDAO;
	}


	public AdvancedUpdatableDAO<PortalTrackingEventType, Criteria> getPortalTrackingEventTypeDAO() {
		return this.portalTrackingEventTypeDAO;
	}


	public void setPortalTrackingEventTypeDAO(AdvancedUpdatableDAO<PortalTrackingEventType, Criteria> portalTrackingEventTypeDAO) {
		this.portalTrackingEventTypeDAO = portalTrackingEventTypeDAO;
	}


	public DaoNamedEntityCache<PortalTrackingEventType> getPortalTrackingEventTypeCache() {
		return this.portalTrackingEventTypeCache;
	}


	public void setPortalTrackingEventTypeCache(DaoNamedEntityCache<PortalTrackingEventType> portalTrackingEventTypeCache) {
		this.portalTrackingEventTypeCache = portalTrackingEventTypeCache;
	}


	public PortalEmailService getPortalEmailService() {
		return this.portalEmailService;
	}


	public void setPortalEmailService(PortalEmailService portalEmailService) {
		this.portalEmailService = portalEmailService;
	}
}
