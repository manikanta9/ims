package com.clifton.portal.tracking.generator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.tracking.PortalTrackingEventMethod;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAwareObserver;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public interface PortalTrackingEventGenerator {


	/**
	 * Custom method for audit trail, called from the {@link PortalTrackingAuditAwareObserver}
	 */
	public <T extends IdentityObject> void generatePortalTrackingAuditTrailEvent(DaoEventTypes eventType, String tableName, T bean, T originalBean, Map<String, PortalTrackingAuditColumnConfig> columnConfigMap);


	/**
	 * Custom method for login because the current user isn't in the context
	 */
	public void generatePortalTrackingLoginEvent(HttpServletRequest request, String userName);


	public void generatePortalTrackingWebRequestMethodEvent(PortalTrackingEventMethod eventMethodAnnotation, HttpServletRequest request);


	public void generatePortalTrackingNotificationAcknowledgementEvent(int portalNotificationId, Boolean acknowledge, Set<PortalEntity> portalEntitySet);
}
