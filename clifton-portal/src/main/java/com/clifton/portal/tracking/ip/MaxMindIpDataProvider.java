package com.clifton.portal.tracking.ip;


import com.clifton.core.converter.string.StringToBigDecimalConverter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.http.HttpUtils;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>MaxMindIpDataProvider</code> class is an implementation of retrieving information for a given ip address
 */
public class MaxMindIpDataProvider implements IpDataProvider {

	private String maxMindUrl;
	private String maxMinKey;
	private String excludeIpRangeRegEx;
	private Pattern ipPattern;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalTrackingIpData getIpData(String ip) {
		try {
			if (this.ipPattern == null) {
				this.ipPattern = Pattern.compile(getExcludeIpRangeRegEx());
			}
			Matcher m = this.ipPattern.matcher(ip);
			if (m.find()) {
				return null;
			}

			PortalTrackingIpData result = null;
			String rawIpData = getRawData(ip);
			if (!StringUtils.isEmpty(rawIpData)) {
				String[] ipDataItems = rawIpData.split(",");

				result = new PortalTrackingIpData();
				result.setIpAddress(ip);
				result.setCountryCode(ipDataItems[0]);
				result.setRegion(ipDataItems[1]);
				result.setCity(ipDataItems[2]);
				result.setPostalCode(ipDataItems[3]);
				if (!StringUtils.isEmpty(ipDataItems[4])) {
					result.setLatitude(new StringToBigDecimalConverter().convert(ipDataItems[4]));
				}
				if (!StringUtils.isEmpty(ipDataItems[5])) {
					result.setLongitude(new StringToBigDecimalConverter().convert(ipDataItems[5]));
				}
				if (!StringUtils.isEmpty(ipDataItems[6])) {
					result.setMetroCode(Integer.parseInt(ipDataItems[6]));
				}
				if (!StringUtils.isEmpty(ipDataItems[7])) {
					result.setAreaCode(Integer.parseInt(ipDataItems[7]));
				}
				if (!StringUtils.isEmpty(ipDataItems[8])) {
					result.setIsp(ipDataItems[8].replace("\"", ""));
				}
				if (!StringUtils.isEmpty(ipDataItems[9])) {
					result.setOrganization(ipDataItems[9].replace("\"", ""));
				}
				result.setQueryDate(DateUtils.clearTime(new Date()));
			}
			return result;
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to get ip data for [" + ip + "]", e);
			return null;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String getRawData(String ip) throws IOException {
		return HttpUtils.get(getMaxMindUrl() + "?l=" + getMaxMinKey() + "&i=" + ip, true).trim();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getMaxMindUrl() {
		return this.maxMindUrl;
	}


	public void setMaxMindUrl(String maxMindUrl) {
		this.maxMindUrl = maxMindUrl;
	}


	public String getExcludeIpRangeRegEx() {
		return this.excludeIpRangeRegEx;
	}


	public void setExcludeIpRangeRegEx(String excludeIpRangeRegEx) {
		this.excludeIpRangeRegEx = excludeIpRangeRegEx;
	}


	public String getMaxMinKey() {
		return this.maxMinKey;
	}


	public void setMaxMinKey(String maxMinKey) {
		this.maxMinKey = maxMinKey;
	}
}
