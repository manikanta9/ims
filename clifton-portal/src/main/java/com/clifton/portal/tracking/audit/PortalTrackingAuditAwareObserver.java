package com.clifton.portal.tracking.audit;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.portal.tracking.generator.PortalTrackingEventGenerator;

import java.util.HashMap;
import java.util.Map;


/**
 * @author manderson
 */
public class PortalTrackingAuditAwareObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private PortalTrackingEventGenerator portalTrackingEventGenerator;

	// Contains customized overrides for specific columns
	private final Map<String, PortalTrackingAuditColumnConfig> columnConfigMap;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalTrackingAuditAwareObserver(Map<String, PortalTrackingAuditColumnConfig> columnConfigMap) {
		super();
		setOrder(-1000); // make sure that audit observer is always the first one to run
		this.columnConfigMap = columnConfigMap == null ? new HashMap<>() : columnConfigMap;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// Get the Original Bean, so it's set for after call
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			T originalBean = (event.isUpdate() ? getOriginalBean(dao, bean) : null);
			getPortalTrackingEventGenerator().generatePortalTrackingAuditTrailEvent(event, dao.getConfiguration().getTableName(), bean, originalBean, getColumnConfigMap());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalTrackingEventGenerator getPortalTrackingEventGenerator() {
		return this.portalTrackingEventGenerator;
	}


	public void setPortalTrackingEventGenerator(PortalTrackingEventGenerator portalTrackingEventGenerator) {
		this.portalTrackingEventGenerator = portalTrackingEventGenerator;
	}


	public Map<String, PortalTrackingAuditColumnConfig> getColumnConfigMap() {
		return this.columnConfigMap;
	}
}
