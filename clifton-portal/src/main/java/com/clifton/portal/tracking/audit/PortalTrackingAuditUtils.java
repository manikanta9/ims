package com.clifton.portal.tracking.audit;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portal.tracking.PortalTrackingEventTypes;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public class PortalTrackingAuditUtils {


	/**
	 * For the given class returns a map of property field name to the {@link PortalTrackingAuditColumnConfig} annotation that applies to the field
	 */
	public static <T> Map<String, PortalTrackingAuditColumnConfig> getPortalTrackingAuditColumnConfigMap(Class<T> clazz) {
		Map<String, PortalTrackingAuditColumnConfig> columnConfigMap = new HashMap<>();
		Field[] fields = ClassUtils.getClassFields(clazz, true, true);
		for (Field field : fields) {
			PortalTrackingAuditColumnConfig columnConfig = AnnotationUtils.getAnnotation(field, PortalTrackingAuditColumnConfig.class);
			if (columnConfig == null) {
				continue;
			}
			columnConfigMap.put(field.getName(), columnConfig);
		}
		return columnConfigMap;
	}


	/**
	 * Generates the Audit Description for the changed field values
	 */
	@SuppressWarnings("unchecked")
	public static <T> String generatePortalTrackingAuditEventDescription(DaoEventTypes eventType, T bean, T originalBean, Map<String, PortalTrackingAuditColumnConfig> columnConfigMap) {
		Set<String> auditedFields = new HashSet<>();
		StringBuilder description = new StringBuilder(50);

		// If Inserting - No Comparison only Track was is set to always audit for inserts
		if (!eventType.isInsert()) {
			// If Deleting - Compare to a new copy of the bean
			if (eventType.isDelete()) {
				originalBean = (T) BeanUtils.newInstance(bean.getClass());
			}
			List<String> changedList = CoreCompareUtils.getNoEqualPropertiesWithSettersUsingCustomComparison(bean, originalBean, false, PortalTrackingAuditUtils::compareForAuditUpdate);
			for (String field : CollectionUtils.getIterable(changedList)) {
				PortalTrackingAuditColumnConfig columnConfig = columnConfigMap.get(field);
				if (columnConfig != null && columnConfig.doNotAudit()) {
					continue;
				}
				String originalValue = PortalTrackingAuditUtils.getPropertyValueAsString(originalBean, field);
				String newValue = PortalTrackingAuditUtils.getPropertyValueAsString(bean, field);

				if (eventType.isUpdate() && columnConfig != null && (columnConfig.doNotAuditUpdateFromNull() || !StringUtils.isEmpty(columnConfig.doNotAuditUpdateFromValue()))) {
					if (originalValue == null && columnConfig.doNotAuditUpdateFromNull()) {
						continue;
					}
					if (!StringUtils.isEmpty(columnConfig.doNotAuditUpdateFromValue())) {
						if (StringUtils.isEqualIgnoreCase(originalValue, columnConfig.doNotAuditUpdateFromValue())) {
							continue;
						}
					}
				}

				auditedFields.add(field);
				if (eventType.isUpdate()) {
					description.append(StringUtils.NEW_LINE).append(field).append(": [").append(originalValue).append("] to [").append(newValue).append("]");
				}
				// Delete
				else {
					description.append(StringUtils.NEW_LINE).append(field).append(": [").append(newValue).append("]");
				}
			}
		}

		if ((eventType.isInsert() || eventType.isDelete()) && !CollectionUtils.isEmpty(columnConfigMap)) {
			for (Map.Entry<String, PortalTrackingAuditColumnConfig> stringPortalTrackingAuditColumnConfigEntry : columnConfigMap.entrySet()) {
				if (!auditedFields.contains(stringPortalTrackingAuditColumnConfigEntry.getKey())) {
					PortalTrackingAuditColumnConfig columnConfig = stringPortalTrackingAuditColumnConfigEntry.getValue();
					if (eventType.isInsert() && columnConfig.alwaysAuditInsert()) {
						auditedFields.add(stringPortalTrackingAuditColumnConfigEntry.getKey());
						description.append(StringUtils.NEW_LINE).append(stringPortalTrackingAuditColumnConfigEntry.getKey()).append(": [").append(PortalTrackingAuditUtils.getPropertyValueAsString(bean, stringPortalTrackingAuditColumnConfigEntry.getKey())).append("]");
					}
					if (eventType.isDelete() && columnConfig.alwaysAuditDelete()) {
						auditedFields.add(stringPortalTrackingAuditColumnConfigEntry.getKey());
						description.append(StringUtils.NEW_LINE).append(stringPortalTrackingAuditColumnConfigEntry.getKey()).append(": [").append(PortalTrackingAuditUtils.getPropertyValueAsString(bean, stringPortalTrackingAuditColumnConfigEntry.getKey())).append("]");
					}
				}
			}
		}
		// Nothing to Audit
		if (eventType.isUpdate() && CollectionUtils.isEmpty(auditedFields)) {
			return null;
		}
		return BeanUtils.getLabel(bean) + description.toString();
	}


	/**
	 * Custom handling for Dates to show more friendly and shorter string value
	 */
	private static <T> String getPropertyValueAsString(T bean, String field) {
		Object beanValue = BeanUtils.getPropertyValue(bean, field);
		if (beanValue instanceof Date) {
			Date dateBeanValue = (Date) beanValue;
			return DateUtils.fromDateSmart(dateBeanValue);
		}
		return StringUtils.toNullableString(beanValue);
	}


	/**
	 * Similar to core CompareUtils, but special comparison for dates
	 * If the date is saved with time, but the database doesn't have time, the hibernate object still has the time, but original object doesn't
	 * In other cases with time, the milliseconds appears to be the same but it's still registering as different, so clear that and compare to the minute
	 */
	private static int compareForAuditUpdate(Object o1, Object o2) {
		int compareResult = CompareUtils.compare(o1, o2);
		if (compareResult != 0) {
			if (o1 instanceof Date && o2 instanceof Date) {
				Date d1 = (Date) o1;
				if (d1 instanceof Timestamp) {
					d1 = new Date(d1.getTime());
				}
				Date d2 = (Date) o2;
				if (d2 instanceof Timestamp) {
					d2 = new Date(d2.getTime());
				}
				// If either value doesn't have time, then compare without time
				if (DateUtils.getMillisecondsFromMidnight(d1) == 0 || DateUtils.getMillisecondsFromMidnight(d2) == 0) {
					return DateUtils.compare(d1, d2, false);
				}
				// Else - clear milliseconds and try again
				else {
					d1 = DateUtils.clearMilliseconds(d1);
					d2 = DateUtils.clearMilliseconds(d2);
					return DateUtils.compare(d1, d2, true);
				}
			}
		}
		return compareResult;
	}


	public static PortalTrackingEventTypes getPortalTrackingAuditTrailEventTypeForDaoEventType(DaoEventTypes eventType) {
		if (eventType == DaoEventTypes.INSERT) {
			return PortalTrackingEventTypes.AUDIT_TRAIL_INSERT;
		}
		if (eventType == DaoEventTypes.DELETE) {
			return PortalTrackingEventTypes.AUDIT_TRAIL_DELETE;
		}
		return PortalTrackingEventTypes.AUDIT_TRAIL_UPDATE;
	}
}
