package com.clifton.portal.tracking.generator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.mvc.WebRequestUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.tracking.PortalTrackingEvent;
import com.clifton.portal.tracking.PortalTrackingEventMethod;
import com.clifton.portal.tracking.PortalTrackingEventType;
import com.clifton.portal.tracking.PortalTrackingEventTypes;
import com.clifton.portal.tracking.PortalTrackingService;
import com.clifton.portal.tracking.audit.PortalTrackingAuditColumnConfig;
import com.clifton.portal.tracking.audit.PortalTrackingAuditUtils;
import com.clifton.portal.tracking.ip.PortalTrackingIpData;
import com.clifton.portal.tracking.ip.PortalTrackingIpService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalTrackingEventGeneratorImpl</code> handles generation and saving of events
 *
 * @author manderson
 */
@Component
public class PortalTrackingEventGeneratorImpl implements PortalTrackingEventGenerator {

	private PortalFileSetupService portalFileSetupService;
	private PortalSecurityUserService portalSecurityUserService;
	private PortalTrackingIpService portalTrackingIpService;
	private PortalTrackingService portalTrackingService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> void generatePortalTrackingAuditTrailEvent(DaoEventTypes eventType, String tableName, T bean, T originalBean, Map<String, PortalTrackingAuditColumnConfig> columnConfigMap) {
		PortalTrackingEvent event = generatePortalTrackingEvent(PortalTrackingAuditUtils.getPortalTrackingAuditTrailEventTypeForDaoEventType(eventType), null, null);
		if (event != null) {
			event.setSourceTableName(tableName);
			event.setSourceFkFieldId(BeanUtils.getIdentityAsInteger(bean));
			String description = PortalTrackingAuditUtils.generatePortalTrackingAuditEventDescription(eventType, bean, originalBean, columnConfigMap);
			// If description is returned as empty then there is nothing to audit
			if (!StringUtils.isEmpty(description)) {
				event.setDescription(description);
				getPortalTrackingService().savePortalTrackingEvent(event);
			}
		}
	}


	@Override
	public void generatePortalTrackingLoginEvent(HttpServletRequest request, String userName) {
		PortalTrackingEvent event = generatePortalTrackingEvent(PortalTrackingEventTypes.LOGIN, userName, request);
		// Nothing else to add for Login Event
		if (event != null) {
			getPortalTrackingService().savePortalTrackingEvent(event);
		}
	}


	@Override
	public void generatePortalTrackingWebRequestMethodEvent(PortalTrackingEventMethod eventMethodAnnotation, HttpServletRequest request) {
		Map<String, ?> parameterMap = WebRequestUtils.getParameterMap(request);
		PortalTrackingEvent event = generatePortalTrackingEvent(eventMethodAnnotation.eventType(), null, request);
		if (event != null) {
			if (!StringUtils.isEmpty(eventMethodAnnotation.portalEntityIdUrlParameter())) {
				Integer portalEntityId = MapUtils.getParameterAsInteger(eventMethodAnnotation.portalEntityIdUrlParameter(), parameterMap);
				event.setPortalEntityId(portalEntityId);
			}

			Set<String> additionalIgnoreProperties = new HashSet<>();
			List<Integer> sourceFkFieldIdList = getSourceFkFieldIdListFromRequest(event.getTrackingEventType(), eventMethodAnnotation, request, additionalIgnoreProperties);
			event.setDescription(getDescriptionFromRequest(eventMethodAnnotation, request, additionalIgnoreProperties));

			// File Downloads - If source Fk field id is required but doesn't exist - don't log the event
			if (CollectionUtils.isEmpty(sourceFkFieldIdList) && eventMethodAnnotation.eventType().isWebRequestRequireFkFieldId()) {
				// Log at warn level so we know this is happening
				LogUtils.warn(PortalTrackingEventGeneratorImpl.class, "Not saving event because missing required source fk field id.  Event Details: " + event);
				return;
			}

			if (CollectionUtils.getSize(sourceFkFieldIdList) > 1) {
				List<PortalTrackingEvent> saveList = new ArrayList<>();
				for (Integer sourceFkFieldId : sourceFkFieldIdList) {
					PortalTrackingEvent trackingEvent = BeanUtils.cloneBean(event, false, false);
					trackingEvent.setSourceFkFieldId(sourceFkFieldId);
					saveList.add(trackingEvent);
				}
				getPortalTrackingService().savePortalTrackingEventList(saveList);
			}
			else {
				Integer sourceFkFieldId = CollectionUtils.getFirstElement(sourceFkFieldIdList);
				event.setSourceFkFieldId(sourceFkFieldId);
				getPortalTrackingService().savePortalTrackingEvent(event);
			}
		}
	}


	@Override
	public void generatePortalTrackingNotificationAcknowledgementEvent(int portalNotificationId, Boolean acknowledge, Set<PortalEntity> portalEntitySet) {
		PortalTrackingEvent event = generatePortalTrackingEvent(PortalTrackingEventTypes.NOTIFICATION_ACKNOWLEDGEMENT, null, null);
		if (event != null) {
			event.setDescription(acknowledge == null ? null : acknowledge + "");
			event.setSourceFkFieldId(portalNotificationId);

			if (!CollectionUtils.isEmpty(portalEntitySet)) {
				portalEntitySet.forEach(portalEntityId -> {
					PortalTrackingEvent clonedEvent = BeanUtils.cloneBean(event, false, false);
					clonedEvent.setPortalEntityId(portalEntityId.getId());
					getPortalTrackingService().savePortalTrackingEvent(clonedEvent);
				});
			}
			else {
				getPortalTrackingService().savePortalTrackingEvent(event);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalTrackingEvent generatePortalTrackingEvent(PortalTrackingEventTypes eventType, String userName, HttpServletRequest request) {
		// Note: Exception will be thrown if event type with specified name doesn't exist
		PortalTrackingEventType type = getPortalTrackingService().getPortalTrackingEventTypeByName(eventType.getName());
		PortalSecurityUser securityUser = (!StringUtils.isEmpty(userName)) ? getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false) : getPortalSecurityUserService().getPortalSecurityUserCurrent();
		if (type.isExcludeNonPortalEntitySpecific() && !securityUser.getUserRole().isPortalEntitySpecific()) {
			return null;
		}
		PortalTrackingEvent event = new PortalTrackingEvent();
		event.setPortalSecurityUser(securityUser);
		event.setTrackingEventType(type);
		if (request != null) {
			event.setPortalTrackingIpData(getPortalTrackingIpData(request));
		}
		event.setEventDate(new Date());
		return event;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////               Request Tracking Helper Methods              //////////
	////////////////////////////////////////////////////////////////////////////////


	private List<Integer> getSourceFkFieldIdListFromRequest(PortalTrackingEventType eventType, PortalTrackingEventMethod eventMethodAnnotation, HttpServletRequest request, Set<String> additionalIgnoreProperties) {
		Map<String, ?> parameterMap = WebRequestUtils.getParameterMap(request);
		List<Integer> sourceFkFieldIdList = new ArrayList<>();
		if (!StringUtils.isEmpty(eventMethodAnnotation.eventType().getWebRequestFkFieldIdUrlParameter())) {
			Integer sourceFkFieldId = MapUtils.getParameterAsInteger(eventMethodAnnotation.eventType().getWebRequestFkFieldIdUrlParameter(), parameterMap);
			if (sourceFkFieldId != null) {
				sourceFkFieldIdList.add(sourceFkFieldId);
			}
		}
		else if (!StringUtils.isEmpty(eventMethodAnnotation.eventType().getWebRequestFkFieldIdArrayUrlParameter())) {
			String result = MapUtils.getParameterAsString(eventMethodAnnotation.eventType().getWebRequestFkFieldIdArrayUrlParameter(), parameterMap);
			String[] resultArray = (StringUtils.isEmpty(result) ? null : result.split(","));
			if (resultArray != null && resultArray.length > 0) {
				for (String fkFieldId : resultArray) {
					sourceFkFieldIdList.add(new Integer(fkFieldId));
				}
			}
		}
		if (CollectionUtils.isEmpty(sourceFkFieldIdList) && "PortalFileCategory".equals(eventType.getSourceTableName())) {
			String fileGroupName = MapUtils.getParameterAsString("fileGroupName", parameterMap);
			if (!StringUtils.isEmpty(fileGroupName)) {
				PortalFileCategory category = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, fileGroupName);
				if (category != null) {
					additionalIgnoreProperties.add("fileGroupName");  // If mapped by ID, then ignore in description
					sourceFkFieldIdList.add(category.getId().intValue());
				}
			}
			// Else See if "categoryName" is present and if so, get the category by name and set that as the id
			else {
				String categoryName = MapUtils.getParameterAsString("categoryName", parameterMap);
				if (!StringUtils.isEmpty(categoryName)) {
					// Note: Assume this property would only be used at the highest level?  We are not unique across categories on Name only
					PortalFileCategory category = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY, categoryName);
					if (category != null) {
						additionalIgnoreProperties.add("categoryName");  // If mapped by ID, then ignore in description
						sourceFkFieldIdList.add(category.getId().intValue());
					}
				}
			}
		}
		return sourceFkFieldIdList;
	}


	private String getDescriptionFromRequest(PortalTrackingEventMethod eventMethodAnnotation, HttpServletRequest request, Set<String> additionalIgnoreProperties) {
		StringBuilder descriptionSb = new StringBuilder();
		if (!StringUtils.isEmpty(eventMethodAnnotation.eventType().getWebRequestDescriptionUrlParameters())) {
			Map<String, ?> parameterMap = WebRequestUtils.getParameterMap(request);
			String[] parameterNames = eventMethodAnnotation.eventType().getWebRequestDescriptionUrlParameters().split(",");
			boolean first = true;
			for (String parameterName : parameterNames) {
				if (!additionalIgnoreProperties.contains(parameterName)) {
					String value = MapUtils.getParameterAsString(parameterName, parameterMap);
					if (!StringUtils.isEmpty(value)) {
						if (!first) {
							descriptionSb.append(",");
						}
						descriptionSb.append(parameterName).append("=").append(value);
						first = false;
					}
				}
			}
		}
		return descriptionSb.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortalTrackingIpData getPortalTrackingIpData(HttpServletRequest request) {
		if (request != null) {
			// I'm not sure this actually works - my IP keeps coming up as 0:0:0:0:0:0:0:1
			String ipAddress = request.getHeader("X-Forwarded-For");
			if (StringUtils.isEmpty(ipAddress)) {
				ipAddress = request.getRemoteAddr();
			}
			else {
				ipAddress = ipAddress.split(",")[0];
			}
			return getPortalTrackingIpService().getPortalTrackingIpServiceByIp(ipAddress);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalTrackingIpService getPortalTrackingIpService() {
		return this.portalTrackingIpService;
	}


	public void setPortalTrackingIpService(PortalTrackingIpService portalTrackingIpService) {
		this.portalTrackingIpService = portalTrackingIpService;
	}


	public PortalTrackingService getPortalTrackingService() {
		return this.portalTrackingService;
	}


	public void setPortalTrackingService(PortalTrackingService portalTrackingService) {
		this.portalTrackingService = portalTrackingService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
