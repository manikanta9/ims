package com.clifton.portal.tracking.ip;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Date;


/**
 * The <code>PortalTrackingIpServiceImpl</code> manages the ip data for request events.
 *
 * @author mwacker
 */
public class PortalTrackingIpServiceImpl implements PortalTrackingIpService {

	private AdvancedUpdatableDAO<PortalTrackingIpData, Criteria> portalTrackingIpDataDAO;
	private IpDataProvider ipDataProvider;

	private int monthsToRequery = 6;
	private int ipNoOlderThanDateMonths = 12;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalTrackingIpData getPortalTrackingIpServiceByIp(final String ip) {
		final Date ipNoOlderThanDate = DateUtils.addMonths(DateUtils.clearTime(new Date()), -1 * getIpNoOlderThanDateMonths());
		HibernateSearchConfigurer config = new HibernateSearchConfigurer() {

			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.addOrder(Order.desc("queryDate"));
				return true;
			}


			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("ipAddress", ip));
				criteria.add(Restrictions.or(Restrictions.ge("queryDate", ipNoOlderThanDate), Restrictions.isNull("queryDate")));
			}
		};
		PortalTrackingIpData data = CollectionUtils.getFirstElement(getPortalTrackingIpDataDAO().findBySearchCriteria(config));
		if (data == null || data.getQueryDate() == null || DateUtils.getMonthsDifference(DateUtils.clearTime(new Date()), DateUtils.addMonths(data.getQueryDate(), getMonthsToRequery())) >= 0) {
			PortalTrackingIpData result = getIpDataProvider().getIpData(ip);
			// if no ip data is returned, and there is existing data use the existing data
			if (result == null && data != null) {
				return data;
			}
			// if no data exists then create the ip data with just the ip address
			else if (result == null) {
				result = new PortalTrackingIpData();
				result.setIpAddress(ip);
			}
			getPortalTrackingIpDataDAO().save(result);
			return result;
		}
		return data;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalTrackingIpData, Criteria> getPortalTrackingIpDataDAO() {
		return this.portalTrackingIpDataDAO;
	}


	public void setPortalTrackingIpDataDAO(AdvancedUpdatableDAO<PortalTrackingIpData, Criteria> portalTrackingIpDataDAO) {
		this.portalTrackingIpDataDAO = portalTrackingIpDataDAO;
	}


	public int getMonthsToRequery() {
		return this.monthsToRequery;
	}


	public void setMonthsToRequery(int monthsToRequery) {
		this.monthsToRequery = monthsToRequery;
	}


	public IpDataProvider getIpDataProvider() {
		return this.ipDataProvider;
	}


	public void setIpDataProvider(IpDataProvider ipDataProvider) {
		this.ipDataProvider = ipDataProvider;
	}


	public int getIpNoOlderThanDateMonths() {
		return this.ipNoOlderThanDateMonths;
	}


	public void setIpNoOlderThanDateMonths(int ipNoOlderThanDateMonths) {
		this.ipNoOlderThanDateMonths = ipNoOlderThanDateMonths;
	}
}
