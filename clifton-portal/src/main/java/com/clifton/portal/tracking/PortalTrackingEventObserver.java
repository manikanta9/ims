package com.clifton.portal.tracking;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalTrackingEventObserver</code> truncates the description prior to saving to prevent truncation errors for cases where before and after values are both too long to fit within the 4000 character limit (i.e. notification definition email templates)
 *
 * @author manderson
 */
@Component
public class PortalTrackingEventObserver extends SelfRegisteringDaoObserver<PortalTrackingEvent> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortalTrackingEvent> dao, DaoEventTypes event, PortalTrackingEvent bean) {
		bean.setDescription(StringUtils.formatStringUpToNCharsWithDots(bean.getDescription(), DataTypes.DESCRIPTION_LONG.getLength(), true));
	}
}
