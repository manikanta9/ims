package com.clifton.portal.tracking.audit;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Map;


/**
 * The <code>PortalTrackingAuditAwareObserverRegistrator</code> class is a Spring {@link ApplicationListener} that
 * registers {@link PortalTrackingAuditAwareObserver}s for DAO's whose DTO classes are annotated with {@link PortalTrackingAuditAware}
 *
 * @author manderson
 */
public class PortalTrackingAuditAwareObserverRegistrator<T extends IdentityObject> implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			ApplicationContext context = event.getApplicationContext();
			Map<String, UpdatableDAO<T>> beanMap = (Map<String, UpdatableDAO<T>>) (Map<?, ?>) context.getBeansOfType(UpdatableDAO.class);
			for (Map.Entry<String, UpdatableDAO<T>> stringUpdatableDAOEntry : beanMap.entrySet()) {
				UpdatableDAO<T> updatableDAO = stringUpdatableDAOEntry.getValue();
				// register only for DAO's that whose DTOs are annotated with PortalTrackingAuditAware
				if (updatableDAO.getConfiguration() != null) {
					Class<T> clazz = updatableDAO.getConfiguration().getBeanClass();
					if (AnnotationUtils.isAnnotationPresent(clazz, PortalTrackingAuditAware.class)) {
						registerObservers(updatableDAO, clazz, AnnotationUtils.getAnnotation(clazz, PortalTrackingAuditAware.class));
						LogUtils.info(getClass(), "Registered Portal Tracking Audit Aware Observer for dao: " + stringUpdatableDAOEntry.getKey());
					}
				}
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering PortalTrackingAuditAwareObservers: " + event, e);
		}
	}


	@SuppressWarnings("unchecked")
	public void registerObservers(UpdatableDAO<T> updatableDAO, Class<T> clazz, PortalTrackingAuditAware portalTrackingAuditAware) {
		// get DAO that's being observed
		ObserverableDAO<T> dao = (ObserverableDAO<T>) updatableDAO;
		DaoEventObserver<T> observer = new PortalTrackingAuditAwareObserver<>(PortalTrackingAuditUtils.getPortalTrackingAuditColumnConfigMap(clazz));
		getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(observer, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// register observers as necessary
		if (portalTrackingAuditAware.insert()) {
			dao.registerEventObserver(DaoEventTypes.INSERT, observer);
		}
		if (portalTrackingAuditAware.update()) {
			dao.registerEventObserver(DaoEventTypes.UPDATE, observer);
		}
		if (portalTrackingAuditAware.delete()) {
			dao.registerEventObserver(DaoEventTypes.DELETE, observer);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
