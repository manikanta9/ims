package com.clifton.portal.tracking.ip;


/**
 * The <code>IpDataProvider</code> interface defines the method for getting the details for a given ip address
 */
public interface IpDataProvider {

	public PortalTrackingIpData getIpData(String ip);
}
