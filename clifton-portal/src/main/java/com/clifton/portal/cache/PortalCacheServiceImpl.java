package com.clifton.portal.cache;

import com.clifton.core.beans.NamedObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileAssignment;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedHandler;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalCacheServiceImpl implements PortalCacheService {

	private ApplicationContextService applicationContextService;

	private CacheHandler<?, ?> cacheHandler;

	private DaoLocator daoLocator;

	private PortalEntityService portalEntityService;

	private PortalFileSetupService portalFileSetupService;

	private PortalFileService portalFileService;

	private PortalSecurityUserService portalSecurityUserService;

	private PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void reloadPortalCache(boolean doNotClearCache) {
		if (!doNotClearCache) {
			clearPortalCache();
		}
		loadAllLookupTables();
		loadAllSelfRegisteringNamedEntityCache();
		loadPortalFileCategories();
		loadPortalEntitiesSecurable();
		loadPortalEntitiesFileSource();
		loadPortalSecurityUsersAndPermissions();
		loadPortalFiles();
	}

	////////////////////////////////////////////////////////////////////////////////


	private void clearPortalCache() {
		String[] caches = getCacheHandler().getCacheNames();
		if (caches != null && caches.length > 0) {
			for (String cacheName : caches) {
				try {
					getCacheHandler().clear(cacheName);
				}
				catch (Throwable e) {
					String msg = "Error clearing cache [" + cacheName + "]: " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), msg, e);
				}
			}
			LogUtils.info(getClass(), "Cleared [" + caches.length + "] caches.");
		}
	}


	/**
	 * Runs findAll on all Dao's that use a Short for PK
	 * which means the data in the table will be small and used for look ups
	 */
	private void loadAllLookupTables() {
		int count = 0;
		Collection<ReadOnlyDAO<?>> daoList = getDaoLocator().locateAll();

		for (ReadOnlyDAO<?> dao : CollectionUtils.getIterable(daoList)) {
			DAOConfiguration<?> config = dao.getConfiguration();
			// Skip DAO Configs with no Tables
			if (config instanceof NoTableDAOConfig<?>) {
				continue;
			}
			if (!config.getTable().isCacheUsed()) {
				continue;
			}

			try {
				DataTypes dt = config.getPrimaryKeyColumn().getDataType();
				if (DataTypes.IDENTITY_SHORT == dt) {
					dao.findAll();
					count++;
				}
			}
			catch (Throwable e) {
				String msg = "Error loading table " + config.getTableName() + " for pre-caching: " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), msg, e);
			}
		}
		LogUtils.info(getClass(), "All Rows loaded for [" + count + "] table(s).");
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private void loadAllSelfRegisteringNamedEntityCache() {
		Map<String, NamedEntityCache> cacheMap = getApplicationContextService().getContextBeansOfType(NamedEntityCache.class);

		int count = 0;
		if (cacheMap != null) {
			// for each DAO event cache bean
			for (Map.Entry<String, NamedEntityCache> stringNamedEntityCacheEntry : cacheMap.entrySet()) {
				try {
					NamedEntityCache<NamedObject> cacheBean = stringNamedEntityCacheEntry.getValue();
					Class dtoClass = cacheBean.getDtoClass();
					ReadOnlyDAO<NamedObject> dao = getDaoLocator().locate(dtoClass);
					List<NamedObject> list = dao.findAll();

					for (NamedObject bean : CollectionUtils.getIterable(list)) {
						cacheBean.setBean(bean);
					}
					count++;
				}
				catch (Throwable e) {
					String msg = "Error loading cache " + stringNamedEntityCacheEntry.getKey() + ": " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), msg, e);
				}
			}
		}
		LogUtils.info(getClass(), "Entities loaded for [" + count + "] BaseDaoNamedEntityCache(s).");
	}


	private void loadPortalSecurityUsersAndPermissions() {
		// Load All Non Disabled Users and Their Permissions
		PortalSecurityUserSearchForm securityUserSearchForm = new PortalSecurityUserSearchForm();
		securityUserSearchForm.setDisabled(false);
		List<PortalSecurityUser> portalSecurityUserList = getPortalSecurityUserService().getPortalSecurityUserList(securityUserSearchForm, true);
		for (PortalSecurityUser securityUser : CollectionUtils.getIterable(portalSecurityUserList)) {
			getPortalSecurityUserService().getPortalSecurityUserByUserName(securityUser.getUsername(), false);
		}
		LogUtils.info(getClass(), "User Related Caches loaded for [" + CollectionUtils.getSize(portalSecurityUserList) + "] User(s).");
	}


	private void loadPortalFileCategories() {
		List<PortalFileCategory> categoryList = getPortalFileSetupService().getPortalFileCategoryList(new PortalFileCategorySearchForm());
		for (PortalFileCategory fileCategory : CollectionUtils.getIterable(categoryList)) {
			getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(fileCategory.getFileCategoryType(), fileCategory.getName());
			if (fileCategory.getFileCategoryType() != PortalFileCategoryTypes.FILE_GROUP) {
				getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId());
			}
		}

		LogUtils.info(getClass(), "File Category Related Caches loaded for [" + CollectionUtils.getSize(categoryList) + "] Categories.");
	}


	private void loadPortalEntitiesSecurable() {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setSecurableEntity(true);
		searchForm.setSecurableEntityHasApprovedFilesPosted(true);
		List<PortalEntity> portalEntityList = getPortalEntityService().getPortalEntityList(searchForm);

		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			// Load All Field Values and Related Entity List
			getPortalEntityService().getPortalEntityFieldValueListByEntity(portalEntity.getId());
			getPortalEntityService().getRelatedPortalEntityList(portalEntity.getId(), true, true, true, true);
		}

		LogUtils.info(getClass(), "Portal Entity Related Caches loaded for [" + CollectionUtils.getSize(portalEntityList) + "] Securable Entities.");
	}


	private void loadPortalEntitiesFileSource() {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setFileSource(true);

		List<PortalEntity> portalEntityList = getPortalEntityService().getPortalEntityList(searchForm);

		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			// Load All Field Values and find by source
			getPortalEntityService().getPortalEntityBySourceSection(portalEntity.getEntitySourceSection().getId(), portalEntity.getSourceFkFieldId(), true);
		}

		LogUtils.info(getClass(), "Portal Entity Related Caches loaded for [" + CollectionUtils.getSize(portalEntityList) + "] File Source Entities.");
	}


	private void loadPortalFiles() {
		// Pre-Load all default files and who has access to them
		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setDefaultDisplay(true);
		List<PortalFile> portalFileList = getPortalFileService().getPortalFileList(searchForm);

		// Specific files aren't secured, internally we care about security resource, externally, we care about security resource and entity both tracked in this set
		Set<String> loadedKeys = new HashSet<>();

		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			loadPortalSecurityUserAccessForFile(portalFile, loadedKeys);
		}

		LogUtils.info(getClass(), "Portal File User Access Related Caches loaded for [" + CollectionUtils.getSize(portalFileList) + "] Files.");
	}


	private void loadPortalSecurityUserAccessForFile(PortalFile portalFile, Set<String> loadedKeys) {
		short securityResourceId = portalFile.getFileCategory().getSecurityResource().getId();
		// Internal User Access
		loadPortalSecurityUserAccessForSecurityResourceAndEntity(securityResourceId, null, loadedKeys);

		// External User Access
		// Global Files apply to all
		if (portalFile.isGlobalFile()) {
			// nothing to pre-cache???
		}
		else {
			// If specific posting
			if (portalFile.getPostPortalEntity() != null && portalFile.getPostPortalEntity().getEntitySourceSection().getEntityType().isSecurableEntity()) {
				loadPortalSecurityUserAccessForSecurityResourceAndEntity(securityResourceId, portalFile.getPostPortalEntity().getId(), loadedKeys);
			}
			else {
				// Need to Check PortalFileAssignment Table
				List<PortalFileAssignment> assignmentList = getPortalFileService().getPortalFileAssignmentListByFile(portalFile.getId());
				for (PortalFileAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
					loadPortalSecurityUserAccessForSecurityResourceAndEntity(securityResourceId, assignment.getReferenceTwo().getId(), loadedKeys);
				}
			}
		}
	}


	private void loadPortalSecurityUserAccessForSecurityResourceAndEntity(short securityResourceId, Integer portalEntityId, Set<String> loadedKeys) {
		String loadKey = securityResourceId + "_" + ObjectUtils.coalesce(portalEntityId, "INTERNAL");
		if (!loadedKeys.contains(loadKey)) {
			loadedKeys.add(loadKey);
			getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(securityResourceId, portalEntityId);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalSecurityUserAssignmentExpandedHandler getPortalSecurityUserAssignmentExpandedHandler() {
		return this.portalSecurityUserAssignmentExpandedHandler;
	}


	public void setPortalSecurityUserAssignmentExpandedHandler(PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler) {
		this.portalSecurityUserAssignmentExpandedHandler = portalSecurityUserAssignmentExpandedHandler;
	}
}
