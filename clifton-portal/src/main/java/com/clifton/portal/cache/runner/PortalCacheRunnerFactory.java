package com.clifton.portal.cache.runner;


import java.util.Date;


/**
 * The <code>PortalCacheRunnerFactory</code> interface defines methods for creating PortalCache instances.
 */
public interface PortalCacheRunnerFactory {

	/**
	 * Create a top level portal cache job runner.
	 */
	public PortalCacheRunner createPortalCacheRunner(Date runDate);
}
