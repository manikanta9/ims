package com.clifton.portal.cache.runner;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The <code>PortalCacheRunnerProvider</code> class returns PortalNotificationDefinition Runner instances that are scheduled for the specified time
 * period.
 * NOTE: THIS COMPONENT IS DEFINED IN SPRING CONTEXT
 */
public class PortalCacheRunnerProvider implements RunnerProvider<Object> {


	private PortalCacheRunnerFactory portalCacheRunnerFactory;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * What time of day to run the cache load - i.e. 4am
	 */
	private Set<Time> runTimeSet;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		List<Runner> results = new ArrayList<>();

		Time startTime = new Time(startDateTime);
		Time endTime = new Time(endDateTime);

		for (Time time : CollectionUtils.getIterable(getRunTimeSet())) {
			if (startTime.compareTo(time) <= 0 && endTime.compareTo(time) >= 0) {
				// SCHEDULE IT
				Date runDate = DateUtils.clearTime(startDateTime);
				runDate = DateUtils.setTime(runDate, time);
				results.add(getPortalCacheRunnerFactory().createPortalCacheRunner(runDate));
			}
		}
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(Object entity, Date runnerDate) {
		// UNUSED
		return null;
	}


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(Object entity, Date startDateTime, Date endDateTime) {
		// UNUSED
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortalCacheRunnerFactory getPortalCacheRunnerFactory() {
		return this.portalCacheRunnerFactory;
	}


	public void setPortalCacheRunnerFactory(PortalCacheRunnerFactory portalCacheRunnerFactory) {
		this.portalCacheRunnerFactory = portalCacheRunnerFactory;
	}


	public Set<Time> getRunTimeSet() {
		return this.runTimeSet;
	}


	public void setRunTimeSet(Set<Time> runTimeSet) {
		this.runTimeSet = runTimeSet;
	}
}
