package com.clifton.portal.cache.runner;


import com.clifton.core.context.ApplicationContextService;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class PortalCacheRunnerFactoryImpl implements PortalCacheRunnerFactory {

	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalCacheRunner createPortalCacheRunner(Date runDate) {
		PortalCacheRunner runner = new PortalCacheRunner(runDate);
		getApplicationContextService().autowireBean(runner);
		return runner;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
