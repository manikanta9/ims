package com.clifton.portal.cache.runner;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.portal.cache.PortalCacheService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;

import java.util.Date;


/**
 * The <code>PortalCacheRunner</code> class represents a Runner for a loading portal cache
 * <p>
 */
public class PortalCacheRunner extends AbstractStatusAwareRunner {

	public static final String RUNNER_TYPE = "PORTAL_CACHE_LOAD";

	////////////////////////////////////////////////////////////////////////////

	private PortalCacheService portalCacheService;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PortalCacheRunner(Date runDate) {
		super(RUNNER_TYPE, "ALL", runDate);
	}


	@Override
	public void run() {
		getStatus().setMessage("Started on " + DateUtils.fromDate(new Date()));
		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> getPortalCacheService().reloadPortalCache(false));
		getStatus().setMessage("Finished on " + DateUtils.fromDate(new Date()));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalCacheService getPortalCacheService() {
		return this.portalCacheService;
	}


	public void setPortalCacheService(PortalCacheService portalCacheService) {
		this.portalCacheService = portalCacheService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}
}
