package com.clifton.portal.converter.json.appenders;

import com.clifton.core.converter.json.appenders.ObjectValueAppender;

import java.util.Map;


/**
 * Used to filter out sensitive user properties during response serialization.
 * It is currently used for the client portal so we only return the fields that are needed by the UI.
 * For an example mapping configuration see app-clifton-client-portal-web.xml
 *
 * @author KellyJ
 */
public class PortalSecurityUserValueAppender extends ObjectValueAppender {

	@Override
	protected void removeProperties(Map<String, Object> beanMap) {
		super.removeProperties(beanMap);
		beanMap.remove("password");
		beanMap.remove("passwordUpdateDate");
		beanMap.remove("accountLocked");
		beanMap.remove("accountNonExpired");
		beanMap.remove("accountNonLocked");
		beanMap.remove("createDate");
		beanMap.remove("createUserId");
		beanMap.remove("credentialsNonExpired");
		beanMap.remove("newBean");
		beanMap.remove("termsOfUseAcceptanceDate");
		beanMap.remove("termsOfUseAccepted");
		beanMap.remove("updateDate");
		beanMap.remove("updateUserId");
		beanMap.remove("enabled");
		beanMap.remove("disabled");
		beanMap.remove("identity");
		beanMap.remove("rv");
	}
}
