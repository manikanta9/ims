package com.clifton.portal.file.setup.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>PortalFileCategoryChildrenListCacheImpl</code> caches list of child portal file category ids for a given file category at the level of "File Group" which is the level where files are assigned.
 * <p>
 * The cache is cleared when there is a parent change (insert of a new file category or update of existing)
 *
 * @author Mary Anderson
 */
@Component
public class PortalFileCategoryChildrenListCacheImpl extends SelfRegisteringSimpleDaoCache<PortalFileCategory, Short, Short[]> implements PortalFileCategoryChildrenListCache {


	@Override
	public Short[] getPortalFileCategoryChildrenIdList(short portalFileCategoryId, ReadOnlyDAO<PortalFileCategory> portalFileCategoryDAO) {
		Short[] result = getCacheHandler().get(getCacheName(), portalFileCategoryId);
		if (result == null) {
			Set<Short> childrenIdList = new HashSet<>();
			PortalFileCategory portalFileCategory = portalFileCategoryDAO.findByPrimaryKey(portalFileCategoryId);
			addAllPortalFileCategoryChildren(portalFileCategory, childrenIdList, portalFileCategoryDAO);
			if (CollectionUtils.isEmpty(childrenIdList)) {
				result = new Short[0];
			}
			else {
				result = childrenIdList.toArray(new Short[0]);
			}
			getCacheHandler().put(getCacheName(), portalFileCategoryId, result);
		}
		return result;
	}


	protected void addAllPortalFileCategoryChildren(PortalFileCategory portalFileCategory, Set<Short> childrenIdList, ReadOnlyDAO<PortalFileCategory> portalFileCategoryDAO) {
		// AS LONG AS NOT A FILE GROUP - GET IT'S CHILDREN
		if (portalFileCategory != null && portalFileCategory.getFileCategoryType() != PortalFileCategoryTypes.FILE_GROUP) {
			List<PortalFileCategory> childrenList = portalFileCategoryDAO.findByField("parent.id", portalFileCategory.getId());
			for (PortalFileCategory childCategory : CollectionUtils.getIterable(childrenList)) {
				// If it's a File Group - add it to the children list...
				if (PortalFileCategoryTypes.FILE_GROUP == childCategory.getFileCategoryType()) {
					childrenIdList.add(childCategory.getId());
				}
				// Otherwise load up it's children
				else {
					addAllPortalFileCategoryChildren(childCategory, childrenIdList, portalFileCategoryDAO);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                   Observer Methods                    //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<PortalFileCategory> dao, DaoEventTypes event, PortalFileCategory bean, Throwable e) {
		if (e == null) {
			if (event.isUpdate()) {
				PortalFileCategory originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CompareUtils.isEqual(originalBean.getParent(), bean.getParent())) {
					clearCacheForPortalFileCategory(originalBean.getParent());
					clearCacheForPortalFileCategory(bean.getParent());
				}
			}
			else if (bean.getParent() != null) {
				clearCacheForPortalFileCategory(bean.getParent());
			}
		}
	}


	private void clearCacheForPortalFileCategory(PortalFileCategory portalFileCategory) {
		if (portalFileCategory != null) {
			getCacheHandler().remove(getCacheName(), portalFileCategory.getId());
			clearCacheForPortalFileCategory(portalFileCategory.getParent());
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<PortalFileCategory> dao, DaoEventTypes event, PortalFileCategory bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}
}
