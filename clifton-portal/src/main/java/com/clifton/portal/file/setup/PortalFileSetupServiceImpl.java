package com.clifton.portal.file.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.setup.cache.PortalFileCategoryChildrenListCache;
import com.clifton.portal.file.setup.cache.PortalFileCategoryListByUploadAccessCache;
import com.clifton.portal.file.setup.observers.PortalFileCategoryObserver;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedHandler;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalFileSetupServiceImpl implements PortalFileSetupService {

	public static final String CACHE_CONTACT_US_CATEGORY_IDS = "CONTACT_US_CATEGORY_IDS";

	private AdvancedUpdatableDAO<PortalFileCategory, Criteria> portalFileCategoryDAO;

	private DaoCompositeKeyCache<PortalFileCategory, PortalFileCategoryTypes, String> portalFileCategoryByCategoryTypeAndNameCache;
	private PortalFileCategoryListByUploadAccessCache portalFileCategoryListByUploadAccessCache;

	private PortalEntityService portalEntityService;
	private PortalSecurityUserService portalSecurityUserService;
	private PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler;

	private PortalFileCategoryChildrenListCache portalFileCategoryChildrenListCache;


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Category Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFileCategory getPortalFileCategory(short id) {
		return getPortalFileCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public PortalFileCategory getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes categoryType, String categoryName) {
		return getPortalFileCategoryByCategoryTypeAndNameCache().getBeanForKeyValues(getPortalFileCategoryDAO(), categoryType, categoryName);
	}


	@Override
	public List<PortalFileCategory> getPortalFileCategoryList(PortalFileCategorySearchForm searchForm) {
		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("orderExpanded");
		}
		return getPortalFileCategoryDAO().findBySearchCriteria(new PortalFileCategorySearchFormConfigurer(searchForm, getPortalSecurityUserService(), getPortalEntityService()));
	}


	@Override
	public List<PortalFileCategory> getPortalFileCategoryForUploadList(PortalFileCategorySearchForm searchForm, String categoryName, Short postEntityTypeId, boolean includeParents) {
		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		List<PortalFileCategory> categoryList = getPortalFileCategoryListByUploadAccessCache().getPortalFileCategoryUploadListForUser(currentUser.getId(), getPortalFileCategoryDAO());
		// If not in the cache, get the list for the user and cache the full list
		if (categoryList == null) {
			PortalFileCategorySearchForm categorySearchForm = new PortalFileCategorySearchForm();
			if (!currentUser.getUserRole().isAdministrator()) {
				List<PortalSecurityUserAssignmentExpanded> expandedList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForUser(currentUser.getId());
				categorySearchForm.setSecurityResourceIds(BeanUtils.getPropertyValues(expandedList, PortalSecurityUserAssignmentExpanded::getPortalSecurityResourceId, Short.class));
				// No access - give user error message
				if (categorySearchForm.getSecurityResourceIds() == null || categorySearchForm.getSecurityResourceIds().length == 0) {
					getPortalFileCategoryListByUploadAccessCache().setPortalFileCategoryUploadListForUser(currentUser.getId(), new ArrayList<>());
					throw new ValidationException("You do not have access to post to any file groups.");
				}
			}
			categoryList = getPortalFileCategoryList(categorySearchForm);
			getPortalFileCategoryListByUploadAccessCache().setPortalFileCategoryUploadListForUser(currentUser.getId(), categoryList);
		}
		if (!CollectionUtils.isEmpty(categoryList)) {
			if (!StringUtils.isEmpty(categoryName)) {
				categoryList = BeanUtils.filter(categoryList, category -> StringUtils.isEqual(categoryName, category.getRootParent().getName()));
			}
			if (postEntityTypeId != null) {
				categoryList = BeanUtils.filter(categoryList, category -> (category.getPostEntityType() == null || category.getPostEntityType().getId().equals(postEntityTypeId)));
			}
		}
		// Finally if not including parents - return only those at FILE_GROUP level
		if (!includeParents) {
			categoryList = BeanUtils.filter(categoryList, category -> PortalFileCategoryTypes.FILE_GROUP == category.getFileCategoryType());
		}
		// Then filter on the search pattern if entered, and return PagingArrayList for UI combo box
		if (!CollectionUtils.isEmpty(categoryList) && searchForm != null && !StringUtils.isEmpty(searchForm.getSearchPattern())) {
			return new PagingArrayList<>(BeanUtils.filter(categoryList, category -> StringUtils.isLikeIgnoringCase(category.getName(), searchForm.getSearchPattern())));
		}
		return new PagingArrayList<>(categoryList);
	}


	@Override
	public Short[] getPortalFileGroupIdsForPortalFileCategory(short portalFileCategoryId) {
		return getPortalFileCategoryChildrenListCache().getPortalFileCategoryChildrenIdList(portalFileCategoryId, getPortalFileCategoryDAO());
	}


	@Override
	public Short[] getPortalFileCategoryIdsForContactUsCategories() {
		PortalFileCategory contactUsCategory = getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY, PortalFileCategory.CATEGORY_NAME_CONTACT_US);
		return getPortalFileGroupIdsForPortalFileCategory(contactUsCategory.getId());
	}


	/**
	 * See {@link PortalFileCategoryObserver} for validation
	 */
	@Override
	public PortalFileCategory savePortalFileCategory(PortalFileCategory bean) {
		if (bean.isNewBean() || StringUtils.isEmpty(bean.getFolderName())) {
			String folderName = FileUtils.replaceInvalidCharacters(bean.getName(), "");
			if (folderName.length() > 50) {
				folderName = folderName.substring(0, 50);
			}
			bean.setFolderName(folderName);
		}
		return getPortalFileCategoryDAO().save(bean);
	}


	/**
	 * See {@link PortalFileCategoryObserver} for validation
	 */
	@Override
	public void deletePortalFileCategory(short id) {
		getPortalFileCategoryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortalFileCategory, Criteria> getPortalFileCategoryDAO() {
		return this.portalFileCategoryDAO;
	}


	public void setPortalFileCategoryDAO(AdvancedUpdatableDAO<PortalFileCategory, Criteria> portalFileCategoryDAO) {
		this.portalFileCategoryDAO = portalFileCategoryDAO;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public PortalSecurityUserAssignmentExpandedHandler getPortalSecurityUserAssignmentExpandedHandler() {
		return this.portalSecurityUserAssignmentExpandedHandler;
	}


	public void setPortalSecurityUserAssignmentExpandedHandler(PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler) {
		this.portalSecurityUserAssignmentExpandedHandler = portalSecurityUserAssignmentExpandedHandler;
	}


	public DaoCompositeKeyCache<PortalFileCategory, PortalFileCategoryTypes, String> getPortalFileCategoryByCategoryTypeAndNameCache() {
		return this.portalFileCategoryByCategoryTypeAndNameCache;
	}


	public void setPortalFileCategoryByCategoryTypeAndNameCache(DaoCompositeKeyCache<PortalFileCategory, PortalFileCategoryTypes, String> portalFileCategoryByCategoryTypeAndNameCache) {
		this.portalFileCategoryByCategoryTypeAndNameCache = portalFileCategoryByCategoryTypeAndNameCache;
	}


	public PortalFileCategoryListByUploadAccessCache getPortalFileCategoryListByUploadAccessCache() {
		return this.portalFileCategoryListByUploadAccessCache;
	}


	public void setPortalFileCategoryListByUploadAccessCache(PortalFileCategoryListByUploadAccessCache portalFileCategoryListByUploadAccessCache) {
		this.portalFileCategoryListByUploadAccessCache = portalFileCategoryListByUploadAccessCache;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalFileCategoryChildrenListCache getPortalFileCategoryChildrenListCache() {
		return this.portalFileCategoryChildrenListCache;
	}


	public void setPortalFileCategoryChildrenListCache(PortalFileCategoryChildrenListCache portalFileCategoryChildrenListCache) {
		this.portalFileCategoryChildrenListCache = portalFileCategoryChildrenListCache;
	}
}
