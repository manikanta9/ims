package com.clifton.portal.file.setup.security;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.core.util.MathUtils;


/**
 * The <code>PortalFileCategorySecurityResourceExpanded</code> stores for each security resource the file categories that can apply
 * This is used for rollup file category views - so a user has access to "Investment Statements" folder, then they also have access to category sub set Accounting Reporting, and then ultimately Reporting
 * <p>
 * This is used for Database Level Joins Only and not used in code
 *
 * @author manderson
 */
public class PortalFileCategorySecurityResourceExpanded extends BaseSimpleEntity<Integer> {

	private Short portalSecurityResourceId;

	private Short portalFileCategoryId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategorySecurityResourceExpanded() {
		//
	}


	public PortalFileCategorySecurityResourceExpanded(PortalFileCategory portalFileCategory, short portalSecurityResourceId) {
		this.portalFileCategoryId = portalFileCategory.getId();
		this.portalSecurityResourceId = portalSecurityResourceId;
	}


	@Override
	public boolean equals(Object o) {
		// Equal If all Same File Category / Security Resource
		if (!(o instanceof PortalFileCategorySecurityResourceExpanded)) {
			return false;
		}
		PortalFileCategorySecurityResourceExpanded oBean = (PortalFileCategorySecurityResourceExpanded) o;
		if (MathUtils.isEqual(this.getPortalFileCategoryId(), oBean.getPortalFileCategoryId())) {
			if (MathUtils.isEqual(this.getPortalSecurityResourceId(), oBean.getPortalSecurityResourceId())) {
				return true;
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPortalFileCategoryId() {
		return this.portalFileCategoryId;
	}


	public void setPortalFileCategoryId(Short portalFileCategoryId) {
		this.portalFileCategoryId = portalFileCategoryId;
	}


	public Short getPortalSecurityResourceId() {
		return this.portalSecurityResourceId;
	}


	public void setPortalSecurityResourceId(Short portalSecurityResourceId) {
		this.portalSecurityResourceId = portalSecurityResourceId;
	}
}
