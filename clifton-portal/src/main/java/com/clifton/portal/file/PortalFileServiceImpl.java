package com.clifton.portal.file;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoThreeKeyListCache;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.email.PortalEmailService;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.search.PortalFileExtendedSearchFormConfigurer;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMappingService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.file.template.PortalFileTemplate;
import com.clifton.portal.file.template.PortalFileTemplateGenerator;
import com.clifton.portal.security.resource.PortalSecurityResource;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedHandler;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import com.clifton.portal.tracking.PortalTrackingEventMethod;
import com.clifton.portal.tracking.PortalTrackingEventTypes;
import com.clifton.portal.tracking.audit.PortalTrackingAuditAwareObserver;
import org.hibernate.Criteria;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalFileServiceImpl implements PortalFileService {

	/**
	 * When retrieving source entity list under Relationship Manager category, the team cc email addresses are appended to the PortalEntityFieldValue list
	 * This is not saved, and varies depending on the relationship, client, OCIO, etc.
	 */
	private static final String RELATIONSHIP_MANAGER_TEAM_CC_EMAIL_ADDRESS_FIELD_TYPE_NAME = "CCEmailAddress";

	private static final String RELATIONSHIP_MANAGER_PORTRAIT_FILE = "PortalFileId";

	private static final int MAX_DOWNLOAD_FILE_COUNT = 75; // Also update portal-api-shared if this number changes

	private AdvancedUpdatableDAO<PortalFile, Criteria> portalFileDAO;
	private AdvancedReadOnlyDAO<PortalFileExtended, Criteria> portalFileExtendedDAO;
	private AdvancedUpdatableDAO<PortalFileAssignment, Criteria> portalFileAssignmentDAO;

	private DaoSingleKeyListCache<PortalFileAssignment, Integer> portalFileAssignmentCache;
	private DaoThreeKeyListCache<PortalFile, Short, String, Integer> portalFileListBySourceCache;

	private JdbcTemplate jdbcTemplate;

	private PortalEmailService portalEmailService; // Note: Circular Dependency

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;
	private PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService;
	private PortalFileSetupService portalFileSetupService;
	private PortalFileTemplateGenerator portalFileTemplateGenerator;
	private PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler;
	private PortalSecurityUserService portalSecurityUserService;

	private RunnerHandler runnerHandler;

	/**
	 * The root location where the files are stored
	 */
	private String rootDirectory;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPortalFileRootDirectory() {
		return getRootDirectory();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                  Portal File Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFile getPortalFile(int id) {
		return getPortalFileDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortalFile> getPortalFileList(final PortalFileSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getDefaultDisplay() != null) {
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(searchForm.getDefaultDisplay(), null, "reportDate", "endDate", null));
				}
			}
		};
		return getPortalFileDAO().findBySearchCriteria(config);
	}


	/**
	 * Limits the file searches for those only where the current user has write access
	 * i.e. What are my files pending approval
	 */
	@Override
	public List<PortalFile> getPortalFileListWithEditAccess(final PortalFileSearchForm searchForm, String supportTeamEmail) {
		List<PortalFileCategory> categoryList = getPortalFileSetupService().getPortalFileCategoryForUploadList(new PortalFileCategorySearchForm(), null, null, false);
		searchForm.setFileGroupIds(BeanUtils.getBeanIdentityArray(categoryList, Short.class));

		// If filtering by supportTeamEmail Track Start/Limit and Reset On SearchForm so filtered results are accurate and not just filtered off of the first page
		int start = searchForm.getStart();
		int pageSize = searchForm.getLimit();

		if (!StringUtils.isEmpty(supportTeamEmail)) {
			searchForm.setStart(PagingCommand.DEFAULT_START);
			searchForm.setLimit(PagingCommand.DEFAULT_LIMIT);
		}
		List<PortalFile> fileList = getPortalFileList(searchForm);

		if (!StringUtils.isEmpty(supportTeamEmail)) {
			List<PortalFile> filteredList = new ArrayList<>();
			for (PortalFile portalFile : CollectionUtils.getIterable(fileList)) {
				List<String> teamEmailList = getPortalEmailService().getPortalSupportEmailAddressListForPortalFile(portalFile);
				if (CollectionUtils.contains(teamEmailList, supportTeamEmail)) {
					filteredList.add(portalFile);
				}
			}
			// Reset Limits on the Search Form to what they were
			searchForm.setStart(start);
			searchForm.setLimit(pageSize);

			// Need to return PagingArrayList with proper start/size/page size so filtering in UI works properly
			// If more than one page of data
			int size = CollectionUtils.getSize(filteredList);
			if (size > pageSize) {
				int maxIndex = start + pageSize;
				if (maxIndex > size) {
					maxIndex = size;
				}
				// Filter actual rows returned to the current page
				filteredList = filteredList.subList(start, maxIndex);
			}
			// Return Paging Array List with (if more than one page, then list is already a subset of data, else all the data), start index and the total size of the original list without paging
			return new PagingArrayList<>(filteredList, start, size);
		}
		return fileList;
	}


	@Override
	public List<PortalFile> getPortalFileListForSourceEntity(String sourceSystemName, String sourceTableName, Integer sourceFkFieldId) {
		PortalEntitySourceSystem sourceSystem = getPortalEntitySetupService().getPortalEntitySourceSystemByName(sourceSystemName);
		return getPortalFileListBySourceCache().getBeanListForKeyValues(getPortalFileDAO(), sourceSystem.getId(), sourceTableName, sourceFkFieldId);
	}


	@Override
	public Set<Integer> getPortalFileSourceFkFieldIdList(String sourceSystemName, String sourceTableName, Date minReportDate, boolean approvedOnly) {
		// This uses custom sql instead of the search form because we only need one integer field, not the fully populated object
		Object[] params = new Object[]{sourceTableName};
		StringBuilder sql = new StringBuilder(50);
		sql.append("SELECT f.SourceFKFieldID ");
		sql.append(" FROM PortalFile f ");
		sql.append(" INNER JOIN PortalEntitySourceSection ss ON f.PortalEntitySourceSectionID = ss.PortalEntitySourceSectionID ");
		sql.append(" WHERE ss.PortalEntitySourceSystemID = ");
		sql.append(getPortalEntitySetupService().getPortalEntitySourceSystemByName(sourceSystemName).getId());
		sql.append(" AND ss.SourceSystemTableName = ? ");
		if (minReportDate != null) {
			sql.append(" AND f.ReportDate >= ? ");
			params = new Object[]{sourceTableName, minReportDate};
		}
		if (approvedOnly) {
			sql.append(" AND f.IsApproved = 1 ");
		}
		sql.append(" GROUP BY f.SourceFKFieldID ");
		return new HashSet<>(getJdbcTemplate().queryForList(sql.toString(), Integer.class, params));
	}


	@Override
	public PortalFile savePortalFile(PortalFile portalFile) {
		if (portalFile.isNewBean()) {
			// Don't think you can get here directly since all existing cases go through the savePortalFileNew method, but if you can want to ensure we go through all of the validation for new files
			portalFile = savePortalFileNew(portalFile, null, null, FileDuplicateActions.ERROR);
		}
		else {
			validatePortalFileAccess(portalFile, getPortalSecurityUserService().getPortalSecurityUserCurrent(), false);
			applyPortalFileSystemGeneratedProperties(portalFile, true);
			portalFile = getPortalFileDAO().save(portalFile);
		}
		return portalFile;
	}


	@Override
	public PortalFile savePortalFileManual(PortalFile portalFile, List<PortalFileAssignment> assignmentList) {
		// Currently manual files are relationship managers and team assignments we don't have files for them
		// although we may have them for relationship managers we wouldn't have the file set here so make sure the file is empty to avoid confusion if someone tries to use this with a file
		ValidationUtils.assertNull(portalFile.getFile(), "File Uploads are not currently supported for manual portal files.");
		// Required Admin Security, so not necessary to check permissions
		if (CollectionUtils.isEmpty(assignmentList)) {
			if (portalFile.isNewBean()) {
				// Do Nothing
			}
			else {
				deletePortalFile(portalFile.getId());
			}
			return null;
		}
		else {
			List<PortalFileAssignment> oldAssignmentList = (portalFile.isNewBean() ? null : getPortalFileAssignmentListByFile(portalFile.getId()));
			if (portalFile.isNewBean()) {
				applyPortalFileSystemGeneratedProperties(portalFile, false);
			}

			portalFile = getPortalFileDAO().save(portalFile);
			// Make sure Portal File is set on each assignment - if it's a new bean the reference to the bean is lost when passed from external service
			for (PortalFileAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
				assignment.setReferenceOne(portalFile);
			}
			getPortalFileAssignmentDAO().saveList(assignmentList, oldAssignmentList);
		}
		return portalFile;
	}


	@Override
	@Transactional
	public PortalFile savePortalFileNew(PortalFile portalFile, File file, String originalFileName, FileDuplicateActions fileDuplicateAction) {
		// We null out the file here, because if it was populated we've pulled it out to a separate parameter and we don't want the return value to reference the file
		// Otherwise, if called via an external service, then the response will attempt to send the file back.
		portalFile.setFile(null);
		String fileDocumentType = (file == null ? null : FileUtils.getFileExtension(file.getName()));
		File originalFile = null; // If we are replacing a file, then we'll do that delete last to ensure no other errors
		validatePortalFileAccess(portalFile, getPortalSecurityUserService().getPortalSecurityUserCurrent(), false);

		// Give an error to the user if they are posting to an entity that is already ended
		if (portalFile.getPostPortalEntity() != null && !portalFile.getPostPortalEntity().isActive()) {
			throw new ValidationException("Cannot post to [" + portalFile.getPostPortalEntity().getEntityLabelWithSourceSection() + "] because it has already been terminated and de-activated on the portal.");
		}

		applyPortalFileSystemGeneratedProperties(portalFile, false);

		// Validate this file doesn't already exist
		List<PortalFile> existingFileList = getPortalFileExistingList(portalFile, fileDocumentType);
		// There should only ever be one - historically there could be more, but going forward this validation prevents that.  So, if more than one just always throw an exception
		if (!CollectionUtils.isEmpty(existingFileList)) {
			if (fileDuplicateAction == null) {
				fileDuplicateAction = FileDuplicateActions.ERROR;
			}
			String fileDetails = "matched on the following properties: " + "File Category: " + portalFile.getFileCategory().getName() //
					+ ", Post Entity: " + (portalFile.getPostPortalEntity() == null ? "Global File" : portalFile.getPostPortalEntity().getEntityLabelWithSourceSection()) //
					+ ", Report Date: " + DateUtils.fromDateShort(portalFile.getReportDate()) //
					+ (!StringUtils.isEmpty(fileDocumentType) ? "" : ", File Type: " + fileDocumentType) //
					+ ", Display Name: " + portalFile.getDisplayName();

			if (CollectionUtils.getSize(existingFileList) > 1) {
				throw new PortalFileDuplicateException("This file appears to be a duplicate file.  Found [" + existingFileList.size() + "] files that " + fileDetails, false);
			}
			PortalFile existingFile = existingFileList.get(0);
			if (fileDuplicateAction.isError()) {
				if (existingFile.isApproved()) {
					throw new PortalFileDuplicateException("This file appears to be a duplicate file of an existing approved file.  If you wish to replace the file, please unapprove the existing file and use the Replace option. File Found that " + fileDetails, false);
				}
				else {
					throw new PortalFileDuplicateException("This file appears to be a duplicate file of an existing un-approved file.  If you wish to replace the file, please use the Replace option. File Found that " + fileDetails, true);
				}
			}
			// Use the Same ID
			if (fileDuplicateAction.isReplaceFile()) {
				if (existingFile.isApproved()) {
					throw new ValidationException("This file appears to be a duplicate file, and the file cannot be replaced, because the original is still marked as approved.  If you wish to replace the file, please unapprove the existing file and use the Replace option. File Found that " + fileDetails);
				}
				portalFile.setId(existingFile.getId());
				portalFile.setCreateUserId(existingFile.getCreateUserId()); // Create User and Create Date Need to Be set otherwise they don't sync back up with the database after the update
				portalFile.setCreateDate(existingFile.getCreateDate());
				portalFile.setRv(existingFile.getRv()); // This needs to be set to prevent "versioning" errors

				// Delete the original file
				if (!StringUtils.isEmpty(existingFile.getFileNameAndPath())) {
					originalFile = new File(FileUtils.combinePath(getRootDirectory(), existingFile.getFileNameAndPath()));
				}
			}
			else {
				// Skipping the file - return null so no action is performed
				return null;
			}
		}

		// As Long as it's really a new file - set up assignments, otherwise not necessary and we are just updating the existing file which already has it's assignments
		boolean newPortalFile = portalFile.isNewBean();

		// Portal File is Good to Save, so let's save the real file
		if (file != null) {
			String fileName = PortalFileUtils.generatePortalFileName(portalFile, StringUtils.coalesce(true, originalFileName, file.getName()), true);
			try {
				FileUtils.copyFileCreatePath(file, FileUtils.combinePath(getPortalFileRootDirectory(), fileName));
			}
			catch (IOException e) {
				throw new ValidationException("Error saving file " + file.getName() + ": " + e.getMessage(), e);
			}
			portalFile.setFileCreateUserId(getPortalSecurityUserService().getPortalSecurityUserCurrent().getId());
			portalFile.setFileNameAndPath(fileName);
			portalFile.setFileSize(file.length());
		}
		portalFile = getPortalFileDAO().save(portalFile);

		if (newPortalFile) {
			generateAndSavePortalFileAssignmentListForPortalFile(portalFile, null);
		}

		if (originalFile != null && originalFile.exists()) {
			FileUtils.delete(originalFile);
		}
		return portalFile;
	}


	private void applyPortalFileSystemGeneratedProperties(PortalFile portalFile, boolean doNotSetApprovalFields) {
		ValidationUtils.assertNotNull(portalFile.getFileCategory(), "File Group is Required.");
		ValidationUtils.assertTrue(portalFile.getFileCategory().getFileCategoryType() == PortalFileCategoryTypes.FILE_GROUP, "You can only post files to File Group categories.");
		if (portalFile.isApproved() && !doNotSetApprovalFields) {
			portalFile.setApprovedByUserId(getPortalSecurityUserService().getPortalSecurityUserCurrent().getId());
			portalFile.setApprovalDate(new Date());
		}

		// Reset Report Date based on current date select and frequency
		PortalFileFrequencies frequency = portalFile.getCoalescePortalFileFrequency();
		portalFile.setReportDate(frequency.getReportDateForDate(portalFile.getReportDate()));

		// Clear File Frequency if same as Category
		if (portalFile.getFileCategory().getPortalFileFrequency() == frequency) {
			portalFile.setFileFrequency(null);
		}

		PortalFileTemplate template = getPortalFileTemplateGenerator().generatePortalFileTemplate(portalFile);
		portalFile.setDisplayName(template.getDisplayName());
		portalFile.setDisplayText(template.getDisplayText());

		if (portalFile.getEndDate() == null) {
			if (portalFile.getFileCategory().getDefaultDaysToDisplay() != null) {
				portalFile.setEndDate(DateUtils.addDays(portalFile.getReportDate(), portalFile.getFileCategory().getDefaultDaysToDisplay()));
			}
			else {
				portalFile.setEndDate(frequency.getDefaultDisplayEndDate(portalFile.getReportDate()));
			}
		}
		if (portalFile.getPublishDate() == null) {
			portalFile.setPublishDate(new Date());
		}
	}


	/**
	 * Historically we aren't going to check this, but for new files posted, we check if there is already an existing file with the same properties:
	 * File Category, Post Entity, Report Date, File Type (pdf, xls, csv), Display Name
	 */
	private List<PortalFile> getPortalFileExistingList(PortalFile portalFile, String fileDocumentType) {
		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		// File Category
		searchForm.setFileGroupId(portalFile.getFileCategory().getId());
		// Post Entity (or Global File)
		if (portalFile.getPostPortalEntity() != null) {
			searchForm.setPostPortalEntityId(portalFile.getPostPortalEntity().getId());
		}
		else {
			searchForm.setPostPortalEntityIdPopulated(false);
		}
		// Report Date
		searchForm.setReportDate(portalFile.getReportDate());
		// File Type (Searches File Name & Path ends with)
		if (!StringUtils.isEmpty(fileDocumentType)) {
			searchForm.setFileType(fileDocumentType);
		}
		else {
			searchForm.setFileNameAndPathPopulated(false);
		}
		// Display Name
		searchForm.setDisplayNameEquals(portalFile.getDisplayName());

		return getPortalFileList(searchForm);
	}


	@Override
	@Transactional
	public void movePortalFileList(Integer[] portalFileIds, short portalFileCategoryId) {
		PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategory(portalFileCategoryId);
		ValidationUtils.assertTrue(fileCategory.getFileCategoryType() == PortalFileCategoryTypes.FILE_GROUP, "You can only post files to File Group categories.  Selected category [" + fileCategory.getName() + "] is invalid.");

		List<PortalFile> portalFileList = getPortalFileDAO().findByPrimaryKeys(portalFileIds);
		List<PortalFile> saveList = new ArrayList<>();

		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();

		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			PortalFileCategory currentCategory = portalFile.getFileCategory();

			// If it's already there - skip it
			if (!currentCategory.equals(fileCategory)) {
				// Validate access to previous category
				validatePortalFileAccess(portalFile, currentUser, false);
				portalFile.setFileCategory(fileCategory);
				// Validate access to new category - filtered in UI dropdown, but to be sure validate again
				if (!currentCategory.getSecurityResource().equals(fileCategory.getSecurityResource())) {
					validatePortalFileAccess(portalFile, currentUser, false);
				}
				saveList.add(portalFile);
			}
		}
		getPortalFileDAO().saveList(saveList);
	}


	@Override
	@Transactional
	public void approvePortalFileList(Integer[] portalFileIds, boolean approve) {
		List<PortalFile> portalFileList = getPortalFileDAO().findByPrimaryKeys(portalFileIds);
		List<PortalFile> saveList = new ArrayList<>();

		PortalSecurityUser currentUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();

		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			// Approver Can Never Be Same User as Create User when approved explicitly (unless a system Administrator)
			if (approve && MathUtils.isEqual(currentUser.getId(), portalFile.getCoalesceFileCreateUserId()) && !currentUser.getUserRole().isAdministrator()) {
				throw new ValidationException("You cannot approve file " + getPortalFileTemplateGenerator().generatePortalFileName(portalFile) + " because you created the file.");
			}
			validatePortalFileAccess(portalFile, currentUser, false);
			if (portalFile.isApproved() != approve) {
				portalFile.setApproved(approve);
				if (approve) {
					portalFile.setApprovalDate(new Date());
					portalFile.setApprovedByUserId(currentUser.getId());
				}
				else {
					portalFile.setApprovalDate(null);
					portalFile.setApprovedByUserId(null);
				}
				saveList.add(portalFile);
			}
		}
		getPortalFileDAO().saveList(saveList);
	}


	@Override
	@Transactional
	public void deletePortalFile(int id) {
		PortalFile portalFile = getPortalFile(id);
		validatePortalFileAccess(portalFile, getPortalSecurityUserService().getPortalSecurityUserCurrent(), false);
		if (!StringUtils.isEmpty(portalFile.getFileNameAndPath())) {
			File file = new File(FileUtils.combinePath(getRootDirectory(), portalFile.getFileNameAndPath()));
			if (file.exists()) {
				FileUtils.delete(file);
			}
		}
		getPortalFileAssignmentDAO().deleteList(getPortalFileAssignmentListByFile(id));
		getPortalFileDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Template Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFileTemplate getPortalFileTemplateForPortalFile(int portalFileId) {
		PortalFile portalFile = getPortalFile(portalFileId);
		return getPortalFileTemplateGenerator().generatePortalFileTemplate(portalFile);
	}


	@Override
	public void generatePortalFileTemplatesForPortalFiles(Integer[] portalFileIds) {
		generatePortalFileTemplatesForPortalFileList(getPortalFileDAO().findByPrimaryKeys(portalFileIds));
	}


	@Override
	public void generatePortalFileTemplatesForPortalFilesBySource(Integer portalEntitySourceSectionId, Integer sourceFkFieldId) {
		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setEntitySourceSectionId(portalEntitySourceSectionId);
		searchForm.setSourceFkFieldId(sourceFkFieldId);
		List<PortalFile> fileList = getPortalFileList(searchForm);
		if (!CollectionUtils.isEmpty(fileList)) {
			generatePortalFileTemplatesForPortalFiles(BeanUtils.getBeanIdentityArray(fileList, Integer.class));
		}
	}


	@Override
	public void generatePortalFileTemplatesForPortalFileCategory(short fileCategoryId) {
		generatePortalFileTemplatesForPortalFileList(getPortalFileDAO().findByField("fileCategory.id", fileCategoryId));
	}


	// Note: Did not make this transactional because there is one save list call that is already transactional
	protected void generatePortalFileTemplatesForPortalFileList(List<PortalFile> portalFileList) {
		List<PortalFile> saveList = new ArrayList<>();
		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			PortalFileTemplate template = getPortalFileTemplateGenerator().generatePortalFileTemplate(portalFile);
			boolean save = false;
			if (!StringUtils.isEqual(portalFile.getDisplayName(), template.getDisplayName())) {
				save = true;
				portalFile.setDisplayName(template.getDisplayName());
			}
			if (!StringUtils.isEqual(portalFile.getDisplayText(), template.getDisplayText())) {
				save = true;
				portalFile.setDisplayText(template.getDisplayText());
			}
			if (save) {
				saveList.add(portalFile);
			}
		}
		// Disable Audit Trail Tracking for System Updated Fields
		DaoUtils.executeWithSpecificObserversDisabled(() -> getPortalFileDAO().saveList(saveList), PortalTrackingAuditAwareObserver.class);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Download Methods                //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_DOWNLOAD)
	public FileWrapper downloadPortalFile(int portalFileId, Short viewAsUserId) {
		File file = downloadPortalFileImpl(portalFileId, viewAsUserId, null);
		PortalFile portalFile = getPortalFile(portalFileId);
		String fileName = getPortalFileTemplateGenerator().generatePortalFileName(portalFile);
		return new FileWrapper(file, fileName + "." + FileUtils.getFileExtension(file.getName()), false);
	}


	@Override
	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_DOWNLOAD)
	public FileWrapper downloadPortalFileList(Integer[] portalFileIds, Short viewAsUserId, boolean zipFiles) {
		if (portalFileIds == null || portalFileIds.length == 0) {
			throw new ValidationException("No files selected to download for " + (zipFiles ? "Zip file" : "PDF Concatenation") + ".");
		}
		if (portalFileIds.length > MAX_DOWNLOAD_FILE_COUNT) {
			throw new ValidationException("You have selected " + portalFileIds.length + " files. Please limit your selection to " + MAX_DOWNLOAD_FILE_COUNT + " files or less.");
		}
		String fileName = "Files_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE);
		// Zipping
		if (zipFiles) {
			List<FileWrapper> fileList = new ArrayList<>();
			for (Integer portalFileId : portalFileIds) {
				fileList.add(downloadPortalFile(portalFileId, viewAsUserId));
			}
			String fileNameAndPath = FileUtils.combinePath(FileUtils.JAVA_TEMP_DIRECTORY, fileName + ".zip");
			FileContainer file = ZipUtils.zipFiles(fileList, fileNameAndPath);
			return new FileWrapper(FilePath.forPath(file.getPath()), file.getName(), true);
		}
		// Single File (No Zipping)
		if (portalFileIds.length == 1) {
			return downloadPortalFile(portalFileIds[0], viewAsUserId);
		}
		// PDF Concatenation
		List<File> fileList = new ArrayList<>();
		for (Integer portalFileId : portalFileIds) {
			fileList.add(downloadPortalFileImpl(portalFileId, viewAsUserId, FileFormats.PDF));
		}
		File file = FileUtils.concatenatePDFs("Files_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE), ".pdf", fileList, false, false);
		file.deleteOnExit();
		return new FileWrapper(file, file.getName(), true);
	}


	private File downloadPortalFileImpl(int portalFileId, Short viewAsUserId, FileFormats requiredType) {
		PortalFile portalFile = getPortalFile(portalFileId);
		if (portalFile == null) {
			throw new ValidationException("Missing Portal File with ID: " + portalFileId);
		}
		if (StringUtils.isEmpty(portalFile.getFileNameAndPath())) {
			throw new ValidationException("Missing File for Portal File with ID: " + portalFileId);
		}
		if (requiredType != null && requiredType != FileFormats.getEnum(FileUtils.getFileExtension(portalFile.getFileNameAndPath()))) {
			throw new ValidationException("Selected File Type [" + FileUtils.getFileExtension(portalFile.getFileNameAndPath()) + "] is not valid.  File must be of type [" + requiredType.name() + "].");
		}
		PortalSecurityUser viewAsUser = (viewAsUserId != null ? getPortalSecurityUserService().getPortalSecurityUser(viewAsUserId) : null);
		validatePortalFileAccess(portalFile, viewAsUser, true);
		return new File(FileUtils.combinePath(getRootDirectory(), portalFile.getFileNameAndPath()));
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////               Portal File Extended Methods                 //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_VIEW)
	public List<PortalFileExtended> getPortalFileExtendedList(final PortalFileExtendedSearchForm searchForm) {
		// Convert Name Equals Search Fields to IDs for Performance Improvements
		if (!StringUtils.isEmpty(searchForm.getCategoryName())) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY, searchForm.getCategoryName());
			if (fileCategory != null) {
				searchForm.setCategoryName(null);
				searchForm.addToFileGroupIds(getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId()));
			}
		}
		if (!StringUtils.isEmpty(searchForm.getCategorySubsetName())) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY_SUBSET, searchForm.getCategoryName());
			if (fileCategory != null) {
				searchForm.addToFileGroupIds(getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId()));
				searchForm.setCategorySubsetName(null);
			}
		}
		if (!StringUtils.isEmpty(searchForm.getExcludeCategorySubsetName())) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY_SUBSET, searchForm.getCategoryName());
			if (fileCategory != null) {
				searchForm.addToExcludeFileGroupIds(getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId()));
				searchForm.setExcludeCategorySubsetName(null);
			}
		}
		if (!StringUtils.isEmpty(searchForm.getExcludeCategoryName())) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.CATEGORY, searchForm.getExcludeCategoryName());
			if (fileCategory != null) {
				searchForm.addToExcludeFileGroupIds(getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId()));
				searchForm.setExcludeCategoryName(null);
			}
		}
		if (!StringUtils.isEmpty(searchForm.getFileGroupName())) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, searchForm.getFileGroupName());
			if (fileCategory != null) {
				searchForm.setFileGroupId(fileCategory.getId());
				searchForm.setFileGroupName(null);
			}
		}
		if (searchForm.getFileCategoryIdExpanded() != null) {
			PortalFileCategory fileCategory = getPortalFileSetupService().getPortalFileCategory(searchForm.getFileCategoryIdExpanded());
			if (fileCategory != null) {
				if (fileCategory.getFileCategoryType() == PortalFileCategoryTypes.FILE_GROUP) {
					searchForm.setFileGroupId(fileCategory.getId());
				}
				else {
					searchForm.addToFileGroupIds(getPortalFileSetupService().getPortalFileGroupIdsForPortalFileCategory(fileCategory.getId()));
				}
			}
			searchForm.setFileCategoryIdExpanded(null);
		}

		if (searchForm.getMaxReportDate() != null && searchForm.getMinReportDate() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(searchForm.getMaxReportDate(), searchForm.getMinReportDate()), "The start date [" + DateUtils.fromDateShort(searchForm.getMinReportDate()) + "] must be equal to or before the end date [" + DateUtils.fromDateShort(searchForm.getMaxReportDate()) + "].", "startDate");
		}
		return getPortalFileExtendedDAO().findBySearchCriteria(new PortalFileExtendedSearchFormConfigurer(searchForm, getPortalSecurityUserService(), getPortalEntityService()));
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                 Portal File Entity Methods                   /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Should be used rarely - currently will be used by Client UI to get Relationship Manager and Team Email information
	 * Same tracking and security as the file extended list
	 */
	@Override
	@PortalTrackingEventMethod(eventType = PortalTrackingEventTypes.FILE_LIST_VIEW)
	public List<PortalEntity> getPortalFileSourceEntityList(PortalFileExtendedSearchForm searchForm) {
		searchForm.setEntitySourceSectionIdPopulated(true);
		searchForm.setSourceFkFieldIdPopulated(true);
		List<PortalFileExtended> fileExtendedList = getPortalFileExtendedList(searchForm);
		fileExtendedList = filterPortalFileExtendedListByMostSpecificAssignment(fileExtendedList);

		Map<String, PortalEntity> portalEntityMap = new HashMap<>(); // Track the source entities already retrieved so we don't get them again

		for (PortalFileExtended fileExtended : CollectionUtils.getIterable(fileExtendedList)) {
			String key = getSourceKeyForPortalFileExtended(fileExtended);
			PortalEntity sourceEntity = portalEntityMap.get(key);
			if (sourceEntity == null) {
				sourceEntity = getPortalEntityService().getPortalEntityBySourceSection(fileExtended.getEntitySourceSection().getId(), fileExtended.getSourceFkFieldId(), true);
			}
			if (sourceEntity != null) {
				// If we are retrieving relationship managers - dynamically add the Team emails that need to be CC'd as a field value
				if (PortalFileCategory.CATEGORY_NAME_RELATIONSHIP_MANAGER.equals(fileExtended.getFileCategory().getName())) {
					List<String> ccEmailAddressList = getPortalEmailService().getPortalContactTeamEmailAddressListForPortalFile(fileExtended);
					if (!CollectionUtils.isEmpty(ccEmailAddressList)) {
						PortalEntityFieldValue ccFieldValue = CollectionUtils.getOnlyElement(BeanUtils.filter(sourceEntity.getFieldValueList(), portalEntityFieldValue -> RELATIONSHIP_MANAGER_TEAM_CC_EMAIL_ADDRESS_FIELD_TYPE_NAME.equals(portalEntityFieldValue.getPortalEntityFieldType().getName())));
						if (ccFieldValue == null) {
							ccFieldValue = new PortalEntityFieldValue();
							PortalEntityFieldType fieldType = new PortalEntityFieldType();
							fieldType.setName(RELATIONSHIP_MANAGER_TEAM_CC_EMAIL_ADDRESS_FIELD_TYPE_NAME);
							ccFieldValue.setPortalEntityFieldType(fieldType);
							ccFieldValue.setValue("");
							if (sourceEntity.getFieldValueList() == null) {
								sourceEntity.setFieldValueList(new ArrayList<>());
							}
							sourceEntity.getFieldValueList().add(ccFieldValue);
						}
						for (String ccEmail : ccEmailAddressList) {
							if (!ccFieldValue.getValue().contains(ccEmail + ";")) {
								ccFieldValue.setValue(ccFieldValue.getValue() + ccEmail + ";");
							}
						}
					}

					// Add portalFileId if there is a file name and path value set.
					if (fileExtended.getFileNameAndPath() != null) {
						PortalEntityFieldValue fileFieldValue = CollectionUtils.getOnlyElement(BeanUtils.filter(sourceEntity.getFieldValueList(), portalEntityFieldValue -> RELATIONSHIP_MANAGER_PORTRAIT_FILE.equals(portalEntityFieldValue.getPortalEntityFieldType().getName())));

						if (fileFieldValue == null) {
							fileFieldValue = new PortalEntityFieldValue();
							PortalEntityFieldType fieldType = new PortalEntityFieldType();
							fieldType.setName(RELATIONSHIP_MANAGER_PORTRAIT_FILE);
							fileFieldValue.setPortalEntityFieldType(fieldType);
							fileFieldValue.setValue(fileExtended.getPortalFileId().toString());
							if (sourceEntity.getFieldValueList() == null) {
								sourceEntity.setFieldValueList(new ArrayList<>());
							}
							sourceEntity.getFieldValueList().add(fileFieldValue);
						}
					}
				}
				portalEntityMap.put(key, sourceEntity);
			}
		}
		return CollectionUtils.isEmpty(portalEntityMap) ? new ArrayList<>() : BeanUtils.sortWithFunction(new ArrayList<>(portalEntityMap.values()), PortalEntity::getName, true);
	}


	/**
	 * See Jira: INVESTMENT-612 for Relationship Manager Specific Overrides at the Account Level
	 * In this case both RMs are defined at the Relationship Level, One of them is also assigned to a specific account as the override to that account
	 * So, when getting the Relationship Managers with Team CCs, we only want to include the Teams for the most specific assignment
	 * Example: New Mexico PERA - Justin is for all MN accounts, Dan Ryan is an override on just the Seattle account - so when clicking to email Dan Ryan specifically - only include the Seattle Team email
	 */
	private List<PortalFileExtended> filterPortalFileExtendedListByMostSpecificAssignment(List<PortalFileExtended> fileExtendedList) {
		// If none or only one - nothing to filter - this should be most cases
		if (CollectionUtils.getSize(fileExtendedList) <= 1) {
			return fileExtendedList;
		}
		Map<String, List<PortalFileExtended>> sourceFileListMap = BeanUtils.getBeansMap(fileExtendedList, this::getSourceKeyForPortalFileExtended);
		List<PortalFileExtended> filteredPortalFileExtendedList = new ArrayList<>();
		for (Map.Entry<String, List<PortalFileExtended>> stringListEntry : sourceFileListMap.entrySet()) {
			List<PortalFileExtended> sourceFileList = stringListEntry.getValue();
			// If only one - just add it
			if (CollectionUtils.getSize(sourceFileList) == 1) {
				filteredPortalFileExtendedList.add(sourceFileList.get(0));
			}
			else {
				for (int i = 0; i < sourceFileList.size(); i++) {
					PortalEntity postEntity = sourceFileList.get(i).getAssignedPortalEntity();
					boolean foundMoreSpecificAssignment = false;
					// Go Through the List and See if there is another file whose parent = this files assigned entity
					// i.e. Account File overrides Client Relationship File
					for (int j = 0; j < sourceFileList.size(); j++) {
						if (i == j) {
							continue;
						}
						PortalEntity postEntity2 = sourceFileList.get(j).getAssignedPortalEntity();
						if (postEntity2 != null) {
							postEntity2 = postEntity2.getParentPortalEntity();
						}
						while (postEntity2 != null && !foundMoreSpecificAssignment) {
							if (postEntity2.equals(postEntity)) {
								foundMoreSpecificAssignment = true;
							}
							postEntity2 = postEntity2.getParentPortalEntity();
						}
					}
					if (!foundMoreSpecificAssignment) {
						filteredPortalFileExtendedList.add(sourceFileList.get(i));
					}
				}
			}
		}
		return filteredPortalFileExtendedList;
	}


	private String getSourceKeyForPortalFileExtended(PortalFileExtended fileExtended) {
		return fileExtended.getEntitySourceSection().getId() + "-" + fileExtended.getSourceFkFieldId();
	}


	/**
	 * Used for selecting View As Entity logic for a given file
	 * Returns the list of securable portal entities and their parents for view as functionality that have the file assigned to them
	 */
	@Override
	public List<PortalEntity> getPortalFileSecurableEntityListForFile(int portalFileId) {
		PortalFile portalFile = getPortalFile(portalFileId);
		ValidationUtils.assertNotNull(portalFile, "Could not file portal file with id " + portalFileId);
		// Global Files - No Filter - List of All with Approved Files
		if (portalFile.isGlobalFile()) {
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setSecurableEntityHasApprovedFilesPosted(true);
			// If global file limited to specific view type, then limit entities to that view type
			searchForm.setEntityViewTypeId(PortalUtils.getPortalEntityViewTypeId(portalFile));
			searchForm.setOrderBy("labelExpanded");
			return getPortalEntityService().getPortalEntityList(searchForm);
		}

		List<PortalEntity> entityList;

		// If File is Posted Directly to a Securable Entity (That isn't a Rollup)
		if (portalFile.getPostPortalEntity() != null && portalFile.getPostPortalEntity().getEntitySourceSection().getEntityType().isSecurableEntity() && portalFile.getPostPortalEntity().getEntitySourceSection().getChildEntityType() == null) {
			entityList = getPortalEntityService().getRelatedPortalEntityList(portalFile.getPostPortalEntity().getId(), true, true, false, true);
		}
		else {
			// Otherwise we Need to Check File Assignments Table
			List<PortalFileAssignment> fileAssignmentList = getPortalFileAssignmentListByFile(portalFileId);
			entityList = new ArrayList<>();
			for (PortalFileAssignment fileAssignment : CollectionUtils.getIterable(fileAssignmentList)) {
				entityList.addAll(getPortalEntityService().getRelatedPortalEntityList(fileAssignment.getReferenceTwo().getId(), true, true, false, true));
			}
			// Clear Out Potential Duplicates
			entityList = CollectionUtils.getDistinct(entityList, PortalEntity::getId);
		}

		// Sort in Hierarchical and Name order - so we can default to the first one found
		return BeanUtils.sortWithFunction(entityList, PortalEntity::getLabelExpanded, true);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////             Portal File Assignment Methods             ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if a database update is applied
	 */
	private boolean generateAndSavePortalFileAssignmentListForPortalFile(PortalFile portalFile, List<PortalFileAssignment> originalList) {

		// See if we need to add explicit assignments - i.e. Posted to an Entity that is a Rollup (Like Commingled Vehicles)
		if (!portalFile.isGlobalFile()) {
			List<PortalFileAssignment> assignmentList = new ArrayList<>();

			ValidationUtils.assertNotNull(portalFile.getPostPortalEntity(), "Post Portal Entity is Required");
			List<PortalEntityRollup> portalEntityRollups = getPortalEntityService().getPortalEntityRollupListByParent(portalFile.getPostPortalEntity().getId());
			for (PortalEntityRollup rollup : CollectionUtils.getIterable(portalEntityRollups)) {
				// No Report Date - then it applies only to those active today, otherwise, check if active on report date
				if (isPortalFileApplyToPortalEntityRollupChild(portalFile, rollup)) {
					PortalFileAssignment fileAssignment = new PortalFileAssignment();
					fileAssignment.setReferenceOne(portalFile);
					fileAssignment.setReferenceTwo(rollup.getReferenceTwo());
					assignmentList.add(fileAssignment);
				}
			}
			return mergeAndSavePortalFileAssignmentList(originalList, assignmentList);
		}
		return false;
	}


	private boolean isPortalFileApplyToPortalEntityRollupChild(PortalFile portalFile, PortalEntityRollup portalEntityRollup) {
		// No Report Date - then it applies
		if (portalFile.getReportDate() == null) {
			return true;
		}
		// FINAL Document - as long as dated before End Date
		if (portalFile.getCoalescePortalFileFrequency() == PortalFileFrequencies.FINAL) {
			return portalEntityRollup.getEndDate() == null || DateUtils.isDateBeforeOrEqual(portalFile.getReportDate(), portalEntityRollup.getEndDate(), false);
		}
		// Otherwise Active on Report Date
		else {
			return portalEntityRollup.isActiveOnDate(portalFile.getReportDate());
		}
	}


	/**
	 * We don't want to actually trigger saves if nothing is being updated
	 * So this will merge the original and new and only insert where missing and delete where no longer applies
	 * <p>
	 * Returns true if a database update is applied
	 */
	private boolean mergeAndSavePortalFileAssignmentList(List<PortalFileAssignment> originalList, List<PortalFileAssignment> newList) {
		// All new Assignments, just save the list
		if (CollectionUtils.isEmpty(originalList) && !CollectionUtils.isEmpty(newList)) {
			getPortalFileAssignmentDAO().saveList(newList);
			return true;
		}
		// Have existing assignments, but nothing applies now - delete the original list
		else if (!CollectionUtils.isEmpty(originalList) && CollectionUtils.isEmpty(newList)) {
			getPortalFileAssignmentDAO().deleteList(originalList);
			return true;
		}

		boolean save = false;
		Map<PortalEntity, PortalFileAssignment> originalMap = BeanUtils.getBeanMap(originalList, PortalFileAssignment::getReferenceTwo);
		Map<PortalEntity, PortalFileAssignment> newMap = BeanUtils.getBeanMap(newList, PortalFileAssignment::getReferenceTwo);

		// Iterate through the existing, if not in the new map delete it
		for (Map.Entry<PortalEntity, PortalFileAssignment> originalEntry : originalMap.entrySet()) {
			if (!newMap.containsKey(originalEntry.getKey())) {
				getPortalFileAssignmentDAO().delete(originalEntry.getValue());
				save = true;
			}
			else {
				// Remove from the new map so we know we don't have to save it since it already exists
				newMap.remove(originalEntry.getKey());
			}
		}
		for (Map.Entry<PortalEntity, PortalFileAssignment> newEntry : newMap.entrySet()) {
			getPortalFileAssignmentDAO().save(newEntry.getValue());
			save = true;
		}
		return save;
	}


	@Override
	public List<PortalFileAssignment> getPortalFileAssignmentListByFile(int portalFileId) {
		return getPortalFileAssignmentCache().getBeanListForKeyValue(getPortalFileAssignmentDAO(), portalFileId);
	}


	@Override
	public PortalFileAssignment savePortalFileAssignmentExclude(int portalFileAssignmentId, boolean exclude) {
		PortalFileAssignment fileAssignment = getPortalFileAssignmentDAO().findByPrimaryKey(portalFileAssignmentId);
		if (fileAssignment != null && fileAssignment.isExcluded() != exclude) {
			ValidationUtils.assertNotNull(fileAssignment.getReferenceOne().getPostPortalEntity(), "Cannot update excluded flag on system managed file assignments.");
			validatePortalFileAccess(fileAssignment.getReferenceOne(), getPortalSecurityUserService().getPortalSecurityUserCurrent(), false);
			fileAssignment.setExcluded(exclude);
			fileAssignment = getPortalFileAssignmentDAO().save(fileAssignment);
			// If changed, need to rebuild:
			getPortalFileCategoryPortalEntityMappingService().schedulePortalFileCategoryPortalEntityMappingListRebuild();
		}
		return fileAssignment;
	}


	@Override
	public void updatePortalFileManualAssignments(PortalFile portalFile, PortalEntitySourceSystem sourceSystem, Set<PortalEntitySourceSection> sourceSectionSet, List<PortalEntity> assignedPortalEntityList) {
		ValidationUtils.assertNotNull(portalFile, "Portal File is Required");
		ValidationUtils.assertNotNull(sourceSystem, "Portal Source System is Required");
		List<PortalFileAssignment> existingAssignmentList = (portalFile.isNewBean() ? null : getPortalFileAssignmentListByFile(portalFile.getId()));
		// Keep assignments for posts from another source system
		List<PortalFileAssignment> newAssignmentList = BeanUtils.filter(existingAssignmentList, portalFileAssignment -> (!sourceSystem.equals(portalFileAssignment.getReferenceTwo().getEntitySourceSection().getSourceSystem()) || (!CollectionUtils.isEmpty(sourceSectionSet) && !sourceSectionSet.contains(portalFileAssignment.getReferenceTwo().getEntitySourceSection()))));
		for (PortalEntity portalEntity : CollectionUtils.getIterable(assignedPortalEntityList)) {
			boolean found = false;
			for (PortalFileAssignment portalFileAssignment : CollectionUtils.getIterable(existingAssignmentList)) {
				if (portalFileAssignment.getReferenceTwo().equals(portalEntity)) {
					found = true;
					newAssignmentList.add(portalFileAssignment);
					break;
				}
			}
			if (!found) {
				PortalFileAssignment assignment = new PortalFileAssignment();
				assignment.setReferenceOne(portalFile);
				assignment.setReferenceTwo(portalEntity);
				newAssignmentList.add(assignment);
			}
		}
		savePortalFileManual(portalFile, newAssignmentList);
	}


	@Override
	public void schedulePortalFileAssignmentListForPostEntityRebuild(PortalEntity postEntity) {
		String runId = "ENTITY_" + postEntity.getId();
		final Date scheduledDate = DateUtils.addSeconds(new Date(), 10);

		Runner runner = new AbstractStatusAwareRunner("PORTAL_FILE_ASSIGNMENT_REBUILD", runId, scheduledDate) {

			@Override
			public void run() {
				rebuildPortalFileAssignmentListForPostEntity(postEntity.getId());
				getStatus().setMessage("Portal File Assignment Rebuild Completed for files that are posted to entity [" + postEntity.getEntityLabelWithSourceSection() + "].");
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	private void rebuildPortalFileAssignmentListForPostEntity(int portalEntityId) {
		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setPostPortalEntityId(portalEntityId);

		List<PortalFile> portalFileList = getPortalFileList(searchForm);
		boolean update = false;
		for (PortalFile portalFile : CollectionUtils.getIterable(portalFileList)) {
			if (generateAndSavePortalFileAssignmentListForPortalFile(portalFile, getPortalFileAssignmentListByFile(portalFile.getId()))) {
				update = true;
			}
		}

		if (update) {
			getPortalFileCategoryPortalEntityMappingService().schedulePortalFileCategoryPortalEntityMappingListRebuild();
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                   Portal File Security                     //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of users that have access to the file.
	 * Required parameter on the search form is the "userRolePortalEntitySpecific" property
	 * If userRolePortalEntitySpecific = false, then would be internal PPA employees that have access to upload, edit the file
	 * Otherwise, client users that have access to download the file
	 */
	@Override
	public List<PortalSecurityUser> getPortalFileSecurityUserList(int portalFileId, PortalSecurityUserSearchForm searchForm, Integer assignedPortalEntityId) {
		PortalFile portalFile = getPortalFile(portalFileId);
		ValidationUtils.assertNotNull(portalFile, "Missing Portal File with id: " + portalFileId);
		ValidationUtils.assertNotNull(searchForm.getUserRolePortalEntitySpecific(), "You must select a display type of External Users or Our Users.");
		PortalSecurityResource securityResource = portalFile.getFileCategory().getSecurityResource();

		// If portal entity specific, then need to check entity assignments
		if (BooleanUtils.isTrue(searchForm.getUserRolePortalEntitySpecific())) {
			List<PortalSecurityUserAssignmentExpanded> securityUserAssignmentExpandedList = new ArrayList<>();

			// Global Files apply to all - add filtering directly to user search - no entity filters and too many users to do an in clause explicitly
			if (portalFile.isGlobalFile() && assignedPortalEntityId == null) {
				searchForm.setAssignmentAccessForPortalSecurityResourceId(securityResource.getId());
				searchForm.setAssignmentPortalEntityViewTypeId(PortalUtils.getPortalEntityViewTypeId(portalFile));
			}
			else {
				// If specific posting
				if (portalFile.getPostPortalEntity() != null && portalFile.getPostPortalEntity().getEntitySourceSection().getEntityType().isSecurableEntity()) {
					securityUserAssignmentExpandedList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(securityResource.getId(), portalFile.getPostPortalEntity().getId());
				}
				else {
					if (assignedPortalEntityId != null) {
						// Limit to the requested assigned entity
						securityUserAssignmentExpandedList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(securityResource.getId(), assignedPortalEntityId);
					}
					else {
						// Need to Check PortalFileAssignment Table
						List<PortalFileAssignment> assignmentList = getPortalFileAssignmentListByFile(portalFile.getId());
						for (PortalFileAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
							securityUserAssignmentExpandedList.addAll(CollectionUtils.asNonNullList(getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(securityResource.getId(), assignment.getReferenceTwo().getId())));
						}
					}
				}
				if (CollectionUtils.isEmpty(securityUserAssignmentExpandedList)) {
					return null;
				}
				searchForm.setIds(BeanUtils.getPropertyValuesUniqueExcludeNull(securityUserAssignmentExpandedList, PortalSecurityUserAssignmentExpanded::getPortalSecurityUserId, Short.class));
			}
		}
		// Otherwise - internal users with write access, need to add admin users since they aren't explicit
		else {
			List<PortalSecurityUserAssignmentExpanded> securityUserAssignmentExpandedList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForSecurityResourceAndEntity(securityResource.getId(), null);
			searchForm.setIdsOrAdmin(BeanUtils.getPropertyValuesUniqueExcludeNull(securityUserAssignmentExpandedList, PortalSecurityUserAssignmentExpanded::getPortalSecurityUserId, Short.class));
		}

		return getPortalSecurityUserService().getPortalSecurityUserList(searchForm, false);
	}


	private void validatePortalFileAccess(PortalFile portalFile, PortalSecurityUser portalSecurityUser, boolean readOnly) {
		if (!isPortalFileAccessAllowed(portalFile, portalSecurityUser, readOnly)) {
			if (readOnly) {
				throw new AccessDeniedException("You do not have access to selected file [" + portalFile.getId() + "].");
			}
			else {
				// This would be internal only as client users never have write access to our files
				throw new AccessDeniedException("You do not have write access to security resource [" + portalFile.getFileCategory().getSecurityResource().getName() + "].");
			}
		}
	}


	private boolean isPortalFileAccessAllowed(PortalFile portalFile, PortalSecurityUser portalSecurityUser, boolean readOnly) {
		if (portalSecurityUser == null) {
			portalSecurityUser = getPortalSecurityUserService().getPortalSecurityUserCurrent();
		}
		if (portalSecurityUser == null) {
			throw new ValidationException("No current user available to confirm portal file permissions for.");
		}

		// Admins have full unrestricted access
		if (portalSecurityUser.getUserRole().isAdministrator()) {
			return true;
		}

		PortalSecurityResource securityResource = portalFile.getFileCategory().getSecurityResource();

		// Read Only: Files are ONLY locked down for users that are under roles that are portal entity specific
		if (readOnly) {
			if (!portalSecurityUser.getUserRole().isPortalEntitySpecific()) {
				return true;
			}

			List<PortalSecurityUserAssignmentExpanded> expandedList = getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForUser(portalSecurityUser.getId());
			expandedList = BeanUtils.filter(expandedList, PortalSecurityUserAssignmentExpanded::getPortalSecurityResourceId, securityResource.getId());
			// If it's a Global File, then they need access to at least one entity
			if (portalFile.isGlobalFile()) {
				if (portalFile.getEntityViewType() != null) {
					// Go through the list and find the first one - more efficient than evaluating all
					for (PortalSecurityUserAssignmentExpanded expanded : CollectionUtils.getIterable(expandedList)) {
						PortalEntity portalEntity = getPortalEntityService().getPortalEntity(expanded.getPortalEntityId());
						if (portalFile.getEntityViewType().equals(portalEntity.getEntityViewType())) {
							return true;
						}
					}
					return false;
				}
				return !CollectionUtils.isEmpty(expandedList);
			}
			else if (portalFile.getPostPortalEntity() != null && portalFile.getPostPortalEntity().getEntitySourceSection().getEntityType().isSecurableEntity()) {
				return !CollectionUtils.isEmpty(BeanUtils.filter(expandedList, PortalSecurityUserAssignmentExpanded::getPortalEntityId, portalFile.getPostPortalEntity().getId()));
			}
			// Need to Check PortalFileAssignment Table
			List<PortalFileAssignment> assignmentList = getPortalFileAssignmentListByFile(portalFile.getId());
			for (PortalFileAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
				if (!CollectionUtils.isEmpty(BeanUtils.filter(expandedList, PortalSecurityUserAssignmentExpanded::getPortalEntityId, assignment.getReferenceTwo().getId()))) {
					return true;
				}
			}
			return false;
		}
		// Otherwise Posting or editing Files - available ONLY for not portal entity specific and check permissions (if not admin)
		else {
			if (portalSecurityUser.getUserRole().isPortalEntitySpecific()) {
				return false;
			}
			return !CollectionUtils.isEmpty(BeanUtils.filter(getPortalSecurityUserAssignmentExpandedHandler().getPortalSecurityUserAssignmentExpandedListForUser(portalSecurityUser.getId()), PortalSecurityUserAssignmentExpanded::getPortalSecurityResourceId, securityResource.getId()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getRootDirectory() {
		return this.rootDirectory;
	}


	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}


	public AdvancedUpdatableDAO<PortalFile, Criteria> getPortalFileDAO() {
		return this.portalFileDAO;
	}


	public void setPortalFileDAO(AdvancedUpdatableDAO<PortalFile, Criteria> portalFileDAO) {
		this.portalFileDAO = portalFileDAO;
	}


	public AdvancedReadOnlyDAO<PortalFileExtended, Criteria> getPortalFileExtendedDAO() {
		return this.portalFileExtendedDAO;
	}


	public void setPortalFileExtendedDAO(AdvancedReadOnlyDAO<PortalFileExtended, Criteria> portalFileExtendedDAO) {
		this.portalFileExtendedDAO = portalFileExtendedDAO;
	}


	public AdvancedUpdatableDAO<PortalFileAssignment, Criteria> getPortalFileAssignmentDAO() {
		return this.portalFileAssignmentDAO;
	}


	public void setPortalFileAssignmentDAO(AdvancedUpdatableDAO<PortalFileAssignment, Criteria> portalFileAssignmentDAO) {
		this.portalFileAssignmentDAO = portalFileAssignmentDAO;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalFileTemplateGenerator getPortalFileTemplateGenerator() {
		return this.portalFileTemplateGenerator;
	}


	public void setPortalFileTemplateGenerator(PortalFileTemplateGenerator portalFileTemplateGenerator) {
		this.portalFileTemplateGenerator = portalFileTemplateGenerator;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public DaoSingleKeyListCache<PortalFileAssignment, Integer> getPortalFileAssignmentCache() {
		return this.portalFileAssignmentCache;
	}


	public void setPortalFileAssignmentCache(DaoSingleKeyListCache<PortalFileAssignment, Integer> portalFileAssignmentCache) {
		this.portalFileAssignmentCache = portalFileAssignmentCache;
	}


	public PortalSecurityUserAssignmentExpandedHandler getPortalSecurityUserAssignmentExpandedHandler() {
		return this.portalSecurityUserAssignmentExpandedHandler;
	}


	public void setPortalSecurityUserAssignmentExpandedHandler(PortalSecurityUserAssignmentExpandedHandler portalSecurityUserAssignmentExpandedHandler) {
		this.portalSecurityUserAssignmentExpandedHandler = portalSecurityUserAssignmentExpandedHandler;
	}


	public DaoThreeKeyListCache<PortalFile, Short, String, Integer> getPortalFileListBySourceCache() {
		return this.portalFileListBySourceCache;
	}


	public void setPortalFileListBySourceCache(DaoThreeKeyListCache<PortalFile, Short, String, Integer> portalFileListBySourceCache) {
		this.portalFileListBySourceCache = portalFileListBySourceCache;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public PortalEmailService getPortalEmailService() {
		return this.portalEmailService;
	}


	public void setPortalEmailService(PortalEmailService portalEmailService) {
		this.portalEmailService = portalEmailService;
	}


	public PortalFileCategoryPortalEntityMappingService getPortalFileCategoryPortalEntityMappingService() {
		return this.portalFileCategoryPortalEntityMappingService;
	}


	public void setPortalFileCategoryPortalEntityMappingService(PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService) {
		this.portalFileCategoryPortalEntityMappingService = portalFileCategoryPortalEntityMappingService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
