package com.clifton.portal.file.setup.search;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntityType;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMapping;
import com.clifton.portal.file.setup.security.PortalFileCategorySecurityResourceExpanded;
import com.clifton.portal.security.search.BasePortalEntitySpecificAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.List;


/**
 * @author manderson
 */
public class PortalFileCategorySearchFormConfigurer extends BasePortalEntitySpecificAwareSearchFormConfigurer {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategorySearchFormConfigurer(BaseEntitySearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PortalFileCategorySearchForm categorySearchForm = (PortalFileCategorySearchForm) getPortalEntitySpecificAwareSearchForm();
		if (categorySearchForm.getSecurityResourceId() != null || !ArrayUtils.isEmpty(categorySearchForm.getSecurityResourceIds())) {
			DetachedCriteria catSub = DetachedCriteria.forClass(PortalFileCategorySecurityResourceExpanded.class, "cr");
			catSub.setProjection(Projections.property("id"));
			catSub.add(Restrictions.eqProperty("portalFileCategoryId", criteria.getAlias() + ".id"));
			if (categorySearchForm.getSecurityResourceId() != null) {
				catSub.add(Restrictions.eq("portalSecurityResourceId", categorySearchForm.getSecurityResourceId()));
			}
			else {
				catSub.add(Restrictions.in("portalSecurityResourceId", categorySearchForm.getSecurityResourceIds()));
			}
			criteria.add(Subqueries.exists(catSub));
		}


		if (BooleanUtils.isTrue(categorySearchForm.getLimitToCategoriesWithAvailableApprovedFiles()) || (BooleanUtils.isTrue(categorySearchForm.getLimitToCategoriesWithAvailableFiles()))) {
			if (categorySearchForm.getLimitToCategoriesForAssignedPortalEntityId() != null) {
				List<PortalEntity> entityList = getPortalEntityService().getRelatedPortalEntityList(categorySearchForm.getLimitToCategoriesForAssignedPortalEntityId(), true, !BooleanUtils.isTrue(categorySearchForm.getExcludeParentsOfAssignedPortalEntityId()), false, true);
				// If there is more than 1, then remove the OCIO.  We don't post anything at that level and will return categories for relationships under the OCIO that don't
				// apply directly to that relationship.  SEE: Jira PORTAL-67.  This is a bigger issue, but OCIO fix is most important since it affect goes across relationships
				if (entityList.size() > 1) {
					entityList = BeanUtils.filter(entityList, portalEntity -> !StringUtils.isEqual(portalEntity.getEntitySourceSection().getEntityType().getName(), PortalEntityType.ENTITY_TYPE_ORGANIZATION_GROUP));
				}
				configureCriteriaForViewAsEntityImpl(criteria, entityList, BooleanUtils.isTrue(categorySearchForm.getLimitToCategoriesWithAvailableApprovedFiles()));
			}
			else {
				DetachedCriteria sub = DetachedCriteria.forClass(PortalFileCategoryPortalEntityMapping.class, "fcm");
				sub.setProjection(Projections.property("id"));
				sub.add(Restrictions.eqProperty("portalFileCategoryId", criteria.getAlias() + ".id"));
				if (BooleanUtils.isTrue(categorySearchForm.getLimitToCategoriesWithAvailableApprovedFiles())) {
					sub.add(Restrictions.eq("approved", true));
				}
				criteria.add(Subqueries.exists(sub));
			}
		}
	}


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId) {
		PortalFileCategorySearchForm categorySearchForm = (PortalFileCategorySearchForm) getPortalEntitySpecificAwareSearchForm();

		DetachedCriteria catSub = DetachedCriteria.forClass(PortalFileCategorySecurityResourceExpanded.class, "cr");
		catSub.setProjection(Projections.property("id"));
		catSub.add(Restrictions.eqProperty("portalFileCategoryId", criteria.getAlias() + ".id"));

		DetachedCriteria userSub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "se");
		userSub.setProjection(Projections.property("id"));
		userSub.add(Restrictions.eq("portalSecurityUserId", portalSecurityUserId));
		if (categorySearchForm.getLimitToCategoriesForAssignedPortalEntityId() != null) {
			userSub.add(Restrictions.eq("portalEntityId", categorySearchForm.getLimitToCategoriesForAssignedPortalEntityId()));
		}
		// If no specific view as entity, then still need to limit to the entities the user has access to
		else if (categorySearchForm.getViewAsEntityId() == null) {
			// Get this entity id, all parents, and all children - active only
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setViewAsUserId(portalSecurityUserId);
			searchForm.setSecurableEntity(true);
			searchForm.setActive(true);
			List<PortalEntity> entityList = getPortalEntityService().getPortalEntityList(searchForm);
			// If there is more than 1, then remove the OCIO.  We don't post anything at that level and will return categories for relationships under the OCIO that don't
			// apply directly to that relationship.  SEE: Jira PORTAL-67.  This is a bigger issue, but OCIO fix is most important since it affect goes across relationships
			if (entityList.size() > 1) {
				entityList = BeanUtils.filter(entityList, portalEntity -> !StringUtils.isEqual(portalEntity.getEntitySourceSection().getEntityType().getName(), PortalEntityType.ENTITY_TYPE_ORGANIZATION_GROUP));
			}
			configureCriteriaForViewAsEntityImpl(criteria, entityList, true);
		}
		userSub.add(Restrictions.eqProperty("portalSecurityResourceId", "cr.portalSecurityResourceId"));
		catSub.add(Subqueries.exists(userSub));
		criteria.add(Subqueries.exists(catSub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		configureCriteriaForViewAsEntityImpl(criteria, viewAsEntityList, true);
	}


	private void configureCriteriaForViewAsEntityImpl(Criteria criteria, List<PortalEntity> viewAsEntityList, boolean approvedOnly) {
		DetachedCriteria sub = DetachedCriteria.forClass(PortalFileCategoryPortalEntityMapping.class, "fcm");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eqProperty("portalFileCategoryId", criteria.getAlias() + ".id"));
		if (approvedOnly) {
			sub.add(Restrictions.eq("approved", true));
		}
		LogicalExpression trueGlobalFileRestriction = Restrictions.and(Restrictions.eq("global", true), Restrictions.isNull("portalEntityViewTypeId"));
		if (CollectionUtils.isEmpty(viewAsEntityList)) {
			// No Entities, then only TRUE global files (no view type)
			sub.add(trueGlobalFileRestriction);
		}
		else {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(trueGlobalFileRestriction);
			// Entity Specific File Restriction
			disjunction.add(Restrictions.in("portalEntityId", BeanUtils.getBeanIdentityArray(viewAsEntityList, Integer.class)));
			// Global Files limited by view type
			Short[] entityViewTypes = PortalUtils.getPortalEntityViewTypeIds(viewAsEntityList);
			if (!ArrayUtils.isEmpty(entityViewTypes)) {
				disjunction.add(Restrictions.and(
						Restrictions.eq("global", true),
						(entityViewTypes.length == 1 ? Restrictions.eq("portalEntityViewTypeId", entityViewTypes[0]) : Restrictions.in("portalEntityViewTypeId", entityViewTypes))));
			}
			sub.add(disjunction);
		}
		criteria.add(Subqueries.exists(sub));
	}
}

