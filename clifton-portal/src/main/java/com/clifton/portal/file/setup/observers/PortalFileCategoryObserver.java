package com.clifton.portal.file.setup.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.security.PortalFileCategorySecurityResourceExpandedRebuildService;
import com.clifton.portal.notification.subscription.PortalNotificationSubscriptionService;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileCategoryObserver extends SelfRegisteringDaoObserver<PortalFileCategory> {


	private PortalFileCategorySecurityResourceExpandedRebuildService portalFileCategorySecurityResourceExpandedRebuildService;

	private PortalNotificationSubscriptionService portalNotificationSubscriptionService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortalFileCategory> dao, DaoEventTypes event, PortalFileCategory bean) {
		if (event.isDelete()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "You cannot delete a system defined file category.");
		}
		else {
			if (event.isInsert()) {
				ValidationUtils.assertFalse(bean.isSystemDefined(), "You cannot add a system defined file category.");
			}
			if (event.isUpdate()) {
				PortalFileCategory originalBean = getOriginalBean(dao, bean);
				if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
					throw new FieldValidationException("You cannot change system defined flag of an existing category.", "systemDefined");
				}
				if (originalBean.isSystemDefined()) {
					ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "You cannot change the name of a system defined category.", "name");
				}
			}
			if (bean.getNotificationDefinition() != null) {
				if (bean.getNotificationDefinition().getDefinitionType().getSecurityResource() != null) {
					ValidationUtils.assertTrue(bean.getNotificationDefinition().getDefinitionType().getSecurityResource().equals(bean.getSecurityResource()), "Portal Notification Definition [" + bean.getNotificationDefinition().getName() + "] is not valid because it's associated security resource [" + bean.getNotificationDefinition().getDefinitionType().getSecurityResource().getName() + "].");
				}
				ValidationUtils.assertTrue(bean.getNotificationDefinition().getDefinitionType().isFileNotification(), "Portal Notification Definition [" + bean.getNotificationDefinition().getName() + "] is not valid because it's not a file notification.");
				if (bean.getNotificationDefinition().getDefinitionType().isSubscriptionAllowed() && bean.getNotificationDefinition().getDefinitionType().isSubscriptionForFileCategory()) {
					ValidationUtils.assertNotNull(bean.getNotificationSubscriptionOption(), "Subscription Default Option is required when using notification definition [" + bean.getNotificationDefinition().getLabel() + "].");
				}
				else {
					bean.setNotificationSubscriptionOption(null);
				}
			}
			else {
				bean.setNotificationSubscriptionOption(null);
			}
			if (bean.getFileCategoryType().isSecurityResourceRequired()) {
				ValidationUtils.assertTrue(bean.getSecurityResource() != null, "Security resource is required for categories under type " + bean.getFileCategoryType().name());
			}
			else {
				ValidationUtils.assertTrue(bean.getSecurityResource() == null, "Security resource is not allowed for categories under type " + bean.getFileCategoryType().name());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortalFileCategory> dao, DaoEventTypes event, PortalFileCategory bean, Throwable e) {
		if (e == null) {
			// On changes to File Categories, which are rare, need to rebuild the {@link PortalFileCategorySecurityResourceExpanded}
			// Unless it's an update and security hasn't been changed
			boolean securityRebuild = true;
			if (event.isUpdate()) {
				PortalFileCategory originalBean = getOriginalBean(dao, bean);
				if (CompareUtils.isEqual(bean.getSecurityResource(), originalBean.getSecurityResource())) {
					securityRebuild = false;
				}
				// If subscriptions were updated to be required, set enabled for all existing subscriptions
				if (bean.getNotificationSubscriptionOption() != null && bean.getNotificationSubscriptionOption().isRequired() && !CompareUtils.isEqual(bean.getNotificationSubscriptionOption(), originalBean.getNotificationSubscriptionOption())) {
					getPortalNotificationSubscriptionService().requirePortalNotificationSubscriptionListForFileCategory(bean.getId());
				}
			}
			if (securityRebuild) {
				getPortalFileCategorySecurityResourceExpandedRebuildService().rebuildPortalFileCategorySecurityResourceExpanded();
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategorySecurityResourceExpandedRebuildService getPortalFileCategorySecurityResourceExpandedRebuildService() {
		return this.portalFileCategorySecurityResourceExpandedRebuildService;
	}


	public void setPortalFileCategorySecurityResourceExpandedRebuildService(PortalFileCategorySecurityResourceExpandedRebuildService portalFileCategorySecurityResourceExpandedRebuildService) {
		this.portalFileCategorySecurityResourceExpandedRebuildService = portalFileCategorySecurityResourceExpandedRebuildService;
	}


	public PortalNotificationSubscriptionService getPortalNotificationSubscriptionService() {
		return this.portalNotificationSubscriptionService;
	}


	public void setPortalNotificationSubscriptionService(PortalNotificationSubscriptionService portalNotificationSubscriptionService) {
		this.portalNotificationSubscriptionService = portalNotificationSubscriptionService;
	}
}
