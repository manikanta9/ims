package com.clifton.portal.file.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMappingService;
import com.clifton.portal.notification.PortalNotificationService;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalFileObserver</code> on inserts, updates (if changing portal file category or approved), and deletes will rebuild the PortalFileCategoryPortalEntityMapping table - schedules it for 30 seconds in the future so multiple changes can all be rebuilt in one shot
 * <p>
 * Note: We could make the rebuild smarter, but the logic can get complicated because of portal files and portal file assignments and since the rebuild takes less than half a second
 * running it 30 seconds in the future will merge multiple changes into one rebuild it shouldn't be an issue.  That will also allow picking up the file assignments which are saved just after the file is saved
 *
 * @author manderson
 */
@Component
public class PortalFileObserver extends SelfRegisteringDaoObserver<PortalFile> {


	private PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService;

	private PortalNotificationService portalNotificationService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortalFile> dao, DaoEventTypes event, PortalFile bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
		if (event.isDelete()) {
			// Remove File References from Portal Notification Portal File table
			getPortalNotificationService().deletePortalNotificationPortalFileListForFile(bean.getId());
		}
		else {
			if (!bean.isGlobalFile()) {
				ValidationUtils.assertNull(bean.getEntityViewType(), "Portal Entity View type selection is only allowed for Global Files.");
			}
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortalFile> dao, DaoEventTypes event, PortalFile bean, Throwable e) {
		if (e == null) {
			boolean rebuild = event.isInsert() || event.isDelete();
			if (event.isUpdate()) {
				PortalFile originalBean = getOriginalBean(dao, bean);
				// If we are moving a file, need to rebuild (this is rare) but could result in no results in the category we are moving from ( like a delete)
				// Or if Approved Flag is Changed Need to Rebuild
				if (!CompareUtils.isEqual(originalBean.getFileCategory(), bean.getFileCategory()) || !CompareUtils.isEqual(originalBean.isApproved(), bean.isApproved())) {
					rebuild = true;
				}
			}

			if (rebuild) {
				getPortalFileCategoryPortalEntityMappingService().schedulePortalFileCategoryPortalEntityMappingListRebuild();
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileCategoryPortalEntityMappingService getPortalFileCategoryPortalEntityMappingService() {
		return this.portalFileCategoryPortalEntityMappingService;
	}


	public void setPortalFileCategoryPortalEntityMappingService(PortalFileCategoryPortalEntityMappingService portalFileCategoryPortalEntityMappingService) {
		this.portalFileCategoryPortalEntityMappingService = portalFileCategoryPortalEntityMappingService;
	}


	public PortalNotificationService getPortalNotificationService() {
		return this.portalNotificationService;
	}


	public void setPortalNotificationService(PortalNotificationService portalNotificationService) {
		this.portalNotificationService = portalNotificationService;
	}
}
