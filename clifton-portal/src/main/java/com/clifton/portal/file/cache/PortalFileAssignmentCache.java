package com.clifton.portal.file.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portal.file.PortalFileAssignment;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalFileAssignmentCache</code> caches the list of PortalFileAssignment rows for a PortalFile
 *
 * @author manderson
 */
@Component
public class PortalFileAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<PortalFileAssignment, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortalFileAssignment bean) {
		if (bean.getReferenceOne() == null) {
			return null;
		}
		return bean.getReferenceOne().getId();
	}
}
