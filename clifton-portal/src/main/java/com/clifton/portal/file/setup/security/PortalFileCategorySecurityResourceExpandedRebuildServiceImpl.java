package com.clifton.portal.file.setup.security;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class PortalFileCategorySecurityResourceExpandedRebuildServiceImpl implements PortalFileCategorySecurityResourceExpandedRebuildService {

	private UpdatableDAO<PortalFileCategorySecurityResourceExpanded> portalFileCategorySecurityResourceExpandedDAO;

	private PortalFileSetupService portalFileSetupService;


	////////////////////////////////////////////////////////////////////////////////
	///////     Portal Security User Assignment Expanded Rebuild Methods     ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebuildPortalFileCategorySecurityResourceExpanded() {
		// Get a list of all categories that are file group level
		PortalFileCategorySearchForm searchForm = new PortalFileCategorySearchForm();
		searchForm.setFileCategoryType(PortalFileCategoryTypes.FILE_GROUP);

		List<PortalFileCategory> categoryList = getPortalFileSetupService().getPortalFileCategoryList(searchForm);
		List<PortalFileCategorySecurityResourceExpanded> expandedList = new ArrayList<>();
		for (PortalFileCategory category : CollectionUtils.getIterable(categoryList)) {
			addPortalFileCategoryToList(category, category.getSecurityResource().getId(), expandedList);
		}
		savePortalFileCategorySecurityResourceExpandedList(expandedList);
		LogUtils.info(getClass(), "rebuildPortalFileCategorySecurityResourceExpanded completed");
	}


	private void savePortalFileCategorySecurityResourceExpandedList(List<PortalFileCategorySecurityResourceExpanded> newList) {
		List<PortalFileCategorySecurityResourceExpanded> existingList = getPortalFileCategorySecurityResourceExpandedDAO().findAll();
		// IF BOTH lists aren't empty, then need to go through them and properly set ids, etc for updates.
		if (!CollectionUtils.isEmpty(newList) && !CollectionUtils.isEmpty(existingList)) {
			for (PortalFileCategorySecurityResourceExpanded newBean : CollectionUtils.getIterable(newList)) {
				for (PortalFileCategorySecurityResourceExpanded existingBean : CollectionUtils.getIterable(existingList)) {
					// Call special overridden equals method to see if both entities are really equal
					if (newBean.equals(existingBean)) {
						// Set ID on the the new list, so updates
						newBean.setId(existingBean.getId());
					}
				}
			}
		}
		getPortalFileCategorySecurityResourceExpandedDAO().saveList(newList, existingList);
	}


	/**
	 * Adds if not in the list and also traverses the add up the category tree.  If already added, then won't continue up the tree since if that specific entry exists, then it's parents also exist
	 */
	private void addPortalFileCategoryToList(PortalFileCategory category, short securityResourceId, List<PortalFileCategorySecurityResourceExpanded> expandedList) {
		PortalFileCategorySecurityResourceExpanded categoryExpanded = new PortalFileCategorySecurityResourceExpanded(category, securityResourceId);
		if (!expandedList.contains(categoryExpanded)) {
			expandedList.add(categoryExpanded);

			if (category.getParent() != null) {
				addPortalFileCategoryToList(category.getParent(), securityResourceId, expandedList);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<PortalFileCategorySecurityResourceExpanded> getPortalFileCategorySecurityResourceExpandedDAO() {
		return this.portalFileCategorySecurityResourceExpandedDAO;
	}


	public void setPortalFileCategorySecurityResourceExpandedDAO(UpdatableDAO<PortalFileCategorySecurityResourceExpanded> portalFileCategorySecurityResourceExpandedDAO) {
		this.portalFileCategorySecurityResourceExpandedDAO = portalFileCategorySecurityResourceExpandedDAO;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
