package com.clifton.portal.file.setup.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortalFileCategoryListByUploadAccessCache</code> caches the list of FILE_GROUP categories for a user
 * that they have access to uploading to.
 * <p>
 * The cache clears for a user on changes to their {@link com.clifton.portal.security.user.PortalSecurityUserAssignmentResource}
 * And clears all on changes to the file categories (rare)
 *
 * @author manderson
 */
@Component
public class PortalFileCategoryListByUploadAccessCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<Short, Short[]>, PortalFileCategoryListByUploadAccessCache {

	private CacheHandler<Short, Short[]> cacheHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalFileCategory> getPortalFileCategoryUploadListForUser(short userId, ReadOnlyDAO<PortalFileCategory> dao) {
		Short[] categoryIds = getCacheHandler().get(getCacheName(), userId);
		if (categoryIds != null) {
			return dao.findByPrimaryKeys(categoryIds);
		}
		return null;
	}


	@Override
	public void setPortalFileCategoryUploadListForUser(short userId, List<PortalFileCategory> categoryList) {
		getCacheHandler().put(getCacheName(), userId, BeanUtils.getBeanIdentityArray(categoryList, Short.class));
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                      Observer Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			if (bean instanceof PortalFileCategory) {
				// On changes to File Categories, which are rare, need to clear the cache
				// Unless it's an update and security hasn't been changed
				boolean clear = true;
				if (event.isUpdate()) {
					PortalFileCategory originalBean = (PortalFileCategory) getOriginalBean(dao, bean);
					if (CompareUtils.isEqual(((PortalFileCategory) bean).getSecurityResource(), originalBean.getSecurityResource())) {
						clear = false;
					}
				}
				if (clear) {
					getCacheHandler().clear(getCacheName());
				}
			}
			// Clear on any change to the user's permissions
			else if (bean instanceof PortalSecurityUserAssignmentResource) {
				// Only applies to "our" users - so those that have a security user, not an assignment on the resource record
				if (((PortalSecurityUserAssignmentResource) bean).getSecurityUser() != null) {
					getCacheHandler().remove(getCacheName(), ((PortalSecurityUserAssignmentResource) bean).getSecurityUser().getId());
				}
			}
			else {
				throw new IllegalStateException("PortalFileCategoryListByUploadAccessCacheImpl: No definition for clearing the cache on changes to Bean ["
						+ bean + "] of type " + dao.getConfiguration().getBeanClass().getName() + ".");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public CacheHandler<Short, Short[]> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Short, Short[]> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
