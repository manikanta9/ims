package com.clifton.portal.file.template;

import com.clifton.portal.file.PortalFile;


/**
 * @author manderson
 */
public interface PortalFileTemplateGenerator {


	/**
	 * Generates the PortalFile displayName and displayText properties based on the file category's templates
	 * For displayName - If no template, would use the display name specified, if that is blank will default to the category name
	 * For displayText - If no template, and no displayText supplied on the file, will be left blank
	 */
	public PortalFileTemplate generatePortalFileTemplate(PortalFile portalFile);


	/**
	 * Generates the downloaded file name for the Portal File.  This is called on demand as the file is downloaded and only when an individual file
	 * is downloaded.
	 * Will truncate the file name to max of 260 characters which is the limit that outlook supports for dragging and dropping attachments
	 */
	public String generatePortalFileName(PortalFile portalFile);
}
