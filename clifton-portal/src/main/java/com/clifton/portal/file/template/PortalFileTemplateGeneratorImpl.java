package com.clifton.portal.file.template;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.PortalFile;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileTemplateGeneratorImpl implements PortalFileTemplateGenerator {

	private PortalEntityService portalEntityService;

	private TemplateConverter templateConverter;

	private static final int MAX_FILE_NAME_LENGTH = 260;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFileTemplate generatePortalFileTemplate(PortalFile portalFile) {
		PortalEntity sourceEntity = getSourcePortalEntityForPortalFile(portalFile);

		PortalFileTemplate portalFileTemplate = new PortalFileTemplate();
		if (portalFile.getFileCategory() != null) {
			if (!StringUtils.isEmpty(portalFile.getFileCategory().getDisplayNameTemplate())) {
				portalFileTemplate.setDisplayName(generatePortalFileTemplateValue(portalFile, portalFile.getFileCategory().getDisplayNameTemplate(), sourceEntity));
			}
			if (!StringUtils.isEmpty(portalFile.getFileCategory().getDisplayTextTemplate())) {
				portalFileTemplate.setDisplayText(generatePortalFileTemplateValue(portalFile, portalFile.getFileCategory().getDisplayTextTemplate(), sourceEntity));
			}
		}
		applyDefaultsFromPortalFile(portalFile, portalFileTemplate);
		return portalFileTemplate;
	}


	@Override
	public String generatePortalFileName(PortalFile portalFile) {
		String fileName = null;
		if (portalFile.getFileCategory() != null && !StringUtils.isEmpty(portalFile.getFileCategory().getFileNameTemplate())) {
			PortalEntity sourceEntity = getSourcePortalEntityForPortalFile(portalFile);
			fileName = generatePortalFileTemplateValue(portalFile, portalFile.getFileCategory().getFileNameTemplate(), sourceEntity);
		}
		if (StringUtils.isEmpty(fileName)) {
			fileName = portalFile.getDisplayName();
		}
		fileName = FileUtils.replaceInvalidCharacters(fileName, "");
		if (fileName != null && fileName.length() > MAX_FILE_NAME_LENGTH) {
			fileName = fileName.substring(0, MAX_FILE_NAME_LENGTH - 1);
		}
		return fileName;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String generatePortalFileTemplateValue(PortalFile portalFile, String template, PortalEntity sourceEntity) {
		if (!StringUtils.isEmpty(template)) {
			TemplateConfig templateConfig = new TemplateConfig(template);
			templateConfig.addBeanToContext("file", portalFile);
			if (sourceEntity != null) {
				templateConfig.addBeanToContext("sourceEntity", sourceEntity);
				for (PortalEntityFieldValue fieldValue : CollectionUtils.getIterable(sourceEntity.getFieldValueList())) {
					templateConfig.addBeanToContext(fieldValue.getPortalEntityFieldType().getName().replaceAll(" ", "_"), fieldValue);
				}
			}
			return getTemplateConverter().convert(templateConfig);
		}
		return null;
	}


	private void applyDefaultsFromPortalFile(PortalFile portalFile, PortalFileTemplate portalFileTemplate) {
		// If Display Name Is Missing - Use the Existing Display Name, else Category Name
		if (StringUtils.isEmpty(portalFileTemplate.getDisplayName())) {
			portalFileTemplate.setDisplayName(StringUtils.coalesce(false, portalFile.getDisplayName(), portalFile.getFileCategory().getName()));
		}
		// If Display Text is Missing - Use the Existing Display Text Only
		if (StringUtils.isEmpty(portalFileTemplate.getDisplayText())) {
			portalFileTemplate.setDisplayText(portalFile.getDisplayText());
		}
	}


	private PortalEntity getSourcePortalEntityForPortalFile(PortalFile portalFile) {
		if (portalFile.getFileCategory().getSourceEntityType() != null && portalFile.getEntitySourceSection() != null && portalFile.getSourceFkFieldId() != null) {
			return getPortalEntityService().getPortalEntityBySourceSection(portalFile.getEntitySourceSection().getId(), portalFile.getSourceFkFieldId(), true);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}
}
