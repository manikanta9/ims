package com.clifton.portal.file.observers;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.file.PortalFileService;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalFileAssignmentPortalEntityRollupObserver</code> observes changes to PortalEntityRollup tables.
 * <p>
 * On all (inserts/updates/deletes) will check all files posted to the parent entity and insert or delete file assignments as they should apply.
 * <p>
 * Examples:  Files posted to the Commingled Vehicles apply to all of the accounts.  Even if an account is added to the Fund now, they have access to FINAL historical documents and some reports based on the inception / termination dates.
 * Files posted to a client group - if a client is removed from the group those files should no longer apply to them.  If a Client is Terminated (end date is set) then new documents won't be applied to them.
 * <p>
 * The rebuild of file assignments is scheduled with a 10 second delay so that if multiple rollups are updated the file will rebuild its assignments only once
 */
@Component
public class PortalFileAssignmentPortalEntityRollupObserver extends SelfRegisteringDaoObserver<PortalEntityRollup> {

	private PortalFileService portalFileService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<PortalEntityRollup> dao, DaoEventTypes event, PortalEntityRollup bean, Throwable e) {
		if (e == null) {
			getPortalFileService().schedulePortalFileAssignmentListForPostEntityRebuild(bean.getReferenceOne());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}
}
