package com.clifton.portal.file.setup.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.portal.file.setup.PortalFileCategory;


/**
 * The <code>PortalFileCategoryChildrenListCache</code> caches list of child portal file category ids for a given file category at the level of "File Group" which is the level where files are assigned.
 *
 * @author manderson
 */
public interface PortalFileCategoryChildrenListCache {


	public Short[] getPortalFileCategoryChildrenIdList(short portalFileCategoryId, ReadOnlyDAO<PortalFileCategory> portalFileCategoryDAO);
}
