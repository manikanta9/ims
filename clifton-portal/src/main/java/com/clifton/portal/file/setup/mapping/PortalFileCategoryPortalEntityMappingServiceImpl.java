package com.clifton.portal.file.setup.mapping;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildEvent;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalFileCategoryPortalEntityMappingServiceImpl</code> rebuilds the PortalFileCategoryPortalEntityMapping table
 *
 * @author manderson
 */
@Service
public class PortalFileCategoryPortalEntityMappingServiceImpl implements PortalFileCategoryPortalEntityMappingService {

	private UpdatableDAO<PortalFileCategoryPortalEntityMapping> portalFileCategoryPortalEntityMappingDAO;

	private PortalEntityService portalEntityService;

	private EventHandler eventHandler;

	private RunnerHandler runnerHandler;

	private SqlHandler sqlHandler;

	// Used as the Entity ID value of a Global file when rebuilding so we know a global file was updated and all user's security needs to be rebuilt.
	private static final Integer GLOBAL_FILE_INDICATOR = 0;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void schedulePortalFileCategoryPortalEntityMappingListRebuild() {
		// asynchronous support
		final Date scheduledDate = DateUtils.addSeconds(new Date(), 10);

		Runner runner = new AbstractStatusAwareRunner("PORTAL_FILE_CATEGORY_PORTAL_ENTITY_MAPPING_REBUILD", "ALL", scheduledDate) {

			@Override
			public void run() {
				// Get a List of all the entities affected (by change in approvals) - rebuilds security ONLY for those that changed
				Set<Integer> updatedEntityIdList = rebuildPortalFileCategoryPortalEntityMappingListImpl();
				boolean update = CollectionUtils.getSize(updatedEntityIdList) > 0;
				// If set contains an integer 0, then there was a global file that was affected
				boolean globalUpdate = update && updatedEntityIdList.contains(GLOBAL_FILE_INDICATOR);
				getStatus().setMessage("Portal File Category Portal Entity Mapping Rebuild completed. (" + (update ? "Changes applied that affect security." : "No Changes applied that affect security.") + ").");
				if (update) {
					// If there was any change - then there are new file groups or existing file groups that don't apply anymore
					// Need to Rebuild Security Expanded so those users properly see or don't see the category changes.
					// If Entity Count <= 1000 - Rebuild Security Expanded Table - but ONLY rebuild for the Entities that were affected
					if (!globalUpdate && updatedEntityIdList.size() < 1000) {
						// Else run for all (global file might trigger it, so just run for all)
						getEventHandler().raiseEvent(PortalSecurityUserAssignmentExpandedRebuildEvent.ofPortalEntityChanges(updatedEntityIdList.toArray(new Integer[0])));
					}
					else {
						// Else run for all (global file might trigger it, so just run for all)
						getEventHandler().raiseEvent(PortalSecurityUserAssignmentExpandedRebuildEvent.ofPortalSecurityUserChange(null));
					}
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	@Override
	public boolean rebuildPortalFileCategoryPortalEntityMappingList() {
		return !CollectionUtils.isEmpty(rebuildPortalFileCategoryPortalEntityMappingListImpl());
	}


	/**
	 * Returns set of portal entity ids affected that may require a security rebuild.
	 * NOTE: IF A GLOBAL FILE IS AFFECTED, THE SET WILL CONTAIN A 0 value (GLOBAL_FILE_INDICATOR) - this allows use to easily rebuild all users in this case
	 * Allows limited the number of users whose security needs to be rebuilt
	 */
	@Transactional
	protected Set<Integer> rebuildPortalFileCategoryPortalEntityMappingListImpl() {
		ResultSetExtractor<Collection<PortalFileCategoryPortalEntityMapping>> resultSetExtractor = rs -> {
			Map<String, PortalFileCategoryPortalEntityMapping> categoryEntityMap = new HashMap<>();
			while (rs.next()) {
				Integer portalEntityId = rs.getInt(1); // Note: Returns 0 if NULL
				Short portalEntityViewTypeId = rs.getShort(2); // Note: Returns 0 if NULL
				short portalFileCategoryId = rs.getShort(3);
				short parentPortalFileCategoryId = rs.getShort(4);
				short grandParentPortalFileCategoryId = rs.getShort(5);
				boolean approved = rs.getBoolean(6);
				Date firstApprovalDate = rs.getTimestamp(7);

				PortalEntity portalEntity = (portalEntityId == 0 ? null : getPortalEntityService().getPortalEntity(portalEntityId));
				updatePortalFileCategoryCount(portalEntity, portalEntityViewTypeId == 0 ? null : portalEntityViewTypeId, portalFileCategoryId, approved, categoryEntityMap, firstApprovalDate);
				updatePortalFileCategoryCount(portalEntity, portalEntityViewTypeId == 0 ? null : portalEntityViewTypeId, parentPortalFileCategoryId, approved, categoryEntityMap, firstApprovalDate);
				updatePortalFileCategoryCount(portalEntity, portalEntityViewTypeId == 0 ? null : portalEntityViewTypeId, grandParentPortalFileCategoryId, approved, categoryEntityMap, firstApprovalDate);
			}
			return categoryEntityMap.values();
		};
		Collection<PortalFileCategoryPortalEntityMapping> newList = getSqlHandler().executeSelect(getSqlSelectCommand(), resultSetExtractor);
		Map<String, PortalFileCategoryPortalEntityMapping> newMap = BeanUtils.getBeanMap(newList, this::getUniqueKey);

		// Get existing rows and merge results
		List<PortalFileCategoryPortalEntityMapping> existingList = getPortalFileCategoryPortalEntityMappingDAO().findAll();
		Map<String, PortalFileCategoryPortalEntityMapping> existingMap = BeanUtils.getBeanMap(existingList, this::getUniqueKey);

		// There won't be many changes, so create a list of saves and deletes which improves performance (about 10 seconds)
		List<PortalFileCategoryPortalEntityMapping> saveList = new ArrayList<>();
		List<PortalFileCategoryPortalEntityMapping> deleteList = new ArrayList<>();
		Set<Integer> portalEntityIds = new HashSet<>();
		for (Map.Entry<String, PortalFileCategoryPortalEntityMapping> newEntry : newMap.entrySet()) {
			PortalFileCategoryPortalEntityMapping newMapping = newEntry.getValue();
			PortalFileCategoryPortalEntityMapping existingMapping = existingMap.get(newEntry.getKey());
			if (existingMapping == null) {
				saveList.add(newMapping);
				if (newMapping.isApproved()) {
					portalEntityIds.add(newMapping.getPortalEntityId() == null ? GLOBAL_FILE_INDICATOR : newMapping.getPortalEntityId());
				}
			}
			else {
				// Approved OR First Approval Date would be the only field that could change
				if ((existingMapping.isApproved() != newMapping.isApproved()) || DateUtils.compare(existingMapping.getFirstApprovalDate(), newMapping.getFirstApprovalDate(), true) != 0) {
					newMapping.setId(existingMapping.getId());
					// Only considered a real change if approval flag changed
					if (existingMapping.isApproved() != newMapping.isApproved()) {
						portalEntityIds.add(newMapping.getPortalEntityId() == null ? GLOBAL_FILE_INDICATOR : newMapping.getPortalEntityId());
					}
					saveList.add(newMapping);
				}
				// Remove it from the existing map - those left over are considered deletes
				existingMap.remove(newEntry.getKey());
			}
		}

		if (!CollectionUtils.isEmpty(existingMap)) {
			for (PortalFileCategoryPortalEntityMapping existingMapping : existingMap.values()) {
				deleteList.add(existingMapping);
				if (existingMapping.isApproved()) {
					portalEntityIds.add(existingMapping.getPortalEntityId() == null ? GLOBAL_FILE_INDICATOR : existingMapping.getPortalEntityId());
				}
			}
		}
		getPortalFileCategoryPortalEntityMappingDAO().saveList(saveList);
		getPortalFileCategoryPortalEntityMappingDAO().deleteList(deleteList);
		LogUtils.info(getClass(), "rebuildPortalFileCategoryPortalEntityMappingList completed");
		return portalEntityIds;
	}


	private String getUniqueKey(PortalFileCategoryPortalEntityMapping mapping) {
		return getUniqueKey(mapping.getPortalEntityId(), mapping.isGlobal(), mapping.getPortalEntityViewTypeId(), mapping.getPortalFileCategoryId());
	}


	private String getUniqueKey(Integer portalEntityId, boolean global, Short portalEntityViewTypeId, short portalFileCategoryId) {
		if (global) {
			return "GLOBAL_" + (portalEntityViewTypeId == null ? "" : portalEntityViewTypeId + "_") + portalFileCategoryId;
		}
		if (portalEntityId == null) {
			return "ALL_" + portalFileCategoryId;
		}
		return "ENTITY_" + portalEntityId + "_" + portalFileCategoryId;
	}


	private void updatePortalFileCategoryCount(PortalEntity portalEntity, Short portalEntityViewTypeId, short portalFileCategoryId, boolean approved, Map<String, PortalFileCategoryPortalEntityMapping> categoryEntityMap, Date firstApprovalDate) {
		String key = getUniqueKey(portalEntity == null ? null : portalEntity.getId(), portalEntity == null, portalEntityViewTypeId, portalFileCategoryId);
		PortalFileCategoryPortalEntityMapping mapping = categoryEntityMap.get(key);
		if (mapping != null) {
			if (approved && !mapping.isApproved()) {
				mapping.setApproved(true);
			}
			if (mapping.getFirstApprovalDate() == null || DateUtils.isDateAfter(mapping.getFirstApprovalDate(), firstApprovalDate)) {
				mapping.setFirstApprovalDate(firstApprovalDate);
			}
		}
		else {
			mapping = (portalEntity == null ? PortalFileCategoryPortalEntityMapping.ofGlobalMapping(portalEntityViewTypeId, portalFileCategoryId, approved, firstApprovalDate) : PortalFileCategoryPortalEntityMapping.ofEntityMapping(portalEntity.getId(), portalFileCategoryId, approved, firstApprovalDate));
			categoryEntityMap.put(key, mapping);
		}
		// ADD TO THE ALL ENTITY MAP
		String allKey = getUniqueKey(null, false, null, portalFileCategoryId);
		PortalFileCategoryPortalEntityMapping allCount = categoryEntityMap.get(allKey);
		if (allCount != null) {
			if (approved && !allCount.isApproved()) {
				allCount.setApproved(true);
			}
		}
		else {
			allCount = PortalFileCategoryPortalEntityMapping.ofAllMapping(portalFileCategoryId, approved, firstApprovalDate);
			categoryEntityMap.put(allKey, allCount);
		}

		// Add parent to map
		if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
			updatePortalFileCategoryCount(portalEntity.getParentPortalEntity(), portalEntityViewTypeId, portalFileCategoryId, approved, categoryEntityMap, firstApprovalDate);
		}
	}


	private SqlSelectCommand getSqlSelectCommand() {
		SqlSelectCommand sqlSelectCommand = new SqlSelectCommand();
		sqlSelectCommand.setSql(
				"SELECT x.PortalEntityID AS PortalEntityID, x.PortalEntityViewTypeID AS PortalEntityViewTypeID, x.PortalFileCategoryID AS PortalFileCategoryID, x.ParentPortalFileCategoryID AS ParentPortalFileCategoryID, x.GrandParentPortalFileCategoryID AS GrandParentPortalFileCategoryID, MAX(x.Approved) AS IsApproved, MIN(x.FirstApprovalDate) AS FirstApprovalDate" + //
						" FROM ( " + //
						//Global
						" SELECT NULL AS PortalEntityID, f.PortalEntityViewTypeID, fc.PortalFileCategoryID AS PortalFileCategoryID, pfc.PortalFileCategoryID AS ParentPortalFileCategoryID, pfc.ParentPortalFileCategoryID AS GrandParentPortalFileCategoryID, MAX(CASE WHEN f.IsApproved = 1 THEN 1 ELSE 0 END) AS Approved, MIN(f.ApprovalDate) AS FirstApprovalDate" + //
						" FROM PortalFile f " + //
						" INNER JOIN PortalFileCategory fc ON f.PortalFileCategoryID = fc.PortalFileCategoryID " + //
						" INNER JOIN PortalFileCategory pfc ON fc.ParentPortalFileCategoryID = pfc.PortalFileCategoryID " + //
						" WHERE f.IsGlobalFile = 1 " + //
						" GROUP BY f.PortalEntityViewTypeID, f.PortalFileCategoryID, fc.PortalFileCategoryID, pfc.PortalFileCategoryID, pfc.ParentPortalFileCategoryID " + //

						" UNION ALL " + //

						" SELECT e.PortalEntityID, NULL, fc.PortalFileCategoryID, pfc.PortalFileCategoryID \"ParentPortalFileCategoryID\", pfc.ParentPortalFileCategoryID \"GrandParentPortalFileCategoryID\", MAX(CASE WHEN f.IsApproved = 1 THEN 1 ELSE 0 END) \"Approved\", MIN(f.ApprovalDate) AS FirstApprovalDate " + //
						" FROM PortalFile f " + //
						" INNER JOIN PortalFileCategory fc ON f.PortalFileCategoryID = fc.PortalFileCategoryID " + //
						" INNER JOIN PortalFileCategory pfc ON fc.ParentPortalFileCategoryID = pfc.PortalFileCategoryID " + //
						" INNER JOIN PortalEntity e ON f.PostPortalEntityID = e.PortalEntityID " + //
						" INNER JOIN PortalEntitySourceSection pes ON e.PortalEntitySourceSectionID = pes.PortalEntitySourceSectionID " + //
						" INNER JOIN PortalEntityType pet ON pes.PortalEntityTypeID = pet.PortalEntityTypeID " + //
						" WHERE pet.IsSecurableEntity = 1 AND pes.ChildPortalEntityTypeID IS NULL " + //
						" AND (e.StartDate IS NULL OR e.StartDate <= ?) " + //
						" AND (e.EndDate IS NULL OR e.EndDate >= ?) " + //
						" GROUP BY e.PortalEntityID, fc.PortalFileCategoryID, pfc.PortalFileCategoryID, pfc.ParentPortalFileCategoryID " + //

						" UNION ALL " + //

						" SELECT e.PortalEntityID, NULL, fc.PortalFileCategoryID, pfc.PortalFileCategoryID \"ParentPortalFileCategoryID\", pfc.ParentPortalFileCategoryID \"GrandParentPortalFileCategoryID\", MAX(CASE WHEN f.IsApproved = 1 THEN 1 ELSE 0 END) \"Approved\", MIN(f.ApprovalDate) AS FirstApprovalDate " + //
						" FROM PortalFile f " + //
						" INNER JOIN PortalFileCategory fc ON f.PortalFileCategoryID = fc.PortalFileCategoryID " + //
						" INNER JOIN PortalFileCategory pfc ON fc.ParentPortalFileCategoryID = pfc.PortalFileCategoryID " + //
						" INNER JOIN PortalFileAssignment fa ON f.PortalFileID = fa.PortalFileID AND fa.IsExcluded = 0 " + //
						" INNER JOIN PortalEntity e ON fa.PortalEntityID = e.PortalEntityID " + //
						" INNER JOIN PortalEntitySourceSection pes ON e.PortalEntitySourceSectionID = pes.PortalEntitySourceSectionID " + //
						" INNER JOIN PortalEntityType pet ON pes.PortalEntityTypeID = pet.PortalEntityTypeID " + //
						" WHERE pet.IsSecurableEntity = 1 " + //
						" AND (e.StartDate IS NULL OR e.StartDate <= ?) " + //
						" AND (e.EndDate IS NULL OR e.EndDate >= ?) " + //
						" GROUP BY e.PortalEntityID, fc.PortalFileCategoryID, pfc.PortalFileCategoryID, pfc.ParentPortalFileCategoryID " + //
						" ) x " + //
						" GROUP BY x.PortalEntityID, x.PortalEntityViewTypeID, x.PortalFileCategoryID, x.ParentPortalFileCategoryID, x.GrandParentPortalFileCategoryID "
		);
		// We use "today" date param 4 times
		Date today = DateUtils.clearTime(new Date());
		sqlSelectCommand.addSqlParameterValue(SqlParameterValue.ofDate(today));
		sqlSelectCommand.addSqlParameterValue(SqlParameterValue.ofDate(today));
		sqlSelectCommand.addSqlParameterValue(SqlParameterValue.ofDate(today));
		sqlSelectCommand.addSqlParameterValue(SqlParameterValue.ofDate(today));
		return sqlSelectCommand;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<PortalFileCategoryPortalEntityMapping> getPortalFileCategoryPortalEntityMappingDAO() {
		return this.portalFileCategoryPortalEntityMappingDAO;
	}


	public void setPortalFileCategoryPortalEntityMappingDAO(UpdatableDAO<PortalFileCategoryPortalEntityMapping> portalFileCategoryPortalEntityMappingDAO) {
		this.portalFileCategoryPortalEntityMappingDAO = portalFileCategoryPortalEntityMappingDAO;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
