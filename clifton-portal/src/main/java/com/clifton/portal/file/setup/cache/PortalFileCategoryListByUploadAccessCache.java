package com.clifton.portal.file.setup.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.portal.file.setup.PortalFileCategory;

import java.util.List;


/**
 * The <code>PortalFileCategoryListByUploadAccessCache</code> caches the list of FILE_GROUP categories for a user
 * that they have access to uploading to.
 * <p>
 * The cache clears for a user on changes to their {@link com.clifton.portal.security.user.PortalSecurityUserAssignmentResource}
 * And clears all on changes to categories (rare)
 *
 * @author manderson
 */
public interface PortalFileCategoryListByUploadAccessCache {

	public List<PortalFileCategory> getPortalFileCategoryUploadListForUser(short userId, ReadOnlyDAO<PortalFileCategory> dao);


	public void setPortalFileCategoryUploadListForUser(short userId, List<PortalFileCategory> categoryList);
}
