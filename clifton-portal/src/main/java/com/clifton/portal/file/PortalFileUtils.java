package com.clifton.portal.file;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.file.upload.PortalFileUploadPending;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>PortalFileUtils</code> class is a utility method for working with Portal Files - generating file names, locations, etc.
 *
 * @author manderson
 */
public class PortalFileUtils {

	private static final String QUARTER_STRING = "QQ";
	private static final String SEPARATOR_STRING_REGEX = "[^a-zA-Z0-9]";
	private static final Map<String, String> DATE_FORMAT_REGEX_REPLACEMENT_MAP = new HashMap<>();

	// Date formats used to find the date in the file name. Order is important as we go from more specific to less specific
	private static final HashSet<String[]> SPECIFIC_DATE_FORMATS = new LinkedHashSet<>();
	// Some monthly date formats will require a separator (2016.09)
	private static final HashSet<String[]> MONTHLY_DATE_FORMATS = new LinkedHashSet<>();
	// Other monthly date formats will NOT require a separator (Sep17)
	private static final HashSet<String[]> MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR = new LinkedHashSet<>();
	private static final HashSet<String[]> QUARTERLY_DATE_FORMATS = new LinkedHashSet<>();


	static {
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("yyyy", "(19|20)\\d\\d");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("yy", "\\d\\d");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("MM", "(0[1-9]|1[012])");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("MMM", "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec])");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("MMMM", "(January|February|March|April|May|June|July|August|September|October|November|December])");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put("dd", "((0[1-9]|[12]\\d|3[01])|([1-9]|[12]\\d|3[01]))");
		DATE_FORMAT_REGEX_REPLACEMENT_MAP.put(QUARTER_STRING, "(([1-4]Q)|(Q[1-4]))");

		SPECIFIC_DATE_FORMATS.add(new String[]{"yyyy", "MM", "dd"});
		SPECIFIC_DATE_FORMATS.add(new String[]{"MM", "dd", "yyyy"});

		MONTHLY_DATE_FORMATS.add(new String[]{"yyyy", "MM"});
		MONTHLY_DATE_FORMATS.add(new String[]{"MM", "yyyy"});

		MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR.add(new String[]{"MMMM", "yyyy"});
		MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR.add(new String[]{"MMMM", "yy"});
		MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR.add(new String[]{"MMM", "yyyy"});
		MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR.add(new String[]{"MMM", "yy"});

		QUARTERLY_DATE_FORMATS.add(new String[]{"yyyy", "QQ"});
		QUARTERLY_DATE_FORMATS.add(new String[]{"QQ", "yyyy"});
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the portal file name, optionally with the relative path, that is used when saving the file to the system.
	 * This includes appended a timestamp to the file name which ensures each file is unique.
	 */
	public static String generatePortalFileName(PortalFile portalFile, String originalFileName, boolean includeRelativePath) {
		// If retaining original file name use it, else If there is a display name use that, Otherwise uses the original file name
		String fileName = FileUtils.getFileNameWithoutExtension(originalFileName);
		fileName = FileUtils.replaceInvalidCharacters(fileName, "");
		// Append "Now" Time Stamp
		fileName += "_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE);

		// Append File Extension
		fileName += "." + FileUtils.getFileExtension(originalFileName);
		if (includeRelativePath) {
			return FileUtils.combinePath(generatePortalFileRelativePath(portalFile), fileName);
		}
		return fileName;
	}


	/**
	 * Returns the relative path for the give portal file.  This includes getting the folder name from the category it's assigned to, as well as which entity it's posted to
	 */
	public static String generatePortalFileRelativePath(PortalFile portalFile) {
		ValidationUtils.assertNotNull(portalFile.getFileCategory(), "File Category Is Required.");
		ValidationUtils.assertNotEmpty(portalFile.getFileCategory().getFolderNameExpanded(), "File Category [" + portalFile.getFileCategory().getNameExpanded() + "] is missing folder structure.");

		StringBuilder location = new StringBuilder(16);
		if (portalFile.isGlobalFile()) {
			location.append("Global Files").append(File.separator);
		}
		else if (portalFile.getPostPortalEntity() != null) {
			location.append(portalFile.getPostPortalEntity().getFolderNameExpanded());
		}
		else {
			location.append("Manual Files").append(File.separator);
		}
		location.append(portalFile.getFileCategory().getFolderNameExpanded());
		return location.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For a given file name, attempts to extract the report date from the file name.  This uses various daily, monthly, and quarterly formats
	 *
	 * @param populateRegexUsedForDate - if not null, the regex that was used to extract the report date is populated here.  Which is used later when trying
	 *                                 to determine the post entity we can first remove the date from the file name
	 */
	public static Date getReportDateFromFileName(String fileName, StringBuilder populateRegexUsedForDate) {
		// If we are using an override date - use that
		Matcher m = Pattern.compile(PortalFileUploadPending.REPORT_DATE_OVERRIDE_EXPRESSION).matcher(fileName);
		if (m.find()) {
			if (populateRegexUsedForDate != null) {
				populateRegexUsedForDate.append(PortalFileUploadPending.REPORT_DATE_OVERRIDE_EXPRESSION);
			}
			String result = m.group();
			result = result.replace(PortalFileUploadPending.REPORT_DATE_OVERRIDE_PREFIX, "");
			return DateUtils.toDate(result, DateUtils.DATE_FORMAT_SQL);
		}

		// Otherwise parse the file name based on various date formats until we can find one...

		// Specific Date
		Date reportDate = findDateInFileNameUsingFormats(fileName, populateRegexUsedForDate, null, false, SPECIFIC_DATE_FORMATS);

		// Specific Month
		if (reportDate == null) {
			reportDate = findDateInFileNameUsingFormats(fileName, populateRegexUsedForDate, DateUtils::getLastDayOfMonth, false, MONTHLY_DATE_FORMATS);
		}
		if (reportDate == null) {
			reportDate = findDateInFileNameUsingFormats(fileName, populateRegexUsedForDate, DateUtils::getLastDayOfMonth, true, MONTHLY_DATE_FORMATS_OPTIONAL_SEPARATOR);
		}
		// Specific Quarter
		if (reportDate == null) {
			reportDate = findDateInFileNameUsingFormats(fileName, populateRegexUsedForDate, DateUtils::getLastDayOfQuarter, false, QUARTERLY_DATE_FORMATS);
		}
		return reportDate;
	}


	private static Date findDateInFileNameUsingFormats(String fileName, StringBuilder populateRegexUsedForDate, UnaryOperator<Date> applyFunction, boolean allowNoSeparator, Set<String[]> dateFormats) {
		for (String[] datePartArray : dateFormats) {
			Date reportDate = findDateInFileNameUsingFormat(fileName, populateRegexUsedForDate, applyFunction, allowNoSeparator, datePartArray);
			if (reportDate != null) {
				return reportDate;
			}
		}
		return null;
	}


	private static Date findDateInFileNameUsingFormat(String fileName, StringBuilder populateRegexUsedForDate, UnaryOperator<Date> applyFunction, boolean allowNoSeparator, String... dateParts) {
		AssertUtils.assertNotEmpty(dateParts, "Date parts to make up the date format must have at least 1 entry");
		StringBuilder formatSb = new StringBuilder();
		StringBuilder regexFormatSb = new StringBuilder("(" + SEPARATOR_STRING_REGEX + "|^)"); // Start regex with non digit or nothing (start of filename)
		// Some cases we'll need to allow for no separator, i.e. Sep17 - most cases we don't want to do this because dates with account numbers gets parsed wrong
		String separatorSuffix = (allowNoSeparator ? "*" : "");
		for (String datePart : dateParts) {
			formatSb.append(datePart);
			regexFormatSb.append(DATE_FORMAT_REGEX_REPLACEMENT_MAP.get(datePart)).append(SEPARATOR_STRING_REGEX).append(separatorSuffix);
		}
		String format = formatSb.toString();
		String regexFormat = regexFormatSb.toString();

		Matcher m = Pattern.compile(regexFormat).matcher(fileName);
		if (m.find()) {
			if (populateRegexUsedForDate != null) {
				populateRegexUsedForDate.append(regexFormat);
			}
			String result = m.group();
			// Special Case for Quarters - Since Quarters is not a real date format change them to the month
			if (format.contains("QQ")) {
				result = result.replaceFirst("(1Q|Q1)", "03");
				result = result.replaceFirst("(2Q|Q2)", "06");
				result = result.replaceFirst("(3Q|Q3)", "09");
				result = result.replaceFirst("(4Q|Q4)", "12");
				format = format.replace("QQ", "MM");
			}
			result = result.replaceAll(SEPARATOR_STRING_REGEX, "");

			Date date = DateUtils.toDate(result, format);
			if (applyFunction != null) {
				return applyFunction.apply(date);
			}
			return date;
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * After removing the file extension and date (if usedDateRegex is populated) returns the first
	 * word in the file name.  This is then used to help filter on the entities that the file could have been posted to
	 * to try to find the one to map it to.
	 */
	public static String getPortalEntitySearchPatternFromFileName(String fileName, String usedDateRegex, String fileCategoryName) {
		// Remove the Extension
		fileName = FileUtils.getFileNameWithoutExtension(fileName);
		// Remove the Date
		if (!StringUtils.isEmpty(usedDateRegex)) {
			fileName = fileName.replaceAll(usedDateRegex, "");
		}
		// Remove the File Category Name
		if (!StringUtils.isEmpty(fileCategoryName)) {
			fileName = fileName.replace(fileCategoryName, "");
		}
		// Get the First Word - customized to include letters, numbers, and - to support names, account numbers (and account numbers with - in them)
		Matcher m = Pattern.compile("([(a-zA-Z0-9-]+)").matcher(fileName);
		while (m.find()) {
			String result = m.group(1);
			if (!StringUtils.isEqualIgnoreCase("the", result)) {
				return result;
			}
		}
		return null;
	}
}
