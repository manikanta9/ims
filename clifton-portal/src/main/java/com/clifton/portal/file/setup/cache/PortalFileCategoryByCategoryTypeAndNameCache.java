package com.clifton.portal.file.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PortalFileCategoryByCategoryTypeAndNameCache extends SelfRegisteringCompositeKeyDaoCache<PortalFileCategory, PortalFileCategoryTypes, String> {


	@Override
	protected String getBeanKey1Property() {
		return "fileCategoryType";
	}


	@Override
	protected String getBeanKey2Property() {
		return "name";
	}


	@Override
	protected PortalFileCategoryTypes getBeanKey1Value(PortalFileCategory bean) {
		return bean.getFileCategoryType();
	}


	@Override
	protected String getBeanKey2Value(PortalFileCategory bean) {
		return bean.getName();
	}
}
