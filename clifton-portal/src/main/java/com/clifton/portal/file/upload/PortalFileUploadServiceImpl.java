package com.clifton.portal.file.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.PortalFileUtils;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
public class PortalFileUploadServiceImpl implements PortalFileUploadService {

	private PortalEntityService portalEntityService;
	private PortalFileService portalFileService;
	private PortalFileSetupService portalFileSetupService;


	/**
	 * The root location where the files are stored that are available for uploading
	 */
	private String uploadDirectory;


	////////////////////////////////////////////////////////////////////////////////
	//////////                Portal File Upload Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortalFile uploadPortalFile(PortalFile portalFile, FileDuplicateActions fileDuplicateAction) {
		if (portalFile.isGlobalFile()) {
			throw new UserIgnorableValidationException(getClass(), "uploadPortalFileAllowGlobal", "You are posting this file/text globally so it will be available to ALL " + (portalFile.getEntityViewType() == null ? "" : portalFile.getEntityViewType().getName() + " ") + "clients.  Are you sure you want to do this?");
		}
		return uploadPortalFileAllowGlobal(portalFile, fileDuplicateAction);
	}


	@Override
	public PortalFile uploadPortalFileAllowGlobal(PortalFile portalFile, FileDuplicateActions fileDuplicateAction) {
		File file = null;
		String originalFileName = null;
		if (portalFile.getFile() != null && portalFile.getFile().getSize() > 0) {
			file = FileUtils.convertMultipartFileToFile(portalFile.getFile());
			originalFileName = portalFile.getFile().getOriginalFilename();
		}
		return uploadPortalFileImpl(portalFile, file, originalFileName, fileDuplicateAction);
	}


	@Transactional
	protected PortalFile uploadPortalFileImpl(PortalFile portalFile, File file, String originalFileName, FileDuplicateActions fileDuplicateAction) {
		if (!CollectionUtils.isEmpty(portalFile.getPostEntityList())) {
			if (portalFile.getPostEntityList().size() == 1) {
				portalFile.setPostPortalEntity(portalFile.getPostEntityList().get(0));
				return getPortalFileService().savePortalFileNew(portalFile, file, originalFileName, fileDuplicateAction);
			}

			// Validate for duplicates - we could just remove them but likely someone may have just selected the wrong one
			Set<Integer> portalEntityIdSet = new HashSet<>();
			for (PortalEntity entity : portalFile.getPostEntityList()) {
				if (portalEntityIdSet.contains(entity.getId())) {
					throw new ValidationException("Please review your rollup entity list selections.  Found duplicate selection for " + entity.getEntityLabelWithSourceSection());
				}
				portalEntityIdSet.add(entity.getId());
			}

			portalFile.setFile(null); // Clear out the file from the portal file
			PortalFile portalFileTemplate = BeanUtils.cloneBean(portalFile, false, false);
			for (PortalEntity portalEntity : portalFile.getPostEntityList()) {
				PortalFile thisPortalFile = BeanUtils.cloneBean(portalFileTemplate, false, false);
				thisPortalFile.setPostPortalEntity(portalEntity);
				getPortalFileService().savePortalFileNew(thisPortalFile, file, originalFileName, fileDuplicateAction);
			}
			return null; // Multiple files were created, so we return null
		}
		return getPortalFileService().savePortalFileNew(portalFile, file, originalFileName, fileDuplicateAction);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////  File Share Feature: Read, Drag and Drop and Remove Files        ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortalFileUploadPending> getPortalFileUploadPendingList(Short fileCategoryId) {
		List<PortalFileCategory> securedCategoryList = getPortalFileSetupService().getPortalFileCategoryForUploadList(null, null, null, false);

		Map<String, PortalFileCategory> filePathCategoryMap = getPortalFileCategoryFilePathMap();
		List<PortalFileUploadPending> list = new ArrayList<>();
		FilePath inputDirectory = FilePath.forPath(getUploadDirectory());
		try {
			Map<FilePath, BasicFileAttributes> fileAttributesMap = FileUtils.getFilesFromDirectory(inputDirectory, true);
			for (Map.Entry<FilePath, BasicFileAttributes> pathBasicFileAttributesEntry : fileAttributesMap.entrySet()) {
				FilePath file = pathBasicFileAttributesEntry.getKey();
				String fileName = FileUtils.getFileName(file.getPath());
				// File added to Linux machine to prevent it from locking up. Don't want users to upload/see this file.
				if (StringUtils.isEqual("heartbeat.log", fileName)) {
					continue;
				}

				BasicFileAttributes attributes = pathBasicFileAttributesEntry.getValue();

				PortalFileUploadPending filePending = new PortalFileUploadPending();

				filePending.setFilePath(FileUtils.getFilePath(FileUtils.getRelativePath(file.getPath(), getUploadDirectory() + File.separator)));
				filePending.setFileCategory(filePathCategoryMap.get(filePending.getFilePath()));
				// If a specific category is selected - only keep those in the list
				if (!isFileCategoryFilterApply(fileCategoryId, filePending)) {
					continue;
				}

				// If current user doesn't have access to post to that category - skip that file from their list
				if (filePending.getFileCategory() != null && !securedCategoryList.contains(filePending.getFileCategory())) {
					continue;
				}

				filePending.setFileName(fileName);
				filePending.setModifiedDate(new Date(attributes.lastModifiedTime().toMillis()));
				filePending.setPublishDate(filePending.getModifiedDate());
				filePending.setFileSize(attributes.size());

				// When pulling the date from the file name, this will be populated with the regex that found the date so we can remove it when trying to find the post entity
				StringBuilder dateRegex = new StringBuilder(25);
				Date reportDate = PortalFileUtils.getReportDateFromFileName(filePending.getFileName(), dateRegex);
				if (filePending.getFileCategory() != null) {
					filePending.setFileFrequency(filePending.getFileCategory().getPortalFileFrequency());
					// If it's a quarterly file, but a month end date, change frequency to monthly
					if (PortalFileFrequencies.QUARTERLY == filePending.getFileFrequency() && filePending.getReportDate() != null) {
						if (DateUtils.isLastDayOfMonth(filePending.getReportDate()) && !DateUtils.isEqualWithoutTime(filePending.getReportDate(), DateUtils.getLastDayOfQuarter(filePending.getReportDate()))) {
							filePending.setFileFrequency(PortalFileFrequencies.MONTHLY);
						}
					}
					if (filePending.getFileFrequency() != null && reportDate != null) {
						reportDate = filePending.getFileFrequency().getReportDateForDate(reportDate);
					}
				}
				filePending.setReportDate(reportDate);
				filePending.setPostPortalEntity(getPostPortalEntityFromFileName(filePending.getFileName(), dateRegex.toString(), filePending.getFileCategory()));
				list.add(filePending);
			}
		}
		catch (Throwable e) {
			throw new ValidationException("Error loading file list: " + ExceptionUtils.getOriginalMessage(e), e);
		}
		return list;
	}


	private boolean isFileCategoryFilterApply(Short fileCategoryId, PortalFileUploadPending fileUploadPending) {
		if (fileCategoryId == null) {
			return true;
		}
		PortalFileCategory fileCategory = fileUploadPending.getFileCategory();
		while (fileCategory != null) {
			if (MathUtils.isEqual(fileCategoryId, fileCategory.getId())) {
				return true;
			}
			fileCategory = fileCategory.getParent();
		}
		return false;
	}


	/**
	 * Used for individual errors to be able to replace the file if an error is found because the file already exists
	 */
	@Override
	public void uploadPortalFileUploadPendingReplace(PortalFileUploadPendingCommand uploadPendingCommand) {
		uploadPendingCommand.setFileAction(PortalFileUploadPendingFileActions.REPLACE);
		uploadPortalFileUploadPending(uploadPendingCommand);
	}


	@Override
	public void uploadPortalFileUploadPending(PortalFileUploadPendingCommand uploadPendingCommand) {
		ValidationUtils.assertNotNull(uploadPendingCommand.getFile(), "File is Required");
		ValidationUtils.assertNotNull(uploadPendingCommand.getFileAction(), "File Action is Required (SKIP, REPLACE, ERROR)");
		String path = "";
		PortalFileCategory category;
		// If category is specified on screen - use it
		if (uploadPendingCommand.getPortalFileCategoryId() != null) {
			category = getPortalFileSetupService().getPortalFileCategory(uploadPendingCommand.getPortalFileCategoryId());
		}
		// Otherwise, try to determine it from the file name
		else {
			category = getPortalFileCategoryFromFileName(uploadPendingCommand.getFile().getOriginalFilename());
		}
		if (category != null) {
			path = category.getFolderNameExpanded();
		}

		String newFileNameAndPath = FileUtils.combinePath(path, uploadPendingCommand.getFile().getOriginalFilename());
		// If report date is set we need to append it to the file name so we can map it as a property later
		if (uploadPendingCommand.getReportDateOverride() != null) {
			newFileNameAndPath = FileUtils.getFileNameWithoutExtension(newFileNameAndPath) + PortalFileUploadPending.REPORT_DATE_OVERRIDE_PREFIX + DateUtils.fromDate(uploadPendingCommand.getReportDateOverride(), DateUtils.DATE_FORMAT_SQL) + "." + FileUtils.getFileExtension(newFileNameAndPath);
		}

		File newFile = new File(FileUtils.combinePath(getUploadDirectory(), newFileNameAndPath));
		// If the new file exists then it was already moved in a prior deployments so just skip, but still update the file name and path on the PortalFile table
		if (newFile.exists()) {
			if (uploadPendingCommand.getFileAction().isReplaceFile()) {
				FileUtils.delete(newFile);
			}
			else if (uploadPendingCommand.getFileAction().isError()) {
				// NOTE: THIS DOESN'T ACTUALLY DO ANYTHING - SEE FileUploadPendingListWindow.js for current workaround until error handling is fixed
				// in ExternalServiceInvocationHandler.isJsonResponseError method
				throw new UserIgnorableValidationException(PortalFileUploadService.class, "uploadPortalFileUploadPendingReplace", "File: [" + uploadPendingCommand.getFile().getOriginalFilename() + "] already exists.");
			}
			else {
				// Do Nothing
				return;
			}
		}
		try {
			FileUtils.createDirectory(FileUtils.combinePath(getUploadDirectory(), path));
			FileUtils.convertInputStreamToFile(FileUtils.combinePath(getUploadDirectory(), newFileNameAndPath), uploadPendingCommand.getFile().getInputStream());
		}
		catch (IOException e) {
			throw new ValidationException("Error uploading file [" + uploadPendingCommand.getFile().getOriginalFilename() + "]: " + e.getMessage(), e);
		}
	}


	@Override
	public void deletePortalFileUploadPending(String filePath, String fileName) {
		File file = new File(FileUtils.combinePaths(getUploadDirectory(), filePath, fileName));
		if (!file.exists()) {
			throw new ValidationException("File [" + fileName + "] no longer exists.");
		}
		FileUtils.delete(file);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Post/Save As Real Portal File             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public PortalFile savePortalFileUploadPending(PortalFileUploadPending bean, FileDuplicateActions fileDuplicateAction) {
		PortalFile portalFile = new PortalFile();
		File file = new File(FileUtils.combinePaths(getUploadDirectory(), bean.getFilePath(), bean.getFileName()));
		if (!file.exists()) {
			throw new ValidationException("File [" + bean.getFileName() + "] no longer exists.");
		}
		portalFile.setFileCategory(bean.getFileCategory());
		portalFile.setDisplayName(bean.getFileName());
		portalFile.setReportDate(bean.getReportDate());
		portalFile.setPublishDate(bean.getPublishDate());
		portalFile.setApproved(false);
		portalFile.setFileFrequency(bean.getFileFrequency());
		portalFile.setPostPortalEntity(bean.getPostPortalEntity());

		portalFile = uploadPortalFileImpl(portalFile, file, bean.getFileName(), fileDuplicateAction);
		FileUtils.delete(file);
		return portalFile;
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////                 Mapping Helper Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////////////


	protected Map<String, PortalFileCategory> getPortalFileCategoryFilePathMap() {
		PortalFileCategorySearchForm searchForm = new PortalFileCategorySearchForm();
		searchForm.setFileCategoryType(PortalFileCategoryTypes.FILE_GROUP);

		List<PortalFileCategory> list = getPortalFileSetupService().getPortalFileCategoryList(searchForm);
		return BeanUtils.getBeanMap(list, PortalFileCategory::getFolderNameExpanded);
	}


	protected PortalEntity getPostPortalEntityFromFileName(String fileName, String dateRegex, PortalFileCategory fileCategory) {
		String portalEntitySearchPattern = PortalFileUtils.getPortalEntitySearchPatternFromFileName(fileName, dateRegex, fileCategory == null ? null : fileCategory.getName());
		if (StringUtils.isEmpty(portalEntitySearchPattern)) {
			return null;
		}

		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setPostableEntity(true);
		if (fileCategory != null && fileCategory.getPostEntityType() != null) {
			searchForm.setEntityTypeOrChildEntityTypeId(fileCategory.getPostEntityType().getId());
		}
		searchForm.setSearchPattern(portalEntitySearchPattern);

		List<PortalEntity> entityList = getPortalEntityService().getPortalEntityList(searchForm);
		if (CollectionUtils.isEmpty(entityList)) {
			return null;
		}
		if (CollectionUtils.getSize(entityList) == 1) {
			return entityList.get(0);
		}
		// If more than one - try finding by name/label in file name or vice versa
		for (PortalEntity portalEntity : entityList) {
			if (isMatchToFileName(fileName, portalEntity.getName())) {
				return portalEntity;
			}
			if (isMatchToFileName(fileName, portalEntity.getLabel())) {
				return portalEntity;
			}
			if (isMatchToFileName(fileName, portalEntity.getDescription())) {
				return portalEntity;
			}
		}
		return null;
	}


	private boolean isMatchToFileName(String fileName, String entityName) {
		if (entityName != null) {
			if (fileName.contains(entityName)) {
				return true;
			}
			entityName = FileUtils.replaceInvalidCharacters(entityName, "");
			if (fileName.contains(entityName)) {
				return true;
			}
		}
		return false;
	}


	protected PortalFileCategory getPortalFileCategoryFromFileName(String fileName) {
		List<PortalFileCategory> list = getPortalFileSetupService().getPortalFileCategoryForUploadList(null, null, null, false);

		PortalFileCategory bestMatchCategory = null;

		for (PortalFileCategory category : CollectionUtils.getIterable(list)) {
			if (isFileCategoryBestMatchForFileName(fileName, category, bestMatchCategory)) {
				bestMatchCategory = category;
			}
		}
		return bestMatchCategory;
	}


	/**
	 * Find the best match and if the current category is a better match then the existing best match.
	 * i.e. IMA vs. IMA Amendments
	 * Also allows for some flexibility - i.e. category name = Performance Reports, file name contains Performance Report
	 */
	private boolean isFileCategoryBestMatchForFileName(String fileName, PortalFileCategory category, PortalFileCategory currentBestMatchCategory) {
		boolean match = false;
		// Contains category name
		// Or Contains category name without ending s (i.e. Performance Report vs. Performance Reports)
		if (StringUtils.isLikeIgnoringCase(fileName, category.getName()) || (category.getName().endsWith("s") && StringUtils.isLikeIgnoringCase(fileName, StringUtils.substringBeforeLast(category.getName(), "s")))) {
			match = true;
		}

		// If it was a match, but we had a previous match - keep the one with the longer name
		// i.e. IMA vs. IMA Amendments
		if (match) {
			if (currentBestMatchCategory != null) {
				match = StringUtils.length(category.getName()) > StringUtils.length(currentBestMatchCategory.getName());
			}
		}
		return match;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public String getUploadDirectory() {
		return this.uploadDirectory;
	}


	public void setUploadDirectory(String uploadDirectory) {
		this.uploadDirectory = uploadDirectory;
	}
}
