package com.clifton.portal.file.observers;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.file.PortalFileService;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>PortalFileSourceEntityObserver</code> observes changes to PortalEntity and PortalEntityFieldValue tables.
 * <p>
 * On changes when the PortalEntity IsFileSource = true, a runner is scheduled to update the Display Name/Text for any PortalFile that references this entity as it's source
 * <p>
 * The schedule runs on a 10 second delay, so all entity data and entity field values and completed before the runner actually runs to prevent running multiple times.
 */
@Component
public class PortalFileSourceEntityObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private PortalFileService portalFileService;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			final PortalEntity portalEntity = getPortalEntity(bean);

			if (portalEntity != null && portalEntity.getEntitySourceSection().getEntityType().isFileSource()) {
				// asynchronous support
				String runId = "ENTITY_" + portalEntity.getId();
				final Date scheduledDate = DateUtils.addSeconds(new Date(), 10);

				Runner runner = new AbstractStatusAwareRunner("PORTAL_FILE_TEMPLATE_GENERATOR_FOR_SOURCE", runId, scheduledDate) {

					@Override
					public void run() {
						getPortalFileService().generatePortalFileTemplatesForPortalFilesBySource(portalEntity.getEntitySourceSection().getId(), portalEntity.getSourceFkFieldId());
						getStatus().setMessage("Portal File Template Generator Completed for files that reference source entity [" + portalEntity.getEntityLabelWithSourceSection() + "].");
					}
				};
				getRunnerHandler().rescheduleRunner(runner);
			}
		}
	}


	private PortalEntity getPortalEntity(T bean) {
		if (bean instanceof PortalEntity) {
			return (PortalEntity) bean;
		}
		else if (bean instanceof PortalEntityFieldValue) {
			return ((PortalEntityFieldValue) bean).getPortalEntity();
		}
		throw new ValidationException("PortalFileSourceEntityObserver does not support observing changes to " + bean.getClass().getName());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
