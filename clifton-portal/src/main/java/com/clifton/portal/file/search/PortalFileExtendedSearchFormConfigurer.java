package com.clifton.portal.file.search;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.PortalUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.setup.mapping.PortalFileCategoryPortalEntityMapping;
import com.clifton.portal.security.search.BasePortalEntitySpecificAwareSearchFormConfigurer;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpanded;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.List;


/**
 * @author manderson
 */
public class PortalFileExtendedSearchFormConfigurer extends BasePortalEntitySpecificAwareSearchFormConfigurer {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileExtendedSearchFormConfigurer(PortalFileExtendedSearchForm searchForm, PortalSecurityUserService portalSecurityUserService, PortalEntityService portalEntityService) {
		super(searchForm, portalSecurityUserService, portalEntityService);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PortalFileExtendedSearchForm portalFileExtendedSearchForm = (PortalFileExtendedSearchForm) getSortableSearchForm();

		if (portalFileExtendedSearchForm.getDefaultDisplay() != null) {
			criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(((PortalFileExtendedSearchForm) getSortableSearchForm()).getDefaultDisplay(), null, "reportDate", "endDate", null));
		}

		if (portalFileExtendedSearchForm.getAssignedPortalEntityId() != null) {
			List<PortalEntity> relatedPortalEntityList = getPortalEntityService().getRelatedPortalEntityList(portalFileExtendedSearchForm.getAssignedPortalEntityId(), true, true, true, false);
			configureCriteriaForViewAsEntity(criteria, relatedPortalEntityList);
		}
		if (BooleanUtils.isTrue(portalFileExtendedSearchForm.getActiveAssignedPortalEntity())) {
			String alias = getPathAlias("assignedPortalEntity", criteria, JoinType.LEFT_OUTER_JOIN);
			criteria.add(ActiveExpressionForDates.forActiveWithAlias(true, alias));
		}

		if (!StringUtils.isEmpty(portalFileExtendedSearchForm.getSourceFieldTypeName())) {
			// Filter on Portal Entity Field Value
			DetachedCriteria sub = DetachedCriteria.forClass(PortalEntityFieldValue.class, "sfv");
			sub.setProjection(Projections.id());
			sub.createAlias("sfv.portalEntity", "spe");
			sub.createAlias("sfv.portalEntityFieldType", "sft");
			sub.add(Restrictions.eqProperty("spe.sourceFkFieldId", criteria.getAlias() + ".sourceFkFieldId"));
			sub.add(Restrictions.eqProperty("spe.entitySourceSection.id", criteria.getAlias() + ".entitySourceSection.id"));
			sub.add(Restrictions.eq("sft.name", portalFileExtendedSearchForm.getSourceFieldTypeName()));
			if (!StringUtils.isEmpty(portalFileExtendedSearchForm.getSourceFieldValue())) {
				sub.add(Restrictions.eq("sfv.value", portalFileExtendedSearchForm.getSourceFieldValue()));
			}
			if (portalFileExtendedSearchForm.getSourceFieldValueUpdateDateMin() != null) {
				sub.add(Restrictions.ge("sfv.updateDate", portalFileExtendedSearchForm.getSourceFieldValueUpdateDateMin()));
			}
			if (portalFileExtendedSearchForm.getSourceFieldValueUpdateDateMax() != null) {
				sub.add(Restrictions.le("sfv.updateDate", portalFileExtendedSearchForm.getSourceFieldValueUpdateDateMax()));
			}
			criteria.add(Subqueries.exists(sub));
		}

		if (portalFileExtendedSearchForm.getFirstApprovedFileForEntity() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(PortalFileCategoryPortalEntityMapping.class, "fcm");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("firstApprovalDate", criteria.getAlias() + ".approvalDate"));
			sub.add(Restrictions.eqProperty("portalFileCategoryId", criteria.getAlias() + ".fileCategory.id"));
			sub.add(Restrictions.eqProperty("portalEntityId", criteria.getAlias() + ".assignedPortalEntity.id"));
			criteria.add(Subqueries.exists(sub));
		}
	}


	@Override
	public void configureCriteriaForUser(Criteria criteria, short portalSecurityUserId) {
		DetachedCriteria sub = DetachedCriteria.forClass(PortalSecurityUserAssignmentExpanded.class, "se");
		sub.setProjection(Projections.property("id"));
		sub.add(Restrictions.eq("portalSecurityUserId", portalSecurityUserId));
		String assignedPortalEntityAlias = getPathAlias("assignedPortalEntity", criteria, JoinType.LEFT_OUTER_JOIN);
		sub.add(
				Restrictions.or(
						Restrictions.and(
								Restrictions.isNull(assignedPortalEntityAlias + ".id"),
								Restrictions.or(Restrictions.isNull(getPathAlias("entityViewType", criteria, JoinType.LEFT_OUTER_JOIN) + ".id")
										, Restrictions.eqProperty("portalEntityViewTypeId", getPathAlias("entityViewType", criteria, JoinType.LEFT_OUTER_JOIN) + ".id")
								))
						, Restrictions.eqProperty("portalEntityId", assignedPortalEntityAlias + ".id")));
		sub.add(Restrictions.eqProperty("portalSecurityResourceId", getPathAlias("fileCategory.securityResource", criteria) + ".id"));
		criteria.add(Subqueries.exists(sub));
	}


	@Override
	public void configureCriteriaForViewAsEntity(Criteria criteria, List<PortalEntity> viewAsEntityList) {
		// Note: viewAsEntityList is already pre-filtered to active only
		LogicalExpression trueGlobalFileRestriction = Restrictions.and(Restrictions.eq("globalFile", true), Restrictions.isNull("entityViewType.id"));
		if (CollectionUtils.isEmpty(viewAsEntityList)) {
			// No Entities, then only TRUE global files (no view type)
			criteria.add(trueGlobalFileRestriction);
		}
		else {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(trueGlobalFileRestriction);
			// Entity Specific File Restriction
			disjunction.add(Restrictions.in("assignedPortalEntity.id", BeanUtils.getBeanIdentityArray(viewAsEntityList, Integer.class)));
			// Global Files limited by view type
			Short[] entityViewTypes = PortalUtils.getPortalEntityViewTypeIds(viewAsEntityList);
			if (!ArrayUtils.isEmpty(entityViewTypes)) {
				disjunction.add(Restrictions.and(
						Restrictions.eq("globalFile", true),
						(entityViewTypes.length == 1 ? Restrictions.eq("entityViewType.id", entityViewTypes[0]) : Restrictions.in("entityViewType.id", entityViewTypes))));
			}
			criteria.add(disjunction);
		}
	}
}
