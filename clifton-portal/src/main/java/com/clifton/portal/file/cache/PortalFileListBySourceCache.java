package com.clifton.portal.file.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringThreeKeyDaoListCache;
import com.clifton.portal.file.PortalFile;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalFileListBySourceCache</code> caches a list of portal files associated with a particular source record
 * i.e. IMS PortfolioRun ID 123.  Normally there would only be one file per source, but some cases
 * there could be more (i.e. Invoices or Investment Statements that could contain additional supporting data)
 *
 * @author manderson
 */
@Component
public class PortalFileListBySourceCache extends SelfRegisteringThreeKeyDaoListCache<PortalFile, Short, String, Integer> {

	@Override
	protected String getBeanKey1Property() {
		return "entitySourceSection.sourceSystem.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "entitySourceSection.sourceSystemTableName";
	}


	@Override
	protected String getBeanKey3Property() {
		return "sourceFkFieldId";
	}


	@Override
	protected Short getBeanKey1Value(PortalFile bean) {
		if (bean.getEntitySourceSection() != null && bean.getEntitySourceSection().getSourceSystem() != null) {
			return bean.getEntitySourceSection().getSourceSystem().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(PortalFile bean) {
		if (bean.getEntitySourceSection() != null) {
			return bean.getEntitySourceSection().getSourceSystemTableName();
		}
		return null;
	}


	@Override
	protected Integer getBeanKey3Value(PortalFile bean) {
		return bean.getSourceFkFieldId();
	}
}
