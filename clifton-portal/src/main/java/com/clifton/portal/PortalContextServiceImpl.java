package com.clifton.portal;

import com.clifton.core.cache.CacheStats;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.spring.UrlDefinitionDescriptor;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.logging.configuration.LogAppender;
import com.clifton.core.logging.configuration.LogConfiguration;
import com.clifton.core.logging.configuration.LogConfigurationService;
import com.clifton.core.security.authorization.SecureClass;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetailService;
import com.clifton.core.util.runner.config.RunnerConfig;
import com.clifton.core.util.runner.config.RunnerConfigSearchForm;
import com.clifton.core.util.runner.config.RunnerConfigService;
import com.clifton.core.web.stats.MemoryState;
import com.clifton.core.web.stats.SystemRequestStats;
import com.clifton.core.web.stats.SystemRequestStatsService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * @author manderson
 */
@SecureClass
@Service
public class PortalContextServiceImpl implements PortalContextService {


	private ApplicationContextService applicationContextService;

	private MigrationSchemaService migrationSchemaService;

	private LogConfigurationService logConfigurationService;

	private RunnerConfigService runnerConfigService;

	private SystemRequestStatsService systemRequestStatsService;

	private SecurityOAuthClientDetailService securityOAuthClientDetailService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<UrlDefinitionDescriptor> getPortalContextUrlMappingDefinitionList() {
		return getApplicationContextService().getContextUrlMappingDefinitionList();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	@Override
	public String getPortalJsonMigrationAction(String entityTableName, Integer entityFkFieldId) throws Exception {
		return getMigrationSchemaService().getJsonMigrationAction(entityTableName, entityFkFieldId);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a Collection of all Portal SystemRequestStats objects.
	 */
	@Override
	public List<SystemRequestStats> getPortalSystemRequestStatsList(AuditableEntitySearchForm searchForm) {
		return getSystemRequestStatsService().getSystemRequestStatsList(searchForm);
	}


	/**
	 * Removes request stats from PORTAL APP for the specified source and URI from the cache.
	 * Can be used to cleanup invalid data.
	 */
	@Override
	public void deletePortalSystemRequestStats(String requestSource, String requestURI) {
		getSystemRequestStatsService().deleteSystemRequestStats(requestSource, requestURI);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public int setPortalSystemCacheStatsEnabled(String cacheName, boolean enableStatistics) {
		return getSystemRequestStatsService().setSystemCacheStatsEnabled(cacheName, enableStatistics);
	}


	@Override
	public List<CacheStats> getPortalSystemCacheStats(boolean calculateSize) {
		return getSystemRequestStatsService().getSystemCacheStatsList(calculateSize);
	}


	@Override
	public void deletePortalSystemCache(String cacheName) {
		getSystemRequestStatsService().deleteSystemCache(cacheName);
	}


	@Override
	public MemoryState getPortalSystemMemoryState() {
		return getSystemRequestStatsService().getSystemMemoryState();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RunnerConfig> getPortalRunnerConfigList(RunnerConfigSearchForm searchForm) {
		return getRunnerConfigService().getRunnerConfigList(searchForm);
	}


	@Override
	public void rescheduleRunnersFromProviders() {
		getRunnerConfigService().rescheduleRunnersFromProviders();
	}


	@Override
	public void restartPortalRunnerHandler() {
		getRunnerConfigService().restartRunnerHandler();
	}


	@Override
	public void clearCompletedPortalRunners() {
		getRunnerConfigService().clearCompletedRunners();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityOAuthClientDetail getPortalClientDetailsFromFile(String id) {
		return getSecurityOAuthClientDetailService().getClientDetailsFromFile(id);
	}


	@Override
	public SecurityOAuthClientDetail deletePortalClientDetailFromFiles(String id) {
		return getSecurityOAuthClientDetailService().deleteClientDetailFromFiles(id);
	}


	@Override
	public List<SecurityOAuthClientDetail> getPortalClientDetailsList() {
		return getSecurityOAuthClientDetailService().getClientDetailsList();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public LogConfiguration savePortalLogConfigurationLogAppenderLink(String loggerName, String appenderName) {
		return getLogConfigurationService().saveLogConfigurationLogAppenderLink(loggerName, appenderName);
	}


	@Override
	public LogConfiguration deletePortalLogConfigurationLogAppenderLink(String loggerName, String appenderName) {
		return getLogConfigurationService().deleteLogConfigurationLogAppenderLink(loggerName, appenderName);
	}


	@Override
	public LogAppender getPortalLogAppender(String name) {
		return getLogConfigurationService().getLogAppender(name);
	}


	@Override
	public List<LogAppender> getPortalLogAppenderList() {
		return getLogConfigurationService().getLogAppenderList();
	}


	@Override
	public List<LogAppender> getPortalLogAppenderListForLogConfiguration(String loggerName) {
		return getLogConfigurationService().getLogAppenderListForLogConfiguration(loggerName);
	}


	@Override
	public LogConfiguration getPortalLogConfiguration(String name) {
		return getLogConfigurationService().getLogConfiguration(name);
	}


	@Override
	public LogConfiguration savePortalLogConfiguration(LogConfiguration configuration) {
		return getLogConfigurationService().saveLogConfiguration(configuration);
	}


	@Override
	public LogConfiguration deletePortalLogConfiguration(String name) {
		return getLogConfigurationService().deleteLogConfiguration(name);
	}


	@Override
	public List<LogConfiguration> getPortalLogConfigurationList() {
		return getLogConfigurationService().getLogConfigurationList();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public MigrationSchemaService getMigrationSchemaService() {
		return this.migrationSchemaService;
	}


	public void setMigrationSchemaService(MigrationSchemaService migrationSchemaService) {
		this.migrationSchemaService = migrationSchemaService;
	}


	public RunnerConfigService getRunnerConfigService() {
		return this.runnerConfigService;
	}


	public void setRunnerConfigService(RunnerConfigService runnerConfigService) {
		this.runnerConfigService = runnerConfigService;
	}


	public SecurityOAuthClientDetailService getSecurityOAuthClientDetailService() {
		return this.securityOAuthClientDetailService;
	}


	public void setSecurityOAuthClientDetailService(SecurityOAuthClientDetailService securityOAuthClientDetailService) {
		this.securityOAuthClientDetailService = securityOAuthClientDetailService;
	}


	public LogConfigurationService getLogConfigurationService() {
		return this.logConfigurationService;
	}


	public void setLogConfigurationService(LogConfigurationService logConfigurationService) {
		this.logConfigurationService = logConfigurationService;
	}
}
