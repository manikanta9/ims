package com.clifton.portal;

import com.clifton.core.beans.BeanUtils;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.PortalEntityViewTypeAware;

import java.util.List;


/**
 * @author manderson
 */
public class PortalUtils {


	/**
	 * Returns the {@link PortalEntityViewType} ID (if populated) for the given {@link PortalEntityViewTypeAware} bean
	 */
	public static Short getPortalEntityViewTypeId(PortalEntityViewTypeAware viewTypeAware) {
		if (viewTypeAware != null && viewTypeAware.getEntityViewType() != null) {
			return viewTypeAware.getEntityViewType().getId();
		}
		return null;
	}


	/**
	 * Returns unique Short[] of {@link PortalEntityViewType} IDs (where populated) for the given list of {@link PortalEntityViewTypeAware} beans
	 */
	public static <T extends PortalEntityViewTypeAware> Short[] getPortalEntityViewTypeIds(List<T> viewTypeAwareList) {
		return BeanUtils.getPropertyValuesUniqueExcludeNull(viewTypeAwareList, PortalUtils::getPortalEntityViewTypeId, Short.class);
	}
}
