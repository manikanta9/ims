Ext.ns(
	'Clifton.portal',
	'Clifton.portal.entity', 'Clifton.portal.entity.setup', 'Clifton.portal.entity.upload',
	'Clifton.portal.login',
	'Clifton.portal.logging',
	'Clifton.portal.feedback',
	'Clifton.portal.file', 'Clifton.portal.file.setup', 'Clifton.portal.file.enableDD',
	'Clifton.portal.notification', 'Clifton.portal.notification.setup', 'Clifton.portal.notification.subscription',
	'Clifton.portal.security', 'Clifton.portal.security.user', 'Clifton.portal.security.resource', 'Clifton.portal.security.user.group',
	'Clifton.portal.tracking',
	'Clifton.portal.page'
);

Clifton.portal.logging.LoggingAdditionalTabs = [];

Clifton.portal.file.PortalFileFrequencies = [
	['DAILY', 'DAILY', 'Daily files.  Files are associated with a specific date.  Date format is MM/dd/yyyy'],
	['WEEKLY', 'WEEKLY', 'Weekly files.  Files are expected to be posted once a week, however there is no specific day of the week that is forced on these files.  Date format is MM/dd/yyyy'],
	['MONTHLY', 'MONTHLY', 'Monthly files.  Files are associated with a specific month (and report date is the last day of the month)  Date format is Month Name YYYY (i.e. January 2017)'],
	['QUARTER_TO_DATE', 'QUARTER_TO_DATE', 'Quarter to Date Files.  Files are associated with a specific month, but contain information as of the first day of the calendar quarter. (report date is the last day of the month)  Date format is Month Name YYYY QTD (i.e. January 2017 QTD)'],
	['QUARTERLY', 'QUARTERLY', 'Quarterly files.  Files are associated with a specific calendar quarter (and report date is the last day of the quarter)  Date format is Quarter # YYYY (i.e. 4Q 2016)'],
	['ANNUALLY', 'ANNUALLY', 'Annual files.  Files are associated with a specific calendar year however there is no specific day of the year that is forced on these files. Date format is YYYY (i.e. 2016)'],
	['FINAL', 'FINAL', 'Files are posted as needed and are considered final and available as long as they are approved.  Used most commonly for Legal Documentation.']
];


Clifton.portal.notification.PortalNotificationRecurrences = [
	['NONE', 'NONE', 'No recurrence.  Each recipient is sent email if there is data that applies and could result in duplicate notifications.  Should be used rarely and likely only for internal type notifications.'],
	['ONCE', 'ONCE', 'One occurrence.  Each recipient is sent only one email (if file specific, then one email per file)'],
	['DAYS', 'DAYS', 'Each recipient is sent email every XX days'],
	['DAYS_ONCE', 'DAYS_ONCE', 'Each recipient is sent only one email, but after XX days'],
	['MONTHS', 'MONTHS', 'Each recipient is sent email every XX months'],
	['MONTHS_ONCE', 'MONTHS_ONCE', 'Each recipient is sent only one email, but after XX months'],
	['YEARS', 'YEARS', 'Each recipient is sent email every XX years'],
	['YEARS_ONCE', 'YEARS_ONCE', 'Each recipient is sent only one email, but after XX years']
];

// Notification Subscription Defaults
Clifton.portal.notification.subscription.SubscriptionOptions = [
	['DEFAULT_OFF', 'DEFAULT_OFF', 'Users have the ability to subscribe to notifications for this file category, however that option is turned off by default.'],
	['DEFAULT_ON', 'DEFAULT_ON', 'Users have the ability to subscribe to notifications for this file category.  The subscriptions are ON by default.'],
	['REQUIRED_ON', 'REQUIRED_ON', 'Users are required to receive these notifications and cannot turn them off.']
];

// List of all used only for list search form filters
Clifton.portal.notification.subscription.SubscriptionFrequencies = [
	['WEEKLY_FIRST_BUSINESS_DAY', 'First Business Day of the Week', 'First Business Day of the Week'],
	['MONTHLY_FIRST_BUSINESS_DAY', 'First Business Day of the Month', 'First Business Day of the Month'],
	['QUARTERLY_FIRST_BUSINESS_DAY', 'First Business Day of the Quarter', 'First Business Day of the Quarter']
];

Clifton.portal.notification.subscription.GetSubscriptionFrequenciesForFileFrequency = function(fileFrequency) {
	if (TCG.isEquals('DAILY', fileFrequency)) {
		return [
			['WEEKLY_FIRST_BUSINESS_DAY', 'First Business Day of the Week', 'First Business Day of the Week'],
			['MONTHLY_FIRST_BUSINESS_DAY', 'First Business Day of the Month', 'First Business Day of the Month'],
			['QUARTERLY_FIRST_BUSINESS_DAY', 'First Business Day of the Quarter', 'First Business Day of the Quarter']
		];
	}
	return [];
};

Clifton.portal.file.MaxDownloadCount = 75; // Also update PortalFileServiceImpl if this number changes

Clifton.portal.GetPreviousBusinessDay = function(date) {
	if (TCG.isNull(date)) {
		date = new Date();
	}
	const dateString = date.format('m/d/Y');
	let result = TCG.CacheUtils.get('PORTAL_PREVIOUS_BUSINESS_DAY_CACHE', dateString);
	if (!result) {
		result = TCG.parseDate(TCG.getResponseText('portalHolidayPreviousBusinessDay.json?date=' + dateString));
		TCG.CacheUtils.put('PORTAL_PREVIOUS_BUSINESS_DAY_CACHE', result, dateString);
	}
	return result;
};


/**
 *  Opens New Window of Portal Admin Client UI with the ability to view files as the view as user or entity
 */
Clifton.portal.ViewClientUIAsUser = function(portalSecurityUserId) {
	Clifton.portal.ViewClientUIAsUserOrEntity(portalSecurityUserId, null);
};

Clifton.portal.ViewClientUIAsEntity = function(portalEntityId) {
	Clifton.portal.ViewClientUIAsUserOrEntity(null, portalEntityId);
};

Clifton.portal.ViewClientUIAsUserOrEntity = function(portalSecurityUserId, portalEntityId) {
	const url = `${Clifton.portal.AdminClientUIUrl}/?viewAsUserId=${portalSecurityUserId || ''}&viewAsEntityId=${portalEntityId || ''}`;
	window.open(url, '_blank');
};

/**
 * Open Client UI from Portal Admin - Pops up window to select View as User or View As Entity
 * assignedPortalEntityId is not required and if populated, used to default the View as Entity to the root parent of that entity
 * For Example, viewing files for a Client - Client UI view should default to that Client's Relationship (or whatever is at it's highest level)
 */
Clifton.portal.ViewClientUIForFile = function(portalFileId, assignedPortalEntityId) {
	TCG.data.getDataPromise('portalFileSecurityUserListFind.json', null, {params: {disabled: false, portalFileId: portalFileId, userRolePortalEntitySpecific: true, requestedPropertiesRoot: 'data', requestedProperties: 'id|labelLong'}})
		.then(function(userResult) {
			const userArrayResult = [];
			if (userResult && userResult instanceof Array) {
				for (let i = 0; i < userResult.length; i++) {
					userArrayResult.push([userResult[i].id, userResult[i].labelLong, userResult[i].labelLong]);
				}
			}
			TCG.data.getDataPromise('portalFileSecurableEntityListForFile.json', null, {params: {portalFileId: portalFileId, requestedPropertiesRoot: 'data', requestedProperties: 'id|entityLabelWithSourceSection'}})
				.then(function(entityResult) {
					const entityArrayResult = [];
					if (entityResult && entityResult instanceof Array) {
						for (let i = 0; i < entityResult.length; i++) {
							entityArrayResult.push([entityResult[i].id, entityResult[i].entityLabelWithSourceSection, entityResult[i].entityLabelWithSourceSection]);
						}
					}

					new TCG.app.OKCancelWindow({
						okButtonText: 'View Client Portal',
						okButtonTooltip: 'View Client Portal for selected user or entity',
						iconCls: 'www',
						width: 550,
						height: 350,
						modal: true,
						allowOpenFromModal: true,
						title: 'View Client Portal As User or Entity',
						items: [
							{
								xtype: 'formpanel',
								labelWidth: 1,
								instructions: 'Select either a View as User or View as Entity to view the Client Portal as.<br>'
									+ '<ul class="c-list">'
									+ '<li><b>View As User:</b> When viewing as a selected user, you will be able to see their notification subscriptions.  Administration screen is only available if it is available to the selected user. Any entity this selected user has access to will be available.</li>'
									+ '<li><b>View As Entity:</b> When viewing as a selected entity, notification subscriptions will not be available for viewing.  The selected entity and any parents or children will be available.</li>'
									+ '</ul>',
								loadDefaultDataAfterRender: true,
								listeners: {
									afterrender: function(fp) {
										if (userArrayResult.length === 0) {
											const userField = fp.getForm().findField('viewAsUser');
											userField.emptyText = 'No Users Available';
										}
										const entityField = fp.getForm().findField('viewAsEntity');
										if (entityArrayResult.length === 0) {
											entityField.emptyText = 'No Entities Available';
										}
										else if (TCG.isNotBlank(assignedPortalEntityId)) {
											TCG.data.getDataPromiseUsingCaching('portalEntity.json?id=' + assignedPortalEntityId, fp, 'portal.entity.' + assignedPortalEntityId)
												.then(function(portalEntity) {
													if (portalEntity) {
														fp.setFormValue('viewAsEntity', {value: portalEntity.rootParent.id, text: portalEntity.rootParent.entityLabelWithSourceSection});
													}
												});
										}
									}
								},
								items: [
									{
										xtype: 'radiogroup', columns: 1,
										items: [
											{fieldLabel: '', boxLabel: 'View Client Portal As Selected User', name: 'viewAsOption', inputValue: 'USER'},
											{
												xtype: 'combo', anchor: '0 -20', name: 'viewAsUser', mode: 'local', disabled: true, typeAhead: false,
												store: {xtype: 'arraystore-with-filter', data: userArrayResult},
												detailPageClass: 'Clifton.portal.security.user.UserWindow', disableAddNewItem: true
											},


											{fieldLabel: '', boxLabel: 'View Client Portal As Selected Entity', name: 'viewAsOption', inputValue: 'ENTITY', checked: true},
											{
												xtype: 'combo', anchor: '0 -20', name: 'viewAsEntity', mode: 'local', typeAhead: false,
												store: {xtype: 'arraystore-with-filter', data: entityArrayResult},
												detailPageClass: 'Clifton.portal.entity.EntityWindow', disableAddNewItem: true
											}
										],
										listeners: {
											change: function(rg, r) {
												const p = TCG.getParentFormPanel(rg);
												const userField = p.getForm().findField('viewAsUser');
												userField.setDisabled(TCG.isNotEquals('USER', r.inputValue));

												const entityField = p.getForm().findField('viewAsEntity');
												entityField.setDisabled(TCG.isNotEquals('ENTITY', r.inputValue));
											}
										}
									}
								]

							}
						],
						saveForm: function(closeOnSuccess, forms, form, panel, params) {
							const viewAsOption = form.findField('viewAsOption').getGroupValue();
							if (TCG.isEquals('USER', viewAsOption)) {
								const userId = panel.getFormValue('viewAsUser', true, true);
								if (TCG.isNotBlank(userId)) {
									Clifton.portal.ViewClientUIAsUser(userId);
									panel.getWindow().closeWindow();
								}
								else {
									TCG.showError('No User Selected.');
								}
								return;
							}
							else if (TCG.isEquals('ENTITY', viewAsOption)) {
								const entityId = panel.getFormValue('viewAsEntity', true, true);
								if (TCG.isNotBlank(entityId)) {
									Clifton.portal.ViewClientUIAsEntity(entityId);
									panel.getWindow().closeWindow();
								}
								else {
									TCG.showError('No Entity Selected.');
								}
								return;
							}
							TCG.showError('Unable to determine what to view as.  Please check your selections.');
						}
					});
				});
		});
};

Clifton.portal.security.renderPortalSecurityUser = function(val, metaData) {
	if (Ext.isNumber(val)) {
		if (val === 0) {
			return 'System User';
		}
		const user = TCG.data.getData('portalSecurityUser.json?id=' + val, this, 'portal.security.user.' + val);
		if (user && user.label) {
			if (user.username && metaData) {
				metaData.attr = TCG.renderQtip(user.username);
			}
			return user.label;
		}
	}
	return '';
};


Clifton.portal.security.PortalSecurityUserLinkField = Ext.extend(TCG.form.LinkField, {
	// Should be overridden
	fieldLabel: 'Security User',
	name: 'updateUserId',
	detailIdField: 'updateUserId',

	detailPageClass: 'Clifton.portal.security.user.UserWindow',
	submitDetailField: false,


	setValue: function(v) {
		if (TCG.isNumber(v)) {
			const user = TCG.data.getData('portalSecurityUser.json?id=' + v, this, 'portal.security.user.' + v);
			if (user && user.label) {
				v = user.label;
			}
			else {
				v = 'Unknown User';
			}
		}
		Clifton.portal.security.PortalSecurityUserLinkField.superclass.setValue.apply(this, [v]);
	}

});
Ext.reg('portal-security-user-linkfield', Clifton.portal.security.PortalSecurityUserLinkField);

Clifton.portal.DetailWindow = Ext.extend(TCG.app.DetailWindow, {
	getInfoWindowClass: function() {
		return Clifton.portal.InfoWindow;
	}
});

Clifton.portal.InfoWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Info Window',
	iconCls: 'info',
	modal: true,
	allowOpenFromModal: true,
	minimizable: false,
	maximizable: false,
	enableShowInfo: false,
	closeWindowTrail: true,
	width: 770,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel'
				}]
			},
			{
				title: 'Audit Trail',
				items: [{
					xtype: 'portal-gridpanel',
					name: 'portalTrackingAuditTrailEventListForSource',
					appendStandardColumns: false,

					getTableName: function() {
						return this.getWindow().tableName;
					},
					getEntityId: function() {
						return this.getWindow().idFieldValue;
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Type Name', width: 50, dataIndex: 'trackingEventType.name', filter: false},
						{header: 'User', width: 75, dataIndex: 'portalSecurityUser.label', filter: false},
						{header: 'Event Date', width: 50, dataIndex: 'eventDate', filter: false},
						{header: 'Description', width: 150, dataIndex: 'description', renderer: TCG.stripTags, filter: false}
					],
					getLoadParams: function(firstLoad) {
						return {sourceTableName: this.getTableName(), sourceFkFieldId: this.getEntityId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.tracking.EventWindow'
					}
				}]
			},
			{
				title: 'Object Data',
				items: [{
					xtype: 'json-migration-form',
					jsonMigrationUrl: 'portalJsonMigrationAction.json',
					getTableName: function() {
						return this.getWindow().tableName;
					},
					getEntityId: function() {
						return this.getWindow().idFieldValue;
					}
				}]
			}
		]
	}],

	init: function() {
		const d = this.data;
		let f = this.items[0].items[0].items[0];
		f.items = [];
		f = f.items;
		f.push({fieldLabel: 'Window ID', xtype: 'displayfield', value: d.windowId});
		f.push({fieldLabel: 'URL', xtype: 'displayfield', value: d.url});
		// show table info
		let table = '';
		if (d.table) {
			table = d.table;
		}
		else if (d.url) {
			table = d.url.substring(0, d.url.indexOf('.'));
			table = table.charAt(0).toUpperCase() + table.substr(1);
		}
		f.push({fieldLabel: 'Table Name', xtype: 'displayfield', value: table});
		this.tableName = table;

		// show entity info
		if (d.data) {
			f.push({xtype: 'label', html: '<hr />'});
			this.idFieldValue = d.data.id;
			if (d.data.label) {
				f.push({fieldLabel: 'Label', xtype: 'displayfield', value: d.data.label});
			}
			f.push({fieldLabel: 'Created By', xtype: 'displayfield', value: this.getUserLabel(d.data.createUserId)});
			f.push({fieldLabel: 'Create Date', xtype: 'displayfield', value: d.data.createDate, type: 'date'});
			f.push({fieldLabel: 'Updated By', xtype: 'displayfield', value: this.getUserLabel(d.data.updateUserId)});
			f.push({fieldLabel: 'Update Date', xtype: 'displayfield', value: d.data.updateDate, type: 'date'});
		}

		const label = TCG.getValue('data.label', this.data);
		this.title = 'Info Window for ' + table + ' ' + this.idFieldValue + (TCG.isNull(label) ? '' : ' (' + label + ')');
		TCG.app.InfoWindow.superclass.init.apply(this, arguments);
	},

	getUserLabel: function(userId) {
		return Clifton.portal.security.renderPortalSecurityUser(userId);
	}
});

// Changes Standard Columns to include Create User/Update User
Clifton.portal.GridPanel = Ext.extend(TCG.grid.GridPanel, {
	standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
		{
			header: 'Created By', width: 30, dataIndex: 'createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'createDate', hidden: true},
		{
			header: 'Updated By', width: 30, dataIndex: 'updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 40, dataIndex: 'updateDate', hidden: true}
	]
});
Ext.reg('portal-gridpanel', Clifton.portal.GridPanel);

Clifton.portal.FormPanel = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false // Validation Service - No Dao Locator
});
Ext.reg('portal-formpanel', Clifton.portal.FormPanel);

Clifton.portal.file.downloadFiles = function(portalFileIds, zipFiles, componentScope) {
	let url = 'portalFileDownload.json?portalFileId=';
	if (Ext.isArray(portalFileIds)) {
		if (portalFileIds.length > Clifton.portal.file.MaxDownloadCount) {
			TCG.showError('You have selected [' + portalFileIds.length + '] files.  Please limit your selection to ' + Clifton.portal.file.MaxDownloadCount + ' files.', 'Too many files selected');
			return;
		}
		url = 'portalFileListDownload.json?portalFileIds=';
	}
	url += portalFileIds;
	if (TCG.isTrue(zipFiles)) {
		url += '&zipFiles=true';
	}
	TCG.downloadFile(url, null, componentScope);
};

// Note: Overridden in portal-shared for portal admin specific functionality - not used by default
Clifton.portal.GetPortalFileCurrentViewEntity = function() {
	return null;
};

// Note: Overridden in portal-shared for portal admin specific functionality - not used by default
Clifton.portal.GetPortalFileCurrentViewUser = function() {
	return null;
};

// Note: Overridden in portal-shared for portal admin specific functionality - not used by default
Clifton.portal.GetPortalEntityCurrentViewActiveOnly = function() {
	return false;
};


// Base Grid Panel that has edit and toolbar buttons for Viewing Files, Approving files, etc.
// Should add name/url, load params, and column overrides for each use case
Clifton.portal.file.BasePortalFileGrid = Ext.extend(Clifton.portal.GridPanel, {
	rowSelectionModel: 'checkbox',
	showApproveButton: true,
	showUnapproveButton: true,
	showMoveButton: true,
	hideGlobeColumn: false, // Globe Column is used to allow internal users to view Client Portal as a specific user

	initComponent: function() {
		Clifton.portal.file.BasePortalFileGrid.superclass.initComponent.call(this, arguments);

		const cm = this.grid.getColumnModel();
		if (TCG.isTrue(this.hideGlobeColumn)) {
			cm.setHidden(cm.findColumnIndex('View Client Portal'), true);
		}
	},

	additionalPropertiesToRequest: 'id|fileCategory.id|fileCategory.name|postPortalEntity.id|postPortalEntity.entitySourceSection.entityType.securableEntity',
	columns: [
		{header: 'File Group', width: 100, dataIndex: 'fileCategory.nameExpanded', hidden: true},
		{
			header: '', menuHeader: 'Format', width: 30, fixed: true, dataIndex: 'documentFileType', filter: false, align: 'center', tooltip: 'File Format',
			renderer: function(ft, args, r) {
				if (!TCG.isBlank(ft)) {
					return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
				}
			}
		},
		{
			header: '', menuHeader: 'View Client Portal', width: 30, fixed: true, sortable: false, filter: false, tooltip: 'View Client Portal as User with Access to this file or an Entity this File is posted under',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.approved)) {
					return TCG.renderActionColumn('www', '', 'View This File in Client Portal', 'ViewClientUI');
				}
				metaData.attr = TCG.renderQtip('This file is NOT approved and therefore is not visible in the Client Portal');
				return '&nbsp;';
			}
		},
		{header: 'Display Name', width: 100, dataIndex: 'displayName'},
		{header: 'Display Text', width: 100, dataIndex: 'displayText', hidden: true},
		{header: 'Post Entity', width: 100, dataIndex: 'postPortalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'postPortalEntityLabel'}},
		{header: 'Publish Date', width: 30, dataIndex: 'publishDate'},
		{header: 'Fk Field Id', width: 45, dataIndex: 'sourceFkFieldId', hidden: true},
		{header: 'Table Name', width: 45, dataIndex: 'entitySourceSection.sourceSystemTableName', hidden: true},
		{header: 'Report Date', width: 30, dataIndex: 'reportDate'},
		{header: 'Approved', width: 30, dataIndex: 'approved', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
		{
			header: 'Approved By', width: 30, dataIndex: 'approvedByUserId', filter: {type: 'combo', searchFieldName: 'approvedByUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Approved On', width: 45, dataIndex: 'approvalDate', hidden: true},
		{header: 'File Name', width: 100, dataIndex: 'fileName', hidden: true, filter: {searchFieldName: 'fileNameAndPath'}},
		{header: 'File Name and Path', width: 300, dataIndex: 'fileNameAndPath', hidden: true},
		{
			header: 'File Created By', width: 40, dataIndex: 'coalesceFileCreateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'coalesceFileCreateUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		}
	],

	editor: {
		addEnabled: false,
		detailPageClass: 'Clifton.portal.file.FileWindow',
		deleteEnabled: true,
		deleteURL: 'portalFileDelete.json',
		allowToDeleteMultiple: true
	},

	addFirstToolbarButtons: function(toolbar, gridPanel) {
		if (TCG.isNotBlank(this.groupField)) {
			toolbar.add({
				iconCls: 'expand-all',
				tooltip: 'Expand or Collapse all Groups',
				scope: gridPanel.grid,
				handler: function() {
					this.expanded = (this.expanded === undefined) ? false : !this.expanded;
					this.view.toggleAllGroups(this.expanded);
				}
			});
			toolbar.add('-');
		}
	},

	addToolbarButtons: function(toolbar, gridPanel) {
		toolbar.add({
			text: 'View File',
			tooltip: 'Download Selected Files (Multiple selections applies to PDF files only)',
			iconCls: 'view',
			handler: function() {
				gridPanel.downloadSelectedFiles();
			}
		});
		toolbar.add('-');
		if (TCG.isTrue(this.showApproveButton)) {
			toolbar.add({
				text: 'Approve',
				tooltip: 'Approve Selected Files',
				iconCls: 'row_reconciled',
				handler: function() {
					gridPanel.approveSelectedFiles(true);
				}
			});
			toolbar.add('-');
		}
		if (TCG.isTrue(this.showUnapproveButton)) {
			toolbar.add({
				text: 'Un-Approve',
				tooltip: 'Un-Approve Selected Files',
				iconCls: 'cancel',
				handler: function() {
					gridPanel.approveSelectedFiles(false);
				}
			});
			toolbar.add('-');
		}
		if (TCG.isTrue(this.showMoveButton)) {
			toolbar.add({
				text: 'Move',
				tooltip: 'Move Selected Files to a different file category.  If more than one file is selected they should all currently belong to the same file category.',
				iconCls: 'move',
				handler: function() {
					gridPanel.moveSelectedFiles();
				}
			});
			toolbar.add('-');
		}

		this.addAdditionalToolbarButtons(toolbar, gridPanel);
	},

	addAdditionalToolbarButtons: function(toolbar, gridPanel) {
		// OVERRIDE IF NEEDED
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (TCG.isEquals('ViewFile', eventName)) {
						Clifton.portal.file.downloadFiles(row.json.id, null, grid);
					}
					if (TCG.isEquals('ViewClientUI', eventName)) {
						let entityId = undefined;
						if (TCG.isNotNull(row.json.postPortalEntity)) {
							if (TCG.isTrue(TCG.getValue('postPortalEntity.entitySourceSection.entityType.securableEntity', row.json))) {
								entityId = row.json.postPortalEntity.id;
							}
						}
						Clifton.portal.ViewClientUIForFile(row.json.id, entityId);
					}
				}
			}
		}
	},

	approveSelectedFiles: function(approve) {
		const gridPanel = this;
		const sm = gridPanel.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a row.', 'No Row(s) Selected');
		}
		else {
			const portalFiles = sm.getSelections();
			const portalFileIds = [];
			for (let i = 0; i < portalFiles.length; i++) {
				const portalFile = portalFiles[i];
				portalFileIds.push(portalFile.json.id);
			}
			const loader = new TCG.data.JsonLoader({
				waitTarget: gridPanel,
				waitMsg: approve ? 'Approving...' : 'Un-Approving',
				params: {
					portalFileIds: portalFileIds,
					approve: approve
				},
				onLoad: function(data, conf) {
					gridPanel.reload();
				}
			});
			loader.load('portalFileListApprove.json');
		}
	},

	moveSelectedFiles: function() {
		const gridPanel = this;
		const sm = gridPanel.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a row.', 'No Row(s) Selected');
		}
		else {
			const portalFiles = sm.getSelections();
			const portalFileIds = [];
			const currentFileCategoryId = portalFiles[0].json.fileCategory.id;
			const currentFileCategoryName = portalFiles[0].json.fileCategory.name;
			for (let i = 0; i < portalFiles.length; i++) {
				const portalFile = portalFiles[i];
				if (TCG.isNotEquals(currentFileCategoryId, portalFile.json.fileCategory.id)) {
					TCG.showError('Different current category selections found [' + currentFileCategoryName + ', ' + portalFile.json.fileCategory.name + '].  To move multiple files, please have them all come from the same original file category.');
					return;
				}
				portalFileIds.push(portalFile.json.id);
			}

			TCG.data.getDataPromise('portalFileCategory.json?id=' + currentFileCategoryId, this)
				.then(function(currentFileCategory) {
						TCG.createComponent('Clifton.portal.file.MovePortalFileWindow', {
							defaultData: {
								portalFileIds: portalFileIds,
								currentFileCategory: currentFileCategory
							},
							openerCt: gridPanel
						});
					}
				);
		}
	},

	downloadSelectedFiles: function() {
		const grid = this.grid;
		const sm = grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a file to be downloaded.', 'No Row(s) Selected');
		}
		else if (sm.getCount() === 1) {
			const fileType = sm.getSelected().json.documentFileType;
			if (TCG.isBlank(fileType)) {
				TCG.showError('There is no file associated with selected row to download.', 'No File Available');
			}
			else {
				Clifton.portal.file.downloadFiles(sm.getSelected().json.id, null, this);
			}
		}
		else {
			const portalFiles = sm.getSelections();
			const downloadFileIds = [];
			let skipFiles = 0;
			for (let i = 0; i < portalFiles.length; i++) {
				const portalFile = portalFiles[i];
				const fileType = portalFile.json.documentFileType;
				if (TCG.isBlank(fileType) || fileType !== 'pdf') {
					skipFiles++;
					this.getRowSelectionModel().deselectRow(i);
				}
				else {
					downloadFileIds.push(portalFile.json.portalFileId);
				}
			}
			if (skipFiles > 0) {
				if (downloadFileIds.length === 0) {
					TCG.showError('All rows selected either have no file associated with them or they are not PDF files.  Only PDF files can be downloaded together.  Otherwise you must download the file individually.  Skipped Rows were de-selected for you.', 'Skipped Files');
				}
				else {
					TCG.showError(skipFiles + ' rows selected either have no file associated with them or they are not PDF files.  Only PDF files can be downloaded together.  Otherwise you must download the file individually.  Skipped Rows were de-selected for you.', 'Skipped Files');
				}
			}
			if (downloadFileIds.length > 0) {
				Clifton.portal.file.downloadFiles(downloadFileIds, null, this);
			}
		}
	}
});
Ext.reg('portal-files-base-grid', Clifton.portal.file.BasePortalFileGrid);


Clifton.portal.file.PortalFileGridForSourceEntity = Ext.extend(Clifton.portal.file.BasePortalFileGrid, {
	name: 'portalFileListForSourceEntity',
	sourceSystemName: 'IMS', // Note: setting to ims as that is the current only use case
	sourceTableName: 'REPLACE_ME', // Replace with the source table name, i.e. PortfolioRun, AccountingPeriodClosing

	getSourceTableName: function() {
		return this.sourceTableName;
	},

	getSourceFkFieldId: function() {
		// Replace if necessary, but usually will be the main form id of the window since it's a tab on that window
		return this.getWindow().getMainFormId();
	},

	groupField: 'fileCategory.nameExpanded',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

	columnOverrides: [
		{dataIndex: 'coalesceFileCreateUserId', hidden: false},
		{dataIndex: 'updateDate', hidden: false},
		{dataIndex: 'reportDate', hidden: true},
		{dataIndex: 'publishDate', hidden: true},
		{dataIndex: 'postPortalEntity.entityLabelWithSourceSection', hidden: true}
	],

	getLoadParams: function(firstLoad) {
		return {
			sourceSystemName: this.sourceSystemName,
			sourceTableName: this.getSourceTableName(),
			sourceFkFieldId: this.getSourceFkFieldId()
		};
	}
});
Ext.reg('portal-files-grid-for-source-entity', Clifton.portal.file.PortalFileGridForSourceEntity);

Clifton.portal.file.MovePortalFileWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Move Portal File(s)',
	iconCls: 'move',
	height: 175,
	modal: true,
	allowOpenFromModal: true,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		instructions: 'Select a new file category to move selected file to.',
		items: [
			{fieldLabel: 'From Category', xtype: 'linkfield', name: 'currentFileCategory.name', detailIdField: 'currentFileCategory.id', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow'},
			{xtype: 'hidden', name: 'currentFileCategory.postEntityType.id'},
			{
				fieldLabel: 'To Category', name: 'portalFileCategoryName', hiddenName: 'portalFileCategoryId', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json', allowBlank: false, detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow',
				beforequery: function(queryEvent) {
					const postEntityTypeId = queryEvent.combo.getParentForm().getFormValue('currentFileCategory.postEntityType.id');
					if (!TCG.isBlank(postEntityTypeId)) {
						queryEvent.combo.store.setBaseParam('postEntityTypeId', postEntityTypeId);
					}
				}
			}
		],
		getSaveURL: function() {
			return 'portalFileListMove.json';
		},
		getSubmitParams: function() {
			const data = this.getWindow().defaultData;
			return {portalFileIds: data.portalFileIds};
		}
	}]
});


Clifton.portal.file.BasePortalFileExtendedGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portalFileExtendedListFind',
	remoteSort: true,
	instructions: 'If no report date filters are present, displays files that are active today.',

	getCategoryName: function() {
		return 'OVERRIDE_ME';
	},

	getApprovedFilesOnly: function() {
		return true;
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Report Date from', xtype: 'toolbar-datefield', name: 'reportDateStart', allowBlank: true, qtip: 'If no report date filters are present, displays files that are active today.'},
			{fieldLabel: 'to', labelSeparator: '', xtype: 'toolbar-datefield', name: 'reportDateEnd', allowBlank: true}
		];
	},

	getCategoryParams: function() {
		// Note: This is overridden by the tree grid/folder navigation
		return {categoryName: this.getCategoryName()};
	},

	getLoadParams: function(firstLoad) {
		const params = this.getCategoryParams();
		const t = this.getTopToolbar();
		let startDateValue;
		let endDateValue = null;
		const start = TCG.getChildByName(t, 'reportDateStart');
		const end = TCG.getChildByName(t, 'reportDateEnd');
		let defaultDisplay = null;

		if (TCG.isNotBlank(start.getValue()) || TCG.isNotBlank(end.getValue())) {
			startDateValue = TCG.isNotBlank(start.getValue()) ? (start.getValue()).format('m/d/Y') : null;
			endDateValue = TCG.isNotBlank(end.getValue()) ? (end.getValue()).format('m/d/Y') : null;
		}
		else {
			defaultDisplay = true;
		}

		if (startDateValue && (endDateValue < startDateValue)) {
			endDateValue = null;
			end.setValue('');
		}

		params.assignedPortalEntityId = Clifton.portal.GetPortalFileCurrentViewEntity();
		if (TCG.isTrue(Clifton.portal.GetPortalEntityCurrentViewActiveOnly())) {
			params.activeAssignedPortalEntity = true;
		}
		params.viewAsUserId = Clifton.portal.GetPortalFileCurrentViewUser();
		params.minReportDate = startDateValue;
		params.maxReportDate = endDateValue;
		params.defaultDisplay = defaultDisplay;
		params.orderBy = 'fileCategoryOrderExpanded:ASC#assignedPortalEntityName:ASC#reportDate:DESC';

		if (TCG.isTrue(this.getApprovedFilesOnly())) {
			params.approved = true;
		}
		return this.getAdditionalLoadParams(firstLoad, params);
	},

	// Can be overridden to apply additional parameters
	getAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},

	groupField: 'fileCategoryWithAssignmentLabel',
	groupTextTpl: '{values.group}',
	rowSelectionModel: 'checkbox',

	viewConfig: {
		processEvent: function(name, e) {
			if (name === 'mousedown' && e && e.target && e.target.getAttribute('class') === 'email') {
				const fileCategoryId = e.target.getAttribute('categoryId');
				const portalEntityId = e.target.getAttribute('assignedPortalEntityId');
				TCG.data.getDataPromise('portalEmailForCategoryAndEntity.json?categoryId=' + fileCategoryId + '&portalEntityId=' + portalEntityId)
					.then(function(portalEmail) {
						TCG.mailto(portalEmail.toAddress, portalEmail.ccAddress, null, portalEmail.subject, null);
					});
			}
			else {
				Ext.grid.GroupingView.prototype.processEvent.call(this, name, e);
			}
		}
	},

	additionalPropertiesToRequest: 'fileCategory.id|assignedPortalEntity.id',
	columns: [
		{header: 'UUID', dataIndex: 'uuid', width: 30, hidden: true, skipExport: true},
		{header: 'PortalFileID', width: 12, dataIndex: 'portalFileId', hidden: true},
		{header: 'Category Sub-Set', width: 100, dataIndex: 'fileCategory.parent.name', hidden: true},
		{header: 'File Group', width: 100, dataIndex: 'fileCategory.name', hidden: true},
		{header: 'Service', width: 100, dataIndex: 'serviceName', hidden: true},
		{header: 'Assigned Entity', width: 100, dataIndex: 'assignedPortalEntity.label', hidden: true},

		{
			header: 'Group Label', width: 100, dataIndex: 'fileCategoryWithAssignmentLabel', hidden: true, skipExport: true,
			groupRenderer: function(v, unused, r, rowIndex, colIndex, ds) {
				let value = '';
				if (!TCG.isBlank(r.data['assignedPortalEntity.label'])) {
					value = r.data['assignedPortalEntity.label'] + ': ';
				}
				value = value + '<span style="font-weight: normal;" >' + r.data['fileCategory.name'] + '</span>';
				if (!TCG.isBlank(r.data['serviceName'])) {
					value += '<span style="font-weight: normal; color: green;" > (' + r.data['serviceName'] + ')</span>';
				}
				return '<span class="action-column"><span style="width: 16px; height: 16px; float: left;" class="email" categoryId="' + r.json.fileCategory.id + '" assignedPortalEntityId="' + (TCG.isNull(r.json.assignedPortalEntity) ? '' : r.json.assignedPortalEntity.id) + '" ext:qtip = "Questions? Email Us">&nbsp;</span>&nbsp;' + value + '</span>';
			}
		},
		{
			header: 'Format', width: 30, dataIndex: 'documentFileType', filter: false, align: 'center',
			renderer: function(ft, args, r) {
				if (!TCG.isBlank(ft)) {
					return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
				}
			}
		},
		{header: '&nbsp;', width: 300, dataIndex: 'displayName', renderer: TCG.renderText, exportHeader: 'File Label'},
		{header: 'Display Text', width: 50, dataIndex: 'displayText', renderer: TCG.renderText, hidden: true},
		{header: 'Report Date', width: 45, dataIndex: 'reportDate', searchFieldName: 'reportDate'},
		{header: 'Publish Date', width: 45, dataIndex: 'publishDate', searchFieldName: 'publishDate'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.portal.file.FileWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.portalFileId;
		}
	},

	addFirstToolbarButtons: function(toolbar, gridPanel) {
		if (TCG.isNotBlank(this.groupField)) {
			toolbar.add({
				iconCls: 'expand-all',
				tooltip: 'Expand or Collapse all Groups',
				scope: gridPanel.grid,
				handler: function() {
					this.expanded = (this.expanded === undefined) ? false : !this.expanded;
					this.view.toggleAllGroups(this.expanded);
				}
			});
			toolbar.add('-');
		}
	},
	addToolbarButtons: function(toolbar, gridPanel) {
		const w = gridPanel.getWindow();
		if (!TCG.isTrue(w.hideViewFileButton)) {
			toolbar.add({
				text: 'View File',
				tooltip: 'Download Selected File',
				iconCls: 'view',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a file to be downloaded.', 'No Row Selected');
					}
					else if (sm.getCount() > 1) {
						TCG.showError('Please select only one file to be downloaded. If you would like to download multiple files, please use the PDF or Zip Download options.', 'Multiple Rows Selected');
					}
					else {
						gridPanel.downloadSelectedFiles();
					}
				}
			});
			toolbar.add('-');
			toolbar.add({
				text: 'View PDF',
				tooltip: 'Create one PDF file as a concatenation of the selected PDF files.',
				iconCls: 'pdf',
				handler: function() {
					gridPanel.downloadSelectedFiles();
				}
			});
			toolbar.add('-');
			toolbar.add({
				text: 'View ZIP',
				tooltip: 'Create one ZIP file containing all of the selected files.',
				iconCls: 'zip',
				handler: function() {
					gridPanel.downloadSelectedFiles(true);
				}
			});
			toolbar.add('-');
		}
		if (!TCG.isTrue(gridPanel.getApprovedFilesOnly())) {
			toolbar.add({
				text: 'Approve',
				tooltip: 'Approve Selected Files',
				iconCls: 'row_reconciled',
				handler: function() {
					gridPanel.approveSelectedFiles(true);
				}
			});
			toolbar.add('-');
			toolbar.add({
				text: 'Un-Approve',
				tooltip: 'Un-Approve Selected Files',
				iconCls: 'cancel',
				handler: function() {
					gridPanel.approveSelectedFiles(false);
				}
			});
			toolbar.add('-');
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (eventName === 'ViewFile') {
						Clifton.portal.file.downloadFiles(row.json.portalFileId, null, grid);
					}
					if (TCG.isEquals('ViewClientUI', eventName)) {
						if (TCG.isNotNull(row.json.assignedPortalEntity)) {
							Clifton.portal.ViewClientUIForFile(row.json.portalFileId, row.json.assignedPortalEntity.id);
						}
						else {
							// Global File wouldn't have an assignment, so get what was used to load the grid if possible
							Promise.resolve(grid.ownerGridPanel.getLoadParams())
								.then(function(params) {
									Clifton.portal.ViewClientUIForFile(row.json.portalFileId, params.assignedPortalEntityId);
								});
						}
					}
				}
			}
		}
	},

	approveSelectedFiles: function(approve) {
		const gridPanel = this;
		const sm = gridPanel.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a row.', 'No Row(s) Selected');
		}
		else {
			const portalFiles = sm.getSelections();
			const portalFileIds = [];
			for (let i = 0; i < portalFiles.length; i++) {
				const portalFile = portalFiles[i];
				portalFileIds.push(portalFile.json.portalFileId);
			}
			const loader = new TCG.data.JsonLoader({
				waitTarget: gridPanel,
				waitMsg: approve ? 'Approving...' : 'Un-Approving',
				params: {
					portalFileIds: portalFileIds,
					approve: approve
				},
				onLoad: function(data, conf) {
					gridPanel.reload();
				}
			});
			loader.load('portalFileListApprove.json');
		}
	},

	downloadSelectedFiles: function(zipFiles) {
		const grid = this.grid;
		const sm = grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a file to be downloaded.', 'No Row(s) Selected');
		}
		else if (sm.getCount() === 1) {
			const fileType = sm.getSelected().json.documentFileType;
			if (TCG.isBlank(fileType)) {
				TCG.showError('There is no file associated with selected row to download.', 'No File Available');
			}
			else {
				Clifton.portal.file.downloadFiles(sm.getSelected().json.portalFileId, null, this);
			}
		}
		else {
			const portalFiles = sm.getSelections();
			const downloadFileIds = [];
			let skipFiles = 0;
			for (let i = 0; i < portalFiles.length; i++) {
				const portalFile = portalFiles[i];
				const fileType = portalFile.json.documentFileType;
				if (TCG.isBlank(fileType) || (!TCG.isTrue(zipFiles) && (fileType !== 'pdf'))) {
					skipFiles++;
					this.getRowSelectionModel().deselectRow(i);
				}
				else {
					downloadFileIds.push(portalFile.json.portalFileId);
				}
			}
			if (skipFiles > 0) {
				if (downloadFileIds.length === 0) {
					if (TCG.isTrue(zipFiles)) {
						TCG.showError('All rows selected no file associated with them.  Skipped Rows were de-selected for you.', 'Skipped Files');
					}
					else {
						TCG.showError('All rows selected either have no file associated with them or they are not PDF files.  Only PDF files can be concatenated together.  Otherwise you must download the file individually or use the Zip option.  Skipped Rows were de-selected for you.', 'Skipped Files');
					}
				}
				else {
					if (TCG.isTrue(zipFiles)) {
						TCG.showError(skipFiles + ' rows selected have no file associated with them.  Skipped Rows were de-selected for you.', 'Skipped Files');
					}
					else {
						TCG.showError(skipFiles + ' rows selected either have no file associated with them or they are not PDF files.  Only PDF files can be concatenated together.  Otherwise you must download the file individually or use the Zip option.  Skipped Rows were de-selected for you.', 'Skipped Files');
					}
				}
			}
			if (downloadFileIds.length > 0) {
				Clifton.portal.file.downloadFiles(downloadFileIds, zipFiles, null, this);
			}
		}
	},


	getRowSelectionModel: function() {
		if (typeof this.rowSelectionModel !== 'object') {
			let config;
			if (TCG.isTrue(this.getApprovedFilesOnly())) {
				config = Ext.apply(
					{
						renderer: function(v, p, record) {
							if (TCG.isNotBlank(record.data.documentFileType)) {
								return '<div class="x-grid3-row-checker">&#160;</div>';
							}
							else {
								return '';
							}
						},
						header: '<div class="x-grid3-hd-checker" qtip="Select/Deselect All">&#160;</div>'
					},
					this.getRowSelectionModelOverrideConfig());
			}
			else {
				config = this.getRowSelectionModelOverrideConfig();
			}
			this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel(config);
		}
		return this.rowSelectionModel;
	}
});
Ext.reg('portal-files-extended-base-grid', Clifton.portal.file.BasePortalFileExtendedGrid);


Clifton.portal.security.user.UserAssignmentListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portalSecurityUserAssignmentListFind',
	xtype: 'portal-gridpanel',
	additionalPropertiesToRequest: 'id|securityUser.id|portalEntity.endDate',

	topToolbarSearchParameter: 'userSearchPattern',
	groupField: 'portalEntity.entityLabelWithSourceSection',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Users" : "User"]})',

	viewNames: ['Default', 'Grouped By Portal Entity'],
	defaultViewName: 'Default', // Need to start grouped so we get the grouping store but switch to un-grouped immediately
	listeners: {
		afterrender: function() {
			this.switchToView(this.defaultViewName);
		}
	},
	switchToViewBeforeReload: function(viewName) {
		const portalEntityProperty = 'portalEntity.entityLabelWithSourceSection';
		if (viewName === 'Default') {
			this.grid.getStore().clearGrouping();
		}
		else {
			this.grid.getStore().groupBy(portalEntityProperty, false);
			this.ds.setDefaultSort(portalEntityProperty, 'asc');
		}
	},

	assignmentColumns: [
		{header: 'View Type', width: 75, hidden: true, dataIndex: 'portalEntity.entityViewType.name', filter: {searchFieldName: 'entityViewTypeId', type: 'combo', url: 'portalEntityViewTypeListFind.json'}},
		{
			header: 'Portal Entity', width: 250, hidden: true, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityLabelExpanded'}, defaultSortColumn: true, viewNames: ['Default'],
			renderer: function(value, metaData, record) {
				const pendingTermination = record.get('pendingTermination');
				if (TCG.isTrue(pendingTermination)) {
					metaData.css += 'x-grid3-row-highlight-light-red';
					metaData.attr = TCG.renderQtip('Pending Termination: Assignment will be disabled on ' + TCG.parseDate(TCG.getValue('portalEntity.endDate', record.json)).format('m/d/Y'));
				}
				return value;
			}
		},
		{header: 'User', width: 125, dataIndex: 'securityUser.label', filter: {searchFieldName: 'userSearchPattern'}},
		{header: 'Assignment Role', width: 100, dataIndex: 'assignmentRole.name', filter: {type: 'combo', url: 'portalSecurityUserRoleListFind.json?portalEntitySpecific=true', searchFieldName: 'assignmentRoleId'}},
		{header: 'User name', hidden: true, width: 150, dataIndex: 'securityUser.username', filter: {searchFieldName: 'username'}},
		{header: 'E-mail', width: 150, dataIndex: 'securityUser.emailAddress', filter: {searchFieldName: 'emailAddress'}},
		{header: 'Company', hidden: true, width: 125, dataIndex: 'securityUser.companyName', filter: {searchFieldName: 'userCompanyName'}},
		{header: 'Title', hidden: true, width: 125, dataIndex: 'securityUser.title', filter: {searchFieldName: 'userTitle'}},
		{header: 'Phone Number', hidden: true, width: 75, dataIndex: 'securityUser.phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
		{header: 'Terms Accepted', width: 60, type: 'boolean', dataIndex: 'securityUser.termsOfUseAccepted', filter: {searchFieldName: 'termsOfUseAccepted'}},
		{header: 'Terms Accepted On', width: 50, dataIndex: 'securityUser.termsOfUseAcceptanceDate', hidden: true, filter: {searchFieldName: 'termsOfUseAcceptanceDate'}},
		{header: 'Pending Termination', width: 60, dataIndex: 'pendingTermination', type: 'boolean', hidden: true},
		{header: 'Disabled', width: 70, dataIndex: 'disabled', type: 'boolean'},
		{header: 'Disabled Date', width: 75, dataIndex: 'disabledDate', hidden: true},
		{header: 'Disabled Reason', width: 75, dataIndex: 'disabledReason', hidden: true}
	],

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'View Type', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json', linkedFilter: 'portalEntity.entityViewType.name'},
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},

	editor: {
		detailPageClass: 'Clifton.portal.security.user.UserAssignmentWindow',
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			toolBar.add({
				text: 'Add User',
				tooltip: 'Add a New User',
				iconCls: 'add',
				handler: function() {
					Ext.Msg.prompt('User Name', 'Please enter the user name (e-mail address) for the user you would like to add:', function(btn, text) {
							if (btn === 'ok') {
								if (TCG.isBlank(text)) {
									TCG.showError('A user name (e-mail address) is required.', 'No Name Entered');
									return;
								}
								const params = {username: text, validateEmail: true};
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Finding User...',
									params: params,
									conf: params,
									onLoad: function(record, conf) {
										if (record && !TCG.isTrue(record.newBean)) {
											TCG.createComponent('Clifton.portal.security.user.UserAssignmentWizardWindow', {
												defaultData: {securityUser: record},
												openerCt: gridPanel
											});
										}
										else {
											Ext.Msg.confirm('User Not Found', 'User with username/e-mail ' + text + ' does not exist.  Would you like to add them as a new user to the Client Portal?', function(a) {
												if (a === 'yes') {
													TCG.createComponent('Clifton.portal.security.user.UserWizardWindow', {
														defaultData: {username: text},
														openerCt: gridPanel
													});
												}
											});

										}
									}
								});
								loader.load('portalSecurityUserByUserName.json');
							}
						}, this // scope
					);
				}
			});
			toolBar.add('-');
			toolBar.add({
				text: 'Add Assignment',
				tooltip: 'Add an assignment to Selected User',
				iconCls: 'add',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a user.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multiple selections are not supported.  Please select one user.', 'NOT SUPPORTED');
					}
					TCG.createComponent('Clifton.portal.security.user.UserAssignmentWizardWindow', {
						defaultData: {securityUser: sm.getSelected().json.securityUser},
						openerCt: gridPanel
					});
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('portal-security-user-assignment-list-grid', Clifton.portal.security.user.UserAssignmentListGrid);


Clifton.portal.tracking.TrackingEventListGrid = Ext.extend(TCG.grid.GridPanel, {
	eventTypeName: undefined, // Overridden for tabs that are for a specific event type
	eventTypeNames: undefined, // Can be used instead of eventTypeName to show a list of event types.  (Audit Trail)
	appendStandardColumns: false, // Create/Update fields don't apply
	populateRelationshipManagerNames: false, // Used for File Downloads List to populate RM info for each file

	name: 'portalTrackingEventExtendedListFind',
	xtype: 'portal-gridpanel',
	instructions: 'The following events have been logged in the system.',
	useBufferView: false,

	initComponent: function() {
		const currentColumns = [];
		Ext.each(TCG.clone(this.firstColumns), function(f) {
			currentColumns.push(f);
		});
		Ext.each(TCG.clone(this.additionalColumns), function(f) {
			currentColumns.push(f);
		});
		this.columns = currentColumns;

		Clifton.portal.tracking.TrackingEventListGrid.superclass.initComponent.call(this);

		if (this.eventTypeName) {
			const cm = this.getColumnModel();
			cm.setHidden(cm.findColumnIndex('trackingEventType.name'), true);
		}
	},

	// Common columns
	// Note: Can use Column Overrides to change visibility
	firstColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Event Type Name', width: 50, dataIndex: 'trackingEventType.name', filter: {searchFieldName: 'trackingEventTypeId', type: 'combo', url: 'portalTrackingEventTypeList.json', loadAll: true}},
		{header: 'User', width: 75, dataIndex: 'portalSecurityUser.label', filter: {searchFieldName: 'portalSecurityUserId', type: 'combo', url: 'portalSecurityUserListFind.json', displayField: 'label'}},
		{header: 'Username', width: 75, hidden: true, dataIndex: 'portalSecurityUser.username', filter: {searchFieldName: 'portalSecurityUserName'}},
		{header: 'Company', width: 75, dataIndex: 'portalSecurityUser.companyName', filter: {searchFieldName: 'portalSecurityUserCompanyName'}},
		{header: 'User Title', width: 75, hidden: true, dataIndex: 'portalSecurityUser.title', filter: {searchFieldName: 'portalSecurityUserTitle'}},
		{header: 'User Phone', width: 75, hidden: true, dataIndex: 'portalSecurityUser.phoneNumber', filter: {searchFieldName: 'portalSecurityUserPhoneNumber'}},
		{header: 'City/State', width: 50, hidden: true, dataIndex: 'portalTrackingIpData.cityState', filter: {searchFieldName: 'cityState'}},
		{header: 'Country', width: 50, hidden: true, dataIndex: 'portalTrackingIpData.countryCode', filter: {searchFieldName: 'countryCode'}},
		{header: 'Event Date', width: 50, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Portal Entity', width: 150, hidden: true, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityName'}}
	],

	// Event Type Specific Columns
	additionalColumns: [
		{header: 'Description', width: 150, dataIndex: 'description', renderer: TCG.stripTags}
	],

	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.portal.tracking.EventWindow'
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 100, minListWidth: 100, mode: 'local', value: 'ALL_USERS',
				store: {
					xtype: 'arraystore',
					data: [['ALL_USERS', 'All Users', 'Display events for all users'], ['OUR_USERS', 'Our Users', 'Display events for our (PPA Employee) users only'], ['EXTERNAL_USERS', 'External Users', 'Display events for external users only.']]
				}
			},
			{fieldLabel: 'View Type (External Users Only)', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json'}
		];
	},
	getDefaultEventDateFilter: function() {
		// default to last 7 days of events
		return new Date().add(Date.DAY, -7);
	},
	getLoadParams: function(firstLoad) {
		if (TCG.isTrue(firstLoad)) {
			const defaultDate = this.getDefaultEventDateFilter();
			if (TCG.isNotBlank(defaultDate)) {
				this.setFilterValue('eventDate', {'after': defaultDate});
			}
		}
		const t = this.getTopToolbar();
		const params = {};

		const displayType = TCG.getChildByName(t, 'displayType');
		if (displayType) {
			if (displayType.getValue() === 'OUR_USERS') {
				params.portalSecurityUserRolePortalEntitySpecific = false;
			}
			else if (displayType.getValue() === 'EXTERNAL_USERS') {
				params.portalSecurityUserRolePortalEntitySpecific = true;
			}
		}
		if (this.eventTypeName) {
			params.trackingEventTypeNameEquals = this.eventTypeName;
		}
		else if (this.eventTypeNames) {
			params.trackingEventTypeNames = this.eventTypeNames;
		}
		if (TCG.isTrue(this.populateRelationshipManagerNames)) {
			params.populateRelationshipManagerNames = true;
		}
		const viewType = TCG.getChildByName(t, 'viewTypeId');
		if (viewType) {
			params.portalEntityViewTypeId = viewType.getValue();
		}
		return this.applyAdditionalLoadParams(firstLoad, params);
	},
	applyAdditionalLoadParams: function(firstLoad, params) {
		// Default - Do Nothing - see File Window where we add filter to limit download events to the specific file
		return params;
	}
});
Ext.reg('portal-tracking-event-list-grid', Clifton.portal.tracking.TrackingEventListGrid);


Clifton.portal.SupportTeamToolbarCombo = Ext.extend(TCG.form.StringComboBox, {
	width: 200,
	stringArrayUrl: 'portalSupportTeamEmailList.json',
	listeners: {
		select: function(field) {
			TCG.getParentByClass(field, TCG.grid.GridPanel).reload();
		},
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				TCG.getParentByClass(field, TCG.grid.GridPanel).reload();
			}
		}
	}
});
Ext.reg('portal-support-team-combo', Clifton.portal.SupportTeamToolbarCombo);


Clifton.portal.logging.LoggingAdditionalTabs.push({
		title: 'Portal Loggers',
		items: [{
			xtype: 'log-configuration-grid',
			name: 'portalLogConfigurationList',
			editor: {
				detailPageClass: 'Clifton.core.logging.configuration.LogConfigurationWindow',
				getDetailPageId: function(gridPanel, row) {
					const data = row.data;
					return data.name;
				},
				getDefaultData: function(win) {
					return {urlPrefix: 'portal'};
				},
				deleteUrl: 'portalLogConfigurationDelete.json',
				getDeleteParams: function(selectionModel) {
					const selection = selectionModel.getSelected();
					if (TCG.isBlank(selection)) {
						TCG.showError('You must select a configuration to delete');
					}
					const data = selection.data;
					return {name: data.name};
				},
				getSaveURL: function() {
					return 'portalLogConfigurationSave.json';
				}
			}
		}]
	},
	{
		title: 'Portal Appenders',
		items: [{
			xtype: 'log-appender-grid',
			name: 'portalLogAppenderList',
			editor: {
				detailPageClass: 'Clifton.core.logging.configuration.LogAppenderWindow',
				drillDownOnly: true,
				getDetailPageId: function(gridPanel, row) {
					const data = row.data;
					return data.name;
				},
				getDefaultData: function(win) {
					return {urlPrefix: 'portal'};
				}
			}
		}]
	});

