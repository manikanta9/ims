Clifton.portal.PortalManagementWindow = Ext.extend(TCG.app.Window, {
	id: 'portalManagementWindow',
	title: 'Client Portal Management',
	iconCls: 'www',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Pending Approval Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-base-grid',
						name: 'portalFileListWithEditAccessFind',
						instructions: 'The following lists files under categories that you have access to post to that are pending approval.  Note: You cannot approve files that you created.',
						showUnapproveButton: false,

						groupField: 'fileCategory.nameExpanded',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

						columnOverrides: [{dataIndex: 'fileName', hidden: false}, {dataIndex: 'coalesceFileCreateUserId', hidden: false}, {dataIndex: 'updateDate', hidden: false}, {dataIndex: 'approved', hidden: true}, {dataIndex: 'approvedByUserId', hidden: true}],

						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'Post Entity', name: 'postEntityId', xtype: 'toolbar-combo', width: 250, url: 'portalEntityListFind.json?postableEntity=true&orderBy=labelExpanded', displayField: 'entityLabelWithSourceSection'},
								{fieldLabel: 'Support Team Email', name: 'supportTeamEmail', xtype: 'portal-support-team-combo'},
								{fieldLabel: 'File Category', xtype: 'toolbar-combo', name: 'fileCategoryIdExpanded', width: 200, url: 'portalFileCategoryForUploadListFind.json?includeParents=true', comboDisplayField: 'labelWithLevels'}
							];
						},

						getLoadParams: function(firstLoad) {
							const params = {};
							const supportTeamEmailField = TCG.getChildByName(this.getTopToolbar(), 'supportTeamEmail');
							if (TCG.isTrue(firstLoad)) {
								// NOTE: THESE COME FROM THE FILES PENDING NOTIFICATION THAT GETS SENT OUT AND WE PASS THE PARAMS INCLUDED IN EACH EMAIL HERE
								const windowParams = this.getWindow().params;
								if (TCG.isNotNull(windowParams)) {
									if (TCG.isNotBlank(windowParams.minReportDate)) {
										const minReportDate = Date.parseDate(windowParams.minReportDate, 'm/d/Y');
										this.setFilterValue('reportDate', {'after': minReportDate});
									}
									if (TCG.isNotBlank(windowParams.unapprovedMinCreateDate)) {
										const minCreateDate = Date.parseDate(windowParams.unapprovedMinCreateDate, 'm/d/Y');
										this.setFilterValue('createDate', {'after': minCreateDate});
									}
									if (TCG.isNotBlank(windowParams.supportTeamEmail)) {
										supportTeamEmailField.setValue(windowParams.supportTeamEmail);
									}
									if (TCG.isNotBlank(windowParams.orderBy)) {
										params.orderBy = windowParams.orderBy;
									}
								}
								else {
									// default to last 7 days of files
									const defaultDate = new Date().add(Date.DAY, -7);
									this.setFilterValue('updateDate', {'after': defaultDate});
								}
								// and not approved
								this.setFilterValue('approved', false);
							}

							const fileCategoryIdExpanded = TCG.getChildByName(this.getTopToolbar(), 'fileCategoryIdExpanded');
							if (TCG.isNotBlank(fileCategoryIdExpanded.getValue())) {
								params.fileCategoryIdExpanded = fileCategoryIdExpanded.getValue();
							}
							const postEntityId = TCG.getChildByName(this.getTopToolbar(), 'postEntityId');
							if (TCG.isNotBlank(postEntityId.getValue())) {
								params.postPortalEntityId = postEntityId.getValue();
							}
							if (TCG.isNotBlank(supportTeamEmailField.getValue())) {
								params.supportTeamEmail = supportTeamEmailField.getValue();
							}
							return params;
						}
					}

				]
			},


			{
				title: 'Approved  Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-base-grid',
						name: 'portalFileListWithEditAccessFind',
						instructions: 'The following lists files under categories that you have access to post to that have been approved.',
						showApproveButton: false,

						groupField: 'fileCategory.nameExpanded',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

						columnOverrides: [{dataIndex: 'approved', hidden: true}, {dataIndex: 'approvedByUserId', hidden: false}, {dataIndex: 'approvalDate', hidden: false}],

						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'Post Entity', name: 'postEntityId', xtype: 'toolbar-combo', width: 250, url: 'portalEntityListFind.json?postableEntity=true&orderBy=labelExpanded', displayField: 'entityLabelWithSourceSection'},
								{fieldLabel: 'Support Team Email', name: 'supportTeamEmail', xtype: 'portal-support-team-combo'},
								{fieldLabel: 'File Category', xtype: 'toolbar-combo', name: 'fileCategoryIdExpanded', width: 200, url: 'portalFileCategoryForUploadListFind.json?includeParents=true', comboDisplayField: 'labelWithLevels'}
							];
						},

						getLoadParams: function(firstLoad) {
							if (TCG.isTrue(firstLoad)) {
								// default to last 7 days of files
								const defaultDate = new Date().add(Date.DAY, -7);
								this.setFilterValue('approvalDate', {'after': defaultDate});
								// and not approved
								this.setFilterValue('approved', true);
							}
							const params = {};
							const fileCategoryIdExpanded = TCG.getChildByName(this.getTopToolbar(), 'fileCategoryIdExpanded');
							if (TCG.isNotBlank(fileCategoryIdExpanded.getValue())) {
								params.fileCategoryIdExpanded = fileCategoryIdExpanded.getValue();
							}
							const postEntityId = TCG.getChildByName(this.getTopToolbar(), 'postEntityId');
							if (TCG.isNotBlank(postEntityId.getValue())) {
								params.postPortalEntityId = postEntityId.getValue();
							}
							const supportTeamEmail = TCG.getChildByName(this.getTopToolbar(), 'supportTeamEmail');
							if (TCG.isNotBlank(supportTeamEmail.getValue())) {
								params.supportTeamEmail = supportTeamEmail.getValue();
							}
							return params;
						}
					}

				]
			},


			{
				title: 'All Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-base-grid',
						name: 'portalFileListFind',
						instructions: 'The following list files under any category and their current status.',

						groupField: 'fileCategory.nameExpanded',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

						columnOverrides: [{dataIndex: 'approvedByUserId', hidden: false}, {dataIndex: 'updateDate', hidden: false, defaultSortColumn: true, defaultSortDirection: 'DESC'}],

						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'Post Entity', name: 'postEntityId', xtype: 'toolbar-combo', width: 250, url: 'portalEntityListFind.json?postableEntity=true&orderBy=labelExpanded', displayField: 'entityLabelWithSourceSection'},
								{fieldLabel: '', boxLabel: 'Include Contacts', xtype: 'toolbar-checkbox', name: 'includeContacts', qtip: 'By Default exclude "Files" listed under Contact Us categories'},
								{fieldLabel: 'File Category', xtype: 'toolbar-combo', name: 'fileCategoryIdExpanded', width: 200, url: 'portalFileCategoryListFind.json?includeParents=true', comboDisplayField: 'labelWithLevels'}
							];
						},

						getLoadParams: function(firstLoad) {
							if (TCG.isTrue(firstLoad)) {
								// default to last 7 days of files
								const defaultDate = new Date().add(Date.DAY, -7);
								this.setFilterValue('updateDate', {'after': defaultDate});
							}
							const params = {};
							const fileCategoryIdExpanded = TCG.getChildByName(this.getTopToolbar(), 'fileCategoryIdExpanded');
							if (TCG.isNotBlank(fileCategoryIdExpanded.getValue())) {
								params.fileCategoryIdExpanded = fileCategoryIdExpanded.getValue();
							}
							const postEntityId = TCG.getChildByName(this.getTopToolbar(), 'postEntityId');
							if (TCG.isNotBlank(postEntityId.getValue())) {
								params.postPortalEntityId = postEntityId.getValue();
							}

							const includeContacts = TCG.getChildByName(this.getTopToolbar(), 'includeContacts');
							if (!TCG.isTrue(includeContacts.getValue())) {
								params.excludeCategoryName = 'Contact Us';
							}
							return params;
						}
					}

				]
			},


			{
				title: 'User Assignments',
				items: [
					{
						xtype: 'portal-security-user-assignment-list-grid',
						instructions: 'The following user assignments have been defined in the portal.  If you have permissions to edit users, you can either click Add User to add a new user, or select an existing user to add additional assignments to that user.',
						remoteSort: true,
						// Not sure why, but these are not being inherited correctly in IMS
						standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
							{
								header: 'Created By', width: 30, dataIndex: 'createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
								renderer: Clifton.portal.security.renderPortalSecurityUser,
								exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
							},
							{header: 'Created On', width: 40, dataIndex: 'createDate', hidden: true},
							{
								header: 'Updated By', width: 30, dataIndex: 'updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
								renderer: Clifton.portal.security.renderPortalSecurityUser,
								exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
							},
							{header: 'Updated On', width: 40, dataIndex: 'updateDate', hidden: true}
						],
						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'View Type', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json', linkedFilter: 'portalEntity.entityViewType.name'},
								{fieldLabel: 'Assigned Entity', width: 250, xtype: 'toolbar-combo', name: 'assignedEntity', url: 'portalEntityListFind.json?securableEntity=true&securableEntityHasApprovedFilesPosted=true&active=true&orderBy=labelExpanded', displayField: 'entityLabelWithSourceSection'},
								{fieldLabel: 'User Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},

						getTopToolbarInitialLoadParams: function(firstLoad) {
							if (firstLoad) {
								// default to not disabled
								this.setFilterValue('disabled', false);
							}

							const params = {
								populateResourcePermissionMap: false
							};

							const assignedEntity = TCG.getChildByName(this.getTopToolbar(), 'assignedEntity').getValue();
							if (TCG.isNotBlank(assignedEntity)) {
								params.portalEntityIdOrRelatedEntity = assignedEntity;
							}
							return params;
						},

						initComponent: function() {
							const cols = [];
							Ext.each(this.assignmentColumns, function(f) {
								cols.push(f);
							});
							this.columns = cols;
							Clifton.portal.GridPanel.prototype.initComponent.call(this, arguments);
						},

						columnOverrides: [
							{dataIndex: 'securityUser.companyName', hidden: false},
							{dataIndex: 'securityUser.title', hidden: false},
							{dataIndex: 'securityUser.title', hidden: false}
						]
					}
				]
			},


			{
				title: 'Users',
				items: [
					{
						name: 'portalSecurityUserListFind',
						xtype: 'portal-gridpanel',
						groupField: 'userRole.name',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Users" : "User"]})',
						getTopToolbarFilters: function(toolbar) {
							return [
								{
									fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 100, minListWidth: 100, mode: 'local', value: 'ALL_USERS',
									store: {
										xtype: 'arraystore',
										data: [['ALL_USERS', 'All Users', 'Display events for all users'], ['OUR_USERS', 'Our Users', 'Display events for our (PPA Employee) users only'], ['EXTERNAL_USERS', 'External Users', 'Display events for external users only.']]
									}
								},
								{fieldLabel: 'View Type (External Users Only)', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json'},
								{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},
						topToolbarSearchParameter: 'searchPattern',
						getTopToolbarInitialLoadParams: function(firstLoad) {
							const t = this.getTopToolbar();
							const displayType = TCG.getChildByName(t, 'displayType');

							if (firstLoad) {
								this.setFilterValue('disabled', false);
								displayType.setValue({value: 'EXTERNAL_USERS', text: 'External Users'});
							}

							const params = {};
							if (TCG.isEquals(displayType.getValue(), 'OUR_USERS')) {
								params.userRolePortalEntitySpecific = false;
							}
							else if (TCG.isEquals(displayType.getValue(), 'EXTERNAL_USERS')) {
								params.userRolePortalEntitySpecific = true;
							}

							const viewType = TCG.getChildByName(t, 'viewTypeId');
							params.assignmentPortalEntityViewTypeId = viewType.getValue();
							return params;
						},
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'User Role', width: 50, dataIndex: 'userRole.name', hidden: true, filter: {searchFieldName: 'userRoleId', type: 'combo', url: 'portalSecurityUserRoleListFind.json'}, defaultSortColumn: true},
							{header: 'User Name/E-mail', width: 100, dataIndex: 'username'},
							{header: 'First Name', width: 50, dataIndex: 'firstName'},
							{header: 'Last Name', width: 50, dataIndex: 'lastName'},
							{header: 'Title', width: 50, dataIndex: 'title'},
							{header: 'Company Name', width: 50, dataIndex: 'companyName'},
							{header: 'Phone Number', width: 50, dataIndex: 'phoneNumber', hidden: true},
							{header: 'Source System', width: 35, dataIndex: 'portalEntitySourceSystem.name', filter: {searchFieldName: 'portalEntitySourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
							{header: 'Password Updated On', width: 50, hidden: true, dataIndex: 'passwordUpdateDate'},
							{header: 'Invalid Login Attempts', width: 35, hidden: true, type: 'int', useNull: true, dataIndex: 'invalidLoginAttemptCount'},
							{header: 'Account Locked', width: 35, type: 'boolean', dataIndex: 'accountLocked'},
							{header: 'Password Reset Required', width: 50, type: 'boolean', dataIndex: 'passwordResetRequired'},
							{header: 'Terms Accepted', width: 35, type: 'boolean', dataIndex: 'termsOfUseAccepted', hidden: true},
							{header: 'Terms Accepted On', width: 50, dataIndex: 'termsOfUseAcceptanceDate', hidden: true},
							{header: 'Disabled', width: 35, type: 'boolean', dataIndex: 'disabled'}
						],
						editor: {
							detailPageClass: 'Clifton.portal.security.user.UserWindow',
							drillDownOnly: true
						}
					}
				]
			},


			{
				title: 'Logins',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					name: 'portalTrackingEventListFind',
					instructions: 'The following login events have been logged in the system.',
					eventTypeName: 'Login',
					columnOverrides: [
						{dataIndex: 'portalTrackingIpData.cityState', hidden: false},
						{dataIndex: 'portalTrackingIpData.countryCode', hidden: false}
					],
					additionalColumns: [] // Empty
				}]
			},


			{
				title: 'File Downloads',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					eventTypeName: 'File Download',
					populateRelationshipManagerNames: true,
					instructions: 'The following file download events have been logged in the system.',
					columnOverrides: [
						{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false}
					],
					additionalColumns: [
						{header: 'Relationship Manager(s)', width: 100, dataIndex: 'relationshipManagerNames', nonPersistentField: true},
						{header: 'File Category', width: 150, dataIndex: 'portalFileCategory.nameExpanded', filter: {searchFieldName: 'fileCategoryIdExpanded', type: 'combo', url: 'portalFileCategoryListFind.json', displayField: 'nameExpanded'}},
						{header: 'File', width: 75, dataIndex: 'portalFile.displayName', filter: {searchFieldName: 'fileDisplayName'}},
						{header: 'URL Parameters', width: 75, dataIndex: 'description', renderer: TCG.stripTags}
					]
				}]
			}
		]
	}]
});


