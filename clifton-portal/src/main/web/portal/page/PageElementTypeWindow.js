Clifton.portal.page.PageElementTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Page Element Type',
	iconCls: 'preview',
	height: 400,
	items: [{
		xtype: 'portal-formpanel',
		url: 'portalPageElementType.json',
		instructions: 'The definition for a Page Element placeholder.',
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', xtype: 'textarea', name: 'description'},
			{fieldLabel: 'Element Key', name: 'key', qtip: 'The unique key designator for this element type. This is the key which is used by the UI to determine which placeholder to populate.', readOnly: true},
			{boxLabel: 'Allow multiple elements of this type to apply simultaneously.', name: 'allowMultipleElementsOfType', xtype: 'checkbox', labelSeparator: '', qtip: 'If checked, then the UI supports multiple simultaneous elements of this type. This means that multiple elements can apply to a user at once.', disabled: true},
			{boxLabel: 'Require a global element of this type.', name: 'requireGlobalElement', xtype: 'checkbox', labelSeparator: '', qtip: 'If checked, then at least one global element for this type is required. This is typically enabled when the UI either is unable to function or is incoherent without at least one element of this type.', disabled: true}
		]
	}]
});
