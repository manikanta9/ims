Clifton.portal.page.PageElementWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Page Element',
	iconCls: 'preview',
	height: 400,
	items: [{
		xtype: 'portal-formpanel',
		url: 'portalPageElement.json',
		instructions: 'The value to be used by the UI for a single Page Element Type placeholder when specificity constraints apply to the current user.',
		getFormLabel: function() {
			return `${this.getFormValue('type.name')} (${this.getFormValue('viewType.name') || 'Global'})`;
		},
		items: [
			{fieldLabel: 'Element Type', xtype: 'combo', name: 'type.name', hiddenName: 'type.id', detailPageClass: 'Clifton.portal.page.PageElementTypeWindow', url: 'portalPageElementTypeListFind.json', qtip: 'The Page Element Type for the element. This type defines the page placeholder which this element will populate.'},
			{fieldLabel: 'Entity View Type', xtype: 'combo', name: 'viewType.name', hiddenName: 'viewType.id', detailPageClass: 'Clifton.portal.entity.setup.EntityViewTypeWindow', url: 'portalEntityViewTypeListFind.json', qtip: 'The view type to which this element applies. This element will only apply to users who have assignments to entities with this view type.'},
			{fieldLabel: 'Priority', name: 'priority', xtype: 'numberfield', qtip: 'The priority level for the element. If multiple elements of the same type apply to the same user (e.g., for users with multiple view types), then only the element(s) with the highest priority will be used.'},
			{fieldLabel: 'Order', name: 'order', xtype: 'numberfield', qtip: 'The order for the element. When multiple elements of a single type apply to a user, they will be applied in ascending order. For example, the order may be used to specify the order for a list of email addresses.'},
			{fieldLabel: 'Value', name: 'value', xtype: 'script-textarea', height: 150, qtip: 'The value used for the element. This value may include HTML in many cases.'}
		]
	}]
});
