Clifton.portal.page.PageElementListWindow = Ext.extend(TCG.app.Window, {
	id: 'pageElementList',
	title: 'Page Element Setup',
	iconCls: 'views',
	height: 500,
	items: [{
		xtype: 'tabpanel',
		items: [{
			xtype: 'gridpanel',
			title: 'Page Elements',
			name: 'portalPageElementListFind',
			instructions: 'Page Elements define content within the portal. The element type corresponds to a placeholder on the page whose content will be populated depending the Page Elements assigned to the user. These elements can be made visible to all users (global) or overridden for a specific view type. For example, an email address or a page title may be configured globally or on a per-view-type basis.<br/><br/>In some cases, element types allow multiple elements to be simultaneously visible to the user. When this is the case, elements for all view types that match the user\'s assignments will be made visible. The manner in which they are displayed will depend on how the page is configured for the specific element type. For example,',
			wikiPage: 'IT/Portal+Page+Elements',
			getLoadParams: () => ({viewAsEntityId: Clifton.portal.GetPortalFileCurrentViewEntity(), viewAsUserId: Clifton.portal.GetPortalFileCurrentViewUser()}),
			editor: {detailPageClass: 'Clifton.portal.page.PageElementWindow'},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Element Type', width: 75, dataIndex: 'type.name', defaultSortColumn: true, filter: {type: 'combo', searchFieldName: 'typeId', url: 'portalPageElementTypeListFind.json'}},
				{header: 'Element Key', width: 50, dataIndex: 'type.key', hidden: true, filter: {searchFieldName: 'typeKey'}},
				{header: 'View Type', width: 30, dataIndex: 'viewType.name', filter: {type: 'combo', searchFieldName: 'viewTypeId', url: 'portalEntityViewTypeListFind.json'}},
				{header: 'Priority', width: 25, dataIndex: 'priority', type: 'int', useNull: true, tooltip: 'The priority level for the element. If multiple elements of the same type apply to the same user (e.g., for users with multiple view types), then only the element(s) with the highest priority will be used.'},
				{header: 'Order', width: 25, dataIndex: 'order', type: 'int', useNull: true, tooltip: 'The order for the element. When multiple elements of a single type apply to a user, they will be applied in ascending order.'},
				{header: 'Value', width: 150, dataIndex: 'value', renderer: Ext.util.Format.htmlEncode, tooltip: 'The value used for the element.'}
			]
		}, {
			xtype: 'gridpanel',
			title: 'Page Element Types',
			name: 'portalPageElementTypeListFind',
			instructions: 'Page Element Types define content placeholders within the portal. These placeholders match up with specific sections of the page which have customizable text.',
			editor: {detailPageClass: 'Clifton.portal.page.PageElementTypeWindow', drillDownOnly: true},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
				{header: 'Key', width: 75, dataIndex: 'key'},
				{header: 'Description', width: 120, dataIndex: 'description'},
				{header: 'Allow Multiple', width: 30, dataIndex: 'allowMultipleElementsOfType', type: 'boolean', tooltip: 'If checked, then the UI supports multiple simultaneous elements of this type. This means that multiple elements can apply to a user at once.'},
				{header: 'Global Required', width: 30, dataIndex: 'requireGlobalElement', type: 'boolean', tooltip: 'If checked, then at least one global element for this type is required. This is typically enabled when the UI either is unable to function or is incoherent without at least one element of this type.'}
			]
		}]
	}]
});


