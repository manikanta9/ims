Clifton.portal.file.FileListForSourceEntityWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Portal Files For',
	iconCls: 'www',
	width: 800,
	showMoveButton: false,

	groupField: undefined,

	sourceLabel: 'REPLACE_ME',
	sourceTableName: 'REPLACE_ME',
	sourceFkFieldId: 'REPLACE_ME',

	postUrl: undefined,
	postParams: {}, // Replace if using postUrl
	postButtonText: 'Post',
	postButtonTooltip: 'Post File(s) associated with this entity',


	setTitle: function(title, iconCls) {
		title = this.titlePrefix + ' - ' + this.sourceLabel;
		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	},

	items: [{
		xtype: 'portal-files-grid-for-source-entity',
		groupField: undefined,
		showMoveButton: false,

		addAdditionalToolbarButtons: function(toolbar, gridPanel) {
			if (!TCG.isBlank(gridPanel.getWindow().postUrl)) {
				toolbar.add({
					text: gridPanel.getWindow().postButtonText,
					tooltip: gridPanel.getWindow().postButtonTooltip,
					iconCls: 'www',
					handler: function() {
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							timeout: 120000,
							waitMsg: 'Posting Report<marquee scrollamount="5" direction="right" width="15" scrolldelay="150">...</marquee>',
							params: gridPanel.getWindow().postParams,
							scope: gridPanel,
							success: function(form, action) {
								const result = Ext.decode(form.responseText);
								if (result.success) {
									if (this.isUseWaitMsg()) {
										this.getMsgTarget().mask('Posting Report... Success');
										const unmaskFunction = function() {
											this.getMsgTarget().unmask();
										};
										unmaskFunction.defer(600, this);
									}
									gridPanel.reload();
								}
								else {
									this.onFailure();
									this.getMsgTarget().unmask();
									TCG.data.ErrorHandler.handleFailure(this, result);
								}
							},
							error: function(form, action) {
								Ext.Msg.alert('Failed to post report', action.result.data);
							}
						});
						loader.load(gridPanel.getWindow().postUrl);
					}
				});
				toolbar.add('-');
			}
		},

		getSourceTableName: function() {
			return this.getWindow().sourceTableName;
		},

		getSourceFkFieldId: function() {
			return this.getWindow().sourceFkFieldId;
		}
	}]
});
