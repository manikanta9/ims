Clifton.portal.file.FileReplaceWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Upload Portal file',
	iconCls: 'upload',
	width: 600,
	height: 200,
	items: [{
		xtype: 'portal-formpanel',
		fileUpload: true,
		instructions: 'Use the following screen to manually upload one replacement portal file.',
		getSaveURL: function() {
			return 'portalFileUpload.json?enableValidatingBinding=true';
		},
		items: [
			{name: 'id', xtype: 'hidden'},
			{name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{name: 'fileDuplicateAction', value: 'REPLACE', xtype: 'hidden'}
		]
	}]
});
