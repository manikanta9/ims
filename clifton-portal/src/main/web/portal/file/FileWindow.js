Clifton.portal.file.FileWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal File',
	iconCls: 'disk',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				layout: 'border',
				items: [
					{
						xtype: 'portal-formpanel',
						url: 'portalFile.json',
						defaults: {anchor: '-5'},
						region: 'north',
						labelFieldName: 'displayName',
						flex: 0.49,
						height: 325,
						labelWidth: 180,

						listeners: {
							afterload: function(panel) {
								const f = panel.getForm();

								if (TCG.isBlank(TCG.getValue('sourceFkFieldId', f.formValues))) {
									panel.hideField('entitySourceLabel');
								}
								if (TCG.isTrue(TCG.getValue('globalFile', f.formValues))) {
									panel.hideField('postPortalEntity.label');
									TCG.getChildByName(panel.getWindow(), 'portalFileAssignmentListByFile').setVisible(false);
								}
								else {
									panel.hideField('globalFile');
									panel.hideField('entityViewType.name');
									if (TCG.isBlank('postPortalEntity.label')) {
										panel.hideField('postPortalEntity.label');
									}
								}
								if (!TCG.isBlank(TCG.getValue('postPortalEntity.entitySourceSection.label', f.formValues))) {
									const sourceSectionLabel = TCG.getValue('postPortalEntity.entitySourceSection.label', f.formValues);
									panel.setFieldLabel('postPortalEntity.label', sourceSectionLabel + ':');
								}
								else {
									panel.hideField('postPortalEntity.label');
								}

								panel.setReadOnly(true);
								if (TCG.isBlank(TCG.getValue('fileCategory.displayNameTemplate', f.formValues))) {
									panel.setReadOnlyField('displayName', false);
								}
								panel.setReadOnlyField('reportDate', false);
								panel.setReadOnlyField('publishDate', false);
								panel.setReadOnlyField('fileFrequency', false);

								// If blank set it to the category default
								if (TCG.isBlank(TCG.getValue('fileFrequency', f.formValues))) {
									panel.setFormValue('fileFrequency', TCG.getValue('coalescePortalFileFrequency', f.formValues), true);
								}
							}
						},

						tbar: [
							{
								text: 'View File',
								tooltip: 'Download portal file for viewing',
								iconCls: 'view',
								handler: function() {
									const fp = TCG.getParentFormPanel(this);
									const fileNameAndPath = TCG.getValue('fileNameAndPath', fp.getForm().formValues);
									if (!TCG.isBlank(fileNameAndPath)) {
										TCG.downloadFile('portalFileDownload.json?portalFileId=' + fp.getWindow().getMainFormId(), null, this);
									}
									else {
										TCG.showError('There is no document associated with this portal file.', 'No File');
									}
								}
							},
							'-',
							{
								text: 'Upload File',
								tooltip: 'Edit Portal file to upload a new file',
								iconCls: 'upload',
								handler: function() {
									const fp = TCG.getParentFormPanel(this);
									TCG.createComponent('Clifton.portal.file.FileReplaceWindow', {
										defaultData: fp.getForm().formValues
									});
								}
							}
						],

						items: [
							{fieldLabel: 'File Category', name: 'fileCategory.nameExpanded', detailIdField: 'fileCategory.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow'},

							{fieldLabel: 'Display Name', name: 'displayName'},
							{fieldLabel: 'Display Text', name: 'displayText', grow: true, xtype: 'textarea', height: 35},

							{
								xtype: 'columnpanel',
								columns: [
									{
										config: {columnWidth: 0.33},
										rows: [
											{
												fieldLabel: 'Frequency', name: 'fileFrequency', xtype: 'combo', mode: 'local',
												store: {
													xtype: 'arraystore',
													data: Clifton.portal.file.PortalFileFrequencies
												}
											},
											{fieldLabel: 'File Created By', name: 'fileCreateUserId', detailIdField: 'fileCreateUserId', xtype: 'portal-security-user-linkfield'},
											{fieldLabel: 'File Size (Bytes)', name: 'fileSize', xtype: 'integerfield', submitValue: false}
										]
									},
									{
										config: {columnWidth: 0.33},
										rows: [
											{fieldLabel: 'Report Date', name: 'reportDate', xtype: 'datefield'},
											{fieldLabel: 'Publish Date', name: 'publishDate', xtype: 'datefield', qtip: 'When the file was posted.'},
											{
												fieldLabel: 'Display End Date', name: 'endDate', xtype: 'datefield',
												qtip: 'The end date for this file.  Used to determine when the file is considered active and displayed by default. Users can still search for approved files based on the report date.'
											}
										]
									},
									{
										config: {columnWidth: 0.33},
										rows: [
											{fieldLabel: 'Approved', name: 'approved', xtype: 'checkbox', qtip: 'Only Approved files are available to clients for viewing.'},
											{fieldLabel: 'Approved On', name: 'approvalDate', xtype: 'datefield', qtip: 'When the file was posted.'},
											{fieldLabel: 'Approved By', name: 'approvedByUserId', detailIdField: 'approvedByUserId', xtype: 'portal-security-user-linkfield'}
										]
									}

								]
							},

							{fieldLabel: 'File Name and Path', name: 'fileNameAndPath'},

							{
								fieldLabel: 'Source Entity', name: 'entitySourceLabel', detailIdField: 'sourceFkFieldId', xtype: 'linkfield', detailPageClass: 'Clifton.portal.entity.EntityWindow',
								sourceEntityId: undefined,
								setValue: function(v) {
									const fp = TCG.getParentFormPanel(this);

									const sourceSectionId = TCG.getValue('entitySourceSection.id', fp.getForm().formValues);
									const sourceFkFieldId = TCG.getValue('sourceFkFieldId', fp.getForm().formValues);
									if (TCG.isNotBlank(sourceSectionId) && TCG.isNotBlank(sourceFkFieldId)) {
										const sourceEntity = TCG.data.getData('portalEntityBySourceSection.json?portalEntitySourceSectionId=' + sourceSectionId + '&sourceFkFieldId=' + sourceFkFieldId, this);
										if (sourceEntity) {
											this.sourceEntityId = sourceEntity.id;
											v = sourceEntity.label + ' (' + v + ')';
										}
									}
									TCG.form.LinkField.superclass.setValue.apply(this, [v]);
								},
								getDetailIdFieldValue: function(formPanel) {
									if (this.sourceEntityId) {
										return this.sourceEntityId;
									}
									// Return false - no source entity
									TCG.showError('Source entity ' + TCG.getValue('entitySourceLabel', formPanel.getForm().formValues) + ' is not available.');
									return false;
								}
							},
							{xtype: 'sectionheaderfield', header: 'Post Information'},
							{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Global File (Visible By All Users)', name: 'globalFile', qtip: 'Optionally limited to the selected view type.'},
							{fieldLabel: 'View Type', xtype: 'linkfield', name: 'entityViewType.name', detailIdField: 'entityViewType.id', detailPageClass: 'Clifton.portal.entity.setup.EntityViewTypeWindow'},
							{fieldLabel: 'Post Entity', name: 'postPortalEntity.label', detailIdField: 'postPortalEntity.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.entity.EntityWindow'}
						]
					},
					{flex: 0.02},
					{

						name: 'portalFileAssignmentListByFile',
						xtype: 'gridpanel',
						title: 'File Assignments',
						instructions: 'A file can be posted/assigned to multiple accounts/clients/relationships. There are no file assignments if the file is posted directly to a single account/client/relationship. The file assignments are generated based on the children of the post entity rollup table.',
						flex: 0.49,
						region: 'center',

						getLoadParams: function() {
							return {
								portalFileId: this.getWindow().getMainFormId()
							};
						},
						columns: [
							{header: 'Portal Entity Type', width: 100, dataIndex: 'referenceTwo.entitySourceSection.name'},
							{header: 'Portal Entity', width: 300, dataIndex: 'referenceTwo.label'},
							{header: 'Excluded', width: 50, dataIndex: 'excluded', type: 'boolean'}
						],


						addToolbarButtons: function(toolbar, gridPanel) {
							toolbar.add({
								text: 'Exclude',
								tooltip: 'Exclude Selected File Assignment.  Applies only if File Assignment Exclude column is unchecked.',
								iconCls: 'cancel',
								handler: function() {
									gridPanel.excludeSelectedFile(true);
								}
							});
							toolbar.add('-');

							toolbar.add({
								text: 'Include',
								tooltip: 'Include Selected File Assignment. Applies only if File Assignment Exclude column is checked.',
								iconCls: 'row_reconciled',
								handler: function() {
									gridPanel.excludeSelectedFile(false);
								}
							});
							toolbar.add('-');

						},

						excludeSelectedFile: function(exclude) {
							const gridPanel = this;
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select a row.', 'No Row(s) Selected');
							}
							else if (sm.getCount() > 1) {
								TCG.showError('Multiple selections are not supported.  Please select only one row', 'NOT SUPPORTED');
							}
							else {
								const portalFileAssignment = sm.getSelected().json;
								if (TCG.isEquals(exclude, TCG.getValue('excluded', portalFileAssignment))) {
									TCG.showError('Selected row is already ' + (TCG.isTrue(exclude) ? 'excluded' : 'included') + '. No changes will apply', 'NO CHANGE');
								}
								else {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: exclude ? 'Excluding...' : 'Including',
										params: {
											portalFileAssignmentId: portalFileAssignment.id,
											exclude: exclude
										},
										onLoad: function(data, conf) {
											gridPanel.reload();
										}
									});
									loader.load('portalFileAssignmentExcludeSave.json');
								}
							}
						}
					}
				]
			},


			{
				title: 'User Access',
				items: [
					{
						name: 'portalFileSecurityUserListFind',
						xtype: 'gridpanel',
						instructions: 'The following shows the list of users with access to this file.  For <i>Our Users</i> it shows what users have write access to the file.  For <i>External Users</i> it shows what users have access to download the file (provided the file is approved).',
						getTopToolbarFilters: function(toolbar) {
							return [
								{
									fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 100, minListWidth: 100, mode: 'local', value: 'EXTERNAL_USERS',
									store: {
										xtype: 'arraystore',
										data: [['EXTERNAL_USERS', 'External Users', 'Display external users with read access to this file.'], ['OUR_USERS', 'Our Users', 'Display our users (PPA Employees) with write access to this file']]
									}
								},
								{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},
						topToolbarSearchParameter: 'searchPattern',
						getTopToolbarInitialLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('disabled', false);
							}

							const t = this.getTopToolbar();
							const displayType = TCG.getChildByName(t, 'displayType');
							const params = {};
							params.portalFileId = this.getWindow().getMainFormId();
							if (TCG.isEquals('OUR_USERS', displayType.getValue())) {
								params.userRolePortalEntitySpecific = false;
							}
							else if (TCG.isEquals('EXTERNAL_USERS', displayType.getValue())) {
								params.userRolePortalEntitySpecific = true;
							}
							return params;
						},
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'User Role', width: 50, dataIndex: 'userRole.name', hidden: true, filter: {searchFieldName: 'userRoleId', type: 'combo', url: 'portalSecurityUserRoleListFind.json'}},
							{header: 'User Name/E-mail', width: 100, dataIndex: 'username'},
							{header: 'First Name', width: 50, dataIndex: 'firstName'},
							{header: 'Last Name', width: 50, dataIndex: 'lastName'},
							{header: 'Title', width: 50, dataIndex: 'title'},
							{header: 'Company Name', width: 50, dataIndex: 'companyName'},
							{header: 'Phone Number', width: 50, dataIndex: 'phoneNumber', hidden: true},
							{header: 'Source System', hidden: true, width: 35, dataIndex: 'portalEntitySourceSystem.name', filter: {searchFieldName: 'portalEntitySourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
							{header: 'Password Updated On', width: 50, hidden: true, dataIndex: 'passwordUpdateDate'},
							{header: 'Invalid Login Attempts', width: 35, hidden: true, type: 'int', useNull: true, dataIndex: 'invalidLoginAttemptCount'},
							{header: 'Account Locked', width: 35, type: 'boolean', dataIndex: 'accountLocked'},
							{header: 'Password Reset Required', hidden: true, width: 50, type: 'boolean', dataIndex: 'passwordResetRequired'},
							{header: 'Disabled', width: 35, type: 'boolean', dataIndex: 'disabled'},
							{header: 'Disabled Reason', width: 50, hidden: true, dataIndex: 'disabledReason'}
						],
						editor: {
							detailPageClass: 'Clifton.portal.security.user.UserWindow',
							drillDownOnly: true
						}
					}
				]
			},


			{
				title: 'File Downloads',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					eventTypeName: 'File Download',
					instructions: 'The following file download events have been logged in the system for this file.',
					getDefaultEventDateFilter: function() {
						// No Date Filter
						return '';
					},
					columnOverrides: [
						{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false}
					],
					applyAdditionalLoadParams: function(firstLoad, params) {
						params.portalFileId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			},

			{
				title: 'File Notifications',
				items: [{
					name: 'portalNotificationListFind',
					instructions: 'The following notifications were sent to external users regarding this file.',
					xtype: 'gridpanel',

					columns: [
						{header: 'Definition Name', width: '100', dataIndex: 'notificationDefinition.name', filter: {searchFieldName: 'notificationDefinitionId', type: 'combo', url: 'portalNotificationDefinitionListFind.json'}},
						{header: 'Recipient', width: 100, dataIndex: 'coalesceRecipientLabel', filter: {searchFieldName: 'recipientSearchPattern'}},
						{header: 'Subject', width: 100, dataIndex: 'subject'},
						{header: 'Sent', width: 50, dataIndex: 'sentDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{
							header: 'Error', width: 50, dataIndex: 'error', type: 'boolean',
							renderer: function(v, metaData, r) {
								return TCG.renderBoolean(v, r.data.errorMessage);
							}
						},
						{header: 'Error Message', width: 200, dataIndex: 'errorMessage', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.notification.NotificationWindow'
					},
					getLoadParams: function(firstLoad) {
						return {portalFileId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
