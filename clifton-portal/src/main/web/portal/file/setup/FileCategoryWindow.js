Clifton.portal.file.setup.FileCategoryWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal File Category',
	iconCls: 'folder-open',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 150,
					url: 'portalFileCategory.json',
					instructions: 'File categories define the menu items and layout within each category.  Top level categories are menu items, category sub-sets are used for additional grouping, and then file group categories are where files are actually posted.',

					listeners: {
						afterload: function(panel) {
							const f = panel.getForm();
							const w = panel.getWindow();

							const tabs = w.items.get(0);
							const type = TCG.getValue('fileCategoryType', f.formValues);
							if (type === 'CATEGORY' || type === 'CATEGORY_SUBSET') {
								if (!TCG.getChildByName(tabs, 'childrenTab')) {
									tabs.add(w.childrenTab);
								}
							}
							else {
								if (!TCG.getChildByName(tabs, 'filesTab')) {
									tabs.add(w.filesTab);
								}
								if (TCG.isNotBlank(TCG.getValue('notificationDefinition', f.formValues))) {
									if (!TCG.getChildByName(tabs, 'subscriptionsTab')) {
										tabs.add(w.subscriptionsTab);
									}
								}
							}
							this.updateDisplay(this);
						}
					},
					afterRenderPromise: function(panel) {
						panel.updateDisplay(panel);
					},

					updateDisplay: function(panel) {
						const type = TCG.getValue('fileCategoryType', panel.getForm().formValues);
						if (type === 'CATEGORY') {
							this.hideField('parent.nameExpanded');
							this.hideField('securityResource.name');
							this.hideField('fileType.name');
							this.hideField('categoryEmailAddress');
							this.hideField('approvalRequired');
							this.hideField('portalFileFrequency');
							this.hideField('postEntityType.name');
							this.hideField('sourceEntityType.name');
							this.hideField('displayNameTemplate');
							this.hideField('fileNameTemplate');
							this.hideField('displayTextTemplate');
							this.hideField('notificationHeader');
							this.hideField('notificationDefinition.name');
							this.hideField('notificationSubscriptionOption');
							this.hideField('defaultDaysToDisplay');
						}
						else if (type === 'CATEGORY_SUBSET') {
							this.hideField('securityResource.name');
							this.hideField('fileType.name');
							this.hideField('approvalRequired');
							this.hideField('portalFileFrequency');
							this.hideField('postEntityType.name');
							this.hideField('sourceEntityType.name');
							this.hideField('displayNameTemplate');
							this.hideField('fileNameTemplate');
							this.hideField('displayTextTemplate');
							this.hideField('notificationHeader');
							this.hideField('notificationDefinition.name');
							this.hideField('notificationSubscriptionOption');
							this.hideField('defaultDaysToDisplay');
						}
					},

					items: [
						{fieldLabel: 'Parent Category', name: 'parent.nameExpanded', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow'},
						{fieldLabel: 'Category Type', name: 'fileCategoryType', readOnly: true},
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: '', boxLabel: 'System Defined (Name Cannot Be Changed)', xtype: 'checkbox', name: 'systemDefined', readOnly: true},
						{fieldLabel: 'Category Order', name: 'categoryOrder', xtype: 'spinnerfield', allowNegative: false},
						{fieldLabel: 'Folder Name', name: 'folderNameExpanded', xtype: 'displayfield', qtip: 'Location where files are stored.  These folders are nested under the posted entity folder.'},
						{fieldLabel: 'Security Resource', name: 'securityResource.name', xtype: 'combo', hiddenName: 'securityResource.id', detailPageClass: 'Clifton.portal.security.resource.ResourceWindow', url: 'portalSecurityResourceListFind.json', loadAll: true},
						{
							fieldLabel: 'Post Entity Type', name: 'postEntityType.name', hiddenName: 'postEntityType.id', xtype: 'combo', url: 'portalEntityTypeListFind.json?securableEntity=true',
							qtip: 'When selected, the post entity of the file must be of the selected type. For cases where we post to a rollup entity, then that entities children must be of the selected type.'
						},
						{fieldLabel: 'Category E-mail Address', name: 'categoryEmailAddress', qtip: 'Internal PPA email that clients should use to contact us regarding questions under this category.  Leave blank to use the parent file category email address. If that is blank as well, will try to determine the investment team email address.'},
						{fieldLabel: '', boxLabel: 'Require File Approval (Leave un-checked to automatically mark files as approved when posted)', xtype: 'checkbox', name: 'approvalRequired', qtip: 'If unchecked, files posted from an outside system (i.e. IMS) will automatically be marked as approved when posted.  These types of files are known to go through the approval process outside of the portal.  ANY file directly added to the portal MUST be approved by a secondary person (not the creator of the file). Un-approved files can ONLY be viewed under the Admin section.'},
						{
							fieldLabel: 'Frequency', name: 'portalFileFrequency', xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.portal.file.PortalFileFrequencies
							},
							qtip: 'Default frequency for files in this category.  When posting files, the frequency can be overridden.  Frequency is used to set report dates consistently (i.e. Monthly always uses the last day of the month) as well as file display names (i.e. Monthly - January 2017)'
						},
						{
							fieldLabel: 'Display Days', name: 'defaultDaysToDisplay', xtype: 'integerfield',
							qtip: 'Calendar days. When left blank, the display days is defined by the file frequency. Daily = 7, Monthly = 105 days, and Quarterly = 395 days. Final documents should have no display days as they are considered active until un-approved and replaced.'
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, height: 35},
						{
							fieldLabel: 'Source Entity Type', name: 'sourceEntityType.name', hiddenName: 'sourceEntityType.id', xtype: 'combo', url: 'portalEntityTypeListFind.json?fileSource=true',
							qtip: 'When selected, the source entity of the file must be of the selected type.  The freemarker template is assumed it needs to reference data from the source entity (i.e. Contacts) in which case the freemarker will be passed the bean under name "sourceEntity".  If the source entity has field values, they are passed in as well, where the field name is the key (spaces replaced with _, i.e. Phone Number would be Phone_Number).'
						},
						{
							fieldLabel: 'Display Name Template', name: 'displayNameTemplate', xtype: 'textarea', grow: true, height: 35,
							qtip: 'Freemarker template used to generate the display name of each file.  The context will contain beans for "file" which is the PortalFile object, and optionally "sourceEntity" (when source entity type is selected).'
						},
						{
							fieldLabel: 'File Name Template', name: 'fileNameTemplate', xtype: 'textarea', grow: true, height: 35,
							qtip: 'Freemarker template used to generate the file name of the file when downloaded.  The context will contain beans for "file" which is the PortalFile object, and optionally "sourceEntity" (when source entity type is selected).  If blank, will use the display name.'
						},
						{
							fieldLabel: 'Display Text Template', name: 'displayTextTemplate', xtype: 'textarea', grow: true, height: 35,
							qtip: 'Freemarker template used to generate the display text of each file.  The context will contain beans for "file" which is the PortalFile object, and optionally "sourceEntity" (when source entity type is selected).'
						},
						{xtype: 'sectionheaderfield', header: 'Notifications', name: 'notificationHeader'},
						{
							fieldLabel: 'Notification', xtype: 'combo', name: 'notificationDefinition.name', hiddenName: 'notificationDefinition.id', url: 'portalNotificationDefinitionListFind.json?fileNotification=true',
							requestedProps: 'definitionType.subscriptionForFileCategory',
							listeners: {
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									if (record.json) {
										fp.setFormValue('notificationDefinition.definitionType.subscriptionForFileCategory', record.json.definitionType.subscriptionForFileCategory);
									}
								}
							}
						},
						{xtype: 'hidden', name: 'notificationDefinition.definitionType.subscriptionForFileCategory', submitValue: false},
						{
							fieldLabel: 'Subscription Option', name: 'notificationSubscriptionOption', xtype: 'combo', mode: 'local', requiredFields: ['notificationDefinition.name', 'notificationDefinition.definitionType.subscriptionForFileCategory'],
							store: {
								xtype: 'arraystore',
								data: Clifton.portal.notification.subscription.SubscriptionOptions
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									if (TCG.isEquals('REQUIRED_ON', newValue)) {
										TCG.showInfo('Note: By changing the subscription option to REQUIRED_ON all existing user subscriptions will be turned on for this file category.', 'User Subscription Updates');
									}
								}
							},
							qtip: 'Default / Requirement for notification subscriptions for this file category.  Required and only allowed to be entered if the selected notification allows subscriptions and supports subscriptions for the file category.'
						}

					]
				}]
			}
		]
	}],

	childrenTab: {
		title: 'Child Categories',
		name: 'childrenTab',
		items: [{
			name: 'portalFileCategoryListFind',
			xtype: 'portal-gridpanel',
			instructions: 'Portal File Categories define the layout of the tabs/menus and each page.  Categories can be assigned to a security resource for restricting access to the files.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Category Type', width: 50, hidden: true, dataIndex: 'fileCategoryType'},
				{header: 'Category Name', width: 100, dataIndex: 'name'},
				{header: 'Description', width: 150, dataIndex: 'description'},
				{header: 'Security Resource', width: 50, dataIndex: 'securityResource.name'},
				{header: 'Order', width: 30, dataIndex: 'categoryOrder', defaultSortColumn: true},
				{header: 'E-mail Address', width: 50, dataIndex: 'categoryEmailAddress'},
				{
					header: 'Notifications', width: 30, dataIndex: 'notificationUsed', type: 'boolean',
					tooltip: 'Users can receive e-mail notifications when new files are posted under this category.'
				},
				{
					header: 'Notification Definition', width: 50, dataIndex: 'notificationDefinition.name', hidden: true, filter: {searchFieldName: 'notificationDefinitionId', url: 'portalNotificationDefinitionListFind.json?subscriptionForFileCategory=true'},
					tooltip: 'Defines what notifications and how often they are sent for this category.'
				}
			],
			getLoadParams: function() {
				const w = this.getWindow();
				return {
					parentId: w.getMainFormId()
				};
			},
			editor: {
				detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow',
				getDefaultData: function(gridPanel) {
					const parent = gridPanel.getWindow().getMainForm().formValues;
					const fileCategoryType = (parent.fileCategoryType === 'CATEGORY' ? 'CATEGORY_SUBSET' : 'FILE_GROUP');
					return {
						parent: parent,
						fileCategoryType: fileCategoryType
					};
				}
			}
		}]
	},


	filesTab: {
		title: 'Portal Files',
		name: 'filesTab',
		items: [{
			name: 'portalFileListFind',
			xtype: 'portal-gridpanel',
			instructions: 'Portal Files associated with selected File Group.',
			useBufferView: false, // allow cell wrapping
			rowSelectionModel: 'checkbox',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{
					header: 'Format', width: 30, dataIndex: 'documentFileType', align: 'center', filter: {searchFieldName: 'fileType'},
					renderer: function(ft, args, r) {
						if (!TCG.isBlank(ft)) {
							return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
						}
					}
				},
				{header: 'Post Entity', width: 100, dataIndex: 'postPortalEntity.label', filter: {searchFieldName: 'postPortalEntityLabel'}},
				{header: 'Display Name', width: 150, dataIndex: 'displayName', renderer: TCG.renderText},
				{header: 'Display Text', width: 150, dataIndex: 'displayText', renderer: TCG.renderText},
				{header: 'Approved', width: 35, dataIndex: 'approved', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
				{header: 'Report Date', width: 45, dataIndex: 'reportDate', searchFieldName: 'reportDate'},
				{header: 'Publish Date', width: 45, dataIndex: 'publishDate', searchFieldName: 'publishDate'}
			],
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// Default to those that are Approved and Published within the last 30 days
					this.setFilterValue('publishDate', {'after': new Date().add(Date.DAY, -30)});
					this.setFilterValue('approved', true);
				}
				const w = this.getWindow();
				return {
					fileCategoryIdExpanded: w.getMainFormId()
				};
			},
			editor: {
				detailPageClass: 'Clifton.portal.file.FileWindow',
				drillDownOnly: true
			},
			gridConfig: {
				listeners: {
					'rowclick': function(grid, rowIndex, evt) {
						if (TCG.isActionColumn(evt.target)) {
							const eventName = TCG.getActionColumnEventName(evt);
							const row = grid.store.data.items[rowIndex];
							if (eventName === 'ViewFile') {
								Clifton.portal.file.downloadFiles(row.json.portalFileId, null, grid);
							}
						}
					}
				}
			},
			addToolbarButtons: function(toolbar, gridPanel) {
				toolbar.add({
					text: 'Preview Display',
					tooltip: 'Preview Display Name and Text using the current display name and text templates.',
					iconCls: 'config',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a file to preview.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection previews are not supported.  Please select one file.', 'NOT SUPPORTED');
						}
						else {
							const portalFile = sm.getSelected().json;
							TCG.data.getDataPromise('portalFileTemplateForPortalFile.json?portalFileId=' + sm.getSelected().id, this)
								.then(function(previewFile) {
									TCG.createComponent('TCG.app.OKCancelWindow', {
										doNotWarnOnCloseModified: true,
										okButtonText: 'Update Portal File',
										okButtonTooltip: 'Save portal file with newly generated display name and text',
										title: 'Preview Display Properties',
										iconCls: 'config',
										height: 400,
										width: 600,
										modal: true,

										items: [{
											xtype: 'formpanel',
											loadValidation: false,
											items: [
												{xtype: 'sectionheaderfield', header: 'Display Name'},
												{fieldLabel: 'Current', xtype: 'displayfield', value: portalFile.displayName},
												{fieldLabel: 'New', xtype: 'displayfield', value: previewFile.displayName},
												{xtype: 'sectionheaderfield', header: 'Display Text'},
												{fieldLabel: 'Current', xtype: 'displayfield', value: portalFile.displayText},
												{fieldLabel: 'New', xtype: 'displayfield', value: previewFile.displayText}
											]
										}],
										saveWindow: function(closeOnSuccess, forceSubmit) {
											gridPanel.updatePortalFileDisplayProperties([sm.getSelected().id]);
											this.closeWindow();
										}
									});
								});
						}
					}
				});
				toolbar.add('-');
				toolbar.add({
					text: 'Update Display',
					tooltip: 'Update Display Name and Text using the current display name and text templates for selected files. Updates all files for the category if no files are selected.',
					iconCls: 'config',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							Ext.Msg.confirm('Update Display Properties', 'Are you sure you want to update display name and text for <b>ALL</b> portal files in this file group?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Updating...',
										params: {fileCategoryId: gridPanel.getWindow().getMainFormId()},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('portalFileTemplatesForPortalFileCategoryGenerate.json');
								}
							});
						}
						else {
							const ids = [];
							const ut = sm.getSelections();

							for (let i = 0; i < ut.length; i++) {
								ids.push(ut[i].json.id);
							}
							gridPanel.updatePortalFileDisplayProperties(ids);

						}
					}
				});
				toolbar.add('-');
			},

			updatePortalFileDisplayProperties: function(portalFileIds) {
				const gridPanel = this;
				const loader = new TCG.data.JsonLoader({
					waitTarget: gridPanel,
					waitMsg: 'Updating...',
					params: {portalFileIds: portalFileIds},
					onLoad: function(record, conf) {
						gridPanel.reload();
					}
				});
				loader.load('portalFileTemplatesForPortalFilesGenerate.json');
			}

		}]
	},

	subscriptionsTab: {
		title: 'Notification Subscriptions',
		name: 'subscriptionsTab',
		items: [{
			name: 'portalNotificationSubscriptionListFind',
			xtype: 'gridpanel',
			topToolbarSearchParameter: 'userSearchPattern',
			additionalPropertiesToRequest: 'id|userAssignment.securityUser.id|portalEntity.id',
			getTopToolbarFilters: function(toolbar) {
				if (this.topToolbarSearchParameter) {
					return [{fieldLabel: 'Recipient Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}];
				}
				return undefined;
			},
			columns: [
				{header: 'User', width: 100, dataIndex: 'userAssignment.securityUser.label', filter: {searchFieldName: 'userSearchPattern'}},
				{header: 'User name', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.username', filter: {searchFieldName: 'username'}},
				{header: 'E-mail', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.emailAddress', filter: {searchFieldName: 'emailAddress'}},
				{header: 'Company', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.companyName', filter: {searchFieldName: 'userCompanyName'}},
				{header: 'Title', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.title', filter: {searchFieldName: 'userTitle'}},
				{header: 'Phone Number', hidden: true, width: 75, dataIndex: 'userAssignment.securityUser.phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
				{header: 'Assigned Entity', width: 100, hidden: true, dataIndex: 'userAssignment.portalEntity.entityLabelWithSourceSection', filter: false},
				{header: 'Entity', width: 175, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityId', type: 'combo', url: 'portalEntityListFind.json?orderBy=labelExpanded&securableEntity=true&securableEntityHasApprovedFilesPosted=true&active=true'}},
				{header: 'Definition Type', width: 50, hidden: true, dataIndex: 'notificationDefinitionType.name', filter: {searchFieldName: 'portalNotificationDefinitionTypeId', type: 'combo', url: 'portalNotificationTypeListFind.json?subscriptionAllowed=true&subscriptionForFileCategory=false'}},
				{header: 'File Category', width: 50, hidden: true, dataIndex: 'fileCategory.name', filter: {searchFieldName: 'portalFileCategoryId', type: 'combo', url: 'portalFileCategoryListFind.json?notificationSubscriptionForFileCategory=true'}},
				{header: 'Notification', width: 100, dataIndex: 'coalesceDefinitionTypeFileCategoryName'},
				{header: 'Subscribed', width: 50, dataIndex: 'notificationEnabled', type: 'boolean'},
				{header: 'Frequency', width: 75, dataIndex: 'subscriptionFrequencyLabel', tooltip: 'Optional frequency of notifications.  Selections are allowed for file categories where file frequency allows.', filter: {searchFieldName: 'subscriptionFrequency', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.portal.notification.subscription.SubscriptionFrequencies}}}
			],

			getTopToolbarInitialLoadParams: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('notificationEnabled', true);
				}
				return {
					portalFileCategoryId: this.getWindow().getMainFormId()
				};
			},
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.portal.notification.subscription.NotificationSubscriptionEntryWindow',
				getDefaultData: function(gridPanel, row) {
					return {
						securityUser: {id: row.json.userAssignment.securityUser.id},
						portalEntity: {id: row.json.portalEntity.id}
					};
				}
			}
		}]
	}
});
