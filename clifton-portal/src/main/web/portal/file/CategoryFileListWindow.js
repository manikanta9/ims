Clifton.portal.file.CategoryFileListWindow = Ext.extend(TCG.app.Window, {
	id: 'categoryFileListWindow',
	title: 'Files',
	width: 1500,
	height: 700,

	categoryName: 'OVERRIDE_ME',
	hideViewFileButton: false, // Contact Us we hide the View File Button
	columnOverrides: undefined, // ability for specific tabs to add column overrides - i.e. invoices tab shows the displayText as Status

	layout: 'border',
	items: [
		{
			region: 'west',
			xtype: 'portal-file-folders'

		},
		{
			region: 'center',
			layout: 'fit',
			items: [{

				xtype: 'portal-files-extended-base-grid',
				instructions: 'The following are all approved files for selected category folder.  If no report date filters are present, displays files that should be displayed "today".',
				getCategoryName: function() {
					return this.getWindow().categoryName;
				},
				getColumnOverrides: function() {
					return this.getWindow().columnOverrides;
				}
			}]
		}
	]
});


