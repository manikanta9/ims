Clifton.portal.file.FileUploadPendingListWindow = Ext.extend(TCG.app.Window, {
	id: 'fileImportWindow',
	title: 'Bulk Portal File Upload',
	iconCls: 'upload',
	width: 1200,
	height: 700,

	items: [
		{
			name: 'portalFileUploadPendingList.json',
			xtype: 'editorgrid',
			instructions: 'The following files are available for uploading to the portal under file groups that you have access to post to.  You can drag and drop multiple files here, review their mappings and save them to the portal.  When dragging and dropping files, you can optionally select the file group they belong to which will help with file mappings.  This is optional, and no necessary if the file name contains the file group it belongs to.',
			groupField: 'fileCategory.nameExpanded',
			groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]}',
			rowSelectionModel: 'checkbox',
			appendStandardColumns: false,
			additionalPropertiesToRequest: 'fileCategory.id|postPortalEntity.id|fileCategory.postEntityType.id',
			updateRecord: function() {
				// DO NOT REMOVE: This is here to avoid automatic saving of the grid
			},


			columns: [
				{header: 'File Category Expanded', width: 75, dataIndex: 'fileCategory.nameExpanded', hidden: true},
				{header: 'File Path', width: 100, dataIndex: 'filePath', hidden: true},
				{header: 'File Name', width: 200, dataIndex: 'fileNameFriendly'},
				{header: 'System File Name', width: 200, hidden: true, dataIndex: 'fileName'},
				{header: 'Modified', width: 75, dataIndex: 'modifiedDate', hidden: true},
				{header: 'File Size', width: 50, dataIndex: 'fileSize', hidden: true},

				// Mapping Properties
				{
					header: 'Frequency', width: 50, dataIndex: 'fileFrequency',
					editor: {
						xtype: 'combo', mode: 'local',
						store: {
							xtype: 'arraystore',
							data: Clifton.portal.file.PortalFileFrequencies
						}
					}
				},
				{header: 'Report Date', width: 50, dataIndex: 'reportDate', editor: {xtype: 'datefield'}},
				{header: 'Publish Date', width: 50, dataIndex: 'publishDate', editor: {xtype: 'datefield'}},
				{
					header: 'File Group', width: 75, dataIndex: 'fileCategory.name', idDataIndex: 'fileCategory.id', editor: {xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json', tooltipField: 'nameExpanded'},
					renderer: function(v, metaData, r) {
						if (r.json.fileCategory) {
							metaData.attr = TCG.renderQtip(r.json.fileCategory.nameExpanded);
						}
						return v;
					}
				},
				{
					header: 'Post Entity', width: 150, dataIndex: 'postPortalEntity.label', idDataIndex: 'postPortalEntity.id',
					editor: {
						xtype: 'combo', displayField: 'label', url: 'portalEntityListFind.json?postableEntity=true&securableEntity=true&active=true',
						beforequery: function(queryEvent) {
							// Reset Combo so re-queries each time (since each row has different results)
							this.resetStore();
							const editor = queryEvent.combo.gridEditor;
							const record = editor.record;
							const typeId = TCG.getValue('fileCategory.postEntityType.id', record.json);
							queryEvent.combo.store.setBaseParam('entityTypeId', typeId);
						}
					}
				}

			],

			addToolbarButtons: function(toolbar, gridPanel) {
				toolbar.add({
					text: 'Upload File(s)',
					tooltip: 'Upload and Post selected files as real files on the portal.',
					iconCls: 'upload',
					scope: this,
					handler: function() {
						const gridEditor = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (TCG.isEquals(0, sm.getCount())) {
							TCG.showError('Please select a file to import.', 'No File(s) Selected');
						}
						else {
							const rows = sm.getSelections();
							// import one at a time
							const fileCount = 0;
							gridEditor.importFile(rows, fileCount, gridPanel, 'ERROR');
						}
					}
				});
				toolbar.add('-');
				toolbar.add({
					text: 'Upload File(s) (Replace)',
					tooltip: 'Upload and Post selected files as real files on the portal. If the file already exists and is not approved, replace the file.',
					iconCls: 'upload',
					scope: this,
					handler: function() {
						const gridEditor = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (TCG.isEquals(0, sm.getCount())) {
							TCG.showError('Please select a file to import.', 'No File(s) Selected');
						}
						else {
							const rows = sm.getSelections();
							// import one at a time
							const fileCount = 0;
							gridEditor.importFile(rows, fileCount, gridPanel, 'REPLACE');
						}
					}
				});
				toolbar.add('-');
				toolbar.add({
					text: 'Remove File(s)',
					tooltip: 'Remove selected files.',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						const gridEditor = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (TCG.isEquals(0, sm.getCount())) {
							TCG.showError('Please select a file to remove.', 'No File(s) Selected');
						}
						else {
							const rows = sm.getSelections();
							// remove one at a time
							const fileCount = 0;
							gridEditor.removeFile(rows, fileCount, gridPanel);
						}
					}
				});
				toolbar.add('-');
			},

			getLoadParams: function(firstLoad) {
				const fileCategoryId = TCG.getChildByName(this.getTopToolbar(), 'fileCategoryId');
				if (TCG.isNotBlank(fileCategoryId.getValue())) {
					return {'fileCategoryId': fileCategoryId.getValue()};
				}
			},
			getTopToolbarFilters: function(toolbar) {
				const gridPanel = this;
				// Not an actual filter, but would like the paperclip to show up on the right
				toolbar.add({
					xtype: 'drag-drop-container',
					layout: 'fit',
					allowMultiple: true,
					cls: undefined,
					popupComponentName: 'Clifton.portal.file.FileImportSelectFileGroupWindow',
					maxFiles: 100,
					message: '&nbsp;',
					tooltipMessage: 'Drag and drop a files to this grid to make them available for import.',
					// Object to reload - i.e. gridPanel to reload after attaching new file
					getReloadObject: function() {
						return gridPanel;
					}
				});

				return [
					{fieldLabel: 'File Category', xtype: 'toolbar-combo', name: 'fileCategoryId', width: 250, displayField: 'label', url: 'portalFileCategoryForUploadListFind.json?includeParents=true', comboDisplayField: 'labelWithLevels'}
				];
			},

			importFile: function(rows, fileCount, gridPanel, fileDuplicateAction) {
				const gridEditor = this;

				// Validate Properties And Set Params
				const record = rows[fileCount];
				let skip = false;
				if (TCG.isBlank(record.get('reportDate'))) {
					TCG.showError('File ' + record.get('fileNameFriendly') + ' is missing a report date selection.  This file will be skipped.', 'Missing Data');
					skip = true;
				}
				if (TCG.isBlank(record.get('fileCategory.id')) || TCG.isEquals(-1, record.get('fileCategory.id'))) {
					TCG.showError('File ' + record.get('fileNameFriendly') + ' is missing a file category selection.  This file will be skipped.', 'Missing Data');
					skip = true;
				}
				if (TCG.isBlank(record.get('postPortalEntity.id')) || TCG.isEquals(-1, record.get('postPortalEntity.id'))) {
					TCG.showError('File ' + record.get('fileNameFriendly') + ' is missing a post entity selection.  This file will be skipped.', 'Missing Data');
					skip = true;
				}
				if (skip) {
					fileCount++;
					if (fileCount < rows.length) {
						gridEditor.importFile(rows, fileCount, gridPanel, fileDuplicateAction);
					}
				}
				else {
					const params = {};
					params.filePath = record.get('filePath');
					params.fileName = record.get('fileName');
					params.fileFrequency = record.get('fileFrequency');
					params.reportDate = TCG.parseDate(record.get('reportDate')).format('m/d/Y');
					params.publishDate = TCG.parseDate(record.get('publishDate')).format('m/d/Y');
					params['fileCategory.id'] = record.get('fileCategory.id');
					params['postPortalEntity.id'] = record.get('postPortalEntity.id');
					params.fileDuplicateAction = (TCG.isBlank(fileDuplicateAction) ? 'ERROR' : fileDuplicateAction);

					const loader = new TCG.data.JsonLoader({
						waitMsg: 'Importing File',
						waitTarget: gridPanel,
						params: params,
						onLoad: function(record, conf) {
							gridPanel.grid.getStore().removeAt(gridPanel.grid.getStore().indexOfId(rows[fileCount].id));
							gridPanel.updateCount();
							fileCount++;
							if (fileCount < rows.length) {
								gridEditor.importFile(rows, fileCount, gridPanel);
							}
						}
					});
					const url = 'portalFileUploadPendingSave.json';
					loader.load(url);
				}
			},

			removeFile: function(rows, fileCount, gridPanel) {
				const gridEditor = this;

				// Validate Properties And Set Params
				const record = rows[fileCount];
				const params = {};
				params.filePath = record.get('filePath');
				params.fileName = record.get('fileName');

				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Removing File',
					waitTarget: gridPanel,
					params: params,
					onLoad: function(record, conf) {
						gridPanel.grid.getStore().removeAt(gridPanel.grid.getStore().indexOfId(rows[fileCount].id));
						gridPanel.updateCount();
						fileCount++;
						if (fileCount < rows.length) {
							gridEditor.removeFile(rows, fileCount, gridPanel);
						}
					}
				});
				const url = 'portalFileUploadPendingDelete.json';
				loader.load(url);
			}
		}
	]
});


Clifton.portal.file.FileImportSelectFileGroupWindow = Ext.extend(TCG.file.DragAndDropPopupWindow, {
	title: 'Bulk Portal File Upload',
	iconCls: 'upload',
	height: 375,
	width: 550,

	instructions: 'Optionally select the file group the files belong to.  If the file name does not contain the file group name, then it is recommended to select the file group here, otherwise it is not necessary.  File group selections and mappings are limited to those groups you have access to post to. File groups help pre-define mappings for each file based on the file naming conventions. Example: 123456.Performance Report.2016 12.pdf would map to Performance Reports file group, posted to Account 123456, with Report date of 12/31/2016.',

	okButtonTooltip: 'Upload File(s)',

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		const files = win.ddFiles;
		Clifton.portal.file.enableDD.upload(files, 0, panel.getEl(), panel.getFormValuesFormatted(), win, this.saveUrl, this.replaceUrl, false);
	},

	saveUrl: 'portalFileUploadPendingUpload.json',
	replaceUrl: 'portalFileUploadPendingReplaceUpload.json',
	formItems: [
		{fieldLabel: 'File Group', name: 'portalFileCategoryName', hiddenName: 'portalFileCategoryId', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json', displayField: 'name', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow'},
		{fieldLabel: 'Report Date', name: 'reportDateOverride', xtype: 'datefield', qtip: 'Optional report date override to use instead of trying to determine the date from the file name. If set, this will take precedence over any date in the file name.'},
		{xtype: 'sectionheaderfield', header: 'If same file has already been uploaded and not processed:'},
		{
			xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'fileAction', allowBlank: false, items: [
				{fieldLabel: '', boxLabel: 'Error if file already exists', name: 'fileAction', inputValue: 'ERROR', checked: true},
				{fieldLabel: '', boxLabel: 'Skip files that already exist.', name: 'fileAction', inputValue: 'SKIP'},
				{fieldLabel: '', boxLabel: 'Replace files that already exist', name: 'fileAction', inputValue: 'REPLACE'}
			]
		}
	]
});


Clifton.portal.file.enableDD.upload = function(files, fileCount, el, params, reloadObj, loadUrl, replaceFileUrl, useReplaceFileUrl) {
	const file = files[fileCount];
	const form = new FormData();
	form.append('file', file);
	for (const p in params) {
		if (params[p] || params[p] === false) {
			form.append(p, params[p]);
		}
	}
	const xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	el.mask('Uploading ' + file.name);
	xhr.addEventListener('load', function(obj) {
		el.unmask();
		const json = obj.currentTarget.responseText;
		const o = Ext.decode(json);
		if (o && o.success === false) {
			if (o.message.indexOf('already exists') > 0) {
				const friendlyMessage = o.message.replace('Unexpected Error Occurred: ', '');
				Ext.Msg.confirm('File Exists', friendlyMessage + '<br><br><b>Would you like to replace the file?</b>&nbsp; Click Yes to replace the file, No to skip it.', function(a) {
					if (a === 'yes') {
						Clifton.portal.file.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl, replaceFileUrl, true);
					}
					else {
						fileCount++;
						if (fileCount === files.length) {
							if (reloadObj && reloadObj.reload) {
								reloadObj.reload.defer(300, reloadObj);
							}
						}
						else {
							Clifton.portal.file.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl, replaceFileUrl, false);
						}
					}
				});
			}
			else {
				TCG.showError('Upload Error: ' + o.message, 'Upload Error');
			}
		}
		else {
			fileCount++;
			if (fileCount === files.length) {
				if (reloadObj && reloadObj.reload) {
					reloadObj.reload.defer(300, reloadObj);
				}
			}
			else {
				Clifton.portal.file.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl, replaceFileUrl, false);
			}
		}

	}, false);
	xhr.addEventListener('error', function(obj) {
		el.unmask();
		TCG.showError('Upload Failed for: ' + file.name, 'Upload Error');
	}, false);
	xhr.open('POST', TCG.isTrue(useReplaceFileUrl) ? replaceFileUrl : loadUrl);
	xhr.send(form);
};
