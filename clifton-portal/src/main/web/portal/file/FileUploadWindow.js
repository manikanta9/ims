Clifton.portal.file.FileUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload Portal File',
	iconCls: 'upload',
	width: 1000,
	height: 600,
	doNotWarnOnCloseModified: true,
	enableShowInfo: false,

	items: [{
		xtype: 'portal-formpanel',
		url: 'portalFile.json',
		fileUpload: true,
		instructions: 'Use the following screen to manually upload one portal file.  Note: Manually uploaded files must be approved by a second person.',
		getSaveURL: function() {
			return 'portalFileUpload.json';
		},

		getDefaultData: function(win) {
			let dd = win.defaultData || {};
			if (win.defaultDataIsReal) {
				return dd;
			}

			const now = new Date().format('Y-m-d 00:00:00');

			dd = Ext.apply({
				publishDate: now
			}, dd);
			return dd;
		},


		listeners: {
			// After Creating - switch to the real window
			// Right now this opens new window and closes this one -
			// Not sure the best way to just switch this window
			aftercreate: function(panel, closeOnSuccess) {
				if (TCG.isFalse(closeOnSuccess)) {
					// upload only returns the id of the object - so we'll need to retrieve it again
					// There is an issue with the upload if the response contains html so instead of filtering out a bunch of properties
					// Just re-retrieve the object
					// If Uploading for a list of rollup entities, the file is not returned, so just close the window
					if (TCG.isNumber(panel.getFormValue('id'))) {
						const config = {
							params: {id: panel.getFormValue('id')},
							openerCt: panel.ownerCt.openerCt
						};
						TCG.createComponent('Clifton.portal.file.FileWindow', config);
					}
					panel.getWindow().closeWindow();
				}
			}
		},

		items: [
			{xtype: 'hidden', name: 'enableValidatingBinding', value: true}, // Must be here so that user ignorable exception new url will also pass it
			{xtype: 'hidden', name: 'requestedPropertiesRoot', value: 'data'}, // Must be here so that user ignorable exception new url will also pass it
			{xtype: 'hidden', name: 'requestedProperties', value: 'id'}, // Must be here so that user ignorable exception new url will also pass it
			{xtype: 'hidden', name: 'fileCategory.parent.id', submitValue: false},
			{xtype: 'hidden', name: 'postEntityType.id', submitValue: false},
			{fieldLabel: 'File Category', name: 'fileCategory.parent.nameExpanded', detailIdField: 'fileCategory.parent.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow', doNotSubmitValue: true},
			{
				fieldLabel: 'File Group', name: 'fileCategory.name', hiddenName: 'fileCategory.id', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json', displayField: 'name', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow',
				requestedProps: 'parent.nameExpanded|parent.id|portalFileFrequency|postEntityType.id|postEntityType.name|displayNameTemplate',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						if (record.json) {
							if (!TCG.isBlank(record.json.portalFileFrequency)) {
								fp.setFormValue('fileFrequency', record.json.portalFileFrequency);
								fp.getForm().findField('fileFrequency').resetReportDateField(record.json.portalFileFrequency);
							}
							fp.setFormValue('fileCategory.parent.nameExpanded', record.json.parent.nameExpanded);
							fp.setFormValue('fileCategory.parent.id', record.json.parent.id);
							if (record.json.postEntityType) {
								fp.setFormValue('postEntityType.id', record.json.postEntityType.id);
								fp.setFormValue('onePostEntitySourceSectionName', '');
								fp.setFormValue('onePostEntitySourceSectionId', '');
								fp.hideField('onePostEntitySourceSectionName');
								fp.setFieldLabel('onePostPortalEntityLabel', record.json.postEntityType.name + ':');
							}
							else {
								fp.setFormValue('postEntityType.id', '');
								fp.showField('onePostEntitySourceSectionName');
								fp.setFieldLabel('onePostPortalEntityLabel', 'Post Entity:');
							}
							if (!TCG.isBlank(record.json.displayNameTemplate)) {
								fp.setFormValue('displayName', '');
								fp.getForm().findField('displayName').allowBlank = true;
								fp.hideField('displayName');
							}
							else {
								// Default to Category Name
								fp.setFormValue('displayName', record.json.name);
								fp.getForm().findField('displayName').allowBlank = false;
								fp.showField('displayName');
							}
						}
					}
				}
			},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				fieldLabel: 'Frequency', name: 'fileFrequency', hiddenName: 'fileFrequency', xtype: 'combo', mode: 'local',
				store: {
					xtype: 'arraystore',
					data: Clifton.portal.file.PortalFileFrequencies
				},
				resetReportDateField: function(frequency) {
					const fp = this.getParentForm();

					const ff = TCG.getChildByName(fp, 'reportDateFormFragment');
					ff.removeAll(true);

					const reportDateField = {fieldLabel: 'Report Date', name: 'reportDate', xtype: 'datefield', allowBlank: false};
					let defaultDate = Clifton.portal.GetPreviousBusinessDay();
					if ('QUARTERLY' === frequency) {
						// Change Report Date field to Quarter Picker
						reportDateField.hidden = true;
						defaultDate = TCG.getLastDateOfPreviousQuarter();
						const defaultYear = defaultDate.getFullYear();
						let defaultQuarter = {value: '11', text: 'Fourth Quarter'};
						if (defaultDate.getMonth() <= 2) {
							defaultQuarter = {value: '02', text: 'First Quarter'};
						}
						else if (defaultDate.getMonth() <= 5) {
							defaultQuarter = {value: '05', text: 'Second Quarter'};
						}
						else if (defaultDate.getMonth() <= 8) {
							defaultQuarter = {value: '08', text: 'Third Quarter'};
						}
						const quarterPicker = {
							xtype: 'columnpanel',
							defaults: {
								labelWidth: 110
							},
							columns: [
								{
									config: {columnWidth: 0.25},
									rows: [
										{fieldLabel: 'Year', name: 'yearNumber', xtype: 'numberfield', allowBlank: false, submitValue: false, maxLength: 4, allowDecimals: false, minValue: 2000, maxValue: 2999, value: defaultYear}
									]
								},
								{
									config: {columnWidth: 0.75},
									rows: [
										{
											fieldLabel: 'Quarter', name: 'quarterPicker', requiredFields: ['yearNumber'], xtype: 'combo', mode: 'local', submitValue: false, allowBlank: false, valueField: 'value', displayField: 'description', tooltipField: 'description', value: defaultQuarter,
											store: {
												xtype: 'arraystore',
												fields: ['value', 'description'],
												data: [['02', 'First Quarter'], ['05', 'Second Quarter'], ['08', 'Third Quarter'], ['11', 'Fourth Quarter']]
											},
											listeners: {
												select: function(combo, record, index) {
													const yearNumber = fp.getFormValue('yearNumber');
													if (!TCG.isBlank(yearNumber)) {
														let date = new Date(yearNumber, record.json[0], 1);
														date = date.getLastDateOfMonth();
														fp.setFormValue('reportDate', TCG.parseDate(date).format('m/d/Y'), false);
													}
												}
											}
										}
									]
								}
							]
						};
						ff.add(quarterPicker);
					}
					if ('MONTHLY' === frequency || 'QUARTER_TO_DATE' === frequency) {
						// Change Report Date field to Month Picker
						defaultDate = TCG.getLastDateOfPreviousMonth();
						reportDateField.hidden = true;
						const monthPicker = {
							fieldLabel: 'Report Date', name: 'monthPicker', xtype: 'datefield-monthYear', allowBlank: false,
							value: defaultDate,
							listeners: {
								select: function(fld, date) {
									TCG.getParentFormPanel(fld).setFormValue('reportDate', TCG.parseDate(date).format('m/d/Y'));
								}
							}
						};
						ff.add(monthPicker);
					}
					if ('ANNUALLY' === frequency) {
						// Change Default Report Date to Last Day of Previous Quarter
						defaultDate = TCG.getLastDateOfPreviousQuarter();
					}
					ff.add(reportDateField);
					try {
						fp.doLayout();
					}
					catch (e) {
						// strange error due to removal of elements with allowBlank = false
					}
					if (defaultDate) {
						fp.setFormValue('reportDate', TCG.parseDate(defaultDate).format('m/d/Y'), false);
					}
				},
				listeners: {
					select: function(combo, record, index) {
						combo.resetReportDateField(record.json[0]);
					}
				}
			},
			{
				xtype: 'formfragment',
				name: 'reportDateFormFragment',
				bodyStyle: 'padding: 0px 0px 0',
				labelWidth: 110,
				defaults: {
					anchor: '-35' // leave room for error icon
				},
				frame: false,
				items: [
					{fieldLabel: 'Report Date', name: 'reportDate', xtype: 'datefield'}
				]
			},
			{fieldLabel: 'Publish Date', name: 'publishDate', xtype: 'datefield', qtip: 'Leave blank to default to today'},
			{fieldLabel: 'Display Name', name: 'displayName'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: 'If File Exists',
				items: [
					{boxLabel: 'Error', name: 'fileDuplicateAction', inputValue: 'ERROR', checked: true, qtip: 'Returns an error if file appears to be a duplicate on the new portal'},
					{boxLabel: 'Replace (Applies If Existing file is Not Approved)', name: 'fileDuplicateAction', inputValue: 'REPLACE', qtip: 'Replacing the file is only allowed if the file that is being replaced is not currently approved.'},
					{boxLabel: 'Skip', name: 'fileDuplicateAction', inputValue: 'SKIP', qtip: 'Do Nothing - Leave the existing file as is and do not post this one.'}
				]
			},

			{xtype: 'sectionheaderfield', header: 'Post Information'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'postAssignmentOptions',
				items: [
					{
						fieldLabel: '', boxLabel: 'One File Assignment', name: 'postAssignmentOptions', inputValue: 'ONE', checked: true,
						qtip: 'Assign this file to a single relationship, client, or account (based on file group selection).'
					},
					{
						fieldLabel: '', boxLabel: 'Rollup File Assignment(s)', name: 'postAssignmentOptions', inputValue: 'ROLLUP',
						qtip: 'Assign this file to a roll up (client group, commingled vehicle) which will assign it to all clients or client accounts for that rollup.'
					},
					{
						fieldLabel: '', boxLabel: 'Global File Assignment (Visible By All Users)', name: 'postAssignmentOptions', inputValue: 'GLOBAL',
						qtip: 'Assign this file to all relationships, clients, and accounts.  Used rarely and only allowed by users will elevated portal security permissions (i.e. PPA Administrator and PPA Administrator Support).'
					}
				],
				listeners: {
					change: function(f) {
						const p = TCG.getParentFormPanel(f);
						p.setResetPostEntityFields();
					}
				}
			},
			{xtype: 'hidden', name: 'postPortalEntity.id'},
			{xtype: 'hidden', name: 'globalFile', value: false},

			// One File Assignment Fields
			{fieldLabel: 'Post Section', name: 'onePostEntitySourceSectionName', hiddenName: 'onePostEntitySourceSectionId', xtype: 'combo', url: 'portalEntitySourceSectionListFind.json?securableEntity=true&postableEntity=true', doNotSubmitValue: true, requiredFields: ['fileCategory.name'], displayField: 'nameWithSourceSystem'},
			{
				fieldLabel: 'Post Entity', name: 'onePostPortalEntityLabel', hiddenName: 'onePostPortalEntityId', requiredFields: ['fileCategory.name'], xtype: 'combo', url: 'portalEntityListFind.json?active=true', detailPageClass: 'Clifton.portal.entity.EntityWindow', displayField: 'label', allowBlank: false, doNotSubmitValue: true,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.setBaseParam('entityTypeId', fp.getForm().findField('postEntityType.id').getValue());
						combo.store.setBaseParam('entitySourceSectionId', fp.getForm().findField('onePostEntitySourceSectionName').getValue());
					},
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.setFormValue('postPortalEntity.id', record.json.id);
					}

				}
			},

			// Rollup File Assignment Fields
			{
				xtype: 'formgrid',
				title: 'Rollup Assignment(s)',
				name: 'rollupAssignmentListGrid',
				storeRoot: 'postEntityList',
				dtoClass: 'com.clifton.portal.entity.PortalEntity',
				frame: false,
				hidden: true,
				doNotSubmitFields: ['entitySourceSection.id'],
				alwaysSubmitFields: [id],
				columnsConfig: [
					{
						header: 'Rollup Section', width: 250, dataIndex: 'entitySourceSection.nameWithSourceSystem', idDataIndex: 'entitySourceSection.id',
						editor: {
							xtype: 'combo', url: 'portalEntitySourceSectionListFind.json?postableEntity=true&childEntityTypeIdPopulated=true', allowBlank: false, displayField: 'nameWithSourceSystem',
							listeners: {
								beforequery: function(queryEvent) {
									// Clear Query Store so Re-Queries each time
									this.store.removeAll();
									this.lastQuery = null;

									// limit based on post entity type
									const grid = queryEvent.combo.gridEditor.containerGrid;
									const fp = TCG.getParentFormPanel(grid);
									queryEvent.combo.store.setBaseParam('childEntityTypeId', fp.getForm().findField('postEntityType.id').getValue());
								}
							}
						}
					},
					{
						header: 'Rollup Entity', width: 500, dataIndex: 'label', idDataIndex: 'id',
						editor: {
							xtype: 'combo', url: 'portalEntityListFind.json?active=true', allowBlank: false, displayField: 'label',
							listeners: {
								beforequery: function(queryEvent) {
									// Clear Query Store so Re-Queries each time
									this.store.removeAll();
									this.lastQuery = null;

									const record = queryEvent.combo.gridEditor.record;
									if (TCG.isBlank(record.get('entitySourceSection.id'))) {
										TCG.showError('Please Select a Rollup Section first.');
										return false;
									}

									queryEvent.combo.store.setBaseParam('entitySourceSectionId', record.get('entitySourceSection.id'));
								}
							}
						}
					}
				]
			},
			// Global File Assignment Fields
			{fieldLabel: 'Entity View Type', name: 'entityViewType.name', hiddenName: 'entityViewType.id', xtype: 'combo', url: 'portalEntityViewTypeListFind.json', hidden: true, qtip: 'When posting global files, can optionally limit it to a specific view type. i.e. Global Institutional File'}
		],

		setResetPostEntityFields: function() {
			const fp = this;
			const postAssignmentOption = fp.getForm().findField('postAssignmentOptions');

			// Clear All Values When Changed
			fp.setFormValue('postPortalEntity.id', ''); // Clear Post Entity
			fp.setFormValue('globalFile', false); // Clear Global File
			fp.getForm().findField('entityViewType.name').clearAndReset();
			fp.getForm().findField('onePostPortalEntityLabel').clearAndReset();

			const rollupGrid = TCG.getChildByName(fp, 'rollupAssignmentListGrid');
			if (rollupGrid.getStore()) {
				rollupGrid.getStore().removeAll();
			}

			if (postAssignmentOption.getValue().getGroupValue() === 'ONE') {
				if (TCG.isBlank(fp.getFormValue('postEntityType.id'))) {
					fp.showField('onePostEntitySourceSectionName');
				}
				else {
					fp.hideField('onePostEntitySourceSectionName');
				}
				fp.showField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = false;
				rollupGrid.setVisible(false);

				fp.hideField('entityViewType.name');
			}
			else if (postAssignmentOption.getValue().getGroupValue() === 'ROLLUP') {
				rollupGrid.setVisible(true);

				fp.hideField('onePostEntitySourceSectionName');
				fp.hideField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = true;

				fp.hideField('entityViewType.name');
			}
			else {
				rollupGrid.setVisible(false);

				fp.hideField('onePostEntitySourceSectionName');
				fp.hideField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = true;

				fp.setFormValue('globalFile', true); // Set Global File
				fp.showField('entityViewType.name');
			}
		}
	}]
});
