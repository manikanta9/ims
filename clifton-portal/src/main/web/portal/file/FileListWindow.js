Clifton.portal.file.FileListWindow = Ext.extend(TCG.app.Window, {
	id: 'fileListWindow',
	title: 'Portal Files',
	iconCls: 'disk',
	width: 1500,
	height: 700,
	categoryName: null,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Files',
				layout: 'border',
				items: [
					{
						region: 'west',
						xtype: 'portal-file-folders',
						approvedOnly: false
					},
					{
						region: 'center',
						layout: 'fit',
						items: [{

							xtype: 'portal-files-extended-base-grid',
							reloadOnRender: false,
							getCategoryName: function() {
								return null;
							},

							getApprovedFilesOnly: function() {
								return false;
							},

							groupField: 'fileCategory.name',
							groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

							columns: [
								{header: 'UUID', dataIndex: 'uuid', width: 30, hidden: true, skipExport: true},
								{header: 'PortalFileID', width: 12, dataIndex: 'portalFileId', hidden: true},
								{header: 'Category Sub-Set', width: 100, dataIndex: 'fileCategory.parent.name', hidden: true},
								{header: 'File Group', width: 100, dataIndex: 'fileCategory.name', hidden: true},
								{
									header: 'Format', width: 30, dataIndex: 'documentFileType', filter: false, align: 'center',
									renderer: function(ft, args, r) {
										if (!TCG.isBlank(ft)) {
											return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
										}
									}
								},
								{header: 'Display Name', width: 100, dataIndex: 'displayName'},
								{header: 'Display Text', width: 100, dataIndex: 'displayText', hidden: true},
								{header: 'Assigned Entity', width: 100, dataIndex: 'assignedPortalEntity.label', filter: {searchFieldName: 'assignedPortalEntityLabel'}},
								{header: 'Post Entity', width: 100, dataIndex: 'postPortalEntity.label', filter: {searchFieldName: 'postPortalEntityLabel'}},
								{header: 'Publish Date', width: 45, dataIndex: 'publishDate', filter: false},
								{header: 'Fk Field Id', width: 45, dataIndex: 'sourceFkFieldId', hidden: true},
								{header: 'Table Name', width: 45, dataIndex: 'entitySourceSection.sourceSystemTableName', hidden: true},
								{header: 'Report Date', width: 45, dataIndex: 'reportDate'},
								{header: 'Approved', width: 35, dataIndex: 'approved', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
								{header: 'File Name', width: 100, dataIndex: 'fileName', hidden: true, filter: {searchFieldName: 'fileNameAndPath'}},
								{header: 'File Name and Path', width: 300, dataIndex: 'fileNameAndPath', hidden: true}
							],

							editor: {
								addEnabled: false,
								detailPageClass: 'Clifton.portal.file.FileWindow',
								getDetailPageId: function(gridPanel, row) {
									return row.json.portalFileId;
								},
								deleteEnabled: true,
								deleteURL: 'portalFileDelete.json',
								allowToDeleteMultiple: true,
								getDeleteParams: function(selectionModel) {
									return {id: selectionModel.getSelected().json.portalFileId};
								}
							}
						}]
					}
				]
			},

			{
				title: 'File Categories',
				items: [{
					name: 'portalFileCategoryListFind',
					xtype: 'gridpanel',
					remoteSort: true, // Otherwise UI changes sort on Order column which should be hierarchy order
					instructions: 'Portal File Categories define the layout of the tabs/menus and each page.  Categories can be assigned to a security resource for restricting access to the files.  Top level categories cannot be added here, however children can be added to existing categories or category sub-sets by drilling into the parent you would like to add to.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Type', width: 50, hidden: true, dataIndex: 'fileCategoryType'},
						{
							header: 'Category Name', width: 75, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								if (r.data.fileCategoryType === 'CATEGORY') {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								if (r.data.fileCategoryType === 'CATEGORY_SUBSET') {
									return '<div style="FONT-WEIGHT: bold; COLOR: #777777; PADDING-LEFT: 15px">' + v + '</div>';
								}
								return '<div style="PADDING-LEFT: 30px">' + v + '</div>';
							}
						},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Security Resource', width: 50, dataIndex: 'securityResource.name', filter: {searchFieldName: 'securityResourceId', type: 'combo', url: 'portalSecurityResourceListFind.json', loadAll: true}},
						{header: 'Order', width: 20, dataIndex: 'categoryOrder', type: 'int', filter: {sortFieldName: 'orderExpanded'}},
						{header: 'E-mail Address', width: 50, dataIndex: 'categoryEmailAddress'},
						{header: 'Frequency', width: 50, dataIndex: 'portalFileFrequency', tooltip: 'Default Frequency for the category.', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.portal.file.PortalFileFrequencies}}},
						{header: 'Approval Required', width: 30, dataIndex: 'approvalRequired', type: 'boolean'},
						{header: 'Display Name Template', width: 50, hidden: true, dataIndex: 'displayNameTemplate'},
						{header: 'Display Text Template', width: 50, hidden: true, dataIndex: 'displayTextTemplate'},
						{header: 'File Name Template', width: 50, hidden: true, dataIndex: 'fileNameTemplate'},
						{header: 'Post Entity Type', width: 30, dataIndex: 'postEntityType.name', filter: {searchFieldName: 'postEntityTypeId', url: 'portalEntityTypeListFind.json?securableEntity=true'}},
						{header: 'Source Entity Type', width: 30, hidden: true, dataIndex: 'sourceEntityType.name', filter: {searchFieldName: 'sourceEntityTypeId', url: 'portalEntityTypeListFind.json?fileSource=true'}},
						{
							header: 'Notifications', width: 30, dataIndex: 'notificationUsed', type: 'boolean',
							tooltip: 'Users can receive e-mail notifications when new files are posted under this category.'
						},
						{
							header: 'Notification Definition', width: 50, dataIndex: 'notificationDefinition.name', hidden: true, filter: {searchFieldName: 'notificationDefinitionId', url: 'portalNotificationDefinitionListFind.json?subscriptionForFileCategory=true'},
							tooltip: 'Defines what notifications and how often they are sent for this category.'
						},
						{
							header: 'Subscription Option', width: 50, dataIndex: 'notificationSubscriptionOption', hidden: true, filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.portal.notification.subscription.SubscriptionOptions}},
							tooltip: 'Defines (if applicable) the default subscriptions and/or required subscriptions.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow',
						drillDownOnly: true
					},
					addToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Rebuild Security Data',
							tooltip: 'Rebuilds table that tracks for each security resource the categories that apply.  i.e. you can have access to Investment Statements, and thus should have access to Accounting Reporting and ultimately Reporting.',
							iconCls: 'run',
							scope: this,
							handler: function() {
								const grid = this.grid;
								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									waitMsg: 'Rebuilding...'
								});
								loader.load('portalFileCategorySecurityResourceExpandedRebuild.json');

							}
						});
						toolBar.add('-');
						toolBar.add({
							text: 'Rebuild Entity Mapping',
							tooltip: 'Rebuilds table that tracks for each file category and entity that have files mapped.  Also tracks at the "all" entity level and global level separately.  Used for cases for finding file categories that apply to an entity, excluding entities without files posted, etc.',
							iconCls: 'run',
							scope: this,
							handler: function() {
								const grid = this.grid;
								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									waitMsg: 'Rebuilding...'
								});
								loader.load('portalFileCategoryPortalEntityMappingListRebuild.json');

							}
						});
						toolBar.add('-');
					}
				}]
			}
		]
	}]
});


