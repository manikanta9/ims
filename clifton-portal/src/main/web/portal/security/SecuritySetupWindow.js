Clifton.portal.security.SecuritySetupWindow = Ext.extend(TCG.app.Window, {
	id: 'securitySetupWindow',
	title: 'Security',
	iconCls: 'lock',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Security Users',
				items: [
					{
						name: 'portalSecurityUserListFind',
						xtype: 'gridpanel',
						groupField: 'userRole.name',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Users" : "User"]})',
						getTopToolbarFilters: function(toolbar) {
							return [
								{
									fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 100, minListWidth: 100, mode: 'local', value: 'ALL_USERS',
									store: {
										xtype: 'arraystore',
										data: [['ALL_USERS', 'All Users', 'Display events for all users'], ['OUR_USERS', 'Our Users', 'Display events for our (PPA Employee) users only'], ['EXTERNAL_USERS', 'External Users', 'Display events for external users only.']]
									}
								},
								{fieldLabel: 'View Type (External Users Only)', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json'},
								{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},
						topToolbarSearchParameter: 'searchPattern',
						getTopToolbarInitialLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('disabled', false);
							}

							const t = this.getTopToolbar();
							const displayType = TCG.getChildByName(t, 'displayType');
							const params = {};
							if (TCG.isEquals(displayType.getValue(), 'OUR_USERS')) {
								params.userRolePortalEntitySpecific = false;
							}
							else if (TCG.isEquals(displayType.getValue(), 'EXTERNAL_USERS')) {
								params.userRolePortalEntitySpecific = true;
							}
							const viewType = TCG.getChildByName(t, 'viewTypeId');
							params.assignmentPortalEntityViewTypeId = viewType.getValue();
							return params;
						},
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'User Role', width: 50, dataIndex: 'userRole.name', hidden: true, filter: {searchFieldName: 'userRoleId', type: 'combo', url: 'portalSecurityUserRoleListFind.json'}, defaultSortColumn: true},
							{header: 'User Name/E-mail', width: 100, dataIndex: 'username'},
							{header: 'First Name', width: 50, dataIndex: 'firstName'},
							{header: 'Last Name', width: 50, dataIndex: 'lastName'},
							{header: 'Title', width: 50, dataIndex: 'title'},
							{header: 'Company Name', width: 50, dataIndex: 'companyName'},
							{header: 'Phone Number', width: 50, dataIndex: 'phoneNumber', hidden: true},
							{header: 'Source System', width: 35, dataIndex: 'portalEntitySourceSystem.name', filter: {searchFieldName: 'portalEntitySourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
							{header: 'Password Updated On', width: 50, hidden: true, dataIndex: 'passwordUpdateDate'},
							{header: 'Invalid Login Attempts', width: 35, hidden: true, type: 'int', useNull: true, dataIndex: 'invalidLoginAttemptCount'},
							{header: 'Account Locked', width: 35, type: 'boolean', dataIndex: 'accountLocked'},
							{header: 'Password Reset Required', width: 50, type: 'boolean', dataIndex: 'passwordResetRequired'},
							{header: 'Terms Accepted', width: 35, type: 'boolean', dataIndex: 'termsOfUseAccepted', hidden: true},
							{header: 'Terms Accepted On', width: 50, dataIndex: 'termsOfUseAcceptanceDate', hidden: true},
							{header: 'Disabled', width: 35, type: 'boolean', dataIndex: 'disabled'}
						],
						editor: {
							detailPageClass: 'Clifton.portal.security.user.UserWindow',
							drillDownOnly: true
						},

						addToolbarButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Rebuild User Data',
								tooltip: 'As user assignments/resources are updated user meta-data for locating which files and entities they have access to should be re-built automatically.  You can trigger this re-build manually for selected user.  If no user is selected will trigger for all users.',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										Ext.Msg.confirm('Confirm All User Data Rebuild', 'Are you sure you want to rebuild meta-data for all users?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid,
													waitMsg: 'Rebuilding...'
												});
												loader.load('portalSecurityUserAssignmentExpandedRebuild.json');
											}
										});
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection rebuilds are not supported.  Please select one user.', 'NOT SUPPORTED');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid,
											waitMsg: 'Rebuilding...',
											params: {
												userId: sm.getSelected().id
											}
										});
										loader.load('portalSecurityUserAssignmentExpandedForUserRebuild.json');
									}
								}
							});
							toolBar.add('-');

						}

					}
				]
			},

			{
				title: 'Our User Security',
				items: [
					{
						name: 'portalSecurityUserListFind',
						xtype: 'gridpanel',
						forceFit: false,
						appendStandardColumns: false,
						instructions: 'This tab shows our internal users and the access they have to posting/updating files under specific security resources.  To see external user assignments and security, please go to Settings -> User Management.',
						additionalPropertiesToRequest: 'id|userRole.administrator',
						getTopToolbarInitialLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('disabled', false);
							}

							return {
								populateResourcePermissionMap: true,
								userRolePortalEntitySpecific: false
							};
						},

						addToolbarButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Rebuild Security Resource Permissions',
								tooltip: 'All user resource permissions are defined by group memberships and their permissions.  User permissions should rebuild automatically as changes are made.  You can trigger this re-build manually for selected user.  If no user is selected will trigger for all users.',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										Ext.Msg.confirm('Confirm All User Data Rebuild', 'Are you sure you want to rebuild permissions for all internal users?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid,
													waitMsg: 'Rebuilding...'
												});
												loader.load('portalSecurityUserAssignmentResourceListRebuild.json');
											}
										});
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection rebuilds are not supported.  Please select one user.', 'NOT SUPPORTED');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid,
											waitMsg: 'Rebuilding...',
											params: {
												securityUserId: sm.getSelected().id
											}
										});
										loader.load('portalSecurityUserAssignmentResourceListRebuild.json');
									}
								}
							});
							toolBar.add('-');

						},

						groupField: 'userRole.name',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Users" : "User"]})',

						initComponent: function() {
							const url = 'portalSecurityResourceListFind.json';

							const resourceList = TCG.data.getData(url, this);
							const resourceCols = [];
							let count = 0;

							Ext.each(resourceList, function(f) {
								const resourceName = f.name;
								resourceCols.push({
									header: resourceName, width: 100, dataIndex: 'resourcePermissionMap.' + f.id + '.accessAllowed', type: 'boolean', filter: false, sortable: false,
									renderer: function(v, metaData, record) {
										if (TCG.isTrue(record.json.userRole.administrator)) {
											return TCG.renderBoolean(true);
										}
										return TCG.renderBoolean(v);
									}
								});
								count++;
							});

							const cols = [];
							Ext.each(this.userColumns, f => cols.push(f));
							Ext.each(resourceCols, f => cols.push(f));
							this.columns = cols;
							TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
						},
						topToolbarSearchParameter: 'searchPattern',

						exportGrid: function(outputFormat, includeHidden) {
							TCG.showError('Export is not available for this screen at this time.  Please use the Print option.', 'Not Available');
						},

						userColumns: [
							{header: 'User', width: 150, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}},
							{header: 'E-mail', hidden: true, width: 150, dataIndex: 'username', filter: {searchFieldName: 'username'}},
							{header: 'Company', hidden: true, width: 150, dataIndex: 'companyName', filter: {searchFieldName: 'userCompanyName'}},
							{header: 'Title', hidden: true, width: 150, dataIndex: 'title', filter: {searchFieldName: 'userTitle'}},
							{header: 'Phone Number', hidden: true, width: 75, dataIndex: 'phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
							{header: 'User Role', hidden: true, width: 150, dataIndex: 'userRole.name', filter: {type: 'combo', url: 'portalSecurityUserRoleListFind.json?portalEntitySpecific=true', searchFieldName: 'userRoleId'}, defaultSortColumn: true},
							{header: 'Disabled', width: 75, type: 'boolean', dataIndex: 'disabled'}
						],

						editor: {
							detailPageClass: 'Clifton.portal.security.user.UserWindow',
							drillDownOnly: true
						}
					}
				]
			},
			{
				title: 'Security User Roles',
				items: [{
					name: 'portalSecurityUserRoleListFind',
					xtype: 'gridpanel',
					groupField: 'portalEntitySpecific',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Roles" : "Role"]})',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Group Label', width: 50, hidden: true, dataIndex: 'portalEntitySpecific',
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.portalEntitySpecific)) {
									return 'External Users';
								}
								return 'Our Users';
							}
						},
						{header: 'Role Name', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{
							header: 'Administrator', width: 35, dataIndex: 'administrator', type: 'boolean',
							tooltip: 'Administrators are members of IT staff that have fully unrestricted access'
						},
						{
							header: 'Portal Administrator', width: 35, dataIndex: 'portalAdministrator', type: 'boolean',
							tooltip: 'Portal Administrators are those with additional access to portal setup screens.'
						},
						{
							header: 'Portal Security Administrator', width: 35, dataIndex: 'portalSecurityAdministrator', type: 'boolean',
							tooltip: 'Portal Security Administrators are those with additional access to security setup. If the role is portal entity specific then they only have security access within their mapped entities.'
						},
						{
							header: 'Assignment Role Only', width: 35, dataIndex: 'assignmentRoleOnly', type: 'boolean',
							tooltip: 'Indicates that this role can only be applied to user assignments, not users.  This is used only for Portal Entity Specific which indicates assignments are used.'
						},
						{
							header: 'View Type', dataIndex: 'entityViewType.name', width: 50, filter: {type: 'combo', orNull: true, searchFieldName: 'entityViewTypeId', url: 'portalEntityViewTypeListFind.json'},
							tooltip: 'Applies to Portal Entity Specific (i.e. External User) Assignments only. If populated, the user assignment portal entity must be of the selected view type. i.e. Limits Institutional specific roles to Institutional clients.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.portal.security.user.UserRoleWindow',
						drillDownOnly: true
					}
				}]
			},
			{
				title: 'Security Resources',
				items: [{
					name: 'portalSecurityResourceListFind',
					xtype: 'gridpanel',
					instructions: 'A security resource is used to restrict user access to specific portal file categories and their files.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parent Resource Name', width: 100, dataIndex: 'parent.name', hidden: true},
						{
							header: 'Resource Name', width: 75, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								if (TCG.isBlank(r.json.parent)) {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								return '<div style="PADDING-LEFT: 15px;">' + v + '</div>';
							}
						},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'},
						{
							header: 'Do Not Default Access', width: 50, dataIndex: 'doNotDefaultPortalEntitySpecificAccess', type: 'boolean',
							tooltip: 'Applies to External Users only. When left unchecked, the first time the security resource becomes available for an assignment, the user will automatically be given access.  If checked, then portal admins will need to explicitly give access (unless the user is given access to the parent security resource - if applies).  This does not apply to Portal Admins who are given access to ALL resources.'
						},
						{header: 'Description', width: 200, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.portal.security.resource.ResourceWindow'
					}
				}]
			},
			{
				title: 'Our Security Groups',
				items: [
					{
						name: 'portalSecurityGroupListFind',
						xtype: 'gridpanel',
						instructions: 'This tab shows our internal security groups. These groups can be manual or applied from an external source system.  If applied from an external source system their membership is also applied from the source system, however you can edit the security resources that group membership should have access to post to.',
						topToolbarSearchParameter: 'searchPattern',


						columns: [
							{header: 'Group Name', width: 100, dataIndex: 'name'},
							{header: 'Group Description', width: 200, dataIndex: 'description'},
							{header: 'Source System', width: 50, dataIndex: 'sourceSystem.name', filter: {searchFieldName: 'sourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
							{header: 'Source FK Field ID', hidden: true, width: 50, dataIndex: 'sourceFkFieldId'}
						],

						editor: {
							detailPageClass: 'Clifton.portal.security.user.group.GroupWindow'
						}
					}
				]
			},
			{
				title: 'OAuth Administration',
				items: [{
					xtype: 'oauth-client-grid',
					instructions: 'This is for administering outgoing OAuth connections from the Client Portal Administration application.',
					editor: {
						detailPageClass: 'Clifton.core.oauth.OAuth2ClientWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.clientId;
						},
						getDetailPageParams: function(id) {
							return {'id': id, 'titlePrefix': 'Portal Admin'};
						},
						deleteUrl: 'securityOauthClientDetailDelete.json',
						getDeleteParams: function(selectionModel) {
							return {id: selectionModel.getSelected().json.clientId};
						},
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Register OAuth Connection',
								tooltip: 'Register the Portal Admin with a resource server using OAuth',
								iconCls: 'run',
								scope: this,
								handler: function() {
									TCG.createComponent('Clifton.core.oauth.OAuth2RegistrationWindow', {
										openerCt: gridPanel
									});
								}
							});
							t.add('-');
							TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
						}
					}
				}]
			},
			{
				title: 'Incoming OAuth Administration',
				items: [{
					name: 'portalOauthClientDetailList',
					xtype: 'oauth-client-grid',
					instructions: 'Displays the OAuth clients registered with the Client Portal. These connections have access to consume resources on the Client Portal.',
					editor: {
						addEnabled: false,
						detailPageClass: 'Clifton.core.oauth.OAuth2ClientWindow',
						openDetailPage: function(className, gridPanel, id, row, itemText) {
							const defaultData = id ? this.getDefaultDataForExisting(gridPanel, row, className, itemText) : this.getDefaultData(gridPanel, row, className, itemText);
							if (defaultData === false) {
								return;
							}

							const params = id ? this.getDetailPageParams(id) : undefined;
							const cmpId = TCG.getComponentId(className, id);
							TCG.createComponent(className, {
								id: cmpId + '_portal',
								defaultData: defaultData,
								params: params,
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						},
						getDetailPageId: function(gridPanel, row) {
							return row.json.clientId;
						},
						getDetailPageParams: function(id) {
							return {'id': id, 'titlePrefix': 'Portal', 'loadUrl': 'portalOauthClientDetail.json'};
						},
						deleteUrl: 'portalOauthClientDetailDelete.json',
						getDeleteParams: function(selectionModel) {
							return {id: selectionModel.getSelected().json.clientId};
						}
					}
				}]
			}
		]
	}]
});


