Clifton.portal.security.resource.ResourceWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Security Resource',
	iconCls: 'user',
	width: 750,
	height: 400,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 160,
					url: 'portalSecurityResource.json',

					items: [
						{fieldLabel: 'Parent Resource', name: 'parent.name', hiddenName: 'parent.id', detailPageClass: 'Clifton.portal.security.resource.ResourceWindow', xtype: 'combo', url: 'portalSecurityResourceListFind.json?rootOnly=true'},
						{fieldLabel: 'Resource Name', name: 'name'},
						{fieldLabel: '', boxLabel: 'System Defined (Name cannot be changed)', xtype: 'checkbox', name: 'systemDefined', readonly: true},
						{
							fieldLabel: '', boxLabel: 'Do Not Default Access (Portal Entity Specific Users Only)', xtype: 'checkbox', name: 'doNotDefaultPortalEntitySpecificAccess',
							qtip: 'Applies to External Users only. When left unchecked, the first time the security resource becomes available for an assignment, the user will automatically be given access.  If checked, then portal admins will need to explicitly give access (unless the user is given access to the parent security resource - if applies).  This does not apply to Portal Admins who are given access to ALL resources.'
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: '35'}
					]
				}]
			}
		]
	}]
});
