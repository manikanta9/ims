Clifton.portal.security.user.UserRoleWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Security User Role',
	iconCls: 'user',
	width: 1000,
	height: 600,

	items: [
		{
			xtype: 'portal-formpanel',
			table: 'PortalSecurityUserRole',
			url: 'portalSecurityUserRoleExpanded.json',
			listeners: {
				afterload: function(form) {
					this.showHideResourceMappingGrid();
				}
			},

			showHideResourceMappingGrid: function() {
				const show = this.getFormValue('applyRoleResourceMapping', true);
				const grid = TCG.getChildByName(this, 'roleResourceMappingListGrid');
				grid.setVisible(show);
			},

			items: [
				{fieldLabel: 'Role Name', name: 'name', readOnly: true},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
				{
					xtype: 'columnpanel',
					columns: [
						{
							config: {columnWidth: 0.49},
							rows: [
								{
									fieldLabel: '', boxLabel: 'Administrator', name: 'administrator', xtype: 'checkbox', disabled: true,
									qtip: 'Administrators have full unrestricted access to all parts of the system.'
								},
								{
									fieldLabel: '', boxLabel: 'Portal Administrator', name: 'portalAdministrator', xtype: 'checkbox', disabled: true,
									qtip: 'Portal Administrators have additional write access to various setup screens in the portal.'
								},
								{
									fieldLabel: '', boxLabel: 'Portal Security Administrator', name: 'portalSecurityAdministrator', xtype: 'checkbox', disabled: true,
									qtip: 'Portal Security Administrators have additional write access to security settings. Note: if the role is also entity specific then the user only has access to security settings within their own entity mapping'
								}
							]
						},

						{
							config: {columnWidth: 0.49},
							rows: [
								{
									fieldLabel: '', boxLabel: 'Portal Entity Specific', name: 'portalEntitySpecific', xtype: 'checkbox', disabled: true,
									qtip: 'Indicates that users in this role must be granted access at a specific entity level (Relationship, Client, Account).'
								},

								{
									fieldLabel: '', boxLabel: 'Assignment Role Only', name: 'assignmentRoleOnly', xtype: 'checkbox', disabled: true,
									qtip: 'Indicates that this role can only be applied to user assignments, not users.  This is used only for Portal Entity Specific which indicates assignments are used.'
								},
								{
									fieldLabel: 'View Type', xtype: 'combo', name: 'entityViewType.name', hiddenName: 'entityViewType.id', url: 'portalEntityViewTypeListFind.json', requiredFields: ['portalEntitySpecific'],
									qtip: 'Applies to Portal Entity Specific Roles only. If populated, the user assignment portal entity must be of the selected view type. i.e. Limits Institutional specific roles to Institutional clients.'
								}
							]
						}
					]
				},
				{xtype: 'sectionheaderfield', header: 'Role Resource Mapping Restrictions'},
				{
					xtype: 'checkbox', fieldLabel: '', name: 'applyRoleResourceMapping', boxLabel: 'Restrict Security Resource Access for this Role',
					qtip: 'Used rarely for cases where specific roles are only allowed access to specified sub-set of resources. Currently used only for Custodians.',
					listeners: {
						'check': function(f) {
							const p = TCG.getParentFormPanel(f);
							p.showHideResourceMappingGrid();
						}
					}
				},
				{
					xtype: 'formgrid',
					name: 'roleResourceMappingListGrid',
					storeRoot: 'roleResourceMappingList',
					dtoClass: 'com.clifton.portal.security.user.PortalSecurityUserRoleResourceMapping',
					alwaysSubmitFields: ['securityResource.id', 'accessAllowed'],
					readOnly: true, // Cannot Add or Remove - only check/uncheck the access column
					instructions: 'Optionally limit the resources available for users assigned this role.  Not used often and only if locking down a role is necessary (like Custodian)',
					listeners: {
						afteredit: function(grid, rowIndex, colIndex) {
							// on checkbox change and if it's a parent - synchronize it's children
							if (grid.colModel) {
								const accessAllowedCheckbox = grid.colModel.findColumnIndex('accessAllowed');
								if (colIndex === accessAllowedCheckbox) {

									const thisRow = grid.store.data.items[rowIndex];
									const v = thisRow.data.accessAllowed;
									const resourceId = thisRow.json.securityResource.id;
									let i = 0;
									const items = grid.store.data.items;
									let row = items[i];

									// Where row's parent = this security resource id = synchronize the checkbox values
									while (row) {
										if (TCG.getValue('securityResource.parent.id', row.json) === resourceId) {
											grid.startEditing(i, colIndex);
											row.set('accessAllowed', v);
											grid.stopEditing();
										}
										row = items[i++];
									}
								}
							}
						}
					},


					columnsConfig: [
						{header: 'ID', hidden: true, width: 50, dataIndex: 'id'},
						{header: 'Parent Security Resource', width: 400, hidden: true, dataIndex: 'securityResource.parent.name'},
						{
							header: 'Security Resource', width: 400, dataIndex: 'securityResource.name',
							renderer: function(v, metaData, r) {
								if (TCG.isBlank(r.json.securityResource.parent)) {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
							}
						},
						{header: 'Access', width: 100, dataIndex: 'accessAllowed', type: 'boolean', editor: {xtype: 'checkbox'}}
					]
				}

			]
		}
	]

});
