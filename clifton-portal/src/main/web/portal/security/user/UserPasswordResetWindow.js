Clifton.portal.security.user.UserPasswordResetWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Change Your Password',
	iconCls: 'key',
	width: 500,
	height: 350,
	modal: true,
	centerAlign: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		getSaveURL: function() {
			return 'portalSecurityCurrentUserPasswordUpdate.json';
		},

		instructions: 'Please enter your current password and then your new password.' +
			'<ul class="c-list">' +
			'<li>Passwords must be at least 8 characters long (not longer than 30)</li>' +
			'<li>Passwords cannot contain all or part of the user name</li>' +
			'<li>Contains elements from three of the following four categories:' +
			'<ul class="c-list">' +
			'<li>English uppercase characters (A through Z)</li>' +
			'<li>English lowercase characters (a through z)</li>' +
			'<li>Numeric characters (0 through 9)</li>' +
			'<li>Non-alphanumeric (for example, !, $, #, %)</li>' +
			'</ul>' +
			'</li>' +
			'<li>Passwords cannot contain sequential numeric or alpha characters (i.e. abc, 123, qwerty)</li>' +
			'<li>Passwords cannot contain the same character in succession more than 3 times (i.e. 1111)</li>' +
			'</ul>',

		items: [
			{fieldLabel: 'Current Password', name: 'oldPassword', inputType: 'password', allowBlank: false},

			{
				fieldLabel: 'New Password', name: 'password', inputType: 'password', allowBlank: false, minLength: 8, maxLength: 30,
				validator: function(v) {
					const formPanel = TCG.getParentFormPanel(this);
					// Simple UI Validation - Server has validation also
					const oldPasswordField = formPanel.getForm().findField('oldPassword');
					if (oldPasswordField && oldPasswordField.getValue() === v) {
						return 'New password cannot be the same as your current password.';
					}
					return true;
				}
			},
			{
				fieldLabel: 'Confirm Password', name: 'passwordConfirm', inputType: 'password', allowBlank: false, submitValue: false,
				validator: function(v) {
					const formPanel = TCG.getParentFormPanel(this);
					if (formPanel.getForm().findField('password').getValue() !== v) {
						return 'Passwords do not match';
					}
					return true;
				}
			}
		]
	}]
});
