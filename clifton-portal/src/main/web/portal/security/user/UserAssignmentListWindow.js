// This is the window used to add users/assign them to specific portal entities
// Part of the Client Admin UI
Clifton.portal.security.user.UserAssignmentListWindow = Ext.extend(TCG.app.Window, {
	id: 'userAssignmentList',
	title: 'User Management',
	iconCls: 'lock',
	width: 1500,

	items: [{
		xtype: 'portal-security-user-assignment-list-grid',
		appendStandardColumns: false,
		instructions: 'The following user assignments apply to your organization(s).  You can either click Add User to add a new user to your organization, or select an existing user to add additional assignments to that user.',

		getTopToolbarInitialLoadParams: function(firstLoad) {
			if (firstLoad) {
				// default to not disabled
				this.setFilterValue('disabled', false);
			}

			return {
				populateResourcePermissionMap: true,
				viewAsUserId: Clifton.portal.GetPortalFileCurrentViewUser(),
				viewSecurityAdminRole: true,
				portalEntityIdOrRelatedEntity: Clifton.portal.GetPortalFileCurrentViewEntity()
			};
		},

		exportGrid: function(outputFormat, includeHidden) {
			TCG.showError('Export is not available for this screen at this time.  Please use the Print option.', 'Not Available');
		},

		remoteSort: true,

		initComponent: function() {
			const viewAsUserId = Clifton.portal.GetPortalFileCurrentViewUser();
			const url = 'portalSecurityResourceListFind.json?viewAsUserId=' + viewAsUserId;


			const resourceList = TCG.data.getData(url, this);
			const resourceCols = [];
			let count = 0;

			Ext.each(resourceList, function(f) {
				const resourceName = f.name;
				// If view as user, will show only what that user has access to
				// otherwise will just show top level by default
				let hidden = false;
				if (TCG.isBlank(viewAsUserId) && TCG.isNotBlank(f.parent)) {
					hidden = true;
				}
				resourceCols.push({header: resourceName, hidden: hidden, width: 100, dataIndex: 'resourcePermissionMap.' + f.id + '.accessAllowed', type: 'boolean', filter: false, sortable: false});
				count++;
			});

			const cols = [];
			Ext.each(this.assignmentColumns, f => cols.push(f));
			Ext.each(resourceCols, f => cols.push(f));
			this.columns = cols;
			TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
		}
	}]
});


