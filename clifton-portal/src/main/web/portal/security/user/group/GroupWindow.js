Clifton.portal.security.user.group.GroupWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Security Group',
	iconCls: 'group',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 160,
					url: 'portalSecurityGroupExpanded.json',
					instructions: 'For groups maintained by an outside source system, resource permissions only can be applied here.  If creating a new group, please apply the group name/description first then you can add resource permissions.',

					getSaveURL: function() {
						const w = this.getWindow();
						if (TCG.isBlank(TCG.getValue('sourceSystem.id', w.getMainForm().formValues))) {
							return 'portalSecurityGroupSave.json';
						}
						return 'portalSecurityGroupSave.json?saveResourceListOnly=true';
					},

					listeners: {
						afterload: function(panel) {
							const f = panel.getForm();
							const w = panel.getWindow();

							const tabs = w.items.get(0);
							const editableUsersTab = TCG.getChildByName(tabs, 'editableUsersTab');
							const readOnlyUsersTab = TCG.getChildByName(tabs, 'readOnlyUsersTab');

							// Source System - i.e. IMS
							const sourceSystem = TCG.getValue('sourceSystem.name', f.formValues);

							// Once we have an entity we can display the resource grid
							TCG.getChildByName(panel, 'groupResourceFormGrid').setVisible(true);

							if (!TCG.isBlank(sourceSystem)) {
								this.setReadOnlyField('name', true);
								this.setReadOnlyField('description', true);

								if (!readOnlyUsersTab) {
									tabs.add(w.readOnlyUsersTab);
								}
								if (editableUsersTab) {
									editableUsersTab.setVisible(false);
								}
							}
							else {
								this.hideField('sourceSystem.name');
								if (!editableUsersTab) {
									tabs.add(w.editableUsersTab);
								}
								if (readOnlyUsersTab) {
									readOnlyUsersTab.setVisible(false);
								}
							}
						}
					},

					getWarningMessage: function(form) {
						let msg = undefined;
						if (!TCG.isBlank(TCG.getValue('sourceSystem.id', form.formValues))) {
							msg = 'This group is maintained by an outside source system. You can only change resource permissions for this group.  Group name and membership is maintained by the external system';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Source System', name: 'sourceSystem.name', xtype: 'displayfield'},
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: '35'},

						{
							xtype: 'formgrid',
							name: 'groupResourceFormGrid',
							storeRoot: 'resourceList',
							hidden: true,
							dtoClass: 'com.clifton.portal.security.user.group.PortalSecurityGroupResource',
							alwaysSubmitFields: ['securityResource.id', 'accessAllowed'],
							readOnly: true, // Cannot Add or Remove - only check/uncheck the access column
							instructions: 'This group gives its users access to post files to categories under the selected security resources',

							listeners: {
								afteredit: function(grid, rowIndex, colIndex) {
									// on checkbox change and if it's a parent - synchronize it's children
									if (grid.colModel) {
										const accessAllowedCheckbox = grid.colModel.findColumnIndex('accessAllowed');
										if (colIndex === accessAllowedCheckbox) {

											const thisRow = grid.store.data.items[rowIndex];
											const v = thisRow.data.accessAllowed;
											const resourceId = thisRow.json.securityResource.id;
											let i = 0;
											const items = grid.store.data.items;
											let row = items[i];

											// Where row's parent = this security resource id = synchronize the checkbox values
											while (row) {
												if (TCG.getValue('securityResource.parent.id', row.json) === resourceId) {
													grid.startEditing(i, colIndex);
													row.set('accessAllowed', v);
													grid.stopEditing();
												}
												row = items[i++];
											}
										}
									}
								}
							},


							columnsConfig: [
								{header: 'ID', hidden: true, width: 50, dataIndex: 'id'},
								{header: 'Parent Security Resource', width: 400, hidden: true, dataIndex: 'securityResource.parent.name'},
								{
									header: 'Security Resource', width: 400, dataIndex: 'securityResource.name',
									renderer: function(v, metaData, r) {
										if (TCG.isBlank(r.json.securityResource.parent)) {
											return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
										}
										return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
									}
								},
								{header: 'Access', width: 100, dataIndex: 'accessAllowed', type: 'boolean', editor: {xtype: 'checkbox'}}
							]
						}
					]
				}]
			}

		]
	}],


	readOnlyUsersTab: {
		title: 'Users',
		name: 'readOnlyUsersTab',
		items: [{
			name: 'portalSecurityUserGroupListForGroup',
			xtype: 'gridpanel',
			instructions: 'The following users are members of the selected group.',
			additionalPropertiesToRequest: 'securityUser.id',

			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'User Role', width: 50, dataIndex: 'securityUser.userRole.name'},
				{header: 'User Name', width: 50, dataIndex: 'securityUser.username'},
				{header: 'First Name', width: 50, dataIndex: 'securityUser.firstName'},
				{header: 'Last Name', width: 50, dataIndex: 'securityUser.lastName'}
			],
			editor: {
				detailPageClass: 'Clifton.portal.security.user.UserWindow',
				getDetailPageId: function(gridPanel, row) {
					return row.json.securityUser.id;
				},
				drillDownOnly: true
			},
			getLoadParams: function(firstLoad) {
				return {groupId: this.getWindow().getMainForm().formValues.id};
			}
		}]
	},

	editableUsersTab: {
		title: 'Users',
		name: 'editableUsersTab',
		items: [{
			name: 'portalSecurityUserGroupListForGroup',
			xtype: 'gridpanel',
			instructions: 'The following users are members of the selected group.',
			additionalPropertiesToRequest: 'securityUser.id',

			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'User Role', width: 50, dataIndex: 'securityUser.userRole.name'},
				{header: 'User Name', width: 50, dataIndex: 'securityUser.username'},
				{header: 'First Name', width: 50, dataIndex: 'securityUser.firstName'},
				{header: 'Last Name', width: 50, dataIndex: 'securityUser.lastName'}
			],
			editor: {
				detailPageClass: 'Clifton.portal.security.user.UserWindow',
				getDetailPageId: function(gridPanel, row) {
					return row.json.securityUser.id;
				},
				deleteURL: 'portalSecurityUserGroupDelete.json',
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					toolBar.add(new TCG.form.ComboBox({name: 'user', url: 'portalSecurityUserListFind.json?userRolePortalEntitySpecific=false&disabled=false', displayField: 'label', width: 150, listWidth: 230}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add selected user to this group',
						iconCls: 'add',
						handler: function() {
							const userId = TCG.getChildByName(toolBar, 'user').getValue();
							if (TCG.isBlank(userId)) {
								TCG.showError('You must first select desired user User from the list.');
							}
							else {
								const groupId = gridPanel.getWindow().getMainFormId();
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Linking...',
									params: {'securityUser.id': userId, 'securityGroup.id': groupId},
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.getChildByName(toolBar, 'user').reset();
									}
								});
								loader.load('portalSecurityUserGroupSave.json');
							}
						}
					});
					toolBar.add('-');
				}
			},
			getLoadParams: function(firstLoad) {
				return {groupId: this.getWindow().getMainForm().formValues.id};
			}
		}]

	}

});
