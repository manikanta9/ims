Clifton.portal.security.user.UserWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Security User',
	iconCls: 'user',
	width: 1000,
	height: 650,

	windowOnShow: function(w) {
		if (w.defaultData && w.defaultData.userRole && TCG.isTrue(w.defaultData.portalEntitySpecific)) {
			const tabs = w.items.get(0).items.get(0);
			tabs.add(w.assignmentsTab);
			tabs.add(w.notificationsTab);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 160,
					url: 'portalSecurityUser.json',

					tbar: [
						{
							text: 'Change User Role',
							name: 'changeRole',
							iconCls: 'user',
							tooltip: 'Change the role this user should have.',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								TCG.createComponent('Clifton.portal.security.user.UserChangeRoleWindow', {
									defaultData: {
										portalSecurityUserId: TCG.getValue('id', fp.getForm().formValues),
										portalEntitySpecific: TCG.getValue('userRole.portalEntitySpecific', fp.getForm().formValues),
										portalSecurityUserLabel: TCG.getValue('label', fp.getForm().formValues)
									},
									defaultDataIsReal: true,
									openerCt: fp
								});
							}
						},
						{xtype: 'tbseparator', name: 'changeRoleSeparator'},

						{
							text: 'Lock Account',
							name: 'lockAccount',
							iconCls: 'lock',
							tooltip: 'Lock this user account.',
							handler: function() {
								TCG.getParentFormPanel(this).lockUnlockUser(true);
							}
						},
						{xtype: 'tbseparator', name: 'lockAccountSeparator'},

						{
							text: 'Unlock Account',
							name: 'unlockAccount',
							iconCls: 'unlock',
							tooltip: 'Unlock this user account.',
							handler: function() {
								TCG.getParentFormPanel(this).lockUnlockUser(false);
							}
						},
						{xtype: 'tbseparator', name: 'unlockAccountSeparator'},


						{
							text: 'Reset Password',
							name: 'resetPassword',
							iconCls: 'key',
							tooltip: 'Reset Password',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								TCG.createComponent('Clifton.portal.security.user.UserChangePasswordWindow', {
									defaultData: {
										portalSecurityUserId: TCG.getValue('id', fp.getForm().formValues),
										portalSecurityUserLabel: TCG.getValue('label', fp.getForm().formValues)
									},
									defaultDataIsReal: true,
									openerCt: fp
								});
							}
						},
						{xtype: 'tbseparator', name: 'resetPasswordSeparator'},

						{
							text: 'Send Welcome Email',
							name: 'sendWelcomeEmail',
							iconCls: 'email',
							tooltip: 'Send Welcome Email to this user.  Will also generate a random password and include that password in the email.',
							handler: function() {
								TCG.getParentFormPanel(this).sendWelcomeEmail();
							}
						},
						{xtype: 'tbseparator', name: 'sendWelcomeEmailSeparator'},


						{
							text: 'View Client Portal',
							name: 'viewClientUI',
							iconCls: 'www',
							tooltip: 'View Client Portal As this User',
							handler: function() {
								Clifton.portal.ViewClientUIAsUser(TCG.getParentFormPanel(this).getWindow().getMainFormId());
							}
						},
						{xtype: 'tbseparator', name: 'viewClientUISeparator'}
					],

					lockUnlockUser: function(lock) {
						const fp = this;
						const userId = fp.getForm().formValues.id;
						if (TCG.isNull(userId)) {
							TCG.showError('Missing User ID.');
							return;
						}

						let msg = 'Unlocking User';
						let url = 'portalSecurityUserUnlock.json';
						if (lock === true) {
							msg = 'Locking User';
							url = 'portalSecurityUserLock.json';
						}
						const loader = new TCG.data.JsonLoader({
							waitMsg: msg,
							waitTarget: this,
							params: {portalSecurityUserId: userId},
							onLoad: function(record, conf) {
								fp.reload();
							}
						});
						loader.load(url);
					},

					sendWelcomeEmail: function() {
						const fp = this;
						const userId = fp.getForm().formValues.id;
						if (TCG.isBlank(userId)) {
							TCG.showError('Missing User ID.');
							return;
						}

						const msg = 'Sending Email';
						const url = 'portalSecurityUserWelcomeNotificationSend.json';
						const loader = new TCG.data.JsonLoader({
							waitMsg: msg,
							waitTarget: this,
							params: {portalSecurityUserId: userId},
							onLoad: function(record, conf) {
								if (record) {
									TCG.showMessage(record, 'Email Result');
								}
								fp.reload();
							}
						});
						loader.load(url);
					},

					// Iterates through all the toolbar buttons and if in the list - they are set to visible, else hidden
					showToolbarButtons: function(toolbarButtonNamesToShow) {
						const tb = this.getTopToolbar();
						// Increment By two because of the | separators
						for (let i = 0; i < tb.items.length; i = i + 2) {
							const button = tb.items.items[i];
							let visible = false;
							if (Ext.isArray(toolbarButtonNamesToShow)) {
								for (let j = 0; j < toolbarButtonNamesToShow.length; j++) {
									if (toolbarButtonNamesToShow[j] === button.name) {
										visible = true;
										break;
									}
								}
							}
							else if (toolbarButtonNamesToShow === button.name) {
								visible = true;
							}
							button.setVisible(visible);
							tb.items.items[i + 1].setVisible(visible);
						}

					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('portalSecurityUser.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					listeners: {
						afterload: function(panel) {
							const f = panel.getForm();
							const w = panel.getWindow();

							const tabs = w.items.get(0);
							// External user Tabs
							const assignmentTab = TCG.getChildByName(tabs, 'assignments');
							const notificationsTab = TCG.getChildByName(tabs, 'notifications');
							const subscriptionsTab = TCG.getChildByName(tabs, 'subscriptions');

							// Internal User Tabs
							const groupsTab = TCG.getChildByName(tabs, 'groups');

							const entitySpecific = TCG.getValue('userRole.portalEntitySpecific', f.formValues);
							const formGrid = TCG.getChildByName(panel, 'userAssignmentResourceFormGrid');
							// Not necessary to show assignments tab for anything that isn't portal entity specific
							if (TCG.isTrue(entitySpecific)) {
								if (!assignmentTab) {
									tabs.add(w.assignmentsTab);
								}
								if (!notificationsTab) {
									tabs.add(w.notificationsTab);
								}
								if (!subscriptionsTab) {
									tabs.add(w.subscriptionsTab);
								}
								if (groupsTab) {
									groupsTab.setVisible(false);
								}
								formGrid.setVisible(false);
							}
							else {
								if (!groupsTab) {
									tabs.add(w.groupsTab);
								}
								if (assignmentTab) {
									assignmentTab.setVisible(false);
								}
								if (notificationsTab) {
									notificationsTab.setVisible(false);
								}
								if (subscriptionsTab) {
									subscriptionsTab.setVisible(false);
								}
								// Show the Security Form Grid which allows specifying upload access - as long as not an admin or disabled user
								if (TCG.isTrue(TCG.getValue('userRole.administrator', f.formValues)) || TCG.isTrue(TCG.getValue('disabled', f.formValues))) {
									formGrid.setVisible(false);
								}
								else {
									formGrid.setVisible(true);
								}

							}


							// Hide Entire Toolbar for Disabled Users
							if (TCG.isTrue(TCG.getValue('disabled', f.formValues))) {
								this.getTopToolbar().setVisible(false);
							}

							// Source System - i.e. IMS
							// These users use external authentication (Crowd) and do not need many of the fields here and also cannot be edited
							const sourceSystem = TCG.getValue('portalEntitySourceSystem.id', f.formValues);
							if (!TCG.isBlank(sourceSystem)) {
								this.hideField('passwordUpdateDate');
								this.hideField('invalidLoginAttemptCount');
								this.hideField('accountLocked');

								this.setReadOnlyField('username', true);
								this.setReadOnlyField('firstName', true);
								this.setReadOnlyField('lastName', true);
								this.setReadOnlyField('title', true);
								this.setReadOnlyField('phoneNumber', true);
								this.setReadOnlyField('companyName', true);

								// Change User Role is Only Valid Toolbar Button
								panel.showToolbarButtons('changeRole');
							}
							else {
								this.hideField('portalEntitySourceSystem.name');

								if (TCG.isTrue(TCG.getValue('accountLocked', f.formValues))) {
									panel.showToolbarButtons(['unlockAccount', 'resetPassword', 'sendWelcomeEmail', 'viewClientUI']);
								}
								else {
									panel.showToolbarButtons(['lockAccount', 'resetPassword', 'sendWelcomeEmail', 'viewClientUI']);
								}
							}
						}
					},

					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isTrue(TCG.getValue('disabled', form.formValues))) {
							msg = 'This user\'s account has been disabled.';
						}
						else if (!TCG.isBlank(TCG.getValue('portalEntitySourceSystem.id', form.formValues))) {
							msg = 'This user is maintained by an outside source system. You can only change the user role for this user.  File permissions are maintained by group permissions and cannot be edited for individual users.';
						}
						if (TCG.isTrue(TCG.getValue('accountLocked', form.formValues))) {
							msg = 'This user\'s account is currently locked.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Source System', name: 'portalEntitySourceSystem.name', xtype: 'displayfield'},
						{fieldLabel: 'User Name/E-mail', name: 'username'},
						{fieldLabel: 'User Role', name: 'userRole.name', xtype: 'linkfield', detailIdField: 'userRole.id', detailPageClass: 'Clifton.portal.security.user.UserRoleWindow'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									config: {columnWidth: 0.49},
									rows: [
										{fieldLabel: 'First Name', name: 'firstName', xtype: 'textfield', allowBlank: false},
										{fieldLabel: 'Last Name', name: 'lastName', xtype: 'textfield', allowBlank: false},
										{fieldLabel: 'Password Updated On', name: 'passwordUpdateDate', xtype: 'displayfield', submitValue: false},
										{
											fieldLabel: 'Invalid Login Attempts', name: 'invalidLoginAttemptCount', xtype: 'integerfield', readOnly: true,
											qtip: 'The number of sequential invalid log in attempts that have been made.  This is reset after a successful login.'
										},
										{
											fieldLabel: '', boxLabel: 'Account Locked', name: 'accountLocked', xtype: 'checkbox', disabled: true,
											qtip: 'Accounts are automatically locked when too many (12) incorrect login attempts are made. Users are prevented from accessing the system and the only way to re-gain access would be for a security admin to unlock the account.'
										},
										{fieldLabel: 'Terms Accepted On', name: 'termsOfUseAcceptanceDate', xtype: 'displayfield', submitValue: false}
									]
								},
								{
									config: {columnWidth: 0.49},
									rows: [
										{fieldLabel: 'Title', name: 'title', xtype: 'textfield'},
										{fieldLabel: 'Company Name', name: 'companyName', xtype: 'textfield'},
										{fieldLabel: 'Phone Number', name: 'phoneNumber', xtype: 'textfield'}
									]
								}
							]
						},

						{
							xtype: 'formgrid',
							hidden: true,
							name: 'userAssignmentResourceFormGrid',
							storeRoot: 'userAssignmentResourceList',
							dtoClass: 'com.clifton.portal.security.user.PortalSecurityUserAssignmentResource',
							alwaysSubmitFields: ['securityResource.id', 'accessAllowed'],
							readOnly: true, // Cannot Add or Remove - only check/uncheck the access column
							instructions: 'This user has access to post files to categories under the selected security resources.  Permissions are maintained via group permissions and cannot be edited here. As long as the user is a member of at least one group that has access to a security resource the user will be granted access to that resource.',

							columnsConfig: [
								{header: 'ID', hidden: true, width: 50, dataIndex: 'id'},
								{header: 'Parent Security Resource', width: 400, hidden: true, dataIndex: 'securityResource.parent.name'},
								{
									header: 'Security Resource', width: 400, dataIndex: 'securityResource.name',
									renderer: function(v, metaData, r) {
										if (TCG.isBlank(r.json.securityResource.parent)) {
											return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
										}
										return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
									}
								},
								{header: 'Access', width: 100, dataIndex: 'accessAllowed', type: 'boolean'}
							]
						}
					]
				}]
			}
		]

	}],


	assignmentsTab: {
		name: 'assignments',
		title: 'Assignments',

		listeners: {
			beforerender: function() {
				this.add(this.assignmentItem);
			}
		},

		items: [],
		assignmentItem: {
			name: 'portalSecurityUserAssignmentListFind',
			xtype: 'gridpanel',
			instructions: 'Selected user has access to the following entities.',
			appendStandardColumns: false,
			additionalPropertiesToRequest: 'id|portalEntity.endDate',
			getLoadParams: function() {
				return {
					portalSecurityUserId: this.getWindow().getMainForm().formValues.id
				};
			},


			columns: [
				{
					header: 'Portal Entity', width: 300, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityLabelExpanded'},
					renderer: function(value, metaData, record) {
						const pendingTermination = record.get('pendingTermination');
						if (TCG.isTrue(pendingTermination)) {
							metaData.css += 'x-grid3-row-highlight-light-red';
							metaData.attr = TCG.renderQtip('Pending Termination: Assignment will be disabled on ' + TCG.parseDate(TCG.getValue('portalEntity.endDate', record.json)).format('m/d/Y'));
						}
						return value;
					}
				},
				{header: 'Assignment Role', width: 100, dataIndex: 'assignmentRole.name', filter: {type: 'combo', url: 'portalSecurityUserRoleListFind.json', searchFieldName: 'assignmentRoleId'}},
				{header: 'Pending Termination', width: 60, dataIndex: 'pendingTermination', type: 'boolean', hidden: true},
				{header: 'Disabled', width: 60, dataIndex: 'disabled', type: 'boolean'},
				{header: 'Disabled Date', width: 75, dataIndex: 'disabledDate', hidden: true},
				{header: 'Disabled Reason', width: 75, dataIndex: 'disabledReason', hidden: true}
			],

			editor: {
				detailPageClass: 'Clifton.portal.security.user.UserAssignmentWindow',
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					toolBar.add({
						text: 'Add Assignment',
						tooltip: 'Add an assignment to Selected User',
						iconCls: 'add',
						handler: function() {
							TCG.createComponent('Clifton.portal.security.user.UserAssignmentWizardWindow', {
								defaultData: {securityUser: gridPanel.getWindow().getMainForm().formValues},
								openerCt: gridPanel
							});
						}
					});
					toolBar.add('-');
				}
			}
		}
	},

	notificationsTab: {
		name: 'notifications',
		title: 'Notifications',

		items: [{
			name: 'portalNotificationListFind',
			xtype: 'gridpanel',
			instructions: 'The following email notifications have been sent to the selected user.',

			columns: [
				{header: 'Definition Name', width: '100', dataIndex: 'notificationDefinition.name', filter: {searchFieldName: 'notificationDefinitionId', type: 'combo', url: 'portalNotificationDefinitionListFind.json'}},
				{header: 'Recipient', width: 100, hidden: true, dataIndex: 'coalesceRecipientLabel', filter: {searchFieldName: 'recipientSearchPattern'}},
				{header: 'Subject', width: 100, dataIndex: 'subject'},
				{header: 'Sent', width: 50, dataIndex: 'sentDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
				{header: 'Pop-Up', width: 50, type: 'boolean', dataIndex: 'notificationDefinition.popUpNotification', filter: {searchFieldName: 'popUpNotification'}},
				{
					header: 'Error', width: 50, dataIndex: 'error', type: 'boolean',
					renderer: function(v, metaData, r) {
						return TCG.renderBoolean(v, r.data.errorMessage);
					}
				},
				{header: 'Error Message', width: 200, dataIndex: 'errorMessage', hidden: true},
				{
					header: 'Acknowledged', width: 50, dataIndex: 'acknowledged', type: 'boolean', tooltip: 'Acknowledgements only apply to pop-up notifications',
					renderer: function(v, metaData, r) {
						if (TCG.isTrue(r.data['notificationDefinition.popUpNotification'])) {
							return TCG.renderBoolean(v);
						}
						return '&nbsp;';
					}
				}
			],
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.portal.notification.NotificationWindow'
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('sentDate', {'after': new Date().add(Date.DAY, -60)});
				}
				return {recipientUserId: this.getWindow().getMainForm().formValues.id};
			}
		}]
	},


	subscriptionsTab: {
		name: 'subscriptions',
		title: 'Subscriptions',

		items: [{
			name: 'portalNotificationSubscriptionListFind',
			xtype: 'gridpanel',
			instructions: 'By default, notification subscriptions are turned off.  Notifications can be subscribed to in a hierarchical manner.  For example, a user can subscribe at the client level, but turn them off for a specific account and vice versa.',

			additionalPropertiesToRequest: 'id|portalEntity.id',
			columns: [
				{header: 'Assigned Entity', width: 100, hidden: true, dataIndex: 'userAssignment.portalEntity.entityLabelWithSourceSection', filter: false},
				{header: 'Entity', width: 200, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityId', type: 'combo', url: 'portalEntityListFind.json?orderBy=labelExpanded&securableEntity=true&securableEntityHasApprovedFilesPosted=true&active=true'}},
				{header: 'Definition Type', width: 50, hidden: true, dataIndex: 'notificationDefinitionType.name', filter: {searchFieldName: 'portalNotificationDefinitionTypeId', type: 'combo', url: 'portalNotificationTypeListFind.json?subscriptionAllowed=true&subscriptionForFileCategory=false'}},
				{header: 'File Category', width: 50, hidden: true, dataIndex: 'fileCategory.name', filter: {searchFieldName: 'portalFileCategoryId', type: 'combo', url: 'portalFileCategoryListFind.json?notificationSubscriptionForFileCategory=true'}},
				{header: 'Notification', width: 100, dataIndex: 'coalesceDefinitionTypeFileCategoryName'},
				{header: 'Subscribed', width: 50, dataIndex: 'notificationEnabled', type: 'boolean'},
				{header: 'Frequency', width: 100, dataIndex: 'subscriptionFrequencyLabel', tooltip: 'Optional frequency of notifications.  Selections are allowed for file categories where file frequency allows.', filter: {searchFieldName: 'subscriptionFrequency', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.portal.notification.subscription.SubscriptionFrequencies}}}
			],
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.portal.notification.subscription.NotificationSubscriptionEntryWindow',
				getDefaultData: function(gridPanel, row) {
					return {
						securityUser: {id: this.getWindow().getMainForm().formValues.id},
						portalEntity: {id: row.json.portalEntity.id}
					};
				}
			},
			getLoadParams: function(firstLoad) {
				return {portalSecurityUserId: this.getWindow().getMainForm().formValues.id};
			}
		}]
	},


	groupsTab: {
		title: 'Groups',
		name: 'groupsTab',
		items: [{
			name: 'portalSecurityUserGroupListForUser',
			xtype: 'gridpanel',
			instructions: 'This user is a member of the following groups.  You can manually add a user to a group ONLY if the group is a manual group.  Otherwise, group memberships are maintained via an outside source system.',
			additionalPropertiesToRequest: 'securityGroup.id',

			columns: [
				{header: 'Group Name', width: 100, dataIndex: 'securityGroup.name'},
				{header: 'Group Description', width: 200, dataIndex: 'securityGroup.description'},
				{header: 'Source System', width: 50, dataIndex: 'securityGroup.sourceSystem.name'}
			],
			editor: {
				detailPageClass: 'Clifton.portal.security.user.group.GroupWindow',
				getDetailPageId: function(gridPanel, row) {
					return row.json.securityGroup.id;
				},
				deleteURL: 'portalSecurityUserGroupDelete.json',
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					toolBar.add(new TCG.form.ComboBox({name: 'group', url: 'portalSecurityGroupListFind.json?sourceSystemEmpty=true', width: 150, listWidth: 230}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add user to selected group',
						iconCls: 'add',
						handler: function() {
							const groupId = TCG.getChildByName(toolBar, 'group').getValue();
							if (TCG.isBlank(groupId)) {
								TCG.showError('You must first select desired group from the list.');
							}
							else {
								const userId = gridPanel.getWindow().getMainFormId();
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Linking...',
									params: {'securityUser.id': userId, 'securityGroup.id': groupId},
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.getChildByName(toolBar, 'group').reset();
									}
								});
								loader.load('portalSecurityUserGroupSave.json');
							}
						}
					});
					toolBar.add('-');
				}
			},
			getLoadParams: function(firstLoad) {
				return {userId: this.getWindow().getMainForm().formValues.id};
			}
		}]

	}
});


Clifton.portal.security.user.UserChangeRoleWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Change User Role',
	iconCls: 'user',
	width: 400,
	height: 150,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelFieldName: 'portalSecurityUserLabel',
		getSaveURL: function() {
			return 'portalSecurityUserRoleUpdate.json';
		},

		items: [
			{xtype: 'hidden', name: 'portalSecurityUserId'},
			{xtype: 'hidden', name: 'portalEntitySpecific'},
			{xtype: 'hidden', name: 'portalSecurityUserLabel', submitValue: false},
			{
				fieldLabel: 'User Role', name: 'portalSecurityUserRoleName', xtype: 'combo', hiddenName: 'portalSecurityUserRoleId', detailPageClass: 'Clifton.portal.security.user.UserRoleWindow', url: 'portalSecurityUserRoleListFind.json?assignmentRoleOnly=false',
				listeners: {
					beforequery: function(queryEvent) {
						const portalEntitySpecific = queryEvent.combo.getParentForm().getForm().findField('portalEntitySpecific').getValue();
						queryEvent.combo.store.setBaseParam('portalEntitySpecific', portalEntitySpecific);
					}
				}
			}
		]
	}]
});


Clifton.portal.security.user.UserChangePasswordWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Change User Password',
	iconCls: 'key',
	width: 500,
	height: 325,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelFieldName: 'portalSecurityUserLabel',
		getSaveURL: function() {
			return 'portalSecurityUserPasswordUpdate.json';
		},

		instructions: '<ul class="c-list">' +
			'<li>Passwords must be at least 8 characters long (not longer than 30)</li>' +
			'<li>Passwords cannot contain all or part of the user name</li>' +
			'<li>Contains elements from three of the following four categories:' +
			'<ul class="c-list">' +
			'<li>English uppercase characters (A through Z)</li>' +
			'<li>English lowercase characters (a through z)</li>' +
			'<li>Numeric characters (0 through 9)</li>' +
			'<li>Non-alphanumeric (for example, !, $, #, %)</li>' +
			'</ul>' +
			'</li>' +
			'<li>Passwords cannot contain sequential numeric or alpha characters (i.e. abc, 123, qwerty)</li>' +
			'<li>Passwords cannot contain the same character in succession more than 3 times (i.e. 1111)</li>' +
			'</ul>',

		items: [
			{xtype: 'hidden', name: 'portalSecurityUserId'},
			{xtype: 'hidden', name: 'portalSecurityUserLabel', submitValue: false},
			{fieldLabel: 'New Password', name: 'password', inputType: 'password', allowBlank: false},
			{
				fieldLabel: 'Confirm Password', name: 'passwordConfirm', inputType: 'password', allowBlank: false, submitValue: false,
				validator: function(v) {
					const formPanel = TCG.getParentFormPanel(this);
					if (formPanel.getForm().findField('password').getValue() !== v) {
						return 'Passwords do not match';
					}
					return true;
				}
			}
		]
	}]
});
