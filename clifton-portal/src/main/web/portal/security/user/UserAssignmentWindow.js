Clifton.portal.security.user.UserAssignmentWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Security User Assignment',
	iconCls: 'user',
	width: 750,
	height: 400,

	items: [{
		xtype: 'portal-formpanel',
		labelWidth: 160,
		url: 'portalSecurityUserAssignment.json',

		reloadPermissionGrid: function() {
			const fp = this;
			const grid = TCG.getChildByName(fp, 'userAssignmentResourceListGrid');
			const loader = new TCG.data.JsonLoader({
				waitTarget: grid,
				waitMsg: 'Reloading Permissions for Role...',
				params: {userAssignmentId: fp.getWindow().getMainFormId(), roleId: fp.getFormValue('assignmentRole.id', true)},
				scope: grid,
				onLoad: function(record, conf) {
					const data = {};
					data.userAssignmentResourceList = record;
					grid.store.loadData(data);
					grid.markModified();
				}
			});
			loader.load('portalSecurityUserAssignmentResourceListForAssignmentAndRole.json');
		},

		items: [
			{fieldLabel: 'Security User', name: 'securityUser.username', detailIdField: 'securityUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.security.user.UserWindow'},
			{
				fieldLabel: 'Assignment Role', name: 'assignmentRole.name', hiddenName: 'assignmentRole.id', xtype: 'combo', url: 'portalSecurityUserRoleListFind.json?portalEntitySpecific=true',
				listeners: {
					beforequery: function(queryEvent) {
						const portalEntityId = queryEvent.combo.getParentForm().getFormValue('portalEntity.id');
						if (!TCG.isBlank(portalEntityId)) {
							queryEvent.combo.store.setBaseParam('assignmentForPortalEntityId', portalEntityId);
						}
					},
					select: function(field) {
						TCG.getParentByClass(field, TCG.form.FormPanel).reloadPermissionGrid();
					},
					specialkey: function(field, e) {
						if (e.getKey() === e.ENTER) {
							TCG.getParentByClass(field, TCG.form.FormPanel).reloadPermissionGrid();
						}
					}
				}
			},
			{fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', detailIdField: 'portalEntity.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.entity.EntityWindow'},
			{
				xtype: 'fieldset-checkbox',
				title: 'Disabled',
				checkboxName: 'disabled',
				items: [
					{fieldLabel: 'Disabled Date', name: 'disabledDate', xtype: 'datefield'},
					{
						fieldLabel: 'Disabled Reason', name: 'disabledReason', hiddenName: 'disabledReason', mode: 'local', xtype: 'combo', requiredFields: ['disabledDate'], allowBlank: false,
						store: {
							xtype: 'arraystore',
							data: [['Left Organization', 'Left Organization', 'User has left the company'],
								['New Role', 'New Role', 'User has new role and no longer requires access to Portal'],
								['Other', 'Other', 'Other reason for disabling the user']
							]
						}
					}
				]
			},
			{
				xtype: 'formgrid',
				storeRoot: 'userAssignmentResourceList',
				name: 'userAssignmentResourceListGrid',
				dtoClass: 'com.clifton.portal.security.user.PortalSecurityUserAssignmentResource',
				alwaysSubmitFields: ['securityResource.id', 'accessAllowed'],
				readOnly: true, // Cannot Add or Remove - only check/uncheck the access column
				listeners: {
					afteredit: function(grid, rowIndex, colIndex) {
						// on checkbox change and if it's a parent - synchronize it's children
						if (grid.colModel) {
							const accessAllowedCheckbox = grid.colModel.findColumnIndex('accessAllowed');
							if (TCG.isEquals(colIndex, accessAllowedCheckbox)) {

								const thisRow = grid.store.data.items[rowIndex];
								const v = thisRow.data.accessAllowed;
								const resourceId = thisRow.json.securityResource.id;
								let i = 0;
								const items = grid.store.data.items;
								let row = items[i];

								// Where row's parent = this security resource id = synchronize the checkbox values
								while (row) {
									if (TCG.isEquals(TCG.getValue('securityResource.parent.id', row.json), resourceId)) {
										grid.startEditing(i, colIndex);
										row.set('accessAllowed', v);
										grid.stopEditing();
									}
									row = items[i++];
								}
							}
						}
					}
				},

				columnsConfig: [
					{header: 'ID', hidden: true, width: 50, dataIndex: 'id', useNull: true},
					{header: 'Parent Security Resource', width: 400, hidden: true, dataIndex: 'securityResource.parent.name'},
					{
						header: 'Security Resource', width: 400, dataIndex: 'securityResource.name',
						renderer: function(v, metaData, r) {
							if (Ext.isArray(r.json.fileCategoryList) && r.json.fileCategoryList.length > 0) {
								let tooltip = 'File Categories:<br/><ul class="c-list">';
								for (let j = 0; j < r.json.fileCategoryList.length; j++) {
									tooltip += '<li>' + r.json.fileCategoryList[j].name + '</li>';
								}
								tooltip += '</ul>';
								metaData.attr = TCG.renderQtip(tooltip);
							}

							if (TCG.isBlank(r.json.securityResource.parent)) {
								return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
							}
							return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
						}
					},
					{header: 'Access', width: 100, dataIndex: 'accessAllowed', type: 'boolean', editor: {xtype: 'checkbox'}}
				]
			}

		]
	}]
});
