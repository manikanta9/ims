Clifton.portal.security.user.UserWizardWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Create New User',
	iconCls: 'user',
	width: 750,
	height: 500,

	okButtonText: 'Continue',
	okButtonTooltip: 'Save the user and continue to adding an assignment.',

	items: [{
		xtype: 'portal-formpanel',
		labelWidth: 160,
		url: 'portalSecurityUser.json',

		listeners: {
			// After Creating - switch to the real window
			// Right now this opens new window and closes this one -
			// Not sure the best way to just switch this window
			aftercreate: function(panel, closeOnSuccess) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				const config = {
					defaultData: {securityUser: panel.getForm().formValues},
					openerCt: panel.ownerCt.openerCt
				};
				TCG.createComponent('Clifton.portal.security.user.UserAssignmentWizardWindow', config);
			}
		},

		items: [
			{fieldLabel: 'User Name/E-mail', name: 'username', readOnly: true},
			{fieldLabel: 'First Name', name: 'firstName', allowBlank: false},
			{fieldLabel: 'Last Name', name: 'lastName', allowBlank: false},
			{fieldLabel: 'Title', name: 'title', allowBlank: false},
			{fieldLabel: 'Company Name', name: 'companyName', allowBlank: false},
			{fieldLabel: 'Phone Number', name: 'phoneNumber', allowBlank: false},
			{
				xtype: 'fieldset',
				title: 'Password',
				collapsed: true,
				instructions: 'Optionally define a tempoarary password for the user.  Users will be required to reset their password after their first successful login.  If you do not assign a password, the user could use Forgot Password to have the system generate a temporary one for them.' +
					'<ul class="c-list">' +
					'<li>Passwords must be at least 8 characters long (not longer than 30)</li>' +
					'<li>Passwords cannot contain all or part of the user name</li>' +
					'<li>Contains elements from three of the following four categories:' +
					'<ul class="c-list">' +
					'<li>English uppercase characters (A through Z)</li>' +
					'<li>English lowercase characters (a through z)</li>' +
					'<li>Numeric characters (0 through 9)</li>' +
					'<li>Non-alphanumeric (for example, !, $, #, %)</li>' +
					'</ul>' +
					'</li>' +
					'<li>Passwords cannot contain sequential numeric or alpha characters (i.e. abc, 123, qwerty)</li>' +
					'<li>Passwords cannot contain the same character in succession more than 3 times (i.e. 1111)</li>' +
					'</ul>',
				items: [
					{fieldLabel: 'Password', name: 'password', inputType: 'password'},
					{
						fieldLabel: 'Confirm Password', name: 'passwordConfirm', inputType: 'password', requiredFields: ['password'], allowBlank: false, submitValue: false,
						validator: function(v) {
							const formPanel = TCG.getParentFormPanel(this);
							if (formPanel.getForm().findField('password').getValue() !== v) {
								return 'Passwords do not match';
							}
							return true;
						}
					}
				]
			}
		]

	}]
});
