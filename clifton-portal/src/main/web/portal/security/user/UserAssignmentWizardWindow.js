Clifton.portal.security.user.UserAssignmentWizardWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Create New User Assignment',
	iconCls: 'user',
	width: 750,
	height: 200,

	okButtonText: 'Continue',
	okButtonTooltip: 'Save the assignment and continue to editing security permissions.',

	items: [{
		xtype: 'formpanel',
		labelWidth: 160,
		loadValidation: false,
		url: 'portalSecurityUserAssignment.json',
		labelFieldName: 'securityUser.username',
		getSaveURL: function() {
			return 'portalSecurityUserAssignmentBulkSave.json';
		},

		listeners: {
			// After Creating - switch to the real window
			// Right now this opens new window and closes this one -
			// Not sure the best way to just switch this window
			aftercreate: function(panel, closeOnSuccess) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				const config = {
					defaultDataIsReal: true,
					defaultData: panel.getForm().formValues,
					openerCt: panel.ownerCt.openerCt
				};
				TCG.createComponent('Clifton.portal.security.user.UserAssignmentWindow', config);
			}
		},

		items: [
			{xtype: 'hidden', name: 'securityUser.id'},
			{fieldLabel: 'Security User', name: 'securityUser.username', xtype: 'displayfield'},
			{
				fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', hiddenName: 'portalEntity.id', xtype: 'combo',
				url: 'portalEntityListFind.json?securableEntity=true&excludeEntityTypeName=Client Account&active=true', displayField: 'entityLabelWithSourceSection',
				beforequery: function(queryEvent) {
					if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('entitySearchAll').checked)) {
						queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', null);
					}
					else {
						queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', true);
					}
				}
			},
			{
				xtype: 'listfield',
				allowBlank: true,
				name: 'additionalEntities',
				fieldLabel: 'Additional Entities',
				qtip: 'Select additional entities to create user assignments with a list of portal entities',

				controlConfig: {
					fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', hiddenName: 'portalEntity.id', valueField: 'id', displayfield: 'label', xtype: 'combo',
					url: 'portalEntityListFind.json?securableEntity=true&excludeEntityTypeName=Client Account&active=true', detailIdField: 'portalEntity.entityLabelWithSourceSection',
					beforequery: function(queryEvent) {
						if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('entitySearchAll').checked)) {
							queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', null);
						}
						else {
							queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', true);
						}
					}
				}
			},
			{
				boxLabel: 'Leave unchecked to search for Entities with Approved Client Specific Documents Posted Only.', xtype: 'checkbox', name: 'entitySearchAll', submitValue: 'false',
				qtip: 'If checked, will search ONLY for portal entities that have a non-global approved file posted.',
				listeners: {
					check: function(f) {
						// On changes reset contract drop down
						const p = TCG.getParentFormPanel(f);
						const bc = p.getForm().findField('portalEntity.entityLabelWithSourceSection');
						bc.clearValue();
						bc.store.removeAll();
						bc.lastQuery = null;
					}
				}
			},
			{
				fieldLabel: 'Assignment Role', name: 'assignmentRole.name', hiddenName: 'assignmentRole.id', xtype: 'combo', url: 'portalSecurityUserRoleListFind.json?portalEntitySpecific=true', requiredFields: ['portalEntity.id'],
				beforequery: function(queryEvent) {
					const portalEntityId = queryEvent.combo.getParentForm().getFormValue('portalEntity.id');
					if (!TCG.isBlank(portalEntityId)) {
						queryEvent.combo.store.setBaseParam('assignmentForPortalEntityId', portalEntityId);
					}
				}
			}

			/*
					{
						xtype: 'fieldset', title: 'Additional Entities', collapsible: true, collapsed: true, allowBlank: true, displayField: 'entityLabelWithSourceSection',
						items: {
							fieldlabel: 'Entities', xtype: 'listfield', name: 'assignmentRole.namelist',
							controlConfig: {
								fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', hiddenName: 'portalEntity.id', xtype: 'combo', url: 'portalEntityListFind.json?securableEntity=true&excludeEntityTypeName=Client Account&active=true', displayField: 'entityLabelWithSourceSection',
								beforequery: function(queryEvent) {
									if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('entitySearchAll').checked)) {
										queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', null);
									}
									else {
										queryEvent.combo.store.setBaseParam('securableEntityHasApprovedFilesPosted', true);
									}
								}
							}
						}
					}
			 */

		]
	}]
});
