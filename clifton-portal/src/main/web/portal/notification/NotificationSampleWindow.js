Clifton.portal.notification.NotificationSampleWindow = function(conf) {
	this.win = new TCG.app.Window(Ext.apply({
		title: 'Notification Sample',
		width: 900,
		height: 600,
		buttons: [{
			text: 'Close',
			tooltip: 'Close this window',
			handler: function() {
				this.ownerCt.ownerCt.closeWindow();
			}
		}],

		closeWindow: function() {
			const win = this;
			win.closing = true;
			win.close();
		},

		items: [{
			xtype: 'portal-formpanel',
			readOnly: true,
			instructions: 'The following is a sample notification generated for selected Notification Definition, Subject, & Text templates.',
			url: 'portalNotificationSamplePreview.json',
			items: [
				{fieldLabel: 'Recipient', name: 'coalesceRecipientLabel', xtype: 'displayfield'},
				{fieldLabel: 'Subject', name: 'subject', xtype: 'textarea', height: 35},
				{
					fieldLabel: 'Text', name: 'text', xtype: 'htmleditor', readOnly: true, anchor: '-35 -100',
					listeners: {
						afterrender: function() {
							this.getToolbar().hide();
						}
					}
				}
			]
		}]
	}, conf));
};
