Clifton.portal.notification.setup.NotificationSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'portalNotificationSetupWindow',
	title: 'Notification Setup',
	iconCls: 'email',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Notifications',
				items: [{
					name: 'portalNotificationListFind',
					xtype: 'gridpanel',

					columns: [
						{header: 'Definition Name', width: '100', dataIndex: 'notificationDefinition.name', filter: {searchFieldName: 'notificationDefinitionId', type: 'combo', url: 'portalNotificationDefinitionListFind.json'}},
						{header: 'Recipient', width: 100, dataIndex: 'coalesceRecipientLabel', filter: {searchFieldName: 'recipientSearchPattern'}},
						{header: 'Subject', width: 100, dataIndex: 'subject'},
						{header: 'Sent', width: 50, dataIndex: 'sentDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Pop-Up', width: 50, type: 'boolean', dataIndex: 'notificationDefinition.popUpNotification', filter: {searchFieldName: 'popUpNotification'}},
						{
							header: 'Error', width: 50, dataIndex: 'error', type: 'boolean',
							renderer: function(v, metaData, r) {
								return TCG.renderBoolean(v, r.data.errorMessage);
							}
						},
						{header: 'Error Message', width: 200, dataIndex: 'errorMessage', hidden: true},
						{header: 'Re-send', width: 50, dataIndex: 'resend', type: 'boolean'},
						{
							header: 'Acknowledged', width: 50, dataIndex: 'acknowledged', type: 'boolean', tooltip: 'Acknowledgements only apply to pop-up notifications',
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.data['notificationDefinition.popUpNotification'])) {
									return TCG.renderBoolean(v);
								}
								return '&nbsp;';
							}
						}

					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.notification.NotificationWindow'
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'ALL_NOTIFICATIONS',
								store: {
									xtype: 'arraystore',
									data: [['ALL_NOTIFICATIONS', 'All Notifications', 'Display notifications sent internally and externally'], ['INTERNAL_NOTIFICATIONS', 'Internal Notifications', 'Display notifications sent internally (PPA Employees)'], ['EXTERNAL_NOTIFICATIONS', 'External Notifications', 'Display notifications for external (client) users only.']]
								}
							},
							{fieldLabel: 'Recipient Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					topToolbarSearchParameter: 'recipientSearchPattern',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('sentDate', {'after': new Date().add(Date.DAY, -1)});
						}

						const t = this.getTopToolbar();
						const displayType = TCG.getChildByName(t, 'displayType');
						const params = {};
						if (displayType.getValue() === 'INTERNAL_NOTIFICATIONS') {
							params.internalNotification = true;
						}
						else if (displayType.getValue() === 'EXTERNAL_NOTIFICATIONS') {
							params.internalNotification = false;
						}
						return params;
					},
					addToolbarButtons: function(toolbar, gridPanel) {
						toolbar.add({
							text: 'Re-send',
							tooltip: 'Re-send the notifications.  Generates/sends notifications to the original recipient',
							iconCls: 'run',
							handler: function() {
								gridPanel.runSelectedDefinition();
							}
						});
						toolbar.add('-');
						toolbar.add({
							text: 'Send Test Notification',
							tooltip: 'Open a window to generate and send a test email based on the selected info',
							iconCls: 'run',
							scope: this,
							handler: function() {
								TCG.createComponent('Clifton.portal.notification.NotificationTestSendWindow');
							}
						});
						toolbar.add('-');
					},
					runSelectedDefinition: function() {
						const gridPanel = this;
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to run notifications for.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selections are not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else if (TCG.isTrue(sm.getSelected().get('notificationDefinition.popUpNotification'))) {
							TCG.showError('Selected notification is a pop-up notification.  You cannot re-send a pop up notification');
						}
						else {
							Ext.Msg.confirm('Run Notifications', 'Are you sure you want to generate and send notifications for selected definition?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Generating...',
										params: {
											commandType: 'SEND',
											notificationId: sm.getSelected().id
										},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('portalNotificationResendGenerate.json');
								}
							});
						}
					}
				}]
			},


			{
				title: 'Notification Acknowledgements',
				items: [{

					xtype: 'portal-tracking-event-list-grid',
					instructions: 'The following acknowledgements events apply notifications.  Note: If a notification is set to confirm at the entity level, every entity that applies to the notification for the recipient is tracked separately.',
					eventTypeName: 'Portal Notification Acknowledgement',

					columnOverrides: [
						{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false}
					],

					// Event Type Specific Columns
					additionalColumns: [
						{header: 'Recipient', width: 150, dataIndex: 'portalNotification.coalesceRecipientLabel', nonPersistentField: true},
						{header: 'Notification Subject', width: 150, dataIndex: 'portalNotification.subject', nonPersistentField: true},
						{
							header: 'Acknowledged', width: 50, dataIndex: 'description', align: 'center',
							renderer: function(v, metaData, r) {
								if (TCG.isEquals(v, 'true')) {
									return TCG.renderEvalResultIcon('Success');
								}
								if (TCG.isEquals(v, 'false')) {
									return TCG.renderEvalResultIcon('Failure');
								}
								return v;
							}
						}
					],

					getTopToolbarFilters: function(toolbar) {
						return []; // Not Used
					}
				}]
			},

			{
				title: 'Subscriptions',
				items: [{
					name: 'portalNotificationSubscriptionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'userSearchPattern',
					additionalPropertiesToRequest: 'id|userAssignment.securityUser.id|portalEntity.id',
					getTopToolbarFilters: function(toolbar) {
						if (this.topToolbarSearchParameter) {
							return [{fieldLabel: 'Recipient Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}];
						}
						return undefined;
					},
					columns: [
						{header: 'User', width: 100, dataIndex: 'userAssignment.securityUser.label', filter: {searchFieldName: 'userSearchPattern'}},
						{header: 'User name', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.username', filter: {searchFieldName: 'username'}},
						{header: 'E-mail', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.emailAddress', filter: {searchFieldName: 'emailAddress'}},
						{header: 'Company', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.companyName', filter: {searchFieldName: 'userCompanyName'}},
						{header: 'Title', hidden: true, width: 150, dataIndex: 'userAssignment.securityUser.title', filter: {searchFieldName: 'userTitle'}},
						{header: 'Phone Number', hidden: true, width: 75, dataIndex: 'userAssignment.securityUser.phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
						{header: 'Assigned Entity', width: 100, hidden: true, dataIndex: 'userAssignment.portalEntity.entityLabelWithSourceSection', filter: false},
						{header: 'Entity', width: 175, dataIndex: 'portalEntity.entityLabelWithSourceSection', filter: {searchFieldName: 'portalEntityId', type: 'combo', url: 'portalEntityListFind.json?orderBy=labelExpanded&securableEntity=true&securableEntityHasApprovedFilesPosted=true&active=true'}},
						{header: 'Definition Type', width: 50, hidden: true, dataIndex: 'notificationDefinitionType.name', filter: {searchFieldName: 'portalNotificationDefinitionTypeId', type: 'combo', url: 'portalNotificationTypeListFind.json?subscriptionAllowed=true&subscriptionForFileCategory=false'}},
						{header: 'File Category', width: 50, hidden: true, dataIndex: 'fileCategory.name', filter: {searchFieldName: 'portalFileCategoryId', type: 'combo', url: 'portalFileCategoryListFind.json?notificationSubscriptionForFileCategory=true'}},
						{header: 'Notification', width: 100, dataIndex: 'coalesceDefinitionTypeFileCategoryName'},
						{header: 'Subscribed', width: 50, dataIndex: 'notificationEnabled', type: 'boolean'},
						{header: 'Frequency', width: 75, dataIndex: 'subscriptionFrequencyLabel', tooltip: 'Optional frequency of notifications.  Selections are allowed for file categories where file frequency allows.', filter: {searchFieldName: 'subscriptionFrequency', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.portal.notification.subscription.SubscriptionFrequencies}}}
					],

					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('notificationEnabled', true);
						}
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.notification.subscription.NotificationSubscriptionEntryWindow',
						getDefaultData: function(gridPanel, row) {
							return {
								securityUser: {id: row.json.userAssignment.securityUser.id},
								portalEntity: {id: row.json.portalEntity.id}
							};
						}
					},
					addToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Rebuild User Subscriptions',
							tooltip: 'As assignments are updated, the notification subscriptions for each user should automatically rebuild.  For example, a user assignment is disabled, their notification subscriptions are removed.  Admins can use this feature to force rebuild of subscriptions for a specific user (or all users).  Existing subscriptions are not removed as long as their security still applies.',
							iconCls: 'run',
							scope: this,
							handler: function() {
								const grid = this.grid;
								const sm = grid.getSelectionModel();
								if (sm.getCount() === 0) {
									Ext.Msg.confirm('Confirm All User Subscriptions Rebuild', 'Are you sure you want to rebuild subscriptions for all users?  Existing subscriptions are not removed as long as their security still applies.', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: grid,
												waitMsg: 'Rebuilding...'
											});
											loader.load('portalNotificationSubscriptionListRebuild.json');
										}
									});
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection rebuilds are not supported.  Please select one user.', 'NOT SUPPORTED');
								}
								else {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										waitMsg: 'Rebuilding...',
										params: {
											portalSecurityUserId: sm.getSelected().json.userAssignment.securityUser.id
										}
									});
									loader.load('portalNotificationSubscriptionListForUserRebuild.json');
								}
							}
						});
						toolBar.add('-');

					}

				}]
			},


			{
				title: 'Definitions',
				items: [{
					name: 'portalNotificationDefinitionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',

					groupField: 'definitionType.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Definitions" : "Definition"]})',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Type', width: 100, dataIndex: 'definitionType.name', hidden: true},
						{header: 'Definition Name', width: 150, dataIndex: 'name'},
						{header: 'Definition Description', width: 200, dataIndex: 'description', renderer: TCG.renderValueWithTooltip},
						{header: 'Pop-Up', width: 50, dataIndex: 'popUpNotification', type: 'boolean', tooltip: 'If checked a user will receive a pop-up upon succesful login vs. email being sent.  Pop-ups also track acknowledgements'},
						{header: 'Notification Recurrence', width: 100, dataIndex: 'notificationRecurrence', hidden: true},
						{header: 'Recurrence Interval', width: 40, dataIndex: 'notificationRecurrenceInterval', type: 'int', useNull: true, hidden: true},
						{header: 'Start Time', width: 40, dataIndex: 'startTime', hidden: true},
						{header: 'End Time', width: 40, dataIndex: 'endTime', hidden: true},
						{header: 'Run Frequency (Minutes)', width: 40, dataIndex: 'runFrequencyInMinutes', type: 'int', useNull: true, hidden: true},
						{
							header: 'Internal Notification', width: 50, dataIndex: 'definitionType.internalNotification', type: 'boolean', filter: {searchFieldName: 'internalNotification'},
							tooltip: 'Internal notifications are sent to PPA employees or internal mailing lists and may not be sent to an individual user.'
						},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
						{header: 'Skip Pending Termination', width: 50, dataIndex: 'skipPendingTermination', type: 'boolean', tooltip: 'If checked a user will not be sent the notification if all of their assignments are pending termination.'},
						{header: 'Subscription Allowed', width: 50, dataIndex: 'definitionType.subscriptionAllowed', type: 'boolean'},
						{header: 'File Category Subscription', width: 50, dataIndex: 'definitionType.subscriptionForFileCategory', type: 'boolean'},
						{header: 'E-mail Subject Template', width: 300, hidden: true, dataIndex: 'emailSubjectTemplate'}
					],
					editor: {
						detailPageClass: 'Clifton.portal.notification.setup.DefinitionWindow',
						copyURL: 'portalNotificationDefinitionCopy.json'
					},

					addToolbarButtons: function(toolbar, gridPanel) {
						toolbar.add({
							text: 'Activate',
							tooltip: 'Activate Selected Definition.  If the definition type is not set up for subscriptions for file categories, then there can only be one active at a time and the existing active definition will be de-activated.',
							iconCls: 'row_reconciled',
							handler: function() {
								gridPanel.activateSelectedDefinition(true);
							}
						});
						toolbar.add('-');
						toolbar.add({
							text: 'Deactivate',
							tooltip: 'Deactivate Selected Definition',
							iconCls: 'cancel',
							handler: function() {
								gridPanel.activateSelectedDefinition(false);
							}
						});
						toolbar.add('-');
						toolbar.add({
							text: 'Preview',
							tooltip: 'Preview this Notification.  Various options allow for just previewing results, viewing a sample, or sending test emails to yourself.',
							iconCls: 'preview',
							handler: function() {
								gridPanel.previewRunSelectedDefinition();
							}
						});
						toolbar.add('-');
						toolbar.add({
							text: 'Run',
							tooltip: 'Run the notifications.  Generates/sends notifications',
							iconCls: 'run',
							handler: function() {
								gridPanel.runSelectedDefinition();
							}
						});
						toolbar.add('-');
					},

					activateSelectedDefinition: function(active) {
						const gridPanel = this;
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to be ' + (TCG.isTrue(active) ? 'activated.' : 'de-activated.'), 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selections are not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							let confirmTitle = 'Activate Selected Row';
							let confirmMessage = 'Are you sure you would like to activate selected definition?<br/><b>Note:</b> If the definition type is not associated with subscriptions for the file category, the currently active definition for this type will be de-activated.';
							if (!TCG.isTrue(active)) {
								confirmTitle = 'De-' + confirmTitle;
								confirmMessage = 'Are you sure you would like to de-activate selected definition?';
							}
							Ext.Msg.confirm(confirmTitle, confirmMessage, function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: active ? 'Activating...' : 'De-Activating',
										params: {
											id: sm.getSelected().id,
											active: active
										},
										onLoad: function(data, conf) {
											gridPanel.reload();
										}
									});
									loader.load('portalNotificationDefinitionActive.json');

								}
							});
						}
					},

					previewRunSelectedDefinition: function() {
						const gridPanel = this;
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to preview a run for.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selections are not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							const params = {portalNotificationDefinitionId: sm.getSelected().id};
							TCG.createComponent('Clifton.portal.notification.NotificationRunPreviewWindow', {
								params: params,
								openerCt: gridPanel
							});
						}
					},
					runSelectedDefinition: function() {
						const gridPanel = this;
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a row to run notifications for.', 'No Row(s) Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selections are not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							Ext.Msg.confirm('Run Notifications', 'Are you sure you want to generate and send notifications for selected definition?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Generating...',
										params: {
											portalNotificationDefinitionId: sm.getSelected().id,
											commandType: 'SEND'
										}
									});
									loader.load('portalNotificationListGenerate.json');
								}
							});
						}
					}
				}]
			},


			{
				title: 'Definition Types',
				items: [{
					name: 'portalNotificationDefinitionTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Type Description', hidden: true, width: 200, dataIndex: 'description', renderer: TCG.renderValueWithTooltip},
						{
							header: 'Internal Notification', width: 50, dataIndex: 'internalNotification', type: 'boolean', filter: {searchFieldName: 'internalNotification'},
							tooltip: 'Internal notifications are sent to PPA employees or internal mailing lists and may not be sent to an individual user.'
						},
						{header: 'Subscription Allowed', width: 50, dataIndex: 'subscriptionAllowed', type: 'boolean'},
						{header: 'File Category Subscription', width: 50, dataIndex: 'subscriptionForFileCategory', type: 'boolean'},
						{header: 'File Notification', width: 50, dataIndex: 'fileNotification', type: 'boolean'},
						{header: 'Security Resource', width: 50, dataIndex: 'securityResource.name'},
						{header: 'Triggered By Event', width: 50, dataIndex: 'triggeredByEvent', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.notification.setup.DefinitionTypeWindow'
					}
				}]
			},


			{
				title: 'Acknowledgement Types',
				items: [{
					name: 'portalNotificationAcknowledgementTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Type Description', hidden: true, width: 200, dataIndex: 'description', renderer: TCG.renderValueWithTooltip},

						{header: 'Confirm Used', width: 50, type: 'boolean', dataIndex: 'confirmUsed'},
						{header: 'Confirm Button', width: 50, dataIndex: 'confirmButtonLabel'},
						{header: 'Confirm Tooltip', width: 150, hidden: true, dataIndex: 'confirmButtonTooltip'},
						{header: 'Confirm Entity', width: 50, dataIndex: 'confirmEntity', type: 'boolean', tooltip: 'Confirmation applies to the portal entity the user has access to.  This allows the concept of "at least one" must confirm and the system will silently acknowledge the notification for other users under the same portal entities.'},

						{header: 'Decline Used', width: 50, type: 'boolean', dataIndex: 'declineUsed'},
						{header: 'Decline Button', width: 50, dataIndex: 'declineButtonLabel'},
						{header: 'Decline Tooltip', width: 150, hidden: true, dataIndex: 'declineButtonTooltip'},

						{header: 'Cancel Used', width: 50, type: 'boolean', dataIndex: 'cancelUsed'},
						{header: 'Cancel Button', width: 50, dataIndex: 'cancelButtonLabel'},
						{header: 'Cancel Tooltip', width: 150, hidden: true, dataIndex: 'cancelButtonTooltip'},
						{header: 'Cancel Acknowledgement', width: 50, dataIndex: 'cancelAcknowledgement', type: 'boolean', tooltip: 'Cancel button selection is considered an acknowledgement.  If false, it acts like a defer and user will receive the pop up again after next successful login.'}
					],
					editor: {
						detailPageClass: 'Clifton.portal.notification.setup.AcknowledgementTypeWindow',
						copyURL: 'portalNotificationAcknowledgementTypeCopy.json'
					}
				}]
			},

			{
				title: 'Holidays',
				items: [{
					name: 'portalHolidayListFind',
					xtype: 'gridpanel',
					forceFit: false,
					instructions: 'The following dates are considered to be holidays in the portal.  These are used for notification subscriptions where we send notifications out on the first business day of the week, month, quarter, etc.',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Date', width: 150, dataIndex: 'holidayDate'}
					],

					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Start Date', xtype: 'toolbar-datefield', name: 'startDate', value: new Date().add(Date.DAY, -90).format('m/d/Y')},
							{fieldLabel: 'End Date', xtype: 'toolbar-datefield', name: 'endDate', value: new Date().add(Date.DAY, 90).format('m/d/Y')}
						];
					},

					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const start = TCG.getChildByName(t, 'startDate');
						const end = TCG.getChildByName(t, 'endDate');

						return {
							startDate: TCG.isNotBlank(start.getValue()) ? (start.getValue()).format('m/d/Y') : null,
							endDate: TCG.isNotBlank(end.getValue()) ? (end.getValue()).format('m/d/Y') : null
						};
					}
				}]
			},

			{
				title: 'Scheduled Runs',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					name: 'portalRunnerConfigListFind',
					restartUrl: 'portalRunnerControllerReschedule.json',
					clearCompletedUrl: 'portalRunnerCompletedClear.json',
					typeName: 'PORTAL_NOTIFICATION_DEFINITION'
				}]
			}
		]
	}
	]
});


