Clifton.portal.notification.setup.DefinitionWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Notification Definition',
	iconCls: 'email',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'formwithtabs',
		url: 'portalNotificationDefinition.json',
		loadValidation: false,

		listeners: {
			afterload: function(panel) {
				if (TCG.isTrue(this.getFormValue('active'))) {
					this.setReadOnly(true);

					// Take read only off of name and description and skip pending termination
					this.setReadOnlyField('name', false);
					this.setReadOnlyField('description', false);
					this.setReadOnlyField('skipPendingTermination', false);
				}
				panel.resetDefinitionTypeFields(this.getFormValue('definitionType'));
			}
		},

		resetDefinitionTypeFields: function(definitionType) {
			const fp = this;
			if (definitionType && TCG.isTrue(definitionType.triggeredByEvent)) {
				TCG.getChildByName(fp, 'recurrenceFieldSet').setVisible(false);
				TCG.getChildByName(fp, 'runFieldSet').setVisible(false);
			}
			else {
				TCG.getChildByName(fp, 'recurrenceFieldSet').setVisible(true);
				TCG.getChildByName(fp, 'runFieldSet').setVisible(true);
			}
			if (definitionType && TCG.isTrue(definitionType.fileNotification)) {
				this.showField('fileApprovedDaysBack');
			}
			else {
				this.hideField('fileApprovedDaysBack');
			}
			if (definitionType && TCG.isTrue(definitionType.internalNotification)) {
				this.hideField('popUpNotification');
				this.hideField('acknowledgementType.name');
			}
			else {
				this.showField('popUpNotification');
				this.showField('acknowledgementType.name');
			}
			// Freemarker Instructions:
			let freemarkerInstructions = '<b>Freemarker Options: </b>The following beans are available in the templates:';
			freemarkerInstructions += '<ul class="c-list">';
			if (definitionType && TCG.isTrue(definitionType.internalNotification)) {
				freemarkerInstructions += '<li>recipientEmailAddress: Recipient Email Address that is receiving the email.  Used for internal notifications that go to a mailing list.</li>';
			}
			else {
				freemarkerInstructions += '<li>recipientUser: Portal Security User that is receiving the email.  Useful for personalizing the greeting.</li>';
				freemarkerInstructions += '<li>entityViewTypeList: The list of view types available for selected user.  Usually just one for Institutional or Retail, but could have both apply. Can be used to customize the default contact email address.</li>';
			}
			if (TCG.isEquals('Portal User: Forgot Password', definitionType.name) || TCG.isEquals('Portal User: Welcome', definitionType.name)) {
				freemarkerInstructions += '<li>newPassword: Random Generated Password for the User</li>';
			}
			if (TCG.isEquals('Portal File Posted', definitionType.name) || TCG.isEquals('Management Fee Invoice Overdue', definitionType.name) || TCG.isEquals('Management Fee Invoice Paid', definitionType.name)) {
				freemarkerInstructions += '<li>fileList: Portal File Extended List of files the notification is for.</li>';
			}
			if (TCG.isEquals('Portal Feedback Entry', definitionType.name)) {
				freemarkerInstructions += '<li>feedbackEntryList: Feedback Results grouped by User and Date. The list contains a list of results for each question (resultList)</li>';
			}
			if (TCG.isEquals('Portal Files Pending', definitionType.name)) {
				freemarkerInstructions += '<li>fileList: List of files included in the notification</li>';
				freemarkerInstructions += '<li>userList: List of create users for the files.  Used to write out user names for the file create user.</li>';
				freemarkerInstructions += '<li>loadWindowParams: The window params to pass to the File Management Window for filtering in the application the same results that are in the email.</li>';
			}
			if (TCG.isEquals('Portal Administrator: New File Category Available', definitionType.name)) {
				freemarkerInstructions += '<li>fileCategoryPermissionList: List of custom objects that include references to: PortalFileCategory and PortalSecurityUserAssignmentResource</li>';
				freemarkerInstructions += '<li>categoryList: List of unique PortalFileCategory objects included.</li>';
			}
			freemarkerInstructions += '</ul>';
			freemarkerInstructions += '<b>Images: </b> The following images are available for insertion into the email body. Enter them exactly as listed below. The image will be added as an attachment and inserted into the email at the specified location. In order to preview the image you must send a test email (see Preview screen for options).';
			freemarkerInstructions += '<ul class="c-list">';
			freemarkerInstructions += '<li>[IMAGE_ppa-logo-small]</li>';

			const ff = TCG.getChildByName(this, 'messageFormFragment');
			if (TCG.isTrue(this.freemarkerInstructionsLoaded)) {
				ff.remove(0);
			}
			ff.insert(0, {xtype: 'label', html: freemarkerInstructions + '<hr />'});
			this.freemarkerInstructionsLoaded = true;
		},
		freemarkerInstructionsLoaded: false,

		items: [{
			xtype: 'tabpanel',
			requiredFormIndex: [0, 1],
			items: [
				{
					title: 'Info',
					items: [{
						xtype: 'formfragment',
						labelWidth: 160,

						getWarningMessage: function(form) {
							if (TCG.isTrue(form.formValues.active)) {
								return 'This notification is currently active and changes are not allowed.';
							}
							return undefined;
						},

						items: [
							{
								fieldLabel: 'Definition Type', name: 'definitionType.name', hiddenName: 'definitionType.id', xtype: 'combo', url: 'portalNotificationDefinitionTypeListFind.json', detailPageClass: 'Clifton.portal.notification.setup.DefinitionTypeWindow',
								requestedProps: ['triggeredByEvent|fileNotification|internalNotification'],
								listeners: {
									select: function(combo, record, index) {
										const fp = this.getParentForm();
										fp.setFormValue('definitionType.triggeredByEvent', record.json.triggeredByEvent);
										fp.resetDefinitionTypeFields(record.json);
									}
								}
							},
							{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Notifications for this definition type are triggered by events, and not scheduled.', name: 'definitionType.triggeredByEvent', submitValue: false, disabled: true},
							{fieldLabel: 'Definition Name', name: 'name'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
							{fieldLabel: '', boxLabel: 'Active', name: 'active', xtype: 'checkbox', disabled: true},
							{fieldLabel: '', boxLabel: 'Skip Notifications to Users Pending Termination', name: 'skipPendingTermination', xtype: 'checkbox'},

							{xtype: 'label', html: '<hr/>'},
							{fieldLabel: '', boxLabel: 'Pop-up Notification (leave un-checked for email notifications)', name: 'popUpNotification', xtype: 'checkbox'},
							{fieldLabel: 'Acknowledgement Type', name: 'acknowledgementType.name', hiddenName: 'acknowledgementType.id', xtype: 'combo', url: 'portalNotificationAcknowledgementTypeListFind.json', detailPageClass: 'Clifton.portal.notification.setup.AcknowledgementTypeWindow', allowBlank: false, requiredFields: ['popUpNotification']},

							{
								xtype: 'fieldset',
								title: 'Notification Recurrences',
								name: 'recurrenceFieldSet',
								items: [
									{
										fieldLabel: 'Recurrence', name: 'notificationRecurrence', xtype: 'combo', mode: 'local',
										qtip: 'How often the user should be sent the notification.  Most cases this is NONE for no recurrence as users will get an email only once.  If the notification is for posted files it would imply once per user per file. If Days is used, and interval is one, will use weekdays, otherwise will use calendar days.',
										store: {
											xtype: 'arraystore',
											data: Clifton.portal.notification.PortalNotificationRecurrences
										},
										listeners: {
											select: function(combo, record, index) {
												const hideIntervalField = TCG.isEquals(record.json[0], 'ONCE') || TCG.isEquals(record.json[0], 'NONE');
												const fp = this.getParentForm();
												if (TCG.isTrue(hideIntervalField)) {
													fp.hideField('notificationRecurrenceInterval');
												}
												else {
													fp.showField('notificationRecurrenceInterval');
												}
											}
										}
									},
									{fieldLabel: 'Interval', name: 'notificationRecurrenceInterval', xtype: 'spinnerfield', minValue: 1, maxValue: 100000, requiredFields: ['notificationRecurrence']},
									{
										fieldLabel: 'File Approved Days Back', name: 'fileApprovedDaysBack', xtype: 'spinnerfield', minValue: 1, maxValue: 100000,
										qtip: 'If set, will limit search for files to those approved within given amount of days.  If day count = 1 will use Weekdays, otherwise will use Days.  This is because we do not schedule notifications on weekends.'
									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Schedule Times',
								name: 'runFieldSet',
								items: [
									{fieldLabel: 'Start Time', name: 'startTime', xtype: 'timefield', qtip: 'Time of day this notification should start scheduling'},
									{fieldLabel: 'End Time', name: 'endTime', xtype: 'timefield', requiredFields: ['startTime'], qtip: 'Time of day this notification should stop scheduling.  If blank will run until the end of the day, or if frequency is blank will just run once that day.'},
									{fieldLabel: 'Freq (Minutes)', name: 'runFrequencyInMinutes', xtype: 'spinnerfield', minValue: 5, maxValue: 1440, requiredFields: ['startTime'], qtip: 'Time interval in minutes whe the notification runs are scheduled'}
								]
							}

						]
					}]
				},

				{
					title: 'Message',
					tbar: [
						{
							text: 'Preview',
							tooltip: 'Preview this Notification.  Various options allow for just previewing results, viewing a sample, or sending test emails to yourself.',
							iconCls: 'preview',
							handler: function() {
								const p = TCG.getParentFormPanel(this);
								const f = p.getForm();
								const params = {portalNotificationDefinitionId: p.getWindow().getMainFormId(), subjectTemplate: p.getFormValue('emailSubjectTemplate', true), bodyTemplate: p.getFormValue('emailBodyTemplate', true)};
								TCG.createComponent('Clifton.portal.notification.NotificationRunPreviewWindow', {
									params: params,
									openerCt: f,
									defaultIconCls: p.getWindow().iconCls
								});
							}
						}, '-'
					],
					items: {
						xtype: 'formfragment',
						name: 'messageFormFragment',
						items: [
							{fieldLabel: 'Message Subject', name: 'emailSubjectTemplate'},
							{fieldLabel: 'Message Body', name: 'emailBodyTemplate', xtype: 'textarea', anchor: '-35 -100'}
						]
					}
				},

				{
					title: 'Notifications',
					items: [{
						name: 'portalNotificationListFind',
						xtype: 'gridpanel',

						listeners: {
							beforerender: function(gridpanel) {
								const fp = TCG.getParentFormPanel(gridpanel);
								const cm = this.getColumnModel();
								const popUp = (TCG.isTrue(fp.getFormValue('popUpNotification')));
								cm.setHidden(cm.findColumnIndex('error'), popUp);
								cm.setHidden(cm.findColumnIndex('resend'), popUp);
								cm.setHidden(cm.findColumnIndex('acknowledged'), !popUp);
							}
						},

						columns: [
							{header: 'Recipient', width: 100, dataIndex: 'coalesceRecipientLabel', filter: {searchFieldName: 'recipientSearchPattern'}},
							{header: 'Subject', width: 100, dataIndex: 'subject'},
							{header: 'Sent', width: 50, dataIndex: 'sentDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
							{
								header: 'Error', width: 50, dataIndex: 'error', type: 'boolean',
								renderer: function(v, metaData, r) {
									return TCG.renderBoolean(v, r.data.errorMessage);
								}
							},
							{header: 'Error Message', width: 200, dataIndex: 'errorMessage', hidden: true},
							{header: 'Re-send', width: 50, dataIndex: 'resend', type: 'boolean'},
							{header: 'Acknowledged', width: 50, dataIndex: 'acknowledged', type: 'boolean'}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.portal.notification.NotificationWindow'
						},
						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'Recipient Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},
						topToolbarSearchParameter: 'recipientSearchPattern',
						getTopToolbarInitialLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('sentDate', {'after': new Date().add(Date.DAY, -7)});
							}
							return {notificationDefinitionId: this.getWindow().getMainFormId()};
						}
					}]
				}
			]
		}]
	}
	]
});
