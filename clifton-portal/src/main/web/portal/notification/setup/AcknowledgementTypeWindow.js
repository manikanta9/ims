Clifton.portal.notification.setup.AcknowledgementTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Notification Acknowledgement Type',
	iconCls: 'email',
	width: 900,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',

				items: [{
					xtype: 'portal-formpanel',
					url: 'portalNotificationAcknowledgementType.json',
					instructions: 'Notification Acknowledgement Types are used by pop-up notifications to determine how the user acknowledgement is handled.  Confirm, Decline, and Cancel. The appropriate buttons will only appear to the user when a label is supplied for them.',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Type Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
						{
							xtype: 'fieldset-checkbox',
							title: 'Confirm Acknowledgement (User can positively acknowledge)',
							checkboxName: 'confirmUsed',
							instructions: 'If you would like users to be able to positively acknowledge the notifications, enter at least a button label.  Examples: Confirm, Accept, Agree',
							items: [
								{fieldLabel: 'Confirm Button Label', name: 'confirmButtonLabel'},
								{fieldLabel: 'Confirm Button Tooltip', name: 'confirmButtonTooltip', xtype: 'textarea', requiredFields: ['confirmButtonLabel'], height: 35},
								{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Apply confirm acknowledgements to the portal entity.', name: 'confirmEntity', requiredFields: ['confirmButtonLabel'], qtip: 'When a user positively acknowledges, other users under the same portal entity will be silently acknowledged and they will not have to accept themselves'}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Decline Acknowledgement (User can negatively acknowledge)',
							checkboxName: 'declineUsed',
							instructions: 'If you would like users to be able to negatively acknowledge the notifications, enter at least a button label.  Examples: Decline, Do Not Accept',
							items: [
								{fieldLabel: 'Decline Button Label', name: 'declineButtonLabel'},
								{fieldLabel: 'Decline Button Tooltip', name: 'declineButtonTooltip', xtype: 'textarea', requiredFields: ['declineButtonLabel'], height: 35}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Cancel / No Response Acknowledgement (User can close the notification without positive or negative acknowledgement)',
							checkboxName: 'cancelUsed',
							instructions: 'If you would like users to be able to just close the notification without tracking positive or negative acknowledgements, enter at least a button label.  Examples: Cancel, Close',
							items: [

								{fieldLabel: 'Cancel Button Label', name: 'cancelButtonLabel'},
								{fieldLabel: 'Cancel Button Tooltip', name: 'cancelButtonTooltip', xtype: 'textarea', requiredFields: ['cancelButtonLabel'], height: 35},
								{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Track <b>Cancel</b> as an Acknowledgement', name: 'cancelAcknowledgement', requiredFields: ['cancelButtonLabel'], qtip: 'When a user selects cancel, and this is checked, that is considered an acknowledgement and they will no longer receive the pop up notification.  Otherwise the notification is deferred and they will receive it again upon subsequent log ins.'}
							]
						}
					]
				}]
			},

			{
				title: 'Notification Definitions',
				items: [{
					name: 'portalNotificationDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'The following notification definitions are using this acknowledgement type.',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Type', width: 100, dataIndex: 'definitionType.name', hidden: true},
						{header: 'Definition Name', width: 150, dataIndex: 'name'},
						{header: 'Definition Description', width: 200, dataIndex: 'description', renderer: TCG.renderValueWithTooltip},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						const w = this.getWindow();
						return {
							acknowledgementTypeId: w.getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.portal.notification.setup.DefinitionWindow',
						drillDownOnly: true
					}
				}]

			}
		]
	}]
});
