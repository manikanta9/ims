Clifton.portal.notification.setup.DefinitionTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Notification Definition Type',
	iconCls: 'email',
	width: 750,
	height: 300,

	items: [{
		xtype: 'portal-formpanel',
		url: 'portalNotificationDefinitionType.json',
		readOnly: true,

		items: [
			{fieldLabel: 'Type Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: '', boxLabel: 'Subscriptions Allowed', qtip: 'If checked, users are allowed to subscribe to this definition', name: 'subscriptionAllowed', xtype: 'checkbox'},
			{fieldLabel: '', boxLabel: 'File Category Subscriptions', qtip: 'If checked, subscriptions are defined through the file categories users have access to', name: 'subscriptionForFileCategory', xtype: 'checkbox'},
			{fieldLabel: '', boxLabel: 'File Notification', qtip: 'If checked, each notification is associated with a list of files', name: 'fileNotification', xtype: 'checkbox'},
			{fieldLabel: '', boxLabel: 'Triggered By Event', qtip: 'If checked, notifications are triggered by an event - i.e. user clicks they forgot password. Otherwise, they run on a scheduled basis.', name: 'triggeredByEvent', xtype: 'checkbox'},
			{fieldLabel: 'Security Resource', name: 'securityResource.name', hiddenName: 'securityResource.id', xtype: 'combo', url: 'portalSecurityResourceListFind.json', qtip: 'Limits notifications to be sent to users with access to specified security resource.  If file category subscriptions are allowed, then limits file category notifications based on security.'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true}
		]
	}]
});
