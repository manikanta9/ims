Clifton.portal.notification.NotificationWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Notification',
	iconCls: 'email',
	width: 900,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					url: 'portalNotification.json',
					readOnly: true,

					getWarningMessage: function(form) {
						let msg = form.formValues.errorMessage;
						if (TCG.isNotBlank(msg)) {
							msg = '<b>Error Sending Notification:</b> ' + msg;
						}
						return msg;
					},

					listeners: {
						afterload: function(panel) {
							const f = panel.getForm();
							const w = panel.getWindow();

							const tabs = w.items.get(0);
							const showFiles = TCG.getValue('notificationDefinition.definitionType.fileNotification', f.formValues);
							if (TCG.isTrue(showFiles)) {
								if (!TCG.getChildByName(tabs, 'filesTab')) {
									tabs.add(w.filesTab);
								}
							}

							const acknowledgeable = TCG.getValue('acknowledgeable', f.formValues);
							if (TCG.isFalse(acknowledgeable)) {
								TCG.getChildByName(panel, 'acknowledgedFieldSet').setVisible(false);
							}
							else {
								TCG.getChildByName(panel, 'acknowledgedFieldSet').setVisible(true);
								if (!TCG.getChildByName(tabs, 'acknowledgementsTab')) {
									tabs.add(w.acknowledgementsTab);
								}
							}
						}
					},

					items: [
						{fieldLabel: 'Definition Name', name: 'notificationDefinition.name', detailIdField: 'notificationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.notification.setup.DefinitionWindow'},
						{fieldLabel: 'Recipient', name: 'coalesceRecipientLabel', detailIdField: 'recipientUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.security.user.UserWindow'},
						{fieldLabel: 'Sent Date', name: 'sentDate', xtype: 'displayfield'},
						{fieldLabel: 'Subject', name: 'subject', xtype: 'displayfield'},
						{
							fieldLabel: 'Text', name: 'text', xtype: 'htmleditor', readOnly: true, height: 400,
							listeners: {
								afterrender: function() {
									this.getToolbar().hide();
								}
							}
						},
						{
							xtype: 'fieldset',
							name: 'acknowledgedFieldSet',
							collapsible: false,
							border: false,
							hidden: true,
							items: [
								{fieldLabel: '', boxLabel: 'Acknowledged (See Acknowledgements Tab for Details)', name: 'acknowledged', xtype: 'checkbox'}
							],
							buttons: [
								{
									xtype: 'button', text: 'Mark Acknowledged (Client View)', tooltip: 'Mark the notification as acknowledged for the recipient using the buttons/options as the Client would see them', width: 150,
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										f.openAcknowledgementWindow(false);
									},
									listeners: {
										afterRender: function() {
											const btn = this;
											const fp = TCG.getParentFormPanel(this);
											fp.on('afterload', function() {
												if (TCG.isTrue(fp.getFormValue('acknowledged'))) {
													btn.disable();
												}
											}, this);
										}
									}
								},
								{
									xtype: 'button', text: 'Mark Acknowledged (Admin View)', tooltip: 'Mark the notification as acknowledged with all options (Positive, Negative, Neutral) acknowledgement, even if that option is not available for the user.', width: 150,
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										f.openAcknowledgementWindow(true);
									},
									listeners: {
										afterRender: function() {
											const btn = this;
											const fp = TCG.getParentFormPanel(this);
											fp.on('afterload', function() {
												if (TCG.isTrue(fp.getFormValue('acknowledged'))) {
													btn.disable();
												}
											}, this);
										}
									}
								}
							]
						}
					],
					openAcknowledgementWindow: function(adminView) {
						const f = this;
						const ackType = f.getFormValue('notificationDefinition.acknowledgementType');

						const portalNotificationId = f.getFormValue('id');
						const clz = 'Clifton.portal.notification.NotificationAcknowledgementWindow';
						const cmpId = TCG.getComponentId(clz, portalNotificationId);
						TCG.createComponent(clz, {
							cmpId: cmpId,
							params: {portalNotificationId: portalNotificationId},
							openerCt: f,
							adminWindow: adminView,
							acknowledgementType: ackType,
							confirmEntity: ackType.confirmEntity
						});
					}
				}]
			}
		]
	}],

	filesTab: {
		title: 'Portal Files',
		name:
			'filesTab',
		items:
			[{
				name: 'portalNotificationPortalFileListForNotification',
				xtype: 'gridpanel',
				instructions: 'The following files were referenced in this notification.',
				groupField: 'referenceTwo.fileCategory.nameExpanded',
				groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]}',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'FileID', width: 15, dataIndex: 'referenceTwo.id', hidden: true},
					{header: 'File Category', width: 50, dataIndex: 'referenceTwo.fileCategory.nameExpanded', hidden: true},
					{
						header: 'Format', width: 30, dataIndex: 'referenceTwo.documentFileType', filter: false, align: 'center',
						renderer: function(ft, args, r) {
							if (!TCG.isBlank(ft)) {
								return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
							}
						}
					},
					{header: 'Post Entity', width: 100, dataIndex: 'referenceTwo.postPortalEntity.label'},
					{header: 'Display Name', width: 150, dataIndex: 'referenceTwo.displayName', renderer: TCG.renderText},
					{header: 'Approved', width: 35, dataIndex: 'referenceTwo.approved', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
					{header: 'Report Date', width: 45, dataIndex: 'referenceTwo.reportDate'},
					{header: 'Publish Date', width: 45, dataIndex: 'referenceTwo.publishDate'}
				],
				getLoadParams: function(firstLoad) {
					const w = this.getWindow();
					return {
						portalNotificationId: w.getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.portal.file.FileWindow',
					drillDownOnly: true,
					getDetailPageId: function(gridPanel, row) {
						return row.json.referenceTwo.id;
					}
				},
				gridConfig: {
					listeners: {
						'rowclick': function(grid, rowIndex, evt) {
							if (TCG.isActionColumn(evt.target)) {
								const eventName = TCG.getActionColumnEventName(evt);
								const row = grid.store.data.items[rowIndex];
								if (eventName === 'ViewFile') {
									Clifton.portal.file.downloadFiles(row.json.portalFileId, null, grid);
								}
							}
						}
					}
				}
			}]
	},

	acknowledgementsTab: {
		title: 'Acknowledgements',
		name:
			'acknowledgementsTab',
		items:
			[{

				xtype: 'portal-tracking-event-list-grid',
				instructions: 'The following acknowledgements events apply to this notification.  Note: If a notification is set to confirm at the entity level, every entity that applies to the notification for the recipient is tracked separately.',
				eventTypeName: 'Portal Notification Acknowledgement',

				columnOverrides: [
					{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false},
					{
						header: 'Acknowledged', width: 50, dataIndex: 'description', align: 'center',
						renderer: function(v, metaData, r) {
							if (TCG.isEquals(v, 'true')) {
								return TCG.renderEvalResultIcon('Success');
							}
							if (TCG.isEquals(v, 'false')) {
								return TCG.renderEvalResultIcon('Failure');
							}
							return v;
						}
					}
				],

				getTopToolbarFilters: function(toolbar) {
					return []; // Not Used
				},

				getDefaultEventDateFilter: function() {
					// no default since limited to a specific notification
					return null;
				},

				applyAdditionalLoadParams: function(firstLoad, params) {
					params.portalNotificationId = this.getWindow().getMainFormId();
					return params;
				}
			}]
	}

});
