Clifton.portal.notification.subscription.NotificationSubscriptionEntryWindow = Ext.extend(Clifton.portal.DetailWindow, {
	title: 'Portal Notification Subscriptions',
	iconCls: 'email',
	width: 700,
	height: 500,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		loadDefaultDataAfterRender: true,
		getSaveURL: function() {
			return 'portalNotificationSubscriptionEntrySave.json';
		},
		instructions: 'Use the following screen to view and edit user notification subscriptions for the currently selected user. Select a Portal Entity from the list and click Load Subscriptions to view what is currently defined and Save Subscriptions to save any changes.',
		getDefaultData: function(win) {
			const dd = Ext.apply(win.defaultData || {}, {});
			const securityUserId = dd.securityUser.id;
			const portalEntityId = (dd.portalEntity) ? dd.portalEntity.id : undefined;
			const formPanel = this;
			return TCG.data.getDataPromise('portalSecurityUser.json?id=' + securityUserId, formPanel)
				.then(function(user) {
					dd.securityUser = user;
					if (portalEntityId) {
						return TCG.data.getDataPromise('portalEntity.json?id=' + portalEntityId, formPanel)
							.then(function(entity) {
								if (entity) {
									dd.portalEntity = entity;
								}
								return dd;
							});
					}
					else {
						return dd;
					}
				});
		},
		afterRenderPromise: function(panel) {
			// If portal entity is selected - load the subscriptions
			const portalEntity = this.getFormValue('portalEntity.id');
			if (!TCG.isBlank(portalEntity)) {
				TCG.getChildByName(panel, 'subscriptionListGrid').reloadGrid();
			}

		},

		items: [
			{fieldLabel: 'Security User', name: 'securityUser.label', detailIdField: 'securityUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.security.user.UserWindow'},
			{
				fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', hiddenName: 'portalEntity.id', displayField: 'entityLabelWithSourceSection', xtype: 'combo', url: 'portalEntityListFind.json?orderBy=labelExpanded&securableEntity=true&hasSecurityResourceAccess=true&securableEntityHasApprovedFilesPosted=true&active=true',
				beforequery: function(queryEvent) {
					const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
					const userId = form.findField('securityUser.id').value;
					queryEvent.combo.store.setBaseParam('viewAsUserId', userId);
				},
				listeners: {
					select: function(field) {
						TCG.getChildByName(TCG.getParentByClass(field, TCG.form.FormPanel), 'subscriptionListGrid').reloadGrid();
					},
					specialkey: function(field, e) {
						if (TCG.isEquals(e.getKey(), e.ENTER)) {
							TCG.getChildByName(TCG.getParentByClass(field, TCG.form.FormPanel), 'subscriptionListGrid').reloadGrid();
						}
					}
				}
			},

			{
				xtype: 'formgrid-scroll',
				name: 'subscriptionListGrid',
				height: 300,
				heightResized: true,
				storeRoot: 'subscriptionList',
				dtoClass: 'com.clifton.portal.notification.subscription.PortalNotificationSubscription',
				alwaysSubmitFields: ['userAssignment.id', 'portalEntity.id', 'notificationDefinitionType.id', 'fileCategory.id', 'notificationEnabled'],
				readOnly: true,
				collapsible: false,

				groupField: 'notificationGroupingLabel',
				groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Notifications" : "Notification"]})',

				addToolbarButtons: function(toolBar) {
					const gp = this;
					toolBar.add({
						text: 'Load Subscriptions',
						tooltip: 'Load Subscriptions for the selected user and portal entity',
						iconCls: 'table-refresh',
						scope: this,
						handler: function() {
							gp.reloadGrid();
						}
					});

					toolBar.add('-');
					toolBar.add({
						text: 'Save Subscriptions',
						tooltip: 'Save selected Subscriptions for the selected user and portal entity',
						iconCls: 'disk',
						scope: this,
						handler: function() {
							gp.getWindow().saveWindow(false);
						}
					});

					toolBar.add('-');
				},

				reloadGrid: function() {
					const grid = this;
					// get the submit parameters
					const panel = TCG.getParentFormPanel(this);
					const form = panel.getForm();

					const securityUserId = panel.getFormValue('securityUser.id');
					const portalEntityId = form.findField('portalEntity.entityLabelWithSourceSection').getValue();

					if (TCG.isBlank(securityUserId) || TCG.isBlank(portalEntityId)) {
						TCG.showError('A user and entity must be selected in order to view subscriptions.');
						return;
					}

					const loader = new TCG.data.JsonLoader({
						waitTarget: TCG.getParentFormPanel(grid).getEl(),
						params: {securityUserId: securityUserId, portalEntityId: portalEntityId},
						scope: grid,
						onLoad: function(record, conf) {
							grid.store.loadData(record);
							grid.markModified(true);
						}
					});
					loader.load('portalNotificationSubscriptionEntry.json');
				},

				listeners: {
					afterRender: function() {
						const grid = this;
						const fp = TCG.getParentFormPanel(this);
						fp.on('afterload', function() {
							if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
								this.reloadGrid();
							}
						}, this);
					},

					afteredit: function(grid, row, col) {
						if (grid.colModel) {
							// on checkbox clear, clear frequency column
							const enabledColumn = TCG.isEquals('Enabled', grid.colModel.getColumnHeader(col));
							if (TCG.isTrue(enabledColumn)) {
								const thisRow = grid.store.data.items[row];
								thisRow.set('subscriptionFrequency', '');
								thisRow.set('subscriptionFrequencyLabel', '');
								grid.getView().refresh();
							}
						}
					}
				},

				// Overridden to support making enabled or frequency cells un-editable for rows it doesn't apply
				startEditing: function(row, col) {
					const ed = this.colModel.getCellEditor(col, row);

					const frequencyColumn = TCG.isEquals('Frequency', this.colModel.getColumnHeader(col));
					if (TCG.isTrue(frequencyColumn)) {
						// If notifications are on:
						if (TCG.isTrue(this.getStore().getAt(row).get('notificationEnabled'))) {
							const storeData = Clifton.portal.notification.subscription.GetSubscriptionFrequenciesForFileFrequency(this.getStore().getAt(row).get('fileCategory.portalFileFrequency'));
							if (storeData.length !== 0) {
								ed.field.store.removeAll();
								ed.field.store.loadData(storeData);
							}
							else {
								// No Options Available
								return false;
							}
						}
						else {
							// Notifications Are Not Enabled
							return false;
						}
					}

					TCG.callOverridden(this, 'startEditing', arguments);
				},


				columnsConfig: [
					{header: 'ID', hidden: true, width: 50, dataIndex: 'id'},
					{header: 'FileCategoryFileFrequency', hidden: true, width: 50, dataIndex: 'fileCategory.portalFileFrequency'},
					{header: 'FileCategorySubscriptionOptionRequired', hidden: true, width: 50, dataIndex: 'fileCategory.notificationSubscriptionRequired'},
					{header: 'Grouping Label', hidden: true, width: 50, dataIndex: 'notificationGroupingLabel'},
					{header: 'UserAssignmentID', hidden: true, width: 50, dataIndex: 'userAssignment.id'},
					{header: 'EntityID', hidden: true, width: 50, dataIndex: 'portalEntity.id'},
					{header: 'Parent', width: 200, dataIndex: 'fileCategory.parent.name', hidden: true},
					{header: 'Notification', width: 300, dataIndex: 'coalesceDefinitionTypeFileCategoryName'},
					{
						header: 'Enabled', width: 75, dataIndex: 'notificationEnabled', type: 'boolean',

						editor: {xtype: 'checkcolumn'},
						isCheckEnabled: function(record) {
							return !TCG.isTrue(record.data['fileCategory.notificationSubscriptionRequired']);
						},
						overrideRenderer: true,
						renderer: function(v, p, record) {
							if (TCG.isTrue(this.isCheckEnabled(record))) {
								p.css += ' x-grid3-check-col-td';
								return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
							}
							return TCG.renderBoolean(true, 'Notifications are required to be enabled for the selected file category.');
						}
					},
					{
						header: 'Frequency', width: 200, dataIndex: 'subscriptionFrequencyLabel', idDataIndex: 'subscriptionFrequency',
						tooltip: 'Optional frequency of notifications.  Selections are allowed for file categories where file frequency allows.',
						renderer: function(v, metaData, r) {
							const storeData = Clifton.portal.notification.subscription.GetSubscriptionFrequenciesForFileFrequency(r.data['fileCategory.portalFileFrequency']);
							if (storeData.length === 0) {
								metaData.attr = TCG.renderQtip('Frequency Options are not available for selected notification.');
							}
							else if (!TCG.isTrue(r.data.notificationEnabled)) {
								metaData.attr = TCG.renderQtip('Select the enabled checkbox to see options.');
							}
							else {
								metaData.attr = TCG.renderQtip('Leave blank for notifications of all posted files');
							}
							return v;
						},
						editor: {
							xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore'
								// Data is Dynamically Loaded based on File Category File Frequency
							}
						}
					}
				]
			}
		]
	}]
});
