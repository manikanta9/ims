Clifton.portal.notification.NotificationAcknowledgementWindow = Ext.extend(TCG.app.Window, {
	title: 'Acknowledge Notification',
	iconCls: 'email',
	width: 400,
	height: 200,

	adminWindow: false,
	adminInstructions: 'When acknowledging using the admin view, you can choose any type of acknowledgement (positive, negative, neutral) and the tracking response will be created and the notification will be acknowledged.  You can chose any response, not just what is available to the client. For example, a Client may not be able to decline a notification, but you can decline it for them.',
	clientInstructions: 'You can use this screen to acknowledge a notification for a user.  The buttons displayed below are as the user would see them. If a button is disabled (i.e. cancel) then the button is displayed to the actual user, but since it does not track as an actual acknowledgement you cannot select it.',

	acknowledgementType: undefined,

	initComponent: function() {
		if (TCG.isTrue(this.adminWindow)) {
			this.items[0].items[0].html = this.adminInstructions;
		}
		else {
			this.items[0].items[0].html = this.clientInstructions;
		}

		const ackType = this.acknowledgementType;
		this.buttons = [];
		if (TCG.isTrue(this.adminWindow) || TCG.isNotBlank(ackType.confirmUsed)) {
			this.buttons.push({
				text: this.adminWindow ? 'Confirm' : ackType.confirmButtonLabel,
				xtype: 'button',
				tooltip: this.adminWindow ? 'Force Confirm (Positive Acknowledgement) of the Notification' : ackType.confirmButtonTooltip,
				handler: function() {
					this.ownerCt.ownerCt.confirmButtonHandler();
				}
			});
		}
		if (TCG.isTrue(this.adminWindow) || TCG.isTrue(ackType.declineUsed)) {
			this.buttons.push(
				{
					text: this.adminWindow ? 'Decline' : ackType.declineButtonLabel,
					tooltip: this.adminWindow ? 'Force Decline (Negative Acknowledgement) of the Notification' : ackType.declineButtonTooltip,
					xtype: 'button',
					handler: function() {
						this.ownerCt.ownerCt.declineButtonHandler();
					}
				}
			);
		}
		if (TCG.isTrue(this.adminWindow) || TCG.isTrue(ackType.cancelUsed)) {
			this.buttons.push(
				{
					text: this.adminWindow ? 'Cancel (Acknowledge)' : ackType.cancelButtonLabel,
					tooltip: this.adminWindow ? 'Force "Cancel" a.k.a. Neutral Acknowledgement of the Notification' : ackType.cancelButtonTooltip,
					xtype: 'button',
					handler: function() {
						this.ownerCt.ownerCt.cancelButtonHandler();
					},
					disabled: !this.adminWindow && !ackType.cancelAcknowledgement
				}
			);
		}
		this.buttons.push(
			{
				text: 'Exit',
				tooltip: 'Close window without acknowledging',
				handler: function() {
					this.ownerCt.ownerCt.closeWindow();
				}
			}
		);
		TCG.Window.superclass.initComponent.apply(this, arguments);
	},

	items: [{
		xtype: 'formpanel',
		items: [
			{xtype: 'label', html: 'Placeholder instructions'},
			{xtype: 'label', html: '<br/><br/>Use the <b>Exit</b> button to cancel out of this screen without applying any acknowledgments.'}
		]
	}],


	confirmButtonHandler: function() {
		let msg = 'Are you sure you would like to <b>confirm</b> (positive acknowledgement) this notification for the recipient?';
		if (TCG.isTrue(this.confirmEntity)) {
			msg += '<br/><br/>This notification is set to confirm entity, so additional user\'s notifications may subsequently be acknowledged as well.';
		}
		this.buttonHandler(msg, true);
	},

	declineButtonHandler: function() {
		this.buttonHandler('Are you sure you would like to <b>decline</b> (negative acknowledgement) this notification for the recipient?', false);
	},

	cancelButtonHandler: function() {
		this.buttonHandler('Are you sure you would like to <b>cancel</b> (neutral acknowledgement) this notification for the recipient?  The notification WILL be marked as acknowledged.', null);
	},

	buttonHandler: function(message, acknowledgement) {
		const w = this;
		Ext.Msg.confirm('Notification Acknowledgement', message, function(a) {
			if (a === 'yes') {
				const loader = new TCG.data.JsonLoader({
					params: {portalNotificationId: w.params.portalNotificationId, acknowledgement: acknowledgement},
					waitTarget: w,
					waitMsg: 'Acknowledging...',
					onLoad: function(record, conf) {
						w.closeWindow();
					}
				});
				if (w.adminWindow) {
					loader.load('portalNotificationAcknowledgementAdminSave.json');
				}
				else {
					loader.load('portalNotificationAcknowledgementSave.json');
				}
			}
		});
	}
});
