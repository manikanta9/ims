Clifton.portal.notification.NotificationTestSendWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Test Notification',
	iconCls: 'email',
	width: 1000,
	height: 600,
	items: [{
		xtype: 'portal-formpanel',
		getSaveURL() {
			return 'portalTestNotificationGenerate.json';
		},
		listeners: {
			beforerender: function(formPanel) {
				const loader = new TCG.data.JsonLoader({
					params: {
						name: 'Test Email'
					},
					onLoad: function(record, conf) {
						const form = formPanel.getForm();
						form.findField('bodyTemplate').setValue(record.emailBodyTemplate);
						form.findField('subjectTemplate').setValue(record.emailSubjectTemplate);
						form.findField('portalNotificationDefinitionId').setValue(record.id);
					}
				});
				loader.load('portalNotificationDefinitionByName.json', formPanel);
			}
		},
		items: [
			{fieldLabel: 'Recipient', name: 'recipientUser', hiddenName: 'recipientUser.id', displayField: 'emailAddress', xtype: 'combo', url: 'portalSecurityUserListFind.json', detailPageClass: 'Clifton.portal.security.user.UserWindow', requestedProps: 'username|emailAddress'},
			{name: 'commandType', value: 'SEND', hidden: true},
			{fieldLabel: 'Subject', name: 'subjectTemplate'},
			{fieldLabel: 'Body', name: 'bodyTemplate', xtype: 'htmleditor', height: 300},
			{name: 'portalNotificationDefinitionId', hidden: true}
		]
	}]
});
