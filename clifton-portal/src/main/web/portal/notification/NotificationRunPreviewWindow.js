Clifton.portal.notification.NotificationRunPreviewWindow = function(conf) {
	this.win = new TCG.app.Window(Ext.apply({
		title: 'Notification Run Preview',
		width: 750,
		height: 500,
		buttons: [{
			text: 'Close',
			tooltip: 'Close this window',
			handler: function() {
				this.ownerCt.ownerCt.closeWindow();
			}
		}],

		closeWindow: function() {
			const win = this;
			win.closing = true;
			win.close();
		},

		items: [{
			xtype: 'gridpanel',
			appendStandardColumns: false,
			name: 'portalNotificationListPreview',
			instructions: 'The following can be used to preview real notifications that would be generated or sample ones, and is useful to test the format and Freemarker syntax for the definition.  You can also optionally send a test email to yourself.  See Preview Type for various options.',

			additionalPropertiesToRequest: 'text|recipientUser.id',
			columns: [
				{header: 'Recipient', dataIndex: 'coalesceRecipientLabel', width: 150},
				//{header: 'Email Address', dataIndex: 'recipientUser.emailAddress', width: 150},
				{header: 'Subject', dataIndex: 'subject', width: 300}

			],
			getTopToolbarFilters: function(toolbar) {
				return [
					{
						fieldLabel: 'Preview Type', xtype: 'toolbar-combo', name: 'commandType', width: 150, minListWidth: 100, mode: 'local', value: 'PREVIEW_ONE_SAMPLE',
						store: {
							xtype: 'arraystore',
							data: [
								['PREVIEW', 'Preview Run', 'Display notifications if this notification were to run right now.'],
								['SEND_PREVIEW', 'Send Preview Run', 'Display notifications if this notification were to run right now, and also generates emails to you (current logged in user).'],
								['PREVIEW_ONE_SAMPLE', 'View Sample', 'Generate a sample.  This may contain fake data and is useful to validate freemarker syntax in the notification subject and text.'],
								['SEND_PREVIEW_ONE_SAMPLE', 'Send Sample', 'Generate a sample.  This may contain fake data and is useful to validate freemarker syntax in the notification subject and text. Will also generate the email and send to you (current logged in user)']
							]
						}
					}
				];
			},

			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.portal.notification.NotificationSampleWindow',
				getDefaultDataForExisting: function(gridPanel, row) {
					return row.json;
				},
				openDetailPage: function(className, gridPanel, id, row, itemText, defaultActiveTabName) {
					const defaultData = this.getDefaultDataForExisting(gridPanel, row);

					const editor = this;

					const params = id ? editor.getDetailPageParams(id) : undefined;
					const cmpId = TCG.getComponentId(this.detailPageClass, id);
					TCG.createComponent(this.detailPageClass, {
						id: cmpId,
						defaultData: defaultData,
						defaultDataIsReal: true,
						params: params,
						openerCt: gridPanel
					});
				}
			},

			getLoadParams: function() {
				const params = {};
				const t = this.getTopToolbar();
				const commandType = TCG.getChildByName(t, 'commandType');
				params.commandType = commandType.getValue();
				Ext.apply(params, this.getWindow().params);
				return params;
			}
		}]
	}, conf));
};
