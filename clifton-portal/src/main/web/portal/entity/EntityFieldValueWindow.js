Clifton.portal.entity.EntityFieldValueWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Entity Field Value',
	iconCls: 'list',
	height: 400,

	items: [{
		xtype: 'portal-formpanel',
		url: 'portalEntityFieldValue.json',
		readOnly: true,
		items: [
			{fieldLabel: 'Portal Entity', name: 'portalEntity.label', xtype: 'linkfield', detailIdField: 'portalEntity.id', detailPageClass: 'Clifton.portal.entity.EntityWindow'},
			{fieldLabel: 'Field Name', name: 'portalEntityFieldType.name', xtype: 'displayfield'},
			{fieldLabel: 'Value', name: 'value', xtype: 'textarea', height: 50, grow: true}
		]
	}]
});
