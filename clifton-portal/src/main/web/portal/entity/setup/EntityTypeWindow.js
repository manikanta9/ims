Clifton.portal.entity.setup.EntityTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Entity Type',
	iconCls: 'list',
	width: 700,
	height: 400,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 150,
					url: 'portalEntityType.json',
					readOnly: true,

					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Postable', boxLabel: 'Files can be posted to entities of this type', name: 'postableEntity', xtype: 'checkbox'},
						{fieldLabel: 'Securable', boxLabel: 'Viewable entities that users can be assigned to entities of this type ', name: 'securableEntity', xtype: 'checkbox'},
						{fieldLabel: 'File Source', boxLabel: 'Entities can be linked to a file as its source', name: 'fileSource', xtype: 'checkbox'}
					]
				}]
			},

			{
				title: 'Source Sections',
				items: [{
					name: 'portalEntitySourceSectionListFind',
					xtype: 'gridpanel',

					getLoadParams: function() {
						const w = this.getWindow();

						return {
							entityTypeId: w.getMainFormId()
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Section Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Source System', width: 50, dataIndex: 'sourceSystem.name', filter: {searchFieldName: 'sourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
						{header: 'Source Table Name', width: 75, dataIndex: 'sourceSystemTableName'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.setup.EntitySourceSectionWindow'
					}
				}]
			},

			{
				title: 'Field Types',
				items: [{
					name: 'portalEntityFieldTypeListByEntityType',
					xtype: 'gridpanel',

					getLoadParams: function() {
						const w = this.getWindow();

						return {
							portalEntityTypeId: w.getMainFormId()
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Field Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description'}
					]
				}]
			}
		]
	}]
});
