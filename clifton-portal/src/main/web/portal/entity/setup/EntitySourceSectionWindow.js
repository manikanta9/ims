Clifton.portal.entity.setup.EntitySourceSectionWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Entity Source Section',
	iconCls: 'list',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 150,
					url: 'portalEntitySourceSection.json',

					items: [
						{fieldLabel: 'Entity Type', name: 'entityType.name', xtype: 'linkfield', detailIdField: 'entityType.id', detailPageClass: 'Clifton.portal.entity.setup.EntityTypeWindow'},
						{xtype: 'checkbox', hidden: true, name: 'entityType.postableEntity', disabled: true},
						{
							fieldLabel: 'Child Entity Type', name: 'childEntityType.name', hiddenName: 'childEntityType.id', requiredFields: ['entityType.postableEntity'], setVisibilityOnRequired: true, xtype: 'combo', url: 'portalEntityTypeListFind.json?securableEntity=true',
							qtip: 'Defines the children type in the rollup table for this entity.  Used to determine what can be posted at this roll up level.', readOnly: true
						},
						{fieldLabel: 'Source System', name: 'sourceSystem.name', xtype: 'displayfield'},
						{fieldLabel: 'Source Table Name', name: 'sourceSystemTableName', xtype: 'displayfield'},
						{fieldLabel: 'Section Name', name: 'name', readOnly: true},
						{fieldLabel: 'Section Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},

			{
				title: 'Entities',
				items: [{
					name: 'portalEntityListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'View Type', dataIndex: 'entityViewType.name', width: 75, hidden: true, filter: {type: 'combo', searchFieldName: 'entityViewTypeId', url: 'portalEntityViewTypeListFind.json'}, tooltip: 'Applies to Securable Entities only (i.e. Client Relationships, Clients, Accounts)'},
						{header: 'Entity Name', width: 100, dataIndex: 'name'},
						{header: 'Parent Entity', width: 100, dataIndex: 'parentPortalEntity.label', hidden: true},
						{header: 'Entity Label', width: 100, dataIndex: 'label'},
						{header: 'Entity Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'FKFieldID', width: 30, dataIndex: 'sourceFkFieldId', hidden: true},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'},
						{header: 'Start Date', width: 30, dataIndex: 'startDate'},
						{header: 'End Date', width: 30, dataIndex: 'endDate'},
						{header: 'Last Updated On', width: 30, dataIndex: 'updateDate'}
					],

					getTopToolbarFilters: function(toolbar) {
						const securable = TCG.getValue('entityType.securableEntity', this.getWindow().getMainForm().formValues);
						if (TCG.isTrue(securable)) {
							return [
								{fieldLabel: 'View Type', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json', linkedFilter: 'entityViewType.name'},
								{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						}
						return [{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const w = this.getWindow();

						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
						return {
							entitySourceSectionId: w.getMainFormId()
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.EntityWindow'
					}
				}]
			}
		]
	}]
});
