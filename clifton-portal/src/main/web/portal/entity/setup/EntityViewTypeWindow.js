Clifton.portal.entity.setup.EntityViewTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Entity View Type',
	iconCls: 'list',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					labelWidth: 150,
					url: 'portalEntityViewType.json',

					items: [
						{fieldLabel: 'View Type Name', name: 'name', xtype: 'displayfield'},
						{fieldLabel: '', boxLabel: 'Default View', name: 'defaultView', disabled: true, xtype: 'checkbox'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Default Contact Email', name: 'defaultContactEmailAddress', vtype: 'email'}
					]
				}]
			},

			{
				title: 'Portal Entities',
				items: [{
					name: 'portalEntityListFind',
					xtype: 'gridpanel',
					instructions: 'A portal entity is a record from an outside system. Based on the entity type, the entity can contain additional field values that can be used for display.',
					groupField: 'entitySourceSection.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Entities" : "Entity"]}',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Source System', dataIndex: 'entitySourceSection.sourceSystem.name', width: 40, filter: {type: 'combo', searchFieldName: 'entitySourceSystemId', url: 'portalEntitySourceSystemListFind.json'}},
						{header: 'Source Section', dataIndex: 'entitySourceSection.name', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'entitySourceSectionId', url: 'portalEntitySourceSectionListFind.json?securableEntity=true', displayField: 'nameWithSourceSystem'}},
						{header: 'Entity Type', width: 50, dataIndex: 'entitySourceSection.entityType.name', hidden: true},
						{header: 'Entity Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Entity Label', width: 100, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Entity Description', width: 100, dataIndex: 'description', hidden: true, searchFieldName: 'searchPattern'},
						{header: 'Parent Entity', width: 100, hidden: true, dataIndex: 'parentPortalEntity.label'},
						{header: 'FKFieldID', width: 30, dataIndex: 'sourceFkFieldId', hidden: true},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'},
						{header: 'Start Date', width: 30, dataIndex: 'startDate'},
						{header: 'End Date', width: 30, dataIndex: 'endDate'},
						{header: 'Last Updated On', width: 30, dataIndex: 'updateDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Source Section', xtype: 'toolbar-combo', name: 'sourceSectionId', width: 180, url: 'portalEntitySourceSectionListFind.json?securableEntity=true', linkedFilter: 'entitySourceSection.name', displayField: 'nameWithSourceSystem'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
						return {
							entityViewTypeId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.EntityWindow'
					}
				}]
			}
		]
	}]
});
