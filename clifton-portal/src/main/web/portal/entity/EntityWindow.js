Clifton.portal.entity.EntityWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Entity',
	iconCls: 'list',
	width: 1000,
	height: 725,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				layout: 'border',
				items: [{
					xtype: 'portal-formpanel',
					url: 'portalEntity.json',
					region: 'north',
					flex: 0.49,
					height: 375,
					labelWidth: 185,
					readOnly: true,

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('portalEntity.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					tbar: [
						{
							text: 'Update End Date',
							name: 'updateEndDate',
							iconCls: 'pencil',
							hidden: true,
							tooltip: 'Update the end date for this portal entity.',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								TCG.createComponent('Clifton.portal.entity.EntityChangeEndDateWindow', {
									defaultData: {
										portalEntityId: TCG.getValue('id', fp.getForm().formValues),
										entityLabelWithSourceSection: TCG.getValue('entityLabelWithSourceSection', fp.getForm().formValues),
										currentEndDate: TCG.getValue('endDate', fp.getForm().formValues)
									},
									openerCt: fp
								});
							}
						}
					],

					instructionsLoaded: false,
					listeners: {
						afterload: function(panel) {
							const f = panel.getForm();
							const w = panel.getWindow();

							if (TCG.isBlank(TCG.getValue('parentPortalEntity.id', f.formValues))) {
								panel.hideField('parentPortalEntity.label');
							}
							else {
								panel.setFieldLabel('parentPortalEntity.label', TCG.getValue('parentPortalEntity.entitySourceSection.label', f.formValues) + ': ');
							}

							const sourceSectionLabel = panel.getFormValue('entitySourceSection.label');
							panel.setFieldLabel('name', sourceSectionLabel + ' Name:');
							panel.setFieldLabel('label', sourceSectionLabel + ' Label:');

							const tabs = w.items.get(0);
							const type = panel.getFormValue('entitySourceSection.entityType');
							if (type && (TCG.isTrue(type.postableEntity) || TCG.isTrue(type.securableEntity))) {
								tabs.add(w.childrenTab);
								tabs.add(w.rollupTab);
								panel.showField('folderNameExpanded');
								if (TCG.isTrue(type.securableEntity)) {
									panel.showField('entityViewType.name');
									panel.showField('terminationDate');
									if (TCG.isNotBlank(TCG.getValue('endDate', f.formValues))) {
										TCG.getChildByName(panel.getTopToolbar(), 'updateEndDate').setVisible(true);
									}
								}
							}
							this.getWindow().titlePrefix = panel.getFormValue('entitySourceSection.name');
							this.updateTitle();
							if (this.instructionsLoaded === false) {
								const description = panel.getFormValue('entitySourceSection.description');
								this.insert(0, {xtype: 'label', html: description + '<br /><br />'});
								this.instructionsLoaded = true;
							}
							this.doLayout();
						}
					},

					items: [
						{fieldLabel: 'Parent Entity', qtip: 'Parent Entity', name: 'parentPortalEntity.label', xtype: 'linkfield', detailIdField: 'parentPortalEntity.id', detailPageClass: 'Clifton.portal.entity.EntityWindow'},

						{fieldLabel: 'Entity Name', name: 'name'},
						{fieldLabel: 'Entity Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
						{fieldLabel: 'Folder Name', name: 'folderNameExpanded', xtype: 'displayfield', hidden: true, qtip: 'Location for files posted directly to this entity.'},
						{fieldLabel: 'View Type', name: 'entityViewType.name', xtype: 'linkfield', hidden: true, detailIdField: 'entityViewType.id', detailPageClass: 'Clifton.portal.entity.setup.EntityViewTypeWindow'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Source Section', name: 'entitySourceSection.label', xtype: 'linkfield', detailIdField: 'entitySourceSection.id', detailPageClass: 'Clifton.portal.entity.setup.EntitySourceSectionWindow'},
										{fieldLabel: 'Source FK Field ID', name: 'sourceFkFieldId', xtype: 'displayfield'},
										{
											fieldLabel: 'Last Updated On', name: 'updateDate', xtype: 'displayfield',
											qtip: 'When this entity was last updated by the source system.'
										}
									]
								},
								{
									rows: [
										{
											fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield',
											qtip: 'The start date for the entity.  For securable entities, would be the inception date of the account/client/relationship.'
										},
										{
											fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield',
											qtip: 'The end date for the entity.  For securable entities, would be a post date relative to the termination date so clients can have access to the portal for a specified amount of time after termination.'
										},
										{
											fieldLabel: 'Termination Date', name: 'terminationDate', xtype: 'datefield',
											qtip: 'The termination date for the entity.  For securable entities, would be the termination date of the account/client/relationship.'
										}
									]
								}
							]
						}

					]
				},
					{
						flex: 0.02
					},
					{

						name: 'portalEntityFieldValueListByEntity',
						xtype: 'gridpanel',
						title: 'Field Values',
						instructions: 'Some entities can contain additional information available for display throughout the portal.',
						flex: 0.49,
						region: 'center',

						getLoadParams: function() {
							const w = this.getWindow();

							return {
								portalEntityId: w.getMainFormId()
							};
						},
						columns: [
							{header: 'Field', width: 100, dataIndex: 'portalEntityFieldType.label'},
							{header: 'Value', width: 300, dataIndex: 'value', renderer: TCG.renderValueWithTooltip}
						],
						editor: {
							detailPageClass: 'Clifton.portal.entity.EntityFieldValueWindow',
							drillDownOnly: true
						}
					}
				]
			}
		]
	}],

	childrenTab: {
		title: 'Child Entities',
		items: [{

			name: 'portalEntityListFind',
			xtype: 'gridpanel',
			instructions: 'Children entities are those that reference this entity directly as its\' parent.  This applies to securable entities only.',

			getLoadParams: function(firstLoad) {
				const w = this.getWindow();
				if (firstLoad) {
					// default to active
					this.setFilterValue('active', true);
				}
				return {
					parentId: w.getMainFormId()
				};
			},
			columns: [
				{header: 'Entity Type', width: 50, dataIndex: 'entitySourceSection.entityType.name', hidden: true},
				{header: 'Source Section', width: 100, dataIndex: 'entitySourceSection.name'},
				{header: 'Entity Name', width: 100, dataIndex: 'name'},
				{header: 'Entity Label', width: 100, dataIndex: 'label'},
				{header: 'Entity Description', width: 100, dataIndex: 'description'},
				{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'},
				{header: 'Start Date', width: 30, dataIndex: 'startDate'},
				{header: 'End Date', width: 30, dataIndex: 'endDate'}
			],
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.portal.entity.EntityWindow'
			}
		}]

	},

	rollupTab: {
		title: 'Rollups',
		layout: 'border',
		items: [
			{
				name: 'portalEntityRollupListByChild',
				xtype: 'gridpanel',
				title: 'Parent Entities',
				instructions: 'When files are posted to any of the following entities, this entity will automatically be assigned the same file if they are active on the report date.',
				region: 'north',
				height: 300,

				getLoadParams: function() {
					const w = this.getWindow();
					return {
						portalEntityId: w.getMainFormId()
					};
				},
				additionalPropertiesToRequest: 'id|referenceOne.id',
				columns: [
					{header: 'Entity Type', width: 50, dataIndex: 'referenceOne.entitySourceSection.entityType.name', hidden: true},
					{header: 'Source Section', width: 100, dataIndex: 'referenceOne.entitySourceSection.name'},
					{header: 'Entity Name', width: 100, dataIndex: 'referenceOne.name'},
					{header: 'Entity Label', width: 100, dataIndex: 'referenceOne.label'},
					{header: 'Entity Description', width: 100, dataIndex: 'referenceOne.description'},
					{header: 'Start Date', width: 30, dataIndex: 'startDate'},
					{header: 'End Date', width: 30, dataIndex: 'endDate'}
				],
				editor: {
					drillDownOnly: true,
					detailPageClass: 'Clifton.portal.entity.EntityWindow',
					getDetailPageId: function(gridPanel, row) {
						return row.json.referenceOne.id;
					}
				}
			},

			{

				name: 'portalEntityRollupListByParent',
				xtype: 'gridpanel',
				title: 'Child Entities',
				instructions: 'When files are posted to this entity, the following children will automatically be assigned the same file if they are active on the report date.',
				region: 'center',
				height: 300,

				getLoadParams: function() {
					const w = this.getWindow();
					return {
						portalEntityId: w.getMainFormId()
					};
				},
				additionalPropertiesToRequest: 'id|referenceTwo.id',
				columns: [
					{header: 'Entity Type', width: 50, dataIndex: 'referenceTwo.entitySourceSection.entityType.name', hidden: true},
					{header: 'Source Section', width: 100, dataIndex: 'referenceTwo.entitySourceSection.name'},
					{header: 'Entity Name', width: 100, dataIndex: 'referenceTwo.name'},
					{header: 'Entity Label', width: 100, dataIndex: 'referenceTwo.label'},
					{header: 'Entity Description', width: 100, dataIndex: 'referenceTwo.description'},
					{header: 'Start Date', width: 30, dataIndex: 'startDate'},
					{header: 'End Date', width: 30, dataIndex: 'endDate'}
				],
				editor: {
					drillDownOnly: true,
					detailPageClass: 'Clifton.portal.entity.EntityWindow',
					getDetailPageId: function(gridPanel, row) {
						return row.json.referenceTwo.id;
					}
				}
			}]
	}

});


Clifton.portal.entity.EntityChangeEndDateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Change End Date for Entity',
	iconCls: 'list',
	width: 600,
	height: 350,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'The End Date for Portal Entities drives when user assignments for this entity are disabled, and ultimately when the user is disabled.  There can be a delay between termination and end date.  The end date is either set directly, or if not set when termination date is set it is auto filled in to a default specified number of days in the future of the termination date.  You can change the end date here to end access earlier, or extend access.'
			+ '<br/><br/>Note: End dates use a bottom up approach, so if you are updating the end date you should perform this on the lowest level.  i.e. on the Client Account level - which will automatically persist up to the top level with the latest date.  Otherwise the accounts under which you are updating the end date for will still be inactive and reports unavailable.'
			+ '<br/><br/><b>Warning: If you extend access after the existing end date has already passed user assignments will not be automatically re-enabled.</b>',
		getSaveURL: function() {
			return 'portalEntityEndDateUpdate.json';
		},

		items: [
			{xtype: 'hidden', name: 'portalEntityId'},
			{fieldLabel: 'Entity', name: 'entityLabelWithSourceSection', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Current End Date', name: 'currentEndDate', xtype: 'datefield', readOnly: true, submitValue: false},
			{fieldLabel: 'New End Date', name: 'endDate', xtype: 'datefield', allowBlank: false}
		]
	}]
});
