Clifton.portal.entity.upload.EntityRollupUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'portalEntityRollupUploadWindow',
	title: 'Portal Entity Rollup Uploads from Source System',
	iconCls: 'import',
	height: 450,
	width: 750,
	hideOKButton: true,
	applyButtonText: 'Upload',
	applyButtonTooltip: 'Upload portal entity rollups from selected file',

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('portal/entity/upload/PortalEntityRollupUploadSampleFile.xls');
		}
	}],

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 165,
		instructions: 'Portal Entity Rollup Uploads allow uploading relationships between 2 entities, i.e. a Rollup and all of it\'s children.  Rollups are used to post to a single entity (i.e. Commingled Vehicle) and all children (investor client accounts) would get the file.  All parent and child entities should already exist, this upload is used only to associate the existing entities together.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: 'Source System', name: 'sourceSystem.name', hiddenName: 'sourceSystem.id', xtype: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true, allowBlank: false},
			{
				fieldLabel: '', boxLabel: 'Delete Rollup Entities Not In File (Where Parent Entity Included in File)', xtype: 'checkbox', name: 'deleteChildEntitiesMissingFromFile',
				qtip: 'If checked, then the list in the upload file for each rollup is considered to be all encompassing - so any child not in the list will be removed as a child of the rollup.  If unchecked, then any rollup child not in the file for a rollup parent included in the file will not be updated.'
			}
		],

		getSaveURL: function() {
			return 'portalEntityRollupUploadFileUpload.json';
		}

	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				const result = action.result.status;
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Portal Entity Rollup Upload Status',
					defaultData: {status: result}
				});
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
