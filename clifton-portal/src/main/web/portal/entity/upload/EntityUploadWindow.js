Clifton.portal.entity.upload.EntityUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'portalEntityUploadWindow',
	title: 'Portal Entity Uploads from Source System',
	iconCls: 'import',
	height: 450,
	width: 750,
	hideOKButton: true,
	applyButtonText: 'Upload',
	applyButtonTooltip: 'Upload portal entities from selected file',

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('portal/entity/upload/PortalEntityUploadSampleFile.xls');
		}
	}],

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 165,
		instructions: 'Portal Entity Uploads allow uploading entities and related information into the portal from a source system. For example, a list of Client Relationships, Clients, etc. These operations are limited to Administrators or Portal Administrators only depending on the source system you are importing data for.  If you are uploading source data in separate files, make sure the parent entity files are loaded first.  i.e. Load Client Relationships before Clients.  Clients before Client Accounts, etc.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: 'Source System', name: 'sourceSystem.name', hiddenName: 'sourceSystem.id', xtype: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true, allowBlank: false},
			{
				fieldLabel: '', boxLabel: 'Delete Entities Not In File (Leave Unchecked to Insert/Update Entities in the File Only)', xtype: 'checkbox', name: 'deleteEntitiesMissingFromFile',
				qtip: 'If checked, any source section included in the file is considered to be a FULL list.  So anything in the file is inserted/updated.  Anything missing from the file will be deleted.'
			},
			{xtype: 'sectionheaderfield', header: 'Default Field Values'},
			{xtype: 'label', html: 'If the following fields apply to the uploaded entities and the values are not supplied in the file, they will default to the values entered below.'},
			{fieldLabel: 'Entity View Type', qtip: 'If not supplied in the file will only apply default value to securable entities.', name: 'entityViewType.name', hiddenName: 'entityViewType.id', xtype: 'combo', url: 'portalEntityViewTypeListFind.json'},
			{fieldLabel: 'Portal Contact Team Email', qtip: 'This the department email that receives emails from clients when they click Contact Us from the Client Institutional Center. When selected, this will override the Minneapolis teams (defined by file categories and investment teams).', name: 'portalContactTeamEmail'},
			{fieldLabel: 'Portal Support Team Email', qtip: 'This the department email that manages posting the files for all applicable categories for clients/accounts under this relationship. When populated, this will override the Minneapolis teams (defined by file categories and investment teams). Used for notifications when there are files pending approval for any file posted to entities under this relationship.', name: 'portalSupportTeamEmail'}
		],

		getSaveURL: function() {
			return 'portalEntityUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		}

	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				const result = action.result.data;
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Portal Entity Upload Status',
					defaultData: {status: result}
				});
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
