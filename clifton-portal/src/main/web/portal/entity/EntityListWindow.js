Clifton.portal.entity.EntityListWindow = Ext.extend(TCG.app.Window, {
	id: 'entityListWindow',
	title: 'Portal Entities',
	iconCls: 'list',
	width: 1500,
	height: 700,


	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Portal Entities',
				items: [{
					name: 'portalEntityListFind',
					xtype: 'gridpanel',
					instructions: 'A portal entity is a record from an outside system. Based on the entity type, the entity can contain additional field values that can be used for display. Red highlighted rows indicate securable entities that are in a pending termination phase (still active but will be disabled soon).',
					groupField: 'entitySourceSection.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Entities" : "Entity"]}',
					topToolbarSearchParameter: 'searchPattern',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Source System', dataIndex: 'entitySourceSection.sourceSystem.name', width: 50, filter: {type: 'combo', searchFieldName: 'entitySourceSystemId', url: 'portalEntitySourceSystemListFind.json'}},
						{header: 'Source Section', dataIndex: 'entitySourceSection.name', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'entitySourceSectionId', url: 'portalEntitySourceSectionListFind.json', displayField: 'nameWithSourceSystem'}},
						{header: 'View Type', dataIndex: 'entityViewType.name', width: 75, hidden: true, filter: {type: 'combo', searchFieldName: 'entityViewTypeId', url: 'portalEntityViewTypeListFind.json'}},
						{header: 'Entity Type', width: 50, dataIndex: 'entitySourceSection.entityType.name', hidden: true},
						{
							header: 'Entity Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
							renderer: function(value, metaData, record) {
								const pendingTermination = record.get('pendingTermination');
								if (TCG.isTrue(pendingTermination)) {
									metaData.css += 'x-grid3-row-highlight-light-red';
									metaData.attr = TCG.renderQtip('Pending Termination');
								}
								return value;
							}

						},
						{header: 'Entity Label', width: 100, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Entity Description', width: 100, dataIndex: 'description', hidden: true, searchFieldName: 'searchPattern'},
						{header: 'Parent Entity', width: 100, dataIndex: 'parentPortalEntity.label'},
						{header: 'FKFieldID', width: 30, dataIndex: 'sourceFkFieldId', hidden: true},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'},
						{header: 'Start Date', width: 30, dataIndex: 'startDate'},
						{header: 'End Date', width: 30, dataIndex: 'endDate'},
						{header: 'Termination Date', width: 30, dataIndex: 'terminationDate'},
						{header: 'Pending Termination', width: 30, dataIndex: 'pendingTermination', type: 'boolean', tooltip: 'Entity is still active, but termination date is set and will be de-activated on end date.'},
						{header: 'Last Updated On', width: 30, dataIndex: 'updateDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'View Type', xtype: 'toolbar-combo', name: 'viewTypeId', width: 120, url: 'portalEntityViewTypeListFind.json', linkedFilter: 'entityViewType.name'},
							{fieldLabel: 'Source Section', xtype: 'toolbar-combo', name: 'sourceSectionId', width: 180, url: 'portalEntitySourceSectionListFind.json', linkedFilter: 'entitySourceSection.name', displayField: 'nameWithSourceSystem'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.EntityWindow'
					}
				}]
			},

			{
				title: 'Source Sections',
				items: [{
					name: 'portalEntitySourceSectionListFind',
					xtype: 'gridpanel',
					instructions: 'Portal Entity Source Section defines a specific table/set of data for a source system.',
					groupField: 'entityType.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Sections" : "Section"]}',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Entity Type', width: 50, dataIndex: 'entityType.name', hidden: true, filter: {searchFieldName: 'entityTypeId', type: 'combo', url: 'portalEntityTypeListFind.json'}},
						{header: 'Section Name', width: 75, dataIndex: 'name'},
						{header: 'Section Label', width: 75, dataIndex: 'label'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Child Entity Type', width: 50, dataIndex: 'childEntityType.name', filter: {searchFieldName: 'childEntityTypeId', type: 'combo', url: 'portalEntityTypeListFind.json?securableEntity=true'}},
						{header: 'Source System', width: 50, dataIndex: 'sourceSystem.name', filter: {searchFieldName: 'sourceSystemId', type: 'combo', url: 'portalEntitySourceSystemList.json', loadAll: true}},
						{header: 'Source Table Name', width: 75, dataIndex: 'sourceSystemTableName'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.setup.EntitySourceSectionWindow'
					}
				}]
			},
			{
				title: 'Entity Types',
				items: [{
					name: 'portalEntityTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Portal Entity Types define the type of source data a specific source section record is.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 75, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Postable', width: 50, dataIndex: 'postableEntity', type: 'boolean', tooltip: 'An entity type that can be posted to (Client, Account, Service)'},
						{header: 'Securable', width: 50, dataIndex: 'securableEntity', type: 'boolean', tooltip: 'A securable entity is a special type of postable entity that differentiates those that our customers log in/secure (Accounts) from other ways we post (Services)'},
						{header: 'File Source', width: 50, dataIndex: 'fileSource', type: 'boolean', tooltip: 'An entity type that can be referenced as the file source, like an Invoice, Portfolio Run, Contact'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.setup.EntityTypeWindow'
					}
				}]
			},
			{
				title: 'Source Systems',
				items: [{
					name: 'portalEntitySourceSystemList',
					xtype: 'gridpanel',
					instructions: 'Portal Entity Source Systems defines systems that populate the portal data.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'System Name', width: 100, dataIndex: 'name'},
						{header: 'Priority', width: 20, dataIndex: 'priorityOrder', type: 'int', tooltip: 'If there is a common entity across systems, the one with the smallest priority will be the one responsible for it updates'},
						{header: 'Administrators Only', width: 35, type: 'boolean', dataIndex: 'administratorOnly', tooltip: 'Outside of API calls from the source system, if true then ONLY system administrators can edit portal entities for this source system.  Otherwise, Portal Administrators can edit (i.e. Salesforce). These edits can come only from uploads if not directly from the source system.'},
						{header: 'Description', width: 200, dataIndex: 'description'}
					]
				}]
			},

			{
				title: 'View Types',
				items: [{
					name: 'portalEntityViewTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Portal Entity View Types are used to broadly differentiate portal entities. The difference can be used to drive how the system looks for a user as well as to separate entities for reporting purposes.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'View Type Name', width: 75, dataIndex: 'name'},
						{header: 'Default View', dataIndex: 'defaultView', width: 35, type: 'boolean', tooltip: 'Used to determine the default when none or multiple views apply.'},
						{header: 'Default Contact Email', width: 75, dataIndex: 'defaultContactEmailAddress', tooltip: 'The default "contact us" type email address to use for general portal questions.'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.entity.setup.EntityViewTypeWindow'
					}
				}]
			}


		]
	}]
});


