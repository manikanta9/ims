Clifton.portal.tracking.EventWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Tracking Event',
	iconCls: 'event',
	width: 750,
	height: 500,

	items: [{
		xtype: 'portal-formpanel',
		labelWidth: 160,
		labelFieldName: 'id',
		url: 'portalTrackingEventExtended.json',
		readOnly: true,

		listeners: {
			afterload: function(panel) {
				if (TCG.isBlank(panel.getFormValue('portalEntity.id'))) {
					panel.hideField('portalEntity.entityLabelWithSourceSection');
				}
				if (TCG.isBlank(panel.getFormValue('portalFile.id'))) {
					panel.hideField('portalFile.displayName');
				}
				if (TCG.isBlank(panel.getFormValue('portalFileCategory.id'))) {
					panel.hideField('portalFileCategory.nameExpanded');
				}
				if (TCG.isBlank(panel.getFormValue('portalNotification.id'))) {
					panel.hideField('portalNotification.label');
				}

			}
		},

		items: [
			{xtype: 'sectionheaderfield', header: 'User Information'},
			{fieldLabel: 'User', name: 'portalSecurityUser.label', detailIdField: 'portalSecurityUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.security.user.UserWindow'},
			{fieldLabel: 'Company', name: 'portalSecurityUser.companyName', xtype: 'displayfield'},

			{xtype: 'sectionheaderfield', header: 'Event Information'},
			{fieldLabel: 'Event Type', name: 'trackingEventType.name', xtype: 'displayfield'},
			{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'displayfield'},

			{fieldLabel: 'Portal Entity', name: 'portalEntity.entityLabelWithSourceSection', detailIdField: 'portalEntity.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.entity.EntityWindow'},
			{fieldLabel: 'Portal File', name: 'portalFile.displayName', detailIdField: 'portalFile.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.file.FileWindow'},
			{fieldLabel: 'Portal File Category', name: 'portalFileCategory.nameExpanded', detailIdField: 'portalFileCategory.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow'},
			{fieldLabel: 'Portal Notification', name: 'portalNotification.label', detailIdField: 'portalNotification.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.notification.NotificationWindow'},
			{fieldLabel: 'Source Entity', name: 'sourceEntityLabel', xtype: 'displayfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},

			{
				xtype: 'fieldset', title: 'IP Information', collapsed: true,
				items: [
					{fieldLabel: 'IP Address', name: 'portalTrackingIpData.ipAddress', xtype: 'displayfield'},
					{fieldLabel: 'Country Code', name: 'portalTrackingIpData.countryCode', xtype: 'displayfield'},
					{fieldLabel: 'City/State', name: 'portalTrackingIpData.cityState', xtype: 'displayfield'}
				]
			}
		]
	}]
});
