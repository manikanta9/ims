Clifton.portal.tracking.EventTypeWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Portal Tracking Event Type',
	iconCls: 'event',
	width: 750,
	height: 300,

	items: [{
		xtype: 'portal-formpanel',
		url: 'portalTrackingEventType.json',

		items: [
			{fieldLabel: 'Type Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Source Table', name: 'sourceTableName', xtype: 'displayfield'},
			{fieldLabel: '', boxLabel: 'Exclude Internal Employees (Leave un-checked to create events for all users)', xtype: 'checkbox', name: 'excludeNonPortalEntitySpecific'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true}
		]
	}]
});
