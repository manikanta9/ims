Clifton.portal.tracking.TrackingSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'trackingSetupWindow',
	title: 'Event Tracking',
	iconCls: 'event',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Logins',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					name: 'portalTrackingEventListFind',
					instructions: 'The following login events have been logged in the system.',
					eventTypeName: 'Login',
					columnOverrides: [
						{dataIndex: 'portalTrackingIpData.cityState', hidden: false},
						{dataIndex: 'portalTrackingIpData.countryCode', hidden: false}
					],
					additionalColumns: [] // Empty
				}]
			},
			{
				title: 'File Downloads',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					eventTypeName: 'File Download',
					populateRelationshipManagerNames: true,
					instructions: 'The following file download events have been logged in the system.',
					columnOverrides: [
						{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false}
					],
					additionalColumns: [
						{header: 'Relationship Manager(s)', width: 100, dataIndex: 'relationshipManagerNames', nonPersistentField: true},
						{header: 'File Category', width: 150, dataIndex: 'portalFileCategory.nameExpanded', filter: {searchFieldName: 'fileCategoryIdExpanded', type: 'combo', url: 'portalFileCategoryListFind.json', displayField: 'nameExpanded'}},
						{header: 'File', width: 75, dataIndex: 'portalFile.displayName', filter: {searchFieldName: 'fileDisplayName'}},
						{header: 'URL Parameters', width: 75, dataIndex: 'description', renderer: TCG.stripTags}
					]
				}]
			},
			{
				title: 'File List View',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					eventTypeName: 'File List View',
					instructions: 'The following file list view events have been logged in the system. These are logged when a user is viewing/searching for files for a given category.',
					columnOverrides: [
						{dataIndex: 'portalEntity.entityLabelWithSourceSection', hidden: false}
					],
					additionalColumns: [
						{header: 'File Category', width: 150, dataIndex: 'portalFileCategory.nameExpanded', filter: {searchFieldName: 'fileCategoryIdExpanded', type: 'combo', url: 'portalFileCategoryListFind.json', displayField: 'nameExpanded'}},
						{header: 'URL Parameters', width: 75, dataIndex: 'description', renderer: TCG.stripTags}
					]
				}]
			},
			{
				title: 'Audit Trail',
				items: [{
					xtype: 'portal-tracking-event-list-grid',
					eventTypeNames: ['Audit Trail: Insert', 'Audit Trail: Update', 'Audit Trail: Delete'],
					instructions: 'The following audit trail events have been logged by the system.',
					additionalColumns: [
						{header: 'Table Name', width: 75, dataIndex: 'sourceTableName'},
						{header: 'Description', width: 150, dataIndex: 'description', renderer: TCG.stripTags}
					]
				}]
			},

			{
				title: 'All Events',
				items: [{
					xtype: 'portal-tracking-event-list-grid'
					// No Column Overrides?
				}]
			},
			{
				title: 'Event Types',
				items: [{
					name: 'portalTrackingEventTypeList.json',
					xtype: 'gridpanel',
					instructions: 'Tracking event types define a specific action type that a user can do.  For example, Login, Download a File, etc.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Type Name', width: 75, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{header: 'Source Table', width: 75, dataIndex: 'sourceTableName'},
						{header: 'Exclude Internal Employees', width: 75, dataIndex: 'excludeNonPortalEntitySpecific', type: 'boolean', tooltip: 'When checked, tracking for this event type is not logged for our internal employees (i.e. role is not portal entity specific)'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.tracking.EventTypeWindow'
					}
				}]
			}
		]
	}]
});


