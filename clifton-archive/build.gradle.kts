import com.clifton.gradle.plugin.build.registerVariant

// Provide separate variants for consumers
// registerVariant("client", dependsOnMain = true, primaryCapability = true)
// registerVariant("server", dependsOnMain = true, primaryCapability = true)
// TODO: Pulling in both variants is temporarily allowed in order to allow IMS to cover both functions until the Archive application is activated
registerVariant("client", dependsOnMain = true)
registerVariant("server", dependsOnMain = true)

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	api(project(":clifton-system"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
}
