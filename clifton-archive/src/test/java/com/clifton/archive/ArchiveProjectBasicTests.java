package com.clifton.archive;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


/**
 * @author NickK
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ArchiveProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "archive";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("descriptor");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.hibernate.transform.Transformers");
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("entityJsonLength");
	}
}
