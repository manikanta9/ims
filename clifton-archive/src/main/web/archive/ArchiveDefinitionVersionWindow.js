Clifton.archive.ArchiveDefinitionVersionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Archive Definition Version',
	iconCls: 'archive',
	height: 600,
	width: 800,
	hideApplyButton: true,
	hideCancelButton: true,

	items: [
		{
			xtype: 'formpanel',
			url: 'archiveDefinitionVersion.json',
			readOnly: true,

			addToolbarButtons: function(toolBar) {
				const fp = this;

				toolBar.add({
					text: 'Copy Entity JSON Template',
					tooltip: 'Copy the entity JSON template to clipboard.',
					iconCls: 'copy',
					scope: fp,
					handler: function() {
						Clifton.archive.CopyFormPanelJsonEditorContent(fp);
					}
				});
			},

			listeners: {
				change: function() {
					Clifton.archive.UpdateFormPanelJsonEditorContent(this, this.getFormValue('entityTemplate'));
				}
			},

			getFormLabel: function() {
				return this.getFormValue('definition.name') + ' (' + this.getFormValue('version') + ')';
			},

			items: [
				{name: 'id', hidden: true},
				{fieldLabel: 'Name', name: 'definition.name', xtype: 'displayfield'},
				{fieldLabel: 'Description', name: 'definition.description', xtype: 'textarea', height: 50, anchor: '-56'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						columnWidth: .50,
						layout: 'form',
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [
								{fieldLabel: 'Version', name: 'version', xtype: 'integerfield'},
								{fieldLabel: 'Template Length', name: 'entityTemplateLength', xtype: 'integerfield'}
							]
						},
						{
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
								{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
							]
						}
					]
				},
				{
					xtype: 'archive-json-editor',
					height: 350,
					options: {
						name: 'entityTemplate',
						mode: 'view'
					}
				}
			]
		}
	]
});
