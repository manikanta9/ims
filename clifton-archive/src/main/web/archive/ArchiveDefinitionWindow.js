Clifton.archive.ArchiveDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Archive Definition',
	iconCls: 'archive',
	height: 600,
	width: 800,
	hideApplyButton: true,
	hideCancelButton: true,

	items: [
		{
			xtype: 'formpanel',
			url: 'archiveDefinition.json',
			readOnly: true,
			layout: 'form',
			controlWindowModified: false,

			listRequestedProperties: 'id|definition.name|version|startDate|endDate|active|entityTemplateLength',
			listRequestedPropertiesRoot: 'data.versionList',

			getLoadURL: function() {
				return this.url + '?requestedPropertiesRoot=' + this.listRequestedPropertiesRoot + '&requestedProperties=' + this.listRequestedProperties;
			},

			getFormLabel: function() {
				return this.getFormValue('name');
			},

			items: [
				{name: 'id', hidden: true},
				{fieldLabel: 'Definition Name', name: 'name', xtype: 'displayfield'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
				{fieldLabel: 'Entity Source', name: 'entitySourceName', xtype: 'displayfield'},
				{fieldLabel: 'Parent Source', name: 'parentEntitySourceName', xtype: 'displayfield'},
				{
					name: 'archiveDefinitionVersionListFind',
					xtype: 'gridpanel',
					title: 'Versions',
					instructions: 'A list of definition versions for this definition sorted by version (descending).',
					rowSelectionModel: 'multiple',
					appendStandardColumns: false,
					readOnly: true,
					heightResized: true,
					frame: true,
					reloadOnRender: false,
					height: 375,

					listeners: {
						afterrender: function() {
							const formPanel = TCG.getParentFormPanel(this);
							formPanel.on('afterload', this.reload, this, {delay: 100});
						},
						afterlayout: TCG.grid.registerDynamicHeightResizing
					},

					getLoadParams: function(firstLoad) {
						const formPanel = TCG.getParentFormPanel(this);
						const parentDefinitionName = formPanel.getFormValue('name');

						return Ext.applyIf({
							definitionName: parentDefinitionName,
							orderBy: 'version:DESC'
						}, TCG.grid.GridPanel.prototype.getLoadParams.call(this, firstLoad));
					},

					addToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Compare Versions',
							tooltip: 'Compare the entity JSON templates between two versions.',
							iconCls: 'diff',
							scope: gridPanel,
							handler: function() {
								const selectionModel = this.grid.getSelectionModel();
								if (selectionModel.getCount() < 2) {
									TCG.showError('Please select two rows to compare.', 'Less Than Two Rows Selected');
								}
								else if (selectionModel.getCount() > 2) {
									TCG.showError('Only two rows can be selected. Please select only two rows.', 'NOT SUPPORTED');
								}
								else {
									const selections = selectionModel.getSelections();
									const row1 = selections[0].json,
										row2 = selections[1].json;
									if ((row1.definition.name === row2.definition.name)) {
										const entityJsonPromise1 = TCG.data.getDataPromise('archiveDefinitionVersion.json?requestedPropertiesRoot=data&requestedProperties=entityTemplate', this, {params: {id: row1.id}})
												.then(entity => entity && entity.entityTemplate),
											entityJsonPromise2 = TCG.data.getDataPromise('archiveDefinitionVersion.json?requestedPropertiesRoot=data&requestedProperties=entityTemplate', this, {params: {id: row2.id}})
												.then(entity => entity && entity.entityTemplate);
										Promise.all([entityJsonPromise1, entityJsonPromise2])
											.then(function(values) {
												Clifton.archive.ShowJsonDifference(values[0], values[1]);
											});
									}
									else {
										TCG.showError('Unable to compare rows. Please select two rows of the same archive definition type.', 'NOT SUPPORTED');
									}
								}
							}
						}, '-');
					},

					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'Definition Name', dataIndex: 'definition.name', width: 100},
						{header: 'Version', dataIndex: 'version', type: 'int', width: 50},
						{header: 'Active', dataIndex: 'active', type: 'boolean', width: 50},
						{header: 'Start Date', dataIndex: 'startDate', type: 'date', hidden: true, width: 75},
						{header: 'End Date', dataIndex: 'endDate', type: 'date', hidden: true, width: 75},
						{header: 'Template Length', dataIndex: 'entityTemplateLength', type: 'int', sortable: false, filter: false, width: 50}
					],

					editor: {
						detailPageClass: 'Clifton.archive.ArchiveDefinitionVersionWindow',
						drillDownOnly: true
					}
				}
			]
		}

	]
});
