/**
 * Detail window that will show an archived entity. This window has support for both
 * ArchiveEntity and ArchiveEntityDescriptor Java types; determined by the useArchiveEntityDescriptor
 * boolean property passed to the window upon load. By default, ArchiveEntity type is used.
 */
Clifton.archive.ArchiveEntityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Archive Entity',
	iconCls: 'archive',
	height: 600,
	width: 800,
	hideApplyButton: true,
	hideCancelButton: true,

	items: [{
		xtype: 'formpanel',
		url: 'archiveEntity.json',
		readOnly: true,
		loadDefaultDataAfterRender: true,

		addToolbarButtons: function(toolBar) {
			const fp = this;

			toolBar.add({
				text: 'Copy Entity JSON',
				tooltip: 'Copy the entity JSON to clipboard.',
				iconCls: 'copy',
				scope: fp,
				handler: function() {
					Clifton.archive.CopyFormPanelJsonEditorContent(fp);
				}
			});
		},

		listeners: {
			afterrender: function() {
				if (TCG.isTrue(this.getWindow().useArchiveEntityDescriptor)) {
					// live preview is an ArchiveEntityDescriptor and has definition name direct
					const form = this.getForm();
					form.findField('definitionVersion.definition.name').setVisible(false);
					form.findField('definitionName').setVisible(true);
					form.findField('definitionVersion.version').setVisible(false);
					form.findField('definitionVersion').setVisible(true);
				}
			},
			change: function() {
				Clifton.archive.UpdateFormPanelJsonEditorContent(this, this.getFormValue('entityJson'));
			}
		},

		getFormLabel: function() {
			if (TCG.isBlank(this.getFormValue('id'))) {
				return 'PREVIEW';
			}
			else {
				return this.getFormValue('definitionVersion.definition.name') + ' (' + this.getFormValue('definitionVersion.version') + ') - ' + this.getFormValue('id');
			}
		},

		getSourceScreenForDataPath: function(formDataPath, alternateFormDataPath) {
			const formValues = this.getForm().formValues;
			let entitySource = TCG.getValue(formDataPath, formValues);
			if (TCG.isBlank(entitySource)) {
				entitySource = TCG.getValue(alternateFormDataPath, formValues);
			}
			if (!TCG.isBlank(entitySource)) {
				const sourceTable = TCG.data.getData('systemTableByName.json?tableName=' + entitySource, this, 'system.table.' + entitySource);
				if (sourceTable) {
					return sourceTable.detailScreenClass;
				}
			}
			return null;
		},

		items: [
			{name: 'id', hidden: true},
			{fieldLabel: 'Definition Name', name: 'definitionVersion.definition.name', xtype: 'displayfield'},
			{fieldLabel: 'Definition Name', name: 'definitionName', xtype: 'displayfield', hidden: true},
			{
				xtype: 'panel',
				layout: 'column',
				defaults: {
					columnWidth: .33,
					layout: 'form',
					defaults: {anchor: '-20'}
				},
				items: [
					{
						items: [
							{
								fieldLabel: 'Entity ID', name: 'entityIdentifier', xtype: 'linkfield', detailIdField: 'entityIdentifier',
								getDetailPageClass: function(fp) {
									return fp.getSourceScreenForDataPath('definitionVersion.definition.entitySourceName', 'entitySourceName');
								}
							},
							{
								fieldLabel: 'Parent ID', name: 'parentEntityIdentifier', xtype: 'linkfield', detailIdField: 'parentEntityIdentifier',
								getDetailPageClass: function(fp) {
									return fp.getSourceScreenForDataPath('definitionVersion.definition.parentEntitySourceName', 'parentEntitySourceName');
								}
							}
						]
					},
					{
						items: [
							{fieldLabel: 'Definition Version', name: 'definitionVersion.version', xtype: 'integerfield'},
							{fieldLabel: 'Definition Version', name: 'definitionVersion', xtype: 'integerfield', hidden: true},
							{fieldLabel: 'Content Length', name: 'entityJsonLength', xtype: 'integerfield'}
						]
					},
					{
						items: [
							{fieldLabel: 'Archived Date', name: 'archiveDate', xtype: 'datefield'},
							{fieldLabel: 'Deleted Date', name: 'deletedDate', xtype: 'datefield'}
						]
					}
				]
			},
			{
				xtype: 'archive-json-editor',
				height: 400,
				options: {
					name: 'entityJson',
					mode: 'view'
				}
			}
		]
	}]
});
