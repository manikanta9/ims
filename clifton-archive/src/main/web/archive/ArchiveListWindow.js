Clifton.archive.ArchiveListWindow = Ext.extend(TCG.app.Window, {
	id: 'archiveListWindow',
	title: 'Archived Entities',
	iconCls: 'archive',
	width: 1000,
	height: 600,

	items: [
		{
			xtype: 'tabpanel',
			items: [
				{
					title: 'Entities',
					items: [
						{
							xtype: 'archive-entity-gridpanel'
						}
					]
				},


				{
					title: 'Archive Definitions',
					items: [
						{
							name: 'archiveDefinitionList',
							xtype: 'gridpanel',
							instructions: 'A list of all archive definition types.',
							columns: [
								{header: 'ID', dataIndex: 'id', hidden: true},
								{header: 'Definition Name', dataIndex: 'name', width: 30},
								{header: 'Description', dataIndex: 'description', width: 60},
								{header: 'Entity Source', dataIndex: 'entitySourceName', width: 30},
								{header: 'Parent Source', dataIndex: 'parentEntitySourceName', width: 30}
							],

							editor: {
								detailPageClass: 'Clifton.archive.ArchiveDefinitionWindow',
								drillDownOnly: true
							}
						}
					]
				},


				{
					title: 'Accounting Journal Archiving',
					items: [
						{
							xtype: 'formpanel',
							bodyStyle: 'padding: 5px 0 0 0',
							defaults: {
								anchor: '0'
							},
							items: [
								{
									title: 'Accounting Journal Types',
									xtype: 'gridpanel',
									name: 'accountingJournalTypeList',
									instructions: 'Journals types define available types of journals. Subsystem journals are journals generated automatically by corresponding subsystems: trading, mark to market, etc.',
									readOnly: true,
									heightResized: true,
									frame: true,
									height: 325,

									listeners: {
										afterlayout: TCG.grid.registerDynamicHeightResizing
									},

									addToolbarButtons: function(toolBar, gridPanel) {
										toolBar.add({
											text: 'Enable Archiving',
											tooltip: 'Enable the select journal type for archival when booked.',
											iconCls: 'add',
											handler: function() {
												const selectionModel = gridPanel.grid.getSelectionModel();
												if (selectionModel.getCount() < 1) {
													TCG.showError('Please select a journal type to enable.', 'No Row Selected');
												}
												else {
													const journalTypeId = selectionModel.getSelections()[0].json.id;
													TCG.data.getDataPromise('accountingJournalTypeArchivingEnable.json', this, {params: {id: journalTypeId, enable: true}})
														.then(() => gridPanel.reload());
												}
											}
										}, '-');
										toolBar.add({
											text: 'Disable Archiving',
											tooltip: 'Disable the select journal type for archival when booked.',
											iconCls: 'remove',
											handler: function() {
												const selectionModel = gridPanel.grid.getSelectionModel();
												if (selectionModel.getCount() < 1) {
													TCG.showError('Please select a journal type to disable.', 'No Row Selected');
												}
												else {
													const journalTypeId = selectionModel.getSelections()[0].json.id;
													TCG.data.getDataPromise('accountingJournalTypeArchivingEnable.json', this, {params: {id: journalTypeId, enable: false}})
														.then(() => gridPanel.reload());
												}
											}
										}, '-');
										toolBar.add({
											text: 'Start Archiving',
											tooltip: 'Start archiving historic journals for the select journal type.',
											iconCls: 'run',
											handler: function() {
												const selectionModel = gridPanel.grid.getSelectionModel();
												if (selectionModel.getCount() < 1) {
													TCG.showError('Please select a journal type to start archiving.', 'No Row Selected');
												}
												else {
													const formPanel = TCG.getParentFormPanel(gridPanel);
													const journalTypeId = selectionModel.getSelections()[0].json.id;
													TCG.data.getDataPromise('accountingJournalArchiveOfType.json', this, {params: {id: journalTypeId}})
														.then(status => TCG.getChildByName(formPanel, 'runnerConfigListFind').reload());
												}
											}
										}, '-');
									},

									columns: [
										{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
										{header: 'Type Name', dataIndex: 'name', width: 100},
										{header: 'Description', dataIndex: 'description', width: 250, hidden: true},
										{header: 'Subsystem', dataIndex: 'subsystemJournal', width: 50, type: 'boolean', hidden: true},
										{header: 'Subsystem Table', dataIndex: 'systemTable.name', width: 100, filter: {type: 'combo', searchFieldName: 'systemTableId', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
										{header: 'Max Count', dataIndex: 'maxSubsystemJournalCount', width: 50, type: 'int', hidden: true, useNull: true, tooltip: 'Maximum number of journals allowed for a single sub-system entity'},
										{header: 'Archivable', dataIndex: 'archiveAllowed', width: 50, type: 'boolean', tooltip: 'If checked, journals of this journal type can be archived when booked.'}
									],

									editor: {
										detailPageClass: 'Clifton.accounting.journal.JournalTypeWindow',
										drillDownOnly: true
									}
								},
								{
									xtype: 'core-scheduled-runner-grid',
									typeName: 'JOURNAL-ARCHIVE',
									instantRunner: true,
									title: 'Currently Processing',
									height: 200,
									frame: true
								}
							]
						}
					]
				}
			]
		}
	]
});
