Ext.ns('Clifton.archive');

Clifton.archive.ShowJsonDifference = function(jsonString1, jsonString2) {
	if (TCG.isBlank(jsonString1) || TCG.isBlank(jsonString2)) {
		TCG.showError();
	}
	const json1 = JSON.parse(jsonString1),
		json2 = JSON.parse(jsonString2);
	TCG.showDifferences(JSON.stringify(json1, null, '\t'), JSON.stringify(json2, null, '\t'));
};

Clifton.archive.UpdateFormPanelJsonEditorContent = function(formPanel, content) {
	if (formPanel) {
		const jsonEditor = formPanel.find('name', 'json-editor');
		if (jsonEditor && jsonEditor.length === 1) {
			if (typeof content == 'object') {
				jsonEditor[0].setJson(content);
			}
			else {
				jsonEditor[0].setJsonText(content);
			}
		}
	}
};

Clifton.archive.CopyFormPanelJsonEditorContent = function(formPanel) {
	if (formPanel) {
		const jsonEditor = formPanel.find('name', 'json-editor');
		if (jsonEditor && jsonEditor.length === 1) {
			const textArea = document.createElement('textarea');
			textArea.value = jsonEditor[0].getJsonText();
			document.body.appendChild(textArea);
			textArea.select();
			try {
				document.execCommand('copy');
			}
			catch (err) {
				TCG.showError('Copying to clipboard failed, or is not supported in your browser. Error: (' + err.name + ') ' + err.message, 'Copy to Clipboard Failure');
			}
			finally {
				document.body.removeChild(textArea);
			}
		}
	}
};

Clifton.archive.JsonEditorContainer = Ext.extend(Ext.Container, {
	name: 'json-editor',
	jsonEditor: undefined,
	height: 325,
	options: {},

	listeners: {
		afterrender: function(panel) {
			this.jsonEditor = new JSONEditor(panel.el, this.options);
		},
		afterlayout: TCG.grid.registerDynamicHeightResizing
	},

	setJson: function(jsonObject) {
		this.jsonEditor.set(jsonObject);
	},

	setJsonText: function(jsonText) {
		this.jsonEditor.setText(jsonText);
	},

	getJsonText: function() {
		if (this.jsonEditor) {
			return JSON.stringify(this.jsonEditor.get(), null, '\t');
		}
	}
});
Ext.reg('archive-json-editor', Clifton.archive.JsonEditorContainer);

/**
 * Grid panel that will show a list of archived entities. This panel has support for both
 * ArchiveEntity and ArchiveEntityDescriptor Java types; determined by the useArchiveEntityDescriptor
 * boolean property. By default, ArchiveEntity type is used.
 *
 * Similar entities can be compared for textual differences in the archived JSON.
 */
Clifton.archive.ArchiveEntityGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'archiveEntityListFind',
	instructions: 'A list of all archived entities.',
	rowSelectionModel: 'multiple',
	// Flag to indicate that the list of entities is for ArchiveEntityDescriptor and not ArchiveEntity.
	useArchiveEntityDescriptor: false,
	includeTopToolbarDefinitionFilter: true,

	listeners: {
		afterrender: function() {
			const columnModel = this.getColumnModel();
			const entityDefinitionNameIndex = columnModel.findColumnIndex('definitionVersion.definition.name'),
				descriptorDefinitionNameIndex = columnModel.findColumnIndex('definitionName'),
				entityDefinitionVersionIndex = columnModel.findColumnIndex('definitionVersion.version'),
				descriptorDefinitionVersionIndex = columnModel.findColumnIndex('definitionVersion');
			if (this.useArchiveEntityDescriptor) {
				columnModel.setHidden(entityDefinitionNameIndex, true);
				columnModel.setHidden(descriptorDefinitionNameIndex, false);
				columnModel.setHidden(entityDefinitionVersionIndex, true);
				columnModel.setHidden(descriptorDefinitionVersionIndex, false);
			}
			else {
				columnModel.setHidden(entityDefinitionNameIndex, false);
				columnModel.setHidden(descriptorDefinitionNameIndex, true);
				columnModel.setHidden(entityDefinitionVersionIndex, false);
				columnModel.setHidden(descriptorDefinitionVersionIndex, true);
			}
		}
	},

	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Compare Entities',
			tooltip: 'Compare the entity JSON between two archived entities. Highlight earlier archived entity first to see change in chronological order',
			iconCls: 'diff',
			scope: gridPanel,
			handler: function() {
				const selectionModel = this.grid.getSelectionModel();
				if (selectionModel.getCount() < 2) {
					TCG.showError('Please select two rows to compare.', 'Less Than Two Rows Selected');
				}
				else if (selectionModel.getCount() > 2) {
					TCG.showError('Only two rows can be selected. Please select only two rows.', 'NOT SUPPORTED');
				}
				else {
					const selections = selectionModel.getSelections();
					const row1 = selections[0].json,
						row2 = selections[1].json;
					if ((gridPanel.getEntityDefinitionName(row1) === gridPanel.getEntityDefinitionName(row2)) && (row1.entityIdentifier === row2.entityIdentifier)) {
						const entityJsonPromise1 = TCG.data.getDataPromise('archiveEntity.json?requestedPropertiesRoot=data&requestedProperties=entityJson', this, {params: {id: row1.id}})
								.then(function(entity) {
									if (entity) {
										return entity.entityJson;
									}
								}),
							entityJsonPromise2 = TCG.data.getDataPromise('archiveEntity.json?requestedPropertiesRoot=data&requestedProperties=entityJson', this, {params: {id: row2.id}})
								.then(function(entity) {
									if (entity) {
										return entity.entityJson;
									}
								});
						Promise.all([entityJsonPromise1, entityJsonPromise2])
							.then(function(values) {
								Clifton.archive.ShowJsonDifference(values[0], values[1]);
							});
					}
					else {
						TCG.showError('Unable to compare rows. Please select two rows of the same archive definition type and entity identifier.', 'NOT SUPPORTED');
					}
				}
			}
		}, '-');
		this.additionalAddToolbarButtons(toolBar, gridPanel);
	},

	getEntityDefinitionName: function(entityJson) {
		return this.useArchiveEntityDescriptor ? entityJson.definitionName : entityJson.definitionVersion.definition.name;
	},

	additionalAddToolbarButtons: function(toolBar, gridPanel) {
		// nothing here by default
	},

	getTopToolbarFilters: function(toolbar) {
		if (this.includeTopToolbarDefinitionFilter) {
			return [
				{fieldLabel: 'Archive Definition', xtype: 'toolbar-combo', name: 'toolbarDefinitionId', width: 150, loadAll: true, url: 'archiveDefinitionList.json'}
			];
		}
	},

	columns: [
		{header: 'ID', dataIndex: 'id', hidden: true, width: 30},
		{header: 'Definition Name', dataIndex: 'definitionVersion.definition.name', filter: {type: 'combo', searchFieldName: 'definitionId', loadAll: true, url: 'archiveDefinitionList.json'}, width: 200},
		{header: 'Definition Name', dataIndex: 'definitionName', filter: {type: 'combo', searchFieldName: 'definitionId', loadAll: true, url: 'archiveDefinitionList.json'}, width: 200, hidden: true},
		{header: 'Definition Version', dataIndex: 'definitionVersion.version', type: 'int', filter: {searchFieldName: 'definitionVersion'}, width: 100},
		{header: 'Definition Version', dataIndex: 'definitionVersion', type: 'int', filter: {searchFieldName: 'definitionVersion'}, width: 100, hidden: true},
		{
			header: 'Entity ID', dataIndex: 'entityIdentifier', type: 'int', width: 100,
			renderer: function(v) {
				return TCG.isBlank(v) ? v : TCG.numberFormat(v, '0');
			}
		},
		{
			header: 'Parent ID', dataIndex: 'parentEntityIdentifier', type: 'int', useNull: true, width: 100,
			renderer: function(v) {
				return TCG.isBlank(v) ? v : TCG.numberFormat(v, '0');
			}
		},
		{header: 'Archive Date', dataIndex: 'archiveDate', type: 'date', width: 100, defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Deleted Date', dataIndex: 'deletedDate', type: 'date', width: 100},
		{header: 'Content Length', dataIndex: 'entityJsonLength', type: 'int', sortable: false, filter: false, width: 100}
	],

	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to midnight of current day to fetch today's items
			const currentDate = new Date();
			currentDate.setHours(0, 0, 0, 0);
			this.setFilterValue('archiveDate', {'after': currentDate});
		}

		const params = {
			excludeEntityJsonInResults: true
		};

		if (this.includeTopToolbarDefinitionFilter) {
			const definitionIdFilter = TCG.getChildByName(this.getTopToolbar(), 'toolbarDefinitionId');
			if (definitionIdFilter && !TCG.isBlank(definitionIdFilter.getValue())) {
				params.definitionId = definitionIdFilter.getValue();
			}
		}

		return params;
	},

	editor: {
		detailPageClass: 'Clifton.archive.ArchiveEntityWindow',
		drillDownOnly: true
	}
});
Ext.reg('archive-entity-gridpanel', Clifton.archive.ArchiveEntityGridPanel);
