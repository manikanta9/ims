package com.clifton.archive.descriptor;

import com.clifton.archive.descriptor.search.ArchiveEntityDescriptorSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * <code>ArchiveEntityDescriptorService</code> defines client side service methods for saving and getting archived entities.
 *
 * @author NickK
 */
public interface ArchiveEntityDescriptorService {

	/**
	 * Gets the {@link ArchiveEntityDescriptor} associated witht he provided ID.
	 */
	@SecureMethod(table = "ArchiveEntity")
	public ArchiveEntityDescriptor getArchiveEntityDescriptor(long id);


	/**
	 * Returns a list of {@link ArchiveEntityDescriptor}s matching the provided criteria.
	 */
	@SecureMethod(table = "ArchiveEntity")
	public List<ArchiveEntityDescriptor> getArchiveEntityDescriptorList(ArchiveEntityDescriptorSearchForm descriptorSearchForm);


	/**
	 * Archives an entity based upon provided details in the {@link ArchiveEntityDescriptor}.
	 * If the entity being archived is the first to use a new definition, the descriptor can
	 * be used to define the necessary details for the new definition.
	 */
	@SecureMethod(table = "ArchiveEntity")
	@RequestMapping("archiveEntitySave")
	public ArchiveEntityDescriptor archiveEntity(ArchiveEntityDescriptor descriptor);


	/**
	 * It is not possible to delete archived entities. Instead, the entities can be marked as deleted when the
	 * original entity was deleted from the external data source. The archived entities will be looked up
	 * by definition name and  entity ID.
	 */
	@SecureMethod(table = "ArchiveEntity")
	@RequestMapping("archiveEntityDeleted")
	public List<ArchiveEntityDescriptor> markArchiveEntityDescriptorDeleted(ArchiveEntityDescriptor descriptor);
}
