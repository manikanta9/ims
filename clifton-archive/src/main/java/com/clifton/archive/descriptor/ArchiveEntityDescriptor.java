package com.clifton.archive.descriptor;

import java.util.Date;


/**
 * <code>ArchiveEntityDescriptor</code> represents values for archiving an entity
 * from a client perspective. Each constructor defines a set of values required to
 * archive an entity.
 * <p>
 * This object can also be used to create definitions and definition version metadata
 * when saving an entity. Here are the required properties for saving each of the
 * metadata types.
 * <p>
 * Definition:
 * <br/>-definitionName
 * <br/>-entitySourceName
 * <br/>-(Optional) definitionDescription
 * <br/>-(Optional) parentEntitySourceName
 * <br/>-(Optional) definitionDescription
 * <p>
 * Definition Version:
 * <br/>-definitionName (to match a definition name)
 * <br/>-entityTemplate
 *
 * @author NickK
 */
public class ArchiveEntityDescriptor {

	private final String definitionName;

	private final Long entityIdentifier;
	private final Long parentEntityIdentifier;

	private final String entityJson;

	// Optional values
	private Long id;
	private String definitionDescription;
	private String entitySourceName;
	private String parentEntitySourceName;
	private Integer definitionVersion;
	private String entityTemplate;
	private Date archiveDate;
	private Date deletedDate;
	private int entityJsonLength;

	///////////////////////////////////////////////////////////////////////////


	public ArchiveEntityDescriptor(String definitionName, Long entityIdentifier, String entityJson) {
		this(definitionName, entityIdentifier, null, entityJson);
	}


	public ArchiveEntityDescriptor(String definitionName, Long entityIdentifier, Long parentEntityIdentifier, String entityJson) {
		this.definitionName = definitionName;
		this.entityIdentifier = entityIdentifier;
		this.parentEntityIdentifier = parentEntityIdentifier;
		this.entityJson = entityJson;
	}

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Get the name of the definition the entity is to be archived under.
	 * The definition name is essentially a partition key for archived entities.
	 */
	public String getDefinitionName() {
		return this.definitionName;
	}


	/**
	 * Get the ID of the archived entity.
	 */
	public Long getEntityIdentifier() {
		return this.entityIdentifier;
	}


	/**
	 * Get the ID of the archived entity's parent entity.
	 */
	public Long getParentEntityIdentifier() {
		return this.parentEntityIdentifier;
	}


	/**
	 * Get the JSON content of the archived entity.
	 */
	public String getEntityJson() {
		return this.entityJson;
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////             Getters and Setters                //////////////
	///////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * Get the description from the definition an entity was archived under.
	 */
	public String getDefinitionDescription() {
		return this.definitionDescription;
	}


	/**
	 * Set the description for the definition for an archived entity.
	 * <p>
	 * From a client perspective, setting the description is optional when creating a new archive
	 * definition when archiving an entity.
	 */
	public void setDefinitionDescription(String definitionDescription) {
		this.definitionDescription = definitionDescription;
	}


	/**
	 * Returns the source name for the entity. This name describes a table or DTO the entity is from
	 */
	public String getEntitySourceName() {
		return this.entitySourceName;
	}


	/**
	 * Set the source name for the entity. This name describes a table or DTO the entity is from.
	 * <p>
	 * From a client perspective, setting the entity source name is needed when creating a new archive
	 * definition when archiving an entity.
	 */
	public void setEntitySourceName(String entitySourceName) {
		this.entitySourceName = entitySourceName;
	}


	/**
	 * Returns the source name for the entity's parent entity. This name describe a table or DTO the entity's
	 * parent entity is from.
	 */
	public String getParentEntitySourceName() {
		return this.parentEntitySourceName;
	}


	/**
	 * Set the source name for the entity. This name is optional and describes a table or DTO the entity's parent is from.
	 * <p>
	 * From a client perspective, setting the parent entity source name is needed when creating a new archive definition
	 * when archiving an entity.
	 */
	public void setParentEntitySourceName(String parentEntitySourceName) {
		this.parentEntitySourceName = parentEntitySourceName;
	}


	/**
	 * Get the definition version this entity was archived with.
	 */
	public Integer getDefinitionVersion() {
		return this.definitionVersion;
	}


	/**
	 * Set the definition version the entity is to be archived with.
	 */
	public void setDefinitionVersion(Integer definitionVersion) {
		this.definitionVersion = definitionVersion;
	}


	/**
	 * Get the JSON template describing the entity, containing no values. This template is associated with
	 * the definition version for comparison between versions.
	 */
	public String getEntityTemplate() {
		return this.entityTemplate;
	}


	/**
	 * Set the JSON template describing the entity. The template should mimic the JSON serialized view of
	 * the entity, but without values. The template is used to compare different versions of archived entities.
	 * <p>
	 * From a client perspective, setting the entity template is necessary when creating a new archive
	 * definition when archiving an entity.
	 */
	public void setEntityTemplate(String entityTemplate) {
		this.entityTemplate = entityTemplate;
	}


	/**
	 * Returns the date this entity was archived.
	 */
	public Date getArchiveDate() {
		return this.archiveDate;
	}


	/**
	 * Set the date the entity should use when archiving. This can generally be avoided, but can
	 * be useful when archiving historic data so the historic entry matches the archived version.
	 */
	public void setArchiveDate(Date archiveDate) {
		this.archiveDate = archiveDate;
	}


	/**
	 * Returns the date this entity was deleted.
	 */
	public Date getDeletedDate() {
		return this.deletedDate;
	}


	/**
	 * This is not for client use, and ignored if used.
	 */
	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}


	/**
	 * Returns the length of the Entity's JSON representation.
	 */
	public int getEntityJsonLength() {
		return getEntityJson() == null ? this.entityJsonLength : getEntityJson().length();
	}


	/**
	 * This is not for client use, and ignored if used.
	 */
	public void setEntityJsonLength(int entityJsonLength) {
		this.entityJsonLength = entityJsonLength;
	}
}
