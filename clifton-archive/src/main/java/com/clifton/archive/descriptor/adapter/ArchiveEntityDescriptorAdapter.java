package com.clifton.archive.descriptor.adapter;

import com.clifton.archive.descriptor.ArchiveEntityDescriptor;


/**
 * <cod>ArchiveEntityDescriptorAdapter</cod> can be implemented to assist in converting
 * an entity that is to be archived into an {@link ArchiveEntityDescriptor}
 *
 * @author NickK
 */
public interface ArchiveEntityDescriptorAdapter<E> {

	public ArchiveEntityDescriptor toArchiveEntityDescriptor(E entity);


	public String toJson(E entity);


	public E fromArchiveEntityDescriptor(ArchiveEntityDescriptor archieEntityDescriptor);


	public E fromJson(String entityJson);
}
