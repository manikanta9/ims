package com.clifton.archive.descriptor.search;

import com.clifton.archive.descriptor.ArchiveEntityDescriptor;

import java.util.Date;


/**
 * <code>ArchiveEntityDescriptorSearchForm</code> defines search configuration for {@link ArchiveEntityDescriptor} objects.
 *
 * @author NickK
 */
public class ArchiveEntityDescriptorSearchForm {

	private String definitionName;

	private Long entityIdentifier;
	private Long parentEntityIdentifier;

	private Integer definitionVersion;
	private String entitySourceName;
	private String parentEntitySourceName;

	private Date archiveDate;
	private Date deletedDate;

	/**
	 * This property allows the rows returned from the DB to exclude the entity JSON content
	 * significantly reducing the query performance.
	 */
	private boolean excludeEntityJsonInResults;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Long getEntityIdentifier() {
		return this.entityIdentifier;
	}


	public void setEntityIdentifier(Long entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}


	public Long getParentEntityIdentifier() {
		return this.parentEntityIdentifier;
	}


	public void setParentEntityIdentifier(Long parentEntityIdentifier) {
		this.parentEntityIdentifier = parentEntityIdentifier;
	}


	public Integer getDefinitionVersion() {
		return this.definitionVersion;
	}


	public void setDefinitionVersion(Integer definitionVersion) {
		this.definitionVersion = definitionVersion;
	}


	public String getEntitySourceName() {
		return this.entitySourceName;
	}


	public void setEntitySourceName(String entitySourceName) {
		this.entitySourceName = entitySourceName;
	}


	public String getParentEntitySourceName() {
		return this.parentEntitySourceName;
	}


	public void setParentEntitySourceName(String parentEntitySourceName) {
		this.parentEntitySourceName = parentEntitySourceName;
	}


	public Date getArchiveDate() {
		return this.archiveDate;
	}


	public void setArchiveDate(Date archiveDate) {
		this.archiveDate = archiveDate;
	}


	public Date getDeletedDate() {
		return this.deletedDate;
	}


	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}


	public boolean isExcludeEntityJsonInResults() {
		return this.excludeEntityJsonInResults;
	}


	public void setExcludeEntityJsonInResults(boolean excludeEntityJsonInResults) {
		this.excludeEntityJsonInResults = excludeEntityJsonInResults;
	}
}
