package com.clifton.archive.server.definition;

import com.clifton.archive.server.definition.search.ArchiveDefinitionVersionSearchForm;

import java.util.List;


/**
 * <code>ArchiveDefinitionService</code> defines service methods for managing archive
 * entity definitions and versions.
 *
 * @author NickK
 */
public interface ArchiveDefinitionService {

	///////////////////////////////////////////////////////////////////////////
	/////////////              ArchiveDefinition APIs           ///////////////
	///////////////////////////////////////////////////////////////////////////


	public ArchiveDefinition getArchiveDefinition(short id);


	public ArchiveDefinition getArchiveDefinitionByName(String definitionName);


	public List<ArchiveDefinition> getArchiveDefinitionList();


	public ArchiveDefinition saveArchiveDefinition(ArchiveDefinition definition);


	///////////////////////////////////////////////////////////////////////////
	/////////////         ArchiveDefinitionVersion APIs         ///////////////
	///////////////////////////////////////////////////////////////////////////


	public ArchiveDefinitionVersion getArchiveDefinitionVersion(short id);


	public List<ArchiveDefinitionVersion> getArchiveDefinitionVersionListByName(String definitionName);


	public List<ArchiveDefinitionVersion> getArchiveDefinitionVersionList(ArchiveDefinitionVersionSearchForm archiveDefinitionVersionSearchForm);


	public ArchiveDefinitionVersion saveArchiveDefinitionVersion(ArchiveDefinitionVersion definitionVersion);
}
