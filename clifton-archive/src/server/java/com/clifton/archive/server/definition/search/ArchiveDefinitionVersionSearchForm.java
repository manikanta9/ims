package com.clifton.archive.server.definition.search;

import com.clifton.archive.server.definition.ArchiveDefinitionVersion;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntityDateRangeWithTimeSearchForm;


/**
 * <code>ArchiveDefinitionVersionSearchForm</code> defines search configuration for {@link ArchiveDefinitionVersion} objects.
 *
 * @author NickK
 */
public class ArchiveDefinitionVersionSearchForm extends BaseEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "definition.name,definition.entitySourceName,definition.parentEntitySourceName")
	private String searchPattern;

	@SearchField(searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchFieldPath = "definition", searchField = "name")
	private String definitionName;

	@SearchField(searchFieldPath = "definition", searchField = "entitySourceName")
	private String entitySourceName;

	@SearchField(searchFieldPath = "definition", searchField = "parentEntitySourceName")
	private String parentEntitySourceName;

	@SearchField
	private Integer version;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getEntitySourceName() {
		return this.entitySourceName;
	}


	public void setEntitySourceName(String entitySourceName) {
		this.entitySourceName = entitySourceName;
	}


	public String getParentEntitySourceName() {
		return this.parentEntitySourceName;
	}


	public void setParentEntitySourceName(String parentEntitySourceName) {
		this.parentEntitySourceName = parentEntitySourceName;
	}


	public Integer getVersion() {
		return this.version;
	}


	public void setVersion(Integer version) {
		this.version = version;
	}
}
