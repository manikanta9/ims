package com.clifton.archive.server;

import com.clifton.archive.server.search.ArchiveEntitySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @author NickK
 */
@Service
public class ArchiveEntityServiceImpl implements ArchiveEntityService {

	private AdvancedUpdatableDAO<ArchiveEntity, Criteria> archiveEntityDAO;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public ArchiveEntity getArchiveEntity(long id) {
		return getArchiveEntityDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ArchiveEntity> getArchiveEntityList(ArchiveEntitySearchForm entitySearchForm) {
		return getArchiveEntityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(entitySearchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (entitySearchForm.isExcludeEntityJsonInResults()) {
					// Exclude the JSON content from the returned list.
					ProjectionList projectionList = Projections.projectionList();
					criteria.setProjection(projectionList);
					for (String name : CollectionUtils.getIterable(BeanUtils.getPropertyNames(ArchiveEntity.class, true, true))) {
						if (!"entityJson".equals(name)) {
							if ("entityJsonLength".equals(name)) {
								projectionList.add(Projections.sqlProjection("LEN(EntityJson) as " + name, new String[]{name}, new Type[]{new IntegerType()}));
							}
							else {
								projectionList.add(Projections.property(name), name);
							}
						}
					}
					criteria.setResultTransformer(Transformers.aliasToBean(ArchiveEntity.class));
				}
			}
		});
	}


	@Override
	public ArchiveEntity saveArchiveEntity(ArchiveEntity entity) {
		if (entity != null) {
			ValidationUtils.assertNotNull(entity.getEntityIdentifier(), "An archive entity cannot be saved without an entity identifier specified.", "entityIdentifier");
			ValidationUtils.assertNotNull(entity.getEntityJson(), "An archive entity cannot be saved without an entity's JSON content.", "entityJson");
			ValidationUtils.assertNotNull(entity.getDefinitionVersion(), "An archive entity cannot be saved without an archive definition version.", "definitionVersion");

			if (entity.getArchiveDate() == null) {
				entity.setArchiveDate(new Date());
			}
			return getArchiveEntityDAO().save(entity);
		}
		return entity;
	}


	@Override
	public ArchiveEntity markArchiveEntityDeleted(long archiveEntityId) {
		ArchiveEntity entity = getArchiveEntityDAO().findByPrimaryKey(archiveEntityId);
		if (entity != null) {
			if (entity.getDeletedDate() == null) {
				entity.setDeletedDate(new Date());
				getArchiveEntityDAO().save(entity);
			}
		}
		return entity;
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////         Getters and Setters                  ///////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ArchiveEntity, Criteria> getArchiveEntityDAO() {
		return this.archiveEntityDAO;
	}


	public void setArchiveEntityDAO(AdvancedUpdatableDAO<ArchiveEntity, Criteria> archiveEntityDAO) {
		this.archiveEntityDAO = archiveEntityDAO;
	}
}
