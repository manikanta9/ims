package com.clifton.archive.server.definition;

import com.clifton.archive.server.definition.search.ArchiveDefinitionVersionSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author NickK
 */
@Service
public class ArchiveDefinitionServiceImpl implements ArchiveDefinitionService {

	private AdvancedUpdatableDAO<ArchiveDefinition, Criteria> archiveDefinitionDAO;
	private AdvancedUpdatableDAO<ArchiveDefinitionVersion, Criteria> archiveDefinitionVersionDAO;

	private DaoNamedEntityCache<ArchiveDefinition> archiveDefinitionCache;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public ArchiveDefinition getArchiveDefinition(short id) {
		return getArchiveDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public ArchiveDefinition getArchiveDefinitionByName(String definitionName) {
		return getArchiveDefinitionCache().getBeanForKeyValue(getArchiveDefinitionDAO(), definitionName);
	}


	@Override
	public List<ArchiveDefinition> getArchiveDefinitionList() {
		return getArchiveDefinitionDAO().findAll();
	}


	@Override
	public ArchiveDefinition saveArchiveDefinition(ArchiveDefinition definition) {
		return getArchiveDefinitionDAO().save(definition);
	}

	///////////////////////////////////////////////////////////////////////////


	@Override
	public ArchiveDefinitionVersion getArchiveDefinitionVersion(short id) {
		return getArchiveDefinitionVersionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ArchiveDefinitionVersion> getArchiveDefinitionVersionListByName(String definitionName) {
		ArchiveDefinitionVersionSearchForm searchForm = new ArchiveDefinitionVersionSearchForm();
		searchForm.setDefinitionName(definitionName);
		return getArchiveDefinitionVersionList(searchForm);
	}


	@Override
	public List<ArchiveDefinitionVersion> getArchiveDefinitionVersionList(ArchiveDefinitionVersionSearchForm archiveDefinitionVersionSearchForm) {
		return getArchiveDefinitionVersionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(archiveDefinitionVersionSearchForm));
	}


	@Override
	public ArchiveDefinitionVersion saveArchiveDefinitionVersion(ArchiveDefinitionVersion definitionVersion) {
		return getArchiveDefinitionVersionDAO().save(definitionVersion);
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////         Getters and Setters                  ///////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ArchiveDefinition, Criteria> getArchiveDefinitionDAO() {
		return this.archiveDefinitionDAO;
	}


	public void setArchiveDefinitionDAO(AdvancedUpdatableDAO<ArchiveDefinition, Criteria> archiveDefinitionDAO) {
		this.archiveDefinitionDAO = archiveDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ArchiveDefinitionVersion, Criteria> getArchiveDefinitionVersionDAO() {
		return this.archiveDefinitionVersionDAO;
	}


	public void setArchiveDefinitionVersionDAO(AdvancedUpdatableDAO<ArchiveDefinitionVersion, Criteria> archiveDefinitionVersionDAO) {
		this.archiveDefinitionVersionDAO = archiveDefinitionVersionDAO;
	}


	public DaoNamedEntityCache<ArchiveDefinition> getArchiveDefinitionCache() {
		return this.archiveDefinitionCache;
	}


	public void setArchiveDefinitionCache(DaoNamedEntityCache<ArchiveDefinition> archiveDefinitionCache) {
		this.archiveDefinitionCache = archiveDefinitionCache;
	}
}
