package com.clifton.archive.server.definition;

import com.clifton.core.beans.NamedSimpleEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>ArchiveDefinition</code> represents a type of entity that can be archived. A
 * definition's name must be unique. The definition defines a reference name for the
 * entity being archive, which could be the entity reference table or DTO name.
 * An optional parent reference can also be defined.
 *
 * @author NickK
 */
@CacheByName
public class ArchiveDefinition extends NamedSimpleEntity<Short> {

	/**
	 * The name of the external entity source. This could be the table or DTO name
	 * of the entities being archived for this definition.
	 */
	private String entitySourceName;
	/**
	 * The optional name of the external parent entity source.
	 */
	private String parentEntitySourceName;

	///////////////////////////////////////////////////////////////////////////


	public String getEntitySourceName() {
		return this.entitySourceName;
	}


	public void setEntitySourceName(String entitySourceName) {
		this.entitySourceName = entitySourceName;
	}


	public String getParentEntitySourceName() {
		return this.parentEntitySourceName;
	}


	public void setParentEntitySourceName(String parentEntitySourceName) {
		this.parentEntitySourceName = parentEntitySourceName;
	}
}
