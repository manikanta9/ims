package com.clifton.archive.server.search;

import com.clifton.archive.server.ArchiveEntity;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * <code>ArchiveEntitySearchForm</code> defines search configuration for {@link ArchiveEntity} objects.
 *
 * @author NickK
 */
public class ArchiveEntitySearchForm extends BaseEntitySearchForm {

	@SearchField(searchFieldPath = "definitionVersion", searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchFieldPath = "definitionVersion.definition", searchField = "name")
	private String definitionName;

	@SearchField(searchFieldPath = "definitionVersion.definition", searchField = "entitySourceName")
	private String entitySourceName;

	@SearchField(searchFieldPath = "definitionVersion.definition", searchField = "parentEntitySourceName")
	private String parentEntitySourceName;

	@SearchField(searchField = "definitionVersion.id")
	private Short definitionVersionId;

	@SearchField(searchFieldPath = "definitionVersion", searchField = "version")
	private Integer definitionVersion;

	@SearchField
	private Long entityIdentifier;
	@SearchField
	private Long parentEntityIdentifier;

	@SearchField
	private Date archiveDate;
	@SearchField
	private Date deletedDate;

	/**
	 * This property allows the rows returned from the DB to exclude the entity JSON content
	 * significantly reducing the query performance.
	 */
	private boolean excludeEntityJsonInResults;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@Override
	public void validate() {
		SearchUtils.validateRequiredFilter(this, "definitionVersion", "excludeEntityJsonInResults");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getEntitySourceName() {
		return this.entitySourceName;
	}


	public void setEntitySourceName(String entitySourceName) {
		this.entitySourceName = entitySourceName;
	}


	public String getParentEntitySourceName() {
		return this.parentEntitySourceName;
	}


	public void setParentEntitySourceName(String parentEntitySourceName) {
		this.parentEntitySourceName = parentEntitySourceName;
	}


	public Short getDefinitionVersionId() {
		return this.definitionVersionId;
	}


	public void setDefinitionVersionId(Short definitionVersionId) {
		this.definitionVersionId = definitionVersionId;
	}


	public Integer getDefinitionVersion() {
		return this.definitionVersion;
	}


	public void setDefinitionVersion(Integer definitionVersion) {
		this.definitionVersion = definitionVersion;
	}


	public Long getEntityIdentifier() {
		return this.entityIdentifier;
	}


	public void setEntityIdentifier(Long entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}


	public Long getParentEntityIdentifier() {
		return this.parentEntityIdentifier;
	}


	public void setParentEntityIdentifier(Long parentEntityIdentifier) {
		this.parentEntityIdentifier = parentEntityIdentifier;
	}


	public Date getArchiveDate() {
		return this.archiveDate;
	}


	public void setArchiveDate(Date archiveDate) {
		this.archiveDate = archiveDate;
	}


	public Date getDeletedDate() {
		return this.deletedDate;
	}


	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}


	public boolean isExcludeEntityJsonInResults() {
		return this.excludeEntityJsonInResults;
	}


	public void setExcludeEntityJsonInResults(boolean excludeEntityJsonInResults) {
		this.excludeEntityJsonInResults = excludeEntityJsonInResults;
	}
}
