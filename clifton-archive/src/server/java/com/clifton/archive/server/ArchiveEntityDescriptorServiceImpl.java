package com.clifton.archive.server;

import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.archive.descriptor.ArchiveEntityDescriptorService;
import com.clifton.archive.descriptor.search.ArchiveEntityDescriptorSearchForm;
import com.clifton.archive.server.definition.ArchiveDefinition;
import com.clifton.archive.server.definition.ArchiveDefinitionService;
import com.clifton.archive.server.definition.ArchiveDefinitionVersion;
import com.clifton.archive.server.search.ArchiveEntitySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author NickK
 */
@Service
public class ArchiveEntityDescriptorServiceImpl implements ArchiveEntityDescriptorService {

	private ArchiveDefinitionService archiveDefinitionService;
	private ArchiveEntityService archiveEntityService;

	///////////////////////////////////////////////////////////////////////////


	@Override
	public ArchiveEntityDescriptor getArchiveEntityDescriptor(long id) {
		return convertArchiveEntityToDescriptor(getArchiveEntityService().getArchiveEntity(id));
	}


	@Override
	public List<ArchiveEntityDescriptor> getArchiveEntityDescriptorList(ArchiveEntityDescriptorSearchForm descriptorSearchForm) {
		ArchiveEntitySearchForm entitySearchForm = new ArchiveEntitySearchForm();
		BeanUtils.copyProperties(descriptorSearchForm, entitySearchForm);
		List<ArchiveEntity> archiveEntityList = getArchiveEntityService().getArchiveEntityList(entitySearchForm);
		return CollectionUtils.getStream(archiveEntityList).map(this::convertArchiveEntityToDescriptor).collect(Collectors.toList());
	}


	@Override
	@Transactional
	public ArchiveEntityDescriptor archiveEntity(ArchiveEntityDescriptor descriptor) {
		ValidationUtils.assertNotNull(descriptor.getDefinitionName(), "Unable to archive an entity without specifying a definition name to archive the entity with.", "DefinitionName");

		ArchiveEntity entity = new ArchiveEntity();
		entity.setEntityIdentifier(descriptor.getEntityIdentifier());
		entity.setParentEntityIdentifier(descriptor.getParentEntityIdentifier());
		entity.setEntityJson(descriptor.getEntityJson());
		entity.setArchiveDate(descriptor.getArchiveDate());

		List<ArchiveDefinitionVersion> definitionVersionList = getArchiveDefinitionService().getArchiveDefinitionVersionListByName(descriptor.getDefinitionName());
		if (CollectionUtils.isEmpty(definitionVersionList)) {
			ArchiveDefinition definition = getArchiveDefinitionService().getArchiveDefinitionByName(descriptor.getDefinitionName());
			if (definition == null) {
				definition = createArchiveDefinition(descriptor);
			}
			ArchiveDefinitionVersion definitionVersion = createArchiveDefinitionVersion(definition, descriptor);
			entity.setDefinitionVersion(definitionVersion);
		}
		else {
			BeanUtils.sortWithFunctions(definitionVersionList, CollectionUtils.createList(ArchiveDefinitionVersion::getVersion), CollectionUtils.createList(false));
			Stream<ArchiveDefinitionVersion> definitionVersionStream = definitionVersionList.stream();
			if (descriptor.getDefinitionVersion() != null) {
				definitionVersionStream.filter(definitionVersion -> definitionVersion.getVersion().equals(descriptor.getDefinitionVersion()));
			}
			Optional<ArchiveDefinitionVersion> definitionVersion = definitionVersionStream.findFirst();
			definitionVersion.ifPresent(entity::setDefinitionVersion);
		}

		return convertArchiveEntityToDescriptor(getArchiveEntityService().saveArchiveEntity(entity));
	}


	@Override
	public List<ArchiveEntityDescriptor> markArchiveEntityDescriptorDeleted(ArchiveEntityDescriptor descriptor) {
		ValidationUtils.assertNotNull(descriptor.getEntityIdentifier(), "Entity identifier is required to mark archived versions deleted.", "entityIdentifier");
		ArchiveEntitySearchForm searchForm = new ArchiveEntitySearchForm();
		searchForm.setDefinitionName(descriptor.getDefinitionName());
		searchForm.setEntityIdentifier(descriptor.getEntityIdentifier());
		List<ArchiveEntity> archiveEntityList = getArchiveEntityService().getArchiveEntityList(searchForm);
		return CollectionUtils.getStream(archiveEntityList)
				.map(entity -> convertArchiveEntityToDescriptor(getArchiveEntityService().markArchiveEntityDeleted(entity.getId())))
				.collect(Collectors.toList());
	}


	private ArchiveEntityDescriptor convertArchiveEntityToDescriptor(ArchiveEntity entity) {
		ArchiveEntityDescriptor descriptor = new ArchiveEntityDescriptor(entity.getDefinitionVersion().getDefinition().getName(), entity.getEntityIdentifier(), entity.getParentEntityIdentifier(), entity.getEntityJson());
		descriptor.setDefinitionVersion(entity.getDefinitionVersion().getVersion());
		descriptor.setEntitySourceName(entity.getDefinitionVersion().getDefinition().getEntitySourceName());
		descriptor.setParentEntitySourceName(entity.getDefinitionVersion().getDefinition().getParentEntitySourceName());
		descriptor.setId(entity.getId());
		descriptor.setArchiveDate(entity.getArchiveDate());
		descriptor.setDeletedDate(entity.getDeletedDate());
		descriptor.setEntityJsonLength(entity.getEntityJsonLength());
		return descriptor;
	}


	private ArchiveDefinition createArchiveDefinition(ArchiveEntityDescriptor descriptor) {
		ValidationUtils.assertNotNull(descriptor.getEntitySourceName(), "Unable to save new archive definition without specifying the entity reference name.", "EntityReferenceName");

		ArchiveDefinition definition = new ArchiveDefinition();
		definition.setName(descriptor.getDefinitionName());
		definition.setDescription(descriptor.getDefinitionDescription());
		definition.setEntitySourceName(descriptor.getEntitySourceName());
		definition.setParentEntitySourceName(descriptor.getParentEntitySourceName());
		return getArchiveDefinitionService().saveArchiveDefinition(definition);
	}


	private ArchiveDefinitionVersion createArchiveDefinitionVersion(ArchiveDefinition definition, ArchiveEntityDescriptor descriptor) {
		ArchiveDefinitionVersion definitionVersion = new ArchiveDefinitionVersion();
		definitionVersion.setDefinition(definition);
		definitionVersion.setStartDate(new Date());
		definitionVersion.setVersion(1);
		definitionVersion.setEntityTemplate(descriptor.getEntityTemplate());
		return getArchiveDefinitionService().saveArchiveDefinitionVersion(definitionVersion);
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////         Getters and Setters                  ///////////////
	///////////////////////////////////////////////////////////////////////////


	public ArchiveDefinitionService getArchiveDefinitionService() {
		return this.archiveDefinitionService;
	}


	public void setArchiveDefinitionService(ArchiveDefinitionService archiveDefinitionService) {
		this.archiveDefinitionService = archiveDefinitionService;
	}


	public ArchiveEntityService getArchiveEntityService() {
		return this.archiveEntityService;
	}


	public void setArchiveEntityService(ArchiveEntityService archiveEntityService) {
		this.archiveEntityService = archiveEntityService;
	}
}
