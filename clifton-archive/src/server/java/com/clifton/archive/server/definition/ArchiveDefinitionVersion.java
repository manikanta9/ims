package com.clifton.archive.server.definition;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * <code>ArchiveDefinitionVersion</code> represents a version for a {@link ArchiveDefinition}.
 * The version contains a start and end date to symbolize its active lifespan and also contains
 * a JSON template representation that can be used to determine changes between versions.
 *
 * @author NickK
 */
public class ArchiveDefinitionVersion extends BaseSimpleEntity<Short> {

	private ArchiveDefinition definition;
	private Integer version;
	private Date startDate;
	private Date endDate;
	private String entityTemplate;

	///////////////////////////////////////////////////////////////////////////


	public ArchiveDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(ArchiveDefinition definition) {
		this.definition = definition;
	}


	public Integer getVersion() {
		return this.version;
	}


	public void setVersion(Integer version) {
		this.version = version;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), true);
	}


	public String getEntityTemplate() {
		return this.entityTemplate;
	}


	public void setEntityTemplate(String entityTemplate) {
		this.entityTemplate = entityTemplate;
	}


	public int getEntityTemplateLength() {
		return getEntityTemplate().length();
	}
}
