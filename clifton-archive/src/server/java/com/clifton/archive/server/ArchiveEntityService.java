package com.clifton.archive.server;

import com.clifton.archive.server.search.ArchiveEntitySearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * <code>ArchiveEntityService</code> defines service methods for saving and getting archived entities.
 *
 * @author NickK
 */
public interface ArchiveEntityService {

	public ArchiveEntity getArchiveEntity(long id);


	public List<ArchiveEntity> getArchiveEntityList(ArchiveEntitySearchForm entitySearchForm);


	@DoNotAddRequestMapping // Should use the client descriptor service for web access
	public ArchiveEntity saveArchiveEntity(ArchiveEntity entity);


	@DoNotAddRequestMapping // Should use the client descriptor service for web access
	public ArchiveEntity markArchiveEntityDeleted(long archiveEntityId);
}
