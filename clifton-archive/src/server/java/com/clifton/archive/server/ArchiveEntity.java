package com.clifton.archive.server;

import com.clifton.archive.server.definition.ArchiveDefinitionVersion;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;

import java.util.Date;


/**
 * <code>ArchiveEntity</code> represents an entity that is to be, or is, archived. The
 * archived entity will reference the external entity ID, it's optional parent ID, the
 * timestamp it was archived, and a JSON representation of the entity.
 * <p>
 * Archived entities should never be deleted, so upon deletion of the external entity,
 * the archived entity can be marked as deleted.
 *
 * @author NickK
 */
public class ArchiveEntity extends BaseSimpleEntity<Long> {

	private ArchiveDefinitionVersion definitionVersion;

	/**
	 * The ID of the entity being archived.
	 */
	private Long entityIdentifier;
	/**
	 * The ID of the parent entity of the entity being archived.
	 * e.g. Trade of a booked Journal for TradeFill
	 */
	private Long parentEntityIdentifier;

	/**
	 * Timestamp the entity was archived from an external data source. Each archived
	 * entity will get a new timestamp, so revisions will sortable by this field.
	 */
	private Date archiveDate;
	/**
	 * Date the archived entity is deleted from an external data source.
	 */
	private Date deletedDate;

	/**
	 * JSON representation of the archived entity
	 */
	private String entityJson;
	@NonPersistentField
	private int entityJsonLength;

	///////////////////////////////////////////////////////////////////////////


	public ArchiveDefinitionVersion getDefinitionVersion() {
		return this.definitionVersion;
	}


	public void setDefinitionVersion(ArchiveDefinitionVersion definitionVersion) {
		this.definitionVersion = definitionVersion;
	}


	public Long getEntityIdentifier() {
		return this.entityIdentifier;
	}


	public void setEntityIdentifier(Long entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}


	public Long getParentEntityIdentifier() {
		return this.parentEntityIdentifier;
	}


	public void setParentEntityIdentifier(Long parentEntityIdentifier) {
		this.parentEntityIdentifier = parentEntityIdentifier;
	}


	public Date getArchiveDate() {
		return this.archiveDate;
	}


	public void setArchiveDate(Date archiveDate) {
		this.archiveDate = archiveDate;
	}


	public Date getDeletedDate() {
		return this.deletedDate;
	}


	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}


	public String getEntityJson() {
		return this.entityJson;
	}


	public void setEntityJson(String entityJson) {
		this.entityJson = entityJson;
	}


	public int getEntityJsonLength() {
		return getEntityJson() == null ? this.entityJsonLength : getEntityJson().length();
	}


	public void setEntityJsonLength(int entityJsonLength) {
		this.entityJsonLength = entityJsonLength;
	}
}
