package com.clifton.archive.client.adapter;

import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.archive.descriptor.adapter.ArchiveEntityDescriptorAdapter;


/**
 * <code>BaseArchiveEntityDescriptorAdapter</code> is a base implementation that can be extended
 * to handle conversion between an entity to be archived and {@link ArchiveEntityDescriptor}.
 *
 * @author NickK
 */
public abstract class BaseArchiveEntityDescriptorAdapter<E> implements ArchiveEntityDescriptorAdapter<E> {

	/**
	 * Convert an archived object from it's {@link ArchiveEntityDescriptor} representation into it's original object.
	 */
	@Override
	public E fromArchiveEntityDescriptor(ArchiveEntityDescriptor archiveEntityDescriptor) {
		return fromJson(archiveEntityDescriptor.getEntityJson());
	}
}
