package com.clifton.accounting;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetriever;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetrieverImpl;
import com.clifton.accounting.gl.booking.rule.AccountingAggregateCurrencyBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingAggregateReceivableBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCloseAtCostBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionCollateralOffsetsBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveJournalDetailsBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>AccountingTestObjectFactory</code> class contains helper methods for creating various accounting objects used in tests.
 *
 * @author vgomelsky
 */
public class AccountingTestObjectFactory {

	public static AccountingPositionTransferType TO_TRANSFER_TYPE;
	public static AccountingPositionTransferType FROM_TRANSFER_TYPE;
	public static AccountingPositionTransferType BETWEEN_TRANSFER_TYPE;
	public static AccountingPositionTransferType INTERNAL_TRANSFER_TYPE;

	public static AccountingPositionTransferType CLIENT_COLLATERAL_TRANSFER_POST_TYPE;
	public static AccountingPositionTransferType CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE;
	public static AccountingPositionTransferType COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE;
	public static AccountingPositionTransferType COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE;

	public static AccountingPositionTransferType CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE;
	public static AccountingPositionTransferType CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE;
	public static AccountingPositionTransferType COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE;
	public static AccountingPositionTransferType COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static AccountingAccountService accountingAccountService;


	public static AccountingAccountService newAccountingAccountService() {
		AccountingAccountService result = accountingAccountService;

		if (result == null) {
			result = Mockito.mock(AccountingAccountService.class);

			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION).asPosition().withId(120).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().withId(753).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_RECEIVABLE).asPosition().asReceivable().withId(811).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_CASH)).thenReturn(newAccountingAccount_Cash());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_CASH_COLLATERAL)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_CASH_COLLATERAL).asCash().asCurrency().asCollateral().withId(751).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY)).thenReturn(newAccountingAccount_Currency());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE).asReceivable().withId(170).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_INTEREST_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_INTEREST_RECEIVABLE).asReceivable().withId(171).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_PAYMENT_RECEIVABLE).asReceivable().withId(172).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE).asPosition().asCollateral().asReceivable().asExecutingBrokerSpecific().withId(800).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL).asPosition().asCollateral().asNotOurAccount().withId(802).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.COUNTERPARTY_POSITION).asPosition().asCollateral().asReceivable().asNotOurAccount().withId(803).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.CASH_COLLATERAL_RECEIVABLE).asCash().asCollateral().asReceivable().asExecutingBrokerSpecific().withId(804).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH_COLLATERAL)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.COUNTERPARTY_CASH_COLLATERAL).asCash().asCollateral().asNotOurAccount().withId(806).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH)).thenReturn(AccountingAccountBuilder.newAsset(AccountingAccount.COUNTERPARTY_CASH).asCash().asCollateral().asNotOurAccount().withId(807).build());

			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE)).thenReturn(AccountingAccountBuilder.newLiability(AccountingAccount.POSITION_COLLATERAL_PAYABLE).asPosition().asCollateral().asReceivable().asExecutingBrokerSpecific().withId(801).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE)).thenReturn(AccountingAccountBuilder.newLiability(AccountingAccount.CASH_COLLATERAL_PAYABLE).asCash().asCollateral().asReceivable().asExecutingBrokerSpecific().withId(805).build());

			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EQUITY_SECURITY_CONTRIBUTION)).thenReturn(AccountingAccountBuilder.newEquity(AccountingAccount.EQUITY_SECURITY_CONTRIBUTION).withId(756).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EQUITY_SECURITY_DISTRIBUTION)).thenReturn(AccountingAccountBuilder.newEquity(AccountingAccount.EQUITY_SECURITY_DISTRIBUTION).withId(757).build());

			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_REALIZED).asGainLoss().withId(510).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_UNREALIZED)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_UNREALIZED).asGainLoss().asUnrealizedGainLoss().withId(511).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS).asCurrency().asGainLoss().asCurrencyTranslation().withId(512).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_INCOME)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_INTEREST_INCOME).withId(852).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_DIVIDEND_INCOME).withId(601).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REPO_REVENUE_INTEREST_INCOME)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REPO_REVENUE_INTEREST_INCOME).withId(856).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_PREMIUM_LEG)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_PREMIUM_LEG).asGainLoss().withId(799).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_LEG)).thenReturn(AccountingAccountBuilder.newRevenue(AccountingAccount.REVENUE_INTEREST_LEG).asGainLoss().withId(789).build());

			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EXPENSE_INTEREST_EXPENSE)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.EXPENSE_INTEREST_EXPENSE).withId(850).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EXPENSE_PRINCIPAL_LOSSES)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.EXPENSE_PRINCIPAL_LOSSES).asGainLoss().withId(765).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EXPENSE_COLLATERAL_USAGE_FEE)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.EXPENSE_COLLATERAL_USAGE_FEE).withId(901).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EXPENSE_COMMISSION)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.EXPENSE_COMMISSION).asCommission().withId(750).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.EXPENSE_SEC_FEES)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.EXPENSE_SEC_FEES).asCommission().withId(642).build());
			Mockito.lenient().when(result.getAccountingAccountByName(AccountingAccount.REPO_EXPENSE_INTEREST_EXPENSE)).thenReturn(AccountingAccountBuilder.newExpense(AccountingAccount.REPO_EXPENSE_INTEREST_EXPENSE).withId(854).build());

			// update cash accounts
			result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION).setCashAccount(result.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
			result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION).setReceivableAccount(result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_RECEIVABLE));
			result.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL).setCashAccount(result.getAccountingAccountByName(AccountingAccount.ASSET_CASH_COLLATERAL));
			result.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE).setCashAccount(result.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE));
			result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL).setCashAccount(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH_COLLATERAL));
			result.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE).setCashAccount(result.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE));
			result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION).setCashAccount(result.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH));

			accountingAccountService = result;
		}

		return result;
	}


	public static AccountingAccount newAccountingAccount_Cash() {
		return AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_CASH).asCash().asCurrency().withId(100).build();
	}


	public static AccountingAccount newAccountingAccount_Currency() {
		return AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_CURRENCY).asCurrency().withId(105).build();
	}


	public static AccountingAccount newAccountingAccount_Position() {
		return newAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);
	}


	public static AccountingAccount newAccountingAccount_CollateralPosition() {
		return newAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL);
	}


	public static AccountingAccount newAccountingAccount_CollateralPositionReceivable() {
		return newAccountingAccountService().getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE);
	}


	public static AccountingAccount newAccountingAccount_CollateralPositionPayable() {
		return newAccountingAccountService().getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingPositionTransferService newAccountingPositionTransferService() {
		AccountingPositionTransferService result = Mockito.mock(AccountingPositionTransferService.class);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.FROM_TRANSFER)).thenReturn(FROM_TRANSFER_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.TO_TRANSFER)).thenReturn(TO_TRANSFER_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.BETWEEN_TRANSFER)).thenReturn(BETWEEN_TRANSFER_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.INTERNAL_TRANSFER)).thenReturn(INTERNAL_TRANSFER_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COLLATERAL_TRANSFER_POST)).thenReturn(CLIENT_COLLATERAL_TRANSFER_POST_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COLLATERAL_TRANSFER_RETURN)).thenReturn(CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COUNTERPARTY_COLLATERAL_TRANSFER_POST)).thenReturn(COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COUNTERPARTY_COLLATERAL_TRANSFER_RETURN)).thenReturn(COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_POST)).thenReturn(CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_RETURN)).thenReturn(CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST)).thenReturn(COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE);
		Mockito.when(result.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN)).thenReturn(COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE);
		return result;
	}


	static {
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();

		FROM_TRANSFER_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.FROM_TRANSFER, false, true, new Short("1"));
		TO_TRANSFER_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.TO_TRANSFER, true, false, new Short("2"));
		BETWEEN_TRANSFER_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.BETWEEN_TRANSFER, true, true, new Short("3"));
		INTERNAL_TRANSFER_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.INTERNAL_TRANSFER, true, true, new Short("4"));
		INTERNAL_TRANSFER_TYPE.setInternalTransfer(true);


		CLIENT_COLLATERAL_TRANSFER_POST_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COLLATERAL_TRANSFER_POST, true, true, true, new Short("5"));
		CLIENT_COLLATERAL_TRANSFER_POST_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
		CLIENT_COLLATERAL_TRANSFER_POST_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));
		CLIENT_COLLATERAL_TRANSFER_POST_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL));
		CLIENT_COLLATERAL_TRANSFER_POST_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));


		CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COLLATERAL_TRANSFER_RETURN, true, true, false, new Short("6"));
		CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL));
		CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));
		CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
		CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));


		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COUNTERPARTY_COLLATERAL_TRANSFER_POST, true, true, true, new Short("7"));
		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE.setCounterpartyCollateral(true);
		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION));
		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));
		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL));
		COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));

		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COUNTERPARTY_COLLATERAL_TRANSFER_RETURN, true, true, false, new Short("8"));
		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE.setCounterpartyCollateral(true);
		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL));
		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));
		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION));
		COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));


		CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_POST, true, true, true, true, new Short("9"));
		CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE));
		CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH_COLLATERAL));
		CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE));

		CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.CASH_COLLATERAL_TRANSFER_RETURN, true, true, false, true, new Short("10"));
		CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH_COLLATERAL));
		CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE));
		CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE));

		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST, true, true, true, true, new Short("11"));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE.setCounterpartyCollateral(true);
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH_COLLATERAL));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE));

		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE = new AccountingPositionTransferType(AccountingPositionTransferType.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN, true, true, false, true, new Short("12"));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setCounterpartyCollateral(true);
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setToAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH_COLLATERAL));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setToOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_PAYABLE));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setFromAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_CASH));
		COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE.setFromOffsettingAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.CASH_COLLATERAL_RECEIVABLE));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static <T extends BookableEntity> AccountingBookingPositionRetriever newAccountingBookingPositionRetriever(AccountingCommonBookingRule<T> bookingRule) {
		AccountingBookingPositionRetrieverImpl accountingBookingPositionRetriever = new AccountingBookingPositionRetrieverImpl();
		accountingBookingPositionRetriever.setAccountingPositionService(bookingRule.getAccountingPositionService());
		accountingBookingPositionRetriever.setAccountingTransactionService(bookingRule.getAccountingTransactionService());
		bookingRule.setAccountingBookingPositionRetriever(accountingBookingPositionRetriever);
		return accountingBookingPositionRetriever;
	}


	public static <T extends BookableEntity> AccountingPositionSplitterBookingRule<T> newAccountingPositionSplitterBookingRule() {
		AccountingPositionSplitterBookingRule<T> bookingRule = new AccountingPositionSplitterBookingRule<>();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		newAccountingBookingPositionRetriever(bookingRule);

		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingPositionSplitterBookingRule<T> newAccountingPositionSplitterBookingRuleByTransaction(AccountingTransaction... existingTransactions) {
		AccountingPositionSplitterBookingRule<T> bookingRule = newAccountingPositionSplitterBookingRule();

		if (!ArrayUtils.isEmpty(existingTransactions)) {
			List<AccountingPosition> existingPositions = new ArrayList<>();
			for (AccountingTransaction tran : existingTransactions) {
				existingPositions.add(AccountingTestObjectFactory.newAccountingTransaction(tran));
				Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);
			}

			Mockito.when(bookingRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
					new AccountingPositionCommand().forInvestmentSecurity(existingTransactions[0].getInvestmentSecurity().getId())
			))).thenReturn(existingPositions);
		}

		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingPositionSplitterBookingRule<T> newAccountingPositionSplitterBookingRuleByPosition(AccountingPosition... existingPositions) {
		AccountingPositionSplitterBookingRule<T> bookingRule = newAccountingPositionSplitterBookingRule();

		if (!ArrayUtils.isEmpty(existingPositions)) {
			for (AccountingPosition pos : existingPositions) {
				Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(pos.getOpeningTransaction().getId()))).thenReturn((AccountingTransaction) pos.getOpeningTransaction());
			}

			Mockito.when(bookingRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
					new AccountingPositionCommand().forInvestmentSecurity(existingPositions[0].getInvestmentSecurity().getId())
			))).thenReturn(CollectionUtils.createList(existingPositions));
		}

		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingPositionCollateralOffsetsBookingRule<T> newAccountingPositionCollateralOffsetsBookingRule(AccountingPositionTransferDetail accountingPositionTransferDetail, AccountingTransaction... existingTransactions) {
		AccountingPositionCollateralOffsetsBookingRule<T> bookingRule = new AccountingPositionCollateralOffsetsBookingRule<>();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setAccountingPositionTransferService(Mockito.mock(AccountingPositionTransferService.class));
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		if (!ArrayUtils.isEmpty(existingTransactions)) {
			List<AccountingPosition> existingPositions = new ArrayList<>();
			for (AccountingTransaction tran : existingTransactions) {
				existingPositions.add(AccountingTestObjectFactory.newAccountingTransaction(tran));
				Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(tran.getId()))).thenReturn(tran);
			}
			Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransactionList(ArgumentMatchers.any(AccountingTransactionSearchForm.class)))
					.thenAnswer(arguments -> {
						AccountingTransactionSearchForm searchForm = arguments.getArgument(0);
						List<Short> accountingAccountIds = Arrays.stream(searchForm.getAccountingAccountIds()).collect(Collectors.toList());
						return Arrays.stream(existingTransactions)
								.filter(a -> a.getParentTransaction() != null)
								.filter(a -> a.getParentTransaction().getId().equals(searchForm.getParentId()))
								.filter(a -> accountingAccountIds.contains(a.getAccountingAccount().getId()))
								.collect(Collectors.toList());
					});
			Mockito.when(bookingRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))
			).thenAnswer(a -> {
				AccountingPositionCommand command = a.getArgument(0, AccountingPositionCommand.class);
				Long position = command.getExistingPositionId();
				return existingPositions.stream().filter(p -> MathUtils.isEqual(p.getId(), position)).collect(Collectors.toList());
			});
		}
		Mockito.when(bookingRule.getAccountingPositionTransferService().getAccountingPositionTransferDetail(ArgumentMatchers.any(Integer.class)))
				.thenAnswer(arguments -> {
					Integer detailId = arguments.getArgument(0);
					Assertions.assertEquals(accountingPositionTransferDetail.getId(), detailId);
					return accountingPositionTransferDetail;
				});

		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingRealizedGainLossBookingRule<T> newAccountingRealizedGainLossBookingRule(boolean sameJournalClosingOnly) {
		AccountingRealizedGainLossBookingRule<T> result = newAccountingRealizedGainLossBookingRule();
		result.setSameJournalClosingsOnly(sameJournalClosingOnly);
		return result;
	}


	public static <T extends BookableEntity> AccountingRealizedGainLossBookingRule<T> newAccountingRealizedGainLossBookingRule() {
		AccountingRealizedGainLossBookingRule<T> bookingRule = new AccountingRealizedGainLossBookingRule<>();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingRemoveZeroValueRecordBookingRule<T> newRemoveZeroValueRecordBookingRule() {
		return new AccountingRemoveZeroValueRecordBookingRule<>();
	}


	public static <T extends BookableEntity> AccountingRemoveJournalDetailsBookingRule<T> newRemoveJournalDetailsBookingRule(String... accountName) {
		AccountingRemoveJournalDetailsBookingRule<T> bookingRule = new AccountingRemoveJournalDetailsBookingRule<>();
		bookingRule.setGlAccountNameList(Arrays.asList(accountName));
		return bookingRule;
	}


	public static <T extends BookableEntity> AccountingCloseAtCostBookingRule<T> newCloseAtCostBookingRule() {
		return new AccountingCloseAtCostBookingRule<>();
	}


	public static <T extends BookableEntity> AccountingAggregateCurrencyBookingRule<T> newAccountingAggregateCurrencyBookingRule() {
		return new AccountingAggregateCurrencyBookingRule<>();
	}


	public static <T extends BookableEntity> AccountingAggregateReceivableBookingRule<T> newAccountingAggregateReceivableBookingRule() {
		return new AccountingAggregateReceivableBookingRule<>();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingJournal newAccountingJournal() {
		return newAccountingJournal(AccountingJournalType.TRADE_JOURNAL);
	}


	public static AccountingJournal newAccountingJournal(String journalName) {
		return newAccountingJournal(journalName, null);
	}


	public static AccountingJournal newAccountingJournal(String journalName, Short systemTableId) {
		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(new AccountingJournalType());
		journal.getJournalType().setName(journalName);
		journal.setId(5L);
		if (systemTableId != null) {
			SystemTable table = new SystemTable();
			table.setName("AccountingM2MDaily");
			table.setId(systemTableId);
			journal.setSystemTable(table);
		}
		return journal;
	}


	////////////////////////////////////////////////////////////////////////////


	public static AccountingPosition newAccountingPosition(long id, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		return newAccountingTransaction(id, security, AccountingTestObjectFactory.newAccountingAccount_Position(), quantity, price, costBasis, exchangeRateToBase, date);
	}


	public static AccountingPosition newAccountingPosition(AccountingTransaction tran) {
		return newAccountingTransaction(tran.getId(), tran.getInvestmentSecurity(), AccountingTestObjectFactory.newAccountingAccount_Position(), tran.getQuantity(), tran.getPrice(), tran.getPositionCostBasis(), tran.getExchangeRateToBase(),
				tran.getTransactionDate());
	}


	public static AccountingPosition newAccountingPosition(AccountingTransaction tran, BigDecimal remainingQuantity, BigDecimal remainingCostBasis, BigDecimal remainingBaseDebitCredit) {
		AccountingPosition position = newAccountingTransaction(tran.getId(), tran.getInvestmentSecurity(), AccountingTestObjectFactory.newAccountingAccount_Position(), tran.getQuantity(), tran.getPrice(), tran.getPositionCostBasis(), tran.getExchangeRateToBase(),
				tran.getTransactionDate());
		position.setRemainingQuantity(remainingQuantity);
		position.setRemainingCostBasis(remainingCostBasis);
		position.setRemainingBaseDebitCredit(remainingBaseDebitCredit);
		return position;
	}


	public static AccountingPosition newAccountingPosition(long id, InvestmentSecurity security, int quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		return newAccountingTransaction(id, security, AccountingTestObjectFactory.newAccountingAccount_Position(), new BigDecimal(quantity), price, costBasis, exchangeRateToBase, date);
	}


	public static AccountingPosition newAccountingPosition(long id, InvestmentSecurity security, int quantity, BigDecimal price, BigDecimal costBasis, Date date) {
		return newAccountingTransaction(id, security, AccountingTestObjectFactory.newAccountingAccount_Position(), new BigDecimal(quantity), price, costBasis, BigDecimal.ONE, date);
	}

	////////////////////////////////////////////////////////////////////////////


	public static AccountingPosition newAccountingTransaction(long id, InvestmentSecurity security, AccountingAccount accountingAccount, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		AccountingTransaction transaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(id, security).accountingAccount(accountingAccount).qty(quantity).price(price).costBasis(costBasis).exchangeRateToBase(exchangeRateToBase).on(date).build();

		AccountingPosition result = AccountingPosition.forOpeningTransaction(transaction);

		if (InvestmentUtils.isNoPaymentOnOpen(security)) {
			result.setRemainingBaseDebitCredit(BigDecimal.ZERO);
		}
		else {
			result.setRemainingBaseDebitCredit(MathUtils.multiply(costBasis, exchangeRateToBase, 2));
		}

		return result;
	}


	public static AccountingPosition newAccountingTransaction(AccountingTransaction tran) {
		return newAccountingTransaction(tran.getId(), tran.getInvestmentSecurity(), tran.getAccountingAccount(), tran.getQuantity(), tran.getPrice(), tran.getPositionCostBasis(), tran.getExchangeRateToBase(),
				tran.getTransactionDate());
	}


	public static AccountingPosition newAccountingTransaction(long id, InvestmentSecurity security, AccountingAccount accountingAccount, int quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		return newAccountingTransaction(id, security, accountingAccount, new BigDecimal(quantity), price, costBasis, exchangeRateToBase, date);
	}


	public static AccountingPosition newAccountingTransaction(long id, InvestmentSecurity security, AccountingAccount accountingAccount, int quantity, BigDecimal price, BigDecimal costBasis, Date date) {
		return newAccountingTransaction(id, security, accountingAccount, new BigDecimal(quantity), price, costBasis, BigDecimal.ONE, date);
	}


	public static AccountingPosition newAccountingPosition(long id, InvestmentSecurity security, int quantity, BigDecimal price, Date date) {
		return newAccountingPosition(id, security, quantity, price, InvestmentCalculatorUtils.getNotionalCalculator(security).calculateNotional(price, BigDecimal.ONE, new BigDecimal(quantity)), date);
	}

	////////////////////////////////////////////////////////////////////////////


	public static AccountingPositionCommand newAccountingPositionCommandMatcher(AccountingPositionCommand command) {
		return ArgumentMatchers.argThat(argument -> {
			if (argument == null) {
				return false;
			}
			if (command.getClientInvestmentAccountId() != null && !MathUtils.isEqual(command.getClientInvestmentAccountId(), argument.getClientInvestmentAccountId())) {
				return false;
			}
			if (!CompareUtils.isEqual(CollectionUtils.createList(command.getClientInvestmentAccountIds()), CollectionUtils.createList(argument.getClientInvestmentAccountIds()))) {
				return false;
			}
			if (command.getHoldingInvestmentAccountId() != null && !MathUtils.isEqual(command.getHoldingInvestmentAccountId(), argument.getHoldingInvestmentAccountId())) {
				return false;
			}
			if (!CompareUtils.isEqual(CollectionUtils.createList(command.getHoldingInvestmentAccountIds()), CollectionUtils.createList(argument.getHoldingInvestmentAccountIds()))) {
				return false;
			}
			if (command.getInvestmentSecurityId() != null && !MathUtils.isEqual(command.getInvestmentSecurityId(), argument.getInvestmentSecurityId())) {
				return false;
			}
			if (command.getExistingPositionId() != null && !MathUtils.isEqual(command.getExistingPositionId(), argument.getExistingPositionId())) {
				return false;
			}
			return true;
		});
	}


	public static AccountingTransactionSearchForm newAccountingTransactionSearchFormMatcher(AccountingTransactionSearchForm command) {
		return ArgumentMatchers.argThat(argument -> {
			if (argument == null) {
				return false;
			}
			if (command.getClientInvestmentAccountId() != null && !MathUtils.isEqual(command.getClientInvestmentAccountId(), argument.getClientInvestmentAccountId())) {
				return false;
			}
			if (!CompareUtils.isEqual(CollectionUtils.createList(command.getClientInvestmentAccountIds()), CollectionUtils.createList(argument.getClientInvestmentAccountIds()))) {
				return false;
			}
			if (command.getHoldingInvestmentAccountId() != null && !MathUtils.isEqual(command.getHoldingInvestmentAccountId(), argument.getHoldingInvestmentAccountId())) {
				return false;
			}
			if (!CompareUtils.isEqual(CollectionUtils.createList(command.getHoldingInvestmentAccountIds()), CollectionUtils.createList(argument.getHoldingInvestmentAccountIds()))) {
				return false;
			}
			if (command.getInvestmentSecurityId() != null && !MathUtils.isEqual(command.getInvestmentSecurityId(), argument.getInvestmentSecurityId())) {
				return false;
			}
			return true;
		});
	}


	////////////////////////////////////////////////////////////////////////////


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase, Date date, AccountingAccount accountingAccount) {
		AccountingJournalDetail result = new AccountingJournalDetail();
		result.setAccountingAccount(accountingAccount);
		result.setClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		result.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		result.setInvestmentSecurity(security);

		result.setQuantity(quantity);
		result.setPrice(price);
		result.setExchangeRateToBase(exchangeRateToBase);
		result.setLocalDebitCredit(localDebitCredit);
		result.setBaseDebitCredit(MathUtils.multiply(result.getLocalDebitCredit(), result.getExchangeRateToBase(), 2));
		result.setPositionCostBasis(costBasis);
		result.setPositionCommission(BigDecimal.ZERO);
		result.setTransactionDate(date);
		result.setSettlementDate(date);
		result.setOriginalTransactionDate(date);
		return result;
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase, Date date) {
		return newAccountingJournalDetail(security, quantity, price, costBasis, localDebitCredit, exchangeRateToBase, date, newAccountingAccount_Position());
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase, Date date, Date originalTransactionDate) {
		AccountingJournalDetail detail = newAccountingJournalDetail(security, quantity, price, costBasis, localDebitCredit, exchangeRateToBase, date, newAccountingAccount_Position());
		detail.setOriginalTransactionDate(originalTransactionDate);
		return detail;
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		BigDecimal localDebitCredit = InvestmentUtils.isNoPaymentOnOpen(security) ? BigDecimal.ZERO : costBasis;
		return newAccountingJournalDetail(security, quantity, price, costBasis, localDebitCredit, exchangeRateToBase, date);
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, int quantity, BigDecimal price, BigDecimal costBasis, BigDecimal exchangeRateToBase, Date date) {
		return newAccountingJournalDetail(security, new BigDecimal(quantity), price, costBasis, exchangeRateToBase, date);
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, int quantity, BigDecimal price, BigDecimal costBasis, Date date) {
		return newAccountingJournalDetail(security, new BigDecimal(quantity), price, costBasis, BigDecimal.ONE, date);
	}


	public static AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, Date date) {
		return newAccountingJournalDetail(security, quantity, price, costBasis, BigDecimal.ONE, date);
	}
}
