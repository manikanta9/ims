package com.clifton.accounting.gl.booking;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


/**
 * @author vgomelsky
 */
public class AccountingBookingInMemoryDBTestExecutor<B extends BookableEntity> extends AccountingBookingRuleTestExecutor<B> {

	private AccountingBookingService<B> accountingBookingService;
	private String journalTypeName;
	private Predicate<AccountingJournalDetailDefinition> transactionFilterPredicate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingBookingInMemoryDBTestExecutor(B bookableEntity) {
		super(bookableEntity);
	}


	public static <B extends BookableEntity> AccountingBookingInMemoryDBTestExecutor<B> newTestForEntity(String journalTypeName, B bookableEntity, AccountingBookingService<B> accountingBookingService) {
		AccountingBookingInMemoryDBTestExecutor<B> executor = new AccountingBookingInMemoryDBTestExecutor<>(bookableEntity);
		executor.accountingBookingService = accountingBookingService;
		executor.journalTypeName = journalTypeName;

		return executor;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingInMemoryDBTestExecutor<B> withTransactionFilter(Predicate<AccountingJournalDetailDefinition> transactionFilterPredicate) {
		this.transactionFilterPredicate = transactionFilterPredicate;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<AccountingJournalDetailDefinition> executeTest() {
		AccountingJournal bookedJournal = this.accountingBookingService.bookAccountingJournal(this.journalTypeName, this.bookableEntity, true);
		List<? extends AccountingJournalDetailDefinition> detailList = bookedJournal.getJournalDetailList();
		if (this.transactionFilterPredicate != null) {
			detailList = BeanUtils.filter(((List<AccountingJournalDetailDefinition>) detailList), this.transactionFilterPredicate);
		}
		return (List<AccountingJournalDetailDefinition>) detailList;
	}


	@Override
	protected String[] getStringValuesForResultEntity(AccountingJournalDetailDefinition resultEntity) {
		return Arrays.asList(resultEntity.toStringFormatted(false, this.withAdditionalFields)).toArray(new String[0]);
	}
}
