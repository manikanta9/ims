package com.clifton.accounting.gl.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.AssertUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author vgomelsky
 */
public class AccountingBookingRuleTestExecutor<B extends BookableEntity> extends BaseTestExecutor<AccountingJournalDetailDefinition> {

	protected final B bookableEntity;
	protected boolean withAdditionalFields;

	private AccountingJournal accountingJournal;
	private List<AccountingBookingRule<B>> ruleList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected AccountingBookingRuleTestExecutor(B bookableEntity) {
		this.bookableEntity = bookableEntity;
	}


	private AccountingBookingRuleTestExecutor(AccountingJournal journal, B bookableEntity) {
		this(bookableEntity);
		this.accountingJournal = journal;
	}


	public static <B extends BookableEntity> AccountingBookingRuleTestExecutor<B> newTestForEntity(B bookableEntity) {
		return newTestForJournalAndEntity(AccountingTestObjectFactory.newAccountingJournal(), bookableEntity);
	}


	public static <B extends BookableEntity> AccountingBookingRuleTestExecutor<B> newTestForJournal(AccountingJournal accountingJournal) {
		return newTestForJournalAndEntity(accountingJournal, null);
	}


	public static <B extends BookableEntity> AccountingBookingRuleTestExecutor<B> newTestForJournalAndEntity(AccountingJournal accountingJournal, B bookableEntity) {
		return new AccountingBookingRuleTestExecutor<>(accountingJournal, bookableEntity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Journal Detail";
	}


	@Override
	protected List<AccountingJournalDetailDefinition> executeTest() {
		AssertUtils.assertNotEmpty(this.ruleList, "At least one Booking Rule must be configured.");

		BookingSession<B> bookingSession = new SimpleBookingSession<>(this.accountingJournal, this.bookableEntity);
		for (AccountingBookingRule<B> bookingRule : this.ruleList) {
			bookingRule.applyRule(bookingSession);
		}

		return (List<AccountingJournalDetailDefinition>) this.accountingJournal.getJournalDetailList();
	}


	@Override
	protected String[] getStringValuesForResultEntity(AccountingJournalDetailDefinition resultEntity) {
		return Arrays.asList(resultEntity.toStringFormatted(false, this.withAdditionalFields)).toArray(new String[0]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Prepares the specified booking rule as the only rule to be executed (clears existing list first).
	 */
	public AccountingBookingRuleTestExecutor<B> forBookingRule(AccountingBookingRule<B> bookingRule) {
		this.ruleList = new ArrayList<>();
		this.ruleList.add(bookingRule);

		return this;
	}


	/**
	 * Prepares the specified booking rules as the only rules to be executed (clears existing list first).
	 */
	@SafeVarargs
	public final AccountingBookingRuleTestExecutor<B> forBookingRules(AccountingBookingRule<B>... bookingRules) {
		this.ruleList = new ArrayList<>();
		this.ruleList.addAll(Arrays.asList(bookingRules));

		return this;
	}


	/**
	 * Prepares the specified booking rules as the only rules to be executed (clears existing list first).
	 */
	public final AccountingBookingRuleTestExecutor<B> forBookingRules(List<AccountingBookingRule<B>> bookingRuleList) {
		this.ruleList = new ArrayList<>();
		this.ruleList.addAll(bookingRuleList);

		return this;
	}


	/**
	 * Allows additional data in JournalDetail to be compared in the test.  Currently this consists of the isOpening() boolean value.
	 */
	public AccountingBookingRuleTestExecutor<B> withAdditionalFields() {
		this.withAdditionalFields = true;
		return this;
	}
}
