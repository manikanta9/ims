package com.clifton.accounting.gl;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Builder designed to make it easier for creating {@link AccountingTransaction}s. Note that some values are updated in the {@link AccountingTransactionBuilder#build}
 * method. To avoid updating these values, pass <code>false</code> to {@link AccountingTransactionBuilder#build}.
 *
 * @author michaelm
 */
public class AccountingTransactionBuilder {

	private final AccountingTransaction accountingTransaction;


	private AccountingTransactionBuilder(AccountingTransaction accountingTransaction) {
		this.accountingTransaction = accountingTransaction;
	}


	public static AccountingTransactionBuilder newTransactionForIdAndSecurity(long transactionId, InvestmentSecurity investmentSecurity) {
		return new AccountingTransactionBuilder(new AccountingTransaction())
				.id(transactionId)
				.security(investmentSecurity)
				.journal(AccountingTestObjectFactory.newAccountingJournal())
				.clientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault())
				.holdingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault())
				.accountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position())
				.exchangeRateToBase(BigDecimal.ONE)
				.localDebitCredit(BigDecimal.ZERO)
				.baseDebitCredit(BigDecimal.ZERO)
				.opening(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransaction build() {
		return build(true);
	}


	/**
	 * Some {@link AccountingTransaction} values are updated in this method based on logic that applies to most test cases so that it is easier to use this builder.
	 * Pass <code>false</code> to avoid updating any values in this method.
	 */
	public AccountingTransaction build(boolean applyDefaults) {
		if (applyDefaults) {
			if (this.accountingTransaction.getQuantity() != null && this.accountingTransaction.getPrice() != null && this.accountingTransaction.getPositionCostBasis() == null) {
				costBasis(InvestmentCalculatorUtils.getNotionalCalculator(this.accountingTransaction.getInvestmentSecurity()).calculateNotional(this.accountingTransaction.getPrice(), this.accountingTransaction.getInvestmentSecurity().getPriceMultiplier(), this.accountingTransaction.getQuantity()));
			}

			if (this.accountingTransaction.getInvestmentSecurity().isCurrency()) {
				accountingAccount(AccountingTestObjectFactory.newAccountingAccount_Currency());
				qty((BigDecimal) null);
				price((BigDecimal) null);
			}

			BeanUtils.setPropertyValueIfNotSet(this.accountingTransaction, "originalTransactionDate", this.accountingTransaction.getTransactionDate(), true);
			BeanUtils.setPropertyValueIfNotSet(this.accountingTransaction, "settlementDate", this.accountingTransaction.getTransactionDate(), true);

			if (InvestmentUtils.isNoPaymentOnOpen(this.accountingTransaction.getInvestmentSecurity())) {
				localDebitCredit(BigDecimal.ZERO);
				baseDebitCredit(BigDecimal.ZERO);
			}
			else {
				BigDecimal costBasis = this.accountingTransaction.getPositionCostBasis();
				localDebitCredit(costBasis);
				baseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(costBasis, this.accountingTransaction.getExchangeRateToBase(), this.accountingTransaction.getClientInvestmentAccount()));
			}
		}
		return this.accountingTransaction;
	}


	public AccountingTransactionBuilder id(long id) {
		this.accountingTransaction.setId(id);
		return this;
	}


	public AccountingTransactionBuilder security(InvestmentSecurity investmentSecurity) {
		this.accountingTransaction.setInvestmentSecurity(investmentSecurity);
		return this;
	}


	public AccountingTransactionBuilder qty(int qty) {
		this.accountingTransaction.setQuantity(new BigDecimal(qty));
		return this;
	}


	public AccountingTransactionBuilder qty(String qtyString) {
		this.accountingTransaction.setQuantity(new BigDecimal(qtyString));
		return this;
	}


	public AccountingTransactionBuilder qty(BigDecimal qty) {
		this.accountingTransaction.setQuantity(qty);
		return this;
	}


	public AccountingTransactionBuilder price(String price) {
		this.accountingTransaction.setPrice(new BigDecimal(price));
		return this;
	}


	public AccountingTransactionBuilder price(BigDecimal price) {
		this.accountingTransaction.setPrice(price);
		return this;
	}


	public AccountingTransactionBuilder costBasis(String costBasisString) {
		this.accountingTransaction.setPositionCostBasis(new BigDecimal(costBasisString));
		return this;
	}


	public AccountingTransactionBuilder costBasis(BigDecimal costBasis) {
		this.accountingTransaction.setPositionCostBasis(costBasis);
		return this;
	}


	public AccountingTransactionBuilder localDebitCredit(BigDecimal localDebitCredit) {
		this.accountingTransaction.setLocalDebitCredit(localDebitCredit);
		return this;
	}


	public AccountingTransactionBuilder baseDebitCredit(BigDecimal baseDebitCredit) {
		this.accountingTransaction.setBaseDebitCredit(baseDebitCredit);
		return this;
	}


	public AccountingTransactionBuilder opening(boolean opening) {
		this.accountingTransaction.setOpening(opening);
		return this;
	}


	public AccountingTransactionBuilder on(String transactionDateString) {
		this.accountingTransaction.setTransactionDate(DateUtils.toDate(transactionDateString));
		return this;
	}


	public AccountingTransactionBuilder on(Date transactionDate) {
		this.accountingTransaction.setTransactionDate(transactionDate);
		return this;
	}


	public AccountingTransactionBuilder originalDate(String originalTransactionDateString) {
		this.accountingTransaction.setTransactionDate(DateUtils.toDate(originalTransactionDateString));
		return this;
	}


	public AccountingTransactionBuilder settlementDate(String settlementDateString) {
		this.accountingTransaction.setTransactionDate(DateUtils.toDate(settlementDateString));
		return this;
	}


	public AccountingTransactionBuilder journal(AccountingJournal journal) {
		this.accountingTransaction.setJournal(journal);
		return this;
	}


	public AccountingTransactionBuilder clientInvestmentAccount(InvestmentAccount clientAccount) {
		this.accountingTransaction.setClientInvestmentAccount(clientAccount);
		return this;
	}


	public AccountingTransactionBuilder holdingInvestmentAccount(InvestmentAccount holdingAccount) {
		this.accountingTransaction.setHoldingInvestmentAccount(holdingAccount);
		return this;
	}


	public AccountingTransactionBuilder accountingAccount(AccountingAccount accountingAccount) {
		this.accountingTransaction.setAccountingAccount(accountingAccount);
		return this;
	}


	public AccountingTransactionBuilder exchangeRateToBase(String exchangeRateToBaseString) {
		this.accountingTransaction.setExchangeRateToBase(new BigDecimal(exchangeRateToBaseString));
		return this;
	}


	public AccountingTransactionBuilder exchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.accountingTransaction.setExchangeRateToBase(exchangeRateToBase);
		return this;
	}
}
