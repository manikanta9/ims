package com.clifton.accounting.account;

public class AccountingAccountBuilder {

	private static final AccountingAccountType ASSET = new AccountingAccountType(AccountingAccountType.ASSET, true, true);
	private static final AccountingAccountType LIABILITY = new AccountingAccountType(AccountingAccountType.LIABILITY, true, false);
	private static final AccountingAccountType EQUITY = new AccountingAccountType(AccountingAccountType.EQUITY, true, false);
	private static final AccountingAccountType REVENUE = new AccountingAccountType(AccountingAccountType.REVENUE, false, false);
	private static final AccountingAccountType EXPENSE = new AccountingAccountType(AccountingAccountType.EXPENSE, false, true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingAccount accountingAccount;


	private AccountingAccountBuilder(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public static AccountingAccountBuilder newAsset(String accountName) {
		AccountingAccountBuilder result = new AccountingAccountBuilder(new AccountingAccount());
		result.withAccountType(ASSET);
		result.withName(accountName);
		return result;
	}


	public static AccountingAccountBuilder newLiability(String accountName) {
		AccountingAccountBuilder result = new AccountingAccountBuilder(new AccountingAccount());
		result.withAccountType(LIABILITY);
		result.withName(accountName);
		return result;
	}


	public static AccountingAccountBuilder newEquity(String accountName) {
		AccountingAccountBuilder result = new AccountingAccountBuilder(new AccountingAccount());
		result.withAccountType(EQUITY);
		result.withName(accountName);
		return result;
	}


	public static AccountingAccountBuilder newRevenue(String accountName) {
		AccountingAccountBuilder result = new AccountingAccountBuilder(new AccountingAccount());
		result.withAccountType(REVENUE);
		result.withName(accountName);
		return result;
	}


	public static AccountingAccountBuilder newExpense(String accountName) {
		AccountingAccountBuilder result = new AccountingAccountBuilder(new AccountingAccount());
		result.withAccountType(EXPENSE);
		result.withName(accountName);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccount build() {
		return this.accountingAccount;
	}


	public AccountingAccountBuilder asCash() {
		this.accountingAccount.setCash(true);
		return this;
	}


	public AccountingAccountBuilder asCurrency() {
		this.accountingAccount.setCurrency(true);
		return this;
	}


	public AccountingAccountBuilder asCurrencyTranslation() {
		this.accountingAccount.setCurrencyTranslation(true);
		return this;
	}


	public AccountingAccountBuilder asCollateral() {
		this.accountingAccount.setCollateral(true);
		return this;
	}


	public AccountingAccountBuilder asCommission() {
		this.accountingAccount.setCommission(true);
		return this;
	}


	public AccountingAccountBuilder asFee() {
		this.accountingAccount.setFee(true);
		return this;
	}


	public AccountingAccountBuilder asGainLoss() {
		this.accountingAccount.setGainLoss(true);
		return this;
	}


	public AccountingAccountBuilder asPosition() {
		this.accountingAccount.setPosition(true);
		return this;
	}


	public AccountingAccountBuilder asReceivable() {
		this.accountingAccount.setReceivable(true);
		return this;
	}


	public AccountingAccountBuilder asUnrealizedGainLoss() {
		this.accountingAccount.setUnrealizedGainLoss(true);
		return this;
	}


	public AccountingAccountBuilder asExecutingBrokerSpecific() {
		this.accountingAccount.setExecutingBrokerSpecific(true);
		return this;
	}


	public AccountingAccountBuilder asNotOurAccount() {
		this.accountingAccount.setNotOurAccount(true);
		return this;
	}


	public AccountingAccountBuilder withId(int id) {
		this.accountingAccount.setId((short) id);
		return this;
	}


	public AccountingAccountBuilder withName(String name) {
		this.accountingAccount.setName(name);
		return this;
	}


	public AccountingAccountBuilder withDescription(String description) {
		this.accountingAccount.setDescription(description);
		return this;
	}


	public AccountingAccountBuilder withAccountType(AccountingAccountType accountType) {
		this.accountingAccount.setAccountType(accountType);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingAccountBuilder createCommission() {
		return new AccountingAccountBuilder(new AccountingAccount())
				.withId(750)
				.withAccountType(EXPENSE)
				.withName("Commission")
				.withDescription("Commission Expense")
				.asCommission();
	}


	public static AccountingAccountBuilder createSecFees() {
		return new AccountingAccountBuilder(new AccountingAccount())
				.withId(642)
				.withAccountType(EXPENSE)
				.withName("SEC Fees")
				.withDescription("SEC Fees")
				.asCommission();
	}


	public static AccountingAccountBuilder createNfaFees() {
		return new AccountingAccountBuilder(new AccountingAccount())
				.withId(608)
				.withAccountType(EXPENSE)
				.withName("NFA Fees")
				.withDescription("NFA (National Futures Association) assessment fees")
				.asCommission();
	}


	public static AccountingAccountBuilder createTransactionFee() {
		return new AccountingAccountBuilder(new AccountingAccount())
				.withId(764)
				.withAccountType(EXPENSE)
				.withName("Transaction Fee (Other)")
				.withDescription("Miscellaneous fees: transaction servicing, account maintenance, options regulatory fees, etc.")
				.asCommission()
				.asFee();
	}


	public static AccountingAccountBuilder createPremiumLeg() {
		return new AccountingAccountBuilder(new AccountingAccount())
				.withId(799)
				.withAccountType(REVENUE)
				.withName("Premium Leg")
				.withDescription("Revenue (Expense if negative) on Premium Leg Payment of Credit Default Swap.  The sign depends on whether the client bought or sold protection.")
				.asGainLoss();
	}
}
