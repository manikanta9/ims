package com.clifton.accounting.positiontransfer;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingPositionTransferDetailBuilder {

	private AccountingPositionTransferDetail accountingPositionTransferDetail;


	private AccountingPositionTransferDetailBuilder(AccountingPositionTransferDetail accountingPositionTransferDetail) {
		this.accountingPositionTransferDetail = accountingPositionTransferDetail;
	}


	public static AccountingPositionTransferDetailBuilder createEmpty() {
		return new AccountingPositionTransferDetailBuilder(new AccountingPositionTransferDetail());
	}


	public AccountingPositionTransferDetailBuilder withId(Integer id) {
		getAccountingPositionTransferDetail().setId(id);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withSecurity(InvestmentSecurity security) {
		getAccountingPositionTransferDetail().setSecurity(security);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withQuantity(BigDecimal quantity) {
		getAccountingPositionTransferDetail().setQuantity(quantity);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withExchangeRateToBase(BigDecimal exchangeRateToBase) {
		getAccountingPositionTransferDetail().setExchangeRateToBase(exchangeRateToBase);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withPositionCostBasis(BigDecimal positionCostBasis) {
		getAccountingPositionTransferDetail().setPositionCostBasis(positionCostBasis);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withCommissionPerUnitOverride(BigDecimal commissionPerUnitOverride) {
		getAccountingPositionTransferDetail().setCommissionPerUnitOverride(commissionPerUnitOverride);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withCostPrice(BigDecimal costPrice) {
		getAccountingPositionTransferDetail().setCostPrice(costPrice);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withTransferPrice(BigDecimal transferPrice) {
		getAccountingPositionTransferDetail().setTransferPrice(transferPrice);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withPositionTransfer(AccountingPositionTransfer positionTransfer) {
		getAccountingPositionTransferDetail().setPositionTransfer(positionTransfer);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withOriginalPositionOpenDate(Date originalPositionOpenDate) {
		getAccountingPositionTransferDetail().setOriginalPositionOpenDate(originalPositionOpenDate);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withExistingPosition(AccountingTransaction existingPosition) {
		getAccountingPositionTransferDetail().setExistingPosition(existingPosition);
		return this;
	}


	public AccountingPositionTransferDetailBuilder withOriginalFace(BigDecimal originalFace) {
		getAccountingPositionTransferDetail().setOriginalFace(originalFace);
		return this;
	}


	private AccountingPositionTransferDetail getAccountingPositionTransferDetail() {
		return this.accountingPositionTransferDetail;
	}


	public AccountingPositionTransferDetail toAccountingPositionTransferDetail() {
		return this.accountingPositionTransferDetail;
	}
}
