package com.clifton.accounting.positiontransfer;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;


public class AccountingPositionTransferTypeBuilder {

	private AccountingPositionTransferType accountingPositionTransferType;


	private AccountingPositionTransferTypeBuilder(AccountingPositionTransferType accountingPositionTransferType) {
		this.accountingPositionTransferType = accountingPositionTransferType;
	}


	public static AccountingPositionTransferTypeBuilder createClientCashCollateralTransferPost() {
		AccountingPositionTransferTypeBuilder builder = new AccountingPositionTransferTypeBuilder(new AccountingPositionTransferType());
		builder
				.withId((short) 9)
				.withName("Client Cash Collateral Transfer - Post")
				.withDescription("We are posting collateral to an external counterparty.")
				.withToAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash Collateral")
						.withId(751)
						.withDescription("Cash used for collateral purposes only. Must never be mixed with non-collateral cash.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.build()
				)
				.withToOffsettingAccountingAccount(AccountingAccountBuilder
						.newLiability("Cash Collateral Payable")
						.withId(805)
						.withDescription("After the counterparty posts Counterparty Cash Collateral to our client's account, identifies the liability that the client is expected to return back to that counterparty. Executing Broker field is used to denote the counterparty.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.withFromAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash")
						.withId(100)
						.withDescription("Cash in base currency")
						.asCash()
						.asCurrency()
						.build()
				)
				.withFromOffsettingAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash Collateral Receivable")
						.withId(804)
						.withDescription("After our client posts Cash Collateral to a counterparty, identifies that asset that the client expects to receive back from that counterparty. Executing Broker field is used to denote the counterparty.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.asReceivingAsset()
				.asSendingAsset()
				.asCollateral()
				.asCollateralPosting()
				.asCash();
		return builder;
	}


	public static AccountingPositionTransferTypeBuilder createClientCashCollateralTransferReturn() {
		AccountingPositionTransferTypeBuilder builder = new AccountingPositionTransferTypeBuilder(new AccountingPositionTransferType());
		builder
				.withId((short) 10)
				.withName("Client Cash Collateral Transfer - Return")
				.withDescription("Counter party is returning our post collateral")
				.withToAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash")
						.withId(100)
						.withDescription("Cash in base currency")
						.asCash()
						.asCurrency()
						.asCollateral()
						.build()
				)
				.withToOffsettingAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash Collateral Receivable")
						.withId(804)
						.withDescription("After our client posts Cash Collateral to a counterparty, identifies that asset that the client expects to receive back from that counterparty.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.withFromAccountingAccount(AccountingAccountBuilder
						.newAsset("Cash Collateral")
						.withId(751)
						.withDescription("Cash used for collateral purposes only. Must never be mixed with non-collateral cash.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.build()
				)
				.withFromOffsettingAccountingAccount(AccountingAccountBuilder
						.newLiability("Cash Collateral Payable")
						.withId(805)
						.withDescription("After the counterparty posts Counterparty Cash Collateral to our client's account, identifies the liability that the client is expected to return back to that counterparty. Executing Broker field is used to denote the counterparty.")
						.asCash()
						.asCurrency()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.asReceivingAsset()
				.asSendingAsset()
				.asCollateral()
				.asCash();
		return builder;
	}


	public static AccountingPositionTransferTypeBuilder createClientCollateralTransferPost() {
		AccountingPositionTransferTypeBuilder builder = new AccountingPositionTransferTypeBuilder(new AccountingPositionTransferType());
		builder
				.withId((short) 5)
				.withName("Client Collateral Transfer - Post")
				.withDescription("We are posting collateral to an external counterparty.")
				.withToAccountingAccount(AccountingAccountBuilder
						.newAsset("Position Collateral")
						.withId(753)
						.withDescription("Security position used for collateral purposes only. Must never be mixed with non-collateral positions.")
						.asPosition()
						.asCollateral()
						.build()
				)
				.withToOffsettingAccountingAccount(AccountingAccountBuilder
						.newLiability("Position Collateral Payable")
						.withId(801)
						.withDescription("After counterparty posts position collateral to us, that position will be identified as a liability that we are expected to return to the counterparty.  Executing Broker field is used to denote the counterparty.")
						.asPosition()
						.asReceivable()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.withFromAccountingAccount(AccountingAccountBuilder
						.newAsset("Position")
						.withId(120)
						.withDescription("Positions in a security instrument")
						.asPosition()
						.build()
				)
				.withFromOffsettingAccountingAccount(AccountingAccountBuilder
						.newAsset("Position Collateral Receivable")
						.withId(800)
						.withDescription("After we post collateral to a counterparty, identifies the asset that we expect to receive back from that counterparty.  Executing Broker field is used to denote the counterparty.")
						.asPosition()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.asReceivingAsset()
				.asSendingAsset()
				.asCollateral()
				.asCollateralPosting();
		return builder;
	}


	public static AccountingPositionTransferTypeBuilder createClientCollateralTransferReturn() {
		AccountingPositionTransferTypeBuilder builder = new AccountingPositionTransferTypeBuilder(new AccountingPositionTransferType());
		builder
				.withId((short) 6)
				.withName("Client Collateral Transfer - Return")
				.withDescription("Counter party is returning our post collateral")
				.withToAccountingAccount(AccountingAccountBuilder
						.newAsset("Position")
						.withId(120)
						.withDescription("Positions in a security instrument")
						.asPosition()
						.build()
				)
				.withToOffsettingAccountingAccount(AccountingAccountBuilder
						.newAsset("Position Collateral Receivable")
						.withId(800)
						.withDescription("After we post collateral to a counterparty, identifies the asset that we expect to receive back from that counterparty.  Executing Broker field is used to denote the counterparty.")
						.asPosition()
						.asReceivable()
						.asCollateral()
						.asExecutingBrokerSpecific()
						.build()
				)
				.withFromAccountingAccount(AccountingAccountBuilder
						.newAsset("Position Collateral")
						.withId(753)
						.withDescription("Security position used for collateral purposes only. Must never be mixed with non-collateral positions.")
						.asPosition()
						.asCollateral()
						.build()
				)
				.withFromOffsettingAccountingAccount(AccountingAccountBuilder
						.newLiability("Position Collateral Payable")
						.withId(801)
						.withDescription("After counterparty posts position collateral to us, that position will be identified as a liability that we are expected to return to the counterparty.  Executing Broker field is used to denote the counterparty.")
						.asPosition()
						.asCollateral()
						.asReceivable()
						.asExecutingBrokerSpecific()
						.build()
				)
				.asReceivingAsset()
				.asSendingAsset()
				.asCollateral();
		return builder;
	}


	public AccountingPositionTransferTypeBuilder withId(short id) {
		getAccountingPositionTransferType().setId(id);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withName(String name) {
		getAccountingPositionTransferType().setName(name);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withDescription(String description) {
		getAccountingPositionTransferType().setDescription(description);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withToAccountingAccount(AccountingAccount toAccountingAccount) {
		getAccountingPositionTransferType().setToAccountingAccount(toAccountingAccount);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withToOffsettingAccountingAccount(AccountingAccount toOffsettingAccountingAccount) {
		getAccountingPositionTransferType().setToOffsettingAccountingAccount(toOffsettingAccountingAccount);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withFromAccountingAccount(AccountingAccount fromAccountingAccount) {
		getAccountingPositionTransferType().setFromAccountingAccount(fromAccountingAccount);
		return this;
	}


	public AccountingPositionTransferTypeBuilder withFromOffsettingAccountingAccount(AccountingAccount fromOffsettingAccountingAccount) {
		getAccountingPositionTransferType().setFromOffsettingAccountingAccount(fromOffsettingAccountingAccount);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asReceivingAsset() {
		getAccountingPositionTransferType().setReceivingAsset(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asSendingAsset() {
		getAccountingPositionTransferType().setSendingAsset(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asInternalTransfer() {
		getAccountingPositionTransferType().setInternalTransfer(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asCash() {
		getAccountingPositionTransferType().setCash(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asCollateralPosting() {
		getAccountingPositionTransferType().setCollateralPosting(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asCounterpartyCollateral() {
		getAccountingPositionTransferType().setCounterpartyCollateral(true);
		return this;
	}


	public AccountingPositionTransferTypeBuilder asCollateral() {
		getAccountingPositionTransferType().setCollateral(true);
		return this;
	}


	private AccountingPositionTransferType getAccountingPositionTransferType() {
		return this.accountingPositionTransferType;
	}


	public AccountingPositionTransferType toAccountingPositionTransferType() {
		return this.accountingPositionTransferType;
	}
}
