package com.clifton.accounting.positiontransfer;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;

import java.util.Date;
import java.util.List;


public class AccountingPositionTransferBuilder {

	private AccountingPositionTransfer accountingPositionTransfer;


	private AccountingPositionTransferBuilder(AccountingPositionTransfer accountingPositionTransfer) {
		this.accountingPositionTransfer = accountingPositionTransfer;
	}


	public static AccountingPositionTransferBuilder createEmpty() {
		return new AccountingPositionTransferBuilder(new AccountingPositionTransfer());
	}


	public AccountingPositionTransferBuilder withId(int id) {
		getAccountingPositionTransfer().setId(id);
		return this;
	}


	public AccountingPositionTransferBuilder withEntityModifyCondition(SystemCondition entityModifyCondition) {
		getAccountingPositionTransfer().setEntityModifyCondition(entityModifyCondition);
		return this;
	}


	public AccountingPositionTransferBuilder withViolationStatus(RuleViolationStatus violationStatus) {
		getAccountingPositionTransfer().setViolationStatus(violationStatus);
		return this;
	}


	public AccountingPositionTransferBuilder withToClientInvestmentAccount(InvestmentAccount toClientInvestmentAccount) {
		getAccountingPositionTransfer().setToClientInvestmentAccount(toClientInvestmentAccount);
		return this;
	}


	public AccountingPositionTransferBuilder withToHoldingInvestmentAccount(InvestmentAccount toHoldingInvestmentAccount) {
		getAccountingPositionTransfer().setToHoldingInvestmentAccount(toHoldingInvestmentAccount);
		return this;
	}


	public AccountingPositionTransferBuilder withFromClientInvestmentAccount(InvestmentAccount fromClientInvestmentAccount) {
		getAccountingPositionTransfer().setFromClientInvestmentAccount(fromClientInvestmentAccount);
		return this;
	}


	public AccountingPositionTransferBuilder withFromHoldingInvestmentAccount(InvestmentAccount fromHoldingInvestmentAccount) {
		getAccountingPositionTransfer().setFromHoldingInvestmentAccount(fromHoldingInvestmentAccount);
		return this;
	}


	public AccountingPositionTransferBuilder withDetailList(List<AccountingPositionTransferDetail> detailList) {
		getAccountingPositionTransfer().setDetailList(detailList);
		return this;
	}


	public AccountingPositionTransferBuilder withNote(String note) {
		getAccountingPositionTransfer().setNote(note);
		return this;
	}


	public AccountingPositionTransferBuilder withTransactionDate(Date transactionDate) {
		getAccountingPositionTransfer().setTransactionDate(transactionDate);
		return this;
	}


	public AccountingPositionTransferBuilder withSettlementDate(Date settlementDate) {
		getAccountingPositionTransfer().setSettlementDate(settlementDate);
		return this;
	}


	public AccountingPositionTransferBuilder withBookingDate(Date bookingDate) {
		getAccountingPositionTransfer().setBookingDate(bookingDate);
		return this;
	}


	public AccountingPositionTransferBuilder withType(AccountingPositionTransferType type) {
		getAccountingPositionTransfer().setType(type);
		return this;
	}


	public AccountingPositionTransferBuilder withExposureDate(Date exposureDate) {
		getAccountingPositionTransfer().setExposureDate(exposureDate);
		return this;
	}


	public AccountingPositionTransferBuilder asUseOriginalTransactionDate() {
		getAccountingPositionTransfer().setUseOriginalTransactionDate(true);
		return this;
	}


	public AccountingPositionTransferBuilder withSourceSystemTable(SystemTable sourceSystemTable) {
		getAccountingPositionTransfer().setSourceSystemTable(sourceSystemTable);
		return this;
	}


	public AccountingPositionTransferBuilder withSourceFkFieldId(Integer sourceFkFieldId) {
		getAccountingPositionTransfer().setSourceFkFieldId(sourceFkFieldId);
		return this;
	}


	public AccountingPositionTransferBuilder asCollateralTransfer() {
		getAccountingPositionTransfer().setCollateralTransfer(true);
		return this;
	}


	public AccountingPositionTransfer toAccountPositionTransfer() {
		return this.accountingPositionTransfer;
	}


	private AccountingPositionTransfer getAccountingPositionTransfer() {
		return this.accountingPositionTransfer;
	}
}
