package com.clifton.accounting.eventjournal;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.PopulatorCommand;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @author nickk
 */
public class AccountingEventJournalTestObjectFactory {

	private static final AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();


	private AccountingEventJournalTestObjectFactory() {
		// prevent instantiation
	}

	///////////////////////////////////////////////////////////////////////////
	//////////           AccountingEventJournalType                 ///////////
	///////////////////////////////////////////////////////////////////////////


	public static AccountingEventJournalType newAccountingEventJournalTypeForEvent(EventData eventData) {
		return newAccountingEventJournalTypeForEventWithAccountingEventTypeName(eventData, eventData.getEvent().getType().getName());
	}


	public static AccountingEventJournalType newAccountingEventJournalTypeForEventWithAccountingEventTypeName(EventData eventData, String accountingEventTypeName) {
		InvestmentSecurityEvent event = eventData.getEvent();
		ValidationUtils.assertNotNull(event, "No Security Event was provided.");

		AccountingEventJournalType eventJournalType = new AccountingEventJournalType();
		eventJournalType.setEventType(event.getType());
		eventJournalType.setName(accountingEventTypeName);

		switch (accountingEventTypeName) {
			case InvestmentSecurityEventType.STOCK_SPINOFF: {
				eventJournalType.setAffectedQuantityLabel("Parent Qty");
				eventJournalType.setAffectedCostLabel("Parent Remaining Cost Basis");
				eventJournalType.setTransactionPriceLabel("Parent Price");
				eventJournalType.setTransactionAmountLabel("Parent Cost Basis");
				eventJournalType.setAdditionalAmountLabel("Spinoff Qty");
				eventJournalType.setAdditionalAmount2Label("Spinoff Cost Basis");
				eventJournalType.setAdditionalAmount3Label("Fraction to Close");
				break;
			}
			case InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));

				eventJournalType.setAffectedQuantityLabel("Old Qty");
				eventJournalType.setAffectedCostLabel("Old Cost Basis");
				eventJournalType.setTransactionPriceLabel("Old Price");
				eventJournalType.setTransactionAmountLabel("New Price");
				eventJournalType.setAdditionalAmountLabel("New Qty");
				eventJournalType.setAdditionalAmount2Label("New Cost Basis");
				eventJournalType.setAdditionalAmount3Label("Fraction to Close");
				break;
			}
			case InvestmentSecurityEventType.STOCK_SPLIT: {
				eventJournalType.setAffectedQuantityLabel("Closing Qty");
				eventJournalType.setAffectedCostLabel("Closing Cost Basis");
				eventJournalType.setTransactionPriceLabel("Closing Price");
				eventJournalType.setTransactionAmountLabel("Opening Price");
				eventJournalType.setAdditionalAmountLabel("Opening Qty");
				eventJournalType.setAdditionalAmount2Label("Opening Cost Basis");
				eventJournalType.setAdditionalAmount3Label("Fraction to Close");
				break;
			}
			case InvestmentSecurityEventType.SYMBOL_CHANGE: {
				eventJournalType.setAffectedQuantityLabel("Quantity");
				eventJournalType.setAffectedCostLabel("Cost Basis");
				eventJournalType.setTransactionPriceLabel("Open Price");
				break;
			}
			case InvestmentSecurityEventType.DIVIDEND_SCRIP: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));

				eventJournalType.setAffectedQuantityLabel("Quantity");
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionPriceAdjustable(true);
				eventJournalType.setTransactionAmountLabel("Payment Amount");
				eventJournalType.setTransactionAmountAdjustable(true);
				eventJournalType.setAdditionalAmountLabel("Opening Qty");
				eventJournalType.setAdditionalAmountAdjustable(true);
				eventJournalType.setAdditionalAmount2Label("Opening Cost Basis");
				eventJournalType.setAdditionalAmount2Adjustable(true);
				eventJournalType.setAdditionalAmount3Label("Fraction to Close");
				eventJournalType.setAdditionalAmount3Adjustable(true);
				break;
			}
			case InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));

				eventJournalType.setAffectedQuantityLabel("Quantity");
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionPriceAdjustable(true);
				eventJournalType.setTransactionAmountLabel("Payment Amount");
				eventJournalType.setTransactionAmountAdjustable(true);
				break;
			}
			case InvestmentSecurityEventType.RETURN_OF_CAPITAL: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));

				eventJournalType.setAffectedQuantityLabel("Quantity");
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionPriceAdjustable(true);
				eventJournalType.setTransactionAmountLabel("Payment Amount");
				eventJournalType.setTransactionAmountAdjustable(true);
				eventJournalType.setAffectedCostLabel("Old Cost Basis");
				eventJournalType.setAdditionalAmount2Label("New Cost Basis");
				break;
			}
			case InvestmentSecurityEventType.MERGER: {
				eventJournalType.setAffectedQuantityLabel("Quantity");
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionAmountLabel("Payment Amount");
				eventJournalType.setAffectedCostLabel("Old Cost Basis");
				eventJournalType.setAdditionalAmountLabel("New Quantity");
				eventJournalType.setAdditionalAmount2Label("New Cost Basis");
				eventJournalType.setAdditionalAmount3Label("Fraction to Close");
				break;
			}
			case InvestmentSecurityEventType.FACTOR_CHANGE: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
				eventJournalType.setSettlementDateComparison(true);

				eventJournalType.setAffectedQuantityLabel("Current Fase");
				eventJournalType.setAffectedCostLabel("Cost Basis Reduction");
				eventJournalType.setAffectedCostAdjustable(true);
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionPriceAdjustable(true);
				eventJournalType.setTransactionAmountLabel("Payment Amount");
				eventJournalType.setTransactionAmountAdjustable(true);
				eventJournalType.setAdditionalAmountLabel("Loss Amount");
				eventJournalType.setAdditionalAmountAdjustable(true);
				break;
			}
			case AccountingEventJournalType.FACTOR_CHANGE_ASSUMED_SELL: {
				eventJournalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
				eventJournalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.INTEREST_INCOME));
				eventJournalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
				eventJournalType.setSettlementDateComparison(true);

				eventJournalType.setAffectedQuantityLabel("Principal Payment");
				eventJournalType.setAffectedQuantityAdjustable(true);
				eventJournalType.setAffectedCostLabel("Cost Basis Reduction");
				eventJournalType.setAffectedCostAdjustable(true);
				eventJournalType.setTransactionPriceLabel("Price");
				eventJournalType.setTransactionAmountLabel("Interest Income");
				eventJournalType.setTransactionAmountAdjustable(true);
				break;
			}
			default: {
				throw new ValidationException("Unsupported InvestmentSecurityEventType name: " + event.getType().getName());
			}
		}

		return eventJournalType;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////            AccountingEventJournal                    ///////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Creates and return an {@link AccountingEventJournal} for the provided security event.
	 *
	 * @see #newAccountingEventJournalTypeForEvent(EventData)
	 */
	public static AccountingEventJournal newAccountingEventJournalForEvent(EventData eventData) {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		eventJournal.setSecurityEvent(eventData.getEvent());
		eventJournal.setJournalType(newAccountingEventJournalTypeForEvent(eventData));
		return eventJournal;
	}


	/**
	 * Creates and return an {@link AccountingEventJournal} for the provided security event and creates and adds a detail for each provided transaction.
	 *
	 * @see #newAccountingEventJournalForEvent(EventData)
	 * @see #newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal, AccountingTransaction, InvestmentSecurityEventPayout, AccountingEventJournalDetailPopulator, boolean)
	 */
	public static AccountingEventJournal newAccountingEventJournalForEventWithTransactions(EventData eventData, AccountingTransaction... transactions) {
		return newAccountingEventJournalForEventWithTransactions(eventData, null, transactions);
	}


	/**
	 * Creates and return an {@link AccountingEventJournal} for the provided security event and creates and adds a detail for each provided transaction.
	 * Each detail will be populated with the provided populator and payout when not null.
	 *
	 * @see #newAccountingEventJournalForEvent(EventData)
	 * @see #newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal, AccountingTransaction, InvestmentSecurityEventPayout, AccountingEventJournalDetailPopulator, boolean)
	 */
	public static AccountingEventJournal newAccountingEventJournalForEventWithTransactions(EventData eventData, AccountingEventJournalDetailPopulator populator, AccountingTransaction... transactions) {
		AccountingEventJournal eventJournal = newAccountingEventJournalForEvent(eventData);
		if (transactions != null) {
			for (AccountingTransaction tran : transactions) {
				AccountingEventJournalTestObjectFactory.newAccountingEventJournalDetailForPositionAndPayout(eventJournal, tran, eventData.getPayout(), populator, true);
			}
		}

		return eventJournal;
	}


	/**
	 * Creates and return an {@link AccountingEventJournal} for the provided security event data. A detail is created for each transaction and payout.
	 * Each detail will be populated with the provided populator and payout when not null. When the rounder is defined, it will be used for each payout set that is not of currency type.
	 *
	 * @see #newAccountingEventJournalForEvent(EventData)
	 * @see #newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal, AccountingTransaction, EventData, AccountingEventJournalDetailPopulator, boolean)
	 * @see #aggregateAndRoundAccountingEventJournalDetailList(AccountingEventJournal, AccountingEventJournalDetailPopulator, AccountingEventJournalDetailRounder)
	 */
	public static AccountingEventJournal newAccountingEventJournalForEventWithTransactions(InvestmentSecurityEvent event, List<InvestmentSecurityEventPayout> payoutList, AccountingEventJournalDetailPopulator populator, AccountingEventJournalDetailRounder rounder, AccountingTransaction... transactions) {
		AccountingEventJournal eventJournal = newAccountingEventJournalForEvent(EventData.forEvent(event));
		if (transactions != null) {
			for (AccountingTransaction tran : transactions) {
				for (InvestmentSecurityEventPayout payout : payoutList) {
					EventData eventData = EventData.forEventPayoutOrEvent(payout, eventJournal.getSecurityEvent());
					eventData.setElectionPayoutList(payoutList);
					AccountingEventJournalTestObjectFactory.newAccountingEventJournalDetailForPositionAndPayout(eventJournal, tran, eventData, populator, true);
				}
			}
			AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);
		}
		return eventJournal;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////         AccountingEventJournalDetail                 ///////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Creates and returns an {@link AccountingEventJournalDetail} for the provided journal and transaction.
	 * If a a populator is defined, the detail will be populated using it.
	 * If addToJournal is true, the detail will be added to the journal.
	 *
	 * @see #newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal, AccountingTransaction, InvestmentSecurityEventPayout, AccountingEventJournalDetailPopulator, boolean)
	 */
	public static AccountingEventJournalDetail newAccountingEventJournalDetailForPosition(AccountingEventJournal journal, AccountingTransaction openPosition, AccountingEventJournalDetailPopulator populator, boolean addToJournal) {
		return newAccountingEventJournalDetailForPositionAndPayout(journal, openPosition, EventData.forEvent(journal.getSecurityEvent()), populator, addToJournal);
	}


	/**
	 * Creates and returns an {@link AccountingEventJournalDetail} for the provided journal and transaction.
	 * If a payout is provided, it will be set on the detail and it will be used by the populator.
	 * If a a populator is defined, the detail will be populated using it.
	 * If addToJournal is true, the detail will be added to the journal.
	 */
	public static AccountingEventJournalDetail newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal journal, AccountingTransaction openPosition, InvestmentSecurityEventPayout payout, AccountingEventJournalDetailPopulator populator, boolean addToJournal) {
		return newAccountingEventJournalDetailForPositionAndPayout(journal, openPosition, EventData.forEventPayoutOrEvent(payout, journal.getSecurityEvent()), populator, addToJournal);
	}


	/**
	 * Creates and returns an {@link AccountingEventJournalDetail} for the provided journal, transaction, and event data.
	 * If a a populator is defined, the detail will be populated using it.
	 * If addToJournal is true, the detail will be added to the journal.
	 */
	public static AccountingEventJournalDetail newAccountingEventJournalDetailForPositionAndPayout(AccountingEventJournal journal, AccountingTransaction openPosition, EventData eventData, AccountingEventJournalDetailPopulator populator, boolean addToJournal) {
		AccountingEventJournalType journalType = journal.getJournalType();
		InvestmentSecurityEvent event = journal.getSecurityEvent();
		InvestmentSecurityEventType eventType = event.getType();

		AccountingEventJournalDetail detail = new AccountingEventJournalDetail();
		detail.setId(CollectionUtils.getSize(journal.getDetailList()) + 1);
		detail.setJournal(journal);
		detail.setEventPayout(eventData.getPayout());
		detail.setAccountingTransaction(openPosition);
		if (journalType.isAffectedQuantitySupported()) {
			detail.setAffectedQuantity(openPosition.getQuantity());
		}
		if (journalType.isAffectedCostSupported()) {
			detail.setAffectedCost(openPosition.getPositionCostBasis());
		}
		if (journalType.isTransactionPriceSupported()) {
			detail.setTransactionPrice(openPosition.getPrice());
		}
		if (journalType.isTransactionAmountSupported()) {
			if (eventType.isNewPositionCreated()) {
				// set amount to cost basis when a new position is created (stock split needs to know what's being closed
				detail.setTransactionAmount(openPosition.getPositionCostBasis());
			}
			else {
				detail.setTransactionAmount(InvestmentCalculatorUtils.roundLocalAmount(eventData.getAfterEventValue().multiply(openPosition.getQuantity()), journal.getSecurityEvent().getSecurity()));
			}
		}
		if (eventData.isPayoutDefined()) {
			detail.setEventPayout(eventData.getPayout());
		}

		if (populator != null) {
			populator.populate(new PopulatorCommand(detail, eventData, AccountingTestObjectFactory.newAccountingPosition(detail.getAccountingTransaction()), null));
		}

		if (addToJournal) {
			journal.addDetail(detail);
		}
		return detail;
	}


	/**
	 * Aggregates the details of the provided journal into an aggregated detail. The provided populator is used to populated the aggregated detail. The proved rounder is used to round and adjust the journal details.
	 * Returns the aggregated detail.
	 * The resulting list is the aggregate details that result for the journal. Aggregates are processed by details applicable to a payout. Some payouts will not round, so an aggregate is not returned.
	 */
	public static List<AccountingEventJournalDetailPayoutAggregate> aggregateAndRoundAccountingEventJournalDetailList(AccountingEventJournal eventJournal, AccountingEventJournalDetailPopulator populator, AccountingEventJournalDetailRounder rounder) {
		Map<InvestmentSecurityEventPayout, List<AccountingEventJournalDetail>> payoutTypeToPayoutListMap = BeanUtils.getBeansMap(eventJournal.getDetailList(),
				eventJournalDetail -> Optional.ofNullable(eventJournalDetail.getEventPayout())
						.orElse(new InvestmentSecurityEventPayout()));
		List<AccountingEventJournalDetailPayoutAggregate> payoutAggregateList = new ArrayList<>();
		payoutTypeToPayoutListMap.forEach((payout, detailList) -> {
			if (rounder != null && (payout.getPayoutType() == null || !payout.getPayoutType().isAdditionalSecurityCurrency())) {
				AccountingEventJournalDetail aggregateDetail = AccountingEventJournalTestObjectFactory.newAggregateAccountingEventJournalDetail(eventJournal, detailList, populator);
				rounder.adjustForLotRounding(aggregateDetail, detailList);
				payoutAggregateList.add(new AccountingEventJournalDetailPayoutAggregate(payout, detailList, aggregateDetail));
			}
		});
		return payoutAggregateList;
	}


	/**
	 * Aggregates the details of the provided journal into an aggregated detail. The provided populator is used to populated the aggregated detail.
	 * Returns the aggregated detail.
	 */
	private static AccountingEventJournalDetail newAggregateAccountingEventJournalDetail(AccountingEventJournal eventJournal, List<AccountingEventJournalDetail> eventDetailList, AccountingEventJournalDetailPopulator populator) {
		// aggregate all lots for the holding account into a single "lot"
		AccountingTransaction aggregateTransaction = BeanUtils.cloneBean(eventDetailList.get(0).getAccountingTransaction(), false, true);
		aggregateTransaction.setQuantity(CoreMathUtils.sumProperty(eventDetailList, d -> d.getAccountingTransaction().getQuantity()));
		aggregateTransaction.setLocalDebitCredit(CoreMathUtils.sumProperty(eventDetailList, d -> d.getAccountingTransaction().getLocalDebitCredit()));
		aggregateTransaction.setBaseDebitCredit(CoreMathUtils.sumProperty(eventDetailList, d -> d.getAccountingTransaction().getBaseDebitCredit()));
		aggregateTransaction.setPositionCostBasis(CoreMathUtils.sumProperty(eventDetailList, d -> d.getAccountingTransaction().getPositionCostBasis()));
		aggregateTransaction.setPrice(InvestmentCalculatorUtils.getNotionalCalculator(aggregateTransaction.getInvestmentSecurity()).calculatePriceFromNotional(aggregateTransaction.getQuantity(), aggregateTransaction.getPositionCostBasis(), aggregateTransaction.getInvestmentSecurity().getPriceMultiplier()));
		Optional<InvestmentSecurityEventPayout> payout = CollectionUtils.getStream(eventDetailList)
				.filter(detail -> detail.getEventPayout() != null)
				.map(AccountingEventJournalDetail::getEventPayout)
				.findFirst();
		// generate single detail for the aggregated "lot"
		return newAccountingEventJournalDetailForPositionAndPayout(eventJournal, aggregateTransaction, payout.orElse(null), populator, false);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * IMS event journal generator processes events with multiple payouts by payout. Thus,
	 */
	public static final class AccountingEventJournalDetailPayoutAggregate {

		private final InvestmentSecurityEventPayout payout;
		private final List<AccountingEventJournalDetail> detailList;
		private final AccountingEventJournalDetail aggregateDetail;


		private AccountingEventJournalDetailPayoutAggregate(InvestmentSecurityEventPayout payout, List<AccountingEventJournalDetail> detailList, AccountingEventJournalDetail aggregateDetail) {
			this.payout = payout;
			this.detailList = detailList;
			this.aggregateDetail = aggregateDetail;
		}


		public InvestmentSecurityEventPayout getPayout() {
			return this.payout;
		}


		public List<AccountingEventJournalDetail> getDetailList() {
			return this.detailList;
		}


		public AccountingEventJournalDetail getAggregateDetail() {
			return this.aggregateDetail;
		}
	}
}
