package com.clifton.accounting.m2m.builder;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;


public class AccountingM2MDailyBuilder {

	private AccountingM2MDaily m2m;


	private AccountingM2MDailyBuilder(AccountingM2MDaily m2m) {
		this.m2m = m2m;
	}


	public static AccountingM2MDailyBuilder createAccountingM2MDaily() {
		return createAccountingM2MDaily(42);
	}


	public static AccountingM2MDailyBuilder createNewAccountingM2MDaily() {
		return createAccountingM2MDaily(null);
	}


	public static AccountingM2MDailyBuilder createAccountingM2MDaily(Integer id) {
		AccountingM2MDaily innerAccountingM2MDaily = new AccountingM2MDaily();
		if (id != null) {
			innerAccountingM2MDaily.setId(id);
		}
		innerAccountingM2MDaily.setHoldingInvestmentAccount(new InvestmentAccount(50, "HoldingAccount"));
		return new AccountingM2MDailyBuilder(innerAccountingM2MDaily);
	}


	public AccountingM2MDaily toAccountingM2MDaily() {
		return this.m2m;
	}


	public AccountingM2MDailyBuilder forClient(String clientName, Integer clientID, InvestmentSecurity baseCurrency) {
		return forClient(clientName, clientID, null, baseCurrency);
	}


	public AccountingM2MDailyBuilder forClient(String clientName, Integer clientID, String accountNumber, InvestmentSecurity baseCurrency) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setName(clientName);
		investAcc.setBaseCurrency(baseCurrency);
		investAcc.setId(clientID);
		investAcc.setNumber(accountNumber);
		investAcc.setType(new InvestmentAccountType());
		investAcc.getType().setName("");
		this.m2m.setClientInvestmentAccount(investAcc);
		this.m2m.setHoldingInvestmentAccount(investAcc);
		return this;
	}


	public AccountingM2MDailyBuilder withHoldingAccountIssuer(BusinessCompany issuingCompany) {
		if (this.m2m.getHoldingInvestmentAccount() != null) {
			this.m2m.getHoldingInvestmentAccount().setIssuingCompany(issuingCompany);
		}
		return this;
	}


	public AccountingM2MDailyBuilder withCustodian(Integer accountId, String accountNumber, BusinessCompany custodianCompany) {
		InvestmentAccount investAcc = new InvestmentAccount();
		investAcc.setId(accountId);
		investAcc.setName(custodianCompany.getName() + " " + accountNumber);
		investAcc.setNumber(accountNumber);

		investAcc.setIssuingCompany(custodianCompany);

		this.m2m.setCustodianInvestmentAccount(investAcc);
		return this;
	}


	public AccountingM2MDailyBuilder forCurrency(InvestmentSecurity markCurrency) {
		this.m2m.setMarkCurrency(markCurrency);
		return this;
	}


	public AccountingM2MDailyBuilder withMarkDate(Date markDate) {
		this.m2m.setMarkDate(markDate);
		return this;
	}


	public AccountingM2MDailyBuilder withMarkValues(double ourTransferAmount, double expectedTransferAmount, double transferAmount) {
		this.m2m.setOurTransferAmount(new BigDecimal(ourTransferAmount));
		this.m2m.setExpectedTransferAmount(new BigDecimal(expectedTransferAmount));
		this.m2m.setTransferAmount(new BigDecimal(transferAmount));
		return this;
	}


	public AccountingM2MDailyBuilder withExpense(AccountingAccount accountingAccount, BigDecimal expenseAmount, InvestmentSecurity expenseCurrency, BigDecimal expenseFxRate) {
		return withExpense(accountingAccount, expenseAmount, expenseCurrency, expenseFxRate, 1);
	}


	public AccountingM2MDailyBuilder withExpense(AccountingAccount accountingAccount, BigDecimal expenseAmount, InvestmentSecurity expenseCurrency, BigDecimal expenseFxRate, int daysToSettle) {
		AccountingM2MDailyExpenseType type = new AccountingM2MDailyExpenseType();
		type.setAccountingAccount(accountingAccount);
		type.setDaysToSettle(daysToSettle);

		AccountingM2MDailyExpense expense = new AccountingM2MDailyExpense();
		expense.setM2mDaily(this.m2m);
		expense.setType(type);
		expense.setExpenseCurrency(expenseCurrency);
		expense.setExpenseFxRate(expenseFxRate);
		expense.setExpenseAmount(expenseAmount);

		if (this.m2m.getExpenseList() == null) {
			this.m2m.setExpenseList(new ArrayList<>());
		}
		this.m2m.getExpenseList().add(expense);
		return this;
	}
}
