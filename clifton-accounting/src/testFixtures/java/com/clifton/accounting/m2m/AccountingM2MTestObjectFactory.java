package com.clifton.accounting.m2m;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.m2m.booking.AccountingM2MDailyExpenseBookingRule;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Date;


public class AccountingM2MTestObjectFactory {

	public static AccountingM2MDailyExpenseBookingRule newAccountingM2MDailyExpenseBookingRule() {
		AccountingM2MDailyExpenseBookingRule bookingRule = new AccountingM2MDailyExpenseBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setCalendarBusinessDayService(newCalendarBusinessDayService());
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService());
		return bookingRule;
	}


	public static InvestmentAccountRelationshipService newInvestmentAccountRelationshipService() {
		InvestmentAccountRelationshipService result = Mockito.mock(InvestmentAccountRelationshipService.class);
		InvestmentAccountRelationshipConfig config = new InvestmentAccountRelationshipConfig(400);

		Mockito.when(result.getInvestmentAccountRelationshipConfig(ArgumentMatchers.anyInt())).thenReturn(config);
		return result;
	}


	public static AccountingM2MService newAccountingM2MService() {
		AccountingM2MService result = Mockito.mock(AccountingM2MService.class);

		InvestmentAccount m2mAccount = new InvestmentAccount();
		m2mAccount.setName("M2M Account");
		m2mAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		m2mAccount.setId(500);
		m2mAccount.setNumber("123456");
		m2mAccount.setType(new InvestmentAccountType());
		m2mAccount.getType().setName("");

		Mockito.when(result.getAccountingM2MInvestmentAccount(ArgumentMatchers.any(InvestmentAccount.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(m2mAccount);
		Mockito.when(result.getAccountingMarkToMarketRelationshipPurposeName()).thenReturn("Mark to Market");
		return result;
	}


	public static CalendarBusinessDayService newCalendarBusinessDayService() {
		return Mockito.mock(CalendarBusinessDayService.class);
	}
}
