package com.clifton.accounting.commission.validation;

import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.AccountingCommissionTypes;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Unit tests for Commission related validation.
 *
 * @author michaelm
 */
@ContextConfiguration(locations = "../AccountingCommissionTests-context.xml")
@ExtendWith(SpringExtension.class)
public class AccountingCommissionValidationTests {

	private static final int COMMISSION_PER_UNIT_HALF_TURN_CLEARING = 2;
	private static final int COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING = 42;
	private static final int COMMISSION_TIERED_NO_TIERS_CLEARING = 45;


	@Resource
	private AccountingCommissionService accountingCommissionService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private AccountingCommissionValidator accountingCommissionValidator;


	////////////////////////////////////////////////////////////////////////////
	//////////////      AccountingCommissionValidator Tests       //////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCommissionDefinition_MissingTypeSecurityOrInstrumentSelection() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TIERED_NO_TIERS_CLEARING);
			commissionDefinition.setInvestmentType(null);
			commissionDefinition.setInvestmentSecurity(null);
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Must select Investment Type, Investment Subtype, Instrument Hierarchy, Investment Instrument, or Investment Security", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_MissingCommissionMinimum() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setCommissionMinimum(new BigDecimal(1));
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Minimum Calculation Method must be populated when using a Minimum Commission Amount", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_MissingCommissionMaximum() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setCommissionMaximum(new BigDecimal(1));
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Maximum Calculation Method must be populated when using a Maximum Commission Amount", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_CalculatedMaxLessThanCalculatedMin() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setMaximumCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
			commissionDefinition.setMinimumCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
			commissionDefinition.setCommissionMaximum(new BigDecimal(1));
			commissionDefinition.setCommissionMinimum(new BigDecimal(2));
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Calculated Maximum Amount cannot be less than the calculated Minimum Amount", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_EndDateAfterStartDate() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setStartDate(new Date());
			commissionDefinition.setEndDate(DateUtils.getPreviousWeekday(commissionDefinition.getStartDate()));
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Start Date must be before End Date.", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_CommissionAmountNotAllowedForTieredCommission() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_PER_UNIT_HALF_TURN_CLEARING);
			commissionDefinition.setCommissionType(AccountingCommissionTypes.TIERED_AMORTIZATION);
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Commission Amount is not allowed for Commission/Fee Type [TIERED_AMORTIZATION].", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_OneTierNotAllowed() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setCommissionTierList(createAccountingCommissionTiers(1));
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Amortized/Tiered commission definitions must have at least two rates defined.", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_MatchingThresholdsNotAllowed() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			List<AccountingCommissionTier> tiers = createAccountingCommissionTiers(2);
			tiers.get(1).setThreshold(tiers.get(0).getThreshold());
			commissionDefinition.setCommissionTierList(tiers);
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Cannot have multiple commission rates with the same threshold value.", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_MultipleTiersNoThresholdValue() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			List<AccountingCommissionTier> tiers = createAccountingCommissionTiers(2);
			tiers.forEach(tier -> tier.setThreshold(null));
			commissionDefinition.setCommissionTierList(tiers);
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Cannot have more than one commission rate that has no threshold value specified.", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_RatesNotAllowedForSimpleCommission() {
		FieldValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
			commissionDefinition.setCommissionType(AccountingCommissionTypes.SIMPLE);
			this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
		});
		Assertions.assertEquals("Non-Amortized/Tiered commission definitions must NOT have rates defined.", validationException.getMessage());
	}


	@Test
	public void testCommissionDefinition_AmountAllowedForSimpleCommission() {
		AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_PER_UNIT_HALF_TURN_CLEARING);
		this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
	}


	@Test
	public void testCommissionDefinition_RatesNotAllowedForTieredCommission() {
		AccountingCommissionDefinition commissionDefinition = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);
		this.accountingCommissionValidator.validate(commissionDefinition, DaoEventTypes.MODIFY);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingCommissionDefinition getCommission(int id) {
		AccountingCommissionDefinition definition = this.accountingCommissionService.getAccountingCommissionDefinition(id);
		definition.setInvestmentSecurity(getSecurity(1));
		return definition;
	}


	private InvestmentSecurity getSecurity(int id) {
		return this.investmentInstrumentService.getInvestmentSecurity(id);
	}


	private List<AccountingCommissionTier> createAccountingCommissionTiers(int numTiers) {
		List<AccountingCommissionTier> tiers = new ArrayList<>();
		for (int i = 0; i < numTiers; i++) {
			AccountingCommissionTier tier = new AccountingCommissionTier();
			tier.setFeeAmount(new BigDecimal(i + 1));
			tier.setThreshold(new BigDecimal(i + ".99"));
			tiers.add(tier);
		}
		return tiers;
	}
}
