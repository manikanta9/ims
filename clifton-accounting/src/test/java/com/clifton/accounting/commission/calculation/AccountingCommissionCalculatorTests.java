package com.clifton.accounting.commission.calculation;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;


@ContextConfiguration(locations = "../AccountingCommissionTests-context.xml")
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class AccountingCommissionCalculatorTests {

	private static final int COMMISSION_PER_UNIT_HALF_TURN_EXECUTING = 1;
	private static final int COMMISSION_PER_UNIT_HALF_TURN_CLEARING = 2;

	private static final BigDecimal COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT = new BigDecimal("100.00");
	private static final int COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING = 3;
	private static final int COMMISSION_FULL_AMOUNT_HALF_TURN_CLEARING = 4;

	private static final int COMMISSION_PER_NOTIONAL_UNIT_HALF_TURN_EXECUTING = 5;
	private static final int COMMISSION_PER_NOTIONAL_UNIT_HALF_TURN_CLEARING = 6;

	private static final int COMMISSION_FULL_AMOUNT_TIERED_EXECUTING = 10;
	private static final int COMMISSION_FULL_AMOUNT_AMORTIZED_EXECUTING = 15;

	private static final int COMMISSION_EARLY_TERMINATION_AMORTIZED_STRAIGHT_LINE_EXECUTING = 40;

	private static final int COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING = 42;

	private static final int COMMISSION_TIERED_NO_TIERS_CLEARING = 45;

	private static final BigDecimal COST_BASIS = BigDecimal.ONE;
	private static final BigDecimal EXCHANGE_RATE_TO_BASE = BigDecimal.ONE;
	private static final Date OCTOBER_2_2013 = DateUtils.toDate("10/2/2013");
	private static final Date OCTOBER_1_2013 = DateUtils.toDate("10/1/2013");
	private static final Date SEPT_1_2013 = DateUtils.toDate("9/1/2013");
	private static final Date AUGUST_15_2013 = DateUtils.toDate("8/15/2013");
	private static final Date JULY_15_2013 = DateUtils.toDate("7/15/2013");
	private static final Date JUNE_15_2013 = DateUtils.toDate("6/15/2013");
	@Resource
	private AccountingCommissionService accountingCommissionService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private BusinessCompanyService businessCompanyService;


	@Test
	public void testPerUnitCommission() {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.30);
		BigDecimal notional = createBigDecimal(1.70);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), true);
		AccountingCommissionDefinition commissionDefinitionExecuting = getCommission(COMMISSION_PER_UNIT_HALF_TURN_EXECUTING);

		AccountingCommissionCalculationCommand executingCommand = buildCommissionCalculationInfo(journalDetail, commissionDefinitionExecuting, notional);
		BigDecimal expectedExecutingResult = commissionDefinitionExecuting.getCommissionAmount().multiply(new BigDecimal(quantity)).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCommand);

		Assertions.assertEquals(expectedExecutingResult, executingResult);

		AccountingCommissionDefinition commissionDefinitionClearing = getCommission(COMMISSION_PER_UNIT_HALF_TURN_CLEARING);
		AccountingCommissionCalculationCommand clearingCommand = buildCommissionCalculationInfo(journalDetail, commissionDefinitionClearing, notional);
		BigDecimal expectedClearingResult = commissionDefinitionClearing.getCommissionAmount().multiply(new BigDecimal(quantity)).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal clearingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(clearingCommand);

		Assertions.assertEquals(expectedClearingResult, clearingResult);
	}


	@Test
	public void testFullAmountCommission() {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(1.7);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), true);

		AccountingCommissionDefinition executingDefinition = getCommission(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING);
		AccountingCommissionCalculationCommand executionCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		BigDecimal executionExpectedResult = executingDefinition.getCommissionAmount();
		BigDecimal executionResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executionCommand);
		Assertions.assertEquals(executionExpectedResult, executionResult);

		AccountingCommissionDefinition clearingDefinition = getCommission(COMMISSION_FULL_AMOUNT_HALF_TURN_CLEARING);
		AccountingCommissionCalculationCommand clearingCommand = buildCommissionCalculationInfo(journalDetail, clearingDefinition, notional);
		BigDecimal clearingExpectedResult = clearingDefinition.getCommissionAmount();
		BigDecimal clearingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(clearingCommand);
		Assertions.assertEquals(clearingExpectedResult, clearingResult);
	}


	@Test
	public void testPerNotionalAmountCommission() {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(1.7);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), true);

		AccountingCommissionDefinition executingDefinition = getCommission(COMMISSION_PER_NOTIONAL_UNIT_HALF_TURN_EXECUTING);
		AccountingCommissionCalculationCommand executingCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		BigDecimal executingExpectedResult = executingDefinition.getCommissionAmount().multiply(notional).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCommand);

		Assertions.assertEquals(executingExpectedResult, executingResult);

		AccountingCommissionDefinition clearingDefinition = getCommission(COMMISSION_PER_NOTIONAL_UNIT_HALF_TURN_CLEARING);
		AccountingCommissionCalculationCommand clearingCommand = buildCommissionCalculationInfo(journalDetail, clearingDefinition, notional);
		BigDecimal clearingExpectedResult = clearingDefinition.getCommissionAmount().multiply(notional).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal clearingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(clearingCommand);

		Assertions.assertEquals(clearingExpectedResult, clearingResult);
	}


	@Test
	public void testNotionalTieredCommission() {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal executingNotional = createBigDecimal(75);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);

		AccountingCommissionDefinition executingDefinition = getCommission(COMMISSION_FULL_AMOUNT_TIERED_EXECUTING);
		AccountingCommissionCalculationCommand executingCalculator = buildCommissionCalculationInfo(journalDetail, executingDefinition, executingNotional);

		BigDecimal executingExpectedResult = createBigDecimal(10);
		BigDecimal executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCalculator);
		Assertions.assertEquals(executingExpectedResult, executingResult);

		executingNotional = createBigDecimal(150);
		executingCalculator.setNotionalAmount(executingNotional);
		executingExpectedResult = createBigDecimal(20);
		executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCalculator);
		Assertions.assertEquals(executingExpectedResult, executingResult);

		executingNotional = createBigDecimal(300);
		executingCalculator.setNotionalAmount(executingNotional);
		executingExpectedResult = createBigDecimal(30);
		executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCalculator);
		Assertions.assertEquals(executingExpectedResult, executingResult);
	}


	@Test
	public void testManualAmortizedCommission() {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(5);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);
		journalDetail.setOriginalTransactionDate(AUGUST_15_2013);

		AccountingCommissionDefinition executingDefinition = getCommission(COMMISSION_FULL_AMOUNT_AMORTIZED_EXECUTING);
		AccountingCommissionCalculationCommand executingCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		BigDecimal executingExpectedResult = createBigDecimal(25);
		BigDecimal executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCommand);
		Assertions.assertEquals(executingExpectedResult, executingResult);


		journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);
		journalDetail.setOriginalTransactionDate(JULY_15_2013);
		executingCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		executingExpectedResult = createBigDecimal(15);
		executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCommand);
		Assertions.assertEquals(executingExpectedResult, executingResult);

		journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);
		journalDetail.setOriginalTransactionDate(JUNE_15_2013);
		executingCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		executingExpectedResult = createBigDecimal(5);
		executingResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executingCommand);
		Assertions.assertEquals(executingExpectedResult, executingResult);
	}


	@Test
	public void testStraightLineAmortizedCommission() {
		AccountingCommissionDefinition commission = getCommission(COMMISSION_EARLY_TERMINATION_AMORTIZED_STRAIGHT_LINE_EXECUTING);

		int quantity = 100;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(5000);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(12), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);
		journalDetail.setOriginalTransactionDate(AUGUST_15_2013);

		AccountingCommissionCalculationCommand calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, notional);

		BigDecimal result = (new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);

		//days_left = difference(sept 1 2013, jan 1 2014) = 122 days
		//days_total = difference(aug 15 2013, jan 1 2014) = 139 days
		//amortization_multiplier = days_left / days_total = 0.877...
		//notional = 5000
		//rate (BPS) = 7
		//commission = notional * rate * amortization_multiplier / 10,000 = 3.07
		BigDecimal expectedResult = createBigDecimal(3.07);

		Assertions.assertEquals(expectedResult, result);
	}


	@Test
	public void testOverriddenStraightLineAmortizedCommission() {
		AccountingCommissionDefinition commission = getCommission(COMMISSION_EARLY_TERMINATION_AMORTIZED_STRAIGHT_LINE_EXECUTING);

		int quantity = 100;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(5000);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(12), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), false);
		journalDetail.setOriginalTransactionDate(AUGUST_15_2013);

		AccountingCommissionCalculationCommand calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, notional);

		BigDecimal result = (new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);

		//days_left = difference(sept 1 2013, jan 1 2014) = 122 days
		//days_total = difference(aug 15 2013, jan 1 2014) = 139 days
		//amortization_multiplier = days_left / days_total = 0.877...
		//notional = 5000
		//rate (BPS) = 7
		//commission = notional * rate * amortization_multiplier / 10,000 = 3.07
		BigDecimal expectedResult = createBigDecimal(3.07);

		Assertions.assertEquals(expectedResult, result);
	}


	@Test
	public void testClearedIRSTransactionMaturityTieredCommission() {
		TestUtils.expectException(ValidationException.class, () -> {
			BigDecimal quantity = BigDecimal.valueOf(7500000);
			BigDecimal price = createBigDecimal(1.3);

			AccountingCommissionDefinition commission = getCommission(COMMISSION_TRANSACTION_MATURITY_TIERED_CLEARING);

			//Security 12 has end date (maturity date of 1/1/2014
			AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(12), quantity.intValue(), price, OCTOBER_2_2013, getBroker(2), getHoldingAccount(4), false);
			AccountingCommissionCalculationCommand calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, quantity);
			// 75 Notional * .25 (maturity tier fee)
			BigDecimal expectedResult = createBigDecimal(1.875);
			BigDecimal result = (new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);
			Assertions.assertEquals(expectedResult, result);


			journalDetail = createJournalDetail(getSecurity(12), quantity.intValue(), price, OCTOBER_1_2013, getBroker(2), getHoldingAccount(4), false);
			calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, quantity);
			// 75 Notional * 1 (maturity tier fee)
			expectedResult = createBigDecimal(7.5);
			result = (new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);
			Assertions.assertEquals(expectedResult, result);

			journalDetail = createJournalDetail(getSecurity(12), quantity.intValue(), price, JULY_15_2013, getBroker(2), getHoldingAccount(4), false);
			calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, quantity);
			// 75 Notional * 5 (maturity tier fee)
			expectedResult = createBigDecimal(37.5);
			result = (new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);
			Assertions.assertEquals(expectedResult, result);

			journalDetail = createJournalDetail(getSecurity(12), quantity.intValue(), price, JUNE_15_2013, getBroker(2), getHoldingAccount(4), false);
			calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, quantity);
			// Should not find a tier

			(new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);
		}, "Could not find an applicable TRANSACTION_MATURITY commission tier for the journal detail.");
	}


	@Test
	public void testTieredCommissionDefinitionWithNoTiersIsInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			int quantity = 5;
			BigDecimal price = createBigDecimal(1.3);
			BigDecimal notional = createBigDecimal(7500000);

			AccountingCommissionDefinition commission = getCommission(COMMISSION_TIERED_NO_TIERS_CLEARING);

			//Security 12 has end date (maturity date of 1/1/2014
			AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(12), quantity, price, OCTOBER_2_2013, getBroker(2), getHoldingAccount(4), false);
			AccountingCommissionCalculationCommand calculationInfo = buildCommissionCalculationInfo(journalDetail, commission, notional);
			// Should not find a tier
			(new AccountingCommissionCalculatorImpl()).computeCommission(calculationInfo);
		}, "Commission definition [AccountingCommissionDefinition [ID=45, Label=Global Expense for Security 11 never ending.]] is configured to have tiers, but none are defined.");
	}


	/**
	 * Tests what happens when the calculated commission is below the specified minimum (should return the minimum)
	 */
	@Test
	public void testFullAmountCommissionBelowMinimum() {
		BigDecimal min = MathUtils.add(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the commission floor at the commissionAmount + 10 = 110
		doMinAndMaxTest(min, null, min);
	}


	/**
	 * Tests what happens when the commission amount is above the commission minimum (should return the original commission)
	 */
	@Test
	public void testFullAmountCommissionAboveMinimum() {
		BigDecimal min = MathUtils.subtract(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the floor at the commissionAmount - 10 = 90
		doMinAndMaxTest(min, null, COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT);
	}


	/**
	 * Tests what happens when the commission is above the maximum (should return the max value)
	 */
	@Test
	public void testFullAmountCommissionAboveMaximum() {
		BigDecimal max = MathUtils.subtract(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the ceiling at the commissionAmount - 10 = 90
		doMinAndMaxTest(null, max, max);
	}


	/**
	 * Tests what happens when the commission is below the maximum value (should return the original commission)
	 */
	@Test
	public void testFullAmountCommissionBelowMaximum() {
		BigDecimal max = MathUtils.add(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the ceiling at the commissionAmount + 10 = 110
		doMinAndMaxTest(null, max, COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT);
	}


	/**
	 * Tests what happens when the commission is between the minimum and the maximum (should return the original commission)
	 */
	@Test
	public void testFullAmountCommissionMinimumAndMaximumBetween() {
		BigDecimal min = MathUtils.subtract(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the floor at the commissionAmount - 10 = 90
		BigDecimal max = MathUtils.add(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the ceiling at the commissionAmount + 10 = 110
		doMinAndMaxTest(min, max, COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT);
	}


	/**
	 * Tests what happens when the commission is above both the min and the max (should return the max)
	 */
	@Test
	public void testFullAmountCommissionAboveMinimumAndMaximum() {
		BigDecimal min = MathUtils.subtract(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("30")); //Sets the floor at the commissionAmount - 30 = 70
		BigDecimal max = MathUtils.subtract(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the ceiling at the commissionAmount - 10 = 90
		doMinAndMaxTest(min, max, max);
	}


	/**
	 * Tests what happens when the commission is below both the max and the min (should return the min)
	 */
	@Test
	public void testFullAmountCommissionBelowMinimumAndMaximum() {
		BigDecimal min = MathUtils.add(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("10")); //Sets the floor at the commissionAmount + 10 = 110
		BigDecimal max = MathUtils.add(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING_COMMISSION_AMOUNT, new BigDecimal("30")); //Sets the ceiling at the commissionAmount + 30 = 130
		doMinAndMaxTest(min, max, min);
	}


	private void doMinAndMaxTest(BigDecimal min, BigDecimal max, BigDecimal expectedResult) {
		int quantity = 5;
		BigDecimal price = createBigDecimal(1.3);
		BigDecimal notional = createBigDecimal(1.7);

		AccountingJournalDetail journalDetail = createJournalDetail(getSecurity(1), quantity, price, SEPT_1_2013, getBroker(1), getHoldingAccount(1), true);

		AccountingCommissionDefinition executingDefinition = getCommission(COMMISSION_FULL_AMOUNT_HALF_TURN_EXECUTING);
		if (!MathUtils.isNullOrZero(min)) {
			executingDefinition.setCommissionMinimum(min);
			executingDefinition.setMinimumCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
		}
		if (!MathUtils.isNullOrZero(max)) {
			executingDefinition.setCommissionMaximum(max);
			executingDefinition.setMaximumCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
		}
		this.accountingCommissionService.saveAccountingCommissionDefinition(executingDefinition);

		AccountingCommissionCalculationCommand executionCommand = buildCommissionCalculationInfo(journalDetail, executingDefinition, notional);
		BigDecimal executionResult = (new AccountingCommissionCalculatorImpl()).computeCommission(executionCommand);
		Assertions.assertEquals(expectedResult, executionResult);
	}


	private AccountingJournalDetail createJournalDetail(InvestmentSecurity security, int quantity, BigDecimal price, Date date, BusinessCompany executingBroker, InvestmentAccount holdingAccount,
	                                                    boolean opening) {
		AccountingJournalDetail journalDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, quantity, price, COST_BASIS, EXCHANGE_RATE_TO_BASE, date);
		journalDetail.setExecutingCompany(executingBroker);
		journalDetail.setHoldingInvestmentAccount(holdingAccount);
		journalDetail.setOpening(opening);

		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(new AccountingJournalType(AccountingJournalType.TRADE_JOURNAL, null));

		journalDetail.setJournal(journal);

		return journalDetail;
	}


	private BigDecimal createBigDecimal(double value) {
		BigDecimal bigDec = new BigDecimal(value);
		return bigDec.setScale(2, RoundingMode.HALF_UP);
	}


	private AccountingCommissionDefinition getCommission(int id) {
		return this.accountingCommissionService.getAccountingCommissionDefinition(id);
	}


	private InvestmentAccount getHoldingAccount(int id) {
		return this.investmentAccountService.getInvestmentAccount(id);
	}


	private BusinessCompany getBroker(int id) {
		return this.businessCompanyService.getBusinessCompany(id);
	}


	private InvestmentSecurity getSecurity(int id) {
		return this.investmentInstrumentService.getInvestmentSecurity(id);
	}


	private AccountingCommissionCalculationCommand buildCommissionCalculationInfo(AccountingJournalDetailDefinition journalDetail, AccountingCommissionDefinition commissionDefinition, BigDecimal notional) {
		AccountingCommissionCalculationCommand commissionCalculationCommand = AccountingCommissionCalculatorImpl.createCommissionCalculationCommand(journalDetail, null);
		commissionCalculationCommand.setNotionalAmount(notional);
		commissionCalculationCommand.setCommissionDefinition(commissionDefinition);
		return commissionCalculationCommand;
	}
}
