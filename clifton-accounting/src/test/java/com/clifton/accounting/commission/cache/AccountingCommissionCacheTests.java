package com.clifton.accounting.commission.cache;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.AccountingTransactionDetailInfo;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>CommissionSpecificityCacheTests</code> relies on CommissionSpecificityCacheTests-context.xml to generate
 * several instruments and commission definitions to be tested. This set of tests verifies that all 12 instruments
 * defined in this configuration return the correct commission definitions. These tests fit more into the category of
 * automated functional tests rather than unit tests. Each test is not testing a specific component or method, rather
 * it is testing the algorithm and data structure as a whole.
 *
 * @author jgommels
 */
@ContextConfiguration(locations = "../AccountingCommissionTests-context.xml")
@ExtendWith(SpringExtension.class)
public class AccountingCommissionCacheTests {

	@Resource
	private AccountingCommissionService accountingCommissionService;

	@Resource
	private AccountingCommissionDefinitionSpecificityCache accountingCommissionDefinitionSpecificityCache;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private AccountingTransactionTypeService accountingTransactionTypeService;

	private static final BigDecimal QUANTITY = new BigDecimal(10);
	private static final BigDecimal PRICE = new BigDecimal(2);
	private static final Date CURRENT_DATE = DateUtils.toDate("9/1/2013");

	private static final int COMMISSION_INSTRUMENT1_ACCOUNT1_EXECUTING = 1;
	private static final int COMMISSION_INSTRUMENT1_ACCOUNT1_CLEARING = 2;
	private static final int COMMISSION_INSTRUMENT8_ACCOUNT3_EXECUTING = 3;
	private static final int COMMISSION_INSTRUMENT8_ACCOUNT3_CLEARING = 4;
	private static final int SEC_FEE_HIERARCHY4_ACCOUNT2 = 8;
	private static final int MANAGEMENT_FEE_FUTURES_ACCOUNT4 = 9;
	private static final int COMMISSION_SWAPS_ACCOUNT4_EXECUTING = 10;
	private static final int COMMISSION_SWAPS_ACCOUNT4_CLEARING = 11;
	@SuppressWarnings("unused")
	private static final int SEC_FEE_INSTRUMENT6_BROKER1 = 12;
	@SuppressWarnings("unused")
	private static final int COMMISSION_INSTRUMENT6_BROKER1_PAST_EXECUTING = 13;
	private static final int COMMISSION_INSTRUMENT6_BROKER1_PAST_CLEARING = 14;

	private static final int COMMISSION_INSTRUMENT6_BROKER1_PRESENT_EXECUTING = 15;
	private static final int COMMISSION_INSTRUMENT6_BROKER1_PRESENT_CLEARING = 16;

	@SuppressWarnings("unused")
	private static final int MANAGEMENT_FEE_INSTRUMENT6_BROKER1_FUTURE = 17;
	private static final int MANAGEMENT_FEE_INSTRUMENT6_BROKER2 = 18;
	private static final int SEC_FEE_HIERARCHY2_BROKER1 = 19;
	private static final int SEC_FEE_HIERARCHY3_BROKER2 = 20;
	private static final int COMMISSION_FUTURES_BROKER1_EXECUTING = 21;
	private static final int COMMISSION_FUTURES_BROKER1_CLEARING = 22;
	private static final int COMMISSION_FUTURES_BROKER2_EXECUTING = 23;
	private static final int COMMISSION_FUTURES_BROKER2_CLEARING = 24;
	private static final int COMMISSION_SWAPS_BROKER2_EXECUTING = 25;
	private static final int COMMISSION_SWAPS_BROKER2_CLEARING = 26;
	@SuppressWarnings("unused")
	private static final int SEC_FEE_INSTRUMENT1_PAST = 27;
	@SuppressWarnings("unused")
	private static final int SEC_FEE_INSTRUMENT1_FUTURE = 28;
	private static final int SEC_FEE_INSTRUMENT1_PRESENT = 29;
	@SuppressWarnings("unused")
	private static final int COMMISSION_INSTRUMENT12_EXECUTING = 30;
	private static final int COMMISSION_INSTRUMENT12_CLEARING = 31;
	private static final int MANAGEMENT_FEE_HIERARCHY4 = 32;
	private static final int MANAGEMENT_FEE_FUTURES = 33;
	private static final int COMMISSION_SECURITY5_EXECUTING = 34;
	private static final int COMMISSION_SECURITY5_CLEARING = 35;
	private static final int COMMISSION_INVESTMENT_TYPE2_EXCHANGE3_EXECUTING = 36;
	private static final int COMMISSION_INVESTMENT_TYPE2_EXCHANGE3_CLEARING = 37;
	private static final int COMMISSION_TRANSACTION_TYPE_MATURITY_EXECUTING = 38;
	private static final int COMMISSION_TRANSACTION_TYPE_MATURITY_CLEARING = 39;


	@Test
	public void test3GLAccountsOnCloseWithDateCheck() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(1);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(1), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(4, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INSTRUMENT1_ACCOUNT1_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INSTRUMENT1_ACCOUNT1_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_INSTRUMENT1_PRESENT)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES)));
	}


	@Test
	public void test2GlAccountsOnClose() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(2);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(1), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES)));
	}


	@Test
	public void testDifferentBrokersOnClose() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(3);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(4), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES_ACCOUNT4)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER2_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER2_CLEARING)));
	}


	@Test
	public void test3GlAccountsOnClose() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(4);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(1), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(4, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY2_BROKER1)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES)));
	}


	@Test
	public void test3GlAccountsOnClose2() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(5);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(1), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(4, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY2_BROKER1)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES)));
	}


	@Test
	public void testDifferentBrokersOnOpenWithDateCheck() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(6);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(1), true);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(4, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INSTRUMENT6_BROKER1_PRESENT_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_FUTURES_BROKER1_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY2_BROKER1)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_FUTURES)));
	}


	@Test
	public void testDifferentBrokersOnOpen2() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(7);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(4), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_ACCOUNT4_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_ACCOUNT4_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY3_BROKER2)));
	}


	@Test
	public void testDifferentBrokersOnClose2() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(8);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(1), getHoldingAccount(3), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INSTRUMENT8_ACCOUNT3_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INSTRUMENT8_ACCOUNT3_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY3_BROKER2)));
	}


	@Test
	public void test2GlAccountsOnClose2() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(9);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(4), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_ACCOUNT4_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_ACCOUNT4_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY3_BROKER2)));
	}


	@Test
	public void testDifferentBrokersOnClose3() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(10);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(2), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(2, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(SEC_FEE_HIERARCHY4_ACCOUNT2)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_HIERARCHY4)));
	}


	@Test
	public void testDifferentBrokersOnClose4() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(12);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(3), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		Assertions.assertEquals(3, commissionDefinitions.size());
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_BROKER2_EXECUTING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SWAPS_BROKER2_CLEARING)));
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(MANAGEMENT_FEE_HIERARCHY4)));
	}


	@Test
	public void testSecuritySpecificCommission() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(5);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(3), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		//Here we are primarily concerned with making sure the security-specific commission is contained in the list
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_SECURITY5_EXECUTING)));
	}


	@Test
	public void testExchangeSpecificCommission() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(11);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(3), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		//Here we are primarily concerned with making sure the exchange-specific commission is contained in the list
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_INVESTMENT_TYPE2_EXCHANGE3_EXECUTING)));
	}


	@Test
	public void testMaturitySpecificCommission() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(11);
		AccountingJournalDetail journalDetail = newAccountingJournalDetail(security, getBroker(2), getHoldingAccount(3), false);

		AccountingMatchingCommissionDefinitionResultSet result = getCachedCommissions(journalDetail, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.MATURITY));

		List<AccountingCommissionDefinition> commissionDefinitions = result.getCommissionDefinitions();

		//Here we are primarily concerned with making sure the maturity-specific commission is contained in the list
		Assertions.assertTrue(commissionDefinitions.contains(getCommission(COMMISSION_TRANSACTION_TYPE_MATURITY_EXECUTING)));
	}


	private AccountingJournalDetail newAccountingJournalDetail(InvestmentSecurity security, BusinessCompany executingBroker, InvestmentAccount holdingAccount, boolean opening) {
		AccountingJournalDetail journalDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, QUANTITY, PRICE, BigDecimal.ONE, BigDecimal.ONE, CURRENT_DATE);
		journalDetail.setExecutingCompany(executingBroker);
		journalDetail.setHoldingInvestmentAccount(holdingAccount);
		journalDetail.setOpening(opening);

		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(new AccountingJournalType(AccountingJournalType.TRADE_JOURNAL, null));

		journalDetail.setJournal(journal);

		return journalDetail;
	}


	private AccountingMatchingCommissionDefinitionResultSet getCachedCommissions(AccountingJournalDetail journalDetail, AccountingTransactionType transactionType) {
		AccountingTransactionDetailInfo transactionInfo = new AccountingTransactionDetailInfo(journalDetail, Mockito.mock(BookableEntity.class), transactionType);
		return this.accountingCommissionDefinitionSpecificityCache.getMostSpecificCommissionDefinitions(transactionInfo);
	}


	private InvestmentAccount getHoldingAccount(int id) {
		return this.investmentAccountService.getInvestmentAccount(id);
	}


	private BusinessCompany getBroker(int id) {
		return this.businessCompanyService.getBusinessCompany(id);
	}


	private AccountingCommissionDefinition getCommission(int id) {
		return this.accountingCommissionService.getAccountingCommissionDefinition(id);
	}
}
