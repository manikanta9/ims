package com.clifton.accounting.commission.calculation.complex;

import com.clifton.accounting.commission.calculation.AccountingCommissionCalculatorTests;
import com.clifton.accounting.commission.calculation.calculators.AccountingCommissionCalculationCommand;
import com.clifton.accounting.commission.calculation.calculators.adjustment.MerrillLynchCommissionAdjustmentCalculator;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author theodorez
 */
public class MerrillLynchCommissionAdjustmentCalculatorTests {

	private static final String CALCULATOR_JSON_CONFIG = "{" +
			"\"minimumCommissionThreshold\":\"433.33\"," +
			"\"minimumCommissionAmount\":\"65\"," +
			"\"minimumCommissionMultiplier\":\"0.15\"," +
			"\"premiumLowerThreshold\":\"130\"," +
			"\"premiumUpperThreshold\":\"500\"," +
			"\"premiumMinimum\":\"65\"," +
			"\"premiumTiers\":[" +
			"{\"threshold\":\"500\",\"feeAmount\":\"0.15\",\"baseValue\":\"0\"}," +
			"{\"threshold\":\"1500\",\"feeAmount\":\"0.015\",\"baseValue\":\"25\"}," +
			"{\"threshold\":\"10000\",\"feeAmount\":\"0.013\",\"baseValue\":\"28\"}," +
			"{\"threshold\":\"20000\",\"feeAmount\":\"0.009\",\"baseValue\":\"53\"}," +
			"{\"threshold\":\"50000\",\"feeAmount\":\"0.008\",\"baseValue\":\"73\"}," +
			"{\"threshold\":\"\",\"feeAmount\":\"0.007\",\"baseValue\":\"123\"}" +
			"]," +
			"\"contractThreshold\":\"500\"," +
			"\"contractTiers\":[" +
			"{\"threshold\":\"10\",\"feeAmount\":\"9\",\"baseValue\":\"0\"}," +
			"{\"threshold\":\"20\",\"feeAmount\":\"8\",\"baseValue\":\"10\"}," +
			"{\"threshold\":\"50\",\"feeAmount\":\"7\",\"baseValue\":\"30\"}," +
			"{\"threshold\":\"100\",\"feeAmount\":\"6\",\"baseValue\":\"80\"}," +
			"{\"threshold\":\"200\",\"feeAmount\":\"5\",\"baseValue\":\"180\"}," +
			"{\"threshold\":\"500\",\"feeAmount\":\"4\",\"baseValue\":\"380\"}," +
			"{\"threshold\":\"\",\"feeAmount\":\"3\",\"baseValue\":\"880\"}" +
			"]," +
			"\"maxCommissionTiers\":[" +
			"{\"threshold\":\"433.333\",\"feeAmount\":\"0.15\",\"baseValue\":\"0\"}," +
			"{\"threshold\":\"542\",\"feeAmount\":\"0\",\"baseValue\":\"65\"}," +
			"{\"threshold\":\"1000\",\"feeAmount\":\"0.12\",\"baseValue\":\"0\"}," +
			"{\"threshold\":\"1500\",\"feeAmount\":\"0.09\",\"baseValue\":\"30\"}," +
			"{\"threshold\":\"2500\",\"feeAmount\":\"0.08\",\"baseValue\":\"45\"}," +
			"{\"threshold\":\"5000\",\"feeAmount\":\"0.07\",\"baseValue\":\"70\"}," +
			"{\"threshold\":\"10000\",\"feeAmount\":\"0.05\",\"baseValue\":\"170\"}," +
			"{\"threshold\":\"\",\"feeAmount\":\"0.03\",\"baseValue\":\"370\"}" +
			"]" +
			"}";

	private static final JsonHandler<?> JSON_HANDLER = new JacksonHandlerImpl();


	@Test
	public void testPick15PercentNotional() {
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("100");
		BigDecimal expectedValue = new BigDecimal("15");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPick15Percent130Notional() {
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("130.01");
		BigDecimal expectedValue = new BigDecimal("19.5");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts5() {
		BigDecimal expectedValue = new BigDecimal("99.65");
		BigDecimal quantity = new BigDecimal("5");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts15() {
		BigDecimal expectedValue = new BigDecimal("184.65");
		BigDecimal quantity = new BigDecimal("15");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts25() {
		BigDecimal expectedValue = new BigDecimal("209");
		BigDecimal quantity = new BigDecimal("25");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts55() {
		BigDecimal expectedValue = new BigDecimal("209");
		BigDecimal quantity = new BigDecimal("55");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts105() {
		BigDecimal expectedValue = new BigDecimal("209");
		BigDecimal quantity = new BigDecimal("105");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts205() {
		BigDecimal expectedValue = new BigDecimal("209");
		BigDecimal quantity = new BigDecimal("205");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2050Contracts505() {
		BigDecimal expectedValue = new BigDecimal("209");
		BigDecimal quantity = new BigDecimal("505");
		BigDecimal notional = new BigDecimal("2050");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickCalculated500() {
		BigDecimal expectedValue = new BigDecimal("65");
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("499.99");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional501() {
		BigDecimal expectedValue = new BigDecimal("65");
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("500.01");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional545() {
		BigDecimal expectedValue = new BigDecimal("65.40");
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("545");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional1042() {
		BigDecimal expectedValue = new BigDecimal("123.78");
		BigDecimal quantity = new BigDecimal("10");
		BigDecimal notional = new BigDecimal("1042");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2242() {
		BigDecimal expectedValue = new BigDecimal("224.36");
		BigDecimal quantity = new BigDecimal("100");
		BigDecimal notional = new BigDecimal("2242");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional2742() {
		BigDecimal expectedValue = new BigDecimal("261.94");
		BigDecimal quantity = new BigDecimal("100");
		BigDecimal notional = new BigDecimal("2742");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional5542() {
		BigDecimal expectedValue = new BigDecimal("447.10");
		BigDecimal quantity = new BigDecimal("100");
		BigDecimal notional = new BigDecimal("5542");
		doTest(expectedValue, quantity, notional);
	}


	@Test
	public void testPickNotional10042() {
		BigDecimal expectedValue = new BigDecimal("671.26");
		BigDecimal quantity = new BigDecimal("100");
		BigDecimal notional = new BigDecimal("10042");
		doTest(expectedValue, quantity, notional);
	}


	private void doTest(BigDecimal expectedValue, BigDecimal quantity, BigDecimal notional) {
		AccountingCommissionCalculationCommand command = new AccountingCommissionCalculationCommand(null, quantity);
		command.setNotionalAmount(notional);
		MerrillLynchCommissionAdjustmentCalculator calculator = new MerrillLynchCommissionAdjustmentCalculator();
		calculator.setAdjustmentCalculatorConfiguration(CALCULATOR_JSON_CONFIG);
		BigDecimal result = calculator.computeCommission(BigDecimal.ZERO, command, JSON_HANDLER);
		LogUtils.info(AccountingCommissionCalculatorTests.class, "Expected: " + expectedValue + " Result: " + result);
		Assertions.assertTrue(MathUtils.isEqual(MathUtils.round(expectedValue, 2), MathUtils.round(result, 2)));
	}
}
