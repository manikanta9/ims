package com.clifton.accounting.gl.position;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class AccountingPositionHandlerImplTests {

	@Resource
	private AccountingPositionHandler accountingPositionHandler;

	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////

	private static final int TEST_ACCOUNT_1 = 1;
	private static final int TEST_ACCOUNT_2 = 2;
	private static final int TEST_ACCOUNT_3 = 3;


	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupCalendarData() {
		// Setup Once
		short year = 2015;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
		year = 2016;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}


	@Test
	public void testIsClientAccountPositionsOnForDate() {
		// Test Account 1 Positions On: 03/08/2016, Positions Off: 06/07/2016
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDate(TEST_ACCOUNT_1, DateUtils.toDate("03/01/2016")));
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDate(TEST_ACCOUNT_1, DateUtils.toDate("03/08/2016")));
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDate(TEST_ACCOUNT_1, DateUtils.toDate("06/01/2016")));
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDate(TEST_ACCOUNT_1, DateUtils.toDate("10/01/2016")));
	}


	@Test
	public void testIsClientAccountPositionsOnForDateRange() {
		// Test Account 1 Positions On: 03/08/2016, Positions Off: 06/07/2016
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_1, DateUtils.toDate("03/01/2016"), DateUtils.toDate("03/31/2016")));
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_1, DateUtils.toDate("04/01/2016"), DateUtils.toDate("05/31/2016")));
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_1, DateUtils.toDate("06/01/2016"), DateUtils.toDate("06/06/2016")));
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_1, DateUtils.toDate("06/01/2016"), DateUtils.toDate("06/07/2016")));
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_1, DateUtils.toDate("10/01/2016"), DateUtils.toDate("10/01/2016")));
	}


	@Test
	public void testIsClientAccountPositionsOnForDateRange_BusinessDays() {
		// Test Account 3 Positions On: 03/02/2015, Positions Off: 05/29/2015
		// Should consider March Full Month because 3/2 was a Monday - Sunday is ignored in the check.
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_3, DateUtils.toDate("03/01/2015"), DateUtils.toDate("03/31/2015")));
		// April is a Full Month
		Assertions.assertTrue(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_3, DateUtils.toDate("04/01/2015"), DateUtils.toDate("04/30/2015")));
		// May is NOT a full month because they took positions off on the last business day of the month
		Assertions.assertFalse(this.accountingPositionHandler.isClientAccountPositionsOnForDateRange(TEST_ACCOUNT_3, DateUtils.toDate("05/01/2015"), DateUtils.toDate("05/31/2015")));
	}


	@Test
	public void testGetClientAccountPositionsOnDays() {
		// Test Account 1 Positions On: 03/08/2016, Positions Off: 06/07/2016
		Assertions.assertEquals(24, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_1, DateUtils.toDate("03/01/2016"), DateUtils.toDate("03/31/2016"), null));
		Assertions.assertEquals(61, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_1, DateUtils.toDate("04/01/2016"), DateUtils.toDate("05/31/2016"), null));
		Assertions.assertEquals(6, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_1, DateUtils.toDate("06/01/2016"), DateUtils.toDate("06/07/2016"), null));
		Assertions.assertEquals(0, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_1, DateUtils.toDate("10/01/2016"), DateUtils.toDate("10/01/2016"), null));

		StringBuilder resultMessage = new StringBuilder(10);
		// Test Account 2 Positions On 1/20/2016, Off 2/1/2016, On 2/18/2016, Off 3/7/2016, On 3/9/2016, Off 4/5/2016, On 06/30/2016, Off 07/01/2016
		Assertions.assertEquals(12, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_2, DateUtils.toDate("01/01/2016"), DateUtils.toDate("01/31/2016"), resultMessage));
		Assertions.assertEquals("Positions On: 01/20/2016; ", resultMessage.toString());
		resultMessage = new StringBuilder(10);
		Assertions.assertEquals(13, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_2, DateUtils.toDate("02/01/2016"), DateUtils.toDate("03/01/2016"), resultMessage));
		Assertions.assertEquals("Positions Off: 02/01/2016; Positions On: 02/18/2016; ", resultMessage.toString());
		resultMessage = new StringBuilder(10);
		Assertions.assertEquals(11, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_2, DateUtils.toDate("03/10/2016"), DateUtils.toDate("03/20/2016"), resultMessage));
		Assertions.assertEquals("Positions On - Full Period", resultMessage.toString());
		resultMessage = new StringBuilder(10);
		Assertions.assertEquals(0, this.accountingPositionHandler.getClientAccountPositionsOnDays(TEST_ACCOUNT_2, DateUtils.toDate("05/01/2016"), DateUtils.toDate("05/31/2016"), resultMessage));
		Assertions.assertEquals("Positions Off All Period", resultMessage.toString());
	}
}
