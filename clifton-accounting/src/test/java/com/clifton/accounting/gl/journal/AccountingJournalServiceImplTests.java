package com.clifton.accounting.gl.journal;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class AccountingJournalServiceImplTests {

	private AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
	@Resource
	private AccountingJournalServiceImpl accountingJournalService;


	////////////////////////////////////////////////////////////////////////////
	////////             Test SAVE AccountingJournal               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveAccountingJournal_UnpostedGeneralJournal() {
		AccountingJournal journal = this.accountingJournalService.getAccountingJournal(1);
		Assertions.assertNotNull(journal);

		Assertions.assertNotSame("New Name", journal.getDescription());
		journal.setDescription("New Name");
		this.accountingJournalService.saveAccountingJournal(journal);
		Assertions.assertEquals("New Name", this.accountingJournalService.getAccountingJournal(1).getDescription());
	}


	@Test
	public void testReferenceToDeletedTransaction() {
		AccountingJournal journal = createDefaultJournal();
		AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(0);
		detail.setParentTransaction(new AccountingTransaction());
		detail.getParentTransaction().setDeleted(true);

		TestUtils.expectException(ValidationException.class, () -> this.accountingJournalService.validateAccountingJournal(journal), "Journal detail " + detail + " cannot reference a Deleted transaction.");
	}


	@Test
	public void testQuantityAndCostBasisOppositeSign() {
		TestUtils.expectException(ValidationException.class, () -> {
			// test what's allowed first
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setQuantity(new BigDecimal(-100));
			journal.getJournalDetailList().get(0).setPositionCostBasis(new BigDecimal(0));
			this.accountingJournalService.validateAccountingJournal(journal);

			journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setQuantity(new BigDecimal(0));
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
			journal.getJournalDetailList().get(0).setPositionCostBasis(new BigDecimal(1));
			this.accountingJournalService.validateAccountingJournal(journal);
			journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setQuantity(new BigDecimal(-100));
			journal.getJournalDetailList().get(0).setPositionCostBasis(new BigDecimal(100));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Quantity and PositionCostBasis sign must be the same for each journal detail. Journal: {id = null; fkFieldId = null; systemTable = null}");
	}


	@Test
	public void testNegativePrice() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setPrice(new BigDecimal(-100));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Price of -100 cannot be negative.");
	}


	@Test
	public void testNegativeFxRate() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setExchangeRateToBase(new BigDecimal(-1));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Exchange Rate cannot be negative or zero for each journal detail.");
	}


	@Test
	public void testZeroFxRate() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setExchangeRateToBase(BigDecimal.ZERO);
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Exchange Rate cannot be negative or zero for each journal detail.");
	}


	@Test
	public void testCurrencyGLAccountUsedIncorrectly() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));

			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Currency GL account 'Currency' cannot be used for a journal detail where the client's base currency is the same as the security's trading currency.");
	}


	@Test
	public void testCashGLAccountUsedWithNonCash() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
			journal.getJournalDetailList().get(0).setInvestmentSecurity(new InvestmentSecurity("IBM", 3));

			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Cash GL account 'Cash' cannot be used for a journal detail where the client's base currency is different from security.");
	}


	@Test
	public void testExchangeRateNotOneForDomesticSecurity() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME));
			journal.getJournalDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.23"));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Exchange Rate for security that has currency denomination of client's base currency must be 1 and not: 1.23");
	}


	@Test
	public void testLocalAndBaseDebitCreditMustEqual_ForDomesticSecurity() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_DIVIDEND_INCOME));
			journal.getJournalDetailList().get(0).setBaseDebitCredit(new BigDecimal("2"));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Base DebitCredit = 2 must equal local DebitCredit = 1 for security that has currency denomination of client's base currency.");
	}


	@Test
	public void testGainLossWithParent() {
		AccountingJournal journal = createDefaultJournal();
		AccountingJournalDetailDefinition firstDetail = journal.getJournalDetailList().get(0);
		firstDetail.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		firstDetail.setParentTransaction(new AccountingTransaction());
		firstDetail.getParentTransaction().setClientInvestmentAccount(firstDetail.getClientInvestmentAccount());
		this.accountingJournalService.validateAccountingJournal(journal);

		journal = createDefaultJournal();
		firstDetail.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		firstDetail.setParentDefinition(new AccountingTransaction());
		this.accountingJournalService.validateAccountingJournal(journal);
	}


	@Test
	public void testGainLossWithoutParent() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			journal.getJournalDetailList().get(0).setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
			this.accountingJournalService.validateAccountingJournal(journal);
		}, "Gain/Loss GL Account entry must be tied to corresponding Position that the gain/loss is for.");
	}


	@Test
	public void testNonOtcPositionInOtcHoldingAccount_Fail() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = createDefaultJournal();
			AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(0);
			detail.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
			detail.setPositionCostBasis(BigDecimal.ONE);
			detail.getHoldingInvestmentAccount().getType().setOtc(true);

			this.accountingJournalService.validateAccountingJournal(journal);
		}, "[H-200: Test Holding Account (Broker)] Holding Account cannot have non OTC position that is not collateral [USD]");
	}


	@Test
	public void testNonOtcPositionInOtcHoldingAccount_Success() {
		AccountingJournal journal = createDefaultJournal();
		AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(0);
		detail.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL));
		detail.setPositionCostBasis(BigDecimal.ONE);
		detail.getHoldingInvestmentAccount().getType().setOtc(true);

		this.accountingJournalService.validateAccountingJournal(journal);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a default journal with 2 details. Makes it easy to populate additional fields.
	 */
	private AccountingJournal createDefaultJournal() {
		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(new AccountingJournalType());

		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		detail.setInvestmentSecurity(InvestmentSecurityBuilder.newUSD());
		detail.setClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		detail.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		detail.setTransactionDate(new Date());
		detail.setSettlementDate(new Date());
		detail.setOriginalTransactionDate(new Date());
		detail.setLocalDebitCredit(BigDecimal.ONE);
		detail.setBaseDebitCredit(BigDecimal.ONE);
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(BigDecimal.ZERO);
		detail.setOpening(true);
		journal.addJournalDetail(detail);

		AccountingJournalDetail detail2 = BeanUtils.cloneBean(detail, false, false);
		detail2.setLocalDebitCredit(BigDecimal.ONE.negate());
		detail2.setBaseDebitCredit(BigDecimal.ONE.negate());
		journal.addJournalDetail(detail2);

		return journal;
	}


	@Test
	public void testSaveAccountingJournal_SetPostingDate() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(1);
			Assertions.assertNotNull(journal);

			journal.setPostingDate(new Date());
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "Cannot manually change posting date of a Journal. It can only be set by GL posting process.");
	}


	@Test
	public void testSaveAccountingJournal_BaseDebitsNotEqualBaseCredits() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(1);
			Assertions.assertNotNull(journal);

			journal.getJournalDetailList().get(0).setLocalDebitCredit(BigDecimal.valueOf(777));
			journal.getJournalDetailList().get(0).setBaseDebitCredit(BigDecimal.valueOf(777));

			this.accountingJournalService.saveAccountingJournal(journal);
		}, "The sum of base debits for journal [id = 1] \"Unposted General Journal\" does not equal the sum of base credits by client investment account for transaction or original transaction date and the differences does not fall within allowed adjustment range.  The difference is 764.48");
	}


	@Test
	public void testSaveAccountingJournal_InvalidFXRate() {

		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(1);
			Assertions.assertNotNull(journal);

			journal.getJournalDetailList().get(0).setBaseDebitCredit(BigDecimal.valueOf(777));

			this.accountingJournalService.saveAccountingJournal(journal);
		}, "(Base Debit - Base Credit) <> FX Rate * (Local Debit - Local Credit): 777 <> 1 * 12.52 (12.52) on 10/01/2010");
	}


	@Test
	public void testSaveAccountingJournal_PostedGeneralJournal() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(2);
			Assertions.assertNotNull(journal);

			journal.setDescription("New Name");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "Cannot modify a journal that has already been posted to GL.");
	}


	@Test
	public void testSaveAccountingJournal_InactiveSecurity() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(4);
			Assertions.assertNotNull(journal);

			journal.setDescription("inactive security");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "Investment security TEST (Test Inactive) [INACTIVE] must be active on transaction date 10/01/2010");
	}


	@Test
	public void testSaveAccountingJournal_OTCSecurityMissingISDA() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(9);
			Assertions.assertNotNull(journal);

			journal.setDescription("no holding account isda for OTC security");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "ISDA must be selected for holding account [GS-ISDA: Goldman's ISDA Account (OTC ISDA)] in order to post OTC security [OTC].");
	}


	@Test
	public void testSaveAccountingJournal_OTCSecurityWithISDA() {
		AccountingJournal journal = this.accountingJournalService.getAccountingJournal(10);
		Assertions.assertNotNull(journal);

		journal.setDescription("holding account WITH isda for OTC security");
		this.accountingJournalService.saveAccountingJournal(journal);
	}


	@Test
	public void testSaveAccountingJournal_UpdateSubSystemJournal() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(6);
			Assertions.assertNotNull(journal);

			journal.setDescription("New Name");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "Sub-system journals cannot be modified.");
	}


	@Test
	public void testSaveAccountingJournal_InvalidRelationship() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(7);
			Assertions.assertNotNull(journal);

			journal.setDescription("invalid relationship");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "Relationship between client account 222: Clifton's Duke Account and holding account 333: Citi Test Account (Broker Account) is not defined on Transaction Date 10/01/2010");
	}


	@Test
	public void testSaveAccountingJournal_OutOfBalanceByTransactionDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			AccountingJournal journal = this.accountingJournalService.getAccountingJournal(8);
			Assertions.assertNotNull(journal);

			journal.setDescription("out of balance");
			this.accountingJournalService.saveAccountingJournal(journal);
		}, "The sum of base debits for journal [id = 8] \"out of balance\" does not equal the sum of base credits by client investment account for transaction or original transaction date and the differences does not fall within allowed adjustment range.  The difference is 12.52");
	}


	////////////////////////////////////////////////////////////////////////////
	////////             Test DELETE AccountingJournal             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDeleteAccountingJournal() {
		this.accountingJournalService.deleteAccountingJournal(3, false);
		Assertions.assertNull(this.accountingJournalService.getAccountingJournal(3), "The journal was deleted and should no longer exist.");
	}


	@Test
	public void testDeleteAccountingJournal_Posted() {
		TestUtils.expectException(ValidationException.class, () -> this.accountingJournalService.deleteAccountingJournal(2, false), "Cannot delete a journal that has already been posted to GL.");
	}
}
