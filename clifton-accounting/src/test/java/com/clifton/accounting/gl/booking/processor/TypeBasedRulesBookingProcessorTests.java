package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.booking.AccountingEventJournalBookingProcessor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TypeBasedRulesBookingProcessorTests {


	@Test
	public void testGetBookingRulesKeyOverride_InvestmentType() {
		TypeBasedRulesBookingProcessor<AccountingEventJournal> processor = new AccountingEventJournalBookingProcessor();
		Map<String, List<AccountingBookingRule<AccountingEventJournal>>> rulesMap = new HashMap<>();
		processor.setBookingRulesMap(rulesMap);

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(11634, "1350Z7TZ7", InvestmentType.BONDS);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		hierarchy.setName("Canada");
		hierarchy.setParent(new InvestmentInstrumentHierarchy("T-Bills"));
		hierarchy.getParent().setParent(new InvestmentInstrumentHierarchy("Governments and Sovereigns"));
		hierarchy.getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Physicals"));
		hierarchy.getParent().getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Fixed Income"));

		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// invalid override: still no match
		rulesMap.put("Security Maturity: Bonds", new ArrayList<>());
		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// match InvestmentType override
		rulesMap.put("Security Maturity: [Bonds]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds]", processor.getBookingRulesKeyOverride("Security Maturity", security));
	}


	@Test
	public void testGetBookingRulesKeyOverride_InvestmentType_SubType() {
		TypeBasedRulesBookingProcessor<AccountingEventJournal> processor = new AccountingEventJournalBookingProcessor();
		Map<String, List<AccountingBookingRule<AccountingEventJournal>>> rulesMap = new HashMap<>();
		processor.setBookingRulesMap(rulesMap);

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(11634, "1350Z7TZ7", InvestmentType.BONDS);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		hierarchy.setName("Canada");
		InvestmentTypeSubType subType = new InvestmentTypeSubType();
		subType.setName("Central Government Bonds");
		hierarchy.setInvestmentTypeSubType(subType);
		hierarchy.setParent(new InvestmentInstrumentHierarchy("T-Bills"));
		hierarchy.getParent().setParent(new InvestmentInstrumentHierarchy("Governments and Sovereigns"));
		hierarchy.getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Physicals"));
		hierarchy.getParent().getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Fixed Income"));

		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// invalid override: still no match
		rulesMap.put("Security Maturity: Bonds", new ArrayList<>());
		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// match InvestmentType override
		rulesMap.put("Security Maturity: [Bonds]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds]", processor.getBookingRulesKeyOverride("Security Maturity", security));

		// subtype is more specific than type
		rulesMap.put("Security Maturity: [Bonds:Central Government Bonds]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds:Central Government Bonds]", processor.getBookingRulesKeyOverride("Security Maturity", security));
	}


	@Test
	public void testGetBookingRulesKeyOverride_InvestmentType_SubType_SubType2() {
		TypeBasedRulesBookingProcessor<AccountingEventJournal> processor = new AccountingEventJournalBookingProcessor();
		Map<String, List<AccountingBookingRule<AccountingEventJournal>>> rulesMap = new HashMap<>();
		processor.setBookingRulesMap(rulesMap);

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(11634, "1350Z7TZ7", InvestmentType.BONDS);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		hierarchy.setName("Canada");
		InvestmentTypeSubType subType = new InvestmentTypeSubType();
		subType.setName("Central Government Bonds");
		hierarchy.setInvestmentTypeSubType(subType);
		InvestmentTypeSubType2 subType2 = new InvestmentTypeSubType2();
		subType2.setName("Bills");
		hierarchy.setInvestmentTypeSubType2(subType2);
		hierarchy.setParent(new InvestmentInstrumentHierarchy("T-Bills"));
		hierarchy.getParent().setParent(new InvestmentInstrumentHierarchy("Governments and Sovereigns"));
		hierarchy.getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Physicals"));
		hierarchy.getParent().getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Fixed Income"));

		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// invalid override: still no match
		rulesMap.put("Security Maturity: Bonds", new ArrayList<>());
		Assertions.assertNull(processor.getBookingRulesKeyOverride("Security Maturity", security));

		// match InvestmentType override
		rulesMap.put("Security Maturity: [Bonds]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds]", processor.getBookingRulesKeyOverride("Security Maturity", security));

		// subtype is more specific than type
		rulesMap.put("Security Maturity: [Bonds:Central Government Bonds]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds:Central Government Bonds]", processor.getBookingRulesKeyOverride("Security Maturity", security));

		// subtype2 is more specific
		rulesMap.put("Security Maturity: [Bonds:Central Government Bonds:Bills]", new ArrayList<>());
		Assertions.assertEquals("Security Maturity: [Bonds:Central Government Bonds:Bills]", processor.getBookingRulesKeyOverride("Security Maturity", security));
	}
}
