package com.clifton.accounting.gl.journal;


import com.clifton.accounting.gl.position.AccountingPosition;

import java.util.Comparator;


public class AccountingPositionComparator implements Comparator<AccountingPosition> {

	/**
	 * Order first by the originalTransactionDate (earliest first) then by price (lowest first).
	 * This provides FIFO accounting.
	 */
	@Override
	public int compare(AccountingPosition compareFrom, AccountingPosition compareTo) {
		if (compareTo == null && compareFrom == null) {
			return 0;
		}
		else if (compareTo != null && compareFrom == null) {
			return 1;
		}
		else if (compareTo == null) {
			return -1;
		}
		if (compareFrom.getOriginalTransactionDate() == null && compareTo.getOriginalTransactionDate() == null) {
			return 0;
		}
		else if (compareFrom.getOriginalTransactionDate() != null && compareTo.getOriginalTransactionDate() == null) {
			return 1;
		}
		else if (compareFrom.getOriginalTransactionDate() == null && compareTo.getOriginalTransactionDate() != null) {
			return -1;
		}
		if (compareFrom.getOriginalTransactionDate().compareTo(compareTo.getOriginalTransactionDate()) == 0) {
			if (compareTo.getPrice() == null && compareFrom.getPrice() == null) {
				return 0;
			}
			else if (compareFrom.getPrice() != null && compareTo.getPrice() == null) {
				return 1;
			}
			else if (compareFrom.getPrice() == null && compareTo.getPrice() != null) {
				return -1;
			}
			return compareFrom.getPrice().compareTo(compareTo.getPrice());
		}
		return compareFrom.getOriginalTransactionDate().compareTo(compareTo.getOriginalTransactionDate());
	}
}
