package com.clifton.accounting.gl.balance.removal;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.account.InvestmentAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * These tests confirm the basic validation.  Real tests are added as integration tests
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class AccountingBalanceRemovalServiceTests {


	@Resource
	private AccountingBalanceRemovalServiceImpl accountingBalanceRemovalService;

	@Resource
	private InvestmentAccountService investmentAccountService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingBalanceRemoval_MissingAccounts() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(new AccountingBalanceRemovalCommand());
		}, "No accounts selected");
	}


	@Test
	public void testAccountingBalanceRemoval_InvalidClientAccount() {
		InvestmentAccount holdingAccount = getActiveHoldingAccount();
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(holdingAccount.getId())).thenReturn(holdingAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setClientInvestmentAccountId(holdingAccount.getId());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Selected client account [" + holdingAccount.getLabel() + "] is not a client account.");
	}


	@Test
	public void testAccountingBalanceRemoval_InvalidClientAccountStatus() {
		InvestmentAccount clientAccount = getActiveClientAccount();
		clientAccount.getWorkflowStatus().setName("Terminated");
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(clientAccount.getId())).thenReturn(clientAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setClientInvestmentAccountId(clientAccount.getId());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Client Account [" + clientAccount.getLabel() + "] must currently be active in order to book and post gl transactions to it.");
	}


	@Test
	public void testAccountingBalanceRemoval_InvalidHoldingAccount() {
		InvestmentAccount clientAccount = getActiveClientAccount();
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(clientAccount.getId())).thenReturn(clientAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setHoldingInvestmentAccountId(clientAccount.getId());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Selected holding account [" + clientAccount.getLabel() + "] is a client account.");
	}


	@Test
	public void testAccountingBalanceRemoval_InvalidHoldingAccountStatus() {
		InvestmentAccount holdingAccount = getActiveHoldingAccount();
		holdingAccount.getWorkflowStatus().setName("Terminated");
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(holdingAccount.getId())).thenReturn(holdingAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Holding Account [" + holdingAccount.getLabel() + "] must currently be active in order to book and post gl transactions to it.");
	}


	@Test
	public void testAccountingBalanceRemoval_MissingRemovalDate() {
		InvestmentAccount holdingAccount = getActiveHoldingAccount();
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(holdingAccount.getId())).thenReturn(holdingAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Removal Date is required.");
	}


	@Test
	public void testAccountingBalanceRemoval_RemovalDateInFuture() {
		InvestmentAccount holdingAccount = getActiveHoldingAccount();
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(holdingAccount.getId())).thenReturn(holdingAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			command.setRemovalDate(DateUtils.addDays(new Date(), 5));
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "Removal Date cannot be in the future.");
	}


	@Test
	public void testAccountingBalanceRemoval_MissingDistributionOptions() {
		InvestmentAccount holdingAccount = getActiveHoldingAccount();
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.investmentAccountService.getInvestmentAccount(holdingAccount.getId())).thenReturn(holdingAccount);
			AccountingBalanceRemovalCommand command = new AccountingBalanceRemovalCommand();
			command.setHoldingInvestmentAccountId(holdingAccount.getId());
			command.setRemovalDate(new Date());
			this.accountingBalanceRemovalService.processAccountingBalanceRemoval(command);
		}, "You must select at least one distribution option.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getActiveHoldingAccount() {
		return InvestmentAccountBuilder.createTestAccount1().toInvestmentAccount();
	}


	private InvestmentAccount getActiveClientAccount() {
		return InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount();
	}
}
