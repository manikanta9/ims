package com.clifton.accounting.gl.balance;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * @author NickK
 */
@AccountingInMemoryDatabaseContext
public class AccountingBalanceServiceTests extends BaseInMemoryDatabaseTests {

	// Client Accounts
	private static final Integer U_PITT_DE_CLIENT_ACCOUNT_ID = 2272;
	private static final Integer BFOK_DE_CLIENT_ACCOUNT_ID = 4616;
	private static final Integer SONOMA_DE_CLIENT_ACCOUNT_ID = 5571;

	// Holding Accounts
	private static final Integer U_PITT_CUSTODIAN_HOLDING_ACCOUNT_ID = 2332;
	private static final Integer SONOMA_CUSTODIAN_HOLDING_ACCOUNT_ID = 5572;
	private static final Integer SONOMA_MARGIN_HOLDING_ACCOUNT_ID = 5651;

	private static final Date DATE_10_08_2018 = DateUtils.clearTime(DateUtils.toDate("10/08/2018"));

	private static final String SPX_20D_CLIENT_ACCOUNT_GROUP_NAME = "SPX-20D";

	@Resource
	private AccountingBalanceService accountingBalanceService;
	@Resource
	private AccountingAccountIdsCache accountingAccountIdsCache;
	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;


	@Test
	public void testGetCashBalanceForNullClientAndHoldingAccount() {
		AccountingBalanceCommand command = AccountingBalanceCommand.forTransactionDate(DATE_10_08_2018)
				.withExcludeCashCollateral(false)
				.withNetOnly(true);
		Assertions.assertThrows(ValidationException.class, () -> this.accountingBalanceService.getAccountingBalance(command));
	}


	@Test
	public void testGetCashBalanceForClientAccount() {
		BigDecimal pittExpected = new BigDecimal("2879.16");
		assertCashBalanceForClientAccountEquals(pittExpected, U_PITT_DE_CLIENT_ACCOUNT_ID, false);
		assertCashBalanceForClientAccountEquals(pittExpected, U_PITT_DE_CLIENT_ACCOUNT_ID, true);

		assertCashBalanceForClientAccountEquals(new BigDecimal("15130127.33"), SONOMA_DE_CLIENT_ACCOUNT_ID, false);
		assertCashBalanceForClientAccountEquals(new BigDecimal("9096127.33"), SONOMA_DE_CLIENT_ACCOUNT_ID, true);
	}


	@Test
	public void testGetCashBalanceForHoldingAccount() {
		BigDecimal pittExpected = new BigDecimal("2879.16");
		assertCashBalanceForHoldingAccountEquals(pittExpected, U_PITT_CUSTODIAN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForHoldingAccountEquals(pittExpected, U_PITT_CUSTODIAN_HOLDING_ACCOUNT_ID, true);

		assertCashBalanceForHoldingAccountEquals(new BigDecimal("9830127.33"), SONOMA_CUSTODIAN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForHoldingAccountEquals(new BigDecimal("9096127.33"), SONOMA_CUSTODIAN_HOLDING_ACCOUNT_ID, true);

		assertCashBalanceForHoldingAccountEquals(new BigDecimal("5300000.00"), SONOMA_MARGIN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForHoldingAccountEquals(BigDecimal.ZERO, SONOMA_MARGIN_HOLDING_ACCOUNT_ID, true);
	}


	@Test
	public void testGetCashBalanceForClientAndHoldingAccount() {
		BigDecimal pittExpected = new BigDecimal("2879.16");
		assertCashBalanceForClientAndHoldingAccountEquals(pittExpected, U_PITT_DE_CLIENT_ACCOUNT_ID, U_PITT_CUSTODIAN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForClientAndHoldingAccountEquals(pittExpected, U_PITT_DE_CLIENT_ACCOUNT_ID, U_PITT_CUSTODIAN_HOLDING_ACCOUNT_ID, true);

		assertCashBalanceForClientAndHoldingAccountEquals(new BigDecimal("9830127.33"), SONOMA_DE_CLIENT_ACCOUNT_ID, SONOMA_CUSTODIAN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForClientAndHoldingAccountEquals(new BigDecimal("9096127.33"), SONOMA_DE_CLIENT_ACCOUNT_ID, SONOMA_CUSTODIAN_HOLDING_ACCOUNT_ID, true);

		assertCashBalanceForClientAndHoldingAccountEquals(new BigDecimal("5300000.00"), SONOMA_DE_CLIENT_ACCOUNT_ID, SONOMA_MARGIN_HOLDING_ACCOUNT_ID, false);
		assertCashBalanceForClientAndHoldingAccountEquals(BigDecimal.ZERO, SONOMA_DE_CLIENT_ACCOUNT_ID, SONOMA_MARGIN_HOLDING_ACCOUNT_ID, true);
	}


	@Test
	public void testGetCollateralBalanceForClientAndHoldingAccount() {

		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountAndHoldingAccount(SONOMA_DE_CLIENT_ACCOUNT_ID, SONOMA_MARGIN_HOLDING_ACCOUNT_ID)
				.withTransactionDate(DateUtils.toDate("10/08/2018"))
				.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);

		BigDecimal collateralBalance = this.accountingBalanceService.getAccountingBalance(command);
		Assertions.assertEquals(new BigDecimal("5300000.00"), collateralBalance);

		Short[] cashCollateralAccountingAccounts = this.accountingAccountIdsCache.getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);
		BigDecimal cashCollateralBalance = ArrayUtils.getStream(cashCollateralAccountingAccounts)
				.map(accountingAccountId -> {
					AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
					searchForm.setAccountingAccountId(accountingAccountId);
					searchForm.setClientAccountId(SONOMA_DE_CLIENT_ACCOUNT_ID);
					searchForm.setHoldingAccountId(SONOMA_MARGIN_HOLDING_ACCOUNT_ID);
					searchForm.setTransactionDate(DATE_10_08_2018);
					searchForm.setSettlementDate(DateUtils.addDays(DATE_10_08_2018, -1));
					return this.accountingBalanceService.getAccountingAccountBalanceBase(searchForm);
				}).reduce(BigDecimal.ZERO, MathUtils::add);

		Assertions.assertEquals(collateralBalance, cashCollateralBalance);
	}


	@Test
	public void testGetAccountingCashBalanceMap() {
		InvestmentAccountGroup spx20dClientAccountGroup = this.investmentAccountGroupService.getInvestmentAccountGroupByName(SPX_20D_CLIENT_ACCOUNT_GROUP_NAME);
		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountGroupOnTransactionDate(spx20dClientAccountGroup.getId(), DATE_10_08_2018)
				.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS);

		// Investment Account with ID 4616 is no longer in SPX-20D account group so it does not show up in the results below
		Map<Integer, BigDecimal> clientAccountBalanceMapByGroup = this.accountingBalanceService.getAccountingCashBalanceMap(command);
		Assertions.assertEquals(1, clientAccountBalanceMapByGroup.size());
		command = AccountingBalanceCommand.forClientAccountsOnTransactionDate(ArrayUtils.toIntegerArray(BFOK_DE_CLIENT_ACCOUNT_ID, SONOMA_DE_CLIENT_ACCOUNT_ID), DATE_10_08_2018)
				.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS);
		Map<Integer, BigDecimal> clientAccountBalanceMapByIds = this.accountingBalanceService.getAccountingCashBalanceMap(command);
		Assertions.assertEquals(1, clientAccountBalanceMapByGroup.size());

		clientAccountBalanceMapByGroup.forEach((clientAccountId, cashBalance) -> Assertions.assertEquals(cashBalance, clientAccountBalanceMapByIds.get(clientAccountId)));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void assertCashBalanceForClientAccountEquals(BigDecimal expected, Integer clientAccountId, boolean excludeCollateral) {
		BigDecimal balance = getCashBalanceForClientAndHoldingAccountId(clientAccountId, null, excludeCollateral);
		Assertions.assertEquals(expected, balance);
	}


	private void assertCashBalanceForHoldingAccountEquals(BigDecimal expected, Integer holdingAccountId, boolean excludeCollateral) {
		BigDecimal balance = getCashBalanceForClientAndHoldingAccountId(null, holdingAccountId, excludeCollateral);
		Assertions.assertEquals(expected, balance);
	}


	private void assertCashBalanceForClientAndHoldingAccountEquals(BigDecimal expected, Integer clientAccountId, Integer holdingAccountId, boolean excludeCollateral) {
		BigDecimal balance = getCashBalanceForClientAndHoldingAccountId(clientAccountId, holdingAccountId, excludeCollateral);
		Assertions.assertEquals(expected, balance);
	}


	private BigDecimal getCashBalanceForClientAndHoldingAccountId(Integer clientAccountId, Integer holdingAccountId, boolean excludeCollateral) {
		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountAndHoldingAccount(clientAccountId, holdingAccountId)
				.withTransactionDate(DATE_10_08_2018)
				.withExcludeCashCollateral(excludeCollateral)
				.withNetOnly(true);
		return this.accountingBalanceService.getAccountingBalance(command);
	}
}
