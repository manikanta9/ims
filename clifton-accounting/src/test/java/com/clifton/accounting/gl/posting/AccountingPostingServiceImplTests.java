package com.clifton.accounting.gl.posting;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalServiceImpl;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author vgomelsky
 */
@ContextConfiguration(locations = "../journal/AccountingJournalServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class AccountingPostingServiceImplTests {

	@Resource
	private AccountingPostingServiceImpl accountingPostingService;
	@Resource
	private AccountingJournalServiceImpl accountingJournalService;


	public static final long BOOKED_JOURNAL = 5;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPostAndUnpostAccountingJournal() {
		AccountingJournal journal = this.accountingJournalService.getAccountingJournal(BOOKED_JOURNAL);
		Assertions.assertNotNull(journal);
		Assertions.assertNull(journal.getPostingDate(), "Posting date cannot be set before posting.");
		Assertions.assertEquals(2, journal.getJournalDetailList().size(), "The journal has two detail items before posting.");

		// mock the method that triggers rebuild of DW
		AdvancedUpdatableDAO<AccountingJournal, Criteria> accountingJournalDAO = Mockito.spy(this.accountingJournalService.getAccountingJournalDAO());
		Mockito.doNothing().when(accountingJournalDAO).executeNamedQuery(ArgumentMatchers.anyString(), ArgumentMatchers.any());
		this.accountingJournalService.setAccountingJournalDAO(accountingJournalDAO);

		// Post the journal
		this.accountingPostingService.postAccountingJournal(BOOKED_JOURNAL);
		journal = this.accountingJournalService.getAccountingJournal(BOOKED_JOURNAL);
		Assertions.assertNotNull(journal);

		Date postingDate = journal.getPostingDate();
		Assertions.assertNotNull(postingDate, "After posting, posting date on the journal should be set.");
		Assertions.assertEquals(AccountingJournalStatus.STATUS_ORIGINAL, journal.getJournalStatus().getName(), "After initial posting, the journal should be in Original");

		Assertions.assertTrue(journal.getJournalDetailList().get(0) instanceof AccountingTransaction, "There should be no journal details after postings (moved to transaction)");

		this.accountingPostingService.setSecurityAuthorizationService(Mockito.mock(SecurityAuthorizationService.class));
		Mockito.when(this.accountingPostingService.getSecurityAuthorizationService().isSecurityAccessAllowed("AccountingTransaction", SecurityPermission.PERMISSION_DELETE)).thenReturn(true);

		// Unpost the journal
		AccountingJournal newJournal = this.accountingPostingService.unpostAccountingJournal(journal.getId());

		journal = this.accountingJournalService.getAccountingJournal(BOOKED_JOURNAL);
		Assertions.assertNotNull(journal);
		Assertions.assertNotEquals(journal.getId(), newJournal.getId(), "A copy of Deleted journal must be created.");
		Assertions.assertNotNull(journal.getPostingDate(), "Posting Date must stay on Deleted journal.");
		Assertions.assertNull(newJournal.getPostingDate(), "Posting Date must be clear after unposting.");
		Assertions.assertEquals(AccountingJournalStatus.STATUS_UNPOSTED, newJournal.getJournalStatus().getName(), "After unposting, the new journal should be in Unposted");
		Assertions.assertEquals(AccountingJournalStatus.STATUS_DELETED, journal.getJournalStatus().getName(), "After unposting, the journal should be in Deleted");

		//Repost the journal
		this.accountingPostingService.postAccountingJournal(newJournal.getId());
		journal = this.accountingJournalService.getAccountingJournal(newJournal.getId());
		Assertions.assertNotNull(journal);

		postingDate = journal.getPostingDate();
		Assertions.assertNotNull(postingDate, "After posting, posting date on the journal should be set.");
		Assertions.assertEquals(AccountingJournalStatus.STATUS_UNCHANGED, journal.getJournalStatus().getName(), "After re-posting, the journal should be in Unchanged");
	}
}
