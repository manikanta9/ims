package com.clifton.accounting.gl.position.closing;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author mwacker
 */
public class AccountingPositionClosingServiceTest {

	@Mock
	private AccountingPositionService accountingPositionService;

	@Mock
	private AccountingPositionHandler accountingPositionHandler;

	@Mock
	private InvestmentInstrumentService investmentInstrumentService;

	private AccountingPositionClosingService accountingPositionClosingService;

	private static InvestmentSecurity BOND_912796FP9;
	private static InvestmentSecurity BOND_912796GD5;
	private static InvestmentSecurity BOND_912796GD5_2;


	static {
		BOND_912796FP9 = InvestmentTestObjectFactory.newInvestmentSecurity(27704, "912796FP9", "Bonds");
		BOND_912796GD5 = InvestmentTestObjectFactory.newInvestmentSecurity(30568, "912796GD5", "Bonds");
		BOND_912796GD5_2 = InvestmentTestObjectFactory.newInvestmentSecurity(305682, "912796GD5_2", "Bonds");
	}


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);

		final List<AccountingPosition> positionList = buildPositionList();

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			AccountingPositionClosingCommand command = (AccountingPositionClosingCommand) args[0];
			return BeanUtils.filter(positionList, (AccountingPosition a) -> a.getInvestmentSecurity().getId(), command.getInvestmentSecurityId());
		}).when(this.accountingPositionService).getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionClosingCommand.class));

		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(ArgumentMatchers.eq(305682))).thenReturn(BOND_912796GD5_2);
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(ArgumentMatchers.eq(30568))).thenReturn(BOND_912796GD5);
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(ArgumentMatchers.eq(27704))).thenReturn(BOND_912796FP9);

		InvestmentCalculatorImpl investmentCalculator = new InvestmentCalculatorImpl();

		this.accountingPositionClosingService = new AccountingPositionClosingServiceImpl();
		((AccountingPositionClosingServiceImpl) this.accountingPositionClosingService).setAccountingPositionHandler(this.accountingPositionHandler);
		((AccountingPositionClosingServiceImpl) this.accountingPositionClosingService).setAccountingPositionService(this.accountingPositionService);
		((AccountingPositionClosingServiceImpl) this.accountingPositionClosingService).setInvestmentCalculator(investmentCalculator);
	}


	@Test
	public void testAccountingPositionClosingLotList_RoundError() {
		AccountingPositionClosingCommand command = new AccountingPositionClosingCommand();
		command.setInvestmentSecurityId(BOND_912796GD5_2.getId());

		AccountingPositionClosingSecurityQuantity securityQuantity = new AccountingPositionClosingSecurityQuantity();
		securityQuantity.setInvestmentSecurityId(BOND_912796GD5_2.getId());
		securityQuantity.setQuantity(new BigDecimal("295000"));

		command.setSecurityQuantityList(Collections.singletonList(securityQuantity));

		List<AccountingPositionClosing> closingLotList = this.accountingPositionClosingService.getAccountingPositionClosingLotList(command);
		Assertions.assertEquals(1, closingLotList.size());

		Assertions.assertEquals("{id=5229229, Symbol = 912796GD5_2, GL Account = Position, Price = 99.8045, Closing Price = 99.8045, Remaining Quantity = 295,000.00, Cost Basis = 294,423.27, Settlement Date = 02/19/2016}", toString(closingLotList.get(0)));
	}


	@Test
	public void testAccountingPositionClosingLotList() {
		AccountingPositionClosingCommand command = new AccountingPositionClosingCommand();
		command.setInvestmentSecurityId(BOND_912796FP9.getId());

		AccountingPositionClosingSecurityQuantity securityQuantity = new AccountingPositionClosingSecurityQuantity();
		securityQuantity.setInvestmentSecurityId(BOND_912796FP9.getId());
		securityQuantity.setQuantity(new BigDecimal("1000000"));

		command.setSecurityQuantityList(Collections.singletonList(securityQuantity));

		List<AccountingPositionClosing> closingLotList = this.accountingPositionClosingService.getAccountingPositionClosingLotList(command);
		Assertions.assertEquals(6, closingLotList.size());


		Assertions.assertEquals("{id=482062, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 384,000.00, Cost Basis = 383,442.69, Settlement Date = 10/07/2015}", toString(closingLotList.get(0)));
		Assertions.assertEquals("{id=4826058, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 78,000.00, Cost Basis = 77,886.80, Settlement Date = 10/07/2015}", toString(closingLotList.get(1)));
		Assertions.assertEquals("{id=4826054, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 155,000.00, Cost Basis = 154,775.04, Settlement Date = 10/07/2015}", toString(closingLotList.get(2)));
		Assertions.assertEquals("{id=4826050, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 240,000.00, Cost Basis = 239,651.68, Settlement Date = 10/07/2015}", toString(closingLotList.get(3)));
		Assertions.assertEquals("{id=4823780, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 30,000.00, Cost Basis = 29,956.46, Settlement Date = 10/06/2015}", toString(closingLotList.get(4)));
		Assertions.assertEquals("{id=4823776, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 113,000.00, Cost Basis = 112,836.00, Settlement Date = 10/06/2015}", toString(closingLotList.get(5)));

		List<AccountingPositionClosingTransaction> selectedTransactionList = new ArrayList<>();
		for (AccountingPositionClosing closingLot : CollectionUtils.getIterable(closingLotList)) {
			AccountingPositionClosingTransaction transaction = new AccountingPositionClosingTransaction();
			transaction.setTransactionId(closingLot.getId());
			transaction.setClosingPrice(closingLot.getPrice());
			transaction.setClosingQuantity(closingLot.getRemainingQuantity());
			selectedTransactionList.add(transaction);
		}
		command.setSelectedPositionList(selectedTransactionList);
		securityQuantity.setQuantity(new BigDecimal("1000"));

		closingLotList = this.accountingPositionClosingService.getAccountingPositionClosingLotList(command);
		Assertions.assertEquals(6, closingLotList.size());


		Assertions.assertEquals("{id=482062, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 384,000.00, Cost Basis = 383,442.69, Settlement Date = 10/07/2015}", toString(closingLotList.get(0)));
		Assertions.assertEquals("{id=4826058, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 78,000.00, Cost Basis = 77,886.80, Settlement Date = 10/07/2015}", toString(closingLotList.get(1)));
		Assertions.assertEquals("{id=4826054, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 155,000.00, Cost Basis = 154,775.04, Settlement Date = 10/07/2015}", toString(closingLotList.get(2)));
		Assertions.assertEquals("{id=4826050, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 240,000.00, Cost Basis = 239,651.68, Settlement Date = 10/07/2015}", toString(closingLotList.get(3)));
		Assertions.assertEquals("{id=4823780, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 30,000.00, Cost Basis = 29,956.46, Settlement Date = 10/06/2015}", toString(closingLotList.get(4)));
		Assertions.assertEquals("{id=4823776, Symbol = 912796FP9, GL Account = Position, Price = 99.854866667, Closing Price = 99.854866667, Remaining Quantity = 114,000.00, Cost Basis = 113,834.55, Settlement Date = 10/06/2015}", toString(closingLotList.get(5)));
	}


	public String toString(AccountingPositionClosing closingLot) {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(closingLot.getId());
		if (closingLot.getInvestmentSecurity() != null) {
			result.append(", Symbol = ");
			result.append(closingLot.getInvestmentSecurity().getSymbol());
		}
		if (closingLot.getAccountingAccount() != null) {
			result.append(", GL Account = ");
			result.append(closingLot.getAccountingAccount().getName());
		}
		result.append(", Price = ");
		result.append(CoreMathUtils.formatNumberDecimal(closingLot.getPrice()));
		result.append(", Closing Price = ");
		result.append(CoreMathUtils.formatNumberDecimal(closingLot.getClosingPrice()));
		result.append(", Remaining Quantity = ");
		result.append(CoreMathUtils.formatNumberMoney(closingLot.getRemainingQuantity()));
		result.append(", Cost Basis = ");
		result.append(CoreMathUtils.formatNumberMoney(closingLot.getRemainingCostBasis()));
		result.append(", Settlement Date = ");
		result.append(DateUtils.fromDateShort(closingLot.getSettlementDate()));
		result.append('}');
		return result.toString();
	}


	private List<AccountingPosition> buildPositionList() {

		List<AccountingPosition> result = new ArrayList<>();
		result.add(AccountingTestObjectFactory.newAccountingPosition(482062, BOND_912796FP9, new BigDecimal("384000"), new BigDecimal("99.854866667"), new BigDecimal("383442.69"), BigDecimal.ONE, DateUtils.toDate("10/07/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4826058, BOND_912796FP9, new BigDecimal("78000"), new BigDecimal("99.854866667"), new BigDecimal("77886.80"), BigDecimal.ONE, DateUtils.toDate("10/07/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4826054, BOND_912796FP9, new BigDecimal("155000"), new BigDecimal("99.854866667"), new BigDecimal("154775.04"), BigDecimal.ONE, DateUtils.toDate("10/07/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4826050, BOND_912796FP9, new BigDecimal("240000"), new BigDecimal("99.854866667"), new BigDecimal("239651.68"), BigDecimal.ONE, DateUtils.toDate("10/07/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4823780, BOND_912796FP9, new BigDecimal("30000"), new BigDecimal("99.854866667"), new BigDecimal("29956.46"), BigDecimal.ONE, DateUtils.toDate("10/06/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4823776, BOND_912796FP9, new BigDecimal("1275000"), new BigDecimal("99.854866667"), new BigDecimal("1273149.55"), BigDecimal.ONE, DateUtils.toDate("10/06/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4823772, BOND_912796FP9, new BigDecimal("807000"), new BigDecimal("99.854866667"), new BigDecimal("805828.77"), BigDecimal.ONE, DateUtils.toDate("10/06/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4821366, BOND_912796FP9, new BigDecimal("154000"), new BigDecimal("99.854866667"), new BigDecimal("153776.49"), BigDecimal.ONE, DateUtils.toDate("10/06/2015")));
		result.add(AccountingTestObjectFactory.newAccountingPosition(4821362, BOND_912796FP9, new BigDecimal("205000"), new BigDecimal("99.854866667"), new BigDecimal("204702.48"), BigDecimal.ONE, DateUtils.toDate("10/06/2015")));


		result.add(AccountingTestObjectFactory.newAccountingPosition(5229229, BOND_912796GD5_2, new BigDecimal("295000"), new BigDecimal("99.8045"), new BigDecimal("294423.27"), BigDecimal.ONE, DateUtils.toDate("02/19/2016")));

		result.add(AccountingTestObjectFactory.newAccountingPosition(4286000, BOND_912796GD5, new BigDecimal("25000000"), new BigDecimal("99.8045"), new BigDecimal("24951125.00"), BigDecimal.ONE, DateUtils.toDate("04/21/2015")));

		return result;
	}
}
