package com.clifton.accounting.gl.booking.rule;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetailBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferTypeBuilder;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemTableBuilder;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
class AccountingPositionCollateralOffsetsBookingRuleTest {

	@Test
	public void testPartialClose() {
		AccountingJournal journal = new AccountingJournal();
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(3, "912796TV1", InvestmentType.BONDS);

		// accounting transactions
		List<AccountingTransaction> accountingTransactionList = createAccountingTransactions(security);

		// journal details
		AccountingJournalDetail detail = AccountingTestObjectFactory.newAccountingJournalDetail(
				security,
				null,
				null,
				new BigDecimal("0"),
				new BigDecimal("15422500.00"),
				new BigDecimal("1.0000000000000000"),
				DateUtils.toDate("11/18/2019"),
				AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_CASH_COLLATERAL).asCash().asCurrency().asCollateral().build()
		);
		journal.addJournalDetail(detail);

		detail = AccountingTestObjectFactory.newAccountingJournalDetail(
				security,
				new BigDecimal("-15500000.0000000000"),
				new BigDecimal("99.500000000000000"),
				new BigDecimal("-15422500.00"),
				new BigDecimal("-15422500.00"),
				new BigDecimal("1.0000000000000000"),
				DateUtils.toDate("11/18/2019"),
				AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().build()
		);
		detail.setParentTransaction(CollectionUtils.getOnlyElementStrict(accountingTransactionList.stream().filter(t -> t.getId().equals(16344153L)).collect(Collectors.toList())));
		journal.addJournalDetail(detail);

		// accounting position transfer
		AccountingPositionTransferDetail accountingPositionTransferDetail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withPositionTransfer(AccountingPositionTransferBuilder
						.createEmpty()
						.withId(49913)
						.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
						.toAccountPositionTransfer()
				)
				.withId(334464)
				.toAccountingPositionTransferDetail();

		// book
		// execute and test booking rules.
		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						AccountingTestObjectFactory.newAccountingPositionCollateralOffsetsBookingRule(
								accountingPositionTransferDetail,
								accountingTransactionList.toArray(new AccountingTransaction[0])
						)
				)
				.withExpectedResults(
						// ID, ParentID, ClientAccountNumber, HoldingAccountNumber, AccountingAccountName, Symbol, price, quantity, localdebitcredit, transactionDate, fxrate, basedebitcredit, positionCostbasis, originalDate, settlementDate, description
						"-10	null	100000	H-200	Cash Collateral	912796TV1			15,422,500	11/18/2019	1	15,422,500	0	11/18/2019	11/18/2019	null",
						"-11	16344153	100000	H-200	Position Collateral	912796TV1	99.5	-15,500,000	-15,422,500	11/18/2019	1	-15,422,500	-15,422,500	11/18/2019	11/18/2019	null",
						"-12	16344154	100000	H-200	Position Collateral Receivable	912796TV1	99.2438166667	-15,500,000	-15,382,791.59	11/18/2019	1	-15,382,791.59	-15,382,791.59	null	11/18/2019	[Position Collateral Receivable] offset for [912796TV1]",
						"-13	16344155	100000	H-200	Position Collateral Payable	912796TV1	99.2438166667	15,500,000	15,382,791.59	11/18/2019	1	15,382,791.59	15,382,791.59	null	11/18/2019	[Position Collateral Payable] offset for [912796TV1]"
				)
				.execute();
	}


	@Test
	public void testFullClose() {
		AccountingJournal journal = new AccountingJournal();
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(3, "912796TV1", InvestmentType.BONDS);

		// accounting transactions
		List<AccountingTransaction> accountingTransactionList = createAccountingTransactions(security);

		// journal details
		AccountingJournalDetail detail = AccountingTestObjectFactory.newAccountingJournalDetail(
				security,
				null,
				null,
				new BigDecimal("0"),
				new BigDecimal("30845000.00"),
				new BigDecimal("1.0000000000000000"),
				DateUtils.toDate("11/18/2019"),
				AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_CASH_COLLATERAL).asCash().asCurrency().asCollateral().build()
		);
		journal.addJournalDetail(detail);

		detail = AccountingTestObjectFactory.newAccountingJournalDetail(
				security,
				new BigDecimal("-31000000.0000000000"),
				new BigDecimal("99.500000000000000"),
				new BigDecimal("-30845000.00"),
				new BigDecimal("-30845000.00"),
				new BigDecimal("1.0000000000000000"),
				DateUtils.toDate("11/18/2019"),
				AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().build()
		);
		detail.setParentTransaction(CollectionUtils.getOnlyElementStrict(accountingTransactionList.stream().filter(t -> t.getId().equals(16344153L)).collect(Collectors.toList())));
		journal.addJournalDetail(detail);

		// accounting position transfer
		AccountingPositionTransferDetail accountingPositionTransferDetail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withPositionTransfer(AccountingPositionTransferBuilder
						.createEmpty()
						.withId(49913)
						.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
						.toAccountPositionTransfer()
				)
				.withId(334464)
				.toAccountingPositionTransferDetail();

		// execute and test booking rules.
		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						AccountingTestObjectFactory.newAccountingPositionCollateralOffsetsBookingRule(
								accountingPositionTransferDetail,
								accountingTransactionList.toArray(new AccountingTransaction[0])
						)
				)
				.withExpectedResults(
						// ID, ParentID, ClientAccountNumber, HoldingAccountNumber, AccountingAccountName, Symbol, price, quantity, localdebitcredit, transactionDate, fxrate, basedebitcredit, positionCostbasis, originalDate, settlementDate, description
						"-10	null	100000	H-200	Cash Collateral	912796TV1			30,845,000	11/18/2019	1	30,845,000	0	11/18/2019	11/18/2019	null",
						"-11	16344153	100000	H-200	Position Collateral	912796TV1	99.5	-31,000,000	-30,845,000	11/18/2019	1	-30,845,000	-30,845,000	11/18/2019	11/18/2019	null",
						"-12	16344154	100000	H-200	Position Collateral Receivable	912796TV1	99.2438166667	-31,000,000	-30,765,583.17	11/18/2019	1	-30,765,583.17	-30,765,583.17	null	11/18/2019	[Position Collateral Receivable] offset for [912796TV1]",
						"-13	16344155	100000	H-200	Position Collateral Payable	912796TV1	99.2438166667	31,000,000	30,765,583.17	11/18/2019	1	30,765,583.17	30,765,583.17	null	11/18/2019	[Position Collateral Payable] offset for [912796TV1]"
				)
				.execute();
	}


	private List<AccountingTransaction> createAccountingTransactions(InvestmentSecurity security) {
		final List<AccountingTransaction> accountingTransactionList = new ArrayList<>();

		AccountingTransaction parentTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16342599)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION).asPosition().withId(120).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		parentTransaction.setDescription("Positions in a security instrument");
		parentTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		parentTransaction.setFkFieldId(333709);
		accountingTransactionList.add(parentTransaction);

		AccountingTransaction accountingTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344153)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().withId(753).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		accountingTransaction.setParentTransaction(parentTransaction);
		accountingTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		accountingTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		accountingTransaction.setFkFieldId(334464);
		accountingTransactionList.add(accountingTransaction);

		AccountingTransaction offsetTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344154)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE).asPosition().asCollateral().withId(800).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		offsetTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setParentTransaction(accountingTransaction);
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setFkFieldId(334464);
		accountingTransactionList.add(offsetTransaction);

		offsetTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344155)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.POSITION_COLLATERAL_PAYABLE).asPosition().asCollateral().asExecutingBrokerSpecific().withId(800).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("-31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("-30765583.17")
				.build();
		offsetTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setParentTransaction(accountingTransaction);
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setFkFieldId(334464);
		accountingTransactionList.add(offsetTransaction);

		return accountingTransactionList;
	}
}
