package com.clifton.accounting.gl.booking.rule;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class AccountingAggregateCurrencyBookingRuleTests {

	@Test
	public void testAggregateMultipleCurrenciesAtDifferentFxRates() {
		AccountingJournal journal = createTestJournalWithJpyCurrency();
		for (int i = 2; i < journal.getJournalDetailList().size(); i++) {
			AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(i);
			detail.setExchangeRateToBase(new BigDecimal("0.00834097"));
			detail.setBaseDebitCredit(InvestmentCalculatorUtils.roundBaseAmount(MathUtils.multiply(detail.getLocalDebitCredit(), detail.getExchangeRateToBase()), detail.getClientInvestmentAccount()));
		}

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-14	null	100000	H-200	Currency	JPY			-7,665,350	10/05/2015	0.00831427	-63,731.79	0	10/05/2015	10/05/2015	null",
						"-15	null	100000	H-200	Currency	JPY			-11,712,600	10/05/2015	0.00834097	-97,694.45	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrenciesAtDifferentSettlementDates() {
		AccountingJournal journal = createTestJournalWithJpyCurrency();
		journal.getJournalDetailList().get(0).setSettlementDate(DateUtils.toDate("10/06/2015"));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	JPY			-7,623,000	10/05/2015	0.00831427	-63,379.68	0	10/05/2015	10/06/2015	null",
						"-11	null	100000	H-200	Currency	JPY			-11,754,950	10/05/2015	0.00831427	-97,733.83	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrencies() {
		AccountingBookingRuleTestExecutor.newTestForJournal(createTestJournalWithJpyCurrency())
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	JPY			-19,377,950	10/05/2015	0.00831427	-161,113.51	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrenciesWithDifferentParentTransactions() {
		AccountingJournal journal = createTestJournalWithJpyCurrency();
		InvestmentSecurity ibm = InvestmentTestObjectFactory.newInvestmentSecurity(1, "IBM", InvestmentType.STOCKS);

		Date transactionDate = DateUtils.toDate("12/09/2019");
		for (int i = 0; i < journal.getJournalDetailList().size(); i++) {
			AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(i);
			AccountingTransaction parentTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(i + 1, ibm)
					.qty(95).price("118.258").exchangeRateToBase(detail.getExchangeRateToBase()).on(DateUtils.addDays(transactionDate, i)).build();
			detail.setParentTransaction(parentTransaction);
			// detail.setOriginalTransactionDate(parentTransaction.getOriginalTransactionDate());
		}

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Currency	JPY			-7,623,000	10/05/2015	0.00831427	-63,379.68	0	10/05/2015	10/05/2015	null",
						"-11	2	100000	H-200	Currency	JPY			-42,350	10/05/2015	0.00831427	-352.11	0	10/05/2015	10/05/2015	null",
						"-12	3	100000	H-200	Currency	JPY			-11,700,000	10/05/2015	0.00831427	-97,276.96	0	10/05/2015	10/05/2015	null",
						"-13	4	100000	H-200	Currency	JPY			-12,600	10/05/2015	0.00831427	-104.76	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrenciesWithSameParentTransactions() {
		AccountingJournal journal = new AccountingJournal();
		InvestmentSecurity ibm = InvestmentTestObjectFactory.newInvestmentSecurity(1, "IBM", InvestmentType.STOCKS);
		InvestmentSecurity jpy = InvestmentSecurityBuilder.newJPY();

		AccountingAccount account = AccountingTestObjectFactory.newAccountingAccount_Currency();
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-7623000"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-42350"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));

		Date transactionDate = DateUtils.toDate("12/09/2019");
		for (int i = 0; i < journal.getJournalDetailList().size(); i++) {
			AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(i);
			detail.setParentTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1L, ibm)
					.qty(95).price("118.258").exchangeRateToBase(detail.getExchangeRateToBase()).on(DateUtils.addDays(transactionDate, i)).build());
		}

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Currency	JPY			-7,665,350	10/05/2015	0.00831427	-63,731.79	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrenciesWithDifferentParentDefinitions() {
		AccountingJournal journal = createTestJournalWithJpyCurrency();
		InvestmentSecurity ibm = InvestmentTestObjectFactory.newInvestmentSecurity(1, "IBM", InvestmentType.STOCKS);

		Date transactionDate = DateUtils.toDate("12/09/2019");
		for (int i = 0; i < journal.getJournalDetailList().size(); i++) {
			AccountingJournalDetailDefinition detail = journal.getJournalDetailList().get(i);
			AccountingJournalDetailDefinition parentDetail = AccountingTestObjectFactory.newAccountingJournalDetail(ibm, 95, new BigDecimal("118.258"), detail.getPositionCostBasis(), detail.getExchangeRateToBase(), DateUtils.addDays(transactionDate, i));
			parentDetail.setId((long) i + 1);
			detail.setParentDefinition(parentDetail);
		}

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Currency	JPY			-7,623,000	10/05/2015	0.00831427	-63,379.68	0	10/05/2015	10/05/2015	null",
						"-11	2	100000	H-200	Currency	JPY			-42,350	10/05/2015	0.00831427	-352.11	0	10/05/2015	10/05/2015	null",
						"-12	3	100000	H-200	Currency	JPY			-11,700,000	10/05/2015	0.00831427	-97,276.96	0	10/05/2015	10/05/2015	null",
						"-13	4	100000	H-200	Currency	JPY			-12,600	10/05/2015	0.00831427	-104.76	0	10/05/2015	10/05/2015	null"
				).execute();
	}


	@Test
	public void testAggregateMultipleCurrenciesWithDifferentOriginalTransactionDates() {
		AccountingJournal journal = createTestJournalWithJpyCurrency();
		journal.getJournalDetailList().get(0).setOriginalTransactionDate(DateUtils.toDate("10/06/2015"));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRule(AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	JPY			-7,623,000	10/05/2015	0.00831427	-63,379.68	0	10/06/2015	10/05/2015	null",
						"-11	null	100000	H-200	Currency	JPY			-11,754,950	10/05/2015	0.00831427	-97,733.83	0	10/05/2015	10/05/2015	null"
				).execute();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingJournal createTestJournalWithJpyCurrency() {
		InvestmentSecurity jpy = InvestmentSecurityBuilder.newJPY();
		AccountingAccount account = AccountingTestObjectFactory.newAccountingAccount_Currency();

		AccountingJournal journal = new AccountingJournal();
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-7623000"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-42350"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-11700000"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(jpy, null, null, BigDecimal.ZERO, new BigDecimal("-12600"), new BigDecimal("0.00831427"), DateUtils.toDate("10/05/2015"), account));

		return journal;
	}
}
