package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class AccountingRealizedGainLossBookingRuleTests {

	@Test
	public void testPartialCloseOfOneLotInternationalFuture() {
		// start with existing positive lot of 59 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(16857, "PTM2", InvestmentType.FUTURES, "CAD");
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1745141L, security)
				.qty(59).price("707.95").costBasis("8353810.00").exchangeRateToBase("1.0112757243").on("03/12/2012").build();

		AccountingPositionSplitterBookingRule<BookableEntity> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		// partial close for 33 contracts
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -33, new BigDecimal("699.4848485"), new BigDecimal("-4616600.00"), new BigDecimal(
				"1.0121969735"), DateUtils.toDate("05/01/2012"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(positionSplitterRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						"-12	1745141	100000	H-200	Position	PTM2	699.4848485	-33	0	05/01/2012	1.0112757243	0	-4,672,470	03/12/2012	05/01/2012	Partial position close for PTM2",
						"-13	-12	100000	H-200	Realized Gain / Loss	PTM2	699.4848485	33	55,870	05/01/2012	1.0121969735	56,551.44	0	03/12/2012	05/01/2012	Realized Gain / Loss loss for PTM2",
						"-14	-12	100000	H-200	Currency	CAD			-55,870	05/01/2012	1.0121969735	-56,551.44	0	03/12/2012	05/01/2012	Currency expense from close of PTM2"
				)
				.execute();
	}


	@Test
	public void testPartialCloseOfOneLotInternationalFuture_OpeningCurrencyLotClosesExistingCurrencyToo() {
		// start with existing positive lot of 125 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(16857, "PTM2", InvestmentType.FUTURES, "CAD");
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1746289L, security)
				.qty(125).price("707.92").costBasis("17698000.00").exchangeRateToBase("1.0102030508").on("03/12/2012").build();
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1170302, security.getInstrument().getTradingCurrency())
				.qty(0).costBasis("17698000.00").exchangeRateToBase("1.0386373079").on("07/05/2011").build();
		AccountingPositionSplitterBookingRule<BookableEntity> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran, tran2);

		// partial close for 4 contracts
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -4, new BigDecimal("698.3"), new BigDecimal("-558640.00"), new BigDecimal(
				"1.0195758564"), DateUtils.toDate("04/30/2012"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(positionSplitterRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						"-12	1746289	100000	H-200	Position	PTM2	698.3	-4	0	04/30/2012	1.0102030508	0	-566,336	03/12/2012	04/30/2012	Partial position close for PTM2",
						"-13	-12	100000	H-200	Realized Gain / Loss	PTM2	698.3	4	7,696	04/30/2012	1.0195758564	7,846.66	0	03/12/2012	04/30/2012	Realized Gain / Loss loss for PTM2",
						"-14	-12	100000	H-200	Currency	CAD			-7,696	04/30/2012	1.0195758564	-7,846.66	0	03/12/2012	04/30/2012	Currency expense from close of PTM2"
				)
				.execute();
	}


	@Test
	public void testFullCloseOfTwoInternationalBondLots() {
		// start with existing positive lot of 125 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(17567, "DE0001030518", InvestmentType.BONDS, "EUR");
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1718842L, security)
				.qty(294000).price("103.448").costBasis("337440.13").exchangeRateToBase("1.3377").on("03/01/2012").build();
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1797503L, security)
				.qty(19000).price("104.05").costBasis("21813.67").exchangeRateToBase("1.3301").on("04/03/2012").build();

		AccountingPositionSplitterBookingRule<BookableEntity> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran1, tran2);

		// full close of 2 existing lots
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -313000, new BigDecimal("103.35"), new BigDecimal("-347467.56"), new BigDecimal(
				"1.3225"), DateUtils.toDate("05/02/2012"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(positionSplitterRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						"-12	1718842	100000	H-200	Position	DE0001030518	103.35	-294,000	-337,440.13	05/02/2012	1.3377	-451,393.66	-337,440.13	03/01/2012	05/02/2012	Full position close for DE0001030518",
						"-13	1797503	100000	H-200	Position	DE0001030518	103.35	-19,000	-21,813.67	05/02/2012	1.3301	-29,014.36	-21,813.67	04/03/2012	05/02/2012	Full position close for DE0001030518",
						"-14	-12	100000	H-200	Currency	EUR			326,375.28	05/02/2012	1.3225	431,631.31	0	03/01/2012	05/02/2012	Currency proceeds from close of DE0001030518",
						"-15	-13	100000	H-200	Currency	EUR			21,092.28	05/02/2012	1.3225	27,894.54	0	04/03/2012	05/02/2012	Currency proceeds from close of DE0001030518",
						"-16	-12	100000	H-200	Realized Gain / Loss	DE0001030518	103.35	294,000	11,064.85	05/02/2012	1.3225	14,633.26	0	03/01/2012	05/02/2012	Realized Gain / Loss loss for DE0001030518",
						"-17	-12	100000	H-200	Currency Translation Gain / Loss	DE0001030518			0	05/02/2012	1.3225	5,129.09	0	03/01/2012	05/02/2012	Currency Translation Gain / Loss for DE0001030518",
						"-18	-13	100000	H-200	Realized Gain / Loss	DE0001030518	103.35	19,000	721.39	05/02/2012	1.3225	954.04	0	04/03/2012	05/02/2012	Realized Gain / Loss loss for DE0001030518",
						"-19	-13	100000	H-200	Currency Translation Gain / Loss	DE0001030518			0	05/02/2012	1.3225	165.78	0	04/03/2012	05/02/2012	Currency Translation Gain / Loss for DE0001030518"
				)
				.execute();
	}
}
