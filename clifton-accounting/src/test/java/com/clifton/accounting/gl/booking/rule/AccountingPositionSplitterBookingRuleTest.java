package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubTypeBuilder;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AccountingPositionSplitterBookingRuleTest {

	private static final AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNoSplitNeededBecauseNoCurrentPositions() {
		AccountingJournal journal = new AccountingJournal();
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(3, "IBM", InvestmentType.STOCKS);
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(security, 20, new BigDecimal("120.55"), new BigDecimal("2401.10"), new Date()));

		SimpleBookingSession<BookableEntity> bookingSession = new SimpleBookingSession<>(journal, null);

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		bookingRule.applyRule(bookingSession);

		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(1, journalDetailList.size(), "No existing positions means no splitting");
		Assertions.assertTrue(journalDetailList.get(0).isOpening());
	}


	@Test
	public void testNoSplitNeededBecausePositionGoingSameDirection() {
		// start with existing positive lot
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(10, "ESM1", InvestmentType.FUTURES);
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, security).qty(211).price("108.7812500000").on("1/1/2010").build());

		// add a new lot to it: no splitting because there's no closing
		AccountingJournal journal = new AccountingJournal();
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(security, 5, new BigDecimal("115.55"), new BigDecimal("577.75"), new Date()));
		SimpleBookingSession<BookableEntity> bookingSession = new SimpleBookingSession<>(journal, null);

		bookingRule.applyRule(bookingSession);

		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(1, journalDetailList.size());
		Assertions.assertTrue(journalDetailList.get(0).isOpening());

		// now verify that position was actually returned: negative quantity must result in a split
		journal = new AccountingJournal();
		journal.addJournalDetail(AccountingTestObjectFactory.newAccountingJournalDetail(security, -5, new BigDecimal("115.55"), new BigDecimal("577.75"), new Date()));
		bookingSession = new SimpleBookingSession<>(journal, null);

		bookingRule.applyRule(bookingSession);

		journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(1, journalDetailList.size());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFullCloseOfOneLotDomesticStock_AndOpenRemainingQuantity() {
		// start with existing positive lot of 300 shares
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(9346, "XOM", InvestmentType.STOCKS);

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(11796L, security).qty(300).price("103.216").on("12/30/2009").build());

		// close 400 shares which must result in full close of 300 and a new lot of -100 open (also 2 cash entries)
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -400, new BigDecimal("10.50"), BigDecimal.valueOf(-4200.0), DateUtils.toDate("5/8/2012"));
		positionDetail.setPositionCommission(new BigDecimal("250.34"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(bookingRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 4 entries were created: full close of 300, cash, open of -100, cash
				.withExpectedResults(
						"-12	null	100000	H-200	Position	XOM	10.5	-100	-1,050	05/08/2012	1	-1,050	-1,050	05/08/2012	05/08/2012	Position opening of XOM",
						"-13	11796	100000	H-200	Position	XOM	10.5	-300	-3,150	05/08/2012	1	-3,150	-3,150	12/30/2009	05/08/2012	Full position close for XOM",
						"-14	-12	100000	H-200	Cash	USD			1,050	05/08/2012	1	1,050	0	05/08/2012	05/08/2012	Cash proceeds from opening of XOM",
						"-15	-13	100000	H-200	Cash	USD			3,150	05/08/2012	1	3,150	0	12/30/2009	05/08/2012	Cash proceeds from close of XOM"
				)
				.execute();
	}


	@Test
	public void testFullCloseOfPartiallyClosedOneLotAndFullCloseOfSecondLotForDomesticStock_AndOpenRemainingQuantity() {
		// start with existing positive lot of 300 shares remaining from 300 original
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(9346, "XOM", InvestmentType.STOCKS);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11796L, security).qty(300).price("103.216").on("12/30/2009").build();
		AccountingPosition openStock = AccountingTestObjectFactory.newAccountingPosition(tran1);
		openStock.setRemainingQuantity(new BigDecimal("30"));
		openStock.setRemainingCostBasis(MathUtils.multiply(openStock.getPositionCostBasis(), MathUtils.divide(openStock.getRemainingQuantity(), openStock.getQuantity()), 2));
		openStock.setRemainingBaseDebitCredit(openStock.getRemainingCostBasis());
		existingPositions.add(openStock);
		// and another lot of 100
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12797L, security).qty(100).price("50.55").on("12/30/2010").build();
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(tran2));

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran1, tran2);
		Mockito.when(bookingRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(100).forInvestmentSecurity(security.getId())
		))).thenReturn(existingPositions);

		// close 400 shares which must result in full close of 30 lot, 100 lot and a new lot of -270 open (also 3 cash entries)
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -400, new BigDecimal("10.50"), BigDecimal.valueOf(-4200.0), DateUtils.toDate("5/8/2012"));
		positionDetail.setPositionCommission(new BigDecimal("250.34"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(bookingRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				// verify that 4 entries were created: full close of 30 and 100 lots, cash, open of -270, cash
				.withExpectedResults(
						"-12	null	100000	H-200	Position	XOM	10.5	-270	-2,835	05/08/2012	1	-2,835	-2,835	05/08/2012	05/08/2012	Position opening of XOM",
						"-13	11796	100000	H-200	Position	XOM	10.5	-30	-315	05/08/2012	1	-315	-315	12/30/2009	05/08/2012	Full position close for XOM",
						"-14	12797	100000	H-200	Position	XOM	10.5	-100	-1,050	05/08/2012	1	-1,050	-1,050	12/30/2010	05/08/2012	Full position close for XOM",
						"-15	-12	100000	H-200	Cash	USD			2,835	05/08/2012	1	2,835	0	05/08/2012	05/08/2012	Cash proceeds from opening of XOM",
						"-16	-13	100000	H-200	Cash	USD			315	05/08/2012	1	315	0	12/30/2009	05/08/2012	Cash proceeds from close of XOM",
						"-17	-14	100000	H-200	Cash	USD			1,050	05/08/2012	1	1,050	0	12/30/2010	05/08/2012	Cash proceeds from close of XOM"
				)
				.execute();
	}


	@Test
	public void testPartialCloseOfOneLotInternationalFuture() {
		// start with existing positive lot of 59 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(16857, "PTM2", InvestmentType.FUTURES, "CAD");

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1745141L, security).qty(59).price("707.95").exchangeRateToBase("1.0112757243").on("03/12/2012").build());
		Assertions.assertEquals("com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule: lookupUsingSettlementDate=false, closingOrder=null", bookingRule.toString());

		// partial close for 33 contracts
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -33, new BigDecimal("699.4848485"), new BigDecimal("-4672470.00"), new BigDecimal("1.0121969735"), DateUtils.toDate("05/01/2012"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(bookingRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 1 entry was created: partial close
				.withExpectedResults(
						"-12	1745141	100000	H-200	Position	PTM2	699.4848485	-33	0	05/01/2012	1.0121969735	0	-4,672,470	03/12/2012	05/01/2012	Partial position close for PTM2")
				.execute();
	}


	@Test
	public void testFullCloseOfShortFutureLotAndOpenRemainder() {
		// start with existing short lot of -5 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(15980, "FVM2", InvestmentType.FUTURES, new BigDecimal(1000), "USD");

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1849049L, security).qty(-5).price("123.78125").on("04/30/2012").build());
		Assertions.assertEquals("com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule: lookupUsingSettlementDate=false, closingOrder=null", bookingRule.toString());

		// fully close -5 and open a long lot for 2 contracts
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, 7, new BigDecimal("124.1015625"), new BigDecimal("868710.92"), DateUtils.toDate("05/11/2012"));
		journal.addJournalDetail(positionDetail);

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 2 entries were created: full close of short position and long open
				.withExpectedResults(
						"-11	null	100000	H-200	Position	FVM2	124.1015625	2	0	05/11/2012	1	0	248,203.12	05/11/2012	05/11/2012	Position opening of FVM2",
						"-12	1849049	100000	H-200	Position	FVM2	124.1015625	5	0	05/11/2012	1	0	620,507.8	04/30/2012	05/11/2012	Full position close for FVM2"
				)
				.execute();
	}


	@Test
	public void testMultiLotPennyRounding() {
		InvestmentSecurity security = InvestmentSecurityBuilder
				.ofType("SPXW US 02/28/20 P3115", InvestmentType.OPTIONS)
				.withEndDate(DateUtils.toDate("02/28/2020"))
				.withInstrument(InvestmentInstrumentBuilder
						.createEmptyInvestmentInstrument()
						.withHierarchy(
								InvestmentInstrumentHierarchyBuilder
										.createEmptyInvestmentInstrumentHierarchy()
										.withInvestmentTypeSubType(
												InvestmentTypeSubTypeBuilder.createIndices()
														.withName("Options On Indices")
														.toInvestmentTypeSubType()
										)
										.toInvestmentInstrumentHierarchy()
						)
						.withPriceMultiplier(new BigDecimal("100"))
						.toInvestmentInstrument()
				)
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.withOptionStrikePrice(new BigDecimal("3115"))
				.build();

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("10"), new BigDecimal("0.764053"), new BigDecimal("764.05"), new BigDecimal("764.05"), new BigDecimal("1"), DateUtils.toDate("02/19/2020"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null, null, BigDecimal.ZERO, new BigDecimal("-764.05"), new BigDecimal("1"), DateUtils.toDate("02/19/2020"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		currency.setParentDefinition(position);
		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);

		position.setSettlementDate(DateUtils.toDate("02/20/2020"));
		currency.setSettlementDate(DateUtils.toDate("02/20/2020"));

		// positions will be ordered using FIFO before booking
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(16825466L, security).qty(-9).price("22.57")
						.localDebitCredit(new BigDecimal("20313.00"))
						.costBasis(new BigDecimal("-20313.00")).exchangeRateToBase("1").on("01/30/2020").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(16866534L, security).qty(-1).price("19.48")
						.localDebitCredit(new BigDecimal("1948.00"))
						.costBasis(new BigDecimal("-1948.00")).exchangeRateToBase("1").on("02/03/2020").build());

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule()
				)
				.withExpectedResults(
						"-12	16825466	100000	H-200	Position	SPXW US 02/28/20 P3115	0.764053	9	20,313	02/19/2020	1	20,313	20,313	01/30/2020	02/20/2020	Full position close for SPXW US 02/28/20 P3115",
						"-13	16866534	100000	H-200	Position	SPXW US 02/28/20 P3115	0.764053	1	1,948	02/19/2020	1	1,948	1,948	02/03/2020	02/20/2020	Full position close for SPXW US 02/28/20 P3115",
						"-14	-12	100000	H-200	Cash	USD			-687.64	02/19/2020	1	-687.64	0	01/30/2020	02/20/2020	Cash expense from close of SPXW US 02/28/20 P3115",
						"-15	-13	100000	H-200	Cash	USD			-76.41	02/19/2020	1	-76.41	0	02/03/2020	02/20/2020	Cash expense from close of SPXW US 02/28/20 P3115",
						"-16	-12	100000	H-200	Realized Gain / Loss	SPXW US 02/28/20 P3115	0.764053	9	-19,625.36	02/19/2020	1	-19,625.36	0	01/30/2020	02/20/2020	Realized Gain / Loss gain for SPXW US 02/28/20 P3115",
						"-17	-13	100000	H-200	Realized Gain / Loss	SPXW US 02/28/20 P3115	0.764053	1	-1,871.59	02/19/2020	1	-1,871.59	0	02/03/2020	02/20/2020	Realized Gain / Loss gain for SPXW US 02/28/20 P3115"

				)
				.execute();
	}


	@Test
	public void testMultiLotPennyRoundingReductionOfFace() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = new AccountingJournal();
		//(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase, Date date) {
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-8960000")
				, new BigDecimal("98.6875"), new BigDecimal("-8842400"), new BigDecimal("-8842400"), new BigDecimal("1"), DateUtils.toDate("02/13/2020"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null
				, null, BigDecimal.ZERO, new BigDecimal("8842400.00"), new BigDecimal("1"), DateUtils.toDate("02/13/2020"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		currency.setParentDefinition(position);

		AccountingJournalDetail position2 = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("8179204.01")
				, new BigDecimal("98.6875"), new BigDecimal("8071851.96"), new BigDecimal("8071851.96"), new BigDecimal("1"), DateUtils.toDate("02/13/2020"));
		AccountingJournalDetail currency2 = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null
				, null, BigDecimal.ZERO, new BigDecimal("-8071851.96"), new BigDecimal("1"), DateUtils.toDate("02/13/2020"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		position2.setParentDefinition(position);
		currency2.setParentDefinition(position2);

		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);
		journal.addJournalDetail(position2);
		journal.addJournalDetail(currency2);

		CollectionUtils.getIterable(journal.getJournalDetailList()).forEach(detail -> detail.setSettlementDate(DateUtils.toDate("02/19/2020")));

		// positions will be ordered using FIFO before booking
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByPosition(
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(6116235L, security).qty(8000000).price("99.1875")
						.localDebitCredit(new BigDecimal("7935000.00"))
						.costBasis(new BigDecimal("7935000.00")).exchangeRateToBase("1").on("10/27/2016").build(), new BigDecimal("697139.28"), new BigDecimal("691475.03"), new BigDecimal("691475.03")),

				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(7938553L, security).qty(960000).price("99.34375")
						.localDebitCredit(new BigDecimal("953700.00"))
						.costBasis(new BigDecimal("953700.00")).exchangeRateToBase("1").on("01/05/2018").build(), new BigDecimal("83656.71"), new BigDecimal("83107.67"), new BigDecimal("83107.67")));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule()
				)
				.withExpectedResults(
						"-15	6116235	100000	H-200	Position	3137AEG90	98.6875	-697,139.28	-691,475.03	02/13/2020	1	-691,475.03	-691,475.03	10/27/2016	02/19/2020	Full position close for 3137AEG90",
						"-16	7938553	100000	H-200	Position	3137AEG90	98.6875	-83,656.71	-83,107.67	02/13/2020	1	-83,107.67	-83,107.67	01/05/2018	02/19/2020	Full position close for 3137AEG90",
						"-17	-15	100000	H-200	Cash	USD			687,989.32	02/13/2020	1	687,989.32	0	10/27/2016	02/19/2020	Cash proceeds from close of 3137AEG90",
						"-18	-16	100000	H-200	Cash	USD			82,558.72	02/13/2020	1	82,558.72	0	01/05/2018	02/19/2020	Cash proceeds from close of 3137AEG90",
						"-19	-15	100000	H-200	Realized Gain / Loss	3137AEG90	98.6875	697,139.28	3,485.71	02/13/2020	1	3,485.71	0	10/27/2016	02/19/2020	Realized Gain / Loss loss for 3137AEG90",
						"-20	-16	100000	H-200	Realized Gain / Loss	3137AEG90	98.6875	83,656.71	548.95	02/13/2020	1	548.95	0	01/05/2018	02/19/2020	Realized Gain / Loss loss for 3137AEG90"

				)
				.execute();
	}


	@Test
	public void testMultiLotPennyRoundingWithMultipleLotsAndOneSame() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(9169, "912810PS1", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		AccountingJournal journal = new AccountingJournal();
		//(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal costBasis, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase, Date date) {
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-829000")
				, new BigDecimal("120.171872"), new BigDecimal("-1270246.42"), new BigDecimal("-1270246.42"), new BigDecimal("1"), DateUtils.toDate("03/05/2020"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null
				, null, BigDecimal.ZERO, new BigDecimal("1270246.42"), new BigDecimal("1"), DateUtils.toDate("03/05/2020"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		currency.setParentDefinition(position);


		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);


		CollectionUtils.getIterable(journal.getJournalDetailList()).forEach(detail -> detail.setSettlementDate(DateUtils.toDate("03/06/2020")));

		// positions will be ordered using FIFO before booking
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByPosition(
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(7905700L, security).qty(103000).price("116.6953125")
						.localDebitCredit(new BigDecimal("147025.16"))
						.costBasis(new BigDecimal("147025.16")).exchangeRateToBase("1").on("12/28/2017").build(), new BigDecimal("98400"), new BigDecimal("140458.99"), new BigDecimal("140458.99")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(7935766L, security).qty(16600).price("116.79296875")
						.localDebitCredit(new BigDecimal("23713.79"))
						.costBasis(new BigDecimal("23713.79")).exchangeRateToBase("1").on("01/04/2018").build(), new BigDecimal("16600"), new BigDecimal("23713.79"), new BigDecimal("23713.79")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(8035261L, security).qty(59000).price("115.30078125")
						.localDebitCredit(new BigDecimal("83208.47"))
						.costBasis(new BigDecimal("83208.47")).exchangeRateToBase("1").on("01/30/2018").build(), new BigDecimal("59000"), new BigDecimal("83208.47"), new BigDecimal("83208.47")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(8078242L, security).qty(193000).price("114.65625")
						.localDebitCredit(new BigDecimal("270631.25"))
						.costBasis(new BigDecimal("270631.25")).exchangeRateToBase("1").on("02/07/2018").build(), new BigDecimal("193000"), new BigDecimal("270631.25"), new BigDecimal("270631.25")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(8844681L, security).qty(7000).price("113.75")
						.localDebitCredit(new BigDecimal("9905.83"))
						.costBasis(new BigDecimal("9905.83")).exchangeRateToBase("1").on("07/10/2018").build(), new BigDecimal("7000"), new BigDecimal("9905.83"), new BigDecimal("9905.83")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(8952343L, security).qty(78000).price("112.26953125")
						.localDebitCredit(new BigDecimal("109260.51"))
						.costBasis(new BigDecimal("109260.51")).exchangeRateToBase("1").on("08/02/2018").build(), new BigDecimal("78000"), new BigDecimal("109260.51"), new BigDecimal("109260.51")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(9720776L, security).qty(7000).price("110.734375")
						.localDebitCredit(new BigDecimal("9708.02"))
						.costBasis(new BigDecimal("9708.02")).exchangeRateToBase("1").on("12/07/2018").build(), new BigDecimal("7000"), new BigDecimal("9708.02"), new BigDecimal("9708.02")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(13016328L, security).qty(370000).price("110.734375")
						.localDebitCredit(new BigDecimal("518184.43"))
						.costBasis(new BigDecimal("518184.43")).exchangeRateToBase("1").on("02/07/2019").build(), new BigDecimal("370000"), new BigDecimal("518184.43"), new BigDecimal("518184.43"))
		);

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-12	7905700	100000	H-200	Position	912810PS1	120.171872	-98,400	-150,774.73	03/05/2020	1	-150,774.73	-150,774.73	12/28/2017	03/06/2020	Full position close for 912810PS1",
						"-13	7935766	100000	H-200	Position	912810PS1	120.171872	-16,600	-25,435.57	03/05/2020	1	-25,435.57	-25,435.57	01/04/2018	03/06/2020	Full position close for 912810PS1",
						"-14	8035261	100000	H-200	Position	912810PS1	120.171872	-59,000	-90,403.54	03/05/2020	1	-90,403.54	-90,403.54	01/30/2018	03/06/2020	Full position close for 912810PS1",
						"-15	8078242	100000	H-200	Position	912810PS1	120.171872	-193,000	-295,726.85	03/05/2020	1	-295,726.85	-295,726.85	02/07/2018	03/06/2020	Full position close for 912810PS1",
						"-16	8844681	100000	H-200	Position	912810PS1	120.171872	-7,000	-10,725.84	03/05/2020	1	-10,725.84	-10,725.84	07/10/2018	03/06/2020	Full position close for 912810PS1",
						"-17	8952343	100000	H-200	Position	912810PS1	120.171872	-78,000	-119,516.55	03/05/2020	1	-119,516.55	-119,516.55	08/02/2018	03/06/2020	Full position close for 912810PS1",
						"-18	9720776	100000	H-200	Position	912810PS1	120.171872	-7,000	-10,725.85	03/05/2020	1	-10,725.85	-10,725.85	12/07/2018	03/06/2020	Full position close for 912810PS1",
						"-19	13016328	100000	H-200	Position	912810PS1	120.171872	-370,000	-566,937.49	03/05/2020	1	-566,937.49	-566,937.49	02/07/2019	03/06/2020	Full position close for 912810PS1",
						"-20	-12	100000	H-200	Cash	USD			150,774.73	03/05/2020	1	150,774.73	0	12/28/2017	03/06/2020	Cash proceeds from close of 912810PS1",
						"-21	-13	100000	H-200	Cash	USD			25,435.57	03/05/2020	1	25,435.57	0	01/04/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-22	-14	100000	H-200	Cash	USD			90,403.54	03/05/2020	1	90,403.54	0	01/30/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-23	-15	100000	H-200	Cash	USD			295,726.85	03/05/2020	1	295,726.85	0	02/07/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-24	-16	100000	H-200	Cash	USD			10,725.84	03/05/2020	1	10,725.84	0	07/10/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-25	-17	100000	H-200	Cash	USD			119,516.55	03/05/2020	1	119,516.55	0	08/02/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-26	-18	100000	H-200	Cash	USD			10,725.85	03/05/2020	1	10,725.85	0	12/07/2018	03/06/2020	Cash proceeds from close of 912810PS1",
						"-27	-19	100000	H-200	Cash	USD			566,937.49	03/05/2020	1	566,937.49	0	02/07/2019	03/06/2020	Cash proceeds from close of 912810PS1"


				)
				.execute();
	}


	@Test
	public void testCDSSwapMultiLotPennyRounding() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(120189, "XI3301D24U0100XXI", InvestmentType.SWAPS, new BigDecimal("0.01"), "USD");

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-58300000")
				, new BigDecimal("100.5206824633"), new BigDecimal("303557.88"), new BigDecimal("303557.88"), new BigDecimal("1"), DateUtils.toDate("03/06/2020"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null
				, null, BigDecimal.ZERO, new BigDecimal("-303557.88"), new BigDecimal("1"), DateUtils.toDate("03/06/2020"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		currency.setParentDefinition(position);

		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);

		CollectionUtils.getIterable(journal.getJournalDetailList()).forEach(detail -> detail.setSettlementDate(DateUtils.toDate("03/06/2020")));

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByPosition(
				AccountingTestObjectFactory.newAccountingPosition(
						AccountingTransactionBuilder.newTransactionForIdAndSecurity(17233652L, security).qty(10640000).price("102.62307699")
								.localDebitCredit(new BigDecimal("279095.39")).costBasis(new BigDecimal("279095.39")).exchangeRateToBase("1").on("01/02/2020").build(),
						new BigDecimal("10640000"), new BigDecimal("10640000"), new BigDecimal("10640000")),
				AccountingTestObjectFactory.newAccountingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(17042884L, security).qty(38900000).price("102.46449511")
								.localDebitCredit(new BigDecimal("958688.60")).costBasis(new BigDecimal("958688.60")).exchangeRateToBase("1").on("02/21/2020").build(),
						new BigDecimal("38900000"), new BigDecimal("38900000"), new BigDecimal("38900000"))
		);

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-12	null	100000	H-200	Position	XI3301D24U0100XXI	100.5206824633	-8,760,000	45,611.78	03/06/2020	1	45,611.78	45,611.78	03/06/2020	03/06/2020	Position opening of XI3301D24U0100XXI",
						"-13	17233652	100000	H-200	Position	XI3301D24U0100XXI	100.5206824633	-10,640,000	55,400.61	03/06/2020	1	55,400.61	55,400.61	01/02/2020	03/06/2020	Full position close for XI3301D24U0100XXI",
						"-14	17042884	100000	H-200	Position	XI3301D24U0100XXI	100.5206824633	-38,900,000	202,545.49	03/06/2020	1	202,545.49	202,545.49	02/21/2020	03/06/2020	Full position close for XI3301D24U0100XXI",
						"-15	-12	100000	H-200	Cash	USD			-45,611.78	03/06/2020	1	-45,611.78	0	03/06/2020	03/06/2020	Cash expense from opening of XI3301D24U0100XXI",
						"-16	-13	100000	H-200	Cash	USD			-55,400.61	03/06/2020	1	-55,400.61	0	01/02/2020	03/06/2020	Cash expense from close of XI3301D24U0100XXI",
						"-17	-14	100000	H-200	Cash	USD			-202,545.49	03/06/2020	1	-202,545.49	0	02/21/2020	03/06/2020	Cash expense from close of XI3301D24U0100XXI"
				)
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	/////////// TESTS FOR ORIGINAL TO CURRENT FACTOR REDUCTIONS ////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testShortBondPositionOpenAtFactor_NoSplit() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail originalPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-250000.00"), new BigDecimal("101.625"), new BigDecimal("-254062.50"), DateUtils.toDate("08/27/2012"));
		AccountingJournalDetail reductionPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("90738.81"), new BigDecimal("101.625"), new BigDecimal("92213.32"), DateUtils.toDate("08/27/2012"));
		reductionPosition.setParentDefinition(originalPosition);
		journal.addJournalDetail(originalPosition);
		journal.addJournalDetail(reductionPosition);

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		Assertions.assertEquals("com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule: lookupUsingSettlementDate=false, closingOrder=null", bookingRule.toString());


		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 2 entries are still there unmodified by the splitter
				.withExpectedResults(
						"-10	null	100000	H-200	Position	3137AEG90	101.625	-250,000	-254,062.5	08/27/2012	1	-254,062.5	-254,062.5	08/27/2012	08/27/2012	Position opening of 3137AEG90",
						"-11	-10	100000	H-200	Position	3137AEG90	101.625	90,738.81	92,213.32	08/27/2012	1	92,213.32	92,213.32	08/27/2012	08/27/2012	null"
				)
				.execute();
	}


	@Test
	public void testFullShortBondPositionCloseAtFactor() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail originalPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-250000.00"), new BigDecimal("101.625"), new BigDecimal("-254062.50"), DateUtils.toDate("08/27/2012"));
		AccountingJournalDetail reductionPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("90738.81"), new BigDecimal("101.625"), new BigDecimal("92213.32"), DateUtils.toDate("08/27/2012"));
		reductionPosition.setParentDefinition(originalPosition);
		journal.addJournalDetail(originalPosition);
		journal.addJournalDetail(reductionPosition);

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1815848L, security).qty("159261.19").price("103.25").on("04/12/2012").build());
		Assertions.assertEquals("com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule: lookupUsingSettlementDate=false, closingOrder=null", bookingRule.toString());

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 1 closing entry was created: group original + reduction into one current
				.withExpectedResults(
						"-11	1815848	100000	H-200	Position	3137AEG90	101.625	-159,261.19	-161,849.18	08/27/2012	1	-161,849.18	-161,849.18	04/12/2012	08/27/2012	Full position close for 3137AEG90")
				.execute();
	}


	@Test
	public void testFullShortBondPositionCloseAtFactor_AndOpenOfNewPosition() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail originalPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-250000.00"), new BigDecimal("101.625"), new BigDecimal("-254062.50"), DateUtils.toDate("08/27/2012"));
		AccountingJournalDetail reductionPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("90738.81"), new BigDecimal("101.625"), new BigDecimal("92213.32"), DateUtils.toDate("08/27/2012"));
		reductionPosition.setParentDefinition(originalPosition);
		journal.addJournalDetail(originalPosition);
		journal.addJournalDetail(reductionPosition);

		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1815848L, security).qty("109261.19").price("103.25").on("04/12/2012").build());
		Assertions.assertEquals("com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule: lookupUsingSettlementDate=false, closingOrder=null", bookingRule.toString());

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				// verify that 1 closing entry was created: group original + reduction into one current and 2 opening entries: new original face and reduction to new current face
				.withExpectedResults(
						"-11	null	100000	H-200	Position	3137AEG90	101.625	-78,487.42	-79,762.84	08/27/2012	1	-79,762.84	-79,762.84	08/27/2012	08/27/2012	Position opening at new Original Face for 3137AEG90",
						"-12	1815848	100000	H-200	Position	3137AEG90	101.625	-109,261.19	-111,036.68	08/27/2012	1	-111,036.68	-111,036.68	04/12/2012	08/27/2012	Full position close for 3137AEG90",
						"-13	-11	100000	H-200	Position	3137AEG90	101.625	28,487.42	28,950.34	08/27/2012	1	28,950.34	28,950.34	08/27/2012	08/27/2012	Reducing Original Face to Current Face for 3137AEG90"
				)
				.execute();
	}


	@Test
	public void testMultiLotCDS_FullAndPartialClose() {
		InvestmentSecurity security = InvestmentSecurityBuilder.newCDS("TE2401D20E0100XXI").withTradingCurrency(InvestmentSecurityBuilder.newEUR()).build();

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-61900000"), new BigDecimal("102.34189034"), new BigDecimal("697883.32"), new BigDecimal("1449630.12"), new BigDecimal("1.15185"), DateUtils.toDate("07/19/2017"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newEUR(), null, null, BigDecimal.ZERO, new BigDecimal("-1449630.12"), new BigDecimal("1.15185"), DateUtils.toDate("07/19/2017"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
		currency.setParentDefinition(position);
		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);

		// positions will be ordered using FIFO before booking
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(7111831L, security).qty(11700000).price("99.57807654").exchangeRateToBase("1.1033").on("06/22/2017").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(5411562L, security).qty(10500000).price("100.99116677").exchangeRateToBase("1.13745").on("04/06/2016").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(5853994L, security).qty(51800000).price("101.50536575").exchangeRateToBase("1.1249").on("08/24/2016").build());

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						"-12	5411562	100000	H-200	Position	TE2401D20E0100XXI	102.34189034	-10,500,000	104,072.51	07/19/2017	1.13745	118,377.28	104,072.51	04/06/2016	07/19/2017	Full position close for TE2401D20E0100XXI",
						"-13	5853994	100000	H-200	Position	TE2401D20E0100XXI	102.34189034	-51,400,000	773,758	07/19/2017	1.1249	870,400.37	773,758	08/24/2016	07/19/2017	Partial position close for TE2401D20E0100XXI",
						"-14	5853994	100000	H-200	Position	TE2401D20E0100XXI	101.50536575	-400,000	6,021.46	07/19/2017	1.1249	6,773.54	6,021.46	08/24/2016	07/19/2017	Full position close for TE2401D20E0100XXI",
						"-15	5853994	100000	H-200	Position	TE2401D20E0100XXI	101.50536575	400,000	-6,021.46	07/19/2017	1.1249	-6,773.54	-6,021.46	08/24/2016	07/19/2017	Position opening at new Original Notional for TE2401D20E0100XXI",
						"-16	-12	100000	H-200	Currency	EUR			-245,898.49	07/19/2017	1.15185	-283,238.17	0	04/06/2016	07/19/2017	Currency expense from close of TE2401D20E0100XXI",
						"-17	-13	100000	H-200	Currency	EUR			-1,203,731.63	07/19/2017	1.15185	-1,386,518.28	0	08/24/2016	07/19/2017	Currency expense from close of TE2401D20E0100XXI",
						"-18	-14	100000	H-200	Currency	EUR			-9,367.56	07/19/2017	1.15185	-10,790.03	0	08/24/2016	07/19/2017	Currency expense from close of TE2401D20E0100XXI",
						"-19	-15	100000	H-200	Currency	EUR			9,367.56	07/19/2017	1.15185	10,790.03	0	08/24/2016	07/19/2017	Currency proceeds from opening of TE2401D20E0100XXI",
						"-20	-12	100000	H-200	Realized Gain / Loss	TE2401D20E0100XXI	102.34189034	10,500,000	14,308.34	07/19/2017	1.15185	16,481.06	0	04/06/2016	07/19/2017	Realized Gain / Loss loss for TE2401D20E0100XXI",
						"-21	-12	100000	H-200	Currency Translation Gain / Loss	TE2401D20E0100XXI			0	07/19/2017	1.15185	148,379.83	0	04/06/2016	07/19/2017	Currency Translation Gain / Loss for TE2401D20E0100XXI",
						"-23	-13	100000	H-200	Realized Gain / Loss	TE2401D20E0100XXI	102.34189034	51,400,000	-194,255.53	07/19/2017	1.15185	-223,753.23	0	08/24/2016	07/19/2017	Realized Gain / Loss gain for TE2401D20E0100XXI",
						"-24	-13	100000	H-200	Currency Translation Gain / Loss	TE2401D20E0100XXI			0	07/19/2017	1.15185	739,871.14	0	08/24/2016	07/19/2017	Currency Translation Gain / Loss for TE2401D20E0100XXI"
				)
				.execute();
	}


	@Test
	public void testMultiLotCDS_FullAndPartialClose_RoundingError() {
		InvestmentSecurity security = InvestmentSecurityBuilder.newCDS("XY2106D18U0500XXI").withTradingCurrency(InvestmentSecurityBuilder.newUSD()).build();
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail position = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("3200000"), new BigDecimal("108.29"), new BigDecimal("-265280"), new BigDecimal("-265280"), new BigDecimal("1"), DateUtils.toDate("03/09/2015"));
		AccountingJournalDetail currency = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null, null, BigDecimal.ZERO, new BigDecimal("265280"), new BigDecimal("1"), DateUtils.toDate("03/09/2015"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		currency.setParentDefinition(position);
		journal.addJournalDetail(position);
		journal.addJournalDetail(currency);

		AccountingJournalDetail position2 = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-96000"), new BigDecimal("108.29"), new BigDecimal("7958.4"), new BigDecimal("7958.4"), new BigDecimal("1"), DateUtils.toDate("03/09/2015"));
		AccountingJournalDetail currency2 = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), null, null, BigDecimal.ZERO, new BigDecimal("-7958.4"), new BigDecimal("1"), DateUtils.toDate("03/09/2015"), accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		position2.setParentDefinition(position);
		currency2.setParentDefinition(position);
		journal.addJournalDetail(position2);
		journal.addJournalDetail(currency2);


		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(3828959L, security).qty(-1527750).price("107.85").on("05/01/2014").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(3478790L, security).qty(-6208000).price("107.5").on("10/17/2014").build());

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-15	3828959	100000	H-200	Position	XY2106D18U0500XXI	108.29	1,527,750	-126,650.47	03/09/2015	1	-126,650.47	-126,650.47	05/01/2014	03/09/2015	Full position close for XY2106D18U0500XXI",
						"-16	3478790	100000	H-200	Position	XY2106D18U0500XXI	108.29	1,576,250	-130,671.13	03/09/2015	1	-130,671.13	-130,671.13	10/17/2014	03/09/2015	Partial position close for XY2106D18U0500XXI",
						"-17	3478790	100000	H-200	Position	XY2106D18U0500XXI	107.5	4,631,750	-347,381.25	03/09/2015	1	-347,381.25	-347,381.25	10/17/2014	03/09/2015	Full position close for XY2106D18U0500XXI",
						"-18	3478790	100000	H-200	Position	XY2106D18U0500XXI	107.5	-4,775,000	358,125	03/09/2015	1	358,125	358,125	10/17/2014	03/09/2015	Position opening at new Original Notional for XY2106D18U0500XXI",
						"-19	-18	100000	H-200	Position	XY2106D18U0500XXI	107.5	143,250	-10,743.75	03/09/2015	1	-10,743.75	-10,743.75	10/17/2014	03/09/2015	Reducing Original Notional to Current Notional for XY2106D18U0500XXI",
						"-20	-15	100000	H-200	Cash	USD			126,650.47	03/09/2015	1	126,650.47	0	05/01/2014	03/09/2015	Cash proceeds from close of XY2106D18U0500XXI",
						"-21	-16	100000	H-200	Cash	USD			130,671.13	03/09/2015	1	130,671.13	0	10/17/2014	03/09/2015	Cash proceeds from close of XY2106D18U0500XXI",
						"-22	-17	100000	H-200	Cash	USD			383,972.08	03/09/2015	1	383,972.08	0	10/17/2014	03/09/2015	Cash proceeds from close of XY2106D18U0500XXI",
						"-23	-18	100000	H-200	Cash	USD			-395,847.5	03/09/2015	1	-395,847.5	0	10/17/2014	03/09/2015	Cash expense from opening of XY2106D18U0500XXI",
						"-24	-19	100000	H-200	Cash	USD			11,875.42	03/09/2015	1	11,875.42	0	10/17/2014	03/09/2015	Cash proceeds from close of XY2106D18U0500XXI"
				)
				.execute();
	}


	@Test
	public void testPartialShortBondPositionCloseAtFactor_AndReopenAtNewFace() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = createPartialShortBondPositionCloseAtFactorJournal(security);
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(getAccountingTransactions(security, false));

		// verify that 1 full close (NOT PARTIAL) entry was created: group original + reduction into one current and 2 opening entries: new original face and reduction to new current face
		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-11	1815848	100000	H-200	Position	3137AEG90	101.625	-159,261.19	-161,849.18	08/27/2012	1	-161,849.18	-161,849.18	04/12/2012	08/27/2012	Partial position close for 3137AEG90",
						"-12	1815848	100000	H-200	Position	3137AEG90	103.25	-50,000	-51,625	08/27/2012	1	-51,625	-51,625	04/12/2012	08/27/2012	Full position close for 3137AEG90",
						"-13	1815848	100000	H-200	Position	3137AEG90	103.25	78,487.42	81,038.26	08/27/2012	1	81,038.26	81,038.26	04/12/2012	08/27/2012	Position opening at new Original Face for 3137AEG90",
						"-14	-13	100000	H-200	Position	3137AEG90	103.25	-28,487.42	-29,413.26	08/27/2012	1	-29,413.26	-29,413.26	04/12/2012	08/27/2012	Reducing Original Face to Current Face for 3137AEG90"
				)
				.execute();

		// also verify that closing transaction Executing Broker is used
		Assertions.assertEquals("Closing Executing Broker", journal.getJournalDetailList().get(0).getExecutingCompany().getName());
		Assertions.assertEquals("Closing Executing Broker", journal.getJournalDetailList().get(1).getExecutingCompany().getName());
		Assertions.assertEquals("Closing Executing Broker", journal.getJournalDetailList().get(2).getExecutingCompany().getName());
		Assertions.assertEquals("Closing Executing Broker", journal.getJournalDetailList().get(3).getExecutingCompany().getName());
	}


	@Test
	public void testPartialShortBondPositionCloseAtFactor_AndReopenAtNewFaceWithParentTransaction() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "3137AEG90", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		security.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());

		AccountingJournal journal = createPartialShortBondPositionCloseAtFactorJournal(security);
		AccountingPositionSplitterBookingRule<BookableEntity> bookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(getAccountingTransactions(security, true));

		// verify that 1 full close (NOT PARTIAL) entry was created: group original + reduction into one current and 2 opening entries: new original face and reduction to new current face
		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-11	1815848	100000	H-200	Position	3137AEG90	101.625	-159,261.19	-161,849.18	08/27/2012	1	-161,849.18	-161,849.18	04/12/2012	08/27/2012	Partial position close for 3137AEG90",
						"-12	1815848	100000	H-200	Position	3137AEG90	103.25	-50,000	-51,625	08/27/2012	1	-51,625	-51,625	04/12/2012	08/27/2012	Full position close for 3137AEG90",
						"-13	10	100000	H-200	Position	3137AEG90	103.25	78,487.42	81,038.26	08/27/2012	1	81,038.26	81,038.26	04/12/2012	08/27/2012	Position opening at new Original Face for 3137AEG90",
						"-14	-13	100000	H-200	Position	3137AEG90	103.25	-28,487.42	-29,413.26	08/27/2012	1	-29,413.26	-29,413.26	04/12/2012	08/27/2012	Reducing Original Face to Current Face for 3137AEG90"
				)
				.execute();
	}


	private AccountingJournal createPartialShortBondPositionCloseAtFactorJournal(InvestmentSecurity security) {
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail originalPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("-250000.00"), new BigDecimal("101.625"), new BigDecimal("-254062.50"), DateUtils.toDate("08/27/2012"));
		originalPosition.setExecutingCompany(new BusinessCompany());
		originalPosition.getExecutingCompany().setName("Closing Executing Broker");
		AccountingJournalDetail reductionPosition = AccountingTestObjectFactory.newAccountingJournalDetail(security, new BigDecimal("90738.81"), new BigDecimal("101.625"), new BigDecimal("92213.32"), DateUtils.toDate("08/27/2012"));
		reductionPosition.setParentDefinition(originalPosition);
		journal.addJournalDetail(originalPosition);
		journal.addJournalDetail(reductionPosition);

		return journal;
	}


	private AccountingTransaction[] getAccountingTransactions(InvestmentSecurity security, boolean withParent) {
		List<AccountingTransaction> transactionList = new ArrayList<>();

		AccountingTransaction parentTransaction = null;
		if (withParent) {
			parentTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(10L, security).qty("209261.19").price("103.25").on("04/12/2012").build();
			parentTransaction.setExecutingCompany(new BusinessCompany());
			parentTransaction.getExecutingCompany().setName("Opening Executing Broker");
		}

		AccountingTransaction transaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1815848L, security).qty("209261.19").price("103.25").on("04/12/2012").build();
		transaction.setExecutingCompany(new BusinessCompany());
		transaction.getExecutingCompany().setName("Opening Executing Broker");
		transaction.setParentTransaction(parentTransaction);

		transactionList.add(transaction);

		return transactionList.toArray(new AccountingTransaction[0]);
	}
}
