package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class AccountingCloseAtCostBookingRuleTests {

	@Test
	public void testPartialCloseOfOneLotInternationalFuture() {
		// start with existing positive lot of 59 contracts
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(16857, "PTM2", InvestmentType.FUTURES, "CAD");
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1745141L, security)
				.qty(59).price("707.95").costBasis("8353810.00").exchangeRateToBase("1.0112757243").on("03/12/2012").build();

		AccountingPositionSplitterBookingRule<BookableEntity> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		// partial close for 33 contracts
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail positionDetail = AccountingTestObjectFactory.newAccountingJournalDetail(security, -33, new BigDecimal("699.4848485"), new BigDecimal("-4616600.00"), new BigDecimal(
				"1.0121969735"), DateUtils.toDate("05/01/2012"));
		journal.addJournalDetail(positionDetail);
		journal.addJournalDetail(positionSplitterRule.createCashCurrencyRecord(positionDetail));

		AccountingBookingRuleTestExecutor.newTestForJournal(journal)
				.forBookingRules(
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule(),
						AccountingTestObjectFactory.newRemoveJournalDetailsBookingRule(AccountingAccount.REVENUE_REALIZED, AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS),
						AccountingTestObjectFactory.newCloseAtCostBookingRule()
				)
				.withExpectedResults(
						"-12	1745141	100000	H-200	Position	PTM2	707.95	-33	0	05/01/2012	1.0112757243	0	-4,672,470	03/12/2012	05/01/2012	Partial position close for PTM2",
						"-14	-12	100000	H-200	Currency	CAD			-55,870	05/01/2012	1.0121969735	-56,551.44	0	03/12/2012	05/01/2012	Currency expense from close of PTM2"
				)
				.execute();
	}
}
