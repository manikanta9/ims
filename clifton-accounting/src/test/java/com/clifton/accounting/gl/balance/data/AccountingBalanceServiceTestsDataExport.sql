SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE HoldingInvestmentAccountID IN (SELECT RelatedAccountID FROM InvestmentAccountRelationship WHERE MainAccountId IN (2272,4616,5571) AND StartDate <= '10/08/2018' AND (EndDate >= '10/08/2018' OR EndDate IS NULL)) AND AccountingAccountID IN (Select AccountingAccountID FROM AccountingAccount WHERE IsCash = 1) and TransactionDate < '10/01/2018'
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountId IN (2272,4616,5571)
UNION
SELECT 'InvestmentAccountGroupAccount' AS entityTableName, InvestmentAccountGroupAccountID AS entityId FROM InvestmentAccountGroupAccount WHERE InvestmentAccountID IN (2272,4616,5571)
;
