package com.clifton.accounting.gl.position;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
public class AccountingPositionOrdersTests {


	@Test
	public void testHIFO() {
		List<AccountingPosition> positionList = newPositionList();

		AccountingPositionOrders.HIFO.sortPositions(positionList);

		Assertions.assertEquals(4, positionList.size());

		Assertions.assertEquals(2, positionList.get(0).getId());
		Assertions.assertEquals(4, positionList.get(1).getId());
		Assertions.assertEquals(1, positionList.get(2).getId());
		Assertions.assertEquals(3, positionList.get(3).getId());
	}


	@Test
	public void testFIFO() {
		List<AccountingPosition> positionList = newPositionList();

		AccountingPositionOrders.FIFO.sortPositions(positionList);

		Assertions.assertEquals(4, positionList.size());

		Assertions.assertEquals(4, positionList.get(0).getId());
		Assertions.assertEquals(1, positionList.get(1).getId());
		Assertions.assertEquals(3, positionList.get(2).getId());
		Assertions.assertEquals(2, positionList.get(3).getId());
	}


	@Test
	public void testLIFO() {
		List<AccountingPosition> positionList = newPositionList();

		AccountingPositionOrders.LIFO.sortPositions(positionList);

		Assertions.assertEquals(4, positionList.size());

		Assertions.assertEquals(2, positionList.get(0).getId());
		Assertions.assertEquals(3, positionList.get(1).getId());
		Assertions.assertEquals(1, positionList.get(2).getId());
		Assertions.assertEquals(4, positionList.get(3).getId());
	}


	@Test
	public void testLOFO() {
		List<AccountingPosition> positionList = newPositionList();

		AccountingPositionOrders.LOFO.sortPositions(positionList);

		Assertions.assertEquals(4, positionList.size());

		Assertions.assertEquals(3, positionList.get(0).getId());
		Assertions.assertEquals(1, positionList.get(1).getId());
		Assertions.assertEquals(4, positionList.get(2).getId());
		Assertions.assertEquals(2, positionList.get(3).getId());
	}


	private List<AccountingPosition> newPositionList() {
		List<AccountingPosition> positionList = new ArrayList<>();
		InvestmentSecurity ibm = InvestmentTestObjectFactory.newInvestmentSecurity(100, "IBM", InvestmentType.STOCKS);
		positionList.add(AccountingTestObjectFactory.newAccountingPosition(1L, ibm, 10, new BigDecimal("50"), DateUtils.toDate("01/01/2011")));
		positionList.add(AccountingTestObjectFactory.newAccountingPosition(2L, ibm, 7, new BigDecimal("55"), DateUtils.toDate("02/01/2011")));
		positionList.add(AccountingTestObjectFactory.newAccountingPosition(3L, ibm, 15, new BigDecimal("40"), DateUtils.toDate("01/20/2011")));
		positionList.add(AccountingTestObjectFactory.newAccountingPosition(4L, ibm, 3, new BigDecimal("52"), DateUtils.toDate("01/01/2010")));

		return positionList;
	}
}
