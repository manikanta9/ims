package com.clifton.accounting.gl.journal;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class AccountingJournalTests {

	@Test
	public void testFindChildEntries() {
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail parent = new AccountingJournalDetail();
		journal.addJournalDetail(parent);

		AccountingJournalDetail detail = new AccountingJournalDetail();
		journal.addJournalDetail(detail);

		Assertions.assertEquals(0, journal.findChildEntries(parent).size(), "no parent definition: no children");

		detail.setParentDefinition(parent);
		Assertions.assertEquals(1, journal.findChildEntries(parent).size(), "parent definition set: 1 child");

		detail.setParentTransaction(new AccountingTransaction());
		Assertions.assertEquals(0, journal.findChildEntries(parent).size(), "parent transaction set: ignore definition");
	}


	@Test
	public void testIsSameTransactionSet() {
		AccountingJournal journalOne = new AccountingJournal();
		AccountingJournal journalTwo = new AccountingJournal();

		Assertions.assertTrue(journalOne.isSameTransactionSet(journalTwo));

		AccountingJournalDetail detailOne = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), new BigDecimal("5"), new BigDecimal("100"), BigDecimal.ZERO, DateUtils.toDate("01/01/2017"));
		AccountingJournalDetail detailTwo = AccountingTestObjectFactory.newAccountingJournalDetail(InvestmentSecurityBuilder.newUSD(), new BigDecimal("6"), new BigDecimal("100"), BigDecimal.ZERO, DateUtils.toDate("01/01/2017"));

		journalOne.addJournalDetail(detailOne);
		Assertions.assertFalse(journalOne.isSameTransactionSet(journalTwo));

		journalOne.addJournalDetail(detailTwo);
		Assertions.assertFalse(journalOne.isSameTransactionSet(journalTwo));

		journalTwo.addJournalDetail(detailTwo);
		Assertions.assertFalse(journalOne.isSameTransactionSet(journalTwo));

		journalTwo.addJournalDetail(detailOne);
		Assertions.assertTrue(journalOne.isSameTransactionSet(journalTwo));
	}
}
