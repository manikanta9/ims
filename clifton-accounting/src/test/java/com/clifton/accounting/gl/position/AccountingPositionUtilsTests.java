package com.clifton.accounting.gl.position;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.core.test.util.TestUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The test class for {@link AccountingPositionUtils} utilities.
 *
 * @author MikeH
 */
@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
public class AccountingPositionUtilsTests {

	private InvestmentSecurity security1;
	private InvestmentSecurity security2;
	private InvestmentSecurity security3;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		this.security1 = InvestmentTestObjectFactory.newInvestmentSecurity(TestUtils.generateId(), "AAPL", "Stocks");
		this.security2 = InvestmentTestObjectFactory.newInvestmentSecurity(TestUtils.generateId(), "GOOG", "Stocks");
		this.security3 = InvestmentTestObjectFactory.newInvestmentSecurity(TestUtils.generateId(), "EUR-USD", "Forwards");
		this.security3.getInstrument().getHierarchy().setCloseOnMaturityOnly(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMergePendingAccountingPositionList() {
		assertExpectedMergedPositions(
				Arrays.asList(),
				Arrays.asList());
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, 1, 10)),
				Arrays.asList(
						generatePosition(this.security1, 1, 10)));
		assertExpectedMergedPositions(
				Arrays.asList(),
				Arrays.asList(
						generatePosition(this.security1, 1, 20),
						generatePendingPosition(this.security1, -1, 10)));
		assertExpectedMergedPositions(
				Arrays.asList(),
				Arrays.asList(
						generatePosition(this.security1, 1, 10),
						generatePosition(this.security2, 1, 20),
						generatePendingPosition(this.security1, -1, 1),
						generatePendingPosition(this.security2, -1, 1)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, 1, 15),
						new ExpectedPositionResult(this.security1, 1, 10)),
				Arrays.asList(
						generatePosition(this.security1, 1, 15),
						generatePosition(this.security2, 1, 10),
						generatePendingPosition(this.security1, 1, 10),
						generatePendingPosition(this.security2, -1, 5)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, 1, 6)),
				Arrays.asList(
						generatePosition(this.security1, 1, 11),
						generatePosition(this.security1, 2, 12),
						generatePendingPosition(this.security1, -2, 13)));
	}


	@Test
	public void testMergePendingAccountingPositionList_RedundantPending() {
		assertExpectedMergedPositions(
				Arrays.asList(),
				Arrays.asList(
						generatePosition(this.security1, 1, 11),
						generatePendingPosition(this.security1, 1, 12),
						generatePendingPosition(this.security1, 1, 13),
						generatePendingPosition(this.security1, -1, 14),
						generatePendingPosition(this.security1, -1, 15),
						generatePendingPosition(this.security1, -1, 16)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, -1, 16)),
				Arrays.asList(
						generatePosition(this.security1, 1, 11),
						generatePendingPosition(this.security1, 1, 12),
						generatePendingPosition(this.security1, 1, 13),
						generatePendingPosition(this.security1, -1, 14),
						generatePendingPosition(this.security1, -1, 15),
						generatePendingPosition(this.security1, -1, 16),
						generatePendingPosition(this.security1, -1, 16)));
		assertExpectedMergedPositions(
				Arrays.asList(),
				Arrays.asList(
						generatePosition(this.security1, 3, 11),
						generatePendingPosition(this.security1, 2, 12),
						generatePendingPosition(this.security1, -5, 13)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, 1, 15)),
				Arrays.asList(
						generatePosition(this.security1, 3, 45),
						generatePendingPosition(this.security1, -1, 5),
						generatePendingPosition(this.security1, -1, 5)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, -1, 50)),
				Arrays.asList(
						generatePosition(this.security1, 3, 10),
						generatePendingPosition(this.security1, -1, 10),
						generatePendingPosition(this.security1, -3, 150)));
		assertExpectedMergedPositions(
				Arrays.asList(
						new ExpectedPositionResult(this.security1, 1, 13)),
				Arrays.asList(
						generatePosition(this.security1, 1, 10),
						generatePendingPosition(this.security1, -1, 11),
						generatePendingPosition(this.security1, 1, 12),
						generatePendingPosition(this.security1, 1, 13),
						generatePendingPosition(this.security1, -1, 14)));
	}


	@Test
	public void testMergePendingAccountingPositionList_HierarchyIsCloseOnMaturity() {
		assertExpectedMergedPositions(Arrays.asList(
				new ExpectedPositionResult(this.security1, 1, 11),
				new ExpectedPositionResult(this.security3, 1, 13),
				new ExpectedPositionResult(this.security1, 1, 14),
				new ExpectedPositionResult(this.security3, -1, 16)
		), Arrays.asList(
				generatePosition(this.security1, 1, 11),
				generatePosition(this.security2, 1, 12),
				generatePosition(this.security3, 1, 13),
				generatePendingPosition(this.security1, 1, 14),
				generatePendingPosition(this.security2, -1, 15),
				generatePendingPosition(this.security3, -1, 16)
		));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPosition generatePosition(InvestmentSecurity security, int quantity, int costBasis) {
		return AccountingTestObjectFactory.newAccountingPosition(TestUtils.generateId(), security, quantity, BigDecimal.TEN, BigDecimal.valueOf(costBasis), new Date());
	}


	private AccountingPosition generatePendingPosition(InvestmentSecurity security, int quantity, int costBasis) {
		AccountingPosition position = generatePosition(security, quantity, costBasis);
		position.setPendingActivity(true);
		return position;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Runs {@link AccountingPositionUtils#mergePendingAccountingPositionList(List, boolean)} on the given <code>originalPositionList</code> and tests the produced results against the
	 * provided <code>expectedResultList</code>.
	 */
	private void assertExpectedMergedPositions(List<ExpectedPositionResult> expectedResultList, List<AccountingPosition> originalPositionList) {
		List<AccountingPosition> positionList = AccountingPositionUtils.mergePendingAccountingPositionList(originalPositionList, false);
		List<AccountingPosition> remainingPositionList = new ArrayList<>(positionList);
		List<ExpectedPositionResult> remainingExpectedResultList = new ArrayList<>(expectedResultList);
		for (AccountingPosition position : positionList) {
			remainingExpectedResultList.stream()
					.filter(expectedPosition -> Objects.equals(expectedPosition.getSecurity(), position.getInvestmentSecurity()))
					.filter(expectedPosition -> MathUtils.isEqual(expectedPosition.getQuantity(), position.getRemainingQuantity()))
					.filter(expectedPosition -> MathUtils.isEqual(expectedPosition.getCostBasis(), position.getRemainingCostBasis()))
					.findAny()
					.ifPresent(matchedExpectedPosition -> {
						remainingPositionList.remove(position);
						remainingExpectedResultList.remove(matchedExpectedPosition);
					});
		}
		if (!remainingPositionList.isEmpty() || !remainingExpectedResultList.isEmpty()) {
			Assertions.fail(String.format("Expected: %s. Actual: %s. Missing expected results: %s. Extra actual results: %s.", expectedResultList, positionList, remainingExpectedResultList, remainingPositionList));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The expected result holder for produced accounting position properties.
	 */
	private static class ExpectedPositionResult {

		private final InvestmentSecurity security;
		private final BigDecimal quantity;
		private final BigDecimal costBasis;


		public ExpectedPositionResult(InvestmentSecurity security, int quantity, int costBasis) {
			this.security = security;
			this.quantity = BigDecimal.valueOf(quantity);
			this.costBasis = BigDecimal.valueOf(costBasis);
		}


		@Override
		public String toString() {
			return "ExpectedPositionResult{" +
					"security=" + this.security +
					", quantity=" + this.quantity +
					'}';
		}


		public InvestmentSecurity getSecurity() {
			return this.security;
		}


		public BigDecimal getQuantity() {
			return this.quantity;
		}


		public BigDecimal getCostBasis() {
			return this.costBasis;
		}
	}
}
