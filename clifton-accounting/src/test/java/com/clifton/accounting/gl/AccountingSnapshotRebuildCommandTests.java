package com.clifton.accounting.gl;

import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Date;


/**
 * The <code>AccountingSnapshotRebuildCommandTests</code> tests the validation logic in the AccountingSnapshotRebuildCommand
 *
 * @author manderson
 */
public class AccountingSnapshotRebuildCommandTests {


	@Test
	public void validateMissingSnapshotDates() {
		TestUtils.expectException(FieldValidationException.class, () -> {
					AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
					command.validateSnapshotDates();
				},
				"Start snapshot date is required");
	}


	@Test
	public void validateMissingSnapshotEndDate() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
			command.validateSnapshotDates();
		}, "End snapshot date is required");
	}


	@Test
	public void validateEndDateExtendsPastLimit() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setAllowFutureSnapshots(true);
			command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
			command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 75)));
			command.validateSnapshotDates();
		}, "Snapshot End Date cannot be more than 60 days in the future.");
	}


	@Test
	public void validateFutureSnapshotsException_NotAllowedToday() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setAllowFutureSnapshots(false);
			command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
			command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
			command.validateSnapshotDates();
		}, "Cannot enter an end date after " + DateUtils.fromDateShort(DateUtils.addDays(DateUtils.clearTime(new Date()), -1)) + " because the option to build snapshots on or after today is not set.");
	}


	@Test
	public void validateFutureSnapshotsException_AllowedToday() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setAllowFutureSnapshots(false);
			command.setAllowTodaySnapshots(true);
			command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
			command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
			command.validateSnapshotDates();
		}, "Cannot enter an end date after " + DateUtils.fromDateShort(DateUtils.clearTime(new Date())) + " because the option to build snapshots after today is not set.");
	}


	@Test
	public void validateFutureSnapshotsAllowed_WithinDayLimit() {
		AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(true);
		command.setAllowTodaySnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.validateSnapshotDates();

		// Command should have reset end date to Today
		Assertions.assertEquals(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)), command.getEndSnapshotDate());
	}


	@Test
	public void validateFutureSnapshotsNoException_ResetEndDate() {
		AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(false);
		command.setAllowTodaySnapshots(true);
		command.setDoNotThrowExceptionForFutureSnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.validateSnapshotDates();

		// Command should have reset end date to Today
		Assertions.assertEquals(DateUtils.clearTime(new Date()), command.getEndSnapshotDate());

		command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(false);
		command.setAllowTodaySnapshots(false);
		command.setDoNotThrowExceptionForFutureSnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.validateSnapshotDates();

		// Command should have reset end date to Yesterday
		Assertions.assertEquals(DateUtils.clearTime(DateUtils.addDays(new Date(), -1)), command.getEndSnapshotDate());
	}


	@Test
	public void validateGlobalRebuilds_Admin() {
		CalendarBusinessDayService calendarBusinessDayService = Mockito.mock(CalendarBusinessDayService.class);
		SecurityAuthorizationService securityAuthorizationService = Mockito.mock(SecurityAuthorizationService.class);
		Mockito.when(securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.validateGlobalRebuild(calendarBusinessDayService, securityAuthorizationService);
	}


	@Test
	public void validateGlobalRebuilds_NonAdmin() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			Date startSnapshotDate = DateUtils.toDate("01/01/2019");
			Date endSnapshotDate = DateUtils.clearTime(DateUtils.addDays(new Date(), 30));
			CalendarBusinessDayService calendarBusinessDayService = Mockito.mock(CalendarBusinessDayService.class);
			SecurityAuthorizationService securityAuthorizationService = Mockito.mock(SecurityAuthorizationService.class);
			Mockito.when(calendarBusinessDayService.getBusinessDaysBetween(endSnapshotDate, startSnapshotDate, null)).thenReturn(10);
			Mockito.when(securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);

			AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setAllowFutureSnapshots(true);
			command.setStartSnapshotDate(startSnapshotDate);
			command.setEndSnapshotDate(endSnapshotDate);
			command.validateGlobalRebuild(calendarBusinessDayService, securityAuthorizationService);
		}, "Rebuilding more than 5 day(s) of snapshots is not allowed unless you are an administrator.");
	}


	@Test
	public void validateGlobalRebuilds_NotGlobal() {
		CalendarBusinessDayService calendarBusinessDayService = Mockito.mock(CalendarBusinessDayService.class);
		SecurityAuthorizationService securityAuthorizationService = Mockito.mock(SecurityAuthorizationService.class);

		AccountingSnapshotRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.setClientAccountId(1);
		command.validateGlobalRebuild(calendarBusinessDayService, securityAuthorizationService);


		command = new AccountingPositionDailyRebuildCommand();
		command.setAllowFutureSnapshots(true);
		command.setStartSnapshotDate(DateUtils.toDate("01/01/2019"));
		command.setEndSnapshotDate(DateUtils.clearTime(DateUtils.addDays(new Date(), 30)));
		command.setInvestmentAccountGroupId(1);
		command.validateGlobalRebuild(calendarBusinessDayService, securityAuthorizationService);
	}
}
