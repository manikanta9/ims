package com.clifton.accounting.gl.balance;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author JasonS
 */
public class AccountingBalanceUtilsTests {


	@Test
	public void testConvertAndGroupAccountingPositionListIntoBalanceList_basic() {

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(6008, "31288VXX2", InvestmentType.OPTIONS);
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, investmentSecurity)
				.qty(100).price("24.3").on("7/7/2021").build();
		AccountingPosition position1 = AccountingTestObjectFactory.newAccountingTransaction(tran1);
		List<AccountingPosition> positionList = new ArrayList<>();
		positionList.add(position1);
		Assertions.assertEquals(new BigDecimal(100), AccountingBalanceUtils.convertAndGroupAccountingPositionListIntoBalanceList(positionList, true).get(0).getQuantity());
	}


	@Test
	public void testConvertAndGroupAccountingPositionListIntoBalanceList_Add() {

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(6009, "31288VXX3", InvestmentType.OPTIONS);
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, investmentSecurity)
				.qty(100).price("24.3").on("7/7/2021").build();
		AccountingPosition position1 = AccountingTestObjectFactory.newAccountingTransaction(tran1);
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12346L, investmentSecurity)
				.qty(50).price("24.3").on("7/8/2021").build();
		AccountingPosition position2 = AccountingTestObjectFactory.newAccountingTransaction(tran2);
		List<AccountingPosition> positionList = new ArrayList<>();
		positionList.add(position1);
		positionList.add(position2);
		Assertions.assertEquals(new BigDecimal(150), AccountingBalanceUtils.convertAndGroupAccountingPositionListIntoBalanceList(positionList, true).get(0).getQuantity());
		Assertions.assertEquals(new BigDecimal(100), positionList.get(0).getQuantity());
	}


	@Test
	public void testConvertAndGroupAccountingPositionListIntoBalanceList_Subtract() {

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(6007, "31288VXX4", InvestmentType.OPTIONS);
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12347L, investmentSecurity)
				.qty(100).price("24.3").on("7/7/2021").build();
		AccountingPosition position1 = AccountingTestObjectFactory.newAccountingTransaction(tran1);
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12348L, investmentSecurity)
				.qty(-50).price("24.3").on("7/8/2021").build();
		AccountingPosition position2 = AccountingTestObjectFactory.newAccountingTransaction(tran2);
		List<AccountingPosition> positionList = new ArrayList<>();
		positionList.add(position1);
		positionList.add(position2);
		Assertions.assertEquals(new BigDecimal(50), AccountingBalanceUtils.convertAndGroupAccountingPositionListIntoBalanceList(positionList, true).get(0).getQuantity());
		Assertions.assertEquals(new BigDecimal(100), positionList.get(0).getQuantity());
	}
}
