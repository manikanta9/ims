package com.clifton.accounting.taskjournal;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.taskjournal.booking.AccountingTaskJournalBookingRule;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;


public class AccountingTaskJournalBookingRuleTests {

	@Test
	public void testInternationalREPO_InterestIncome_Transfer() {
		// create test task journal
		AccountingTaskJournal taskJournal = new AccountingTaskJournal();
		taskJournal.setJournalType(new AccountingTaskJournalType());
		taskJournal.getJournalType().setName("REPO Coupon Transfer");
		taskJournal.setInvestmentSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(19495, "GB00B85SFQ54", InvestmentType.BONDS, "GBP"));
		taskJournal.setClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		taskJournal.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		taskJournal.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		taskJournal.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		taskJournal.setQuantity(new BigDecimal("2600000"));
		taskJournal.setAmount(new BigDecimal("1467.53"));
		taskJournal.setTransactionDate(DateUtils.toDate("03/22/2013"));
		taskJournal.setSettlementDate(DateUtils.toDate("03/22/2013"));

		// journal with 2 details: Interest Income and Currency
		AccountingJournal baseJournal = new AccountingJournal();
		taskJournal.setAccountingJournal(baseJournal);
		baseJournal.setId(868251L);
		AccountingJournalDetail detail = AccountingTestObjectFactory.newAccountingJournalDetail(taskJournal.getInvestmentSecurity(), 2600, null, new BigDecimal("0"), new BigDecimal("1.4925"),
				taskJournal.getTransactionDate());
		detail.setAccountingAccount(new AccountingAccount(new AccountingAccountType(AccountingAccountType.REVENUE, false, false), AccountingAccount.REVENUE_INTEREST_INCOME, new Short("604")));
		baseJournal.addJournalDetail(detail);
		detail = AccountingTestObjectFactory.newAccountingJournalDetail(taskJournal.getInvestmentSecurity().getInstrument().getTradingCurrency(), null, null, new BigDecimal("1467.53"),
				new BigDecimal("1.6551"), taskJournal.getTransactionDate());
		detail.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Currency());
		baseJournal.addJournalDetail(detail);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingTaskJournal> bookingSession = new SimpleBookingSession<>(journal, taskJournal);

		// create and mock booking rules
		AccountingTaskJournalBookingRule taskRule = new AccountingTaskJournalBookingRule();
		taskRule.setAccountingJournalService(Mockito.mock(AccountingJournalService.class));
		Mockito.when(taskRule.getAccountingJournalService().getAccountingJournal(baseJournal.getId())).thenReturn(baseJournal);

		taskRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();

		Assertions.assertEquals(4, journalDetailList.size());
		Assertions.assertEquals("-10	null	100000	H-200	Interest Income	GB00B85SFQ54		-2,600,000	1,467.53	03/22/2013	1.4925	2,190.29	0	03/22/2013	03/22/2013	REPO Coupon Transfer: GB00B85SFQ54",
				journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	H-201	Interest Income	GB00B85SFQ54		2,600,000	-1,467.53	03/22/2013	1.4925	-2,190.29	0	03/22/2013	03/22/2013	REPO Coupon Transfer: GB00B85SFQ54",
				journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	H-200	Currency	GBP			-1,467.53	03/22/2013	1.4925	-2,190.29	0	03/22/2013	03/22/2013	REPO Coupon Transfer: GB00B85SFQ54", journalDetailList.get(2)
				.toStringFormatted(false));
		Assertions.assertEquals("-13	null	100000	H-201	Currency	GBP			1,467.53	03/22/2013	1.4925	2,190.29	0	03/22/2013	03/22/2013	REPO Coupon Transfer: GB00B85SFQ54",
				journalDetailList.get(3).toStringFormatted(false));
	}
}
