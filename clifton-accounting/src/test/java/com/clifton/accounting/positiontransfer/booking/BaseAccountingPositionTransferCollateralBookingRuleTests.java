package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.system.schema.SystemSchemaServiceImpl;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public abstract class BaseAccountingPositionTransferCollateralBookingRuleTests {

	@Mock
	protected AccountingAccountService accountingAccountService;

	@Mock
	protected AccountingPositionTransferService accountingPositionTransferService;

	protected static BusinessCompany CITIGROUP;


	static {
		CITIGROUP = new BusinessCompany();
		CITIGROUP.setId(101);
		CITIGROUP.setName("Citibank N.A.");
	}


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);

		this.accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<AccountingTransaction> getPostedTransactionsFromDetails(List<? extends AccountingJournalDetailDefinition> detailList) {
		List<AccountingTransaction> result = new ArrayList<>();
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			AccountingTransaction transaction = new AccountingTransaction();
			BeanUtils.copyProperties(detail, transaction);
			transaction.setId(Math.abs(transaction.getId()));
			if (transaction.getParentDefinition() != null) {
				AccountingTransaction parentTransaction = new AccountingTransaction();
				BeanUtils.copyProperties(transaction.getParentDefinition(), parentTransaction);
				transaction.setParentTransaction(parentTransaction);
				transaction.setParentDefinition(null);
			}
			if (transaction.getParentTransaction() != null) {
				transaction.getParentTransaction().setId(Math.abs(transaction.getParentTransaction().getId()));
			}
			result.add(transaction);
		}
		return result;
	}


	protected List<? extends AccountingJournalDetailDefinition> bookTransferJournal(AccountingPositionTransfer transfer, AccountingPositionTransferBookingRule transferRule, String... expectedResults) {
		AccountingJournal journal = getAccountingJournal(100, transfer.getId());

		@SuppressWarnings("unchecked")
		List<? extends AccountingJournalDetailDefinition> resultingDetails = AccountingBookingRuleTestExecutor.newTestForJournalAndEntity(journal, transfer)
				.forBookingRules(
						new AccountingBookingRule[]{
								transferRule,
								AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
								AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
								//new AccountingPositionTransferCommissionBookingRule(),
								AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule(),
								AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
								//new AccountingAutoAccrualReversalBookingRule(),
								AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
						}
				)
				.withExpectedResults(expectedResults)
				.execute();
		return resultingDetails;
	}


	private AccountingJournal getAccountingJournal(long id, Integer fkFieldId) {
		AccountingJournal journal = new AccountingJournal();
		journal.setId(id);
		journal.setFkFieldId(fkFieldId);
		AccountingJournalType journalType = new AccountingJournalType();
		journalType.setId((short) 11);
		journalType.setName(AccountingPositionTransferBookingProcessor.JOURNAL_NAME);
		journal.setJournalType(journalType);
		return journal;
	}


	protected List<AccountingPosition> getCurrentPositionListFromPostedTransactions(List<AccountingTransaction> transactionList) {
		List<AccountingPosition> currentPositions = new ArrayList<>();
		for (AccountingTransaction transaction : CollectionUtils.getIterable(transactionList)) {
			currentPositions.add(AccountingPosition.forOpeningTransaction(transaction));
		}
		return currentPositions;
	}


	protected static InvestmentAccount newInvestmentAccount_Custodian() {
		InvestmentAccount result = new InvestmentAccount(200, "Custodian");
		result.setNumber("Custodian");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setIssuingCompany(CITIGROUP);
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	protected static InvestmentAccount newInvestmentAccount_Counterparty() {
		InvestmentAccount result = new InvestmentAccount(210, "Counterparty");
		result.setNumber("Counterparty");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Goldman Sachs");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	protected AccountingPositionTransferCollateralBookingRule newAccountingPositionTransferCollateralBookingRule() {
		AccountingPositionTransferCollateralBookingRule transferRule = new AccountingPositionTransferCollateralBookingRule();
		transferRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		transferRule.setInvestmentCalculator(new InvestmentCalculatorImpl());
		transferRule.setSystemSchemaService(Mockito.mock(SystemSchemaServiceImpl.class));
		Mockito.when(transferRule.getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail")).thenReturn(new SystemTable());
		transferRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		return transferRule;
	}
}
