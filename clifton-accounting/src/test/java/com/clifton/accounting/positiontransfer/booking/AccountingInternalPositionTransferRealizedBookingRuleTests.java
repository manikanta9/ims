package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaServiceImpl;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AccountingInternalPositionTransferRealizedBookingRuleTests {

	@Test
	public void testFutures_MultiLot() {
		// create test transfer
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();

		AccountingPositionTransferType transferType = new AccountingPositionTransferType();
		transferType.setReceivingAsset(true);
		transferType.setSendingAsset(true);
		transfer.setType(transferType);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setTransactionDate(DateUtils.toDate("08/01/2013"));
		transfer.setSettlementDate(DateUtils.toDate("08/01/2013"));

		// existing domestic future position: leave gains in old client account, open at cost in new client account and immediately reduce gains
		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18187, "CDU3", InvestmentType.FUTURES, new BigDecimal(1000), "USD"));
		detail.setCostPrice(new BigDecimal("97.915"));
		detail.setTransferPrice(new BigDecimal("97.45"));
		detail.setQuantity(new BigDecimal("26"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("2533700.00"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("06/12/2013"));
		detail.setExistingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2753980, detail.getSecurity())
				.qty(26).price(detail.getCostPrice()).on(detail.getOriginalPositionOpenDate()).build());
		detailList.add(detail);

		AccountingPositionTransferDetail detail2 = new AccountingPositionTransferDetail();
		detail2.setPositionTransfer(transfer);
		detail2.setSecurity(detail.getSecurity());
		detail2.setCostPrice(new BigDecimal("95.39"));
		detail2.setTransferPrice(new BigDecimal("97.45"));
		detail2.setQuantity(new BigDecimal("18"));
		detail2.setExchangeRateToBase(BigDecimal.ONE);
		detail2.setPositionCostBasis(new BigDecimal("1717020.00"));
		detail2.setOriginalPositionOpenDate(DateUtils.toDate("06/27/2013"));
		detail2.setExistingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2802117, detail.getSecurity())
				.qty(18).price(detail.getCostPrice()).costBasis("1717020.00").on(detail2.getOriginalPositionOpenDate()).build());
		detailList.add(detail2);

		AccountingPositionTransferDetail detail3 = new AccountingPositionTransferDetail();
		detail3.setPositionTransfer(transfer);
		detail3.setSecurity(detail.getSecurity());
		detail3.setCostPrice(new BigDecimal("95.006129"));
		detail3.setTransferPrice(new BigDecimal("97.45"));
		detail3.setQuantity(new BigDecimal("34"));
		detail3.setExchangeRateToBase(BigDecimal.ONE);
		detail3.setPositionCostBasis(new BigDecimal("3230208.39"));
		detail3.setOriginalPositionOpenDate(DateUtils.toDate("06/28/2013"));
		detail3.setExistingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2806026, detail.getSecurity())
				.qty(34).price(detail.getCostPrice()).costBasis("3230208.39").on(detail3.getOriginalPositionOpenDate()).build());
		detailList.add(detail3);
		transfer.setDetailList(detailList);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = new AccountingPositionTransferBookingRule();
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(detail.getExistingPosition()));
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(detail2.getExistingPosition()));
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(detail3.getExistingPosition()));
		Mockito.when(transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(existingPositions);

		transferRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		transferRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		transferRule.setInvestmentCalculator(Mockito.mock(InvestmentCalculator.class));
		Mockito.when(
				transferRule.getInvestmentCalculator().calculateNotional(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(Date.class)))
				.thenReturn(new BigDecimal("3714100.00"));
		transferRule.setSystemSchemaService(Mockito.mock(SystemSchemaServiceImpl.class));
		Mockito.when(transferRule.getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail")).thenReturn(new SystemTable());

		// apply all rules
		AccountingInternalPositionTransferClosingRealizedBookingRule gainLossRule = new AccountingInternalPositionTransferClosingRealizedBookingRule();
		gainLossRule.setAccountingAccountService(transferRule.getAccountingAccountService());
		AccountingInternalPositionTransferRealizedBookingRule internalGainLoss = new AccountingInternalPositionTransferRealizedBookingRule();
		internalGainLoss.setAccountingAccountService(transferRule.getAccountingAccountService());

		AccountingBookingRuleTestExecutor.newTestForEntity(transfer)
				.forBookingRules(
						transferRule,
						gainLossRule,
						internalGainLoss,
						new AccountingRemoveZeroValueRecordBookingRule<>()
				)
				.withExpectedResults(
						"-10	2753980	100000	H-200	Position	CDU3	97.45	-26	0	08/01/2013	1	0	-2,545,790	06/12/2013	08/01/2013	Position close from transfer to 200000: Test Client Account 2",
						"-12	null	200000	H-200	Position	CDU3	97.915	26	0	08/01/2013	1	0	2,545,790	06/12/2013	08/01/2013	Position opening from transfer from 100000: Test Client Account",
						"-14	2802117	100000	H-200	Position	CDU3	97.45	-18	0	08/01/2013	1	0	-1,717,020	06/27/2013	08/01/2013	Position close from transfer to 200000: Test Client Account 2",
						"-16	null	200000	H-200	Position	CDU3	95.39	18	0	08/01/2013	1	0	1,717,020	06/27/2013	08/01/2013	Position opening from transfer from 100000: Test Client Account",
						"-18	2806026	100000	H-200	Position	CDU3	97.45	-34	0	08/01/2013	1	0	-3,230,208.39	06/28/2013	08/01/2013	Position close from transfer to 200000: Test Client Account 2",
						"-20	null	200000	H-200	Position	CDU3	95.006129	34	0	08/01/2013	1	0	3,230,208.39	06/28/2013	08/01/2013	Position opening from transfer from 100000: Test Client Account",
						"-22	-10	100000	H-200	Realized Gain / Loss	CDU3	97.45	26	-1,168,310	08/01/2013	1	-1,168,310	0	06/12/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-23	-10	100000	H-200	Security Distribution	CDU3	97.45	26	1,168,310	08/01/2013	1	1,168,310	0	06/12/2013	08/01/2013	Gain/Loss distribution from transfer from 100000: Test Client Account",
						"-24	-14	100000	H-200	Realized Gain / Loss	CDU3	97.45	18	-1,997,080	08/01/2013	1	-1,997,080	0	06/27/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-25	-14	100000	H-200	Security Distribution	CDU3	97.45	18	1,997,080	08/01/2013	1	1,997,080	0	06/27/2013	08/01/2013	Gain/Loss distribution from transfer from 100000: Test Client Account",
						"-26	-18	100000	H-200	Realized Gain / Loss	CDU3	97.45	34	-483,891.61	08/01/2013	1	-483,891.61	0	06/28/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-27	-18	100000	H-200	Security Distribution	CDU3	97.45	34	483,891.61	08/01/2013	1	483,891.61	0	06/28/2013	08/01/2013	Gain/Loss distribution from transfer from 100000: Test Client Account",
						"-28	-12	200000	H-200	Realized Gain / Loss	CDU3	97.45	-26	1,168,310	08/01/2013	1	1,168,310	0	06/12/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-29	-12	200000	H-200	Security Contribution	CDU3	97.45	-26	-1,168,310	08/01/2013	1	-1,168,310	0	06/12/2013	08/01/2013	Gain/Loss contribution from transfer from 100000: Test Client Account",
						"-30	-16	200000	H-200	Realized Gain / Loss	CDU3	97.45	-18	1,997,080	08/01/2013	1	1,997,080	0	06/27/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-31	-16	200000	H-200	Security Contribution	CDU3	97.45	-18	-1,997,080	08/01/2013	1	-1,997,080	0	06/27/2013	08/01/2013	Gain/Loss contribution from transfer from 100000: Test Client Account",
						"-32	-20	200000	H-200	Realized Gain / Loss	CDU3	97.45	-34	483,891.61	08/01/2013	1	483,891.61	0	06/28/2013	08/01/2013	Realized Gain / Loss gain for CDU3",
						"-33	-20	200000	H-200	Security Contribution	CDU3	97.45	-34	-483,891.61	08/01/2013	1	-483,891.61	0	06/28/2013	08/01/2013	Gain/Loss contribution from transfer from 100000: Test Client Account"
				)
				.execute();
	}
}
