package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.test.matcher.PropertyMatcher;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public class AccountingPositionTransferCollateralBookingRuleClientTests extends BaseAccountingPositionTransferCollateralBookingRuleTests {


	@Test
	public void testCollateralTransferPost() {
		// verify results
		getTestCollateralTransferPost();
	}


	@Test
	public void testCollateralTransferPost_PARTIAL_OneFactor_AvoidPennyLot() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_COLLATERAL_TRANSFER_POST_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingTransaction = new AccountingTransaction();
		existingTransaction.setJournal(new AccountingJournal());
		existingTransaction.getJournal().setJournalType(new AccountingJournalType());
		existingTransaction.setId(4150252L);
		existingTransaction.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingTransaction.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingTransaction.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingTransaction.setPrice(new BigDecimal("107.1875"));
		existingTransaction.setQuantity(new BigDecimal("1000000"));
		existingTransaction.setExchangeRateToBase(BigDecimal.ONE);
		existingTransaction.setBaseDebitCredit(new BigDecimal("1071875.00"));
		existingTransaction.setLocalDebitCredit(existingTransaction.getBaseDebitCredit());
		existingTransaction.setPositionCostBasis(existingTransaction.getBaseDebitCredit());
		existingTransaction.setExecutingCompany(CITIGROUP);
		existingTransaction.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingTransaction.setOriginalTransactionDate(existingTransaction.getTransactionDate());
		existingTransaction.setSettlementDate(existingTransaction.getTransactionDate());
		existingTransaction.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingTransaction);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingTransaction.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("754664"));
		detail.setQuantity(new BigDecimal("754664"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("808905.48"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingTransaction);
		currentPosition.setRemainingCostBasis(new BigDecimal("1071875.00"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("1000000"));
		currentPositions.add(currentPosition);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingTransaction.getId())))
				.thenReturn(existingTransaction);

		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookTransferJournal(transfer, transferRule,
				"-10	4150252	100000	Custodian	Position	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-11	-13	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-12	4150252	100000	Custodian	Position	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-13	4150252	100000	Counterparty	Position Collateral	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-14	-13	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	-754,664	-808,905.48	05/01/2015	1	-808,905.48	-808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account"
		);

		Assertions.assertEquals(CITIGROUP, journalDetailList.get(0).getExecutingCompany());
		Assertions.assertEquals(CITIGROUP, journalDetailList.get(1).getExecutingCompany());
		Assertions.assertEquals(CITIGROUP, journalDetailList.get(3).getExecutingCompany());
		Assertions.assertEquals(CITIGROUP, journalDetailList.get(4).getExecutingCompany());
	}


	@Test
	public void testCollateralTransferReturn() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		List<AccountingTransaction> postedTransactionList = getPostedTransactions(transfer, security);

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setId(100);
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionList.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(security);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		//detail.setOriginalFace(new BigDecimal("1000000"));
		detail.setQuantity(new BigDecimal("1000000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.hasProperty("journalId", existingPosition.getJournal().getId())))).thenReturn(postedTransactionList);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		bookTransferJournal(transfer, transferRule,
				"-10	1	100000	Custodian	Position	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	null	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-11	20	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	30	100000	Counterparty	Position Collateral	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	40	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account"
		);
	}


	@Test
	public void testCollateralTransferMultipleLots_PARTIAL_Return() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		List<AccountingTransaction> postedTransactionListFirstSet = getPostedTransactions(transfer, security);
		List<AccountingTransaction> postedTransactionListSecondSet = getPostedTransactionsMultipleLots(transfer, security);

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setId(10);
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionListFirstSet.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(security);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("1000000"));
		detail.setQuantity(new BigDecimal("1000000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);


		detail = new AccountingPositionTransferDetail();
		detail.setId(20);
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		existingPosition = postedTransactionListSecondSet.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(security);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("250000"));
		detail.setQuantity(new BigDecimal("250000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("267968.75"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingTransaction> postedTransactionList = new ArrayList<>(postedTransactionListFirstSet);
		postedTransactionList.addAll(postedTransactionListSecondSet);
		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.<AccountingTransactionSearchForm>hasProperty("journalId", existingPosition.getJournal().getId()).andProperty("fkFieldId", 100)))).thenReturn(postedTransactionListFirstSet);
		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.<AccountingTransactionSearchForm>hasProperty("journalId", existingPosition.getJournal().getId()).andProperty("fkFieldId", 1000)))).thenReturn(postedTransactionListSecondSet);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		for (AccountingTransaction transaction : CollectionUtils.getIterable(postedTransactionList)) {
			Mockito.when(transferRule.getAccountingTransactionService()
					.getAccountingTransaction(ArgumentMatchers.eq(transaction.getId())))
					.thenReturn(transaction);
		}

		bookTransferJournal(transfer, transferRule,
				"-10	1	100000	Custodian	Position	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	null	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-11	20	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	30	100000	Counterparty	Position Collateral	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	40	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-14	3	100000	Custodian	Position	38377QSW8	107.1875	250,000	267,968.75	05/01/2015	1	267,968.75	267,968.75	null	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-15	200	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	-500,000	-535,937	05/01/2015	1	-535,937	-535,937	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-16	300	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	250,000	267,968.25	05/01/2015	1	267,968.25	267,968.25	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-17	300	100000	Counterparty	Position Collateral	38377QSW8	107.1875	-500,000	-535,937	05/01/2015	1	-535,937	-535,937	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-18	2	100000	Counterparty	Position Collateral	38377QSW8	107.1875	250,000	267,968.25	05/01/2015	1	267,968.25	267,968.25	null	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-19	400	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	500,000	535,937	05/01/2015	1	535,937	535,937	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-20	300	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	-250,000	-267,968.25	05/01/2015	1	-267,968.25	-267,968.25	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account"
		);
	}


	@Test
	public void testCollateralTransferAssetBacked_PARTIAL_Return() {
		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = testCollateralTransfer_PARTIAL_Return(true);

		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	@Test
	public void testCollateralTransfer_PARTIAL_Return() {
		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = testCollateralTransfer_PARTIAL_Return(false);

		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	private List<? extends AccountingJournalDetailDefinition> testCollateralTransfer_PARTIAL_Return(boolean useOriginalFace) {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		AccountingPositionTransfer openingTransfer = getPostingTransfer();
		Mockito.when(this.accountingPositionTransferService.getAccountingPositionTransferDetail(100)).thenReturn(openingTransfer.getDetailList().get(0));

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		List<AccountingTransaction> postedTransactionList = getPostedTransactionsFromDetails(getTestCollateralTransferPost());

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionList.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(security);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		if (useOriginalFace) {
			detail.setOriginalFace(new BigDecimal("754664"));
		}
		detail.setQuantity(new BigDecimal("754664"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("808905.48"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);


		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.<AccountingTransactionSearchForm>hasProperty("journalId", existingPosition.getJournal().getId()).andProperty("fkFieldId", 100)))).thenReturn(postedTransactionList);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		for (AccountingTransaction transaction : CollectionUtils.getIterable(postedTransactionList)) {
			Mockito.when(transferRule.getAccountingTransactionService()
					.getAccountingTransaction(ArgumentMatchers.eq(transaction.getId())))
					.thenReturn(transaction);
		}

		String[] expectedResults = useOriginalFace ? new String[]{
				"-10	4150252	100000	Custodian	Position	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-11	11	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	13	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-13	13	100000	Counterparty	Position Collateral	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-14	4150252	100000	Counterparty	Position Collateral	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-15	14	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-16	13	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	-245,336	-262,969.52	05/01/2015	1	-262,969.52	-262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account"
		} : new String[]{
				"-10	4150252	100000	Custodian	Position	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-11	11	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	-754,664	-808,905.48	05/01/2015	1	-808,905.48	-808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	13	100000	Counterparty	Position Collateral	38377QSW8	107.1875	-754,664	-808,905.48	05/01/2015	1	-808,905.48	-808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	14	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account"
		};

		return bookTransferJournal(transfer, transferRule, expectedResults);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<? extends AccountingJournalDetailDefinition> getTestCollateralTransferPost() {
		AccountingPositionTransfer transfer = getPostingTransfer();

		AccountingTransaction existingTransaction = transfer.getDetailList().get(0).getExistingPosition();

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingTransaction);
		currentPosition.setRemainingCostBasis(new BigDecimal("1071875.00"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("1000000"));
		currentPositions.add(currentPosition);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingTransaction.getId())))
				.thenReturn(existingTransaction);

		return bookTransferJournal(transfer, transferRule,
				"-10	4150252	100000	Custodian	Position	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-11	-13	100000	Custodian	Position Collateral Receivable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-13	4150252	100000	Counterparty	Position Collateral	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
				"-14	-13	100000	Counterparty	Position Collateral Payable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account"
		);
	}


	private AccountingPositionTransfer getPostingTransfer() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_COLLATERAL_TRANSFER_POST_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setId(100);
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(4150252L);
		existingPosition.setOpening(true);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("1000000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("1071875.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("1000000"));
		detail.setQuantity(new BigDecimal("1000000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		return transfer;
	}


	private List<AccountingTransaction> getPostedTransactions(AccountingPositionTransfer transfer, InvestmentSecurity security) {
		List<AccountingTransaction> result = new ArrayList<>();

		SystemTable table = new SystemTable();
		table.setId((short) 176);
		table.setName("AccountingPositionTransferDetail");

		AccountingTransaction originalParentTransaction = new AccountingTransaction();
		originalParentTransaction.setId(1L);
		originalParentTransaction.setOpening(true);
		originalParentTransaction.setInvestmentSecurity(security);

		AccountingJournal journal = new AccountingJournal();
		journal.setId(200L);

		AccountingTransaction closingPosition = new AccountingTransaction();
		closingPosition.setJournal(journal);
		closingPosition.getJournal().setJournalType(new AccountingJournalType());
		closingPosition.setOpening(false);
		closingPosition.setParentTransaction(originalParentTransaction);
		closingPosition.setId(10L);
		closingPosition.setInvestmentSecurity(security);
		closingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		closingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		closingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
		closingPosition.setPrice(new BigDecimal("107.1875"));
		closingPosition.setQuantity(new BigDecimal("-1000000"));
		closingPosition.setExchangeRateToBase(BigDecimal.ONE);
		closingPosition.setBaseDebitCredit(new BigDecimal("-1071875.00"));
		closingPosition.setLocalDebitCredit(closingPosition.getBaseDebitCredit());
		closingPosition.setPositionCostBasis(closingPosition.getBaseDebitCredit());
		closingPosition.setExecutingCompany(CITIGROUP);
		closingPosition.setSystemTable(table);
		closingPosition.setFkFieldId(100);
		closingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		closingPosition.setOriginalTransactionDate(closingPosition.getTransactionDate());
		closingPosition.setSettlementDate(closingPosition.getTransactionDate());
		closingPosition.setInvestmentSecurity(security);
		result.add(closingPosition);


		AccountingTransaction openingPosition = new AccountingTransaction();
		openingPosition.setJournal(journal);
		openingPosition.getJournal().setJournalType(new AccountingJournalType());
		openingPosition.setOpening(true);
		openingPosition.setParentTransaction(originalParentTransaction);
		openingPosition.setId(30L);
		openingPosition.setInvestmentSecurity(security);
		openingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		openingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		openingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL));
		openingPosition.setPrice(new BigDecimal("107.1875"));
		openingPosition.setQuantity(new BigDecimal("1000000"));
		openingPosition.setExchangeRateToBase(BigDecimal.ONE);
		openingPosition.setBaseDebitCredit(new BigDecimal("1071875.00"));
		openingPosition.setLocalDebitCredit(openingPosition.getBaseDebitCredit());
		openingPosition.setPositionCostBasis(openingPosition.getBaseDebitCredit());
		openingPosition.setExecutingCompany(CITIGROUP);
		openingPosition.setSystemTable(table);
		openingPosition.setFkFieldId(100);
		openingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		openingPosition.setOriginalTransactionDate(openingPosition.getTransactionDate());
		openingPosition.setSettlementDate(openingPosition.getTransactionDate());
		openingPosition.setInvestmentSecurity(security);

		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(20L);
		existingPosition.setInvestmentSecurity(security);
		existingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("1000000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("1071875.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(100);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(security);
		result.add(existingPosition);
		result.add(openingPosition); // add the opening here to keep the typical order

		existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(40L);
		existingPosition.setInvestmentSecurity(security);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("-1000000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("-1071875.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(100);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(security);
		result.add(existingPosition);

		return result;
	}


	private List<AccountingTransaction> getPostedTransactionsMultipleLots(AccountingPositionTransfer transfer, InvestmentSecurity security) {
		List<AccountingTransaction> result = new ArrayList<>();

		SystemTable table = new SystemTable();
		table.setId((short) 176);
		table.setName("AccountingPositionTransferDetail");

		AccountingTransaction originalParentParentTransaction = new AccountingTransaction();
		originalParentParentTransaction.setId(3L);
		originalParentParentTransaction.setOpening(true);
		originalParentParentTransaction.setInvestmentSecurity(security);


		AccountingTransaction originalParentTransaction = new AccountingTransaction();
		originalParentTransaction.setId(2L);
		originalParentTransaction.setParentTransaction(originalParentParentTransaction);
		originalParentTransaction.setOpening(true);
		originalParentTransaction.setInvestmentSecurity(security);

		AccountingJournal journal = new AccountingJournal();
		journal.setId(200L);

		AccountingTransaction closingPosition = new AccountingTransaction();
		closingPosition.setJournal(journal);
		closingPosition.getJournal().setJournalType(new AccountingJournalType());
		closingPosition.setOpening(false);
		closingPosition.setParentTransaction(originalParentTransaction);
		closingPosition.setId(100L);
		closingPosition.setInvestmentSecurity(security);
		closingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		closingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		closingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION));
		closingPosition.setPrice(new BigDecimal("107.1875"));
		closingPosition.setQuantity(new BigDecimal("-500000"));
		closingPosition.setExchangeRateToBase(BigDecimal.ONE);
		closingPosition.setBaseDebitCredit(new BigDecimal("-535937.50"));
		closingPosition.setLocalDebitCredit(closingPosition.getBaseDebitCredit());
		closingPosition.setPositionCostBasis(closingPosition.getBaseDebitCredit());
		closingPosition.setExecutingCompany(CITIGROUP);
		closingPosition.setSystemTable(table);
		closingPosition.setFkFieldId(1000);
		closingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		closingPosition.setOriginalTransactionDate(closingPosition.getTransactionDate());
		closingPosition.setSettlementDate(closingPosition.getTransactionDate());
		result.add(closingPosition);

		AccountingTransaction openingPosition = new AccountingTransaction();
		openingPosition.setJournal(journal);
		openingPosition.getJournal().setJournalType(new AccountingJournalType());
		openingPosition.setOpening(true);
		openingPosition.setParentTransaction(originalParentTransaction);
		openingPosition.setId(300L);
		openingPosition.setInvestmentSecurity(security);
		openingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		openingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		openingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL));
		openingPosition.setPrice(new BigDecimal("107.1875"));
		openingPosition.setQuantity(new BigDecimal("500000"));
		openingPosition.setExchangeRateToBase(BigDecimal.ONE);
		openingPosition.setBaseDebitCredit(new BigDecimal("535937.00"));
		openingPosition.setLocalDebitCredit(openingPosition.getBaseDebitCredit());
		openingPosition.setPositionCostBasis(openingPosition.getBaseDebitCredit());
		openingPosition.setExecutingCompany(CITIGROUP);
		openingPosition.setSystemTable(table);
		openingPosition.setFkFieldId(1000);
		openingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		openingPosition.setOriginalTransactionDate(openingPosition.getTransactionDate());
		openingPosition.setSettlementDate(openingPosition.getTransactionDate());

		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(200L);
		existingPosition.setInvestmentSecurity(security);
		existingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("500000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("535937.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(1000);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		result.add(existingPosition);
		result.add(openingPosition); // add the opening here to keep the typical order


		existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(400L);
		existingPosition.setInvestmentSecurity(security);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("-500000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("-535937.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(1000);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		result.add(existingPosition);

		return result;
	}
}
