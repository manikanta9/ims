package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.system.schema.SystemSchemaServiceImpl;
import com.clifton.system.schema.SystemTable;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public class AccountingPositionTransferCashCollateralBookingRuleTests {

	private static BusinessCompany CITIGROUP;


	static {
		CITIGROUP = new BusinessCompany();
		CITIGROUP.setId(101);
		CITIGROUP.setName("Citibank N.A.");
	}


	@Test
	public void testCashCollateralTransferPost() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_CASH_COLLATERAL_TRANSFER_POST_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);

		detail.setSecurity(InvestmentSecurityBuilder.newUSD());
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("1000000.00"));
		detail.setOriginalPositionOpenDate(transfer.getTransactionDate());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferCollateralBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();
		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(4, journalDetailList.size());


		Assertions.assertEquals("-10	null	100000	Custodian	Cash	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	Custodian	Cash Collateral Receivable	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	Counterparty	Cash Collateral	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	100000	Counterparty	Cash Collateral Payable	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));

		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	@Test
	public void testCashCollateralTransferReturn() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.CLIENT_CASH_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);

		detail.setSecurity(InvestmentSecurityBuilder.newUSD());
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("1000000.00"));
		detail.setOriginalPositionOpenDate(transfer.getTransactionDate());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferCollateralBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();
		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(4, journalDetailList.size());


		Assertions.assertEquals("-10	null	100000	Custodian	Cash	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	Custodian	Cash Collateral Receivable	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	Counterparty	Cash Collateral	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	100000	Counterparty	Cash Collateral Payable	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));

		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	@Test
	public void testCounterpartyCashCollateralTransferPost() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_POST_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);

		detail.setSecurity(InvestmentSecurityBuilder.newUSD());
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("1000000.00"));
		detail.setOriginalPositionOpenDate(transfer.getTransactionDate());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferCollateralBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();
		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(4, journalDetailList.size());


		Assertions.assertEquals("-10	null	100000	Counterparty	Counterparty Cash Collateral	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	Counterparty	Cash Collateral Payable	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	Custodian	Counterparty Cash	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	100000	Custodian	Cash Collateral Receivable	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));


		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	@Test
	public void testCounterpartyCashCollateralTransferReturn() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_CASH_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);

		detail.setSecurity(InvestmentSecurityBuilder.newUSD());
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("1000000.00"));
		detail.setOriginalPositionOpenDate(transfer.getTransactionDate());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferCollateralBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();
		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(4, journalDetailList.size());


		Assertions.assertEquals("-10	null	100000	Counterparty	Counterparty Cash Collateral	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	Counterparty	Cash Collateral Payable	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	Custodian	Counterparty Cash	USD			-1,000,000	05/01/2015	1	-1,000,000	0	05/01/2015	05/01/2015	Cash distribution from transfer to 100000: Test Client Account", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	100000	Custodian	Cash Collateral Receivable	USD			1,000,000	05/01/2015	1	1,000,000	0	05/01/2015	05/01/2015	Cash contribution from transfer from 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));


		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			Assertions.assertEquals(CITIGROUP, journalDetail.getExecutingCompany());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccount newInvestmentAccount_Custodian() {
		InvestmentAccount result = new InvestmentAccount(200, "Custodian");
		result.setNumber("Custodian");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setIssuingCompany(CITIGROUP);
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	public static InvestmentAccount newInvestmentAccount_Counterparty() {
		InvestmentAccount result = new InvestmentAccount(210, "Counterparty");
		result.setNumber("Counterparty");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Goldman Sachs");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	private AccountingPositionTransferCollateralBookingRule newAccountingPositionTransferCollateralBookingRule() {
		AccountingPositionTransferCollateralBookingRule transferRule = new AccountingPositionTransferCollateralBookingRule();
		transferRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		transferRule.setInvestmentCalculator(new InvestmentCalculatorImpl());
		transferRule.setSystemSchemaService(Mockito.mock(SystemSchemaServiceImpl.class));
		Mockito.when(transferRule.getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail")).thenReturn(new SystemTable());
		transferRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		return transferRule;
	}
}
