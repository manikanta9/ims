package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.gl.booking.AccountingBookingInMemoryDBTestExecutor;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


@AccountingInMemoryDatabaseContext
public class AccountingPositionTransferCollateralReturnBookingRuleDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private AccountingBookingService<AccountingPositionTransfer> accountingBookingService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUser(userTemplate.getId());
		if (user == null) {
			this.securityUserService.saveSecurityUser(userTemplate);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testReturnTransferOverClose() {
		AccountingPositionTransfer transfer = this.accountingPositionTransferService.getAccountingPositionTransfer(20191);

		AccountingBookingInMemoryDBTestExecutor.newTestForEntity(AccountingPositionTransferBookingProcessor.JOURNAL_NAME, transfer, this.accountingBookingService)
				.withExpectedResults(
						"1	8053679	297150	DMR06	Position	912828B33	99.5703125	2,630,000	2,618,699.22	02/21/2018	1	2,618,699.22	2,618,699.22	02/01/2018	02/22/2018	Position opening from transfer from 297150: GuideStone - DMS-DE",
						"2	8057866	297150	DMR06	Position Collateral Receivable	912828B33	99.5703125	-2,630,000	-2,618,699.22	02/21/2018	1	-2,618,699.22	-2,618,699.22	02/01/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"3	8057867	297150	033-197450	Position Collateral	912828B33	99.5703125	-2,630,000	-2,618,699.22	02/21/2018	1	-2,618,699.22	-2,618,699.22	02/01/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"4	8057868	297150	033-197450	Position Collateral Payable	912828B33	99.5703125	2,630,000	2,618,699.22	02/21/2018	1	2,618,699.22	2,618,699.22	02/01/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"5	8011919	297150	DMR06	Position	912828WD8	99.59375	2,546,000	2,535,656.87	02/21/2018	1	2,535,656.87	2,535,656.87	01/24/2018	02/22/2018	Position opening from transfer from 297150: GuideStone - DMS-DE",
						"6	8018843	297150	DMR06	Position Collateral Receivable	912828WD8	99.59375	-2,546,000	-2,535,656.87	02/21/2018	1	-2,535,656.87	-2,535,656.87	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"7	8018844	297150	033-197450	Position Collateral	912828WD8	99.59375	-2,546,000	-2,535,656.87	02/21/2018	1	-2,535,656.87	-2,535,656.87	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"8	8018845	297150	033-197450	Position Collateral Payable	912828WD8	99.59375	2,546,000	2,535,656.87	02/21/2018	1	2,535,656.87	2,535,656.87	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"9	8018414	297150	DMR06	Position	912828WD8	99.6523437	14,729,000	14,677,793.71	02/21/2018	1	14,677,793.71	14,677,793.71	01/24/2018	02/22/2018	Position opening from transfer from 297150: GuideStone - DMS-DE",
						"10	8018847	297150	DMR06	Position Collateral Receivable	912828WD8	99.6523437	-14,729,000	-14,677,793.71	02/21/2018	1	-14,677,793.71	-14,677,793.71	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"11	8018848	297150	033-197450	Position Collateral	912828WD8	99.6523437	-14,729,000	-14,677,793.71	02/21/2018	1	-14,677,793.71	-14,677,793.71	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE",
						"12	8018849	297150	033-197450	Position Collateral Payable	912828WD8	99.6523437	14,729,000	14,677,793.71	02/21/2018	1	14,677,793.71	14,677,793.71	01/24/2018	02/22/2018	Position close from transfer to 297150: GuideStone - DMS-DE"
				)
				.execute();
	}
}
