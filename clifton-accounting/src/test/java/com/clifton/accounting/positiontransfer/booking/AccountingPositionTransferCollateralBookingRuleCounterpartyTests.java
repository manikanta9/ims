package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.test.matcher.PropertyMatcher;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public class AccountingPositionTransferCollateralBookingRuleCounterpartyTests extends BaseAccountingPositionTransferCollateralBookingRuleTests {


	@Test
	public void testCounterpartyCollateralTransferPost() {
		// verify results
		getTestCounterpartyCollateralTransferPost();
	}


	@Test
	public void testCounterpartyCollateralTransferReturn() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		List<AccountingTransaction> postedTransactionList = getCounterpartyPostedTransactions(transfer, investmentSecurity);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionList.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		//detail.setOriginalFace(new BigDecimal("-1000000"));
		detail.setQuantity(new BigDecimal("-1000000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("-1071875.00"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);


		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.hasProperty("journalId", existingPosition.getJournal().getId())))).thenReturn(postedTransactionList);
		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		bookTransferJournal(transfer, transferRule,
				"-10	10	100000	Custodian	Counterparty Position Collateral	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-11	20	100000	Custodian	Position Collateral Payable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	30	100000	Counterparty	Counterparty Position	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	40	100000	Counterparty	Position Collateral Receivable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account"
		);
	}


	@Test
	public void testCounterpartyCollateralTransferAssetBacked_PARTIAL_Return() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		investmentSecurity.getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		List<AccountingTransaction> postedTransactionList = getCounterpartyPostedTransactions(transfer, investmentSecurity);


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionList.get(2);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("-754664"));
		detail.setQuantity(new BigDecimal("-754664"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("-808905.48"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);


		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.hasProperty("journalId", existingPosition.getJournal().getId())))).thenReturn(postedTransactionList);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		for (AccountingTransaction transaction : CollectionUtils.getIterable(postedTransactionList)) {
			Mockito.when(transferRule.getAccountingTransactionService()
					.getAccountingTransaction(ArgumentMatchers.eq(transaction.getId())))
					.thenReturn(transaction);
		}

		bookTransferJournal(transfer, transferRule,
				"-10	10	100000	Custodian	Counterparty Position Collateral	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-11	2	100000	Custodian	Counterparty Position Collateral	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	null	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-12	20	100000	Custodian	Position Collateral Payable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	30	100000	Custodian	Position Collateral Payable	38377QSW8	107.1875	-245,336	-262,969.52	05/01/2015	1	-262,969.52	-262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-14	30	100000	Counterparty	Counterparty Position	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-15	2	100000	Counterparty	Counterparty Position	38377QSW8	107.1875	-245,336	-262,969.52	05/01/2015	1	-262,969.52	-262,969.52	null	05/01/2015	Position opening from transfer to 100000: Test Client Account",
				"-16	40	100000	Counterparty	Position Collateral Receivable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-17	30	100000	Counterparty	Position Collateral Receivable	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 100000: Test Client Account"
		);
	}


	@Test
	public void testCounterpartyCollateralTransfer_PARTIAL_Return() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_COLLATERAL_TRANSFER_RETURN_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		AccountingPositionTransfer openingTransfer = getPostingCounterpartyTransfer();
		Mockito.when(this.accountingPositionTransferService.getAccountingPositionTransferDetail(100)).thenReturn(openingTransfer.getDetailList().get(0));

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");
		List<AccountingTransaction> postedTransactionList = getPostedTransactionsFromDetails(getTestCounterpartyCollateralTransferPost());


		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		// get the position collateral transaction
		AccountingTransaction existingPosition = postedTransactionList.get(0);

		detail.setExistingPosition(existingPosition);

		detail.setSecurity(security);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		//detail.setOriginalFace(new BigDecimal("-754664"));
		detail.setQuantity(new BigDecimal("754664"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("808905.48"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();

		List<AccountingPosition> currentPositions = getCurrentPositionListFromPostedTransactions(postedTransactionList);


		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransactionList(Mockito.argThat(
						PropertyMatcher.<AccountingTransactionSearchForm>hasProperty("journalId", existingPosition.getJournal().getId()).andProperty("fkFieldId", 100)))).thenReturn(postedTransactionList);

		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		bookTransferJournal(transfer, transferRule,
				"-10	10	100000	Custodian	Counterparty Position Collateral	38377QSW8	107.1875	-754,664	-808,905.48	05/01/2015	1	-808,905.48	-808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-11	11	100000	Custodian	Position Collateral Payable	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-12	12	100000	Counterparty	Counterparty Position	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account",
				"-13	13	100000	Counterparty	Position Collateral Receivable	38377QSW8	107.1875	-754,664	-808,905.48	05/01/2015	1	-808,905.48	-808,905.48	02/27/2015	05/01/2015	Position close from transfer to 100000: Test Client Account"
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<? extends AccountingJournalDetailDefinition> getTestCounterpartyCollateralTransferPost() {
		AccountingPositionTransfer transfer = getPostingCounterpartyTransfer();


		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferCollateralBookingRule();
		Mockito.when(transferRule.getAccountingPositionService()
				.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(new ArrayList<>());

		return AccountingBookingRuleTestExecutor.newTestForEntity(transfer)
				.forBookingRule(transferRule)
				.withExpectedResults(
						"-10	null	100000	Custodian	Counterparty Position Collateral	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
						"-11	null	100000	Custodian	Position Collateral Payable	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
						"-12	null	100000	Counterparty	Counterparty Position	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account",
						"-13	null	100000	Counterparty	Position Collateral Receivable	38377QSW8	107.1875	1,000,000	1,071,875	05/01/2015	1	1,071,875	1,071,875	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account"
				)
				.execute();
	}


	private AccountingPositionTransfer getPostingCounterpartyTransfer() {

		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.COUNTERPARTY_COLLATERAL_TRANSFER_POST_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(newInvestmentAccount_Counterparty());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(newInvestmentAccount_Custodian());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));
		transfer.getFromHoldingInvestmentAccount().setIssuingCompany(CITIGROUP);

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setId(100);
		detail.setPositionTransfer(transfer);

		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD"));
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(new BigDecimal("107.1875"));
		detail.setTransferPrice(new BigDecimal("107.1875"));
		//detail.setOriginalFace(new BigDecimal("1000000"));
		detail.setQuantity(new BigDecimal("1000000"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("1071875.00"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		return transfer;
	}


	private List<AccountingTransaction> getCounterpartyPostedTransactions(AccountingPositionTransfer transfer, InvestmentSecurity investmentSecurity) {
		List<AccountingTransaction> result = new ArrayList<>();


		BigDecimal quantity = new BigDecimal("1000000");
		BigDecimal baseDebitCredit = new BigDecimal("1071875.00");

		SystemTable table = new SystemTable();
		table.setId((short) 176);
		table.setName("AccountingPositionTransferDetail");

		AccountingTransaction originalParentTransaction = new AccountingTransaction();
		originalParentTransaction.setId(2L);
		originalParentTransaction.setOpening(true);
		originalParentTransaction.setInvestmentSecurity(investmentSecurity);

		AccountingJournal journal = new AccountingJournal();
		journal.setId(200L);
		journal.setFkFieldId(100);
		journal.setJournalType(new AccountingJournalType());

		AccountingTransaction closingPosition = new AccountingTransaction();
		closingPosition.setJournal(journal);
		closingPosition.setOpening(true);
		closingPosition.setParentTransaction(originalParentTransaction);
		closingPosition.setId(10L);
		closingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		closingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		closingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION_COLLATERAL));
		closingPosition.setPrice(new BigDecimal("107.1875"));
		closingPosition.setQuantity(quantity);
		closingPosition.setExchangeRateToBase(BigDecimal.ONE);
		closingPosition.setBaseDebitCredit(baseDebitCredit);
		closingPosition.setLocalDebitCredit(closingPosition.getBaseDebitCredit());
		closingPosition.setPositionCostBasis(closingPosition.getBaseDebitCredit());
		closingPosition.setExecutingCompany(CITIGROUP);
		closingPosition.setSystemTable(table);
		closingPosition.setFkFieldId(100);
		closingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		closingPosition.setOriginalTransactionDate(closingPosition.getTransactionDate());
		closingPosition.setSettlementDate(closingPosition.getTransactionDate());
		closingPosition.setInvestmentSecurity(investmentSecurity);
		result.add(closingPosition);

		AccountingTransaction openingPosition = new AccountingTransaction();
		openingPosition.setJournal(journal);
		openingPosition.setOpening(true);
		openingPosition.setParentTransaction(originalParentTransaction);
		openingPosition.setId(30L);
		openingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		openingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		openingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.COUNTERPARTY_POSITION));
		openingPosition.setPrice(new BigDecimal("107.1875"));
		openingPosition.setQuantity(quantity.negate());
		openingPosition.setExchangeRateToBase(BigDecimal.ONE);
		openingPosition.setBaseDebitCredit(baseDebitCredit.negate());
		openingPosition.setLocalDebitCredit(openingPosition.getBaseDebitCredit());
		openingPosition.setPositionCostBasis(openingPosition.getBaseDebitCredit());
		openingPosition.setExecutingCompany(CITIGROUP);
		openingPosition.setSystemTable(table);
		openingPosition.setFkFieldId(100);
		openingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		openingPosition.setOriginalTransactionDate(openingPosition.getTransactionDate());
		openingPosition.setSettlementDate(openingPosition.getTransactionDate());
		openingPosition.setInvestmentSecurity(investmentSecurity);

		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(20L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_PAYABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(result.get(0).getQuantity().negate());
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(result.get(0).getBaseDebitCredit().negate());
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(100);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		result.add(existingPosition);
		result.add(openingPosition);  // add the opening here to keep the typical order

		existingPosition = new AccountingTransaction();
		existingPosition.setJournal(journal);
		existingPosition.setOpening(true);
		existingPosition.setParentTransaction(openingPosition);
		existingPosition.setId(40L);
		existingPosition.setClientInvestmentAccount(transfer.getToClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getToHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE));
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(result.get(2).getQuantity().negate());
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(result.get(2).getBaseDebitCredit().negate());
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setExecutingCompany(CITIGROUP);
		existingPosition.setSystemTable(table);
		existingPosition.setFkFieldId(100);
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		result.add(existingPosition);

		return result;
	}
}
