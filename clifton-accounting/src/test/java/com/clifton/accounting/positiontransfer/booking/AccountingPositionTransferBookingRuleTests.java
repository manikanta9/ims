package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaServiceImpl;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class AccountingPositionTransferBookingRuleTests {


	@Test
	public void testFutureTransferTo_AtCost() {
		// create test transfer
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.TO_TRANSFER_TYPE);
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setTransactionDate(DateUtils.toDate("05/09/2012"));
		transfer.setSettlementDate(DateUtils.toDate("05/09/2012"));

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(16857, "PTM2", InvestmentType.FUTURES, new BigDecimal("1"), "CAD"));
		detail.setCostPrice(new BigDecimal("10"));
		detail.setTransferPrice(new BigDecimal("10"));
		detail.setQuantity(new BigDecimal("200"));
		detail.setPositionCostBasis(new BigDecimal("2000"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("04/09/2012"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(2, journalDetailList.size());
		Assertions.assertEquals("-10	null	100000	100000	Position	PTM2	10	200	0	05/09/2012			2,000	04/09/2012	05/09/2012	Position opening from transfer from external account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	100000	Security Contribution	PTM2	10	200	0	05/09/2012			0	04/09/2012	05/09/2012	Equity security contribution from transfer from external account", journalDetailList.get(1).toStringFormatted(false));
	}


	@Test
	public void testFutureTransferBetween_NotAtCost() {
		// create test transfer
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.BETWEEN_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		transfer.setTransactionDate(DateUtils.toDate("09/19/2013"));
		transfer.setSettlementDate(DateUtils.toDate("09/19/2013"));

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(19320, "ESZ3", InvestmentType.FUTURES, new BigDecimal("50"), "USD"));
		detail.setCostPrice(new BigDecimal("1678.3"));
		detail.setTransferPrice(new BigDecimal("1717.8"));
		detail.setQuantity(new BigDecimal("625"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("53681250.00"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("09/13/2013"));
		detail.setExistingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2964026, detail.getSecurity())
				.qty(625).price(detail.getCostPrice()).on(detail.getOriginalPositionOpenDate()).build());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(detail.getExistingPosition()));
		Mockito.when(transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(existingPositions);

		transferRule.applyRule(bookingSession);
		AccountingTestObjectFactory.<AccountingPositionTransfer>newAccountingRealizedGainLossBookingRule().applyRule(bookingSession);
		AccountingTestObjectFactory.<AccountingPositionTransfer>newRemoveZeroValueRecordBookingRule().applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(4, journalDetailList.size());
		Assertions.assertEquals("-10	2964026	100000	H-200	Position	ESZ3	1,717.8	-625	0	09/19/2013	1	0	-52,446,875	09/13/2013	09/19/2013	Position close from transfer to 200000: Test Client Account 2", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-12	null	200000	H-201	Position	ESZ3	1,717.8	625	0	09/19/2013	1	0	53,681,250	09/13/2013	09/19/2013	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-14	-10	100000	H-200	Realized Gain / Loss	ESZ3	1,717.8	625	-1,234,375	09/19/2013	1	-1,234,375	0	09/13/2013	09/19/2013	Realized Gain / Loss gain for ESZ3", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-15	-10	100000	H-200	Cash	USD			1,234,375	09/19/2013	1	1,234,375	0	09/13/2013	09/19/2013	Cash proceeds from close of ESZ3", journalDetailList.get(3).toStringFormatted(false));
	}


	@Test
	public void testInterestRateSwapTransferTo_Contribution() {
		// IRS have no payment on open but transferring a valuable asset: generate Contribution and Unrealized Gain / Loss which will be reversed on close 
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.TO_TRANSFER_TYPE);
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setTransactionDate(DateUtils.toDate("10/31/2011"));
		transfer.setSettlementDate(DateUtils.toDate("10/31/2011"));

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18006, "887822N", InvestmentType.SWAPS, new BigDecimal("0.01"), "USD"));
		detail.setCostPrice(new BigDecimal("100"));
		detail.setTransferPrice(new BigDecimal("99.8608368177"));
		detail.setQuantity(new BigDecimal("473000.00"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("472341.76"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("10/07/2011"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(3, journalDetailList.size());
		Assertions.assertEquals("-10	null	100000	100000	Position	887822N	100	473,000	0	10/31/2011	1	0	473,000	10/07/2011	10/31/2011	Position opening from transfer from external account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100000	100000	Security Contribution	887822N	99.8608368177	473,000	658.24	10/31/2011	1	658.24	0	10/07/2011	10/31/2011	Equity security contribution from transfer from external account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	-10	100000	100000	Unrealized Gain / Loss	887822N	99.8608368177	473,000	-658.24	10/31/2011	1	-658.24	0	10/07/2011	10/31/2011	Unrealized Gain / Loss from security contribution from transfer from external account", journalDetailList.get(2).toStringFormatted(false));
	}


	@Test
	public void testBondTransferFrom_WithGains() {
		// create test transfer
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.FROM_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setTransactionDate(DateUtils.toDate("6/19/2012"));
		transfer.setSettlementDate(DateUtils.toDate("6/19/2012"));

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(17181, "912803BL6", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(1711041L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("64.847"));
		existingPosition.setQuantity(new BigDecimal("16943000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("10987027.21"));
		existingPosition.setLocalDebitCredit(new BigDecimal("10987027.21"));
		existingPosition.setPositionCostBasis(new BigDecimal("10987027.21"));
		existingPosition.setTransactionDate(DateUtils.toDate("02/29/2012"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("70.08"));
		detail.setQuantity(new BigDecimal("14097000.00"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("9879177.60"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/29/2012"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingPosition);
		currentPosition.setRemainingCostBasis(existingPosition.getPositionCostBasis());
		currentPosition.setRemainingBaseDebitCredit(existingPosition.getBaseDebitCredit());
		currentPosition.setRemainingQuantity(existingPosition.getQuantity());
		currentPositions.add(currentPosition);
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(2, journalDetailList.size());
		Assertions.assertEquals("-10	1711041	100000	100000	Position	912803BL6	70.08	-14,097,000	-9,879,177.6	06/19/2012	1	-9,879,177.6	-9,879,177.6	02/29/2012	06/19/2012	Position close from transfer to external account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	1711041	100000	100000	Security Distribution	912803BL6	70.08	-14,097,000	9,879,177.6	06/19/2012	1	9,879,177.6	0	02/29/2012	06/19/2012	Equity security distribution form transfer to external account", journalDetailList.get(1).toStringFormatted(false));

		// now verify gain/loss calculation
		AccountingRealizedGainLossBookingRule<AccountingPositionTransfer> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();
		gainLossRule.applyRule(bookingSession);
		journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(3, journalDetailList.size());
		Assertions.assertEquals("-10	1711041	100000	100000	Position	912803BL6	70.08	-14,097,000	-9,141,481.59	06/19/2012	1	-9,141,481.59	-9,141,481.59	02/29/2012	06/19/2012	Position close from transfer to external account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	1711041	100000	100000	Security Distribution	912803BL6	70.08	-14,097,000	9,879,177.6	06/19/2012	1	9,879,177.6	0	02/29/2012	06/19/2012	Equity security distribution form transfer to external account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	-10	100000	100000	Realized Gain / Loss	912803BL6	70.08	14,097,000	-737,696.01	06/19/2012	1	-737,696.01	0	02/29/2012	06/19/2012	Realized Gain / Loss gain for 912803BL6", journalDetailList.get(2).toStringFormatted(false));
	}


	@Test
	public void testFutureTransfer_INTERNAL() {
		// create internal transfer: same holding account but to a different client account
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.INTERNAL_TRANSFER_TYPE);
		transfer.setTransactionDate(DateUtils.toDate("10/11/2012"));
		transfer.setSettlementDate(DateUtils.toDate("10/11/2012"));

		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());

		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.getToClientInvestmentAccount().setId(101);
		transfer.getToClientInvestmentAccount().setNumber("100001");
		transfer.setToHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());

		// existing domestic future position: leave gains in old client account, open at cost in new client account and immediately reduce gains
		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		detail.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17300, "ESZ2", InvestmentType.FUTURES, new BigDecimal(50), "USD"));
		detail.setCostPrice(new BigDecimal("1450.5"));
		detail.setTransferPrice(new BigDecimal("1428.5"));
		detail.setQuantity(new BigDecimal("52"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("3771300.00"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("09/14/2012"));
		detail.setExistingPosition(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2171928, detail.getSecurity())
				.qty(190).price(detail.getCostPrice()).on(detail.getOriginalPositionOpenDate()).build());
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(AccountingTestObjectFactory.newAccountingPosition(detail.getExistingPosition()));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(existingPositions);

		// apply all rules
		transferRule.applyRule(bookingSession);
		AccountingInternalPositionTransferClosingRealizedBookingRule gainLossRule = new AccountingInternalPositionTransferClosingRealizedBookingRule();
		gainLossRule.setAccountingAccountService(transferRule.getAccountingAccountService());
		gainLossRule.applyRule(bookingSession);

		AccountingInternalPositionTransferRealizedBookingRule internalGainLoss = new AccountingInternalPositionTransferRealizedBookingRule();
		internalGainLoss.setAccountingAccountService(transferRule.getAccountingAccountService());
		internalGainLoss.applyRule(bookingSession);
		new AccountingRemoveZeroValueRecordBookingRule<AccountingPositionTransfer>().applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(6, journalDetailList.size());
		Assertions.assertEquals("-10	2171928	100000	H-200	Position	ESZ2	1,428.5	-52	0	10/11/2012	1	0	-3,771,300	09/14/2012	10/11/2012	Position close from transfer to 100001: Test Client Account", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100001	H-200	Position	ESZ2	1,450.5	52	0	10/11/2012	1	0	3,771,300	09/14/2012	10/11/2012	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-14	-10	100000	H-200	Realized Gain / Loss	ESZ2	1,428.5	52	57,200	10/11/2012	1	57,200	0	09/14/2012	10/11/2012	Realized Gain / Loss loss for ESZ2", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-15	-10	100000	H-200	Security Distribution	ESZ2	1,428.5	52	-57,200	10/11/2012	1	-57,200	0	09/14/2012	10/11/2012	Gain/Loss distribution from transfer from 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));
		Assertions.assertEquals("-16	-12	100001	H-200	Realized Gain / Loss	ESZ2	1,428.5	-52	-57,200	10/11/2012	1	-57,200	0	09/14/2012	10/11/2012	Realized Gain / Loss loss for ESZ2", journalDetailList.get(4).toStringFormatted(false));
		Assertions.assertEquals("-17	-12	100001	H-200	Security Contribution	ESZ2	1,428.5	-52	57,200	10/11/2012	1	57,200	0	09/14/2012	10/11/2012	Gain/Loss contribution from transfer from 100000: Test Client Account", journalDetailList.get(5).toStringFormatted(false));
	}


	@Test
	public void testABSTransfer_PARTIAL_NonOneFactor() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.BETWEEN_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		transfer.setTransactionDate(DateUtils.toDate("03/02/2015"));
		transfer.setSettlementDate(DateUtils.toDate("03/02/2015"));

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(23652, "3137A2C74", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(3394839L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("100.40625"));
		existingPosition.setQuantity(new BigDecimal("1300000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("1305281.25"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setTransactionDate(DateUtils.toDate("03/18/2014"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("100.40625"));
		detail.setOriginalFace(new BigDecimal("976786"));
		detail.setQuantity(new BigDecimal("473995.36"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("475920.97"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("03/18/2014"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingPosition);
		currentPosition.setRemainingCostBasis(new BigDecimal("633401.04"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("630838.25"));
		currentPositions.add(currentPosition);
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingPosition.getId())))
				.thenReturn(existingPosition);

		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(7, journalDetailList.size());

		Assertions.assertEquals("-10	3394839	100000	H-200	Position	3137A2C74	100.40625	-630,838.25	-633,401.04	03/02/2015	1	-633,401.04	-633,401.04	03/18/2014	03/02/2015	Position close from transfer to 200000: Test Client Account 2", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	3394839	100000	H-200	Security Distribution	3137A2C74	100.40625	-473,995.36	475,920.97	03/02/2015	1	475,920.97	0	03/18/2014	03/02/2015	Equity security distribution from transfer to 200000: Test Client Account 2", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	H-200	Position	3137A2C74	100.40625	323,214	324,527.06	03/02/2015	1	324,527.06	324,527.06	03/18/2014	03/02/2015	Position opening from transfer to 200000: Test Client Account 2", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	-12	100000	H-200	Position	3137A2C74	100.40625	-166,371.11	-167,046.99	03/02/2015	1	-167,046.99	-167,046.99	03/18/2014	03/02/2015	Reducing Original Face to Current Face for position transfer", journalDetailList.get(3).toStringFormatted(false));
		Assertions.assertEquals("-14	null	200000	H-201	Position	3137A2C74	100.40625	976,786	980,754.2	03/02/2015	1	980,754.2	980,754.2	03/18/2014	03/02/2015	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(4).toStringFormatted(false));
		Assertions.assertEquals("-15	-14	200000	H-201	Position	3137A2C74	100.40625	-502,790.64	-504,833.23	03/02/2015	1	-504,833.23	-504,833.23	03/18/2014	03/02/2015	Reducing Original Face to Current Face for position transfer", journalDetailList.get(5).toStringFormatted(false));
		Assertions.assertEquals("-16	null	200000	H-201	Security Contribution	3137A2C74	100.40625	473,995.36	-475,920.97	03/02/2015	1	-475,920.97	0	03/18/2014	03/02/2015	Equity security contribution from transfer from 100000: Test Client Account", journalDetailList.get(6).toStringFormatted(false));
	}


	@Test
	public void testABSTransfer_PARTIAL_NonOneFactor_v2() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.BETWEEN_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(25246, "36190KAA7", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(3587095L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("107"));
		existingPosition.setQuantity(new BigDecimal("600000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("642000.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setTransactionDate(DateUtils.toDate("06/19/2014"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107"));
		detail.setOriginalFace(new BigDecimal("452798.00"));
		detail.setQuantity(new BigDecimal("440383.61"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("471210.46"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("06/19/2014"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingPosition);
		currentPosition.setRemainingCostBasis(new BigDecimal("624398.24"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("583549.76"));
		currentPositions.add(currentPosition);
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingPosition.getId())))
				.thenReturn(existingPosition);

		transferRule.applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(7, journalDetailList.size());

		Assertions.assertEquals("-10	3587095	100000	H-200	Position	36190KAA7	107	-583,549.76	-624,398.24	05/01/2015	1	-624,398.24	-624,398.24	06/19/2014	05/01/2015	Position close from transfer to 200000: Test Client Account 2", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	3587095	100000	H-200	Security Distribution	36190KAA7	107	-440,383.61	471,210.46	05/01/2015	1	471,210.46	0	06/19/2014	05/01/2015	Equity security distribution from transfer to 200000: Test Client Account 2", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	H-200	Position	36190KAA7	107	147,202	157,506.14	05/01/2015	1	157,506.14	157,506.14	06/19/2014	05/01/2015	Position opening from transfer to 200000: Test Client Account 2", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	-12	100000	H-200	Position	36190KAA7	107	-4,035.85	-4,318.36	05/01/2015	1	-4,318.36	-4,318.36	06/19/2014	05/01/2015	Reducing Original Face to Current Face for position transfer", journalDetailList.get(3).toStringFormatted(false));
		Assertions.assertEquals("-14	null	200000	H-201	Position	36190KAA7	107	452,798	484,493.86	05/01/2015	1	484,493.86	484,493.86	06/19/2014	05/01/2015	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(4).toStringFormatted(false));
		Assertions.assertEquals("-15	-14	200000	H-201	Position	36190KAA7	107	-12,414.39	-13,283.4	05/01/2015	1	-13,283.4	-13,283.4	06/19/2014	05/01/2015	Reducing Original Face to Current Face for position transfer", journalDetailList.get(5).toStringFormatted(false));
		Assertions.assertEquals("-16	null	200000	H-201	Security Contribution	36190KAA7	107	440,383.61	-471,210.46	05/01/2015	1	-471,210.46	0	06/19/2014	05/01/2015	Equity security contribution from transfer from 100000: Test Client Account", journalDetailList.get(6).toStringFormatted(false));
	}


	@Test
	public void testABSTransfer_PARTIAL_OneFactor() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.BETWEEN_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		transfer.setTransactionDate(DateUtils.toDate("03/02/2015"));
		transfer.setSettlementDate(DateUtils.toDate("03/02/2015"));

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(22420, "225458VT2", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(3144803L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("104.796875"));
		existingPosition.setQuantity(new BigDecimal("450000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("471585.94"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setTransactionDate(DateUtils.toDate("12/09/2013"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("104.796875"));
		detail.setOriginalFace(new BigDecimal("338118"));
		detail.setQuantity(new BigDecimal("338118"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("354337.10"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("12/09/2013"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingPosition);
		currentPosition.setRemainingCostBasis(new BigDecimal("471585.94"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("450000"));
		currentPositions.add(currentPosition);
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingPosition.getId())))
				.thenReturn(existingPosition);

		transferRule.applyRule(bookingSession);
		new AccountingRemoveZeroValueRecordBookingRule<AccountingPositionTransfer>().applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(5, journalDetailList.size());

		Assertions.assertEquals("-10	3144803	100000	H-200	Position	225458VT2	104.796875	-450,000	-471,585.94	03/02/2015	1	-471,585.94	-471,585.94	12/09/2013	03/02/2015	Position close from transfer to 200000: Test Client Account 2", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	3144803	100000	H-200	Security Distribution	225458VT2	104.796875	-338,118	354,337.1	03/02/2015	1	354,337.1	0	12/09/2013	03/02/2015	Equity security distribution from transfer to 200000: Test Client Account 2", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	H-200	Position	225458VT2	104.796875	111,882	117,248.84	03/02/2015	1	117,248.84	117,248.84	12/09/2013	03/02/2015	Position opening from transfer to 200000: Test Client Account 2", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	200000	H-201	Position	225458VT2	104.796875	338,118	354,337.1	03/02/2015	1	354,337.1	354,337.1	12/09/2013	03/02/2015	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));
		Assertions.assertEquals("-14	null	200000	H-201	Security Contribution	225458VT2	104.796875	338,118	-354,337.1	03/02/2015	1	-354,337.1	0	12/09/2013	03/02/2015	Equity security contribution from transfer from 100000: Test Client Account", journalDetailList.get(4).toStringFormatted(false));
	}


	@Test
	public void testABSTransfer_PARTIAL_OneFactor_AvoidPennyLot() {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setType(AccountingTestObjectFactory.BETWEEN_TRANSFER_TYPE);
		transfer.setFromClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		transfer.setFromHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		transfer.setToClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault2());
		transfer.setToHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		transfer.setTransactionDate(DateUtils.toDate("05/01/2015"));
		transfer.setSettlementDate(DateUtils.toDate("05/01/2015"));

		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(29534, "38377QSW8", InvestmentType.BONDS, new BigDecimal("0.01"), "USD");

		List<AccountingPositionTransferDetail> detailList = new ArrayList<>();
		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setPositionTransfer(transfer);
		AccountingTransaction existingPosition = new AccountingTransaction();
		existingPosition.setJournal(new AccountingJournal());
		existingPosition.getJournal().setJournalType(new AccountingJournalType());
		existingPosition.setId(4150252L);
		existingPosition.setClientInvestmentAccount(transfer.getFromClientInvestmentAccount());
		existingPosition.setHoldingInvestmentAccount(transfer.getFromHoldingInvestmentAccount());
		existingPosition.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
		existingPosition.setPrice(new BigDecimal("107.1875"));
		existingPosition.setQuantity(new BigDecimal("1000000"));
		existingPosition.setExchangeRateToBase(BigDecimal.ONE);
		existingPosition.setBaseDebitCredit(new BigDecimal("1071875.00"));
		existingPosition.setLocalDebitCredit(existingPosition.getBaseDebitCredit());
		existingPosition.setPositionCostBasis(existingPosition.getBaseDebitCredit());
		existingPosition.setTransactionDate(DateUtils.toDate("02/27/2015"));
		existingPosition.setOriginalTransactionDate(existingPosition.getTransactionDate());
		existingPosition.setSettlementDate(existingPosition.getTransactionDate());
		existingPosition.setInvestmentSecurity(investmentSecurity);
		detail.setExistingPosition(existingPosition);

		detail.setSecurity(investmentSecurity);
		detail.getSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		detail.setCostPrice(existingPosition.getPrice());
		detail.setTransferPrice(new BigDecimal("107.1875"));
		detail.setOriginalFace(new BigDecimal("754664"));
		detail.setQuantity(new BigDecimal("754664"));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setPositionCostBasis(new BigDecimal("808905.48"));
		detail.setOriginalPositionOpenDate(DateUtils.toDate("02/27/2015"));
		detailList.add(detail);
		transfer.setDetailList(detailList);

		AccountingJournal journal = new AccountingJournal();
		BookingSession<AccountingPositionTransfer> bookingSession = new SimpleBookingSession<>(journal, transfer);

		// create and mock booking rules
		AccountingPositionTransferBookingRule transferRule = newAccountingPositionTransferBookingRule();

		List<AccountingPosition> currentPositions = new ArrayList<>();
		AccountingPosition currentPosition = AccountingPosition.forOpeningTransaction(existingPosition);
		currentPosition.setRemainingCostBasis(new BigDecimal("1071875.00"));
		currentPosition.setRemainingBaseDebitCredit(currentPosition.getRemainingCostBasis());
		currentPosition.setRemainingQuantity(new BigDecimal("1000000"));
		currentPositions.add(currentPosition);
		transferRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(
				transferRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class)))
				.thenReturn(currentPositions);

		Mockito.when(transferRule.getAccountingTransactionService()
				.getAccountingTransaction(ArgumentMatchers.eq(existingPosition.getId())))
				.thenReturn(existingPosition);

		transferRule.applyRule(bookingSession);
		new AccountingRemoveZeroValueRecordBookingRule<AccountingPositionTransfer>().applyRule(bookingSession);

		// verify results
		List<? extends AccountingJournalDetailDefinition> journalDetailList = bookingSession.getJournal().getJournalDetailList();
		Assertions.assertEquals(5, journalDetailList.size());

		Assertions.assertEquals("-10	4150252	100000	H-200	Position	38377QSW8	107.1875	-1,000,000	-1,071,875	05/01/2015	1	-1,071,875	-1,071,875	02/27/2015	05/01/2015	Position close from transfer to 200000: Test Client Account 2", journalDetailList.get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	4150252	100000	H-200	Security Distribution	38377QSW8	107.1875	-754,664	808,905.48	05/01/2015	1	808,905.48	0	02/27/2015	05/01/2015	Equity security distribution from transfer to 200000: Test Client Account 2", journalDetailList.get(1).toStringFormatted(false));
		Assertions.assertEquals("-12	null	100000	H-200	Position	38377QSW8	107.1875	245,336	262,969.52	05/01/2015	1	262,969.52	262,969.52	02/27/2015	05/01/2015	Position opening from transfer to 200000: Test Client Account 2", journalDetailList.get(2).toStringFormatted(false));
		Assertions.assertEquals("-13	null	200000	H-201	Position	38377QSW8	107.1875	754,664	808,905.48	05/01/2015	1	808,905.48	808,905.48	02/27/2015	05/01/2015	Position opening from transfer from 100000: Test Client Account", journalDetailList.get(3).toStringFormatted(false));
		Assertions.assertEquals("-14	null	200000	H-201	Security Contribution	38377QSW8	107.1875	754,664	-808,905.48	05/01/2015	1	-808,905.48	0	02/27/2015	05/01/2015	Equity security contribution from transfer from 100000: Test Client Account", journalDetailList.get(4).toStringFormatted(false));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPositionTransferBookingRule newAccountingPositionTransferBookingRule() {
		AccountingPositionTransferBookingRule transferRule = new AccountingPositionTransferBookingRule();
		transferRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		transferRule.setInvestmentCalculator(new InvestmentCalculatorImpl());
		transferRule.setSystemSchemaService(Mockito.mock(SystemSchemaServiceImpl.class));
		Mockito.when(transferRule.getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail")).thenReturn(new SystemTable());
		transferRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		return transferRule;
	}
}
