package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


@AccountingInMemoryDatabaseContext
public class AccountingPositionTransferCollateralBookingRuleDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private AccountingBookingService<AccountingPositionTransfer> accountingBookingService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUser(userTemplate.getId());
		if (user == null) {
			this.securityUserService.saveSecurityUser(userTemplate);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTransferRoundingError() {
		// test that the transfer with a rounding issue can be booked
		AccountingPositionTransfer transfer = this.accountingPositionTransferService.getAccountingPositionTransfer(15417);
		this.accountingBookingService.bookAccountingJournal(AccountingPositionTransferBookingProcessor.JOURNAL_NAME, transfer, true);

		transfer = this.accountingPositionTransferService.getAccountingPositionTransfer(15522);
		this.accountingBookingService.bookAccountingJournal(AccountingPositionTransferBookingProcessor.JOURNAL_NAME, transfer, true);
	}
}
