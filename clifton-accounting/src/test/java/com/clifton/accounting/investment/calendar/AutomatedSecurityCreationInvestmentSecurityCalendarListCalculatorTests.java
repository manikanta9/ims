package com.clifton.accounting.investment.calendar;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.investment.calendar.calculator.PositionSecurityOptions;
import com.clifton.accounting.investment.calendar.calculator.SecurityCalendarOptions;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.calculator.AutomatedSecurityCreationTestUtils;
import com.clifton.investment.instrument.copy.jobs.InvestmentSecurityCreationForwardJob;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;


/**
 * This class is primarily to test functionality in clifton-investment, however to use {@link com.clifton.accounting.investment.calendar.calculator.InvestmentSecurityCalendarListCalculator},
 * this test must be in clifton-account (which has clifton-investment as a dependency)
 *
 * @author mitchellf
 */
@AccountingInMemoryDatabaseContext
public class AutomatedSecurityCreationInvestmentSecurityCalendarListCalculatorTests extends BaseInMemoryDatabaseTests {

	private final String CALCULATOR_BEAN_TYPE = "Investment Security Calendar List Calculator";
	private final String CLIENT_ACCOUNT_GROUP = "Client Account Group";
	private final String POSITION_SECURITY = "Position Security Selection";
	private final String SECURITY_CALENDAR = "Security Calendar Selection";
	private final String INCLUDE_BASE_CCY = "Include Client Account Base CCY";
	private final Integer CCY_HEDGE_REPLICATION_ID = 1020;
	private final Integer SCHEDULE_ID = 784;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentReplicationService investmentReplicationService;


	@Test
	public void testAdeptCurrencyHedgeReplicationSecurityCreation() {
		SystemBean calculatorBean = createInvestmentSecurityCalendarListCalculatorBean();

		// Get instruments to process from replication
		InvestmentReplication replication = this.investmentReplicationService.getInvestmentReplication(this.CCY_HEDGE_REPLICATION_ID);
		Set<InvestmentInstrument> instruments = replication.getAllocationList()
				.stream()
				.map(allocation -> allocation.getReplicationSecurity().getInstrument())
				.collect(Collectors.toSet());

		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Investment Security Calendar Calculator Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		StringJoiner instrumentIdString = new StringJoiner("::");
		for (InvestmentInstrument instrument : instruments) {
			instrumentIdString.add(instrument.getId().toString());
		}

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrumentIdString.toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, this.SCHEDULE_ID.toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.CALENDAR_CALCULATOR, calculatorBean.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.NUMBER_OCCURRENCES, "3", jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.EVALUATION_DATE_OVERRIDE, "11/30/2020", jobBean);

		jobBean = this.systemBeanService.saveSystemBean(jobBean);

		InvestmentSecurityCreationForwardJob job = (InvestmentSecurityCreationForwardJob) this.systemBeanService.getBeanInstance(jobBean);
		Status result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 18, 9, 0, null, null);
	}


	private SystemBean createInvestmentSecurityCalendarListCalculatorBean() {
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.CALCULATOR_BEAN_TYPE);

		SystemBean calculator = new SystemBean();
		calculator.setType(beanType);
		calculator.setName("Investment Security Calendar Calculator Bean");
		calculator.setDescription("Test Bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(calculator);
		List<SystemBeanProperty> properties = new ArrayList<>();

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, this.CLIENT_ACCOUNT_GROUP, this.investmentAccountGroupService.getInvestmentAccountGroupByName("Adept Currency Hedge Accounts").getId().toString(), calculator);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, this.POSITION_SECURITY, PositionSecurityOptions.POSITION_SECURITY_UNDERLYING.name(), calculator);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, this.SECURITY_CALENDAR, SecurityCalendarOptions.SETTLEMENT_CALENDAR.name(), calculator);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, this.INCLUDE_BASE_CCY, "true", calculator);


		calculator.setPropertyList(properties);
		return this.systemBeanService.saveSystemBean(calculator);
	}
}
