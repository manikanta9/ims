package com.clifton.accounting.investment.calendar;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.investment.calendar.calculator.InvestmentSecurityCalendarListCalculator;
import com.clifton.accounting.investment.calendar.calculator.PositionSecurityOptions;
import com.clifton.accounting.investment.calendar.calculator.SecurityCalendarOptions;
import com.clifton.calendar.calculator.CalendarCalculationResult;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * If these tests break it may be because the {@link InvestmentSecurityGroup} securities were added/removed.
 *
 * @author mitchellf
 */

@AccountingInMemoryDatabaseContext
public class CalendarListCalculatorDatabaseTests extends BaseInMemoryDatabaseTests {

	private final String NYSE = "New York Stock Exchange";
	private final String LONDON = "London Stock Exchange";
	private final String WMCO = "WMCO Fixings";
	private final String CME = "CME - Chicago Mercantile";
	private final String ICE = "ICE Futures US";
	private final String MONTREAL = "Montreal Exchange";
	private final String TOKYO = "Tokyo Stock Exchange";
	private final String CMX = "CMX - COMEX";
	private final String US = "United States";
	private final String JAPAN = "Japan";
	private final String UK = "UK - Great Britain";

	private final String BEAN_TYPE = "Investment Security Calendar List Calculator";
	private final String CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE = "Client Account";
	private final String CALENDARS_INCLUDED_BEAN_PROPERTY_TYPE = "Calendars To Include";
	private final String CALENDARS_EXCLUDED_BEAN_PROPERTY_TYPE = "Calendars To Exclude";
	private final String SECURITY_GROUP_BEAN_PROPERTY_TYPE = "Security Group";
	private final String ACCOUNT_GROUP_BEAN_PROPERTY_TYPE = "Client Account Group";
	private final String SECURITY_SELECTION_BEAN_PROPERTY_TYPE = "Position Security Selection";
	private final String CALENDAR_SELECTION_BEAN_PROPERTY_TYPE = "Security Calendar Selection";
	private final String BASE_CCY_BEAN_PROPERTY_TYPE = "Include Client Account Base CCY";

	private final String PARAMETRIC_DE_ACCT = "800020";
	private final String ADEPT_5 = "051200";

	private final String CCY_GROUP = "CCY Forwards";

	private final String ACCT_GROUP = "Adept 1 - Active Client Accts.";

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	@Test
	public void testInvestmentSecurityCalendarListCalculator_includeCalendars() {

		// Plan of attack - Get set of calendars from DB
		Calendar ny = this.calendarSetupService.getCalendarByName(this.NYSE);
		Calendar london = this.calendarSetupService.getCalendarByName(this.LONDON);

		// Create bean with calendars included
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculator = new SystemBean();
		calculator.setType(beanType);
		calculator.setName("Calendars Included Bean");
		calculator.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.PARAMETRIC_DE_ACCT).getId().toString(), calculator);

		String calendarPropertyValue = ny.getId().toString() + "::" + london.getId().toString();
		createSystemBeanProperty(properties, types, this.CALENDARS_INCLUDED_BEAN_PROPERTY_TYPE, calendarPropertyValue, calculator);

		// These properties are not used, but they are required by the bean type
		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculator);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.EXCHANGE_CALENDAR.name(), calculator);

		calculator.setPropertyList(properties);
		calculator = this.systemBeanService.saveSystemBean(calculator);

		// retrieve and evaluate bean with no additional params
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculator);

		CalendarCalculationResult result = calendarListCalculator.calculate(new Date());
		validateCalendarResults(result, this.NYSE, this.LONDON);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_bySecurityGroup() {
		// Set date to 9/21/20
		Date date = DateUtils.toDate("01/21/2021");

		// Set Bean to use position security, settlement calendar, CCY futures security group
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Security Group Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.ADEPT_5).getId().toString(), calculatorBean);

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.SETTLEMENT_CALENDAR.name(), calculatorBean);

		InvestmentSecurityGroup group = this.investmentSecurityGroupService.getInvestmentSecurityGroupByName(this.CCY_GROUP);
		createSystemBeanProperty(properties, types, this.SECURITY_GROUP_BEAN_PROPERTY_TYPE, group.getId().toString(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21 and adept 5 account
		// Should return only MWCO, which will indicate accuracy because this client account holds positions for securities that are not in this security group
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);

		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result, this.WMCO);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_byClientAccountGroup() {
		// Set date to 9/21/20
		Date date = DateUtils.toDate("01/21/2021");

		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Account Group Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.COALESCE.name(), calculatorBean);

		InvestmentAccountGroup group = this.investmentAccountGroupService.getInvestmentAccountGroupByName(this.ACCT_GROUP);
		createSystemBeanProperty(properties, types, this.ACCOUNT_GROUP_BEAN_PROPERTY_TYPE, group.getId().toString(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);

		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_varySecuritySelection() {
		// Position security
		// Set date to 9/21/20
		Date date = DateUtils.toDate("01/21/2021");

		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Position Security Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.ADEPT_5).getId().toString(), calculatorBean);

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.COALESCE.name(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);

		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result, this.LONDON, this.ICE, this.TOKYO, this.WMCO);

		// Position security underlying
		CollectionUtils.getOnlyElement(calculatorBean.getPropertyList()
				.stream()
				.filter(prop -> this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE.equals(prop.getType().getName()))
				.collect(Collectors.toList()))
				.setValue(PositionSecurityOptions.POSITION_SECURITY_UNDERLYING.name());

		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);
		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result2 = calendarListCalculator.calculate(date);
		validateCalendarResults(result2, this.US);

		//position security base ccy
		CollectionUtils.getOnlyElement(calculatorBean.getPropertyList()
				.stream()
				.filter(prop -> this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE.equals(prop.getType().getName()))
				.collect(Collectors.toList()))
				.setValue(PositionSecurityOptions.POSITION_SECURITY_CCY.name());

		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);
		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result3 = calendarListCalculator.calculate(date);
		validateCalendarResults(result3, this.US, this.JAPAN, this.UK);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_varyCalendarSelection() {
		Date date = DateUtils.toDate("01/21/2021");

		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Security Calendar Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.ADEPT_5).getId().toString(), calculatorBean);

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.SETTLEMENT_CALENDAR.name(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result, this.WMCO);

		// Position security underlying
		CollectionUtils.getOnlyElement(calculatorBean.getPropertyList()
				.stream()
				.filter(prop -> this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE.equals(prop.getType().getName()))
				.collect(Collectors.toList()))
				.setValue(SecurityCalendarOptions.EXCHANGE_CALENDAR.name());

		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);
		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result2 = calendarListCalculator.calculate(date);
		validateCalendarResults(result2, this.LONDON, this.ICE, this.TOKYO);

		//position security base ccy
		CollectionUtils.getOnlyElement(calculatorBean.getPropertyList()
				.stream()
				.filter(prop -> this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE.equals(prop.getType().getName()))
				.collect(Collectors.toList()))
				.setValue(SecurityCalendarOptions.COALESCE.name());

		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);
		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result3 = calendarListCalculator.calculate(date);
		validateCalendarResults(result3, this.LONDON, this.ICE, this.TOKYO, this.WMCO);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_includeBaseCCY() {
		// Set date to 9/21/20
		Date date = DateUtils.toDate("01/21/2021");

		// Set Bean to use position security, settlement calendar, CCY futures security group
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Base CCY Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.ADEPT_5).getId().toString(), calculatorBean);

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.SETTLEMENT_CALENDAR.name(), calculatorBean);

		InvestmentSecurityGroup group = this.investmentSecurityGroupService.getInvestmentSecurityGroupByName(this.CCY_GROUP);
		createSystemBeanProperty(properties, types, this.SECURITY_GROUP_BEAN_PROPERTY_TYPE, group.getId().toString(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21 and adept 5 account
		// Should return only MWCO, which will indicate accuracy because this client account holds positions for securities that are not in this security group
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);

		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result, this.WMCO);

		createSystemBeanProperty(properties, types, this.BASE_CCY_BEAN_PROPERTY_TYPE, "true", calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21 and adept 5 account
		// Should return only MWCO, which will indicate accuracy because this client account holds positions for securities that are not in this security group
		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result2 = calendarListCalculator.calculate(date);
		validateCalendarResults(result2, this.WMCO, this.UK);
	}


	@Test
	public void testInvestmentSecurityCalendarListCalculator_excludeCalendars() {
		Date date = DateUtils.toDate("01/21/2021");

		Calendar wmco = this.calendarSetupService.getCalendarByName(this.WMCO);

		// Set Bean to use position security, settlement calendar, CCY futures security group
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(this.BEAN_TYPE);

		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(beanType);
		calculatorBean.setName("Exclude Calendars Bean");
		calculatorBean.setDescription("Test Bean");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> types = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());

		createSystemBeanProperty(properties, types, this.CLIENT_ACCOUNT_BEAN_PROPERTY_TYPE, this.investmentAccountService.getInvestmentAccountByNumber(this.ADEPT_5).getId().toString(), calculatorBean);

		createSystemBeanProperty(properties, types, this.SECURITY_SELECTION_BEAN_PROPERTY_TYPE, PositionSecurityOptions.POSITION_SECURITY.name(), calculatorBean);
		createSystemBeanProperty(properties, types, this.CALENDAR_SELECTION_BEAN_PROPERTY_TYPE, SecurityCalendarOptions.SETTLEMENT_CALENDAR.name(), calculatorBean);


		InvestmentSecurityGroup group = this.investmentSecurityGroupService.getInvestmentSecurityGroupByName(this.CCY_GROUP);
		createSystemBeanProperty(properties, types, this.SECURITY_GROUP_BEAN_PROPERTY_TYPE, group.getId().toString(), calculatorBean);
		createSystemBeanProperty(properties, types, this.BASE_CCY_BEAN_PROPERTY_TYPE, "true", calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		// Pass command for 9/21 and adept 5 account
		InvestmentSecurityCalendarListCalculator calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);

		CalendarCalculationResult result = calendarListCalculator.calculate(date);
		validateCalendarResults(result, this.WMCO, this.UK);


		createSystemBeanProperty(properties, types, this.CALENDARS_EXCLUDED_BEAN_PROPERTY_TYPE, wmco.getId().toString(), calculatorBean);

		calculatorBean.setPropertyList(properties);
		calculatorBean = this.systemBeanService.saveSystemBean(calculatorBean);

		calendarListCalculator = (InvestmentSecurityCalendarListCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		CalendarCalculationResult result2 = calendarListCalculator.calculate(date);
		validateCalendarResults(result2, this.UK);
	}


	private List<SystemBeanProperty> createSystemBeanProperty(List<SystemBeanProperty> propertyList, List<SystemBeanPropertyType> propertyTypes, String propertyTypeName, String propertyValue, SystemBean parentBean) {
		SystemBeanPropertyType securitySelectionType = CollectionUtils.getOnlyElement(propertyTypes.stream().filter(type -> propertyTypeName.equals(type.getName())).collect(Collectors.toList()));
		SystemBeanProperty securitySelection = new SystemBeanProperty();
		securitySelection.setType(securitySelectionType);
		securitySelection.setValue(propertyValue);
		securitySelection.setBean(parentBean);
		propertyList.add(securitySelection);
		return propertyList;
	}


	private void validateCalendarResults(CalendarCalculationResult result, String... calendarNames) {
		List<String> expectedNames = ArrayUtils.getStream(calendarNames).collect(Collectors.toList());
		List<String> resultNames = result.getCalendarList().stream().map(Calendar::getName).collect(Collectors.toList());
		if ((expectedNames.size() != resultNames.size()) || (CollectionUtils.getIntersection(expectedNames, resultNames).size() != expectedNames.size())) {
			throw new ValidationException(String.format("Expected Calendar Results %s do not match Actual Calendar Results %s", expectedNames.toString(), resultNames.toString()));
		}
	}


	private SystemBean setupBean() {
		return null;
	}
}
