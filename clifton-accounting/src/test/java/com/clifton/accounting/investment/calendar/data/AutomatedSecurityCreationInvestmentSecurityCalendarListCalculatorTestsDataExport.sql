SELECT 'Calendar' AS entityTableName, CalendarID AS entityID
FROM Calendar
WHERE CalendarID IN (11, 30, 44)
UNION
SELECT 'CalendarSchedule' AS entityTableName, CalendarScheduleID AS entityID
FROM CalendarSchedule
WHERE CalendarScheduleID IN (784)
   OR CalendarID IN (2, 11, 30, 44, 156)
UNION
SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID
FROM SystemBeanPropertyType
WHERE SystemBeanTypeID IN (603, 588, 589, 591)
UNION
SELECT 'InvestmentAccountGroup' AS entityTableName, InvestmentAccountGroupID AS entityID
FROM InvestmentAccountGroup
WHERE InvestmentAccountGroupID IN (308, 545)
UNION
SELECT 'InvestmentAccountGroupAccount' AS entityTableName, InvestmentAccountGroupAccountID AS entityID
FROM InvestmentAccountGroupAccount
WHERE InvestmentAccountGroupID IN (308, 584)
  AND InvestmentAccountID IN (4426, 4908, 4425)
UNION
SELECT 'InvestmentSecurityGroupSecurity' AS entityTableName, InvestmentSecurityGroupSecurityID AS entityID
FROM InvestmentSecurityGroupSecurity
WHERE InvestmentSecurityGroupID IN (28, 57)
UNION
SELECT 'InvestmentReplicationAllocation' AS entityTableName, InvestmentReplicationAllocationID AS entityID
FROM InvestmentReplicationAllocation
WHERE InvestmentReplicationID IN (1020)
AND ReplicationSecurityID IN (
    SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE InvestmentInstrumentID IN (
    	SELECT InvestmentInstrumentID FROM InvestmentInstrument WHERE InvestmentInstrumentHierarchyID IN (
    		SELECT InvestmentInstrumentHierarchyID FROM InvestmentInstrumentHierarchy WHERE InvestmentTypeID = 3
		)
	)

)
UNION
SELECT 'SystemBeanType' AS entityTableName, SystemBeanTypeID AS entityID
FROM SystemBeanType
WHERE SystemBeanTypeID IN (590)
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityID
FROM SystemColumn
WHERE SystemColumnGroupID IN (3)
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityID
FROM InvestmentSecurityEventType
WHERE InvestmentSecurityEventTypeID IN (6)


