SELECT 'Calendar' AS entityTableName, CalendarID AS entityID
FROM Calendar
WHERE CalendarID IN (11, 30, 44)
UNION
SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID
FROM SystemBeanPropertyType pt
INNER JOIN SystemBeanType t ON t.SystemBeanTypeID = pt.SystemBeanTypeID
WHERE t.SystemBeanTypeName = 'Investment Security Calendar List Calculator'
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityID
FROM InvestmentAccount
WHERE AccountNumber IN ('800020', '051200', '051250')
UNION
SELECT 'InvestmentAccountGroup' AS entityTableName, InvestmentAccountGroupID AS entityID
FROM InvestmentAccountGroup
WHERE InvestmentAccountGroupID = 308
UNION
SELECT 'InvestmentAccountGroupAccount' AS entityTableName, InvestmentAccountGroupAccountID AS entityID
FROM InvestmentAccountGroupAccount
WHERE InvestmentAccountGroupID IN (308, 584)
  AND InvestmentAccountID IN (4426, 4908, 4425)
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityID
FROM AccountingTransaction
WHERE ClientInvestmentAccountID IN (4426, 4908, 4425)
  AND TransactionDate >= '2021-01-01' AND TransactionDate <= '2021-01-21'
  --   AND InvestmentSecurityID IN
  -- 	  (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE InvestmentInstrumentID IN (483, 14589, 14531, 9580))
UNION
SELECT 'InvestmentSecurityGroupSecurity' AS entityTableName, InvestmentSecurityGroupSecurityID AS entityID
FROM InvestmentSecurityGroupSecurity
WHERE InvestmentSecurityGroupID IN (28, 57)


