package com.clifton.accounting.period;


import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class AccountingPeriodServiceImplTests {

	private static final int PERIOD_JAN = 1;
	private static final int PERIOD_FEB = 2;
	private static final int PERIOD_MAR = 3;

	private static final int INVESTMENT_ACCOUNT_NO_CLOSINGS = 1;
	private static final int INVESTMENT_ACCOUNT_CLOSED_JAN_FEB = 2;
	private static final int INVESTMENT_ACCOUNT_CLOSED_JAN = 3;

	private static final int THIRD_TO_LAST_CLOSED_WITH_REVENUE = 52;
	private static final int SECOND_TO_LAST_OPEN = 53;
	private static final int LAST_OPEN = 54;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Resource
	private AccountingPeriodService accountingPeriodService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsAccountingPeriodClosed() {
		Assertions.assertFalse(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_NO_CLOSINGS, DateUtils.toDate("3/17/2012")));

		Assertions.assertTrue(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("1/31/2012")));
		Assertions.assertTrue(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("2/1/2012")));
		Assertions.assertTrue(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("2/17/2012")));
		Assertions.assertTrue(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("2/29/2012"))); // last day
		Assertions.assertFalse(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("3/1/2012"))); // first day
		Assertions.assertFalse(this.accountingPeriodService.isAccountingPeriodClosed(INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, DateUtils.toDate("3/17/2012")));
	}


	@Test
	public void testOpenAccountingPeriod_ClosedWithoutNote() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingPeriodService.openAccountingPeriod(PERIOD_FEB, INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, null);
		}, "A note is required when reopening a period.");
	}


	@Test
	public void testOpenAccountingPeriod_ClosedAfter() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingPeriodService.openAccountingPeriod(PERIOD_JAN, INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, "test reopen");
		}, "Cannot re-open accounting period [01/01/2012 - 01/31/2012] for 22222:  because there are closed periods after it: [02/01/2012 - 02/29/2012] is CLOSED for 22222: ");
	}


	@Test
	public void testOpenAccountingPeriod() {
		this.accountingPeriodService.openAccountingPeriod(PERIOD_FEB, INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, "test reopen");
		this.accountingPeriodService.closeAccountingPeriod(PERIOD_FEB, INVESTMENT_ACCOUNT_CLOSED_JAN_FEB, "test re-close");
	}


	@Test
	public void testCreateCloseAccountingPeriod() {
		// Create Next 2 Accounting Periods
		this.accountingPeriodService.createAccountingPeriodNext(INVESTMENT_ACCOUNT_CLOSED_JAN);
		this.accountingPeriodService.createAccountingPeriodNext(INVESTMENT_ACCOUNT_CLOSED_JAN);

		// Close The First, then the second
		this.accountingPeriodService.closeAccountingPeriod(PERIOD_FEB, INVESTMENT_ACCOUNT_CLOSED_JAN, "test close");
		this.accountingPeriodService.closeAccountingPeriod(PERIOD_MAR, INVESTMENT_ACCOUNT_CLOSED_JAN, "test close");
	}


	@Test
	public void testCloseAccountingPeriod_NoneOpen() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingPeriodService.closeAccountingPeriodNext(INVESTMENT_ACCOUNT_NO_CLOSINGS, null);
		}, "There are no open periods for this account to close.");
	}


	@Test
	public void testDeleteAccountingPeriod_NotLast() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingPeriodService.deleteAccountingPeriodClosing(SECOND_TO_LAST_OPEN);
		}, "Cannot delete selected period for this account because it is not the last accounting period for the account.  The last accounting period is [[04/01/2012 - 04/30/2012]].");
	}


	@Test
	public void testDeleteAccountingPeriod_HasPeriodValues() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.accountingPeriodService.deleteAccountingPeriodClosing(THIRD_TO_LAST_CLOSED_WITH_REVENUE);
		}, "Cannot delete selected accounting period because AUM and/or Projected Revenue values exist for the period.");
	}


	@Test
	public void testDeleteAccountingPeriod_Valid() {
		this.accountingPeriodService.deleteAccountingPeriodClosing(LAST_OPEN);
		this.accountingPeriodService.deleteAccountingPeriodClosing(SECOND_TO_LAST_OPEN);
	}
}
