package com.clifton.accounting.m2m.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MTestObjectFactory;
import com.clifton.accounting.m2m.builder.AccountingM2MDailyBuilder;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;


public class AccountingM2MDailyExpenseBookingRuleTests {

	private static final AccountingAccount REVENUE_INTEREST_INCOME = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_INCOME);
	private static final AccountingAccount EXPENSE_COLLATERAL_USAGE_FEE = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_COLLATERAL_USAGE_FEE);


	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingM2MDailyInterestBookingRule() {
		AccountingM2MDaily m2m = AccountingM2MDailyBuilder.createAccountingM2MDaily(250)
				.forClient("Duke", 1, "100", InvestmentSecurityBuilder.newUSD())
				.withMarkDate(DateUtils.toDate("02/28/2019"))
				.withMarkValues(1000000, 1000000, 999875)
				.withExpense(REVENUE_INTEREST_INCOME, new BigDecimal("125"), InvestmentSecurityBuilder.newCAD(), new BigDecimal("0.9984025559"), 0)
				.toAccountingM2MDaily();

		AccountingM2MDailyExpenseBookingRule bookingRule = AccountingM2MTestObjectFactory.newAccountingM2MDailyExpenseBookingRule();


		Mockito.when(bookingRule.getCalendarBusinessDayService().getBusinessDayFrom(Mockito.any(CalendarBusinessDayCommand.class), ArgumentMatchers.eq(0))).thenReturn(m2m.getMarkDate());
		Mockito.when(bookingRule.getCalendarBusinessDayService().getBusinessDayFrom(Mockito.any(CalendarBusinessDayCommand.class), ArgumentMatchers.eq(1))).thenReturn(DateUtils.toDate("03/01/2019"));

		AccountingBookingRuleTestExecutor.newTestForEntity(m2m)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-10	null	100	100	Interest Income	CAD			-125	02/28/2019	0.9984025559	-124.8	0	02/28/2019	02/28/2019	M2M Interest Income",
						"-11	null	100	100	Currency	CAD			125	02/28/2019	0.9984025559	124.8	0	02/28/2019	02/28/2019	M2M Interest Income"
				).execute();


		m2m.getExpenseList().get(0).getType().setDaysToSettle(1);
		AccountingBookingRuleTestExecutor.newTestForEntity(m2m)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-10	null	100	100	Interest Income	CAD			-125	02/28/2019	0.9984025559	-124.8	0	02/28/2019	03/01/2019	M2M Interest Income",
						"-11	null	100	100	Currency	CAD			125	02/28/2019	0.9984025559	124.8	0	02/28/2019	03/01/2019	M2M Interest Income"
				).execute();
	}


	@Test
	public void testAccountingM2MDailyCollateralUsageFeeBookingRule() {
		AccountingM2MDaily m2m = AccountingM2MDailyBuilder.createAccountingM2MDaily(250).forClient("Duke", 1, "100", InvestmentSecurityBuilder.newUSD()).withMarkValues(1000000, 1000000, 999875)
				//.withExpense(REVENUE_INTEREST_INCOME, new BigDecimal("100"), MarketDataTestObjectFactory.USD, BigDecimal.ONE)
				.withExpense(EXPENSE_COLLATERAL_USAGE_FEE, new BigDecimal("-125"), InvestmentSecurityBuilder.newUSD(), BigDecimal.ONE)
				.toAccountingM2MDaily();

		AccountingM2MDailyExpenseBookingRule bookingRule = AccountingM2MTestObjectFactory.newAccountingM2MDailyExpenseBookingRule();

		BookingSession<AccountingM2MDaily> session = new SimpleBookingSession<>(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.MARK_TO_MARKET_JOURNAL,
				new Short("450")), m2m);
		bookingRule.applyRule(session);

		AccountingJournal journal = session.getJournal();
		Assertions.assertEquals(2, journal.getJournalDetailList().size());

		Assertions.assertEquals("-10	null	100	100	Collateral Usage Fee	USD			125	null	1	125	0	null	null	M2M Collateral Usage Fee", journal.getJournalDetailList().get(0).toStringFormatted(false));
		Assertions.assertEquals("-11	null	100	100	Cash	USD			-125	null	1	-125	0	null	null	M2M Collateral Usage Fee", journal.getJournalDetailList().get(1).toStringFormatted(false));
	}
}
