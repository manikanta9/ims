package com.clifton.accounting.m2m.instruction;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.builder.AccountingM2MDailyBuilder;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class AccountingM2MCounterpartyInstructionGeneratorTests {

	private static BusinessCompany GS = BusinessCompanyBuilder.createEmptyBusinessCompany()
			.withId(7)
			.withName("Goldman Sachs & Co.")
			.withBusinessIdentifierCode("GOLDUS33")
			.toBusinessCompany();
	private static BusinessCompany NTG = BusinessCompanyBuilder.createEmptyBusinessCompany()
			.withId(5079)
			.withName("Northern Trust Co. IL (Global)")
			.withBusinessIdentifierCode("CNORUS40COL")
			.toBusinessCompany();
	private static BusinessCompany HSBC = BusinessCompanyBuilder.createEmptyBusinessCompany()
			.withId(4691)
			.withName("HSBC Bank USA National Association")
			.withBusinessIdentifierCode("MRMDUS30")
			.toBusinessCompany();

	@Mock
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	@Mock
	private SystemSchemaUtilHandler systemSchemaUtilHandler;
	@Mock
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;
	@Mock
	private DaoLocator daoLocator;
	@Mock
	private ReadOnlyDAO<AccountingM2MDaily> dao;


	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(this.businessCompanyUtilHandler.getParametricBusinessIdentifierCode()).thenReturn("PPSCUS66");

		Mockito.when(this.businessCompanyUtilHandler.getIntermediaryCustodian(ArgumentMatchers.eq(GS))).thenReturn(
				HSBC);
		Mockito.when(this.businessCompanyUtilHandler.getIntermediaryCustodianAccountNumber(ArgumentMatchers.eq(GS))).thenReturn(
				"001820451");

		Mockito.when(this.systemSchemaUtilHandler.getEntityUniqueId(ArgumentMatchers.any())).thenReturn("IMS42MTM");
	}


	@Test
	public void testGeneralFinancialInstitutionTransferMessage() {
		AccountingM2MDaily balance = AccountingM2MDailyBuilder.createAccountingM2MDaily()
				.forClient("Duke", 1, "100", InvestmentSecurityBuilder.newUSD()).withMarkValues(1000000, 1000000, -999875)
				.withCustodian(100, "NT-1", NTG)
				.withHoldingAccountIssuer(GS)
				.withMarkDate(DateUtils.toDate("11/14/2016"))
				.toAccountingM2MDaily();
		balance.setMarkCurrency(InvestmentSecurityBuilder.newUSD());
		SystemTable accountingMTM = SystemTableBuilder.createAccountingM2MDaily().toSystemTable();
		Mockito.when(this.daoLocator.<AccountingM2MDaily>locate(accountingMTM.getName())).thenReturn(this.dao);
		Mockito.when(this.dao.findByPrimaryKey(balance.getId())).thenReturn(balance);

		AccountingM2MCounterpartyInstructionGenerator generator = new AccountingM2MCounterpartyInstructionGenerator();
		generator.setBusinessCompanyUtilHandler(this.businessCompanyUtilHandler);
		generator.setSystemSchemaUtilHandler(this.systemSchemaUtilHandler);
		generator.setDaoLocator(this.daoLocator);
		generator.setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);

		InvestmentInstructionItem investmentInstructionItem = buildInvestmentInstructionItem(balance.getId(), accountingMTM);
		InvestmentInstructionDelivery investmentInstructionDelivery = new InvestmentInstructionDelivery();
		investmentInstructionDelivery.setId(1);
		investmentInstructionDelivery.setDeliveryAccountNumber("001820451");
		BusinessCompany businessCompany = new BusinessCompany();
		businessCompany.setBusinessIdentifierCode("MRMDUS30");
		investmentInstructionDelivery.setDeliveryCompany(businessCompany);
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				ArgumentMatchers.same(investmentInstructionItem),
				ArgumentMatchers.same(balance.getHoldingInvestmentAccount().getIssuingCompany()),
				ArgumentMatchers.same(balance.getMarkCurrency())))
				.thenReturn(investmentInstructionDelivery);
		InstructionMessage message = generator.generateInstructionMessageList(investmentInstructionItem).get(0);

		Assertions.assertTrue(message instanceof GeneralFinancialInstitutionTransferMessage);

		GeneralFinancialInstitutionTransferMessage transferMessage = (GeneralFinancialInstitutionTransferMessage) message;
		Assertions.assertEquals("PPSCUS66", transferMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());
		Assertions.assertEquals("USD", transferMessage.getCurrency());
		Assertions.assertEquals(DateUtils.fromDate(transferMessage.getValueDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("999875.00"), "#,###.###############"), CoreMathUtils.formatNumber(transferMessage.getAmount(), "#,###.###############"));

		Assertions.assertEquals("NT-1", transferMessage.getCustodyAccountNumber());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getCustodyBIC());

		Assertions.assertEquals("001820451", transferMessage.getIntermediaryAccountNumber());
		Assertions.assertEquals("MRMDUS30", transferMessage.getIntermediaryBIC());
	}


	@Test
	public void testNoticeToReceiveTransferMessageMessage() {
		AccountingM2MDaily balance = AccountingM2MDailyBuilder.createAccountingM2MDaily()
				.forClient("Duke", 1, "100", InvestmentSecurityBuilder.newUSD()).withMarkValues(1000000, 1000000, 999875)
				.withCustodian(100, "NT-1", NTG)
				.withHoldingAccountIssuer(GS)
				.withMarkDate(DateUtils.toDate("11/14/2016"))
				.toAccountingM2MDaily();
		balance.setMarkCurrency(InvestmentSecurityBuilder.newUSD());
		SystemTable accountingMTM = SystemTableBuilder.createAccountingM2MDaily().toSystemTable();
		Mockito.when(this.daoLocator.<AccountingM2MDaily>locate(accountingMTM.getName())).thenReturn(this.dao);
		Mockito.when(this.dao.findByPrimaryKey(balance.getId())).thenReturn(balance);

		AccountingM2MCounterpartyInstructionGenerator generator = new AccountingM2MCounterpartyInstructionGenerator();
		generator.setBusinessCompanyUtilHandler(this.businessCompanyUtilHandler);
		generator.setSystemSchemaUtilHandler(this.systemSchemaUtilHandler);
		generator.setDaoLocator(this.daoLocator);
		generator.setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);

		InvestmentInstructionItem investmentInstructionItem = buildInvestmentInstructionItem(balance.getId(), accountingMTM);
		InvestmentInstructionDelivery investmentInstructionDelivery = new InvestmentInstructionDelivery();
		investmentInstructionDelivery.setId(1);
		BusinessCompany businessCompany = new BusinessCompany();
		businessCompany.setBusinessIdentifierCode("MRMDUS30");
		investmentInstructionDelivery.setDeliveryCompany(businessCompany);
		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getInvestmentInstructionDelivery(
				investmentInstructionItem,
				balance.getHoldingInvestmentAccount().getIssuingCompany(),
				balance.getMarkCurrency()
		)).thenReturn(investmentInstructionDelivery);
		InstructionMessage message = generator.generateInstructionMessageList(investmentInstructionItem).get(0);

		Assertions.assertTrue(message instanceof NoticeToReceiveTransferMessage);

		NoticeToReceiveTransferMessage transferMessage = (NoticeToReceiveTransferMessage) message;
		Assertions.assertEquals("PPSCUS66", transferMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());
		Assertions.assertEquals("USD", transferMessage.getCurrency());
		Assertions.assertEquals(DateUtils.fromDate(transferMessage.getValueDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("999875.00"), "#,###.###############"), CoreMathUtils.formatNumber(transferMessage.getAmount(), "#,###.###############"));

		Assertions.assertEquals("NT-1", transferMessage.getReceiverAccountNumber());
		Assertions.assertEquals("CNORUS40COL", transferMessage.getReceiverBIC());

		Assertions.assertEquals("GOLDUS33", transferMessage.getOrderingBIC());
		Assertions.assertEquals("MRMDUS30", transferMessage.getIntermediaryBIC());
	}


	private InvestmentInstructionItem buildInvestmentInstructionItem(int fkFieldId, SystemTable systemTable) {
		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = new InvestmentInstructionDeliveryType();
		investmentInstructionDeliveryType.setId((short) 1);

		InvestmentInstructionCategory category = new InvestmentInstructionCategory();
		category.setTable(systemTable);

		InvestmentInstructionDefinition definition = new InvestmentInstructionDefinition();
		definition.setCategory(category);
		definition.setDeliveryType(investmentInstructionDeliveryType);

		InvestmentInstruction investmentInstruction = new InvestmentInstruction();
		investmentInstruction.setDefinition(definition);

		InvestmentInstructionItem investmentInstructionItem = new InvestmentInstructionItem();
		investmentInstructionItem.setId(1);
		investmentInstructionItem.setFkFieldId(fkFieldId);
		investmentInstructionItem.setInstruction(investmentInstruction);

		return investmentInstructionItem;
	}
}
