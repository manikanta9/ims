package com.clifton.accounting;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class AccountingProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "accounting";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		// NOTE: refactor DB calls into CORE API?
		imports.add("java.sql.");
		imports.add("org.springframework.dao.DataAccessException");
		imports.add("org.springframework.jdbc.core.ResultSetExtractor");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		String methodName = method.getName();

		Set<String> skipMethods = new HashSet<>();

		skipMethods.add("getAccountingBalanceList");
		skipMethods.add("getAccountingPeriodClosingExtendedList");

		skipMethods.add("deleteAccountingEventJournal");
		skipMethods.add("deleteAccountingPositionTransfer");
		skipMethods.add("deleteAccountingPeriodClosing");

		skipMethods.add("saveAccountingM2MDailyExpense");
		skipMethods.add("saveAccountingM2MDailyExpenseType");
		skipMethods.add("saveAccountingPositionTransfer");

		skipMethods.add("saveAccountingJournal");

		return skipMethods.contains(methodName);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveAccountingM2MExpenseGroup");
		ignoredVoidSaveMethodSet.add("saveAccountingPositionTransfer");
		return ignoredVoidSaveMethodSet;
	}
}
