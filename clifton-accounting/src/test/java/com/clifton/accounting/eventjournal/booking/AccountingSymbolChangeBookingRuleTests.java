package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class AccountingSymbolChangeBookingRuleTests extends BaseEventJournalBookingRuleTests {

	@Test
	public void testStockSymbolChange_FullyOpenLot() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal())
				.forBookingRules(
						newEventJournalBookingRule(AccountingSymbolChangeBookingRule.class)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	8006252	100000	H-200	Position	FB	false	Goldman Sachs	81.63	-745	-60,814.35	02/01/2018	1	-60,814.35	-60,814.35	01/22/2018	02/01/2018	Symbol Change for FB on 02/01/2018",
						"-11	8006252	100000	H-200	Position	WM	true	Goldman Sachs	81.63	745	60,814.35	02/01/2018	1	60,814.35	60,814.35	01/22/2018	02/01/2018	Symbol Change for FB on 02/01/2018"
				).execute();
	}


	private AccountingEventJournal buildEventJournal() {
		Date originalDate = DateUtils.toDate("01/22/2018");

		// FB Symbol Change to WM on 02/01/2018
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SYMBOL_CHANGE);
		InvestmentSecurityEvent securityEvent = InvestmentTestObjectFactory.newInvestmentSecurityEvent(InvestmentTestObjectFactory.newInvestmentSecurity(110, "FB", InvestmentType.STOCKS), eventType, "02/01/2018");
		securityEvent.setBeforeAndAfterEventValue(new BigDecimal("1"));
		securityEvent.setAdditionalSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(100, "WM", InvestmentType.STOCKS));

		// 745 shares at $81.63 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(8006252, securityEvent.getSecurity())
				.qty(745).price("81.63").on(originalDate).build();
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEvent(securityEvent), tran);
	}
}
