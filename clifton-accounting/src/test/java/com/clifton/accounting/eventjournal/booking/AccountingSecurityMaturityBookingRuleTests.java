package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AccountingSecurityMaturityBookingRuleTests {

	@Test
	public void testDeliverableCurrencyForwardMaturity_TwoCurrencies_NoOffset() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("06/19/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(112776, "BRL/USD20190619", InvestmentType.FORWARDS));
		securityEvent.getSecurity().getInstrument().setDeliverable(true); // Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("06/17/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("06/19/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		AccountingEventJournalDetail.ForwardMaturity forwardMaturity = eventDetail.forForwardMaturity();
		forwardMaturity.setMaturityPrice(new BigDecimal("3.8832070765768"));
		forwardMaturity.setCurrencyAmount1(new BigDecimal("-5974000.00"));
		forwardMaturity.setCurrencyAmount2(new BigDecimal("-22938367.80"));
		forwardMaturity.setExchangeRate1(BigDecimal.ONE);
		forwardMaturity.setExchangeRate2(new BigDecimal("0.25679544"));
		// BUY 5,974,000.00 USD and SELL 22,938,367.80 BRL in USD Account
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(13959085L, securityEvent.getSecurity()).qty("-22938367.80").price("3.8397").on("03/15/2019").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule)
				.withExpectedResults(
						"-10	13959085	100000	H-200	Position	BRL/USD20190619	3.8832070766	22,938,367.8	0	06/19/2019	1	0	5,907,067.88	03/15/2019	06/19/2019	Security Maturity for BRL/USD20190619 on 06/19/2019",
						"-11	-10	100000	H-200	Cash	USD			5,974,000	06/19/2019	1	5,974,000	0	03/15/2019	06/19/2019	Cash proceeds from USD on maturity of BRL/USD20190619 on 06/19/2019",
						"-12	-10	100000	H-200	Currency	BRL			-22,938,367.8	06/19/2019	0.25679544	-5,890,468.25	0	03/15/2019	06/19/2019	Currency expense from BRL on maturity of BRL/USD20190619 on 06/19/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule())
				.withExpectedResults(
						"-10	13959085	100000	H-200	Position	BRL/USD20190619	3.8832070766	22,938,367.8	0	06/19/2019	1	0	5,974,000	03/15/2019	06/19/2019	Security Maturity for BRL/USD20190619 on 06/19/2019",
						"-11	-10	100000	H-200	Cash	USD			5,974,000	06/19/2019	1	5,974,000	0	03/15/2019	06/19/2019	Cash proceeds from USD on maturity of BRL/USD20190619 on 06/19/2019",
						"-12	-10	100000	H-200	Currency	BRL			-22,938,367.8	06/19/2019	0.25679544	-5,890,468.25	0	03/15/2019	06/19/2019	Currency expense from BRL on maturity of BRL/USD20190619 on 06/19/2019",
						"-13	-10	100000	H-200	Realized Gain / Loss	BRL/USD20190619	3.8832070766	22,938,367.8	-83,531.75	06/19/2019	1	-83,531.75	0	03/15/2019	06/19/2019	Realized Gain / Loss gain for BRL/USD20190619"
				).execute();
	}


	@Test
	public void testNonDeliverableCurrencyForwardMaturity_TwoCurrencies_NoOffset() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("06/19/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(112776, "BRL/USD20190619", InvestmentType.FORWARDS));
		securityEvent.getSecurity().getInstrument().setDeliverable(false); // Non Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("06/17/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("06/19/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		AccountingEventJournalDetail.ForwardMaturity forwardMaturity = eventDetail.forForwardMaturity();
		forwardMaturity.setMaturityPrice(new BigDecimal("3.8832070765768"));
		forwardMaturity.setCurrencyAmount1(new BigDecimal("-5974000.00"));
		forwardMaturity.setCurrencyAmount2(new BigDecimal("-22938367.80"));
		forwardMaturity.setExchangeRate1(BigDecimal.ONE);
		forwardMaturity.setExchangeRate2(new BigDecimal("0.25679544"));
		// BUY 5,974,000.00 USD and SELL 22,938,367.80 BRL in USD Account
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(13959085L, securityEvent.getSecurity()).qty("-22938367.80").price("3.8397").on("03/15/2019").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule())
				.withExpectedResults(
						"-10	13959085	100000	H-200	Position	BRL/USD20190619	3.8832070766	22,938,367.8	0	06/19/2019	1	0	5,907,067.88	03/15/2019	06/19/2019	Security Maturity for BRL/USD20190619 on 06/19/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(), AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	13959085	100000	H-200	Position	BRL/USD20190619	3.8832070766	22,938,367.8	0	06/19/2019	1	0	5,974,000	03/15/2019	06/19/2019	Security Maturity for BRL/USD20190619 on 06/19/2019",
						"-12	-10	100000	H-200	Realized Gain / Loss	BRL/USD20190619	3.8832070766	22,938,367.8	-66,932.12	06/19/2019	1	-66,932.12	0	03/15/2019	06/19/2019	Realized Gain / Loss gain for BRL/USD20190619",
						"-13	-10	100000	H-200	Cash	USD			66,932.12	06/19/2019	1	66,932.12	0	03/15/2019	06/19/2019	Cash proceeds from Security Maturity of BRL/USD20190619"
				).execute();
	}


	@Test
	public void testDeliverableCurrencyForwardMaturity_ThreeCurrencies_NoOffset() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("05/03/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(117435, "JPY/GBP20190503", InvestmentType.FORWARDS, "GBP"));
		securityEvent.getSecurity().getInstrument().setDeliverable(true); // Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("05/03/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("05/03/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		AccountingEventJournalDetail.ForwardMaturity forwardMaturity = eventDetail.forForwardMaturity();
		forwardMaturity.setCurrencyAmount2(new BigDecimal("-3083864360.00"));
		forwardMaturity.setMaturityPrice(new BigDecimal("146.5504267635"));
		forwardMaturity.setCurrencyAmount1(new BigDecimal("-21087745.84"));
		forwardMaturity.setExchangeRate1(new BigDecimal("1.3119"));
		forwardMaturity.setExchangeRate2(new BigDecimal("0.008951867483246"));
		// BUY 21,087,745.84 GBP and SELL 3,083,864,360.00 JPY in USD Account
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(14874092L, securityEvent.getSecurity())
				.qty("-3083864360.00").price("146.23964").exchangeRateToBase("1.322").on("03/18/2019").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = getExistingPositionsForAccountingEventJournal(eventJournal);
		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule)
				.withExpectedResults(
						"-10	14874092	100000	H-200	Position	JPY/GBP20190503	146.5504267635	3,083,864,360	0	05/03/2019	1.3119	0	21,043,025.45	03/18/2019	05/03/2019	Security Maturity for JPY/GBP20190503 on 05/03/2019",
						"-11	-10	100000	H-200	Currency	GBP			21,087,745.84	05/03/2019	1.3119	27,665,013.77	0	03/18/2019	05/03/2019	Currency proceeds from GBP on maturity of JPY/GBP20190503 on 05/03/2019",
						"-12	-10	100000	H-200	Currency	JPY			-3,083,864,360	05/03/2019	0.0089518675	-27,606,345.09	0	03/18/2019	05/03/2019	Currency expense from JPY on maturity of JPY/GBP20190503 on 05/03/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule())
				.withExpectedResults(
						"-10	14874092	100000	H-200	Position	JPY/GBP20190503	146.5504267635	3,083,864,360	0	05/03/2019	1.322	0	21,087,745.84	03/18/2019	05/03/2019	Security Maturity for JPY/GBP20190503 on 05/03/2019",
						"-11	-10	100000	H-200	Currency	GBP			21,087,745.84	05/03/2019	1.3119	27,665,013.77	0	03/18/2019	05/03/2019	Currency proceeds from GBP on maturity of JPY/GBP20190503 on 05/03/2019",
						"-12	-10	100000	H-200	Currency	JPY			-3,083,864,360	05/03/2019	0.0089518675	-27,606,345.09	0	03/18/2019	05/03/2019	Currency expense from JPY on maturity of JPY/GBP20190503 on 05/03/2019",
						"-13	-10	100000	H-200	Realized Gain / Loss	JPY/GBP20190503	146.5504267635	3,083,864,360	-44,720.39	05/03/2019	1.3119	-58,668.68	0	03/18/2019	05/03/2019	Realized Gain / Loss gain for JPY/GBP20190503"
				).execute();
	}


	@Test
	public void testDeliverableCurrencyForwardMaturity_ThreeCurrencies_SamePrice_NoGainLoss() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("06/28/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(1788104, "SEK/EUR20190628", InvestmentType.FORWARDS, "EUR"));
		securityEvent.getSecurity().getInstrument().setDeliverable(true); // Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("06/28/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("06/28/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		createAndAddForwardMaturityEventJournalDetail(eventJournal,
				new BigDecimal("143160"), new BigDecimal("10.626901"), new BigDecimal("13471.47"), new BigDecimal("0.8890201173182158"), DateUtils.toDate("06/21/2019"), // Transaction ID: 14756449
				new BigDecimal("10.626901"), new BigDecimal("0.8947906026557713"), new BigDecimal("0.084200521173179")
		);

		List<AccountingPosition> existingPositions = getExistingPositionsForAccountingEventJournal(eventJournal);
		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule)
				.withExpectedResults(
						"-10	1	100000	H-200	Position	SEK/EUR20190628	10.626901	-143,160	0	06/28/2019	0.8947906027	0	-13,471.47	06/21/2019	06/28/2019	Security Maturity for SEK/EUR20190628 on 06/28/2019",
						"-11	-10	100000	H-200	Currency	EUR			-13,471.47	06/28/2019	0.8947906027	-12,054.15	0	06/21/2019	06/28/2019	Currency expense from EUR on maturity of SEK/EUR20190628 on 06/28/2019",
						"-12	-10	100000	H-200	Currency	SEK			143,160	06/28/2019	0.0842005212	12,054.15	0	06/21/2019	06/28/2019	Currency proceeds from SEK on maturity of SEK/EUR20190628 on 06/28/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(), AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Position	SEK/EUR20190628	10.626901	-143,160	0	06/28/2019	0.8890201173	0	-13,471.47	06/21/2019	06/28/2019	Security Maturity for SEK/EUR20190628 on 06/28/2019",
						"-11	-10	100000	H-200	Currency	EUR			-13,471.47	06/28/2019	0.8947906027	-12,054.15	0	06/21/2019	06/28/2019	Currency expense from EUR on maturity of SEK/EUR20190628 on 06/28/2019",
						"-12	-10	100000	H-200	Currency	SEK			143,160	06/28/2019	0.0842005212	12,054.15	0	06/21/2019	06/28/2019	Currency proceeds from SEK on maturity of SEK/EUR20190628 on 06/28/2019"
				).execute();
	}


	@Test
	public void testDeliverableCurrencyForwardMaturity_ThreeCurrencies_DifferentPrice_HasGainLoss() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("06/28/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(1788104, "SEK/EUR20190628", InvestmentType.FORWARDS, "EUR"));
		securityEvent.getSecurity().getInstrument().setDeliverable(true); // Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("06/28/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("06/28/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		createAndAddForwardMaturityEventJournalDetail(eventJournal,
				new BigDecimal("-143160"), new BigDecimal("10.496568"), new BigDecimal("-13638.74"), new BigDecimal("0.8690712974296206"), DateUtils.toDate("03/22/2019"), // Transaction ID: 14007293
				new BigDecimal("10.626901"), new BigDecimal("0.8947906026557713"), new BigDecimal("0.084200521173179")
		);

		List<AccountingPosition> existingPositions = getExistingPositionsForAccountingEventJournal(eventJournal);
		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule)
				.withExpectedResults(
						"-10	1	100000	H-200	Position	SEK/EUR20190628	10.626901	143,160	0	06/28/2019	0.8947906027	0	13,471.47	03/22/2019	06/28/2019	Security Maturity for SEK/EUR20190628 on 06/28/2019",
						"-11	-10	100000	H-200	Currency	EUR			13,638.74	06/28/2019	0.8947906027	12,203.82	0	03/22/2019	06/28/2019	Currency proceeds from EUR on maturity of SEK/EUR20190628 on 06/28/2019",
						"-12	-10	100000	H-200	Currency	SEK			-143,160	06/28/2019	0.0842005212	-12,054.15	0	03/22/2019	06/28/2019	Currency expense from SEK on maturity of SEK/EUR20190628 on 06/28/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(), AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Position	SEK/EUR20190628	10.626901	143,160	0	06/28/2019	0.8690712974	0	13,638.74	03/22/2019	06/28/2019	Security Maturity for SEK/EUR20190628 on 06/28/2019",
						"-11	-10	100000	H-200	Currency	EUR			13,638.74	06/28/2019	0.8947906027	12,203.82	0	03/22/2019	06/28/2019	Currency proceeds from EUR on maturity of SEK/EUR20190628 on 06/28/2019",
						"-12	-10	100000	H-200	Currency	SEK			-143,160	06/28/2019	0.0842005212	-12,054.15	0	03/22/2019	06/28/2019	Currency expense from SEK on maturity of SEK/EUR20190628 on 06/28/2019",
						"-13	-10	100000	H-200	Realized Gain / Loss	SEK/EUR20190628	10.626901	143,160	-167.27	06/28/2019	0.8947906027	-149.67	0	03/22/2019	06/28/2019	Realized Gain / Loss gain for SEK/EUR20190628"
				).execute();
	}


	@Test
	public void testDeliverableCurrencyForwardMaturity_ThreeCurrencies_MultipleOffsettingLots_WithGainLoss() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("07/31/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(1788104, "ILS/EUR20190731", InvestmentType.FORWARDS, "EUR"));
		securityEvent.getSecurity().getInstrument().setDeliverable(true); // Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("07/31/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("07/31/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		createAndAddForwardMaturityEventJournalDetail(eventJournal,
				new BigDecimal("-8540"), new BigDecimal("4.0606"), new BigDecimal("-2103.14"), new BigDecimal("0.8661745952003711"), DateUtils.toDate("04/24/2019"),
				new BigDecimal("4.062045"), new BigDecimal("0.909306219118788"), new BigDecimal("0.223854294848725"));
		createAndAddForwardMaturityEventJournalDetail(eventJournal,
				new BigDecimal("6015"), new BigDecimal("4.041505"), new BigDecimal("1488.31"), new BigDecimal("0.8826846484482826"), DateUtils.toDate("05/28/2019"),
				new BigDecimal("4.062045"), new BigDecimal("0.909306219118788"), new BigDecimal("0.223854294848725"));
		createAndAddForwardMaturityEventJournalDetail(eventJournal,
				new BigDecimal("2525"), new BigDecimal("4.062045"), new BigDecimal("621.61"), new BigDecimal("0.886302666877071"), DateUtils.toDate("06/05/2019"),
				new BigDecimal("4.062045"), new BigDecimal("0.909306219118788"), new BigDecimal("0.223854294848725"));

		List<AccountingPosition> existingPositions = getExistingPositionsForAccountingEventJournal(eventJournal);
		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule)
				.withExpectedResults(
						"-10	1	100000	H-200	Position	ILS/EUR20190731	4.062045	8,540	0	07/31/2019	0.9093062191	0	34,689.86	04/24/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-11	-10	100000	H-200	Currency	EUR			2,103.14	07/31/2019	0.9093062191	1,912.4	0	04/24/2019	07/31/2019	Currency proceeds from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-12	-10	100000	H-200	Currency	ILS			-8,540	07/31/2019	0.2238542948	-1,911.71	0	04/24/2019	07/31/2019	Currency expense from ILS on maturity of ILS/EUR20190731 on 07/31/2019",
						"-13	2	100000	H-200	Position	ILS/EUR20190731	4.062045	-6,015	0	07/31/2019	0.9093062191	0	-24,433.2	05/28/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-14	-13	100000	H-200	Currency	EUR			-1,488.31	07/31/2019	0.9093062191	-1,353.33	0	05/28/2019	07/31/2019	Currency expense from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-15	-13	100000	H-200	Currency	ILS			6,015	07/31/2019	0.2238542948	1,346.48	0	05/28/2019	07/31/2019	Currency proceeds from ILS on maturity of ILS/EUR20190731 on 07/31/2019",
						"-16	3	100000	H-200	Position	ILS/EUR20190731	4.062045	-2,525	0	07/31/2019	0.9093062191	0	-10,256.66	06/05/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-17	-16	100000	H-200	Currency	EUR			-621.61	07/31/2019	0.9093062191	-565.23	0	06/05/2019	07/31/2019	Currency expense from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-18	-16	100000	H-200	Currency	ILS			2,525	07/31/2019	0.2238542948	565.23	0	06/05/2019	07/31/2019	Currency proceeds from ILS on maturity of ILS/EUR20190731 on 07/31/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						maturityRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule())
				.withExpectedResults(
						"-10	1	100000	H-200	Position	ILS/EUR20190731	4.062045	8,540	0	07/31/2019	0.8661745952	0	2,103.14	04/24/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-11	-10	100000	H-200	Currency	EUR			2,103.14	07/31/2019	0.9093062191	1,912.4	0	04/24/2019	07/31/2019	Currency proceeds from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-12	-10	100000	H-200	Currency	ILS			-8,540	07/31/2019	0.2238542948	-1,911.71	0	04/24/2019	07/31/2019	Currency expense from ILS on maturity of ILS/EUR20190731 on 07/31/2019",
						"-13	2	100000	H-200	Position	ILS/EUR20190731	4.062045	-6,015	0	07/31/2019	0.8826846484	0	-1,488.31	05/28/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-14	-13	100000	H-200	Currency	EUR			-1,488.31	07/31/2019	0.9093062191	-1,353.33	0	05/28/2019	07/31/2019	Currency expense from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-15	-13	100000	H-200	Currency	ILS			6,015	07/31/2019	0.2238542948	1,346.48	0	05/28/2019	07/31/2019	Currency proceeds from ILS on maturity of ILS/EUR20190731 on 07/31/2019",
						"-16	3	100000	H-200	Position	ILS/EUR20190731	4.062045	-2,525	0	07/31/2019	0.8863026669	0	-621.61	06/05/2019	07/31/2019	Security Maturity for ILS/EUR20190731 on 07/31/2019",
						"-17	-16	100000	H-200	Currency	EUR			-621.61	07/31/2019	0.9093062191	-565.23	0	06/05/2019	07/31/2019	Currency expense from EUR on maturity of ILS/EUR20190731 on 07/31/2019",
						"-18	-16	100000	H-200	Currency	ILS			2,525	07/31/2019	0.2238542948	565.23	0	06/05/2019	07/31/2019	Currency proceeds from ILS on maturity of ILS/EUR20190731 on 07/31/2019",
						"-19	-10	100000	H-200	Realized Gain / Loss	ILS/EUR20190731	4.062045	8,540	-0.76	07/31/2019	0.9093062191	-0.69	0	04/24/2019	07/31/2019	Realized Gain / Loss gain for ILS/EUR20190731",
						"-20	-13	100000	H-200	Realized Gain / Loss	ILS/EUR20190731	4.062045	6,015	7.53	07/31/2019	0.9093062191	6.85	0	05/28/2019	07/31/2019	Realized Gain / Loss loss for ILS/EUR20190731"
				).execute();
	}


	@Test
	public void testNonDeliverableCurrencyForwardMaturity_ThreeCurrencies_NoOffset() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("05/03/2019"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(117435, "JPY/GBP20190503", InvestmentType.FORWARDS, "GBP"));
		securityEvent.getSecurity().getInstrument().setDeliverable(false); // Non Deliverable Only
		securityEvent.getSecurity().setEndDate(DateUtils.toDate("05/03/2019"));
		securityEvent.getSecurity().setFirstDeliveryDate(DateUtils.toDate("05/03/2019"));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		AccountingEventJournalDetail.ForwardMaturity forwardMaturity = eventDetail.forForwardMaturity();
		forwardMaturity.setCurrencyAmount2(new BigDecimal("-3083864360.00"));
		forwardMaturity.setMaturityPrice(new BigDecimal("146.5504267635"));
		forwardMaturity.setCurrencyAmount1(new BigDecimal("-21087745.84"));
		forwardMaturity.setExchangeRate1(new BigDecimal("1.3119"));
		forwardMaturity.setExchangeRate2(new BigDecimal("0.008951867483246"));
		// BUY 21,087,745.84 GBP and SELL 3,083,864,360.00 JPY in USD Account
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(14874092L, securityEvent.getSecurity())
				.qty("-3083864360.00").price("146.23964").exchangeRateToBase("1.322").on("03/18/2019").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule())
				.withExpectedResults(
						"-10	14874092	100000	H-200	Position	JPY/GBP20190503	146.5504267635	3,083,864,360	0	05/03/2019	1.3119	0	21,043,025.45	03/18/2019	05/03/2019	Security Maturity for JPY/GBP20190503 on 05/03/2019"
				).execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(), AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-10	14874092	100000	H-200	Position	JPY/GBP20190503	146.5504267635	3,083,864,360	0	05/03/2019	1.322	0	21,087,745.84	03/18/2019	05/03/2019	Security Maturity for JPY/GBP20190503 on 05/03/2019",
						"-12	-10	100000	H-200	Realized Gain / Loss	JPY/GBP20190503	146.5504267635	3,083,864,360	-44,720.39	05/03/2019	1.3119	-58,668.68	0	03/18/2019	05/03/2019	Realized Gain / Loss gain for JPY/GBP20190503",
						"-13	-10	100000	H-200	Currency	GBP			44,720.39	05/03/2019	1.3119	58,668.68	0	03/18/2019	05/03/2019	Currency proceeds from Security Maturity of JPY/GBP20190503"
				).execute();
	}


	@Test
	public void testDomesticBondMaturity() {
		// setup event journal with one detail
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("03/29/2012"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17976, "9127955F4", InvestmentType.BONDS));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("100"));
		eventDetail.setAffectedQuantity(new BigDecimal("20000000"));
		eventDetail.setAffectedCost(new BigDecimal("19998711.11"));
		eventDetail.setExchangeRateToBase(BigDecimal.ONE);
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1640429L, securityEvent.getSecurity()).qty(20000000).price("99.99355556").on("01/30/2012").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);
		mockSettlementDate(maturityRule, DateUtils.toDate("03/30/2012"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule())
				.withExpectedResults(
						"-10	1640429	100000	H-200	Position	9127955F4	100	-20,000,000	-19,998,711.11	03/29/2012	1	-19,998,711.11	-19,998,711.11	01/30/2012	03/29/2012	Security Maturity for 9127955F4 on 03/29/2012",
						"-11	-10	100000	H-200	Cash	USD			20,000,000	03/29/2012	1	20,000,000	0	01/30/2012	03/29/2012	Cash proceeds from Security Maturity of 9127955F4",
						"-12	-10	100000	H-200	Realized Gain / Loss	9127955F4	100	20,000,000	-1,288.89	03/29/2012	1	-1,288.89	0	01/30/2012	03/29/2012	Realized Gain / Loss gain for 9127955F4"
				).execute();
	}


	@Test
	public void testForeignBondMaturity() {
		// setup event journal with one detail
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("07/25/2015"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17568, "FR0010135525", InvestmentType.BONDS, "EUR"));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("100"));
		eventDetail.setAffectedQuantity(new BigDecimal("100"));
		eventDetail.setAffectedCost(new BigDecimal("124.89"));
		eventDetail.setExchangeRateToBase(new BigDecimal("1.09655"));
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(2995058L, securityEvent.getSecurity())
				.qty(100).price("105.199").costBasis("124.89").exchangeRateToBase("1.34925").on("09/25/2013").build());
		eventJournal.addDetail(eventDetail);

		List<AccountingPosition> existingPositions = new ArrayList<>();
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);
		mockNotionalCalculator(maturityRule, new BigDecimal("-120.04"));
		mockSettlementDate(maturityRule, DateUtils.toDate("07/29/2015"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(maturityRule, AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule())
				.withExpectedResults(
						"-10	2995058	100000	H-200	Position	FR0010135525	100	-100	-124.89	07/25/2015	1.34925	-124.89	-124.89	09/25/2013	07/25/2015	Security Maturity for FR0010135525 on 07/25/2015",
						"-11	-10	100000	H-200	Currency	EUR			120.04	07/25/2015	1.09655	131.63	0	09/25/2013	07/25/2015	Currency proceeds from Security Maturity of FR0010135525",
						"-12	-10	100000	H-200	Realized Gain / Loss	FR0010135525	100	100	4.85	07/25/2015	1.09655	5.32	0	09/25/2013	07/25/2015	Realized Gain / Loss loss for FR0010135525",
						"-13	-10	100000	H-200	Currency Translation Gain / Loss	FR0010135525			0	07/25/2015	1.09655	-12.06	0	09/25/2013	07/25/2015	Currency Translation Gain / Loss for FR0010135525"
				).execute();
	}


	@Test
	public void testDomesticLME_Maturity() {
		// setup event journal with 3 details: +68, -1, and -67 (355000 account)
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.SECURITY_MATURITY));
		securityEvent.setEventDate(DateUtils.toDate("03/21/2012"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(14802, "LAH2", InvestmentType.FUTURES, new BigDecimal(25), "USD"));
		securityEvent.getSecurity().getInstrument().getHierarchy().setCloseOnMaturityOnly(true);
		eventJournal.setSecurityEvent(securityEvent);

		List<AccountingPosition> existingPositions = new ArrayList<>();

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("2105"));
		eventDetail.setAffectedQuantity(new BigDecimal("68"));
		eventDetail.setAffectedCost(new BigDecimal("3704300"));
		eventDetail.setExchangeRateToBase(BigDecimal.ONE);
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1391108L, securityEvent.getSecurity())
				.qty(eventDetail.getAffectedQuantity()).price("2179").on("10/05/2011").build());
		eventJournal.addDetail(eventDetail);
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("2105"));
		eventDetail.setAffectedQuantity(new BigDecimal("-1"));
		eventDetail.setAffectedCost(new BigDecimal("-52625.00"));
		eventDetail.setExchangeRateToBase(BigDecimal.ONE);
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1515310L, securityEvent.getSecurity())
				.qty(eventDetail.getAffectedQuantity()).price("2105").on("12/06/2011").build());
		eventJournal.addDetail(eventDetail);
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("2105"));
		eventDetail.setAffectedQuantity(new BigDecimal("-67"));
		eventDetail.setAffectedCost(new BigDecimal("-3534250.00"));
		eventDetail.setExchangeRateToBase(BigDecimal.ONE);
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1515316L, securityEvent.getSecurity())
				.qty(eventDetail.getAffectedQuantity()).price("2110").on("12/06/2011").build());
		eventJournal.addDetail(eventDetail);
		existingPositions.add(newAccountingPosition(securityEvent, eventDetail));

		// mock booking rules
		AccountingSecurityMaturityBookingRule maturityRule = newAccountingSecurityMaturityBookingRule(securityEvent, existingPositions);
		mockNotionalCalculatorForQuantity(maturityRule, new BigDecimal("-68"), new BigDecimal("-3578500"));
		mockNotionalCalculatorForQuantity(maturityRule, new BigDecimal("1"), new BigDecimal("52625"));
		mockNotionalCalculatorForQuantity(maturityRule, new BigDecimal("67"), new BigDecimal("3525875"));
		mockSettlementDate(maturityRule, DateUtils.toDate("03/21/2012"));

		// verify that 6 entries were created: 3 closes, 2 realized and one cash
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						maturityRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						"-10	1391108	100000	H-200	Position	LAH2	2,105	-68	0	03/21/2012	1	0	-3,704,300	10/05/2011	03/21/2012	Security Maturity for LAH2 on 03/21/2012",
						"-12	1515310	100000	H-200	Position	LAH2	2,105	1	0	03/21/2012	1	0	52,625	12/06/2011	03/21/2012	Security Maturity for LAH2 on 03/21/2012",
						"-14	1515316	100000	H-200	Position	LAH2	2,105	67	0	03/21/2012	1	0	3,534,250	12/06/2011	03/21/2012	Security Maturity for LAH2 on 03/21/2012",
						"-16	-10	100000	H-200	Realized Gain / Loss	LAH2	2,105	68	125,800	03/21/2012	1	125,800	0	10/05/2011	03/21/2012	Realized Gain / Loss loss for LAH2",
						"-20	-14	100000	H-200	Realized Gain / Loss	LAH2	2,105	67	-8,375	03/21/2012	1	-8,375	0	12/06/2011	03/21/2012	Realized Gain / Loss gain for LAH2",
						"-22	-10	100000	H-200	Cash	USD			-125,800	03/21/2012	1	-125,800	0	10/05/2011	03/21/2012	Cash expense from Security Maturity of LAH2",
						"-24	-14	100000	H-200	Cash	USD			8,375	03/21/2012	1	8,375	0	12/06/2011	03/21/2012	Cash proceeds from Security Maturity of LAH2"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournalDetail createAndAddForwardMaturityEventJournalDetail(AccountingEventJournal eventJournal, BigDecimal openingQuantity, BigDecimal openingPrice, BigDecimal openingCostBasis, BigDecimal openingExchangeRateToBase, Date openingDate, BigDecimal maturityPrice, BigDecimal maturityCurrency1ExchangeRate, BigDecimal maturityCurrency2ExchangeRate) {
		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		AccountingEventJournalDetail.ForwardMaturity forwardMaturity = eventDetail.forForwardMaturity();
		forwardMaturity.setCurrencyAmount2(openingQuantity);
		forwardMaturity.setMaturityPrice(maturityPrice);
		forwardMaturity.setOpeningPrice(openingPrice);
		forwardMaturity.setCurrencyAmount1(openingCostBasis);
		forwardMaturity.setExchangeRate1(maturityCurrency1ExchangeRate);
		forwardMaturity.setExchangeRate2(maturityCurrency2ExchangeRate);
		long transactionId = 1L + CollectionUtils.getSize(eventJournal.getDetailList());

		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(transactionId, eventJournal.getSecurityEvent().getSecurity())
				.qty(openingQuantity).price(openingPrice).costBasis(openingCostBasis).exchangeRateToBase(openingExchangeRateToBase).on(openingDate).build());
		eventJournal.addDetail(eventDetail);
		return eventDetail;
	}


	private List<AccountingPosition> getExistingPositionsForAccountingEventJournal(AccountingEventJournal eventJournal) {
		List<AccountingPosition> existingPositions = new ArrayList<>();
		for (AccountingEventJournalDetail detail : eventJournal.getDetailList()) {
			if (detail.getAccountingTransaction() != null) {
				existingPositions.add(newAccountingPosition(eventJournal.getSecurityEvent(), detail));
			}
		}
		return existingPositions;
	}


	private AccountingSecurityMaturityBookingRule newAccountingSecurityMaturityBookingRule(InvestmentSecurityEvent securityEvent, List<AccountingPosition> existingPositions) {
		AccountingSecurityMaturityBookingRule maturityRule = new AccountingSecurityMaturityBookingRule();
		maturityRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(maturityRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(new AccountingPositionCommand().forInvestmentSecurity(securityEvent.getSecurity().getId()))))
				.thenReturn(existingPositions);
		maturityRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		maturityRule.setInvestmentCalculator(Mockito.mock(InvestmentCalculator.class));
		maturityRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(maturityRule.getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail")).thenReturn(new SystemTable());
		maturityRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		AccountingTestObjectFactory.newAccountingBookingPositionRetriever(maturityRule);

		Mockito.when(maturityRule.getInvestmentCalculator().calculateNotional(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(Date.class)))
				.thenAnswer(arguments -> {
					InvestmentSecurity security = (InvestmentSecurity) arguments.getArguments()[0];
					return InvestmentCalculatorUtils.getNotionalCalculator(security).calculateNotional((BigDecimal) arguments.getArguments()[1], security.getPriceMultiplier(), (BigDecimal) arguments.getArguments()[2]);
				});

		return maturityRule;
	}


	private AccountingPosition newAccountingPosition(InvestmentSecurityEvent securityEvent, AccountingEventJournalDetail eventDetail) {
		return AccountingTestObjectFactory.newAccountingPosition(eventDetail.getAccountingTransaction().getId(), securityEvent.getSecurity(), eventDetail.getAccountingTransaction().getQuantity().intValue(),
				eventDetail.getAccountingTransaction().getPrice(), eventDetail.getAccountingTransaction().getPositionCostBasis(), eventDetail.getAccountingTransaction().getTransactionDate());
	}


	private void mockNotionalCalculatorForQuantity(AccountingSecurityMaturityBookingRule maturityRule, BigDecimal quantity, BigDecimal result) {
		Mockito.when(
				maturityRule.getInvestmentCalculator().calculateNotional(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.eq(quantity), ArgumentMatchers.any(Date.class))
		).thenReturn(result);
	}


	private void mockNotionalCalculator(AccountingSecurityMaturityBookingRule maturityRule, BigDecimal result) {
		Mockito.when(
				maturityRule.getInvestmentCalculator().calculateNotional(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(Date.class))
		).thenReturn(result);
	}


	private void mockSettlementDate(AccountingSecurityMaturityBookingRule maturityRule, Date result) {
		Mockito.when(maturityRule.getInvestmentCalculator().calculateSettlementDate(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(result);
	}
}
