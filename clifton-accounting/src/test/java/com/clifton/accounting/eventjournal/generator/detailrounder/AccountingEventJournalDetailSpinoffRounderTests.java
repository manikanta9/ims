package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailSpinoffPopulator;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class AccountingEventJournalDetailSpinoffRounderTests {

	private static final InvestmentSecurity DWDP_STOCK = InvestmentTestObjectFactory.newInvestmentSecurity(100, "DWDP", InvestmentType.STOCKS);
	private static final InvestmentSecurity CTVA_STOCK = InvestmentTestObjectFactory.newInvestmentSecurity(110, "CTVA", InvestmentType.STOCKS);


	private final static String[] ROUND_DOWN_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	23.80108	5,521.85	77.3333333333	2,305.69	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	23.801104	76,306.34	1,068.6666666668	31,862.25	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	25.43377	3,916.8	51.3333333333	1,635.49	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	24.59403	8,853.85	120	3,696.99	0	null"
	};
	private final static String[] ROUND_UP_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	23.80108	5,521.85	77.3333333333	2,305.69	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	23.801104	76,306.34	1,068.6666666668	31,862.25	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	25.43377	3,916.8	51.3333333333	1,635.49	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	24.59403	8,853.85	120	3,696.99	0	null"
	};
	private final static String[] CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	23.80108	5,521.85	77.3333333333	2,305.69	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	23.801104	76,306.34	1,068.6666666668	31,862.25	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	25.43377	3,916.8	51.3333333333	1,635.49	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	24.59403	8,853.85	120	3,696.99	0	null"
	};
	private final static String[] CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	23.8011	1,523.27	21.3333333333	636.05	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	23.80108	5,521.85	77.3333333333	2,305.69	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	23.801104	76,306.34	1,068.6666666668	31,862.25	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	25.43377	3,916.8	51.3333333333	1,635.49	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	24.59403	8,853.85	120	3,696.99	0	null"
	};

	///////////////////////////////////////////////////////////////////////////
	// Stock Spinoff
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void test_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderTest(null, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(null, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderTest(null, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderTest(null, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	private void executeFirstDetailRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeProportionalRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.PROPORTIONALLY);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeTest(AccountingEventJournalDetailRounder rounder, FractionalShares fractionalShares, String[] expectedDetails) {
		AccountingEventJournalDetailRounderTestExecutor.SpinoffDetailRounderTestExecutor.of(rounder)
				.withEventJournal(getSpinoffJournal(fractionalShares))
				.withFractionalShares(fractionalShares)
				.withExpectedResults(expectedDetails)
				.execute();
	}


	@Test
	public void testQuantityRoundingPrecision() {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		FractionalShares fractionalShares = FractionalShares.CASH_IN_LIEU;

		EventData eventData = getSpinoffSecurityEvent(fractionalShares);
		AccountingTransaction[] transactions = new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, eventData.getSecurity()).qty(140000).price("33.5734278").on("12/13/2017").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, eventData.getSecurity()).qty(125000).price("26.3199291").on("05/02/2019").build()
		};

		AccountingEventJournal accountingEventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, new AccountingEventJournalDetailSpinoffPopulator(), transactions);

		AccountingEventJournalDetailRounderTestExecutor.StockSplitDetailRounderTestExecutor.of(rounder)
				.withEventJournal(accountingEventJournal)
				.withFractionalShares(fractionalShares)
				.withExpectedResults(new String[]{
						"1	null	100000	H-200	Position	12/13/2017	1	140,000	4,700,279.89	23.68400536	3,315,760.75	46,666.6666666666	1,384,519.14	0.3333333333	null",
						"2	null	100000	H-200	Position	05/02/2019	1	125,000	3,289,991.14	18.5671045	2,320,888.06	41,666.6666666667	969,103.08	0	null"
				}).execute();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal getSpinoffJournal(FractionalShares fractionalShares) {
		EventData eventData = getSpinoffSecurityEvent(fractionalShares);
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, new AccountingEventJournalDetailSpinoffPopulator(), getTransactions(eventData.getEvent()));
	}


	private EventData getSpinoffSecurityEvent(FractionalShares fractionalShares) {
		// 10 to 1 FB Stock Spinoff for WM on 02/01/2018
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_SPINOFF);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(DWDP_STOCK, eventType, "06/03/2019");
		event.setAdditionalSecurity(CTVA_STOCK);
		event.setBeforeEventValue(new BigDecimal("3"));
		event.setAfterEventValue(new BigDecimal("1"));
		event.setAdditionalEventValue(new BigDecimal("0.705439"));

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_NEW, CTVA_STOCK, fractionalShares);
		payout.setAdditionalPayoutValue(event.getAdditionalEventValue());

		return EventData.forEventPayoutOrEvent(payout, event);
	}


	private AccountingTransaction[] getTransactions(InvestmentSecurityEvent event) {
		return new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283737, event.getSecurity()).qty(232).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283738, event.getSecurity()).qty(3206).price("33.739423").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283739, event.getSecurity()).qty(154).price("36.0538").on("01/25/2018").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283740, event.getSecurity()).qty(360).price("34.86344").on("02/02/2018").build()
		};
	}
}
