package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailFactorChangePopulator;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetailBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferTypeBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class AccountingFactorChangeBookingRuleTests {

	@Test
	public void testDelayedFactorChangeAccrual() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						configureAccountingFactorChangeBookingRule(eventJournal, false)
				)
				.withExpectedResults(
						"-10	223	100000	H-200	Position	31288VXX2	101.8749985031	-2,343.38	-2,387.32	12/31/2013	1	-2,387.32	-2,387.32	09/12/2007	02/15/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 02/15/2014",
						"-11	-10	100000	H-200	Realized Gain / Loss	31288VXX2	101.8749985031		43.94	12/31/2013	1	43.94	0	09/12/2007	02/15/2014	Realized Gain / Loss from Factor Change for 31288VXX2 on 02/15/2014",
						"-12	223	100000	H-200	Payment Receivable	USD			2,343.38	12/31/2013	1	2,343.38	0	09/12/2007	02/15/2014	Accrual of Factor Change for 31288VXX2 to be paid on 02/15/2014"
				).execute();
	}


	@Test
	public void testDelayedFactorChangeDoNotAccrue() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule accountingFactorChangeBookingRule = configureAccountingFactorChangeBookingRule(eventJournal, false);
		eventJournal.getSecurityEvent().setExDate(DateUtils.toDate("02/15/2014"));
		eventJournal.getSecurityEvent().setEventDate(DateUtils.toDate("01/01/2014"));
		eventJournal.getDetailList().forEach(d -> {
			d.setAdditionalAmount(new BigDecimal("-5989.06"));
			d.setAdditionalAmount2(new BigDecimal("-179.85"));
		});
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						accountingFactorChangeBookingRule
				)
				.withExpectedResults(
						"-10	223	100000	H-200	Position	31288VXX2	101.8749985031	3,645.68	-2,387.32	01/01/2014	1	-2,387.32	-2,387.32	09/12/2007	01/01/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 01/01/2014",
						"-11	-10	100000	H-200	Realized Gain / Loss	31288VXX2	101.8749985031		6,033	01/01/2014	1	6,033	0	09/12/2007	01/01/2014	Realized Gain / Loss from Factor Change for 31288VXX2 on 01/01/2014",
						"-12	-10	100000	H-200	Cash	USD			2,387.32	01/01/2014	1	2,387.32	0	09/12/2007	01/01/2014	Cash proceeds from Factor Change of 31288VXX2",
						"-13	-10	100000	H-200	Cash	USD			-6,033	01/01/2014	1	-6,033	0	09/12/2007	01/01/2014	Cash expense from Factor Change of 31288VXX2",
						"-14	-10	100000	H-200	Principal Losses	31288VXX2	101.8749985031		-5,989.06	01/01/2014	1	-5,989.06	0	09/12/2007	01/01/2014	Principal Losses from Factor Change for 31288VXX2",
						"-15	-10	100000	H-200	Cash	USD			5,989.06	01/01/2014	1	5,989.06	0	09/12/2007	01/01/2014	Cash proceeds from Factor Change of 31288VXX2"
				).execute();
	}


	@Test
	public void testDelayedFactorChangeDoNotAccrueForCollateral() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule accountingFactorChangeBookingRule = configureAccountingFactorChangeBookingRule(eventJournal, true);
		eventJournal.getSecurityEvent().setExDate(DateUtils.toDate("02/15/2014"));
		eventJournal.getSecurityEvent().setEventDate(DateUtils.toDate("01/01/2014"));
		eventJournal.getDetailList().forEach(d -> {
			d.setAdditionalAmount(new BigDecimal("-5989.06"));
			d.setAdditionalAmount2(new BigDecimal("-179.85"));
		});
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						accountingFactorChangeBookingRule
				)
				.withExpectedResults(
						"-10	223	100000	H-201	Position Collateral	31288VXX2	101.8749985031	3,645.68	-2,387.32	01/01/2014	1	-2,387.32	-2,387.32	09/12/2007	01/01/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 01/01/2014",
						"-11	223	100000	H-200	Position Collateral Receivable	31288VXX2	101.8749985031	3,645.68	-2,387.32	01/01/2014	1	-2,387.32	-2,387.32	09/12/2007	01/01/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 01/01/2014",
						"-12	223	100000	H-201	Position Collateral Payable	31288VXX2	101.8749985031	-3,645.68	2,387.32	01/01/2014	1	2,387.32	2,387.32	09/12/2007	01/01/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 01/01/2014",
						"-13	-10	100000	H-201	Realized Gain / Loss	31288VXX2	101.8749985031		6,033	01/01/2014	1	6,033	0	09/12/2007	01/01/2014	Realized Gain / Loss from Factor Change for 31288VXX2 on 01/01/2014",
						"-14	-10	100000	H-201	Cash Collateral	USD			2,387.32	01/01/2014	1	2,387.32	0	09/12/2007	01/01/2014	Cash Collateral proceeds from Factor Change of 31288VXX2",
						"-15	-10	100000	H-201	Cash	USD			-6,033	01/01/2014	1	-6,033	0	09/12/2007	01/01/2014	Cash expense from Factor Change of 31288VXX2",
						"-16	-10	100000	H-201	Principal Losses	31288VXX2	101.8749985031		-5,989.06	01/01/2014	1	-5,989.06	0	09/12/2007	01/01/2014	Principal Losses from Factor Change for 31288VXX2",
						"-17	-10	100000	H-201	Cash	USD			5,989.06	01/01/2014	1	5,989.06	0	09/12/2007	01/01/2014	Cash proceeds from Factor Change of 31288VXX2"
				).execute();
	}


	@Test
	public void testDelayedFactorChangeAccrualForCollateral() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						configureAccountingFactorChangeBookingRule(eventJournal, true)
				)
				.withExpectedResults(
						"-10	223	100000	H-201	Position Collateral	31288VXX2	101.8749985031	-2,343.38	-2,387.32	12/31/2013	1	-2,387.32	-2,387.32	09/12/2007	02/15/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 02/15/2014",
						"-11	223	100000	H-200	Position Collateral Receivable	31288VXX2	101.8749985031	-2,343.38	-2,387.32	12/31/2013	1	-2,387.32	-2,387.32	09/12/2007	02/15/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 02/15/2014",
						"-12	223	100000	H-201	Position Collateral Payable	31288VXX2	101.8749985031	2,343.38	2,387.32	12/31/2013	1	2,387.32	2,387.32	09/12/2007	02/15/2014	0.0103098 to 0.01011238 Factor Change for 31288VXX2 on 02/15/2014",
						"-13	-10	100000	H-201	Realized Gain / Loss	31288VXX2	101.8749985031		43.94	12/31/2013	1	43.94	0	09/12/2007	02/15/2014	Realized Gain / Loss from Factor Change for 31288VXX2 on 02/15/2014",
						"-14	223	100000	H-201	Payment Receivable	USD			2,343.38	12/31/2013	1	2,343.38	0	09/12/2007	02/15/2014	Accrual of Factor Change for 31288VXX2 to be paid on 02/15/2014"
				).execute();
	}


	@Test
	public void testDelayedFactorChangeReversal() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingFactorChangeBookingRule(eventJournal, false);
		factorChangeRule.setAccrualReversal(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(factorChangeRule)
				.withExpectedResults(
						"-10	223	100000	H-200	Payment Receivable	USD			-2,343.38	02/15/2014	1	-2,343.38	0	09/12/2007	02/15/2014	Reverse accrual of Factor Change for 31288VXX2 from 12/31/2013",
						"-11	223	100000	H-200	Cash	USD			2,343.38	02/15/2014	1	2,343.38	0	09/12/2007	02/15/2014	Cash proceeds from Factor Change of 31288VXX2"
				).execute();
	}


	@Test
	public void testDelayedFactorChangeReversalForCollateral() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingFactorChangeBookingRule(eventJournal, true);
		factorChangeRule.setAccrualReversal(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(factorChangeRule)
				.withExpectedResults(
						"-10	223	100000	H-201	Payment Receivable	USD			-2,343.38	02/15/2014	1	-2,343.38	0	09/12/2007	02/15/2014	Reverse accrual of Factor Change for 31288VXX2 from 12/31/2013",
						"-11	223	100000	H-201	Cash	USD			2,343.38	02/15/2014	1	2,343.38	0	09/12/2007	02/15/2014	Cash proceeds from Factor Change of 31288VXX2"
				).execute();
	}


	@Test
	public void testFactorChange_AssumedSell_CashUsesPositionAsParent() {
		InvestmentSecurity bond = InvestmentTestObjectFactory.newInvestmentSecurity(21881, "3136AEZZ3", InvestmentType.BONDS);
		InvestmentSecurityEvent factorChange = InvestmentTestObjectFactory.newInvestmentSecurityEvent(bond, InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.FACTOR_CHANGE), "08/01/2019");
		factorChange.setBeforeEventValue(new BigDecimal("0.1498705"));
		factorChange.setAfterEventValue(new BigDecimal("0.14849709"));
		factorChange.setEventDate(DateUtils.toDate("09/01/2019"));


		AccountingEventJournalDetailFactorChangePopulator populator = new AccountingEventJournalDetailFactorChangePopulator();
		populator.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		Mockito.when(populator.getAccountingPositionService().getAccountingPositionList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.eq(bond.getId()), ArgumentMatchers.any(Date.class)))
				.thenReturn(Collections.emptyList());

		AccountingTransaction closingTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(519030L, bond).qty(124313).price("98.75").on("08/28/2019").build();
		closingTransaction.setOpening(false);

		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEvent(factorChange), populator, closingTransaction);
		eventJournal.setJournalType(AccountingEventJournalTestObjectFactory.newAccountingEventJournalTypeForEventWithAccountingEventTypeName(EventData.forEvent(factorChange), AccountingEventJournalType.FACTOR_CHANGE_ASSUMED_SELL));

		AccountingFactorChangeForAssumedSellBookingRule factorChangeRule = new AccountingFactorChangeForAssumedSellBookingRule();
		factorChangeRule.setAccountingPositionService(populator.getAccountingPositionService());
		factorChangeRule.setAccountingPeriodService(Mockito.mock(AccountingPeriodService.class));
		factorChangeRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		factorChangeRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(factorChangeRule.getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail")).thenReturn(new SystemTable());
		factorChangeRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						factorChangeRule,
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	519030	100000	H-200	Realized Gain / Loss	3136AEZZ3	true	Goldman Sachs			-1,553.91	09/01/2019	1	-1,553.91	0	09/01/2019	09/01/2019	0.1498705 to 0.14849709 Factor Change for 3136AEZZ3 on 09/01/2019",
						"-11	519030	100000	H-200	Interest Income	3136AEZZ3	true	Goldman Sachs			-18,460.12	09/01/2019	1	-18,460.12	0	09/01/2019	09/01/2019	0.1498705 to 0.14849709 Factor Change for 3136AEZZ3 on 09/01/2019",
						"-12	519030	100000	H-200	Cash	USD	true	Goldman Sachs			20,014.03	09/01/2019	1	20,014.03	0	09/01/2019	09/01/2019	Cash proceeds from Factor Change of 3136AEZZ3"
				).execute();
	}


	private AccountingFactorChangeBookingRule configureAccountingFactorChangeBookingRule(AccountingEventJournal eventJournal, boolean collateral) {
		// setup event journal with one detail
		InvestmentSecurityEventType factorChange = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.FACTOR_CHANGE);
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(factorChange);
		securityEvent.setBeforeEventValue(new BigDecimal("0.0103098"));
		securityEvent.setAfterEventValue(new BigDecimal("0.01011238"));
		securityEvent.setExDate(DateUtils.toDate("01/01/2014"));
		securityEvent.setEventDate(DateUtils.toDate("02/15/2014"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(6008, "31288VXX2", InvestmentType.BONDS));
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalType journalType = new AccountingEventJournalType();
		journalType.setEventType(factorChange);
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
		journalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
		journalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		journalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		journalType.setSettlementDateComparison(true);
		eventJournal.setJournalType(journalType);

		List<AccountingEventJournalDetail> details = buildPositions(securityEvent.getSecurity(), collateral);
		for (AccountingEventJournalDetail detail : details) {
			if (!detail.getAccountingTransaction().getAccountingAccount().isReceivable()) {
				eventJournal.addDetail(detail);
			}
		}
		AccountingTransactionService accountingTransactionService = Mockito.mock(AccountingTransactionService.class);
		Mockito.when(accountingTransactionService.getAccountingTransaction(ArgumentMatchers.anyLong())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return details.stream()
					.map(AccountingEventJournalDetail::getAccountingTransaction)
					.filter(t -> t.getId().equals(id))
					.findFirst()
					.orElse(null);
		});

		AccountingFactorChangeBookingRule factorChangeRule = new AccountingFactorChangeBookingRule();
		factorChangeRule.setAccountingTransactionService(accountingTransactionService);
		factorChangeRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		factorChangeRule.setAccountingPeriodService(Mockito.mock(AccountingPeriodService.class));
		factorChangeRule.setAccountingPositionTransferService(Mockito.mock(AccountingPositionTransferService.class));

		List<AccountingPosition> existingPositions = details.stream()
				.map(AccountingEventJournalDetail::getAccountingTransaction)
				.map(t -> {
							AccountingPosition position = AccountingTestObjectFactory.newAccountingTransaction(
									t.getId(),
									t.getInvestmentSecurity(),
									t.getAccountingAccount(),
									t.getQuantity(),
									t.getPrice(),
									t.getPositionCostBasis(),
									t.getExchangeRateToBase(),
									t.getTransactionDate());
							position.getOpeningTransaction().setParentTransaction(t.getParentTransaction());
							position.getOpeningTransaction().setHoldingInvestmentAccount(t.getHoldingInvestmentAccount());
							return position;
						}
				).collect(Collectors.toList());

		Mockito.when(
				factorChangeRule.getAccountingPositionService().getAccountingPositionList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.eq(securityEvent.getSecurity().getId()),
						ArgumentMatchers.any(Date.class))
		).thenReturn(existingPositions.stream().filter(p -> !p.getAccountingAccount().isReceivable()).collect(Collectors.toList()));
		Mockito.when(
				factorChangeRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))
		).thenReturn(existingPositions.stream().filter(p -> p.getAccountingAccount().isReceivable()).collect(Collectors.toList()));
		factorChangeRule.setAccountingAccountService(accountingAccountService);
		factorChangeRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(factorChangeRule.getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail")).thenReturn(new SystemTable());
		factorChangeRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		Mockito.when(factorChangeRule.getAccountingPositionTransferService().getAccountingPositionTransferDetail(ArgumentMatchers.anyInt()))
				.thenReturn(AccountingPositionTransferDetailBuilder.createEmpty()
						.withPositionTransfer(AccountingPositionTransferBuilder.createEmpty()
								.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
								.toAccountPositionTransfer()
						)
						.toAccountingPositionTransferDetail()
				);

		return factorChangeRule;
	}


	private List<AccountingEventJournalDetail> buildPositions(InvestmentSecurity investmentSecurity, boolean collateral) {
		List<AccountingEventJournalDetail> results = new ArrayList<>();
		AccountingEventJournalDetail journalDetail = buildPosition(investmentSecurity, collateral);
		results.add(journalDetail);
		if (collateral) {
			results.addAll(buildPositionCollateralPayableReceivable(journalDetail, investmentSecurity));
		}
		return results;
	}


	private AccountingEventJournalDetail buildPosition(InvestmentSecurity investmentSecurity, boolean collateral) {
		final AccountingAccount positionAccount;
		final InvestmentAccount holdingAccount;
		if (collateral) {
			positionAccount = AccountingTestObjectFactory.newAccountingAccount_CollateralPosition();
			holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2();
		}
		else {
			positionAccount = AccountingTestObjectFactory.newAccountingAccount_Position();
			holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();
		}

		AtomicLong atomicLong = new AtomicLong(222);
		Supplier<AccountingTransaction> transactionSupplier = () -> AccountingTransactionBuilder
				.newTransactionForIdAndSecurity(atomicLong.incrementAndGet(), investmentSecurity)
				.accountingAccount(positionAccount)
				.holdingInvestmentAccount(holdingAccount)
				.qty(11870000)
				.price("101.8749985031")
				.on("09/12/2007")
				.build();

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		eventDetail.setTransactionPrice(new BigDecimal("101.8749985031"));
		eventDetail.setAffectedQuantity(new BigDecimal("122377.33"));
		eventDetail.setAffectedCost(new BigDecimal("2387.32"));
		eventDetail.setTransactionAmount(new BigDecimal("2343.38"));
		eventDetail.setExchangeRateToBase(BigDecimal.ONE);
		eventDetail.setAccountingTransaction(
				transactionSupplier.get()
		);
		if (collateral) {
			AccountingTransaction parentTransaction = transactionSupplier.get();
			parentTransaction.setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_Position());
			parentTransaction.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
			eventDetail.getAccountingTransaction().setParentTransaction(parentTransaction);
			AccountingJournalType transferType = new AccountingJournalType();
			transferType.setName(AccountingJournalType.TRANSFER_JOURNAL);
			eventDetail.getAccountingTransaction().getJournal().setJournalType(transferType);
			eventDetail.getAccountingTransaction().setFkFieldId(33);
		}

		return eventDetail;
	}


	private List<AccountingEventJournalDetail> buildPositionCollateralPayableReceivable(AccountingEventJournalDetail parentDetail, InvestmentSecurity investmentSecurity) {
		List<AccountingEventJournalDetail> results = new ArrayList<>();

		AccountingEventJournalDetail accountingDetail = buildPosition(investmentSecurity, true);
		accountingDetail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionReceivable());
		accountingDetail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		accountingDetail.getAccountingTransaction().setParentTransaction(parentDetail.getAccountingTransaction());
		results.add(accountingDetail);

		accountingDetail = buildPosition(investmentSecurity, true);
		accountingDetail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionPayable());
		accountingDetail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
		accountingDetail.getAccountingTransaction().setParentTransaction(parentDetail.getAccountingTransaction());
		accountingDetail.getAccountingTransaction().setQuantity(accountingDetail.getAccountingTransaction().getQuantity().negate());
		accountingDetail.getAccountingTransaction().setLocalDebitCredit(accountingDetail.getAccountingTransaction().getLocalCredit().negate());
		accountingDetail.getAccountingTransaction().setBaseDebitCredit(accountingDetail.getAccountingTransaction().getBaseCredit().negate());
		accountingDetail.getAccountingTransaction().setPositionCostBasis(accountingDetail.getAccountingTransaction().getPositionCostBasis().negate());
		results.add(accountingDetail);

		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreditEventPennyRounding_6L60S() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingPennyRoundingCreditEventBookingRule_6L60S(eventJournal, false);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRule(factorChangeRule)
				.withExpectedResults(
						"-10	445	100000	H-200	Position	6L60S	100	-356,244.7	-172,600.56	01/14/2016	1.087025	-187,621.12	-172,600.56	03/27/2015	01/21/2016	1 to 0.87276975 Credit Event for 6L60S on 01/21/2016",
						"-11	-10	100000	H-200	Realized Gain / Loss	6L60S	100		-183,644.14	01/14/2016	1.087025	-199,625.78	0	03/27/2015	01/21/2016	Realized Gain / Loss from Credit Event for 6L60S on 01/21/2016",
						"-12	445	100000	H-200	Payment Receivable	EUR			356,244.7	01/14/2016	1.087025	387,246.9	0	03/27/2015	01/21/2016	Accrual of Credit Event for 6L60S to be paid on 01/21/2016",
						"-13	445	100000	H-200	Premium Leg	6L60S			-494.79	01/14/2016	1.087025	-537.85	0	03/27/2015	01/21/2016	Premium Leg from Credit Event for 6L60S on 01/21/2016",
						"-14	445	100000	H-200	Payment Receivable	EUR			494.79	01/14/2016	1.087025	537.85	0	03/27/2015	01/21/2016	Accrual of Credit Event for 6L60S to be paid on 01/21/2016"
				)
				.execute();
	}


	@Test
	public void testCreditEventPennyRoundingCollateral_6L60S() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingPennyRoundingCreditEventBookingRule_6L60S(eventJournal, true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRule(factorChangeRule)
				.withExpectedResults(
						"-10	445	100000	H-201	Position Collateral	6L60S	100	-356,244.7	-172,600.56	01/14/2016	1.087025	-187,621.12	-172,600.56	03/27/2015	01/21/2016	1 to 0.87276975 Credit Event for 6L60S on 01/21/2016",
						"-11	447	100000	H-200	Position Collateral Receivable	6L60S	100	-356,244.7	-172,600.56	01/14/2016	1.087025	-187,621.12	-172,600.56	03/27/2015	01/21/2016	1 to 0.87276975 Credit Event for 6L60S on 01/21/2016",
						"-12	448	100000	H-201	Position Collateral Payable	6L60S	100	356,244.7	172,600.56	01/14/2016	1.087025	187,621.12	172,600.56	03/27/2015	01/21/2016	1 to 0.87276975 Credit Event for 6L60S on 01/21/2016",
						"-13	-10	100000	H-201	Realized Gain / Loss	6L60S	100		-183,644.14	01/14/2016	1.087025	-199,625.78	0	03/27/2015	01/21/2016	Realized Gain / Loss from Credit Event for 6L60S on 01/21/2016",
						"-14	445	100000	H-201	Payment Receivable	EUR			356,244.7	01/14/2016	1.087025	387,246.9	0	03/27/2015	01/21/2016	Accrual of Credit Event for 6L60S to be paid on 01/21/2016",
						"-15	445	100000	H-201	Premium Leg	6L60S			-494.79	01/14/2016	1.087025	-537.85	0	03/27/2015	01/21/2016	Premium Leg from Credit Event for 6L60S on 01/21/2016",
						"-16	445	100000	H-201	Payment Receivable	EUR			494.79	01/14/2016	1.087025	537.85	0	03/27/2015	01/21/2016	Accrual of Credit Event for 6L60S to be paid on 01/21/2016"
				)
				.execute();
	}


	@Test
	public void testCreditEventPennyRounding_TX2201D19E0500XXI() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(eventJournal, false);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRule(factorChangeRule)
				.withExpectedResults(
						"-10	334	100000	H-200	Position	TX2201D19E0500XXI	100	129,493.33	-10,818.36	01/14/2016	1.08665	-11,755.77	-10,818.36	08/05/2015	01/21/2016	1 to 0.9866666667 Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-11	-10	100000	H-200	Realized Gain / Loss	TX2201D19E0500XXI	100		140,311.69	01/14/2016	1.08665	152,469.7	0	08/05/2015	01/21/2016	Realized Gain / Loss from Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-12	334	100000	H-200	Payment Receivable	EUR			-123,504.27	01/14/2016	1.08665	-134,205.91	0	08/05/2015	01/21/2016	Accrual of Credit Event for TX2201D19E0500XXI to be paid on 01/21/2016",
						"-13	-10	100000	H-200	Principal Losses	TX2201D19E0500XXI	100		-5,989.06	01/14/2016	1.08665	-6,508.02	0	08/05/2015	01/21/2016	Principal Losses from Credit Event for TX2201D19E0500XXI",
						"-14	334	100000	H-200	Premium Leg	TX2201D19E0500XXI			179.85	01/14/2016	1.08665	195.43	0	08/05/2015	01/21/2016	Premium Leg from Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-15	334	100000	H-200	Payment Receivable	EUR			-179.85	01/14/2016	1.08665	-195.43	0	08/05/2015	01/21/2016	Accrual of Credit Event for TX2201D19E0500XXI to be paid on 01/21/2016"
				)
				.execute();
	}


	@Test
	public void testCreditEventPennyRoundingDoNotAccrue_TX2201D19E0500XXI() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(eventJournal, false);
		eventJournal.getSecurityEvent().setExDate(DateUtils.toDate("01/21/2016"));
		eventJournal.getSecurityEvent().setEventDate(DateUtils.toDate("01/15/2016"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRule(factorChangeRule)
				.withExpectedResults(
						"-10	334	100000	H-200	Position	TX2201D19E0500XXI	100	129,493.33	-10,818.36	01/15/2016	1.08665	-11,755.77	-10,818.36	08/05/2015	01/15/2016	1 to 0.9866666667 Credit Event for TX2201D19E0500XXI on 01/15/2016",
						"-11	-10	100000	H-200	Realized Gain / Loss	TX2201D19E0500XXI	100		140,311.69	01/15/2016	1.08665	152,469.7	0	08/05/2015	01/15/2016	Realized Gain / Loss from Credit Event for TX2201D19E0500XXI on 01/15/2016",
						"-12	-10	100000	H-200	Currency	EUR			10,818.36	01/15/2016	1.08665	11,755.77	0	08/05/2015	01/15/2016	Currency proceeds from Credit Event of TX2201D19E0500XXI",
						"-13	-10	100000	H-200	Currency	EUR			-140,311.69	01/15/2016	1.08665	-152,469.7	0	08/05/2015	01/15/2016	Currency expense from Credit Event of TX2201D19E0500XXI",
						"-14	-10	100000	H-200	Principal Losses	TX2201D19E0500XXI	100		-5,989.06	01/15/2016	1.08665	-6,508.01	0	08/05/2015	01/15/2016	Principal Losses from Credit Event for TX2201D19E0500XXI",
						"-15	-10	100000	H-200	Currency	EUR			5,989.06	01/15/2016	1.08665	6,508.01	0	08/05/2015	01/15/2016	Currency proceeds from Credit Event of TX2201D19E0500XXI",
						"-16	334	100000	H-200	Premium Leg	TX2201D19E0500XXI			179.85	01/15/2016	1.08665	195.43	0	08/05/2015	01/15/2016	Premium Leg from Credit Event for TX2201D19E0500XXI on 01/15/2016",
						"-17	334	100000	H-200	Currency	EUR			-179.85	01/15/2016	1.08665	-195.43	0	08/05/2015	01/15/2016	Currency expense from Credit Event of TX2201D19E0500XXI"
				)
				.execute();
	}


	@Test
	public void testCreditEventPennyRoundingCollateral_TX2201D19E0500XXI() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(eventJournal, true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRule(factorChangeRule)
				.withExpectedResults(
						"-10	334	100000	H-201	Position Collateral	TX2201D19E0500XXI	100	129,493.33	-10,818.36	01/14/2016	1.08665	-11,755.77	-10,818.36	08/05/2015	01/21/2016	1 to 0.9866666667 Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-11	336	100000	H-200	Position Collateral Receivable	TX2201D19E0500XXI	100	129,493.33	-10,818.36	01/14/2016	1.08665	-11,755.77	-10,818.36	08/05/2015	01/21/2016	1 to 0.9866666667 Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-12	337	100000	H-201	Position Collateral Payable	TX2201D19E0500XXI	100	-129,493.33	10,818.36	01/14/2016	1.08665	11,755.77	10,818.36	08/05/2015	01/21/2016	1 to 0.9866666667 Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-13	-10	100000	H-201	Realized Gain / Loss	TX2201D19E0500XXI	100		140,311.69	01/14/2016	1.08665	152,469.7	0	08/05/2015	01/21/2016	Realized Gain / Loss from Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-14	334	100000	H-201	Payment Receivable	EUR			-123,504.27	01/14/2016	1.08665	-134,205.91	0	08/05/2015	01/21/2016	Accrual of Credit Event for TX2201D19E0500XXI to be paid on 01/21/2016",
						"-15	-10	100000	H-201	Principal Losses	TX2201D19E0500XXI	100		-5,989.06	01/14/2016	1.08665	-6,508.02	0	08/05/2015	01/21/2016	Principal Losses from Credit Event for TX2201D19E0500XXI",
						"-16	334	100000	H-201	Premium Leg	TX2201D19E0500XXI			179.85	01/14/2016	1.08665	195.43	0	08/05/2015	01/21/2016	Premium Leg from Credit Event for TX2201D19E0500XXI on 01/21/2016",
						"-17	334	100000	H-201	Payment Receivable	EUR			-179.85	01/14/2016	1.08665	-195.43	0	08/05/2015	01/21/2016	Accrual of Credit Event for TX2201D19E0500XXI to be paid on 01/21/2016"
				)
				.execute();
	}


	@Test
	public void testCreditEventAccrual() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingCreditEventBookingRule(eventJournal, false);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						factorChangeRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateReceivableBookingRule()
				)
				.withExpectedResults(
						"-10	112	100000	H-200	Position	60901287	100	148,000	-11,840	06/23/2015	1	-11,840	-11,840	05/10/2013	06/26/2015	0.97 to 0.96 Credit Event for 60901287 on 06/26/2015",
						"-11	-10	100000	H-200	Realized Gain / Loss	60901287	100		159,840	06/23/2015	1	159,840	0	05/10/2013	06/26/2015	Realized Gain / Loss from Credit Event for 60901287 on 06/26/2015",
						"-13	-10	100000	H-200	Principal Losses	60901287	100		-23,495	06/23/2015	1	-23,495	0	05/10/2013	06/26/2015	Principal Losses from Credit Event for 60901287",
						"-14	112	100000	H-200	Premium Leg	60901287			493.33	06/23/2015	1	493.33	0	05/10/2013	06/26/2015	Premium Leg from Credit Event for 60901287 on 06/26/2015",
						"-15	112	100000	H-200	Payment Receivable	USD			-124,998.33	06/23/2015	1	-124,998.33	0	05/10/2013	06/26/2015	Accrual of Credit Event for 60901287 to be paid on 06/26/2015"
				)
				.execute();
	}


	@Test
	public void testCreditEventAccrualCollateral() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingCreditEventBookingRule(eventJournal, true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						factorChangeRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateReceivableBookingRule()
				)
				.withExpectedResults(
						"-10	112	100000	H-201	Position Collateral	60901287	100	148,000	-11,840	06/23/2015	1	-11,840	-11,840	05/10/2013	06/26/2015	0.97 to 0.96 Credit Event for 60901287 on 06/26/2015",
						"-11	114	100000	H-200	Position Collateral Receivable	60901287	100	148,000	-11,840	06/23/2015	1	-11,840	-11,840	05/10/2013	06/26/2015	0.97 to 0.96 Credit Event for 60901287 on 06/26/2015",
						"-12	115	100000	H-201	Position Collateral Payable	60901287	100	-148,000	11,840	06/23/2015	1	11,840	11,840	05/10/2013	06/26/2015	0.97 to 0.96 Credit Event for 60901287 on 06/26/2015",
						"-13	-10	100000	H-201	Realized Gain / Loss	60901287	100		159,840	06/23/2015	1	159,840	0	05/10/2013	06/26/2015	Realized Gain / Loss from Credit Event for 60901287 on 06/26/2015",
						"-15	-10	100000	H-201	Principal Losses	60901287	100		-23,495	06/23/2015	1	-23,495	0	05/10/2013	06/26/2015	Principal Losses from Credit Event for 60901287",
						"-16	112	100000	H-201	Premium Leg	60901287			493.33	06/23/2015	1	493.33	0	05/10/2013	06/26/2015	Premium Leg from Credit Event for 60901287 on 06/26/2015",
						"-17	112	100000	H-201	Payment Receivable	USD			-124,998.33	06/23/2015	1	-124,998.33	0	05/10/2013	06/26/2015	Accrual of Credit Event for 60901287 to be paid on 06/26/2015"
				)
				.execute();
	}


	@Test
	public void testCreditEventReversal() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingCreditEventBookingRule(eventJournal, false);
		factorChangeRule.setAccrualReversal(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						factorChangeRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateReceivableBookingRule()
				)
				.withExpectedResults(
						"-13	112	100000	H-200	Cash	USD			-124,998.33	06/26/2015	1	-124,998.33	0	05/10/2013	06/26/2015	Cash expense from Credit Event of 60901287",
						"-14	112	100000	H-200	Payment Receivable	USD			124,998.33	06/26/2015	1	124,998.33	0	05/10/2013	06/26/2015	Reverse accrual of Credit Event for 60901287 from 06/23/2015"
				)
				.execute();
	}


	@Test
	public void testCreditEventReversalCollateral() {
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		AccountingFactorChangeBookingRule factorChangeRule = configureAccountingCreditEventBookingRule(eventJournal, true);
		factorChangeRule.setAccrualReversal(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						factorChangeRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateReceivableBookingRule()
				)
				.withExpectedResults(
						"-13	112	100000	H-201	Cash	USD			-124,998.33	06/26/2015	1	-124,998.33	0	05/10/2013	06/26/2015	Cash expense from Credit Event of 60901287",
						"-14	112	100000	H-201	Payment Receivable	USD			124,998.33	06/26/2015	1	124,998.33	0	05/10/2013	06/26/2015	Reverse accrual of Credit Event for 60901287 from 06/23/2015"
				)
				.execute();
	}


	private AccountingFactorChangeBookingRule configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(AccountingEventJournal eventJournal, boolean collateral) {
		// setup event journal with one detail
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.CREDIT_EVENT));
		securityEvent.setBeforeEventValue(new BigDecimal("1"));
		securityEvent.setAfterEventValue(new BigDecimal("0.9866666667"));
		securityEvent.setAdditionalEventValue(new BigDecimal("4.625"));
		securityEvent.setAccrualStartDate(DateUtils.toDate("09/20/2015"));
		securityEvent.setAccrualEndDate(DateUtils.toDate("12/20/2015"));
		securityEvent.setAdditionalDate(DateUtils.toDate("12/10/2015")); // default date
		securityEvent.setExDate(DateUtils.toDate("01/15/2016"));
		securityEvent.setEventDate(DateUtils.toDate("01/21/2016"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(26709, "TX2201D19E0500XXI", InvestmentType.SWAPS, "EUR"));
		return configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(eventJournal, securityEvent, collateral);
	}


	private AccountingFactorChangeBookingRule configureAccountingPennyRoundingCreditEventBookingRule_TX2201D19E0500XXI(AccountingEventJournal eventJournal, InvestmentSecurityEvent securityEvent, final boolean collateral) {
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalType journalType = new AccountingEventJournalType();
		journalType.setEventType(securityEvent.getType());
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
		journalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
		journalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		journalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		eventJournal.setJournalType(journalType);

		AtomicLong atomicLong = new AtomicLong(333);
		Supplier<AccountingTransaction> transactionSupplier = () -> AccountingTransactionBuilder.newTransactionForIdAndSecurity(atomicLong.incrementAndGet(), securityEvent.getSecurity())
				.qty(-9712000)
				.price("108.35437751")
				.exchangeRateToBase("1.08665")
				.on("08/05/2015").build();

		Supplier<AccountingEventJournalDetail> detailSupplier = () -> {
			AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
			eventDetail.setTransactionPrice(new BigDecimal("100"));
			eventDetail.setAffectedQuantity(new BigDecimal("-9712000"));
			eventDetail.setAffectedCost(new BigDecimal("10818.36"));
			eventDetail.setTransactionAmount(new BigDecimal("-123504.27"));
			eventDetail.setAdditionalAmount(new BigDecimal("-5989.06"));
			eventDetail.setAdditionalAmount2(new BigDecimal("-179.85"));
			eventDetail.setExchangeRateToBase(new BigDecimal("1.08665"));
			eventDetail.setAccountingTransaction(transactionSupplier.get());
			return eventDetail;
		};

		List<AccountingEventJournalDetail> eventJournalDetailList = new ArrayList<>();

		if (collateral) {
			AccountingAccount positionAccount = AccountingTestObjectFactory.newAccountingAccount_CollateralPosition();
			InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2();

			AccountingEventJournalDetail detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(positionAccount);
			detail.getAccountingTransaction().setHoldingInvestmentAccount(holdingAccount);
			detail.getAccountingTransaction().setParentTransaction(transactionSupplier.get());
			eventJournalDetailList.add(detail);
			AccountingTransaction parentTransaction = detail.getAccountingTransaction();

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionReceivable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			eventJournalDetailList.add(detail);

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionPayable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(holdingAccount);
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			detail.getAccountingTransaction().setQuantity(detail.getAccountingTransaction().getQuantity().negate());
			detail.getAccountingTransaction().setLocalDebitCredit(detail.getAccountingTransaction().getLocalDebitCredit().negate());
			detail.getAccountingTransaction().setBaseDebitCredit(detail.getAccountingTransaction().getBaseDebitCredit().negate());
			detail.getAccountingTransaction().setPositionCostBasis(detail.getAccountingTransaction().getPositionCostBasis().negate());
			eventJournalDetailList.add(detail);
		}
		else {
			eventJournalDetailList.add(detailSupplier.get());
		}
		AccountingJournalType transferType = new AccountingJournalType();
		transferType.setName(AccountingJournalType.TRANSFER_JOURNAL);
		eventJournalDetailList.get(0).getAccountingTransaction().getJournal().setJournalType(transferType);
		eventJournalDetailList.get(0).getAccountingTransaction().setFkFieldId(33);
		for (AccountingEventJournalDetail detail : eventJournalDetailList) {
			if (!detail.getAccountingTransaction().getAccountingAccount().isReceivable()) {
				eventJournal.addDetail(detail);
			}
		}
		AccountingTransactionService accountingTransactionService = Mockito.mock(AccountingTransactionService.class);
		Mockito.when(accountingTransactionService.getAccountingTransaction(ArgumentMatchers.anyLong())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return eventJournalDetailList.stream()
					.map(AccountingEventJournalDetail::getAccountingTransaction)
					.filter(t -> t.getId().equals(id))
					.findFirst()
					.orElse(null);
		});
		return configureAccountingCreditEventBookingRule(securityEvent, eventJournalDetailList, accountingAccountService, accountingTransactionService);
	}


	private AccountingFactorChangeBookingRule configureAccountingPennyRoundingCreditEventBookingRule_6L60S(AccountingEventJournal eventJournal, boolean collateral) {
		// setup event journal with one detail
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.CREDIT_EVENT));
		securityEvent.setBeforeEventValue(new BigDecimal("1"));
		securityEvent.setAfterEventValue(new BigDecimal("0.87276975"));
		securityEvent.setAdditionalEventValue(new BigDecimal("4.625"));
		securityEvent.setAccrualStartDate(DateUtils.toDate("09/20/2015"));
		securityEvent.setAccrualEndDate(DateUtils.toDate("12/20/2015"));
		securityEvent.setAdditionalDate(DateUtils.toDate("12/10/2015")); // default date
		securityEvent.setExDate(DateUtils.toDate("01/15/2016"));
		securityEvent.setEventDate(DateUtils.toDate("01/21/2016"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(30360, "6L60S", InvestmentType.SWAPS, "EUR"));
		return configureAccountingPennyRoundingCreditEventBookingRule_6L60S(eventJournal, securityEvent, collateral);
	}


	private AccountingFactorChangeBookingRule configureAccountingPennyRoundingCreditEventBookingRule_6L60S(AccountingEventJournal eventJournal, InvestmentSecurityEvent securityEvent, final boolean collateral) {
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalType journalType = new AccountingEventJournalType();
		journalType.setEventType(securityEvent.getType());
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
		journalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
		journalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		journalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		eventJournal.setJournalType(journalType);

		AtomicLong atomicLong = new AtomicLong(444);
		Supplier<AccountingTransaction> transactionSupplier = () -> AccountingTransactionBuilder.newTransactionForIdAndSecurity(atomicLong.incrementAndGet(), securityEvent.getSecurity())
				.qty(2800000)
				.price("51.55")
				.exchangeRateToBase("1.087025")
				.on("03/27/2015")
				.build();

		Supplier<AccountingEventJournalDetail> detailSupplier = () -> {
			AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
			eventDetail.setTransactionPrice(new BigDecimal("100"));
			eventDetail.setAffectedQuantity(new BigDecimal("2800000"));
			eventDetail.setAffectedCost(new BigDecimal("172600.56"));
			eventDetail.setTransactionAmount(new BigDecimal("356244.70"));
			eventDetail.setAdditionalAmount2(new BigDecimal("494.79"));
			eventDetail.setExchangeRateToBase(new BigDecimal("1.087025"));
			eventDetail.setAccountingTransaction(transactionSupplier.get());
			return eventDetail;
		};

		List<AccountingEventJournalDetail> eventJournalDetailList = new ArrayList<>();

		if (collateral) {
			AccountingAccount positionAccount = AccountingTestObjectFactory.newAccountingAccount_CollateralPosition();
			InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2();

			AccountingEventJournalDetail detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(positionAccount);
			detail.getAccountingTransaction().setHoldingInvestmentAccount(holdingAccount);
			detail.getAccountingTransaction().setParentTransaction(transactionSupplier.get());
			eventJournalDetailList.add(detail);
			AccountingTransaction parentTransaction = detail.getAccountingTransaction();

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionReceivable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			eventJournalDetailList.add(detail);

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionPayable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(holdingAccount);
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			detail.getAccountingTransaction().setQuantity(detail.getAccountingTransaction().getQuantity().negate());
			detail.getAccountingTransaction().setLocalDebitCredit(detail.getAccountingTransaction().getLocalDebitCredit().negate());
			detail.getAccountingTransaction().setBaseDebitCredit(detail.getAccountingTransaction().getBaseDebitCredit().negate());
			detail.getAccountingTransaction().setPositionCostBasis(detail.getAccountingTransaction().getPositionCostBasis().negate());
			eventJournalDetailList.add(detail);
		}
		else {
			eventJournalDetailList.add(detailSupplier.get());
		}
		AccountingJournalType transferType = new AccountingJournalType();
		transferType.setName(AccountingJournalType.TRANSFER_JOURNAL);
		eventJournalDetailList.get(0).getAccountingTransaction().getJournal().setJournalType(transferType);
		eventJournalDetailList.get(0).getAccountingTransaction().setFkFieldId(33);
		for (AccountingEventJournalDetail detail : eventJournalDetailList) {
			if (!detail.getAccountingTransaction().getAccountingAccount().isReceivable()) {
				eventJournal.addDetail(detail);
			}
		}
		AccountingTransactionService accountingTransactionService = Mockito.mock(AccountingTransactionService.class);
		Mockito.when(accountingTransactionService.getAccountingTransaction(ArgumentMatchers.anyLong())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return eventJournalDetailList.stream()
					.map(AccountingEventJournalDetail::getAccountingTransaction)
					.filter(t -> t.getId().equals(id))
					.findFirst()
					.orElse(null);
		});
		return configureAccountingCreditEventBookingRule(securityEvent, eventJournalDetailList, accountingAccountService, accountingTransactionService);
	}


	private AccountingFactorChangeBookingRule configureAccountingCreditEventBookingRule(AccountingEventJournal eventJournal, boolean collateral) {
		// setup event journal with one detail
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.CREDIT_EVENT));
		securityEvent.setBeforeEventValue(new BigDecimal("0.97"));
		securityEvent.setAfterEventValue(new BigDecimal("0.96"));
		securityEvent.setAdditionalEventValue(new BigDecimal("15.875"));
		securityEvent.setAccrualStartDate(DateUtils.toDate("03/19/2015"));
		securityEvent.setAccrualEndDate(DateUtils.toDate("06/21/2015"));
		securityEvent.setAdditionalDate(DateUtils.toDate("05/28/2015")); // default date
		securityEvent.setExDate(DateUtils.toDate("06/24/2015"));
		securityEvent.setEventDate(DateUtils.toDate("06/26/2015"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(21481, "60901287", InvestmentType.SWAPS));
		return configureAccountingCreditEventBookingRule(eventJournal, securityEvent, collateral);
	}


	private AccountingFactorChangeBookingRule configureAccountingCreditEventBookingRule(AccountingEventJournal eventJournal, InvestmentSecurityEvent securityEvent, final boolean collateral) {
		eventJournal.setSecurityEvent(securityEvent);

		AccountingEventJournalType journalType = new AccountingEventJournalType();
		journalType.setEventType(securityEvent.getType());
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
		journalType.setDebitAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE));
		journalType.setCreditAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_REALIZED));
		journalType.setAccrualReversalAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		eventJournal.setJournalType(journalType);

		AtomicLong transactionId = new AtomicLong(111);
		Supplier<AccountingTransaction> transactionSupplier = () -> AccountingTransactionBuilder.newTransactionForIdAndSecurity(transactionId.incrementAndGet(), securityEvent.getSecurity())
				.qty(-14800000)
				.price("108")
				.on("05/10/2013")
				.build();

		Supplier<AccountingEventJournalDetail> detailSupplier = () -> {
			AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
			eventDetail.setTransactionPrice(new BigDecimal("100"));
			eventDetail.setAffectedQuantity(new BigDecimal("-14356000"));
			eventDetail.setAffectedCost(new BigDecimal("11840.00"));
			eventDetail.setTransactionAmount(new BigDecimal("-124505.00"));
			eventDetail.setAdditionalAmount(new BigDecimal("-23495"));
			eventDetail.setAdditionalAmount2(new BigDecimal("-493.33"));
			eventDetail.setExchangeRateToBase(BigDecimal.ONE);
			eventDetail.setAccountingTransaction(transactionSupplier.get());
			return eventDetail;
		};

		List<AccountingEventJournalDetail> eventJournalDetailList = new ArrayList<>();

		if (collateral) {
			AccountingEventJournalDetail detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPosition());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
			detail.getAccountingTransaction().setParentTransaction(transactionSupplier.get());
			detail.getAccountingTransaction().setFkFieldId(5);
			AccountingJournalType transferType = new AccountingJournalType();
			transferType.setName(AccountingJournalType.TRANSFER_JOURNAL);
			detail.getAccountingTransaction().getJournal().setJournalType(transferType);
			eventJournalDetailList.add(detail);
			AccountingTransaction parentTransaction = detail.getAccountingTransaction();

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionReceivable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			eventJournalDetailList.add(detail);

			detail = detailSupplier.get();
			detail.getAccountingTransaction().setAccountingAccount(AccountingTestObjectFactory.newAccountingAccount_CollateralPositionPayable());
			detail.getAccountingTransaction().setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault2());
			detail.getAccountingTransaction().setParentTransaction(parentTransaction);
			detail.getAccountingTransaction().setQuantity(detail.getAccountingTransaction().getQuantity().negate());
			detail.getAccountingTransaction().setLocalDebitCredit(detail.getAccountingTransaction().getLocalDebitCredit().negate());
			detail.getAccountingTransaction().setBaseDebitCredit(detail.getAccountingTransaction().getBaseDebitCredit().negate());
			detail.getAccountingTransaction().setPositionCostBasis(detail.getAccountingTransaction().getPositionCostBasis().negate());
			eventJournalDetailList.add(detail);
		}
		else {
			eventJournalDetailList.add(detailSupplier.get());
		}
		for (AccountingEventJournalDetail detail : eventJournalDetailList) {
			if (!detail.getAccountingTransaction().getAccountingAccount().isReceivable()) {
				eventJournal.addDetail(detail);
			}
		}
		AccountingTransactionService accountingTransactionService = Mockito.mock(AccountingTransactionService.class);
		Mockito.when(accountingTransactionService.getAccountingTransaction(ArgumentMatchers.anyLong())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return eventJournalDetailList.stream()
					.map(AccountingEventJournalDetail::getAccountingTransaction)
					.filter(t -> t.getId().equals(id))
					.findFirst()
					.orElse(null);
		});
		return configureAccountingCreditEventBookingRule(securityEvent, eventJournalDetailList, accountingAccountService, accountingTransactionService);
	}


	private AccountingFactorChangeBookingRule configureAccountingCreditEventBookingRule(InvestmentSecurityEvent securityEvent, List<AccountingEventJournalDetail> eventDetailList, AccountingAccountService accountingAccountService, AccountingTransactionService accountingTransactionService) {
		AccountingFactorChangeBookingRule factorChangeRule = new AccountingFactorChangeBookingRule();
		factorChangeRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		factorChangeRule.setAccountingTransactionService(accountingTransactionService);
		factorChangeRule.setAccountingPeriodService(Mockito.mock(AccountingPeriodService.class));
		factorChangeRule.setAccountingPositionTransferService(Mockito.mock(AccountingPositionTransferService.class));

		List<AccountingPosition> existingPositions = eventDetailList.stream()
				.map(AccountingEventJournalDetail::getAccountingTransaction)
				.map(t -> {
							AccountingPosition position = AccountingTestObjectFactory.newAccountingTransaction(
									t.getId(),
									t.getInvestmentSecurity(),
									t.getAccountingAccount(),
									t.getQuantity(),
									t.getPrice(),
									t.getPositionCostBasis(),
									t.getExchangeRateToBase(),
									t.getTransactionDate());
							position.getOpeningTransaction().setParentTransaction(t.getParentTransaction());
							position.getOpeningTransaction().setHoldingInvestmentAccount(t.getHoldingInvestmentAccount());
							return position;
						}
				)
				.collect(Collectors.toList());

		Mockito.when(
				factorChangeRule.getAccountingPositionService().getAccountingPositionList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.eq(securityEvent.getSecurity().getId()),
						ArgumentMatchers.any(Date.class))
		).thenReturn(existingPositions.stream().filter(d -> !d.getAccountingAccount().isReceivable()).collect(Collectors.toList()));
		Mockito.when(
				factorChangeRule.getAccountingPositionService().getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))
		).thenReturn(existingPositions.stream().filter(d -> d.getAccountingAccount().isReceivable()).collect(Collectors.toList()));
		factorChangeRule.setAccountingAccountService(accountingAccountService);
		factorChangeRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(factorChangeRule.getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail")).thenReturn(new SystemTable());
		factorChangeRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		Mockito.when(factorChangeRule.getAccountingPositionTransferService().getAccountingPositionTransferDetail(ArgumentMatchers.anyInt()))
				.thenReturn(AccountingPositionTransferDetailBuilder.createEmpty()
						.withPositionTransfer(AccountingPositionTransferBuilder.createEmpty()
								.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
								.toAccountPositionTransfer()
						)
						.toAccountingPositionTransferDetail()
				);

		return factorChangeRule;
	}
}
