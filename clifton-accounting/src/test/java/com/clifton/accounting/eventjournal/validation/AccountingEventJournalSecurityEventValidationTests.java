package com.clifton.accounting.eventjournal.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author NickK
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class AccountingEventJournalSecurityEventValidationTests {

	private static final int SCRIP_DIVIDEND_SECURITY_EVENT_ID = 3000;
	private static final int SCRIP_DIVIDEND_BOOKED_JOURNAL_PAYOUT_ID = 2;
	private static final int SCRIP_DIVIDEND_BOOKED_JOURNAL_ELECTION_ID = 1;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;


	///////////////////////////////////////////////////////////////////////////
	/////     Account Event Journal Security Event Observer Tests        //////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingEventJournalSecurityEvent_eventUpdatePaymentDateForBookedJournalFails() {
		InvestmentSecurityEvent scrip = this.investmentSecurityEventService.getInvestmentSecurityEvent(SCRIP_DIVIDEND_SECURITY_EVENT_ID);
		scrip.setPaymentDate(DateUtils.toDate("07/31/2019"));

		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);
		}, "Cannot update [paymentDate, eventDate] fields for security event that is used by booked event journal: 0.0357142857 to 0.0357142857 Scrip Dividend for REPYY on 07/31/2019");
	}


	@Test
	public void testAccountingEventJournalSecurityEvent_eventUpdateStatusApprovedForBookedJournalSucceeds() {
		InvestmentSecurityEvent scrip = this.investmentSecurityEventService.getInvestmentSecurityEvent(SCRIP_DIVIDEND_SECURITY_EVENT_ID);
		InvestmentSecurityEventStatus approvedStatus = this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved");
		Assertions.assertEquals(approvedStatus, scrip.getStatus());
		InvestmentSecurityEventStatus conditionallyApprovedStatus = this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Conditionally Approved");
		scrip.setStatus(conditionallyApprovedStatus);
		InvestmentSecurityEvent savedEvent = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);
		Assertions.assertEquals(conditionallyApprovedStatus, savedEvent.getStatus());
		// revert status
		savedEvent.setStatus(approvedStatus);
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(savedEvent);
	}


	///////////////////////////////////////////////////////////////////////////
	///     Account Event Journal Security Event Payout Observer Tests     ////
	///////////////////////////////////////////////////////////////////////////
	@Test
	public void testAccountingEventJournalSecurityEventPayout_payoutUpdateForBookedJournalFails() {
		InvestmentSecurityEventPayout bookedPayout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(SCRIP_DIVIDEND_BOOKED_JOURNAL_PAYOUT_ID);
		bookedPayout.setElectionNumber((short) 5);
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(bookedPayout);
		}, "The Security Event Payout cannot be updated because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	///////////////////////////////////////////////////////////////////////////
	///     Account Event Journal Security Event Election Observer Tests     //
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingEventJournalSecurityEventClientElection_electionUpdateForBookedJournalFails() {
		InvestmentSecurityEventClientElection bookedElection = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventClientElection(SCRIP_DIVIDEND_BOOKED_JOURNAL_ELECTION_ID);
		bookedElection.setElectionQuantity(BigDecimal.ONE);
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(bookedElection);
		}, "Cannot update [electionQuantity] fields for Security Event Client Election that is used by booked event journal: 0.0357142857 to 0.0357142857 Scrip Dividend for REPYY on 06/29/2019");
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
}
