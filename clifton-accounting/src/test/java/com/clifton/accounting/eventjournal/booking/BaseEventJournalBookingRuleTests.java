package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPriceAfterEventValuePopulator;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;


/**
 * The <code>BaseEventJournalBookingRuleTests</code> class sets up re-usable test data for event journal tests.
 *
 * @author vgomelsky
 */
public abstract class BaseEventJournalBookingRuleTests {

	protected AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected <T extends BaseEventJournalBookingRule> T newEventJournalBookingRule(Class<T> clazz) {
		T bookingRule = BeanUtils.newInstance(clazz);
		bookingRule.setAccountingAccountService(this.accountingAccountService);
		bookingRule.setAccountingPeriodService(Mockito.mock(AccountingPeriodService.class));
		Mockito.when(bookingRule.getAccountingPeriodService().isAccountingPeriodClosed(ArgumentMatchers.anyInt(), ArgumentMatchers.any())).thenReturn(false);
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		bookingRule.setMarketDataExchangeRatesApiService(Mockito.mock(MarketDataExchangeRatesApiService.class));
		bookingRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(bookingRule.getSystemSchemaService().getSystemTableByName("AccountingEventJournalDetail")).thenReturn(new SystemTable());

		AccountingTestObjectFactory.newAccountingBookingPositionRetriever(bookingRule);

		return bookingRule;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected AccountingEventJournal newDividendPaymentJournal() {
		// apply the rule: $3 per share dividend payment on 100 shares of IBM at $50 per share
		InvestmentSecurityEvent securityEvent = newDividendPaymentEvent();
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEvent(securityEvent), new AccountingEventJournalDetailPriceAfterEventValuePopulator(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, securityEvent.getSecurity()).qty(100).price("50").on("6/1/2010").build());
	}


	protected InvestmentSecurityEvent newDividendPaymentEvent() {
		InvestmentSecurityEvent securityEvent = InvestmentTestObjectFactory.newInvestmentSecurityEvent(
				InvestmentTestObjectFactory.newInvestmentSecurity(876, "IBM", InvestmentType.STOCKS),
				InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT),
				"9/20/2010");
		securityEvent.setBeforeAndAfterEventValue(new BigDecimal(3));
		securityEvent.setEventDate(DateUtils.toDate("1/1/2011"));
		securityEvent.setExDate(DateUtils.toDate("9/20/2010"));
		securityEvent.setRecordDate(DateUtils.toDate("9/21/2010"));
		return securityEvent;
	}


	protected AccountingEventJournal newDividendPaymentJournal_PayoutGBP() {
		InvestmentSecurityEventPayout gbpPayout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(newDividendPaymentEvent(), InvestmentSecurityEventPayoutType.CURRENCY, InvestmentSecurityBuilder.newGBP());
		gbpPayout.setAdditionalPayoutDate(DateUtils.toDate("1/3/2011"));

		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayout(gbpPayout), new AccountingEventJournalDetailPriceAfterEventValuePopulator(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, gbpPayout.getSecurityEvent().getSecurity()).qty(100).price("50").on("6/1/2010").build());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.3"));

		return eventJournal;
	}
}
