package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class AccountingPaymentAccrualEventBookingRuleTests extends BaseEventJournalBookingRuleTests {


	@Test
	public void testCashDividendPayment_Domestic() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	USD			300	09/20/2010	1	300	0	09/20/2010	01/01/2011	Accrual of Cash Dividend Payment for IBM to be paid on 01/01/2011",
						"-11	12345	100000	H-200	Dividend Income	IBM	3	100	-300	09/20/2010	1	-300	0	09/20/2010	01/01/2011	3 Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_ForeignPayout() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal_PayoutGBP())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	GBP			300	09/20/2010	1.3	390	0	09/20/2010	01/03/2011	Accrual of Cash Dividend Payment for IBM to be paid on 01/03/2011",
						"-11	12345	100000	H-200	Dividend Income	IBM	3	100	-390	09/20/2010	1	-390	0	09/20/2010	01/03/2011	3 GBP from Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_InForeignCurrency_DividendSecurityDenominationIsSameAsBaseCurrency() {
		AccountingEventJournal eventJournal = newDividendPaymentJournal();
		eventJournal.getSecurityEvent().setAdditionalSecurity(InvestmentSecurityBuilder.newGBP());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.2"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	GBP			300	09/20/2010	1.2	360	0	09/20/2010	01/01/2011	Accrual of Cash Dividend Payment for IBM to be paid on 01/01/2011",
						"-11	12345	100000	H-200	Dividend Income	IBM	3	100	-360	09/20/2010	1	-360	0	09/20/2010	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_InForeignCurrency_DividendSecurityDenominationIsDifferentFromBaseCurrency() {
		AccountingEventJournal eventJournal = newDividendPaymentJournal();

		InvestmentSecurity currencyDenomination = InvestmentSecurityBuilder.newEUR();
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(eventJournal.getDetailList().get(0).getAccountingTransaction());

		eventJournal.getSecurityEvent().getSecurity().getInstrument().setTradingCurrency(currencyDenomination);
		eventJournal.getSecurityEvent().setAdditionalSecurity(InvestmentSecurityBuilder.newGBP());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.2"));

		AccountingPaymentAccrualEventBookingRule bookingRule = newEventJournalBookingRule(AccountingPaymentAccrualEventBookingRule.class);
		// mock FX lookup from EUR to USD
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(currencyDenomination, baseCurrency, new BigDecimal("0.9")));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	GBP			300	09/20/2010	1.2	360	0	09/20/2010	01/01/2011	Accrual of Cash Dividend Payment for IBM to be paid on 01/01/2011",
						"-11	12345	100000	H-200	Dividend Income	IBM	3	100	-400	09/20/2010	0.9	-360	0	09/20/2010	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}
}
