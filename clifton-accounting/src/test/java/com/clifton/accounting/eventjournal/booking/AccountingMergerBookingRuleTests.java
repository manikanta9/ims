package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailMergerPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailWithFractionToCloseRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class AccountingMergerBookingRuleTests extends BaseEventJournalBookingRuleTests {

	AccountingMergerEventBookingRule mergerEventBookingRule;
	AccountingEventJournalDetailMergerPopulator accountingEventJournalDetailMergerPopulator;


	@BeforeEach
	public void setUp() {
		this.mergerEventBookingRule = newEventJournalBookingRule(AccountingMergerEventBookingRule.class);
		this.mergerEventBookingRule.setCalendarBusinessDayService(Mockito.mock(CalendarBusinessDayService.class));
		this.mergerEventBookingRule.setMarketDataRetriever(Mockito.mock(MarketDataRetriever.class));
		Mockito.when(this.mergerEventBookingRule.getCalendarBusinessDayService().getBusinessDayFrom(ArgumentMatchers.any(), ArgumentMatchers.anyInt()))
				.thenReturn(DateUtils.toDate("09/23/2019"));
		Mockito.when(this.mergerEventBookingRule.getMarketDataRetriever().getPrice(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.any(), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any()))
				.thenReturn(new BigDecimal("23.77"));

		this.mergerEventBookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(InvestmentSecurityBuilder.newGBP(), InvestmentSecurityBuilder.newUSD(), new BigDecimal("1.24295")));

		this.accountingEventJournalDetailMergerPopulator = new AccountingEventJournalDetailMergerPopulator();
		this.accountingEventJournalDetailMergerPopulator.setCalendarBusinessDayService(this.mergerEventBookingRule.getCalendarBusinessDayService());
		this.accountingEventJournalDetailMergerPopulator.setMarketDataRetriever(this.mergerEventBookingRule.getMarketDataRetriever());
		this.accountingEventJournalDetailMergerPopulator.setMarketDataExchangeRatesApiService(this.mergerEventBookingRule.getMarketDataExchangeRatesApiService());
	}


	@Test
	public void testMerger_SecurityElection() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout11 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 1, (short) 1, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout11);
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	109.558	22.18	2,430	09/25/2019	1	2,430	2,430	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-12	-11	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.18	-19.72	09/25/2019	1	-19.72	-19.72	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-13	-12	100000	H-200	Cash	USD	true	Goldman Sachs			4.28	09/25/2019	1	4.28	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-14	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-16	-12	100000	H-200	Realized Gain / Loss	IIVI	false	Goldman Sachs	23.77	0.18	15.44	09/25/2019	1	15.44	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_SecurityElection_TwoLots() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout11 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 1, (short) 1, FractionalShares.CASH_IN_LIEU, true);

		// 100 shares of FNSR at $24.3 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, merger.getSecurity()).qty(100).price("24.3").on("9/20/2019").build();
		// 50 shares of FNSR at $24.76 per share
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12346L, merger.getSecurity()).qty(50).price("24.76").on("9/20/2019").build();

		AccountingEventJournal eventJournal = newMergerJournal(merger, new AccountingTransaction[]{tran, tran2}, payout11);
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran, tran2);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	109.558	22.18	2,430	09/25/2019	1	2,430	2,430	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-12	-11	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.27	-29.58	09/25/2019	1	-29.58	-29.58	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-13	-12	100000	H-200	Cash	USD	true	Goldman Sachs			6.42	09/25/2019	1	6.42	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12346	100000	H-200	Position	IIVI	true	Goldman Sachs	111.632	11.09	1,238	09/25/2019	1	1,238	1,238	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-16	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-17	12346	100000	H-200	Position	FNSR	false	Goldman Sachs	24.76	-50	-1,238	09/25/2019	1	-1,238	-1,238	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-20	-12	100000	H-200	Realized Gain / Loss	IIVI	false	Goldman Sachs	23.77	0.27	23.16	09/25/2019	1	23.16	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyElection() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurityEventPayout payout12 = createCurrencyPayout(merger, BigDecimal.valueOf(15.93604384), (short) 1, (short) 2, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout12);
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Cash	USD	true	Goldman Sachs			1,593.6	09/25/2019	1	1,593.6	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Realized Gain / Loss	FNSR	false	Goldman Sachs	24.3	100	836.4	09/25/2019	1	836.4	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for FNSR"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityElection_ForLoss() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout31 = createCurrencyPayout(merger, BigDecimal.valueOf(15.6), (short) 1, (short) 2, true);
		InvestmentSecurityEventPayout payout32 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 3, (short) 2, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout31, payout32);
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Cash	USD	true	Goldman Sachs			1,560	09/25/2019	1	1,560	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	39.2245	22.18	870	09/25/2019	1	870	870	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.18	-7.06	09/25/2019	1	-7.06	-7.06	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-14	-13	100000	H-200	Cash	USD	true	Goldman Sachs			4.28	09/25/2019	1	4.28	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-17	-13	100000	H-200	Realized Gain / Loss	IIVI	false	Goldman Sachs	23.77	0.18	2.78	09/25/2019	1	2.78	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityElection_ForGain() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout31 = createCurrencyPayout(merger, BigDecimal.valueOf(20.6), (short) 1, (short) 2, true);
		InvestmentSecurityEventPayout payout32 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 3, (short) 2, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout31, payout32);
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		// There is no gain/loss for the fractional shares because all gain is in the close of the existing position.
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Cash	USD	true	Goldman Sachs			2,060	09/25/2019	1	2,060	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	23.77	22.18	527.22	09/25/2019	1	527.22	527.22	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.18	-4.28	09/25/2019	1	-4.28	-4.28	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-14	-13	100000	H-200	Cash	USD	true	Goldman Sachs			4.28	09/25/2019	1	4.28	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-16	-15	100000	H-200	Realized Gain / Loss	FNSR	false	Goldman Sachs	24.3	100	-157.22	09/25/2019	1	-157.22	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for FNSR"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityElection_ForGain_FractionalSharesGainLoss() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout31 = createCurrencyPayout(merger, BigDecimal.valueOf(2.6), (short) 1, (short) 2, true);
		InvestmentSecurityEventPayout payout32 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(1.137), (short) 3, (short) 2, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout31, payout32);
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		// There is gain/loss for the fractional shares because the new position's cost basis exceeds the original position.
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Cash	USD	true	Goldman Sachs			260	09/25/2019	1	260	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	21.372	113.7	2,430	09/25/2019	1	2,430	2,430	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.7	-14.96	09/25/2019	1	-14.96	-14.96	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 1.137 IIVI from Merger for FNSR on 09/25/2019",
						"-14	-13	100000	H-200	Cash	USD	true	Goldman Sachs			16.64	09/25/2019	1	16.64	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-16	-15	100000	H-200	Realized Gain / Loss	FNSR	false	Goldman Sachs	24.3	100	-260	09/25/2019	1	-260	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for FNSR",
						"-17	-13	100000	H-200	Realized Gain / Loss	IIVI	false	Goldman Sachs	23.77	0.7	-1.68	09/25/2019	1	-1.68	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityElection_ForeignCurrency_ForLoss() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout12 = createCurrencyPayout(merger, InvestmentSecurityBuilder.newGBP(), BigDecimal.valueOf(12.23), (short) 1, (short) 2, true);
		InvestmentSecurityEventPayout payout11 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 3, (short) 2, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout12, payout11);
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.24295"));
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Currency	GBP	true	Goldman Sachs			1,223	09/25/2019	1.24295	1,520.13	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	41.022	22.18	909.87	09/25/2019	1	909.87	909.87	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.18	-7.38	09/25/2019	1	-7.38	-7.38	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-14	-13	100000	H-200	Cash	USD	true	Goldman Sachs			4.28	09/25/2019	1	4.28	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-17	-13	100000	H-200	Realized Gain / Loss	IIVI	false	Goldman Sachs	23.77	0.18	3.1	09/25/2019	1	3.1	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityElection_ForeignCurrency_ForGain() {
		InvestmentSecurityEvent merger = createSecurityEvent();
		InvestmentSecurity payoutStock = InvestmentSecurityBuilder.newStock("IIVI").build();
		InvestmentSecurityEventPayout payout12 = createCurrencyPayout(merger, InvestmentSecurityBuilder.newGBP(), BigDecimal.valueOf(16.23), (short) 1, (short) 2, true);
		InvestmentSecurityEventPayout payout11 = createSecurityPayout(merger, payoutStock, BigDecimal.valueOf(0.2218), (short) 3, (short) 2, FractionalShares.CASH_IN_LIEU, true);

		AccountingEventJournal eventJournal = newMergerJournal(merger, payout12, payout11);
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.24295"));
		AccountingTransaction tran = eventJournal.getDetailList().get(0).getAccountingTransaction();
		AccountingPositionSplitterBookingRule<AccountingEventJournal> positionSplitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		// There is no gain/loss for the fractional shares because all gain is in the close of the existing position.
		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.mergerEventBookingRule,
						positionSplitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(false),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true),
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-11	12345	100000	H-200	Currency	GBP	true	Goldman Sachs			1,623	09/25/2019	1.24295	2,017.31	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"-12	12345	100000	H-200	Position	IIVI	true	Goldman Sachs	23.77	22.18	527.22	09/25/2019	1	527.22	527.22	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"-13	-12	100000	H-200	Position	IIVI	false	Goldman Sachs	23.77	-0.18	-4.28	09/25/2019	1	-4.28	-4.28	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"-14	-13	100000	H-200	Cash	USD	true	Goldman Sachs			4.28	09/25/2019	1	4.28	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"-15	12345	100000	H-200	Position	FNSR	false	Goldman Sachs	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"-16	-15	100000	H-200	Realized Gain / Loss	FNSR	false	Goldman Sachs	24.3	100	-114.53	09/25/2019	1	-114.53	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for FNSR"
				).execute();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private InvestmentSecurityEvent createSecurityEvent() {
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.MERGER);
		InvestmentSecurityEvent securityEvent = InvestmentTestObjectFactory.newInvestmentSecurityEvent(InvestmentTestObjectFactory.newInvestmentSecurity(80510, "FNSR", InvestmentType.STOCKS), eventType, "9/25/2019");
		securityEvent.setBeforeAndAfterEventValue(BigDecimal.valueOf(15.6));
		securityEvent.setAdditionalSecurity(InvestmentSecurityBuilder.newUSD());
		return securityEvent;
	}


	private InvestmentSecurityEventPayout createCurrencyPayout(InvestmentSecurityEvent event, BigDecimal payoutValue, short electionNumber, short payoutNumber, boolean defaultElection) {
		return createCurrencyPayout(event, InvestmentSecurityBuilder.newUSD(), payoutValue, electionNumber, payoutNumber, defaultElection);
	}


	private InvestmentSecurityEventPayout createCurrencyPayout(InvestmentSecurityEvent event, InvestmentSecurity currency, BigDecimal payoutValue, short electionNumber, short payoutNumber, boolean defaultElection) {
		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.CURRENCY, currency);
		payout.setBeforeAndAfterEventValue(payoutValue);
		payout.setDefaultElection(defaultElection);
		payout.setElectionNumber(electionNumber);
		payout.setPayoutNumber(payoutNumber);
		return payout;
	}


	private InvestmentSecurityEventPayout createSecurityPayout(InvestmentSecurityEvent event, InvestmentSecurity security, BigDecimal afterValue, short electionNumber, short payoutNumber, FractionalShares fractionalShares, boolean defaultElection) {
		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_NEW, security, fractionalShares);
		payout.setBeforeEventValue(BigDecimal.ONE);
		payout.setAfterEventValue(afterValue);
		payout.setDefaultElection(defaultElection);
		payout.setElectionNumber(electionNumber);
		payout.setPayoutNumber(payoutNumber);
		return payout;
	}


	protected AccountingEventJournal newMergerJournal(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventPayout... payouts) {
		// 100 shares of FNSR at $24.3 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, securityEvent.getSecurity())
				.qty(100).price("24.3").on("9/20/2019").build();

		return newMergerJournal(securityEvent, new AccountingTransaction[]{tran}, payouts);
	}


	protected AccountingEventJournal newMergerJournal(InvestmentSecurityEvent securityEvent, AccountingTransaction[] transactions, InvestmentSecurityEventPayout... payouts) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);

		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(securityEvent, CollectionUtils.createList(payouts), this.accountingEventJournalDetailMergerPopulator, rounder, transactions);
	}
}
