package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockDividendPopulator;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class AccountingEventJournalDetailStockDividendRounderTests {

	private final static InvestmentSecurity DWDP_STOCK = InvestmentTestObjectFactory.newInvestmentSecurity(100, "DWDP", InvestmentType.STOCKS);

	private final static String[] ROUND_DOWN_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	33.7394	32.98084	237.336	7,827.54	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	32.980863	3,278.898	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	35.24324	157.542	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	34.0796	368.28	12,550.84	0	null"
	};
	private final static String[] ROUND_UP_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	33.7394	32.98084	237.336	7,827.54	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	32.980863	3,279.898	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	35.24324	157.542	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	34.0796	368.28	12,550.84	0	null"
	};
	private final static String[] CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0.84	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	33.7394	32.98084	237.336	7,827.54	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	32.980863	3,279.738	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	35.24324	157.542	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	34.0796	368.28	12,550.84	0	null"
	};
	private final static String[] CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0.1396056338	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	32.9808	65.472	2,159.32	0.1396056338	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	33.7394	32.98084	237.336	7,827.54	0.0993802817	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	32.980863	3,279.738	108,168.59	0.2182816901	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	35.24324	157.542	5,552.29	0.1603098592	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	34.0796	368.28	12,550.84	0.0828169014	null"
	};

	///////////////////////////////////////////////////////////////////////////
	// Stock Dividend
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void test_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderTest(null, false, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(null, true, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderTest(null, false, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderTest(null, true, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	private void executeFirstDetailRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeProportionalRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.PROPORTIONALLY);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeTest(AccountingEventJournalDetailRounder rounder, FractionalShares fractionalShares, String[] expectedDetails) {
		AccountingEventJournalDetailRounderTestExecutor.StockDividendDetailRounderTestExecutor.of(rounder)
				.withEventJournal(getDividendEventJournal(DWDP_STOCK, fractionalShares))
				.withFractionalShares(fractionalShares)
				.withExpectedResults(expectedDetails)
				.execute();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal getDividendEventJournal(InvestmentSecurity security, FractionalShares fractionalShares) {
		EventData eventData = getDividendSecurityEvent(security, fractionalShares);
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, new AccountingEventJournalDetailStockDividendPopulator(), getTransactions(eventData.getEvent()));
	}


	private EventData getDividendSecurityEvent(InvestmentSecurity security, FractionalShares fractionalShares) {
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(security, eventType, "06/03/2019");
		event.setBeforeAndAfterEventValue(new BigDecimal("0.023"));

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_STOCK_DIVIDEND, fractionalShares);

		return EventData.forEventPayoutOrEvent(payout, event);
	}


	private AccountingTransaction[] getTransactions(InvestmentSecurityEvent event) {
		return new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283737, event.getSecurity()).qty(232).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283738, event.getSecurity()).qty(3206).price("33.739423").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283739, event.getSecurity()).qty(154).price("36.0538").on("01/25/2018").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283740, event.getSecurity()).qty(360).price("34.86344").on("02/02/2018").build()
		};
	}
}
