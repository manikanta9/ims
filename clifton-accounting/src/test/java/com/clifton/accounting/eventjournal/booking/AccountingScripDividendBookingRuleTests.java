package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailScripDividendPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailWithFractionToCloseRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class AccountingScripDividendBookingRuleTests extends BaseEventJournalBookingRuleTests {


	@Test
	public void testScripDividend_SecurityElection_Accrual() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);
		bookingRule.setAccrual(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection(FractionalShares.CASH_IN_LIEU, true))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	55	2.3	-126.5	09/19/2010	1	-126.5	0	09/19/2010	01/01/2011	0.023 Scrip Dividend for IBM on 01/01/2011",
						"-11	null	100000	H-200	Position Receivable	IBM	55	2.3	126.5	09/19/2010	1	126.5	126.5	09/19/2010	01/01/2011	Receivable from 0.023 Scrip Dividend for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testScripDividend_SecurityElection_AccrualReversal() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);
		bookingRule.setAccrual(false);
		AccountingTransaction t = new AccountingTransaction();
		t.setId(7777777L);
		Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransactionList(ArgumentMatchers.any(AccountingTransactionSearchForm.class)))
				.thenReturn(CollectionUtils.createList(t));

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection(FractionalShares.CASH_IN_LIEU, true))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	7777777	100000	H-200	Position Receivable	IBM	55	-2.3	-126.5	09/19/2010	1	-126.5	-126.5	09/19/2010	01/01/2011	Reversal of Receivable from 0.023 Scrip Dividend for IBM on 01/01/2011",
						"-11	null	100000	H-200	Position	IBM	55	2.3	126.5	09/19/2010	1	126.5	126.5	09/19/2010	01/01/2011	0.023 Scrip Dividend for IBM on 01/01/2011",
						"-12	null	100000	H-200	Position	IBM	55	-0.3	-16.5	09/19/2010	1	-16.5	-16.5	09/19/2010	01/01/2011	Fractional Shares Closing from rounding from 0.023 Scrip Dividend for IBM on 01/01/2011",
						"-13	-12	100000	H-200	Cash	USD			16.5	09/19/2010	1	16.5	0	09/19/2010	01/01/2011	Cash proceeds from close of IBM Fractional Shares from Scrip Dividend"
				).execute();
	}


	@Test
	public void testScripDividend_SecurityElection_NoDelay() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection(FractionalShares.CASH_IN_LIEU, false))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	55	2.3	-126.5	09/19/2010	1	-126.5	0	09/19/2010	09/19/2010	0.023 Scrip Dividend for IBM on 09/19/2010",
						"-11	null	100000	H-200	Position	IBM	55	2.3	126.5	09/19/2010	1	126.5	126.5	09/19/2010	09/19/2010	0.023 Scrip Dividend for IBM on 09/19/2010",
						"-12	null	100000	H-200	Position	IBM	55	-0.3	-16.5	09/19/2010	1	-16.5	-16.5	09/19/2010	09/19/2010	Fractional Shares Closing from rounding from 0.023 Scrip Dividend for IBM on 09/19/2010",
						"-13	-12	100000	H-200	Cash	USD			16.5	09/19/2010	1	16.5	0	09/19/2010	09/19/2010	Cash proceeds from close of IBM Fractional Shares from Scrip Dividend"
				).execute();
	}


	@Test
	public void testScripDividend_SecurityElection_Accrual_MultipleLots() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);
		bookingRule.setAccrual(true);

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection_MultipleLots(FractionalShares.CASH_IN_LIEU, true))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	55	0.575	-31.625	09/19/2010	1	-31.63	0	09/19/2010	01/01/2011	0.023 Scrip Dividend for IBM on 01/01/2011",
						"-11	12346	100000	H-200	Dividend Income	IBM	55	1.725	-94.875	09/19/2010	1	-94.88	0	09/19/2010	01/01/2011	0.023 Scrip Dividend for IBM on 01/01/2011",
						"-12	null	100000	H-200	Position Receivable	IBM	55	2.3	126.5	09/19/2010	1	126.51	126.5	09/19/2010	01/01/2011	Receivable from 0.023 Scrip Dividend for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testScripDividend_SecurityElection_AccrualReversal_MultipleLots() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);
		bookingRule.setAccrual(false);
		AccountingTransaction t = new AccountingTransaction();
		t.setId(7777777L);
		Mockito.when(bookingRule.getAccountingTransactionService().getAccountingTransactionList(ArgumentMatchers.any(AccountingTransactionSearchForm.class)))
				.thenReturn(CollectionUtils.createList(t));

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection_MultipleLots(FractionalShares.CASH_IN_LIEU, true))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	7777777	100000	H-200	Position Receivable	IBM	55	-2.3	-126.5	09/19/2010	1	-126.51	-126.5	09/19/2010	01/01/2011	Reversal of Receivable from 0.023 Scrip Dividend for IBM on 01/01/2011",
						"-11	null	100000	H-200	Position	IBM	55	2.3	126.5	09/19/2010	1	126.51	126.5	09/19/2010	01/01/2011	0.023 Scrip Dividend for IBM on 01/01/2011",
						"-12	null	100000	H-200	Position	IBM	55	-0.3	-16.5	09/19/2010	1	-16.5	-16.5	09/19/2010	01/01/2011	Fractional Shares Closing from rounding from 0.023 Scrip Dividend for IBM on 01/01/2011",
						"-13	-12	100000	H-200	Cash	USD			16.5	09/19/2010	1	16.5	0	09/19/2010	01/01/2011	Cash proceeds from close of IBM Fractional Shares from Scrip Dividend"
				).execute();
	}


	@Test
	public void testScripDividend_SecurityElection_NoDelay_MultipleLots() {
		AccountingScripDividendBookingRule bookingRule = newEventJournalBookingRule(AccountingScripDividendBookingRule.class);

		AccountingBookingRuleTestExecutor.newTestForEntity(newScripDividendJournal_SecurityElection_MultipleLots(FractionalShares.CASH_IN_LIEU, false))
				.forBookingRules(bookingRule)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	55	0.575	-31.625	09/19/2010	1	-31.63	0	09/19/2010	09/19/2010	0.023 Scrip Dividend for IBM on 09/19/2010",
						"-11	12346	100000	H-200	Dividend Income	IBM	55	1.725	-94.875	09/19/2010	1	-94.88	0	09/19/2010	09/19/2010	0.023 Scrip Dividend for IBM on 09/19/2010",
						"-12	null	100000	H-200	Position	IBM	55	2.3	126.5	09/19/2010	1	126.51	126.5	09/19/2010	09/19/2010	0.023 Scrip Dividend for IBM on 09/19/2010",
						"-13	null	100000	H-200	Position	IBM	55	-0.3	-16.5	09/19/2010	1	-16.5	-16.5	09/19/2010	09/19/2010	Fractional Shares Closing from rounding from 0.023 Scrip Dividend for IBM on 09/19/2010",
						"-14	-13	100000	H-200	Cash	USD			16.5	09/19/2010	1	16.5	0	09/19/2010	09/19/2010	Cash proceeds from close of IBM Fractional Shares from Scrip Dividend"
				).execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurityEvent createSecurityEvent(boolean delayPayment) {
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.DIVIDEND_SCRIP);
		InvestmentSecurityEvent securityEvent = InvestmentTestObjectFactory.newInvestmentSecurityEvent(InvestmentTestObjectFactory.newInvestmentSecurity(876, "IBM", InvestmentType.STOCKS), eventType, "9/20/2010");
		securityEvent.setBeforeAndAfterEventValue(new BigDecimal(0.5));
		securityEvent.setEventDate(DateUtils.toDate(delayPayment ? "1/1/2011" : "9/19/2010"));
		securityEvent.setAdditionalSecurity(securityEvent.getSecurity());
		return securityEvent;
	}


	private InvestmentSecurityEventPayout createPayout(FractionalShares fractionalShares, InvestmentSecurityEvent securityEvent) {
		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(securityEvent, InvestmentSecurityEventPayoutType.SECURITY_EXISTING, fractionalShares);
		payout.setBeforeAndAfterEventValue(new BigDecimal("0.023"));
		payout.setAdditionalPayoutValue(new BigDecimal("55"));
		return payout;
	}


	protected AccountingEventJournal newScripDividendJournal_SecurityElection(FractionalShares fractionalShares, boolean delayPayment) {
		// apply the rule: 0.023 shares per share on 100 shares of IBM at $55 per share
		InvestmentSecurityEvent securityEvent = createSecurityEvent(delayPayment);
		InvestmentSecurityEventPayout payout = createPayout(fractionalShares, securityEvent);

		AccountingEventJournalDetailScripDividendPopulator populator = new AccountingEventJournalDetailScripDividendPopulator();
		populator.setInvestmentCalculator(Mockito.mock(InvestmentCalculator.class));

		// full position is open
		Mockito.when(populator.getInvestmentCalculator().calculateNotional(ArgumentMatchers.eq(securityEvent.getSecurity()), ArgumentMatchers.eq(payout.getAdditionalPayoutValue()), ArgumentMatchers.eq(new BigDecimal("2.3")), ArgumentMatchers.eq(BigDecimal.ONE)))
				.thenReturn(MathUtils.multiply(payout.getAdditionalPayoutValue(), new BigDecimal("2.3")));

		// 100 shares of IBM at $50 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, securityEvent.getSecurity()).qty(100).price("50").on("6/1/2010").build();
		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, securityEvent), populator, tran);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}


	protected AccountingEventJournal newScripDividendJournal_SecurityElection_MultipleLots(FractionalShares fractionalShares, boolean delayPayment) {
		InvestmentSecurityEvent securityEvent = createSecurityEvent(delayPayment);
		InvestmentSecurityEventPayout payout = createPayout(fractionalShares, securityEvent);

		AccountingEventJournalDetailScripDividendPopulator populator = new AccountingEventJournalDetailScripDividendPopulator();
		populator.setInvestmentCalculator(Mockito.mock(InvestmentCalculator.class));
		Mockito.when(populator.getInvestmentCalculator().calculateNotional(ArgumentMatchers.eq(securityEvent.getSecurity()), ArgumentMatchers.eq(payout.getAdditionalPayoutValue()), ArgumentMatchers.eq(new BigDecimal("0.575")), ArgumentMatchers.eq(BigDecimal.ONE)))
				.thenReturn(MathUtils.multiply(payout.getAdditionalPayoutValue(), new BigDecimal("0.575")));
		Mockito.when(populator.getInvestmentCalculator().calculateNotional(ArgumentMatchers.eq(securityEvent.getSecurity()), ArgumentMatchers.eq(payout.getAdditionalPayoutValue()), ArgumentMatchers.eq(new BigDecimal("1.725")), ArgumentMatchers.eq(BigDecimal.ONE)))
				.thenReturn(MathUtils.multiply(payout.getAdditionalPayoutValue(), new BigDecimal("1.725")));

		// 100 shares of IBM; 2 lots
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345L, securityEvent.getSecurity()).qty(25).price("49.75").on("6/1/2010").build();
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12346L, securityEvent.getSecurity()).qty(75).price("50.25").on("6/1/2010").build();
		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, securityEvent), populator, tran, tran2);

		// round lots
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}
}
