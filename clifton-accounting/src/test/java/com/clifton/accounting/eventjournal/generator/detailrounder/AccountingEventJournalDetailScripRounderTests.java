package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailScripDividendPopulator;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Optional;


/**
 * @author nickk
 */
public class AccountingEventJournalDetailScripRounderTests {

	private static final InvestmentSecurity DWDP_STOCK = InvestmentTestObjectFactory.newInvestmentSecurity(100, "DWDP", InvestmentType.STOCKS);

	private final static String[] ROUND_DOWN_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232		55	293.48	5.336	293.48	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206		55	4,055.59	72.898	4,055.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154		55	194.81	3.542	194.81	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360		55	455.4	8.28	455.4	0	null"
	};
	private final static String[] ROUND_UP_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232		55	293.48	5.336	293.48	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206		55	4,055.59	73.898	4,055.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154		55	194.81	3.542	194.81	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360		55	455.4	8.28	455.4	0	null"
	};
	private final static String[] CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0.84	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232		55	293.48	5.336	293.48	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206		55	4,055.59	73.738	4,055.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154		55	194.81	3.542	194.81	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360		55	455.4	8.28	455.4	0	null"
	};
	private final static String[] CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0.1396056338	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64		55	80.96	1.472	80.96	0.1396056338	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232		55	293.48	5.336	293.48	0.0993802817	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206		55	4,055.59	73.738	4,055.59	0.2182816901	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154		55	194.81	3.542	194.81	0.1603098592	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360		55	455.4	8.28	455.4	0.0828169014	null"
	};

	private final AccountingEventJournalDetailScripDividendPopulator scripDividendPopulator = new AccountingEventJournalDetailScripDividendPopulator();
	private final InvestmentCalculator investmentCalculator = Mockito.mock(InvestmentCalculator.class);


	@BeforeEach
	public void setup() {
		this.scripDividendPopulator.setInvestmentCalculator(this.investmentCalculator);
	}

	///////////////////////////////////////////////////////////////////////////
	// Stock Scrip
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void test_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderTest(null, false, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(null, true, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_FIRST_DETAIL_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderTest(null, false, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, false, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, false, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, false, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderTest(null, true, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.CASH_IN_LIEU, true, CASH_IN_LIEU_PROPORTIONAL_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_DOWN, true, ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void test_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderTest(FractionalShares.ROUND_HALF_UP, true, ROUND_UP_EXPECTED_DETAILS);
	}


	private void executeFirstDetailRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeProportionalRounderTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.PROPORTIONALLY);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeTest(rounder, fractionalShares, expectedDetails);
	}


	private void executeTest(AccountingEventJournalDetailRounder rounder, FractionalShares fractionalShares, String[] expectedDetails) {
		AccountingEventJournal journal = getScripJournal(fractionalShares);

		Optional<InvestmentSecurityEventPayout> payout = CollectionUtils.getStream(journal.getDetailList())
				.filter(detail -> detail.getEventPayout() != null)
				.map(AccountingEventJournalDetail::getEventPayout)
				.findFirst();

		payout.ifPresent(investmentSecurityEventPayout -> {
			for (AccountingEventJournalDetail detail : journal.getDetailList()) {
				BigDecimal detailPayoutValue = MathUtils.multiply(investmentSecurityEventPayout.getAfterEventValue(), detail.getAffectedQuantity()).stripTrailingZeros();
				Mockito.when(this.investmentCalculator.calculateNotional(ArgumentMatchers.eq(journal.getSecurityEvent().getSecurity()), ArgumentMatchers.eq(investmentSecurityEventPayout.getAdditionalPayoutValue()), ArgumentMatchers.eq(detailPayoutValue), ArgumentMatchers.eq(BigDecimal.ONE)))
						.thenReturn(MathUtils.multiply(investmentSecurityEventPayout.getAdditionalPayoutValue(), detailPayoutValue));
			}
			// add mocks for aggregates: round down, round up, cash in lieu
			for (BigDecimal aggregateTotalQuantity : new BigDecimal[]{new BigDecimal("93"), new BigDecimal("94"), new BigDecimal("93.84")}) {
				Mockito.when(this.investmentCalculator.calculateNotional(ArgumentMatchers.eq(journal.getSecurityEvent().getSecurity()), ArgumentMatchers.eq(investmentSecurityEventPayout.getAdditionalPayoutValue()), ArgumentMatchers.eq(aggregateTotalQuantity), ArgumentMatchers.eq(BigDecimal.ONE)))
						.thenReturn(MathUtils.multiply(investmentSecurityEventPayout.getAdditionalPayoutValue(), aggregateTotalQuantity));
			}
		});

		AccountingEventJournalDetailRounderTestExecutor.ScripDetailRounderTestExecutor.of(this.investmentCalculator, rounder)
				.withEventJournal(journal)
				.withFractionalShares(fractionalShares)
				.withExpectedResults(expectedDetails)
				.executeAndPrintResults();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal getScripJournal(FractionalShares fractionalShares) {
		InvestmentSecurityEvent event = getScripSecurityEvent();
		InvestmentSecurityEventPayout payout = createPayout(event, fractionalShares);

		AccountingTransaction[] transactions = getTransactions(event);
		for (AccountingTransaction transaction : transactions) {
			BigDecimal detailPayoutValue = MathUtils.multiply(payout.getAfterEventValue(), transaction.getQuantity()).stripTrailingZeros();
			Mockito.when(this.investmentCalculator.calculateNotional(ArgumentMatchers.eq(event.getSecurity()), ArgumentMatchers.eq(payout.getAdditionalPayoutValue()), ArgumentMatchers.eq(detailPayoutValue), ArgumentMatchers.eq(BigDecimal.ONE)))
					.thenReturn(MathUtils.multiply(payout.getAdditionalPayoutValue(), detailPayoutValue));
		}

		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, event), this.scripDividendPopulator, transactions);
	}


	private InvestmentSecurityEvent getScripSecurityEvent() {
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.DIVIDEND_SCRIP);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(DWDP_STOCK, eventType, "06/03/2019");
		event.setAdditionalSecurity(event.getSecurity());
		event.setBeforeAndAfterEventValue(new BigDecimal("0.5"));
		event.setAdditionalEventValue(new BigDecimal("0.705439"));
		return event;
	}


	private InvestmentSecurityEventPayout createPayout(InvestmentSecurityEvent securityEvent, FractionalShares fractionalShares) {
		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(securityEvent, InvestmentSecurityEventPayoutType.SECURITY_EXISTING, fractionalShares);
		payout.setBeforeAndAfterEventValue(new BigDecimal("0.023"));
		payout.setAdditionalPayoutValue(new BigDecimal("55"));
		return payout;
	}


	private AccountingTransaction[] getTransactions(InvestmentSecurityEvent event) {
		return new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283737, event.getSecurity()).qty(232).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283738, event.getSecurity()).qty(3206).price("33.739423").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283739, event.getSecurity()).qty(154).price("36.0538").on("01/25/2018").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283740, event.getSecurity()).qty(360).price("34.86344").on("02/02/2018").build()
		};
	}
}
