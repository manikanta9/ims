package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockDividendPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailWithFractionToCloseRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class AccountingStockDividendBookingRuleTests extends BaseEventJournalBookingRuleTests {


	private final InvestmentSecurity dividendSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(100, "AAPL", InvestmentType.STOCKS);

	private AccountingPositionSplitterBookingRule<AccountingEventJournal> accountingPositionSplitterBookingRule;
	private AccountingStockDividendBookingRule accountingStockDividendAccrualNonTaxableBookingRule;
	private AccountingStockDividendBookingRule accountingStockDividendAccrualReversalNonTaxableBookingRule;
	private AccountingRealizedGainLossBookingRule<AccountingEventJournal> accountingRealizedGainLossBookingRule;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		AccountingTransaction transaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(999999L, this.dividendSecurity).qty(7).price("42.00808").on("04/16/2018").build();
		this.accountingPositionSplitterBookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(transaction);

		this.accountingStockDividendAccrualNonTaxableBookingRule = newEventJournalBookingRule(AccountingStockDividendBookingRule.class);
		this.accountingStockDividendAccrualNonTaxableBookingRule.setAccrual(true);
		this.accountingStockDividendAccrualNonTaxableBookingRule.setTaxable(false);
		this.accountingStockDividendAccrualNonTaxableBookingRule.setInvestmentCalculator(new InvestmentCalculatorImpl());

		this.accountingStockDividendAccrualReversalNonTaxableBookingRule = newEventJournalBookingRule(AccountingStockDividendBookingRule.class);
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setAccrual(false);
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setTaxable(false);
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setInvestmentCalculator(new InvestmentCalculatorImpl());
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setMarketDataRetriever(Mockito.mock(MarketDataRetriever.class));
		Mockito.when(
				this.accountingStockDividendAccrualReversalNonTaxableBookingRule.getMarketDataRetriever().getPrice(ArgumentMatchers.eq(this.dividendSecurity), ArgumentMatchers.eq(DateUtils.toDate("04/25/2018")), ArgumentMatchers.eq(true), ArgumentMatchers.anyString()))
				.thenReturn(new BigDecimal("45"));
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));

		Mockito.when(
				this.accountingStockDividendAccrualReversalNonTaxableBookingRule.getAccountingTransactionService().getAccountingTransactionList(ArgumentMatchers.any(AccountingTransactionSearchForm.class)))
				.thenReturn(CollectionUtils.createList(transaction));
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		this.accountingStockDividendAccrualReversalNonTaxableBookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		AccountingTestObjectFactory.newAccountingBookingPositionRetriever(this.accountingStockDividendAccrualReversalNonTaxableBookingRule);

		this.accountingRealizedGainLossBookingRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true);
	}


	@Test
	public void testStockDividendAccrualNonTaxable_CashInLieu() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.CASH_IN_LIEU))
				.forBookingRules(
						this.accountingStockDividendAccrualNonTaxableBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Position	AAPL	false	Goldman Sachs	42.9882833333	-300	-12,896.48	04/16/2018	1	-12,896.48	-12,896.48	06/29/2009	04/16/2018	Closing from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	41.93977	300	12,581.93	04/16/2018	1	12,581.93	12,581.93	06/29/2009	04/16/2018	Reopening from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-12	1283735	100000	H-200	Position Receivable	AAPL	true	Goldman Sachs	41.93977	7.5	314.55	04/16/2018	1	314.55	314.55	06/29/2009	04/25/2018	Receivable from 0.025 Stock Dividend Payment for AAPL on 04/25/2018"
				).execute();
	}


	@Test
	public void testStockDividendAccrualNonTaxable_RoundDown() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_DOWN))
				.forBookingRules(
						this.accountingStockDividendAccrualNonTaxableBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Position	AAPL	false	Goldman Sachs	42.9882833333	-300	-12,896.48	04/16/2018	1	-12,896.48	-12,896.48	06/29/2009	04/16/2018	Closing from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	42.00808	300	12,602.42	04/16/2018	1	12,602.42	12,602.42	06/29/2009	04/16/2018	Reopening from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-12	1283735	100000	H-200	Position Receivable	AAPL	true	Goldman Sachs	42.00808	7	294.06	04/16/2018	1	294.06	294.06	06/29/2009	04/25/2018	Receivable from 0.025 Stock Dividend Payment for AAPL on 04/25/2018"
				).execute();
	}


	@Test
	public void testStockDividendAccrualReversalNonTaxable_CashInLieu() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.CASH_IN_LIEU))
				.forBookingRules(
						this.accountingStockDividendAccrualReversalNonTaxableBookingRule,
						this.accountingPositionSplitterBookingRule,
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	999999	100000	H-200	Position Receivable	AAPL	false	Goldman Sachs	41.93977	-7.5	-314.55	04/25/2018	1	-314.55	-314.55	04/16/2018	04/25/2018	Reversal of Receivable from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	41.93977	7.5	314.55	04/25/2018	1	314.55	314.55	06/29/2009	04/25/2018	0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-13	-14	100000	H-200	Cash	USD	true	Goldman Sachs			22.5	04/25/2018	1	22.5	0	06/29/2009	04/25/2018	Cash proceeds from close of AAPL",
						"-14	-11	100000	H-200	Position	AAPL	false	Goldman Sachs	45	-0.5	-20.97	04/25/2018	1	-20.97	-20.97	06/29/2009	04/25/2018	Fractional Shares Closing from rounding from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-15	-14	100000	H-200	Realized Gain / Loss	AAPL	false	Goldman Sachs	45	0.5	-1.53	04/25/2018	1	-1.53	0	06/29/2009	04/25/2018	Realized Gain / Loss gain for AAPL"
				).execute();
	}


	@Test
	public void testStockDividendAccrualReversalNonTaxable_RoundDown() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_DOWN))
				.forBookingRules(
						this.accountingStockDividendAccrualReversalNonTaxableBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	999999	100000	H-200	Position Receivable	AAPL	false	Goldman Sachs	42.00808	-7	-294.06	04/25/2018	1	-294.06	-294.06	06/29/2009	04/25/2018	Reversal of Receivable from 0.025 Stock Dividend Payment for AAPL on 04/25/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	42.00808	7	294.06	04/25/2018	1	294.06	294.06	06/29/2009	04/25/2018	0.025 Stock Dividend Payment for AAPL on 04/25/2018"
				).execute();
	}


	@Test
	public void testStockDividendFxRateRounding_Accrual() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournalRounding(FractionalShares.ROUND_DOWN))
				.forBookingRules(
						this.accountingStockDividendAccrualNonTaxableBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	18433578	100000	H-200	Position	AAPL	false	Goldman Sachs	13.88	-3,000	-41,640	06/08/2020	0.1964194378	-8,178.91	-41,640	06/29/2009	06/08/2020	Closing from 0.05 Stock Dividend Payment for AAPL on 06/23/2020",
						"-11	18433578	100000	H-200	Position	AAPL	true	Goldman Sachs	13.219048	3,000	39,657.14	06/08/2020	0.1964194378	7,789.43	39,657.14	06/29/2009	06/08/2020	Reopening from 0.05 Stock Dividend Payment for AAPL on 06/23/2020",
						"-12	18433578	100000	H-200	Position Receivable	AAPL	true	Goldman Sachs	13.219048	150	1,982.86	06/08/2020	0.1964194378	389.48	1,982.86	06/29/2009	06/23/2020	Receivable from 0.05 Stock Dividend Payment for AAPL on 06/23/2020"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal buildEventJournal(FractionalShares fractionalShares) {
		Date originalDate = DateUtils.toDate("06/29/2009");

		// 0.025 AAPL Stock Dividend on 04/25/2018
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.dividendSecurity, InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT), "04/25/2018");
		event.setBeforeAndAfterEventValue(new BigDecimal("0.025"));
		event.setExDate(DateUtils.toDate("04/17/2018"));

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_STOCK_DIVIDEND, fractionalShares);

		// 300 shares at $42.9882833333 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(300).price("42.9882833333").on(originalDate).build();

		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailStockDividendPopulator();
		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, event), populator, tran);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}


	private AccountingEventJournal buildEventJournalRounding(FractionalShares fractionalShares) {
		Date originalDate = DateUtils.toDate("06/29/2009");

		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.dividendSecurity, InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT), "06/23/2020");
		event.setBeforeAndAfterEventValue(new BigDecimal("0.05"));
		event.setExDate(DateUtils.toDate("06/09/2020"));

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_STOCK_DIVIDEND, fractionalShares);

		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(18433578, event.getSecurity()).qty(3000).price("13.880000000000001").on(originalDate).exchangeRateToBase("0.1964194377581099").build();

		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailStockDividendPopulator();
		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, event), populator, tran);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}
}
