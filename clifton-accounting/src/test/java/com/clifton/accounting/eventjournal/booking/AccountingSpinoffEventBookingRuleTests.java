package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailSpinoffPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailWithFractionToCloseRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author vgomelsky
 */
public class AccountingSpinoffEventBookingRuleTests extends BaseEventJournalBookingRuleTests {

	private final InvestmentSecurity parentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(100, "WM", InvestmentType.STOCKS);
	private final InvestmentSecurity spinoffSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(110, "FB", InvestmentType.STOCKS);

	private AccountingSpinoffEventBookingRule accountingSpinoffEventBookingRule;
	private AccountingRealizedGainLossBookingRule<AccountingEventJournal> accountingRealizedGainLossBookingRule;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		this.accountingSpinoffEventBookingRule = newEventJournalBookingRule(AccountingSpinoffEventBookingRule.class);
		this.accountingSpinoffEventBookingRule.setMarketDataRetriever(Mockito.mock(MarketDataRetriever.class));
		this.accountingSpinoffEventBookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		this.accountingSpinoffEventBookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		Mockito.when(
				this.accountingSpinoffEventBookingRule.getMarketDataRetriever().getPrice(ArgumentMatchers.eq(this.spinoffSecurity), ArgumentMatchers.any(), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any()))
				.thenReturn(new BigDecimal("193.09"));

		AccountingTestObjectFactory.newAccountingBookingPositionRetriever(this.accountingSpinoffEventBookingRule);

		this.accountingRealizedGainLossBookingRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true);
	}


	@Test
	public void testSpinoffOneLot_CashInLieu() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.CASH_IN_LIEU))
				.forBookingRules(
						this.accountingSpinoffEventBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	8006252	100000	H-200	Position	WM	false	Goldman Sachs	81.63	-745	-60,814.35	02/01/2018	1	-60,814.35	-60,814.35	01/22/2018	02/01/2018	Parent Closing from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-11	11111	100000	H-200	Position	WM	true	Goldman Sachs	66.92107	745	49,856.2	02/01/2018	1	49,856.2	49,856.2	01/22/2018	02/01/2018	Parent Reopening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-12	8006252	100000	H-200	Position	FB	true	Goldman Sachs	147.0893	74.5	10,958.15	02/01/2018	1	10,958.15	10,958.15	01/22/2018	02/01/2018	Spinoff Opening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-14	-15	100000	H-200	Cash	USD	true	Goldman Sachs			96.55	02/01/2018	1	96.55	0	01/22/2018	02/01/2018	Cash proceeds from close of FB",
						"-15	-12	100000	H-200	Position	FB	false	Goldman Sachs	193.09	-0.5	-73.54	02/01/2018	1	-73.54	-73.54	01/22/2018	02/01/2018	Fractional Shares Closing from rounding from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-16	-15	100000	H-200	Realized Gain / Loss	FB	false	Goldman Sachs	193.09	0.5	-23.01	02/01/2018	1	-23.01	0	01/22/2018	02/01/2018	Realized Gain / Loss gain for FB"
				).execute();
	}


	@Test
	public void testSpinoffOneLot_RoundDown() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_DOWN))
				.forBookingRules(
						this.accountingSpinoffEventBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	8006252	100000	H-200	Position	WM	false	Goldman Sachs	81.63	-745	-60,814.35	02/01/2018	1	-60,814.35	-60,814.35	01/22/2018	02/01/2018	Parent Closing from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-11	11111	100000	H-200	Position	WM	true	Goldman Sachs	66.92107	745	49,856.2	02/01/2018	1	49,856.2	49,856.2	01/22/2018	02/01/2018	Parent Reopening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-12	8006252	100000	H-200	Position	FB	true	Goldman Sachs	148.0831	74	10,958.15	02/01/2018	1	10,958.15	10,958.15	01/22/2018	02/01/2018	Spinoff Opening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018"
				).execute();
	}


	@Test
	public void testSpinoffOneLot_RoundUp() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_UP))
				.forBookingRules(
						this.accountingSpinoffEventBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	8006252	100000	H-200	Position	WM	false	Goldman Sachs	81.63	-745	-60,814.35	02/01/2018	1	-60,814.35	-60,814.35	01/22/2018	02/01/2018	Parent Closing from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-11	11111	100000	H-200	Position	WM	true	Goldman Sachs	66.92107	745	49,856.2	02/01/2018	1	49,856.2	49,856.2	01/22/2018	02/01/2018	Parent Reopening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-12	8006252	100000	H-200	Position	FB	true	Goldman Sachs	146.1087	75	10,958.15	02/01/2018	1	10,958.15	10,958.15	01/22/2018	02/01/2018	Spinoff Opening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018"
				).execute();
	}


	@Test
	public void testSpinoffOneLot_RoundHalfUp() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_HALF_UP))
				.forBookingRules(
						this.accountingSpinoffEventBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	8006252	100000	H-200	Position	WM	false	Goldman Sachs	81.63	-745	-60,814.35	02/01/2018	1	-60,814.35	-60,814.35	01/22/2018	02/01/2018	Parent Closing from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-11	11111	100000	H-200	Position	WM	true	Goldman Sachs	66.92107	745	49,856.2	02/01/2018	1	49,856.2	49,856.2	01/22/2018	02/01/2018	Parent Reopening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018",
						"-12	8006252	100000	H-200	Position	FB	true	Goldman Sachs	146.1087	75	10,958.15	02/01/2018	1	10,958.15	10,958.15	01/22/2018	02/01/2018	Spinoff Opening from 10 to 1 FB Stock Spinoff for WM on 02/01/2018"
				).execute();
	}


	@Test
	public void testSpinoffTwoLots_CashInLieu() {
		FractionalShares fractionalShares = FractionalShares.CASH_IN_LIEU;

		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_SPINOFF);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(InvestmentTestObjectFactory.newInvestmentSecurity(100, "DWDP", InvestmentType.STOCKS), eventType, "06/01/2019");
		event.setAdditionalSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(110, "CTVA", InvestmentType.STOCKS));
		event.setBeforeEventValue(new BigDecimal("3"));
		event.setAfterEventValue(new BigDecimal("1"));
		event.setAdditionalEventValue(new BigDecimal("0.705439"));
		event.setEventDescription(fractionalShares.getName());

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_NEW, this.spinoffSecurity, fractionalShares);

		AccountingTransaction[] transactions = new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(140000).price("33.5734278").on("12/13/2017").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(125000).price("26.3199291").on("05/02/2019").build()
		};

		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailSpinoffPopulator();

		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, event), populator, transactions);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						this.accountingSpinoffEventBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						this.accountingRealizedGainLossBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Position	DWDP	false	Goldman Sachs	33.5734278	-140,000	-4,700,279.89	06/01/2019	1	-4,700,279.89	-4,700,279.89	12/13/2017	06/01/2019	Parent Closing from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-11	1283735	100000	H-200	Position	DWDP	true	Goldman Sachs	23.68400536	140,000	3,315,760.75	06/01/2019	1	3,315,760.75	3,315,760.75	12/13/2017	06/01/2019	Parent Reopening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-12	1283735	100000	H-200	Position	CTVA	true	Goldman Sachs	29.6682673	46,666.6666666666	1,384,519.14	06/01/2019	1	1,384,519.14	1,384,519.14	12/13/2017	06/01/2019	Spinoff Opening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-14	-18	100000	H-200	Cash	USD	true	Goldman Sachs			64.36	06/01/2019	1	64.36	0	12/13/2017	06/01/2019	Cash proceeds from close of CTVA",
						"-15	1283736	100000	H-200	Position	DWDP	false	Goldman Sachs	26.3199291	-125,000	-3,289,991.14	06/01/2019	1	-3,289,991.14	-3,289,991.14	05/02/2019	06/01/2019	Parent Closing from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-16	1283736	100000	H-200	Position	DWDP	true	Goldman Sachs	18.5671045	125,000	2,320,888.06	06/01/2019	1	2,320,888.06	2,320,888.06	05/02/2019	06/01/2019	Parent Reopening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-17	1283736	100000	H-200	Position	CTVA	true	Goldman Sachs	23.258474	41,666.6666666667	969,103.08	06/01/2019	1	969,103.08	969,103.08	05/02/2019	06/01/2019	Spinoff Opening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-18	-12	100000	H-200	Position	CTVA	false	Goldman Sachs	193.09	-0.3333333333	-9.89	06/01/2019	1	-9.89	-9.89	12/13/2017	06/01/2019	Fractional Shares Closing from rounding from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"-19	-18	100000	H-200	Realized Gain / Loss	CTVA	false	Goldman Sachs	193.09	0.3333333333	-54.47	06/01/2019	1	-54.47	0	12/13/2017	06/01/2019	Realized Gain / Loss gain for CTVA"
				).execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal buildEventJournal(FractionalShares fractionalShares) {
		Date originalDate = DateUtils.toDate("01/22/2018");

		// 10 to 1 FB Stock Spinoff for WM on 02/01/2018
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_SPINOFF);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.parentSecurity, eventType, "02/01/2018");
		event.setAdditionalSecurity(this.spinoffSecurity);
		event.setBeforeEventValue(new BigDecimal("10"));
		event.setAfterEventValue(new BigDecimal("1"));
		event.setAdditionalEventValue(new BigDecimal("0.819809815321158"));

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_NEW, this.spinoffSecurity, fractionalShares);

		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailSpinoffPopulator();

		// 745 shares at $81.63 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(8006252, event.getSecurity()).qty(745).price("81.63").on(originalDate).build();
		// opening lot should reference original parent but not closing lot
		tran.setParentTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11111, event.getSecurity()).qty(1).price("11").on(originalDate).build());

		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(payout, event), populator, tran);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}
}
