package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailScripDividendPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailSpinoffPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockDividendPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockSplitPopulator;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


/**
 * <code>AccountingEventJournalDetailRounderTestExecutor</code> is an abstract class that can be extended to test security event rounding.
 *
 * @author nickk
 */
public abstract class AccountingEventJournalDetailRounderTestExecutor extends BaseTestExecutor<AccountingEventJournalDetail> {

	protected final AccountingEventJournalDetailPopulator detailPopulator;
	protected final AccountingEventJournalDetailRounder detailRounder;
	protected AccountingEventJournal eventJournal;
	protected FractionalShares fractionalShares;


	private AccountingEventJournalDetailRounderTestExecutor(AccountingEventJournalDetailPopulator detailPopulator, AccountingEventJournalDetailRounder detailRounder) {
		this.detailPopulator = detailPopulator;
		this.detailRounder = detailRounder;
	}


	public AccountingEventJournalDetailRounderTestExecutor withEventJournal(AccountingEventJournal eventJournal) {
		this.eventJournal = eventJournal;
		return this;
	}


	public AccountingEventJournalDetailRounderTestExecutor withFractionalShares(FractionalShares fractionalShares) {
		this.fractionalShares = fractionalShares;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Journal Detail";
	}


	@Override
	protected List<AccountingEventJournalDetail> executeTest() {
		AccountingEventJournal journal = this.eventJournal;

		List<AccountingEventJournalTestObjectFactory.AccountingEventJournalDetailPayoutAggregate> aggregateDetailList = AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(journal, this.detailPopulator, this.detailRounder);

		if (this.fractionalShares == null) {
			this.fractionalShares = FractionalShares.DEFAULT;
		}

		CollectionUtils.getStream(aggregateDetailList).forEach(payoutAggregateDetail -> {
			AccountingEventJournalDetail aggregateDetail = payoutAggregateDetail.getAggregateDetail();
			// All values are compared to (DataTypes.QUANTITY.getPrecision() -1) to avoid differences of 1 on the tenth decimal digit.
			int roundingPrecision = DataTypes.QUANTITY.getPrecision() - 1;
			BigDecimal aggregateNewQuantity = MathUtils.round(aggregateDetail.getAdditionalAmount(), roundingPrecision);
			BigDecimal aggregateDetailOpenQuantity = MathUtils.round(CoreMathUtils.sumProperty(payoutAggregateDetail.getDetailList(), AccountingEventJournalDetail::getAdditionalAmount), roundingPrecision);
			Assertions.assertEquals(aggregateNewQuantity, aggregateDetailOpenQuantity, "Opening quantities (new) differ");

			BigDecimal aggregateNewQuantityFraction = aggregateDetail.forFractionToClose().getFractionToClose();
			if (!MathUtils.isNullOrZero(aggregateNewQuantityFraction)) {
				BigDecimal aggregateNewQuantityFractionRounded = MathUtils.round(this.fractionalShares.round(aggregateNewQuantityFraction), roundingPrecision);
				BigDecimal aggregateDetailFractionToClose = CoreMathUtils.sumProperty(payoutAggregateDetail.getDetailList(), detail -> detail.forFractionToClose().getFractionToClose());
				BigDecimal aggregateDetailFractionToCloseRounded = MathUtils.round(this.fractionalShares.round(aggregateDetailFractionToClose), roundingPrecision);
				Assertions.assertEquals(aggregateNewQuantityFractionRounded, aggregateDetailFractionToCloseRounded, "Fractional shares differ (" + this.fractionalShares + ")");
			}
		});

		return journal.getDetailList();
	}


	@Override
	protected String[] getStringValuesForResultEntity(AccountingEventJournalDetail detail) {
		return Arrays.asList(detail.toStringFormatted(false)).toArray(new String[0]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * <code>StockSplitDetailRounderTestExecutor</code> rounder test executor for stock splits security events
	 */
	public static class StockSplitDetailRounderTestExecutor extends AccountingEventJournalDetailRounderTestExecutor {

		private StockSplitDetailRounderTestExecutor(AccountingEventJournalDetailRounder detailRounder) {
			super(new AccountingEventJournalDetailStockSplitPopulator(), detailRounder);
		}


		public static StockSplitDetailRounderTestExecutor of(AccountingEventJournalDetailRounder detailRounder) {
			return new StockSplitDetailRounderTestExecutor(detailRounder);
		}
	}

	/**
	 * <code>StockDividendDetailRounderTestExecutor</code> rounder test executor for stock dividend security events
	 */
	public static class StockDividendDetailRounderTestExecutor extends AccountingEventJournalDetailRounderTestExecutor {

		private StockDividendDetailRounderTestExecutor(AccountingEventJournalDetailRounder detailRounder) {
			super(new AccountingEventJournalDetailStockDividendPopulator(), detailRounder);
		}


		public static StockDividendDetailRounderTestExecutor of(AccountingEventJournalDetailRounder detailRounder) {
			return new StockDividendDetailRounderTestExecutor(detailRounder);
		}
	}


	/**
	 * <code>SpinoffDetailRounderTestExecutor</code> rounder test executor for spinoff security events
	 */
	public static class SpinoffDetailRounderTestExecutor extends AccountingEventJournalDetailRounderTestExecutor {

		private SpinoffDetailRounderTestExecutor(AccountingEventJournalDetailRounder detailRounder) {
			super(new AccountingEventJournalDetailSpinoffPopulator(), detailRounder);
		}


		public static SpinoffDetailRounderTestExecutor of(AccountingEventJournalDetailRounder detailRounder) {
			return new SpinoffDetailRounderTestExecutor(detailRounder);
		}
	}


	/**
	 * <code>ScripDetailRounderTestExecutor</code> rounder test executor for spinoff security events
	 */
	public static class ScripDetailRounderTestExecutor extends AccountingEventJournalDetailRounderTestExecutor {

		private ScripDetailRounderTestExecutor(AccountingEventJournalDetailRounder detailRounder) {
			super(new AccountingEventJournalDetailScripDividendPopulator(), detailRounder);
		}


		public static ScripDetailRounderTestExecutor of(InvestmentCalculator investmentCalculator, AccountingEventJournalDetailRounder detailRounder) {
			ScripDetailRounderTestExecutor executor = new ScripDetailRounderTestExecutor(detailRounder);
			((AccountingEventJournalDetailScripDividendPopulator) executor.detailPopulator).setInvestmentCalculator(investmentCalculator);
			return executor;
		}
	}
}
