package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.AccountingInMemoryDatabaseContext;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.gl.booking.AccountingBookingInMemoryDBTestExecutor;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


@AccountingInMemoryDatabaseContext
public class AccountingCollateralSecurityMaturityBookingRuleTests extends BaseInMemoryDatabaseTests {

	@Resource
	private AccountingBookingService<AccountingEventJournal> accountingBookingService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUser(userTemplate.getId());
		if (user == null) {
			this.securityUserService.saveSecurityUser(userTemplate);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCollateralSecurityMaturity() {
		// test that the transfer with a rounding issue can be booked
		AccountingEventJournal journal = this.accountingEventJournalService.getAccountingEventJournal(201138);
		AccountingBookingInMemoryDBTestExecutor.newTestForEntity(AccountingEventJournalBookingProcessor.JOURNAL_NAME, journal, this.accountingBookingService)
				.withTransactionFilter(detail -> "768-70437-16".equals(detail.getHoldingInvestmentAccount().getNumber()))
				.withExpectedResults(
						"57	7438905	112950	768-70437-16	Position Collateral	912796MF3	100	-708,000	-702,470.25	10/16/2017	1	-702,470.25	-702,470.25	09/18/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"58	57	112950	768-70437-16	Cash Collateral	USD			708,000	10/16/2017	1	708,000	0	09/18/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"59	7438906	112950	768-70437-16	Position Collateral Payable	912796MF3	100	708,000	702,470.25	10/16/2017	1	702,470.25	702,470.25	09/18/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"60	7438906	112950	768-70437-16	Cash Collateral Payable	USD			-708,000	10/16/2017	1	-708,000	0	09/18/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"63	7458309	112950	768-70437-16	Position Collateral	912796MF3	100	-481,000	-475,229.52	10/16/2017	1	-475,229.52	-475,229.52	09/21/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"64	63	112950	768-70437-16	Cash Collateral	USD			481,000	10/16/2017	1	481,000	0	09/21/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"65	7458310	112950	768-70437-16	Position Collateral Payable	912796MF3	100	481,000	475,229.52	10/16/2017	1	475,229.52	475,229.52	09/21/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"66	7458310	112950	768-70437-16	Cash Collateral Payable	USD			-481,000	10/16/2017	1	-481,000	0	09/21/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"69	7468505	112950	768-70437-16	Position Collateral	912796MF3	100	-10,000	-9,880.03	10/16/2017	1	-9,880.03	-9,880.03	09/25/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"70	69	112950	768-70437-16	Cash Collateral	USD			10,000	10/16/2017	1	10,000	0	09/25/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"71	7468506	112950	768-70437-16	Position Collateral Payable	912796MF3	100	10,000	9,880.03	10/16/2017	1	9,880.03	9,880.03	09/25/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"72	7468506	112950	768-70437-16	Cash Collateral Payable	USD			-10,000	10/16/2017	1	-10,000	0	09/25/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"75	7468509	112950	768-70437-16	Position Collateral	912796MF3	100	-713,000	-704,446.26	10/16/2017	1	-704,446.26	-704,446.26	09/25/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"76	75	112950	768-70437-16	Cash Collateral	USD			713,000	10/16/2017	1	713,000	0	09/25/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"77	7468510	112950	768-70437-16	Position Collateral Payable	912796MF3	100	713,000	704,446.26	10/16/2017	1	704,446.26	704,446.26	09/25/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"78	7468510	112950	768-70437-16	Cash Collateral Payable	USD			-713,000	10/16/2017	1	-713,000	0	09/25/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"81	7474347	112950	768-70437-16	Position Collateral	912796MF3	100	-21,000	-20,748.07	10/16/2017	1	-20,748.07	-20,748.07	09/26/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"82	81	112950	768-70437-16	Cash Collateral	USD			21,000	10/16/2017	1	21,000	0	09/26/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"83	7474348	112950	768-70437-16	Position Collateral Payable	912796MF3	100	21,000	20,748.07	10/16/2017	1	20,748.07	20,748.07	09/26/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"84	7474348	112950	768-70437-16	Cash Collateral Payable	USD			-21,000	10/16/2017	1	-21,000	0	09/26/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"87	7474351	112950	768-70437-16	Position Collateral	912796MF3	100	-163,000	-161,044.52	10/16/2017	1	-161,044.52	-161,044.52	09/26/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"88	87	112950	768-70437-16	Cash Collateral	USD			163,000	10/16/2017	1	163,000	0	09/26/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"89	7474352	112950	768-70437-16	Position Collateral Payable	912796MF3	100	163,000	161,044.52	10/16/2017	1	161,044.52	161,044.52	09/26/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"90	7474352	112950	768-70437-16	Cash Collateral Payable	USD			-163,000	10/16/2017	1	-163,000	0	09/26/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"117	7538213	112950	768-70437-16	Position Collateral	912796MF3	100	-729,000	-720,254.31	10/16/2017	1	-720,254.31	-720,254.31	10/05/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"118	117	112950	768-70437-16	Cash Collateral	USD			729,000	10/16/2017	1	729,000	0	10/05/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"119	7538214	112950	768-70437-16	Position Collateral Payable	912796MF3	100	729,000	720,254.31	10/16/2017	1	720,254.31	720,254.31	10/05/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"120	7538214	112950	768-70437-16	Cash Collateral Payable	USD			-729,000	10/16/2017	1	-729,000	0	10/05/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"123	7558685	112950	768-70437-16	Position Collateral	912796MF3	100	-714,000	-705,434.26	10/16/2017	1	-705,434.26	-705,434.26	10/09/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"124	123	112950	768-70437-16	Cash Collateral	USD			714,000	10/16/2017	1	714,000	0	10/09/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"125	7558686	112950	768-70437-16	Position Collateral Payable	912796MF3	100	714,000	705,434.26	10/16/2017	1	705,434.26	705,434.26	10/09/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"126	7558686	112950	768-70437-16	Cash Collateral Payable	USD			-714,000	10/16/2017	1	-714,000	0	10/09/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"129	7561141	112950	768-70437-16	Position Collateral	912796MF3	100	-728,000	-719,266.31	10/16/2017	1	-719,266.31	-719,266.31	10/10/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"130	129	112950	768-70437-16	Cash Collateral	USD			728,000	10/16/2017	1	728,000	0	10/10/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"131	7561142	112950	768-70437-16	Position Collateral Payable	912796MF3	100	728,000	719,266.31	10/16/2017	1	719,266.31	719,266.31	10/10/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"132	7561142	112950	768-70437-16	Cash Collateral Payable	USD			-728,000	10/16/2017	1	-728,000	0	10/10/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"135	7561145	112950	768-70437-16	Position Collateral	912796MF3	100	-3,000	-2,964.01	10/16/2017	1	-2,964.01	-2,964.01	10/10/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"136	135	112950	768-70437-16	Cash Collateral	USD			3,000	10/16/2017	1	3,000	0	10/10/2017	10/16/2017	Cash Collateral proceeds from Security Maturity of 912796MF3",
						"137	7561146	112950	768-70437-16	Position Collateral Payable	912796MF3	100	3,000	2,964.01	10/16/2017	1	2,964.01	2,964.01	10/10/2017	10/16/2017	Security Maturity for 912796MF3 on 10/16/2017",
						"138	7561146	112950	768-70437-16	Cash Collateral Payable	USD			-3,000	10/16/2017	1	-3,000	0	10/10/2017	10/16/2017	Cash Collateral Payable expense from Security Maturity of 912796MF3",
						"167	57	112950	768-70437-16	Interest Income	912796MF3	100	708,000	-5,529.75	10/16/2017	1	-5,529.75	0	09/18/2017	10/16/2017	Interest Income gain for 912796MF3",
						"168	59	112950	768-70437-16	Interest Income	912796MF3	100	708,000	5,529.75	10/16/2017	1	5,529.75	0	09/18/2017	10/16/2017	Interest Income loss for 912796MF3",
						"170	63	112950	768-70437-16	Interest Income	912796MF3	100	481,000	-5,770.48	10/16/2017	1	-5,770.48	0	09/21/2017	10/16/2017	Interest Income gain for 912796MF3",
						"171	65	112950	768-70437-16	Interest Income	912796MF3	100	481,000	5,770.48	10/16/2017	1	5,770.48	0	09/21/2017	10/16/2017	Interest Income loss for 912796MF3",
						"173	69	112950	768-70437-16	Interest Income	912796MF3	100	10,000	-119.97	10/16/2017	1	-119.97	0	09/25/2017	10/16/2017	Interest Income gain for 912796MF3",
						"174	71	112950	768-70437-16	Interest Income	912796MF3	100	10,000	119.97	10/16/2017	1	119.97	0	09/25/2017	10/16/2017	Interest Income loss for 912796MF3",
						"176	75	112950	768-70437-16	Interest Income	912796MF3	100	713,000	-8,553.74	10/16/2017	1	-8,553.74	0	09/25/2017	10/16/2017	Interest Income gain for 912796MF3",
						"177	77	112950	768-70437-16	Interest Income	912796MF3	100	713,000	8,553.74	10/16/2017	1	8,553.74	0	09/25/2017	10/16/2017	Interest Income loss for 912796MF3",
						"179	81	112950	768-70437-16	Interest Income	912796MF3	100	21,000	-251.93	10/16/2017	1	-251.93	0	09/26/2017	10/16/2017	Interest Income gain for 912796MF3",
						"180	83	112950	768-70437-16	Interest Income	912796MF3	100	21,000	251.93	10/16/2017	1	251.93	0	09/26/2017	10/16/2017	Interest Income loss for 912796MF3",
						"182	87	112950	768-70437-16	Interest Income	912796MF3	100	163,000	-1,955.48	10/16/2017	1	-1,955.48	0	09/26/2017	10/16/2017	Interest Income gain for 912796MF3",
						"183	89	112950	768-70437-16	Interest Income	912796MF3	100	163,000	1,955.48	10/16/2017	1	1,955.48	0	09/26/2017	10/16/2017	Interest Income loss for 912796MF3",
						"197	117	112950	768-70437-16	Interest Income	912796MF3	100	729,000	-8,745.69	10/16/2017	1	-8,745.69	0	10/05/2017	10/16/2017	Interest Income gain for 912796MF3",
						"198	119	112950	768-70437-16	Interest Income	912796MF3	100	729,000	8,745.69	10/16/2017	1	8,745.69	0	10/05/2017	10/16/2017	Interest Income loss for 912796MF3",
						"200	123	112950	768-70437-16	Interest Income	912796MF3	100	714,000	-8,565.74	10/16/2017	1	-8,565.74	0	10/09/2017	10/16/2017	Interest Income gain for 912796MF3",
						"201	125	112950	768-70437-16	Interest Income	912796MF3	100	714,000	8,565.74	10/16/2017	1	8,565.74	0	10/09/2017	10/16/2017	Interest Income loss for 912796MF3",
						"203	129	112950	768-70437-16	Interest Income	912796MF3	100	728,000	-8,733.69	10/16/2017	1	-8,733.69	0	10/10/2017	10/16/2017	Interest Income gain for 912796MF3",
						"204	131	112950	768-70437-16	Interest Income	912796MF3	100	728,000	8,733.69	10/16/2017	1	8,733.69	0	10/10/2017	10/16/2017	Interest Income loss for 912796MF3",
						"206	135	112950	768-70437-16	Interest Income	912796MF3	100	3,000	-35.99	10/16/2017	1	-35.99	0	10/10/2017	10/16/2017	Interest Income gain for 912796MF3",
						"207	137	112950	768-70437-16	Interest Income	912796MF3	100	3,000	35.99	10/16/2017	1	35.99	0	10/10/2017	10/16/2017	Interest Income loss for 912796MF3"
				).execute();
	}
}
