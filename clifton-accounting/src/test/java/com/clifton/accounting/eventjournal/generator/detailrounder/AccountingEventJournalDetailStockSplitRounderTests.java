package com.clifton.accounting.eventjournal.generator.detailrounder;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockSplitPopulator;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * <code>AccountingEventJournalDetailStockSplitRounderTests</code> tests lot rounding for various stock split scenarios.
 *
 * @author nickk
 */
public class AccountingEventJournalDetailStockSplitRounderTests {

	private final static InvestmentSecurity DWDP_STOCK = InvestmentTestObjectFactory.newInvestmentSecurity(100, "DWDP", InvestmentType.STOCKS);

	private final static String[] NO_FRACTION_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	232	7,827.54	33.7394	101.2182	77.3333333333	7,827.54	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666668	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	230	7,760.06	33.7394	101.2182	76.3333333334	7,760.06	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666667	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] ONE_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	230	7,760.06	33.7394	101.2182	77.3333333334	7,760.06	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666667	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] ONE_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.3333333333	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	230	7,760.06	33.7394	101.2182	76.6666666667	7,760.06	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666667	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] ONE_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.0476190476	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.0476190476	null",
			"3	null	100000	H-200	Position	04/10/2019	1	230	7,760.06	33.7394	101.2182	76.6666666667	7,760.06	0.0952380952	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666667	108,168.59	0.0952380952	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0.0476190476	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] TWO_THIRD_PROPORTIONAL_ROUND_DOWN_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	231	7,793.8	33.7394	101.2182	77	7,793.8	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.0000000001	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	231	7,793.8	33.7394	101.2182	77	7,793.8	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,069.0000000001	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] TWO_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.6666666667	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0	null",
			"3	null	100000	H-200	Position	04/10/2019	1	231	7,793.8	33.7394	101.2182	77	7,793.8	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666668	108,168.59	0	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	private final static String[] TWO_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS = new String[]{
			"1	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.1333333333	null",
			"2	null	100000	H-200	Position	04/10/2019	1	64	2,159.32	33.7394	101.218	21.3333333333	2,159.32	0.1333333333	null",
			"3	null	100000	H-200	Position	04/10/2019	1	231	7,793.8	33.7394	101.2182	77	7,793.8	0	null",
			"4	null	100000	H-200	Position	04/10/2019	1	3,206	108,168.59	33.739423	101.21827	1,068.6666666668	108,168.59	0.2666666667	null",
			"5	null	100000	H-200	Position	01/25/2018	1	154	5,552.29	36.0538	108.1615	51.3333333333	5,552.29	0.1333333333	null",
			"6	null	100000	H-200	Position	02/02/2018	1	360	12,550.84	34.86344	104.5903	120	12,550.84	0	null"
	};

	// Change to true to enable result printing for each test.
	private boolean printResults = false;

	///////////////////////////////////////////////////////////////////////////
	// Stock Split
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testNoRounding_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderNoRoundingTest(null, false);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.CASH_IN_LIEU, false);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_DOWN, false);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_UP, false);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_HALF_UP, false);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderNoRoundingTest(null, true);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.CASH_IN_LIEU, true);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_DOWN, true);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_UP, true);
	}


	@Test
	public void testNoRounding_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderNoRoundingTest(FractionalShares.ROUND_HALF_UP, true);
	}


	@Test
	public void testNoRounding_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderNoRoundingTest(null, false);
	}


	@Test
	public void testNoRounding_ProportionalRounder_CashInLieu() {
		executeProportionalRounderNoRoundingTest(FractionalShares.CASH_IN_LIEU, false);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundDown() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_DOWN, false);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundUp() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_UP, false);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_HALF_UP, false);
	}


	@Test
	public void testNoRounding_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderNoRoundingTest(null, true);
	}


	@Test
	public void testNoRounding_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderNoRoundingTest(FractionalShares.CASH_IN_LIEU, true);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_DOWN, true);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_UP, true);
	}


	@Test
	public void testNoRounding_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderNoRoundingTest(FractionalShares.ROUND_HALF_UP, true);
	}


	private void executeFirstDetailRounderNoRoundingTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeNoRoundingTest(rounder, fractionalShares);
	}


	private void executeProportionalRounderNoRoundingTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.PROPORTIONALLY);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executeNoRoundingTest(rounder, fractionalShares);
	}


	private void executeNoRoundingTest(AccountingEventJournalDetailRounder rounder, FractionalShares fractionalShares) {
		AccountingEventJournalDetailRounderTestExecutor.StockSplitDetailRounderTestExecutor.of(rounder)
				.withEventJournal(get3for1SplitEventJournalWithNoFractionalShares(DWDP_STOCK, fractionalShares))
				.withFractionalShares(fractionalShares)
				.withExpectedResults(NO_FRACTION_EXPECTED_DETAILS)
				.execute();
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderRoundingTest(null, false, false, TWO_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderRoundingTest(FractionalShares.CASH_IN_LIEU, false, false, TWO_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_DOWN, false, false, TWO_THIRD_PROPORTIONAL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_UP, false, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_HALF_UP, false, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(null, true, false, TWO_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.CASH_IN_LIEU, true, false, TWO_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_DOWN, true, false, TWO_THIRD_PROPORTIONAL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_UP, true, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_HALF_UP, true, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderRoundingTest(null, false, false, TWO_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_CashInLieu() {
		executeProportionalRounderRoundingTest(FractionalShares.CASH_IN_LIEU, false, false, TWO_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundDown() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_DOWN, false, false, TWO_THIRD_PROPORTIONAL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundUp() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_UP, false, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_HALF_UP, false, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(null, true, false, TWO_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.CASH_IN_LIEU, true, false, TWO_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_DOWN, true, false, TWO_THIRD_PROPORTIONAL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_UP, true, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testTwoThirdRounding_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_HALF_UP, true, false, TWO_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_NoFractionalShares() {
		executeFirstDetailRounderRoundingTest(null, false, true, ONE_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_CashInLieu() {
		executeFirstDetailRounderRoundingTest(FractionalShares.CASH_IN_LIEU, false, true, ONE_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundDown() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_DOWN, false, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundUp() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_UP, false, true, ONE_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundHalfUp() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_HALF_UP, false, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(null, true, true, ONE_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_CashInLieu_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.CASH_IN_LIEU, true, true, ONE_THIRD_FIRST_DETAIL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundDown_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_DOWN, true, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_UP, true, true, ONE_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_FirstDetailRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeFirstDetailRounderRoundingTest(FractionalShares.ROUND_HALF_UP, true, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_NoFractionalShares() {
		executeProportionalRounderRoundingTest(null, false, true, ONE_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_CashInLieu() {
		executeProportionalRounderRoundingTest(FractionalShares.CASH_IN_LIEU, false, true, ONE_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundDown() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_DOWN, false, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundUp() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_UP, false, true, ONE_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundHalfUp() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_HALF_UP, false, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_NoFractionalShares_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(null, true, true, ONE_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_CashInLieu_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.CASH_IN_LIEU, true, true, ONE_THIRD_PROPORTIONAL_CASH_IN_LIEU_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundDown_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_DOWN, true, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundUp_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_UP, true, true, ONE_THIRD_PROPORTIONAL_ROUND_UP_EXPECTED_DETAILS);
	}


	@Test
	public void testOneThirdRounding_ProportionalRounder_RoundHalfUp_WithCostBasisAdjustment() {
		executeProportionalRounderRoundingTest(FractionalShares.ROUND_HALF_UP, true, true, ONE_THIRD_FIRST_DETAIL_ROUND_DOWN_EXPECTED_DETAILS);
	}


	private void executeFirstDetailRounderRoundingTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, boolean oneThird, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executingRoundingTest(rounder, fractionalShares, expectedDetails, oneThird);
	}


	private void executeProportionalRounderRoundingTest(FractionalShares fractionalShares, boolean adjustOpeningCostBasis, boolean oneThird, String[] expectedDetails) {
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.PROPORTIONALLY);
		rounder.setAdjustOpeningCostBasis(adjustOpeningCostBasis);
		executingRoundingTest(rounder, fractionalShares, expectedDetails, oneThird);
	}


	private void executingRoundingTest(AccountingEventJournalDetailRounder rounder, FractionalShares fractionalShares, String[] expectedDetails, boolean oneThird) {
		AccountingEventJournalDetailRounderTestExecutor.StockSplitDetailRounderTestExecutor.of(rounder)
				.withEventJournal(get3for1SplitEventJournalWithFractionalShares(DWDP_STOCK, fractionalShares, oneThird))
				.withFractionalShares(fractionalShares)
				.withExpectedResults(expectedDetails)
				.execute();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal get3for1SplitEventJournalWithNoFractionalShares(InvestmentSecurity security, FractionalShares fractionalShares) {
		EventData eventData = get3for1SecurityEvent(security, fractionalShares);
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, new AccountingEventJournalDetailStockSplitPopulator(), getTransactionsWithNoFractionalShares(eventData.getEvent()));
	}


	private AccountingEventJournal get3for1SplitEventJournalWithFractionalShares(InvestmentSecurity security, FractionalShares fractionalShares, boolean oneThird) {
		EventData eventData = get3for1SecurityEvent(security, fractionalShares);
		return AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, new AccountingEventJournalDetailStockSplitPopulator(), getTransactionsWithFractionalShares(eventData.getEvent(), oneThird));
	}


	private EventData get3for1SecurityEvent(InvestmentSecurity security, FractionalShares fractionalShares) {
		InvestmentSecurityEventType eventType = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_SPLIT);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(security, eventType, "06/03/2019");
		event.setBeforeEventValue(BigDecimal.valueOf(3));
		event.setAfterEventValue(BigDecimal.ONE);

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_STOCK_SPLIT, fractionalShares);

		return EventData.forEventPayoutOrEvent(payout, event);
	}


	private AccountingTransaction[] getTransactionsWithNoFractionalShares(InvestmentSecurityEvent event) {
		return new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283737, event.getSecurity()).qty(232).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283738, event.getSecurity()).qty(3206).price("33.739423").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283739, event.getSecurity()).qty(154).price("36.0538").on("01/25/2018").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283740, event.getSecurity()).qty(360).price("34.86344").on("02/02/2018").build()
		};
	}


	private AccountingTransaction[] getTransactionsWithFractionalShares(InvestmentSecurityEvent event, boolean oneThird) {
		return new AccountingTransaction[]{
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283736, event.getSecurity()).qty(64).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283737, event.getSecurity()).qty(oneThird ? 230 : 231).price("33.7394").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283738, event.getSecurity()).qty(3206).price("33.739423").on("04/10/2019").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283739, event.getSecurity()).qty(154).price("36.0538").on("01/25/2018").build(),
				AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283740, event.getSecurity()).qty(360).price("34.86344").on("02/02/2018").build()
		};
	}
}
