package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class AccountingAccrualReversalEventBookingRuleTests extends BaseEventJournalBookingRuleTests {

	@Test
	public void testCashDividendAccrualReversal_Domestic() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualReversalEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	USD			-300	01/01/2011	1	-300	0	01/01/2011	01/01/2011	Reverse accrual of Cash Dividend Payment for IBM from 09/19/2010",
						"-11	12345	100000	H-200	Cash	USD			300	01/01/2011	1	300	0	01/01/2011	01/01/2011	3 Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendAccrualReversal_ForeignPayout() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal_PayoutGBP())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualReversalEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	GBP			-300	01/03/2011	1.3	-390	0	01/03/2011	01/03/2011	Reverse accrual of Cash Dividend Payment for IBM from 09/19/2010",
						"-11	12345	100000	H-200	Currency	GBP			300	01/03/2011	1.3	390	0	01/03/2011	01/03/2011	3 GBP from Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_InForeignCurrency() {
		AccountingEventJournal eventJournal = newDividendPaymentJournal();
		eventJournal.getSecurityEvent().setAdditionalSecurity(InvestmentSecurityBuilder.newGBP());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.2"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentAccrualReversalEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Receivable	GBP			-300	01/01/2011	1.2	-360	0	01/01/2011	01/01/2011	Reverse accrual of Cash Dividend Payment for IBM from 09/19/2010",
						"-11	12345	100000	H-200	Currency	GBP			300	01/01/2011	1.2	360	0	01/01/2011	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testForeignAccrualReversal_NoCurrencySplitting() {
		// setup event journal with one detail
		AccountingEventJournal eventJournal = new AccountingEventJournal();
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		InvestmentSecurityEventType cashCouponPayment = InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
		securityEvent.setType(cashCouponPayment);
		securityEvent.setAccrualStartDate(DateUtils.toDate("11/22/2011"));
		securityEvent.setAccrualEndDate(DateUtils.toDate("05/22/2012"));
		securityEvent.setEventDate(DateUtils.toDate("05/22/2012"));
		securityEvent.setExDate(DateUtils.toDate("05/14/2012"));
		securityEvent.setBeforeAndAfterEventValue(new BigDecimal("0.625"));
		securityEvent.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17578, "GB00B3MYD345", InvestmentType.BONDS, "GBP"));
		eventJournal.setSecurityEvent(securityEvent);
		eventJournal.setJournalType(new AccountingEventJournalType());
		eventJournal.getJournalType().setEventType(cashCouponPayment);
		eventJournal.getJournalType().setDebitAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_INTEREST_RECEIVABLE));
		eventJournal.getJournalType().setCreditAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_INCOME));
		eventJournal.getJournalType().setAccrualReversalAccount(this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_CASH));

		AccountingEventJournalDetail eventDetail = new AccountingEventJournalDetail();
		eventDetail.setAffectedQuantity(new BigDecimal("95000"));
		eventDetail.setTransactionAmount(new BigDecimal("336.06"));
		eventDetail.setExchangeRateToBase(new BigDecimal("1")); // Local to Settlement Currency
		eventDetail.setAccountingTransaction(AccountingTransactionBuilder.newTransactionForIdAndSecurity(1730559L, securityEvent.getSecurity())
				.qty(95000).price("118.258").exchangeRateToBase("1.58695").on(DateUtils.toDate("03/06/2012")).build());
		eventJournal.addDetail(eventDetail);

		BaseEventJournalBookingRule bookingRule = newEventJournalBookingRule(AccountingPaymentAccrualReversalEventBookingRule.class);
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(securityEvent.getSecurity().getInstrument().getTradingCurrency(), eventDetail.getAccountingTransaction().getClientInvestmentAccount().getBaseCurrency(), new BigDecimal("1.6097")));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-10	1730559	100000	H-200	Interest Receivable	GBP			-336.06	05/22/2012	1.6097	-540.96	0	05/22/2012	05/22/2012	Reverse accrual of Cash Coupon Payment for GB00B3MYD345 from 05/13/2012",
						"-11	1730559	100000	H-200	Currency	GBP			336.06	05/22/2012	1.6097	540.96	0	05/22/2012	05/22/2012	0.625% for 182 days Cash Coupon Payment for GB00B3MYD345 on 05/22/2012"
				).execute();
	}
}
