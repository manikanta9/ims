-- TEST USED CUSTOM DB DATA AND CANNOT BE EXPORTED - MANUALLY UPDATE TEST DATA IN THE DataAdditional.xml SUFFIXED FILE
SELECT TOP 0 NULL
-- SELECT 'AccountingEventJournal' "entityTableName", AccountingEventJournalID "entityId" FROM AccountingEventJournal t WHERE AccountingEventJournalID IN (201138) UNION ALL
-- SELECT 'AccountingEventJournalDetail', AccountingEventJournalDetailID FROM AccountingEventJournalDetail WHERE AccountingEventJournalID IN (201138) UNION ALL
-- SELECT 'AccountingJournalType' AS entityTableName, AccountingJournalTypeID AS entityId FROM AccountingJournalType WHERE JournalTypeName = 'Event Journal'  UNION ALL
-- SELECT 'AccountingJournalStatus' AS entityTableName, AccountingJournalStatusID AS entityId FROM AccountingJournalStatus UNION ALL
-- SELECT 'AccountingAccount', AccountingAccountID FROM AccountingAccount UNION ALL
-- SELECT DISTINCT 'InvestmentAccountRelationship', InvestmentAccountRelationshipID FROM InvestmentAccountRelationship WHERE MainAccountID IN (
-- 	SELECT InvestmentAccountID
-- 	FROM
-- 		(
-- 			SELECT HoldingInvestmentAccountID, ClientInvestmentAccountID
-- 			FROM AccountingEventJournalDetail d
-- 				INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = d.AccountingTransactionID
-- 			WHERE AccountingEventJournalID IN (201138)
-- 		) p
-- 		UNPIVOT (InvestmentAccountID FOR ColumnName IN (HoldingInvestmentAccountID, ClientInvestmentAccountID)
-- 	) AS t
-- )
-- OR RelatedAccountID IN (
-- 	SELECT InvestmentAccountID
-- 	FROM
-- 		(
-- 			SELECT HoldingInvestmentAccountID, ClientInvestmentAccountID
-- 			FROM AccountingEventJournalDetail d
-- 				INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = d.AccountingTransactionID
-- 			WHERE AccountingEventJournalID IN (201138)
-- 		) p
-- 		UNPIVOT (InvestmentAccountID FOR ColumnName IN (HoldingInvestmentAccountID, ClientInvestmentAccountID)
-- 	) AS t
-- )
;
