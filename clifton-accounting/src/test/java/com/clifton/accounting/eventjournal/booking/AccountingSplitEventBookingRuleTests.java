package com.clifton.accounting.eventjournal.booking;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailStockSplitPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailWithFractionToCloseRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingSplitEventBookingRuleTests extends BaseEventJournalBookingRuleTests {

	private static final InvestmentSecurity IBM = InvestmentTestObjectFactory.newInvestmentSecurity(111, "IBM", InvestmentType.STOCKS);


	@Test
	public void testSplitOneLot_Stock() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(null, new BigDecimal("2"), IBM))
				.forBookingRules(
						newEventJournalBookingRule(AccountingSplitEventBookingRule.class)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	12345	100000	H-200	Position	IBM	false	Goldman Sachs	50	-100	-5,000	01/01/2011	1	-5,000	-5,000	06/01/2010	01/01/2011	1 to 2 Stock Split for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Position	IBM	true	Goldman Sachs	25	200	5,000	01/01/2011	1	5,000	5,000	06/01/2010	01/01/2011	1 to 2 Stock Split for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testSplitOneLot_FractionalShares() {
		AccountingSplitEventBookingRule stockSplitBookingRule = newEventJournalBookingRule(AccountingSplitEventBookingRule.class);
		stockSplitBookingRule.setMarketDataRetriever(Mockito.mock(MarketDataRetriever.class));
		Mockito.when(
				stockSplitBookingRule.getMarketDataRetriever().getPrice(ArgumentMatchers.eq(IBM), ArgumentMatchers.any(), ArgumentMatchers.anyBoolean(), ArgumentMatchers.any()))
				.thenReturn(new BigDecimal("120"));

		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.CASH_IN_LIEU, new BigDecimal("2.007"), IBM))
				.forBookingRules(
						stockSplitBookingRule,
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule(),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(true)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	12345	100000	H-200	Position	IBM	false	Goldman Sachs	50	-100	-5,000	01/01/2011	1	-5,000	-5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Position	IBM	true	Goldman Sachs	24.9128	200.7	5,000	01/01/2011	1	5,000	5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011",
						"-13	-14	100000	H-200	Cash	USD	true	Goldman Sachs			84	01/01/2011	1	84	0	06/01/2010	01/01/2011	Cash proceeds from close of IBM",
						"-14	-11	100000	H-200	Position	IBM	false	Goldman Sachs	120	-0.7	-17.44	01/01/2011	1	-17.44	-17.44	06/01/2010	01/01/2011	Fractional Shares Closing from rounding from 1 to 2.007 Stock Split for IBM on 01/01/2011",
						"-15	-14	100000	H-200	Realized Gain / Loss	IBM	false	Goldman Sachs	120	0.7	-66.56	01/01/2011	1	-66.56	0	06/01/2010	01/01/2011	Realized Gain / Loss gain for IBM"
				).execute();
	}


	@Test
	public void testSplitOneLot_RoundDown() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_DOWN, new BigDecimal("2.007"), IBM))
				.forBookingRules(
						newEventJournalBookingRule(AccountingSplitEventBookingRule.class)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	12345	100000	H-200	Position	IBM	false	Goldman Sachs	50	-100	-5,000	01/01/2011	1	-5,000	-5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Position	IBM	true	Goldman Sachs	25	200	5,000	01/01/2011	1	5,000	5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testSplitOneLot_RoundUp() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(FractionalShares.ROUND_UP, new BigDecimal("2.007"), IBM))
				.forBookingRules(
						newEventJournalBookingRule(AccountingSplitEventBookingRule.class)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	12345	100000	H-200	Position	IBM	false	Goldman Sachs	50	-100	-5,000	01/01/2011	1	-5,000	-5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Position	IBM	true	Goldman Sachs	24.8756	201	5,000	01/01/2011	1	5,000	5,000	06/01/2010	01/01/2011	1 to 2.007 Stock Split for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testSplitOneLot_TRS() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(null, new BigDecimal("2"), InvestmentTestObjectFactory.newInvestmentSecurity(222, "TRS IBM", InvestmentType.SWAPS)))
				.forBookingRules(
						newEventJournalBookingRule(AccountingSplitEventBookingRule.class)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	12345	100000	H-200	Position	TRS IBM	false	Goldman Sachs	50	-100	0	01/01/2011	1	0	-500,000	06/01/2010	01/01/2011	1 to 2 Stock Split for TRS IBM on 01/01/2011",
						"-11	12345	100000	H-200	Position	TRS IBM	true	Goldman Sachs	25	200	0	01/01/2011	1	0	500,000	06/01/2010	01/01/2011	1 to 2 Stock Split for TRS IBM on 01/01/2011"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal buildEventJournal(FractionalShares fractionalShares, BigDecimal afterEventValue, InvestmentSecurity security) {
		Date originalDate = DateUtils.toDate("6/1/2010");

		// for each share before split, get afterEventValue shares after split
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(security, InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.STOCK_SPLIT), "1/1/2011");
		event.setBeforeEventValue(BigDecimal.ONE);
		event.setAfterEventValue(afterEventValue);

		InvestmentSecurityEventPayout payout = InvestmentTestObjectFactory.newInvestmentSecurityEventPayout(event, InvestmentSecurityEventPayoutType.SECURITY_STOCK_SPLIT, fractionalShares);

		// 100 shares at $50 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12345, security).qty(100).price("50").on(originalDate).build();

		EventData eventData = EventData.forEventPayoutOrEvent(payout, event);

		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailStockSplitPopulator();
		AccountingEventJournal eventJournal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(eventData, populator, tran);

		// round lot
		AccountingEventJournalDetailWithFractionToCloseRounder rounder = new AccountingEventJournalDetailWithFractionToCloseRounder();
		rounder.setAllocationType(AccountingEventJournalDetailWithFractionToCloseRounder.FractionToCloseAllocationType.FIRST_DETAIL);
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(eventJournal, populator, rounder);

		return eventJournal;
	}
}
