package com.clifton.accounting.eventjournal.booking;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class AccountingPaymentSameDayEventBookingRuleTests extends BaseEventJournalBookingRuleTests {

	@Test
	public void testCashDividendPayment_Domestic() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentSameDayEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	3	100	-300	01/01/2011	1	-300	0	01/01/2011	01/01/2011	3 Cash Dividend Payment for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Cash	USD			300	01/01/2011	1	300	0	01/01/2011	01/01/2011	3 Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_ForeignPayout() {
		AccountingBookingRuleTestExecutor.newTestForEntity(newDividendPaymentJournal_PayoutGBP())
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentSameDayEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	3	100	-390	01/01/2011	1	-390	0	01/01/2011	01/01/2011	3 GBP from Cash Dividend Payment for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Currency	GBP			300	01/01/2011	1.3	390	0	01/01/2011	01/01/2011	3 GBP from Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_InForeignCurrency_DividendSecurityDenominationIsSameAsBaseCurrency() {
		AccountingEventJournal eventJournal = newDividendPaymentJournal();
		eventJournal.getSecurityEvent().setAdditionalSecurity(InvestmentSecurityBuilder.newGBP());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.2"));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						newEventJournalBookingRule(AccountingPaymentSameDayEventBookingRule.class)
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	3	100	-360	01/01/2011	1	-360	0	01/01/2011	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Currency	GBP			300	01/01/2011	1.2	360	0	01/01/2011	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}


	@Test
	public void testCashDividendPayment_InForeignCurrency_DividendSecurityDenominationIsDifferentFromBaseCurrency() {
		AccountingEventJournal eventJournal = newDividendPaymentJournal();

		InvestmentSecurity currencyDenomination = InvestmentSecurityBuilder.newEUR();
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(eventJournal.getDetailList().get(0).getAccountingTransaction());

		eventJournal.getSecurityEvent().getSecurity().getInstrument().setTradingCurrency(currencyDenomination);
		eventJournal.getSecurityEvent().setAdditionalSecurity(InvestmentSecurityBuilder.newGBP());
		eventJournal.getDetailList().get(0).setExchangeRateToBase(new BigDecimal("1.2"));

		AccountingPaymentSameDayEventBookingRule bookingRule = newEventJournalBookingRule(AccountingPaymentSameDayEventBookingRule.class);
		// mock FX lookup from EUR to USD
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(currencyDenomination, baseCurrency, new BigDecimal("0.9")));

		AccountingBookingRuleTestExecutor.newTestForEntity(eventJournal)
				.forBookingRules(
						bookingRule
				)
				.withExpectedResults(
						"-10	12345	100000	H-200	Dividend Income	IBM	3	100	-400	01/01/2011	0.9	-360	0	01/01/2011	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011",
						"-11	12345	100000	H-200	Currency	GBP			300	01/01/2011	1.2	360	0	01/01/2011	01/01/2011	3 GBP Cash Dividend Payment for IBM on 01/01/2011"
				).execute();
	}
}
