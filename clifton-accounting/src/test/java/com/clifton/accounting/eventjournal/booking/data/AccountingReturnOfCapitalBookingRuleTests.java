package com.clifton.accounting.eventjournal.booking.data;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalTestObjectFactory;
import com.clifton.accounting.eventjournal.booking.AccountingReturnOfCapitalBookingRule;
import com.clifton.accounting.eventjournal.booking.BaseEventJournalBookingRuleTests;
import com.clifton.accounting.eventjournal.generator.EventData;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailReturnOfCapitalPopulator;
import com.clifton.accounting.eventjournal.generator.detailrounder.AccountingEventJournalDetailTransactionAmountRounder;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Verifies that the <code>AccountingReturnOfCapitalBookingRule</code> generates the correct event journals
 * given various inputs.
 *
 * @author michaelm
 */
public class AccountingReturnOfCapitalBookingRuleTests extends BaseEventJournalBookingRuleTests {


	private InvestmentSecurity returnOfCapitalSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(100, "AAPL", InvestmentType.STOCKS);

	private AccountingReturnOfCapitalBookingRule accountingReturnOfCapitalBookingRule;
	private AccountingReturnOfCapitalBookingRule accountingReturnOfCapitalAccrualBookingRule;
	private AccountingReturnOfCapitalBookingRule accountingReturnOfCapitalAccrualReversalBookingRule;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		this.accountingReturnOfCapitalBookingRule = newEventJournalBookingRule(AccountingReturnOfCapitalBookingRule.class);

		this.accountingReturnOfCapitalAccrualBookingRule = newEventJournalBookingRule(AccountingReturnOfCapitalBookingRule.class);
		this.accountingReturnOfCapitalAccrualBookingRule.setAccrual(true);

		this.accountingReturnOfCapitalAccrualReversalBookingRule = newEventJournalBookingRule(AccountingReturnOfCapitalBookingRule.class);
		this.accountingReturnOfCapitalAccrualReversalBookingRule.setAccrualReversal(true);

		List<AccountingTransaction> receivableList = CollectionUtils.createList(AccountingTransactionBuilder.newTransactionForIdAndSecurity(999999L, this.returnOfCapitalSecurity).qty(100).price("200").on("04/16/2018").build());
		Mockito.when(
				this.accountingReturnOfCapitalBookingRule.getAccountingTransactionService().getAccountingTransactionList(ArgumentMatchers.any(AccountingTransactionSearchForm.class)))
				.thenReturn(receivableList);
	}


	@Test
	public void testReturnOfCapital_Accrual() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(DateUtils.toDate("4/25/2018")))
				.forBookingRules(
						this.accountingReturnOfCapitalAccrualBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Position	AAPL	false	Goldman Sachs	200.9882833333	-100	-20,098.83	04/17/2018	1	-20,098.83	-20,098.83	06/29/2009	04/27/2018	Parent Closing from 5 Return of Capital for AAPL on 04/27/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	195.9883	100	19,598.83	04/17/2018	1	19,598.83	19,598.83	06/29/2009	04/27/2018	Parent Reopening from 5 Return of Capital for AAPL on 04/27/2018",
						"-12	1283735	100000	H-200	Payment Receivable	USD	true	Goldman Sachs			500	04/17/2018	1	500	0	06/29/2009	04/27/2018	Accrual of Return of Capital for AAPL to be paid on 04/27/2018"
				).execute();
	}


	@Test
	public void testReturnOfCapital_AccrualReversal() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(DateUtils.toDate("4/26/2018")))
				.forBookingRules(
						this.accountingReturnOfCapitalAccrualReversalBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Payment Receivable	USD	false	Goldman Sachs			-500	04/27/2018	1	-500	0	06/29/2009	04/27/2018	Reverse accrual of 5 Return of Capital for AAPL paid on 04/27/2018",
						"-11	1283735	100000	H-200	Cash	USD	true	Goldman Sachs			500	04/27/2018	1	500	0	06/29/2009	04/27/2018	5 Return of Capital for AAPL paid on 04/27/2018"
				).execute();
	}


	@Test
	public void testReturnOfCapital_ExDateAfterEventDate() {
		AccountingBookingRuleTestExecutor.newTestForEntity(buildEventJournal(DateUtils.toDate("4/16/2018")))
				.forBookingRules(
						this.accountingReturnOfCapitalBookingRule
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	1283735	100000	H-200	Position	AAPL	false	Goldman Sachs	200.9882833333	-100	-20,098.83	04/27/2018	1	-20,098.83	-20,098.83	06/29/2009	04/27/2018	Parent Closing from 5 Return of Capital for AAPL on 04/27/2018",
						"-11	1283735	100000	H-200	Position	AAPL	true	Goldman Sachs	195.9883	100	19,598.83	04/27/2018	1	19,598.83	19,598.83	06/29/2009	04/27/2018	Parent Reopening from 5 Return of Capital for AAPL on 04/27/2018",
						"-12	1283735	100000	H-200	Cash	USD	true	Goldman Sachs			500	04/27/2018	1	500	0	06/29/2009	04/27/2018	5 Return of Capital for AAPL paid on 04/27/2018"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingEventJournal buildEventJournal(Date eventDate) {
		Date originalDate = DateUtils.toDate("06/29/2009");
		AccountingEventJournalDetailPopulator populator = new AccountingEventJournalDetailReturnOfCapitalPopulator();
		InvestmentSecurityEvent event = createSecurityEvent(eventDate);

		// 100 shares at $200.9882833333 per share
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1283735, event.getSecurity()).qty(100).price("200.9882833333").on(originalDate).build();
		AccountingEventJournal journal = AccountingEventJournalTestObjectFactory.newAccountingEventJournalForEventWithTransactions(EventData.forEventPayoutOrEvent(createPayout(event), event), populator, tran);

		AccountingEventJournalDetail detail = new AccountingEventJournalDetail();
		detail.setAccountingTransaction(tran);
		detail.setEventPayout(createPayout(event));

		AccountingEventJournalDetailTransactionAmountRounder rounder = new AccountingEventJournalDetailTransactionAmountRounder();
		AccountingEventJournalTestObjectFactory.aggregateAndRoundAccountingEventJournalDetailList(journal, populator, rounder);

		return journal;
	}


	private InvestmentSecurityEvent createSecurityEvent(Date eventDate) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setSecurity(this.returnOfCapitalSecurity);
		event.setBeforeAndAfterEventValue(new BigDecimal("5"));
		event.setExDate(DateUtils.toDate("04/17/2018"));
		event.setEventDate(eventDate);
		event.setEventDescription("ROC: Return of Capital");
		event.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.RETURN_OF_CAPITAL));
		return event;
	}


	private InvestmentSecurityEventPayout createPayout(InvestmentSecurityEvent securityEvent) {
		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(securityEvent);
		payout.setPayoutType(InvestmentTestObjectFactory.newInvestmentSecurityEventPayoutType(InvestmentSecurityEventPayoutType.CURRENCY));
		payout.getPayoutType().setBeforeSameAsAfter(true);
		payout.setPayoutSecurity(InvestmentSecurityBuilder.newUSD());
		payout.copyBeforeAndAfterEventValues(securityEvent);
		payout.setAdditionalPayoutDate(DateUtils.toDate("04/27/2018"));
		return payout;
	}
}
