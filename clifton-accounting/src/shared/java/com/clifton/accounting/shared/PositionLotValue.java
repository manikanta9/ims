package com.clifton.accounting.shared;

import java.math.BigDecimal;


/**
 * Represents current valuation information (current price, fx, accruals) for a {@link PositionLot}.
 *
 * @author vgomelsky
 */
public class PositionLotValue extends PositionLot {

	private BigDecimal price;
	private BigDecimal exchangeRateToBase;


	private BigDecimal notionalLocal;

	private BigDecimal accrual1Local;
	private BigDecimal accrual2Local;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getNotionalLocal() {
		return this.notionalLocal;
	}


	public void setNotionalLocal(BigDecimal notionalLocal) {
		this.notionalLocal = notionalLocal;
	}


	public BigDecimal getAccrual1Local() {
		return this.accrual1Local;
	}


	public void setAccrual1Local(BigDecimal accrual1Local) {
		this.accrual1Local = accrual1Local;
	}


	public BigDecimal getAccrual2Local() {
		return this.accrual2Local;
	}


	public void setAccrual2Local(BigDecimal accrual2Local) {
		this.accrual2Local = accrual2Local;
	}
}
