package com.clifton.accounting.shared;

import com.clifton.business.shared.Company;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.investment.shared.Security;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Represents a Lot in the general ledger for a position that can be open, partially open of fully closed.
 * Includes all attributes related to the opening lot as well as remaining balance.
 * Does not include current valuation information: use {@link PositionLotValue} for that.
 *
 * @author vgomelsky
 */
public class PositionLot implements Serializable {

	private long id;

	private ClientAccount clientAccount;
	private HoldingAccount holdingAccount;
	private Company executingCompany;
	private Security security;
	private GlAccount glAccount;

	private BigDecimal openingQuantity;
	private BigDecimal openingPrice;
	private BigDecimal openingExchangeRateToBase;

	private BigDecimal openingDebitCreditLocal;
	private BigDecimal openingCostBasisLocal;


	private BigDecimal remainingQuantity; // remaining balance for currencies
	private BigDecimal remainingCostBasis;
	private BigDecimal remainingBaseDebitCredit; // important for remaining currency cost in base currency (avoids rounding errors)

	private Date openingTransactionDate;
	private Date openingSettlementDate;
	private Date openingOriginalTransactionDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getId() {
		return this.id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public ClientAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public HoldingAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(HoldingAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public Company getExecutingCompany() {
		return this.executingCompany;
	}


	public void setExecutingCompany(Company executingCompany) {
		this.executingCompany = executingCompany;
	}


	public Security getSecurity() {
		return this.security;
	}


	public void setSecurity(Security security) {
		this.security = security;
	}


	public GlAccount getGlAccount() {
		return this.glAccount;
	}


	public void setGlAccount(GlAccount glAccount) {
		this.glAccount = glAccount;
	}


	public BigDecimal getOpeningQuantity() {
		return this.openingQuantity;
	}


	public void setOpeningQuantity(BigDecimal openingQuantity) {
		this.openingQuantity = openingQuantity;
	}


	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}


	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}


	public BigDecimal getOpeningExchangeRateToBase() {
		return this.openingExchangeRateToBase;
	}


	public void setOpeningExchangeRateToBase(BigDecimal openingExchangeRateToBase) {
		this.openingExchangeRateToBase = openingExchangeRateToBase;
	}


	public BigDecimal getOpeningDebitCreditLocal() {
		return this.openingDebitCreditLocal;
	}


	public void setOpeningDebitCreditLocal(BigDecimal openingDebitCreditLocal) {
		this.openingDebitCreditLocal = openingDebitCreditLocal;
	}


	public BigDecimal getOpeningCostBasisLocal() {
		return this.openingCostBasisLocal;
	}


	public void setOpeningCostBasisLocal(BigDecimal openingCostBasisLocal) {
		this.openingCostBasisLocal = openingCostBasisLocal;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getRemainingCostBasis() {
		return this.remainingCostBasis;
	}


	public void setRemainingCostBasis(BigDecimal remainingCostBasis) {
		this.remainingCostBasis = remainingCostBasis;
	}


	public BigDecimal getRemainingBaseDebitCredit() {
		return this.remainingBaseDebitCredit;
	}


	public void setRemainingBaseDebitCredit(BigDecimal remainingBaseDebitCredit) {
		this.remainingBaseDebitCredit = remainingBaseDebitCredit;
	}


	public Date getOpeningTransactionDate() {
		return this.openingTransactionDate;
	}


	public void setOpeningTransactionDate(Date openingTransactionDate) {
		this.openingTransactionDate = openingTransactionDate;
	}


	public Date getOpeningSettlementDate() {
		return this.openingSettlementDate;
	}


	public void setOpeningSettlementDate(Date openingSettlementDate) {
		this.openingSettlementDate = openingSettlementDate;
	}


	public Date getOpeningOriginalTransactionDate() {
		return this.openingOriginalTransactionDate;
	}


	public void setOpeningOriginalTransactionDate(Date openingOriginalTransactionDate) {
		this.openingOriginalTransactionDate = openingOriginalTransactionDate;
	}
}
