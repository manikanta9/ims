package com.clifton.accounting.shared;

import java.io.Serializable;


/**
 * Represents a General Ledger account which is one of the following types: Asset, Liability, Equity, Revenue, Expense.
 *
 * @author vgomelsky
 */
public class GlAccount implements Serializable {

	private short id;

	private String name;
	/**
	 * The type of this GL Account: Asset, Liability, Equity, Revenue or Expense.
	 */
	private String accountType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public short getId() {
		return this.id;
	}


	public void setId(short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAccountType() {
		return this.accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
}
