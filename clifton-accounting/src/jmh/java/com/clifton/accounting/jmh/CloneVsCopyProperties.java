package com.clifton.accounting.jmh;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.jmh.BaseJmhBenchmark;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;


/**
 * Tests the performance of {@link BeanUtils#cloneBean(IdentityObject, boolean, boolean)} against {@link BeanUtils#copyProperties(Object, Object)}.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_201, Java HotSpot(TM) 64-Bit Server VM, 25.201-b09
 * # VM invoker: C:\Program Files\Java\jdk1.8.0_201\jre\bin\java.exe
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 5 iterations, 10 s each
 * # Measurement: 5 iterations, 10 s each
 * # Timeout: 10 min per iteration
 * # Threads: 1 thread, will synchronize iterations
 * # Benchmark mode: Average time, time/op
 * # Benchmark: com.clifton.accounting.jmh.CloneVsCopyProperties.copyProperties
 * Benchmark                             Mode  Cnt      Score   Error  Units
 * CloneVsCopyProperties.cloneBean       avgt       72493.418          ns/op
 * CloneVsCopyProperties.copyProperties  avgt       70392.766          ns/op
 * </pre>
 * </p>
 * The difference is pretty minimal, but copyProperties is consistently the better of the two in "High Accuracy" testing.
 *
 * @author JasonS
 */
@Warmup(iterations = 0)
@Measurement(iterations = 1)
public class CloneVsCopyProperties extends BaseJmhBenchmark {

	@Benchmark
	public void cloneBean() {
		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(6001, "31288VXX5", InvestmentType.OPTIONS);
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12347L, investmentSecurity)
				.qty(100).price("24.3").on("7/7/2021").build();
		AccountingPosition position = AccountingTestObjectFactory.newAccountingTransaction(tran1);

		AccountingJournalDetailDefinition openingTransaction = BeanUtils.cloneBean(position.getOpeningTransaction(), false, true);
	}


	@Benchmark
	public void copyProperties() {
		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(6002, "31288VXX6", InvestmentType.OPTIONS);
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(12347L, investmentSecurity)
				.qty(100).price("24.3").on("7/7/2021").build();
		AccountingPosition position = AccountingTestObjectFactory.newAccountingTransaction(tran1);

		AccountingJournalDetailDefinition openingTransaction = new AccountingTransaction();
		BeanUtils.copyProperties(position.getOpeningTransaction(), openingTransaction);
	}
}
