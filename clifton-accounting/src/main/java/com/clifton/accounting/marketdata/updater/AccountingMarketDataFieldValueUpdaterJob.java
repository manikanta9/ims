package com.clifton.accounting.marketdata.updater;


import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;
import com.clifton.marketdata.updater.jobs.MarketDataFieldValueUpdaterJob;


/**
 * The <code>AccountingMarketDataFieldValueUpdaterJob</code> class updates/creates MarketDataValues
 * objects based on the specified configuration parameters.
 *
 * @author vgomelsky
 */
public class AccountingMarketDataFieldValueUpdaterJob extends MarketDataFieldValueUpdaterJob {

	private AccountingPositionService accountingPositionService;

	/**
	 * Optionally limit securities to those with active positions on the specified date.
	 * If true, skips securities that we don't currently hold across all accounts.
	 */
	private boolean activePositionsOnly;
	/**
	 * Optionally limit securities to these with active positions in of the client accounts that
	 * are a part of this account group.
	 */
	private Integer clientAccountGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected MarketDataFieldValueUpdaterCommand newMarketDataFieldValueUpdaterCommand() {
		return new AccountingMarketDataFieldValueUpdaterCommand(isActivePositionsOnly(), getClientAccountGroupId(), getAccountingPositionService());
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isActivePositionsOnly() {
		return this.activePositionsOnly;
	}


	public void setActivePositionsOnly(boolean activePositionsOnly) {
		this.activePositionsOnly = activePositionsOnly;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}
}
