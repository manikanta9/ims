package com.clifton.accounting.marketdata.updater;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;

import java.util.Date;
import java.util.List;


/**
 * AccountingMarketDataFieldValueUpdaterCommand class allows additional restrictions that are based on whether there is open position for a given security.
 *
 * @author vgomelsky
 */
public class AccountingMarketDataFieldValueUpdaterCommand extends MarketDataFieldValueUpdaterCommand {

	private final AccountingPositionService accountingPositionService;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * Optionally limit securities to those with active positions on the specified date. If true, skips securities that we don't currently hold across all accounts.
	 */
	private boolean activePositionsOnly;
	/**
	 * Optionally limit securities to these with active positions in of the client accounts that are a part of this account group.
	 */
	private Integer clientAccountGroupId;


	////////////////////////////////////////////////////////////////////////////


	public AccountingMarketDataFieldValueUpdaterCommand(boolean activePositionsOnly, Integer clientAccountGroupId, AccountingPositionService accountingPositionService) {
		this.activePositionsOnly = activePositionsOnly;
		this.clientAccountGroupId = clientAccountGroupId;
		this.accountingPositionService = accountingPositionService;
	}


	@Override
	public boolean isSecuritySkipped(InvestmentSecurity security, Date startDate) {
		if (super.isSecuritySkipped(security, startDate)) {
			return true;
		}

		if (isActivePositionsOnly() || getClientAccountGroupId() != null) {
			Date transactionDate = getStartDate();
			if (transactionDate == null) {
				transactionDate = getEndDate();
			}
			if (transactionDate == null) {
				transactionDate = new Date();
			}
			AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
			command.setInvestmentSecurityId(security.getId());
			command.setClientInvestmentAccountGroupId(getClientAccountGroupId());

			List<AccountingPosition> positions = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
			if (CollectionUtils.isEmpty(positions) && getStartDate() != null && getEndDate() != null && DateUtils.compare(getStartDate(), getEndDate(), false) != 0) {
				// if no positions on startDate also check that there are no on endDate
				command.setTransactionDate(getEndDate());
				positions = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
			}
			return CollectionUtils.isEmpty(positions);
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////


	public boolean isActivePositionsOnly() {
		return this.activePositionsOnly;
	}


	public void setActivePositionsOnly(boolean activePositionsOnly) {
		this.activePositionsOnly = activePositionsOnly;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}
}
