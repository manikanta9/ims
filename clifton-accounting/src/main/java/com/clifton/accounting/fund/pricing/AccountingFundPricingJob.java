package com.clifton.accounting.fund.pricing;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingFundPricingJob</code> class is a batch job that updates Proprietary Fund
 * prices for the specified securities and the specified date range.
 *
 * @author vgomelsky
 */
public class AccountingFundPricingJob implements Task, StatusHolderObjectAware<Status> {

	/**
	 * The numbers of days back from Today (inclusive) to start getting values from
	 */
	private Integer daysBackIncludingTodayFrom;
	/**
	 * The numbers of days back from Today (inclusive) to stop getting values to
	 */
	private Integer daysBackIncludingTodayTo;
	private boolean skipLastWeekend = true;

	private Short investmentSecurityGroupId;

	// field and data source to be used for saved market data values
	private Short marketDataFieldId;
	private Short marketDataSourceId;
	private Short managementFeeAccountingAccountId;

	/**
	 * Market data field used to identify latest official price (usually NAV Price obtained from custodian).
	 */
	private Short basePriceMarketDataFieldId;

	////////////////////////////////////////////////////////////////////////////

	private StatusHolderObject<Status> statusHolder;

	////////////////////////////////////////////////////////////////////////////

	private AccountingAccountService accountingAccountService;
	private AccountingFundPricingCalculator accountingFundPricingCalculator;
	private CalendarBusinessDayService calendarBusinessDayService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataSourceService marketDataSourceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		// lookup parameters used in various calculations to make sure they're valid
		MarketDataField priceField = getMarketDataFieldService().getMarketDataField(getMarketDataFieldId());
		ValidationUtils.assertNotNull(priceField, "Cannot find MarketDataField for id = " + getMarketDataFieldId());
		MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSource(getMarketDataSourceId());
		ValidationUtils.assertNotNull(dataSource, "Cannot find MarketDataSource for id = " + getMarketDataSourceId());
		AccountingAccount managementFeeAccount = getAccountingAccountService().getAccountingAccount(getManagementFeeAccountingAccountId());
		ValidationUtils.assertNotNull(managementFeeAccount, "Cannot find Management Fee GL Account for id = " + getManagementFeeAccountingAccountId());

		// optional NAV Price field
		MarketDataField basePriceField = null;
		if (getBasePriceMarketDataFieldId() != null) {
			basePriceField = getMarketDataFieldService().getMarketDataField(getBasePriceMarketDataFieldId());
			ValidationUtils.assertNotNull(basePriceField, "Cannot find BasePriceMarketDataField for id = " + getBasePriceMarketDataFieldId());
		}

		int recordsUpdated = 0;
		int errorCount = 0;
		Date date = new Date();

		Date startDate = DateUtils.clearTime(addDays(date, -getDaysBackIncludingTodayFrom(), isSkipLastWeekend()));
		Date endDate = DateUtils.getEndOfDay(addDays(date, -getDaysBackIncludingTodayTo(), isSkipLastWeekend()));
		date = startDate;

		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInvestmentGroupId(getInvestmentSecurityGroupId());
		List<FundValuation> fundList = getAccountingFundPricingCalculator().getFundValuationList(searchForm);

		// get a price for each day
		Status status = this.statusHolder.getStatus();
		do {
			// skip valuation on non-business days
			if (getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(date))) {
				for (FundValuation fund : fundList) {
					status.setMessage("Processing Fund " + fund.getInvestmentAccount().getLabel() + " on " + DateUtils.fromDateShort(date) + "; Fund count = " + CollectionUtils.getSize(fundList) + "; Update count = " + recordsUpdated + "; Error count = " + errorCount);
					try {
						fund.reset();
						fund.setValuationDate(date);
						getAccountingFundPricingCalculator().calculateFundValuation(fund, managementFeeAccount, basePriceField);
						for (FundShareClassValuation shareClass : fund.getShareClassValuationList()) {
							if (shareClass.getValuationPrice() != null) { // no fund positions on valuation date
								MarketDataValue result = new MarketDataValue();
								result.setDataSource(dataSource);
								result.setDataField(priceField);
								result.setInvestmentSecurity(shareClass.getInvestmentSecurity());
								result.setMeasureValue(MathUtils.round(shareClass.getValuationPrice(), priceField.getDecimalPrecision()));
								result.setMeasureDate(date);
								if (priceField.isTimeSensitive()) {
									result.setMeasureTime(new Time(0));
								}
								if (getMarketDataFieldService().saveMarketDataValueWithOptions(result, true) != null) {
									recordsUpdated++;
								}
							}
						}
					}
					catch (Throwable e) {
						errorCount++;
						status.addError(DateUtils.fromDateShort(fund.getValuationDate()) + ": " + fund.getInvestmentAccount().getLabel() + ": " + e.getMessage());
					}
				}
			}
			date = DateUtils.addDays(date, 1);
		}
		while (DateUtils.compare(date, endDate, false) < 0);

		status.setMessage("Fund count = " + CollectionUtils.getSize(fundList) + "; Update count = " + recordsUpdated + "; Error count = " + errorCount);
		return status;
	}


	private Date addDays(Date date, int days, boolean excludeLastWeekend) {
		Date result = DateUtils.addDays(date, days);
		if (excludeLastWeekend) {
			int weekDay = DateUtils.getDayOfWeek(result);
			if (weekDay == Calendar.SATURDAY) {
				result = DateUtils.addDays(result, -1);
			}
			else if (weekDay == Calendar.SUNDAY) {
				result = DateUtils.addDays(result, -2);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBackIncludingTodayFrom() {
		return this.daysBackIncludingTodayFrom;
	}


	public void setDaysBackIncludingTodayFrom(Integer daysBackIncludingTodayFrom) {
		this.daysBackIncludingTodayFrom = daysBackIncludingTodayFrom;
	}


	public Integer getDaysBackIncludingTodayTo() {
		return this.daysBackIncludingTodayTo;
	}


	public void setDaysBackIncludingTodayTo(Integer daysBackIncludingTodayTo) {
		this.daysBackIncludingTodayTo = daysBackIncludingTodayTo;
	}


	public boolean isSkipLastWeekend() {
		return this.skipLastWeekend;
	}


	public void setSkipLastWeekend(boolean skipLastWeekend) {
		this.skipLastWeekend = skipLastWeekend;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Short getManagementFeeAccountingAccountId() {
		return this.managementFeeAccountingAccountId;
	}


	public void setManagementFeeAccountingAccountId(Short managementFeeAccountingAccountId) {
		this.managementFeeAccountingAccountId = managementFeeAccountingAccountId;
	}


	public Short getBasePriceMarketDataFieldId() {
		return this.basePriceMarketDataFieldId;
	}


	public void setBasePriceMarketDataFieldId(Short basePriceMarketDataFieldId) {
		this.basePriceMarketDataFieldId = basePriceMarketDataFieldId;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingFundPricingCalculator getAccountingFundPricingCalculator() {
		return this.accountingFundPricingCalculator;
	}


	public void setAccountingFundPricingCalculator(AccountingFundPricingCalculator accountingFundPricingCalculator) {
		this.accountingFundPricingCalculator = accountingFundPricingCalculator;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
