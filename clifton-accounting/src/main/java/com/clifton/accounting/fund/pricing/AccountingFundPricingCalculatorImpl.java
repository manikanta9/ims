package com.clifton.accounting.fund.pricing;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingFundPricingCalculatorImpl</code> class provides basic implementation of the AccountingFundPricingCalculator interface.
 * <p>
 * Price of Share Class A = Total Fund Market Value * Share Class A Portion of the Fund / Number of Share Class A Shares
 * <p>
 * The trick is to find "Share Class A Portion of the Fund" for each share class. The approach is to find all fund activities that
 * may change this allocation (Contributions, Distributions and Management Fee payments) and apply them one activity day at a time
 * while constantly recalculating share class allocations.
 * <p>
 * Alternative approach when NAV Price (official price from custodian) for each share class is available on a given date, then we
 * can calculate share class allocation in the fund by multiplying it by the number of shares in a particular share class and then
 * dividing this by the total value of the fund calculated from NAV Prices and shares.
 * <p>
 * To adjust for contributions and distributions of additional funds:
 * - the amount of contribution/distribution is equal to the notional of corresponding trades that buy/sell Share Class(es)
 * - calculate these by retrieving Position transactions from GL and adding corresponding Gain/Loss transactions to closes
 * - calculate each Share Class market value the day before contribution/distribution (known Share Class allocations and total fund value)
 * - add/remove to each Share Class market value corresponding contribution/distribution
 * - calculate new Share Class allocations
 * <p>
 * To adjust for Management Fee:
 * - calculate fund market value the day before Management Fee was charged
 * - add back full Management Fee charged to the market value of the fund (value before fees)
 * - allocate this market value before Management Fee to each share class using latest Share Class allocation
 * - subtract corresponding Management Fee from each Share Class (market value after management fee)
 * - recalculate allocation of each Share Class after the fee
 *
 * @author vgomelsky
 */
@Component
public class AccountingFundPricingCalculatorImpl implements AccountingFundPricingCalculator {

	private AccountingValuationService accountingValuationService;
	private AccountingPositionHandler accountingPositionHandler;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FundValuation> getFundValuationList(SecuritySearchForm searchForm) {
		List<FundValuation> result = null;

		if (searchForm == null) {
			searchForm = new SecuritySearchForm();
		}
		searchForm.setInvestmentAccountPopulated(true);
		searchForm.setInvestmentType(InvestmentType.FUNDS);
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);

		if (!CollectionUtils.isEmpty(securityList)) {
			result = new ArrayList<>();
			Map<Object, List<InvestmentSecurity>> fundMap = BeanUtils.getBeansMap(securityList, "instrument.investmentAccount.id");
			for (List<InvestmentSecurity> fundSecurities : fundMap.values()) {
				FundValuation fund = new FundValuation();
				fund.setInvestmentAccount(fundSecurities.get(0).getInstrument().getInvestmentAccount());
				result.add(fund);
				for (InvestmentSecurity security : fundSecurities) {
					FundShareClassValuation shareClass = new FundShareClassValuation();
					shareClass.setInvestmentSecurity(security);
					fund.addFundShareClassValuation(shareClass);
				}
			}
		}

		return result;
	}


	@Override
	public void calculateFundValuation(FundValuation fundValuation, AccountingAccount managementFeeAccount, MarketDataField basePriceField) {
		if (basePriceField != null) {
			findAndPopulateCalculationBase(fundValuation, basePriceField);
		}

		// get all fund activity that affects share class allocation: contributions, distributions and management fee (different by share class)
		List<DailyActivity> activityList = getFundActivity(fundValuation, managementFeeAccount);

		// move forward one activity day at a time
		for (DailyActivity activity : CollectionUtils.getIterable(activityList)) {
			// move to new base date
			fundValuation.setBaseDate(activity.activityDate);
			BigDecimal previousDateFundMarketValue = getFundMarketValue(fundValuation.getInvestmentAccount().getId(), DateUtils.addDays(activity.activityDate, -1));
			BigDecimal dateStartFundMarketValue = BigDecimal.ZERO; // previous day +/- today's contributions/distributions
			// calculate each share class market value on base date
			for (FundShareClassValuation shareClass : fundValuation.getShareClassValuationList()) {
				// market value the day before
				shareClass.setBaseMarketValue(MathUtils.multiply(previousDateFundMarketValue, shareClass.getBaseAllocation()));
				DailyShareClassActivity shareClassActivity = getDailyShareClassActivity(activity, shareClass.getInvestmentSecurity());
				if (shareClassActivity != null) {
					// add/subtract activity contributions/distributions
					shareClass.setBaseMarketValue(MathUtils.add(shareClass.getBaseMarketValue(), shareClassActivity.changeInInvestment));
					shareClass.setBaseQuantity(MathUtils.add(shareClass.getBaseQuantity(), shareClassActivity.changeInQuantity));
					// adjust for Management Fees
					shareClass.setBaseMarketValue(MathUtils.subtract(shareClass.getBaseMarketValue(), shareClassActivity.managementFee));
					dateStartFundMarketValue = MathUtils.subtract(dateStartFundMarketValue, shareClassActivity.managementFee);
				}
				dateStartFundMarketValue = MathUtils.add(dateStartFundMarketValue, shareClass.getBaseMarketValue());
			}
			// calculate new share class allocation: know each dollar amount and total
			for (FundShareClassValuation shareClass : fundValuation.getShareClassValuationList()) {
				shareClass.setBaseAllocation(MathUtils.isNullOrZero(dateStartFundMarketValue) ? BigDecimal.ZERO : MathUtils.divide(shareClass.getBaseMarketValue(), dateStartFundMarketValue));
			}
		}

		// calculate share class prices on valuation date
		BigDecimal valuationDateFundMarketValue = getFundMarketValue(fundValuation.getInvestmentAccount().getId(), fundValuation.getValuationDate());
		for (FundShareClassValuation shareClass : fundValuation.getShareClassValuationList()) {
			if (MathUtils.isNullOrZero(shareClass.getBaseQuantity())) {
				shareClass.setValuationPrice(null);
			}
			else {
				shareClass.setValuationPrice(MathUtils.divide(MathUtils.multiply(valuationDateFundMarketValue, shareClass.getBaseAllocation()), shareClass.getBaseQuantity()));
			}
		}
	}


	/**
	 * Instead of starting from the very first transaction, start from the last available official custodian price.
	 * Calculate share class allocations on that date. Will need to adjust for activities that happened after.
	 */
	private void findAndPopulateCalculationBase(FundValuation fundValuation, MarketDataField basePriceField) {
		BigDecimal baseFundMarketValue = BigDecimal.ZERO;
		for (int i = 0; i < fundValuation.getShareClassValuationList().size(); i++) {
			FundShareClassValuation shareClass = fundValuation.getShareClassValuationList().get(i);
			InvestmentSecurity shareClassSecurity = shareClass.getInvestmentSecurity();
			MarketDataValueHolder shareClassPrice = getMarketDataFieldService().getMarketDataValueForDateFlexibleNormalized(shareClassSecurity.getId(), fundValuation.getValuationDate(), basePriceField.getName());
			if (shareClassPrice == null) {
				// if price is not present, make sure quantity is 0, otherwise throw exception
				List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(shareClassSecurity.getId(), fundValuation.getValuationDate());
				shareClass.setBaseQuantity(CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity));
				if (!MathUtils.isNullOrZero(shareClass.getBaseQuantity())) {
					throw new ValidationException(basePriceField.getName() + " is not found for " + shareClassSecurity.getSymbol() + " prior to " + DateUtils.fromDateShort(fundValuation.getValuationDate()));
				}
			}
			else {
				Date priceDate = shareClassPrice.getMeasureDate();
				boolean olderPriceFound = false;
				if (fundValuation.getBaseDate() == null) {
					// first share class: no validation needed
				}
				else if (DateUtils.compare(fundValuation.getBaseDate(), priceDate, false) < 0) {
					// newer price found: make sure earlier share classes don't have positions on this date
					for (int j = 0; j < i; j++) {
						FundShareClassValuation earlierShareClass = fundValuation.getShareClassValuationList().get(j);
						List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(earlierShareClass.getInvestmentSecurity().getId(), priceDate);
						earlierShareClass.setBaseQuantity(CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity));
						if (!MathUtils.isNullOrZero(earlierShareClass.getBaseQuantity())) {
							throw new ValidationException(basePriceField.getName() + " must be present on the same date of " + DateUtils.fromDateShort(priceDate) + " for each share class (including "
									+ earlierShareClass.getInvestmentSecurity().getSymbol() + ") of " + fundValuation.getInvestmentAccount().getLabel());
						}
					}
					// reset total: all previous share classes don't have any positions
					baseFundMarketValue = BigDecimal.ZERO;
				}
				else if (DateUtils.compare(fundValuation.getBaseDate(), priceDate, false) > 0) {
					// older price found: make sure there are no positions on Base Date
					olderPriceFound = true;

					List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(shareClassSecurity.getId(), fundValuation.getBaseDate());
					shareClass.setBaseQuantity(CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity));
					if (!MathUtils.isNullOrZero(shareClass.getBaseQuantity())) {
						throw new ValidationException(basePriceField.getName() + " must be present on the same date of " + DateUtils.fromDateShort(fundValuation.getBaseDate()) + " for each share class (including "
								+ shareClassSecurity.getSymbol() + ") of " + fundValuation.getInvestmentAccount().getLabel());
					}
				}

				if (olderPriceFound) {
					fundValuation.setBaseDate(fundValuation.getBaseDate());
					shareClass.setBasePrice(BigDecimal.ZERO);
				}
				else {
					List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(shareClassSecurity.getId(), priceDate);
					shareClass.setBaseQuantity(CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity));
					if (MathUtils.isNullOrZero(shareClassPrice.getMeasureValue()) && !MathUtils.isNullOrZero(shareClass.getBaseQuantity())) {
						throw new ValidationException(basePriceField.getName() + " cannot be 0 on " + DateUtils.fromDateShort(fundValuation.getBaseDate()) + " for share class "
								+ shareClassSecurity.getSymbol() + " of " + fundValuation.getInvestmentAccount().getLabel() + " that has positions on that date");
					}
					fundValuation.setBaseDate(priceDate);
					shareClass.setBasePrice(shareClassPrice.getMeasureValue());
					baseFundMarketValue = MathUtils.add(baseFundMarketValue, getInvestmentCalculator().calculateNotional(shareClassSecurity, shareClass.getBasePrice(), shareClass.getBaseQuantity(), BigDecimal.ONE));
				}
			}
		}

		// update share class allocations on base date
		if (!MathUtils.isNullOrZero(baseFundMarketValue)) {
			for (FundShareClassValuation shareClass : fundValuation.getShareClassValuationList()) {
				shareClass.setBaseAllocation(MathUtils.divide(
						getInvestmentCalculator().calculateNotional(shareClass.getInvestmentSecurity(), shareClass.getBasePrice(), shareClass.getBaseQuantity(), BigDecimal.ONE), baseFundMarketValue));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Market Value for the specified client investment account on the specified date:
	 * the market value of positions plus cash balance.
	 */
	private BigDecimal getFundMarketValue(int fundClientAccountId, Date date) {
		// start with market value of positions at lot level
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForClientAccount(fundClientAccountId, date);
		BigDecimal marketValue = BigDecimal.ZERO;
		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			marketValue = MathUtils.add(marketValue, getAccountingPositionHandler().getAccountingPositionMarketValue(position, date));
		}

		// add remaining assets (cash, etc) and subtract liabilities (add because negative: Debit - Credit);
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(date);
		searchForm.setClientInvestmentAccountId(fundClientAccountId);
		searchForm.setExcludedHoldingAccounts(false);
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.NON_POSITION_ACCOUNTS);
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		List<AccountingBalanceValue> balanceValueList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		for (AccountingBalanceValue value : CollectionUtils.getIterable(balanceValueList)) {
			marketValue = MathUtils.add(marketValue, value.getBaseMarketValue());
		}

		return marketValue;
	}


	/**
	 * Returns the DailyShareClassActivity that matches the specified investment security.
	 */
	private DailyShareClassActivity getDailyShareClassActivity(DailyActivity activity, InvestmentSecurity security) {
		for (DailyShareClassActivity shareClassActivity : activity.shareClassActivityList) {
			if (shareClassActivity.investmentSecurity.equals(security)) {
				return shareClassActivity;
			}
		}
		return null;
	}


	/**
	 * Returns a list of fund activity up until "Valuation Date".  Activities include: Contributions, Distributions, and Management Fee payments.
	 */
	private List<DailyActivity> getFundActivity(FundValuation fundValuation, AccountingAccount managementFeeAccount) {
		List<DailyActivity> result = new ArrayList<>();
		// get positions for each share class
		for (FundShareClassValuation shareClass : fundValuation.getShareClassValuationList()) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setInvestmentSecurityId(shareClass.getInvestmentSecurity().getId());
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
			searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.LESS_THAN_OR_EQUALS, fundValuation.getValuationDate()));
			if (fundValuation.getBaseDate() != null) {
				searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.GREATER_THAN, fundValuation.getBaseDate()));
			}
			List<AccountingTransaction> tranList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
			for (AccountingTransaction tran : CollectionUtils.getIterable(tranList)) {
				// find existing activity or add new
				DailyActivity activity = null;
				for (DailyActivity existingActivity : result) {
					if (tran.getTransactionDate().equals(existingActivity.activityDate)) {
						activity = existingActivity;
						break;
					}
				}
				if (activity == null) {
					// first time for the day: add new
					activity = new DailyActivity();
					activity.activityDate = tran.getTransactionDate();
					activity.shareClassActivityList = new ArrayList<>();
					result.add(activity);
				}
				DailyShareClassActivity shareClassActivity = getDailyShareClassActivity(activity, shareClass.getInvestmentSecurity());
				if (shareClassActivity == null) {
					// first time for the share class: add new
					shareClassActivity = new DailyShareClassActivity();
					shareClassActivity.investmentSecurity = shareClass.getInvestmentSecurity();
					activity.shareClassActivityList.add(shareClassActivity);
				}
				shareClassActivity.changeInQuantity = MathUtils.add(shareClassActivity.changeInQuantity, tran.getQuantity());
				BigDecimal cashAmount = tran.getBaseDebitCredit();
				// need to add gain/loss for closing transactions
				if (tran.getParentTransaction() != null) {
					AccountingTransactionSearchForm sForm = new AccountingTransactionSearchForm();
					sForm.setParentId(tran.getId());
					sForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.GAIN_LOSS_ACCOUNTS);
					List<AccountingTransaction> glList = getAccountingTransactionService().getAccountingTransactionList(sForm);
					for (AccountingTransaction gl : CollectionUtils.getIterable(glList)) {
						cashAmount = MathUtils.add(cashAmount, gl.getBaseDebitCredit()); // adding to closing entry
					}
				}
				shareClassActivity.changeInInvestment = MathUtils.add(shareClassActivity.changeInInvestment, cashAmount);
			}
		}

		// add Management Fee activities
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setClientInvestmentAccountId(fundValuation.getInvestmentAccount().getId());
		if (managementFeeAccount == null) {
			searchForm.setAccountingAccountName(AccountingAccount.EXPENSE_MANAGEMENT_FEE);
		}
		else {
			searchForm.setAccountingAccountId(managementFeeAccount.getId());
		}
		searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.LESS_THAN_OR_EQUALS, fundValuation.getValuationDate()));
		if (fundValuation.getBaseDate() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.GREATER_THAN, fundValuation.getBaseDate()));
		}
		List<AccountingTransaction> tranList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (AccountingTransaction tran : CollectionUtils.getIterable(tranList)) {
			// find existing activity or add new
			DailyActivity activity = null;
			for (DailyActivity existingActivity : result) {
				if (tran.getTransactionDate().equals(existingActivity.activityDate)) {
					activity = existingActivity;
					break;
				}
			}
			if (activity == null) {
				// first time for the day: add new
				activity = new DailyActivity();
				activity.activityDate = tran.getTransactionDate();
				activity.shareClassActivityList = new ArrayList<>();
				result.add(activity);
			}
			DailyShareClassActivity shareClassActivity = getDailyShareClassActivity(activity, tran.getInvestmentSecurity());
			if (shareClassActivity == null) {
				// first time for the share class: add new
				shareClassActivity = new DailyShareClassActivity();
				shareClassActivity.investmentSecurity = tran.getInvestmentSecurity();
				activity.shareClassActivityList.add(shareClassActivity);
			}
			shareClassActivity.managementFee = MathUtils.add(shareClassActivity.managementFee, tran.getBaseDebitCredit());
		}

		// sort in chronological order
		result = BeanUtils.sortWithFunction(result, DailyActivity::getActivityDate, true);

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	///////////                 HELPER CLASSES                  ////////////////
	////////////////////////////////////////////////////////////////////////////

	class DailyActivity {

		Date activityDate;
		List<DailyShareClassActivity> shareClassActivityList;


		public boolean isManagementFeeCharged() {
			for (DailyShareClassActivity shareClassActivity : this.shareClassActivityList) {
				if (!MathUtils.isNullOrZero(shareClassActivity.managementFee)) {
					return true;
				}
			}
			return false;
		}


		public Date getActivityDate() {
			return this.activityDate; // used for sorting
		}
	}

	class DailyShareClassActivity {

		InvestmentSecurity investmentSecurity;
		BigDecimal changeInQuantity = BigDecimal.ZERO; // number of shares
		BigDecimal changeInInvestment = BigDecimal.ZERO; // net change of contributions/distributions
		BigDecimal managementFee = BigDecimal.ZERO;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}
}
