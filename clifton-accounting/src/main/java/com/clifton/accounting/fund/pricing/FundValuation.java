package com.clifton.accounting.fund.pricing;


import com.clifton.investment.account.InvestmentAccount;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>FundValuation</code> class is used during fund share class price calculations.
 *
 * @author vgomelsky
 */
public class FundValuation {

	private InvestmentAccount investmentAccount;

	/**
	 * Date of last fund activity that was processed by valuation logic.
	 * Fund activities include contributions, distributions and management fee payments.
	 * During processing this date usually starts with the date of the very first activity and then is moved
	 * forward one activity date at a time up to valuationDate.
	 */
	private Date baseDate;
	/**
	 * Prices are calculated for this date.
	 */
	private Date valuationDate;

	private List<FundShareClassValuation> shareClassValuationList;


	/**
	 * Clears all valuation run related fields. Prepares the fund for another valuation on a different date.
	 */
	public void reset() {
		this.baseDate = null;
		this.valuationDate = null;
		if (this.shareClassValuationList != null) {
			for (FundShareClassValuation shareClass : this.shareClassValuationList) {
				shareClass.setBaseAllocation(null);
				shareClass.setBaseMarketValue(null);
				shareClass.setBaseQuantity(null);
				shareClass.setValuationPrice(null);
			}
		}
	}


	public void addFundShareClassValuation(FundShareClassValuation shareClass) {
		if (this.shareClassValuationList == null) {
			this.shareClassValuationList = new ArrayList<>();
		}
		this.shareClassValuationList.add(shareClass);
	}


	public Date getBaseDate() {
		return this.baseDate;
	}


	public void setBaseDate(Date baseDate) {
		this.baseDate = baseDate;
	}


	public Date getValuationDate() {
		return this.valuationDate;
	}


	public void setValuationDate(Date valuationDate) {
		this.valuationDate = valuationDate;
	}


	public List<FundShareClassValuation> getShareClassValuationList() {
		return this.shareClassValuationList;
	}


	public void setShareClassValuationList(List<FundShareClassValuation> shareClassValuationList) {
		this.shareClassValuationList = shareClassValuationList;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}
}
