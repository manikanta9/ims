package com.clifton.accounting.fund.pricing;


import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;


/**
 * The <code>FundShareClassValuation</code> class is used during fund share class price calculations.
 * <p/>
 * A fund may have multiple share classes (usually different fee schedules) that may have different prices on the same date.
 *
 * @author vgomelsky
 */
public class FundShareClassValuation {

	private InvestmentSecurity investmentSecurity;

	/**
	 * Market value of this share class: positions + cash. It's calculated by multiplying baseAllocation by Total Fund Market Value.
	 */
	private BigDecimal baseMarketValue;
	/**
	 * Portion of this share class in the fund [0, 1]. Zero value means no shares of this share class exist in the fund.
	 * One value means that only this share class has shares in the fund on base date.
	 */
	private BigDecimal baseAllocation;
	/**
	 * The number of shares in the share class.
	 */
	private BigDecimal baseQuantity;
	private BigDecimal basePrice;
	private BigDecimal valuationPrice;


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getBaseQuantity() {
		return this.baseQuantity;
	}


	public void setBaseQuantity(BigDecimal baseQuantity) {
		this.baseQuantity = baseQuantity;
	}


	public BigDecimal getValuationPrice() {
		return this.valuationPrice;
	}


	public void setValuationPrice(BigDecimal valuationPrice) {
		this.valuationPrice = valuationPrice;
	}


	public BigDecimal getBaseAllocation() {
		return this.baseAllocation;
	}


	public void setBaseAllocation(BigDecimal baseAllocation) {
		this.baseAllocation = baseAllocation;
	}


	public BigDecimal getBaseMarketValue() {
		return this.baseMarketValue;
	}


	public void setBaseMarketValue(BigDecimal baseMarketValue) {
		this.baseMarketValue = baseMarketValue;
	}


	public BigDecimal getBasePrice() {
		return this.basePrice;
	}


	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}
}
