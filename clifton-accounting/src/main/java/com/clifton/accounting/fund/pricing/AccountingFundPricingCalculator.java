package com.clifton.accounting.fund.pricing;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.field.MarketDataField;

import java.util.List;


/**
 * The <code>AccountingFundPricingCalculator</code> interface defines methods for calculating fund
 * share class prices.
 *
 * @author vgomelsky
 */
public interface AccountingFundPricingCalculator {

	/**
	 * Returns a list of FundValuation objects for fund investment securities that match the specified search
	 * form configuration.  If searchForm is null, will return a list of all FundValuation objects.
	 * An investment security is a fund if it is linked to a corresponding InvestmentAccount representing the fund.
	 *
	 * @param searchForm
	 */
	public List<FundValuation> getFundValuationList(SecuritySearchForm searchForm);


	/**
	 * Populates valuationPrice field of each share class of this fund. During processing will also
	 * populate other fields of the specified object.  Implementation starts with the date of the first
	 * activity in the fund and then moves forward up until valuation date while re-adjusting  Share Class
	 * allocations after each activity.
	 *
	 * @param fundValuation
	 * @param managementFeeAccount optional, use default if one is not specified
	 * @param basePriceField       optional field identifying official custodian price. If specified start calculations from the latest value
	 *                             of this field instead of from the very beginning of the fund.
	 */
	public void calculateFundValuation(FundValuation fundValuation, AccountingAccount managementFeeAccount, MarketDataField basePriceField);
}
