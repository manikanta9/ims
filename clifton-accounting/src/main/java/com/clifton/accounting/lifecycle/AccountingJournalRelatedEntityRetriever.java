package com.clifton.accounting.lifecycle;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class AccountingJournalRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private AccountingBookingService<BookableEntity> accountingBookingService;
	private AccountingJournalService accountingJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof AccountingJournal;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		AccountingJournal journal = (AccountingJournal) entity;

		if (journal.getParent() != null) {
			relatedEntityList.add(journal.getParent());
		}

		// only add journal details for unposted journals: once posted, transactions are considered to be part of the journal
		if (journal.getPostingDate() == null && !CollectionUtils.isEmpty(journal.getJournalDetailList())) {
			relatedEntityList.addAll(journal.getJournalDetailList());
		}

		// include the source entity that resulted in this journal
		BookableEntity sourceEntity = getAccountingBookingService().getBookableEntity(journal);
		if (sourceEntity != null) {
			relatedEntityList.add(sourceEntity);
		}

		// get other journals for the same source entity: deleted, adjusted
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setFkFieldId(journal.getFkFieldId());
		searchForm.setJournalTypeId(journal.getJournalType().getId());
		List<AccountingJournal> journalList = getAccountingJournalService().getAccountingJournalList(searchForm);
		if (CollectionUtils.getSize(journalList) > 1) {
			relatedEntityList.addAll(journalList);
		}

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<BookableEntity> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<BookableEntity> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}
}
