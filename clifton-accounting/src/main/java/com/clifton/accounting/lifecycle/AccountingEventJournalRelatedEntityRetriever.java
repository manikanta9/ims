package com.clifton.accounting.lifecycle;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.core.beans.IdentityObject;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class AccountingEventJournalRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof AccountingEventJournal;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		AccountingEventJournal eventJournal = (AccountingEventJournal) entity;

		relatedEntityList.add(eventJournal.getSecurityEvent());

		return relatedEntityList;
	}
}
