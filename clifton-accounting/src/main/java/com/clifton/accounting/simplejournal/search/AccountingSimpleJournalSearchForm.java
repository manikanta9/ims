package com.clifton.accounting.simplejournal.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingSimpleJournalSearchForm</code> class defines search configuration for AccountingSimpleJournal objects.
 *
 * @author vgomelsky
 */
public class AccountingSimpleJournalSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;
	@SearchField(searchFieldPath = "journalType", searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String journalTypeName;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "number", searchFieldPath = "clientInvestmentAccount", comparisonConditions = {ComparisonConditions.EQUALS})
	private String clientInvestmentAccount;

	@SearchField(searchField = "mainHoldingInvestmentAccount.id")
	private Integer mainHoldingInvestmentAccountId;

	@SearchField(searchField = "mainAccountingAccount.id")
	private Short mainAccountingAccountId;

	@SearchField(searchField = "mainInvestmentSecurity.id")
	private Integer mainInvestmentSecurityId;

	@SearchField(searchField = "offsettingAccountingAccount.id")
	private Short offsettingAccountingAccountId;

	@SearchField(searchField = "offsettingInvestmentSecurity.id")
	private Integer offsettingInvestmentSecurityId;

	@SearchField
	private BigDecimal amount;

	@SearchField
	private Date bookingDate;
	@SearchField
	private Date transactionDate;
	@SearchField
	private Date settlementDate;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean journalBooked;

	@SearchField(searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;


	@SearchField
	private String description;

	@SearchField
	private String privateNote;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "AccountingSimpleJournal";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getMainHoldingInvestmentAccountId() {
		return this.mainHoldingInvestmentAccountId;
	}


	public void setMainHoldingInvestmentAccountId(Integer mainHoldingInvestmentAccountId) {
		this.mainHoldingInvestmentAccountId = mainHoldingInvestmentAccountId;
	}


	public Short getMainAccountingAccountId() {
		return this.mainAccountingAccountId;
	}


	public void setMainAccountingAccountId(Short mainAccountingAccountId) {
		this.mainAccountingAccountId = mainAccountingAccountId;
	}


	public Integer getMainInvestmentSecurityId() {
		return this.mainInvestmentSecurityId;
	}


	public void setMainInvestmentSecurityId(Integer mainInvestmentSecurityId) {
		this.mainInvestmentSecurityId = mainInvestmentSecurityId;
	}


	public Short getOffsettingAccountingAccountId() {
		return this.offsettingAccountingAccountId;
	}


	public void setOffsettingAccountingAccountId(Short offsettingAccountingAccountId) {
		this.offsettingAccountingAccountId = offsettingAccountingAccountId;
	}


	public Integer getOffsettingInvestmentSecurityId() {
		return this.offsettingInvestmentSecurityId;
	}


	public void setOffsettingInvestmentSecurityId(Integer offsettingInvestmentSecurityId) {
		this.offsettingInvestmentSecurityId = offsettingInvestmentSecurityId;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public String getJournalTypeName() {
		return this.journalTypeName;
	}


	public void setJournalTypeName(String journalTypeName) {
		this.journalTypeName = journalTypeName;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Boolean getJournalBooked() {
		return this.journalBooked;
	}


	public void setJournalBooked(Boolean journalBooked) {
		this.journalBooked = journalBooked;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(String clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getPrivateNote() {
		return this.privateNote;
	}


	public void setPrivateNote(String privateNote) {
		this.privateNote = privateNote;
	}
}
