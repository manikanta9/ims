package com.clifton.accounting.simplejournal.validation;


import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalType;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingSimpleJournalValidator</code> class validates AccountingSimpleJournal objects using
 * corresponding AccountingSimpleJournalType rules.
 *
 * @author vgomelsky
 */
@Component
public class AccountingSimpleJournalValidator extends SelfRegisteringDaoValidator<AccountingSimpleJournal> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingSimpleJournal bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			if (bean.getBookingDate() != null) {
				throw new ValidationException("Cannot delete a journal that has been booked.");
			}
		}
		else {
			if (config.isUpdate()) {
				AccountingSimpleJournal originalBean = getOriginalBean(bean);
				ValidationUtils.assertNotNull(originalBean, "Cannot find simple journal with id = " + bean.getId());
				if (originalBean.getBookingDate() != null && bean.getBookingDate() != null) {
					throw new ValidationException("Cannot modify a simple journal that has been booked.");
				}
			}
			AccountingSimpleJournalType type = bean.getJournalType();
			ValidationUtils.assertNotNull(type, "You must set the journal type!");
			if (bean.getClientInvestmentAccount().getBaseCurrency().equals(bean.getMainInvestmentSecurity())) {
				if (type.getMainAccountingAccount() != null && !type.getMainAccountingAccount().equals(bean.getMainAccountingAccount())) {
					throw new FieldValidationException("Main accounting account must be the same as the journal type main account: " + type.getMainAccountingAccount().getLabel(),
							"mainAccountingAccount");
				}
				if (type.getOffsettingAccountingAccount() != null && !type.getOffsettingAccountingAccount().equals(bean.getOffsettingAccountingAccount())) {
					throw new FieldValidationException("Offsetting accounting account must be the same as the journal type Offsetting account: " + type.getOffsettingAccountingAccount().getLabel(),
							"offsettingAccountingAccount");
				}
			}
			else {
				if (type.getMainCurrencyAccountingAccount() != null && !type.getMainCurrencyAccountingAccount().equals(bean.getMainAccountingAccount())) {
					throw new FieldValidationException("Main accounting account must be the same as the journal type main currency account: " + type.getMainCurrencyAccountingAccount().getLabel(),
							"mainAccountingAccount");
				}
				if (type.getOffsettingCurrencyAccountingAccount() != null && !type.getOffsettingCurrencyAccountingAccount().equals(bean.getOffsettingAccountingAccount())) {
					throw new FieldValidationException("Offsetting accounting account must be the same as the journal type Offsetting currency account: "
							+ type.getOffsettingCurrencyAccountingAccount().getLabel(), "offsettingAccountingAccount");
				}
			}
			if (bean.getMainAccountingAccount().isPosition()) {
				throw new FieldValidationException("Main accounting account cannot be a position account (not simple).", "mainAccountingAccount");
			}
			if (bean.getOffsettingAccountingAccount().isPosition()) {
				throw new FieldValidationException("Offsetting accounting account cannot be a position account (not simple).", "offsettingAccountingAccount");
			}
			if (type.isMainSecurityBaseCurrency() && !bean.getMainInvestmentSecurity().equals(bean.getClientInvestmentAccount().getBaseCurrency())) {
				throw new FieldValidationException("Main investment security must be the same as client account base currency: " + bean.getClientInvestmentAccount().getBaseCurrency().getLabel(),
						"mainInvestmentSecurity");
			}
			if (type.isOffsettingSecurityBaseCurrency() && !bean.getOffsettingInvestmentSecurity().equals(bean.getClientInvestmentAccount().getBaseCurrency())) {
				throw new FieldValidationException(
						"Offsetting investment security must be the same as client account base currency: " + bean.getClientInvestmentAccount().getBaseCurrency().getLabel(),
						"offsettingInvestmentSecurity");
			}
			if (type.isSameHoldingAccount() && !bean.getMainHoldingInvestmentAccount().equals(bean.getOffsettingHoldingInvestmentAccount())) {
				throw new FieldValidationException("Main and Offsetting holding investment accounts must be the same for this journal type.", "offsettingHoldingInvestmentAccount");
			}
			if (type.isSameSecurity() && !bean.getMainInvestmentSecurity().equals(bean.getOffsettingInvestmentSecurity())) {
				throw new FieldValidationException("Main and Offsetting securities must be the same for this journal type.", "offsettingInvestmentSecurity");
			}

			if (type.isPositiveAmountOnly() && MathUtils.isLessThan(bean.getAmount(), 0.001)) {
				throw new FieldValidationException("Simple journal amount [" + bean.getAmount() + "] for journal [" + bean.getLabel() + "] must be greater than 0 for selected journal type: "
						+ type.getName(), "amount");
			}
			if (type.isDescriptionRequired() && StringUtils.isEmpty(bean.getDescription())) {
				throw new FieldValidationException("Description is required for selected journal type.", "description");
			}
			if (DateUtils.compare(bean.getSettlementDate(), bean.getTransactionDate(), true) < 0) {
				throw new FieldValidationException("Settlement date cannot be before transaction date.", "settlementDate");
			}
			if (DateUtils.compare(bean.getOriginalTransactionDate(), bean.getTransactionDate(), true) > 0) {
				throw new FieldValidationException("Original transaction date cannot be after transaction date.", "originalTransactionDate");
			}
		}
	}
}
