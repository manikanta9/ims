package com.clifton.accounting.simplejournal;


import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalSearchForm;
import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalTypeSearchForm;

import java.util.List;


/**
 * The <code>AccountingSimpleJournalService</code> interface defines methods for working with simple journals.
 *
 * @author vgomelsky
 */
public interface AccountingSimpleJournalService {

	////////////////////////////////////////////////////////////////////////////
	////////       AccountingSimpleJournal Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingSimpleJournal getAccountingSimpleJournal(int id);


	public List<AccountingSimpleJournal> getAccountingSimpleJournalList(AccountingSimpleJournalSearchForm searchForm);


	public AccountingSimpleJournal saveAccountingSimpleJournal(AccountingSimpleJournal bean);


	public void deleteAccountingSimpleJournal(int id);


	////////////////////////////////////////////////////////////////////////////
	////////      AccountingSimpleJournalType Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingSimpleJournalType getAccountingSimpleJournalType(short id);


	public AccountingSimpleJournalType getAccountingSimpleJournalTypeByName(String name);


	public List<AccountingSimpleJournalType> getAccountingSimpleJournalTypeList();


	public List<AccountingSimpleJournalType> getAccountingSimpleJournalTypeList(AccountingSimpleJournalTypeSearchForm searchForm);


	public AccountingSimpleJournalType saveAccountingSimpleJournalType(AccountingSimpleJournalType bean);


	public void deleteAccountingSimpleJournalType(short id);
}
