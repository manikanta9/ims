package com.clifton.accounting.simplejournal;


import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalSearchForm;
import com.clifton.accounting.simplejournal.search.AccountingSimpleJournalTypeSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>AccountingSimpleJournalServiceImpl</code> class provides basic implementation of the AccountingSimpleJournalService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingSimpleJournalServiceImpl implements AccountingSimpleJournalService {

	private AdvancedUpdatableDAO<AccountingSimpleJournal, Criteria> accountingSimpleJournalDAO;
	private AdvancedUpdatableDAO<AccountingSimpleJournalType, Criteria> accountingSimpleJournalTypeDAO;

	private DaoNamedEntityCache<AccountingSimpleJournalType> accountingSimpleJournalTypeCache;


	////////////////////////////////////////////////////////////////////////////
	////////       AccountingSimpleJournal Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingSimpleJournal getAccountingSimpleJournal(int id) {
		return getAccountingSimpleJournalDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingSimpleJournal> getAccountingSimpleJournalList(AccountingSimpleJournalSearchForm searchForm) {
		return getAccountingSimpleJournalDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// see {@link AccountingSimpleJournalValidator} for validation details
	@Override
	public AccountingSimpleJournal saveAccountingSimpleJournal(AccountingSimpleJournal bean) {
		return getAccountingSimpleJournalDAO().save(bean);
	}


	// see {@link AccountingSimpleJournalValidator} for validation details
	@Override
	public void deleteAccountingSimpleJournal(int id) {
		getAccountingSimpleJournalDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////      AccountingSimpleJournalType Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingSimpleJournalType getAccountingSimpleJournalType(short id) {
		return getAccountingSimpleJournalTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingSimpleJournalType getAccountingSimpleJournalTypeByName(String name) {
		return getAccountingSimpleJournalTypeCache().getBeanForKeyValueStrict(getAccountingSimpleJournalTypeDAO(), name);
	}


	@Override
	public List<AccountingSimpleJournalType> getAccountingSimpleJournalTypeList() {
		return getAccountingSimpleJournalTypeDAO().findAll();
	}


	@Override
	public List<AccountingSimpleJournalType> getAccountingSimpleJournalTypeList(AccountingSimpleJournalTypeSearchForm searchForm) {
		return getAccountingSimpleJournalTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingSimpleJournalType saveAccountingSimpleJournalType(AccountingSimpleJournalType bean) {
		// validation
		if (bean.isSystemGenerated()) {
			throw new ValidationException("System generated journal types cannot be modified.");
		}
		if (bean.isSameSecurity() && bean.isMainSecurityBaseCurrency() != bean.isOffsettingSecurityBaseCurrency()) {
			throw new ValidationException("When main and offsetting securities are the same, corresponding base currency indicators must be the same too.");
		}
		if (bean.getMainAccountingAccount() != null && bean.getMainAccountingAccount().isPosition()) {
			throw new FieldValidationException("Main accounting account cannot be a position account (not simple).", "mainAccountingAccount");
		}
		if (bean.getOffsettingAccountingAccount() != null && bean.getOffsettingAccountingAccount().isPosition()) {
			throw new FieldValidationException("Offsetting accounting account cannot be a position account (not simple).", "mainAccountingAccount");
		}

		return getAccountingSimpleJournalTypeDAO().save(bean);
	}


	@Override
	public void deleteAccountingSimpleJournalType(short id) {
		AccountingSimpleJournalType type = getAccountingSimpleJournalType(id);
		if (type != null && type.isSystemGenerated()) {
			throw new ValidationException("System generated journal types cannot be deleted.");
		}
		getAccountingSimpleJournalTypeDAO().delete(type);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingSimpleJournal, Criteria> getAccountingSimpleJournalDAO() {
		return this.accountingSimpleJournalDAO;
	}


	public void setAccountingSimpleJournalDAO(AdvancedUpdatableDAO<AccountingSimpleJournal, Criteria> accountingSimpleJournalDAO) {
		this.accountingSimpleJournalDAO = accountingSimpleJournalDAO;
	}


	public AdvancedUpdatableDAO<AccountingSimpleJournalType, Criteria> getAccountingSimpleJournalTypeDAO() {
		return this.accountingSimpleJournalTypeDAO;
	}


	public void setAccountingSimpleJournalTypeDAO(AdvancedUpdatableDAO<AccountingSimpleJournalType, Criteria> accountingSimpleJournalTypeDAO) {
		this.accountingSimpleJournalTypeDAO = accountingSimpleJournalTypeDAO;
	}


	public DaoNamedEntityCache<AccountingSimpleJournalType> getAccountingSimpleJournalTypeCache() {
		return this.accountingSimpleJournalTypeCache;
	}


	public void setAccountingSimpleJournalTypeCache(DaoNamedEntityCache<AccountingSimpleJournalType> accountingSimpleJournalTypeCache) {
		this.accountingSimpleJournalTypeCache = accountingSimpleJournalTypeCache;
	}
}
