package com.clifton.accounting.simplejournal.upload;


import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * The <code>AccountingSimpleJournalUploadService</code> ...
 *
 * @author Mary Anderson
 */
public interface AccountingSimpleJournalUploadService {

	/**
	 * Uploads AccountingSimpleJournals into the system and also allows immediate book & post of
	 * inserted journals
	 * <p/>
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = AccountingSimpleJournal.class)
	public void uploadAccountingSimpleJournalUploadFile(AccountingSimpleJournalUploadCommand uploadCommand);
}
