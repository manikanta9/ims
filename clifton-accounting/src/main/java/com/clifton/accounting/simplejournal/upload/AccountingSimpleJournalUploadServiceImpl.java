package com.clifton.accounting.simplejournal.upload;


import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalService;
import com.clifton.accounting.simplejournal.booking.AccountingSimpleJournalBookingProcessor;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingSimpleJournalUploadServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class AccountingSimpleJournalUploadServiceImpl implements AccountingSimpleJournalUploadService {

	private AccountingBookingService<AccountingSimpleJournal> accountingBookingService;
	private AccountingSimpleJournalService accountingSimpleJournalService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemUploadHandler systemUploadHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadAccountingSimpleJournalUploadFile(AccountingSimpleJournalUploadCommand uploadCommand) {
		if (!uploadCommand.isSimple()) {
			getSystemUploadHandler().uploadSystemUploadFile(uploadCommand);
		}
		else {
			List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, false);
			List<AccountingSimpleJournal> journalList = new ArrayList<>();
			int skippedCount = 0;
			for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
				AccountingSimpleJournal journal = (AccountingSimpleJournal) obj;

				// From Eric: Skip 0 Amount Rows
				if (MathUtils.isEqual(journal.getAmount(), 0)) {
					skippedCount++;
					continue;
				}

				ValidationUtils.assertNotNull(journal.getJournalType(), "Journal Type is required for all rows.");
				ValidationUtils.assertNotNull(journal.getJournalType().getName(), "Journal Type is required for all rows.");
				ValidationUtils.assertTrue(journal.isNewBean(), "Cannot update existing journals only inserts are allowed.");
				ValidationUtils.assertNotNull(journal.getAmount(), "Amount is required for all rows.  You must explicitly set the value to 0 for the row to be skipped.");

				// Pull From DB again so all data is current and populated
				journal.setJournalType(getAccountingSimpleJournalService().getAccountingSimpleJournalTypeByName(journal.getJournalType().getName()));

				// Transaction Date
				if (journal.getTransactionDate() == null) {
					if (uploadCommand.getTransactionDate() == null) {
						throw new ValidationException("Please specify a default transaction date or include a transaction date for each journal in the file.");
					}
					journal.setTransactionDate(uploadCommand.getTransactionDate());
				}
				// If no original transaction date, use transaction date
				if (journal.getOriginalTransactionDate() == null) {
					journal.setOriginalTransactionDate(journal.getTransactionDate());
				}
				// If no settlement date, use transaction date
				if (journal.getSettlementDate() == null) {
					journal.setSettlementDate(journal.getTransactionDate());
				}

				// Main Holding Account
				if (journal.getMainHoldingInvestmentAccount() == null || journal.getMainHoldingInvestmentAccount().getId() == null) {
					if (journal.getMainHoldingInvestmentAccount() == null || StringUtils.isEmpty(journal.getMainHoldingInvestmentAccount().getNumber())) {
						throw new ValidationException("Main Holding Investment Account Number is required for journal type [" + journal.getJournalType().getName() + "].");
					}
					journal.setMainHoldingInvestmentAccount(getInvestmentAccountByNumber(journal, "mainHoldingInvestmentAccount", false));
				}

				// Client Investment Account
				if (journal.getClientInvestmentAccount() == null || journal.getClientInvestmentAccount().getId() == null) {
					if (journal.getClientInvestmentAccount() != null && !StringUtils.isEmpty(journal.getClientInvestmentAccount().getNumber())) {
						journal.setClientInvestmentAccount(getInvestmentAccountByNumber(journal, "clientInvestmentAccount", true));
					}
					else {
						journal.setClientInvestmentAccount(getClientInvestmentAccount(journal.getMainHoldingInvestmentAccount(), uploadCommand.getPurpose(), journal.getTransactionDate()));
					}
				}

				// Offsetting Investment Account
				if (journal.getJournalType().isSameHoldingAccount()) {
					journal.setOffsettingHoldingInvestmentAccount(journal.getMainHoldingInvestmentAccount());
				}
				else if (journal.getOffsettingHoldingInvestmentAccount() == null || journal.getOffsettingHoldingInvestmentAccount().getId() == null) {
					if (journal.getOffsettingHoldingInvestmentAccount() == null || StringUtils.isEmpty(journal.getOffsettingHoldingInvestmentAccount().getNumber())) {
						throw new ValidationException("Offsetting Holding Investment Account Number is required for journal type [" + journal.getJournalType().getName() + "].");
					}
					journal.setOffsettingHoldingInvestmentAccount(getInvestmentAccountByNumber(journal, "offsettingHoldingInvestmentAccount", false));
				}

				// Main Security Symbol Set in the File
				if (journal.getMainInvestmentSecurity() != null && journal.getMainInvestmentSecurity().getId() == null && !StringUtils.isEmpty(journal.getMainInvestmentSecurity().getSymbol())) {
					// Look up security
					journal.setMainInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(journal.getMainInvestmentSecurity().getSymbol(), null));
				}
				// Offsetting Security Symbol Set in the File
				if (journal.getOffsettingInvestmentSecurity() != null && journal.getOffsettingInvestmentSecurity().getId() == null
						&& !StringUtils.isEmpty(journal.getOffsettingInvestmentSecurity().getSymbol())) {
					// Look up security
					journal.setOffsettingInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(journal.getOffsettingInvestmentSecurity().getSymbol(), null));
				}

				InvestmentSecurity baseCurrency = journal.getClientInvestmentAccount().getBaseCurrency();
				if (journal.getMainInvestmentSecurity() == null || journal.getMainInvestmentSecurity().getId() == null) {
					journal.setMainInvestmentSecurity(baseCurrency);
				}
				if (journal.getOffsettingInvestmentSecurity() == null || journal.getOffsettingInvestmentSecurity().getId() == null) {
					journal.setOffsettingInvestmentSecurity(baseCurrency);
				}

				if (baseCurrency.equals(journal.getMainInvestmentSecurity())) {
					if (journal.getJournalType().getMainAccountingAccount() == null) {
						throw new ValidationException("Unable to set main accounting account for journal: " + journal.getLabel() + ". Journal Type [" + journal.getJournalType().getName()
								+ "] does not have main accounting account set.");
					}
					journal.setMainAccountingAccount(journal.getJournalType().getMainAccountingAccount());
					journal.setExchangeRateToBase(BigDecimal.ONE);
				}
				else {
					if (journal.getJournalType().getMainCurrencyAccountingAccount() == null) {
						throw new ValidationException("Unable to set main currency accounting account for journal: " + journal.getLabel() + ". Journal Type [" + journal.getJournalType().getName()
								+ "] does not have main currency accounting account set.");
					}
					journal.setMainAccountingAccount(journal.getJournalType().getMainCurrencyAccountingAccount());

					BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(journal.getMainHoldingInvestmentAccount().toHoldingAccount(),
							journal.getMainInvestmentSecurity().getSymbol(), baseCurrency.getSymbol(), journal.getTransactionDate()).noExceptionIfRateIsMissing());
					if (fxRate == null) {
						throw new ValidationException("Missing exchange rate from [" + journal.getMainInvestmentSecurity().getSymbol() + " to " + baseCurrency.getSymbol() + "] on ["
								+ DateUtils.fromDateShort(journal.getTransactionDate()) + "] for Holding Account [" + journal.getMainHoldingInvestmentAccount().getLabel() + "]" + ").");
					}
					journal.setExchangeRateToBase(fxRate);
				}

				if (baseCurrency.equals(journal.getOffsettingInvestmentSecurity())) {
					if (journal.getJournalType().getOffsettingAccountingAccount() == null) {
						throw new ValidationException("Unable to set offsetting accounting account for journal: " + journal.getLabel() + ". Journal Type [" + journal.getJournalType().getName()
								+ "] does not have offsetting accounting account set.");
					}
					journal.setOffsettingAccountingAccount(journal.getJournalType().getOffsettingAccountingAccount());
				}
				else {
					if (journal.getJournalType().getOffsettingCurrencyAccountingAccount() == null) {
						throw new ValidationException("Unable to set offsetting currency accounting account for journal: " + journal.getLabel() + ". Journal Type ["
								+ journal.getJournalType().getName() + "] does not have offsetting currency accounting account set.");
					}

					journal.setOffsettingAccountingAccount(journal.getJournalType().getOffsettingCurrencyAccountingAccount());
				}
				journalList.add(journal);
			}

			saveAccountingSimpleJournalList(journalList);
			uploadCommand.getUploadResult().addUploadResults("AccountingSimpleJournal", CollectionUtils.getSize(journalList), false);
			if (skippedCount > 0) {
				uploadCommand.getUploadResult().addUploadResultsSkipped("AccountingSimpleJournal", skippedCount, " because of 0 amounts.");
			}
			if (uploadCommand.isBookAndPost()) {
				int bookedCount = 0;
				int bookedErrorCount = 0;
				for (AccountingSimpleJournal j : CollectionUtils.getIterable(journalList)) {
					try {
						AccountingJournal bookedJournal = getAccountingBookingService().bookAccountingJournal(AccountingSimpleJournalBookingProcessor.JOURNAL_NAME, j.getId(), true);
						if (bookedJournal != null) {
							bookedCount++;
						}
					}
					catch (Exception e) {
						bookedErrorCount++;
						uploadCommand.getUploadResult().addError(j.getLabel(), e.getMessage());
						LogUtils.errorOrInfo(getClass(), "Failed to Book Journal: " + j.getLabel(), e);
					}
				}
				uploadCommand.getUploadResult().addUploadResults("Booked-AccountingSimpleJournals", bookedCount, false);
				uploadCommand.getUploadResult().addError("Booking", "Total Journals Failed Booking: " + bookedErrorCount);
			}
		}
	}


	@Transactional(timeout = 180)
	protected void saveAccountingSimpleJournalList(List<AccountingSimpleJournal> journalList) {
		for (AccountingSimpleJournal j : CollectionUtils.getIterable(journalList)) {
			getAccountingSimpleJournalService().saveAccountingSimpleJournal(j);
		}
	}


	private InvestmentAccount getInvestmentAccountByNumber(AccountingSimpleJournal journal, String accountFieldName, boolean ourAccount) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(ourAccount);
		String accountNumber = (String) BeanUtils.getPropertyValue(journal, accountFieldName + ".number");
		String companyName = null;
		String companyType = null;

		// Only needed for non clifton accounts, otherwise company name is always Parametric Clifton
		if (!ourAccount) {
			BusinessCompany issuingCompany = (BusinessCompany) BeanUtils.getPropertyValue(journal, accountFieldName + ".issuingCompany");
			if (issuingCompany != null) {
				if (!StringUtils.isEmpty(issuingCompany.getName())) {
					companyName = issuingCompany.getName();
				}
				if (issuingCompany.getType() != null && !StringUtils.isEmpty(issuingCompany.getType().getName())) {
					companyType = issuingCompany.getType().getName();
				}
			}
		}

		String message = "";
		if (!StringUtils.isEmpty(accountNumber)) {
			searchForm.setNumberEquals(accountNumber);
			message += " Number [" + accountNumber + "]";
		}
		if (!StringUtils.isEmpty(companyName)) {
			searchForm.setIssuingCompanyName(companyName);
			message += " Issuing Company Name [" + companyName + "]";
		}
		if (!StringUtils.isEmpty(companyType)) {
			searchForm.setIssuingCompanyTypeName(companyType);
			message += " Issuing Company Type [" + companyType + "]";
		}

		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		if (CollectionUtils.isEmpty(accountList)) {
			throw new ValidationException("Cannot find a " + (ourAccount ? "Client" : "Holding") + " account with " + message);
		}
		if (CollectionUtils.getSize(accountList) > 1) {
			throw new ValidationException("There are multiple " + (ourAccount ? "Client" : "Holding") + " accounts with the " + message);
		}
		return accountList.get(0);
	}


	private InvestmentAccount getClientInvestmentAccount(InvestmentAccount holdingAccount, InvestmentAccountRelationshipPurpose purpose, Date transactionDate) {
		// Otherwise look it up based on relationships
		ValidationUtils.assertNotNull(holdingAccount, "Required argument 'holdingAccount' is missing.");
		ValidationUtils.assertNotNull(purpose, "Relationship Purpose is required to look up related accounts for this journal.");

		List<InvestmentAccount> relatedList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null, purpose.getName(), transactionDate);

		InvestmentAccount relatedAccount = null;
		for (InvestmentAccount account : CollectionUtils.getIterable(relatedList)) {
			if (account.getType().isOurAccount()) {
				ValidationUtils.assertNull(relatedAccount, "There are more than one client accounts related to holding account [" + holdingAccount.getNumber() + "] for purpose [" + purpose.getName()
						+ "].  Please provide an account number for the client account in the upload.");
				relatedAccount = account;
			}
		}

		// If nothing specific to the purpose - just any Client Account Relationship
		if (relatedAccount == null) {
			relatedList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null, null, transactionDate);
			for (InvestmentAccount account : CollectionUtils.getIterable(relatedList)) {
				if (account.getType().isOurAccount()) {
					if (relatedAccount != null) {
						ValidationUtils.assertTrue(relatedAccount.equals(account), "There are more than one client accounts related to holding account [" + holdingAccount.getNumber()
								+ "] for purpose [" + purpose.getName() + "].  Please provide an account number for the client account in the upload.");
					}
					relatedAccount = account;
				}
			}
		}

		ValidationUtils.assertNotNull(relatedAccount, "Cannot find any related client account for holding account: " + holdingAccount.getLabel());
		return relatedAccount;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public AccountingSimpleJournalService getAccountingSimpleJournalService() {
		return this.accountingSimpleJournalService;
	}


	public void setAccountingSimpleJournalService(AccountingSimpleJournalService accountingSimpleJournalService) {
		this.accountingSimpleJournalService = accountingSimpleJournalService;
	}


	public AccountingBookingService<AccountingSimpleJournal> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<AccountingSimpleJournal> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
