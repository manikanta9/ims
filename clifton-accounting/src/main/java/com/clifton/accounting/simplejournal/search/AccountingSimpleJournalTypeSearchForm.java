package com.clifton.accounting.simplejournal.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * The <code>AccountingSimpleJournalTypeSearchForm</code> class defines search configuration for AccountingSimpleJournalType objects.
 *
 * @author vgomelsky
 */
public class AccountingSimpleJournalTypeSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "mainAccountingAccount.id")
	private Short mainAccountingAccountId;

	@SearchField
	private Boolean mainSecurityBaseCurrency;

	@SearchField(searchField = "offsettingAccountingAccount.id")
	private Short offsettingAccountingAccountId;

	@SearchField
	private Boolean offsettingSecurityBaseCurrency;

	@SearchField
	private Boolean sameHoldingAccount;

	@SearchField
	private Boolean sameSecurity;

	@SearchField
	private Boolean positiveAmountOnly;

	@SearchField
	private Boolean descriptionRequired;

	@SearchField
	private Boolean systemGenerated;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "AccountingSimpleJournalType";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getMainAccountingAccountId() {
		return this.mainAccountingAccountId;
	}


	public void setMainAccountingAccountId(Short mainAccountingAccountId) {
		this.mainAccountingAccountId = mainAccountingAccountId;
	}


	public Boolean getMainSecurityBaseCurrency() {
		return this.mainSecurityBaseCurrency;
	}


	public void setMainSecurityBaseCurrency(Boolean mainSecurityBaseCurrency) {
		this.mainSecurityBaseCurrency = mainSecurityBaseCurrency;
	}


	public Short getOffsettingAccountingAccountId() {
		return this.offsettingAccountingAccountId;
	}


	public void setOffsettingAccountingAccountId(Short offsettingAccountingAccountId) {
		this.offsettingAccountingAccountId = offsettingAccountingAccountId;
	}


	public Boolean getOffsettingSecurityBaseCurrency() {
		return this.offsettingSecurityBaseCurrency;
	}


	public void setOffsettingSecurityBaseCurrency(Boolean offsettingSecurityBaseCurrency) {
		this.offsettingSecurityBaseCurrency = offsettingSecurityBaseCurrency;
	}


	public Boolean getDescriptionRequired() {
		return this.descriptionRequired;
	}


	public void setDescriptionRequired(Boolean descriptionRequired) {
		this.descriptionRequired = descriptionRequired;
	}


	public Boolean getSystemGenerated() {
		return this.systemGenerated;
	}


	public void setSystemGenerated(Boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}


	public Boolean getSameHoldingAccount() {
		return this.sameHoldingAccount;
	}


	public void setSameHoldingAccount(Boolean sameHoldingAccount) {
		this.sameHoldingAccount = sameHoldingAccount;
	}


	public Boolean getSameSecurity() {
		return this.sameSecurity;
	}


	public void setSameSecurity(Boolean sameSecurity) {
		this.sameSecurity = sameSecurity;
	}


	public Boolean getPositiveAmountOnly() {
		return this.positiveAmountOnly;
	}


	public void setPositiveAmountOnly(Boolean positiveAmountOnly) {
		this.positiveAmountOnly = positiveAmountOnly;
	}
}
