package com.clifton.accounting.simplejournal.booking;


import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingSimpleJournalBookingRule</code> class is a booking rule that adds 2 journal details
 * to existing journal based on specified AccountingSimpleJournal definition.
 *
 * @author vgomelsky
 */
public class AccountingSimpleJournalBookingRule implements AccountingBookingRule<AccountingSimpleJournal> {

	@Override
	public void applyRule(BookingSession<AccountingSimpleJournal> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		AccountingSimpleJournal simpleJournal = bookingSession.getBookableEntity();

		String longDescription = simpleJournal.getDescription();
		String shortDescription = simpleJournal.getDescription();
		if (StringUtils.isEmpty(longDescription)) {
			longDescription = simpleJournal.getLabel();
			shortDescription = simpleJournal.getJournalType().getDefaultJournalDescription();
			if (StringUtils.isEmpty(shortDescription)) {
				shortDescription = simpleJournal.getJournalType().getName();
			}
		}
		journal.setDescription(longDescription);

		journal.addJournalDetail(generateJournalDetail(journal, simpleJournal, true, shortDescription));
		journal.addJournalDetail(generateJournalDetail(journal, simpleJournal, false, shortDescription));
	}


	/**
	 * Generates main or offsetting detail line for the specified journal.
	 */
	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingSimpleJournal simpleJournal, boolean mainEntry, String description) {
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(journal.getFkFieldId());
		detail.setSystemTable(journal.getSystemTable());
		detail.setClientInvestmentAccount(simpleJournal.getClientInvestmentAccount());
		detail.setDescription(description);

		if (mainEntry) {
			detail.setHoldingInvestmentAccount(simpleJournal.getMainHoldingInvestmentAccount());
			detail.setInvestmentSecurity(simpleJournal.getMainInvestmentSecurity());
			detail.setAccountingAccount(simpleJournal.getMainAccountingAccount());
		}
		else {
			detail.setAccountingAccount(simpleJournal.getOffsettingAccountingAccount());
			detail.setHoldingInvestmentAccount(simpleJournal.getOffsettingHoldingInvestmentAccount());
			detail.setInvestmentSecurity(simpleJournal.getOffsettingInvestmentSecurity());
		}

		// determine Debit or Credit
		BigDecimal amount = mainEntry ? simpleJournal.getAmount() : simpleJournal.getAmount().negate();
		if (simpleJournal.getJournalType().isCreditMainAccountingAccount()) {
			amount = amount.negate();
		}
		detail.setLocalDebitCredit(amount);

		detail.setExchangeRateToBase(simpleJournal.getExchangeRateToBase());
		detail.setBaseDebitCredit(amount.multiply(detail.getExchangeRateToBase()).setScale(2, BigDecimal.ROUND_HALF_UP));
		if (detail.getAccountingAccount().isPosition()) {
			detail.setPositionCostBasis(detail.getLocalDebitCredit()); // currency
		}
		else {
			detail.setPositionCostBasis(BigDecimal.ZERO);
		}
		detail.setQuantity(null);
		detail.setPrice(null);

		detail.setSettlementDate(simpleJournal.getSettlementDate());
		detail.setOriginalTransactionDate(simpleJournal.getOriginalTransactionDate());
		detail.setTransactionDate(simpleJournal.getTransactionDate());

		// questionable fields other than possibly cost basis (may need to delete later)
		detail.setOpening(true);
		detail.setPositionCommission(BigDecimal.ZERO);

		return detail;
	}
}
