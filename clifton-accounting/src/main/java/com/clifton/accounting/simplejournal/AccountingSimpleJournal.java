package com.clifton.accounting.simplejournal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>AccountingSimpleJournal</code> class defines a simple journal: a journal that
 * always generates 2 general ledger entries: main row and offsetting row.
 * The sum of debits and credits across both rows is always 0.
 *
 * @author vgomelsky
 */
public class AccountingSimpleJournal extends BaseEntity<Integer> implements BookableEntity {

	private AccountingSimpleJournalType journalType;

	private InvestmentAccount clientInvestmentAccount;

	private InvestmentAccount mainHoldingInvestmentAccount;
	private AccountingAccount mainAccountingAccount;
	private InvestmentSecurity mainInvestmentSecurity;

	private InvestmentAccount offsettingHoldingInvestmentAccount;
	private AccountingAccount offsettingAccountingAccount;
	private InvestmentSecurity offsettingInvestmentSecurity;

	private BigDecimal amount;
	private BigDecimal exchangeRateToBase;

	private String description;
	/**
	 * Private note is saved on the journal for internal research only. It's not passed to GL and won't show in client reports.
	 */
	private String privateNote;

	/**
	 * The date when this simple journal was moved into AccountingJournal. This field is set by the system
	 * after the event occurs and prevents journal from being modified after it was set.
	 */
	private Date bookingDate;
	/**
	 * The date when transaction took place.
	 */
	private Date transactionDate;
	/**
	 * The date of the original transaction which in most cases is the same as transaction date.
	 * However, for transfers it is the date of the opening trade which is prior to transaction date.
	 */
	private Date originalTransactionDate;
	/**
	 * The date when this transaction settles (assets exchange hands). It's usually 1 or more business days
	 * after the trade depending on investment security or trade properties.
	 */
	private Date settlementDate;


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (this.journalType != null) {
			result.append(this.journalType.getName());
			result.append(" of ");
		}
		result.append(this.amount);
		if (this.clientInvestmentAccount != null) {
			result.append(" for ");
			result.append(this.clientInvestmentAccount.getLabel());
		}
		return result.toString();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount());
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public AccountingAccount getMainAccountingAccount() {
		return this.mainAccountingAccount;
	}


	public void setMainAccountingAccount(AccountingAccount mainAccountingAccount) {
		this.mainAccountingAccount = mainAccountingAccount;
	}


	public InvestmentSecurity getMainInvestmentSecurity() {
		return this.mainInvestmentSecurity;
	}


	public void setMainInvestmentSecurity(InvestmentSecurity mainInvestmentSecurity) {
		this.mainInvestmentSecurity = mainInvestmentSecurity;
	}


	public AccountingAccount getOffsettingAccountingAccount() {
		return this.offsettingAccountingAccount;
	}


	public void setOffsettingAccountingAccount(AccountingAccount offsettingAccountingAccount) {
		this.offsettingAccountingAccount = offsettingAccountingAccount;
	}


	public InvestmentSecurity getOffsettingInvestmentSecurity() {
		return this.offsettingInvestmentSecurity;
	}


	public void setOffsettingInvestmentSecurity(InvestmentSecurity offsettingInvestmentSecurity) {
		this.offsettingInvestmentSecurity = offsettingInvestmentSecurity;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public AccountingSimpleJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingSimpleJournalType journalType) {
		this.journalType = journalType;
	}


	public InvestmentAccount getMainHoldingInvestmentAccount() {
		return this.mainHoldingInvestmentAccount;
	}


	public void setMainHoldingInvestmentAccount(InvestmentAccount mainHoldingInvestmentAccount) {
		this.mainHoldingInvestmentAccount = mainHoldingInvestmentAccount;
	}


	public InvestmentAccount getOffsettingHoldingInvestmentAccount() {
		return this.offsettingHoldingInvestmentAccount;
	}


	public void setOffsettingHoldingInvestmentAccount(InvestmentAccount offsettingHoldingInvestmentAccount) {
		this.offsettingHoldingInvestmentAccount = offsettingHoldingInvestmentAccount;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public String getPrivateNote() {
		return this.privateNote;
	}


	public void setPrivateNote(String privateNote) {
		this.privateNote = privateNote;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}
}
