package com.clifton.accounting.simplejournal.upload;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.system.upload.SystemUploadCommand;

import java.util.Date;


/**
 * The <code>AccountingSimpleJournalUploadCommand</code> extends the System Upload
 * however has some simple journal specific fields that will be applied
 * to the journals during the insert process.
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class AccountingSimpleJournalUploadCommand extends SystemUploadCommand {

	private Date transactionDate;
	private InvestmentAccountRelationshipPurpose purpose;
	private boolean bookAndPost;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "AccountingSimpleJournal";
	}


	// Overrides - Accounting Simple Journal is all or nothing, do not insert fk beans, and always inserts, never updates


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public InvestmentAccountRelationshipPurpose getPurpose() {
		return this.purpose;
	}


	public void setPurpose(InvestmentAccountRelationshipPurpose purpose) {
		this.purpose = purpose;
	}


	public boolean isBookAndPost() {
		return this.bookAndPost;
	}


	public void setBookAndPost(boolean bookAndPost) {
		this.bookAndPost = bookAndPost;
	}
}
