package com.clifton.accounting.simplejournal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>AccountingSimpleJournalType</code> class defines types of simple journals
 * along with optional rules that can pre-populate and enforce simple journal attributes.
 * <p/>
 * Each simple journal always generates 2 general ledger entries: main row and offsetting row.
 * The sum of debits and credits across both rows is always 0.
 * <p/>
 * Examples of types:
 * - Cash Contribution: main account is "Cash", offsetting account is "Contribution" and both use base currency.
 * - Cash Distribution: ...
 * - Interest Cash Payment: offsetting account is "Interest Income"
 * - Management Fee Cash Payment: offsetting account is "Management Fee"
 * - Other: none of the attributes are set and it allows for most flexibility
 *
 * @author vgomelsky
 */
@CacheByName
public class AccountingSimpleJournalType extends NamedEntity<Short> {

	private AccountingAccount mainAccountingAccount;
	private AccountingAccount mainCurrencyAccountingAccount;
	private boolean mainSecurityBaseCurrency;
	private String mainLabel;

	private AccountingAccount offsettingAccountingAccount;
	private AccountingAccount offsettingCurrencyAccountingAccount;
	private boolean offsettingSecurityBaseCurrency;
	private String offsettingLabel;

	/**
	 * Specifies whether to Debit or Credit journal amount to the main accounting account.
	 * The offsetting accounting account gets the reverse.
	 */
	private boolean creditMainAccountingAccount;

	/**
	 * Specifies whether both main and offsetting sides must have the same holding account.
	 */
	private boolean sameHoldingAccount;
	/**
	 * Specifies whether both main and offsetting sides must have the same investment security.
	 */
	private boolean sameSecurity;

	/**
	 * Allow only a positive amount that is greater than 0 for the journal.
	 */
	private boolean positiveAmountOnly;
	/**
	 * Specifies whether description field on AccountingSimpleJournal should be required
	 */
	private boolean descriptionRequired;
	/**
	 * System generated journals cannot be entered by a user
	 */
	private boolean systemGenerated;

	/**
	 * If journal description is not specified, the following default description will be used
	 */
	private String defaultJournalDescription;


	public AccountingAccount getMainAccountingAccount() {
		return this.mainAccountingAccount;
	}


	public void setMainAccountingAccount(AccountingAccount mainAccountingAccount) {
		this.mainAccountingAccount = mainAccountingAccount;
	}


	public boolean isMainSecurityBaseCurrency() {
		return this.mainSecurityBaseCurrency;
	}


	public void setMainSecurityBaseCurrency(boolean mainSecurityBaseCurrency) {
		this.mainSecurityBaseCurrency = mainSecurityBaseCurrency;
	}


	public AccountingAccount getOffsettingAccountingAccount() {
		return this.offsettingAccountingAccount;
	}


	public void setOffsettingAccountingAccount(AccountingAccount offsettingAccountingAccount) {
		this.offsettingAccountingAccount = offsettingAccountingAccount;
	}


	public boolean isOffsettingSecurityBaseCurrency() {
		return this.offsettingSecurityBaseCurrency;
	}


	public void setOffsettingSecurityBaseCurrency(boolean offsettingSecurityBaseCurrency) {
		this.offsettingSecurityBaseCurrency = offsettingSecurityBaseCurrency;
	}


	public boolean isSystemGenerated() {
		return this.systemGenerated;
	}


	public void setSystemGenerated(boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}


	public boolean isDescriptionRequired() {
		return this.descriptionRequired;
	}


	public void setDescriptionRequired(boolean descriptionRequired) {
		this.descriptionRequired = descriptionRequired;
	}


	public boolean isSameHoldingAccount() {
		return this.sameHoldingAccount;
	}


	public void setSameHoldingAccount(boolean sameHoldingAccount) {
		this.sameHoldingAccount = sameHoldingAccount;
	}


	public boolean isSameSecurity() {
		return this.sameSecurity;
	}


	public void setSameSecurity(boolean sameSecurity) {
		this.sameSecurity = sameSecurity;
	}


	public boolean isCreditMainAccountingAccount() {
		return this.creditMainAccountingAccount;
	}


	public void setCreditMainAccountingAccount(boolean creditMainAccountingAccount) {
		this.creditMainAccountingAccount = creditMainAccountingAccount;
	}


	public boolean isPositiveAmountOnly() {
		return this.positiveAmountOnly;
	}


	public void setPositiveAmountOnly(boolean positiveAmountOnly) {
		this.positiveAmountOnly = positiveAmountOnly;
	}


	public String getMainLabel() {
		return this.mainLabel;
	}


	public void setMainLabel(String mainLabel) {
		this.mainLabel = mainLabel;
	}


	public String getOffsettingLabel() {
		return this.offsettingLabel;
	}


	public void setOffsettingLabel(String offsettingLabel) {
		this.offsettingLabel = offsettingLabel;
	}


	public String getDefaultJournalDescription() {
		return this.defaultJournalDescription;
	}


	public void setDefaultJournalDescription(String defaultJournalDescription) {
		this.defaultJournalDescription = defaultJournalDescription;
	}


	public AccountingAccount getMainCurrencyAccountingAccount() {
		return this.mainCurrencyAccountingAccount;
	}


	public void setMainCurrencyAccountingAccount(AccountingAccount mainCurrencyAccountingAccount) {
		this.mainCurrencyAccountingAccount = mainCurrencyAccountingAccount;
	}


	public AccountingAccount getOffsettingCurrencyAccountingAccount() {
		return this.offsettingCurrencyAccountingAccount;
	}


	public void setOffsettingCurrencyAccountingAccount(AccountingAccount offsettingCurrencyAccountingAccount) {
		this.offsettingCurrencyAccountingAccount = offsettingCurrencyAccountingAccount;
	}
}
