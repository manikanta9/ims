package com.clifton.accounting.simplejournal.booking;


import com.clifton.accounting.gl.booking.processor.SameRulesBookingProcessor;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalService;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>AccountingSimpleJournalBookingProcessor</code> class is a booking processor for accounting
 * journals of type "Simple Journal".  Simple journals always generate two detail: main and offsetting.
 *
 * @author vgomelsky
 */
public class AccountingSimpleJournalBookingProcessor extends SameRulesBookingProcessor<AccountingSimpleJournal> {

	public static final String JOURNAL_NAME = "Simple Journal";

	private AccountingSimpleJournalService accountingSimpleJournalService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return JOURNAL_NAME;
	}


	@Override
	public AccountingSimpleJournal getBookableEntity(Serializable id) {
		return getAccountingSimpleJournalService().getAccountingSimpleJournal((Integer) id);
	}


	@Override
	public SimpleBookingSession<AccountingSimpleJournal> newBookingSession(AccountingJournal journal, AccountingSimpleJournal simpleJournal) {
		if (simpleJournal == null) {
			simpleJournal = getBookableEntity(journal.getFkFieldId());
			ValidationUtils.assertNotNull(simpleJournal, "Cannot find simple journal with id = " + journal.getFkFieldId());
		}

		return new SimpleBookingSession<>(journal, simpleJournal);
	}


	@Override
	public AccountingSimpleJournal markBooked(AccountingSimpleJournal simpleJournal) {
		ValidationUtils.assertNull(simpleJournal.getBookingDate(),
				"Cannot book Simple Journal with id = " + simpleJournal.getId() + " because it already has bookingDate set to " + simpleJournal.getBookingDate());
		simpleJournal.setBookingDate(new Date());
		return getAccountingSimpleJournalService().saveAccountingSimpleJournal(simpleJournal);
	}


	@Override
	@Transactional
	public AccountingSimpleJournal markUnbooked(int simpleJournalId) {
		AccountingSimpleJournal simpleJournal = getBookableEntity(simpleJournalId);
		simpleJournal.setBookingDate(null);
		return getAccountingSimpleJournalService().saveAccountingSimpleJournal(simpleJournal);
	}


	@Override
	public void deleteSourceEntity(int simpleJournalId) {
		// cannot delete an entity that's marked as booked (clear booked status first)
		getAccountingSimpleJournalService().deleteAccountingSimpleJournal(simpleJournalId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingSimpleJournalService getAccountingSimpleJournalService() {
		return this.accountingSimpleJournalService;
	}


	public void setAccountingSimpleJournalService(AccountingSimpleJournalService accountingSimpleJournalService) {
		this.accountingSimpleJournalService = accountingSimpleJournalService;
	}
}
