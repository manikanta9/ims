package com.clifton.accounting.gl.booking.session;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.position.AccountingPosition;

import java.util.List;


/**
 * The <code>PositionAwareBookingSession</code> class is a BookingSession that is used during
 * booking of position aware transaction sets. It maintains a List of currently open positions
 * that booking process can use (add entries based on current positions: dividend/coupon payment;
 * or add closing entries for existing position).
 * <p/>
 * Booking rules that close positions, should make corresponding updates to those positions.
 *
 * @param <T>
 * @author vgomelsky
 */
public class PositionAwareBookingSession<T extends BookableEntity> extends SimpleBookingSession<T> {

	private final List<AccountingPosition> openPositionList;


	public PositionAwareBookingSession(AccountingJournal journal, T bookableEntity, List<AccountingPosition> openPositionList) {
		super(journal, bookableEntity);
		this.openPositionList = openPositionList;
	}


	public List<AccountingPosition> getOpenPositionList() {
		return this.openPositionList;
	}
}
