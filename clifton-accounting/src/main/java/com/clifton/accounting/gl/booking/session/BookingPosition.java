package com.clifton.accounting.gl.booking.session;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;


/**
 * The <code>BookingPosition</code> class represents AccountingPosition that was open at the beginning of the
 * booking session and can be partially or fully closed by the end of the session.
 *
 * @author vgomelsky
 */
public class BookingPosition {

	// unmodified position that was open at the beginning of booking session
	private final AccountingPosition position;

	// remaining amounts from open "position" after applying session closing
	private BigDecimal remainingQuantity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append('{');
		result.append(this.position);
		result.append(", remainingQuantity = ");
		result.append(CoreMathUtils.formatNumberDecimal(this.remainingQuantity));
		result.append('}');
		return result.toString();
	}


	public BookingPosition(AccountingPosition position) {
		this.position = position;
		this.remainingQuantity = position.getRemainingQuantity();
	}


	public AccountingPosition getPosition() {
		return this.position;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}
}
