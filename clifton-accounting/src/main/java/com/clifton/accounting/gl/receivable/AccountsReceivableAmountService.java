package com.clifton.accounting.gl.receivable;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.receivable.search.AccountsReceivableAmountSearchForm;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


public interface AccountsReceivableAmountService {

	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountsReceivableAmount> getAccountsReceivableAmountList(final AccountsReceivableAmountSearchForm searchForm);
}
