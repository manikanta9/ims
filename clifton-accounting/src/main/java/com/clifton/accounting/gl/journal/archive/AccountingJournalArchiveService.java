package com.clifton.accounting.gl.journal.archive;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * <code>AccountingJournalArchiveService</code> interface defines methods related to
 * archiving {@link AccountingJournal}s.
 *
 * @author NickK
 */
public interface AccountingJournalArchiveService {

	/**
	 * Archives the provided {@link AccountingJournal}. To use the posting date as
	 * the archive date, pass true for usePostingDate. Returns the {@link ArchiveEntityDescriptor}
	 * used to archive the journal.
	 */
	@DoNotAddRequestMapping
	public ArchiveEntityDescriptor archiveAccountingJournal(AccountingJournal journal, boolean usePostingDate);


	/**
	 * Returns a real-time generated archive representation for the {@link AccountingJournal} with the provided ID.
	 */
	@SecureMethod(dtoClass = AccountingJournal.class)
	public ArchiveEntityDescriptor getAccountingJournalArchiveEntityDescriptorPreview(long id);


	/**
	 * Returns the list of {@link ArchiveEntityDescriptor}s for {@link AccountingJournal} with the provided ID.
	 */
	@SecureMethod(dtoClass = AccountingJournal.class)
	public List<ArchiveEntityDescriptor> getAccountingJournalArchiveEntityDescriptorList(long id);


	/**
	 * Archives all of the {@link AccountingJournal}s for the {@link AccountingJournalType} with the provided id.
	 * TODO remove this after historic archival is complete
	 */
	@RequestMapping("accountingJournalArchiveOfType")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public Status archiveAccountingJournalOfType(short id);
}
