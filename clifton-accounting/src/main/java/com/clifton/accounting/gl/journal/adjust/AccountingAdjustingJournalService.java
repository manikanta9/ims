package com.clifton.accounting.gl.journal.adjust;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;


/**
 * The <code>AccountingAdjustingJournalService</code> interface defines methods that create various types of adjusting journals:
 * reversal, price change, etc. Because GL entries cannot be modified, any adjustment must be done via a new journal that can
 * "cancel" existing entries and "replace" them with new ones.
 *
 * @author vgomelsky
 */
public interface AccountingAdjustingJournalService {

	/**
	 * Creates an adjusting journal for the specified closing non-currency position that adjusts Realized Gain/Loss for that position
	 * and creates a corresponding payment.
	 */
	@RequestMapping("accountingJournalAdjustRealized")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal adjustAccountingJournalRealized(long accountingPositionId, BigDecimal realizedAdjustmentAmount, String note);


	/**
	 * Creates an adjusting journal for the specified closing non-currency position that adjusts Commission for that position
	 * and creates a corresponding payment.
	 */
	@RequestMapping("accountingJournalAdjustCommission")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal adjustAccountingJournalCommission(long accountingPositionId, BigDecimal commissionAmount, String note);


	/**
	 * Creates an adjusting journal for the specified coupon payment transaction (Interest Income) and the specified amount.
	 * Finds corresponding coupon event and creates Interest Income and Cash (or Currency for foreign positions) entries on Payment Date.
	 * NOTE: does not create accrual/reversal entries that could be in closed accounting period for long delay bonds.
	 */
	@RequestMapping("accountingJournalAdjustCouponPayment")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal adjustAccountingJournalCouponPayment(long couponTransactionId, BigDecimal adjustmentAmount);


	/**
	 * Creates an adjusting journal for the specified closing non-currency position that adjusts Dividend for that position
	 * and creates a corresponding payment.
	 */
	@RequestMapping("accountingJournalAdjustDividend")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal adjustAccountingJournalDividend(long dividendTransactionId, BigDecimal adjustmentAmount, String note);
}
