package com.clifton.accounting.gl.journal.action;

import com.clifton.accounting.gl.journal.action.search.AccountingJournalActionSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class AccountingJournalActionServiceImpl implements AccountingJournalActionService {

	private AdvancedUpdatableDAO<AccountingJournalAction, Criteria> accountingJournalActionDAO;


	private DaoSingleKeyListCache<AccountingJournalAction, Short> accountingJournalActionListByJournalTypeCache;


	////////////////////////////////////////////////////////////////////////////
	///////       Investment Security Event Action Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingJournalAction getAccountingJournalAction(short id) {
		return getAccountingJournalActionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingJournalAction> getAccountingJournalActionListByJournalType(short journalTypeId) {
		return getAccountingJournalActionListByJournalTypeCache().getBeanListForKeyValue(getAccountingJournalActionDAO(), journalTypeId);
	}


	@Override
	public List<AccountingJournalAction> getAccountingJournalActionList(AccountingJournalActionSearchForm searchForm) {
		return getAccountingJournalActionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingJournalAction saveAccountingJournalAction(AccountingJournalAction eventAction) {
		return getAccountingJournalActionDAO().save(eventAction);
	}


	@Override
	public void deleteAccountingJournalAction(short id) {
		getAccountingJournalActionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingJournalAction, Criteria> getAccountingJournalActionDAO() {
		return this.accountingJournalActionDAO;
	}


	public void setAccountingJournalActionDAO(AdvancedUpdatableDAO<AccountingJournalAction, Criteria> accountingJournalActionDAO) {
		this.accountingJournalActionDAO = accountingJournalActionDAO;
	}


	public DaoSingleKeyListCache<AccountingJournalAction, Short> getAccountingJournalActionListByJournalTypeCache() {
		return this.accountingJournalActionListByJournalTypeCache;
	}


	public void setAccountingJournalActionListByJournalTypeCache(DaoSingleKeyListCache<AccountingJournalAction, Short> accountingJournalActionListByJournalTypeCache) {
		this.accountingJournalActionListByJournalTypeCache = accountingJournalActionListByJournalTypeCache;
	}
}
