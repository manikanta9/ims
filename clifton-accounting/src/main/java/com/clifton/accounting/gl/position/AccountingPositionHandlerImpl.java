package com.clifton.accounting.gl.position;


import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl.AccountingAccountIds;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.receivable.AccountsReceivableAmount;
import com.clifton.accounting.gl.receivable.AccountsReceivableAmountService;
import com.clifton.accounting.gl.receivable.search.AccountsReceivableAmountSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentObjectInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * The <code>AccountingPositionHandlerImpl</code> class provides basic implementation of the AccountingPositionHandler interface.
 *
 * @author manderson
 */
@Component
public class AccountingPositionHandlerImpl implements AccountingPositionHandler {

	public static final String INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_ON = "Positions On";
	public static final String INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_OFF = "Positions Off";

	////////////////////////////////////////////////////////////////////////////////

	private SqlHandler sqlHandler;

	private AccountingAccountIdsCache accountingAccountIdsCache;
	private AccountingTransactionService accountingTransactionService;
	private AccountingEventJournalService accountingEventJournalService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentCalendarService investmentCalendarService;

	private AccountsReceivableAmountService accountsReceivableAmountService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Calculated Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getAccountingPositionPrice(AccountingPosition position, Date date) {
		InvestmentSecurity security = position.getInvestmentSecurity();
		BigDecimal price;
		try {
			// Attempt to retrieve market data price
			price = getMarketDataRetriever().getPriceFlexible(security, date, true);
		}
		catch (ValidationException e) {
			/*
			 * Fallback to using the position price. Situations where this may be acceptable:
			 *
			 * - The requested date is before the security issue date. A bond can be purchased before its issue date as long as it is settled on or after its
			 *   issue date.
			 * - The hierarchy pricing frequency allows fallback to the position price when the security price is missing. This flag is typically active for
			 *   lower pricing frequencies, for which it is more likely that pricing market data for the security has not yet been retrieved. We only want to
			 *   apply this flag if the security is active. If the security is deactivated and past its issue date, then market data should exist already.
			 */
			boolean securityActive = InvestmentUtils.isSecurityActiveOn(security, date);

			// Determine whether the given date is before the security issue date
			boolean securityIssued = securityActive || DateUtils.compare(date, security.getStartDate(), false) >= 0;
			// Determine whether the security is active (may still get market data later) and position-fallback pricing applies for the pricing frequency
			boolean fallbackToPositionPrice = securityActive && security.getInstrument().getHierarchy().getPricingFrequency().isPositionPriceUsedWhenSecurityPriceMissing();

			if (!securityIssued || fallbackToPositionPrice) {
				price = position.getPrice();
			}
			// No fallback source was used; Re-throw original exception
			else {
				throw e;
			}
		}
		return price;
	}


	@Override
	public BigDecimal getAccountingPositionNotional(AccountingPosition position, Date date) {
		return getAccountingPositionNotional(position, date, null);
	}


	@Override
	public BigDecimal getAccountingPositionNotional(AccountingPosition position, Date date, BigDecimal notionalMultiplier) {
		// Accounting Transaction ID = Position.getID()
		InvestmentSecurity security = position.getInvestmentSecurity();
		BigDecimal quantity = position.getRemainingQuantity();
		BigDecimal price = getAccountingPositionPrice(position, date);
		BigDecimal fxRate = getExchangeRateToBaseFlexible(position, date);

		if (notionalMultiplier == null) {
			// TIPS use quantity (face) adjusted by index ratio; default to snapshot date (convention used by most banks)
			notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, date, position.getSettlementDate(), false);
		}
		return MathUtils.multiply(getInvestmentCalculator().calculateNotional(security, price, quantity, notionalMultiplier), fxRate, 2);
	}


	@Override
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date) {
		return getAccountingPositionMarketValue(position, date, null);
	}


	@Override
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date, BigDecimal notionalMultiplier) {
		return getAccountingPositionMarketValue(position, date, false, notionalMultiplier, false);
	}


	@Override
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date, boolean useSettlementDate, BigDecimal notionalMultiplier, boolean includeAccrualLegPayments) {
		// Accounting Transaction ID = Position.getID()
		InvestmentSecurity security = position.getInvestmentSecurity();
		if (notionalMultiplier == null) {
			// TIPS use quantity (face) adjusted by index ratio; default to snapshot date (convention used by most banks)
			notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, date, position.getSettlementDate(), false);
		}

		BigDecimal baseAccrual = BigDecimal.ZERO;
		if (InvestmentUtils.isAccrualSupported(security)) {
			AccrualBasis accrualBasis = AccrualBasis.forSecurity(security, position.getRemainingCostBasis(), position.getRemainingQuantity());
			AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(position, date));
			baseAccrual = getInvestmentCalculator().calculateAccruedInterest(security, accrualBasis, notionalMultiplier, accrualDates);
			if (!MathUtils.isNullOrZero(baseAccrual)) {
				BigDecimal fxRate = getExchangeRateToBaseFlexible(position, date);
				baseAccrual = MathUtils.multiply(baseAccrual, fxRate);
			}
			if (includeAccrualLegPayments && security.getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
				Map<String, BigDecimal> localReceivablesMap = getAccountingPositionReceivableMap(
						position.getClientInvestmentAccount(),
						position.getInvestmentSecurity(),
						date,
						useSettlementDate
				);

				Optional<String> securityEventType1 = Optional.of(security).map(InvestmentSecurity::getInstrument).map(InvestmentInstrument::getHierarchy)
						.map(InvestmentInstrumentHierarchy::getAccrualSecurityEventType).map(InvestmentSecurityEventType::getName);
				Optional<String> securityEventType2 = Optional.of(security).map(InvestmentSecurity::getInstrument).map(InvestmentInstrument::getHierarchy)
						.map(InvestmentInstrumentHierarchy::getAccrualSecurityEventType2).map(InvestmentSecurityEventType::getName);

				BigDecimal fxRate = getExchangeRateToBaseFlexible(position, date);
				if (securityEventType1.isPresent()) {
					BigDecimal accrual1 = localReceivablesMap.getOrDefault(securityEventType1.get(), BigDecimal.ZERO);
					baseAccrual = MathUtils.add(baseAccrual, MathUtils.multiply(accrual1, fxRate));
				}
				if (securityEventType2.isPresent()) {
					BigDecimal accrual2 = localReceivablesMap.getOrDefault(securityEventType2.get(), BigDecimal.ZERO);
					baseAccrual = MathUtils.add(baseAccrual, MathUtils.multiply(accrual2, fxRate));
				}
				if (localReceivablesMap.containsKey(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT)) {
					baseAccrual = MathUtils.add(baseAccrual, localReceivablesMap.get(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
				}
				if (localReceivablesMap.containsKey(AccountingJournalType.TRADE_JOURNAL)) {
					baseAccrual = MathUtils.add(baseAccrual, localReceivablesMap.get(AccountingJournalType.TRADE_JOURNAL));
				}
			}
		}

		// ((( BaseNotional - BaseCostBasis ) + BaseCost ) + BaseAccrual)
		BigDecimal result = getAccountingPositionMarketValueClean(position, date, notionalMultiplier);
		result = MathUtils.add(result, baseAccrual);
		return result;
	}


	@Override
	public BigDecimal getAccountingPositionMarketValueClean(AccountingPosition position, Date date) {
		return getAccountingPositionMarketValueClean(position, date, null);
	}


	@Override
	public BigDecimal getAccountingPositionMarketValueClean(AccountingPosition position, Date date, BigDecimal notionalMultiplier) {
		BigDecimal result = getAccountingPositionNotional(position, date, notionalMultiplier);

		InvestmentSecurity security = position.getInvestmentSecurity();
		if (InvestmentUtils.isNoPaymentOnOpen(security)) {
			// Accounting Transaction ID = Position.getID()
			BigDecimal fxRate = getExchangeRateToBaseFlexible(position, date);

			BigDecimal localCostBasis = position.getRemainingCostBasis();
			BigDecimal baseCostBasis = MathUtils.multiply(localCostBasis, fxRate, 2);
			//(((( BaseNotional - BaseCostBasis )+ BaseCost )
			// Note: BaseCost = 0 for no payment on open securities
			result = MathUtils.subtract(result, baseCostBasis);
		}

		return result;
	}


	@Override
	public Map<String, BigDecimal> getAccountingPositionReceivableMap(InvestmentAccount clientInvestmentAccount, InvestmentSecurity investmentSecurity, Date maximumDate, boolean useSettlementDate) {
		AssertUtils.assertNotNull(investmentSecurity, "Investment Security is required.");
		AssertUtils.assertNotNull(maximumDate, "The transaction or settlement date is required.");
		Integer[] clientInvestmentAccountIds = clientInvestmentAccount == null ? null : new Integer[]{clientInvestmentAccount.getId()};
		return getAccountingPositionReceivableMap(clientInvestmentAccountIds, investmentSecurity, maximumDate, useSettlementDate);
	}


	@Override
	public Map<String, BigDecimal> getAccountingPositionReceivableMap(Integer[] clientInvestmentAccountIds, InvestmentSecurity investmentSecurity, Date maximumDate, boolean useSettlementDate) {
		AccountsReceivableAmountSearchForm searchForm = useSettlementDate ? AccountsReceivableAmountSearchForm.withTradingSecurityAndMaxSettlementDate(investmentSecurity.getId(), maximumDate)
				: AccountsReceivableAmountSearchForm.withTradingSecurityAndMaxTransactionDate(investmentSecurity.getId(), maximumDate);
		if (!CollectionUtils.isEmptyCollectionOrArray(clientInvestmentAccountIds)) {
			searchForm.setClientInvestmentAccountIds(clientInvestmentAccountIds);
		}
		List<AccountsReceivableAmount> receivableAmountList = getAccountsReceivableAmountService().getAccountsReceivableAmountList(searchForm);
		InvestmentSecurity tradingCurrency = InvestmentUtils.getSecurityCurrencyDenomination(investmentSecurity);
		FxRateLookupCache fxRateLookupCache = new FxRateLookupCache(getMarketDataExchangeRatesApiService());
		return receivableAmountList.stream().collect(Collectors.groupingBy(AccountsReceivableAmount::getJournalType,
				Collectors.reducing(BigDecimal.ZERO, r -> adjustReceivableToPositionCurrency(fxRateLookupCache, tradingCurrency, maximumDate, r), BigDecimal::add)));
	}


	private BigDecimal adjustReceivableToPositionCurrency(FxRateLookupCache fxRateLookupCache, InvestmentSecurity tradingCurrency, Date date, AccountsReceivableAmount accountsReceivableAmount) {
		BigDecimal results = accountsReceivableAmount.getLocalDebitCredit();
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(accountsReceivableAmount);
		String toCurrency = tradingCurrency.getSymbol();
		if (StringUtils.isEqual(fromCurrency, toCurrency)) {
			return results;
		}
		BigDecimal fxRate = fxRateLookupCache.getExchangeRate(accountsReceivableAmount.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, date, true);
		return MathUtils.isEqual(BigDecimal.ONE, fxRate) ? results : MathUtils.multiply(results, fxRate, results.scale());
	}


	@Override
	public BigDecimal getAccountingPositionRequiredCollateral(AccountingPosition position, Date date) {
		InvestmentSecurity security = position.getInvestmentSecurity();
		BigDecimal fxRate = getExchangeRateToBaseFlexible(position, date);
		return InvestmentCalculatorUtils.calculateRequiredCollateral(security, position.getHoldingInvestmentAccount(), position.getRemainingQuantity(), fxRate, true);
	}


	private BigDecimal getExchangeRateToBaseFlexible(InvestmentObjectInfo investmentObject, Date date) {
		// all price look ups are flexible: FX is less critical
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(investmentObject);
		String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(investmentObject);
		return getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(investmentObject.fxSourceCompany(), !investmentObject.fxSourceCompanyOverridden(), fromCurrency, toCurrency, date).flexibleLookup());
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Accounting Positions On/Off Methods        ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getAccountingPositionOnDateForClientAccount(int clientAccountId) {
		return getSqlHandler().queryForDate(new SqlSelectCommand(
				"SELECT MIN(TransactionDate) FROM AccountingTransaction " +
						"WHERE AccountingAccountID IN (" + ArrayUtils.toString(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIds.POSITION_ACCOUNTS)) + ")" +
						" AND ClientInvestmentAccountID = ? AND IsDeleted = 0")
				.addIntegerParameterValue(clientAccountId)
		);
	}


	@Override
	public boolean isClientAccountPositionsOnForDate(int clientAccountId, Date date) {
		InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
		searchForm.setEventTypeIds(new Short[]{getInvestmentCalendarService().getInvestmentEventTypeByName(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_ON).getId(), getInvestmentCalendarService().getInvestmentEventTypeByName(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_OFF).getId()});
		searchForm.setInvestmentAccountId(clientAccountId);
		searchForm.addSearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, date);
		searchForm.setOrderBy("eventDate:desc");
		searchForm.setLimit(1);

		// Limit of 1, so wouldn't have more than that
		InvestmentEvent lastEvent = CollectionUtils.getOnlyElement(getInvestmentCalendarService().getInvestmentEventList(searchForm));
		return (lastEvent != null && StringUtils.isEqual(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_ON, lastEvent.getEventType().getName()));
	}


	@Override
	public boolean isClientAccountPositionsOnForDateRange(int clientAccountId, Date startDate, Date endDate) {
		// Move Start/End Date to First/Last Business Day
		startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(DateUtils.addDays(startDate, -1)), 1);
		endDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(DateUtils.addDays(endDate, 1)), -1);

		return getClientAccountPositionsOnDays(clientAccountId, startDate, endDate, null) == DateUtils.getDaysDifferenceInclusive(endDate, startDate);
	}


	@Override
	public int getClientAccountPositionsOnDays(int clientAccountId, Date startDate, Date endDate, StringBuilder positionsOnOffMessage) {
		// Starting with positions on or off
		boolean startPositionsOn = isClientAccountPositionsOnForDate(clientAccountId, DateUtils.addDays(startDate, -1));

		//  Get all events during the period
		InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
		searchForm.setEventTypeIds(new Short[]{getInvestmentCalendarService().getInvestmentEventTypeByName(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_ON).getId(), getInvestmentCalendarService().getInvestmentEventTypeByName(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_OFF).getId()});
		searchForm.setInvestmentAccountId(clientAccountId);
		searchForm.addSearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate);
		searchForm.addSearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate);
		// Sort the List in Order so we step through each one in comparison to previous positions on off event
		searchForm.setOrderBy("eventDate:asc");

		List<InvestmentEvent> eventList = getInvestmentCalendarService().getInvestmentEventList(searchForm);
		if (startPositionsOn && CollectionUtils.isEmpty(eventList)) {
			if (positionsOnOffMessage != null) {
				positionsOnOffMessage.append("Positions On - Full Period");
			}
			return DateUtils.getDaysDifferenceInclusive(endDate, startDate);
		}
		if (!startPositionsOn && CollectionUtils.isEmpty(eventList)) {
			if (positionsOnOffMessage != null) {
				positionsOnOffMessage.append("Positions Off All Period");
			}
			return 0;
		}

		int dayCount = 0;
		Date previousEventDate = startDate;
		for (InvestmentEvent event : eventList) {
			if (StringUtils.isEqual(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_OFF, event.getEventType().getName())) {
				dayCount += DateUtils.getDaysDifference(event.getEventDate(), previousEventDate); // Not Inclusive Here Because Positions are OFF on the Event Date
			}
			startPositionsOn = StringUtils.isEqual(INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_ON, event.getEventType().getName());
			previousEventDate = event.getEventDate();
			if (positionsOnOffMessage != null) {
				positionsOnOffMessage.append(event.getEventType().getName()).append(": ").append(DateUtils.fromDateShort(event.getEventDate())).append("; ");
			}
		}
		if (startPositionsOn) {
			dayCount += DateUtils.getDaysDifferenceInclusive(endDate, previousEventDate); // Inclusive because positions on
		}
		return dayCount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public AccountsReceivableAmountService getAccountsReceivableAmountService() {
		return this.accountsReceivableAmountService;
	}


	public void setAccountsReceivableAmountService(AccountsReceivableAmountService accountsReceivableAmountService) {
		this.accountsReceivableAmountService = accountsReceivableAmountService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
