package com.clifton.accounting.gl.booking;

import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;


/**
 * The BookingPreviewCommand class defines parameters that are used during journal preview booking.
 *
 * @author vgomelsky
 */
public class BookingPreviewCommand<T extends BookableEntity> {

	T bookableEntity;

	String journalTypeName;
	/**
	 * Optionally allows to limit rule execution to only the rules that support this scope.
	 */
	BookingRuleScopes bookingRuleScope;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BookingPreviewCommand() {
		// use static constructors instead
	}


	/**
	 * Constructs and returns a new command for the specified arguments. Includes all scopes.
	 */
	public static <T extends BookableEntity> BookingPreviewCommand<T> of(T bookableEntity, String journalTypeName) {
		BookingPreviewCommand<T> result = new BookingPreviewCommand<>();
		result.bookableEntity = bookableEntity;
		result.journalTypeName = journalTypeName;
		return result;
	}


	/**
	 * Constructs and returns a new command for the specified arguments. Preview will be limited to the rules that match the specified scope.
	 */
	public static <T extends BookableEntity> BookingPreviewCommand<T> ofScope(T bookableEntity, String journalTypeName, BookingRuleScopes bookingRuleScope) {
		BookingPreviewCommand<T> result = of(bookableEntity, journalTypeName);
		result.bookingRuleScope = bookingRuleScope;
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public T getBookableEntity() {
		return this.bookableEntity;
	}


	public String getJournalTypeName() {
		return this.journalTypeName;
	}


	public BookingRuleScopes getBookingRuleScope() {
		return this.bookingRuleScope;
	}


	/**
	 * Returns true if no scope is defined for this command or, if the scope is defined, then the rule matches the scope.
	 */
	public boolean isBookingRuleInScope(AccountingBookingRule<T> bookingRule) {
		if (this.bookingRuleScope == null) {
			return true;
		}
		return bookingRule.getRuleScopes().contains(this.bookingRuleScope);
	}
}
