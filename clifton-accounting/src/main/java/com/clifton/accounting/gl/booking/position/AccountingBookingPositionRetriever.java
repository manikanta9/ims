package com.clifton.accounting.gl.booking.position;

import com.clifton.accounting.gl.booking.session.BookingPosition;

import java.util.List;


/**
 * @author TerryS
 */
public interface AccountingBookingPositionRetriever {

	public List<BookingPosition> getAccountingBookingOpenPositionList(AccountingBookingPositionCommand command);
}
