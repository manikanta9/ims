package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationProperties;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculator;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;


/**
 * The <code>AccountingCommissionBookingRule</code> adds a "Commission" entry and corresponding "Cash" or "Currency"
 * entry to the booking session journal for each "Position" detail that the commission applies to according to commission rules.
 *
 * @param <T>
 */
public class AccountingCommissionBookingRule<T extends BookableEntity> extends AccountingCommonBookingRule<T> implements AccountingBookingRule<T> {

	private AccountingCommissionCalculator accountingCommissionCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> detailRecords = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailRecords)) {
			AccountingAccount accountingAccount = detail.getAccountingAccount();
			if (accountingAccount.isPosition() && detail.isParentDefinitionEmpty()) {
				applyAutomatedCommissions(new AccountingCommissionCalculationProperties<>(detail, bookingSession.getBookableEntity()));
			}
		}
	}


	/**
	 * @see {@link #applyAutomatedCommissions(AccountingCommissionCalculationProperties, BigDecimal, Set)}
	 */
	private void applyAutomatedCommissions(AccountingCommissionCalculationProperties<T> commissionCalculationProperties) {
		applyAutomatedCommissions(commissionCalculationProperties, null, null);
	}


	/**
	 * Generates appropriate commissions and fees for the specified detail using system defined schedules.
	 * Creates corresponding expense and cash entries and adds them to the journal.
	 *
	 * @param commissionCalculationProperties the properties used to calculate the commissions and fees
	 * @param exchangeRateToBase              the exchange rate from the security's currency denomination to the client's base currency (optional)
	 * @param accountingAccountsToSkip        - fee types included in this set will be skipped
	 */
	protected void applyAutomatedCommissions(AccountingCommissionCalculationProperties<T> commissionCalculationProperties, BigDecimal exchangeRateToBase, Set<AccountingAccount> accountingAccountsToSkip) {
		List<AccountingCommission> commissionsToApply = getAccountingCommissionCalculator().getAccountingCommissionListForJournalDetail(commissionCalculationProperties);
		for (AccountingCommission commissionToApply : CollectionUtils.getIterable(commissionsToApply)) {
			if (!MathUtils.isNullOrZero(commissionToApply.getCommissionAmount())) {
				if (!CollectionUtils.contains(accountingAccountsToSkip, commissionToApply.getAccountingAccount())) {
					createJournalDetails(commissionCalculationProperties.getBookableEntity(), commissionCalculationProperties.getPositionDetail(), commissionToApply, exchangeRateToBase);
				}
			}
		}
	}


	/**
	 * @param exchangeRateFromSecurityDenominationToBase the exchange rate from the security's currency denomination to the client account's base currency (optional)
	 */
	protected void createJournalDetails(T bookableEntity, AccountingJournalDetailDefinition positionDetail, AccountingCommission commission, BigDecimal exchangeRateFromSecurityDenominationToBase) {
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(positionDetail);
		InvestmentSecurity securityCurrencyDenomination = positionDetail.getInvestmentSecurity().getInstrument().getTradingCurrency();

		/*
		 * We set 'commissionPaymentCurrency' to 'commission.getCurrency()', which at this point will be one of the
		 * following: - The explicitly-specified currency for the the commission override or commission schedule (if
		 * specified)
		 *
		 * - Otherwise, if currency is not explicitly specified in the commission AND the transaction is a trade, then
		 * it is the settlement currency of the trade (which is usually the currency denomination of the security, but
		 * in some cases is the base currency of the client account)
		 *
		 * - Otherwise, if currency is not explicitly specified in the commission AND the transaction is NOT a trade,
		 * then it is the currency denomination of the security
		 */
		InvestmentSecurity commissionPaymentCurrency = commission.getCurrency();
		if (commissionPaymentCurrency == null) {
			if (bookableEntity instanceof AccountingBean) {
				commissionPaymentCurrency = ((AccountingBean) bookableEntity).getSettlementCurrency();
			}
			else {
				commissionPaymentCurrency = securityCurrencyDenomination;
			}
		}

		//Determine exchange rate from security currency denomination to base if not specified
		if (exchangeRateFromSecurityDenominationToBase == null) {
			if (securityCurrencyDenomination.equals(baseCurrency)) {
				exchangeRateFromSecurityDenominationToBase = BigDecimal.ONE;
			}
			else {
				// need to lookup latest FX rate from FX Source
				exchangeRateFromSecurityDenominationToBase = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(positionDetail.fxSourceCompany(), !positionDetail.fxSourceCompanyOverridden(), securityCurrencyDenomination.getSymbol(), baseCurrency.getSymbol(), positionDetail.getTransactionDate()).flexibleLookup());
			}
		}

		//Conversion rate from 'commissionPaymentCurrency' to 'baseCurrency'
		BigDecimal commissionPaymentFX;

		if (commissionPaymentCurrency.equals(baseCurrency)) {
			commissionPaymentFX = BigDecimal.ONE;
		}
		else if (commissionPaymentCurrency.equals(securityCurrencyDenomination)) {
			commissionPaymentFX = exchangeRateFromSecurityDenominationToBase;
		}
		else {
			// need to lookup latest FX rate from clearing company
			commissionPaymentFX = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(positionDetail.fxSourceCompany(), !positionDetail.fxSourceCompanyOverridden(), commissionPaymentCurrency.getSymbol(), baseCurrency.getSymbol(), positionDetail.getTransactionDate()).flexibleLookup());
		}

		// May need to convert fee amount for commission entry
		BigDecimal feeAmountInCommissionCurrency = commission.getCommissionAmount();
		if (!securityCurrencyDenomination.equals(commissionPaymentCurrency)) {
			//First multiply the commission amount by the exchange rate of the commission payment currency (which gives us the amount in the base currency)
			feeAmountInCommissionCurrency = InvestmentCalculatorUtils.calculateBaseAmount(commission.getCommissionAmount(), commissionPaymentFX, commissionPaymentCurrency); // base amount

			//Then divide that about by the commission security's currency denomination exchange rate
			feeAmountInCommissionCurrency = InvestmentCalculatorUtils.calculateBaseAmount(feeAmountInCommissionCurrency, MathUtils.divide(BigDecimal.ONE, exchangeRateFromSecurityDenominationToBase), baseCurrency);
		}

		// ready to generate commission and offsetting journal details and add them to the journal
		AccountingJournal journal = positionDetail.getJournal();
		AccountingJournalDetailDefinition commissionDetail = createCommissionExpenseRecord(positionDetail, feeAmountInCommissionCurrency, commission.getAccountingAccount(), exchangeRateFromSecurityDenominationToBase);
		journal.addJournalDetail(commissionDetail);
		journal.addJournalDetail(createCashCurrencyRecord(commissionDetail, commissionPaymentCurrency, commission.getCommissionAmount().negate(), commissionPaymentFX));
	}


	private AccountingJournalDetail createCommissionExpenseRecord(AccountingJournalDetailDefinition positionEntry, BigDecimal commissionAmount, AccountingAccount commissionAccount, BigDecimal exchangeRateToBase) {
		AccountingJournalDetail commissionExpenseEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(positionEntry, commissionExpenseEntry, new String[]{"parentTransaction"});

		commissionExpenseEntry.setParentDefinition(positionEntry);

		BigDecimal commissionQuantity = commissionExpenseEntry.getQuantity().abs();

		commissionExpenseEntry.setExchangeRateToBase(exchangeRateToBase);

		commissionExpenseEntry.setQuantity(positionEntry.getQuantity() == null ? null : positionEntry.getQuantity().abs());
		commissionExpenseEntry.setAccountingAccount(commissionAccount);

		//The concept of "opening" isn't really applicable for commissions, but we typically set non-position entries such as this to opening=true
		commissionExpenseEntry.setOpening(true);

		//Local debit/credit is total expense
		commissionExpenseEntry.setLocalDebitCredit(commissionAmount);

		//Base debit/credit is the local debit/credit multiplied by the exchange rate to base
		commissionExpenseEntry.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(commissionAmount, commissionExpenseEntry.getExchangeRateToBase(), commissionExpenseEntry.getClientInvestmentAccount()));

		//Price is amount per unit
		if (MathUtils.isGreaterThan(commissionAmount, BigDecimal.ZERO)) {
			//"unrounded" calculated value
			BigDecimal rawAmountPerUnit = MathUtils.divide(commissionAmount, commissionQuantity);
			//Rounds the raw amount per unit to the smallest possible precision while still calculating to the original localDebitCredit
			Predicate<BigDecimal> roundingTestCondition = testValue -> MathUtils.isEqual(commissionAmount, MathUtils.multiply(testValue, commissionQuantity, commissionAmount.scale()));
			commissionExpenseEntry.setPrice(MathUtils.roundToSmallestPrecision(rawAmountPerUnit, roundingTestCondition));
		}
		else {
			commissionExpenseEntry.setPrice(null);
		}

		//We set position-specific fields to 0 since this is a non-position entry
		commissionExpenseEntry.setPositionCommission(BigDecimal.ZERO);
		commissionExpenseEntry.setPositionCostBasis(BigDecimal.ZERO);

		commissionExpenseEntry.setDescription(commissionAccount.getName() + " expense for " + positionEntry.getInvestmentSecurity().getSymbol());

		return commissionExpenseEntry;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingCommissionCalculator getAccountingCommissionCalculator() {
		return this.accountingCommissionCalculator;
	}


	public void setAccountingCommissionCalculator(AccountingCommissionCalculator accountingCommissionCalculator) {
		this.accountingCommissionCalculator = accountingCommissionCalculator;
	}
}
