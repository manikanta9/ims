package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;


/**
 * The <code>AccountingCloseAtCostBookingRule</code> will set the price, original transaction date (if
 * specified) and the exchange rate. The debit/credit and cost basis must be set correctly before this rule.
 * <p/>
 * This rule is used by REPO's to transfer a position at cost to and from the REPO account.
 * <p/>
 * Typically the <code>AccountingRealizedGainLossBookingRule</code> which will set the correct debit/credits and cost basis,
 * and then <code>AccountingRemoveJournalDetailsBookingRule</code> to remove the realized entries must be run before this rule.
 *
 * @author mwacker
 */
public class AccountingCloseAtCostBookingRule<T extends BookableEntity> implements AccountingBookingRule<T> {

	private boolean useOriginalTransactionDate = true;


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		for (AccountingJournalDetailDefinition positionEntry : CollectionUtils.getIterable(journal.getJournalDetailList())) {
			if (!positionEntry.getAccountingAccount().isCurrency() && positionEntry.isClosing()) {
				setJournalToCost((AccountingJournalDetail) positionEntry);
			}
		}
	}


	private void setJournalToCost(AccountingJournalDetail positionEntry) {
		if (positionEntry.getParentTransaction() != null) {
			positionEntry.setPrice(positionEntry.getParentTransaction().getPrice());
			positionEntry.setExchangeRateToBase(positionEntry.getParentTransaction().getExchangeRateToBase());

			if (isUseOriginalTransactionDate()) {
				positionEntry.setOriginalTransactionDate(positionEntry.getParentTransaction().getOriginalTransactionDate());
			}
		}
	}


	public boolean isUseOriginalTransactionDate() {
		return this.useOriginalTransactionDate;
	}


	public void setUseOriginalTransactionDate(boolean useOriginalTransactionDate) {
		this.useOriginalTransactionDate = useOriginalTransactionDate;
	}
}
