package com.clifton.accounting.gl.booking.rule;

/**
 * The BookingRuleScopes enum is used to define scopes for {@link AccountingBookingRule} implementations.
 * Scopes can be used to limit rule execution to only those that match a given scope.
 * This can be used during specific "preview" actions in order to improve performance and avoid infinite recursions.
 *
 * @author vgomelsky
 */
public enum BookingRuleScopes {

	/**
	 * Identifies booking rules that can create, remove, or update position journal details.
	 */
	POSITION_IMPACT
}
