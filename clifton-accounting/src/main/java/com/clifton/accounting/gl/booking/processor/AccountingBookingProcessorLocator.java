package com.clifton.accounting.gl.booking.processor;

import com.clifton.accounting.gl.booking.BookableEntity;


/**
 * The <code>AccountingBookingProcessorLocator</code> interface defines a method for locating AccountingBookingProcessor objects
 * that are registered for each accounting journal types.
 *
 * @author vgomelsky
 */
public interface AccountingBookingProcessorLocator {

	/**
	 * Returns AccountingBookingProcessor registered for the specified journal type name.
	 */
	public AccountingBookingProcessor<? extends BookableEntity> getAccountingBookingProcessor(String journalTypeName);
}
