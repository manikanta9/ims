package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.bean.SystemBeanType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link AccountingPositionValueCalculator} is a {@link SystemBeanGroup} interface which provides methods to calculate values based on positions.
 * <p>
 * {@link SystemBeanType} implementations of this interface may be configured to produce various types of values from positions, such as {@link
 * AccountingPositionNotionalValueCalculator notional values}, {@link AccountingPositionMarketValueCalculator market values}, or {@link AccountingPositionFixedValueCalculator fixed
 * values}.
 *
 * @author MikeH
 */
public interface AccountingPositionValueCalculator {

	/**
	 * Calculates the value for the given position at the given snapshot date.
	 *
	 * @param position     the position to process for calculation
	 * @param snapshotDate the date for which the calculation shall be executed
	 * @return the calculated value
	 */
	public BigDecimal calculate(AccountingPosition position, Date snapshotDate);
}
