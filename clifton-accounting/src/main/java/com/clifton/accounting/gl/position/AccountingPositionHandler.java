package com.clifton.accounting.gl.position;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentPricingFrequencies;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * The <code>AccountingPositionHandler</code> interface defines internal methods for working with accounting positions (open GL positions)
 * that are not exposed to the UI.
 *
 * @author manderson
 */
public interface AccountingPositionHandler {

	////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Calculated Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the security price for the position on the given date. This attempts to retrieve the price using market data for the given date. If price market
	 * data is not available, then the position price may be used under any of the following conditions:
	 * <ul>
	 * <li>The security has not yet been issued by the specified date
	 * <li>The security is active and no market data exists, but the pricing frequency allows fallbacks to the position price (see
	 * {@link InvestmentPricingFrequencies#isPositionPriceUsedWhenSecurityPriceMissing()})
	 * </ul>
	 *
	 * @param position the position whose price should be returned
	 * @param date     the date for which to retrieve the price
	 * @return the price for the position
	 */
	public BigDecimal getAccountingPositionPrice(AccountingPosition position, Date date);


	/**
	 * Returns the notional of a given accounting position in BASE currency
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 */
	public BigDecimal getAccountingPositionNotional(AccountingPosition position, Date date);


	/**
	 * Returns the notional of a given accounting position in BASE currency with a specific notionalMultiplier.
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 */
	public BigDecimal getAccountingPositionNotional(AccountingPosition position, Date date, BigDecimal notionalMultiplier);


	/**
	 * Returns the market value of a given accounting position in BASE currency
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 * <p>
	 * Base Market Value =
	 * ((([BaseNotional]-[BaseCostBasis])+[BaseCost])+[BaseAccrual])
	 */
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date);


	/**
	 * Returns the market value of a given accounting position in BASE currency with a specific notionalMultiplier.
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 * <p>
	 * Base Market Value =
	 * ((([BaseNotional]-[BaseCostBasis])+[BaseCost])+[BaseAccrual])
	 */
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date, BigDecimal notionalMultiplier);


	/**
	 * Returns the market value of a given accounting position in BASE currency with a specific notionalMultiplier and whether to
	 * include accrual leg payments.
	 *
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 * <p>
	 * Base Market Value =
	 * ((([BaseNotional]-[BaseCostBasis])+[BaseCost])+[BaseAccrual])
	 */
	public BigDecimal getAccountingPositionMarketValue(AccountingPosition position, Date date, boolean useSettlementDate, BigDecimal notionalMultiplier, boolean includeAccrualLegPayments);


	/**
	 * Returns the market value of a given accounting position in BASE currency as it's "clean" value
	 * i.e. doesn't include accrued interest
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 */
	public BigDecimal getAccountingPositionMarketValueClean(AccountingPosition position, Date date);


	/**
	 * Returns the market value of a given accounting position in BASE currency with a specific notionalMultiplier
	 * as it's "clean" value i.e. doesn't include accrued interest
	 * <p>
	 * NOTE: Assumes AccountingPosition object is populated
	 */
	public BigDecimal getAccountingPositionMarketValueClean(AccountingPosition position, Date date, BigDecimal notionalMultiplier);


	/**
	 * Get cumulative 'Leg Receivable' ({@link com.clifton.accounting.eventjournal.AccountingEventJournalType#name}) LOCAL amounts.
	 */
	@DoNotAddRequestMapping
	public Map<String, BigDecimal> getAccountingPositionReceivableMap(InvestmentAccount clientInvestmentAccount, InvestmentSecurity investmentSecurity, Date maximumDate, boolean useSettlementDate);


	/**
	 * Get cumulative 'Leg Receivable' ({@link com.clifton.accounting.eventjournal.AccountingEventJournalType#name}) LOCAL amounts.
	 */
	@DoNotAddRequestMapping
	public Map<String, BigDecimal> getAccountingPositionReceivableMap(Integer[] clientInvestmentAccountIds, InvestmentSecurity investmentSecurity, Date maximumDate, boolean useSettlementDate);


	/**
	 * Returns the Required collateral for the given position on the given date.
	 */
	public BigDecimal getAccountingPositionRequiredCollateral(AccountingPosition position, Date date);

	////////////////////////////////////////////////////////////////////////////
	/////////             Accounting Positions On/Off Methods        ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to automate positions on date/account inception date for client accounts.  There is a batch job
	 * that uses this to set inception date on the account if missing and positions are found.
	 */
	public Date getAccountingPositionOnDateForClientAccount(int clientAccountId);


	/**
	 * Returns true if the client account has positions on for the selected date
	 * NOTE: Uses Accounting Investment Calendar Events for Positions On and Positions Off
	 */
	public boolean isClientAccountPositionsOnForDate(int clientAccountId, Date date);


	/**
	 * Returns true if the client account has positions on for the selected date range
	 * If, positions are off for any date (even if just one day) during the range, then returns false.
	 * NOTE: Uses Accounting Investment Calendar Events for Positions On and Positions Off
	 * <p>
	 * If start date is not a business day, will move start date forward to next business day
	 * If end date is not a business day, will move end date backward to previous business day
	 * <p>
	 * The date moves allow cases where the client puts positions on the first business day of the month because
	 * the first day of the month is a weekend/holiday.  The account is still considered positions on for the full period.
	 */
	public boolean isClientAccountPositionsOnForDateRange(int clientAccountId, Date startDate, Date endDate);


	/**
	 * Returns the number of days the client account had positions on during the date range
	 *
	 * @param positionsOnOffMessage can be used to build a string of results for use in violations
	 */
	public int getClientAccountPositionsOnDays(int clientAccountId, Date startDate, Date endDate, StringBuilder positionsOnOffMessage);
}
