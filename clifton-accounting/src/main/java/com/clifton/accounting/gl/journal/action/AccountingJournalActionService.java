package com.clifton.accounting.gl.journal.action;

import com.clifton.accounting.gl.journal.action.search.AccountingJournalActionSearchForm;

import java.util.List;


/**
 * @author vgomelsky
 */
public interface AccountingJournalActionService {


	public AccountingJournalAction getAccountingJournalAction(short id);


	public List<AccountingJournalAction> getAccountingJournalActionListByJournalType(short journalTypeId);


	public List<AccountingJournalAction> getAccountingJournalActionList(AccountingJournalActionSearchForm searchForm);


	public AccountingJournalAction saveAccountingJournalAction(AccountingJournalAction eventAction);


	public void deleteAccountingJournalAction(short id);
}
