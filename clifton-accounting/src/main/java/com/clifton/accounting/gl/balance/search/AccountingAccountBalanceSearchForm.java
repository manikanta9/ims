package com.clifton.accounting.gl.balance.search;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.core.dataaccess.search.form.SearchForm;

import java.util.Date;


/**
 * The <code>AccountingTransactionAnalysisSearchForm</code> is a search form used to
 * define specific parameters when looking up an account balance for a client account
 * <p>
 * Each parameter is specifically handled and SQL is built dynamically for the most efficient lookup
 * <p>
 * Called a "SearchForm" but doesn't implement the standard SearchForm we used for Hibernate because doesn't support
 * those types of searches
 *
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
public class AccountingAccountBalanceSearchForm implements AccountingAccountFilters {

	private Integer clientAccountId;
	private Integer[] clientAccountIds;
	private Integer holdingAccountId;
	private Integer[] holdingAccountIds;

	private Integer clientInvestmentAccountGroupId;

	/**
	 * Specifies whether holding accounts of type "excludedAccount" should be included or not in the result.
	 * Set to false, to exclude them.  True, to include them only.  Null to have both.
	 */
	private Boolean excludedHoldingAccounts;

	/**
	 * If set ignores accountingAccountTypeId and accountingAccountGroupId
	 */
	private Short accountingAccountId;
	private Short[] accountingAccountIds;

	private AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName;

	private Short accountingAccountTypeId;
	private Short[] accountingAccountTypeIds;
	private String accountingAccountType;
	private String[] accountingAccountTypes;

	private Short accountingAccountGroupId;

	private Boolean cashAccountingAccount;
	private Boolean positionAccountingAccount;
	private Boolean collateralAccountingAccount;

	/**
	 * If set - ignores instrumentId, investmentGroupId, and investmentTypeId
	 */
	private Integer securityId;
	/**
	 * If set - ignores investmentGroupId and investmentTypeId
	 */
	private Integer instrumentId;
	/**
	 * If set - ignores investmentTypeId
	 */
	private Short investmentHierarchyId;
	private Short investmentGroupId;
	private Short investmentTypeId;

	private Date transactionDate; // The date to return the balance for (sums transactions <= this date)
	private Date startTransactionDate; // Optional start date to get balance of transactions within a specific date range

	private Date settlementDate; // similar to transactionDate: only one of those filters can be defined
	private Date startSettlementDate; // Optional start date to get balance of transactions within a specific date range

	/**
	 * Optional filter that includes only Transactions with Source Table = Accounting Simple Journal,
	 * INNER JOINS on AccountingSimpleJournal table via FKFieldID = AccountingSimpleJournalID
	 * WHERE AccountingSimpleJournal.AccountingSimpleJournalTypeID = simpleJournalTypeId passed
	 */
	private Short simpleJournalTypeId;

	/**
	 * Exclude GL accounts that are not ours.
	 */
	private Boolean notOurGLAccounts;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	@Override
	public Short[] getAccountingAccountIds() {
		return this.accountingAccountIds;
	}


	@Override
	public void setAccountingAccountIds(Short[] accountingAccountIds) {
		this.accountingAccountIds = accountingAccountIds;
	}


	@Override
	public AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountIdName() {
		return this.accountingAccountIdName;
	}


	@Override
	public void setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName) {
		this.accountingAccountIdName = accountingAccountIdName;
	}


	@Override
	public Short getAccountingAccountTypeId() {
		return this.accountingAccountTypeId;
	}


	@Override
	public void setAccountingAccountTypeId(Short accountingAccountTypeId) {
		this.accountingAccountTypeId = accountingAccountTypeId;
	}


	@Override
	public Short[] getAccountingAccountTypeIds() {
		return this.accountingAccountTypeIds;
	}


	@Override
	public void setAccountingAccountTypeIds(Short[] accountingAccountTypeIds) {
		this.accountingAccountTypeIds = accountingAccountTypeIds;
	}


	@Override
	public String getAccountingAccountType() {
		return this.accountingAccountType;
	}


	@Override
	public void setAccountingAccountType(String accountingAccountType) {
		this.accountingAccountType = accountingAccountType;
	}


	@Override
	public String[] getAccountingAccountTypes() {
		return this.accountingAccountTypes;
	}


	@Override
	public void setAccountingAccountTypes(String[] accountingAccountTypes) {
		this.accountingAccountTypes = accountingAccountTypes;
	}


	public Short getAccountingAccountGroupId() {
		return this.accountingAccountGroupId;
	}


	public void setAccountingAccountGroupId(Short accountingAccountGroupId) {
		this.accountingAccountGroupId = accountingAccountGroupId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Date getStartTransactionDate() {
		return this.startTransactionDate;
	}


	public void setStartTransactionDate(Date startTransactionDate) {
		this.startTransactionDate = startTransactionDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer[] getHoldingAccountIds() {
		return this.holdingAccountIds;
	}


	public void setHoldingAccountIds(Integer[] holdingAccountIds) {
		this.holdingAccountIds = holdingAccountIds;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getStartSettlementDate() {
		return this.startSettlementDate;
	}


	public void setStartSettlementDate(Date startSettlementDate) {
		this.startSettlementDate = startSettlementDate;
	}


	@Override
	public Boolean getCashAccountingAccount() {
		return this.cashAccountingAccount;
	}


	@Override
	public void setCashAccountingAccount(Boolean cashAccountingAccount) {
		this.cashAccountingAccount = cashAccountingAccount;
	}


	@Override
	public Boolean getPositionAccountingAccount() {
		return this.positionAccountingAccount;
	}


	@Override
	public void setPositionAccountingAccount(Boolean positionAccountingAccount) {
		this.positionAccountingAccount = positionAccountingAccount;
	}


	public Boolean getCollateralAccountingAccount() {
		return this.collateralAccountingAccount;
	}


	public void setCollateralAccountingAccount(Boolean collateralAccountingAccount) {
		this.collateralAccountingAccount = collateralAccountingAccount;
	}


	public Boolean getExcludedHoldingAccounts() {
		return this.excludedHoldingAccounts;
	}


	public void setExcludedHoldingAccounts(Boolean excludedHoldingAccounts) {
		this.excludedHoldingAccounts = excludedHoldingAccounts;
	}


	public Short getSimpleJournalTypeId() {
		return this.simpleJournalTypeId;
	}


	public void setSimpleJournalTypeId(Short simpleJournalTypeId) {
		this.simpleJournalTypeId = simpleJournalTypeId;
	}


	public Boolean getNotOurGLAccounts() {
		return this.notOurGLAccounts;
	}


	public void setNotOurGLAccounts(Boolean notOurGLAccounts) {
		this.notOurGLAccounts = notOurGLAccounts;
	}
}
