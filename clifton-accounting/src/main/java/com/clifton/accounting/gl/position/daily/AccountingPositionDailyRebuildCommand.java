package com.clifton.accounting.gl.position.daily;


import com.clifton.accounting.gl.AccountingSnapshotRebuildCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;


/**
 * The <code>AccountingPositionDailyRebuildCommand</code> ...
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class AccountingPositionDailyRebuildCommand extends AccountingSnapshotRebuildCommand {

	// Note: Client Account and Account Group Fields are standard on the preceding command object

	private Short holdingAccountTypeId;
	private Integer holdingCompanyId;
	private Integer holdingAccountId;
	private Integer investmentSecurityId;
	private Short investmentGroupId;


	// option to use Settlement Date vs Transaction Date for determining positions
	private boolean useSettlementDate;


	/**
	 * Will use the T+=1 index ratio for indexRatioAdjusted securities.
	 */
	private boolean useNextDayIndexRatio;


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder("");
		if (getClientAccountId() != null) {
			runId.append("Acct_").append(getClientAccountId());
		}
		else if (getInvestmentAccountGroupId() != null) {
			runId.append("AcctGroup_").append(getInvestmentAccountGroupId());
		}
		else if (getHoldingAccountId() != null) {
			runId.append("HoldAccount_").append(getHoldingAccountId());
		}
		else if (getHoldingCompanyId() != null || getHoldingAccountTypeId() != null) {
			if (getHoldingCompanyId() != null) {
				runId.append("HoldCompany_").append(getHoldingCompanyId());
			}
			if (getHoldingAccountTypeId() != null) {
				runId.append("AcctType_").append(getHoldingAccountTypeId());
			}
		}
		else {
			runId.append("ALL");
		}
		if (getInvestmentSecurityId() != null) {
			runId.append("-Security_").append(getInvestmentSecurityId());
		}
		runId.append("-").append(DateUtils.fromDateShort(getStartSnapshotDate()));
		runId.append("-").append(DateUtils.fromDateShort(getEndSnapshotDate()));
		return runId.toString();
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public boolean isUseNextDayIndexRatio() {
		return this.useNextDayIndexRatio;
	}


	public void setUseNextDayIndexRatio(boolean useNextDayIndexRatio) {
		this.useNextDayIndexRatio = useNextDayIndexRatio;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}
}
