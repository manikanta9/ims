package com.clifton.accounting.gl.pending;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.dao.xml.XmlBasedCriteria;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * The component for managing pending accounting activity. This component includes methods for handling data from pending transactions.
 *
 * @author mwacker
 */
@Component
public class AccountingPendingActivityHandlerImpl implements AccountingPendingActivityHandler, ApplicationContextAware {

	// TODO: Validate that there is only 1 implementation per bookable entity type.
	@Autowired
	private List<AccountingPendingActivityRetriever> accountingPendingActivityRetrieverList;

	private ApplicationContext applicationContext;
	private DaoLocator daoLocator;


	@Override
	@Transactional(readOnly = true, noRollbackFor = ValidationException.class)
	public List<AccountingJournalDetailDefinition> getAccountingPendingTransactionList(final GeneralLedgerSearchForm searchForm) {
		List<AccountingJournalDetailDefinition> transactionList = new ArrayList<>();
		for (AccountingPendingActivityRetriever retriever : CollectionUtils.getIterable(getAccountingPendingActivityRetrieverList())) {
			List<AccountingJournalDetailDefinition> transactionDetails;
			try {
				transactionDetails = retriever.getPendingTransactionList(searchForm);
			}
			catch (Exception e) {
				// Wrap obscure messages that occur during transaction retrieval, such as booking failure messages, for better user comprehension
				throw new ValidationException(String.format("An error occurred while attempting to retrieve pending transactions: %s", e.getMessage()), e);
			}
			if (!CollectionUtils.isEmpty(transactionDetails)) {
				transactionList.addAll(transactionDetails);
			}
		}

		if (searchForm.getTransactionDate() == null) {
			// Settlement Date lookup: filter out future transactions
			transactionList = BeanUtils.filter(transactionList, t -> !DateUtils.isDateAfter(t.getSettlementDate(), searchForm.getSettlementDate()));
		}

		return applySearchFormFilters(transactionList, searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Filters the transaction list using the XmlBaseCriteria and a copy of the original search form.
	 */
	@SuppressWarnings({"unchecked"})
	private List<AccountingJournalDetailDefinition> applySearchFormFilters(List<AccountingJournalDetailDefinition> transactionList, GeneralLedgerSearchForm searchForm) {
		// Guard-clause: Trivial case
		if (CollectionUtils.isEmpty(transactionList)) {
			return transactionList;
		}

		// Clone search form, allowing restriction list re-population via search form configurer
		GeneralLedgerSearchForm xmlSearchForm = BeanUtils.newInstance(searchForm.getClass());
		BeanUtils.copyProperties(searchForm, xmlSearchForm);
		SearchFormUtils.removeDuplicatesFromSearchRestrictionList(xmlSearchForm);

		// Filter list via XML-based criteria
		ReadOnlyDAO<AccountingTransaction> dao = getDaoLocator().locate(AccountingTransaction.class);
		SearchConfigurer<Criteria> searchConfigurer = new HibernateSearchFormConfigurer(xmlSearchForm);
		// Joins should have been filtered out in the transaction list items received from the providers.
		// Using joins here can be a performance concern because the joined table is listed in its entirety.
		@SuppressWarnings("rawtypes")
		XmlBasedCriteria<AccountingJournalDetailDefinition> criteria = (XmlBasedCriteria<AccountingJournalDetailDefinition>) new XmlBasedCriteria(dao.getConfiguration(), getApplicationContext(), true, true, true);
		searchConfigurer.configureCriteria(criteria);
		return criteria.list(transactionList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<AccountingPendingActivityRetriever> getAccountingPendingActivityRetrieverList() {
		return this.accountingPendingActivityRetrieverList;
	}


	public void setAccountingPendingActivityRetrieverList(List<AccountingPendingActivityRetriever> accountingPendingActivityRetrieverList) {
		this.accountingPendingActivityRetrieverList = accountingPendingActivityRetrieverList;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
