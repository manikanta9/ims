package com.clifton.accounting.gl.position;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;


/**
 * Defines available accounting closing methods for positions. Enables sorting positions according to the specified method.
 * <p>
 * Other cost basis accounting methods to consider: LOFO (Lowest Cost, First Out), LGUT (Loss Gain Utilization), SLID (Specific Lot Identification), Average Cost.
 *
 * @author vgomelsky
 */
public enum AccountingPositionOrders {

	/**
	 * Highest In First Out then Original Transaction Date, then lowest transaction id.
	 * Highest Cost (cost per unit) will appear before positions with lower cost per unit: minimize realized gains.
	 */
	HIFO {
		@Override
		public <T> void sortPositions(List<T> positionList, Function<T, AccountingPosition> functionReturningPosition) {
			if (CollectionUtils.getSize(positionList) > 1) {
				List<Function<T, Object>> sortPropertyFunctions = CollectionUtils.createList(
						p -> {
							AccountingPosition position = functionReturningPosition.apply(p);
							BigDecimal remainingQuantity = position.getRemainingQuantity();
							if (MathUtils.isNullOrZero(remainingQuantity)) {
								return position.getRemainingCostBasis();
							}
							return MathUtils.divide(position.getRemainingCostBasis(), remainingQuantity);
						},
						p -> functionReturningPosition.apply(p).getOriginalTransactionDate(),
						p -> functionReturningPosition.apply(p).getAccountingTransactionId()
				);
				BeanUtils.sortWithFunctions(positionList, sortPropertyFunctions, CollectionUtils.createList(false, true, true));
			}
		}
	},


	/**
	 * First In First Out by Original Transaction Date, then lowest price and then lowest transaction id.
	 */
	FIFO {
		@Override
		public <T> void sortPositions(List<T> positionList, Function<T, AccountingPosition> functionReturningPosition) {
			if (CollectionUtils.getSize(positionList) > 1) {
				List<Function<T, Object>> sortPropertyFunctions = CollectionUtils.createList(
						p -> functionReturningPosition.apply(p).getOriginalTransactionDate(),
						p -> functionReturningPosition.apply(p).getPrice(),
						p -> functionReturningPosition.apply(p).getAccountingTransactionId()
				);
				BeanUtils.sortWithFunctions(positionList, sortPropertyFunctions, CollectionUtils.createList(true, true, true));
			}
		}
	},


	/**
	 * Last In First Out by Original Transaction Date, then highest price and then highest transaction id.
	 * Lowest Cost (cost per unit) will appear before positions with higher cost per unit: maximize realized gains.
	 */
	LIFO {
		@Override
		public <T> void sortPositions(List<T> positionList, Function<T, AccountingPosition> functionReturningPosition) {
			if (CollectionUtils.getSize(positionList) > 1) {
				if (CollectionUtils.getSize(positionList) > 1) {
					List<Function<T, Object>> sortPropertyFunctions = CollectionUtils.createList(
							p -> functionReturningPosition.apply(p).getOriginalTransactionDate(),
							p -> functionReturningPosition.apply(p).getPrice(),
							p -> functionReturningPosition.apply(p).getAccountingTransactionId()
					);
					BeanUtils.sortWithFunctions(positionList, sortPropertyFunctions, CollectionUtils.createList(false, false, false));
				}
			}
		}
	},


	/**
	 * Lowest Cost, First Out, then Original Transaction Date, then lowest transaction id.
	 */
	LOFO {
		@Override
		public <T> void sortPositions(List<T> positionList, Function<T, AccountingPosition> functionReturningPosition) {
			if (CollectionUtils.getSize(positionList) > 1) {
				List<Function<T, Object>> sortPropertyFunctions = CollectionUtils.createList(
						p -> {
							AccountingPosition position = functionReturningPosition.apply(p);
							BigDecimal remainingQuantity = position.getRemainingQuantity();
							if (MathUtils.isNullOrZero(remainingQuantity)) {
								return position.getRemainingCostBasis();
							}
							return MathUtils.divide(position.getRemainingCostBasis(), remainingQuantity);
						},
						p -> functionReturningPosition.apply(p).getOriginalTransactionDate(),
						p -> functionReturningPosition.apply(p).getAccountingTransactionId()
				);
				BeanUtils.sortWithFunctions(positionList, sortPropertyFunctions, CollectionUtils.createList(true, true, true));
			}
		}
	};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sorts the specified list of positions according to the order: FIFO, LIFO, etc.
	 * If the specified list is not a list of AccountingPosition objects (BookingPosition wrapper, etc.), then the function will return the actual positin.
	 */
	public abstract <T> void sortPositions(List<T> positionList, Function<T, AccountingPosition> functionReturningPosition);


	/**
	 * Sorts the specified List of positions according to the method: FIFO, LIFO, etc.
	 */
	public void sortPositions(List<AccountingPosition> positionList) {
		sortPositions(positionList, Function.identity());
	}
}
