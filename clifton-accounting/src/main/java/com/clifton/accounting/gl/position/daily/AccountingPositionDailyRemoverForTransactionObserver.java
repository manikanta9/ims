package com.clifton.accounting.gl.position.daily;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingPositionDailyRemoverForTransactionObserver</code> class is an observer that removes AccountingPositionDaily
 * records that reference the specified AccountingTransaction.  Can be called before AccountingTransaction is un-posted or deleted.
 *
 * @author vgomelsky
 */
@Component
public class AccountingPositionDailyRemoverForTransactionObserver extends BaseDaoEventObserver<AccountingTransaction> {

	private AccountingPositionDailyService accountingPositionDailyService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<AccountingTransaction> dao, DaoEventTypes event, AccountingTransaction bean) {
		// only "Position" GL Accounts can have rows in AccountingPositionDaily
		if (bean.getAccountingAccount().isPosition()) {
			if (bean.isDeleted() || event.isDelete()) {
				getAccountingPositionDailyService().deleteAccountingPositionDailyByAccountingTransaction(bean.getId());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}
}
