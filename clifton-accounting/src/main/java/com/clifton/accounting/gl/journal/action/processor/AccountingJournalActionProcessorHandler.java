package com.clifton.accounting.gl.journal.action.processor;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;


/**
 * The AccountingJournalActionProcessorHandler interface provides method to execute and roll back all action(s)
 * applicable to the specified {@link AccountingJournal} and {@link BookableEntity}.
 *
 * @author vgomelsky
 */
public interface AccountingJournalActionProcessorHandler {


	/**
	 * Retrieves all {@link com.clifton.accounting.gl.journal.action.AccountingJournalAction} object(s), if any, applicable
	 * to the specified journal and bookable entity and executes those actions.
	 */
	public <T extends BookableEntity> void processActions(AccountingJournal journal, T bookableEntity);


	/**
	 * Retrieves all {@link com.clifton.accounting.gl.journal.action.AccountingJournalAction} object(s), if any, applicable
	 * to the specified journal and bookable entity and rolls back those actions.
	 */
	public <T extends BookableEntity> void rollbackActions(AccountingJournal journal, T bookableEntity);
}
