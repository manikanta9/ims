package com.clifton.accounting.gl.comparison;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The <code>AccountingAccountBalanceNotExistsForDateRangeComparison</code> class is a {@link Comparison} that evaluates to true
 * if investment account on the bean (specified by the beanName)
 * does not have an accounting account balance on any date in the specified start/end date range specified by date properties
 * <p/>
 * Example: Billing Schedules can be defined once to apply an additional account fee for a set of accounts.
 * The system can then use this comparison to then "waive" or skip the additional account fee if
 * the client account did not have any positions during the given invoice period
 *
 * @author manderson
 */
public class AccountingAccountBalanceNotExistsForDateRangeComparison implements Comparison<IdentityObject> {

	private AccountingAccountService accountingAccountService;
	private AccountingBalanceService accountingBalanceService;

	/**
	 * Id of account group that make this comparison evaluate to true.
	 */
	private short accountingAccountId;

	/**
	 * The bean property name on the bean that defines the investment account
	 */
	private String accountBeanPropertyName;

	/**
	 * The bean property name on the bean that defines the start date
	 */
	private String startDateBeanPropertyName;

	/**
	 * The bean property name on the bean that defines the end date
	 */
	private String endDateBeanPropertyName;


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		InvestmentAccount clientAccount = (InvestmentAccount) BeanUtils.getPropertyValue(bean, getAccountBeanPropertyName());
		if (clientAccount == null) {
			throw new ValidationException("Unable to evaluate [AccountingAccountBalanceNotExistsForDateRangeComparison] because account is null.  Looking for account on bean ["
					+ bean.getClass().getName() + "], property [" + getAccountBeanPropertyName() + "]");
		}
		Date startDate = (Date) BeanUtils.getPropertyValue(bean, getStartDateBeanPropertyName());
		Date endDate = (Date) BeanUtils.getPropertyValue(bean, getEndDateBeanPropertyName());
		AccountingAccount aa = getAccountingAccountService().getAccountingAccount(getAccountingAccountId());

		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountWithDateRange(clientAccount.getId(), startDate, endDate)
				.withAccountingAccountId(getAccountingAccountId());

		Integer result = getAccountingBalanceService().getAccountingAccountBalanceDaysActive(command);
		if (context != null) {
			if (result == 0) {
				context.recordTrueMessage("(Account " + clientAccount.getNumber() + " had no  [" + aa.getName() + "] balance during the date range [" + DateUtils.fromDateShort(startDate) + " - "
						+ DateUtils.fromDateShort(endDate));
			}
			else {
				context.recordFalseMessage("(Account " + clientAccount.getNumber() + " had [" + aa.getName() + "] balance(s) during the date range [" + DateUtils.fromDateShort(startDate) + " - "
						+ DateUtils.fromDateShort(endDate));
			}
		}

		return result == 0;
	}


	public String getAccountBeanPropertyName() {
		return this.accountBeanPropertyName;
	}


	public void setAccountBeanPropertyName(String accountBeanPropertyName) {
		this.accountBeanPropertyName = accountBeanPropertyName;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public String getStartDateBeanPropertyName() {
		return this.startDateBeanPropertyName;
	}


	public void setStartDateBeanPropertyName(String startDateBeanPropertyName) {
		this.startDateBeanPropertyName = startDateBeanPropertyName;
	}


	public String getEndDateBeanPropertyName() {
		return this.endDateBeanPropertyName;
	}


	public void setEndDateBeanPropertyName(String endDateBeanPropertyName) {
		this.endDateBeanPropertyName = endDateBeanPropertyName;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}
}
