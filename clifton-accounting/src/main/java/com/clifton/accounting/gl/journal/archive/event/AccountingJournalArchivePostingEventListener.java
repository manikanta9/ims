package com.clifton.accounting.gl.journal.archive.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveService;
import com.clifton.accounting.gl.journal.event.AccountingJournalPostingEvent;
import com.clifton.core.util.event.BaseEventListener;
import org.springframework.stereotype.Component;


/**
 * <code>AccountingJournalArchiveEventListener</code> is an event listener for {@link AccountingJournalPostingEvent}s,
 * which use the event name {@link AccountingJournalPostingEvent#EVENT_NAME}.
 *
 * @author NickK
 */
@Component
public class AccountingJournalArchivePostingEventListener extends BaseEventListener<AccountingJournalPostingEvent> {

	private AccountingJournalArchiveService accountingJournalArchiveService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(AccountingJournalPostingEvent event) {
		AccountingJournal journal = event.getTarget();
		if (journal != null) {
			getAccountingJournalArchiveService().archiveAccountingJournal(journal, false);
		}
	}


	@Override
	public String getEventName() {
		return AccountingJournalPostingEvent.EVENT_NAME;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalArchiveService getAccountingJournalArchiveService() {
		return this.accountingJournalArchiveService;
	}


	public void setAccountingJournalArchiveService(AccountingJournalArchiveService accountingJournalArchiveService) {
		this.accountingJournalArchiveService = accountingJournalArchiveService;
	}
}
