package com.clifton.accounting.gl.balance;

import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */
public class AccountingBalanceUtils {

	private AccountingBalanceUtils() {
		// Private constructor
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a new list of {@link AccountingBalance}s merged by generated balance key based on {@link #getBalanceKey(AccountingObjectInfo)}.
	 *
	 * @param balanceList              the list of balances to merge
	 * @param includeZeroLocalBalances includes balances with zero local balances in the returned list when true
	 */
	public static List<AccountingBalance> mergeAccountingBalanceList(List<AccountingBalance> balanceList, boolean includeZeroLocalBalances) {
		Map<String, AccountingBalance> balanceMap = new HashMap<>();
		for (AccountingBalance balanceToMerge : CollectionUtils.getIterable(balanceList)) {
			String key = getBalanceKey(balanceToMerge);
			balanceMap.compute(key, (k, currentValue) -> {
				// always add transaction to the balance
				currentValue = AccountingBalanceUtils.sumAccountingBalance(currentValue, balanceToMerge);
				if (balanceToMerge.isPendingActivity()) {
					// add the pending transaction to the pending balance
					AccountingBalance pendingBalance = currentValue.getPendingBalance() == null ? AccountingBalance.ofAccountingObjectInfo(balanceToMerge)
							: currentValue.getPendingBalance();
					pendingBalance.setPendingActivity(balanceToMerge.isPendingActivity());
					currentValue.setPendingBalance(sumAccountingBalance(pendingBalance, balanceToMerge));
				}
				return currentValue;
			});
		}

		List<AccountingBalance> result = new PagingArrayList<>();
		for (AccountingBalance balance : CollectionUtils.getIterable(balanceMap.values())) {
			if (includeZeroLocalBalances || !(MathUtils.isNullOrZero(balance.getQuantity()) && MathUtils.isNullOrZero(balance.getLocalAmount()) && MathUtils.isNullOrZero(balance.getLocalCostBasis()))) {
				result.add(balance);
			}
		}

		return result;
	}


	/**
	 * Converts the specified transaction list into corresponding balances and groups them by Client/Holding/GL Accounts and Security.
	 */
	public static List<AccountingBalance> convertAndGroupIntoBalanceList(List<AccountingJournalDetailDefinition> transactionList, Predicate<AccountingJournalDetailDefinition> pendingPredicate) {
		return convertAndGroupIntoBalanceList(transactionList, UnaryOperator.identity(), pendingPredicate, false);
	}


	/**
	 * Converts the specified position list into corresponding balances and groups them by Client/Holding/GL Accounts and Security.
	 */
	public static List<AccountingBalance> convertAndGroupAccountingPositionListIntoBalanceList(List<AccountingPosition> positionList, boolean includeZeroLocalBalances) {
		return convertAndGroupIntoBalanceList(positionList,
				position -> {
					// make a copy of the opening transaction to avoid modifying the original lot
					AccountingJournalDetailDefinition openingTransaction = new AccountingTransaction();
					BeanUtils.copyProperties(position.getOpeningTransaction(), openingTransaction);
					// adjust the opening transaction to the remaining values for balance conversion
					openingTransaction.setQuantity(position.getRemainingQuantity());
					BigDecimal remainingBaseDebitCredit = position.getRemainingBaseDebitCredit();
					openingTransaction.setBaseDebitCredit(remainingBaseDebitCredit);
					BigDecimal localDebitCredit = MathUtils.isNullOrZero(remainingBaseDebitCredit) ? remainingBaseDebitCredit
							: InvestmentCalculatorUtils.calculateLocalAmount(openingTransaction.getBaseDebitCredit(), openingTransaction.getExchangeRateToBase(), openingTransaction.getInvestmentSecurity());
					openingTransaction.setLocalDebitCredit(localDebitCredit);
					openingTransaction.setPositionCostBasis(position.getRemainingCostBasis());
					return openingTransaction;
				}, AccountingPosition::isPendingActivity, includeZeroLocalBalances);
	}


	/**
	 * Creates a String key for the provided {@link AccountingObjectInfo} that can be used to link accounting entities for balance aggregation.
	 * The key is similar to {@link AccountingJournalDetailDefinition#createNaturalKey()} without the original transaction date.
	 */
	public static String getBalanceKey(AccountingObjectInfo accountingObject) {
		return BeanUtils.createKeyFromBeans(accountingObject.getClientInvestmentAccount(), accountingObject.getHoldingInvestmentAccount(), accountingObject.getAccountingAccount(), accountingObject.getInvestmentSecurity());
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                 Helper Methods                         //////////
	////////////////////////////////////////////////////////////////////////////


	private static AccountingBalance sumAccountingBalance(AccountingBalance existingBalance, AccountingBalance balance) {
		if (existingBalance == null) {
			// do not include the pending activity in the copy because it will be recreated in the merge processing
			existingBalance = AccountingBalance.ofAccountingBalance(balance, false);
		}
		else {
			// only set pending activity if both balances are pending; the balance will result in pending activity included
			existingBalance.setPendingActivity(existingBalance.isPendingActivity() && balance.isPendingActivity());
			existingBalance.setQuantity(MathUtils.add(existingBalance.getQuantity(), balance.getQuantity()));
			existingBalance.setLocalAmount(MathUtils.add(existingBalance.getLocalAmount(), balance.getLocalAmount()));
			existingBalance.setBaseAmount(MathUtils.add(existingBalance.getBaseAmount(), balance.getBaseAmount()));
			existingBalance.setLocalCostBasis(MathUtils.add(existingBalance.getLocalCostBasis(), balance.getLocalCostBasis()));
			existingBalance.setBaseCostBasis(MathUtils.add(existingBalance.getBaseCostBasis(), balance.getBaseCostBasis()));
		}
		return existingBalance;
	}


	private static <T> List<AccountingBalance> convertAndGroupIntoBalanceList(List<T> entityList, Function<T, AccountingJournalDetailDefinition> transactionFunction, Predicate<T> pending, boolean includeZeroLocalBalances) {
		if (CollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}

		Map<String, AccountingBalance> balanceMap = new HashMap<>();
		for (T entity : CollectionUtils.getIterable(entityList)) {
			AccountingJournalDetailDefinition transaction = transactionFunction.apply(entity);
			boolean transactionPending = pending.test(entity);
			balanceMap.compute(getBalanceKey(transaction), (key, currentValue) -> {
				// always add transaction to the balance
				currentValue = (currentValue == null) ? AccountingBalance.ofAccountingJournalDetailDefinition(transaction, transactionPending)
						: mergeJournalDetailWithAccountingBalance(currentValue, transaction, transactionPending);
				if (transactionPending) {
					// add the pending transaction to the pending balance
					AccountingBalance pendingBalance = (currentValue.getPendingBalance() == null) ? AccountingBalance.ofAccountingJournalDetailDefinition(transaction, transactionPending)
							: mergeJournalDetailWithAccountingBalance(currentValue.getPendingBalance(), transaction, transactionPending);
					pendingBalance.setPendingActivity(transactionPending);
					currentValue.setPendingBalance(pendingBalance);
				}
				return currentValue;
			});
		}

		return balanceMap.values().stream()
				.filter(balance -> includeZeroLocalBalances || !(MathUtils.isNullOrZero(balance.getQuantity()) && MathUtils.isNullOrZero(balance.getLocalAmount()) && MathUtils.isNullOrZero(balance.getLocalCostBasis())))
				.collect(Collectors.toList());
	}


	private static AccountingBalance mergeJournalDetailWithAccountingBalance(AccountingBalance balance, AccountingJournalDetailDefinition journalDetail, boolean pendingActivity) {
		balance.setQuantity(MathUtils.add(balance.getQuantity(), journalDetail.getQuantity()));
		balance.setLocalAmount(MathUtils.add(balance.getLocalAmount(), journalDetail.getLocalDebitCredit()));
		balance.setBaseAmount(MathUtils.add(balance.getBaseAmount(), journalDetail.getBaseDebitCredit()));
		balance.setLocalCostBasis(MathUtils.add(balance.getLocalCostBasis(), journalDetail.getPositionCostBasis()));
		balance.setBaseCostBasis(MathUtils.add(balance.getBaseCostBasis(), MathUtils.multiply(journalDetail.getPositionCostBasis(), journalDetail.getExchangeRateToBase(), 2)));
		// only set pending activity if both are pending; the balance will result in pending activity included
		balance.setPendingActivity(balance.isPendingActivity() && pendingActivity);
		return balance;
	}
}
