package com.clifton.accounting.gl.booking.position;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author TerryS
 */
@Component
public class AccountingBookingPositionRetrieverImpl implements AccountingBookingPositionRetriever {


	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BookingPosition> getAccountingBookingOpenPositionList(AccountingBookingPositionCommand command) {
		List<BookingPosition> currentOpenPositions = new ArrayList<>();

		AccountingPositionCommand positionCommand = command.isUseSettlementDate() ? AccountingPositionCommand.onPositionSettlementDate(command.getSettlementDate())
				: AccountingPositionCommand.onPositionTransactionDate(command.getTransactionDate());
		positionCommand.setClientInvestmentAccountId(command.getClientInvestmentAccountId());
		positionCommand.setHoldingInvestmentAccountId(command.getHoldingInvestmentAccountId());
		positionCommand.setInvestmentSecurityId(command.getInvestmentSecurityId());
		positionCommand.setOrder(null); // will sort in the end
		List<AccountingPosition> openPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand);
		// match only positions for the same accounting account (prevents Collateral positions from closing non-Collateral positions and visa versa)
		openPositionList = BeanUtils.filter(openPositionList, AccountingPosition::getAccountingAccount, command.getPositionAccount());
		if (openPositionList == null) {
			openPositionList = Collections.emptyList();
		}
		for (AccountingPosition position : openPositionList) {
			boolean fullyClosedPosition = false;
			if (command.isUseSettlementDate() && position.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
				// find all closing lots that settle in the future (delayed factor change or credit event)
				AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
				searchForm.setParentId(position.getId());
				searchForm.setAccountingAccountId(position.getAccountingAccount().getId());
				searchForm.setOpening(false);
				searchForm.addSearchRestriction(new SearchRestriction("settlementDate", ComparisonConditions.GREATER_THAN, command.getSettlementDate()));
				List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
				if (!CollectionUtils.isEmpty(transactionList)) {
					// adjust position lot by the closing lots: add because closing lot already has opposite sign
					for (AccountingTransaction transaction : transactionList) {
						position.setRemainingQuantity(MathUtils.add(position.getRemainingQuantity(), transaction.getQuantity()));
						position.setRemainingBaseDebitCredit(MathUtils.add(position.getRemainingBaseDebitCredit(), transaction.getBaseDebitCredit()));
						position.setRemainingCostBasis(MathUtils.add(position.getRemainingCostBasis(), transaction.getPositionCostBasis()));
					}
					fullyClosedPosition = MathUtils.isNullOrZero(position.getRemainingQuantity()) && MathUtils.isNullOrZero(position.getRemainingCostBasis()) && MathUtils.isNullOrZero(position.getRemainingBaseDebitCredit());
				}
			}
			if (!fullyClosedPosition) {
				currentOpenPositions.add(new BookingPosition(position));
			}
		}

		filterEarlyTermination(command, currentOpenPositions);

		ObjectUtils.coalesce(command.getOrder(), AccountingPositionOrders.FIFO).sortPositions(currentOpenPositions, BookingPosition::getPosition);

		return currentOpenPositions;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void filterEarlyTermination(AccountingBookingPositionCommand command, List<BookingPosition> openPositions) {
		if (command.isRemoveEarlyTermination()) {
			InvestmentSecurity investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurity(command.getInvestmentSecurityId());
			boolean closeOnMaturityOnly = InvestmentUtils.isCloseOnMaturityOnly(investmentSecurity);
			boolean earlyTermination = false;
			if (InvestmentUtils.isSecurityOfType(investmentSecurity, InvestmentType.FORWARDS)
					&& DateUtils.compare(command.getTransactionDate(), command.getSettlementDate(), false) != 0) {
				// Currency Forwards can be early settled (if not Transaction Date == Settlement Date): DO NOT WAIT UNTIL MATURITY AND RUN THROUGH SPLITTER
				earlyTermination = true;
			}
			if (!closeOnMaturityOnly || earlyTermination) {
				BigDecimal totalOpenPositionsQuantity = BigDecimal.ZERO;
				// no splitting for securities that close on maturity (LME, Forwards, etc.)
				List<BookingPosition> earlyTerminationIgnoreList = null;
				for (BookingPosition pos : openPositions) {
					if (!MathUtils.isEqual(totalOpenPositionsQuantity, 0)) {
						if (!MathUtils.isSameSignum(totalOpenPositionsQuantity, pos.getRemainingQuantity())) {
							if (earlyTermination) {
								if (earlyTerminationIgnoreList == null) {
									earlyTerminationIgnoreList = new ArrayList<>();
									earlyTerminationIgnoreList.add(pos);
								}
								continue;
							}
							// should never have this: long and short lot at the same time
							throw new ValidationException("Cannot run " + command.getBookableEntity() + " journal detail through accounting position splitter because there are long and short open lots for '"
									+ investmentSecurity.getSymbol()
									+ "' at the same time on Settlement Date.\n\nUnbook corresponding journals and rebook them in correct order to fix the problem.");
						}
					}
					totalOpenPositionsQuantity = totalOpenPositionsQuantity.add(pos.getRemainingQuantity());
				}
				// remove all lots with the same sign (ok for early terminations)
				for (BookingPosition pos : CollectionUtils.getIterable(earlyTerminationIgnoreList)) {
					openPositions.remove(pos);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
