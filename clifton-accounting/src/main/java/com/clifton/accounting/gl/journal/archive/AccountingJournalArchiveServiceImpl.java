package com.clifton.accounting.gl.journal.archive;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.archive.locator.AccountingJournalArchiveEntityAdapterLocator;
import com.clifton.accounting.gl.journal.archive.runner.AccountingJournalArchiveRunner;
import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.archive.descriptor.ArchiveEntityDescriptorService;
import com.clifton.archive.descriptor.search.ArchiveEntityDescriptorSearchForm;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


/**
 * <code>AccountingJournalArchiveServiceImpl</code> class provides implementation archiving {@link AccountingJournal}s.
 *
 * @author NickK
 */
@Service
public class AccountingJournalArchiveServiceImpl implements AccountingJournalArchiveService {

	private ArchiveEntityDescriptorService archiveEntityDescriptorService;
	private AccountingBookingService<?> accountingBookingService;
	private AccountingJournalArchiveEntityAdapterLocator accountingJournalArchiveEntityAdapterLocator;
	private AccountingJournalService accountingJournalService;

	// TODO remove this after historic archival is complete
	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ArchiveEntityDescriptor archiveAccountingJournal(AccountingJournal journal, boolean usePostingDate) {
		if (journal != null && journal.getJournalType() != null && journal.getJournalType().isArchiveAllowed()) {
			ArchiveEntityDescriptor toArchive = getArchiveEntityDescriptor(journal);
			if (usePostingDate) {
				toArchive.setArchiveDate(journal.getPostingDate());
			}
			getArchiveEntityDescriptorService().archiveEntity(toArchive);
			return toArchive;
		}
		return null;
	}


	@Override
	public ArchiveEntityDescriptor getAccountingJournalArchiveEntityDescriptorPreview(long id) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(id);
		if (journal != null) {
			return getArchiveEntityDescriptor(journal);
		}
		return null;
	}


	@Override
	public List<ArchiveEntityDescriptor> getAccountingJournalArchiveEntityDescriptorList(long id) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(id);
		if (journal != null) {
			BookableEntity bookableEntity = getBookableEntityFromJournal(journal);
			Long entityId = bookableEntity == null ? journal.getId() : ((Number) bookableEntity.getIdentity()).longValue();
			ArchiveEntityDescriptorSearchForm searchForm = new ArchiveEntityDescriptorSearchForm();
			searchForm.setEntityIdentifier(entityId);
			searchForm.setExcludeEntityJsonInResults(true);
			return getArchiveEntityDescriptorService().getArchiveEntityDescriptorList(searchForm);
		}
		return Collections.emptyList();
	}


	@Override    // TODO remove this after historic archival is complete
	public Status archiveAccountingJournalOfType(short id) {
		AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalType(id);
		if (journalType != null) {
			AccountingJournalArchiveRunner runner = new AccountingJournalArchiveRunner(journalType, getAccountingJournalService(), this);
			getRunnerHandler().runNow(runner);

			return Status.ofMessage("Journal archive runner scheduled for journal type " + journalType.getName() + ". Processing may take some time.");
		}

		return Status.ofMessage("Journal archive runner not started. Unable to find journal type with ID: " + id + ".");
	}

	////////////////////////////////////////////////////////////////////////////
	////////                      Helper Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends BookableEntity> ArchiveEntityDescriptor getArchiveEntityDescriptor(AccountingJournal journal) {
		AccountingJournalArchiveEntityAdapter<T> adapter = getArchivePrototypeAdapter(journal.getJournalType().getName());
		T bookableEntity = getBookableEntityFromJournal(journal);
		AccountingJournalArchiveEntity<T> journalArchiveEntity = bookableEntity == null ? new AccountingJournalArchiveEntity<>(journal)
				: new AccountingJournalArchiveEntity<>(journal, bookableEntity);
		return adapter.toArchiveEntityDescriptor(journalArchiveEntity);
	}


	@SuppressWarnings("unchecked")
	private <T extends BookableEntity> T getBookableEntityFromJournal(AccountingJournal journal) {
		T bookableEntity = null;
		if (journal.getFkFieldId() != null) {
			return (T) getAccountingBookingService().getBookableEntity(journal);
		}
		return bookableEntity;
	}


	@SuppressWarnings("unchecked")
	private <T extends BookableEntity> AccountingJournalArchiveEntityAdapter<T> getArchivePrototypeAdapter(String journalTypeName) {
		return (AccountingJournalArchiveEntityAdapter<T>) getAccountingJournalArchiveEntityAdapterLocator().locate(journalTypeName);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ArchiveEntityDescriptorService getArchiveEntityDescriptorService() {
		return this.archiveEntityDescriptorService;
	}


	public void setArchiveEntityDescriptorService(ArchiveEntityDescriptorService archiveEntityDescriptorService) {
		this.archiveEntityDescriptorService = archiveEntityDescriptorService;
	}


	public AccountingBookingService<?> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<?> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalArchiveEntityAdapterLocator getAccountingJournalArchiveEntityAdapterLocator() {
		return this.accountingJournalArchiveEntityAdapterLocator;
	}


	public void setAccountingJournalArchiveEntityAdapterLocator(AccountingJournalArchiveEntityAdapterLocator accountingJournalArchiveEntityAdapterLocator) {
		this.accountingJournalArchiveEntityAdapterLocator = accountingJournalArchiveEntityAdapterLocator;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
