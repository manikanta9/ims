package com.clifton.accounting.gl.pending;

/**
 * The PendingActivityRequests enum defines various options for including pending activity.
 *
 * @author vgomelsky
 */
public enum PendingActivityRequests {

	/**
	 * Include all activity (positions, cash, etc.) using GL Booking Preview process and merge preview results with existing GL Data.
	 */
	ALL_WITH_MERGE_AND_PREVIEW(false, false, true, false, true),
	/**
	 * Include all activity (positions, cash, etc.) using GL Booking Preview process and keep preview results as separate separate from existing GL Data.
	 */
	ALL_WITH_PREVIEW(false, false, false, false, true),
	/**
	 * Include position activity only (no cash impact, etc.) and merge preview results with existing GL Data.
	 */
	POSITIONS_WITH_MERGE(true, false, true, false, false),
	/**
	 * Include position activity only (no cash impact, etc.) and merge preview results with existing GL Data.
	 */
	POSITIONS_WITH_MERGE_AND_ZERO_BALANCES(true, false, true, true, false),
	/**
	 * Include position activity only (no cash impact, etc.) using GL Booking Preview process and merge preview results with existing GL Data.
	 */
	POSITIONS_WITH_MERGE_AND_PREVIEW(true, false, true, false, true),
	/**
	 * Include position activity only (no cash impact, etc.) using GL Booking Preview process, throwing an exception on insufficient data for simulations, and merge preview results
	 * with existing GL Data.
	 */
	POSITIONS_STRICT_WITH_MERGE_AND_PREVIEW(true, true, true, false, true),
	/**
	 * Include position activity only (no cash impact, etc.) using GL Booking Preview process and keep preview results separate from existing GL Data.
	 */
	POSITIONS_WITH_PREVIEW(true, false, false, false, true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PendingActivityRequests(boolean positionsOnly, boolean strict, boolean merge, boolean includeZeroLocalBalances, boolean generatedByBookingPreview) {
		this.positionsOnly = positionsOnly;
		this.strict = strict;
		this.merge = merge;
		this.includeLocalBalances = includeZeroLocalBalances;
		this.generatedByBookingPreview = generatedByBookingPreview;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Specifies whether only position should be included: remove any non-position entries that might result from preview.
	 */
	private final boolean positionsOnly;

	/**
	 * Specifies whether pending activity should be strictly inferred for existing pending entities. This is relevant in such cases as trades which do not have sufficient data for
	 * pending position simulation. In these cases, an exception will be thrown.
	 */
	private final boolean strict;

	/**
	 * Specifies whether pending activity should be merged with corresponding real GL information.
	 */
	private final boolean merge;

	/**
	 * Applicable when merge is true and specifies that positions resulting from the merge should remain when the local balances has been brought to zero.
	 */
	private final boolean includeLocalBalances;

	/**
	 * Specifies whether pending items should be generated using the booking preview (run GL Booking process in Preview mode)
	 * This will result in inclusion of all transactions not just positions (cash/currency/gain/loss/etc. generated from booking).
	 */
	private final boolean generatedByBookingPreview;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPositionsOnly() {
		return this.positionsOnly;
	}


	public boolean isStrict() {
		return this.strict;
	}


	public boolean isMerge() {
		return this.merge;
	}


	public boolean isIncludeLocalBalances() {
		return this.includeLocalBalances;
	}


	public boolean isGeneratedByBookingPreview() {
		return this.generatedByBookingPreview;
	}
}
