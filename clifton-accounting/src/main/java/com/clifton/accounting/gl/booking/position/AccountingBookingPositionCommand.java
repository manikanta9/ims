package com.clifton.accounting.gl.booking.position;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.beans.IdentityObject;

import java.util.Date;


/**
 * @author TerryS
 */
public class AccountingBookingPositionCommand extends AccountingPositionCommand {

	private final IdentityObject bookableEntity;
	private final boolean useSettlementDate;
	private final AccountingAccount positionAccount;
	private final boolean removeEarlyTermination;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingPositionCommand(IdentityObject bookableEntity, AccountingPositionOrders positionOrder, boolean useSettlementDate, Date settlementDate, Date transactionDate, Integer clientAccountId,
	                                        Integer holdingAccountId, Integer investmentSecurityId, AccountingAccount positionAccount, boolean removeEarlyTermination) {
		setSettlementDate(settlementDate);
		setTransactionDate(transactionDate);
		setClientInvestmentAccountId(clientAccountId);
		setHoldingInvestmentAccountId(holdingAccountId);
		setInvestmentSecurityId(investmentSecurityId);
		setOrder(positionOrder);

		this.bookableEntity = bookableEntity;
		this.useSettlementDate = useSettlementDate;
		this.positionAccount = positionAccount;
		this.removeEarlyTermination = removeEarlyTermination;
	}


	public static AccountingBookingPositionCommand of(AccountingJournalDetailDefinition journalDetail, AccountingPositionOrders positionOrder, boolean useSettlementDate, boolean removeEarlyTermination) {
		return new AccountingBookingPositionCommand(journalDetail, positionOrder, useSettlementDate, journalDetail.getSettlementDate(), journalDetail.getTransactionDate(),
				journalDetail.getClientInvestmentAccount().getId(), journalDetail.getHoldingInvestmentAccount().getId(), journalDetail.getInvestmentSecurity().getId(),
				journalDetail.getAccountingAccount(), removeEarlyTermination);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IdentityObject getBookableEntity() {
		return this.bookableEntity;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public AccountingAccount getPositionAccount() {
		return this.positionAccount;
	}


	public boolean isRemoveEarlyTermination() {
		return this.removeEarlyTermination;
	}
}
