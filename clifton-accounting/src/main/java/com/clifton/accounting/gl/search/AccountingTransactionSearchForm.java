package com.clifton.accounting.gl.search;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * NOTE: by default all "deleted" rows will be excluded from results.  You need to set "allowDeleted = true" explicitly in order to allow deleted rows.
 * When allowDeleted is set, then "deleted" value will determine filtering for deleted rows.
 */
public class AccountingTransactionSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "symbol,name", searchFieldPath = "investmentSecurity")
	private String searchPattern;

	@SearchField
	private Long id;

	@SearchField(searchField = "journal.id")
	private Long journalId;

	@SearchField(searchField = "journal.id", comparisonConditions = ComparisonConditions.IN)
	private Long[] journalIdList;

	@SearchField(searchField = "journal.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Long[] excludeJournalIdList;

	@SearchField(searchField = "journal.fkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Integer[] journalFkFieldIdList;

	@SearchField(searchField = "id")
	private Long transactionId;

	@SearchField(searchField = "journal.id", searchFieldPath = "parentTransaction")
	private Long parentJournalId;

	@SearchField(searchField = "parentTransaction.id")
	private Long parentId;

	@SearchField(searchField = "parentTransaction.id", comparisonConditions = ComparisonConditions.IN)
	private Long[] parentIdList;

	// custom search using UNION because OR clause results in super-inefficient execution plan
	private Long parentOrGrandParentId;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "clientInvestmentAccount", sortField = "businessClient.name")
	private Integer businessClientId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "number", searchFieldPath = "clientInvestmentAccount")
	private String clientAccountNumber;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientInvestmentAccountGroupId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchField = "number", searchFieldPath = "holdingInvestmentAccount")
	private String holdingAccountNumber;

	@SearchField(searchField = "type.id", searchFieldPath = "holdingInvestmentAccount", sortField = "type.name")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "holdingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer holdingInvestmentAccountGroupId;

	@SearchField(searchField = "executingCompany.id", sortField = "executingCompany.name")
	private Integer executingCompanyId;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "parentTransaction.investmentSecurity.id")
	private Integer parentTransactionInvestmentSecurityId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "investmentSecurity.instrument", sortField = "tradingCurrency.symbol")
	private Integer payingSecurityId;

	@SearchField(searchField = "currency", searchFieldPath = "investmentSecurity")
	private Boolean currency;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "investmentSecurity.instrument")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "instrument.id", searchFieldPath = "investmentSecurity")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "symbol", searchFieldPath = "investmentSecurity")
	private String investmentSecuritySymbol;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "parentTransaction.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short parentTransactionInvestmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", searchFieldPath = "investmentSecurity", comparisonConditions = ComparisonConditions.EXISTS)
	private Short securityGroupId;

	@SearchField(searchField = "priceMultiplierOverride,instrument.priceMultiplier", searchFieldPath = "investmentSecurity", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal priceMultiplier;

	@SearchField(searchField = "accountingAccount.id", sortField = "accountingAccount.name")
	private Short accountingAccountId;

	@SearchField(searchField = "accountingAccount.id", sortField = "accountingAccount.name")
	private Short[] accountingAccountIds;

	@SearchField(searchField = "accountingAccount.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeAccountingAccountIds;

	// Custom Search Filter - Convert to Short[] for accountingAccountIds filter
	private AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName;

	// custom filter to avoid dependencies
	private Short accountingAccountGroupId;

	@SearchField(searchField = "name", searchFieldPath = "accountingAccount")
	private String accountingAccountName;

	@SearchField(searchField = "name", searchFieldPath = "accountingAccount", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeAccountingAccountName;

	@SearchField(searchField = "position", searchFieldPath = "accountingAccount")
	private Boolean positionAccountingAccount;

	@SearchField(searchField = "cash", searchFieldPath = "accountingAccount")
	private Boolean cashAccountingAccount;

	@SearchField(searchField = "currency", searchFieldPath = "accountingAccount")
	private Boolean currencyAccountingAccount;

	@SearchField(searchField = "collateral", searchFieldPath = "accountingAccount")
	private Boolean collateralAccountingAccount;

	@SearchField(searchField = "commission", searchFieldPath = "accountingAccount")
	private Boolean commissionAccountingAccount;

	@SearchField(searchField = "gainLoss", searchFieldPath = "accountingAccount")
	private Boolean gainLossAccountingAccount;

	@SearchField(searchField = "currencyTranslation", searchFieldPath = "accountingAccount")
	private Boolean currencyTranslationAccountingAccount;

	@SearchField(searchField = "receivable", searchFieldPath = "accountingAccount")
	private Boolean receivableAccountingAccount;

	@SearchField(searchField = "postingNotAllowed", searchFieldPath = "accountingAccount")
	private Boolean postingNotAllowedAccountingAccount;

	@SearchField(searchField = "autoAccrualReversalOffsetAccount", searchFieldPath = "accountingAccount", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean autoReverseAccrualAccountingAccount;

	@SearchField(searchField = "balanceSheetAccount", searchFieldPath = "accountingAccount.accountType")
	private Boolean balanceSheetAccountingAccountType;

	@SearchField
	private String description;

	@SearchField(searchField = "journalType.id", searchFieldPath = "journal", sortField = "journalType.name")
	private Short journalTypeId;

	@SearchField(searchField = "name", searchFieldPath = "journal.journalType")
	private String journalTypeName;

	@SearchField(searchField = "description", searchFieldPath = "journal")
	private String journalDescription;

	@SearchField(searchField = "transactionDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date maxTransactionDate;

	@SearchField(searchField = "transactionDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date minTransactionDate;

	@SearchField(searchField = "settlementDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date maxSettlementDate;

	@SearchField(searchField = "settlementDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date minSettlementDate;

	@SearchField
	private Date transactionDate;
	@SearchField
	private Date originalTransactionDate;
	@SearchField
	private Date settlementDate;
	@SearchField(searchFieldPath = "journal", searchField = "postingDate", dateFieldIncludesTime = true)
	private Date postingDate;

	@SearchField
	private BigDecimal price;
	@SearchField
	private BigDecimal exchangeRateToBase;
	@SearchField
	private BigDecimal positionCostBasis;

	@SearchField
	private BigDecimal baseDebitCredit;
	@SearchField
	private BigDecimal localDebitCredit;
	@SearchField(searchField = "baseDebitCredit", searchFieldCustomType = SearchFieldCustomTypes.OR_IGNORE_SIGN_FOR_EQUALS)
	private BigDecimal baseDebitOrCredit;
	@SearchField(searchField = "localDebitCredit", searchFieldCustomType = SearchFieldCustomTypes.OR_IGNORE_SIGN_FOR_EQUALS)
	private BigDecimal localDebitOrCredit;

	@SearchField
	private BigDecimal quantity;

	@SearchField(searchField = "systemTable.id")
	private Short tableId;

	@SearchField(searchField = "name", searchFieldPath = "systemTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	@SearchField
	private Integer fkFieldId;

	@SearchField(searchField = "fkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Integer[] fkFieldIds;

	@SearchField
	private Boolean opening;

	@SearchField
	private Boolean deleted;

	/**
	 * Most of the time we do not want deleted rows so they are excluded by default.  If you want to include them,
	 * then set this value to true and then control filtering of deleted rows using "deleted" property.
	 */
	private boolean allowDeleted = false;

	@SearchField
	private Boolean modified;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		SearchUtils.validateRequiredFilter(this, "allowDeleted", "deleted", "opening", "modified");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getJournalId() {
		return this.journalId;
	}


	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}


	public Long[] getJournalIdList() {
		return this.journalIdList;
	}


	public void setJournalIdList(Long[] journalIdList) {
		this.journalIdList = journalIdList;
	}


	public Long[] getExcludeJournalIdList() {
		return this.excludeJournalIdList;
	}


	public void setExcludeJournalIdList(Long[] excludeJournalIdList) {
		this.excludeJournalIdList = excludeJournalIdList;
	}


	public Integer[] getJournalFkFieldIdList() {
		return this.journalFkFieldIdList;
	}


	public void setJournalFkFieldIdList(Integer[] journalFkFieldIdList) {
		this.journalFkFieldIdList = journalFkFieldIdList;
	}


	public Long getTransactionId() {
		return this.transactionId;
	}


	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}


	public Long getParentJournalId() {
		return this.parentJournalId;
	}


	public void setParentJournalId(Long parentJournalId) {
		this.parentJournalId = parentJournalId;
	}


	public Long getParentId() {
		return this.parentId;
	}


	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	public Long[] getParentIdList() {
		return this.parentIdList;
	}


	public void setParentIdList(Long[] parentIdList) {
		this.parentIdList = parentIdList;
	}


	public Long getParentOrGrandParentId() {
		return this.parentOrGrandParentId;
	}


	public void setParentOrGrandParentId(Long parentOrGrandParentId) {
		this.parentOrGrandParentId = parentOrGrandParentId;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public Integer getExecutingCompanyId() {
		return this.executingCompanyId;
	}


	public void setExecutingCompanyId(Integer executingCompanyId) {
		this.executingCompanyId = executingCompanyId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getParentTransactionInvestmentSecurityId() {
		return this.parentTransactionInvestmentSecurityId;
	}


	public void setParentTransactionInvestmentSecurityId(Integer parentTransactionInvestmentSecurityId) {
		this.parentTransactionInvestmentSecurityId = parentTransactionInvestmentSecurityId;
	}


	public Integer getPayingSecurityId() {
		return this.payingSecurityId;
	}


	public void setPayingSecurityId(Integer payingSecurityId) {
		this.payingSecurityId = payingSecurityId;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getParentTransactionInvestmentGroupId() {
		return this.parentTransactionInvestmentGroupId;
	}


	public void setParentTransactionInvestmentGroupId(Short parentTransactionInvestmentGroupId) {
		this.parentTransactionInvestmentGroupId = parentTransactionInvestmentGroupId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Short[] getAccountingAccountIds() {
		return this.accountingAccountIds;
	}


	public void setAccountingAccountIds(Short[] accountingAccountIds) {
		this.accountingAccountIds = accountingAccountIds;
	}


	public Short[] getExcludeAccountingAccountIds() {
		return this.excludeAccountingAccountIds;
	}


	public void setExcludeAccountingAccountIds(Short[] excludeAccountingAccountIds) {
		this.excludeAccountingAccountIds = excludeAccountingAccountIds;
	}


	public AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountIdName() {
		return this.accountingAccountIdName;
	}


	public void setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName) {
		this.accountingAccountIdName = accountingAccountIdName;
	}


	public Short getAccountingAccountGroupId() {
		return this.accountingAccountGroupId;
	}


	public void setAccountingAccountGroupId(Short accountingAccountGroupId) {
		this.accountingAccountGroupId = accountingAccountGroupId;
	}


	public String getAccountingAccountName() {
		return this.accountingAccountName;
	}


	public void setAccountingAccountName(String accountingAccountName) {
		this.accountingAccountName = accountingAccountName;
	}


	public String getExcludeAccountingAccountName() {
		return this.excludeAccountingAccountName;
	}


	public void setExcludeAccountingAccountName(String excludeAccountingAccountName) {
		this.excludeAccountingAccountName = excludeAccountingAccountName;
	}


	public Boolean getPositionAccountingAccount() {
		return this.positionAccountingAccount;
	}


	public void setPositionAccountingAccount(Boolean positionAccountingAccount) {
		this.positionAccountingAccount = positionAccountingAccount;
	}


	public Boolean getCashAccountingAccount() {
		return this.cashAccountingAccount;
	}


	public void setCashAccountingAccount(Boolean cashAccountingAccount) {
		this.cashAccountingAccount = cashAccountingAccount;
	}


	public Boolean getCurrencyAccountingAccount() {
		return this.currencyAccountingAccount;
	}


	public void setCurrencyAccountingAccount(Boolean currencyAccountingAccount) {
		this.currencyAccountingAccount = currencyAccountingAccount;
	}


	public Boolean getCollateralAccountingAccount() {
		return this.collateralAccountingAccount;
	}


	public void setCollateralAccountingAccount(Boolean collateralAccountingAccount) {
		this.collateralAccountingAccount = collateralAccountingAccount;
	}


	public Boolean getCommissionAccountingAccount() {
		return this.commissionAccountingAccount;
	}


	public void setCommissionAccountingAccount(Boolean commissionAccountingAccount) {
		this.commissionAccountingAccount = commissionAccountingAccount;
	}


	public Boolean getGainLossAccountingAccount() {
		return this.gainLossAccountingAccount;
	}


	public void setGainLossAccountingAccount(Boolean gainLossAccountingAccount) {
		this.gainLossAccountingAccount = gainLossAccountingAccount;
	}


	public Boolean getCurrencyTranslationAccountingAccount() {
		return this.currencyTranslationAccountingAccount;
	}


	public void setCurrencyTranslationAccountingAccount(Boolean currencyTranslationAccountingAccount) {
		this.currencyTranslationAccountingAccount = currencyTranslationAccountingAccount;
	}


	public Boolean getReceivableAccountingAccount() {
		return this.receivableAccountingAccount;
	}


	public void setReceivableAccountingAccount(Boolean receivableAccountingAccount) {
		this.receivableAccountingAccount = receivableAccountingAccount;
	}


	public Boolean getPostingNotAllowedAccountingAccount() {
		return this.postingNotAllowedAccountingAccount;
	}


	public void setPostingNotAllowedAccountingAccount(Boolean postingNotAllowedAccountingAccount) {
		this.postingNotAllowedAccountingAccount = postingNotAllowedAccountingAccount;
	}


	public Boolean getAutoReverseAccrualAccountingAccount() {
		return this.autoReverseAccrualAccountingAccount;
	}


	public void setAutoReverseAccrualAccountingAccount(Boolean autoReverseAccrualAccountingAccount) {
		this.autoReverseAccrualAccountingAccount = autoReverseAccrualAccountingAccount;
	}


	public Boolean getBalanceSheetAccountingAccountType() {
		return this.balanceSheetAccountingAccountType;
	}


	public void setBalanceSheetAccountingAccountType(Boolean balanceSheetAccountingAccountType) {
		this.balanceSheetAccountingAccountType = balanceSheetAccountingAccountType;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public String getJournalTypeName() {
		return this.journalTypeName;
	}


	public void setJournalTypeName(String journalTypeName) {
		this.journalTypeName = journalTypeName;
	}


	public String getJournalDescription() {
		return this.journalDescription;
	}


	public void setJournalDescription(String journalDescription) {
		this.journalDescription = journalDescription;
	}


	public Date getMaxTransactionDate() {
		return this.maxTransactionDate;
	}


	public void setMaxTransactionDate(Date maxTransactionDate) {
		this.maxTransactionDate = maxTransactionDate;
	}


	public Date getMinTransactionDate() {
		return this.minTransactionDate;
	}


	public void setMinTransactionDate(Date minTransactionDate) {
		this.minTransactionDate = minTransactionDate;
	}


	public Date getMaxSettlementDate() {
		return this.maxSettlementDate;
	}


	public void setMaxSettlementDate(Date maxSettlementDate) {
		this.maxSettlementDate = maxSettlementDate;
	}


	public Date getMinSettlementDate() {
		return this.minSettlementDate;
	}


	public void setMinSettlementDate(Date minSettlementDate) {
		this.minSettlementDate = minSettlementDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getPostingDate() {
		return this.postingDate;
	}


	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getPositionCostBasis() {
		return this.positionCostBasis;
	}


	public void setPositionCostBasis(BigDecimal positionCostBasis) {
		this.positionCostBasis = positionCostBasis;
	}


	public BigDecimal getBaseDebitCredit() {
		return this.baseDebitCredit;
	}


	public void setBaseDebitCredit(BigDecimal baseDebitCredit) {
		this.baseDebitCredit = baseDebitCredit;
	}


	public BigDecimal getLocalDebitCredit() {
		return this.localDebitCredit;
	}


	public void setLocalDebitCredit(BigDecimal localDebitCredit) {
		this.localDebitCredit = localDebitCredit;
	}


	public BigDecimal getBaseDebitOrCredit() {
		return this.baseDebitOrCredit;
	}


	public void setBaseDebitOrCredit(BigDecimal baseDebitOrCredit) {
		this.baseDebitOrCredit = baseDebitOrCredit;
	}


	public BigDecimal getLocalDebitOrCredit() {
		return this.localDebitOrCredit;
	}


	public void setLocalDebitOrCredit(BigDecimal localDebitOrCredit) {
		this.localDebitOrCredit = localDebitOrCredit;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer[] getFkFieldIds() {
		return this.fkFieldIds;
	}


	public void setFkFieldIds(Integer[] fkFieldIds) {
		this.fkFieldIds = fkFieldIds;
	}


	public Boolean getOpening() {
		return this.opening;
	}


	public void setOpening(Boolean opening) {
		this.opening = opening;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public boolean isAllowDeleted() {
		return this.allowDeleted;
	}


	public void setAllowDeleted(boolean allowDeleted) {
		this.allowDeleted = allowDeleted;
	}


	public Boolean getModified() {
		return this.modified;
	}


	public void setModified(Boolean modified) {
		this.modified = modified;
	}
}
