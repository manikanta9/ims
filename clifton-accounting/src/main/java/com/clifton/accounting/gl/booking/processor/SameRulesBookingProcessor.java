package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.List;


/**
 * The <code>SameRulesBookingProcessor</code> class is a booking processor base class that can be implemented
 * by sub-systems that always apply the same List of rules for all of its entities.
 * <p/>
 * Sub-system specific booking processor must be defined in application context and "bookingRules" property
 * must be set (the order of rules is important).
 *
 * @param <T>
 * @author akorver
 * @author vgomelsky
 */
public abstract class SameRulesBookingProcessor<T extends BookableEntity> extends BaseBookingProcessor<T> {

	private List<AccountingBookingRule<T>> bookingRules;


	@Override
	public List<AccountingBookingRule<T>> getBookingRules(@SuppressWarnings("unused") BookingSession<T> bookingSession) {
		ValidationUtils.assertNotEmpty(getBookingRules(), "At least one AccountingBookingRule must be set.");
		return getBookingRules();
	}


	public List<AccountingBookingRule<T>> getBookingRules() {
		return this.bookingRules;
	}


	public void setBookingRules(List<AccountingBookingRule<T>> bookingRules) {
		this.bookingRules = bookingRules;
	}
}
