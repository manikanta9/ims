package com.clifton.accounting.gl.position;

import com.clifton.accounting.gl.AccountingTransactionInfo;

import java.util.function.Predicate;


/**
 * A selection of accounting position filter types. These types may be used to filter a list of positions.
 *
 * @author MikeH
 */
public enum AccountingPositionTypes {

	ANY_POSITION(position -> true),
	COLLATERAL_POSITION(position -> position.getAccountingAccount().isCollateral()),
	NON_COLLATERAL_POSITION(position -> !position.getAccountingAccount().isCollateral());


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final Predicate<AccountingTransactionInfo> positionTypeMatcher;


	AccountingPositionTypes(Predicate<AccountingTransactionInfo> positionTypeMatcher) {
		this.positionTypeMatcher = positionTypeMatcher;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks if the given position matches the given position type.
	 *
	 * @param position the position to check
	 * @return {@code true} if the position is of the given type, or {@code false} otherwise
	 */
	public boolean isMatchingPosition(AccountingTransactionInfo position) {
		return this.positionTypeMatcher.test(position);
	}
}
