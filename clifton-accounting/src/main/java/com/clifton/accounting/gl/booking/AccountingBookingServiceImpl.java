package com.clifton.accounting.gl.booking;


import com.clifton.accounting.gl.booking.processor.AccountingBookingProcessor;
import com.clifton.accounting.gl.booking.processor.AccountingBookingProcessorLocator;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * The <code>AccountingBookingServiceImpl</code> class provides basic implementation of AccountingBookingService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingBookingServiceImpl<T extends BookableEntity> implements AccountingBookingService<T> {

	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;
	private AccountingBookingProcessorLocator accountingBookingProcessorLocator;

	private ContextHandler contextHandler;
	private SecurityAuthorizationService securityAuthorizationService;
	private SynchronizationHandler synchronizationHandler;
	private BookableEntityLockProvider<T> bookableEntityLockProvider;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BookableEntity getBookableEntity(AccountingJournal journal) {
		AccountingJournalType journalType = journal.getJournalType();
		if (!journalType.isSubsystemPresent()) {
			// adjusting journals or general journal do not have a source: null
			return null;
		}

		@SuppressWarnings("unchecked")
		AccountingBookingProcessor<T> processor = (AccountingBookingProcessor<T>) getAccountingBookingProcessorLocator().getAccountingBookingProcessor(journalType.getName());
		return processor.getBookableEntity(journal.getFkFieldId());
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<AccountingBookingDescriptor> getAccountingUnbookedEntityList(String journalTypeName) {
		// get matching journal types first (one or all)
		List<AccountingJournalType> typeList;
		if (journalTypeName == null || "null".equalsIgnoreCase(journalTypeName)) {
			typeList = getAccountingJournalService().getAccountingJournalTypeList();
		}
		else {
			AccountingJournalType type = getAccountingJournalService().getAccountingJournalTypeByName(journalTypeName);
			ValidationUtils.assertNotNull(type, "Cannot find journal type for with name = " + journalTypeName);
			typeList = new ArrayList<>();
			typeList.add(type);
		}

		List<AccountingBookingDescriptor> result = new ArrayList<>();
		for (AccountingJournalType type : CollectionUtils.getIterable(typeList)) {
			// skip general journals which are not booked but are posted directly
			if (type.isSubsystemJournal() && type.isSubsystemPresent()) {
				// add all unbooked entities of this type
				AccountingBookingProcessor<T> processor = (AccountingBookingProcessor<T>) getAccountingBookingProcessorLocator().getAccountingBookingProcessor(type.getName());
				List<T> unbookedList = processor.getUnbookedEntityList();
				for (BookableEntity entity : CollectionUtils.getIterable(unbookedList)) {
					AccountingBookingDescriptor descriptor = new AccountingBookingDescriptor();
					descriptor.setJournalType(type);
					descriptor.setLabel(entity.getLabel());
					descriptor.setId(((Number) entity.getIdentity()).longValue());
					descriptor.setCreateDate(entity.getCreateDate());
					descriptor.setCreateUserId(entity.getCreateUserId());
					descriptor.setUpdateDate(entity.getUpdateDate());
					descriptor.setUpdateUserId(entity.getUpdateUserId());
					descriptor.setRv(entity.getRv());
					result.add(descriptor);
				}
			}
		}

		return result;
	}


	@Override
	// Do not automatically propagate rollbacks to top-most @Transactional; allow parent to handle exceptions and bubble them to trigger rollbacks if necessary
	@Transactional(readOnly = true, noRollbackFor = ValidationException.class)
	public AccountingJournal getAccountingJournalPreview(BookingPreviewCommand<T> command) {
		AccountingBookingProcessor<T> processor = getAccountingBookingProcessor(command.getJournalTypeName());
		BookingSession<T> bookingSession = runBookingProcess(processor, command.getBookableEntity(), command.getBookingRuleScope(), false);
		return bookingSession.getJournal();
	}


	/**
	 * Booking process must be synchronized because it may affect existing positions which will be used as a new basis for the following bookings.
	 * Hoping that this won't cause significant performance degradation or deadlocks.
	 */
	@Override
	@Transactional(timeout = 180)
	public AccountingJournal bookAccountingJournal(String journalTypeName, int sourceEntityId, boolean postJournal) {
		// transaction must start and end before getting out of synchronized block to guarantee synchronization
		AccountingBookingProcessor<T> processor = getAccountingBookingProcessor(journalTypeName);
		return doBookAccountingJournal(processor, processor.getBookableEntity(sourceEntityId), postJournal);
	}


	@Override
	@Transactional(timeout = 180)
	public AccountingJournal bookAccountingJournal(String journalTypeName, T bookedEntity, boolean postJournal) {
		// transaction must start and end before getting out of synchronized block to guarantee synchronization
		AccountingBookingProcessor<T> processor = getAccountingBookingProcessor(journalTypeName);
		return doBookAccountingJournal(processor, bookedEntity, postJournal);
	}


	private AccountingJournal doBookAccountingJournal(AccountingBookingProcessor<T> processor, T bookedEntity, boolean postJournal) {
		String lockMessage = getBookableEntityLockProvider().getLockMessage(bookedEntity);
		Set<String> lockKeySet = getBookableEntityLockProvider().getLockKeySet(bookedEntity);

		return getSynchronizationHandler().call(
				SynchronizableBuilder.forLocking(getBookableEntityLockProvider().getSecureAreaName(), lockMessage, lockKeySet)
						.withMillisecondsToWaitIfBusy(getBookableEntityLockProvider().getLockWaitMs())
						.buildWithCallableAction(() -> {
							BookingSession<T> bookingSession = runBookingProcess(processor, bookedEntity, null, true);

							// if the rules have cleared out this journal, delete it. (all zero amounts for example)
							AccountingJournal journal = bookingSession.getJournal();
							if (CollectionUtils.isEmpty(journal.getJournalDetailList())) {
								return null;
							}

							// for {@link RuleViolationAware} entities (AccountingPositionTransfer), make sure that all the rules were processed successfully
							T bookableEntity = bookingSession.getBookableEntity();
							if (bookableEntity instanceof RuleViolationAware) {
								RuleViolationAware violationAware = (RuleViolationAware) bookableEntity;
								if (violationAware.getViolationStatus() != null) {
									if (!violationAware.getViolationStatus().isSuccess()) {
										throw new ValidationException(StringUtils.splitWords(bookableEntity.getClass().getSimpleName()) +
												" with id [" + bookableEntity.getIdentity() + "] and label [" + bookableEntity.getLabel() +
												"] cannot be booked while it has not ignored rule violation(s).");
									}
								}
							}

							// save the journal and mark source entity as booked
							if (postJournal) {
								// immediately post the journal using efficient implementation which avoids duplicate validation during save
								journal = getAccountingPostingService().saveAccountingJournalAndPostIt(journal, bookableEntity);
							}
							else {
								journal = getAccountingJournalService().saveAccountingJournal(journal);
							}
							processor.markBooked(bookableEntity);

							return journal;
						})
		);
	}


	private BookingSession<T> runBookingProcess(AccountingBookingProcessor<T> processor, T bookedEntity, BookingRuleScopes ruleScope, boolean validate) {
		// create new journal and populate header fields
		AccountingJournal journal = new AccountingJournal();
		journal.setFkFieldId((Integer) bookedEntity.getIdentity());

		AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalTypeByName(processor.getJournalTypeName());
		ValidationUtils.assertNotNull(journalType, "Cannot find journal type with name = " + processor.getJournalTypeName());
		journal.setJournalType(journalType);
		journal.setSystemTable(journalType.getSystemTable());

		if (validate) {
			// Allowed to be null for "previews" which don't validate - so need to verify the id is set here.
			ValidationUtils.assertNotNull(journal.getFkFieldId(), "Source Entity ID is required to create BookingSession.");
			// for sub-systems that support more than one booking per source entity (accrue first and then reverse later)
			// make sure that there are no unbooked journals for the same entity: must post first journal before booking second
			AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
			searchForm.setJournalTypeId(journalType.getId());
			searchForm.setFkFieldId(journal.getFkFieldId());
			searchForm.setUnpostedJournalsOnly(true);
			searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
			List<AccountingJournal> unpostedList = getAccountingJournalService().getAccountingJournalList(searchForm);
			AccountingJournal unpostedJournal = CollectionUtils.getFirstElement(unpostedList);
			if (unpostedJournal != null) {
				throw new ValidationException("Cannot book journal '" + journal.getLabel() + "' because another journal for the same entity has not been posted: " + unpostedJournal.getLabel());
			}
		}

		// apply all booking rules: creates appropriate journal details
		BookingSession<T> bookingSession = processor.newBookingSession(journal, bookedEntity);
		for (AccountingBookingRule<T> bookingRule : CollectionUtils.getIterable(processor.getBookingRules(bookingSession))) {
			if (ruleScope == null || bookingRule.getRuleScopes().contains(ruleScope)) {
				bookingRule.applyRule(bookingSession);

				LogUtils.info(LogCommand.ofMessageSupplier(getClass(),
						() -> {
							StringBuilder detailsString = new StringBuilder();
							detailsString.append(bookingRule.toString()).append("\n\n");
							boolean firstDefinition = true;
							for (AccountingJournalDetailDefinition entry : CollectionUtils.getIterable(journal.getJournalDetailList())) {
								detailsString.append(entry.toStringFormatted(firstDefinition, true));
								detailsString.append(System.lineSeparator());
								firstDefinition = false;
							}
							return detailsString.toString();
						})
				);
			}
			else {
				LogUtils.debug(getClass(), "Skipped Booking Rule: " + bookingRule);
			}
		}

		return bookingSession;
	}


	@SuppressWarnings("unchecked")
	private AccountingBookingProcessor<T> getAccountingBookingProcessor(String journalTypeName) {
		return (AccountingBookingProcessor<T>) getAccountingBookingProcessorLocator().getAccountingBookingProcessor(journalTypeName);
	}


	@Override
	@Transactional
	public void unbookAccountingJournal(long journalId, boolean deleteSource) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(journalId);

		boolean allow = getSecurityAuthorizationService().isSecurityAccessAllowed("AccountingJournal", SecurityPermission.PERMISSION_DELETE);
		if (!allow) {
			allow = getSecurityAuthorizationService().isSecurityAccessAllowed(journal.getJournalType().getSystemTable().getSecurityResource().getName(), SecurityPermission.PERMISSION_WRITE);
			// also allow the person who booked Journal to unbook it
			if (allow && journal.getJournalType().isSameUserAllowedToUnbook()) {
				SecurityUser user = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
				if (!user.getId().equals(journal.getCreateUserId())) {
					allow = false;
				}
			}
		}
		if (!allow) {
			throw new AccessDeniedException("DELETE permission is required to AccountingJournal security resource, or current user must be the one who booked journal and " +
					"have write access to the security resource of the journal type table [" + journal.getJournalType().getSystemTable().getSecurityResource().getName() + "].");
		}

		// for sub-systems that support more than one booking per source entity (accrue first and then reverse later)
		// make sure that the latest journal is unbooked first
		if (journal.getJournalType().isSubsystemPresent()) {
			AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
			searchForm.setJournalTypeId(journal.getJournalType().getId());
			searchForm.setFkFieldId(journal.getFkFieldId());
			searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
			List<AccountingJournal> journalList = getAccountingJournalService().getAccountingJournalList(searchForm);
			for (AccountingJournal siblingJournal : CollectionUtils.getIterable(journalList)) {
				if (siblingJournal.getId().compareTo(journal.getId()) > 0) {
					throw new ValidationException("Cannot unbook journal '" + journal.getLabel() + "' because it was booked before another journal for the same sub-system entity: "
							+ siblingJournal.getLabel());
				}
			}
		}


		// delete accounting journal that was just unbooked (also marks sub-system journal as unbooked)
		getAccountingJournalService().deleteAccountingJournal(journalId, deleteSource);
	}


	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public T markAccountingSubSystemJournalUnbooked(AccountingJournal journal, boolean deleteSource) {
		T bookableEntity = null;
		if (journal.getJournalType().isSubsystemPresent()) {
			AccountingBookingProcessor<T> processor = (AccountingBookingProcessor<T>) getAccountingBookingProcessorLocator().getAccountingBookingProcessor(journal.getJournalType().getName());
			bookableEntity = processor.markUnbooked(journal.getFkFieldId());
			if (deleteSource) {
				processor.deleteSourceEntity(journal.getFkFieldId());
			}
		}
		return bookableEntity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public AccountingBookingProcessorLocator getAccountingBookingProcessorLocator() {
		return this.accountingBookingProcessorLocator;
	}


	public void setAccountingBookingProcessorLocator(AccountingBookingProcessorLocator accountingBookingProcessorLocator) {
		this.accountingBookingProcessorLocator = accountingBookingProcessorLocator;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}


	public BookableEntityLockProvider<T> getBookableEntityLockProvider() {
		return this.bookableEntityLockProvider;
	}


	public void setBookableEntityLockProvider(BookableEntityLockProvider<T> bookableEntityLockProvider) {
		this.bookableEntityLockProvider = bookableEntityLockProvider;
	}
}
