package com.clifton.accounting.gl.position.closing;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;


/**
 * Provides methods that will calculate the lots (and market value of the lots) that will be closed for a combination of securities and quantities.
 *
 * @author mwacker
 */
public interface AccountingPositionClosingService {

	/**
	 * Get a list of positions to close for each combination of securities and quantities.
	 *
	 * @param command
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	@ModelAttribute("data")
	public List<AccountingPositionClosing> getAccountingPositionClosingLotList(AccountingPositionClosingCommand command);


	/**
	 * Get the market value of the lots that will be selected.
	 * <p>
	 * NOTE:  If the selectedPositionList is submitted, the result will be the difference of the total market value of the
	 * selected positions and new calculated market value.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getAccountingPositionClosingLotMarketValue(AccountingPositionClosingCommand command);
}
