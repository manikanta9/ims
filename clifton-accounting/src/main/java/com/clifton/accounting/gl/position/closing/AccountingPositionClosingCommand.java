package com.clifton.accounting.gl.position.closing;


import com.clifton.accounting.gl.position.AccountingPositionCommand;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPositionClosingCommand</code> class specifies parameters to be used to retrieve the
 * lots that will be closed for a given security(s) and quantity.
 *
 * @author mwacker
 */
public class AccountingPositionClosingCommand extends AccountingPositionCommand {

	private List<AccountingPositionClosingSecurityQuantity> securityQuantityList;
	private List<AccountingPositionClosingTransaction> selectedPositionList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingPositionClosingCommand onTransactionDate(Date transactionDate) {
		AccountingPositionClosingCommand command = new AccountingPositionClosingCommand();
		command.setTransactionDate(transactionDate);
		return command;
	}


	public static AccountingPositionClosingCommand onSettlementDate(Date settlementDate) {
		AccountingPositionClosingCommand command = new AccountingPositionClosingCommand();
		command.setSettlementDate(settlementDate);
		return command;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<AccountingPositionClosingSecurityQuantity> getSecurityQuantityList() {
		return this.securityQuantityList;
	}


	public void setSecurityQuantityList(List<AccountingPositionClosingSecurityQuantity> securityQuantityList) {
		this.securityQuantityList = securityQuantityList;
	}


	public List<AccountingPositionClosingTransaction> getSelectedPositionList() {
		return this.selectedPositionList;
	}


	public void setSelectedPositionList(List<AccountingPositionClosingTransaction> selectedPositionList) {
		this.selectedPositionList = selectedPositionList;
	}
}
