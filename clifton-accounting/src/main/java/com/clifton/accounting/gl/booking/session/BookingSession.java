package com.clifton.accounting.gl.booking.session;


import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;

import java.util.List;
import java.util.Map;


/**
 * The <code>BookingSession</code> interface defines a booking session that maintains state during booking process.
 * The session usually holds the journal that will be created after booking (each rule add/modifies journal details)
 * as well as sub-system entity being booked.  More complex booking sessions can be implemented.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface BookingSession<T extends BookableEntity> {

	/**
	 * Returns AccountingJournal that will be created after booking process is completed.
	 * Booking rules modify this journal by adding/updating journal details.
	 */
	public AccountingJournal getJournal();


	/**
	 * Returns the sub-system entity that is being booked.
	 * Booking process creates a corresponding AccountingJournal for this entity.
	 */
	public T getBookableEntity();


	/**
	 * Returns a Map of current positions used during this booking session.  Because existing positions maybe reduced
	 * or fully closed while executing a set of rules, we must use the latest available data from the booking session.
	 * <p/>
	 * An empty List means that there is no position while null means that position hasn't been retrieved yet.
	 * <p/>
	 * Use getPositionKey method to generate the key for a given position (client account + holding account + security + GL Account).
	 */
	public Map<String, List<BookingPosition>> getCurrentPositions();


	/**
	 * Add the specified journal detail as an existing position to this booking session.  This is usually used when a new position
	 * created during this booking session (not in GL yet), may need to be immediately partially/fully closed.
	 * For example, fractional shares closing for a Stock Split.
	 */
	public void addNewCurrentPosition(AccountingJournalDetail newPosition);


	/**
	 * Add the specified Accounting position to this booking session.
	 */
	public void addNewCurrentPosition(AccountingPosition newPosition);


	/**
	 * Returns BookingPosition that the specified definition points to (closes).
	 */
	public BookingPosition getBookingPosition(AccountingJournalDetailDefinition definition);


	/**
	 * Generates and returns a key to be used with getCurrentPositions methods.
	 * It's a combination of Client Account, Holding Account, Security, and GL Account.
	 */
	public String getPositionKey(AccountingTransactionInfo accountingTransactionInfo);


	/**
	 * Sorts all current positions using the specified ordering methodology.
	 */
	public void sortPositions(AccountingPositionOrders positionOrder);
}
