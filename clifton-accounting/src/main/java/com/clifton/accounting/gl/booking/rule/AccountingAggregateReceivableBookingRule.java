package com.clifton.accounting.gl.booking.rule;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;


/**
 * The AccountingAggregateReceivableBookingRule class aggregates receivable details that have all fields except for amounts identical.
 *
 * @author vgomelsky
 */
public class AccountingAggregateReceivableBookingRule<T extends BookableEntity> extends AccountingAggregateCurrencyBookingRule<T> {


	@Override
	protected boolean isEligibleForAggregation(AccountingJournalDetailDefinition detail) {
		return detail.getAccountingAccount().isReceivable();
	}


	@Override
	protected String generateNaturalKey(AccountingJournalDetailDefinition detail) {
		StringBuilder result = new StringBuilder();
		result.append(detail.createNaturalKey());
		result.append("TD").append(detail.getTransactionDate());
		result.append("DS").append(detail.getDescription());
		result.append("PT").append(detail.getParentTransaction());
		result.append("PD").append(detail.getParentDefinition());
		return result.toString();
	}
}
