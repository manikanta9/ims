package com.clifton.accounting.gl.comparison;


import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingBalanceValueNotExistsComparison</code> class is a {@link Comparison} that evaluates to true
 * if investment account (supports either client or holding account) does not have an accounting account balance
 * TODAY for the selected gl accounts, or excluded gl accounts
 * <p>
 * Used to determine if client or holding accounts can be put on pause (allowed to have cash) or terminated (nothing allowed)
 *
 * @author manderson
 */
public class AccountingBalanceValueNotExistsComparison implements Comparison<IdentityObject> {

	private AccountingValuationService accountingValuationService;

	/**
	 * Ability to limit to specific gl accounts
	 * Note: If not supplied, then filter isn't applied - i.e. include all
	 */
	private List<Short> accountingAccountIds;

	/**
	 * Ability to exclude specific gl accounts
	 */
	private List<Short> excludeAccountingAccountIds;

	/**
	 * Can leave blank if the bean itself if an Account
	 * Otherwise pulls the account from the bean in the specified path
	 */
	private String investmentAccountBeanPath;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		InvestmentAccount account;
		if (!StringUtils.isEmpty(getInvestmentAccountBeanPath())) {
			account = (InvestmentAccount) BeanUtils.getPropertyValue(bean, getInvestmentAccountBeanPath());
			if (account == null) {
				throw new ValidationException("Unable to evaluate [AccountingBalanceValueNotExistsComparison] because account is null.  Looking for account on bean ["
						+ bean.getClass().getName() + "], property [" + getInvestmentAccountBeanPath() + "]");
			}
		}
		else if (bean instanceof InvestmentAccount) {
			account = (InvestmentAccount) bean;
		}
		else {
			throw new ValidationException("Unable to evaluate [AccountingBalanceValueNotExistsComparison] because bean is not an account and investmentAccountBeanPath is missing.  Looking for account on bean ["
					+ bean.getClass().getName() + "]");
		}
		return evaluateForAccount(account, context);
	}


	private boolean evaluateForAccount(InvestmentAccount bean, ComparisonContext context) {
		if (bean == null) {
			throw new ValidationException("Unable to evaluate [AccountingBalanceValueNotExistsComparison] because account is missing.");
		}

		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(new Date());

		if (bean.getType().isOurAccount()) {
			searchForm.setClientInvestmentAccountId(bean.getId());
		}
		else {
			searchForm.setHoldingInvestmentAccountId(bean.getId());
		}
		searchForm.setAccountingAccountIds(CollectionUtils.toArrayOrNull(getAccountingAccountIds(), Short.class));
		searchForm.setExcludeAccountingAccountIds(CollectionUtils.toArrayOrNull(getExcludeAccountingAccountIds(), Short.class));

		List<AccountingBalanceValue> balanceValueList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);

		boolean result = CollectionUtils.isEmpty(balanceValueList);

		if (context != null) {
			if (result) {
				context.recordTrueMessage("(Account " + bean.getLabel() + " had no specified balance on [" + DateUtils.fromDateShort(searchForm.getTransactionDate()) + "]");
			}
			else {
				Map<String, List<AccountingBalanceValue>> valueMap = BeanUtils.getBeansMap(balanceValueList, balanceValue -> balanceValue.getAccountingAccount().getName());
				context.recordFalseMessage("(Account " + bean.getLabel() + " had [" + CollectionUtils.toString(valueMap.keySet(), 5) + "] balance(s) on [" + DateUtils.fromDateShort(searchForm.getTransactionDate()));
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public List<Short> getAccountingAccountIds() {
		return this.accountingAccountIds;
	}


	public void setAccountingAccountIds(List<Short> accountingAccountIds) {
		this.accountingAccountIds = accountingAccountIds;
	}


	public List<Short> getExcludeAccountingAccountIds() {
		return this.excludeAccountingAccountIds;
	}


	public void setExcludeAccountingAccountIds(List<Short> excludeAccountingAccountIds) {
		this.excludeAccountingAccountIds = excludeAccountingAccountIds;
	}


	public String getInvestmentAccountBeanPath() {
		return this.investmentAccountBeanPath;
	}


	public void setInvestmentAccountBeanPath(String investmentAccountBeanPath) {
		this.investmentAccountBeanPath = investmentAccountBeanPath;
	}
}
