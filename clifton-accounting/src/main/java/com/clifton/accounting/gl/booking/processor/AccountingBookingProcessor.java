package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>AccountingBookingProcessor</code> interface defines methods that should be implemented by accounting booking processors.
 * Each accounting journal type must provide an AccountingBookingProcessor that knows how to book and unbook journals of that type.
 * <p>
 * Booking means creating of AccountingJournal and corresponding details for a particular module/sub-system (trading, simple journals, m2m, etc.).
 * Each module/sub-system can have its own logic and details. During booking, they create unique journal detail transaction sets that follow accounting rules.
 * <p>
 * At the end of booking process, BookingDate is set on booked sub-system entry. The same date field is cleared (set to null) if the journal is deleted (unbooked).
 * Sub-system should implement validation logic that disallows updates to entities that have been booked.
 *
 * @author vgomelsky
 */
public interface AccountingBookingProcessor<T extends BookableEntity> {

	/**
	 * Returns the name of AccountingJournalType that this processor handles.
	 */
	public String getJournalTypeName();


	/**
	 * Returns the bookable entity with the given ID.
	 */
	public T getBookableEntity(Serializable id);


	/**
	 * Returns a List of all unbooked entities for this journal type.
	 * Certain implementations need to filter out more than just (BookingDate IS NULL) entities.
	 * For example, Cancelled trades don't have bookingDate set but should not be considered unbooked.
	 */
	public List<T> getUnbookedEntityList();


	/**
	 * Creates and returns a new BookingSession for the specified journal.
	 * The context will holds the state of a process being booked and is passed
	 * to each booking rule which can update the state.
	 *
	 * @param journal
	 * @param bean    if the bean is null, will look it up based on journal's FKFieldID
	 */
	public BookingSession<T> newBookingSession(AccountingJournal journal, T bean);


	/**
	 * Returns a List of booking rules that are applied to sub-system entities in the order returned.
	 */
	public List<AccountingBookingRule<T>> getBookingRules(BookingSession<T> bookingSession);


	/**
	 * Update the status of the specified source entity as being booked (usually set bookingDate field)
	 */
	public T markBooked(T bookableEntity);


	/**
	 * Update the status of the specified source entity as not booked (usually clear bookingDate field)
	 */
	public T markUnbooked(int sourceEntityId);


	/**
	 * Deletes the source entity identified by the specified primary key.  This is usually done immediately after it was unbooked.
	 */
	public void deleteSourceEntity(int sourceEntityId);
}
