package com.clifton.accounting.gl.booking;


import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.UpdatableEntity;

import java.util.Date;
import java.util.Set;


/**
 * The <code>BookableEntity</code> interface marks an entity as "bookable".
 * When an entity is booked (corresponding accounting journal is created),
 * "bookingDate" field is set. When an entity is "unbooked" this date is cleared.
 *
 * @author vgomelsky
 */
public interface BookableEntity extends IdentityObject, LabeledObject, UpdatableEntity {

	/**
	 * Returns Accounting Closing Method (FIFO, LIFO, HIFO, etc.) that should be used during booking process for this entity.
	 */
	public default AccountingClosingMethods getAccountingClosingMethod() {
		return AccountingClosingMethods.FIFO;
	}


	/**
	 * Returns the date when this entity was booked (a corresponding journal was created for the entity).
	 * If null is returned, then the entity hasn't been booked yet or was unbooked.
	 */
	public Date getBookingDate();


	/**
	 * Makes this entity as being booked on the specified date or unbooks the entity if null is passed.
	 */
	public void setBookingDate(Date bookingDate);


	/**
	 * Returns a set of the {@link IdentityObject}s this bookable entity needs to obtain locks for
	 * during the booking process. The locks will be obtained around the execution of the booking
	 * process to provide narrowly scoped synchronization. The locks will be obtained for each item
	 * in the set based on the set's iterator functionality.
	 */
	public Set<IdentityObject> getEntityLockSet();
}
