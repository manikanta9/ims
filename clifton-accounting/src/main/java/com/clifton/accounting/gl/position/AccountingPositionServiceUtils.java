package com.clifton.accounting.gl.position;


import com.clifton.core.util.concurrent.ExecutableAction;


/**
 * The <code>WorkflowTransitionUtils</code> class contains utility methods for workflow transitions.
 *
 * @author vgomelsky
 */
public class AccountingPositionServiceUtils {

	/**
	 * This is used to run the position retrieval select command using Option (recompile)  Currently only used for REPO uploads when bad index plan is used and timeouts occur.
	 * The only workaround at this point is either splitting the file into much smaller groups OR having support rebuild indexes.
	 */
	private static final ThreadLocal<Boolean> runWithOptionRecompile = new ThreadLocal<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if should run the position retrieval with Option (Recompile) option turned on.
	 */
	public static boolean isRunWithOptionRecompile() {
		return Boolean.TRUE.equals(runWithOptionRecompile.get());
	}


	private static void enableRunWithOptionRecompile() {
		runWithOptionRecompile.set(Boolean.TRUE);
	}


	private static void disableRunWithOptionRecompile() {
		runWithOptionRecompile.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows turns on the option recompile for the position retrieval sql select command. Guarantees to enable
	 * before callback execution and disabling after.
	 */
	public static void executeWithOptionRecompile(ExecutableAction callback) {
		try {
			enableRunWithOptionRecompile();
			callback.execute();
		}
		finally {
			disableRunWithOptionRecompile();
		}
	}
}
