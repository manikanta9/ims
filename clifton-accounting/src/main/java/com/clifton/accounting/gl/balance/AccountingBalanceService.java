package com.clifton.accounting.gl.balance;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.accounting.gl.balance.search.AccountingBalanceExtendedSearchForm;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingBalanceService</code> interface defines methods
 * for retrieving general ledger account balances based on various criterias.
 *
 * @author vgomelsky
 */
public interface AccountingBalanceService {

	/**
	 * Returns the General Ledger balance for the specified accounts on accounts for a specified date or date range.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingBalance> getAccountingBalanceList(AccountingBalanceSearchForm searchForm);


	/**
	 * Returns the General Ledger balance for the specified accounts for the specified date ranging by matching the parent investment security.
	 * Allows accumulating account balances for 'cash' accounts related to a specific non-cash parent security.
	 */
	@DoNotAddRequestMapping
	public List<AccountingBalanceExtended> getChildAccountingBalanceExtendedListForParentSecurity(AccountingBalanceExtendedSearchForm searchForm);


	/**
	 * Returns the list of client accounts associated with the General Ledger balances for the specified parameters.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<InvestmentAccount> getAccountingBalanceClientAccountList(AccountingBalanceSearchForm searchForm);


	/**
	 * Returns the General Ledger balance for the specified accounts on the specified date in base currency based on the
	 * parameters set in the search form
	 * <p>
	 * Balance = SUM(Debit - Credit)
	 */
	@ResponseBody
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getAccountingAccountBalanceBase(AccountingAccountBalanceSearchForm searchForm);


	/**
	 * Returns the General Ledger balance for the specified accounts on the specified date in local currency.
	 * Balance = SUM(Debit - Credit)
	 * <p>
	 * NOTE: Used for viewing Currency Balances
	 */
	@ResponseBody
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getAccountingBalanceForSecurityLocal(AccountingBalanceCommand command);


	/**
	 * Returns a Map of the General Ledger cash base currency balances for each client {@link com.clifton.investment.account.InvestmentAccount} that is a member of the
	 * provided array of client account IDs and/or the {@link com.clifton.investment.account.group.InvestmentAccountGroup} with provided ID for the transaction date.
	 * <p>
	 * The array of client account IDs, the client investment account group ID, or both must be defined.
	 * <p>
	 * Cash Balances are from Accounting Account where IsCash = 1, IsPosition = 0 and (exclude collateral, then IsCollateral = 0 otherwise no filter on collateral)
	 */
	@DoNotAddRequestMapping
	public Map<Integer, BigDecimal> getAccountingCashBalanceMap(AccountingBalanceCommand command);


	/*
	 * Returns the General Ledger cash balance mapped by date for an account
	 *
	 * Similar Logic to {@link AccountingBalanceService getAccountingCashBalance} but specific query
	 * to make it the most efficient - especially when used for period average
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public Map<Date, BigDecimal> getAccountingCashBalanceMapIncludeReceivables(AccountingBalanceCommand command);


	/*
	 * Returns the General Ledger non position assets and liabilities mapped by date for an account
	 *
	 * Specific query to make it the most efficient - especially when used for period average
	 * First groups the results by holding account / date / trading currency, then does FX rate look up (for faster retrieval)
	 */
	@DoNotAddRequestMapping
	public Map<Date, BigDecimal> getAccountingNonPositionAssetsAndLiabilities(AccountingBalanceCommand command);


	/**
	 * Returns the General Ledger aggregated base balance of child transactions of the provided {@link com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl.AccountingAccountIds}
	 * for each parent transaction ID provided. Child transactions to aggregate are determined by those with a parent transaction ID equal to one of the position transaction IDs.
	 * <p>
	 * Similar logic to {@link com.clifton.accounting.gl.AccountingTransactionService#getAccountingTransactionList(AccountingTransactionSearchForm)}
	 * with efficiency of aggregating balances on the database and avoiding parameter constraint issues by using specific SQL query.
	 * NOTE: Only 2100 parameters can be sent in a query generated via search form criteria.
	 */
	@DoNotAddRequestMapping
	public Map<Long, BigDecimal> getAccountingChildBalanceMap(AccountingBalanceCommand command);


	/**
	 * Returns the net or total cash balance for the specified accounts on the specified date in base currency
	 * Balance = SUM(Debit - Credit)
	 * <p>
	 * At minimum a client account or holding account is required, but can have only one
	 * Cash Balances are from Accounting Account where IsCash = 1, IsPosition = 0 and (exclude collateral, then IsCollateral = 0 otherwise no filter on collateral)
	 * <p>
	 * IsNetOnly parameter will specify whether the date for calculation represents a transaction or settlement date.
	 * IsNetOnly = true means date is a transaction date where as IsNetOnly = false means settlement date is used.
	 */


	/**
	 * Depending on which parameters are defined in the {@link AccountingBalanceCommand}, this method will either  -
	 * Returns the balance for the specified accounts on the specified date in base currency
	 * Balance = SUM(Debit - Credit)
	 * <p>
	 * A client account or holding account is required, but can have only one
	 * <p>
	 * OR
	 * <p>
	 * Returns the net or total cash balance for the specified accounts on the specified date in base currency
	 * Balance = SUM(Debit - Credit)
	 * <p>
	 * At minimum a client account or holding account is required, but can have only one
	 * Cash Balances are from Accounting Account where IsCash = 1, IsPosition = 0 and (exclude collateral, then IsCollateral = 0 otherwise no filter on collateral)
	 * <p>
	 * IsNetOnly parameter will specify whether the date for calculation represents a transaction or settlement date.
	 * IsNetOnly = true means date is a transaction date where as IsNetOnly = false means settlement date is used.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getAccountingBalance(AccountingBalanceCommand command);


	/**
	 * Returns Balance Sheet rows for the specified required transactionDate search parameter.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingBalance> getAccountingBalanceSheetList(AccountingBalanceSearchForm searchForm);


	/**
	 * Returns Asset rows for the specified required transactionDate search parameter.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingBalance> getAccountingAssetList(AccountingBalanceSearchForm searchForm);


	/**
	 * Returns Income Statement rows for the specified required data range (fromTransactionDate, toTransactionDate) search parameters.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingBalance> getAccountingIncomeStatementList(AccountingBalanceSearchForm searchForm);


	/**
	 * Returns the number of days in the date range that the given account had a balance for the given
	 * accounting account
	 */
	@DoNotAddRequestMapping
	public Integer getAccountingAccountBalanceDaysActive(AccountingBalanceCommand command);
}
