package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingRemoveJournalDetailsBookingRule</code> removes all journal detail when the accounting account name is in the glAccountNameList.
 *
 * @param <T>
 * @author mwacker
 */
public class AccountingRemoveJournalDetailsBookingRule<T extends BookableEntity> implements AccountingBookingRule<T> {

	private List<String> glAccountNameList;


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		List<? extends AccountingJournalDetailDefinition> details = CoreCollectionUtils.clone(journal.getJournalDetailList());
		List<AccountingJournalDetailDefinition> removeDetails = new ArrayList<>();
		for (AccountingJournalDetailDefinition entry : CollectionUtils.getIterable(details)) {
			if (getGlAccountNameList().contains(entry.getAccountingAccount().getName())) {
				removeDetails.add(entry);
			}
		}
		journal.removeJournalEntries(removeDetails);
	}


	public List<String> getGlAccountNameList() {
		return this.glAccountNameList;
	}


	public void setGlAccountNameList(List<String> glAccountNameList) {
		this.glAccountNameList = glAccountNameList;
	}
}
