package com.clifton.accounting.gl.booking;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>AccountingBookingService</code> interface defines methods for booking/unbooking journals.
 * Booking process applies booking rules to bookable entities in order to convert them to corresponding
 * accounting journals. During booking process a position maybe split into multiple lots to account for
 * closings and openings, realized gain loss and commissions could be calculated, etc.
 * <p/>
 * Booked journals will need to be posted to teh General Ledger (often done immediately after booking).
 *
 * @author vgomelsky
 */
public interface AccountingBookingService<T extends BookableEntity> {

	/**
	 * Returns the bookable entity associated with the journal.
	 * Returns null for journal types that do not have bookable entities: adjusting journals, general journal.
	 */
	@DoNotAddRequestMapping
	public BookableEntity getBookableEntity(AccountingJournal journal);


	/**
	 * Returns a List of descriptors for source entities that haven't been booked yet.
	 * Returns entities of the specified type or if the type is null or "null" then all entities.
	 */
	@SecureMethod(dtoClass = AccountingJournal.class, namingConventionViolation = true)
	public List<AccountingBookingDescriptor> getAccountingUnbookedEntityList(String journalTypeName);


	/**
	 * Returns fully populated AccountingJournal for the specified BookableEntity. Runs full booking process.
	 * Does NOT save anything in the database.
	 * <p/>
	 * Can be used to determine results of booking without actually booking. This may serve as an input to other
	 * processes that would like to calculate something based on closing positions, etc.
	 */
	public AccountingJournal getAccountingJournalPreview(BookingPreviewCommand<T> command);


	/**
	 * Creates and returns new AccountingJournal of the specified type for the specified
	 * source entity (the type determines the source).
	 * <p/>
	 * Optionally, can post newly created journal to the general ledger.
	 */
	@RequestMapping("accountingJournalBook")
	@SecureMethod(tableResolverBeanName = "accountingBookingSecureMethodTableResolver")
	public AccountingJournal bookAccountingJournal(String journalTypeName, int sourceEntityId, boolean postJournal);


	/**
	 * Creates and returns new AccountingJournal of the specified type for the specified
	 * source entity (the type determines the source).
	 * <p/>
	 * Optionally, can post newly created journal to the general ledger.
	 */
	@DoNotAddRequestMapping
	public AccountingJournal bookAccountingJournal(String journalTypeName, T bookedEntity, boolean postJournal);


	/**
	 * Deletes the specified accounting journal (if it hasn't been posted yet) and,
	 * for non systemDefined journals, unbooks them (clears the booking date)
	 * <p/>
	 * Optionally, can delete the source entity(ies).
	 */
	@RequestMapping("accountingJournalUnbook")
	public void unbookAccountingJournal(long journalId, boolean deleteSource);


	@DoNotAddRequestMapping
	public T markAccountingSubSystemJournalUnbooked(AccountingJournal journal, boolean deleteSource);
}
