package com.clifton.accounting.gl.balance.search;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;


/**
 * The AccountingAccountFilters interface defines common filter methods that can be shared across multiple Search Forms and configured via the same code.
 *
 * @author vgomelsky
 */
public interface AccountingAccountFilters {

	public Short[] getAccountingAccountIds();


	public void setAccountingAccountIds(Short[] accountingAccountIds);


	public Boolean getPositionAccountingAccount();


	public void setPositionAccountingAccount(Boolean positionAccountingAccount);

	public Boolean getCashAccountingAccount();


	public void setCashAccountingAccount(Boolean cashAccountingAccount);


	public AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountIdName();


	public void setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName);


	public Short getAccountingAccountTypeId();


	public void setAccountingAccountTypeId(Short accountingAccountTypeId);


	public Short[] getAccountingAccountTypeIds();


	public void setAccountingAccountTypeIds(Short[] accountingAccountTypeIds);


	public String getAccountingAccountType();


	public void setAccountingAccountType(String accountingAccountType);


	public String[] getAccountingAccountTypes();


	public void setAccountingAccountTypes(String[] accountingAccountTypes);
}
