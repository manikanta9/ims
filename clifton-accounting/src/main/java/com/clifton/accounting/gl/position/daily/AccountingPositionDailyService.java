package com.clifton.accounting.gl.position.daily;


import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.util.status.Status;

import java.util.List;


/**
 * The <code>AccountingPositionDailyService</code> interface defines methods for working with AccountingPositionDaily.
 * The table can be rebuilt from GL and market data. It's used by various parts of the system to simplify and speedup processing.
 *
 * @author vgomelsky
 */
public interface AccountingPositionDailyService {

	public List<AccountingPositionDaily> getAccountingPositionDailyList(AccountingPositionDailySearchForm searchForm);


	/**
	 * Returns "Live" positions for the specified parameters. Calculates everything real time instead of looking up date in pre-built snapshot.
	 * Can be used to lookup positions based on Settlement Date.
	 */
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveList(AccountingPositionDailyLiveSearchForm liveSearchForm);


	/**
	 * Returns "Live" currency "positions" for the specified parameters.  Currency is not really a position in the system but this method
	 * aggregates corresponding currency balances into a single "lot" as if it were a position. This method is useful for mark to market calculations.
	 */
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveForCurrencyList(AccountingPositionDailyLiveSearchForm liveSearchForm);


	/**
	 * Deletes all AccountingPositionDaily records that reference the specified AccountingTransaction.
	 */
	public int deleteAccountingPositionDailyByAccountingTransaction(long accountingTransactionId);


	/**
	 * Rebuilds a portion of AccountingPositionDaily based on the specified filters.
	 */
	public Status rebuildAccountingPositionDaily(AccountingPositionDailyRebuildCommand command);


	/**
	 * Rebuilds all AccountingPositionDaily snapshots for all client accounts for the dates specified by the parameters.
	 */
	public Status rebuildAccountingPositionDailyForDate(DateGenerationOptions dateGenerationOptions, Integer dayCount);
}
