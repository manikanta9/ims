package com.clifton.accounting.gl.valuation;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.core.util.MathUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingBalanceValue</code> class represents an aggregate of lots/positions for a particular
 * holding account, client account, and security. It is built from corresponding {@link AccountingBalance}
 * objects but also adds to it current market data (price, exchange rate, accruals) in order to be able to calculate
 * current market value.
 * <p>
 * When possible, use {@link AccountingBalance} object because it's more light weight and is faster to populate.
 *
 * @author vgomelsky
 */
public class AccountingBalanceValue implements AccountingObjectInfo, AccountingBean, Serializable {

	/**
	 * Specifies whether this Balance Value is Pending (has not been posted to the General Ledger) or not.
	 */
	private final boolean pendingActivity;

	private final InvestmentAccount clientInvestmentAccount;
	private final InvestmentAccount holdingInvestmentAccount;
	private final AccountingAccount accountingAccount;
	private final InvestmentSecurity investmentSecurity;

	// snapshot's calculated/looked-up measures (fields represent remaining as opposed to original values):
	private final BigDecimal quantity;
	private final BigDecimal price; // latest known price if snapshotDate price is not available; corresponding Big security price if security has a corresponding Big security
	private final BigDecimal exchangeRateToBase;

	private final BigDecimal localCost; // cost of futures is 0 and for stocks it's the price paid, etc.
	private final BigDecimal baseCost;
	private final BigDecimal localCostBasis; // Market Value = Notional - Cost Basis + Cost + Accrual
	private final BigDecimal baseCostBasis;
	private final BigDecimal localNotional; // Unrealized Gain / Loss = Notional - Cost Basis

	private final BigDecimal localAccrual1; // for securities that support it: Accrued Interest for bonds; set to Zero otherwise
	private final BigDecimal localAccrual2;

	private final Date valuationDate; // need this to implement AccountingBean API

	@NonPersistentField
	private BigDecimal underlyingPrice;

	@NonPersistentField
	private final BigDecimal localEquityAccrual;

	@NonPersistentField
	private final BigDecimal localReceivableAccrual1; // for securities that support it:  unsettled equity accrual payments

	@NonPersistentField
	private final BigDecimal localReceivableAccrual2; // for securities that support it:  unsettled accrual 1 payments

	@NonPersistentField
	private final BigDecimal localEquityReceivableAccrual; // for securities that support it:  unsettled accrual 2 payments


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceValue(AccountingObjectInfo balance, ValuationData data, boolean pendingActivity) {
		this.clientInvestmentAccount = balance.getClientInvestmentAccount();
		this.holdingInvestmentAccount = balance.getHoldingInvestmentAccount();
		this.accountingAccount = balance.getAccountingAccount();
		this.investmentSecurity = balance.getInvestmentSecurity();

		this.quantity = data.getQuantity();
		this.price = data.getPrice();
		this.underlyingPrice = data.getUnderlyingPrice();
		this.exchangeRateToBase = data.getExchangeRateToBase();

		this.localNotional = data.getLocalNotional();
		this.localAccrual1 = data.getLocalAccrual1();
		this.localAccrual2 = data.getLocalAccrual2();
		this.localEquityAccrual = data.getLocalEquityAccrual();
		this.localReceivableAccrual1 = data.getLocalReceivableAccrual1();
		this.localReceivableAccrual2 = data.getLocalReceivableAccrual2();
		this.localEquityReceivableAccrual = data.getLocalEquityReceivableAccrual();
		this.localCostBasis = data.getLocalCostBasis();
		this.baseCostBasis = data.getBaseCostBasis();

		if (this.accountingAccount.isPosition()) {
			if (InvestmentUtils.isNoPaymentOnOpen(this.investmentSecurity)) {
				this.localCost = BigDecimal.ZERO;
				this.baseCost = BigDecimal.ZERO;
			}
			else {
				this.localCost = this.localCostBasis;
				this.baseCost = data.getBaseCost();
			}
		}
		else {
			this.localCost = this.localNotional;
			this.baseCost = data.getBaseCost();
		}

		this.valuationDate = data.getValuationDate();
		this.pendingActivity = pendingActivity;
	}


	/**
	 * Position Market Value = Notional - Cost Basis + Cost + Accrual
	 * Non-Position = Notional (Cost, Cost Basis, and Accrual are 0)
	 */
	public BigDecimal getLocalMarketValue() {
		if (this.getAccountingAccount().isPosition()) {
			return this.localNotional.subtract(this.localCostBasis).add(this.localCost).add(getLocalAccrual().add(getLocalEquityAccrual()));
		}
		return this.localNotional;
	}


	/**
	 * Market Value = Notional - Cost Basis + Cost + Accrual
	 */
	public BigDecimal getBaseMarketValue() {
		BigDecimal localMarketValue = getLocalMarketValue();
		return MathUtils.multiply(localMarketValue, this.exchangeRateToBase, localMarketValue.scale());
	}


	public BigDecimal getBaseNotional() {
		return MathUtils.multiply(this.localNotional, this.exchangeRateToBase, this.localNotional.scale());
	}


	public BigDecimal getBaseAccrual() {
		return MathUtils.multiply(getLocalAccrual(), this.exchangeRateToBase, this.localAccrual1.scale());
	}


	public BigDecimal getBaseAccrual1() {
		return MathUtils.multiply(this.localAccrual1, this.exchangeRateToBase, this.localAccrual1.scale());
	}


	public BigDecimal getBaseAccrual2() {
		return MathUtils.multiply(this.localAccrual2, this.exchangeRateToBase, this.localAccrual2.scale());
	}


	public BigDecimal getBaseEquityAccrual() {
		return MathUtils.multiply(this.localEquityAccrual, this.exchangeRateToBase, this.localAccrual2.scale());
	}


	public BigDecimal getBaseReceivableAccrual1() {
		return MathUtils.multiply(this.localReceivableAccrual1, this.exchangeRateToBase, this.localReceivableAccrual1.scale());
	}


	public BigDecimal getBaseReceivableAccrual2() {
		return MathUtils.multiply(this.localReceivableAccrual2, this.exchangeRateToBase, this.localReceivableAccrual2.scale());
	}


	public BigDecimal getBaseReceivableAccrual() {
		return MathUtils.multiply(getLocalReceivableAccrual(), this.exchangeRateToBase, this.localReceivableAccrual1.scale());
	}


	public BigDecimal getBaseEquityReceivableAccrual() {
		return MathUtils.multiply(getLocalEquityReceivableAccrual(), this.exchangeRateToBase, this.localEquityReceivableAccrual.scale());
	}


	public BigDecimal getBaseCost() {
		return this.baseCost;
	}


	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	@Override
	public Integer getSourceEntityId() {
		return null;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		if (getInvestmentSecurity() != null) {
			return getInvestmentSecurity().getInstrument().getTradingCurrency();
		}
		return null;
	}


	@Override
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	@Override
	public BigDecimal getQuantityNormalized() {
		if (getQuantity() == null && getInvestmentSecurity() != null && getInvestmentSecurity().isCurrency()) {
			return getLocalNotional();
		}
		return getQuantity();
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return getLocalNotional();
	}


	@Override
	public boolean isBuy() {
		return MathUtils.isPositive(getQuantityNormalized());
	}


	@Override
	public boolean isCollateral() {
		return getAccountingAccount().isCollateral();
	}


	@Override
	public Date getTransactionDate() {
		return this.valuationDate;
	}


	@Override
	public Date getSettlementDate() {
		return this.valuationDate;
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public BigDecimal getLocalCost() {
		return this.localCost;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public BigDecimal getLocalNotional() {
		return this.localNotional;
	}


	public BigDecimal getLocalAccrual() {
		return MathUtils.add(this.localAccrual1, this.localAccrual2);
	}


	public BigDecimal getLocalAccrual1() {
		return this.localAccrual1;
	}


	public BigDecimal getLocalAccrual2() {
		return this.localAccrual2;
	}


	public BigDecimal getLocalEquityAccrual() {
		return this.localEquityAccrual;
	}


	public BigDecimal getLocalReceivableAccrual1() {
		return this.localReceivableAccrual1;
	}


	public BigDecimal getLocalReceivableAccrual2() {
		return this.localReceivableAccrual2;
	}


	public BigDecimal getLocalReceivableAccrual() {
		return MathUtils.add(this.localReceivableAccrual1, this.localReceivableAccrual2);
	}


	public BigDecimal getLocalEquityReceivableAccrual() {
		return this.localEquityReceivableAccrual;
	}


	public boolean isPendingActivity() {
		return this.pendingActivity;
	}


	public BigDecimal getUnderlyingPrice() {
		return this.underlyingPrice;
	}


	public void setUnderlyingPrice(BigDecimal underlyingPrice) {
		this.underlyingPrice = underlyingPrice;
	}
}
