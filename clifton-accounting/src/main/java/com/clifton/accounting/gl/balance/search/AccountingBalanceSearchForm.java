package com.clifton.accounting.gl.balance.search;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;

import java.math.BigDecimal;
import java.util.Date;


@SearchForm(skippedPathsForValidation = "executingCompany")
public class AccountingBalanceSearchForm extends GeneralLedgerSearchForm implements AccountingAccountFilters {

	@SearchField(searchField = "investmentSecurity.instrument.hierarchy.investmentType.name")
	private String investmentTypeName;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name", searchFieldPath = "clientInvestmentAccount")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "issuingCompany.dtcNumber", searchFieldPath = "holdingInvestmentAccount", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean dtcParticipant;

	/**
	 * Specifies whether holding accounts of type "excludedAccount" should be included or not in the result. Set to false, to exclude them.  True, to include them only.  Null to
	 * have both.
	 */
	@SearchField(searchField = "excludedAccount", searchFieldPath = "holdingInvestmentAccount.type")
	private Boolean excludedHoldingAccounts;

	@SearchField(searchField = "position", searchFieldPath = "accountingAccount")
	private Boolean positionAccountingAccount;

	@SearchField(searchField = "cash", searchFieldPath = "accountingAccount")
	private Boolean cashAccountingAccount;

	@SearchField(searchField = "currency", searchFieldPath = "accountingAccount")
	private Boolean currencyAccountingAccount;

	@SearchField(searchField = "collateral", searchFieldPath = "accountingAccount")
	private Boolean collateralAccountingAccount;

	@SearchField(searchField = "receivable", searchFieldPath = "accountingAccount")
	private Boolean receivableAccountingAccount;

	@SearchField(searchField = "balanceSheetAccount", searchFieldPath = "accountingAccount.accountType")
	private Boolean balanceSheetAccount;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "investmentSecurity.instrument.investmentAccount")
	private Integer securityFundClientId;

	@SearchField(searchField = "accountingAccount.id", sortField = "accountingAccount.name")
	private Short accountingAccountId;

	@SearchField(searchField = "accountingAccount.id", sortField = "accountingAccount.name", comparisonConditions = ComparisonConditions.IN)
	private Short[] accountingAccountIds;

	@SearchField(searchField = "accountingAccount.id", sortField = "accountingAccount.name", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeAccountingAccountIds;

	// Custom Search Filter - Convert to Short[] for accountingAccountIds filter
	private AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName;

	@SearchField(searchField = "accountType.id", searchFieldPath = "accountingAccount")
	private Short accountingAccountTypeId;

	@SearchField(searchField = "accountType.id", searchFieldPath = "accountingAccount")
	private Short[] accountingAccountTypeIds;

	// custom filter: will be replaced with corresponding accountingAccountTypeId
	private String accountingAccountType;

	// custom filter: will be replaced with corresponding accountingAccountTypeIds
	private String[] accountingAccountTypes;

	@SearchField(searchField = "lastDeliveryDate", searchFieldPath = "investmentSecurity", comparisonConditions = {ComparisonConditions.EQUALS, ComparisonConditions.IS_NULL,
			ComparisonConditions.LESS_THAN, ComparisonConditions.GREATER_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date lastDeliveryDate;

	@SearchField(searchField = "deliverable", searchFieldPath = "investmentSecurity.instrument")
	private Boolean deliverable;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal localAmount;

	@SearchField
	private BigDecimal baseAmount;

	@SearchField
	private BigDecimal localCostBasis;

	// Custom search field used for range look ups (income statement
	private Date transactionStartDate;

	// Custom filter in java code to keep zero local balances. When local currency balance goes to 0,
	// there is often non-zero base balance that is equal to Currency Translation Gain/Loss.
	// It might be useful in rare cases for M2M. See ACCOUNTING-535
	private boolean includeZeroLocalBalances;

	/**
	 * For rare cases when Client and Holding account Base Currencies are different, specify which one to use. Most places use Client Account Base Currency (default). But there are
	 * times (reconciliation with external parties) when you may want to use Holding Account's.
	 */
	private boolean useHoldingAccountBaseCurrency;

	// flag to include underlying price
	private boolean includeUnderlyingPrice;

	// Flag to indicate only pending activity should be looked up without transactions from general ledger.
	// This allows pending activity to be looked up without having to duplicatepending activity retrieval in many places.
	private boolean pendingOnly;

	// Flag to indicate whether unsettled accrual and equity leg payments should be included.
	private boolean includeUnsettledLegPayments;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingBalanceSearchForm onTransactionDate(Date transactionDate) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setTransactionDate(transactionDate);
		return searchForm;
	}


	public static AccountingBalanceSearchForm onSettlementDate(Date settlementDate) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setSettlementDate(settlementDate);
		return searchForm;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getInvestmentTypeName() {
		return this.investmentTypeName;
	}


	public void setInvestmentTypeName(String investmentTypeName) {
		this.investmentTypeName = investmentTypeName;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public Boolean getDtcParticipant() {
		return this.dtcParticipant;
	}


	public void setDtcParticipant(Boolean dtcParticipant) {
		this.dtcParticipant = dtcParticipant;
	}


	@Override
	public Boolean getPositionAccountingAccount() {
		return this.positionAccountingAccount;
	}


	@Override
	public void setPositionAccountingAccount(Boolean positionAccountingAccount) {
		this.positionAccountingAccount = positionAccountingAccount;
	}


	@Override
	public Boolean getCashAccountingAccount() {
		return this.cashAccountingAccount;
	}


	@Override
	public void setCashAccountingAccount(Boolean cashAccountingAccount) {
		this.cashAccountingAccount = cashAccountingAccount;
	}


	public Boolean getCollateralAccountingAccount() {
		return this.collateralAccountingAccount;
	}


	@Override
	public Short[] getAccountingAccountIds() {
		return this.accountingAccountIds;
	}


	@Override
	public void setAccountingAccountIds(Short[] accountingAccountIds) {
		this.accountingAccountIds = accountingAccountIds;
	}


	@Override
	public AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountIdName() {
		return this.accountingAccountIdName;
	}


	@Override
	public void setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIdName) {
		this.accountingAccountIdName = accountingAccountIdName;
	}


	public void setCollateralAccountingAccount(Boolean collateralAccountingAccount) {
		this.collateralAccountingAccount = collateralAccountingAccount;
	}


	public Boolean getBalanceSheetAccount() {
		return this.balanceSheetAccount;
	}


	public void setBalanceSheetAccount(Boolean balanceSheetAccount) {
		this.balanceSheetAccount = balanceSheetAccount;
	}


	public Integer getSecurityFundClientId() {
		return this.securityFundClientId;
	}


	public void setSecurityFundClientId(Integer securityFundClientId) {
		this.securityFundClientId = securityFundClientId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	@Override
	public Short getAccountingAccountTypeId() {
		return this.accountingAccountTypeId;
	}


	@Override
	public void setAccountingAccountTypeId(Short accountingAccountTypeId) {
		this.accountingAccountTypeId = accountingAccountTypeId;
	}


	public Date getLastDeliveryDate() {
		return this.lastDeliveryDate;
	}


	public void setLastDeliveryDate(Date lastDeliveryDate) {
		this.lastDeliveryDate = lastDeliveryDate;
	}


	public Boolean getDeliverable() {
		return this.deliverable;
	}


	public void setDeliverable(Boolean deliverable) {
		this.deliverable = deliverable;
	}


	public Boolean getCurrencyAccountingAccount() {
		return this.currencyAccountingAccount;
	}


	public void setCurrencyAccountingAccount(Boolean currencyAccountingAccount) {
		this.currencyAccountingAccount = currencyAccountingAccount;
	}


	@Override
	public String getAccountingAccountType() {
		return this.accountingAccountType;
	}


	@Override
	public void setAccountingAccountType(String accountingAccountType) {
		this.accountingAccountType = accountingAccountType;
	}


	@Override
	public Short[] getAccountingAccountTypeIds() {
		return this.accountingAccountTypeIds;
	}


	@Override
	public void setAccountingAccountTypeIds(Short[] accountingAccountTypeIds) {
		this.accountingAccountTypeIds = accountingAccountTypeIds;
	}


	@Override
	public String[] getAccountingAccountTypes() {
		return this.accountingAccountTypes;
	}


	@Override
	public void setAccountingAccountTypes(String[] accountingAccountTypes) {
		this.accountingAccountTypes = accountingAccountTypes;
	}


	public Boolean getExcludedHoldingAccounts() {
		return this.excludedHoldingAccounts;
	}


	public void setExcludedHoldingAccounts(Boolean excludedHoldingAccounts) {
		this.excludedHoldingAccounts = excludedHoldingAccounts;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Boolean getReceivableAccountingAccount() {
		return this.receivableAccountingAccount;
	}


	public void setReceivableAccountingAccount(Boolean receivableAccountingAccount) {
		this.receivableAccountingAccount = receivableAccountingAccount;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getLocalAmount() {
		return this.localAmount;
	}


	public void setLocalAmount(BigDecimal localAmount) {
		this.localAmount = localAmount;
	}


	public BigDecimal getBaseAmount() {
		return this.baseAmount;
	}


	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public void setLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
	}


	public Date getTransactionStartDate() {
		return this.transactionStartDate;
	}


	public void setTransactionStartDate(Date transactionStartDate) {
		this.transactionStartDate = transactionStartDate;
	}


	public boolean isIncludeZeroLocalBalances() {
		return this.includeZeroLocalBalances;
	}


	public void setIncludeZeroLocalBalances(boolean includeZeroLocalBalances) {
		this.includeZeroLocalBalances = includeZeroLocalBalances;
	}


	public Short[] getExcludeAccountingAccountIds() {
		return this.excludeAccountingAccountIds;
	}


	public void setExcludeAccountingAccountIds(Short[] excludeAccountingAccountIds) {
		this.excludeAccountingAccountIds = excludeAccountingAccountIds;
	}


	public boolean isUseHoldingAccountBaseCurrency() {
		return this.useHoldingAccountBaseCurrency;
	}


	public void setUseHoldingAccountBaseCurrency(boolean useHoldingAccountBaseCurrency) {
		this.useHoldingAccountBaseCurrency = useHoldingAccountBaseCurrency;
	}


	public boolean isIncludeUnderlyingPrice() {
		return this.includeUnderlyingPrice;
	}


	public void setIncludeUnderlyingPrice(boolean includeUnderlyingPrice) {
		this.includeUnderlyingPrice = includeUnderlyingPrice;
	}


	public boolean isPendingOnly() {
		return this.pendingOnly;
	}


	public void setPendingOnly(boolean pendingOnly) {
		this.pendingOnly = pendingOnly;
	}


	public boolean isIncludeUnsettledLegPayments() {
		return this.includeUnsettledLegPayments;
	}


	public void setIncludeUnsettledLegPayments(boolean includeUnsettledLegPayments) {
		this.includeUnsettledLegPayments = includeUnsettledLegPayments;
	}
}

