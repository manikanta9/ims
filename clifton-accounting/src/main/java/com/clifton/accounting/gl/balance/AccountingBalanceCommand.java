package com.clifton.accounting.gl.balance;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;

import java.util.Date;


/**
 * @author mitchellf
 */
@SearchForm(hasOrmDtoClass = false)
public class AccountingBalanceCommand extends AccountingBalanceSearchForm {

	private Boolean excludeCashCollateral;

	private Date startDate;

	private Date endDate;

	private Boolean netOnly;

	private Long[] parentTransactionIds;

	private Boolean baseCurrency;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static AccountingBalanceCommand forClientAccountWithDateRange(Integer id, Date startDate, Date endDate) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setClientInvestmentAccountId(id);
		command.setStartDate(startDate);
		command.setEndDate(endDate);
		return command;
	}


	public static AccountingBalanceCommand forClientAccountAndHoldingAccount(Integer clientAccountId, Integer holdingAccountId) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setClientInvestmentAccountId(clientAccountId);
		command.setHoldingInvestmentAccountId(holdingAccountId);
		return command;
	}


	public static AccountingBalanceCommand forClientAccountOnTransactionDate(Integer clientAccountId, Date transactionDate) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setClientInvestmentAccountId(clientAccountId);
		command.setTransactionDate(transactionDate);
		return command;
	}


	public static AccountingBalanceCommand forClientAccountsOnTransactionDate(Integer[] ids, Date transactionDate) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setClientInvestmentAccountIds(ids);
		command.setTransactionDate(transactionDate);
		return command;
	}


	public static AccountingBalanceCommand forClientAccountGroupOnTransactionDate(Integer id, Date transactionDate) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setClientInvestmentAccountGroupId(id);
		command.setTransactionDate(transactionDate);
		return command;
	}


	public static AccountingBalanceCommand forParentTransactionIdsAndAccountingAccountId(Long[] parentTransactionIds, AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIds) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setParentTransactionIds(parentTransactionIds);
		command.setAccountingAccountIdName(accountingAccountIds);
		return command;
	}


	public static AccountingBalanceCommand forTransactionDate(Date date) {
		AccountingBalanceCommand command = new AccountingBalanceCommand();
		command.setTransactionDate(date);
		return command;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////                      Convenience 'with' setters                   ///////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceCommand withExcludeCashCollateral(Boolean excludeCashCollateral) {
		setExcludeCashCollateral(excludeCashCollateral);
		return this;
	}


	public AccountingBalanceCommand withDateRange(Date startDate, Date endDate) {
		setStartDate(startDate);
		setEndDate(endDate);
		return this;
	}


	public AccountingBalanceCommand withNetOnly(Boolean netOnly) {
		setNetOnly(netOnly);
		return this;
	}


	public AccountingBalanceCommand withParentTransactionIds(Long[] parentTransactionIds) {
		setParentTransactionIds(parentTransactionIds);
		return this;
	}


	public AccountingBalanceCommand withBaseCurrency(Boolean baseCurrency) {
		setBaseCurrency(baseCurrency);
		return this;
	}


	public AccountingBalanceCommand withInvestmentSecurityId(Integer id) {
		setInvestmentSecurityId(id);
		return this;
	}


	public AccountingBalanceCommand withClientAccountId(Integer id) {
		setClientInvestmentAccountId(id);
		return this;
	}


	public AccountingBalanceCommand withInvestmentGroupId(Short id) {
		setInvestmentGroupId(id);
		return this;
	}


	public AccountingBalanceCommand withHoldingAccountId(Integer id) {
		setHoldingInvestmentAccountId(id);
		return this;
	}


	public AccountingBalanceCommand withTransactionDate(Date transactionDate) {
		setTransactionDate(transactionDate);
		return this;
	}


	public AccountingBalanceCommand withSettlementDate(Date settlementDate) {
		setSettlementDate(settlementDate);
		return this;
	}


	public AccountingBalanceCommand withAccountingAccountId(Short id) {
		setAccountingAccountId(id);
		return this;
	}


	public AccountingBalanceCommand withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountIds) {
		setAccountingAccountIdName(accountingAccountIds);
		return this;
	}


	public AccountingBalanceCommand withClientAccountGroupId(Integer id) {
		setClientInvestmentAccountGroupId(id);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                      Getters and Setters                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getExcludeCashCollateral() {
		return this.excludeCashCollateral;
	}


	public void setExcludeCashCollateral(Boolean excludeCashCollateral) {
		this.excludeCashCollateral = excludeCashCollateral;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Boolean getNetOnly() {
		return this.netOnly;
	}


	public void setNetOnly(Boolean netOnly) {
		this.netOnly = netOnly;
	}


	public Long[] getParentTransactionIds() {
		return this.parentTransactionIds;
	}


	public void setParentTransactionIds(Long[] parentTransactionIds) {
		this.parentTransactionIds = parentTransactionIds;
	}


	public Boolean getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(Boolean baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
}
