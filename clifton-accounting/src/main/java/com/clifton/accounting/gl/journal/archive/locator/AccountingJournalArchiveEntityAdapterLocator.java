package com.clifton.accounting.gl.journal.archive.locator;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveEntityAdapter;


/**
 * <code>AccountingJournalArchiveEntityAdapterLocator</code> is an interface used to locate {@link AccountingJournalArchiveEntityAdapter}
 * objects that are registered for each accounting journal types.
 *
 * @author NickK
 */
public interface AccountingJournalArchiveEntityAdapterLocator {

	public AccountingJournalArchiveEntityAdapter<? extends BookableEntity> locate(String journalTypeName);
}
