package com.clifton.accounting.gl;


import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingTransaction</code> class represents a single GL entry.
 * Posting Date is set on corresponding accounting journal.
 *
 * @author vgomelsky
 */
public class AccountingTransaction extends AccountingJournalDetail {

	@Override
	public boolean isNewBean() {
		return getId() == null;
	}


	/**
	 * When a journal is Unposted from the General Ledger, all of its transactions are update to have deleted = true.
	 * This is done to preserve history of what was originally posted and can be used for research and analysis.
	 * Position lookup logic should filter out deleted transaction.  There's also additional validation that doesn't allow linking to deleted transactions.
	 */
	private boolean deleted;
	/**
	 * When an entity is re-posted (was posted before but then unposted), each transaction that is different from previously posted transaction
	 * will be labeled as modified = true. For example, if an entity is unposted and then immediately re-posted without any changes, then
	 * all transactions (except for id's) should be the same and modified will be set to false.  However, if something changes about the transaction
	 * (amount, fx rate, security, date, ...), then that transaction will be marked as modified.  This flag can be used to simplify research and analysis.
	 */
	private boolean modified;


	/**
	 * Returns Initial Margin Requirement for the security of this transaction.
	 * Based on whether the holding account is used for hedging or speculation, different margin requirement will be used.
	 * Also, if the holding account is setup to have initial margin multiplier that is not 1, will adjust margin requirements accordingly.
	 */
	public BigDecimal getInitialMarginPerUnit() {
		return InvestmentCalculatorUtils.calculateRequiredCollateral(getInvestmentSecurity(), getHoldingInvestmentAccount(), BigDecimal.ONE, BigDecimal.ONE, false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDeleted() {
		return this.deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public boolean isModified() {
		return this.modified;
	}


	public void setModified(boolean modified) {
		this.modified = modified;
	}
}
