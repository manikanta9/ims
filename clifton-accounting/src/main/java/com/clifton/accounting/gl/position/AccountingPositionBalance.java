package com.clifton.accounting.gl.position;

import com.clifton.accounting.AccountingUtils;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>AccountingPositionBalance</code> represents an aggregate of lots/positions for a particular Holding Account,
 * Client Account, GL Account, and Security. Balances can include pending activity, which can be merged or included
 * as separate balances depending on the pending activity criterion provided in a request.
 * <p>
 * Balances merged according to {@link AccountingObjectInfo} details, will always include the aggregated values for
 * quantity (unadjusted; opening face), remaining quantity, remaining base amount, remaining local cost basis, and base cost basis.
 * <p>
 * Unlike {@link AccountingPosition}, this aggregates lots and does not maintain the original opening transaction
 * <p>
 * Unlike {@link com.clifton.accounting.gl.balance.AccountingBalance}, this maintains the original unadjusted quantity (e.g. purchase face)
 *
 * @author nickk
 */
@NonPersistentObject
public class AccountingPositionBalance extends BaseSimpleEntity<String> implements AccountingObjectInfo {

	private AccountingAccount accountingAccount;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentSecurity investmentSecurity;

	/**
	 * Unadjusted quantity from opening of a lot. This will be purchase face of asset-backed bonds or notional of CDS swaps.
	 */
	private BigDecimal unadjustedQuantity;
	private BigDecimal remainingQuantity;
	private BigDecimal remainingCostBasis;
	private BigDecimal remainingBaseDebitCredit;

	/**
	 * Indicates whether this balance includes transaction data that has not yet been posted to the General Ledger.
	 */
	private boolean pendingActivity;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static AccountingPositionBalance of(AccountingPosition position) {
		AccountingPositionBalance balance = new AccountingPositionBalance();
		balance.setId(AccountingUtils.createAccountingObjectNaturalKey(position));
		balance.setClientInvestmentAccount(position.getClientInvestmentAccount());
		balance.setHoldingInvestmentAccount(position.getHoldingInvestmentAccount());
		balance.setInvestmentSecurity(position.getInvestmentSecurity());
		balance.setAccountingAccount(position.getAccountingAccount());

		balance.applyAccountingPositionAdjustment(position);

		return balance;
	}


	public AccountingPositionBalance applyAccountingPositionAdjustment(AccountingPosition position) {
		// Securities with factor change events have the original quantity adjusted upon booking. For these securities, track the
		// original quantity of the position to derive the unadjusted quantity/face. Other securities should use the remaining quantity.
		BigDecimal positionUnadjustedQuantity = position.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() == null ? position.getRemainingQuantity() : position.getQuantity();
		setUnadjustedQuantity(MathUtils.add(getUnadjustedQuantity(), positionUnadjustedQuantity));
		setRemainingQuantity(MathUtils.add(getRemainingQuantity(), position.getRemainingQuantity()));
		setRemainingCostBasis(MathUtils.add(getRemainingCostBasis(), position.getRemainingCostBasis()));
		setRemainingBaseDebitCredit(MathUtils.add(getRemainingBaseDebitCredit(), position.getRemainingBaseDebitCredit()));
		setPendingActivity(isPendingActivity() || position.isPendingActivity());
		return this;
	}


	@Override
	public String toString() {
		return "AccountingPositionBalance [holdingInvestmentAccount=" + getHoldingInvestmentAccount() + ", clientInvestmentAccount=" + getClientInvestmentAccount()
				+ ", accountingAccount=" + getAccountingAccount() + ", investmentSecurity=" + getInvestmentSecurity() + ", remainingQuantity=" + getRemainingQuantity()
				+ ", remainingBaseAmount=" + getRemainingBaseDebitCredit() + ", remainingCostBasis=" + getRemainingCostBasis() + "]";
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getUnadjustedQuantity() {
		return this.unadjustedQuantity;
	}


	public void setUnadjustedQuantity(BigDecimal unadjustedQuantity) {
		this.unadjustedQuantity = unadjustedQuantity;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getRemainingCostBasis() {
		return this.remainingCostBasis;
	}


	public void setRemainingCostBasis(BigDecimal remainingCostBasis) {
		this.remainingCostBasis = remainingCostBasis;
	}


	public BigDecimal getRemainingBaseDebitCredit() {
		return this.remainingBaseDebitCredit;
	}


	public void setRemainingBaseDebitCredit(BigDecimal remainingBaseDebitCredit) {
		this.remainingBaseDebitCredit = remainingBaseDebitCredit;
	}


	public boolean isPendingActivity() {
		return this.pendingActivity;
	}


	public void setPendingActivity(boolean pendingActivity) {
		this.pendingActivity = pendingActivity;
	}
}
