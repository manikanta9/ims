package com.clifton.accounting.gl.position.daily.jobs;

import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * @author BrianH
 */
public class AccountingPositionDailyAccountGroupRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private AccountingPositionDailyService accountingPositionDailyService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private Integer clientInvestmentAccountGroupId;

	private StatusHolderObject<Status> statusHolder;


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {

		// Don't rebuild today, since that will be handled by current build job.
		Date today = DateUtils.clearTime(new Date());
		Date startDate = DateUtils.getLastDayOfPreviousMonth(today);
		Date endDate = DateUtils.getPreviousWeekday(today);

		Status statusToReturn = this.statusHolder.getStatus();
		statusToReturn.setMessage("Rebuilding accounting positions for Investment Account Group [" + getClientInvestmentAccountGroupId() + "] between " + startDate + " and " + endDate);

		AccountingPositionDailyRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setEndSnapshotDate(endDate);

		command.setInvestmentAccountGroupId(clientInvestmentAccountGroupId);
		command.setStartSnapshotDate(startDate);
		command.setSynchronous(true);

		try {
			Status result = getAccountingPositionDailyService().rebuildAccountingPositionDaily(command);
			statusToReturn.mergeStatus(result);
			return statusToReturn;
		}
		catch (Exception e) {
			statusToReturn.setMessageWithErrors("Rebuild failed due to the following:\n" + e, null);
			return statusToReturn;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}
}
