package com.clifton.accounting.gl.pending;

import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;

import java.util.List;


/**
 * Implementations should return a List of pending AccountingJournalDetailDefinition for a specific journal type and search form restrictions.
 * <p>
 * NOTE:  All implementations in the context will be autowired into the accountingPendingActivityRetrieverList property of the AccountingPendingActivityHandler.
 *
 * @author mwacker
 */
public interface AccountingPendingActivityRetriever {

	/**
	 * Retrieves a list of AccountingJournalDetailDefinition objects for pending journal such as trade and position transfers.
	 * <p>
	 * NOTE:  pendingActivityRequest search form property specifies the type of retrieval that should be used
	 */
	public List<AccountingJournalDetailDefinition> getPendingTransactionList(GeneralLedgerSearchForm searchForm);
}
