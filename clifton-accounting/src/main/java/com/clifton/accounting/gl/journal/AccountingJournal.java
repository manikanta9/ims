package com.clifton.accounting.gl.journal;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingJournal</code> class represents an accounting journal DTO.
 * Each journal will have multiple detail items that have to pass various balancing rules
 * (sum of debits = sum of credits, etc.) in order to be posted into GL.
 * <p/>
 * The parent journal field is used to indicate that this journal is an adjusting journal for some other journal.
 *
 * @author vgomelsky
 */
public class AccountingJournal extends BaseEntity<Long> {

	private AccountingJournalType journalType;
	private AccountingJournalStatus journalStatus;
	/**
	 * Adjusting Journals, reference the journal that they adjust.
	 * Event Journal accrual reversal journals, reference the journals that they reverse.
	 * Unposted General Journals, reference unposted (Deleted) journal that they were created from.
	 */
	private AccountingJournal parent;

	/**
	 * The date/time when the journal was posted to the General Ledger.
	 * Null value indicates an unposted journal.
	 */
	private Date postingDate;

	private String description;

	// Used to tie subsystem journals back to source entity: redundant to the same field on journal type (can be removed assuming performance doesn't degrade)
	private SystemTable systemTable;
	@SoftLinkField(tableBeanPropertyName = "journalType.systemTable")
	private Integer fkFieldId;


	private List<AccountingJournalDetailDefinition> journalDetailList; // can't use the class level <T extends AccountingJournalDetailDefinition> because of complexity in dependencies

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isNewBean() {
		return (getId() == null);
	}


	@SuppressWarnings("unchecked")
	public boolean isSameTransactionSet(AccountingJournal journal) {
		List<AccountingJournalDetailDefinition> firstList = (List<AccountingJournalDetailDefinition>) getJournalDetailList();
		List<AccountingJournalDetailDefinition> secondList = CoreCollectionUtils.clone((List<AccountingJournalDetailDefinition>) journal.getJournalDetailList());

		if (CollectionUtils.getSize(firstList) != CollectionUtils.getSize(secondList)) {
			return false;
		}

		for (AccountingJournalDetailDefinition firstDetail : CollectionUtils.getIterable(firstList)) {
			boolean found = false;
			for (AccountingJournalDetailDefinition secondDetail : CollectionUtils.getIterable(secondList)) {
				if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(firstDetail, secondDetail, false,
						"id", "journal", "parentDefinition", "parentTransaction", "modified", "deleted", "description", "positionCommission"))) {
					found = true;
					secondList.remove(secondDetail);
					break;
				}
			}
			if (!found) {
				return false;
			}
		}

		return true;
	}


	public String toStringFormatted(boolean includeHeader) {
		return toStringFormatted(includeHeader, false);
	}


	/**
	 * Converts details of this journal to a formatted string with optional header line.
	 * The output can be easily copy/pasted into Excel.  if withAdditionalFields is set to true,
	 * the additional field values for isOpening() are included in the output.
	 */
	public String toStringFormatted(boolean includeHeader, boolean withAdditionalFields) {
		StringBuilder result = new StringBuilder(512);
		boolean firstRow = includeHeader;
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(this.journalDetailList)) {
			result.append(detail.toStringFormatted(firstRow, withAdditionalFields));
			result.append("\n");
			firstRow = false;
		}
		return result.toString();
	}


	@Override
	public String toString() {
		if (getId() == null) {
			return "{id = null; fkFieldId = " + this.fkFieldId + "; systemTable = " + this.systemTable + "}";
		}
		return getLabel();
	}


	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append("[id = ").append(getId()).append(']');
		if (getJournalStatus() != null) {
			result.append(' ').append(getJournalStatus().getName());
		}
		if (getDescription() != null) {
			result.append(" \"").append(getDescription()).append("\"");
		}
		return result.toString();
	}


	public String getLabelLong() {
		StringBuilder result = new StringBuilder(64);
		result.append("{id = ");
		result.append(getId());
		if (getJournalType() != null) {
			result.append("; ");
			result.append(getJournalType().getName());
		}
		if (getDescription() != null) {
			result.append(": ");
			result.append(getDescription());
		}
		result.append('}');
		return result.toString();
	}


	public <T extends AccountingJournalDetailDefinition> void addJournalDetail(T journalDetail) {
		if (this.journalDetailList == null) {
			this.journalDetailList = new ArrayList<>();
		}
		if (journalDetail.isNewBean()) {
			//get Lowest currentID
			long id = Long.MAX_VALUE;
			for (AccountingJournalDetailDefinition definition : this.journalDetailList) {
				Long identity = (Long) definition.getIdentity();
				if (identity < id) {
					id = identity;
				}
			}
			if (id == Long.MAX_VALUE) {
				id = -9;
			}
			journalDetail.setId(id - 1);
		}
		if (journalDetail.getJournal() == null) {
			journalDetail.setJournal(this);
		}
		this.journalDetailList.add(journalDetail);
	}


	public boolean removeJournalDetail(AccountingJournalDetailDefinition detail) {
		if (this.journalDetailList == null) {
			return false;
		}
		return this.journalDetailList.remove(detail);
	}


	public boolean removeJournalEntries(List<AccountingJournalDetailDefinition> entries) {
		if (this.journalDetailList == null) {
			return false;
		}
		return this.journalDetailList.removeAll(entries);
	}


	/**
	 * Returns a list of journal details that have the specified detail as their parentDefinition.
	 * Ignore records that have parentTransaction field populated - it supersedes parentDefinition.
	 */
	public List<AccountingJournalDetailDefinition> findChildEntries(AccountingJournalDetailDefinition detail) {
		List<AccountingJournalDetailDefinition> children = new ArrayList<>();
		for (AccountingJournalDetailDefinition definition : this.journalDetailList) {
			// transaction supersedes definition
			if (definition.getParentTransaction() == null && definition.getParentDefinition() != null && definition.getParentDefinition().getIdentity().equals(detail.getIdentity())) {
				children.add(definition);
			}
		}
		return children;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingJournalType journalType) {
		this.journalType = journalType;
	}


	public AccountingJournalStatus getJournalStatus() {
		return this.journalStatus;
	}


	public void setJournalStatus(AccountingJournalStatus journalStatus) {
		this.journalStatus = journalStatus;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Date getPostingDate() {
		return this.postingDate;
	}


	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}


	public List<? extends AccountingJournalDetailDefinition> getJournalDetailList() {
		return this.journalDetailList;
	}


	@SuppressWarnings("unchecked")
	public void setJournalDetailList(List<? extends AccountingJournalDetailDefinition> journalDetailList) {
		this.journalDetailList = (List<AccountingJournalDetailDefinition>) journalDetailList;
	}


	public AccountingJournal getParent() {
		return this.parent;
	}


	public void setParent(AccountingJournal parent) {
		this.parent = parent;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
