package com.clifton.accounting.gl.balance;


import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.accounting.gl.balance.search.AccountingAccountFilters;
import com.clifton.accounting.gl.balance.search.AccountingBalanceExtendedSearchForm;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.pending.AccountingPendingActivityHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.hibernate.type.DateType;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class AccountingBalanceServiceImpl implements AccountingBalanceService {

	private AdvancedReadOnlyDAO<AccountingBalance, Criteria> accountingBalanceDAO;
	private AdvancedReadOnlyDAO<AccountingBalanceExtended, Criteria> accountingBalanceExtendedDAO;

	private AccountingAccountIdsCache accountingAccountIdsCache;

	private AccountingAccountService accountingAccountService;
	private AccountingPendingActivityHandler accountingPendingActivityHandler;

	private DataTableRetrievalHandler dataTableRetrievalHandler;
	private SqlHandler sqlHandler;

	private InvestmentSetupService investmentSetupService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true, timeout = 90, noRollbackFor = ValidationException.class)
	public List<AccountingBalance> getAccountingBalanceList(final AccountingBalanceSearchForm searchForm) {
		// The query for retrieving AccountingBalance is defined in schema-hibernate-accounting-queries.xml
		ValidationUtils.assertMutuallyExclusive("Only Transaction Date or Settlement Date restriction can and must be specified.", searchForm.getTransactionDate(), searchForm.getSettlementDate());

		if (!StringUtils.isEmpty(searchForm.getInvestmentTypeName())) {
			searchForm.setInvestmentTypeId(getInvestmentSetupService().getInvestmentTypeByName(searchForm.getInvestmentTypeName()).getId());
			searchForm.setInvestmentTypeName(null);
		}

		configureAccountingAccountFilters(searchForm);

		List<AccountingBalance> balanceList;
		if (searchForm.isPendingOnly()) {
			balanceList = new ArrayList<>();
		}
		else {
			HibernateSearchFormConfigurer config = new AccountingBalanceSearchFormConfigurer(searchForm);
			balanceList = getAccountingBalanceDAO().findBySearchCriteria(config);
		}

		if (!searchForm.isIncludeZeroLocalBalances() && !CollectionUtils.isEmpty(balanceList)) {
			// remove zero balance entries (usually currency) while maintaining paging
			int i = 0;
			int count = balanceList.size();
			while (i < count) {
				AccountingBalance balance = balanceList.get(i);
				if (!balance.getAccountingAccount().isPosition() && MathUtils.isNullOrZero(balance.getLocalAmount()) && MathUtils.isNullOrZero(balance.getLocalCostBasis())) {
					balanceList.remove(i);
					i--;
					count--;
				}
				i++;
			}
		}

		// Include pending activity
		if (searchForm.getPendingActivityRequest() != null) {
			List<AccountingBalance> pendingList = getPendingAccountingBalanceList(searchForm);
			if (!CollectionUtils.isEmpty(pendingList)) {
				balanceList.addAll(pendingList);
				if (searchForm.getPendingActivityRequest().isMerge()) {
					balanceList = AccountingBalanceUtils.mergeAccountingBalanceList(balanceList, searchForm.getPendingActivityRequest().isIncludeLocalBalances());
				}
			}
		}

		// Include pending receivables
		if (searchForm.isIncludeUnsettledLegPayments()) {
			AccountingBalanceExtendedSearchForm extendedSearchForm = new AccountingBalanceExtendedSearchForm();
			BeanUtils.copyProperties(searchForm, extendedSearchForm, new String[]{"restrictionList"});
			extendedSearchForm.setChildAccountingAccountIds(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL));
			List<AccountingBalanceExtended> pendingList = getChildAccountingBalanceExtendedListForParentSecurity(extendedSearchForm);
			boolean includeAccrualReceivables = CollectionUtils.getStream(pendingList).map(ab -> ab.getInvestmentSecurity().getInstrument().getHierarchy()).anyMatch(InvestmentInstrumentHierarchy::isIncludeAccrualReceivables);
			if (!CollectionUtils.isEmpty(pendingList) && includeAccrualReceivables) {
				if (CollectionUtils.isEmpty(balanceList)) {
					balanceList = new PagingArrayList<>();
				}
				final Map<String, List<AccountingBalance>> balanceMap = CollectionUtils.getStream(balanceList).collect(Collectors.groupingBy(AccountingBalanceUtils::getBalanceKey));
				for (AccountingBalanceExtended balanceExtended : CollectionUtils.getIterable(pendingList)) {
					if (balanceExtended.getInvestmentSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
						if (!balanceMap.containsKey(AccountingBalanceUtils.getBalanceKey(balanceExtended))) {
							balanceList.add(balanceExtended);
						}
					}
				}
			}
		}

		return balanceList;
	}


	@Override
	public List<AccountingBalanceExtended> getChildAccountingBalanceExtendedListForParentSecurity(AccountingBalanceExtendedSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new AccountingBalanceSearchFormConfigurer(searchForm);
		return getAccountingBalanceExtendedDAO().findBySearchCriteria(config);
	}


	@Override
	@Transactional(readOnly = true, noRollbackFor = ValidationException.class) // faster PK look ups when there's only one transaction
	public List<InvestmentAccount> getAccountingBalanceClientAccountList(AccountingBalanceSearchForm searchForm) {
		List<AccountingBalance> accountingBalanceList = getAccountingBalanceList(searchForm);
		List<InvestmentAccount> investmentAccountList = CollectionUtils.getConverted(accountingBalanceList, AccountingBalance::getClientInvestmentAccount);
		if (accountingBalanceList instanceof PagingArrayList) {
			PagingArrayList<AccountingBalance> pagingAccountingBalanceList = (PagingArrayList<AccountingBalance>) accountingBalanceList;
			return new PagingArrayList<>(investmentAccountList, pagingAccountingBalanceList.getFirstElementIndex(), pagingAccountingBalanceList.getTotalElementCount(), pagingAccountingBalanceList.getPageSize());
		}
		return investmentAccountList;
	}


	/**
	 * Retrieves the pending balance list according to the conditions provided in the given command.
	 *
	 * @param searchForm the search form specifying the constraints for which pending activity shall be retrieved
	 * @return the list of generated pending balances
	 */
	private List<AccountingBalance> getPendingAccountingBalanceList(final AccountingBalanceSearchForm searchForm) {
		// Retrieve pending activity and convert to positions
		List<AccountingJournalDetailDefinition> pendingTransactionList = getAccountingPendingActivityHandler().getAccountingPendingTransactionList(searchForm);
		return AccountingBalanceUtils.convertAndGroupIntoBalanceList(pendingTransactionList, transaction -> true);
	}


	/**
	 * Note: This method doesn't specifically filter out Position Accounting accounts, but for these you'd either get a 0 or a meaningless number because position accounting
	 * accounts don't use Base or Local Debit Credit like the cash accounting accounts do.
	 */
	@Override
	public BigDecimal getAccountingAccountBalanceBase(AccountingAccountBalanceSearchForm searchForm) {
		return getAccountingAccountBalanceImpl(searchForm, true);
	}


	/**
	 * Returns a single balance resulting from the criteria specified in the provided {@link AccountingAccountBalanceSearchForm}.
	 * <p>
	 * If baseCurrency is true, the balances will be in the client's base currency, otherwise the balances will be in the transactions' local currency.
	 * <p>
	 * If more than one balance is expected based on search criteria use {@link #getAccountingAccountBalanceMapImpl(AccountingAccountBalanceSearchForm, boolean)} instead
	 * (e.g. more than one client or holding account is specified, or a client investment account group is specified).
	 */
	private BigDecimal getAccountingAccountBalanceImpl(AccountingAccountBalanceSearchForm searchForm, boolean baseCurrency) {
		if (searchForm.getClientAccountIds() != null || searchForm.getHoldingAccountIds() != null || searchForm.getClientInvestmentAccountGroupId() != null) {
			throw new ValidationException("Search restrictions for more than one Client Account or Holding Account, or a Client Account Group cannot be used to obtain a single balance value");
		}
		Map<Integer, AccountingClientAccountBalance> clientAccountBalanceMap = getAccountingAccountBalanceMapImpl(searchForm, baseCurrency);
		if (clientAccountBalanceMap.isEmpty()) {
			return BigDecimal.ZERO;
		}
		return clientAccountBalanceMap.values().stream()
				.map(AccountingClientAccountBalance::getClientAccountBalanceValue)
				.reduce(BigDecimal.ZERO, MathUtils::add);
	}


	/**
	 * Queries for a Accounting Account Balance for one more Client or Holding {@link com.clifton.investment.account.InvestmentAccount}.
	 * Returns a Map keyed by the client account with an {@link AccountingClientAccountBalance} containing an aggregated client account balance
	 * or a map of holding account balances.
	 * <p>
	 * If baseCurrency is true, the balances will be in the client's base currency, otherwise the balances will be in the transactions' local currency.
	 */
	private Map<Integer, AccountingClientAccountBalance> getAccountingAccountBalanceMapImpl(AccountingAccountBalanceSearchForm searchForm, boolean baseCurrency) {
		Integer[] clientAccountIds = ArrayUtils.add(searchForm.getClientAccountIds(), searchForm.getClientAccountId());
		int clientAccountIdSize = ArrayUtils.getLength(clientAccountIds);
		Integer[] holdingAccountIds = ArrayUtils.add(searchForm.getHoldingAccountIds(), searchForm.getHoldingAccountId());
		int holdingAccountIdSize = ArrayUtils.getLength(holdingAccountIds);
		if (clientAccountIdSize < 1 && holdingAccountIdSize < 1 && searchForm.getClientInvestmentAccountGroupId() == null) {
			throw new ValidationException("At least one Client Account, Holding Account, or both search restrictions must be specified.");
		}
		configureAccountingAccountFilters(searchForm);

		SqlSelectCommand sql = new SqlSelectCommand();
		StringBuilder selectClause = new StringBuilder(baseCurrency ? "SUM(t.BaseDebitCredit)" : "SUM(LocalDebitCredit)")
				.append(", t.ClientInvestmentAccountID");
		StringBuilder groupByClause = new StringBuilder("t.ClientInvestmentAccountID");
		if (holdingAccountIdSize > 1) {
			selectClause.append(", t. HoldingInvestmentAccountID");
			groupByClause.append(", t. HoldingInvestmentAccountID");
		}
		sql.setSelectClause(selectClause.toString());
		sql.setGroupByClause(groupByClause.toString());

		StringBuilder fromClause = new StringBuilder("AccountingTransaction t");
		StringBuilder whereClause = new StringBuilder(256);
		whereClause.append("t.IsDeleted = 0");

		// Default to Use Transaction Date = Today if No Settlement Date and No Transaction Date Explicitly Set
		boolean useTransactionDateToday = (searchForm.getSettlementDate() == null && searchForm.getTransactionDate() == null);
		if (searchForm.getSettlementDate() != null) {
			whereClause.append(" AND t.SettlementDate <= ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofDate(searchForm.getSettlementDate()));
		}
		if (searchForm.getStartSettlementDate() != null) {
			whereClause.append(" AND t.SettlementDate >= ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofDate(searchForm.getStartSettlementDate()));
		}
		if (useTransactionDateToday || searchForm.getTransactionDate() != null) {
			whereClause.append(" AND t.TransactionDate <= ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofDate(useTransactionDateToday ? DateUtils.clearTime(new Date()) : searchForm.getTransactionDate()));
		}
		if (searchForm.getStartTransactionDate() != null) {
			whereClause.append(" AND t.TransactionDate >= ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofDate(searchForm.getStartTransactionDate()));
		}

		if (clientAccountIdSize > 0) {
			if (clientAccountIdSize == 1) {
				whereClause.append(" AND t.ClientInvestmentAccountID = ? ");
				sql.addSqlParameterValue(SqlParameterValue.ofInteger(clientAccountIds[0]));
			}
			else {
				whereClause.append(" AND t.ClientInvestmentAccountID IN (").append(ArrayUtils.toString(clientAccountIds)).append(')');
			}
		}
		else if (searchForm.getClientInvestmentAccountGroupId() != null) {
			fromClause.append(" INNER JOIN InvestmentAccount ca ON t.ClientInvestmentAccountID = ca.InvestmentAccountID"
					+ " INNER JOIN InvestmentAccountGroupAccount cag ON ca.InvestmentAccountID = cag.InvestmentAccountID");
			whereClause.append(" AND cag.InvestmentAccountGroupID = ?");
			sql.addSqlParameterValue(SqlParameterValue.ofInteger(searchForm.getClientInvestmentAccountGroupId()));
		}

		if (holdingAccountIdSize > 0) {
			if (holdingAccountIdSize == 1) {
				whereClause.append(" AND t.HoldingInvestmentAccountID = ? ");
				sql.addSqlParameterValue(SqlParameterValue.ofInteger(holdingAccountIds[0]));
			}
			else {
				whereClause.append(" AND t.HoldingInvestmentAccountID IN (").append(ArrayUtils.toString(holdingAccountIds)).append(')');
			}
		}
		if (searchForm.getExcludedHoldingAccounts() != null) {
			fromClause.append(" INNER JOIN InvestmentAccount ha ON t.HoldingInvestmentAccountID = ha.InvestmentAccountID "
					+ "INNER JOIN InvestmentAccountType hat ON ha.InvestmentAccountTypeID = hat.InvestmentAccountTypeID");
			whereClause.append(" AND hat.IsExcludedAccount = ");
			whereClause.append(searchForm.getExcludedHoldingAccounts() ? '1' : '0');
		}

		if (searchForm.getAccountingAccountId() != null) {
			whereClause.append(" AND t.AccountingAccountID = ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getAccountingAccountId()));
		}
		else {
			if (searchForm.getAccountingAccountIds() != null) {
				whereClause.append(" AND t.AccountingAccountID IN (").append(ArrayUtils.toString(searchForm.getAccountingAccountIds())).append(") ");
			}

			boolean aaJoin = false;
			if (searchForm.getAccountingAccountTypeId() != null) {
				aaJoin = true;
				whereClause.append(" AND a.AccountingAccountTypeID = ? ");
				sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getAccountingAccountTypeId()));
			}
			if (searchForm.getAccountingAccountGroupId() != null) {
				whereClause.append(" AND t.AccountingAccountID IN (SELECT aga.AccountingAccountID FROM AccountingAccountGroupItemAccountingAccount aga INNER JOIN AccountingAccountGroupItem ag ON aga.AccountingAccountGroupItemID = ag.AccountingAccountGroupItemID WHERE ag.AccountingAccountGroupID = ? )");
				sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getAccountingAccountGroupId()));
			}
			if (searchForm.getCashAccountingAccount() != null) {
				aaJoin = true;
				whereClause.append(" AND a.IsCash = ");
				whereClause.append(searchForm.getCashAccountingAccount() ? '1' : '0');
			}
			if (searchForm.getPositionAccountingAccount() != null) {
				aaJoin = true;
				whereClause.append(" AND a.IsPosition = ");
				whereClause.append(searchForm.getPositionAccountingAccount() ? '1' : '0');
			}
			if (searchForm.getCollateralAccountingAccount() != null) {
				aaJoin = true;
				whereClause.append(" AND a.IsCollateral = ");
				whereClause.append(searchForm.getCollateralAccountingAccount() ? '1' : '0');
			}
			if (searchForm.getNotOurGLAccounts() != null) {
				aaJoin = true;
				whereClause.append(" AND a.notOurAccount = ");
				whereClause.append(searchForm.getNotOurGLAccounts() ? '1' : '0');
			}
			if (aaJoin) {
				fromClause.append(" INNER JOIN AccountingAccount a ON t.AccountingAccountID = a.AccountingAccountID ");
			}
		}

		if (searchForm.getSecurityId() != null) {
			whereClause.append(" AND t.InvestmentSecurityID = ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofInteger(searchForm.getSecurityId()));
		}

		else if (searchForm.getInstrumentId() != null || searchForm.getInvestmentHierarchyId() != null || searchForm.getInvestmentTypeId() != null || searchForm.getInvestmentGroupId() != null) {
			fromClause.append(" INNER JOIN InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID ");
			if (searchForm.getInstrumentId() != null) {
				whereClause.append(" AND s.InvestmentInstrumentID = ? ");
				sql.addSqlParameterValue(SqlParameterValue.ofInteger(searchForm.getInstrumentId()));
			}
			else {
				if (searchForm.getInvestmentHierarchyId() != null || searchForm.getInvestmentTypeId() != null) {
					fromClause.append(" INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID ");
					if (searchForm.getInvestmentHierarchyId() != null) {
						whereClause.append(" AND i.InvestmentInstrumentHierarchyID = ? ");
						sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getInvestmentHierarchyId()));
					}
					else {
						fromClause.append(" INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID ");
						whereClause.append(" AND h.InvestmentTypeID = ? ");
						sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getInvestmentTypeId()));
					}
				}

				if (searchForm.getInvestmentGroupId() != null) {
					whereClause.append(" AND s.InvestmentInstrumentID IN (SELECT ii.InvestmentInstrumentID FROM InvestmentGroupItemInstrument ii INNER JOIN InvestmentGroupItem gi ON ii.InvestmentGroupItemID = gi.InvestmentGroupItemID WHERE gi.InvestmentGroupID = ?)");
					sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getInvestmentGroupId()));
				}
			}
		}

		if (searchForm.getSimpleJournalTypeId() != null) {
			// Filter to AccountingSimpleJournal Table - Avoid Extra Joins
			SystemTable simpleJournalTable = getSystemSchemaService().getSystemTableByName("AccountingSimpleJournal");
			whereClause.append(" AND t.SystemTableID = ").append(simpleJournalTable.getId());

			fromClause.append(" INNER JOIN AccountingSimpleJournal asj ON t.FKFieldID = asj.AccountingSimpleJournalID ");
			whereClause.append(" AND asj.AccountingSimpleJournalTypeID = ? ");
			sql.addSqlParameterValue(SqlParameterValue.ofShort(searchForm.getSimpleJournalTypeId()));
		}

		sql.setFromClause(fromClause.toString());
		sql.setWhereClause(whereClause.toString());

		ResultSetExtractor<Map<Integer, AccountingClientAccountBalance>> resultSetExtractor;
		if (holdingAccountIdSize > 1) {
			resultSetExtractor = resultSet -> {
				Map<Integer, AccountingClientAccountBalance> clientAccountHoldingAccountBalanceMap = new HashMap<>();
				while (resultSet.next()) {
					BigDecimal balance = resultSet.getBigDecimal(1);
					Integer clientAccountId = resultSet.getInt(2);
					Integer holdingAccountId = resultSet.getInt(3);
					clientAccountHoldingAccountBalanceMap.computeIfAbsent(clientAccountId, key -> AccountingClientAccountBalance.ofClientAccountWithHoldingAccountBalances(clientAccountId))
							.addHoldingAccountBalance(holdingAccountId, balance);
				}
				return clientAccountHoldingAccountBalanceMap;
			};
		}
		else {
			resultSetExtractor = resultSet -> {
				Map<Integer, AccountingClientAccountBalance> clientAccountHoldingAccountBalanceMap = new HashMap<>();
				while (resultSet.next()) {
					BigDecimal balance = resultSet.getBigDecimal(1);
					Integer clientAccountId = resultSet.getInt(2);
					clientAccountHoldingAccountBalanceMap.put(clientAccountId, AccountingClientAccountBalance.ofClientAccountWithAggregateBalance(clientAccountId, balance));
				}
				return clientAccountHoldingAccountBalanceMap;
			};
		}
		return getSqlHandler().executeSelect(sql, resultSetExtractor);
	}


	@Override
	public BigDecimal getAccountingBalanceForSecurityLocal(AccountingBalanceCommand command) {

		// Validation
		ValidationUtils.assertNotNull(command.getInvestmentSecurityId(), "Must provide an Investment Security ID");
		ValidationUtils.assertNotNull(command.getClientInvestmentAccountId(), "Must provide a Client Account ID");
		ValidationUtils.assertNotNull(command.getHoldingInvestmentAccountId(), "Must provide a Holding Account ID");
		ValidationUtils.assertNotNull(command.getTransactionDate(), "Must provide a Transaction Date");

		AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
		searchForm.setSecurityId(command.getInvestmentSecurityId());
		searchForm.setClientAccountId(command.getClientInvestmentAccountId());
		searchForm.setHoldingAccountId(command.getHoldingInvestmentAccountId());
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		searchForm.setTransactionDate(command.getTransactionDate());
		return getAccountingAccountBalanceImpl(searchForm, false);
	}


	@Override
	public Map<Integer, BigDecimal> getAccountingCashBalanceMap(AccountingBalanceCommand command) {

		AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
		searchForm.setClientInvestmentAccountGroupId(command.getClientInvestmentAccountGroupId());
		searchForm.setClientAccountIds(command.getClientInvestmentAccountIds());
		searchForm.setAccountingAccountIdName(command.getAccountingAccountIdName());
		searchForm.setTransactionDate(command.getTransactionDate());
		Map<Integer, AccountingClientAccountBalance> clientAccountBalanceMap = getAccountingAccountBalanceMapImpl(searchForm, true);
		return clientAccountBalanceMap.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getClientAccountBalanceValue()));
	}


	@Override
	public Map<Long, BigDecimal> getAccountingChildBalanceMap(AccountingBalanceCommand command) {
		if (ArrayUtils.isEmpty(command.getParentTransactionIds())) {
			return Collections.emptyMap();
		}

		String parentTransactionsString = ArrayUtils.toString(command.getParentTransactionIds());

		// Validate
		ValidationUtils.assertNotNull(command.getAccountingAccountIdName(), "Must provide Accounting Account Id Name");

		Short[] accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccounts(command.getAccountingAccountIdName());
		String accountingAccountIdsString = ArrayUtils.toString(accountingAccountIds);

		String sql = "SELECT SUM(BaseDebitCredit), ParentTransactionID FROM AccountingTransaction"
				+ " WHERE ParentTransactionID IN (" + parentTransactionsString + ") AND AccountingAccountID IN (" + accountingAccountIdsString + ")"
				+ " AND IsDeleted = 0 GROUP BY ParentTransactionID";

		ResultSetExtractor<Map<Long, BigDecimal>> resultSetExtractor = generateResultSetExtractor();
		return getSqlHandler().executeSelect(sql, resultSetExtractor);
	}


	@Override
	public Map<Date, BigDecimal> getAccountingCashBalanceMapIncludeReceivables(AccountingBalanceCommand command) {
		// Include All Cash And Non Position Receivables - Optionally Exclude Cash Collateral
		Short[] accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccounts((command.getExcludeCashCollateral() ? AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL : AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS));
		// And Receivables (Non Position - Optionally Exclude Collateral)
		accountingAccountIds = ArrayUtils.addAll(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccounts((command.getExcludeCashCollateral() ? AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL : AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION)));
		String sql = "SELECT cd.EndDate \"Date\", SUM(CASE WHEN TransactionDate <= cd.EndDate THEN BaseDebitCredit ELSE 0 END) \"Balance\" FROM AccountingTransaction t " //
				+ "INNER JOIN InvestmentAccount ha ON t.HoldingInvestmentAccountID = ha.InvestmentAccountID "
				+ "INNER JOIN InvestmentAccountType at ON ha.InvestmentAccountTypeID = at.InvestmentAccountTypeID AND at.IsExcludedAccount = 0 "
				+ "INNER JOIN CalendarDay cd ON cd.EndDate >= ? AND cd.EndDate <= ? "
				+ "WHERE AccountingAccountID IN (" + ArrayUtils.toString(accountingAccountIds) + ")"
				+ " AND t.IsDeleted = 0 "
				+ " AND TransactionDate <= ? " //
				+ " AND ClientInvestmentAccountID = " + command.getClientInvestmentAccountId() //
				+ " GROUP BY cd.EndDate ";

		return generateBalanceMapFromSql(new SqlSelectCommand(sql), command);
	}


	@Override
	public Map<Date, BigDecimal> getAccountingNonPositionAssetsAndLiabilities(AccountingBalanceCommand command) {
		// Include All Cash And Non Position Assets and Liabilities
		Short[] accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.NON_POSITION_ACCOUNTS);

		String sql = "SELECT x.Date " //
				+ ", SUM(CASE WHEN x.Balance = 0 OR x.TradingCurrencyID = x.BaseCurrencyID THEN x.Balance ELSE x.Balance * COALESCE(MarketData.GetExchangeRate(x.TradingCurrencyID, x.BaseCurrencyID, x.HoldingInvestmentAccountID, x.Date),1) END) \"Balance\" " //
				+ " FROM ( "  //
				+ " SELECT cd.EndDate \"Date\" " //
				+ " , t.HoldingInvestmentAccountID " //
				+ " , i.TradingCurrencyID " //
				+ " , ca.BaseCurrencyID " //
				+ " , SUM(CASE WHEN TransactionDate <= cd.EndDate THEN t.LocalDebitCredit ELSE 0 END) \"Balance\" " //
				+ " FROM AccountingTransaction t " //
				+ " INNER JOIN InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID " //
				+ " INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID " //
				+ " INNER JOIN AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID " //
				+ " INNER JOIN InvestmentAccount ca ON t.ClientInvestmentAccountID = ca.InvestmentAccountID " //
				+ " INNER JOIN InvestmentAccount ha ON t.HoldingInvestmentAccountID = ha.InvestmentAccountID " //
				+ " INNER JOIN InvestmentAccountType at ON ha.InvestmentAccountTypeID = at.InvestmentAccountTypeID AND at.IsExcludedAccount = 0 " //
				+ " INNER JOIN CalendarDay cd ON cd.EndDate >= ? AND cd.EndDate <= ? " //
				+ " WHERE t.AccountingAccountID IN (" + ArrayUtils.toString(accountingAccountIds) + ") " //
				+ " AND t.IsDeleted = 0  AND TransactionDate <= ?  " //
				+ " AND aa.AccountingAccountTypeID IN (" + getAccountingAccountService().getAccountingAccountTypeByName(AccountingAccountType.ASSET).getId() + ", " + getAccountingAccountService().getAccountingAccountTypeByName(AccountingAccountType.LIABILITY).getId() + ") " //
				+ " AND ClientInvestmentAccountID = " + command.getClientInvestmentAccountId() //
				+ " GROUP BY cd.EndDate, i.TradingCurrencyID, ca.BaseCurrencyID, t.HoldingInvestmentAccountID  " //
				+ " ) x  " //
				+ " GROUP BY x.Date  " //
				+ " ORDER BY x.Date ";

		return generateBalanceMapFromSql(new SqlSelectCommand(sql), command);
	}


	private Map<Date, BigDecimal> generateBalanceMapFromSql(SqlSelectCommand sql, AccountingBalanceCommand command) {

		SqlParameterValue[] params = new SqlParameterValue[]{SqlParameterValue.ofDate(command.getStartDate()), SqlParameterValue.ofDate(command.getEndDate()), SqlParameterValue.ofDate(command.getEndDate())};
		sql.setSqlParameterValues(params);
		DataTable result = getDataTableRetrievalHandler().findDataTable(sql);

		if (result != null && result.getTotalRowCount() > 0) {
			Map<Date, BigDecimal> balanceMap = new HashMap<>();
			for (int i = 0; i < result.getTotalRowCount(); i++) {
				DataRow row = result.getRow(i);
				balanceMap.put((Date) row.getValue(0), (BigDecimal) row.getValue(1));
			}
			return balanceMap;
		}
		return null;
	}


	private ResultSetExtractor<Map<Long, BigDecimal>> generateResultSetExtractor() {
		return resultSet -> {
			Map<Long, BigDecimal> transactionCommissionBalanceMap = new HashMap<>();
			while (resultSet.next()) {
				BigDecimal balance = resultSet.getBigDecimal(1);
				Long parentTransactionId = resultSet.getLong(2);
				transactionCommissionBalanceMap.put(parentTransactionId, balance);
			}
			return transactionCommissionBalanceMap;
		};
	}


	@Override
	public BigDecimal getAccountingBalance(AccountingBalanceCommand command) {
		ValidationUtils.assertFalse(command.getClientInvestmentAccountId() == null && command.getHoldingInvestmentAccountId() == null, "You must provide at least a client account and/or broker account");

		AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
		searchForm.setClientAccountId(command.getClientInvestmentAccountId());

		if (command.getHoldingInvestmentAccountId() == null) {
			searchForm.setExcludedHoldingAccounts(false);
		}
		else {
			searchForm.setHoldingAccountId(command.getHoldingInvestmentAccountId());
		}

		if (command.getAccountingAccountIdName() != null) {
			searchForm.setAccountingAccountIdName(command.getAccountingAccountIdName());
		}
		else {
			searchForm.setAccountingAccountIdName(BooleanUtils.isTrue(command.getExcludeCashCollateral()) ? AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL : AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS);
		}

		if (BooleanUtils.isTrue(command.getNetOnly()) || command.getTransactionDate() != null) {
			searchForm.setTransactionDate(command.getTransactionDate());
		}
		else {
			searchForm.setSettlementDate(command.getSettlementDate());
		}

		return getAccountingAccountBalanceImpl(searchForm, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final class AccountingBalanceSearchFormConfigurer extends HibernateSearchFormConfigurer {

		private final AccountingBalanceSearchForm accountingBalanceSearchForm;


		public AccountingBalanceSearchFormConfigurer(AccountingBalanceSearchForm accountingBalanceSearchForm) {
			super(accountingBalanceSearchForm);
			this.accountingBalanceSearchForm = accountingBalanceSearchForm;
		}


		@Override
		public void configureCriteria(Criteria criteria) {
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getTransactionStartDate()));
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getTransactionStartDate()));
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getTransactionDate()));
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getTransactionDate()));
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getSettlementDate()));
			criteria.add(new SubselectParameterExpression(new DateType(), getAccountingBalanceSearchForm().getSettlementDate()));

			super.configureCriteria(criteria);
		}


		public AccountingBalanceSearchForm getAccountingBalanceSearchForm() {
			return this.accountingBalanceSearchForm;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingBalance> getAccountingBalanceSheetList(AccountingBalanceSearchForm searchForm) {
		searchForm.setBalanceSheetAccount(true);

		return getAccountingBalanceList(searchForm);
	}


	@Override
	public List<AccountingBalance> getAccountingAssetList(AccountingBalanceSearchForm searchForm) {
		Short assetTypeId = getAccountingAccountService().getAccountingAccountTypeByName(AccountingAccountType.ASSET).getId();
		searchForm.setAccountingAccountTypeId(assetTypeId);

		return getAccountingBalanceList(searchForm);
	}


	@Override
	public List<AccountingBalance> getAccountingIncomeStatementList(AccountingBalanceSearchForm searchForm) {
		if (searchForm.getTransactionStartDate() == null && searchForm.getTransactionDate() == null) {
			throw new ValidationException("Transaction Start Date and Transaction Date must be specified.");
		}

		searchForm.setBalanceSheetAccount(false);

		List<AccountingBalance> results = getAccountingBalanceList(searchForm);

		//Flip the signs of the amounts for all the results
		for (AccountingBalance result : CollectionUtils.getIterable(results)) {
			result.setLocalAmount(result.getLocalAmount().negate());
			result.setBaseAmount(result.getBaseAmount().negate());
			result.setLocalCostBasis(null);
		}

		return results;
	}


	@Override
	public Integer getAccountingAccountBalanceDaysActive(AccountingBalanceCommand command) {
		String sql = "SELECT COUNT(*) " + //
				" FROM ( " + //
				" SELECT DISTINCT cd.StartDate " + //, t.InvestmentSecurityID, SUM(t.Quantity) "Quantity", SUM(t.LocalDebitCredit) "DebitCredit""
				" FROM CalendarDay cd " + //
				" INNER JOIN AccountingTransaction t on  t.ClientInvestmentAccountID = ? AND t.TransactionDate <= cd.StartDate AND t.AccountingAccountID = ? ";
		if (command.getInvestmentGroupId() != null) {
			sql += " INNER JOIN InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID " + //
					" INNER JOIN InvestmentGroupItemInstrument gii on s.InvestmentInstrumentID = gii.InvestmentInstrumentID " + //
					" INNER JOIN InvestmentGroupItem gi ON gii.InvestmentGroupItemID = gi.InvestmentGroupItemID AND gi.InvestmentGroupID = ? ";
		}
		sql += " WHERE cd.StartDate >= ? AND cd.StartDate <= ? AND t.IsDeleted = 0 " + //
				" GROUP BY cd.StartDate, t.InvestmentSecurityID " + //
				"HAVING SUM(t.Quantity) <> 0 OR SUM(t.LocalDebitCredit) <> 0) tr ";
		Integer days = 0;
		DataTable result = getDataTableRetrievalHandler().findDataTable(
				new SqlSelectCommand(sql, (command.getInvestmentGroupId() == null ? new SqlParameterValue[]{SqlParameterValue.ofInteger(command.getClientInvestmentAccountId()), SqlParameterValue.ofShort(command.getAccountingAccountId()), SqlParameterValue.ofDate(command.getStartDate()), SqlParameterValue.ofDate(command.getEndDate())} : new SqlParameterValue[]{SqlParameterValue.ofInteger(command.getClientInvestmentAccountId()), SqlParameterValue.ofShort(command.getAccountingAccountId()),
						SqlParameterValue.ofShort(command.getInvestmentGroupId()), SqlParameterValue.ofDate(command.getStartDate()), SqlParameterValue.ofDate(command.getEndDate())})));
		if (result != null && result.getTotalRowCount() > 0) {
			days = (Integer) result.getRow(0).getValue(0);
			if (days == null) {
				days = 0;
			}
		}
		return days;
	}


	private void configureAccountingAccountFilters(AccountingAccountFilters searchForm) {
		// optimize filtering by AccountingAccount to eliminate extra joins: enables use of fully covered index
		Short[] accountingAccountIds = null;
		if (searchForm.getAccountingAccountTypeId() != null) {
			// gets remapped to GL Account IDs
			AccountingAccountType accountType = getAccountingAccountService().getAccountingAccountType(searchForm.getAccountingAccountTypeId());
			ValidationUtils.assertNotNull(accountType, "Invalid Accounting Account Type with id = " + searchForm.getAccountingAccountTypeId());
			searchForm.setAccountingAccountType(accountType.getName());
			searchForm.setAccountingAccountTypeId(null);
		}
		if (searchForm.getAccountingAccountType() != null && searchForm.getAccountingAccountTypeId() == null) {
			accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccountsByAccountType(searchForm.getAccountingAccountType());
		}
		if (searchForm.getAccountingAccountTypes() != null && searchForm.getAccountingAccountTypeIds() == null) {
			for (String type : searchForm.getAccountingAccountTypes()) {
				// don't add if already have in the list
				if (searchForm.getAccountingAccountType() == null || !searchForm.getAccountingAccountType().equals(type)) {
					accountingAccountIds = ArrayUtils.addAll(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccountsByAccountType(type));
				}
			}
		}
		if (searchForm.getAccountingAccountIdName() != null) {
			accountingAccountIds = ArrayUtils.intersect(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccounts(searchForm.getAccountingAccountIdName()));
			searchForm.setAccountingAccountIdName(null);
		}
		if (BooleanUtils.isTrue(searchForm.getPositionAccountingAccount())) {
			accountingAccountIds = ArrayUtils.intersect(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS));
			searchForm.setPositionAccountingAccount(null);
		}
		if (BooleanUtils.isTrue(searchForm.getCashAccountingAccount())) {
			accountingAccountIds = ArrayUtils.intersect(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS));
			searchForm.setCashAccountingAccount(null);
		}

		searchForm.setAccountingAccountIds(ArrayUtils.intersect(accountingAccountIds, searchForm.getAccountingAccountIds()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<AccountingBalance, Criteria> getAccountingBalanceDAO() {
		return this.accountingBalanceDAO;
	}


	public void setAccountingBalanceDAO(AdvancedReadOnlyDAO<AccountingBalance, Criteria> accountingBalanceDAO) {
		this.accountingBalanceDAO = accountingBalanceDAO;
	}


	public AdvancedReadOnlyDAO<AccountingBalanceExtended, Criteria> getAccountingBalanceExtendedDAO() {
		return this.accountingBalanceExtendedDAO;
	}


	public void setAccountingBalanceExtendedDAO(AdvancedReadOnlyDAO<AccountingBalanceExtended, Criteria> accountingBalanceExtendedDAO) {
		this.accountingBalanceExtendedDAO = accountingBalanceExtendedDAO;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public AccountingPendingActivityHandler getAccountingPendingActivityHandler() {
		return this.accountingPendingActivityHandler;
	}


	public void setAccountingPendingActivityHandler(AccountingPendingActivityHandler accountingPendingActivityHandler) {
		this.accountingPendingActivityHandler = accountingPendingActivityHandler;
	}
}
