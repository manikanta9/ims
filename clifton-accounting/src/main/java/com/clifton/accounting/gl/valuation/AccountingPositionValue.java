package com.clifton.accounting.gl.valuation;

import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;


/**
 * The <code>AccountingPositionValue</code> class represents a lot that a client has a position for.
 * The primary key "accountingTransactionId" points to the opening position/lot/AccountingTransaction that may be fully open or partially closed.
 * <p>
 * It is built from corresponding {@link AccountingPosition} objects but also adds to it current market data (price, exchange rate, accruals)
 * in order to be able to calculate current market value.
 * <p>
 * When possible, use {@link AccountingPosition} object because it's more light weight and is faster to populate.
 *
 * @author vgomelsky
 */
public class AccountingPositionValue extends AccountingBalanceValue {

	private final long accountingTransactionId; // the ID of the last opening lot (e.g. opening lot or last opening following a security event)
	private final AccountingJournalDetailDefinition openingTransaction; // points to the ultimate opening lot


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionValue(AccountingPosition position, ValuationData data) {
		super(position, data, position.isPendingActivity());
		this.accountingTransactionId = position.getId();
		this.openingTransaction = getUltimateParentTransaction(position.getOpeningTransaction());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Look for the ultimate parent transaction opening lot. Securities that are affected by factor events may have transactions for
	 * the factor events that adjust the cost basis and price. The ultimate parent transaction will show the original price and values.
	 */
	private AccountingJournalDetailDefinition getUltimateParentTransaction(AccountingJournalDetailDefinition accountingTransaction) {
		AccountingJournalDetailDefinition ultimateParent = accountingTransaction;
		while (ultimateParent.getParentTransaction() != null) {
			ultimateParent = ultimateParent.getParentTransaction();
		}
		return ultimateParent;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public AccountingTransactionInfo getOpeningTransaction() {
		return this.openingTransaction;
	}
}
