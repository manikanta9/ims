package com.clifton.accounting.gl.journal.archive;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.archive.descriptor.adapter.ArchiveEntityDescriptorAdapter;


/**
 * <code>AccountingJournalArchiveEntityAdapter</code> assists in converting an {@link AccountingJournalArchiveEntity}
 * into an {@link ArchiveEntityDescriptor} based on the booked {@link BookableEntity}
 * being archived.
 *
 * @author NickK
 */
public interface AccountingJournalArchiveEntityAdapter<T extends BookableEntity> extends ArchiveEntityDescriptorAdapter<AccountingJournalArchiveEntity<T>> {

	public static final String DEFAULT_ACCOUNTING_JOURNAL_ARCHIVE_ADAPTER_KEY = "Default Journal Archive Adapter";


	/**
	 * Returns the name of the {@link com.clifton.accounting.gl.journal.AccountingJournalType}
	 * the adapter is able to handle. This parameter is used for registering the adapters in
	 * the system so they can be looked up by journal type name.
	 */
	public String getJournalTypeName();
}
