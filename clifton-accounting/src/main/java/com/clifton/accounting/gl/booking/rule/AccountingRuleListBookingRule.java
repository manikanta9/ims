package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>AccountingRuleListBookingRule</code> applies a list of booking rules.
 *
 * @author mwacker
 */
public class AccountingRuleListBookingRule<T extends BookableEntity> implements AccountingBookingRule<T> {

	private List<AccountingBookingRule<T>> bookingRulesList;


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		for (AccountingBookingRule<T> bookingRule : CollectionUtils.getIterable(getBookingRulesList())) {
			bookingRule.applyRule(bookingSession);
			LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> {
				StringBuilder detailsString = new StringBuilder();
				detailsString.append(bookingRule.toString()).append("\r\r");
				boolean firstDefinition = true;
				for (AccountingJournalDetailDefinition entry : CollectionUtils.getIterable(journal.getJournalDetailList())) {
					detailsString.append(entry.toStringFormatted(firstDefinition));
					detailsString.append('\r');
					firstDefinition = false;
				}
				return detailsString.toString();
			}));
		}
	}


	public List<AccountingBookingRule<T>> getBookingRulesList() {
		return this.bookingRulesList;
	}


	public void setBookingRulesList(List<AccountingBookingRule<T>> bookingRulesList) {
		this.bookingRulesList = bookingRulesList;
	}
}
