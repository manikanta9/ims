package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.core.util.AssertUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.bean.SystemBeanType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link SystemBeanType} implementation of {@link AccountingPositionValueCalculator} which performs notional calculations for {@link AccountingPosition} entities.
 * <p>
 * This calculator produces a notional value for a position based on its configuration. For example, a calculator configured for the {@link NotionalPriceFields#STRIKE} {@link
 * #notionalPriceField} will produce the strike notional value for any positions passed into its {@link #calculate(AccountingPosition, Date)} method.
 *
 * @author MikeH
 */
public class AccountingPositionNotionalValueCalculator implements AccountingPositionValueCalculator {

	private AccountingPositionHandler accountingPositionHandler;
	private InvestmentCalculator investmentCalculator;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	/**
	 * The field that shall be used to produce the notional value for the given position.
	 */
	private NotionalPriceFields notionalPriceField;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final BigDecimal calculate(AccountingPosition position, Date snapshotDate) {
		// Calculate base notional value
		InvestmentSecurity security = position.getInvestmentSecurity();
		BigDecimal price = getPrice(position, snapshotDate);
		BigDecimal localNotionalValue = getInvestmentCalculator().calculateNotional(security, price, position.getRemainingQuantity(), snapshotDate);

		// Convert from local (transactional) currency to base (client) currency
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(position);
		String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(position);
		BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(position.fxSourceCompany(), !position.fxSourceCompanyOverridden(), fromCurrency, toCurrency, snapshotDate).flexibleLookup());
		return InvestmentCalculatorUtils.calculateBaseAmount(localNotionalValue, fxRate, position.getClientInvestmentAccount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the price to use on the specified date when calculating the notional value for a position.
	 *
	 * @param position the position for which to retrieve the notional price to use
	 * @param date     the date for which to retrieve the price
	 * @return the notional price
	 */
	private BigDecimal getPrice(AccountingPosition position, Date date) {
		switch (getNotionalPriceField()) {
			case STANDARD:
				return getStandardNotionalPrice(position, date);
			case UNDERLYING:
				return getUnderlyingNotionalPrice(position, date);
			case STRIKE:
				return getStrikeNotionalPrice(position);
			default:
				throw new AssertionError(String.format("No handler is specified for behavior of type [%s].", getNotionalPriceField()));
		}
	}


	/**
	 * Gets the notional price for the given position when the {@link NotionalPriceFields#STANDARD} notional type is used.
	 *
	 * @param position the position for which to retrieve the notional price to use
	 * @param date     the date for which to retrieve the price
	 * @return the standard notional price
	 */
	private BigDecimal getStandardNotionalPrice(AccountingPosition position, Date date) {
		return getAccountingPositionHandler().getAccountingPositionPrice(position, date);
	}


	/**
	 * Gets the notional price for the given position when the {@link NotionalPriceFields#UNDERLYING} notional type is used.
	 *
	 * @param position the position for which to retrieve the notional price to use
	 * @param date     the date for which to retrieve the price
	 * @return the underlying notional price
	 */
	private BigDecimal getUnderlyingNotionalPrice(AccountingPosition position, Date date) {
		// Get underlying security
		InvestmentSecurity security = position.getInvestmentSecurity();
		InvestmentSecurity underlyingSecurity = security.getUnderlyingSecurity();
		AssertUtils.assertNotNull(underlyingSecurity, "No underlying security was found for security [%s]. An underlying security is required when calculating the underlying notional price.", security.getLabel());

		/*
		 * TODO:
		 * Underlying security price is not converted to the denomination of the specified security; securities with differing underlying security currency denominations are not
		 * allowed.
		 */
		AssertUtils.assertEquals(security.getInstrument().getTradingCurrency(), underlyingSecurity.getInstrument().getTradingCurrency(), "The security [%s] and its underlying security [%s] do not have the same currency denomination. Underlying notional price calculations are not supported for securities whose currency denomination differs from that of the underlying security.", security.getLabel(), underlyingSecurity.getLabel());

		// Get underlying security price
		return getMarketDataRetriever().getPriceFlexible(underlyingSecurity, date, true);
	}


	/**
	 * Gets the notional price for the given position when the {@link NotionalPriceFields#STRIKE} notional type is used.
	 *
	 * @param position the position for which to retrieve the notional price to use
	 * @return the strike notional price
	 */
	private BigDecimal getStrikeNotionalPrice(AccountingPosition position) {
		InvestmentSecurity security = position.getInvestmentSecurity();
		BigDecimal strikePrice = security.getOptionStrikePrice();
		AssertUtils.assertNotNull(strikePrice, "No strike price was found for security [%s]. A strike price is required when calculating strike values.", security.getLabel());
		return strikePrice;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The field to use when calculating the notional value of a security. This type specifies the relationship between the source position and the produced notional value.
	 */
	public enum NotionalPriceFields {
		STANDARD, UNDERLYING, STRIKE
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public NotionalPriceFields getNotionalPriceField() {
		return this.notionalPriceField;
	}


	public void setNotionalPriceField(NotionalPriceFields notionalPriceField) {
		this.notionalPriceField = notionalPriceField;
	}
}
