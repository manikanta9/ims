package com.clifton.accounting.gl.posting;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.action.processor.AccountingJournalActionProcessorHandler;
import com.clifton.accounting.gl.journal.event.AccountingJournalPostingEvent;
import com.clifton.accounting.gl.journal.event.AccountingJournalUnpostingEvent;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author vgomelsky
 */
@Service
public class AccountingPostingServiceImpl implements AccountingPostingService {

	// DAO's are used here directly in order to avoid exposing potentially dangerous methods outside of this service
	private AdvancedUpdatableDAO<AccountingJournal, Criteria> accountingJournalDAO;
	private UpdatableDAO<AccountingJournalDetail> accountingJournalDetailDAO;
	private UpdatableDAO<AccountingTransaction> accountingTransactionDAO;

	@SuppressWarnings("rawtypes")
	private AccountingBookingService accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private AccountingJournalActionProcessorHandler accountingJournalActionProcessorHandler;
	private AccountingPeriodService accountingPeriodService;
	private AccountingTransactionService accountingTransactionService;

	private ContextHandler contextHandler;
	private EventHandler eventHandler;
	private SecurityAuthorizationService securityAuthorizationService;

	////////////////////////////////////////////////////////////////////////////
	/////////                     POST                               ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void postAccountingJournal(long journalId) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(journalId);

		getAccountingJournalService().validateAccountingJournal(journal);

		BookableEntity bookableEntity = getAccountingBookingService().getBookableEntity(journal);
		doPostAccountingJournal(journal, bookableEntity);
	}


	@Override
	@Transactional
	public AccountingJournal saveAccountingJournalAndPostIt(AccountingJournal journal, BookableEntity bookableEntity) {
		journal = getAccountingJournalService().saveAccountingJournal(journal);

		// don't need to validate again: validation was already performed during save
		return doPostAccountingJournal(journal, bookableEntity);
	}


	@Transactional
	protected AccountingJournal doPostAccountingJournal(AccountingJournal journal, BookableEntity bookableEntity) {
		// 1. make sure the journal hasn't been posted already and
		ValidationUtils.assertNull(journal.getPostingDate(), "Cannot post a journal that has already been posted to GL.", "postingDate");
		ValidationUtils.assertTrue(journal.getJournalStatus().isPostingAllowed(), "Journals are not allowed to be posted in status = " + journal.getJournalStatus().getName());

		// 2. update journal status
		String journalStatus = calculateJournalStatus(journal);
		journal.setJournalStatus(getAccountingJournalService().getAccountingJournalStatusByName(journalStatus));

		// 3. create transactions from corresponding journal details and delete the details
		moveJournalDetailsToTransactions(journal);

		// 4. update posting
		journal.setPostingDate(new Date());
		journal = getAccountingJournalDAO().save(journal);

		// 5. raise journal posting event
		getEventHandler().raiseEvent(AccountingJournalPostingEvent.ofEventTarget(journal));
		getAccountingJournalActionProcessorHandler().processActions(journal, bookableEntity);
		return journal;
	}


	private String calculateJournalStatus(AccountingJournal journal) {
		String journalStatus = AccountingJournalStatus.STATUS_ORIGINAL;
		if (journal.getJournalType().isSubsystemJournal()) {
			// only sub-system journals are linked to corresponding source entities
			AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
			searchForm.setJournalTypeId(journal.getJournalType().getId());
			if (journal.getJournalType().getTransactionTable() == null && journal.getParent() != null) {
				// adjusting journals
				searchForm.setParentId(journal.getParent().getId());
			}
			else {
				searchForm.setFkFieldId(journal.getFkFieldId());
			}
			searchForm.setJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
			searchForm.setIdNotEquals(journal.getId());
			searchForm.setOrderBy("id:desc");
			List<AccountingJournal> journalList = getAccountingJournalService().getAccountingJournalList(searchForm);
			AccountingJournal lastJournal = CollectionUtils.getFirstElement(journalList);
			if (lastJournal != null) {
				// event journals may have 2 journals (accrual and reversal): need to select correct one
				if (journal.getParent() == null && lastJournal.getParent() != null) {
					lastJournal = journalList.size() > 1 ? journalList.get(1) : null;
				}
				if (lastJournal != null) {
					getAccountingJournalService().populateJournal(lastJournal);
					if (lastJournal.isSameTransactionSet(journal)) {
						journalStatus = AccountingJournalStatus.STATUS_UNCHANGED;
					}
					else {
						journalStatus = AccountingJournalStatus.STATUS_MODIFIED;
					}
				}
			}
		}
		else if (journal.getParent() != null) {
			// parent for general journals is the corresponding Deleted journal
			AccountingJournal lastJournal = journal.getParent();
			getAccountingJournalService().populateJournal(lastJournal);
			if (lastJournal.isSameTransactionSet(journal)) {
				journalStatus = AccountingJournalStatus.STATUS_UNCHANGED;
			}
			else {
				journalStatus = AccountingJournalStatus.STATUS_MODIFIED;
			}
		}
		return journalStatus;
	}


	private void moveJournalDetailsToTransactions(AccountingJournal journal) {
		// 1. copy journal details into transactions
		List<AccountingTransaction> transactionList = new ArrayList<>();
		Map<Long, AccountingTransaction> detailIdToTransactionMap = new HashMap<>();
		@SuppressWarnings("unchecked")
		List<AccountingJournalDetail> detailList = (List<AccountingJournalDetail>) journal.getJournalDetailList();
		for (AccountingJournalDetailDefinition detail : detailList) {
			AccountingTransaction transaction = new AccountingTransaction();
			BeanUtils.copyProperties(detail, transaction, new String[]{"id", "parentDefinition", "createUserId", "createDate", "updateUserId", "updateDate", "rv"});
			transaction.setModified(AccountingJournalStatus.STATUS_MODIFIED.equals(journal.getJournalStatus().getName()));
			detailIdToTransactionMap.put(detail.getId(), transaction);

			if (!journal.getJournalType().isSubsystemJournal() && StringUtils.isEmpty(transaction.getDescription())) {
				// copy journal description into empty transaction descriptions for general journals
				transaction.setDescription(journal.getDescription());
			}

			transactionList.add(transaction);
		}

		// 2. remap parent definitions to parent transactions now that we have full mapping
		for (AccountingJournalDetailDefinition detail : detailList) {
			if (!detail.isParentDefinitionEmpty() && detail.getParentTransaction() == null) {
				detailIdToTransactionMap.get(detail.getId()).setParentTransaction(detailIdToTransactionMap.get(detail.getParentDefinition().getId()));
			}
		}

		// 3. sort in case this journal has transactions that reference other transactions from the same journal
		transactionList.sort((o1, o2) -> {
			int result = CompareUtils.compare(o1.getTransactionDate(), o2.getTransactionDate());
			if (result == 0) {
				AccountingJournalDetailDefinition d1 = o1.getParentDefinition();
				AccountingJournalDetailDefinition d2 = o2.getParentDefinition();
				if (d1 != null) {
					return CompareUtils.compare(d1.getIdentity(), o2.getIdentity());
				}
				else if (d2 != null) {
					return CompareUtils.compare(o1.getIdentity(), d2.getIdentity());
				}
				result = CompareUtils.compare(o1.getIdentity(), o2.getIdentity());
			}
			return result;
		});

		// 4. save transactions into GL
		getAccountingTransactionDAO().saveList(transactionList);

		// 5. delete no longer needed journal details
		getAccountingJournalDetailDAO().deleteList(detailList);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                    UNPOST                              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public AccountingJournal unpostAccountingJournal(long journalId) {
		// make sure the journal exists and was posted
		AccountingJournal journal = getAccountingJournalService().getAccountingJournal(journalId);
		ValidationUtils.assertNotNull(journal, "Cannot find journal with id = " + journalId);

		return doUnpostAccountingJournal(journal);
	}


	@Override
	@Transactional
	public AccountingJournal unpostAccountingJournalForSourceEntity(String sourceTable, int sourceEntityId) {
		AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySource(sourceTable, sourceEntityId);
		if (journal != null) {
			return doUnpostAccountingJournal(journal);
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	@Transactional
	protected AccountingJournal doUnpostAccountingJournal(AccountingJournal journal) {
		ValidationUtils.assertNotNull(journal.getPostingDate(), "Cannot unpost a journal that has not been posted: " + journal.getLabel());
		ValidationUtils.assertTrue(journal.getJournalStatus().isPosted(), "Cannot unpost a journal that is not in Posted status: " + journal.getLabel());

		validateCurrentUserPermissionToUnpost(journal);

		validateDependenciesForUnpost(journal);

		// mark transactions in GL as "deleted" and update journal's status to deleted
		List<AccountingTransaction> tranList = (List<AccountingTransaction>) journal.getJournalDetailList();
		for (AccountingTransaction tran : tranList) {
			tran.setDeleted(true);
			getAccountingTransactionDAO().save(tran);
		}

		journal.setJournalStatus(getAccountingJournalService().getAccountingJournalStatusByName(AccountingJournalStatus.STATUS_DELETED));
		journal = getAccountingJournalDAO().save(journal);

		if (journal.getJournalType().isSubsystemJournal()) {
			// for sub-systems that support more than one booking per source entity (accrue first and then reverse later),
			// make sure that the latest journal is unbooked first
			if (journal.getJournalType().isSubsystemPresent()) {
				AccountingJournalSearchForm journalSearchForm = new AccountingJournalSearchForm();
				journalSearchForm.setJournalTypeId(journal.getJournalType().getId());
				journalSearchForm.setFkFieldId(journal.getFkFieldId());
				journalSearchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
				List<AccountingJournal> journalList = getAccountingJournalService().getAccountingJournalList(journalSearchForm);
				for (AccountingJournal siblingJournal : CollectionUtils.getIterable(journalList)) {
					if (siblingJournal.getId().compareTo(journal.getId()) > 0) {
						throw new ValidationException("Cannot unbook journal '" + journal.getLabel() + "' because it was booked before another journal for the same sub-system entity: "
								+ siblingJournal.getLabel());
					}
				}
			}

			// does not restore the journal: unposts and unbooks immediately (unlike general journals)
			BookableEntity bookableEntity = getAccountingBookingService().markAccountingSubSystemJournalUnbooked(journal, false);

			getEventHandler().raiseEvent(AccountingJournalUnpostingEvent.ofEventTarget(journal));
			getAccountingJournalActionProcessorHandler().rollbackActions(journal, bookableEntity);
			return null;
		}
		else {
			// restore general journal and its details: create a copy to preserve original history
			AccountingJournal unpostedJournal = new AccountingJournal();
			BeanUtils.copyPropertiesExceptAudit(journal, unpostedJournal);
			unpostedJournal.setId(null);
			unpostedJournal.setParent(journal); // keep parent reference that can be used to change if the journal was changed
			unpostedJournal.setPostingDate(null);
			unpostedJournal.setJournalStatus(getAccountingJournalService().getAccountingJournalStatusByName(AccountingJournalStatus.STATUS_UNPOSTED));
			unpostedJournal = getAccountingJournalDAO().save(unpostedJournal);

			List<AccountingJournalDetail> detailList = new ArrayList<>();
			for (AccountingTransaction tran : tranList) {
				AccountingJournalDetail detail = new AccountingJournalDetail();
				BeanUtils.copyPropertiesExceptAudit(tran, detail);
				detail.setJournal(unpostedJournal);

				// clear detail description for general journals if it's the same as journal description
				// it will be copied back during posting and clearing allows a change in one place
				if (CompareUtils.isEqual(detail.getDescription(), journal.getDescription())) {
					detail.setDescription(null);
				}
				detailList.add(detail);
			}
			getAccountingJournalDetailDAO().saveList(detailList);

			getEventHandler().raiseEvent(AccountingJournalUnpostingEvent.ofEventTarget(journal));
			getAccountingJournalActionProcessorHandler().rollbackActions(journal, null);
			return unpostedJournal;
		}
	}


	private void validateCurrentUserPermissionToUnpost(AccountingJournal journal) {
		if (!AccountingJournalType.TRADE_JOURNAL.equals(journal.getJournalType().getName())) {
			// unbooking of trade journals can be called from other parts of the system (reconciliation) and should be allowed
			boolean allow = getSecurityAuthorizationService().isSecurityAccessAllowed("AccountingTransaction", SecurityPermission.PERMISSION_DELETE);
			if (!allow) {
				// also allow the person who booked Simple Journal to unbook it
				if (AccountingJournalType.SIMPLE_JOURNAL.equals(journal.getJournalType().getName())) {
					SecurityUser user = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
					if (user.getId().equals(journal.getCreateUserId())) {
						allow = true;
					}
				}
			}
			if (!allow) {
				throw new AccessDeniedException("DELETE permission is required to AccountingTransaction security resource or current user must the one who booked this Simple Journal.");
			}
		}
	}


	private void validateDependenciesForUnpost(AccountingJournal journal) {
		// make sure no transaction outside of this journal points to any of journal's transactions: dependencies must be unposted first
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setParentJournalId(journal.getId());
		List<AccountingTransaction> existingList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (AccountingTransaction tran : CollectionUtils.getIterable(existingList)) {
			if (!journal.equals(tran.getJournal())) {
				// not the same journal
				throw new ValidationException("Cannot unpost journal " + journal.getLabelLong() + " because there is another journal with General Ledger transaction " + tran
						+ " that points to it.\n\nUnpost the following dependent journal first: " + tran.getJournal().getLabelLong());
			}
		}

		// make sure no closing Position from a different journal exists that was posted after closing Position of this journal that closes the same open Position: what's after must be unposted first
		for (AccountingJournalDetailDefinition detail : journal.getJournalDetailList()) {
			// make sure accounting period unposted from is still open
			if (getAccountingPeriodService().isAccountingPeriodClosed(detail.getClientInvestmentAccount().getId(), detail.getTransactionDate())) {
				throw new ValidationException("Cannot unpost " + detail.getClientInvestmentAccount().getLabel() + " entry on " + DateUtils.fromDateShort(detail.getTransactionDate())
						+ " from a closed accounting period.");
			}

			if (detail.getAccountingAccount().isPosition() && detail.getParentTransaction() != null) {
				searchForm = new AccountingTransactionSearchForm();
				searchForm.setParentId(detail.getParentTransaction().getId());
				searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
				searchForm.addSearchRestriction(new SearchRestriction("journalId", ComparisonConditions.NOT_EQUALS, journal.getId()));
				searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.GREATER_THAN, detail.getTransactionDate()));
				searchForm.addSearchRestriction(new SearchRestriction("id", ComparisonConditions.GREATER_THAN, detail.getIdentity()));
				existingList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
				AccountingTransaction tran = CollectionUtils.getFirstElement(existingList);
				if (tran != null) {
					throw new ValidationException("Cannot unpost journal " + journal.getLabelLong() + " because there is another journal with position General Ledger transaction " + tran
							+ " that was booked after this journal and it closes Position " + detail.getParentTransaction()
							+ " which was earlier closed by this journal's position transaction.  You must unbook the later journal first.");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingJournal, Criteria> getAccountingJournalDAO() {
		return this.accountingJournalDAO;
	}


	public void setAccountingJournalDAO(AdvancedUpdatableDAO<AccountingJournal, Criteria> accountingJournalDAO) {
		this.accountingJournalDAO = accountingJournalDAO;
	}


	public UpdatableDAO<AccountingJournalDetail> getAccountingJournalDetailDAO() {
		return this.accountingJournalDetailDAO;
	}


	public void setAccountingJournalDetailDAO(UpdatableDAO<AccountingJournalDetail> accountingJournalDetailDAO) {
		this.accountingJournalDetailDAO = accountingJournalDetailDAO;
	}


	public UpdatableDAO<AccountingTransaction> getAccountingTransactionDAO() {
		return this.accountingTransactionDAO;
	}


	public void setAccountingTransactionDAO(UpdatableDAO<AccountingTransaction> accountingTransactionDAO) {
		this.accountingTransactionDAO = accountingTransactionDAO;
	}


	@SuppressWarnings("rawtypes")
	public AccountingBookingService getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(@SuppressWarnings("rawtypes") AccountingBookingService accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingJournalActionProcessorHandler getAccountingJournalActionProcessorHandler() {
		return this.accountingJournalActionProcessorHandler;
	}


	public void setAccountingJournalActionProcessorHandler(AccountingJournalActionProcessorHandler accountingJournalActionProcessorHandler) {
		this.accountingJournalActionProcessorHandler = accountingJournalActionProcessorHandler;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
