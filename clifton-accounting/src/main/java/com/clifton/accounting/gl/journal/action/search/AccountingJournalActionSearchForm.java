package com.clifton.accounting.gl.journal.action.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class AccountingJournalActionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "scopeSystemCondition.id")
	private Integer scopeSystemConditionId;

	@SearchField(searchField = "actionBean.id")
	private Integer actionBeanId;

	@SearchField
	private Short processingOrder;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public Integer getScopeSystemConditionId() {
		return this.scopeSystemConditionId;
	}


	public void setScopeSystemConditionId(Integer scopeSystemConditionId) {
		this.scopeSystemConditionId = scopeSystemConditionId;
	}


	public Integer getActionBeanId() {
		return this.actionBeanId;
	}


	public void setActionBeanId(Integer actionBeanId) {
		this.actionBeanId = actionBeanId;
	}


	public Short getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(Short processingOrder) {
		this.processingOrder = processingOrder;
	}
}
