package com.clifton.accounting.gl.balance.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;


/**
 * @author TerryS
 */
public class AccountingBalanceExtendedSearchForm extends AccountingBalanceSearchForm {

	@SearchField(searchField = "childAccountingAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] childAccountingAccountIds;


	public void setChildAccountingAccountIds(Short[] childAccountingAccountIds) {
		this.childAccountingAccountIds = childAccountingAccountIds;
	}


	public Short[] getChildAccountingAccountIds() {
		return this.childAccountingAccountIds;
	}
}
