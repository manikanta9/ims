package com.clifton.accounting.gl.booking;


import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.beans.BaseEntity;


/**
 * The <code>AccountingBookingDescriptor</code> class represents a source entity that is ready to be booked.
 * The record stamp is populated with corresponding entity fields.
 *
 * @author vgomelsky
 */
public class AccountingBookingDescriptor extends BaseEntity<Long> {

	private AccountingJournalType journalType;
	private String label;


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public AccountingJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingJournalType journalType) {
		this.journalType = journalType;
	}
}
