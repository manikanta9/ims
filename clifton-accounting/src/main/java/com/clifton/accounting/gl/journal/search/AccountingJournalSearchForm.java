package com.clifton.accounting.gl.journal.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>AccountingJournalSearchForm</code> class defines search configuration for AccountingJournal objects.
 *
 * @author vgomelsky
 */
public class AccountingJournalSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Long idNotEquals;

	@SearchField(searchField = "parent.id")
	private Long parentId;

	@SearchField(searchField = "journalType.id")
	private Short journalTypeId;

	@SearchField(searchField = "journalStatus.id")
	private Short journalStatusId;

	@SearchField(searchFieldPath = "journalStatus", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String journalStatusName;

	@SearchField(searchField = "journalStatus.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeJournalStatusId;

	@SearchField(searchFieldPath = "journalStatus", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeJournalStatusName;

	@SearchField(searchField = "systemTable.id")
	private Short systemTableId;

	@SearchField(searchField = "name", searchFieldPath = "systemTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String systemTableName;

	@SearchField(searchFieldPath = "journalType", searchField = "transactionTable.id")
	private Short transactionTableId;

	@SearchField(searchFieldPath = "journalType.transactionTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String transactionTableName;

	@SearchField
	private Integer fkFieldId;

	@SearchField(searchField = "fkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Integer[] fkFieldIdList;

	@SearchField
	private Date postingDate;

	@SearchField(searchField = "postingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean unpostedJournalsOnly;

	@SearchField(searchField = "postingDate", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean postedJournalsOnly;

	@SearchField
	private String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getIdNotEquals() {
		return this.idNotEquals;
	}


	public void setIdNotEquals(Long idNotEquals) {
		this.idNotEquals = idNotEquals;
	}


	public Long getParentId() {
		return this.parentId;
	}


	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	public Short getJournalTypeId() {
		return this.journalTypeId;
	}


	public void setJournalTypeId(Short journalTypeId) {
		this.journalTypeId = journalTypeId;
	}


	public Short getJournalStatusId() {
		return this.journalStatusId;
	}


	public void setJournalStatusId(Short journalStatusId) {
		this.journalStatusId = journalStatusId;
	}


	public String getJournalStatusName() {
		return this.journalStatusName;
	}


	public void setJournalStatusName(String journalStatusName) {
		this.journalStatusName = journalStatusName;
	}


	public Short getExcludeJournalStatusId() {
		return this.excludeJournalStatusId;
	}


	public void setExcludeJournalStatusId(Short excludeJournalStatusId) {
		this.excludeJournalStatusId = excludeJournalStatusId;
	}


	public String getExcludeJournalStatusName() {
		return this.excludeJournalStatusName;
	}


	public void setExcludeJournalStatusName(String excludeJournalStatusName) {
		this.excludeJournalStatusName = excludeJournalStatusName;
	}


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}


	public Short getTransactionTableId() {
		return this.transactionTableId;
	}


	public void setTransactionTableId(Short transactionTableId) {
		this.transactionTableId = transactionTableId;
	}


	public String getTransactionTableName() {
		return this.transactionTableName;
	}


	public void setTransactionTableName(String transactionTableName) {
		this.transactionTableName = transactionTableName;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Integer[] getFkFieldIdList() {
		return this.fkFieldIdList;
	}


	public void setFkFieldIdList(Integer[] fkFieldIdList) {
		this.fkFieldIdList = fkFieldIdList;
	}


	public Date getPostingDate() {
		return this.postingDate;
	}


	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}


	public Boolean getUnpostedJournalsOnly() {
		return this.unpostedJournalsOnly;
	}


	public void setUnpostedJournalsOnly(Boolean unpostedJournalsOnly) {
		this.unpostedJournalsOnly = unpostedJournalsOnly;
	}


	public Boolean getPostedJournalsOnly() {
		return this.postedJournalsOnly;
	}


	public void setPostedJournalsOnly(Boolean postedJournalsOnly) {
		this.postedJournalsOnly = postedJournalsOnly;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
