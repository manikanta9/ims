package com.clifton.accounting.gl.booking.session;


import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SimpleBookingSession</code> class provides BookingSession that's used by most rules/processors.
 * The session holds the journal that will be created after booking (each rule add/modifies journal details)
 * as well as sub-system entity being booked.
 *
 * @param <T>
 * @author vgomelsky
 */
public class SimpleBookingSession<T extends BookableEntity> implements BookingSession<T> {

	private final AccountingJournal journal;
	private final T bookableEntity;
	/**
	 * A Map of current positions used during this booking session.  Because existing positions maybe reduce
	 * or closing while executing a set of rules, we must use the latest available data from the booking session.
	 * An empty list means that there is no position while null means that position hasn't been retrieved yet.
	 * <p/>
	 * Use getPositionKey method to generate the key for a given position (client account + holding account + security + GL Account).
	 */
	private Map<String, List<BookingPosition>> currentPositionList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SimpleBookingSession(AccountingJournal journal, T bookableEntity) {
		this.journal = journal;
		this.bookableEntity = bookableEntity;
	}


	@Override
	public AccountingJournal getJournal() {
		return this.journal;
	}


	@Override
	public T getBookableEntity() {
		return this.bookableEntity;
	}


	@Override
	public Map<String, List<BookingPosition>> getCurrentPositions() {
		if (this.currentPositionList == null) {
			this.currentPositionList = new HashMap<>();
		}
		return this.currentPositionList;
	}


	@Override
	public void addNewCurrentPosition(AccountingJournalDetail newPosition) {
		String key = getPositionKey(newPosition);
		List<BookingPosition> positionList = getCurrentPositions().computeIfAbsent(key, k -> new ArrayList<>());
		positionList.add(new BookingPosition(AccountingPosition.forOpeningTransaction(newPosition)));
	}


	@Override
	public void addNewCurrentPosition(AccountingPosition newPosition) {
		String key = getPositionKey(newPosition);
		List<BookingPosition> positionList = getCurrentPositions().computeIfAbsent(key, k -> new ArrayList<>());
		positionList.add(new BookingPosition(newPosition));
	}


	@Override
	public BookingPosition getBookingPosition(AccountingJournalDetailDefinition detail) {
		String key = getPositionKey(detail);
		List<BookingPosition> positionList = getCurrentPositions().get(key);
		for (BookingPosition position : CollectionUtils.getIterable(positionList)) {
			// when closing a position from the same journal, parent transaction will not be set because it doesn't exist yet
			if (ObjectUtils.coalesce(detail.getParentTransaction(), detail.getParentDefinition()).getId().equals(position.getPosition().getId())) {
				return position;
			}
		}
		return null;
	}


	@Override
	public String getPositionKey(AccountingTransactionInfo accountingTransactionInfo) {
		StringBuilder key = new StringBuilder();
		key.append("CA").append(accountingTransactionInfo.getClientInvestmentAccount().getIdentity());
		key.append("HA").append(accountingTransactionInfo.getHoldingInvestmentAccount().getIdentity());
		key.append("S").append(accountingTransactionInfo.getInvestmentSecurity().getId());
		key.append("GL").append(accountingTransactionInfo.getAccountingAccount().getId());
		return key.toString();
	}


	@Override
	public void sortPositions(AccountingPositionOrders positionOrder) {
		getCurrentPositions().forEach((key, positions) -> positionOrder.sortPositions(positions, BookingPosition::getPosition));
	}
}
