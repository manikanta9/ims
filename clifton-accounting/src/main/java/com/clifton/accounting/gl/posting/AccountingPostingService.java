package com.clifton.accounting.gl.posting;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author vgomelsky
 */
public interface AccountingPostingService {

	////////////////////////////////////////////////////////////////////////////
	/////////                     POST                               ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Posts AccountingJournal with the specified id to the general ledger.  Performs full validation before posting.
	 * Posting process deletes journal details, creates corresponding AccountingTransaction objects and sets the PostingDate on AccountingJournal.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public void postAccountingJournal(long journalId);


	/**
	 * Saves the specified AccountingJournal object including its list of AccountingJournalDetail objects.
	 * Saves the journal only if it passes all validation logic. If the journal is successfully saved, posts
	 * it to the General Ledger in the same transaction.
	 * <p/>
	 * Use this method for improved performance to avoid duplicate steps performed during save and post.
	 */
	@DoNotAddRequestMapping
	public AccountingJournal saveAccountingJournalAndPostIt(AccountingJournal journal, BookableEntity bookableEntity);

	////////////////////////////////////////////////////////////////////////////
	/////////                    UNPOST                              ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Updates AccountingTransaction records associated with the specified journal to mark them as deleted,
	 * updates the status of the journal to mark it as deleted, and clears booking date for source entity (if any).
	 * The history is never really deleted and is always preserved for historical analysis.
	 *
	 * @return null for sub-system journals and newly created journal for general journals
	 */
	@RequestMapping("accountingJournalUnpost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal unpostAccountingJournal(long journalId);


	/**
	 * Retrieves the journal entry for the source entity (if any) and the calls {@link #unpostAccountingJournal(long)}
	 */
	@RequestMapping("accountingJournalForSourceEntityUnpost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public AccountingJournal unpostAccountingJournalForSourceEntity(String sourceTable, int sourceEntityId);
}
