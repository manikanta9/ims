package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingAggregateCurrencyBookingRule</code> aggregates cash/currency records into one for the same
 * Client Account, Holding Account, GL Account, Original Transaction Date and FX Rate.
 * <p>
 * Currency (and Cash) is additive, it is not a position and aggregation avoids unnecessary rows.
 */
public class AccountingAggregateCurrencyBookingRule<T extends BookableEntity> extends AccountingCommonBookingRule<T> implements AccountingBookingRule<T> {

	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();

		// group Cash details to be aggregated by natural key: Client Account, Holding Account, GL Account, Original Transaction Date
		Map<String, List<AccountingJournalDetailDefinition>> aggregatedMap = new LinkedHashMap<>();
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(journal.getJournalDetailList())) {
			if (isEligibleForAggregation(detail)) {
				String naturalKey = generateNaturalKey(detail);
				List<AccountingJournalDetailDefinition> aggregatedList = aggregatedMap.computeIfAbsent(naturalKey, k -> new ArrayList<>());
				aggregatedList.add(detail);
			}
		}

		// aggregate only if more than 1 record found with same natural key
		for (List<AccountingJournalDetailDefinition> aggregatedList : aggregatedMap.values()) {
			if (aggregatedList.size() > 1) {
				// aggregate all details for the same natural key into one record
				AccountingJournalDetail aggregatedRecord = null;
				boolean sameDescription = true;
				for (AccountingJournalDetailDefinition detail : aggregatedList) {
					if (aggregatedRecord == null) {
						aggregatedRecord = new AccountingJournalDetail();
						BeanUtils.copyProperties(detail, aggregatedRecord);
					}
					else {
						aggregatedRecord.setLocalDebitCredit(aggregatedRecord.getLocalDebitCredit().add(detail.getLocalDebitCredit()));
						// NOTE: Current logic does proper base rounding at lot level. This can result in total being off by a few cents.
						// Should we have accurate base total instead and allocate pennies to corresponding "opposite" journal details?
						// Would need to add "oppositeDefinition" to journal detail and set it in createCashCurrencyRecord method
						// See TradeID = 429719 which is off by 18 cents
						aggregatedRecord.setBaseDebitCredit(aggregatedRecord.getBaseDebitCredit().add(detail.getBaseDebitCredit()));
						aggregatedRecord.setPositionCostBasis(aggregatedRecord.getPositionCostBasis().add(detail.getPositionCostBasis()));
						if (sameDescription) {
							sameDescription = CompareUtils.isEqual(aggregatedRecord.getDescription(), detail.getDescription());
						}
					}

					// re-map detail children to the new aggregatedRecord (avoid orphans)
					for (AccountingJournalDetailDefinition orphan : journal.getJournalDetailList()) {
						if (detail.equals(orphan.getParentDefinition())) {
							orphan.setParentDefinition(aggregatedRecord);
						}
					}
					journal.removeJournalDetail(detail);
				}

				if (!sameDescription) {
					updateCashCurrencyRecordDescription(aggregatedRecord, false);
				}
				journal.addJournalDetail(aggregatedRecord);
			}
		}
	}


	/**
	 * Aggregate all currency details. If parent definition or transaction is populated, aggregation should be by that parent lot.
	 *
	 * @see #generateNaturalKey(AccountingJournalDetailDefinition)
	 */
	protected boolean isEligibleForAggregation(AccountingJournalDetailDefinition detail) {
		return detail.getAccountingAccount().isCurrency();
	}


	protected String generateNaturalKey(AccountingJournalDetailDefinition detail) {
		StringBuilder result = new StringBuilder(128).append(detail.createNaturalKey())
				.append("FX").append(CoreMathUtils.formatNumber(detail.getExchangeRateToBase(), MathUtils.NUMBER_FORMAT_DECIMAL_MAX));
		// currency translation must keep its parent, when applicable, for proper lot-level attribution
		if (!detail.isParentTransactionEmpty()) {
			result.append("PID").append(detail.getParentTransaction().getId());
		}
		else if (!detail.isParentDefinitionEmpty()) {
			result.append("PDID").append(detail.getParentDefinition().getId());
		}
		return result.toString();
	}
}
