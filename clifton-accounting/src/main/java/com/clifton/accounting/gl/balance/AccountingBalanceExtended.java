package com.clifton.accounting.gl.balance;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.dataaccess.dao.NonPersistentObject;


/**
 * Allows aggregating accounting transaction amounts based on the parent transactions' security.
 * Example, get the receivable account totals based on the parent security rather than the child accounts' (cash) security.
 *
 * @author TerryS
 */
@NonPersistentObject
public class AccountingBalanceExtended extends AccountingBalance {

	private AccountingAccount childAccountingAccount;


	public AccountingAccount getChildAccountingAccount() {
		return this.childAccountingAccount;
	}


	public void setChildAccountingAccount(AccountingAccount childAccountingAccount) {
		this.childAccountingAccount = childAccountingAccount;
	}
}
