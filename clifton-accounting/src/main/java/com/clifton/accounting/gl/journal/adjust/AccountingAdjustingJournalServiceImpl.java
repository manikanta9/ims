package com.clifton.accounting.gl.journal.adjust;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingAdjustingJournalServiceImpl</code> class implements methods that create various types of adjusting journals:
 * reversal, price change, etc. Because GL entries cannot be modified, any adjustment must be done via a new journal that can
 * "cancel" existing entries and "replace" them with new ones.
 * <p/>
 * Note: one can un-post, modify and re-post a journal if it's still in the open period and company's accounting policies allow this.
 *
 * @author vgomelsky
 */
@Service
public class AccountingAdjustingJournalServiceImpl implements AccountingAdjustingJournalService {

	private AccountingAccountService accountingAccountService;
	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingEventJournalService accountingEventJournalService;

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingJournal adjustAccountingJournalRealized(long accountingPositionId, BigDecimal realizedAdjustmentAmount, String note) {
		return adjustAccountingJournal(accountingPositionId, AccountingAccount.REVENUE_REALIZED, realizedAdjustmentAmount, note, true, AccountingJournalType.ADJUSTING_JOURNAL_REALIZED);
	}


	@Override
	public AccountingJournal adjustAccountingJournalCommission(long accountingPositionId, BigDecimal commissionAmount, String note) {
		return adjustAccountingJournal(accountingPositionId, AccountingAccount.EXPENSE_COMMISSION, commissionAmount, note, false, AccountingJournalType.ADJUSTING_JOURNAL_COMMISSION);
	}


	@Transactional
	protected AccountingJournal adjustAccountingJournal(long accountingPositionId, String adjustmentAccountingAccount, BigDecimal adjustmentAmount, String note, boolean allowForClosingOnly, String journalTypeName) {
		// validate arguments
		AccountingTransaction position = getAccountingTransactionService().getAccountingTransaction(accountingPositionId);
		ValidationUtils.assertNotNull(position, "Cannot find position with id = " + accountingPositionId);
		ValidationUtils.assertTrue(position.getAccountingAccount().isPosition(), adjustmentAccountingAccount + " can be adjusted only for a position GL Account.");
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(adjustmentAmount), adjustmentAccountingAccount + " adjustment amount cannot be zero.");
		if (allowForClosingOnly) {
			ValidationUtils.assertNotNull(position.getParentTransaction(), adjustmentAccountingAccount + " can be adjusted only for closing position.");
		}

		// create notes
		String tranNote = adjustmentAccountingAccount + " adjustment for " + position.getInvestmentSecurity().getSymbol();
		if (StringUtils.isEmpty(note)) {
			note = tranNote;
		}

		// create new journal and populate header fields
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalTypeByName(journalTypeName);
		ValidationUtils.assertNotNull(journalType, "Cannot find journal type with name = " + journalTypeName);
		journal.setJournalType(journalType);
		journal.setParent(position.getJournal());
		journal.setSystemTable(journalType.getSystemTable());
		journal.setFkFieldId(null);
		journal.setDescription(note);

		// create realized and payment journal details
		AccountingJournalDetail adjustmentDetail = new AccountingJournalDetail();
		BeanUtils.copyProperties(position, adjustmentDetail, new String[]{"id"});
		adjustmentDetail.setParentTransaction(position);
		adjustmentDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(adjustmentAccountingAccount));
		adjustmentDetail.setQuantity(null); // quantity is used to determine the number of units closed: adjusting and not closing additional units
		adjustmentDetail.setPrice(null);
		adjustmentDetail.setLocalDebitCredit(adjustmentAmount.negate());
		adjustmentDetail.setBaseDebitCredit(MathUtils.multiply(adjustmentDetail.getLocalDebitCredit(), adjustmentDetail.getExchangeRateToBase(), 2));
		adjustmentDetail.setPositionCostBasis(BigDecimal.ZERO);
		adjustmentDetail.setDescription(tranNote);

		journal.addJournalDetail(adjustmentDetail);
		journal.addJournalDetail(createPaymentJournalDetail(adjustmentDetail));

		// save the journal and immediately post the journal using efficient implementation which avoids duplicate validation during save
		journal = getAccountingPostingService().saveAccountingJournalAndPostIt(journal, null);
		return journal;
	}


	@Override
	public AccountingJournal adjustAccountingJournalDividend(long dividendTransactionId, BigDecimal adjustmentAmount, String note) {
		// validate arguments
		AccountingTransaction dividendTransaction = getAccountingTransactionService().getAccountingTransaction(dividendTransactionId);
		ValidationUtils.assertNotNull(dividendTransaction, "Cannot find dividend transaction entry with id = " + dividendTransactionId);
		AccountingTransaction position = dividendTransaction.getParentTransaction();
		ValidationUtils.assertNotNull(position, "Dividend Adjustment: " + dividendTransaction + " must be linked to a Position.");
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(adjustmentAmount), "Dividend Adjustment adjustment amount cannot be zero.");
		ValidationUtils.assertNotNull(dividendTransaction.getFkFieldId(), "Dividend Adjustment FKFieldID must be set for: " + dividendTransaction);
		AccountingEventJournalDetail eventJournalDetail = getAccountingEventJournalService().getAccountingEventJournalDetail(dividendTransaction.getFkFieldId());
		ValidationUtils.assertNotNull(eventJournalDetail, "Cannot find Event Journal Detail with id = " + dividendTransaction.getFkFieldId());
		ValidationUtils.assertTrue(position.equals(eventJournalDetail.getAccountingTransaction()), "Event Journal Detail " + eventJournalDetail + " must reference the same position: " + position);
		ValidationUtils.assertFalse(DateUtils.compare(eventJournalDetail.getJournal().getSecurityEvent().getPaymentDate(), new Date(), false) > 0, "Cannot adjust an adjustment that has not happened yet.");

		// create new journal and populate header fields
		AccountingJournal journal = createAccountingJournal(dividendTransaction, AccountingJournalType.ADJUSTING_JOURNAL_DIVIDEND, "Manual adjustment for system generated dividend adjustment");

		// create Interest Income journal details
		AccountingJournalDetail adjustmentDetail = new AccountingJournalDetail();
		BeanUtils.copyProperties(dividendTransaction, adjustmentDetail, new String[]{"id"});
		adjustmentDetail.setLocalDebitCredit(adjustmentAmount.negate());
		adjustmentDetail.setBaseDebitCredit(MathUtils.multiply(adjustmentDetail.getLocalDebitCredit(), adjustmentDetail.getExchangeRateToBase(), 2));
		adjustmentDetail.setTransactionDate(eventJournalDetail.getJournal().getSecurityEvent().getPaymentDate());
		adjustmentDetail.setOriginalTransactionDate(adjustmentDetail.getTransactionDate());
		adjustmentDetail.setSettlementDate(adjustmentDetail.getTransactionDate());

		journal.addJournalDetail(adjustmentDetail);
		journal.addJournalDetail(createPaymentJournalDetail(adjustmentDetail));

		// save the journal and immediately post the journal using efficient implementation which avoids duplicate validation during save
		journal = getAccountingPostingService().saveAccountingJournalAndPostIt(journal, null);
		return journal;
	}


	@Override
	public AccountingJournal adjustAccountingJournalCouponPayment(long couponTransactionId, BigDecimal adjustmentAmount) {
		// validate arguments
		AccountingTransaction couponTransaction = getAccountingTransactionService().getAccountingTransaction(couponTransactionId);
		ValidationUtils.assertNotNull(couponTransaction, "Cannot find Interest Income GL entry with id = " + couponTransaction);
		AccountingTransaction position = couponTransaction.getParentTransaction();
		ValidationUtils.assertNotNull(position, "Coupon Payment must: " + couponTransaction + " must be linked to a Position.");
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(adjustmentAmount), "Coupon Payment adjustment amount cannot be zero.");
		ValidationUtils.assertNotNull(couponTransaction.getFkFieldId(), "Coupon Payment FKFieldID must be set for: " + couponTransaction);
		AccountingEventJournalDetail eventJournalDetail = getAccountingEventJournalService().getAccountingEventJournalDetail(couponTransaction.getFkFieldId());
		ValidationUtils.assertNotNull(eventJournalDetail, "Cannot find Event Journal Detail with id = " + couponTransaction.getFkFieldId());
		ValidationUtils.assertTrue(position.equals(eventJournalDetail.getAccountingTransaction()), "Event Journal Detail " + eventJournalDetail + " must reference the same position: " + position);
		ValidationUtils.assertFalse(DateUtils.compare(eventJournalDetail.getJournal().getSecurityEvent().getPaymentDate(), new Date(), false) > 0, "Cannot adjust a payment that has not happened yet.");

		// create new journal and populate header fields
		AccountingJournal journal = createAccountingJournal(couponTransaction, AccountingJournalType.ADJUSTING_JOURNAL_COUPON_PAYMENT, "Manual adjustment for system generated coupon payment");

		// create Interest Income journal details
		AccountingJournalDetail adjustmentDetail = new AccountingJournalDetail();
		BeanUtils.copyProperties(couponTransaction, adjustmentDetail, new String[]{"id"});
		adjustmentDetail.setLocalDebitCredit(adjustmentAmount.negate());
		adjustmentDetail.setBaseDebitCredit(MathUtils.multiply(adjustmentDetail.getLocalDebitCredit(), adjustmentDetail.getExchangeRateToBase(), 2));
		adjustmentDetail.setTransactionDate(eventJournalDetail.getJournal().getSecurityEvent().getPaymentDate());
		adjustmentDetail.setOriginalTransactionDate(adjustmentDetail.getTransactionDate());
		adjustmentDetail.setSettlementDate(adjustmentDetail.getTransactionDate());

		journal.addJournalDetail(adjustmentDetail);
		journal.addJournalDetail(createPaymentJournalDetail(adjustmentDetail));

		// save the journal and immediately post the journal using efficient implementation which avoids duplicate validation during save
		journal = getAccountingPostingService().saveAccountingJournalAndPostIt(journal, null);
		return journal;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingJournal createAccountingJournal(AccountingTransaction accountingTransaction, String accountingJournalType, String description) {
		// create new journal and populate header fields
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalTypeByName(accountingJournalType);
		ValidationUtils.assertNotNull(journalType, "Cannot find journal type with name = " + accountingJournalType);
		journal.setJournalType(journalType);
		journal.setParent(accountingTransaction.getJournal());
		journal.setSystemTable(journalType.getSystemTable());
		journal.setFkFieldId(null);
		journal.setDescription(description);

		return journal;
	}


	private AccountingJournalDetail createPaymentJournalDetail(AccountingJournalDetail oppositeDetail) {
		AccountingJournalDetail paymentDetail = new AccountingJournalDetail();
		BeanUtils.copyProperties(oppositeDetail, paymentDetail);

		InvestmentAccount cashAccount = getInvestmentSecurityUtilHandler().getCashLocationHoldingAccount(oppositeDetail);
		paymentDetail.setHoldingInvestmentAccount(cashAccount);
		paymentDetail.setInvestmentSecurity(paymentDetail.getInvestmentSecurity().getInstrument().getTradingCurrency());
		if (InvestmentUtils.isSecurityEqualToClientAccountBaseCurrency(paymentDetail)) {
			paymentDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		}
		else {
			paymentDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
		}

		paymentDetail.setOpening(true);
		paymentDetail.setQuantity(null);
		paymentDetail.setLocalDebitCredit(paymentDetail.getLocalDebitCredit().negate());
		paymentDetail.setBaseDebitCredit(paymentDetail.getBaseDebitCredit().negate());

		return paymentDetail;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
