package com.clifton.accounting.gl.journal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.dataaccess.dao.locator.DtoLocator;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemTable;

import java.math.BigDecimal;
import java.util.Date;


@DtoLocator(dtoClass = AccountingJournalDetail.class)
public interface AccountingJournalDetailDefinition extends AccountingTransactionInfo, UpdatableEntity {

	/**
	 * Converts this object to a formatted string with optional header line. Multiple details can be printed one after another.
	 * The output includes all major fields in logical order and is intended to be copy/pasted into Excel for detailed analysis.
	 */
	public String toStringFormatted(boolean includeHeader);


	/**
	 * Converts this object to a formatted string with optional header line. Multiple details can be printed one after another.
	 * The output includes all major fields in logical order and is intended to be copy/pasted into Excel for detailed analysis.
	 * If the withAdditionalFields flag is set to True, the output includes field values for: isOpening()
	 */
	public String toStringFormatted(boolean includeHeader, boolean withAdditionalFields);


	public AccountingJournal getJournal();


	public void setJournal(AccountingJournal journal);


	public AccountingTransaction getParentTransaction();


	public void setParentTransaction(AccountingTransaction parent);


	default public boolean isParentTransactionEmpty() {
		return getParentTransaction() == null;
	}


	/**
	 * As opposed to parentTransaction, this is the parent from the same journal before parentTransaction has been created.
	 * The parent can be corresponding position being reduced within the same journal by this detail (Asset Backed Security: Current Face < Original Face).
	 * Or the parent can be a related position that this transaction is linked to: parent position for commission, realized, accrued interest, etc.
	 */
	public AccountingJournalDetailDefinition getParentDefinition();


	/**
	 * As opposed to parentTransaction, this is the parent from the same journal before parentTransaction has been created.
	 * The parent can be corresponding position being reduced within the same journal by this detail (Asset Backed Security: Current Face < Original Face).
	 * Or the parent can be a related position that this transaction is linked to: parent position for commission, realized, accrued interest, etc.
	 */
	public void setParentDefinition(AccountingJournalDetailDefinition parent);


	/**
	 * Does not have parentDefinition field set: no parent from the same journal.
	 * Parent definition is set for bond positions that reduce from Original Face to Current Face.
	 * It's also set to link commission/realized/accrued interest/etc. to corresponding position.
	 */
	default public boolean isParentDefinitionEmpty() {
		return getParentDefinition() == null;
	}


	/**
	 * Non persisted field that indicates that the detail represents a transaction that is being used to adjust the quantity
	 * on a security with factor changes.
	 */
	public boolean isFactorChangeQuantityAdjustment();


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount);


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity);


	public void setAccountingAccount(AccountingAccount accountingAccount);


	public void setExecutingCompany(BusinessCompany executingCompany);


	public BusinessCompany getExecutingOrClearingCompany();


	public SystemTable getSystemTable();


	public void setSystemTable(SystemTable systemTable);


	public Integer getFkFieldId();


	public void setFkFieldId(Integer fkFieldId);


	public void setQuantity(BigDecimal quantity);


	public BigDecimal getLocalDebitCredit();


	public void setLocalDebitCredit(BigDecimal localDebitCredit);


	public BigDecimal getBaseDebitCredit();


	public void setBaseDebitCredit(BigDecimal baseDebitCredit);


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase);


	public String getDescription();


	public void setDescription(String description);


	public void setTransactionDate(Date transactionDate);


	/**
	 * The date of the original transaction which in most cases is the same as transaction date.
	 * However, for transfers it is the date of the opening trade which is prior to transaction date.
	 * The only time this date is used is to figure out which open position (based on transactionDate) to close first.
	 */
	public Date getOriginalTransactionDate();


	public void setOriginalTransactionDate(Date originalTransactionDate);


	public void setSettlementDate(Date settlementDate);


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount);


	public void setPrice(BigDecimal price);


	public boolean isOpening();


	public void setOpening(boolean opening);


	public boolean isClosing();


	public void setPositionCostBasis(BigDecimal positionCostBasis);


	/**
	 * Returns commission associated with this specific lot.  It is used during booking process only and ideally should not
	 * even be here.  Consider moving to BookingPosition object.
	 */
	public BigDecimal getPositionCommission();


	/**
	 * Sets commission associated with this specific lot.  It is used during booking process only and ideally should not
	 * even be here.  Consider moving to BookingPosition object.
	 */
	public void setPositionCommission(BigDecimal positionCommission);


	public void setId(Long id);


	/**
	 * Generates a natural key for this journal detail. It can be used to aggregate similar records or determine position closing.
	 * Natural Key = Security + Holding Account + Client Account + GL Account + Original Transaction Date + Settlement Date
	 */
	public String createNaturalKey();
}
