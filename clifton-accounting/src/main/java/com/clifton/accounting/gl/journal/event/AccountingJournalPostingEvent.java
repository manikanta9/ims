package com.clifton.accounting.gl.journal.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.util.event.EventObject;


/**
 * <code>AccountingJournalPostingEvent</code> represents an {@link com.clifton.core.util.event.Event} for
 * posting/booking {@link AccountingJournal}s.
 *
 * @author NickK
 */
public class AccountingJournalPostingEvent extends EventObject<AccountingJournal, Object> {

	public static final String EVENT_NAME = "Accounting Journal Posted";

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingJournalPostingEvent(AccountingJournal journal) {
		super(EVENT_NAME);
		setTarget(journal);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static AccountingJournalPostingEvent ofEventTarget(AccountingJournal journal) {
		return new AccountingJournalPostingEvent(journal);
	}
}
