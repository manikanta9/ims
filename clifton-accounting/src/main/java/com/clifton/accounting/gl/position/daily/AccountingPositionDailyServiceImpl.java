package com.clifton.accounting.gl.position.daily;


import com.clifton.accounting.AccountingUtils;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentObjectInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>AccountingPositionDailyServiceImpl</code> class provides basic implementation of AccountingPositionDailyService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingPositionDailyServiceImpl implements AccountingPositionDailyService {

	private AdvancedUpdatableDAO<AccountingPositionDaily, Criteria> accountingPositionDailyDAO;

	private AccountingValuationService accountingValuationService;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingPositionHandler accountingPositionHandler;

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	private InvestmentAccountService investmentAccountService;
	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SecurityAuthorizationService securityAuthorizationService;

	private RunnerHandler runnerHandler;
	private SqlHandler sqlHandler;
	private SynchronizationHandler synchronizationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionDaily> getAccountingPositionDailyList(final AccountingPositionDailySearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.isIncludeOpenOnly()) {
					criteria.add(Restrictions.gt("remainingQuantity", BigDecimal.ZERO));
				}
				if (searchForm.getHoldingInvestmentAccountMainPurposeActiveOnDate() != null) {
					String pathAlias = getPathAlias("accountingTransaction.holdingInvestmentAccount.mainRelationshipList", criteria);
					criteria.add(Restrictions.or(Restrictions.isNull(pathAlias + ".startDate"),
							Restrictions.le(pathAlias + ".startDate", searchForm.getHoldingInvestmentAccountMainPurposeActiveOnDate())));
					criteria.add(Restrictions.or(Restrictions.isNull(pathAlias + ".endDate"), Restrictions.ge(pathAlias + ".endDate", searchForm.getHoldingInvestmentAccountMainPurposeActiveOnDate())));
				}
			}
		};
		List<AccountingPositionDaily> positionDailies = getAccountingPositionDailyDAO().findBySearchCriteria(searchConfigurer);
		if (searchForm.isIncludeUnderlyingPrice()) {
			// populate underlying prices
			CollectionUtils.getStream(positionDailies).forEach(position -> {
				InvestmentSecurity investmentSecurity = position.getInvestmentSecurity();
				if (investmentSecurity != null && investmentSecurity.getUnderlyingSecurity() != null) {
					position.setUnderlyingPrice(getMarketDataRetriever().getPriceFlexible(investmentSecurity.getUnderlyingSecurity(), position.getPositionDate(), false));
				}
			});
		}
		return positionDailies;
	}


	@Override
	@Transactional(readOnly = true)
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveList(AccountingPositionDailyLiveSearchForm liveSearchForm) {
		for (SearchRestriction restriction : CollectionUtils.getIterable(liveSearchForm.getRestrictionList())) {
			if (restriction.getComparison() == ComparisonConditions.EQUALS) {
				BeanUtils.setPropertyValue(liveSearchForm, restriction.getField(), restriction.getValue(), true);
			}
		}

		liveSearchForm.validate();

		SnapshotRebuildContext cache = new SnapshotRebuildContext(getMarketDataExchangeRatesApiService());
		List<AccountingPositionDaily> result = generateAccountingPositionDailyList(liveSearchForm.getClientAccountId(), liveSearchForm.getClientAccountIds(), liveSearchForm.getHoldingAccountId(), liveSearchForm.getHoldingAccountIds(), liveSearchForm.getHoldingCompanyId(), liveSearchForm.getClientAccountGroupId(), liveSearchForm.getHoldingAccountGroupId(),
				liveSearchForm.getInvestmentSecurityId(), liveSearchForm.getInvestmentGroupId(), liveSearchForm.getSnapshotDate(), liveSearchForm.isUseSettlementDate(), liveSearchForm.getRealizedAdjustmentDate(), false, liveSearchForm.isUseNextDayIndexRatio(), liveSearchForm.isIncludeUnsettledLegPayments(), cache);

		// additional filters
		if (liveSearchForm.getInvestmentInstrumentCurrencyDenominationId() != null || liveSearchForm.getCollateralAccountingAccount() != null) {
			Stream<AccountingPositionDaily> accountingPositionDailyStream = CollectionUtils.getStream(result);
			if (liveSearchForm.getInvestmentInstrumentCurrencyDenominationId() != null) {
				accountingPositionDailyStream = accountingPositionDailyStream.filter(accountingPositionDaily -> {
					Integer instrumentCurrencyDenominationId = accountingPositionDaily.getInvestmentSecurity() != null && accountingPositionDaily.getInvestmentSecurity().getInstrument().getTradingCurrency() != null
							? accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument().getTradingCurrency().getId()
							: null;
					return liveSearchForm.getInvestmentInstrumentCurrencyDenominationId().equals(instrumentCurrencyDenominationId);
				});
			}
			if (liveSearchForm.getCollateralAccountingAccount() != null) {
				accountingPositionDailyStream = accountingPositionDailyStream.filter(accountingPositionDaily -> {
					Boolean collateralAccountingAccount = accountingPositionDaily.getAccountingTransaction() == null ? null : accountingPositionDaily.getAccountingTransaction().getAccountingAccount().isCollateral();
					return liveSearchForm.getCollateralAccountingAccount().equals(collateralAccountingAccount);
				});
			}
			result = accountingPositionDailyStream.collect(Collectors.toList());
		}

		return result;
	}


	@Override
	public List<AccountingPositionDaily> getAccountingPositionDailyLiveForCurrencyList(AccountingPositionDailyLiveSearchForm liveSearchForm) {
		liveSearchForm.validate();

		// start with current currency balances
		AccountingBalanceSearchForm balanceSearchForm = new AccountingBalanceSearchForm();
		balanceSearchForm.setClientInvestmentAccountId(liveSearchForm.getClientAccountId());
		balanceSearchForm.setClientInvestmentAccountIds(liveSearchForm.getClientAccountIds());
		balanceSearchForm.setHoldingInvestmentAccountId(liveSearchForm.getHoldingAccountId());
		balanceSearchForm.setHoldingInvestmentAccountIds(liveSearchForm.getHoldingAccountIds());
		balanceSearchForm.setHoldingAccountIssuingCompanyId(liveSearchForm.getHoldingCompanyId());
		balanceSearchForm.setInvestmentSecurityId(liveSearchForm.getInvestmentSecurityId());
		balanceSearchForm.setInvestmentGroupId(liveSearchForm.getInvestmentGroupId());
		balanceSearchForm.setClientInvestmentAccountGroupId(liveSearchForm.getClientAccountGroupId());
		balanceSearchForm.setHoldingInvestmentAccountGroupId(liveSearchForm.getHoldingAccountGroupId());
		balanceSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.getCurrencyNonCashWithCollateralOption(liveSearchForm.getCollateralAccountingAccount()));

		// prior day info is used on today's snapshot too
		// set up a start date as the previous weekday from the earliest date we are looking for
		Date current = liveSearchForm.getFromSnapshotDate() != null ? liveSearchForm.getFromSnapshotDate() : liveSearchForm.getSnapshotDate();
		Date endDate = liveSearchForm.getSnapshotDate();

		// Final List of Results
		List<AccountingPositionDaily> result = new ArrayList<>();

		// 1. get prior currency balances
		Date priorDate = DateUtils.getPreviousWeekday(current);
		balanceSearchForm.setRestrictionList(null); // clear previous filter configuration
		if (liveSearchForm.isUseSettlementDate()) {
			balanceSearchForm.setSettlementDate(priorDate);
		}
		else {
			balanceSearchForm.setTransactionDate(priorDate);
		}
		// prior day local balance for currency may be 0, but base balance that affects OTE (Currency Translation Gain/Loss) might be present
		balanceSearchForm.setIncludeZeroLocalBalances(true);
		List<AccountingBalanceValue> priorBalanceList = getAccountingValuationService().getAccountingBalanceValueList(balanceSearchForm);
		balanceSearchForm.setIncludeZeroLocalBalances(false);
		Map<String, AccountingBalanceValue> priorBalanceMap = new HashMap<>();
		for (AccountingBalanceValue balance : CollectionUtils.getIterable(priorBalanceList)) {
			priorBalanceMap.put(AccountingUtils.createAccountingObjectNaturalKey(balance), balance);
		}

		// 2. get currency transactions: need to adjust for non-end of day FX opening/closing trades (spot currency trades)
		AccountingTransactionSearchForm tranSearchForm = new AccountingTransactionSearchForm();
		tranSearchForm.setClientInvestmentAccountId(liveSearchForm.getClientAccountId());
		tranSearchForm.setClientInvestmentAccountIds(liveSearchForm.getClientAccountIds());
		tranSearchForm.setHoldingInvestmentAccountId(liveSearchForm.getHoldingAccountId());
		tranSearchForm.setHoldingInvestmentAccountIds(liveSearchForm.getHoldingAccountIds());
		tranSearchForm.setHoldingAccountIssuingCompanyId(liveSearchForm.getHoldingCompanyId());
		tranSearchForm.setInvestmentSecurityId(liveSearchForm.getInvestmentSecurityId());
		tranSearchForm.setInvestmentGroupId(liveSearchForm.getInvestmentGroupId());
		tranSearchForm.setClientInvestmentAccountGroupId(liveSearchForm.getClientAccountGroupId());
		tranSearchForm.setHoldingInvestmentAccountGroupId(liveSearchForm.getHoldingAccountGroupId());
		tranSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.getCurrencyNonCashWithCollateralOption(liveSearchForm.getCollateralAccountingAccount()));

		// As long as current day is <= endDate, keep going (usually run only for one day)
		while (DateUtils.compare(current, endDate, false) <= 0) {
			balanceSearchForm.setRestrictionList(null); // clear previous filter configuration
			tranSearchForm.setRestrictionList(null); // clear previous filter configuration
			if (liveSearchForm.isUseSettlementDate()) {
				balanceSearchForm.setSettlementDate(current);
				tranSearchForm.setSettlementDate(current);
			}
			else {
				balanceSearchForm.setTransactionDate(current);
				tranSearchForm.setTransactionDate(current);
			}

			// 3. Get current day's balances
			List<AccountingBalanceValue> currentBalanceList = getAccountingValuationService().getAccountingBalanceValueList(balanceSearchForm);

			// 4. get current day's currency transactions
			List<AccountingTransaction> currencyTransactions = getAccountingTransactionService().getAccountingTransactionList(tranSearchForm);
			Map<String, List<AccountingTransaction>> currencyTransactionsMap = new HashMap<>();
			for (AccountingTransaction tran : CollectionUtils.getIterable(currencyTransactions)) {
				String key = AccountingUtils.createAccountingObjectNaturalKey(tran);
				List<AccountingTransaction> existing = currencyTransactionsMap.computeIfAbsent(key, k -> new ArrayList<>());
				existing.add(tran);
			}

			// 5. create resulting position for each current balance item (include prior day info as well as current transactions)
			for (AccountingBalanceValue currentBalance : CollectionUtils.getIterable(currentBalanceList)) {
				AccountingTransaction transaction = new AccountingTransaction();
				transaction.setClientInvestmentAccount(currentBalance.getClientInvestmentAccount());
				transaction.setHoldingInvestmentAccount(currentBalance.getHoldingInvestmentAccount());
				transaction.setInvestmentSecurity(currentBalance.getInvestmentSecurity());
				transaction.setAccountingAccount(currentBalance.getAccountingAccount());
				AccountingPositionDaily position = AccountingPositionDaily.ofAccountingTransaction(transaction);
				position.setPositionDate(current);

				position.setMarketFxRate(currentBalance.getExchangeRateToBase());
				position.setNotionalValueLocal(currentBalance.getLocalNotional());
				position.setNotionalValueBase(currentBalance.getBaseNotional());

				String key = AccountingUtils.createAccountingObjectNaturalKey(currentBalance);
				AccountingBalanceValue priorBalance = priorBalanceMap.get(key);
				if (priorBalance != null) {
					position.setPriorFxRate(priorBalance.getExchangeRateToBase());
					position.setPriorNotionalValueLocal(priorBalance.getLocalNotional());
					position.setPriorNotionalValueBase(priorBalance.getBaseNotional());
					// no local gain on currency but can have a gain/loss in base currency form change in FX rate
					position.setPriorOpenTradeEquityBase(position.getPriorNotionalValueBase().subtract(priorBalance.getBaseCost()));
				}

				// no local gain on currency but can have a gain/loss in base currency form change in FX rate
				position.setOpenTradeEquityBase(position.getNotionalValueBase().subtract(currentBalance.getBaseCost()));
				position.setDailyGainLossBase(MathUtils.subtract(position.getOpenTradeEquityBase(), position.getPriorOpenTradeEquityBase()));

				// add gain/loss previous day spot trades at mid day FX
				List<AccountingTransaction> tranList = currencyTransactionsMap.remove(key);
				if (tranList != null && priorBalance != null) {
					for (AccountingTransaction tran : tranList) {
						if (!tran.getExchangeRateToBase().equals(priorBalance.getExchangeRateToBase())) {
							BigDecimal adjustment = MathUtils.subtract(
									MathUtils.multiply(tran.getLocalDebitCredit(), priorBalance.getExchangeRateToBase(), 2),
									MathUtils.multiply(tran.getLocalDebitCredit(), tran.getExchangeRateToBase(), 2));
							if (!MathUtils.isNullOrZero(adjustment)) {
								// NOTE: do we need something here? waiting until this case happens so that I can add new integration tests
								//position.setTodayRealizedGainLossBase(MathUtils.add(position.getTodayRealizedGainLossBase(), adjustment));
								//position.setDailyGainLossBase(position.getTodayRealizedGainLossBase());
							}
						}
					}
				}

				result.add(position.updateCalculations());
			}

			// 6. add today's realized for positions that were fully closed on previous day
			for (Map.Entry<String, List<AccountingTransaction>> currencyTransactionEntry : currencyTransactionsMap.entrySet()) {
				List<AccountingTransaction> tranList = currencyTransactionEntry.getValue();
				AccountingBalanceValue prior = priorBalanceMap.get(currencyTransactionEntry.getKey());
				if (prior != null) {
					AccountingPositionDaily position = null;

					for (AccountingTransaction tran : tranList) {
						if (!tran.getExchangeRateToBase().equals(prior.getExchangeRateToBase())) {
							BigDecimal adjustment = MathUtils.subtract(MathUtils.multiply(tran.getLocalDebitCredit(), prior.getExchangeRateToBase(), 2),
									MathUtils.multiply(tran.getLocalDebitCredit(), tran.getExchangeRateToBase(), 2));
							if (!MathUtils.isNullOrZero(adjustment)) {
								if (position == null) {
									position = AccountingPositionDaily.ofAccountingTransaction(tran);
									position.setPositionDate(current);
								}
								position.setTodayRealizedGainLossBase(MathUtils.add(position.getTodayRealizedGainLossBase(), adjustment));
								position.setDailyGainLossBase(position.getTodayRealizedGainLossBase());
							}
						}
					}
					if (position != null) {
						result.add(position.updateCalculations());
					}
				}
			}

			// Move Date Forward and reset "priorMap" to use current
			current = DateUtils.getNextWeekday(current);
			priorBalanceMap = new HashMap<>();
			for (AccountingBalanceValue balance : CollectionUtils.getIterable(currentBalanceList)) {
				priorBalanceMap.put(AccountingUtils.createAccountingObjectNaturalKey(balance), balance);
			}
		}

		// 7. additional filters
		if (liveSearchForm.getInvestmentInstrumentCurrencyDenominationId() != null) {
			result = BeanUtils.filter(result, accountingPositionDaily -> (accountingPositionDaily.getInvestmentSecurity() != null && accountingPositionDaily.getInvestmentSecurity().getInstrument().getTradingCurrency() != null ? accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument().getTradingCurrency().getId() : null), liveSearchForm.getInvestmentInstrumentCurrencyDenominationId());
		}

		return result;
	}


	@Override
	public int deleteAccountingPositionDailyByAccountingTransaction(long accountingTransactionId) {
		List<AccountingPositionDaily> deleteList = getAccountingPositionDailyDAO().findByField("accountingTransaction.id", accountingTransactionId);
		int count = CollectionUtils.getSize(deleteList);
		if (count > 0) {
			getAccountingPositionDailyDAO().deleteList(deleteList);
		}
		return count;
	}


	@Override
	public Status rebuildAccountingPositionDailyForDate(DateGenerationOptions dateGenerationOptions, Integer dayCount) {
		Date dateToUse = getCalendarDateGenerationHandler().generateDate(dateGenerationOptions, dayCount);
		AccountingPositionDailyRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setSnapshotDate(dateToUse);
		command.setSynchronous(true);
		command.setAllowTodaySnapshots(true);
		command.setAllowFutureSnapshots(true);
		return rebuildAccountingPositionDaily(command);
	}


	@Override
	public Status rebuildAccountingPositionDaily(final AccountingPositionDailyRebuildCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage("Synchronously running for " + command);

			getSynchronizationHandler().execute(SynchronizableBuilder.forLocking("APD-REBUILD", status.getMessage(), command.getRunId())
					.buildWithExecutableAction(() -> doRebuildAccountingPositionDaily(command, status)));

			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("POSITION-DAILY", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doRebuildAccountingPositionDaily(command, getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error rebuilding snapshots for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding daily position for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started daily positions rebuild: " + runId + ". Processing will be completed shortly.");
	}


	private void doRebuildAccountingPositionDaily(AccountingPositionDailyRebuildCommand command, Status status) {
		// Validates Start/End Snapshot Dates and If Rebuilding Today or in the Future and global rebuild limites
		command.validate(getCalendarBusinessDayService(), getSecurityAuthorizationService());

		// cache looked up market data for improved performance
		SnapshotRebuildContext cache = new SnapshotRebuildContext(getMarketDataExchangeRatesApiService());

		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		boolean ourAccount = true;
		if (command.getClientAccountId() != null) {
			searchForm.setId(command.getClientAccountId());
		}
		else if (command.getInvestmentAccountGroupId() != null) {
			searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
		}
		else if (command.getHoldingAccountId() != null) {
			searchForm.setId(command.getHoldingAccountId());
			ourAccount = false;
		}
		else if (command.getHoldingCompanyId() != null || command.getHoldingAccountTypeId() != null) {
			searchForm.setAccountTypeId(command.getHoldingAccountTypeId());
			searchForm.setIssuingCompanyId(command.getHoldingCompanyId());
			ourAccount = false;
		}
		else if (command.getInvestmentSecurityId() != null) {
			ValidationUtils.assertTrue(command.getStartSnapshotDate().equals(command.getEndSnapshotDate()), "Security specific rebuilds can only be done for one day at a time.");
			List<AccountingPositionDaily> snapshotList = generateAccountingPositionDailyList(null, null, null, null, null, null, null, command.getInvestmentSecurityId(), null, command.getStartSnapshotDate(),
					command.isUseSettlementDate(), null, true, command.isUseNextDayIndexRatio(), false, cache);
			updateSnapshots(null, null, command.getInvestmentSecurityId(), command.getStartSnapshotDate(), snapshotList);
			status.setMessage("Processing Complete.  All clients for one security: " + command.getInvestmentSecurityId() + " and date range " + DateUtils.fromDateShort(command.getStartSnapshotDate()) + " - "
					+ command.getEndSnapshotDate());
			return;
		}
		searchForm.setOurAccount(ourAccount);
		searchForm.setOrderBy("number"); // sort by account number so that it's easy to see where processing is
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

		int totalAccounts = CollectionUtils.getSize(investmentAccountList);
		int processedSnapshots = 0;
		int errorCount = 0;
		Date snapshotDate = command.getStartSnapshotDate();
		// loop through days first because we're caching prices/fx rates by day: must to be cleared for next day
		while (DateUtils.compare(snapshotDate, command.getEndSnapshotDate(), false) <= 0) {
			for (InvestmentAccount investmentAccount : CollectionUtils.getIterable(investmentAccountList)) {
				try {
					Integer clientAccountId = ourAccount ? investmentAccount.getId() : null;
					Integer holdingAccountId = ourAccount ? null : investmentAccount.getId();
					List<AccountingPositionDaily> snapshotList = generateAccountingPositionDailyList(clientAccountId, null, holdingAccountId, null, null, null, null, command.getInvestmentSecurityId(),
							command.getInvestmentGroupId(), snapshotDate, command.isUseSettlementDate(), null, true, command.isUseNextDayIndexRatio(), false, cache);
					updateSnapshots(clientAccountId, holdingAccountId, command.getInvestmentSecurityId(), snapshotDate, snapshotList);
					processedSnapshots++;
				}
				catch (Throwable e) {
					status.addError(investmentAccount + ": " + ExceptionUtils.getDetailedMessage(e));
					LogUtils.error(getClass(), "Failed to rebuild " + investmentAccount.getLabel(), e);
					errorCount++;
				}
				status.setMessage("Accounts: " + totalAccounts + "; Processed: " + processedSnapshots + " snapshots; Failed: " + errorCount + " snapshots; Last: "
						+ investmentAccount.getNumber() + " on " + DateUtils.fromDateShort(snapshotDate));
			}

			snapshotDate = cache.moveToNextWeekDate(snapshotDate);
		}

		// Used for historical rebuild job so we can track how many snapshots were actually rebuilt
		command.setResultProcessedSnapshots(command.getResultProcessedSnapshots() + processedSnapshots);
		if (errorCount > 0) {
			status.setMessage("Completed with Errors. Accounts: " + totalAccounts + "; Snapshots: " + processedSnapshots + "; Errors: " + errorCount);
		}
		else {
			status.setMessage("Processing Complete. Total accounts: " + totalAccounts + "; Failed accounts: " + errorCount + "; Total client snapshots: " + processedSnapshots);
		}
	}


	/**
	 * Generates and returns a List of AccountingPositionDaily objects for the specified parameters. Does NOT save the list in the database.
	 */
	private List<AccountingPositionDaily> generateAccountingPositionDailyList(Integer clientAccountId, Integer[] clientAccountIds, Integer holdingAccountId, Integer[] holdingAccountIds, Integer holdingCompanyId, Integer clientAccountGroupId, Integer holdingAccountGroupId, Integer investmentSecurityId, Short investmentGroupId,
	                                                                          Date forSnapshotDate, boolean useSettlementDate, Date realizedAdjustmentDate, boolean populateAccountingTransaction, boolean useNextDayIndexRatio, boolean includeUnsettledLegPayments, SnapshotRebuildContext cache) {

		// prior day info is saved on today's snapshot too
		final Date snapshotDate = DateUtils.clearTime(forSnapshotDate);
		Date priorDate = DateUtils.getPreviousWeekday(snapshotDate);
		List<AccountingPosition> priorList = getAccountingPositionList(clientAccountId, clientAccountIds, holdingAccountId, holdingAccountIds, holdingCompanyId, clientAccountGroupId, holdingAccountGroupId, investmentSecurityId, investmentGroupId, priorDate, useSettlementDate, includeUnsettledLegPayments);

		Map<Long, AccountingPosition> priorMap = BeanUtils.getBeanMap(priorList, AccountingPosition::getId);
		Map<Long, IncomeStatementData> incomeStatementMap = getIncomeStatementData(clientAccountId, clientAccountIds, holdingAccountId, holdingAccountIds, holdingCompanyId, clientAccountGroupId, holdingAccountGroupId, investmentSecurityId, investmentGroupId, snapshotDate, useSettlementDate, realizedAdjustmentDate);

		List<AccountingPositionDaily> snapshotList = new ArrayList<>();
		// add today's positions
		List<AccountingPosition> positionList = getAccountingPositionList(clientAccountId, clientAccountIds, holdingAccountId, holdingAccountIds, holdingCompanyId, clientAccountGroupId, holdingAccountGroupId, investmentSecurityId, investmentGroupId, snapshotDate, useSettlementDate, includeUnsettledLegPayments);

		Map<Long, AccountingTransaction> tranMap = null;
		if (populateAccountingTransaction) {
			tranMap = getPositionTransactions(priorList, null);
			tranMap = getPositionTransactions(positionList, tranMap);
		}

		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			try {
				InvestmentSecurity security = position.getInvestmentSecurity();
				InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

				AccountingPositionDaily snapshot = AccountingPositionDaily.ofAccountingTransaction(tranMap != null ? tranMap.get(position.getId()) : position.getOpeningTransaction());
				snapshot.setPositionDate(snapshotDate);
				snapshot.setRemainingQuantity(position.getRemainingQuantity());

				// TIPS use quantity (face) adjusted by index ratio; default to snapshot date (convention used by most banks)
				BigDecimal notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, snapshotDate, position.getSettlementDate(), useNextDayIndexRatio);

				snapshot.setMarketFxRate(getExchangeRate(position, snapshotDate, cache));
				snapshot.setMarketPrice(getPrice(security, snapshotDate, position.getPrice(), true, cache));
				snapshot.setRemainingCostBasisLocal(position.getRemainingCostBasis());
				snapshot.setRemainingCostBasisBase(MathUtils.multiply(snapshot.getRemainingCostBasisLocal(), position.getExchangeRateToBase(), 2));
				snapshot.setNotionalValueLocal(getInvestmentCalculator().calculateNotional(security, snapshot.getMarketPrice(), snapshot.getRemainingQuantity(), notionalMultiplier));
				snapshot.setNotionalValueBase(MathUtils.multiply(snapshot.getNotionalValueLocal(), snapshot.getMarketFxRate(), 2));

				if (InvestmentUtils.isAccrualSupported(security)) {
					BigDecimal remainingQuantity = snapshot.getRemainingQuantity() == null ? BigDecimal.ZERO : snapshot.getRemainingQuantity();
					AccrualBasis accrualBasis = AccrualBasis.forHierarchy(hierarchy, snapshot.getRemainingCostBasisLocal(), remainingQuantity);
					AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(position, snapshotDate));
					snapshot.setAccrual1Local(getInvestmentCalculator().calculateAccruedInterestForEvent1(security, accrualBasis.getAccrualBasis1(), notionalMultiplier, accrualDates.getAccrueUpToDate1(), accrualDates.getAccrueFromDate1()));
					snapshot.setAccrual2Local(getInvestmentCalculator().calculateAccruedInterestForEvent2(security, accrualBasis.getAccrualBasis2(), notionalMultiplier, accrualDates.getAccrueUpToDate2(), accrualDates.getAccrueFromDate2()));

					if (security.getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
						Map<String, BigDecimal> localReceivablesMap = getAccountingPositionHandler().getAccountingPositionReceivableMap(clientAccountIds, security, snapshotDate, useSettlementDate);

						Optional<String> securityEventType1 = Optional.of(security).map(InvestmentSecurity::getInstrument).map(InvestmentInstrument::getHierarchy)
								.map(InvestmentInstrumentHierarchy::getAccrualSecurityEventType).map(InvestmentSecurityEventType::getName);
						Optional<String> securityEventType2 = Optional.of(security).map(InvestmentSecurity::getInstrument).map(InvestmentInstrument::getHierarchy)
								.map(InvestmentInstrumentHierarchy::getAccrualSecurityEventType2).map(InvestmentSecurityEventType::getName);

						securityEventType1.ifPresent(s -> snapshot.setAccrual1PaymentLocal(localReceivablesMap.getOrDefault(s, BigDecimal.ZERO)));
						securityEventType2.ifPresent(s -> snapshot.setAccrual2PaymentLocal(localReceivablesMap.getOrDefault(s, BigDecimal.ZERO)));
						BigDecimal equityLegPaymentLocal = localReceivablesMap.getOrDefault(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT, BigDecimal.ZERO);
						equityLegPaymentLocal = MathUtils.add(equityLegPaymentLocal, localReceivablesMap.getOrDefault(AccountingJournalType.TRADE_JOURNAL, BigDecimal.ZERO));
						snapshot.setEquityLegPaymentLocal(equityLegPaymentLocal);
					}
				}

				// OTE includes unrealized gain/loss + accrued interest
				snapshot.setOpenTradeEquityLocal(snapshot.getNotionalValueLocal().subtract(snapshot.getRemainingCostBasisLocal()));
				snapshot.setOpenTradeEquityLocal(MathUtils.add(snapshot.getOpenTradeEquityLocal(), MathUtils.add(snapshot.getAccrual1Local(), snapshot.getAccrual2Local())));

				// because of rounding sometimes Base OTE will not equal (Base Market Value - Base Cost Basis).  Can be off by a penny.
				if (InvestmentUtils.isNoPaymentOnOpen(security)) {
					snapshot.setOpenTradeEquityBase(MathUtils.multiply(snapshot.getOpenTradeEquityLocal(), snapshot.getMarketFxRate(), 2));
				}
				else {
					// when Cost Basis = Cost, then OTE = Notional - Cost Basis + Accrual
					// For these we want to include CCY Translation, applies to cases like Funds, Stocks, some Swaps, but not Futures
					// Example: Account: 052100 Security: XBAU  Date: 2/1 Base OTE Should be -141,281.15
					snapshot.setOpenTradeEquityBase(snapshot.getNotionalValueBase().subtract(snapshot.getRemainingCostBasisBase()));
					snapshot.setOpenTradeEquityBase(MathUtils.add(snapshot.getOpenTradeEquityBase(), MathUtils.add(snapshot.getAccrual1Base(), snapshot.getAccrual2Base())));
				}

				AccountingPosition prior = priorMap.remove(position.getId()); // what's left needs to be added as positions that were closed yesterday
				boolean activeSecurity = DateUtils.isDateBetween(priorDate, security.getStartDate(), security.getLastPositionDate(), false);
				if (activeSecurity) {
					// if position was open on the snapshot day, then it's ok to have null price for prior day (the system might now have it)
					boolean exceptionIfMissing = (prior != null);
					snapshot.setPriorPrice(getPriorPrice(security, priorDate, prior == null ? null : prior.getPrice(), exceptionIfMissing, cache));
				}
				else if (DateUtils.isDateBefore(priorDate, security.getStartDate(), false)) {
					// if prior date before security start - still try to look up the price (can be price before issued), otherwise will use the opening price
					// Bond positions can be opened before issue date
					snapshot.setPriorPrice(getPriorPrice(security, priorDate, prior == null ? null : prior.getPrice(), false, cache));
				}
				else {
					// security was not active on prior date
					snapshot.setPriorPrice(null);
				}
				snapshot.setPriorFxRate(getExchangeRate(position, priorDate, cache));
				BigDecimal priorAccruedInterestLocal = BigDecimal.ZERO;
				if (prior == null || (!activeSecurity && prior.getPrice() == null)) {
					snapshot.setPriorQuantity(BigDecimal.ZERO);
					snapshot.setPriorNotionalValueBase(BigDecimal.ZERO);
					snapshot.setPriorNotionalValueLocal(BigDecimal.ZERO);
					snapshot.setPriorOpenTradeEquityBase(BigDecimal.ZERO);
					snapshot.setPriorOpenTradeEquityLocal(BigDecimal.ZERO);
					if (prior != null) {
						// no reason to log at WARN level because this is also caught by corresponding notification
						LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "AccountingPositionDaily rebuild: clientAccountId = " + clientAccountId + ", clientAccountIds = " + Arrays.toString(clientAccountIds) + " has position for inactive security " + security.getLabel() + " on " + DateUtils.fromDateShort(priorDate)));
					}
				}
				else {
					ValidationUtils.assertNotNull(prior.getSettlementDate(), "Settlement Date is required.");
					// TIPS use quantity (face) adjusted by index ratio; default to prior snapshot date (convention used by most banks)
					BigDecimal priorNotionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, priorDate, prior.getSettlementDate(), false);

					snapshot.setPriorQuantity(prior.getRemainingQuantity());
					snapshot.setPriorNotionalValueLocal(getInvestmentCalculator().calculateNotional(security, snapshot.getPriorPrice(), snapshot.getPriorQuantity(), priorNotionalMultiplier));
					snapshot.setPriorNotionalValueBase(MathUtils.multiply(snapshot.getPriorNotionalValueLocal(), snapshot.getPriorFxRate(), 2));

					if (InvestmentUtils.isAccrualSupported(security)) {
						if (MathUtils.isNullOrZero(snapshot.getPriorQuantity())) {
							snapshot.setPriorQuantity(BigDecimal.ZERO);
						}
						if (MathUtils.isNullOrZero(snapshot.getRemainingQuantity())) {
							snapshot.setRemainingQuantity(BigDecimal.ZERO);
						}
						AccrualBasis accrualBasis = AccrualBasis.forHierarchy(hierarchy, snapshot.getPriorRemainingCostBasisLocal(), snapshot.getPriorQuantity());
						AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(prior, priorDate));
						priorAccruedInterestLocal = getInvestmentCalculator().calculateAccruedInterest(security, accrualBasis, priorNotionalMultiplier, accrualDates);
					}

					// OTE includes unrealized gain/loss + accrued interest
					snapshot.setPriorOpenTradeEquityLocal(snapshot.getPriorNotionalValueLocal().subtract(prior.getRemainingCostBasis()));
					snapshot.setPriorOpenTradeEquityLocal(snapshot.getPriorOpenTradeEquityLocal().add(priorAccruedInterestLocal));

					// because of rounding sometimes Base OTE will not equal (Base Market Value - Base Cost Basis).  Can be off by a penny.
					if (InvestmentUtils.isNoPaymentOnOpen(security)) {
						snapshot.setPriorOpenTradeEquityBase(MathUtils.multiply(snapshot.getPriorOpenTradeEquityLocal(), snapshot.getPriorFxRate(), 2));
					}
					else {
						// when Cost Basis = Cost, then OTE = Notional - Cost Basis + Accrual
						// For these we want to include CCY Translation, applies to cases like Funds, Stocks, some Swaps, but not Futures
						// Example: Account: 052100 Security: XBAU  Date: 2/1 Base OTE Should be -141,281.15
						snapshot.setPriorOpenTradeEquityBase(snapshot.getPriorNotionalValueBase().subtract(snapshot.getPriorRemainingCostBasisBase()));
						snapshot.setPriorOpenTradeEquityBase(snapshot.getPriorOpenTradeEquityBase().add(snapshot.getPriorAccrualBase()));
					}
				}

				// OpenTradeEquity - PriorOpenTradeEquity +  IncomeStatementData.DailyGainLoss (OTE values already include accrued interest)
				BigDecimal dailyLocal = BigDecimal.ZERO;
				BigDecimal dailyBase = BigDecimal.ZERO;

				// set realized and commissions
				IncomeStatementData data = incomeStatementMap.remove(position.getId());
				if (data != null) {
					snapshot.setTodayClosedQuantity(data.quantityClosed);
					snapshot.setTodayCommissionLocal(data.commissionLocal);
					snapshot.setTodayCommissionBase(data.commissionBase);
					snapshot.setTodayRealizedGainLossLocal(data.realizedLocal);
					snapshot.setTodayRealizedGainLossBase(data.realizedBase);
					snapshot.setRealizedGainLossAdjustmentLocal(data.realizedAdjustmentLocal);
					snapshot.setRealizedGainLossAdjustmentBase(data.realizedAdjustmentBase);

					dailyLocal = data.gainLossLocal; //MathUtils.add(data.realizedLocal, data.gainLossLocal);
					dailyBase = data.gainLossBase; //MathUtils.add(data.realizedBase, data.gainLossBase);
				}

				// OpenTradeEquity - PriorOpenTradeEquity +  IncomeStatementData.DailyGainLoss (OTE values already include accrued interest)
				dailyLocal = MathUtils.add(dailyLocal, snapshot.getOpenTradeEquityLocal());
				dailyLocal = MathUtils.subtract(dailyLocal, snapshot.getPriorOpenTradeEquityLocal());
				snapshot.setDailyGainLossLocal(dailyLocal);

				dailyBase = MathUtils.add(dailyBase, snapshot.getOpenTradeEquityBase());
				dailyBase = MathUtils.subtract(dailyBase, snapshot.getPriorOpenTradeEquityBase());
				snapshot.setDailyGainLossBase(dailyBase);

				snapshotList.add(snapshot.updateCalculations());
			}
			catch (Throwable e) {
				throw new RuntimeException("Error processing clientAccountId = " + clientAccountId + ", clientAccountIds = " + Arrays.toString(clientAccountIds) + ", securityId = " + investmentSecurityId + ", snapshotDate = " + snapshotDate + ", position: "
						+ position, e);
			}
		}

		// add realized and commission for positions that are no longer open
		for (Map.Entry<Long, IncomeStatementData> incomeStatementEntry : incomeStatementMap.entrySet()) {
			IncomeStatementData data = incomeStatementEntry.getValue();
			AccountingTransaction t = data.openingTransaction;
			AccountingPositionDaily snapshot = AccountingPositionDaily.ofAccountingTransaction(t);
			snapshot.setPositionDate(snapshotDate);

			InvestmentSecurity security = t.getInvestmentSecurity();
			boolean currency = security.isCurrency();
			snapshot.setMarketFxRate(getExchangeRate(t, snapshotDate, cache));
			snapshot.setMarketPrice(currency ? null : getPrice(security, snapshotDate, t.getPrice(), true, cache));

			snapshot.setTodayClosedQuantity(data.quantityClosed);
			snapshot.setTodayCommissionLocal(data.commissionLocal);
			snapshot.setTodayCommissionBase(data.commissionBase);
			snapshot.setTodayRealizedGainLossLocal(data.realizedLocal);
			snapshot.setTodayRealizedGainLossBase(data.realizedBase);
			snapshot.setRealizedGainLossAdjustmentLocal(data.realizedAdjustmentLocal);
			snapshot.setRealizedGainLossAdjustmentBase(data.realizedAdjustmentBase);

			AccountingPosition prior = priorMap.remove(t.getId());
			boolean activeSecurity = DateUtils.isDateBetween(priorDate, security.getStartDate(), security.getLastPositionDate(), false);
			if (activeSecurity) {
				// if position was open on the snapshot day, then it's ok to have null price for prior day (the system might now have it)
				boolean exceptionIfMissing = (prior != null);
				snapshot.setPriorPrice(currency ? null : getPriorPrice(security, priorDate, prior == null ? null : prior.getPrice(), exceptionIfMissing, cache));
			}
			else {
				// security was not active on prior date
				snapshot.setPriorPrice(null);
			}
			snapshot.setPriorFxRate(getExchangeRate(t, priorDate, cache));
			if (prior != null) {
				// TIPS use quantity (face) adjusted by index ratio:  default to prior snapshot date (convention used by most banks)
				BigDecimal priorNotionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, priorDate, prior.getSettlementDate(), false);
				snapshot.setPriorQuantity(currency ? null : prior.getRemainingQuantity());
				snapshot.setPriorNotionalValueLocal(currency ? prior.getRemainingCostBasis() : getInvestmentCalculator().calculateNotional(security, snapshot.getPriorPrice(),
						snapshot.getPriorQuantity(), priorNotionalMultiplier));
				snapshot.setPriorNotionalValueBase(MathUtils.multiply(snapshot.getPriorNotionalValueLocal(), snapshot.getPriorFxRate(), 2));

				InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
				AccrualBasis accrualBasis = AccrualBasis.forHierarchy(hierarchy, snapshot.getPriorRemainingCostBasisLocal(), snapshot.getPriorQuantity());
				AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(prior, priorDate));
				BigDecimal priorAccruedInterestLocal = getInvestmentCalculator().calculateAccruedInterest(security, accrualBasis, priorNotionalMultiplier, accrualDates);

				// OTE includes unrealized gain/loss + accrued interest
				snapshot.setPriorOpenTradeEquityLocal(snapshot.getPriorNotionalValueLocal().subtract(prior.getRemainingCostBasis()));
				snapshot.setPriorOpenTradeEquityLocal(snapshot.getPriorOpenTradeEquityLocal().add(priorAccruedInterestLocal));
				if (currency) {
					// no local gain on currency but can have a gain/loss in base currency form change in FX rate
					snapshot.setPriorOpenTradeEquityBase(snapshot.getPriorNotionalValueBase().subtract(MathUtils.multiply(prior.getRemainingCostBasis(), prior.getExchangeRateToBase(), 2)));
				}
				else {
					snapshot.setPriorOpenTradeEquityBase(MathUtils.multiply(snapshot.getPriorOpenTradeEquityLocal(), snapshot.getPriorFxRate(), 2));
				}
			}

			// OpenTradeEquity - PriorOpenTradeEquity +  IncomeStatementData.DailyGainLoss (OTE values already include accrued interest)
			BigDecimal dailyLocal = MathUtils.add(data.gainLossLocal, snapshot.getOpenTradeEquityLocal());
			dailyLocal = MathUtils.subtract(dailyLocal, snapshot.getPriorOpenTradeEquityLocal());
			snapshot.setDailyGainLossLocal(dailyLocal);

			BigDecimal dailyBase = MathUtils.add(data.gainLossBase, snapshot.getOpenTradeEquityBase());
			dailyBase = MathUtils.subtract(dailyBase, snapshot.getPriorOpenTradeEquityBase());
			snapshot.setDailyGainLossBase(dailyBase);

			snapshotList.add(snapshot.updateCalculations());
		}

		// add remaining prior positions that are no longer open and haven't been addressed before
		for (AccountingPosition prior : priorMap.values()) {
			AccountingPositionDaily snapshot = AccountingPositionDaily.ofAccountingTransaction(tranMap != null ? tranMap.get(prior.getId()) : prior.getOpeningTransaction());
			snapshot.setPositionDate(snapshotDate);

			InvestmentSecurity security = prior.getInvestmentSecurity();
			boolean currency = security.isCurrency();
			snapshot.setMarketFxRate(getExchangeRate(prior, snapshotDate, cache));
			snapshot.setMarketPrice(currency ? null : getPrice(security, snapshotDate, prior.getPrice(), true, cache));
			snapshot.setTodayClosedQuantity(prior.getRemainingQuantity());

			snapshot.setPriorQuantity(currency ? null : prior.getRemainingQuantity());
			snapshot.setPriorPrice(currency ? null : getPriorPrice(security, priorDate, prior.getPrice(), true, cache));
			snapshot.setPriorFxRate(getExchangeRate(prior, priorDate, cache));
			// TIPS use quantity (face) adjusted by index ratio; default to prior snapshot date (convention used by most banks)
			BigDecimal priorNotionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(security, priorDate, prior.getSettlementDate(), false);

			snapshot.setPriorNotionalValueLocal(currency ? prior.getRemainingCostBasis() : getInvestmentCalculator().calculateNotional(security, snapshot.getPriorPrice(), snapshot.getPriorQuantity(),
					priorNotionalMultiplier));
			snapshot.setPriorNotionalValueBase(MathUtils.multiply(snapshot.getPriorNotionalValueLocal(), snapshot.getPriorFxRate(), 2));
			snapshot.setPriorOpenTradeEquityLocal(snapshot.getPriorNotionalValueLocal().subtract(prior.getRemainingCostBasis()));
			if (currency) {
				// no local gain on currency but can have a gain/loss in base currency form change in FX rate
				snapshot.setPriorOpenTradeEquityBase(snapshot.getPriorNotionalValueBase().subtract(MathUtils.multiply(prior.getRemainingCostBasis(), prior.getExchangeRateToBase(), 2)));
			}
			else {
				snapshot.setPriorOpenTradeEquityBase(MathUtils.multiply(snapshot.getPriorOpenTradeEquityLocal(), snapshot.getPriorFxRate(), 2));
			}

			// no realized or commission (open and closed at same price). Current OTE = 0 so gain/loss is negated Prior OTE
			snapshot.setDailyGainLossLocal(snapshot.getPriorOpenTradeEquityLocal().negate());
			snapshot.setDailyGainLossBase(snapshot.getPriorOpenTradeEquityBase().negate());

			snapshotList.add(snapshot.updateCalculations());
		}

		return snapshotList;
	}


	private Map<Long, AccountingTransaction> getPositionTransactions(List<AccountingPosition> positionList, Map<Long, AccountingTransaction> tranMap) {
		Map<Long, AccountingTransaction> result = (tranMap == null) ? new HashMap<>() : tranMap;

		int positionCount = CollectionUtils.getSize(positionList);
		if (positionCount == 1) {
			AccountingPosition position = positionList.get(0);
			result.computeIfAbsent(position.getId(), k -> getAccountingTransactionService().getAccountingTransaction(position.getId()));
		}
		else if (positionCount > 1) {
			Long[] transactionIdsToFetch = positionList.stream()
					.map(AccountingPosition::getId)
					.filter(id -> !result.containsKey(id))
					.toArray(Long[]::new);
			List<AccountingTransaction> atList = getAccountingTransactionService().getAccountingTransactionListByIds(transactionIdsToFetch);
			for (AccountingTransaction tran : CollectionUtils.getIterable(atList)) {
				result.put(tran.getId(), tran);
			}
		}
		return result;
	}


	private void updateSnapshots(Integer clientAccountId, Integer holdingAccountId, Integer investmentSecurityId, Date snapshotDate, List<AccountingPositionDaily> snapshotList) {
		// delete existing data before inserting new
		getSqlHandler().executeUpdate(SqlUpdateCommand.forSql("DELETE p FROM AccountingPositionDaily p" +
						// avoid extra join, if possible, to optimize performance
						((holdingAccountId == null && investmentSecurityId == null) ? "" : " INNER JOIN AccountingTransaction t ON p.AccountingTransactionID = t.AccountingTransactionID"))
				.addDateRestriction(snapshotDate, "p.PositionDate = ?")
				.addIntegerRestriction(clientAccountId, "p.ClientInvestmentAccountID = ?")
				.addIntegerRestriction(holdingAccountId, "t.HoldingInvestmentAccountID = ?")
				.addIntegerRestriction(investmentSecurityId, "t.InvestmentSecurityID = ?")
		);

		// insert new data
		getAccountingPositionDailyDAO().saveList(snapshotList);
	}


	private List<AccountingPosition> getAccountingPositionList(Integer clientAccountId, Integer[] clientAccountIds, Integer holdingAccountId, Integer[] holdingAccountIds, Integer holdingCompanyId, Integer clientAccountGroupId, Integer holdingAccountGroupId,
	                                                           Integer investmentSecurityId, Short investmentGroupId, Date date, boolean useSettlementDate, boolean includeUnsettledLegPayments) {
		AccountingPositionCommand command = AccountingPositionCommand.onDate(useSettlementDate, date);
		command.setClientInvestmentAccountId(clientAccountId);
		command.setClientInvestmentAccountIds(clientAccountIds);
		command.setClientInvestmentAccountGroupId(clientAccountGroupId);
		command.setHoldingInvestmentAccountId(holdingAccountId);
		command.setHoldingInvestmentAccountIds(holdingAccountIds);
		command.setHoldingInvestmentCompanyId(holdingCompanyId);
		command.setHoldingInvestmentAccountGroupId(holdingAccountGroupId);
		command.setInvestmentSecurityId(investmentSecurityId);
		command.setInvestmentGroupId(investmentGroupId);
		command.setIncludeUnsettledLegPayments(includeUnsettledLegPayments);
		return getAccountingPositionService().getAccountingPositionListUsingCommand(command);
	}


	/**
	 * Returns a map of opening lot AccountingTransaction.id of the opening lot (parent of parent) to corresponding income statement amounts (sum of more than one) for the
	 * specified filters.
	 * <p>
	 * Need to include transactions on the specified snapshotDate as well as all dates prior to snapshotDate but after priorDate (this will include weekend and holiday activity:
	 * can't skip anything).
	 */
	private Map<Long, IncomeStatementData> getIncomeStatementData(Integer clientAccountId, Integer[] clientAccountIds, Integer holdingAccountId, Integer[] holdingAccountIds, Integer holdingCompanyId, Integer clientAccountGroupId, Integer holdingAccountGroupId, Integer investmentSecurityId, Short investmentGroupId, Date snapshotDate, boolean useSettlementDate, Date realizedAdjustmentDate) {
		Map<Long, IncomeStatementData> result = new HashMap<>();
		// get all GL transactions for the specified parameters
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setClientInvestmentAccountId(clientAccountId);
		searchForm.setClientInvestmentAccountIds(clientAccountIds);
		searchForm.setHoldingInvestmentAccountId(holdingAccountId);
		searchForm.setHoldingInvestmentAccountIds(holdingAccountIds);
		searchForm.setHoldingAccountIssuingCompanyId(holdingCompanyId);
		searchForm.setClientInvestmentAccountGroupId(clientAccountGroupId);
		searchForm.setHoldingInvestmentAccountGroupId(holdingAccountGroupId);
		searchForm.setInvestmentSecurityId(investmentSecurityId);
		searchForm.setInvestmentGroupId(investmentGroupId);
		String dateField = useSettlementDate ? "settlementDate" : "transactionDate";
		// include gain/loss for each holiday/weekend between prior and snapshot dates

		// Cleared CDS Defaults: include payment one business day before settlement
		boolean adjustRealized = (realizedAdjustmentDate != null);
		if (adjustRealized) {
			searchForm.addSearchRestriction(new SearchRestriction(dateField, ComparisonConditions.GREATER_THAN_OR_EQUALS, snapshotDate));
			searchForm.addSearchRestriction(new SearchRestriction(dateField, ComparisonConditions.LESS_THAN_OR_EQUALS, realizedAdjustmentDate));
		}
		else if (DateUtils.isMonday(snapshotDate) || DateUtils.isSunday(snapshotDate)) {
			int daysBack = (DateUtils.isMonday(snapshotDate) ? 2 : 1);
			// need to include Saturday -> Sunday and Saturday and Sunday -> Monday transactions: possible coupon payment
			searchForm.addSearchRestriction(new SearchRestriction(dateField, ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addDays(snapshotDate, -daysBack)));
			searchForm.addSearchRestriction(new SearchRestriction(dateField, ComparisonConditions.LESS_THAN_OR_EQUALS, snapshotDate));
		}
		else {
			searchForm.addSearchRestriction(new SearchRestriction(dateField, ComparisonConditions.EQUALS, snapshotDate));
		}

		List<AccountingTransaction> list = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (AccountingTransaction t : CollectionUtils.getIterable(list)) {
			if (t.getAccountingAccount().isCurrency() && !AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS.equals(t.getAccountingAccount().getName())) {
				// ignore all currency entries from the list except for Currency Translation Gain/Loss
				continue;
			}

			boolean onSnapshotDate = snapshotDate.equals(useSettlementDate ? t.getSettlementDate() : t.getTransactionDate());

			// Cleared CDS Credit or Coupon Payment
			boolean delayedTransaction = !t.getSettlementDate().equals(t.getTransactionDate());
			boolean adjustedAndDelayed = adjustRealized && delayedTransaction;

			// set quantityClosed
			AccountingAccount glAccount = t.getAccountingAccount();
			if (glAccount.isPosition() && t.getParentTransaction() != null) {
				IncomeStatementData data = result.computeIfAbsent(t.getParentTransaction().getId(), k -> new IncomeStatementData(t.getParentTransaction()));
				if (onSnapshotDate) {
					data.quantityClosed = MathUtils.add(data.quantityClosed, t.getQuantity());

					if (adjustedAndDelayed) {
						// offset payment shift to one day earlier
						data.realizedAdjustmentLocal = MathUtils.subtract(data.realizedAdjustmentLocal, t.getLocalDebitCredit().negate());
						data.realizedAdjustmentBase = MathUtils.subtract(data.realizedAdjustmentBase, t.getBaseDebitCredit().negate());
					}
				}
				else if (adjustedAndDelayed) {
					// shift payment one day earlier
					data.realizedAdjustmentLocal = MathUtils.add(data.realizedAdjustmentLocal, t.getLocalDebitCredit().negate());
					data.realizedAdjustmentBase = MathUtils.add(data.realizedAdjustmentBase, t.getBaseDebitCredit().negate());
				}
			}

			// set income statement amounts: realized gain/loss, commission, etc.
			if (!glAccount.getAccountType().isBalanceSheetAccount()) {
				// get the opening lot: parent for closing and grand-parent for opening
				AccountingTransaction openingLot = t.getParentTransaction();
				if (openingLot != null && openingLot.getParentTransaction() != null) {
					openingLot = openingLot.getParentTransaction();
				}
				if (openingLot == null) {
					// only count commissions tied to transactions (positions)
					// Trust Custody Fee isn't; will this skip NFA Fee/etc. if it's not allocated to specific transactions?
					continue;
				}

				IncomeStatementData data = result.get(openingLot.getId());
				if (data == null) {
					data = new IncomeStatementData(openingLot);
					result.put(openingLot.getId(), data);
				}

				if ((onSnapshotDate || !adjustRealized) && !AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS.equals(t.getAccountingAccount().getName())) {
					// include Saturday and Sunday transactions if not adjusting realized (skips Currency Translation Gain/Loss)
					data.gainLossLocal = MathUtils.add(data.gainLossLocal, t.getLocalDebitCredit().negate());
					data.gainLossBase = MathUtils.add(data.gainLossBase, t.getBaseDebitCredit().negate());

					if (glAccount.isCommission()) {
						data.commissionLocal = MathUtils.add(data.commissionLocal, t.getLocalDebitCredit());
						data.commissionBase = MathUtils.add(data.commissionBase, t.getBaseDebitCredit());
					}
					if (glAccount.isGainLoss() && !glAccount.isPostingNotAllowed()) {
						data.realizedLocal = MathUtils.add(data.realizedLocal, t.getLocalDebitCredit().negate());
						data.realizedBase = MathUtils.add(data.realizedBase, t.getBaseDebitCredit().negate());
					}
				}
				if (adjustedAndDelayed && glAccount.isGainLoss() && !glAccount.isPostingNotAllowed()) {
					// shift realized one business day earlier
					if (onSnapshotDate) {
						if (!AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS.equals(t.getAccountingAccount().getName())) {
							data.realizedAdjustmentLocal = MathUtils.subtract(data.realizedAdjustmentLocal, t.getLocalDebitCredit().negate());
							data.realizedAdjustmentBase = MathUtils.subtract(data.realizedAdjustmentBase, t.getBaseDebitCredit().negate());
						}
					}
					else {
						// also includes Currency Translation Gain/Loss: Delayed Foreign Cleared CDS for marks one day before Payment Date
						data.realizedAdjustmentLocal = MathUtils.add(data.realizedAdjustmentLocal, t.getLocalDebitCredit().negate());
						data.realizedAdjustmentBase = MathUtils.add(data.realizedAdjustmentBase, t.getBaseDebitCredit().negate());
					}
				}
			}
		}

		return result;
	}


	/**
	 * Returns the price for the specified security on the specified date. Caches the price for faster future performance. Optional lot openingPrice will be used if not null and
	 * market data price is not available.
	 */
	private BigDecimal getPrice(InvestmentSecurity security, Date snapshotDate, BigDecimal openingPrice, boolean exceptionIfMissing, SnapshotRebuildContext cache) {
		BigDecimal result = cache.getSecurityPrice(security.getId());
		if (result == null) {
			result = getMarketDataRetriever().getPriceFlexible(security, snapshotDate, false);
			if (result == null) {
				result = openingPrice;
				if (result == null) {
					result = cache.getSecurityTradePrice(security.getId());
				}
				else {
					cache.cacheSecurityTradePrice(security.getId(), result);
				}
				if (exceptionIfMissing) {
					ValidationUtils.assertNotNull(result, "Cannot find price for investment security " + security + " on " + snapshotDate);
				}
			}
			else {
				cache.cacheSecurityPrice(security.getId(), result);
			}
		}
		return result;
	}


	/**
	 * Returns the price for the specified security on the specified date. Caches the price for faster future performance. Optional lot openingPrice will be used if not null and
	 * market data price is not available.
	 */
	private BigDecimal getPriorPrice(InvestmentSecurity security, Date snapshotDate, BigDecimal openingPrice, boolean exceptionIfMissing, SnapshotRebuildContext cache) {
		BigDecimal result = cache.getPriorSecurityPrice(security.getId());
		if (result == null) {
			result = getMarketDataRetriever().getPriceFlexible(security, snapshotDate, false);
			if (result == null) {
				result = openingPrice;
				if (result == null) {
					result = cache.getPriorSecurityTradePrice(security.getId());
				}
				else {
					cache.cachePriorSecurityTradePrice(security.getId(), result);
				}
				if (exceptionIfMissing) {
					ValidationUtils.assertNotNull(result, "Cannot find price for investment security " + security + " on " + snapshotDate);
				}
			}
			else {
				cache.cachePriorSecurityPrice(security.getId(), result);
			}
		}
		return result;
	}


	private BigDecimal getExchangeRate(InvestmentObjectInfo investmentObject, Date date, SnapshotRebuildContext context) {
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(investmentObject);
		String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(investmentObject);
		return context.getFxRateLookupCache().getExchangeRate(investmentObject.getHoldingInvestmentAccount().toHoldingAccount(), fromCurrency, toCurrency, date, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingPositionDaily, Criteria> getAccountingPositionDailyDAO() {
		return this.accountingPositionDailyDAO;
	}


	public void setAccountingPositionDailyDAO(AdvancedUpdatableDAO<AccountingPositionDaily, Criteria> accountingPositionDailyDAO) {
		this.accountingPositionDailyDAO = accountingPositionDailyDAO;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}


	static class IncomeStatementData {

		public final AccountingTransaction openingTransaction;

		public BigDecimal quantityClosed = BigDecimal.ZERO;

		public BigDecimal commissionLocal = BigDecimal.ZERO;
		public BigDecimal commissionBase = BigDecimal.ZERO;
		public BigDecimal realizedLocal = BigDecimal.ZERO;
		public BigDecimal realizedBase = BigDecimal.ZERO;

		// used by cleared CDS to shift coupon/credit event m2m payment one day earlier
		public BigDecimal realizedAdjustmentLocal = BigDecimal.ZERO;
		public BigDecimal realizedAdjustmentBase = BigDecimal.ZERO;

		// sum of income statement account balances for the day
		public BigDecimal gainLossLocal = BigDecimal.ZERO;
		public BigDecimal gainLossBase = BigDecimal.ZERO;


		public IncomeStatementData(AccountingTransaction openingTransaction) {
			this.openingTransaction = openingTransaction;
		}
	}
}
