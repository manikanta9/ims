package com.clifton.accounting.gl.journal.action.processor;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.action.AccountingJournalAction;
import com.clifton.accounting.gl.journal.action.AccountingJournalActionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class AccountingJournalActionProcessorHandlerImpl implements AccountingJournalActionProcessorHandler {

	private AccountingJournalActionService accountingJournalActionService;

	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends BookableEntity> void processActions(AccountingJournal journal, T bookableEntity) {
		getActionProcessors(journal, bookableEntity).forEach(p -> p.processAction(bookableEntity, journal));
	}


	@Override
	public <T extends BookableEntity> void rollbackActions(AccountingJournal journal, T bookableEntity) {
		getActionProcessors(journal, bookableEntity).forEach(p -> p.rollbackAction(bookableEntity, journal));
	}


	private <T extends BookableEntity> List<AccountingJournalActionProcessor<T>> getActionProcessors(AccountingJournal journal, T bookableEntity) {
		List<AccountingJournalActionProcessor<T>> processorList = new ArrayList<>();

		List<AccountingJournalAction> actionList = getAccountingJournalActionService().getAccountingJournalActionListByJournalType(journal.getJournalType().getId());
		for (AccountingJournalAction action : CollectionUtils.getIterable(actionList)) {
			SystemCondition scope = action.getScopeSystemCondition();
			if (scope != null) {
				if (!getSystemConditionEvaluationHandler().isConditionTrue(scope, ObjectUtils.coalesce(bookableEntity, journal))) {
					// skip this action because the journal does not match the scope condition
					continue;
				}
			}
			@SuppressWarnings("unchecked")
			AccountingJournalActionProcessor<T> processor = (AccountingJournalActionProcessor<T>) getSystemBeanService().getBeanInstance(action.getActionBean());
			processorList.add(processor);
		}

		return processorList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingJournalActionService getAccountingJournalActionService() {
		return this.accountingJournalActionService;
	}


	public void setAccountingJournalActionService(AccountingJournalActionService accountingJournalActionService) {
		this.accountingJournalActionService = accountingJournalActionService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
