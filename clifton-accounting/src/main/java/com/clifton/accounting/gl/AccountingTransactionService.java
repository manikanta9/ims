package com.clifton.accounting.gl;


import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingTransactionService</code> interface defines methods for extracting information
 * from the General Ledger (AccountingTransaction table).
 *
 * @author vgomelsky
 */
public interface AccountingTransactionService {

	public AccountingTransaction getAccountingTransaction(long id);


	public List<AccountingTransaction> getAccountingTransactionListByIds(Long[] ids);


	public List<AccountingTransaction> getAccountingTransactionListByJournal(long journalId);


	public List<AccountingTransaction> getAccountingTransactionList(AccountingTransactionSearchForm searchForm);


	/**
	 * Returns a Map of Integer Client Investment Account Ids to the earliest date affected for the Client Account
	 * where a transaction for that client account has a posting date from daysBack to today
	 * and transaction date is not equal to today.
	 * <p/>
	 * Used by nightly batch jobs that rebuild historical snapshots for dates affected
	 *
	 * @param daysBack
	 */
	@DoNotAddRequestMapping
	public Map<Integer, Date> getAccountingTransactionHistoricalChangeMap(int daysBack);
}
