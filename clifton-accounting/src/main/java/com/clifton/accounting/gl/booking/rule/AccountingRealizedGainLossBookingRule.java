package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>AccountingRealizedGainLossBookingRule</code> adds a "Realized Gain / Loss" entry and corresponding "Cash" or "Currency"
 * entry to the booking session journal for each closing "Position".
 *
 * @param <T>
 */
public class AccountingRealizedGainLossBookingRule<T extends BookableEntity> extends AccountingCommonBookingRule<T> implements AccountingBookingRule<T> {

	private String overrideAccountingAccount;

	/**
	 * If true, only applies the rule to closing positions that close another open position from the same journal: Stock Spinoff partial lot closing for Cash in Lieu, etc.
	 */
	private boolean sameJournalClosingsOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(128);
		result.append(this.getClass().getName());
		result.append(": overrideAccountingAccount=");
		result.append(getOverrideAccountingAccount());
		return result.toString();
	}


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		Collection<? extends AccountingJournalDetailDefinition> detailRecords = CoreCollectionUtils.clone(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		// get full list of all positions that need realized
		// group them by the lot that they are closing (to avoid gain/loss of pennies due to rounding
		Map<Long, List<AccountingJournalDetailDefinition>> detailsThatNeedRealized = new LinkedHashMap<>();
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailRecords)) {
			if (isPositionThatNeedsRealized(detail)) {
				Long parentId = getParent(detail).getId();
				List<AccountingJournalDetailDefinition> list = detailsThatNeedRealized.computeIfAbsent(parentId, k -> new ArrayList<>());
				list.add(detail);
			}
		}

		// process each group separately
		for (List<AccountingJournalDetailDefinition> list : detailsThatNeedRealized.values()) {
			// if full close, then process largest lot last and give it the difference to avoid penny rounding
			boolean fullClose = false;
			BookingPosition bookingPositionBeingClosed = bookingSession.getBookingPosition(list.get(0));
			if (bookingPositionBeingClosed == null) {
				if (isSameJournalClosingsOnly()) {
					bookingPositionBeingClosed = new BookingPosition(AccountingPosition.forOpeningTransaction(list.get(0).getParentDefinition()));
				}
				else {
					throw new IllegalStateException("Cannot have Realized Gain/Loss without position closing: " + detailsThatNeedRealized);
				}
			}
			if (MathUtils.isNullOrZero(bookingPositionBeingClosed.getRemainingQuantity())) {
				fullClose = true;
				// largest by absolute value is last (hide penny difference due to rounding in the largest lot)
				list = BeanUtils.sortWithFunction(list, AccountingJournalDetailDefinition::getQuantity, MathUtils.isGreaterThan(list.get(0).getQuantity(), BigDecimal.ZERO));
			}

			BigDecimal costBasisRemaining = bookingPositionBeingClosed.getPosition().getRemainingCostBasis();
			BigDecimal baseDebitCreditRemaining = bookingPositionBeingClosed.getPosition().getRemainingBaseDebitCredit();
			int size = CollectionUtils.getSize(list);
			for (int i = 0; i < size; i++) {
				AccountingJournalDetailDefinition detail = list.get(i);
				AccountingPosition closingPosition = bookingPositionBeingClosed.getPosition();
				BigDecimal closingPortion = MathUtils.divide(detail.getQuantity(), bookingPositionBeingClosed.getPosition().getQuantity());
				BigDecimal costBasisReduction = MathUtils.multiply(bookingPositionBeingClosed.getPosition().getPositionCostBasis(), closingPortion, 2);
				BigDecimal baseDebitCreditReduction = MathUtils.multiply(bookingPositionBeingClosed.getPosition().getBaseDebitCredit(), closingPortion, 2);
				if (MathUtils.isLessThanOrEqual(costBasisReduction.abs().subtract(closingPosition.getRemainingCostBasis().abs()).abs(), new BigDecimal("0.02"))) {
					costBasisReduction = closingPosition.getRemainingCostBasis().negate();
					baseDebitCreditReduction = closingPosition.getRemainingBaseDebitCredit().negate();
				}

				if (fullClose) {
					if (i == (size - 1)) {
						// last one
						costBasisReduction = costBasisRemaining.negate();
						baseDebitCreditReduction = baseDebitCreditRemaining.negate();
					}
					else {
						if (MathUtils.isSameSignum(costBasisRemaining, costBasisReduction)) {
							costBasisRemaining = MathUtils.subtract(costBasisRemaining, costBasisReduction);
							baseDebitCreditRemaining = MathUtils.subtract(baseDebitCreditRemaining, baseDebitCreditReduction);
						}
						else {
							costBasisRemaining = MathUtils.add(costBasisRemaining, costBasisReduction);
							baseDebitCreditRemaining = MathUtils.add(baseDebitCreditRemaining, baseDebitCreditReduction);
						}
					}
				}
				InvestmentSecurity settlementCurrency = detail.getInvestmentSecurity().getInstrument().getTradingCurrency();
				if (bookingSession.getBookableEntity() instanceof AccountingBean) {
					settlementCurrency = ((AccountingBean) bookingSession.getBookableEntity()).getSettlementCurrency();
				}
				addRealizedGLRecord(journal, detail, costBasisReduction, baseDebitCreditReduction, settlementCurrency);
			}
		}
	}


	private AccountingJournalDetailDefinition getParent(AccountingJournalDetailDefinition detail) {
		return isSameJournalClosingsOnly() ? detail.getParentDefinition() : detail.getParentTransaction();
	}


	private boolean isPositionThatNeedsRealized(AccountingJournalDetailDefinition detail) {
		AccountingAccount accountingAccount = detail.getAccountingAccount();
		// for each closing child position (points to parent that it closes)
		if (accountingAccount.isPosition() && detail.isClosing()) {
			if (isSameJournalClosingsOnly()) {
				AccountingJournalDetailDefinition parent = detail.getParentDefinition();
				return (parent != null && parent.getJournal().equals(detail.getJournal()));
			}
			else {
				return detail.isParentDefinitionEmpty();
			}
		}
		return false;
	}


	/**
	 * Generates and adds Realized Gain/Loss detail to the specified journal.  Updates closingPosition so that it matches the opening that it closes.
	 * If Currency Translation Gain/Loss from close of a foreign Physical is generated, adds it to the journal too.
	 */
	private void addRealizedGLRecord(AccountingJournal journal, AccountingJournalDetailDefinition closingPosition, BigDecimal costBasisReduction, BigDecimal baseDebitCreditReduction, InvestmentSecurity settlementCurrency) {
		AccountingJournalDetail result = new AccountingJournalDetail();
		BeanUtils.copyProperties(closingPosition, result, new String[]{"parentTransaction"});
		result.setParentDefinition(closingPosition);
		journal.addJournalDetail(result);

		AccountingAccount account = getAccountingAccountService().getAccountingAccountByName(
				getOverrideAccountingAccount() != null ? getOverrideAccountingAccount() : AccountingAccount.REVENUE_REALIZED);
		result.setAccountingAccount(account);
		result.setQuantity(closingPosition.getQuantity() == null ? null : closingPosition.getQuantity().abs());

		// calculate realized gain/loss
		result.setLocalDebitCredit(MathUtils.subtract(result.getPositionCostBasis(), costBasisReduction));
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), result.getClientInvestmentAccount()));
		result.setPositionCostBasis(BigDecimal.ZERO);
		result.setPositionCommission(BigDecimal.ZERO);

		if (InvestmentUtils.isNoPaymentOnOpen(closingPosition.getInvestmentSecurity())) {
			// update closing position to match what's being closed (proportional using quantity to what was open)
			closingPosition.setExchangeRateToBase(getParent(closingPosition).getExchangeRateToBase());
			closingPosition.setLocalDebitCredit(BigDecimal.ZERO);
			closingPosition.setBaseDebitCredit(BigDecimal.ZERO);
			closingPosition.setPositionCostBasis(costBasisReduction);

			// Deliverable Currency Forwards have unique treatment for Realized Gain / Loss
			InvestmentInstrument instrument = closingPosition.getInvestmentSecurity().getInstrument();
			if (instrument.isDeliverable() && InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.FORWARDS)) {
				// Gain/Loss is realized at Maturity buy exchanging amounts agreed upon on opening for the 2 currencies.  Make this position balance.
				BigDecimal translationGainLoss = MathUtils.negate(CoreMathUtils.sumProperty(journal.findChildEntries(closingPosition), AccountingJournalDetailDefinition::getBaseDebitCredit));
				if (!MathUtils.isNullOrZero(translationGainLoss)) {
					result.setBaseDebitCredit(MathUtils.add(result.getBaseDebitCredit(), translationGainLoss));
					if (MathUtils.isEqual(result.getExchangeRateToBase(), BigDecimal.ONE)) {
						result.setLocalDebitCredit(result.getBaseDebitCredit());
					}
					else {
						result.setLocalDebitCredit(MathUtils.add(result.getLocalDebitCredit(), InvestmentCalculatorUtils.calculateLocalAmount(translationGainLoss, result.getExchangeRateToBase(), instrument.getTradingCurrency())));
					}
				}
			}
			else {
				journal.addJournalDetail(createOffsettingEntry(result, settlementCurrency));
			}
		}
		else {
			// apply Currency Translation Gain/Loss for foreign physicals, if non zero
			BigDecimal translationGainLoss = MathUtils.subtract(closingPosition.getBaseDebitCredit(), baseDebitCreditReduction);
			translationGainLoss = MathUtils.subtract(translationGainLoss, result.getBaseDebitCredit());
			if (!MathUtils.isNullOrZero(translationGainLoss)) {
				AccountingJournalDetail currencyTranslation = new AccountingJournalDetail();
				BeanUtils.copyProperties(result, currencyTranslation);
				currencyTranslation.setQuantity(null);
				currencyTranslation.setPrice(null);
				currencyTranslation.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS));
				currencyTranslation.setDescription(AccountingAccount.CURRENCY_TRANSLATION_GAIN_LOSS + " for " + currencyTranslation.getInvestmentSecurity().getSymbol());
				currencyTranslation.setLocalDebitCredit(BigDecimal.ZERO);
				currencyTranslation.setBaseDebitCredit(translationGainLoss);
				currencyTranslation.setPositionCostBasis(BigDecimal.ZERO);
				journal.addJournalDetail(currencyTranslation);
			}

			// update closing position to match what's being closed (proportional using quantity to what was open)
			closingPosition.setExchangeRateToBase(getParent(closingPosition).getExchangeRateToBase());
			closingPosition.setLocalDebitCredit(costBasisReduction);
			closingPosition.setBaseDebitCredit(baseDebitCreditReduction);
			closingPosition.setPositionCostBasis(costBasisReduction);
		}

		boolean gain = MathUtils.isLessThan(result.getBaseDebitCredit(), BigDecimal.ZERO);
		if (account.getAccountType().isGrowingOnDebitSide()) { // expense account
			gain = !gain;
		}
		result.setDescription(account.getName() + (gain ? " gain for " : " loss for ") + closingPosition.getInvestmentSecurity().getSymbol());
	}


	/**
	 * Creates the offsetting entry that is opposite to the specified Gain/Loss entry and makes Debits and Credits balance.
	 * Default implementation adds Cash or Currency entry. However, in some cases one may want to override this method (Gain/Loss Distribution for internal transfer).
	 */
	protected AccountingJournalDetail createOffsettingEntry(AccountingJournalDetail realizedEntry, InvestmentSecurity settlementCurrency) {
		return createCashCurrencyRecord(realizedEntry, settlementCurrency);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public String getOverrideAccountingAccount() {
		return this.overrideAccountingAccount;
	}


	public void setOverrideAccountingAccount(String overrideAccountingAccount) {
		this.overrideAccountingAccount = overrideAccountingAccount;
	}


	public boolean isSameJournalClosingsOnly() {
		return this.sameJournalClosingsOnly;
	}


	public void setSameJournalClosingsOnly(boolean sameJournalClosingsOnly) {
		this.sameJournalClosingsOnly = sameJournalClosingsOnly;
	}
}
