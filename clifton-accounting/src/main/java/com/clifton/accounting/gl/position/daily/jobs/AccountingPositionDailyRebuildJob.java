package com.clifton.accounting.gl.position.daily.jobs;

import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.util.Map;


/**
 * @author mitchellf
 */
public class AccountingPositionDailyRebuildJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private AccountingPositionDailyService accountingPositionDailyService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private DateGenerationOptions dateGenerationOptions;

	private Integer daysFromToday;

	private StatusHolderObject<Status> statusHolder;


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getDateGenerationOptions(), "Date Generation Option must be specified!");
		if (getDateGenerationOptions().isDayCountRequired()) {
			ValidationUtils.assertNotNull(getDaysFromToday(), "A number of days from today must be specified for Date Generation Option [" + getDateGenerationOptions().getLabel() + "].");
		}
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status statusToReturn = this.statusHolder.getStatus();
		statusToReturn.setMessage("Rebuilding accounting positions using Date Generation Option [" + getDateGenerationOptions() + "] for " + getDaysFromToday() + " days from today...");
		try {
			Status result = getAccountingPositionDailyService().rebuildAccountingPositionDailyForDate(getDateGenerationOptions(), getDaysFromToday());
			statusToReturn.mergeStatus(result);
			return statusToReturn;
		}
		catch (Exception e) {
			statusToReturn.setMessageWithErrors("Rebuild failed due to the following:\n" + e, null);
			return statusToReturn;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public DateGenerationOptions getDateGenerationOptions() {
		return this.dateGenerationOptions;
	}


	public void setDateGenerationOptions(DateGenerationOptions dateGenerationOptions) {
		this.dateGenerationOptions = dateGenerationOptions;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}
}
