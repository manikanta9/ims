package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl.AccountingAccountIds;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>AccountingAutoAccrualReversalBookingRule</code> class is a booking rule that automatically
 * reverses non zero accrual balances for GL accounts configured for autoReverseAccrual.
 * <p>
 * For each closing position, it looks up autoReverseAccrual account's balance tied to corresponding opening position.
 * If non, zero, it will add 2 more details: one to reverse accrual to zero and the other to offset reversal.
 * <p>
 * For example, "Unrealized Gain / Loss" and "Recognized Gain / Loss" GL accounts maybe applied at year end for taxable account positions.
 * When closing a position like this, the system will identify non zero "Unrealized Gain / Loss" balance tied to the opening
 * and will auto-generate a GL entry that gets it to zero.  The offsetting account "Realized Gain / Loss" identified by this field
 * will be used to make the journal balance.  It will affect actual "Realized Gain / Loss" from closing.
 *
 * @author vgomelsky
 */
public class AccountingAutoAccrualReversalBookingRule<T extends BookableEntity> implements AccountingBookingRule<T> {

	private AccountingAccountIdsCache accountingAccountIdsCache;
	private AccountingTransactionService accountingTransactionService;


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> detailRecords = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		// keep track of what was processed to avoid duplicate reversals
		Set<AccountingTransaction> processedPositions = new HashSet<>();

		// Hit AccountingTransaction table once and map by opening
		AccountingTransactionSearchForm accountingTransactionSearchForm = new AccountingTransactionSearchForm();
		accountingTransactionSearchForm.setParentIdList(BeanUtils.getPropertyValuesExcludeNull(detailRecords, accountingJournalDetailDefinition -> BeanUtils.getBeanIdentity(accountingJournalDetailDefinition.getParentTransaction()), Long.class));
		accountingTransactionSearchForm.setAccountingAccountIds(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIds.AUTO_REVERSE_ACCRUAL_ACCOUNTS));
		// Only retrieve if there are parent ids populated
		List<AccountingTransaction> fullAccrualList = ArrayUtils.isEmpty(accountingTransactionSearchForm.getParentIdList()) ? null : getAccountingTransactionService().getAccountingTransactionList(accountingTransactionSearchForm);
		Map<Long, List<AccountingTransaction>> openingAccrualListMap = BeanUtils.getBeansMap(fullAccrualList, accountingTransaction -> BeanUtils.getBeanIdentity(accountingTransaction.getParentTransaction()));

		for (AccountingJournalDetailDefinition detail : detailRecords) {
			AccountingTransaction opening = detail.getParentTransaction();
			if (opening != null && detail.getAccountingAccount().isPosition()) {
				// found closing detail
				if (!processedPositions.contains(opening)) {
					processedPositions.add(opening); // mark as processed to avoid duplicate processing

					List<AccountingTransaction> accrualList = openingAccrualListMap.get(opening.getId());

					// could have different accrual accounts that use auto-reversal
					Map<AccountingAccount, List<AccountingTransaction>> transactionMap = BeanUtils.getBeansMap(accrualList, AccountingTransaction::getAccountingAccount);
					if (transactionMap != null) {
						for (Map.Entry<AccountingAccount, List<AccountingTransaction>> accountingAccountListEntry : transactionMap.entrySet()) {
							AccountingAccount account = accountingAccountListEntry.getKey();
							List<AccountingTransaction> transactionList = accountingAccountListEntry.getValue();
							BigDecimal localDebitCredit = CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
							BigDecimal baseDebitCredit = CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getBaseDebitCredit);
							if (!MathUtils.isNullOrZero(localDebitCredit) || !MathUtils.isNullOrZero(baseDebitCredit)) {
								// accrual balance is present: add reversing entries
								AccountingJournalDetail reversalEntry = new AccountingJournalDetail();
								BeanUtils.copyProperties(detail, reversalEntry);
								reversalEntry.setParentTransaction(null);
								reversalEntry.setParentDefinition(detail);
								reversalEntry.setAccountingAccount(account);
								reversalEntry.setQuantity(CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getQuantity));
								reversalEntry.setLocalDebitCredit(localDebitCredit.negate());
								reversalEntry.setBaseDebitCredit(baseDebitCredit.negate());
								reversalEntry.setPositionCostBasis(BigDecimal.ZERO);
								reversalEntry.setPositionCommission(BigDecimal.ZERO);
								reversalEntry.setDescription("Reversing accrual of " + account.getName());

								AccountingJournalDetail offsettingEntry = new AccountingJournalDetail();
								BeanUtils.copyProperties(reversalEntry, offsettingEntry);
								offsettingEntry.setAccountingAccount(account.getAutoAccrualReversalOffsetAccount());
								offsettingEntry.setLocalDebitCredit(offsettingEntry.getLocalDebitCredit().negate());
								offsettingEntry.setBaseDebitCredit(offsettingEntry.getBaseDebitCredit().negate());
								offsettingEntry.setDescription("Adjusting " + account.getAutoAccrualReversalOffsetAccount().getName() + " after accrual reversal of " + account.getName());

								journal.addJournalDetail(reversalEntry);
								journal.addJournalDetail(offsettingEntry);
							}
						}
					}
				}
			}
		}
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}
}
