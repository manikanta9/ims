package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.system.bean.SystemBeanType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link SystemBeanType} implementation of {@link AccountingPositionValueCalculator} which calculates fixed values for positions.
 * <p>
 * This calculator produces a configured fixed value each time that it is evaluated.
 *
 * @author MikeH
 */
public class AccountingPositionFixedValueCalculator implements AccountingPositionValueCalculator {


	private BigDecimal fixedValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculate(AccountingPosition position, Date snapshotDate) {
		return getFixedValue();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getFixedValue() {
		return this.fixedValue;
	}


	public void setFixedValue(BigDecimal fixedValue) {
		this.fixedValue = fixedValue;
	}
}
