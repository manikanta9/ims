package com.clifton.accounting.gl;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentTransactionInfo;

import java.math.BigDecimal;


/**
 * The <code>AccountingTransactionInfo</code> interface defines common GL attributes that are shared
 * among transactions and positions.
 *
 * @author vgomelsky
 */
public interface AccountingTransactionInfo extends AccountingObjectInfo, InvestmentTransactionInfo, IdentityObject {

	public Long getId();


	public BusinessCompany getExecutingCompany();


	public BigDecimal getQuantity();


	public BigDecimal getQuantityNormalized();


	public BigDecimal getPositionCostBasis();


	public BigDecimal getExchangeRateToBase();


	public BigDecimal getPrice();
}
