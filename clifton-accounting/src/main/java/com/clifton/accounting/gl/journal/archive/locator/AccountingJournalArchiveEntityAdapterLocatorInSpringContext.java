package com.clifton.accounting.gl.journal.archive.locator;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveEntityAdapter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>AccountingJournalArchiveEntityAdapterLocatorInSpringContext</code> locates {@link AccountingJournalArchiveEntityAdapter}
 * implementations for corresponding journal types. It auto-discovers all application context beans that implement
 * AccountingJournalArchiveEntityAdapter interface and registers them so that they can be located.
 *
 * @author NickK
 */
@Component
public class AccountingJournalArchiveEntityAdapterLocatorInSpringContext<T extends BookableEntity> implements AccountingJournalArchiveEntityAdapterLocator, InitializingBean, ApplicationContextAware {

	private final Map<String, AccountingJournalArchiveEntityAdapter<T>> adapterMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void afterPropertiesSet() {
		Map<String, AccountingJournalArchiveEntityAdapter> beanMap = getApplicationContext().getBeansOfType(AccountingJournalArchiveEntityAdapter.class);
		// need a map with journal type names as keys instead of bean names
		if (!CollectionUtils.isEmpty(beanMap)) {
			beanMap.entrySet().forEach(adapterEntry -> {
				AccountingJournalArchiveEntityAdapter<T> adapter = adapterEntry.getValue();
				if (getAdapterMap().containsKey(adapter.getJournalTypeName())) {
					throw new IllegalStateException("Cannot register '" + adapterEntry.getKey() + "' as an archive adapter for journal of type '"
							+ adapter.getJournalTypeName() + "' because this journal type already has a registered adapter.");
				}
				getAdapterMap().put(adapter.getJournalTypeName(), adapter);
			});
		}
	}


	@Override
	public AccountingJournalArchiveEntityAdapter<T> locate(String journalTypeName) {
		AssertUtils.assertNotNull(journalTypeName, "Required journal type name cannot be null.");
		AccountingJournalArchiveEntityAdapter<T> result = getAdapterMap().get(journalTypeName);
		if (result == null) {
			result = getAdapterMap().get(AccountingJournalArchiveEntityAdapter.DEFAULT_ACCOUNTING_JOURNAL_ARCHIVE_ADAPTER_KEY);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, AccountingJournalArchiveEntityAdapter<T>> getAdapterMap() {
		return this.adapterMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
