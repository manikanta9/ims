package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.util.math.BinaryOperators;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link SystemBeanType} implementation of {@link AccountingPositionValueCalculator} which calculates values for positions from two other calculators.
 * <p>
 * This calculator computes a value for a position by using a {@link BinaryOperators binary operator} to combine the values for two other calculators. The left and right
 * calculators (operands) and the operator are configurable.
 *
 * @author MikeH
 */
public class AccountingPositionBinaryOperatorValueCalculator implements AccountingPositionValueCalculator {

	private SystemBeanService systemBeanService;

	private BinaryOperators binaryOperator;
	private SystemBean leftOperand;
	private SystemBean rightOperand;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculate(AccountingPosition position, Date snapshotDate) {
		BigDecimal leftResult = generatePositionValueCalculator(getLeftOperand()).calculate(position, snapshotDate);
		BigDecimal rightResult = generatePositionValueCalculator(getRightOperand()).calculate(position, snapshotDate);
		return getBinaryOperator().operate(leftResult, rightResult);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the {@link AccountingPositionValueCalculator} from the given bean.
	 *
	 * @param calculatorBean the bean defining the calculator configuration
	 * @return the generated calculator instance
	 */
	private AccountingPositionValueCalculator generatePositionValueCalculator(SystemBean calculatorBean) {
		return (AccountingPositionValueCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public BinaryOperators getBinaryOperator() {
		return this.binaryOperator;
	}


	public void setBinaryOperator(BinaryOperators binaryOperator) {
		this.binaryOperator = binaryOperator;
	}


	public SystemBean getLeftOperand() {
		return this.leftOperand;
	}


	public void setLeftOperand(SystemBean leftOperand) {
		this.leftOperand = leftOperand;
	}


	public SystemBean getRightOperand() {
		return this.rightOperand;
	}


	public void setRightOperand(SystemBean rightOperand) {
		this.rightOperand = rightOperand;
	}
}
