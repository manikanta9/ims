package com.clifton.accounting.gl.balance;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>AccountingClientAccountBalance</code> represents an aggregate balance for a particular Client {@link com.clifton.investment.account.InvestmentAccount}
 * according to criteria defined on {@link com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm}.
 * <p>
 * The balance will be aggregated across holding accounts unless holding account criterion are specified, in which case each holding account will contain
 * a balance for the client account. Similar to {@link AccountingBalance}, each balance is pulled out of the General Ledger and is not adjusted according
 * to current valuation.
 *
 * @author NickK
 */
@NonPersistentObject
public class AccountingClientAccountBalance {

	/**
	 * ID of the client {@link com.clifton.investment.account.InvestmentAccount} for the balance aggregated across holding accounts
	 * or containing a map of balances by holding account. Use {@link #isAggregateAcrossHoldingAccounts()} to know which.
	 */
	private final Integer clientInvestmentAccountId;
	/**
	 * Flag to tell whether this client account balance is aggregated across holding accounts or contains balances per holding account.
	 */
	private final boolean aggregateAcrossHoldingAccounts;
	/**
	 * Balance aggregated across holding accounts. Only contains a reliable value if {@link #isAggregateAcrossHoldingAccounts()} is true.
	 */
	private final BigDecimal clientAccountBalanceValue;
	/**
	 * Map containing entries for balances by holding account IDs for the client account.
	 */
	private Map<Integer, BigDecimal> holdingInvestmentAccountBalanceMap = new HashMap<>();


	private AccountingClientAccountBalance(Integer clientInvestmentAccountId, boolean aggregateAcrossHoldingAccounts, BigDecimal clientAccountBalanceValue) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
		this.aggregateAcrossHoldingAccounts = aggregateAcrossHoldingAccounts;
		this.clientAccountBalanceValue = clientAccountBalanceValue;
	}


	/**
	 * Create an {@link AccountingClientAccountBalance} with an aggregated balance across holding accounts for a client {@link com.clifton.investment.account.InvestmentAccount}
	 */
	public static AccountingClientAccountBalance ofClientAccountWithAggregateBalance(Integer clientInvestmentAccountId, BigDecimal balanceValue) {
		return new AccountingClientAccountBalance(clientInvestmentAccountId, true, balanceValue);
	}


	/**
	 * Create an {@link AccountingClientAccountBalance} where balances will exist for at least one holding account for a client {@link com.clifton.investment.account.InvestmentAccount}
	 */
	public static AccountingClientAccountBalance ofClientAccountWithHoldingAccountBalances(Integer clientInvestmentAccountId) {
		return new AccountingClientAccountBalance(clientInvestmentAccountId, true, BigDecimal.ZERO);
	}


	/**
	 * Add a holding account balance for the client account.
	 */
	public void addHoldingAccountBalance(Integer holdingAccountId, BigDecimal balanceValue) {
		if (this.holdingInvestmentAccountBalanceMap == null) {
			this.holdingInvestmentAccountBalanceMap = new HashMap<>();
		}
		this.holdingInvestmentAccountBalanceMap.put(holdingAccountId, balanceValue);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public boolean isAggregateAcrossHoldingAccounts() {
		return this.aggregateAcrossHoldingAccounts;
	}


	public Map<Integer, BigDecimal> getClientAccountHoldingAccountBalanceValueMap() {
		return this.holdingInvestmentAccountBalanceMap;
	}


	public BigDecimal getClientAccountBalanceValueForHoldingAccountId(Integer holdingInvestmentAccountId) {
		return this.holdingInvestmentAccountBalanceMap.get(holdingInvestmentAccountId);
	}


	/**
	 * Returns the balance for this client account. If {@link #isAggregateAcrossHoldingAccounts()} is false, the value returned
	 * is the aggregated value of all holding account balances from {@link #getClientAccountHoldingAccountBalanceValueMap()}.
	 */
	public BigDecimal getClientAccountBalanceValue() {
		return isAggregateAcrossHoldingAccounts() ? this.clientAccountBalanceValue
				: CoreMathUtils.sum(this.holdingInvestmentAccountBalanceMap.values());
	}
}
