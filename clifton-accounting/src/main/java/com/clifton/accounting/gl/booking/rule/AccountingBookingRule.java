package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;

import java.util.Collections;
import java.util.Set;


/**
 * The <code>AccountingBookingRule</code> interface is implemented by booking rules that generate/update
 * unique transaction sets (accounting journal details) for entities being booked.
 * <p/>
 * Booking process may use more than one booking rule in order to generate accurate transaction sets. The order of rules is very important.
 * The same booking rule may be used by more than one booking process.
 * <p/>
 * Example (1 rule): create 2 journal details for a simple journal
 * Example (3 rules): create journal details for a trade; close existing positions if necessary; calculate and set commission and commission journal details
 *
 * @param <T>
 * @author akorver
 * @author vgomelsky
 */
public interface AccountingBookingRule<T extends BookableEntity> {


	/**
	 * Defines the scope(s) that this rule should be applicable to. Default implementation returns an empty set: skip the rule.
	 * Rule scopes are used only during preview functionality.  When adding a new rule, carefully review all scopes and configure where applicable.
	 */
	public default Set<BookingRuleScopes> getRuleScopes() {
		return Collections.emptySet();
	}


	/**
	 * Applies this rule to the specified booking context. The rule usually adds new journal details to context's journal.
	 * It can also modify existing details (position closing), etc.
	 *
	 * @param bookingSession
	 */
	public void applyRule(BookingSession<T> bookingSession);
}
