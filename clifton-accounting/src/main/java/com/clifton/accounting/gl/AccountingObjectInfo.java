package com.clifton.accounting.gl;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.investment.instrument.InvestmentObjectInfo;


/**
 * The <code>AccountingObjectInfo</code> interface defines common attributes used by accounting objects.
 *
 * @author vgomelsky
 */
public interface AccountingObjectInfo extends InvestmentObjectInfo {

	public AccountingAccount getAccountingAccount();
}
