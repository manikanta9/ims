package com.clifton.accounting.gl;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>AccountingTransactionInvestmentAccountRelationshipValidator</code>
 * <p/>
 * On deletes of InvestmentAccountRelationships, checks to make sure there are no transactions
 * for the client - related account relationships.
 * <p/>
 * If the related account, is OurAccount = TRUE, will only check
 *
 * @author Mary Anderson
 */
@Component
public class AccountingTransactionInvestmentAccountRelationshipValidator extends SelfRegisteringDaoValidator<InvestmentAccountRelationship> {

	private SqlHandler sqlHandler;

	/**
	 * Returns date of last transaction, else null
	 * If a date is returned, relationship cannot be deleted, but must be de-activated, otherwise if null the relationship can be deleted (not used or another relationship for same account will cover those transactions)
	 */
	private static final String HOLDING_ACCOUNT_DELETE_RELATIONSHIP_VALIDATION_SQL = //
			"SELECT CAST(MAX(tr.TransactionDate) AS DATETIME) FROM InvestmentAccountRelationship r "
					+ " INNER JOIN InvestmentAccountRelationshipPurpose p ON r.InvestmentAccountRelationshipPurposeID = p.InvestmentAccountRelationshipPurposeID "
					+ " LEFT JOIN InvestmentType t ON p.InvestmentTypeID = t.InvestmentTypeID "
					+ " INNER JOIN AccountingTransaction tr ON tr.ClientInvestmentAccountID = r.MainAccountID AND tr.HoldingInvestmentAccountID = r.RelatedAccountID "
					+ " INNER JOIN InvestmentSecurity s ON tr.InvestmentSecurityID = s.InvestmentSecurityID " + " INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID "
					+ " INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID "
					+ " INNER JOIN InvestmentType tp ON h.InvestmentTypeID = tp.InvestmentTypeID AND (p.InvestmentTypeID IS NULL OR p.InvestmentTypeID = tp.InvestmentTypeID) "
					+ " WHERE r.InvestmentAccountRelationshipID = ? " + " AND NOT EXISTS ( " + " SELECT * " + " FROM InvestmentAccountRelationship r2 "
					+ " INNER JOIN InvestmentAccountRelationshipPurpose p2 ON r2.InvestmentAccountRelationshipPurposeID = p2.InvestmentAccountRelationshipPurposeID "
					+ " WHERE r2.InvestmentAccountRelationshipID <> r.InvestmentAccountRelationshipID " + " AND r2.RelatedAccountID = r.RelatedAccountID " + " AND r2.MainAccountID = r.MainAccountID "
					+ " AND COALESCE(p2.InvestmentTypeID,'') = COALESCE(p.InvestmentTypeID,'') " + " ) ";


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.DELETE;
	}


	@Override
	public void validate(InvestmentAccountRelationship bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		// TODO: only look at transactions posted after relationship create date??? CAST(MAX(tr.TransactionDate)

		// validate only relationships FROM our account
		if (bean.getReferenceOne().getType().isOurAccount()) {
			if (bean.getReferenceTwo().getType().isOurAccount()) {
				// client account to client account relationship (parent to child: sub-accounts)
				Date result = getSqlHandler().queryForDate(new SqlSelectCommand(
						"SELECT CAST(MAX(TransactionDate) AS DATETIME) FROM AccountingTransaction WHERE ClientInvestmentAccountID = ? AND IsDeleted = 0")
						.addIntegerParameterValue(bean.getReferenceTwo().getId())
				);
				if (result != null && DateUtils.compare(result, bean.getCreateDate(), false) < 0) {
					throw new ValidationException(
							"Cannot delete relationship to account ["
									+ bean.getReferenceTwo().getLabel()
									+ "] because there are existing transactions in the system after this relationship was created.  If this relationship is no longer valid, please enter an end date to deactivate it.");
				}
			}
			else {
				// client account to holding account relationship
				Date result = getSqlHandler().queryForDate(new SqlSelectCommand(HOLDING_ACCOUNT_DELETE_RELATIONSHIP_VALIDATION_SQL).addIntegerParameterValue(bean.getId()));
				if (result != null) {
					throw new ValidationException("Cannot delete relationship to account [" + bean.getReferenceTwo().getLabel()
							+ "] because there are existing transactions in the system.  If this relationship is no longer valid, please enter an end date to deactivate it after "
							+ DateUtils.fromDateShort(result) + ".");
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
