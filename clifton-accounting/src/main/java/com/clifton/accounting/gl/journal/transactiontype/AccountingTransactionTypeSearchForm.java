package com.clifton.accounting.gl.journal.transactiontype;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author theodorez
 */
public class AccountingTransactionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description", sortField = "displayOrder")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short idNotEquals;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	@SearchField
	private String description;

	@SearchField
	private String name;

	@SearchField
	private Boolean restrictToOtcHierarchy;

	@SearchField
	private Boolean commissionApplyMethodApplicable;

	@SearchField
	private Boolean transfer;


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short getIdNotEquals() {
		return this.idNotEquals;
	}


	public void setIdNotEquals(Short idNotEquals) {
		this.idNotEquals = idNotEquals;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getRestrictToOtcHierarchy() {
		return this.restrictToOtcHierarchy;
	}


	public void setRestrictToOtcHierarchy(Boolean restrictToOtcHierarchy) {
		this.restrictToOtcHierarchy = restrictToOtcHierarchy;
	}


	public Boolean getCommissionApplyMethodApplicable() {
		return this.commissionApplyMethodApplicable;
	}


	public void setCommissionApplyMethodApplicable(Boolean commissionApplyMethodApplicable) {
		this.commissionApplyMethodApplicable = commissionApplyMethodApplicable;
	}


	public Boolean getTransfer() {
		return this.transfer;
	}


	public void setTransfer(Boolean transfer) {
		this.transfer = transfer;
	}
}
