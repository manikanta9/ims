package com.clifton.accounting.gl.receivable.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.AssertUtils;

import java.util.Date;


@SearchForm(hasOrmDtoClass = false)
public class AccountsReceivableAmountSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "settlementDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date maxSettlementDate;

	@SearchField(searchField = "transactionDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date maxTransactionDate;

	@SearchField(searchField = "tradingSecurity.id")
	private Integer tradingSecurityId;


	public static AccountsReceivableAmountSearchForm withTradingSecurityAndMaxSettlementDate(Integer tradingSecurityId, Date maxSettlementDate) {
		AssertUtils.assertNotNull(tradingSecurityId, "Trading Security is required.");
		AssertUtils.assertNotNull(maxSettlementDate, "Maximum Settlement Date is required.");

		AccountsReceivableAmountSearchForm searchForm = new AccountsReceivableAmountSearchForm();
		searchForm.setTradingSecurityId(tradingSecurityId);
		searchForm.setMaxSettlementDate(maxSettlementDate);
		return searchForm;
	}


	public static AccountsReceivableAmountSearchForm withTradingSecurityAndMaxTransactionDate(Integer tradingSecurityId, Date maxTransactionDate) {
		AssertUtils.assertNotNull(tradingSecurityId, "Trading Security is required.");
		AssertUtils.assertNotNull(maxTransactionDate, "Maximum Transaction Date is required.");

		AccountsReceivableAmountSearchForm searchForm = new AccountsReceivableAmountSearchForm();
		searchForm.setTradingSecurityId(tradingSecurityId);
		searchForm.setMaxTransactionDate(maxTransactionDate);
		return searchForm;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Date getMaxSettlementDate() {
		return this.maxSettlementDate;
	}


	public void setMaxSettlementDate(Date maxSettlementDate) {
		this.maxSettlementDate = maxSettlementDate;
	}


	public Date getMaxTransactionDate() {
		return this.maxTransactionDate;
	}


	public void setMaxTransactionDate(Date maxTransactionDate) {
		this.maxTransactionDate = maxTransactionDate;
	}


	public Integer getTradingSecurityId() {
		return this.tradingSecurityId;
	}


	public void setTradingSecurityId(Integer tradingSecurityId) {
		this.tradingSecurityId = tradingSecurityId;
	}
}
