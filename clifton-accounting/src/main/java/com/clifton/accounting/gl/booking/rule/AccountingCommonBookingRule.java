package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionCommand;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetriever;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>AccountingCommonBookingRule</code> class should be extended by most/all booking rules as it provides helpful methods/properties that most rules need.
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class AccountingCommonBookingRule<T extends BookableEntity> {

	protected static final String EXPENSE_DESCRIPTION_SEGMENT = " expense from ";
	protected static final String PROCEEDS_DESCRIPTION_SEGMENT = " proceeds from ";

	protected static final String OPENING_DESCRIPTION_SEGMENT = "opening of ";
	protected static final String CLOSING_DESCRIPTION_SEGMENT = "close of ";

	private AccountingAccountService accountingAccountService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingPositionService accountingPositionService;
	private AccountingBookingPositionRetriever accountingBookingPositionRetriever;

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, then existing lookup will use Settlement vs Transaction date. False by default.
	 */
	private boolean lookupUsingSettlementDate;

	/**
	 * When multiple open lots exist, use the following order when deciding which one to close first.
	 */
	private AccountingPositionOrders closingOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.getClass().getName() + ": lookupUsingSettlementDate=" + isLookupUsingSettlementDate() + ", closingOrder=" + getClosingOrder();
	}


	/**
	 * Creates the offsetting Cash or Currency journal detail for the specified journal detail. Usually a payment for or from a position, commission, etc.
	 * <p>
	 * If the specified detail security has the same currency denomination as client account, then Cash entry is created. Otherwise, a Currency entry is created.
	 */
	protected AccountingJournalDetail createCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry) {
		return createCashCurrencyRecord(oppositeEntry, oppositeEntry.getInvestmentSecurity().getInstrument().getTradingCurrency());
	}


	/**
	 * Creates the offsetting Cash or Currency journal detail for the specified journal detail. Usually a payment for or from a position, commission, etc.
	 * <p>
	 * If the specified detail security has the same currency denomination as client account, then Cash entry is created. Otherwise, a Currency entry is created.
	 *
	 * @param oppositeEntry
	 * @param settlementCurrency currency to use for the record created (usually the same as oppositeEntry security denomination)
	 */
	protected AccountingJournalDetail createCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry, InvestmentSecurity settlementCurrency) {
		BigDecimal exchangeRateToBase = oppositeEntry.getExchangeRateToBase();
		InvestmentSecurity localCurrency = oppositeEntry.getInvestmentSecurity().getInstrument().getTradingCurrency();
		if (!settlementCurrency.equals(localCurrency)) {
			// need to lookup a rate from Settlement Currency to Base Currency as opposed to from Local Currency to Base Currency that is passed
			InvestmentSecurity clientBaseCurrency = InvestmentUtils.getClientAccountBaseCurrency(oppositeEntry);
			if (settlementCurrency.equals(clientBaseCurrency)) {
				exchangeRateToBase = BigDecimal.ONE;
			}
			else {
				exchangeRateToBase = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(oppositeEntry.fxSourceCompany(), !oppositeEntry.fxSourceCompanyOverridden(), settlementCurrency.getSymbol(), clientBaseCurrency.getSymbol(), oppositeEntry.getTransactionDate()).flexibleLookup());
			}
		}

		return createCashCurrencyRecord(oppositeEntry, settlementCurrency, oppositeEntry.getLocalDebitCredit().negate(), exchangeRateToBase);
	}


	/**
	 * Creates the offsetting Cash or Currency journal detail for the specified journal detail. Usually a payment for or from a position, commission, etc.
	 * <p>
	 * If the specified detail security has the same currency denomination as client account, then Cash entry is created. Otherwise, a Currency entry is created.
	 *
	 * @param oppositeEntry
	 * @param settlementCurrency currency to use for the record created (usually the same as oppositeEntry security denomination)
	 * @param localDebitCredit   negated localDebitCredit of the oppositeEntry. However, in some rare cases when 3 currencies are involved, could be different amount
	 * @param exchangeRateToBase from the settlementCurrency
	 */
	protected AccountingJournalDetail createCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry, InvestmentSecurity settlementCurrency, BigDecimal localDebitCredit, BigDecimal exchangeRateToBase) {
		InvestmentSecurity clientBaseCurrency = InvestmentUtils.getClientAccountBaseCurrency(oppositeEntry);
		InvestmentSecurity localCurrency = oppositeEntry.getInvestmentSecurity().getInstrument().getTradingCurrency();
		boolean cashRecord = clientBaseCurrency.equals(settlementCurrency);
		boolean settleInLocal = settlementCurrency.equals(localCurrency);
		boolean oppositeEntryIsCurrency = (!oppositeEntry.getAccountingAccount().isCash() && oppositeEntry.getAccountingAccount().isCurrency());

		AccountingJournalDetail result = newCashCurrencyRecord(oppositeEntry);
		result.setExchangeRateToBase(exchangeRateToBase);
		if (settlementCurrency.equals(localCurrency)) {
			result.setLocalDebitCredit(localDebitCredit);
			result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), exchangeRateToBase, oppositeEntry.getClientInvestmentAccount()));
		}
		else {
			result.setBaseDebitCredit(oppositeEntry.getBaseDebitCredit().negate());
			result.setLocalDebitCredit(InvestmentCalculatorUtils.calculateLocalAmount(result.getBaseDebitCredit(), exchangeRateToBase, settlementCurrency));
		}

		if (oppositeEntryIsCurrency) {
			result.setExchangeRateToBase(BigDecimal.ONE);
			result.setLocalDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), oppositeEntry.getPrice() == null ? BigDecimal.ONE : exchangeRateToBase, oppositeEntry.getClientInvestmentAccount()));
			result.setBaseDebitCredit(result.getLocalDebitCredit());
		}

		// check if there was a penny lost during FX conversion
		BigDecimal diff = MathUtils.add(result.getBaseDebitCredit(), oppositeEntry.getBaseDebitCredit());
		if (!MathUtils.isNullOrZero(diff)) {
			if (MathUtils.isLessThan(diff.abs(), new BigDecimal("0.02"))) {
				result.setBaseDebitCredit(oppositeEntry.getBaseDebitCredit().negate());
			}
			else {
				throw new ValidationException("Base Debit/Credit difference is " + diff + " cash/currency offset for " + oppositeEntry);
			}
		}

		if (cashRecord || oppositeEntryIsCurrency) {
			// CASH
			result.setAccountingAccount(getCashAccountingAccount(oppositeEntry));
			result.setInvestmentSecurity(clientBaseCurrency);
			if (!settleInLocal) {
				// a foreign trade that is settled in client base currency
				ValidationUtils.assertTrue(oppositeEntry.getInvestmentSecurity().getInstrument().getHierarchy().isOtc()
						|| InvestmentUtils.isSecurityOfType(oppositeEntry.getInvestmentSecurity(), InvestmentType.SWAPS), "Only Swaps and OTC securities can settle in non-local currency: "
						+ oppositeEntry.getInvestmentSecurity().getSymbol());
				result.setExchangeRateToBase(BigDecimal.ONE);
				result.setLocalDebitCredit(result.getBaseDebitCredit());
			}
		}
		else {
			// CURRENCY
			result.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY));
			result.setInvestmentSecurity(settlementCurrency);
		}
		updateCashCurrencyRecordDescription(result, true);

		return result;
	}


	protected AccountingJournalDetail newCashCurrencyRecord(AccountingJournalDetailDefinition oppositeEntry) {
		AccountingJournalDetail result = new AccountingJournalDetail();
		BeanUtils.copyProperties(oppositeEntry, result, new String[]{"parentTransaction"});
		// if the opposite entries parent is null, it must be the parent itself.  Otherwise, the oppositeEntry is a child, so use its parent as the cash's parent.
		result.setParentDefinition(oppositeEntry);
		if (oppositeEntry.getParentDefinition() != null && !(oppositeEntry.getAccountingAccount().isPosition() && oppositeEntry.isClosing() && oppositeEntry.getParentDefinition().isOpening() && oppositeEntry.getJournal().equals(oppositeEntry.getParentDefinition().getJournal()))) {
			// do not use grandparent for same journal position closings (for example, fractional Spinoff lot lot closing on Stock Spinoff with Cash in Lieu
			result.setParentDefinition(oppositeEntry.getParentDefinition());
		}
		else if (oppositeEntry.getAccountingAccount().isReceivable() && oppositeEntry.getParentTransaction() != null && oppositeEntry.getParentTransaction().getAccountingAccount().isPosition() && oppositeEntry.getParentTransaction().isOpening()) {
			// instead of referencing Receivable, reference corresponding opening Position
			result.setParentTransaction(oppositeEntry.getParentTransaction());
		}
		else if (!oppositeEntry.getAccountingAccount().isPosition() && oppositeEntry.getParentTransaction() != null && oppositeEntry.getParentTransaction().getAccountingAccount().isPosition()) {
			// instead of referencing a non-position, reference the corresponding position of the referenced detail
			result.setParentTransaction(oppositeEntry.getParentTransaction());
		}

		result.setHoldingInvestmentAccount(getInvestmentSecurityUtilHandler().getCashLocationHoldingAccount(result));

		result.setQuantity(null);
		result.setPrice(null);
		result.setPositionCostBasis(BigDecimal.ZERO);
		result.setPositionCommission(BigDecimal.ZERO);
		result.setOpening(true);

		return result;
	}


	/**
	 * Automatically generates Cash or Currency record description field with the message that accurately describes what happened: open/close, expense/proceeds, etc.
	 *
	 * @param cashCurrencyEntry
	 * @param force             always set description vs only update if it's a standard description (generated by this method)
	 */
	protected void updateCashCurrencyRecordDescription(AccountingJournalDetailDefinition cashCurrencyEntry, boolean force) {
		boolean hasParent = cashCurrencyEntry.getParentDefinition() != null;
		boolean opening = !hasParent || cashCurrencyEntry.getParentDefinition().isOpening();
		String symbol = hasParent ? cashCurrencyEntry.getParentDefinition().getInvestmentSecurity().getSymbol() : cashCurrencyEntry.getInvestmentSecurity().getSymbol();
		String d = cashCurrencyEntry.getDescription();
		if (StringUtils.isEmpty(d)) {
			force = true;
		}

		String prefix = InvestmentUtils.isSecurityEqualToClientAccountBaseCurrency(cashCurrencyEntry) ? AccountingAccount.ASSET_CASH : AccountingAccount.ASSET_CURRENCY;
		String expensePrefix = prefix + EXPENSE_DESCRIPTION_SEGMENT;
		String proceedsPrefix = prefix + PROCEEDS_DESCRIPTION_SEGMENT;
		if (force || (d.startsWith(expensePrefix) || d.startsWith(proceedsPrefix))) {
			cashCurrencyEntry.setDescription(prefix + (MathUtils.isNegative(cashCurrencyEntry.getLocalDebitCredit()) ? EXPENSE_DESCRIPTION_SEGMENT : PROCEEDS_DESCRIPTION_SEGMENT));
			String suffix = (opening ? OPENING_DESCRIPTION_SEGMENT : CLOSING_DESCRIPTION_SEGMENT) + symbol;
			if (!StringUtils.isEmpty(d)) {
				if (d.startsWith(expensePrefix)) {
					d = d.substring(expensePrefix.length());
					if (!(d.startsWith(OPENING_DESCRIPTION_SEGMENT) || d.startsWith(CLOSING_DESCRIPTION_SEGMENT))) {
						suffix = d;
					}
				}
				else if (d.startsWith(proceedsPrefix)) {
					d = d.substring(proceedsPrefix.length());
					if (!(d.startsWith(OPENING_DESCRIPTION_SEGMENT) || d.startsWith(CLOSING_DESCRIPTION_SEGMENT))) {
						suffix = d;
					}
				}
			}
			cashCurrencyEntry.setDescription(cashCurrencyEntry.getDescription() + suffix);
		}
	}


	protected String getCashCurrencyRecordDescriptionPrefix(AccountingJournalDetail cashCurrencyDetail) {
		return cashCurrencyDetail.getAccountingAccount().getName() + (MathUtils.isNegative(cashCurrencyDetail.getLocalDebitCredit()) ? EXPENSE_DESCRIPTION_SEGMENT : PROCEEDS_DESCRIPTION_SEGMENT);
	}


	protected AccountingAccount getCashAccountingAccount(AccountingJournalDetailDefinition oppositeEntry) {
		if (oppositeEntry.getAccountingAccount().getCashAccount() != null) {
			return oppositeEntry.getAccountingAccount().getCashAccount();
		}
		return getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH);
	}


	/**
	 * Returns a List of BookingPosition objects for currently open positions that corresponding to the specified journal detail. Retrieves the list from the bookingSession and if
	 * it's not there yet, then from the General Ledger and stores it in the booking session.
	 */
	protected List<BookingPosition> getCurrentOpenPositions(BookingSession<T> bookingSession, AccountingJournalDetailDefinition journalDetail) {
		List<BookingPosition> currentOpenPositions;
		Map<String, List<BookingPosition>> positionsByClient = bookingSession.getCurrentPositions();
		String key = bookingSession.getPositionKey(journalDetail);
		if (positionsByClient.containsKey(key)) {
			currentOpenPositions = positionsByClient.get(key);
		}
		else {
			AccountingPositionOrders selectedPositionOrder = getClosingOrder();
			if (selectedPositionOrder == null && bookingSession.getBookableEntity() != null) {
				AccountingClosingMethods closingMethod = bookingSession.getBookableEntity().getAccountingClosingMethod();
				if (closingMethod != null) {
					selectedPositionOrder = AccountingPositionOrders.valueOf(closingMethod.name());
				}
			}
			AccountingBookingPositionCommand command = AccountingBookingPositionCommand.of(journalDetail, selectedPositionOrder, isLookupUsingSettlementDate(), false);
			currentOpenPositions = getAccountingBookingPositionRetriever().getAccountingBookingOpenPositionList(command);
			positionsByClient.put(key, currentOpenPositions);
		}
		return currentOpenPositions;
	}


	/**
	 * Will add a previously looked up position list for a specified journal detail.  This is used by TradeFill booking to control the lots that
	 * will be closed.
	 */
	protected void replaceCurrentOpenPositionListToBookingSession(BookingSession<T> bookingSession, List<BookingPosition> openPositionList, AccountingJournalDetailDefinition journalDetail) {
		String positionKey = bookingSession.getPositionKey(journalDetail);
		bookingSession.getCurrentPositions().put(positionKey, openPositionList);
	}


	/**
	 * Returns BookingPosition that the specified journalDetail closes (journalDetail.parentTransaction == bookingPosition.position). Loads currently open positions into the
	 * specified bookingSession if they're not there already.
	 */
	protected BookingPosition getCurrentPositionBeingClosed(BookingSession<T> bookingSession, AccountingJournalDetailDefinition journalDetail) {
		// load (if not already in the session) open positions first
		List<BookingPosition> positionList = getCurrentOpenPositions(bookingSession, journalDetail);
		if (!CollectionUtils.isEmpty(positionList)) {
			return bookingSession.getBookingPosition(journalDetail);
		}
		return null;
	}


	/**
	 * Filters the "actual" position details and returns them as a new list. This is primarily used in the case of Asset-Backed Bonds (ABS) and Credit Default Swaps (CDS).
	 * When a transaction such as a trade occurs with a factor specified, this results results in the splitting of the position(s). This method will return the "actual"
	 * position details to use when processing accrued interest, commissions and fees, etc.
	 * <p>
	 * For example, this method will filter the 4 transactions below so the only remaining transaction is -14 (the first one).<br/>
	 * * Exclude full close - The second on (-15) is filtered out by checking for closing detail with an opening parent transaction whose price matches the parent transaction.<br/>
	 * * Exclude new opening - The second on (-16) is filtered out by checking if the detail is an open with a parent transaction, and any other transaction in the journal shares the parent or grand parent.<br/>
	 * * Exclude Reduction - All reductions are exclude (-17) by requiring that parentDefinition is not set.
	 * <br/>
	 * -14	3583624	100000	H-200	Position	810RI20902595	101	940,000	-9,400	07/24/2017	1	-9,400	-9,400	07/24/2017	07/27/2017	Partial position close  at Original Notional for 810RI20902595<br/>
	 * -15	3583624	100000	H-200	Position	810RI20902595	102.5	18,988,000	-474,700	07/24/2017	1	-474,700	-474,700	01/10/2013	07/27/2017	Full position close  at new Original Notional for 810RI20902595<br/>
	 * -16	2846031	100000	H-200	Position	810RI20902595	102.5	-20,200,000	505,000	07/24/2017	1	505,000	505,000	01/10/2013	07/27/2017	Position opening at new Original Notional for 810RI20902595<br/>
	 * -17	-16	100000	H-200	Position	810RI20902595	102.5	1,212,000	-30,300	07/24/2017	1	-30,300	-30,300	01/10/2013	07/27/2017	Reducing Original Notional to Current Notional for 810RI20902595<br/>
	 *
	 * @param details the list of details to filter out
	 * @return the filtered list of position details
	 */
	protected List<AccountingJournalDetailDefinition> filterActualPositionDetails(List<? extends AccountingJournalDetailDefinition> details) {
		return details.stream().filter(detail ->
				!detail.isFactorChangeQuantityAdjustment()
						&& detail.getAccountingAccount().isPosition()
						&& !detail.getAccountingAccount().isReceivable()
						&& detail.isParentDefinitionEmpty()).collect(Collectors.toList()
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public void setAccountingBookingPositionRetriever(AccountingBookingPositionRetriever accountingBookingPositionRetriever) {
		this.accountingBookingPositionRetriever = accountingBookingPositionRetriever;
	}


	public AccountingBookingPositionRetriever getAccountingBookingPositionRetriever() {
		return this.accountingBookingPositionRetriever;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public boolean isLookupUsingSettlementDate() {
		return this.lookupUsingSettlementDate;
	}


	public void setLookupUsingSettlementDate(boolean lookupUsingSettlementDate) {
		this.lookupUsingSettlementDate = lookupUsingSettlementDate;
	}


	public AccountingPositionOrders getClosingOrder() {
		return this.closingOrder;
	}


	public void setClosingOrder(AccountingPositionOrders closingOrder) {
		this.closingOrder = closingOrder;
	}
}
