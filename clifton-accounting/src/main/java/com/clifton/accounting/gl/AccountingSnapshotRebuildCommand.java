package com.clifton.accounting.gl;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;

import java.util.Date;


/**
 * The <code>AccountingSnapshotRebuildCommand</code> is a common abstract class that can be used by snapshot rebuilds that rebuild for a date range
 * Common functionality involves snapshot dates and by default not rebuilding in the future.
 *
 * @author manderson
 */
public abstract class AccountingSnapshotRebuildCommand {

	/**
	 * When no additional filters are present, the most a user can rebuild for is 5 business days (unless an administrator)
	 */
	private static final int GLOBAL_MAX_REBUILD_BUSINESS_DAYS = 5;


	// Only one of these applies at a time, if all null - rebuilds all client accounts
	private Integer clientAccountId;
	private Integer investmentAccountGroupId;


	// Specific date or date range
	private Date startSnapshotDate;
	private Date endSnapshotDate;

	// If we allow rebuilding for today or in the future - if not the dates will be pushed back
	private boolean allowTodaySnapshots = false;
	private boolean allowFutureSnapshots = false; // Even if true, there is still a max allowed of 60 days in the future and exception is thrown in this case

	/**
	 * If true, allows to "fail" silently and will just skip the future dates rather than throw an exception
	 * This is used for cases like Performance Summaries where we just call to rebuild for the current month but expect it to stop on previous weekday
	 * But on the screen where users explicitly select an end date we want to tell them why something isn't being rebuilt
	 */
	private boolean doNotThrowExceptionForFutureSnapshots;

	// Used by historical jobs to track total snapshots processed
	private int resultProcessedSnapshots = 0;

	// Specifies whether rebuild should be executed asynchronously (runners) or synchronously.
	private boolean synchronous = false;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private Status status;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates snapshot dates and global rebuild limits
	 */
	public void validate(CalendarBusinessDayService calendarBusinessDayService, SecurityAuthorizationService securityAuthorizationService) {
		validateSnapshotDates();
		validateGlobalRebuild(calendarBusinessDayService, securityAuthorizationService);
	}


	/**
	 * Validates Snapshot dates
	 * For endSnapshotDate value will validate and either throw exception (if throwExceptionForFutureSnapshots = true) or reset endSnapshotDate to max allowed date
	 */
	protected void validateSnapshotDates() {
		ValidationUtils.assertNotNull(getStartSnapshotDate(), "Start snapshot date is required", "startSnapshotDate");
		ValidationUtils.assertNotNull(getEndSnapshotDate(), "End snapshot date is required", "endSnapshotDate");

		// Still limiting to 60 days in the future
		Date maxEndDate;
		if (isAllowFutureSnapshots()) {
			ValidationUtils.assertTrue(DateUtils.getDaysDifference(getEndSnapshotDate(), new Date()) <= 60, "Snapshot End Date cannot be more than 60 days in the future.", "endSnapshotDate");
			maxEndDate = null;
		}
		else if (isAllowTodaySnapshots()) {
			maxEndDate = DateUtils.clearTime(new Date());
		}
		else {
			maxEndDate = DateUtils.addDays(DateUtils.clearTime(new Date()), -1);
		}

		if (maxEndDate != null && DateUtils.isDateAfter(getEndSnapshotDate(), maxEndDate)) {
			if (isDoNotThrowExceptionForFutureSnapshots()) {
				setEndSnapshotDate(maxEndDate);
			}
			else {
				throw new FieldValidationException("Cannot enter an end date after " + DateUtils.fromDateShort(maxEndDate) + " because the option to build snapshots " + (isAllowTodaySnapshots() ? "after" : "on or after") + " today is not set.", "endSnapshotDate");
			}
		}
	}


	/**
	 * Validates global rebuilds
	 */
	protected void validateGlobalRebuild(CalendarBusinessDayService calendarBusinessDayService, SecurityAuthorizationService securityAuthorizationService) {
		if (getClientAccountId() == null && getInvestmentAccountGroupId() == null) {
			if (!securityAuthorizationService.isSecurityUserAdmin()) {
				ValidationUtils.assertTrue(Math.abs(calendarBusinessDayService.getBusinessDaysBetween(getEndSnapshotDate(), getStartSnapshotDate(), null)) <= GLOBAL_MAX_REBUILD_BUSINESS_DAYS, String.format("Rebuilding more than %d day(s) of snapshots is not allowed unless you are an administrator.", GLOBAL_MAX_REBUILD_BUSINESS_DAYS), "endSnapshotDate");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	public void setSnapshotDate(Date date) {
		setStartSnapshotDate(date);
		setEndSnapshotDate(date);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Date getStartSnapshotDate() {
		return this.startSnapshotDate;
	}


	public void setStartSnapshotDate(Date startSnapshotDate) {
		this.startSnapshotDate = startSnapshotDate;
	}


	public Date getEndSnapshotDate() {
		return this.endSnapshotDate;
	}


	public void setEndSnapshotDate(Date endSnapshotDate) {
		this.endSnapshotDate = endSnapshotDate;
	}


	public boolean isAllowTodaySnapshots() {
		return this.allowTodaySnapshots;
	}


	public void setAllowTodaySnapshots(boolean allowTodaySnapshots) {
		this.allowTodaySnapshots = allowTodaySnapshots;
	}


	public boolean isAllowFutureSnapshots() {
		return this.allowFutureSnapshots;
	}


	public void setAllowFutureSnapshots(boolean allowFutureSnapshots) {
		this.allowFutureSnapshots = allowFutureSnapshots;
	}


	public boolean isDoNotThrowExceptionForFutureSnapshots() {
		return this.doNotThrowExceptionForFutureSnapshots;
	}


	public void setDoNotThrowExceptionForFutureSnapshots(boolean doNotThrowExceptionForFutureSnapshots) {
		this.doNotThrowExceptionForFutureSnapshots = doNotThrowExceptionForFutureSnapshots;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public int getResultProcessedSnapshots() {
		return this.resultProcessedSnapshots;
	}


	public void setResultProcessedSnapshots(int resultProcessedSnapshots) {
		this.resultProcessedSnapshots = resultProcessedSnapshots;
	}
}
