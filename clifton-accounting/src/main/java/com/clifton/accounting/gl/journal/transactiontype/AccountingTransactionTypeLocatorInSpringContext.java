package com.clifton.accounting.gl.journal.transactiontype;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class AccountingTransactionTypeLocatorInSpringContext implements AccountingTransactionTypeLocator, InitializingBean, ApplicationContextAware {

	/**
	 * Maps a journal type name (String) to a retriever
	 */
	private final Map<String, AccountingTransactionTypeRetriever<?>> retrieverMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public <T extends BookableEntity> AccountingTransactionType getAccountingTransactionType(AccountingJournalDetailDefinition detail, T bookableEntity) {
		String journalTypeName = detail.getJournal().getJournalType().getName();
		AccountingTransactionTypeRetriever retriever = getAccountingTransactionTypeRetriever(journalTypeName);

		//Attempt to determine the transaction type
		AccountingTransactionType transactionType = null;
		if (retriever != null) {
			transactionType = retriever.getAccountingTransactionType(detail, bookableEntity);
		}

		return transactionType;
	}


	@Override
	public AccountingTransactionTypeRetriever<?> getAccountingTransactionTypeRetriever(String journalTypeName) {
		return getRetrieverMap().get(journalTypeName);
	}


	@Override
	public void afterPropertiesSet() {
		//Reflectively get all beans of type AccountingTransactionTypeRetriever
		@SuppressWarnings("unchecked")
		Map<String, AccountingTransactionTypeRetriever<?>> beanMap = (Map<String, AccountingTransactionTypeRetriever<?>>) (Map<String, ?>) getApplicationContext().getBeansOfType(AccountingTransactionTypeRetriever.class);

		for (Map.Entry<String, AccountingTransactionTypeRetriever<?>> stringAccountingTransactionTypeRetrieverEntry : beanMap.entrySet()) {
			AccountingTransactionTypeRetriever<?> retriever = stringAccountingTransactionTypeRetrieverEntry.getValue();
			if (getRetrieverMap().containsKey(retriever.getJournalTypeName())) {
				throw new RuntimeException("Cannot register '" + stringAccountingTransactionTypeRetrieverEntry.getKey() + "' as a AccountingTransactionTypeRetriever for journal of type '" + retriever.getJournalTypeName()
						+ "' because this journal type already has a registered AccountingTransactionTypeRetriever.");
			}

			getRetrieverMap().put(retriever.getJournalTypeName(), retriever);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, AccountingTransactionTypeRetriever<?>> getRetrieverMap() {
		return this.retrieverMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
