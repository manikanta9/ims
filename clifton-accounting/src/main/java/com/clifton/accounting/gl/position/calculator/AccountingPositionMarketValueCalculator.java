package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.system.bean.SystemBeanType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link SystemBeanType} implementation of {@link AccountingPositionValueCalculator} which calculates market values for positions.
 * <p>
 * This calculator produces market values for positions when it is evaluated.
 *
 * @author MikeH
 */
@Component
public class AccountingPositionMarketValueCalculator implements AccountingPositionValueCalculator {

	private AccountingPositionHandler accountingPositionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculate(AccountingPosition position, Date snapshotDate) {
		return getAccountingPositionHandler().getAccountingPositionMarketValue(position, snapshotDate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}
}
