package com.clifton.accounting.gl.journal.transactiontype;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author theodorez
 */
@Service
public class AccountingTransactionTypeServiceImpl implements AccountingTransactionTypeService {

	private AdvancedUpdatableDAO<AccountingTransactionType, Criteria> accountingTransactionTypeDAO;

	private DaoNamedEntityCache<AccountingTransactionType> accountingTransactionTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////      AccountingTransactionType Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingTransactionType getAccountingTransactionType(short id) {
		return getAccountingTransactionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingTransactionType getAccountingTransactionTypeByName(String name) {
		return getAccountingTransactionTypeCache().getBeanForKeyValue(getAccountingTransactionTypeDAO(), name);
	}


	@Override
	public List<AccountingTransactionType> getAccountingTransactionTypeList(AccountingTransactionTypeSearchForm searchForm) {
		return getAccountingTransactionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public AccountingTransactionType saveAccountingTransactionType(AccountingTransactionType bean) {
		if (bean.getAdditionalScopeSystemTable() != null || !StringUtils.isEmpty(bean.getAdditionalScopeBeanIdPath())) {
			ValidationUtils.assertTrue(bean.getAdditionalScopeSystemTable() != null && !StringUtils.isEmpty(bean.getAdditionalScopeBeanIdPath()), "Both Scope System Table and Bean ID Path must be specified.");
		}

		return getAccountingTransactionTypeDAO().save(bean);
	}


	@Override
	public void deleteAccountingTransactionType(short id) {
		getAccountingTransactionTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                  Getters and Setters                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<AccountingTransactionType, Criteria> getAccountingTransactionTypeDAO() {
		return this.accountingTransactionTypeDAO;
	}


	public void setAccountingTransactionTypeDAO(AdvancedUpdatableDAO<AccountingTransactionType, Criteria> accountingTransactionTypeDAO) {
		this.accountingTransactionTypeDAO = accountingTransactionTypeDAO;
	}


	public DaoNamedEntityCache<AccountingTransactionType> getAccountingTransactionTypeCache() {
		return this.accountingTransactionTypeCache;
	}


	public void setAccountingTransactionTypeCache(DaoNamedEntityCache<AccountingTransactionType> accountingTransactionTypeCache) {
		this.accountingTransactionTypeCache = accountingTransactionTypeCache;
	}
}

