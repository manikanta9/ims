package com.clifton.accounting.gl.journal.action;

import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.action.processor.AccountingJournalActionProcessor;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;


/**
 * @author vgomelsky
 */
public class AccountingJournalAction extends NamedEntityWithoutLabel<Short> {

	private AccountingJournalType journalType;
	/**
	 * Optionally limits this action to a sub-set of journals of this type that have this condition evaluate to true.
	 * For example, one can limit actions to a specific trade or transfer type, specific service, security type, etc.
	 */
	private SystemCondition scopeSystemCondition;

	/**
	 * When the journal type and scope conditions are met, {@link AccountingJournalActionProcessor} will
	 * be processed or rolled back for the journal/entity being booked/unbooked.
	 */
	private SystemBean actionBean;
	/**
	 * When multiple actions are defined for the specified journal type, defines the order of execution.
	 */
	private short processingOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalType getJournalType() {
		return this.journalType;
	}


	public void setJournalType(AccountingJournalType journalType) {
		this.journalType = journalType;
	}


	public SystemCondition getScopeSystemCondition() {
		return this.scopeSystemCondition;
	}


	public void setScopeSystemCondition(SystemCondition scopeSystemCondition) {
		this.scopeSystemCondition = scopeSystemCondition;
	}


	public SystemBean getActionBean() {
		return this.actionBean;
	}


	public void setActionBean(SystemBean actionBean) {
		this.actionBean = actionBean;
	}


	public short getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(short processingOrder) {
		this.processingOrder = processingOrder;
	}
}
