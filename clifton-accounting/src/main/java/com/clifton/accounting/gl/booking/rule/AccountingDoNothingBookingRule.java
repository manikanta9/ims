package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;


/**
 * The <code>AccountingDoNothingBookingRule</code> class does exactly what the name implies: NOTHING
 *
 * @author vgomelsky
 */
public class AccountingDoNothingBookingRule implements AccountingBookingRule<BookableEntity> {

	@Override
	public void applyRule(@SuppressWarnings("unused") BookingSession<BookableEntity> bookingSession) {
		// DO NOTHING
	}
}
