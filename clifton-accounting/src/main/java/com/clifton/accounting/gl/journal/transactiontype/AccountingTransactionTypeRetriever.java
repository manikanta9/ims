package com.clifton.accounting.gl.journal.transactiontype;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;


/**
 * The <code>AccountingTransactionTypeRetriever</code> retrieves a {@link AccountingTransactionType} type based on
 * a Source Entity ID (sometimes called a FK Field ID).
 *
 * @author jgommels
 */
public interface AccountingTransactionTypeRetriever<T extends BookableEntity> {

	/**
	 * Returns a {@link } type based on a {@link BookableEntity}.
	 */
	public AccountingTransactionType getAccountingTransactionType(AccountingJournalDetailDefinition journalDetail, T bookableEntity);


	/**
	 * Returns the journal type name that this retriever returns relevant {@link AccountingTransactionType} types for
	 */
	public String getJournalTypeName();
}
