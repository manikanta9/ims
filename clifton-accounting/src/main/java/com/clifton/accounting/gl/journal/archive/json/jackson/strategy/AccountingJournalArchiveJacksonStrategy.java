package com.clifton.accounting.gl.journal.archive.json.jackson.strategy;

import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveEntity;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.util.CollectionUtils;

import java.util.Set;
import java.util.function.Supplier;


/**
 * <code>AccountingJournalArchiveJacksonStrategy</code> defines a JSON serialization strategy
 * for {@link AccountingJournalArchiveEntity} objects.
 *
 * @author NickK
 */
public class AccountingJournalArchiveJacksonStrategy extends JacksonStrategy {

	private static final Set<String> EXCLUDED_MIGRATION_PROPERTIES = CollectionUtils.createHashSet("createUserId", "createDate", "updateUserId", "updateDate", "rv", "workflowState", "workflowStatus", "tradeList");

	private static final AccountingJournalArchiveJacksonStrategy DEFAULT_INSTANCE = ((Supplier<AccountingJournalArchiveJacksonStrategy>) () -> {
		AccountingJournalArchiveJacksonStrategy strategy = new AccountingJournalArchiveJacksonStrategy();
		strategy.setMaxDepth(Integer.MAX_VALUE);
		return strategy;
	}).get();

	private static final AccountingJournalArchiveJacksonStrategy TEMPLATE_INSTANCE = ((Supplier<AccountingJournalArchiveJacksonStrategy>) () -> {
		AccountingJournalArchiveJacksonStrategy strategy = new AccountingJournalArchiveJacksonStrategy() {
			@Override
			public boolean isIncludeNullValues() {
				return true;
			}
		};
		strategy.setMaxDepth(Integer.MAX_VALUE);
		return strategy;
	}).get();


	/**
	 * Returns a singleton instance of this strategy which does not serialize null property values.
	 */
	public static AccountingJournalArchiveJacksonStrategy getInstance() {
		return DEFAULT_INSTANCE;
	}


	/**
	 * Returns a singleton instance of this strategy which does serialize null property values for template JSON.
	 */
	public static AccountingJournalArchiveJacksonStrategy getTemplateInstance() {
		return TEMPLATE_INSTANCE;
	}


	/**
	 * Only serialize DTO fields.
	 */
	@Override
	public boolean isMigration() {
		return true;
	}


	/**
	 * Use annotations to avoid cyclical reference errors.
	 */
	@Override
	public boolean isDisableAnnotations() {
		return false;
	}


	/**
	 * Serialize the entities as is without wrapping in a web data response.
	 */
	@Override
	public boolean isWrapInDataResponse() {
		return false;
	}


	@Override
	public Set<String> getPropertiesToExclude() {
		Set<String> propertiesToExclude = super.getPropertiesToExclude();
		if (propertiesToExclude == null) {
			propertiesToExclude = EXCLUDED_MIGRATION_PROPERTIES;
		}
		else {
			propertiesToExclude.addAll(EXCLUDED_MIGRATION_PROPERTIES);
		}
		return propertiesToExclude;
	}
}
