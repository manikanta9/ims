package com.clifton.accounting.gl.booking;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <code>BookableEntityLockProvider</code> provide synchronization parameters necessary for ensuring single threaded access to
 * a block of code.
 *
 * @see com.clifton.core.util.concurrent.synchronize.Synchronizable
 * @see com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler
 */
@Component
public class BookableEntityLockProvider<T extends BookableEntity> {

	public static final String SECURE_AREA_NAME = "ACCOUNTING-BOOKING";
	public static final int LOCK_WAIT_MS = 3000;
	public static final String LOCK_MESSAGE_PREFIX = "Accounting journal booking for";

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecureAreaName() {
		return SECURE_AREA_NAME;
	}


	public int getLockWaitMs() {
		return LOCK_WAIT_MS;
	}


	public String getLockMessagePrefix() {
		return LOCK_MESSAGE_PREFIX;
	}


	public String getLockMessage(T bookedEntity) {
		return LOCK_MESSAGE_PREFIX + " " + bookedEntity.getLabel();
	}


	public Set<String> getLockKeySet(T bookedEntity) {
		return CollectionUtils.getStream(bookedEntity.getEntityLockSet())
				.filter(Objects::nonNull)
				.map(object -> {
					@SuppressWarnings("unchecked")
					Class<IdentityObject> dtoClass = (Class<IdentityObject>) object.getClass();
					String tableName = getDaoLocator().locate(dtoClass).getConfiguration().getTable().getName();
					return tableName + "-" + object.getIdentity();
				})
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
