package com.clifton.accounting.gl.receivable;

import com.clifton.accounting.gl.receivable.search.AccountsReceivableAmountSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class AccountsReceivableAmountServiceImpl implements AccountsReceivableAmountService {

	private AdvancedReadOnlyDAO<AccountsReceivableAmount, Criteria> accountsReceivableAmountDAO;


	@Override
	@Transactional(readOnly = true)
	public List<AccountsReceivableAmount> getAccountsReceivableAmountList(final AccountsReceivableAmountSearchForm searchForm) {
		return getAccountsReceivableAmountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	public AdvancedReadOnlyDAO<AccountsReceivableAmount, Criteria> getAccountsReceivableAmountDAO() {
		return this.accountsReceivableAmountDAO;
	}


	public void setAccountsReceivableAmountDAO(AdvancedReadOnlyDAO<AccountsReceivableAmount, Criteria> accountsReceivableAmountDAO) {
		this.accountsReceivableAmountDAO = accountsReceivableAmountDAO;
	}
}
