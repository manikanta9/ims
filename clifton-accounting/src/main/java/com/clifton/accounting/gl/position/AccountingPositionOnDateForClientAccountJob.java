package com.clifton.accounting.gl.position;


import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>AccountingPositionOnDateForClientAccountJob</code> finds all active Client Accounts
 * without an inception date set and attempts to populate the date if it finds positions.
 *
 * @author manderson
 */
public class AccountingPositionOnDateForClientAccountJob implements Task {

	private AccountingPositionHandler accountingPositionHandler;
	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setInceptionDatePopulated(false);
		searchForm.setWorkflowStatusName(WorkflowStatus.STATUS_ACTIVE);
		// Exclude Virtual Clients/Client Groups
		searchForm.setExcludeClientCategoryType(BusinessClientCategoryTypes.VIRTUAL_CLIENT);
		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

		int recordsUpdated = 0;
		int recordsFailed = 0;
		List<String> skippedAccounts = new ArrayList<>();
		StringBuilder results = new StringBuilder("");

		Status status = Status.ofMessage("Start");
		for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
			try {
				Date inceptionDate = getAccountingPositionHandler().getAccountingPositionOnDateForClientAccount(account.getId());
				if (inceptionDate != null) {
					account.setInceptionDate(inceptionDate);
					getInvestmentAccountService().saveInvestmentAccount(account);
					recordsUpdated++;
					results.append("\nAccount # ").append(account.getNumber()).append(": ").append(DateUtils.fromDateShort(inceptionDate));
				}
				else {
					skippedAccounts.add(account.getNumber());
				}
			}
			catch (Exception e) {
				recordsFailed++;
				String msg = "Account # " + account.getNumber() + ": " + ExceptionUtils.getOriginalMessage(e);
				status.addError(msg);
				LogUtils.errorOrInfo(getClass(), "Error setting Positions On Date for " + msg, e);
			}
		}

		status.setActionPerformed(recordsUpdated != 0);
		status.setMessage("Set Inception Date on " + recordsUpdated + " account(s). Failed " + recordsFailed +
				". Skipped " + skippedAccounts.size() + " account(s): " + StringUtils.collectionToCommaDelimitedString(skippedAccounts)
				+ "\n" + results.toString());
		return status;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
