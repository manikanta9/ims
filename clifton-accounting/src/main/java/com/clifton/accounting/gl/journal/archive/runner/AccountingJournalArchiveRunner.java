package com.clifton.accounting.gl.journal.archive.runner;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveService;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * <code>AccountingJournalArchiveRunner</code> is an {@link com.clifton.core.util.runner.Runner}
 * that will archive all {@link AccountingJournal}s for a provided {@link AccountingJournalType}.
 *
 * @author NickK
 */
public class AccountingJournalArchiveRunner extends AbstractStatusAwareRunner {

	private static final String JOURNAL_ARCHIVE_RUNNER_TYPE = "JOURNAL-ARCHIVE";

	private final AccountingJournalType journalType;
	private final AccountingJournalService accountingJournalService;
	private final AccountingJournalArchiveService accountingJournalArchiveService;


	public AccountingJournalArchiveRunner(AccountingJournalType journalType, AccountingJournalService accountingJournalService, AccountingJournalArchiveService accountingJournalArchiveService) {
		super(JOURNAL_ARCHIVE_RUNNER_TYPE, journalType.getName(), new Date());
		this.journalType = journalType;
		this.accountingJournalService = accountingJournalService;
		this.accountingJournalArchiveService = accountingJournalArchiveService;
	}


	@Override
	public void run() {
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setJournalTypeId(this.journalType.getId());
		searchForm.setStart(0);
		searchForm.setLimit(1000);
		int processedCount = 0;
		boolean complete = false;
		boolean error = false;
		while (!complete && !Thread.interrupted()) {
			try {
				int processedBatch = archiveAccountingJournalBatch(searchForm);
				processedCount += processedBatch;
				getStatus().setMessage("Working - Archived " + processedCount + " journals of type " + this.journalType.getName());
				if (processedBatch < searchForm.getLimit()) {
					complete = true;
				}
				else {
					searchForm.getRestrictionList().clear();
					searchForm.setStart(processedCount);
				}
			}
			catch (Exception e) {
				complete = true;
				error = true;
				getStatus().addError(ExceptionUtils.getOriginalMessage(e));
			}
		}
		StringBuilder messageBuilder = new StringBuilder("Finished ");
		if (error) {
			messageBuilder.append(" with error ");
		}
		getStatus().setMessage(messageBuilder.append("- Archived ").append(processedCount).append(" journals of type ").append(this.journalType.getName()).toString());
	}


	@Transactional
	protected int archiveAccountingJournalBatch(AccountingJournalSearchForm journalSearchForm) {
		List<AccountingJournal> journalList = this.accountingJournalService.getAccountingJournalList(journalSearchForm);
		int count = 0;
		for (AccountingJournal journal : CollectionUtils.getIterable(journalList)) {
			this.accountingJournalArchiveService.archiveAccountingJournal(journal, true);
			count++;
		}
		return count;
	}
}
