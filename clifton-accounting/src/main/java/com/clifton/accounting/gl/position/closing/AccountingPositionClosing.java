package com.clifton.accounting.gl.position.closing;

import com.clifton.accounting.gl.position.AccountingPosition;

import java.math.BigDecimal;


/**
 * The <code>AccountingPositionClosing</code> represents a lot that will be closed for a given amount of a security being closed.
 *
 * @author mwacker
 */
public class AccountingPositionClosing extends AccountingPosition {

	private BigDecimal marketValue;
	private BigDecimal closingPrice;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public AccountingPositionClosing(AccountingPosition position) {
		super(position);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}


	public BigDecimal getClosingPrice() {
		return this.closingPrice;
	}


	public void setClosingPrice(BigDecimal closingPrice) {
		this.closingPrice = closingPrice;
	}
}
