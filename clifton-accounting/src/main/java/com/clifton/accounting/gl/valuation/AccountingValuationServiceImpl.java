package com.clifton.accounting.gl.valuation;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.SnapshotRebuildContext;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class AccountingValuationServiceImpl implements AccountingValuationService {

	private AccountingBalanceService accountingBalanceService;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingPositionHandler accountingPositionHandler;

	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true) // faster PK look ups when there's only one transaction
	public List<AccountingPositionValue> getAccountingPositionValueList(AccountingPositionCommand command) {
		List<AccountingPositionValue> result = new ArrayList<>();
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		int size = CollectionUtils.getSize(positionList);
		if (size > 0) {
			SnapshotRebuildContext context = new SnapshotRebuildContext(getMarketDataExchangeRatesApiService());

			Date valuationDate = command.getTransactionOrSettlementDate();
			ValidationUtils.assertNotNull(valuationDate, "Transaction Date restriction is required.");

			for (AccountingPosition position : positionList) {
				ValuationData data = ValuationData.onValuationDate(valuationDate);
				data.withQuantity(position.getRemainingQuantity());
				data.withLocalCostBasis(position.getRemainingCostBasis());
				data.withBaseCostBasis(InvestmentUtils.isNoPaymentOnOpen(position.getInvestmentSecurity()) ? MathUtils.multiply(position.getRemainingCostBasis(), data.getExchangeRateToBase(), 2) : position.getRemainingBaseDebitCredit());
				data.withBaseCost(position.getRemainingBaseDebitCredit());

				populatePositionValuationData(data, position, position.getRemainingQuantity(), command.isUseHoldingAccountBaseCurrency(), false, context);
				populateAccrualAmounts(data, position.getInvestmentSecurity(), position.getTransactionDate(), position.getSettlementDate(), position.getRemainingQuantity(), position.getRemainingCostBasis());
				populateAccrualReceivableAmounts(data, position.getClientInvestmentAccount(), position.getInvestmentSecurity(), command.isIncludeUnsettledLegPayments(), data.getValuationDate(), command.getTransactionDate() == null);

				result.add(new AccountingPositionValue(position, data));
			}
		}

		// convert result to paging list
		if (positionList instanceof PagingArrayList) {
			PagingArrayList<AccountingPosition> pagingList = (PagingArrayList<AccountingPosition>) positionList;
			result = new PagingArrayList<>(result, pagingList.getFirstElementIndex(), pagingList.getTotalElementCount(), pagingList.getPageSize());
		}

		return result;
	}


	@Override
	@Transactional(readOnly = true, noRollbackFor = ValidationException.class) // faster PK look ups when there's only one transaction
	public List<AccountingBalanceValue> getAccountingBalanceValueList(AccountingBalanceSearchForm searchForm) {
		// Update to initialize to empty list instead of null to avoid NPEs
		List<AccountingBalanceValue> result = new ArrayList<>();
		List<AccountingBalance> balanceList = getAccountingBalanceService().getAccountingBalanceList(searchForm);

		if (!CollectionUtils.isEmpty(balanceList)) {
			result = new ArrayList<>(balanceList.size());
			SnapshotRebuildContext context = new SnapshotRebuildContext(getMarketDataExchangeRatesApiService());

			Date valuationDate = searchForm.getTransactionOrSettlementDate();
			ValidationUtils.assertNotNull(valuationDate, "Transaction or Settlement Date restriction is required.");

			boolean includeAccruals = searchForm.isIncludeUnsettledLegPayments() && hasAccruals(balanceList) && hasAccrualsFromPositionOpen(balanceList);
			Map<PositionBalanceGrouping, ValuationData> accrualsMap = includeAccruals ? getAccrualDataMap(searchForm) : Collections.emptyMap();

			for (AccountingBalance balance : balanceList) {
				ValuationData data = ValuationData.onValuationDate(valuationDate);
				data.withQuantity(balance.getQuantity());
				data.withLocalCostBasis(balance.getLocalCostBasis());
				data.withBaseCostBasis(balance.getBaseCostBasis());
				data.withBaseCost(balance.getBaseAmount());

				if (balance.getAccountingAccount().isPosition()) {
					if (hasAccruals(balance.getInvestmentSecurity())) {
						ValuationData accrualData = accrualsMap.get(new PositionBalanceGrouping(balance));
						if (accrualData != null) {
							data.withLocalAccrual1(accrualData.getLocalAccrual1());
							data.withLocalAccrual2(accrualData.getLocalAccrual2());
							data.withLocalEquityAccrual(accrualData.getLocalEquityAccrual());

							data.withLocalReceivableAccrual1(accrualData.getLocalReceivableAccrual1());
							data.withLocalReceivableAccrual2(accrualData.getLocalReceivableAccrual2());
							data.withLocalEquityReceivableAccrual(accrualData.getLocalEquityReceivableAccrual());
						}
						else {
							populateAccrualAmounts(data, balance.getInvestmentSecurity(), null, null, balance.getQuantity(), balance.getLocalCostBasis());
							populateAccrualReceivableAmounts(data, balance.getClientInvestmentAccount(), balance.getInvestmentSecurity(), searchForm.isIncludeUnsettledLegPayments(), data.getValuationDate(), searchForm.getTransactionDate() == null);
						}
					}
					populatePositionValuationData(data, balance, balance.getQuantity(), searchForm.isUseHoldingAccountBaseCurrency(), searchForm.isIncludeUnderlyingPrice(), context);
				}
				else {
					data.withExchangeRateToBase(getExchangeRate(balance, searchForm.isUseHoldingAccountBaseCurrency(), valuationDate, context.getFxRateLookupCache()));
					data.withLocalNotional(balance.getLocalAmount());
				}

				result.add(new AccountingBalanceValue(balance, data, balance.isPendingActivityIncluded()));
			}

			// convert result to paging list
			if (balanceList instanceof PagingArrayList) {
				PagingArrayList<AccountingBalance> pagingList = (PagingArrayList<AccountingBalance>) balanceList;
				result = new PagingArrayList<>(result, pagingList.getFirstElementIndex(), pagingList.getTotalElementCount(), pagingList.getPageSize());
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void populatePositionValuationData(ValuationData data, AccountingObjectInfo accountingObject, BigDecimal quantity, boolean useHoldingAccountBaseCurrency, boolean includeUnderlyingPrice, SnapshotRebuildContext context) {
		data.withPrice(getPrice(accountingObject, data.getValuationDate(), context));
		data.withExchangeRateToBase(getExchangeRate(accountingObject, useHoldingAccountBaseCurrency, data.getValuationDate(), context.getFxRateLookupCache()));

		// TIPS use quantity (face) adjusted by index ratio
		InvestmentSecurity security = accountingObject.getInvestmentSecurity();
		BigDecimal notionalMultiplier = getNotionalMultiplier(security, data.getValuationDate());
		data.withLocalNotional(getInvestmentCalculator().calculateNotional(security, data.getPrice(), quantity, notionalMultiplier));

		if (includeUnderlyingPrice) {
			// populate underlying price
			if (security.getUnderlyingSecurity() != null) {
				BigDecimal underlyingPrice = context.getSecurityPrice(security.getUnderlyingSecurity().getId());
				if (underlyingPrice == null) {
					underlyingPrice = getMarketDataRetriever().getPriceFlexible(security.getUnderlyingSecurity(), data.getValuationDate(), false);
					context.cacheSecurityPrice(security.getUnderlyingSecurity().getId(), data.getUnderlyingPrice());
				}
				data.withUnderlyingPrice(underlyingPrice);
			}
		}
	}


	private BigDecimal getNotionalMultiplier(InvestmentSecurity security, Date getValuationDate) {
		BigDecimal notionalMultiplier = null;
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.isNotionalMultiplierUsed()) {
			NotionalMultiplierRetriever notionalMultiplierRetriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(hierarchy.getNotionalMultiplierRetriever());
			// To be consistent, look up index ratio on settlement date because we include accrued interest as of the next day (end of day)
			notionalMultiplier = notionalMultiplierRetriever.retrieveNotionalMultiplier(security, () -> getValuationDate, true);
		}
		return notionalMultiplier;
	}


	private BigDecimal getPrice(AccountingObjectInfo accountingObject, Date valuationDate, SnapshotRebuildContext context) {
		InvestmentSecurity security = accountingObject.getInvestmentSecurity();
		BigDecimal price = context.getSecurityPrice(security.getId());
		if (price == null) {
			price = getMarketDataRetriever().getPriceFlexible(security, valuationDate, false);
			if (price == null) {
				// price is still not found: find the latest transaction prior or on valuationDate and use its price
				AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
				searchForm.setInvestmentSecurityId(security.getId());
				searchForm.setAccountingAccountId(accountingObject.getAccountingAccount() == null ? null : accountingObject.getAccountingAccount().getId());
				searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.LESS_THAN_OR_EQUALS, valuationDate));
				searchForm.setOrderBy("transactionDate:desc");
				searchForm.setLimit(1);
				AccountingTransaction lastTran = CollectionUtils.getFirstElement(getAccountingTransactionService().getAccountingTransactionList(searchForm));
				if (lastTran != null) {
					price = lastTran.getPrice();
				}
				if (price == null) {
					throw new ValidationException("Cannot find price for investment security " + security + " on " + valuationDate);
				}
			}
			context.cacheSecurityPrice(security.getId(), price);
		}
		return price;
	}


	private BigDecimal getExchangeRate(AccountingObjectInfo accountingObject, boolean useHoldingAccountBaseCurrency, Date valuationDate, FxRateLookupCache cache) {
		String from = InvestmentUtils.getSecurityCurrencyDenominationSymbol(accountingObject);
		String to = (useHoldingAccountBaseCurrency ? InvestmentUtils.getHoldingAccountBaseCurrency(accountingObject) : InvestmentUtils.getClientAccountBaseCurrency(accountingObject)).getSymbol();
		BigDecimal result = cache.getExchangeRate(accountingObject.fxSourceCompany(), accountingObject.fxSourceCompanyOverridden(), from, to, valuationDate, true);
		if (result == null) {
			throw new ValidationException("Cannot find exchange rate between " + from + " and " + to + " on " + valuationDate);
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   accruals                             ////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<PositionBalanceGrouping, ValuationData> getAccrualDataMap(AccountingBalanceSearchForm searchForm) {
		Map<PositionBalanceGrouping, ValuationData> accrualsMap = new HashMap<>();

		if (searchForm.isIncludeUnsettledLegPayments()) {
			AccountingPositionCommand command = new AccountingPositionCommand();
			BeanUtils.copyProperties(searchForm, command, new String[]{"accountingAccountIds", "restrictionList"});
			Map<PositionBalanceGrouping, List<AccountingPosition>> positionGroups = getAccountingPositionService().getAccountingPositionListUsingCommand(command).stream()
					.filter(p -> hasAccruals(p.getInvestmentSecurity()))
					.filter(p -> hasAccrualsFromPositionOpen(p.getInvestmentSecurity()))
					.collect(Collectors.groupingBy(PositionBalanceGrouping::new));
			positionGroups.forEach((key, value) -> {
				for (AccountingPosition position : value) {
					ValuationData data = ValuationData.onValuationDate(searchForm.getTransactionOrSettlementDate());
					populateAccrualAmounts(data, position.getInvestmentSecurity(), position.getTransactionDate(), position.getSettlementDate(), position.getQuantity(), position.getRemainingCostBasis());
					populateAccrualReceivableAmounts(data, position.getClientInvestmentAccount(), position.getInvestmentSecurity(), command.isIncludeUnsettledLegPayments(), data.getValuationDate(), command.getTransactionDate() == null);

					ValuationData accrualData = accrualsMap.computeIfAbsent(key, k -> ValuationData.onValuationDate(searchForm.getTransactionOrSettlementDate()));
					accrualData.withLocalAccrual1(MathUtils.add(accrualData.getLocalAccrual1(), data.getLocalAccrual1()));
					accrualData.withLocalAccrual2(MathUtils.add(accrualData.getLocalAccrual2(), data.getLocalAccrual2()));
					accrualData.withLocalEquityAccrual(MathUtils.add(accrualData.getLocalEquityAccrual(), data.getLocalEquityAccrual()));
					// accrual receivables - swap specific reset information
					accrualData.withLocalEquityReceivableAccrual(MathUtils.add(accrualData.getLocalEquityReceivableAccrual(), data.getLocalEquityReceivableAccrual()));
					accrualData.withLocalReceivableAccrual1(MathUtils.add(accrualData.getLocalReceivableAccrual1(), data.getLocalReceivableAccrual1()));
					accrualData.withLocalReceivableAccrual2(MathUtils.add(accrualData.getLocalReceivableAccrual2(), data.getLocalReceivableAccrual2()));
				}
			});
		}
		return accrualsMap;
	}


	private void populateAccrualAmounts(ValuationData data, InvestmentSecurity security, Date transactionDate, Date settlementDate, BigDecimal quantity, BigDecimal localCostBasis) {
		if (hasAccruals(security)) {
			InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
			BigDecimal notionalMultiplier = getNotionalMultiplier(security, data.getValuationDate());

			AccrualBasis accrualBasis = AccrualBasis.forHierarchy(hierarchy, localCostBasis, quantity);
			if (accrualBasis.isAccrualBasisNotZero()) {
				AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(security, transactionDate, settlementDate, data.getValuationDate()));

				if (accrualBasis.getAccrualBasis1() != null) {
					BigDecimal localAccrual1 = getInvestmentCalculator().calculateAccruedInterestForEvent1(security, accrualBasis.getAccrualBasis1(), notionalMultiplier, accrualDates.getAccrueUpToDate1(), accrualDates.getAccrueFromDate1());
					data.withLocalAccrual1(localAccrual1);
				}
				if (accrualBasis.getAccrualBasis2() != null) {
					BigDecimal localAccrual2 = getInvestmentCalculator().calculateAccruedInterestForEvent2(security, accrualBasis.getAccrualBasis2(), notionalMultiplier, accrualDates.getAccrueUpToDate2(), accrualDates.getAccrueFromDate2());
					data.withLocalAccrual2(localAccrual2);
				}
			}
		}
	}


	private boolean hasAccruals(List<AccountingBalance> balanceList) {
		return CollectionUtils.getStream(balanceList).map(AccountingBalance::getInvestmentSecurity).anyMatch(this::hasAccruals);
	}


	private boolean hasAccruals(InvestmentSecurity security) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		return (hierarchy.getAccrualSecurityEventType() != null || hierarchy.getAccrualSecurityEventType2() != null);
	}


	private boolean hasAccrualsFromPositionOpen(List<AccountingBalance> balanceList) {
		return CollectionUtils.getStream(balanceList)
				.map(AccountingBalance::getInvestmentSecurity)
				.filter(this::hasAccruals)
				.anyMatch(this::hasAccrualsFromPositionOpen);
	}


	private boolean hasAccrualsFromPositionOpen(InvestmentSecurity security) {
		boolean isAccrualsFromPositionOpen = false;
		AccrualDateCalculators dateCalculator1 = InvestmentCalculatorUtils.getAccrualDateCalculator(security);
		if (dateCalculator1 != null) {
			isAccrualsFromPositionOpen = !dateCalculator1.isAccrualFromPeriodStart();
		}
		AccrualDateCalculators dateCalculator2 = InvestmentCalculatorUtils.getAccrualDateCalculator2(security);
		if (dateCalculator2 != null) {
			isAccrualsFromPositionOpen = isAccrualsFromPositionOpen || !dateCalculator2.isAccrualFromPeriodStart();
		}
		return isAccrualsFromPositionOpen;
	}


	private void populateAccrualReceivableAmounts(ValuationData data, InvestmentAccount clientInvestmentAccount, InvestmentSecurity security, boolean includeUnsettledLegPayments, Date date, boolean useSettlementDate) {
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

		if (hierarchy.isIncludeAccrualReceivables() && includeUnsettledLegPayments && hasAccruals(security)) {
			Map<String, BigDecimal> localReceivablesMap = getAccountingPositionHandler().getAccountingPositionReceivableMap(clientInvestmentAccount, security, date, useSettlementDate);

			String securityEventType1 = InvestmentUtils.getAccrualEventTypeName(security);
			if (securityEventType1 != null && localReceivablesMap.containsKey(securityEventType1)) {
				BigDecimal localAccrual1 = Optional.ofNullable(data.getLocalAccrual1()).orElse(BigDecimal.ZERO);
				BigDecimal localReceivableAccrual1 = localReceivablesMap.get(securityEventType1);
				data.withLocalReceivableAccrual1(localReceivableAccrual1);
				// accruals include unsettled payments
				localAccrual1 = MathUtils.add(localAccrual1, localReceivableAccrual1);
				data.withLocalAccrual1(localAccrual1);
			}

			String securityEventType2 = InvestmentUtils.getAccrualEventTypeName2(security);
			if (securityEventType2 != null && localReceivablesMap.containsKey(securityEventType2)) {
				BigDecimal localAccrual2 = Optional.ofNullable(data.getLocalAccrual2()).orElse(BigDecimal.ZERO);
				BigDecimal localReceivableAccrual2 = localReceivablesMap.get(securityEventType2);
				data.withLocalReceivableAccrual2(localReceivableAccrual2);
				// accruals include unsettled payments
				localAccrual2 = MathUtils.add(localAccrual2, localReceivableAccrual2);
				data.withLocalAccrual2(localAccrual2);
			}

			if (localReceivablesMap.containsKey(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT)) {
				BigDecimal localEquityAccrual = Optional.ofNullable(data.getLocalEquityAccrual()).orElse(BigDecimal.ZERO);
				BigDecimal localEquityReceivableAccrual = localReceivablesMap.get(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);
				data.withLocalEquityReceivableAccrual(localEquityReceivableAccrual);
				// accruals include unsettled payments
				localEquityAccrual = MathUtils.add(localEquityAccrual, localEquityReceivableAccrual);
				data.withLocalEquityAccrual(localEquityAccrual);
			}

			if (localReceivablesMap.containsKey(AccountingJournalType.TRADE_JOURNAL)) {
				BigDecimal localEquityAccrual = Optional.ofNullable(data.getLocalEquityAccrual()).orElse(BigDecimal.ZERO);
				BigDecimal localEquityReceivableAccrual = Optional.ofNullable(data.getLocalEquityReceivableAccrual()).orElse(BigDecimal.ZERO);
				BigDecimal localEquityReceivableTradeJournalAccrual = localReceivablesMap.get(AccountingJournalType.TRADE_JOURNAL);
				// accruals include unsettled payments
				localEquityAccrual = MathUtils.add(localEquityAccrual, localEquityReceivableTradeJournalAccrual);
				localEquityReceivableAccrual = MathUtils.add(localEquityReceivableAccrual, localEquityReceivableTradeJournalAccrual);
				data.withLocalEquityAccrual(localEquityAccrual);
				data.withLocalEquityReceivableAccrual(localEquityReceivableAccrual);
			}
		}
	}


	/**
	 * Allow grouping of positions based on holding account, client account, accounting account and security.
	 */
	private static class PositionBalanceGrouping implements AccountingObjectInfo {

		private final AccountingObjectInfo accountingObjectInfo;


		private PositionBalanceGrouping(AccountingObjectInfo accountingObjectInfo) {
			this.accountingObjectInfo = accountingObjectInfo;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			PositionBalanceGrouping that = (PositionBalanceGrouping) o;
			return getClientInvestmentAccount().equals(that.getClientInvestmentAccount()) &&
					getHoldingInvestmentAccount().equals(that.getHoldingInvestmentAccount()) &&
					getAccountingAccount().equals(that.getAccountingAccount()) &&
					getInvestmentSecurity().equals(that.getInvestmentSecurity());
		}


		@Override
		public int hashCode() {
			return Objects.hash(getClientInvestmentAccount(), getHoldingInvestmentAccount(),
					getAccountingAccount(), getInvestmentSecurity());
		}


		@Override
		public InvestmentAccount getClientInvestmentAccount() {
			return this.accountingObjectInfo.getClientInvestmentAccount();
		}


		@Override
		public InvestmentAccount getHoldingInvestmentAccount() {
			return this.accountingObjectInfo.getHoldingInvestmentAccount();
		}


		@Override
		public AccountingAccount getAccountingAccount() {
			return this.accountingObjectInfo.getAccountingAccount();
		}


		@Override
		public InvestmentSecurity getInvestmentSecurity() {
			return this.accountingObjectInfo.getInvestmentSecurity();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
