package com.clifton.accounting.gl.balance;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.UUID;


/**
 * The <code>AccountingBalance</code> represents an aggregate of lots/positions for a particular Holding Account,
 * Client Account, GL Account, and Security. Balances can include pending activity, which can be merged or included
 * as separate balances depending on the pending activity criterion provided in a request.
 * <p>
 * Balances merged according to {@link AccountingObjectInfo} details, will always include the aggregated values for
 * quantity, local amount, base amount, local cost basis, and base cost basis. The corresponding original and pending
 * values can be obtained as well depending on whether original and/or pending activity exists for the accounting object.
 * <p>
 * Unlike {@link com.clifton.accounting.gl.valuation.AccountingBalanceValue} object, it does not contain
 * information about current valuation: only current quantity and cost basis.
 * <p>
 * When possible, use this {@link AccountingBalance} object because it's more light weight and is faster to populate.
 *
 * @author jgommels
 * @see com.clifton.accounting.gl.pending.PendingActivityRequests
 */
@NonPersistentObject
public class AccountingBalance extends BaseSimpleEntity<String> implements AccountingObjectInfo {

	public static final String BALANCE_PRINT_HEADER = "Client #\tHolding #\tGL Account\tSecurity\tQty\tLocal Amount\tBase Amount\tLocal Cost Basis\tBase Cost Basis Date\tPending Included\tPending\r";
	public static final String BALANCE_PRINT_FORMAT = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s";
	/**
	 * Database Server generated GUID that is required by Hibernate: must have a PK.
	 */
	private String uuid;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private AccountingAccount accountingAccount;
	private InvestmentSecurity investmentSecurity;

	private BigDecimal quantity;
	private BigDecimal localAmount;
	private BigDecimal baseAmount;
	private BigDecimal localCostBasis;
	private BigDecimal baseCostBasis;

	/**
	 * Indicates whether this balance is for Pending transactions that have not yet been posted to the General Ledger.
	 * <p>
	 * This flag is not to be confused with the {@link #isPendingActivityIncluded()} calculated getter, which defines if the balance includes pending activity.
	 * If a balance is for pending transactions, {@link #isPendingActivityIncluded()} will be <code>true</code> and {@link #isPendingActivity()} will return <code>true</code>.
	 * If a balance includes pending transactions with original transactions posted to GL, {@link #isPendingActivityIncluded()} will be <code>true</code> and {@link #isPendingActivity()} will return <code>false</code>;
	 * <p>
	 * The two boolean getters ({@link #isPendingActivityIncluded()} and {@link #isPendingActivity()}) are necessary to differentiate between a balance
	 * including pending balance and one that represents pending transaction(s). This avoids having to set a pending transaction balance's {@link #pendingBalance}
	 * to itself and potentially causing infinite recursion.
	 */
	private boolean pendingActivity;
	/**
	 * When pending and actual lot positions or position balances are merged together, there are case where we need to know
	 * what portion of the new balance is pending vs actual. In order to do this, this balance is used to track the
	 * pending position for the balance to determine the amount for each.
	 */
	private AccountingBalance pendingBalance;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates an {@link AccountingBalance} with client account, holding account, security, and accounting account populated, which are all required fields for a balance.
	 */
	public static AccountingBalance ofAccountingObjectInfo(AccountingObjectInfo accountingObjectInfo) {
		AccountingBalance result = new AccountingBalance();
		// Set ID for uniqueness of objects. If ID is null, equality checks will determine all as unique.
		result.setId(UUID.randomUUID().toString());
		result.setClientInvestmentAccount(accountingObjectInfo.getClientInvestmentAccount());
		result.setHoldingInvestmentAccount(accountingObjectInfo.getHoldingInvestmentAccount());
		result.setAccountingAccount(accountingObjectInfo.getAccountingAccount());
		result.setInvestmentSecurity(accountingObjectInfo.getInvestmentSecurity());
		return result;
	}


	/**
	 * Creates an {@link AccountingBalance} with all required fields according to {@link #ofAccountingObjectInfo(AccountingObjectInfo)}
	 * and also populates position quantity properties according to the journal provided.
	 */
	public static AccountingBalance ofAccountingJournalDetailDefinition(AccountingJournalDetailDefinition journalDetail, boolean pendingActivity) {
		AccountingBalance result = ofAccountingObjectInfo(journalDetail);
		result.setPendingActivity(pendingActivity);
		result.setQuantity(journalDetail.getQuantity());
		result.setLocalAmount(journalDetail.getLocalDebitCredit());
		result.setBaseAmount(journalDetail.getBaseDebitCredit());
		result.setLocalCostBasis(journalDetail.getPositionCostBasis());
		result.setBaseCostBasis(MathUtils.multiply(journalDetail.getPositionCostBasis(), journalDetail.getExchangeRateToBase(), 2));
		return result;
	}


	/**
	 * Creates a copy of the provided {@link AccountingBalance}.
	 * <p>
	 * The includePendingBalance flag defines whether the pending details should be included in the copy.
	 * In some cases, such as with merging of balances, the pending balance is recreated and needs
	 * to be excluded in the copy to prevent doubling the pending balance.
	 */
	public static AccountingBalance ofAccountingBalance(AccountingBalance toCopy, boolean includePendingBalance) {
		if (toCopy == null) {
			return null;
		}
		AccountingBalance clone = ofAccountingObjectInfo(toCopy);
		clone.setId(toCopy.getId());
		clone.setQuantity(toCopy.getQuantity());
		clone.setLocalAmount(toCopy.getLocalAmount());
		clone.setBaseAmount(toCopy.getBaseAmount());
		clone.setLocalCostBasis(toCopy.getLocalCostBasis());
		clone.setBaseCostBasis(toCopy.getBaseCostBasis());
		clone.setPendingActivity(toCopy.isPendingActivity());
		if (includePendingBalance) {
			clone.setPendingBalance(ofAccountingBalance(toCopy.getPendingBalance(), false));
		}
		return clone;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getLocalDebit() {
		BigDecimal result = getLocalAmount();
		if (result != null) {
			if (result.compareTo(BigDecimal.ZERO) < 0) {
				result = BigDecimal.ZERO;
			}
		}
		return result;
	}


	public BigDecimal getLocalCredit() {
		BigDecimal result = getLocalAmount();
		if (result != null) {
			if (result.compareTo(BigDecimal.ZERO) > 0) {
				result = BigDecimal.ZERO;
			}
			else {
				result = result.negate();
			}
		}
		return result;
	}


	public BigDecimal getBaseDebit() {
		BigDecimal result = getBaseAmount();
		if (result != null) {
			if (result.compareTo(BigDecimal.ZERO) < 0) {
				result = BigDecimal.ZERO;
			}
		}
		return result;
	}


	public BigDecimal getBaseCredit() {
		BigDecimal result = getBaseAmount();
		if (result != null) {
			if (result.compareTo(BigDecimal.ZERO) > 0) {
				result = BigDecimal.ZERO;
			}
			else {
				result = result.negate();
			}
		}
		return result;
	}


	@Override
	public String toString() {
		return "AccountingBalance [holdingInvestmentAccount=" + this.holdingInvestmentAccount + ", clientInvestmentAccount=" + this.clientInvestmentAccount + ", accountingAccount="
				+ this.accountingAccount + ", investmentSecurity=" + this.investmentSecurity + ", quantity=" + this.quantity + ", localAmount=" + this.localAmount + ", baseAmount=" + this.baseAmount
				+ ", localCostBasis=" + this.localCostBasis + ", baseCostBasis=" + this.baseCostBasis + "]";
	}


	public String toStringFormatted() {
		return toStringFormatted(false);
	}


	public String toStringFormatted(boolean includeHeader) {
		Object[] values = ArrayUtils.createArray(
				getClientInvestmentAccount().getNumber(),
				getHoldingInvestmentAccount().getNumber(),
				getAccountingAccount().getName(),
				getInvestmentSecurity().getSymbol(),
				CoreMathUtils.formatNumberDecimal(getQuantity()),
				CoreMathUtils.formatNumberDecimal(getLocalAmount()),
				CoreMathUtils.formatNumberDecimal(getBaseAmount()),
				CoreMathUtils.formatNumberDecimal(getLocalCostBasis()),
				CoreMathUtils.formatNumberDecimal(getBaseCostBasis()),
				isPendingActivityIncluded(),
				isPendingActivity()
		);

		String result = String.format(BALANCE_PRINT_FORMAT, values);
		return includeHeader ? BALANCE_PRINT_HEADER + result : result;
	}


	@Override
	public String getId() {
		return getUuid();
	}


	@Override
	public void setId(String id) {
		setUuid(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	/**
	 * Returns the quantity for transactions posted to GL
	 */
	public BigDecimal getOriginalQuantity() {
		BigDecimal totalAmount = getQuantity();
		BigDecimal pendingAmount = getPendingQuantity();
		return pendingAmount == null ? totalAmount : MathUtils.subtract(totalAmount, pendingAmount);
	}


	/**
	 * Returns the quantity for pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public BigDecimal getPendingQuantity() {
		return getPendingBalance() == null ? null : getPendingBalance().getQuantity();
	}


	/**
	 * Returns the quantity including both pending and posted transactions
	 */
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	/**
	 * Returns the local amount for transactions posted to GL
	 */
	public BigDecimal getOriginalLocalAmount() {
		BigDecimal totalAmount = getLocalAmount();
		BigDecimal pendingAmount = getPendingLocalAmount();
		return pendingAmount == null ? totalAmount : MathUtils.subtract(totalAmount, pendingAmount);
	}


	/**
	 * Returns the local amount for pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public BigDecimal getPendingLocalAmount() {
		return getPendingBalance() == null ? null : getPendingBalance().getLocalAmount();
	}


	/**
	 * Returns the local amount including both pending and posted transactions
	 */
	public BigDecimal getLocalAmount() {
		return this.localAmount;
	}


	public void setLocalAmount(BigDecimal localAmount) {
		this.localAmount = localAmount;
	}


	/**
	 * Returns the base amount for transactions posted to GL
	 */
	public BigDecimal getOriginalBaseAmount() {
		BigDecimal totalAmount = getBaseAmount();
		BigDecimal pendingAmount = getPendingBaseAmount();
		return pendingAmount == null ? totalAmount : MathUtils.subtract(totalAmount, pendingAmount);
	}


	/**
	 * Returns the base amount for pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public BigDecimal getPendingBaseAmount() {
		return getPendingBalance() == null ? null : getPendingBalance().getBaseAmount();
	}


	/**
	 * Returns the base amount including both pending and posted transactions
	 */
	public BigDecimal getBaseAmount() {
		return this.baseAmount;
	}


	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}


	/**
	 * Returns the local cost basis for transactions posted to GL
	 */
	public BigDecimal getOriginalLocalCostBasis() {
		BigDecimal totalAmount = getLocalCostBasis();
		BigDecimal pendingAmount = getPendingLocalCostBasis();
		return pendingAmount == null ? totalAmount : MathUtils.subtract(totalAmount, pendingAmount);
	}


	/**
	 * Returns the local cost basis for pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public BigDecimal getPendingLocalCostBasis() {
		return getPendingBalance() == null ? null : getPendingBalance().getLocalCostBasis();
	}


	/**
	 * Returns the local cost basis including both pending and posted transactions
	 */
	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public void setLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
	}


	/**
	 * Returns the base cost basis for transactions posted to GL
	 */
	public BigDecimal getOriginalBaseCostBasis() {
		BigDecimal totalAmount = getBaseCostBasis();
		BigDecimal pendingAmount = getPendingBaseCostBasis();
		return pendingAmount == null ? totalAmount : MathUtils.subtract(totalAmount, pendingAmount);
	}


	/**
	 * Returns the base cost basis for pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public BigDecimal getPendingBaseCostBasis() {
		return getPendingBalance() == null ? null : getPendingBalance().getBaseCostBasis();
	}


	/**
	 * Returns the base cost basis including both pending and posted transactions
	 */
	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	public void setBaseCostBasis(BigDecimal baseCostBasis) {
		this.baseCostBasis = baseCostBasis;
	}


	/**
	 * Returns true if this balance is {@link #isPendingActivity()} or includes merged pending activity ({@link #getPendingBalance()} has a value).
	 * If trying to determine if the balance has pending activity, this method is preferred to {@link #isPendingActivity()} because it
	 * will return true any time a balance is completely pending or includes pending activity.
	 */
	public boolean isPendingActivityIncluded() {
		return getPendingBalance() != null || isPendingActivity();
	}


	/**
	 * Returns true if this balance represents pending transaction(s) only. This balance will not have a value for {@link #getPendingBalance()}.
	 */
	public boolean isPendingActivity() {
		return this.pendingActivity;
	}


	public void setPendingActivity(boolean pendingActivity) {
		this.pendingActivity = pendingActivity;
	}


	/**
	 * Returns a balance representing pending transactions not posted to GL
	 * <p>
	 * Returns null if {@link #isPendingActivityIncluded()} is false.
	 */
	public AccountingBalance getPendingBalance() {
		return this.pendingBalance;
	}


	public void setPendingBalance(AccountingBalance pendingBalance) {
		this.pendingBalance = pendingBalance;
	}
}
