package com.clifton.accounting.gl.journal.archive;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.archive.json.jackson.strategy.AccountingJournalArchiveJacksonStrategy;
import com.clifton.archive.client.adapter.BaseArchiveEntityDescriptorAdapter;
import com.clifton.archive.descriptor.ArchiveEntityDescriptor;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;

import java.io.Serializable;


/**
 * <code>BaseAccountingJournalArchiveEntityAdapter</code> represents a base {@link AccountingJournalArchiveEntityAdapter}
 * that handles most of the conversion between an {@link AccountingJournalArchiveEntity} and {@link ArchiveEntityDescriptor}.
 * Each {@link AccountingJournalType} that is enabled for archival should extend this class to provide specifics for the
 * {@link BookableEntity} the journal type represents.
 *
 * @author NickK
 */
@Component
public class AccountingJournalArchiveEntityAdapterImpl<T extends BookableEntity> extends BaseArchiveEntityDescriptorAdapter<AccountingJournalArchiveEntity<T>> implements AccountingJournalArchiveEntityAdapter<T> {

	private DaoLocator daoLocator;

	private JsonHandler<JacksonStrategy> jsonHandler;

	private SystemSchemaService systemSchemaService;

	/**
	 * This property defines the path on the {@link BookableEntity} to the parent {@link IdentityObject}
	 * if one is necessary. Can be left null if the archived entity does not need to reference a parent.
	 */
	private String parentIdentityObjectPath;


	///////////////////////////////////////////////////////////////////////////
	/////////////            Interface Implementations          ///////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return AccountingJournalArchiveEntityAdapter.DEFAULT_ACCOUNTING_JOURNAL_ARCHIVE_ADAPTER_KEY;
	}


	/**
	 * Convert an {@link AccountingJournalArchiveEntity} into an {@link ArchiveEntityDescriptor} for archival.
	 */
	@Override
	public ArchiveEntityDescriptor toArchiveEntityDescriptor(AccountingJournalArchiveEntity<T> journalArchiveEntity) {
		AccountingJournal journal = journalArchiveEntity.getAccountingJournal();
		T entity = journalArchiveEntity.getBookableEntity();
		AccountingJournalType journalType = journal.getJournalType();

		Long entityId = entity == null ? getIdentityObjectLongId(journal) : getIdentityObjectLongId(entity);
		IdentityObject parent = getParentIdentityObject(entity);
		String journalJson = toJson(journalArchiveEntity);

		ArchiveEntityDescriptor descriptor = parent != null
				? new ArchiveEntityDescriptor(journalType.getName(), entityId, getIdentityObjectLongId(parent), journalJson)
				: new ArchiveEntityDescriptor(journalType.getName(), entityId, journalJson);
		descriptor.setDefinitionDescription(journalType.getDescription());
		descriptor.setEntitySourceName(journalType.getSystemTable().getName());
		if (parent != null) {
			descriptor.setParentEntitySourceName(getSystemTableNameForEntity(parent));
		}
		descriptor.setEntityTemplate(getAccountingJournalArchiveEntityJsonTemplate(entity));
		return descriptor;
	}


	/**
	 * Convert the JSON representation of a {@link AccountingJournalArchiveEntity} into the object.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public AccountingJournalArchiveEntity<T> fromJson(String json) {
		return (AccountingJournalArchiveEntity<T>) getJsonHandler().fromJson(json, AccountingJournalArchiveEntity.class, AccountingJournalArchiveJacksonStrategy.getInstance());
	}


	/**
	 * Convert a {@link AccountingJournalArchiveEntity} object into it's JSON representation.
	 */
	@Override
	public String toJson(AccountingJournalArchiveEntity<T> journalArchiveEntity) {
		return getJsonHandler().toJson(journalArchiveEntity, AccountingJournalArchiveJacksonStrategy.getInstance());
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////           Concrete Instance Methods             /////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Get a {@link Long} representation of the provided {@link BookableEntity} object ID.
	 * This method will convert the entity's identity to a long if it is a {@link Number}.
	 * If the serializable id is not a number, an {@link IllegalStateException} will be thrown.
	 * This method can be overridden to modify the value returned.
	 */
	protected Long getIdentityObjectLongId(IdentityObject entity) {
		Serializable id = entity.getIdentity();
		if (id instanceof Number) {
			return ((Number) id).longValue();
		}
		throw new IllegalStateException("Unable to convert entity ID to Long: [" + id + "].");
	}


	/**
	 * Returns an unpopulated {@link BookableEntity} this adapters is responsible for converting.
	 * This object is used to generate the base JSON template for this archived journal object. The
	 * default value for this object is null, which will result in a new object of the base entity's
	 * class. This method can be overridden to provide a more thorough object, such as one populated
	 * with a parent entity or other values.
	 */
	protected T createNewUnpopulatedBookableEntity() {
		return null;
	}


	/**
	 * Returns the name of the {@link SystemTable} for the provided {@link IdentityObject};
	 */
	protected String getSystemTableNameForEntity(IdentityObject bean) {
		ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(bean);
		SystemTable table = getSystemSchemaService().getSystemTableByName(dao.getConfiguration().getTableName());
		return table == null ? null : table.getName();
	}


	/**
	 * Returns the parent {@link IdentityObject} from the provided {@link BookableEntity} using the path returned
	 * from {@link AccountingJournalArchiveEntityAdapterImpl#getParentIdentityObjectPath()}. If a parent path is
	 * not defined, null is returned resulting in no parent reference or ID for the archived entity.
	 */
	protected IdentityObject getParentIdentityObject(T bookableEntity) {
		String parentPath = getParentIdentityObjectPath();
		if (bookableEntity != null && parentPath != null) {
			return (IdentityObject) BeanUtils.getPropertyValue(bookableEntity, parentPath);
		}
		return null;
	}


	///////////////////////////////////////////////////////////////////////////
	/////////////               Helper Methods                    /////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an JSON template representation of an {@link AccountingJournalArchiveEntity}
	 * considering the {@link BookableEntity} this adapter is responsible for.
	 */
	private String getAccountingJournalArchiveEntityJsonTemplate(T bookableEntity) {
		AccountingJournalArchiveEntity<T> entityTemplate = bookableEntity == null ? new AccountingJournalArchiveEntity<>(createNewUnpopulatedAccountingJournal())
				: new AccountingJournalArchiveEntity<>(createNewUnpopulatedAccountingJournal(), createNewUnpopulatedBookableEntityForTemplate(bookableEntity));
		return getJsonHandler().toJson(entityTemplate, AccountingJournalArchiveJacksonStrategy.getTemplateInstance());
	}


	/**
	 * Returns an unpopulated {@link AccountingJournal} that can be used with the response of
	 * {@link AccountingJournalArchiveEntityAdapterImpl#createNewUnpopulatedBookableEntity()}
	 * to generate the base JSON template for this archived journal object.
	 */
	private AccountingJournal createNewUnpopulatedAccountingJournal() {
		AccountingJournal journal = new AccountingJournal();
		AccountingJournalDetail detail = new AccountingJournalDetail();
		journal.setJournalDetailList(CollectionUtils.createList(detail));
		return journal;
	}


	/**
	 * Returns the result of {@link AccountingJournalArchiveEntityAdapterImpl#createNewUnpopulatedBookableEntity()}
	 * if it is not null. If it is null, a new instance of the {@link BookableEntity} is created from the entity
	 * objects class type.
	 */
	@SuppressWarnings("unchecked")
	private T createNewUnpopulatedBookableEntityForTemplate(T bookableEntity) {
		if (bookableEntity == null) {
			return null;
		}
		T overridden = createNewUnpopulatedBookableEntity();
		return overridden == null ? (T) BeanUtils.newInstance(bookableEntity.getClass()) : overridden;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////               Getters and Setters             /////////////
	///////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public JsonHandler<JacksonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public String getParentIdentityObjectPath() {
		return this.parentIdentityObjectPath;
	}


	public void setParentIdentityObjectPath(String parentIdentityObjectPath) {
		this.parentIdentityObjectPath = parentIdentityObjectPath;
	}
}
