package com.clifton.accounting.gl.position.daily.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class AccountingPositionDailySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "accountingTransaction.id")
	private Long accountingTransactionId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "clientInvestmentAccount", sortField = "issuingCompany.name")
	private Integer clientAccountIssuingCompanyId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientInvestmentAccountGroupId;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "clientInvestmentAccount")
	private Integer clientId;

	@SearchField(searchField = "type.id", searchFieldPath = "accountingTransaction.holdingInvestmentAccount", sortField = "type.name")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "accountingTransaction", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "accountingTransaction.holdingInvestmentAccount", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "accountingTransaction.holdingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer holdingInvestmentAccountGroupId;

	@SearchField(searchField = "mainRelationshipList.purpose.name", searchFieldPath = "accountingTransaction.holdingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String holdingInvestmentAccountMainPurpose;

	// Custom search filter - validates relationship is active on given date
	private Date holdingInvestmentAccountMainPurposeActiveOnDate;

	@SearchField(searchField = "relatedRelationshipList.purpose.name", searchFieldPath = "accountingTransaction.holdingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String holdingInvestmentAccountRelatedPurpose;

	@SearchField(searchField = "accountingAccount.id", searchFieldPath = "accountingTransaction", sortField = "accountingAccount.name")
	private Short accountingAccountId;

	@SearchField(searchField = "collateral", searchFieldPath = "accountingTransaction.accountingAccount")
	private Boolean collateralAccountingAccount;

	@SearchField(searchField = "position", searchFieldPath = "accountingTransaction.accountingAccount")
	private Boolean positionAccountingAccount;

	@SearchField(searchField = "cash", searchFieldPath = "accountingTransaction.accountingAccount")
	private Boolean cashAccountingAccount;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "accountingTransaction", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument", sortField = "investmentAccount.number")
	private Integer fundAccountId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument", sortField = "tradingCurrency.symbol")
	private Integer investmentInstrumentCurrencyDenominationId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", searchFieldPath = "accountingTransaction.investmentSecurity", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentSecurityGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.group.name", searchFieldPath = "accountingTransaction.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String investmentGroupName;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument", sortField = "hierarchy.name")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument.hierarchy", sortField = "investmentType.name")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument.hierarchy", sortField = "investmentTypeSubType.name")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType2.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument.hierarchy", sortField = "investmentTypeSubType2.name")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "priceMultiplierOverride,instrument.priceMultiplier", searchFieldPath = "accountingTransaction.investmentSecurity", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal priceMultiplier;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "accountingTransaction.investmentSecurity.underlyingSecurity.instrument", sortField = "hierarchy.name")
	private Short underlyingHierarchyId;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "accountingTransaction.investmentSecurity", sortField = "underlyingSecurity.symbol")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "m2mCalculator", searchFieldPath = "accountingTransaction.investmentSecurity.instrument.hierarchy", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean m2mCalculatorPresent;

	@SearchField(searchField = "endDate", searchFieldPath = "accountingTransaction.investmentSecurity")
	private Date securityEndDate;

	@SearchField(searchField = "countryOfRisk.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument")
	private Integer countryOfRiskId;

	@SearchField(searchField = "countryOfIncorporation.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument")
	private Integer countryOfIncorporationId;

	@SearchField(searchField = "exchange.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument")
	private Short exchangeId;

	@SearchField(searchField = "compositeExchange.id", searchFieldPath = "accountingTransaction.investmentSecurity.instrument")
	private Short compositeExchangeId;

	@SearchField
	private Date positionDate;

	@SearchField(searchField = "positionDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date startPositionDate;

	@SearchField(searchField = "positionDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date endPositionDate;

	@SearchField(searchField = "originalTransactionDate", searchFieldPath = "accountingTransaction")
	private Date originalTransactionDate;

	@SearchField(searchField = "transactionDate", searchFieldPath = "accountingTransaction")
	private Date transactionDate;

	@SearchField(searchField = "settlementDate", searchFieldPath = "accountingTransaction")
	private Date settlementDate;

	@SearchField
	private BigDecimal remainingQuantity;
	@SearchField(searchField = "remainingQuantity")
	private BigDecimal unadjustedQuantity;
	@SearchField
	private BigDecimal priorQuantity;
	@SearchField
	private BigDecimal todayClosedQuantity;

	@SearchField
	private BigDecimal marketPrice;
	@SearchField
	private BigDecimal priorPrice;

	@SearchField
	private BigDecimal marketFxRate;
	@SearchField
	private BigDecimal priorFxRate;

	@SearchField
	private BigDecimal remainingCostBasisLocal; // remaining
	@SearchField
	private BigDecimal remainingCostBasisBase;

	// unrealized gain/loss (keep OTE name for backwards compatibility
	@SearchField
	private BigDecimal openTradeEquityLocal;
	@SearchField
	private BigDecimal openTradeEquityBase;
	@SearchField
	private BigDecimal priorOpenTradeEquityLocal;
	@SearchField
	private BigDecimal priorOpenTradeEquityBase;

	@SearchField
	private BigDecimal todayCommissionLocal;
	@SearchField
	private BigDecimal todayCommissionBase;
	@SearchField
	private BigDecimal todayRealizedGainLossLocal;
	@SearchField
	private BigDecimal todayRealizedGainLossBase;

	@SearchField
	private BigDecimal dailyGainLossLocal;

	@SearchField
	private BigDecimal dailyGainLossBase;

	@SearchField
	private BigDecimal accrualLocal;

	@SearchField
	private BigDecimal accrualBase;


	// custom filter that can restrict results to open positions only (excludes fully closed positions on the positionDate
	private boolean includeOpenOnly;

	// flag to include underlying price
	private boolean includeUnderlyingPrice;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(Long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getUnadjustedQuantity() {
		return this.unadjustedQuantity;
	}


	public void setUnadjustedQuantity(BigDecimal unadjustedQuantity) {
		this.unadjustedQuantity = unadjustedQuantity;
	}


	public BigDecimal getPriorQuantity() {
		return this.priorQuantity;
	}


	public void setPriorQuantity(BigDecimal priorQuantity) {
		this.priorQuantity = priorQuantity;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getPriorPrice() {
		return this.priorPrice;
	}


	public void setPriorPrice(BigDecimal priorPrice) {
		this.priorPrice = priorPrice;
	}


	public BigDecimal getMarketFxRate() {
		return this.marketFxRate;
	}


	public void setMarketFxRate(BigDecimal marketFxRate) {
		this.marketFxRate = marketFxRate;
	}


	public BigDecimal getPriorFxRate() {
		return this.priorFxRate;
	}


	public void setPriorFxRate(BigDecimal priorFxRate) {
		this.priorFxRate = priorFxRate;
	}


	public BigDecimal getRemainingCostBasisLocal() {
		return this.remainingCostBasisLocal;
	}


	public void setRemainingCostBasisLocal(BigDecimal remainingCostBasisLocal) {
		this.remainingCostBasisLocal = remainingCostBasisLocal;
	}


	public BigDecimal getRemainingCostBasisBase() {
		return this.remainingCostBasisBase;
	}


	public void setRemainingCostBasisBase(BigDecimal remainingCostBasisBase) {
		this.remainingCostBasisBase = remainingCostBasisBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public Integer getInvestmentInstrumentCurrencyDenominationId() {
		return this.investmentInstrumentCurrencyDenominationId;
	}


	public void setInvestmentInstrumentCurrencyDenominationId(Integer investmentInstrumentCurrencyDenominationId) {
		this.investmentInstrumentCurrencyDenominationId = investmentInstrumentCurrencyDenominationId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public Boolean getCollateralAccountingAccount() {
		return this.collateralAccountingAccount;
	}


	public void setCollateralAccountingAccount(Boolean collateralAccountingAccount) {
		this.collateralAccountingAccount = collateralAccountingAccount;
	}


	public Boolean getPositionAccountingAccount() {
		return this.positionAccountingAccount;
	}


	public void setPositionAccountingAccount(Boolean positionAccountingAccount) {
		this.positionAccountingAccount = positionAccountingAccount;
	}


	public Boolean getCashAccountingAccount() {
		return this.cashAccountingAccount;
	}


	public void setCashAccountingAccount(Boolean cashAccountingAccount) {
		this.cashAccountingAccount = cashAccountingAccount;
	}


	public BigDecimal getTodayClosedQuantity() {
		return this.todayClosedQuantity;
	}


	public void setTodayClosedQuantity(BigDecimal todayClosedQuantity) {
		this.todayClosedQuantity = todayClosedQuantity;
	}


	public BigDecimal getTodayCommissionLocal() {
		return this.todayCommissionLocal;
	}


	public void setTodayCommissionLocal(BigDecimal todayCommissionLocal) {
		this.todayCommissionLocal = todayCommissionLocal;
	}


	public BigDecimal getTodayCommissionBase() {
		return this.todayCommissionBase;
	}


	public void setTodayCommissionBase(BigDecimal todayCommissionBase) {
		this.todayCommissionBase = todayCommissionBase;
	}


	public BigDecimal getTodayRealizedGainLossLocal() {
		return this.todayRealizedGainLossLocal;
	}


	public void setTodayRealizedGainLossLocal(BigDecimal todayRealizedGainLossLocal) {
		this.todayRealizedGainLossLocal = todayRealizedGainLossLocal;
	}


	public BigDecimal getTodayRealizedGainLossBase() {
		return this.todayRealizedGainLossBase;
	}


	public void setTodayRealizedGainLossBase(BigDecimal todayRealizedGainLossBase) {
		this.todayRealizedGainLossBase = todayRealizedGainLossBase;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public void setInvestmentGroupName(String investmentGroupName) {
		this.investmentGroupName = investmentGroupName;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public String getHoldingInvestmentAccountMainPurpose() {
		return this.holdingInvestmentAccountMainPurpose;
	}


	public void setHoldingInvestmentAccountMainPurpose(String holdingInvestmentAccountMainPurpose) {
		this.holdingInvestmentAccountMainPurpose = holdingInvestmentAccountMainPurpose;
	}


	public String getHoldingInvestmentAccountRelatedPurpose() {
		return this.holdingInvestmentAccountRelatedPurpose;
	}


	public void setHoldingInvestmentAccountRelatedPurpose(String holdingInvestmentAccountRelatedPurpose) {
		this.holdingInvestmentAccountRelatedPurpose = holdingInvestmentAccountRelatedPurpose;
	}


	public Date getStartPositionDate() {
		return this.startPositionDate;
	}


	public void setStartPositionDate(Date startPositionDate) {
		this.startPositionDate = startPositionDate;
	}


	public Date getEndPositionDate() {
		return this.endPositionDate;
	}


	public void setEndPositionDate(Date endPositionDate) {
		this.endPositionDate = endPositionDate;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public BigDecimal getDailyGainLossLocal() {
		return this.dailyGainLossLocal;
	}


	public void setDailyGainLossLocal(BigDecimal dailyGainLossLocal) {
		this.dailyGainLossLocal = dailyGainLossLocal;
	}


	public BigDecimal getDailyGainLossBase() {
		return this.dailyGainLossBase;
	}


	public void setDailyGainLossBase(BigDecimal dailyGainLossBase) {
		this.dailyGainLossBase = dailyGainLossBase;
	}


	public Integer getFundAccountId() {
		return this.fundAccountId;
	}


	public void setFundAccountId(Integer fundAccountId) {
		this.fundAccountId = fundAccountId;
	}


	public Date getHoldingInvestmentAccountMainPurposeActiveOnDate() {
		return this.holdingInvestmentAccountMainPurposeActiveOnDate;
	}


	public void setHoldingInvestmentAccountMainPurposeActiveOnDate(Date holdingInvestmentAccountMainPurposeActiveOnDate) {
		this.holdingInvestmentAccountMainPurposeActiveOnDate = holdingInvestmentAccountMainPurposeActiveOnDate;
	}


	public Integer getClientAccountIssuingCompanyId() {
		return this.clientAccountIssuingCompanyId;
	}


	public void setClientAccountIssuingCompanyId(Integer clientAccountIssuingCompanyId) {
		this.clientAccountIssuingCompanyId = clientAccountIssuingCompanyId;
	}


	public Boolean getM2mCalculatorPresent() {
		return this.m2mCalculatorPresent;
	}


	public void setM2mCalculatorPresent(Boolean m2mCalculatorPresent) {
		this.m2mCalculatorPresent = m2mCalculatorPresent;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getUnderlyingHierarchyId() {
		return this.underlyingHierarchyId;
	}


	public void setUnderlyingHierarchyId(Short underlyingHierarchyId) {
		this.underlyingHierarchyId = underlyingHierarchyId;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public BigDecimal getAccrualLocal() {
		return this.accrualLocal;
	}


	public void setAccrualLocal(BigDecimal accrualLocal) {
		this.accrualLocal = accrualLocal;
	}


	public BigDecimal getAccrualBase() {
		return this.accrualBase;
	}


	public void setAccrualBase(BigDecimal accrualBase) {
		this.accrualBase = accrualBase;
	}


	public Date getSecurityEndDate() {
		return this.securityEndDate;
	}


	public void setSecurityEndDate(Date securityEndDate) {
		this.securityEndDate = securityEndDate;
	}


	public Integer getCountryOfRiskId() {
		return this.countryOfRiskId;
	}


	public void setCountryOfRiskId(Integer countryOfRiskId) {
		this.countryOfRiskId = countryOfRiskId;
	}


	public Integer getCountryOfIncorporationId() {
		return this.countryOfIncorporationId;
	}


	public void setCountryOfIncorporationId(Integer countryOfIncorporationId) {
		this.countryOfIncorporationId = countryOfIncorporationId;
	}


	public Short getExchangeId() {
		return this.exchangeId;
	}


	public void setExchangeId(Short exchangeId) {
		this.exchangeId = exchangeId;
	}


	public Short getCompositeExchangeId() {
		return this.compositeExchangeId;
	}


	public void setCompositeExchangeId(Short compositeExchangeId) {
		this.compositeExchangeId = compositeExchangeId;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public boolean isIncludeOpenOnly() {
		return this.includeOpenOnly;
	}


	public void setIncludeOpenOnly(boolean includeOpenOnly) {
		this.includeOpenOnly = includeOpenOnly;
	}


	public boolean isIncludeUnderlyingPrice() {
		return this.includeUnderlyingPrice;
	}


	public void setIncludeUnderlyingPrice(boolean includeUnderlyingPrice) {
		this.includeUnderlyingPrice = includeUnderlyingPrice;
	}
}
