package com.clifton.accounting.gl.journal.transactiontype;

import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.schema.SystemTable;


/**
 * Defines different transactions or events
 *
 * @author theodorez
 */
@CacheByName
public class AccountingTransactionType extends NamedHierarchicalEntity<AccountingTransactionType, Short> {

	public static final String TRADE = "Trade";
	public static final String BLOCK_TRADE = "Block Trade";
	public static final String INTERNAL_TRANSFER = "Internal Transfer";
	public static final String NON_INTERNAL_TRANSFER = "Non Internal Transfer";
	public static final String MATURITY = "Maturity";
	public static final String EARLY_TERMINATION = "Early Termination";
	public static final String EXCHANGE_FOR_PHYSICAL = "Exchange for Physical";
	public static final String ASSIGNMENT = "Assignment";
	public static final String DELIVERY = "Delivery";
	public static final String EXERCISE = "Exercise";


	public final static String SEPARATOR = " - ";

	/**
	 * An optional additionalScopeSystemTable that defines an additional scope source
	 */
	private SystemTable additionalScopeSystemTable;
	/**
	 * An optional label to display on the field in the UI
	 */
	private String additionalScopeUserInterfaceLabel;
	/**
	 * The path to the ID of the additional scope object
	 */
	private String additionalScopeBeanIdPath;

	private boolean transfer;

	private Short displayOrder;

	/**
	 * Determines if this Type requires a Commission Apply Method
	 * <p>
	 * (if true, the Commission Apply Method field will be displayed)
	 */
	private boolean commissionApplyMethodApplicable;

	/**
	 * If the Instrument or Hierarchy is OTC traded and this value is set, it will be dis played in the list of options
	 * <p>
	 * Otherwise, if false, will be excluded from the list of results
	 */
	private boolean restrictToOtcHierarchy;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getDelimiter() {
		return SEPARATOR;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTransfer() {
		return this.transfer;
	}


	public void setTransfer(boolean transfer) {
		this.transfer = transfer;
	}


	public Short getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}


	public boolean isCommissionApplyMethodApplicable() {
		return this.commissionApplyMethodApplicable;
	}


	public void setCommissionApplyMethodApplicable(boolean commissionApplyMethodApplicable) {
		this.commissionApplyMethodApplicable = commissionApplyMethodApplicable;
	}


	public boolean isRestrictToOtcHierarchy() {
		return this.restrictToOtcHierarchy;
	}


	public void setRestrictToOtcHierarchy(boolean restrictToOtcHierarchy) {
		this.restrictToOtcHierarchy = restrictToOtcHierarchy;
	}


	public SystemTable getAdditionalScopeSystemTable() {
		return this.additionalScopeSystemTable;
	}


	public void setAdditionalScopeSystemTable(SystemTable additionalScopeSystemTable) {
		this.additionalScopeSystemTable = additionalScopeSystemTable;
	}


	public String getAdditionalScopeUserInterfaceLabel() {
		return this.additionalScopeUserInterfaceLabel;
	}


	public void setAdditionalScopeUserInterfaceLabel(String additionalScopeUserInterfaceLabel) {
		this.additionalScopeUserInterfaceLabel = additionalScopeUserInterfaceLabel;
	}


	public String getAdditionalScopeBeanIdPath() {
		return this.additionalScopeBeanIdPath;
	}


	public void setAdditionalScopeBeanIdPath(String additionalScopeBeanIdPath) {
		this.additionalScopeBeanIdPath = additionalScopeBeanIdPath;
	}
}
