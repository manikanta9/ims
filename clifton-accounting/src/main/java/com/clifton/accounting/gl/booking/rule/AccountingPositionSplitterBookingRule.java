package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>AccountingPositionSplitterBookingRule</code> looks at each "Position" journal detail and if there is
 * an existing position in the opposite direction, splits session journal position records into close and open records.
 * <p>
 * New position is used to close as many open lots as possible and will be split to have a journal detail for each lot being
 * closed. If there is still quantity remaining after all existing lots have been closed, remainder will be used to open
 * a new position.
 * <p>
 * The rule skips any closing positions from splitting.
 *
 * @param <T>
 */
public class AccountingPositionSplitterBookingRule<T extends BookableEntity> extends AccountingCommonBookingRule<T> implements AccountingBookingRule<T> {

	public static final BigDecimal ROUNDING_ERROR_THRESHOLD = new BigDecimal("0.02");


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		ValidationUtils.assertNotEmpty(journal.getJournalDetailList(), "At least one detail must be present for journal: " + journal.getLabel());
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			if (isSplittablePosition(detail)) {
				doApplyRule(journal, bookingSession, detail);
			}
		}

		CollectionUtils.getIterable(journal.getJournalDetailList()).forEach(this::adjustAccountingJournalDetailDefinitionOriginalTransactionDate);
	}


	/**
	 * Adjusts the original transaction date on the provide detail to that of its parent's original transaction date when the detail is a currency or closing detail.
	 */
	private void adjustAccountingJournalDetailDefinitionOriginalTransactionDate(AccountingJournalDetailDefinition detail) {
		if (!detail.getAccountingAccount().isCurrency() && !detail.isClosing()) {
			return;
		}
		AccountingJournalDetailDefinition parentDefinition = detail.getParentDefinition();
		// Definitions are generated with negative numbers during booking. If the parent definition is less than this detail, the parent should be processed first for original transaction date.
		if (parentDefinition != null && MathUtils.isLessThan(parentDefinition.getId(), detail.getId())) {
			adjustAccountingJournalDetailDefinitionOriginalTransactionDate(parentDefinition);
		}
		// Details with a parent transaction should have the original transaction date matching the parent definition.
		// This assumes the parent detail was processed before the currency detail and still exists.
		AccountingJournalDetailDefinition parentDetail = ObjectUtils.coalesce(parentDefinition, detail.getParentTransaction());
		if (parentDetail != null && !DateUtils.isEqualWithoutTime(detail.getOriginalTransactionDate(), parentDetail.getOriginalTransactionDate())) {
			detail.setOriginalTransactionDate(parentDetail.getOriginalTransactionDate());
		}
	}


	/**
	 * Returns true if the specified journal detail is a position that may be split (not a closing).
	 */
	private boolean isSplittablePosition(AccountingJournalDetailDefinition detail) {
		AccountingAccount accountingAccount = detail.getAccountingAccount();
		if (accountingAccount.isPosition()) {
			// cannot split a closing position
			if (detail.getParentTransaction() == null || !detail.getInvestmentSecurity().equals(detail.getParentTransaction().getInvestmentSecurity())) {
				// cannot split reduction of Original Face to Current Face
				if (detail.getParentDefinition() == null || !detail.getInvestmentSecurity().equals(detail.getParentDefinition().getInvestmentSecurity())) {
					return true;
				}
			}
		}
		return false;
	}


	private void doApplyRule(AccountingJournal journal, BookingSession<T> bookingSession, AccountingJournalDetailDefinition detailToSplit) {
		// get open positions first to see if splitting is necessary
		List<BookingPosition> currentOpenPositions = getCurrentOpenPositions(bookingSession, detailToSplit);

		// calculate total quantity that could be closed (EXISTING POSITIONS)
		BigDecimal totalOpenPositionsQuantity = BigDecimal.ZERO;
		boolean closeOnMaturityOnly = InvestmentUtils.isCloseOnMaturityOnly(detailToSplit.getInvestmentSecurity());
		boolean earlyTermination = false;
		if (InvestmentUtils.isSecurityOfType(detailToSplit.getInvestmentSecurity(), InvestmentType.FORWARDS)
				&& DateUtils.compare(detailToSplit.getTransactionDate(), detailToSplit.getSettlementDate(), false) != 0) {
			// Currency Forwards can be early settled (if not Transaction Date == Settlement Date): DO NOT WAIT UNTIL MATURITY AND RUN THROUGH SPLITTER
			earlyTermination = true;
		}

		if (!closeOnMaturityOnly || earlyTermination) {
			// no splitting for securities that close on maturity (LME, Forwards, etc.)
			List<BookingPosition> earlyTerminationIgnoreList = null;
			for (BookingPosition pos : currentOpenPositions) {
				if (!MathUtils.isEqual(totalOpenPositionsQuantity, 0)) {
					if (!MathUtils.isSameSignum(totalOpenPositionsQuantity, pos.getRemainingQuantity())) {
						if (earlyTermination) {
							if (earlyTerminationIgnoreList == null) {
								earlyTerminationIgnoreList = new ArrayList<>();
								earlyTerminationIgnoreList.add(pos);
							}
							continue;
						}
						// should never have this: long and short lot at the same time
						throw new ValidationException("Cannot run " + detailToSplit + " journal detail through accounting position splitter because there are long and short open lots for '"
								+ detailToSplit.getInvestmentSecurity().getSymbol()
								+ "' at the same time on Settlement Date.\n\nUnbook corresponding journals and rebook them in correct order to fix the problem.");
					}
				}
				totalOpenPositionsQuantity = totalOpenPositionsQuantity.add(pos.getRemainingQuantity());
			}
			// remove all lots with the same sign (ok for early terminations)
			for (BookingPosition pos : CollectionUtils.getIterable(earlyTerminationIgnoreList)) {
				currentOpenPositions.remove(pos);
			}
		}

		// calculate original quantity to trade (adjust quantity for Factor if necessary: original + reduction to current = current)
		BigDecimal originalQuantityToTrade = detailToSplit.getQuantity();
		AccountingJournalDetailDefinition originalToCurrentFactorReduction = null;
		if (detailToSplit.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
			for (AccountingJournalDetailDefinition child : CollectionUtils.getIterable(journal.findChildEntries(detailToSplit))) {
				if (child.createNaturalKey().equals(detailToSplit.createNaturalKey())) {
					ValidationUtils.assertNull(originalToCurrentFactorReduction, "Cannot have more than child position entry that reduces original to current factor: " + detailToSplit);
					originalToCurrentFactorReduction = child;
					originalQuantityToTrade = MathUtils.add(originalQuantityToTrade, child.getQuantity());
				}
			}
		}

		// First check if we don't need to close any existing positions
		if (MathUtils.isNullOrZero(totalOpenPositionsQuantity) || (closeOnMaturityOnly && !earlyTermination) || MathUtils.isSameSignum(totalOpenPositionsQuantity, originalQuantityToTrade)) {
			// NO NEED TO SPLIT: the trade is moving in the same direction as the current position(s) or the trade is establishing a new position.
			detailToSplit.setOpening(true);
			if (StringUtils.isEmpty(detailToSplit.getDescription())) {
				// keep original description if was set
				detailToSplit.setDescription("Position opening of " + detailToSplit.getInvestmentSecurity().getSymbol());
			}
		}
		// Otherwise, there are existing position(s) that need to be closed by this detailToSplit
		else {
			// Will need to re-map Cash/Currency entries to new positions and removes original entries
			List<AccountingJournalDetailDefinition> afterSplitList = new ArrayList<>();

			// Check if we need to close all open positions (since the quantity of this transaction is greater than the total quantity of open positions and is in the opposite direction)
			if (originalQuantityToTrade.abs().compareTo(totalOpenPositionsQuantity.abs()) > 0) {
				// NEED TO SPLIT: close all existing open lot(s) and open new lot
				// Create 2+ journal details (the close rule will handle the details for multiple position records)
				// There is no need to worry about which positions, as long as the quantities are correct.
				BigDecimal openQuantity = originalQuantityToTrade.abs().subtract(totalOpenPositionsQuantity.abs()).multiply(new BigDecimal(originalQuantityToTrade.signum()));
				BigDecimal closeQuantity = totalOpenPositionsQuantity.multiply(new BigDecimal(originalQuantityToTrade.signum()));
				BigDecimal openingPortion = MathUtils.divide(openQuantity, originalQuantityToTrade);
				BigDecimal closingPortion = MathUtils.subtract(BigDecimal.ONE, openingPortion);

				BigDecimal originalFaceToTrade = detailToSplit.getQuantity();
				if (originalToCurrentFactorReduction != null) {
					// add reduction back to original face and remove it from the journal
					mergeJournalDetails(detailToSplit, originalToCurrentFactorReduction, journal);
					mergeCashJournalDetails(detailToSplit, bookingSession, journal);
				}

				// existing detail is modified to be the OPENING lot: rounded properly
				AccountingJournalDetail openingDetail = new AccountingJournalDetail();
				BeanUtils.copyProperties(detailToSplit, openingDetail);
				openingDetail.setQuantity(openingDetail.getInvestmentSecurity().isCurrency() ? null : openQuantity);
				allocateJournalDetailPortion(openingDetail, detailToSplit, openingPortion);
				openingDetail = addOpeningPosition(journal, openingDetail);
				afterSplitList.add(openingDetail);

				// CLOSING detail gets the rest: detailToSplit - opening (to avoid lost/gained pennies from rounding)
				AccountingJournalDetail closingDetail = new AccountingJournalDetail();
				BeanUtils.copyProperties(detailToSplit, closingDetail);
				closingDetail.setQuantity(closingDetail.getInvestmentSecurity().isCurrency() ? null : closeQuantity);
				// use the un-rounded close proportion for closing details
				allocateJournalDetailPortion(closingDetail, detailToSplit, closingPortion, closingPortion.scale());
				generateClosePositions(journal, closingDetail, currentOpenPositions, afterSplitList);

				if (originalToCurrentFactorReduction != null) {
					// split the opening into 2 entries: Original Quantity and Reduction to Current Quantity (both will sum to openQuantity)
					AccountingJournalDetail reductionToCurrentFace = new AccountingJournalDetail();
					BeanUtils.copyProperties(openingDetail, reductionToCurrentFace);
					reductionToCurrentFace.setParentDefinition(openingDetail);
					reductionToCurrentFace.setOpening(false);
					reductionToCurrentFace.setExecutingCompany(detailToSplit.getExecutingCompany());

					BigDecimal factor = MathUtils.divide(originalQuantityToTrade, originalFaceToTrade);
					openingDetail.setQuantity(MathUtils.divide(openQuantity, factor, 2));
					String unadjustedQuantityName = InvestmentUtils.getUnadjustedQuantityFieldName(openingDetail.getInvestmentSecurity());
					openingDetail.setDescription("Position opening at new " + unadjustedQuantityName + " for " + openingDetail.getInvestmentSecurity().getSymbol());
					allocateJournalDetailPortion(openingDetail, openingDetail, MathUtils.divide(BigDecimal.ONE, factor));

					// reduction = openingDetail - reductionToCurrentFace
					reductionToCurrentFace.setQuantity(MathUtils.subtract(reductionToCurrentFace.getQuantity(), openingDetail.getQuantity()));
					reductionToCurrentFace.setLocalDebitCredit(MathUtils.subtract(reductionToCurrentFace.getLocalDebitCredit(), openingDetail.getLocalDebitCredit()));
					reductionToCurrentFace.setBaseDebitCredit(MathUtils.subtract(reductionToCurrentFace.getBaseDebitCredit(), openingDetail.getBaseDebitCredit()));
					reductionToCurrentFace.setPositionCostBasis(MathUtils.subtract(reductionToCurrentFace.getPositionCostBasis(), openingDetail.getPositionCostBasis()));
					reductionToCurrentFace.setPositionCommission(MathUtils.subtract(reductionToCurrentFace.getPositionCommission(), openingDetail.getPositionCommission()));
					reductionToCurrentFace.setDescription("Reducing " + unadjustedQuantityName + " to " + InvestmentUtils.getQuantityFieldName(openingDetail.getInvestmentSecurity()) + " for " + reductionToCurrentFace.getInvestmentSecurity().getSymbol());

					journal.addJournalDetail(reductionToCurrentFace);
					afterSplitList.add(reductionToCurrentFace);
				}
			}
			else {
				// FULL/PARTIAL closing
				BigDecimal originalFaceToTrade = detailToSplit.getQuantity();
				if (originalToCurrentFactorReduction != null) {
					// add reduction back to original face and remove it from the journal
					mergeJournalDetails(detailToSplit, originalToCurrentFactorReduction, journal);
					mergeCashJournalDetails(detailToSplit, bookingSession, journal);
				}

				boolean doNotPartiallyClose = detailToSplit.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null;

				// close existing open lot(s); no new position is created yet
				BookingPosition lastClosed = generateClosePositions(journal, detailToSplit, currentOpenPositions, afterSplitList);

				if (doNotPartiallyClose && !MathUtils.isEqual(originalQuantityToTrade.negate(), totalOpenPositionsQuantity)) {
					// partial close at factor: need to fully close lastClosed position at cost
					// and reopen it at new Original Face and reduction to Current face using trade factor but original price
					BigDecimal factor = MathUtils.divide(originalQuantityToTrade, originalFaceToTrade);

					// close remaining of position being partially closed at cost
					AccountingJournalDetail remainder = new AccountingJournalDetail();
					AccountingTransaction t = getAccountingTransactionService().getAccountingTransaction(lastClosed.getPosition().getId());
					BeanUtils.copyProperties(t, remainder);
					remainder.setFactorChangeQuantityAdjustment(true);
					remainder.setExecutingCompany(detailToSplit.getExecutingCompany());
					remainder.setQuantity(lastClosed.getRemainingQuantity().negate());
					remainder.setParentTransaction(t);
					remainder.setSystemTable(detailToSplit.getSystemTable());
					remainder.setFkFieldId(detailToSplit.getFkFieldId());
					remainder.setTransactionDate(detailToSplit.getTransactionDate());
					remainder.setSettlementDate(detailToSplit.getSettlementDate());
					remainder.setOriginalTransactionDate(remainder.getParentTransaction().getOriginalTransactionDate());
					allocateJournalDetailPortion(remainder, remainder, MathUtils.divide(remainder.getQuantity(), lastClosed.getPosition().getQuantityNormalized()));
					generateClosePositions(journal, remainder, currentOpenPositions, afterSplitList);

					// open remainder at Original Face and reduce it to Current (remainder) face
					AccountingJournalDetail originalFace = new AccountingJournalDetail();
					BeanUtils.copyProperties(remainder, originalFace);
					originalFace.setFactorChangeQuantityAdjustment(true);
					// opening lots from partial closes always reference the original opening lot via parent
					originalFace.setParentTransaction(t.getParentTransaction() == null ? t : t.getParentTransaction());
					originalFace.setOpening(true);
					String unadjustedQuantityName = InvestmentUtils.getUnadjustedQuantityFieldName(originalFace.getInvestmentSecurity());
					originalFace.setQuantity(MathUtils.divide(originalFace.getQuantity().negate(), factor, 2));
					originalFace.setDescription("Position opening at new " + unadjustedQuantityName + " for " + originalFace.getInvestmentSecurity().getSymbol());
					originalFace.setOriginalTransactionDate(originalFace.getParentTransaction().getOriginalTransactionDate());
					allocateJournalDetailPortion(originalFace, originalFace, MathUtils.divide(BigDecimal.ONE, factor.negate()));
					journal.addJournalDetail(originalFace);
					afterSplitList.add(originalFace);

					if (!MathUtils.isEqual(factor, BigDecimal.ONE)) {
						// reduction = openingDetail - reductionToCurrentFace
						AccountingJournalDetail reductionToCurrentFace = new AccountingJournalDetail();
						BeanUtils.copyProperties(remainder, reductionToCurrentFace);
						reductionToCurrentFace.setFactorChangeQuantityAdjustment(true);
						reductionToCurrentFace.setParentTransaction(null);
						reductionToCurrentFace.setParentDefinition(originalFace);
						reductionToCurrentFace.setOpening(false);
						reductionToCurrentFace.setQuantity(MathUtils.subtract(reductionToCurrentFace.getQuantity().negate(), originalFace.getQuantity()));
						reductionToCurrentFace.setLocalDebitCredit(MathUtils.subtract(reductionToCurrentFace.getLocalDebitCredit().negate(), originalFace.getLocalDebitCredit()));
						reductionToCurrentFace.setBaseDebitCredit(MathUtils.subtract(reductionToCurrentFace.getBaseDebitCredit().negate(), originalFace.getBaseDebitCredit()));
						reductionToCurrentFace.setPositionCostBasis(MathUtils.subtract(reductionToCurrentFace.getPositionCostBasis().negate(), originalFace.getPositionCostBasis()));
						reductionToCurrentFace.setPositionCommission(MathUtils.subtract(MathUtils.negate(reductionToCurrentFace.getPositionCommission()), originalFace.getPositionCommission()));
						reductionToCurrentFace.setDescription("Reducing " + unadjustedQuantityName + " to " + InvestmentUtils.getQuantityFieldName(originalFace.getInvestmentSecurity()) + " for " + reductionToCurrentFace.getInvestmentSecurity().getSymbol());
						reductionToCurrentFace.setOriginalTransactionDate(originalFace.getParentTransaction().getOriginalTransactionDate());
						journal.addJournalDetail(reductionToCurrentFace);
						afterSplitList.add(reductionToCurrentFace);
					}
				}
			}

			// make sure we don't lose pennies due to rounding
			AccountingJournalDetailDefinition adjustedJournal = adjustAllocationForPennyRounding(afterSplitList, detailToSplit, null);

			// remove position that was split
			List<AccountingJournalDetailDefinition> removeList = journal.findChildEntries(detailToSplit);
			journal.removeJournalDetail(detailToSplit);
			// remove children of original position and proportionally allocate them to new after split positions
			for (AccountingJournalDetailDefinition removeChild : CollectionUtils.getIterable(removeList)) {
				if (MathUtils.isNullOrZero(removeChild.getQuantityNormalized()) && MathUtils.isNullOrZero(removeChild.getLocalDebitCredit())) {
					// just remove pointless entry
					journal.removeJournalDetail(removeChild);
				}
				else if (afterSplitList.size() == 1) {
					// no need to remove: remap to new after split parent
					removeChild.setParentDefinition(afterSplitList.get(0));
					updateCashCurrencyRecordDescription(removeChild, false);
				}
				else {
					journal.removeJournalDetail(removeChild);

					// proportionally split removed detail across after split positions
					BigDecimal totalQuantity = CoreMathUtils.sumProperty(afterSplitList, AccountingJournalDetailDefinition::getQuantityNormalized);
					List<AccountingJournalDetailDefinition> allocationList = splitCashRecordAcrossDetails(afterSplitList, removeChild, journal, totalQuantity, true);

					// make sure we don't lose pennies due to rounding
					if (!CollectionUtils.isEmpty(allocationList)) {
						adjustAllocationForPennyRounding(allocationList, removeChild, adjustedJournal);
					}
				}
			}
		}
	}


	/**
	 * Creates a new cash record "slice" based on an old cash record and a proportion.
	 *
	 * @param oldCashRecord        the cash record being broken up into multiple slices
	 * @param parentRecord         the parent detail that this cash record should point to (may be null)
	 * @param allocationProportion the proportion that this new cash record slice is of <code>oldCashRecord</code>
	 */
	private AccountingJournalDetailDefinition generateCashRecordSlice(AccountingJournalDetailDefinition oldCashRecord, AccountingJournalDetailDefinition parentRecord, BigDecimal allocationProportion) {
		AccountingJournalDetail allocation = new AccountingJournalDetail();
		BeanUtils.copyProperties(oldCashRecord, allocation);

		if (parentRecord != null) {
			allocation.setParentDefinition(parentRecord);
			allocation.setOriginalTransactionDate(parentRecord.getOriginalTransactionDate()); // keep original transaction date so that everything balances
		}

		allocateJournalDetailPortion(allocation, oldCashRecord, allocationProportion);
		updateCashCurrencyRecordDescription(allocation, false);

		return allocation;
	}


	/**
	 * Proportionally splits the cash record across multiple details.
	 *
	 * @param detailDefinitions    the iterable collection of details to split the cash record across
	 * @param cashRecordBeingSplit the cash record being split
	 * @param journal              the journal to add the new cash records to
	 * @param total                the total of <code>list</code> (either total quantity or cost basis, depending on the value of <code>useQuantityForProportion</code>)
	 * @param totalIsTotalQuantity if true, the total is the total quantity, otherwise it is the total cost basis
	 */
	private List<AccountingJournalDetailDefinition> splitCashRecordAcrossDetails(Iterable<AccountingJournalDetailDefinition> detailDefinitions, AccountingJournalDetailDefinition cashRecordBeingSplit, AccountingJournal journal, BigDecimal total,
	                                                                             boolean totalIsTotalQuantity) {

		List<AccountingJournalDetailDefinition> allocationList = new ArrayList<>();
		for (AccountingJournalDetailDefinition afterSplit : detailDefinitions) {
			BigDecimal allocationPortion = MathUtils.divide(totalIsTotalQuantity ? afterSplit.getQuantity() : afterSplit.getPositionCostBasis(), total);
			AccountingJournalDetailDefinition allocation = generateCashRecordSlice(cashRecordBeingSplit, afterSplit, allocationPortion);

			// skip 0 entries
			if (!(MathUtils.isNullOrZero(allocation.getQuantity()) && MathUtils.isNullOrZero(allocation.getLocalDebitCredit()))) {
				journal.addJournalDetail(allocation);
				allocationList.add(allocation);
			}
		}

		return allocationList;
	}


	/**
	 * Creates new opening position Journal Detail which is a copy of the specified positionEntry
	 * and adds it to the specified journal.
	 */
	private AccountingJournalDetail addOpeningPosition(AccountingJournal journal, AccountingJournalDetailDefinition positionEntry) {
		AccountingJournalDetail openDetail = new AccountingJournalDetail();
		BeanUtils.copyProperties(positionEntry, openDetail);
		openDetail.setOpening(true);
		if (StringUtils.isEmpty(positionEntry.getDescription())) {
			// keep original description if was set
			openDetail.setDescription("Position opening of " + positionEntry.getInvestmentSecurity().getSymbol());
		}

		journal.addJournalDetail(openDetail);
		return openDetail;
	}


	/**
	 * Adds journal detail(s) for closing positions to the specified journal.
	 * Uses the specified closingDetail to close existing position(s).
	 * Multiple entries maybe created if closingDetail quantity is bigger than the first open lot remaining quantity.
	 *
	 * @param journal
	 * @param closingDetail
	 * @param currentOpenPositions
	 * @param afterSplitList       adds newly split positions to this list
	 * @return returns the BookingPosition that was closed (fully or partially)
	 */
	private BookingPosition generateClosePositions(AccountingJournal journal, AccountingJournalDetailDefinition closingDetail, List<BookingPosition> currentOpenPositions,
	                                               List<AccountingJournalDetailDefinition> afterSplitList) {
		BigDecimal remainingQtyToClose = closingDetail.getQuantity().abs();

		if (CollectionUtils.isEmpty(currentOpenPositions)) {
			throw new RuntimeException("Cannot close a position for " + closingDetail.getClientInvestmentAccount().getName() + " with SecurityID of " + closingDetail.getInvestmentSecurity().getId()
					+ " because it has no positions that are open.  ");
		}

		BigDecimal totalOpenQty = BigDecimal.ZERO;
		for (BookingPosition pos : currentOpenPositions) {
			totalOpenQty = totalOpenQty.add(pos.getRemainingQuantity().abs());
		}

		if (remainingQtyToClose.compareTo(totalOpenQty.abs()) > 0) {
			throw new RuntimeException("Trying to close " + remainingQtyToClose.intValue() + " positions when there are only " + totalOpenQty + " available. ID=[" + closingDetail.getFkFieldId()
					+ "] SecurityID=[" + closingDetail.getInvestmentSecurity() + "]");
		}

		BookingPosition lastClosed = null;
		Iterator<BookingPosition> iterator = currentOpenPositions.iterator();
		while (iterator.hasNext() && !MathUtils.isNullOrZero(remainingQtyToClose)) {
			BookingPosition positionBeingClosed = iterator.next();
			// skip lots that already have been fully closed in this booking session
			if (!MathUtils.isNullOrZero(positionBeingClosed.getRemainingQuantity())) {
				AccountingJournalDetail closingPosition = createClosingPosition(closingDetail, positionBeingClosed, remainingQtyToClose);
				journal.addJournalDetail(closingPosition);

				remainingQtyToClose = remainingQtyToClose.subtract(closingPosition.getQuantity().abs());
				afterSplitList.add(closingPosition);
				lastClosed = positionBeingClosed;
			}
		}
		return lastClosed;
	}


	/**
	 * Returns fully populated closing journal detail for the specified arguments.
	 *
	 * @param closingDetail       if position being closed is smaller than the closing detail, use only a portion of closingDetail that's needed
	 * @param positionBeingClosed position that is being closed by the specified closingDetail
	 * @param qtyToClose          the quantity to close - must be a positive number.
	 */
	private AccountingJournalDetail createClosingPosition(AccountingJournalDetailDefinition closingDetail, BookingPosition positionBeingClosed, BigDecimal qtyToClose) {
		if (MathUtils.isNegative(qtyToClose)) {
			throw new IllegalArgumentException("qtyToClose cannot be negative");
		}

		AccountingJournalDetail result = new AccountingJournalDetail();
		BeanUtils.copyProperties(closingDetail, result);
		if (positionBeingClosed.getPosition().isNewBean()) {
			// closing a new position from the same journal that has not been created in GL yet
			result.setParentDefinition(positionBeingClosed.getPosition().getOpeningTransaction());
			if (result.getParentDefinition() != null) {
				result.setOriginalTransactionDate(result.getParentDefinition().getOriginalTransactionDate());
			}
		}
		else {
			result.setParentTransaction(getAccountingTransactionService().getAccountingTransaction(positionBeingClosed.getPosition().getId()));
			result.setOriginalTransactionDate(result.getParentTransaction().getOriginalTransactionDate());
		}

		boolean partialClose;
		if (MathUtils.isGreaterThanOrEqual(qtyToClose, positionBeingClosed.getRemainingQuantity().abs())) {
			partialClose = false;
			result.setQuantity(positionBeingClosed.getRemainingQuantity().negate());
		}
		else {
			partialClose = true;
			result.setQuantity(qtyToClose.multiply(new BigDecimal(closingDetail.getQuantity().signum())));
		}

		if (StringUtils.isEmpty(result.getDescription())) {
			// keep original description if was set
			result.setDescription((partialClose ? "Partial" : "Full") + " position close for " + result.getInvestmentSecurity().getSymbol());
		}
		else if (result.getDescription().startsWith("Open ")) {
			// overwrite description for currency open from foreign position close that became a close of existing currency position
			result.setDescription("Close" + result.getDescription().substring(4));
		}
		else if (result.getDescription().startsWith("Position opening ")) {
			// overwrite description for position open from REPO/Transfer that became a close of existing position
			result.setDescription((partialClose ? "Partial" : "Full") + " position close " + result.getDescription().substring(16));
		}

		BigDecimal closingPortion = MathUtils.divide(result.getQuantity(), closingDetail.getQuantity()).abs();
		allocateJournalDetailPortion(result, closingDetail, closingPortion);
		result.setOpening(false);

		// update remaining quantity to close to avoid double closing
		positionBeingClosed.setRemainingQuantity(positionBeingClosed.getRemainingQuantity().add(result.getQuantity()));

		return result;
	}


	/**
	 * Adjust largest definition DebitCredit/CostBasis/Commission if necessary so that the sum of allocations equals the total.
	 */
	private AccountingJournalDetailDefinition adjustAllocationForPennyRounding(List<AccountingJournalDetailDefinition> allocationList, AccountingJournalDetailDefinition total, AccountingJournalDetailDefinition parentOfDetailToAdjust) {
		Lazy<AccountingJournalDetailDefinition> detailToAdjust = new Lazy<>(() -> {
			if (parentOfDetailToAdjust == null) {
				// sort by local debit credit, then transaction date, and id
				return sortAndFilterSTransactionList(new ArrayList<>(allocationList));
			}
			else {
				return sortAndFilterSTransactionList(parentOfDetailToAdjust.getJournal().findChildEntries(parentOfDetailToAdjust).stream()
						.filter(detail -> detail.getAccountingAccount().isCash() || detail.getAccountingAccount().isCurrency())
						.collect(Collectors.toList()));
			}
		});

		CoreMathUtils.applySumPropertyDifference(allocationList, AccountingJournalDetailDefinition::getLocalDebitCredit, AccountingJournalDetailDefinition::setLocalDebitCredit, total.getLocalDebitCredit(), detailToAdjust, ROUNDING_ERROR_THRESHOLD);
		CoreMathUtils.applySumPropertyDifference(allocationList, AccountingJournalDetailDefinition::getBaseDebitCredit, AccountingJournalDetailDefinition::setBaseDebitCredit, total.getBaseDebitCredit(), detailToAdjust, ROUNDING_ERROR_THRESHOLD);
		CoreMathUtils.applySumPropertyDifference(allocationList, AccountingJournalDetailDefinition::getPositionCostBasis, AccountingJournalDetailDefinition::setPositionCostBasis, total.getPositionCostBasis(), detailToAdjust, ROUNDING_ERROR_THRESHOLD);
		CoreMathUtils.applySumPropertyDifference(allocationList, AccountingJournalDetailDefinition::getPositionCommission, AccountingJournalDetailDefinition::setPositionCommission, total.getPositionCommission(), detailToAdjust, ROUNDING_ERROR_THRESHOLD);

		return detailToAdjust.get();
	}


	private AccountingJournalDetailDefinition sortAndFilterSTransactionList(List<AccountingJournalDetailDefinition> allocationList) {
		return CollectionUtils.getFirstElement(BeanUtils.filter(
				BeanUtils.sortWithFunction(
						BeanUtils.sortWithFunction(
								BeanUtils.sortWithFunction(allocationList, AccountingJournalDetailDefinition::getId, true)
								, AccountingJournalDetailDefinition::getOriginalTransactionDate, false)
						, AccountingJournalDetailDefinition::getLocalDebitCredit, false)
				, this::isAdjustmentApplicable));
	}


	/**
	 * If the security has factor changes (CDS,ABS), only apply adjustments to transactions where the original transaction date matches the transaction date.  This will remove
	 * the Full position close and reopening as well as the reducing entry that are create for securities with factor changes.  Transaction -16, -17, and -18 will be excluded
	 * in the example below.
	 * <p>
	 * -14	3828959	222491	047-484696	Position	XY2106D18U0500XXI	108.29	1,527,750	-126,650.48	3/9/2015	1	-126,650.48	-126,650.48	3/9/2015	3/9/2015	Full position close  at Original Notional for XY2106D18U0500XXI
	 * -15	3478790	222491	047-484696	Position	XY2106D18U0500XXI	108.29	1,576,250	-130,671.13	3/9/2015	1	-130,671.13	-130,671.13	3/9/2015	3/9/2015	Partial position close  at Original Notional for XY2106D18U0500XXI
	 * -16	3478790	222491	047-484696	Position	XY2106D18U0500XXI	107.5	4,631,750	-347,381.25	3/9/2015	1	-347,381.25	-347,381.25	5/1/2014	3/9/2015	Full position close  of XY2101D18U0500XXI
	 * -17	3478790	222491	047-484696	Position	XY2106D18U0500XXI	107.5	-4,775,000	358,125.01	3/9/2015	1	358,125.01	358,125.01	5/1/2014	3/9/2015	Position opening at new Original Notional for XY2106D18U0500XXI
	 * -18	-17	222491	047-484696	Position	XY2106D18U0500XXI	107.5	143,250	-10,743.75	3/9/2015	1	-10,743.75	-10,743.75	5/1/2014	3/9/2015	Reducing Original Notional to Current Notional for XY2106D18U0500XXI
	 * -19	-14	222491	047-484696	Cash	USD			130,567.50	3/9/2015	1	130,567.50	0	3/9/2015	3/9/2015	Cash proceeds from close of XY2106D18U0500XXI
	 * -20	-15	222491	047-484696	Cash	USD			134,712.50	3/9/2015	1	134,712.50	0	3/9/2015	3/9/2015	Cash proceeds from close of XY2106D18U0500XXI
	 * -21	-14	222491	047-484696	Cash	USD			-3,917.02	3/9/2015	1	-3,917.02	0	3/9/2015	3/9/2015	Cash expense from close of XY2106D18U0500XXI
	 * -22	-15	222491	047-484696	Cash	USD			-4,041.38	3/9/2015	1	-4,041.38	0	3/9/2015	3/9/2015	Cash expense from close of XY2106D18U0500XXI
	 */
	private boolean isAdjustmentApplicable(AccountingJournalDetailDefinition allocation) {
		return !allocation.isFactorChangeQuantityAdjustment();
	}


	/**
	 * toAmount = ROUND(fromAmount * allocationPortion, 2)
	 */
	private void allocateJournalDetailPortion(AccountingJournalDetailDefinition toAllocation, AccountingJournalDetailDefinition fromAllocation, BigDecimal allocationPortion) {
		allocateJournalDetailPortion(toAllocation, fromAllocation, allocationPortion, 2);
	}


	/**
	 * When allocating proportions (e.g. closing lots) it can be important to use exact proportions without rounding
	 * <p>
	 * toAmount = ROUND(fromAmount * allocationPortion, scale)
	 */
	private void allocateJournalDetailPortion(AccountingJournalDetailDefinition toAllocation, AccountingJournalDetailDefinition fromAllocation, BigDecimal allocationPortion, int scale) {
		toAllocation.setLocalDebitCredit(MathUtils.multiply(fromAllocation.getLocalDebitCredit(), allocationPortion, scale));
		toAllocation.setBaseDebitCredit(MathUtils.multiply(fromAllocation.getBaseDebitCredit(), allocationPortion, scale));
		toAllocation.setPositionCostBasis(MathUtils.multiply(fromAllocation.getPositionCostBasis(), allocationPortion, scale));
		toAllocation.setPositionCommission(MathUtils.multiply(fromAllocation.getPositionCommission(), allocationPortion, scale));
	}


	/**
	 * Merges the from detail into the to detail: adds all 'from' numbers to 'to' numbers, remaps all from children and removes from.
	 */
	private void mergeJournalDetails(AccountingJournalDetailDefinition to, AccountingJournalDetailDefinition from, AccountingJournal journal) {
		to.setQuantity(MathUtils.add(to.getQuantity(), from.getQuantity()));
		to.setLocalDebitCredit(MathUtils.add(to.getLocalDebitCredit(), from.getLocalDebitCredit()));
		to.setBaseDebitCredit(MathUtils.add(to.getBaseDebitCredit(), from.getBaseDebitCredit()));
		to.setPositionCostBasis(MathUtils.add(to.getPositionCostBasis(), from.getPositionCostBasis()));
		to.setPositionCommission(MathUtils.add(to.getPositionCommission(), from.getPositionCommission()));
		// re-map child records and remove the reduction record
		for (AccountingJournalDetailDefinition child : CollectionUtils.getIterable(journal.findChildEntries(from))) {
			child.setParentDefinition(to);
		}
		journal.removeJournalDetail(from);
	}


	private void mergeCashJournalDetails(AccountingJournalDetailDefinition parent, BookingSession<T> bookingSession, AccountingJournal journal) {
		List<AccountingJournalDetailDefinition> transactionList = journal.findChildEntries(parent).stream()
				.filter(detail -> detail.getAccountingAccount().isCash() || detail.getAccountingAccount().isCurrency())
				.collect(Collectors.toList());


		Map<String, List<AccountingJournalDetailDefinition>> cashTransactionMap = BeanUtils.getBeansMap(transactionList, bookingSession::getPositionKey);
		for (Map.Entry<String, List<AccountingJournalDetailDefinition>> entry : CollectionUtils.getIterable(cashTransactionMap.entrySet())) {
			List<AccountingJournalDetailDefinition> cashTransactionList = entry.getValue();
			if (!CollectionUtils.isEmpty(cashTransactionList)) {
				AccountingJournalDetailDefinition firstCashRecord = CollectionUtils.getFirstElement(cashTransactionList);
				AccountingJournalDetail result = new AccountingJournalDetail();
				BeanUtils.copyProperties(firstCashRecord, result);

				result.setLocalDebitCredit(CoreMathUtils.sumProperty(cashTransactionList, AccountingJournalDetailDefinition::getLocalDebitCredit));
				result.setBaseDebitCredit(CoreMathUtils.sumProperty(cashTransactionList, AccountingJournalDetailDefinition::getBaseDebitCredit));
				result.setPositionCostBasis(CoreMathUtils.sumProperty(cashTransactionList, AccountingJournalDetailDefinition::getPositionCostBasis));

				journal.addJournalDetail(result);
				CollectionUtils.getIterable(cashTransactionList).forEach(journal::removeJournalDetail);
			}
		}
	}
}
