package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.List;


/**
 * The <code>BaseBookingProcessor</code> class is an abstract class that can be extended by all AccountingBookingProcessor(s).
 * It provides default implementation of basic methods.
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class BaseBookingProcessor<T extends BookableEntity> implements AccountingBookingProcessor<T> {

	private DaoLocator daoLocator;
	private AccountingJournalService accountingJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generic implementation that works for all booking source that must always book or delete all entities.
	 */
	@Override
	public List<T> getUnbookedEntityList() {
		AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalTypeByName(getJournalTypeName());
		ValidationUtils.assertNotNull(journalType, "Cannot find journal type for with name = " + getJournalTypeName());

		ReadOnlyDAO<T> dao = getDaoLocator().locate(journalType.getSystemTable().getName());
		if (!dao.getConfiguration().isValidBeanField("bookingDate")) {
			throw new RuntimeException("Cannot find bookingDate field on " + dao.getConfiguration().getBeanClass());
		}

		return dao.findByField("bookingDate", (Object) null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}
}
