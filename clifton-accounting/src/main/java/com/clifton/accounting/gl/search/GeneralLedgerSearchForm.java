package com.clifton.accounting.gl.search;

import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * The GeneralLedgerSearchForm class combines common attributes of Balance/Position lookup search forms (including support for pending activity) in a single reusable place.
 *
 * @author vgomelsky
 */
public abstract class GeneralLedgerSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer[] investmentSecurityIds;

	@SearchField(searchField = "investmentSecurity.instrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "investmentSecurity.instrument.id")
	private Integer[] investmentInstrumentIds;

	@SearchField(searchFieldPath = "investmentSecurity.instrument", searchField = "hierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchFieldPath = "investmentSecurity.instrument.hierarchy", searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short[] investmentGroupIds;

	@SearchField(searchField = "groupItemList.referenceOne.id", searchFieldPath = "investmentSecurity", comparisonConditions = ComparisonConditions.EXISTS)
	private Short[] investmentSecurityGroupIds;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "investmentSecurity", sortField = "underlyingSecurity.symbol")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientInvestmentAccountGroupId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount", sortField = "issuingCompany.name")
	private Integer holdingInvestmentCompanyId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "holdingInvestmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer holdingInvestmentAccountGroupId;


	// custom search fields: aggregates data up to and including this date.
	// only transaction or settlement date can and must be set
	private Date transactionDate;
	private Date settlementDate;

	//Custom command: include pending transactions (pending trades, transfers, etc.)
	private PendingActivityRequests pendingActivityRequest;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Transaction Date if it is not null.  Returns Settlement Date otherwise.
	 */
	public Date getTransactionOrSettlementDate() {
		return getTransactionDate() == null ? getSettlementDate() : getTransactionDate();
	}


	/**
	 * Note: if Settlement Date lookup is used, then this method will return null.
	 */
	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	/**
	 * Note: if Transaction Date lookup is used, then this method will return null.
	 */
	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer[] getInvestmentSecurityIds() {
		return this.investmentSecurityIds;
	}


	public void setInvestmentSecurityIds(Integer[] investmentSecurityIds) {
		this.investmentSecurityIds = investmentSecurityIds;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer[] getInvestmentInstrumentIds() {
		return this.investmentInstrumentIds;
	}


	public void setInvestmentInstrumentIds(Integer[] investmentInstrumentIds) {
		this.investmentInstrumentIds = investmentInstrumentIds;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short[] getInvestmentGroupIds() {
		return this.investmentGroupIds;
	}


	public void setInvestmentGroupIds(Short[] investmentGroupIds) {
		this.investmentGroupIds = investmentGroupIds;
	}


	public Short[] getInvestmentSecurityGroupIds() {
		return this.investmentSecurityGroupIds;
	}


	public void setInvestmentSecurityGroupIds(Short[] investmentSecurityGroupIds) {
		this.investmentSecurityGroupIds = investmentSecurityGroupIds;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public Integer getHoldingInvestmentCompanyId() {
		return this.holdingInvestmentCompanyId;
	}


	public void setHoldingInvestmentCompanyId(Integer holdingInvestmentCompanyId) {
		this.holdingInvestmentCompanyId = holdingInvestmentCompanyId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public PendingActivityRequests getPendingActivityRequest() {
		return this.pendingActivityRequest;
	}


	public void setPendingActivityRequest(PendingActivityRequests pendingActivityRequest) {
		this.pendingActivityRequest = pendingActivityRequest;
	}
}
