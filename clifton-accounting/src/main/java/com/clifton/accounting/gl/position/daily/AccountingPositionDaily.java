package com.clifton.accounting.gl.position.daily;


import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.position.AccountingPositionInfo;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.InvestmentM2MCalculatorTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


/**
 * The <code>AccountingPositionDaily</code> class represents a daily snapshot of open positions (lot level).
 * It also contains positions that were closed on that day (remainingQuantity = 0).
 * <p>
 * NOTE: "prior" refers to previous weekday because different securities use different calendars.
 * When using this logic around holidays, you may need to add the data from Holiday to avoid gaps.
 */
public class AccountingPositionDaily extends BaseSimpleEntity<Long> implements AccountingPositionInfo {

	private AccountingTransactionInfo accountingTransaction; // references the opening position lot

	// redundant to 'accountingTransaction.clientInvestmentAccount', used to improve SQL performance for SELECT and DELETE statements
	private InvestmentAccount clientInvestmentAccount;

	private Date positionDate;

	private BigDecimal priorQuantity = BigDecimal.ZERO;
	private BigDecimal remainingQuantity = BigDecimal.ZERO;

	/**
	 * Units closed on "positionDate": if open and full close on same day will have trade quantity and remaining and prior will be 0.
	 * Positive number means quantity bought (buy to close) and negative number quantity sold (sell to close).
	 */
	private BigDecimal todayClosedQuantity = BigDecimal.ZERO;

	private BigDecimal marketPrice;
	private BigDecimal priorPrice;

	private BigDecimal marketFxRate;
	private BigDecimal priorFxRate;

	private BigDecimal remainingCostBasisLocal = BigDecimal.ZERO;
	private BigDecimal remainingCostBasisBase = BigDecimal.ZERO;

	private BigDecimal notionalValueLocal = BigDecimal.ZERO;
	private BigDecimal notionalValueBase = BigDecimal.ZERO;
	private BigDecimal priorNotionalValueLocal = BigDecimal.ZERO;
	private BigDecimal priorNotionalValueBase = BigDecimal.ZERO;

	// unrealized gain/loss (keep OTE name for backwards compatibility
	private BigDecimal openTradeEquityLocal = BigDecimal.ZERO;
	private BigDecimal openTradeEquityBase = BigDecimal.ZERO;
	private BigDecimal priorOpenTradeEquityLocal = BigDecimal.ZERO;
	private BigDecimal priorOpenTradeEquityBase = BigDecimal.ZERO;

	// gain/loss/commission that was realized/paid on "positionDate"
	private BigDecimal todayCommissionLocal = BigDecimal.ZERO; // includes all commissions and fees: accountingAccount.commission == true
	private BigDecimal todayCommissionBase = BigDecimal.ZERO;
	private BigDecimal todayRealizedGainLossLocal = BigDecimal.ZERO;
	private BigDecimal todayRealizedGainLossBase = BigDecimal.ZERO;

	// includes full Gain/Loss for the day: realized, unrealized, accrual
	private BigDecimal dailyGainLossLocal = BigDecimal.ZERO;
	private BigDecimal dailyGainLossBase = BigDecimal.ZERO;

	////////////////////////////////////////////////////////////////////////////
	/////////             CALCULATED DATABASE FIELDS                 ///////////

	private BigDecimal accrualLocal; // AccrualLocal = (RemainingCostBasisLocal + OpenTradeEquityLocal - NotionalValueLocal)
	private BigDecimal accrualBase; // AccrualBase = (RemainingCostBasisLocal + OpenTradeEquityLocal - NotionalValueLocal) * MarketFxRate

	////////////////////////////////////////////////////////////////////////////
	/////////       THE FOLLOWING FIELDS ARE NOT IN THE DATABASE     ///////////

	// accrued interest only applies to securities that support it: can have up to 2 legs (IRS)
	// accrual fields are not not persisted in the database and are populated during "live" retrieval only
	@NonPersistentField
	private BigDecimal accrual1Local;
	@NonPersistentField
	private BigDecimal accrual2Local;

	// accrualPayment only applies to securities that support it: can have up to 2 legs (IRS)
	// payment fields are not not persisted in the database and are populated during "live" retrieval only
	@NonPersistentField
	private BigDecimal accrual1PaymentLocal;
	@NonPersistentField
	private BigDecimal accrual2PaymentLocal;

	// equityLegPayment only applies to securities that support it
	// not not persisted in the database and are populated during "live" retrieval only
	@NonPersistentField
	private BigDecimal equityLegPaymentLocal;

	// Price Alignment Interest expense charged by CCP for Cleared Swaps
	@NonPersistentField
	private BigDecimal paiAmountLocal;

	// used by ICE for Cleared CDS to shift mark for coupon/credit event payment one day earlier
	@NonPersistentField
	private BigDecimal realizedGainLossAdjustmentLocal = BigDecimal.ZERO;
	@NonPersistentField
	private BigDecimal realizedGainLossAdjustmentBase = BigDecimal.ZERO;

	//transient field allowing computed getter to be overridden by external code.
	@NonPersistentField
	private BigDecimal requiredCollateralBase;
	//transient field allowing external code to group positions together for any given purpose (e.g. collateral calculations).
	@NonPersistentField
	private String positionGroupId;

	@NonPersistentField
	private BigDecimal underlyingPrice;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingPositionDaily ofAccountingTransaction(AccountingTransactionInfo tran) {
		AccountingPositionDaily result = new AccountingPositionDaily();
		result.setAccountingTransaction(tran);
		result.setClientInvestmentAccount(tran.getClientInvestmentAccount());
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Long getAccountingTransactionId() {
		return getAccountingTransaction() != null ? getAccountingTransaction().getId() : null;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		if (this.accountingTransaction != null) {
			return this.accountingTransaction.getInvestmentSecurity();
		}
		return null;
	}


	/**
	 * Updates accrual and mark to market calculations based on other fields and returns "this" object.
	 * NOTE: all other fields that the calculations depend on, must be set prior to calling this method.
	 */
	public AccountingPositionDaily updateCalculations() {
		setAccrualLocal(getCalculatedAccrualLocal());
		setAccrualBase(getCalculatedAccrualBase());

		return this;
	}


	public BigDecimal getCalculatedAccrualLocal() {
		// OTE includes accrual and notional doesn't
		return MathUtils.subtract(MathUtils.add(this.remainingCostBasisLocal, this.openTradeEquityLocal), this.notionalValueLocal);
	}


	public BigDecimal getCalculatedAccrualBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getCalculatedAccrualLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.marketFxRate, result.scale());
			}
		}
		return result;
	}


	public BigDecimal getMarkAmountLocal() {
		InvestmentSecurity security = this.accountingTransaction.getInvestmentSecurity();
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentM2MCalculatorTypes calculator = instrument.getM2mCalculator();
		if (calculator == null) {
			calculator = hierarchy.getM2mCalculator();
		}

		BigDecimal result = BigDecimal.ZERO;
		if (calculator == null || calculator == InvestmentM2MCalculatorTypes.NONE) {
			// no mark
		}
		else if (calculator == InvestmentM2MCalculatorTypes.COST_AND_REALIZED) {
			// realized - commission + (remainingCostBasis - priorCostBasis)
			result = MathUtils.subtract(this.todayRealizedGainLossLocal, this.todayCommissionLocal);

			result = adjustForChangeInCostBasisLocal(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_OTE_CHANGE) {
			// OTE + Realized - Commission - Prior OTE
			result = MathUtils.add(this.openTradeEquityLocal, this.todayRealizedGainLossLocal);
			result = MathUtils.subtract(result, this.priorOpenTradeEquityLocal);
			result = MathUtils.subtract(result, this.todayCommissionLocal);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_NPV_CHANGE) {
			// NPV + Realized - Commission - Prior NPV
			result = MathUtils.add(getNpvLocal(), this.todayRealizedGainLossLocal);
			result = MathUtils.subtract(result, getPriorNpvLocal());
			result = MathUtils.subtract(result, this.todayCommissionLocal);

			result = adjustForChangeInCostBasisLocal(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_NPV_CHANGE_REALIZED_ON_TRANSACTION) {
			// NPV + Realized - Commission - Prior NPV + Realized Adjustment
			result = MathUtils.add(getNpvLocal(), this.todayRealizedGainLossLocal);
			result = MathUtils.subtract(result, getPriorNpvLocal());
			result = MathUtils.subtract(result, this.todayCommissionLocal);
			result = MathUtils.add(result, this.realizedGainLossAdjustmentLocal);

			result = adjustForChangeInCostBasisLocal(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_GAIN_LOSS) {
			return this.dailyGainLossLocal;
		}
		else {
			throw new IllegalStateException("Unsupported mark to market calculator: " + calculator);
		}
		return result;
	}


	private BigDecimal adjustForChangeInCostBasisLocal(BigDecimal result, InvestmentInstrumentHierarchy hierarchy, InvestmentSecurity security) {
		if (!hierarchy.isNoPaymentOnOpen()) {
			// need to subtract change in cost basis on open and add on close
			BigDecimal priorCostBasisLocal = this.priorNotionalValueLocal;
			if (!security.isCurrency()) {
				if (MathUtils.isEqual(this.priorQuantity, this.remainingQuantity)) {
					priorCostBasisLocal = this.remainingCostBasisLocal;
				}
				else {
					if (MathUtils.isNullOrZero(this.remainingQuantity)) {
						priorCostBasisLocal = MathUtils.multiply(this.accountingTransaction.getPositionCostBasis(), MathUtils.divide(this.priorQuantity, this.accountingTransaction.getQuantity()), 2);
					}
					else {
						priorCostBasisLocal = MathUtils.multiply(this.remainingCostBasisLocal, MathUtils.divide(this.priorQuantity, this.remainingQuantity), 2);
					}
				}
			}
			BigDecimal changeInCostBasisLocal = MathUtils.subtract(this.remainingCostBasisLocal, priorCostBasisLocal);
			result = MathUtils.subtract(result, changeInCostBasisLocal);
		}
		return result;
	}


	private BigDecimal adjustForChangeInCostBasisBase(BigDecimal result, InvestmentInstrumentHierarchy hierarchy, InvestmentSecurity security) {
		if (!hierarchy.isNoPaymentOnOpen()) {
			// need to subtract change in cost basis on open and add on close
			BigDecimal priorCostBasisBase = this.priorNotionalValueBase;
			if (!security.isCurrency()) {
				if (MathUtils.isEqual(this.priorQuantity, this.remainingQuantity)) {
					priorCostBasisBase = this.remainingCostBasisBase;
				}
				else {
					if (MathUtils.isNullOrZero(this.remainingQuantity)) {
						priorCostBasisBase = MathUtils.multiply(MathUtils.multiply(this.accountingTransaction.getPositionCostBasis(), this.marketFxRate),
								MathUtils.divide(this.priorQuantity, this.accountingTransaction.getQuantity()), 2);
					}
					else {
						priorCostBasisBase = MathUtils.multiply(this.remainingCostBasisBase, MathUtils.divide(this.priorQuantity, this.remainingQuantity), 2);
					}
				}
			}
			BigDecimal changeInCostBasisBase = MathUtils.subtract(this.remainingCostBasisBase, priorCostBasisBase);
			result = MathUtils.subtract(result, changeInCostBasisBase);
		}

		return result;
	}


	public BigDecimal getMarkAmountBase() {
		InvestmentSecurity security = this.accountingTransaction.getInvestmentSecurity();
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentM2MCalculatorTypes calculator = instrument.getM2mCalculator();
		if (calculator == null) {
			calculator = hierarchy.getM2mCalculator();
		}

		BigDecimal result = BigDecimal.ZERO;
		if (calculator == null || calculator == InvestmentM2MCalculatorTypes.NONE) {
			// no mark
		}
		else if (calculator == InvestmentM2MCalculatorTypes.COST_AND_REALIZED) {
			// realized - commission + (remainingCostBasis - priorCostBasis)
			result = MathUtils.subtract(this.todayRealizedGainLossBase, this.todayCommissionBase);

			result = adjustForChangeInCostBasisBase(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_OTE_CHANGE) {
			// OTE + Realized - Commission - Previous OTE
			result = MathUtils.add(this.openTradeEquityBase, this.todayRealizedGainLossBase);
			result = MathUtils.subtract(result, this.priorOpenTradeEquityBase);
			result = MathUtils.subtract(result, this.todayCommissionBase);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_NPV_CHANGE) {
			// NPV + Realized - Commission - Previous NPV
			result = MathUtils.add(getNpvBase(), this.todayRealizedGainLossBase);
			result = MathUtils.subtract(result, getPriorNpvBase());
			result = MathUtils.subtract(result, this.todayCommissionBase);

			result = adjustForChangeInCostBasisBase(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_NPV_CHANGE_REALIZED_ON_TRANSACTION) {
			// NPV + Realized - Commission - Previous NPV + Realized Adjustment
			result = MathUtils.add(getNpvBase(), this.todayRealizedGainLossBase);
			result = MathUtils.subtract(result, getPriorNpvBase());
			result = MathUtils.subtract(result, this.todayCommissionBase);
			result = MathUtils.add(result, this.realizedGainLossAdjustmentBase);

			result = adjustForChangeInCostBasisBase(result, hierarchy, security);
		}
		else if (calculator == InvestmentM2MCalculatorTypes.DAILY_GAIN_LOSS) {
			return this.dailyGainLossBase;
		}
		else {
			throw new IllegalStateException("Unsupported mark to market calculator: " + calculator);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For securities with "No Payment On Open": Market Value = Open Trade Equity
	 * For security with Payment on Open (Cost Basis =  Cost): Market Value = Open Trade Equity + Remaining Cost Basis
	 */
	public BigDecimal getMarketValueLocal() {
		BigDecimal result = this.openTradeEquityLocal;
		if (!this.accountingTransaction.getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			result = MathUtils.add(result, this.remainingCostBasisLocal);
		}
		return result;
	}


	/**
	 * For securities with "No Payment On Open": Market Value = Open Trade Equity
	 * For security with Payment on Open (Cost Basis =  Cost): Market Value = Open Trade Equity + Remaining Cost Basis
	 */
	public BigDecimal getMarketValueBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getMarketValueLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.marketFxRate, result.scale());
			}
		}
		return result;
	}


	public BigDecimal getPriorMarketValueLocal() {
		BigDecimal result = this.priorOpenTradeEquityLocal;
		if (!this.accountingTransaction.getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			result = MathUtils.add(result, getPriorRemainingCostBasisLocal());
		}
		return result;
	}


	public BigDecimal getPriorMarketValueBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getPriorMarketValueLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.priorFxRate, result.scale());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////


	/**
	 * NPV = Market Value = Open Trade Equity + Remaining Cost Basis (for securities with Payment on Open: Cost Basis = Cost).
	 * Otherwise: NPV = Market Value = Open Trade Equity
	 */
	public BigDecimal getNpvLocal() {
		return getMarketValueLocal();
	}


	/**
	 * NPV = Market Value = Open Trade Equity + Remaining Cost Basis (for securities with Payment on Open: Cost Basis = Cost).
	 * Otherwise: NPV = Market Value = Open Trade Equity
	 */
	public BigDecimal getNpvBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getNpvLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.marketFxRate, result.scale());
			}
		}
		return result;
	}


	public BigDecimal getPriorNpvLocal() {
		return getPriorMarketValueLocal();
	}


	public BigDecimal getPriorNpvBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getPriorNpvLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.priorFxRate, result.scale());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPriorAccrualLocal() {
		// OTE includes accrual and notional doesn't
		return MathUtils.subtract(MathUtils.add(getPriorRemainingCostBasisLocal(), this.priorOpenTradeEquityLocal), this.priorNotionalValueLocal);
	}


	public BigDecimal getPriorAccrualBase() {
		// cannot use "local" formula for foreign positions because it uses opening FX for cost basis
		BigDecimal result = getPriorAccrualLocal();
		if (result != null) {
			double local = result.doubleValue();
			if (!MathUtils.isZeroOrOne(local)) {
				result = MathUtils.multiply(result, this.priorFxRate, result.scale());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPriorRemainingCostBasisLocal() {
		BigDecimal result = this.remainingCostBasisLocal;
		BigDecimal priorQty = Optional.ofNullable(getPriorQuantityNormalized()).orElse(BigDecimal.ZERO);
		BigDecimal remainingQty = Optional.ofNullable(getRemainingQuantityNormalized()).orElse(BigDecimal.ZERO);
		if (!priorQty.equals(remainingQty)) {
			if (MathUtils.isEqual(priorQty, 0)) {
				result = BigDecimal.ZERO;
			}
			else if (MathUtils.isEqual(remainingQty, 0)) {
				// position was fully closed today
				AccountingTransactionInfo t = getAccountingTransaction();
				result = MathUtils.divide(MathUtils.multiply(t.getPositionCostBasis(), priorQty), t.getQuantityNormalized(), this.remainingCostBasisLocal.scale());
			}
			else {
				result = MathUtils.divide(MathUtils.multiply(result, priorQty), remainingQty, this.remainingCostBasisLocal.scale());
			}
		}
		return result;
	}


	public BigDecimal getPriorRemainingCostBasisBase() {
		BigDecimal result = this.remainingCostBasisBase;
		BigDecimal priorQty = Optional.ofNullable(getPriorQuantityNormalized()).orElse(BigDecimal.ZERO);
		BigDecimal remainingQty = Optional.ofNullable(getRemainingQuantityNormalized()).orElse(BigDecimal.ZERO);
		if (!priorQty.equals(remainingQty)) {
			if (MathUtils.isEqual(priorQty, 0)) {
				result = BigDecimal.ZERO;
			}
			else if (MathUtils.isEqual(remainingQty, 0)) {
				// position was fully closed today
				AccountingTransactionInfo t = getAccountingTransaction();
				result = MathUtils.divide(MathUtils.multiply(t.getPositionCostBasis(), priorQty), t.getQuantityNormalized(), this.remainingCostBasisLocal.scale());
				double local = result.doubleValue();
				if (!MathUtils.isZeroOrOne(local)) {
					result = MathUtils.multiply(result, this.priorFxRate, result.scale());
				}
			}
			else {
				result = MathUtils.divide(MathUtils.multiply(result, priorQty), remainingQty, this.remainingCostBasisBase.scale());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getRequiredCollateralBase() {
		if (this.requiredCollateralBase == null) {
			InvestmentSecurity sec = this.accountingTransaction.getInvestmentSecurity();
			// Don't throw an exception for missing margin because this is just a getter - will be thrown when collateral margin is calculated for the account in total for Collateral Balances
			return InvestmentCalculatorUtils.calculateRequiredCollateral(sec, this.accountingTransaction.getHoldingInvestmentAccount(), getRemainingQuantity(), getMarketFxRate(), false);
		}
		return this.requiredCollateralBase;
	}


	public void setRequiredCollateralBase(BigDecimal requiredCollateral) {
		this.requiredCollateralBase = requiredCollateral;
	}


	public BigDecimal getAbsNotionalValueBase() {
		return MathUtils.abs(getNotionalValueBase());
	}


	/**
	 * Returns prior day quantity for non-currency position and local notional for currencies.
	 */
	public BigDecimal getPriorQuantityNormalized() {
		if (this.accountingTransaction.getInvestmentSecurity().isCurrency()) {
			return this.priorNotionalValueLocal;
		}
		return this.priorQuantity;
	}


	/**
	 * Returns remaining quantity for non-currency position and local notional for currencies.
	 */
	public BigDecimal getRemainingQuantityNormalized() {
		if (this.accountingTransaction.getInvestmentSecurity().isCurrency()) {
			return this.notionalValueLocal;
		}
		return this.remainingQuantity;
	}


	/**
	 * Returns today closed quantity for non-currency position and local notional closed for currencies.
	 */
	public BigDecimal getTodayClosedQuantityNormalized() {
		if (this.accountingTransaction.getInvestmentSecurity().isCurrency()) {
			return MathUtils.subtract(this.priorNotionalValueLocal, this.notionalValueLocal);
		}
		return this.todayClosedQuantity;
	}


	/**
	 * Returns "Remaining Quantity" for all securities that do not have Factor Change Event.
	 * For securities with Factor Change Event (CDS and ABS), returns opening quantity (Original Notional or Face)
	 */
	public BigDecimal getUnadjustedQuantity() {
		BigDecimal result = getRemainingQuantity();
		if (this.accountingTransaction.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
			if (!MathUtils.isNullOrZero(result)) {
				// fully closed positions maintain a record for one more day
				result = this.accountingTransaction.getQuantity();
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionInfo getAccountingTransaction() {
		return this.accountingTransaction;
	}


	public void setAccountingTransaction(AccountingTransactionInfo accountingTransaction) {
		this.accountingTransaction = accountingTransaction;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getPriorQuantity() {
		return this.priorQuantity;
	}


	public void setPriorQuantity(BigDecimal priorQuantity) {
		this.priorQuantity = priorQuantity;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getPriorPrice() {
		return this.priorPrice;
	}


	public void setPriorPrice(BigDecimal priorPrice) {
		this.priorPrice = priorPrice;
	}


	public BigDecimal getMarketFxRate() {
		return this.marketFxRate;
	}


	public void setMarketFxRate(BigDecimal marketFxRate) {
		this.marketFxRate = marketFxRate;
	}


	public BigDecimal getPriorFxRate() {
		return this.priorFxRate;
	}


	public void setPriorFxRate(BigDecimal priorFxRate) {
		this.priorFxRate = priorFxRate;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getRemainingCostBasisLocal() {
		return this.remainingCostBasisLocal;
	}


	public void setRemainingCostBasisLocal(BigDecimal remainingCostBasisLocal) {
		this.remainingCostBasisLocal = remainingCostBasisLocal;
	}


	public BigDecimal getRemainingCostBasisBase() {
		return this.remainingCostBasisBase;
	}


	public void setRemainingCostBasisBase(BigDecimal remainingCostBasisBase) {
		this.remainingCostBasisBase = remainingCostBasisBase;
	}


	public BigDecimal getTodayClosedQuantity() {
		return this.todayClosedQuantity;
	}


	public void setTodayClosedQuantity(BigDecimal todayClosedQuantity) {
		this.todayClosedQuantity = todayClosedQuantity;
	}


	public BigDecimal getTodayCommissionLocal() {
		return this.todayCommissionLocal;
	}


	public void setTodayCommissionLocal(BigDecimal todayCommissionLocal) {
		this.todayCommissionLocal = todayCommissionLocal;
	}


	public BigDecimal getTodayCommissionBase() {
		return this.todayCommissionBase;
	}


	public void setTodayCommissionBase(BigDecimal todayCommissionBase) {
		this.todayCommissionBase = todayCommissionBase;
	}


	public BigDecimal getTodayRealizedGainLossLocal() {
		return this.todayRealizedGainLossLocal;
	}


	public void setTodayRealizedGainLossLocal(BigDecimal todayRealizedGainLossLocal) {
		this.todayRealizedGainLossLocal = todayRealizedGainLossLocal;
	}


	public BigDecimal getTodayRealizedGainLossBase() {
		return this.todayRealizedGainLossBase;
	}


	public void setTodayRealizedGainLossBase(BigDecimal todayRealizedGainLossBase) {
		this.todayRealizedGainLossBase = todayRealizedGainLossBase;
	}


	public BigDecimal getNotionalValueLocal() {
		return this.notionalValueLocal;
	}


	public void setNotionalValueLocal(BigDecimal notionalValueLocal) {
		this.notionalValueLocal = notionalValueLocal;
	}


	public BigDecimal getNotionalValueBase() {
		return this.notionalValueBase;
	}


	public void setNotionalValueBase(BigDecimal notionalValueBase) {
		this.notionalValueBase = notionalValueBase;
	}


	public BigDecimal getPriorNotionalValueLocal() {
		return this.priorNotionalValueLocal;
	}


	public void setPriorNotionalValueLocal(BigDecimal priorNotionalValueLocal) {
		this.priorNotionalValueLocal = priorNotionalValueLocal;
	}


	public BigDecimal getPriorNotionalValueBase() {
		return this.priorNotionalValueBase;
	}


	public void setPriorNotionalValueBase(BigDecimal priorNotionalValueBase) {
		this.priorNotionalValueBase = priorNotionalValueBase;
	}


	public BigDecimal getDailyGainLossLocal() {
		return this.dailyGainLossLocal;
	}


	public void setDailyGainLossLocal(BigDecimal dailyGainLossLocal) {
		this.dailyGainLossLocal = dailyGainLossLocal;
	}


	public BigDecimal getDailyGainLossBase() {
		return this.dailyGainLossBase;
	}


	public void setDailyGainLossBase(BigDecimal dailyGainLossBase) {
		this.dailyGainLossBase = dailyGainLossBase;
	}


	public BigDecimal getAccrualLocal() {
		return this.accrualLocal;
	}


	public void setAccrualLocal(BigDecimal accrualLocal) {
		this.accrualLocal = accrualLocal;
	}


	public BigDecimal getAccrualBase() {
		return this.accrualBase;
	}


	public void setAccrualBase(BigDecimal accrualBase) {
		this.accrualBase = accrualBase;
	}


	public BigDecimal getAccrual1Local() {
		return this.accrual1Local;
	}


	public void setAccrual1Local(BigDecimal accrual1Local) {
		this.accrual1Local = accrual1Local;
	}


	public BigDecimal getAccrual2Local() {
		return this.accrual2Local;
	}


	public void setAccrual2Local(BigDecimal accrual2Local) {
		this.accrual2Local = accrual2Local;
	}


	public BigDecimal getAccrual1Base() {
		BigDecimal result = getAccrual1Local();
		if (result != null) {
			result = MathUtils.multiply(result, getMarketFxRate(), result.scale());
		}
		return result;
	}


	public BigDecimal getAccrual2Base() {
		BigDecimal result = getAccrual2Local();
		if (result != null) {
			result = MathUtils.multiply(result, getMarketFxRate(), result.scale());
		}
		return result;
	}


	public BigDecimal getAccrual1PaymentLocal() {
		return this.accrual1PaymentLocal;
	}


	public void setAccrual1PaymentLocal(BigDecimal accrual1PaymentLocal) {
		this.accrual1PaymentLocal = accrual1PaymentLocal;
	}


	public BigDecimal getAccrual2PaymentLocal() {
		return this.accrual2PaymentLocal;
	}


	public void setAccrual2PaymentLocal(BigDecimal accrual2PaymentLocal) {
		this.accrual2PaymentLocal = accrual2PaymentLocal;
	}


	public BigDecimal getEquityLegPaymentLocal() {
		return this.equityLegPaymentLocal;
	}


	public void setEquityLegPaymentLocal(BigDecimal equityLegPaymentLocal) {
		this.equityLegPaymentLocal = equityLegPaymentLocal;
	}


	public BigDecimal getAccrualPayment1Base() {
		BigDecimal result = getAccrual1PaymentLocal();
		if (result != null) {
			result = MathUtils.multiply(result, getMarketFxRate(), result.scale());
		}
		return result;
	}


	public BigDecimal getAccrualPayment2Base() {
		BigDecimal result = getAccrual2PaymentLocal();
		if (result != null) {
			result = MathUtils.multiply(result, getMarketFxRate(), result.scale());
		}
		return result;
	}


	public BigDecimal getEquityLegPaymentBase() {
		BigDecimal result = getEquityLegPaymentLocal();
		if (result != null) {
			result = MathUtils.multiply(result, getMarketFxRate(), result.scale());
		}
		return result;
	}


	public BigDecimal getPaiAmountLocal() {
		return this.paiAmountLocal;
	}


	public void setPaiAmountLocal(BigDecimal paiAmountLocal) {
		this.paiAmountLocal = paiAmountLocal;
	}


	public BigDecimal getRealizedGainLossAdjustmentLocal() {
		return this.realizedGainLossAdjustmentLocal;
	}


	public void setRealizedGainLossAdjustmentLocal(BigDecimal realizedGainLossAdjustmentLocal) {
		this.realizedGainLossAdjustmentLocal = realizedGainLossAdjustmentLocal;
	}


	public BigDecimal getRealizedGainLossAdjustmentBase() {
		return this.realizedGainLossAdjustmentBase;
	}


	public void setRealizedGainLossAdjustmentBase(BigDecimal realizedGainLossAdjustmentBase) {
		this.realizedGainLossAdjustmentBase = realizedGainLossAdjustmentBase;
	}


	@Override
	public String getPositionGroupId() {
		return this.positionGroupId;
	}


	@Override
	public void setPositionGroupId(String positionGroupId) {
		this.positionGroupId = positionGroupId;
	}


	public BigDecimal getUnderlyingPrice() {
		return this.underlyingPrice;
	}


	public void setUnderlyingPrice(BigDecimal underlyingPrice) {
		this.underlyingPrice = underlyingPrice;
	}
}
