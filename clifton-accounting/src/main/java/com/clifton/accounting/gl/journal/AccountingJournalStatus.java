package com.clifton.accounting.gl.journal;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The AccountingJournalStatus class defines available statuses for accounting journals:
 * <ul>
 * <li>Booked – when a journal is Booked to journal tables but not yet posted to the General Ledger</li>
 * <li>Original – when a journal is Posted for the first time for an entity (99%+ of all journals)</li>
 * <li>Deleted – updates current status to Deleted when the journal is Unposted from the General Ledger</li>
 * <li>Unposted – creates a copy of "Deleted" journal during Unposting</li>
 * <li>Unchanged – when the journal is Re-Posted, compares new journal to previous and sets Unchanged status if all transactions are the same (except for id’s)</li>
 * <li>Modified – when the journal is Re-Posted, compares new journal to previous and set Modified status if at least one transaction is different (except for id’?)</li>
 * </ul>
 *
 * @author vgomelsky
 */
@CacheByName
public class AccountingJournalStatus extends NamedEntity<Short> {

	public static final String STATUS_BOOKED = "Booked";
	public static final String STATUS_ORIGINAL = "Original";
	public static final String STATUS_DELETED = "Deleted";
	public static final String STATUS_UNPOSTED = "Unposted";
	public static final String STATUS_UNCHANGED = "Unchanged";
	public static final String STATUS_MODIFIED = "Modified";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Specifies whether a journal is this status is posted to the GL: Original, Unchanged, Modified.
	 * Booked, Deleted and Unposted journals are not valid entries in the general ledger.
	 */
	private boolean posted;

	/**
	 * Only Booked and Unposted journals are allowed to be posted.  These are the only states where journal's PostingDate is not set.
	 */
	private boolean postingAllowed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPosted() {
		return this.posted;
	}


	public void setPosted(boolean posted) {
		this.posted = posted;
	}


	public boolean isPostingAllowed() {
		return this.postingAllowed;
	}


	public void setPostingAllowed(boolean postingAllowed) {
		this.postingAllowed = postingAllowed;
	}
}
