package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.core.util.AssertUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>AccountingBookingProcessorLocatorInSpringContext</code> class locates AccountingBookingProcessor implementations for corresponding journal types.
 * It auto-discovers all application context beans that implement AccountingBookingProcessor interface and registers them so that they can be located.
 *
 * @author vgomelsky
 */
@Component
public class AccountingBookingProcessorLocatorInSpringContext<T extends BookableEntity> implements AccountingBookingProcessorLocator, InitializingBean, ApplicationContextAware {

	private final Map<String, AccountingBookingProcessor<T>> processorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void afterPropertiesSet() {
		Map<String, AccountingBookingProcessor> beanMap = getApplicationContext().getBeansOfType(AccountingBookingProcessor.class);

		// need a map with journal type names as keys instead of bean names
		for (Map.Entry<String, AccountingBookingProcessor> beanEntry : beanMap.entrySet()) {
			String beanName = beanEntry.getKey();
			AccountingBookingProcessor processor = beanEntry.getValue();
			if (getProcessorMap().containsKey(processor.getJournalTypeName())) {
				throw new RuntimeException("Cannot register '" + beanName + "' as a processor for journal of type '" + processor.getJournalTypeName()
						+ "' because this journal type already has a registered processor.");
			}
			getProcessorMap().put(processor.getJournalTypeName(), processor);
		}
	}


	@Override
	public AccountingBookingProcessor<T> getAccountingBookingProcessor(String journalTypeName) {
		AssertUtils.assertNotNull(journalTypeName, "Required journal type name cannot be null.");
		AccountingBookingProcessor<T> result = getProcessorMap().get(journalTypeName);
		AssertUtils.assertNotNull(result, "Cannot locate AccountingBookingProcessor for '%1s' journal type.", journalTypeName);

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, AccountingBookingProcessor<T>> getProcessorMap() {
		return this.processorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
