package com.clifton.accounting.gl.pending;

import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;

import java.util.List;


/**
 * Defines a service that returns balances for pending journals in the system.
 *
 * @author mwacker
 */
public interface AccountingPendingActivityHandler {

	public List<AccountingJournalDetailDefinition> getAccountingPendingTransactionList(final GeneralLedgerSearchForm searchForm);
}
