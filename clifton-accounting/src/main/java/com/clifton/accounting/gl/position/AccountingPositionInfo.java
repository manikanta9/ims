package com.clifton.accounting.gl.position;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;


/**
 * The <code>AccountingPositionInfo</code> interface acts as a 'bridge' between AccountingPosition and AccountingPositionDaily
 * and forces them both to implement necessary getters and setters that are needed when processing collateral rebuild functionality
 * e.g. AccountingPositionDaily is utilized from the UI, and AccountingPosition from actual rebuild logic so both need to be supported
 * by a common interface.
 *
 * @author StevenF
 */
public interface AccountingPositionInfo {

	public Long getAccountingTransactionId();


	public String getPositionGroupId();


	public void setPositionGroupId(String positionGroupId);


	public InvestmentSecurity getInvestmentSecurity();


	public BigDecimal getRemainingQuantity();
}
