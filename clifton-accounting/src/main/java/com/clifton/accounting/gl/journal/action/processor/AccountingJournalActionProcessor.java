package com.clifton.accounting.gl.journal.action.processor;

import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;


/**
 * The AccountingJournalActionProcessor interface should be implemented by specific actions to be performed immediately after
 * {@link AccountingJournal} is posted to the General Ledger or rolled back immediately after it was unposted from the GL.
 *
 * @author vgomelsky
 */
public interface AccountingJournalActionProcessor<T extends BookableEntity> {


	/**
	 * Custom action to be executed immediately after the specified journal is posted to the General Ledger.
	 * Note, {@link BookableEntity} will be null for journal types without corresponding sources (general journals, adjusting journals).
	 */
	public void processAction(T bookableEntity, AccountingJournal journal);


	/**
	 * Custom action to be executed immediately after the specified journal is unposted from the General Ledger.
	 * This method usually reverses what was done by the {@link #processAction(BookableEntity, AccountingJournal)} method.
	 * Note, {@link BookableEntity} will be null for journal types without corresponding sources (general journals, adjusting journals).
	 */
	public void rollbackAction(T bookableEntity, AccountingJournal journal);
}
