package com.clifton.accounting.gl.booking;


import com.clifton.core.security.authorization.SecureMethodTableResolver;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>AccountingBookingSecureMethodTableResolver</code> class configures custom security:
 * when posting require permissions to AccountingTransaction and when booking only to AccountingJournal.
 *
 * @author vgomelsky
 */
@Component
public class AccountingBookingSecureMethodTableResolver implements SecureMethodTableResolver {

	@Override
	public String getTableName(@SuppressWarnings("unused") Method method, Map<String, ?> config) {
		Object postJournal = config.get("postJournal");
		if (postJournal != null && "TRUE".equalsIgnoreCase(postJournal.toString())) {
			return "AccountingTransaction";
		}
		return "AccountingJournal";
	}
}
