package com.clifton.accounting.gl.balance.removal;

import java.util.Date;


/**
 * @author manderson
 */
public class AccountingBalanceRemovalCommand {

	/**
	 * The client account to remove positions and/or balances from
	 */
	private Integer clientInvestmentAccountId;

	/**
	 * The holding account to remove positions and/or balances from
	 */
	private Integer holdingInvestmentAccountId;

	/**
	 * Date to remove positions and/or balances on
	 */
	private Date removalDate;

	/**
	 * Option - currently only used on account termination to remove positions using latest available price
	 * In some cases (retail accounts) we terminate today, but we don't have today's prices in the system yet
	 */
	private boolean useFlexiblePriceLookup;

	private String removalNote;

	private boolean distributePositions;

	private boolean distributeNonPositionAssetsAndLiabilities;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Date getRemovalDate() {
		return this.removalDate;
	}


	public void setRemovalDate(Date removalDate) {
		this.removalDate = removalDate;
	}


	public boolean isDistributePositions() {
		return this.distributePositions;
	}


	public void setDistributePositions(boolean distributePositions) {
		this.distributePositions = distributePositions;
	}


	public boolean isDistributeNonPositionAssetsAndLiabilities() {
		return this.distributeNonPositionAssetsAndLiabilities;
	}


	public void setDistributeNonPositionAssetsAndLiabilities(boolean distributeNonPositionAssetsAndLiabilities) {
		this.distributeNonPositionAssetsAndLiabilities = distributeNonPositionAssetsAndLiabilities;
	}


	public String getRemovalNote() {
		return this.removalNote;
	}


	public void setRemovalNote(String removalNote) {
		this.removalNote = removalNote;
	}


	public boolean isUseFlexiblePriceLookup() {
		return this.useFlexiblePriceLookup;
	}


	public void setUseFlexiblePriceLookup(boolean useFlexiblePriceLookup) {
		this.useFlexiblePriceLookup = useFlexiblePriceLookup;
	}
}
