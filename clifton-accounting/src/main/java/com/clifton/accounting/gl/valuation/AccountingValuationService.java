package com.clifton.accounting.gl.valuation;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * The <code>AccountingValuationService</code> interface defines methods for retrieving fully populated
 * {@link AccountingPositionValue} and {@link AccountingBalanceValue} objects.
 *
 * @author vgomelsky
 */
public interface AccountingValuationService {

	/**
	 * Returns the General Ledger position balances for the specified parameters.  Adds live valuation information using
	 * the latest prices, exchange rates, index ratios and accrual calculators in the system.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPositionValue> getAccountingPositionValueList(AccountingPositionCommand command);


	/**
	 * Returns the General Ledger balances for the specified parameters.  Adds live valuation information using
	 * the latest prices, exchange rates, index ratios and accrual calculators in the system.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingBalanceValue> getAccountingBalanceValueList(AccountingBalanceSearchForm searchForm);
}
