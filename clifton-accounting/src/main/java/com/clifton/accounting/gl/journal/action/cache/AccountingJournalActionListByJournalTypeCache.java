package com.clifton.accounting.gl.journal.action.cache;

import com.clifton.accounting.gl.journal.action.AccountingJournalAction;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class AccountingJournalActionListByJournalTypeCache extends SelfRegisteringSingleKeyDaoListCache<AccountingJournalAction, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "journalType.id";
	}


	@Override
	protected Short getBeanKeyValue(AccountingJournalAction bean) {
		if (bean.getJournalType() != null) {
			return bean.getJournalType().getId();
		}
		return null;
	}
}
