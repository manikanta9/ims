package com.clifton.accounting.gl.valuation;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The ValuationData class represents calculations for a particular accounting position or balance that
 * can be combined along with GL data in order to create fully populated valuation object.
 *
 * @author vgomelsky
 */
public class ValuationData {

	private BigDecimal quantity = null;
	private BigDecimal price = null;
	private BigDecimal underlyingPrice = null;
	private BigDecimal exchangeRateToBase;

	private BigDecimal localNotional;

	private BigDecimal localAccrual1 = BigDecimal.ZERO;
	private BigDecimal localAccrual2 = BigDecimal.ZERO;

	private BigDecimal localEquityAccrual = BigDecimal.ZERO;

	private BigDecimal localReceivableAccrual1 = BigDecimal.ZERO;
	private BigDecimal localReceivableAccrual2 = BigDecimal.ZERO;

	private BigDecimal localEquityReceivableAccrual = BigDecimal.ZERO;

	private BigDecimal localCostBasis;
	private BigDecimal baseCostBasis;

	private BigDecimal baseCost;

	private Date valuationDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ValuationData onValuationDate(Date valuationDate) {
		ValuationData data = new ValuationData();
		data.valuationDate = valuationDate;
		return data;
	}


	public ValuationData withQuantity(BigDecimal quantity) {
		this.quantity = quantity;
		return this;
	}


	public ValuationData withPrice(BigDecimal price) {
		this.price = price;
		return this;
	}


	public ValuationData withUnderlyingPrice(BigDecimal underlyingPrice) {
		this.underlyingPrice = underlyingPrice;
		return this;
	}


	public ValuationData withExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
		return this;
	}


	public ValuationData withLocalNotional(BigDecimal localNotional) {
		this.localNotional = localNotional;
		return this;
	}


	public ValuationData withLocalAccrual1(BigDecimal localAccrual1) {
		this.localAccrual1 = localAccrual1;
		return this;
	}


	public ValuationData withLocalAccrual2(BigDecimal localAccrual2) {
		this.localAccrual2 = localAccrual2;
		return this;
	}


	public ValuationData withLocalEquityAccrual(BigDecimal localEquityAccrual) {
		this.localEquityAccrual = localEquityAccrual;
		return this;
	}


	public ValuationData withLocalReceivableAccrual1(BigDecimal localReceivableAccrual1) {
		this.localReceivableAccrual1 = localReceivableAccrual1;
		return this;
	}


	public ValuationData withLocalReceivableAccrual2(BigDecimal localReceivableAccrual2) {
		this.localReceivableAccrual2 = localReceivableAccrual2;
		return this;
	}


	public ValuationData withLocalEquityReceivableAccrual(BigDecimal localEquityReceivableAccrual) {
		this.localEquityReceivableAccrual = localEquityReceivableAccrual;
		return this;
	}


	public ValuationData withLocalCostBasis(BigDecimal localCostBasis) {
		this.localCostBasis = localCostBasis;
		return this;
	}


	public ValuationData withBaseCostBasis(BigDecimal baseCostBasis) {
		this.baseCostBasis = baseCostBasis;
		return this;
	}


	public ValuationData withBaseCost(BigDecimal baseCost) {
		this.baseCost = baseCost;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public BigDecimal getUnderlyingPrice() {
		return this.underlyingPrice;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public BigDecimal getLocalNotional() {
		return this.localNotional;
	}


	public BigDecimal getLocalAccrual1() {
		return this.localAccrual1;
	}


	public BigDecimal getLocalAccrual2() {
		return this.localAccrual2;
	}


	public BigDecimal getLocalEquityAccrual() {
		return this.localEquityAccrual;
	}


	public BigDecimal getLocalReceivableAccrual1() {
		return this.localReceivableAccrual1;
	}


	public BigDecimal getLocalReceivableAccrual2() {
		return this.localReceivableAccrual2;
	}


	public BigDecimal getLocalEquityReceivableAccrual() {
		return this.localEquityReceivableAccrual;
	}


	public BigDecimal getLocalCostBasis() {
		return this.localCostBasis;
	}


	public BigDecimal getBaseCostBasis() {
		return this.baseCostBasis;
	}


	public BigDecimal getBaseCost() {
		return this.baseCost;
	}


	public Date getValuationDate() {
		return this.valuationDate;
	}
}
