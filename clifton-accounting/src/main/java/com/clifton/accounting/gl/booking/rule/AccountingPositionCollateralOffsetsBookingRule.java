package com.clifton.accounting.gl.booking.rule;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * @author terrys
 */
public class AccountingPositionCollateralOffsetsBookingRule<T extends BookableEntity> extends AccountingCommonBookingRule<T> implements AccountingBookingRule<T> {

	private AccountingPositionTransferService accountingPositionTransferService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		ValidationUtils.assertNotEmpty(journal.getJournalDetailList(), "At least one detail must be present for journal: " + journal.getLabel());
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>(journal.getJournalDetailList());

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			if (isCollateralAccountingPositionTransferPosition(detail)) {
				doApplyRule(journal, detail, bookingSession);
			}
		}
	}


	private boolean isCollateralPositionAccount(AccountingJournalDetailDefinition detail) {
		return detail.getAccountingAccount().isCollateral() && detail.getAccountingAccount().isPosition();
	}


	private boolean isCollateralAccountingPositionTransferPosition(AccountingJournalDetailDefinition detail) {
		return isCollateralPositionAccount(detail) && detail.getParentTransaction() != null && detail.getParentTransaction().getJournal() != null
				&& AccountingJournalType.TRANSFER_JOURNAL.equals(detail.getParentTransaction().getJournal().getJournalType().getName()) && detail.getParentTransaction().getFkFieldId() != null;
	}


	private void doApplyRule(AccountingJournal journal, AccountingJournalDetailDefinition detail, BookingSession<T> bookingSession) {
		Short[] offsetAccounts = getCollateralTransferOffsetAccountIds(detail);
		if (!ArrayUtils.isEmpty(offsetAccounts)) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setParentId(detail.getParentTransaction().getId());
			searchForm.setDeleted(false);
			searchForm.setOpening(true);
			searchForm.setAccountingAccountIds(offsetAccounts);
			List<AccountingTransaction> children = getAccountingTransactionService().getAccountingTransactionList(searchForm);
			for (AccountingTransaction child : CollectionUtils.getIterable(children)) {
				AccountingPosition position = getCollateralPosition(child);
				if (position != null) {
					bookingSession.addNewCurrentPosition(position);
				}
				else {
					bookingSession.addNewCurrentPosition(child);
				}
				AccountingJournalDetail result = new AccountingJournalDetail();
				BeanUtils.copyProperties(child, result);

				result.setId(null);
				result.setOpening(!child.isOpening());
				result.setDescription(String.format("[%s] offset for [%s]", child.getAccountingAccount().getName(), result.getInvestmentSecurity().getSymbol()));
				result.setJournal(journal);
				result.setSystemTable(detail.getSystemTable());
				result.setFkFieldId(detail.getFkFieldId());
				result.setParentDefinition(null);
				result.setParentTransaction(child);
				result.setSettlementDate(detail.getSettlementDate());
				result.setTransactionDate(detail.getTransactionDate());
				result.setExecutingCompany(detail.getExecutingCompany());

				result.setQuantity(detail.getQuantity().abs().multiply(new BigDecimal(child.getQuantity().signum())).negate());
				BigDecimal proportionality = MathUtils.divide(detail.getQuantity(), child.getQuantity()).abs();

				InvestmentNotionalCalculatorTypes baseNotionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(result.getClientInvestmentAccount().getBaseCurrency());
				result.setBaseDebitCredit(baseNotionalCalculator.round(MathUtils.multiply(proportionality, result.getBaseDebitCredit().negate())));

				BigDecimal difference = position != null ? MathUtils.absoluteDiff(result.getBaseDebitCredit().abs(), position.getRemainingBaseDebitCredit().abs()) : BigDecimal.ZERO;
				if (position != null && MathUtils.isNotEqual(difference, BigDecimal.ZERO) && MathUtils.isLessThanOrEqual(difference, BigDecimal.valueOf(0.02))) {
					BigDecimal sign = new BigDecimal(result.getQuantity().signum());
					result.setBaseDebitCredit(sign.multiply(position.getRemainingBaseDebitCredit().abs()));
					result.setLocalDebitCredit(InvestmentCalculatorUtils.calculateLocalAmount(result.getBaseDebitCredit(), position.getExchangeRateToBase(), position.getInvestmentSecurity()));
					result.setPositionCostBasis(sign.multiply(position.getRemainingCostBasis().abs()));
				}
				else {

					InvestmentNotionalCalculatorTypes localNotionalCalculator = InvestmentCalculatorUtils.getNotionalCalculator(result.getInvestmentSecurity().getInstrument().getTradingCurrency());
					result.setLocalDebitCredit(localNotionalCalculator.round(MathUtils.multiply(proportionality, result.getLocalDebitCredit().negate())));
					result.setPositionCostBasis(localNotionalCalculator.round(MathUtils.multiply(proportionality, result.getPositionCostBasis().negate())));
				}
				journal.addJournalDetail(result);
			}
		}
	}


	private AccountingPosition getCollateralPosition(AccountingTransaction child) {
		AccountingPositionCommand accountingPositionCommand = new AccountingPositionCommand();
		accountingPositionCommand = accountingPositionCommand.forExistingPosition(child.getId());
		return CollectionUtils.getOnlyElement(getAccountingPositionService().getAccountingPositionListUsingCommand(accountingPositionCommand));
	}


	private Short[] getCollateralTransferOffsetAccountIds(AccountingJournalDetailDefinition detail) {
		if (AccountingJournalType.TRANSFER_JOURNAL.equals(detail.getParentTransaction().getJournal().getJournalType().getName()) && detail.getParentTransaction().getFkFieldId() != null) {
			AccountingPositionTransferDetail accountingPositionTransferDetail = getAccountingPositionTransferService().getAccountingPositionTransferDetail(detail.getParentTransaction().getFkFieldId());
			AccountingPositionTransferType transferType = Optional.ofNullable(accountingPositionTransferDetail)
					.map(AccountingPositionTransferDetail::getPositionTransfer)
					.map(AccountingPositionTransfer::getType)
					.orElse(null);
			if (transferType != null && transferType.getToOffsettingAccountingAccount() != null && transferType.getFromOffsettingAccountingAccount() != null) {
				return new Short[]{transferType.getToOffsettingAccountingAccount().getId(), transferType.getFromOffsettingAccountingAccount().getId()};
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}
}
