package com.clifton.accounting.gl.booking.rule;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AccountingRemoveZeroValueRecordBookingRule</code> removes any cash records with zero amount.
 * This could happen if we open a Future position, then we would generate a cash record, but not pay anything, so this zero record needs to be removed
 * before booking.  Note that position records could have zero debit/credit.
 */
public class AccountingRemoveZeroValueRecordBookingRule<T extends BookableEntity> implements AccountingBookingRule<T> {

	@Override
	public void applyRule(BookingSession<T> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		if (!CollectionUtils.isEmpty(journal.getJournalDetailList())) {
			List<AccountingJournalDetailDefinition> detailRecords = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

			for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailRecords)) {
				if (detail.getAccountingAccount().isPosition() && !MathUtils.isNullOrZero(detail.getQuantity())) {
					// don't remove zero position stocks as are stock dividends in kind
					// also CDX and IRS position open can have price of 100
					continue;
				}
				// remove zero valued non-position records
				if (MathUtils.isNullOrZero(detail.getLocalDebitCredit()) && MathUtils.isNullOrZero(detail.getBaseDebitCredit()) && MathUtils.isNullOrZero(detail.getPositionCostBasis())) {
					journal.removeJournalEntries(journal.findChildEntries(detail));
					journal.removeJournalDetail(detail);
				}
			}
		}
	}
}
