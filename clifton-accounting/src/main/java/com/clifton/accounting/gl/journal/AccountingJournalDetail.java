package com.clifton.accounting.gl.journal;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;


/**
 * The <code>AccountingJournalDetail</code> class represents an accounting journal line item.
 *
 * @author vgomelsky
 */
public class AccountingJournalDetail extends BaseEntity<Long> implements AccountingJournalDetailDefinition {

	public static final String JOURNAL_DETAIL_PRINT_HEADER = "ID\tPID\tClient #\tHolding #\tGL Account\tSecurity\tPrice\tQty\tLocal Debit/Credit\tTran Date\tFX Rate\tBase Debit/Credit\tCost Basis\tOriginal Date\tSettlement Date\tDescription\r";
	public static final String JOURNAL_DETAIL_PRINT_FORMAT = "%d\t%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s";
	public static final String JOURNAL_DETAIL_PRINT_HEADER_WITH_ADDITIONAL_FIELDS = "ID\tPID\tClient #\tHolding #\tGL Account\tSecurity\tIsOpening\tExecuting/Clearing Company\tPrice\tQty\tLocal Debit/Credit\tTran Date\tFX Rate\tBase Debit/Credit\tCost Basis\tOriginal Date\tSettlement Date\tDescription\r";
	public static final String JOURNAL_DETAIL_PRINT_FORMAT_WITH_ADDITIONAL_FIELDS = "%d\t%d\t%s\t%s\t%s\t%s\t%b\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s";

	private AccountingJournal journal;
	/**
	 * Note: this is not a self-referencing parent but points to AccountingTransaction instead.
	 * <p/>
	 * Optional parent transaction which is used by closing transactions to identify what they're closing or
	 * by other transactions to identify related transactions (commission, fee, etc.)
	 */
	private AccountingTransaction parentTransaction;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private AccountingAccount accountingAccount;
	/**
	 * Optional executing broker that can be used in commission calculation: clearing vs executing
	 */
	private BusinessCompany executingCompany;

	// soft link to source table row that originated this transaction
	private SystemTable systemTable;
	@SoftLinkField(tableBeanPropertyName = "systemTable")
	private Integer fkFieldId;

	/**
	 * For certain investment instruments, the quantity can have fractions.
	 * Face value for bonds is stored in Quantity fields and can go up to 2 decimal places.
	 */
	private BigDecimal quantity;
	/**
	 * The price of the recorded transaction. Can have up to 10 decimal places.
	 */
	private BigDecimal price;
	/**
	 * (Debit - Credit) represented in the local currency of the transaction.
	 */
	private BigDecimal localDebitCredit;
	/**
	 * (Debit - Credit) represented in the base currency of the client account.
	 */
	private BigDecimal baseDebitCredit;
	/**
	 * Costs basis (LOCAL) is reduced proportionally to quantity as position gets closed.
	 * It is be used to calculate realized gain loss for this position.
	 * We store LOCAL cost basis only.  Base cost basis can be calculated by multiplying it by exchange rate.
	 */
	private BigDecimal positionCostBasis;
	/**
	 * Transaction exchange rate multiplier to convert local to base currency when they're different.
	 * The value should be 1.0 when the base currency is the same as the local currency.
	 * investmentSecurity field determines the local currency and reporting account determines the base currency.
	 */
	private BigDecimal exchangeRateToBase;

	private String description;

	/**
	 * The date when transaction took place. Current position look-ups must be based on this field.
	 */
	private Date transactionDate;
	/**
	 * The date of the original transaction which in most cases is the same as transaction date.
	 * However, for transfers it is the date of the opening trade which is prior to transaction date.
	 * The only time this date is used is to figure out which open position (based on transactionDate) to close first.
	 */
	private Date originalTransactionDate;
	/**
	 * The date when this transaction settles (assets exchange hands). It's usually 1 or more business days
	 * after the trade depending on investment security or trade properties.
	 */
	private Date settlementDate;

	/**
	 * Primarily applies to Position GL Accounts and indicates whether this transaction opens a new position or closes an existing position.
	 * <p/>
	 * The following logic can be used to determine closing positions:
	 * if (getAccountingAccount().isPosition() && getParent() != null && getQuantity().signum() != getParent().getQuantity().signum()) {
	 * <p/>
	 * NOTE: may want to compare security too. Also stock splits generate opening and closing records that both point to the same lot.
	 * Bond factor change may increase cost basis (history correction) which will result in increase of remaining cost basis: still opening.
	 * <p/>
	 */
	private boolean opening;

	// THE FOLLOWING FIELDS ARE NOT PERSISTED AND ARE USED BY BOOKING RULES ONLY
	// BETTER DESIGN WOULD BE TO HAVE A SEPARATE BOOKING OBJECT THAT WRAPS CORRESPONDING AccountingJournalDetail

	/**
	 * As opposed to parentTransaction, this is the parent from the same journal before parentTransaction has been created.
	 * The parent can be corresponding position being reduced within the same journal by this detail (Asset Backed Security: Current Face < Original Face).
	 * Or the parent can be a related position that this transaction is linked to: parent position for commission, realized, accrued interest, etc.
	 */
	private AccountingJournalDetailDefinition parentDefinition;
	@NonPersistentField
	private BigDecimal positionCommission; // used to override default commission calculation logic

	/**
	 * Non persisted field that indicates that the detail represents a transaction that is being used to adjust the quantity
	 * on a security with factor changes.
	 */
	@NonPersistentField
	private boolean factorChangeQuantityAdjustment;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * In some cases we manually set IDs, only way to know if
	 * a bean really is a new bean is if RV is null
	 */
	@Override
	public boolean isNewBean() {
		return getRv() == null;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		if (getInvestmentSecurity() != null) {
			result.append(", Symbol = ");
			result.append(getInvestmentSecurity().getSymbol());
		}
		if (getAccountingAccount() != null) {
			result.append(", GL Account = ");
			result.append(getAccountingAccount().getName());
		}
		result.append(", Cost Basis = ");
		result.append(CoreMathUtils.formatNumberMoney(getPositionCostBasis()));
		result.append(", Settlement Date = ");
		result.append(DateUtils.fromDateShort(getSettlementDate()));
		result.append('}');
		return result.toString();
	}


	@Override
	public String toStringFormatted(boolean includeHeader) {
		return toStringFormatted(includeHeader, false);
	}


	@Override
	public String toStringFormatted(boolean includeHeader, boolean withAdditionalFields) {
		String result = "";

		String journalDetailHeader = withAdditionalFields ? JOURNAL_DETAIL_PRINT_HEADER_WITH_ADDITIONAL_FIELDS : JOURNAL_DETAIL_PRINT_HEADER;
		String journalDetailPrintFormat = withAdditionalFields ? JOURNAL_DETAIL_PRINT_FORMAT_WITH_ADDITIONAL_FIELDS : JOURNAL_DETAIL_PRINT_FORMAT;

		if (includeHeader) {
			result = journalDetailHeader;
		}

		// InvestmentSecurity might be null in some cases for integration tests
		String securitySymbol = getInvestmentSecurity() != null ? getInvestmentSecurity().getSymbol() : "";

		AccountingJournalDetailDefinition parentEntity = ObjectUtils.coalesce(getParentTransaction(), getParentDefinition());
		Number parentId = parentEntity != null ? parentEntity.getId() : null;

		ArrayList<Object> journalDetailValues = new ArrayList<>();
		journalDetailValues.add(getIdentity());
		journalDetailValues.add(parentId);
		journalDetailValues.add(getClientInvestmentAccount().getNumber());
		journalDetailValues.add(getHoldingInvestmentAccount().getNumber());
		journalDetailValues.add(getAccountingAccount().getName());
		journalDetailValues.add(securitySymbol);
		if (withAdditionalFields) {
			journalDetailValues.add(isOpening());
			journalDetailValues.add(getExecutingOrClearingCompany().getName());
		}
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getPrice()));
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getQuantity()));
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getLocalDebitCredit()));
		journalDetailValues.add(DateUtils.fromDateShort(getTransactionDate()));
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getExchangeRateToBase()));
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getBaseDebitCredit()));
		journalDetailValues.add(CoreMathUtils.formatNumberDecimal(getPositionCostBasis()));
		journalDetailValues.add(DateUtils.fromDateShort(getOriginalTransactionDate()));
		journalDetailValues.add(DateUtils.fromDateShort(getSettlementDate()));
		journalDetailValues.add(getDescription());

		return result + String.format(journalDetailPrintFormat, journalDetailValues.toArray());
	}


	/**
	 * Negates all GL amount fields: quantity, localDebitCredit, baseDebitCredit, positionCostBasis.
	 * This is usually used when creating a reversing GL entry.
	 */
	public void reverseSignForGLAmounts() {
		setQuantity(MathUtils.negate(getQuantity()));
		setLocalDebitCredit(MathUtils.negate(getLocalDebitCredit()));
		setBaseDebitCredit(MathUtils.negate(getBaseDebitCredit()));
		setPositionCostBasis(MathUtils.negate(getPositionCostBasis()));
	}


	public BigDecimal getLocalDebit() {
		BigDecimal result = this.localDebitCredit;
		if (result != null) {
			if (MathUtils.isLessThan(result, BigDecimal.ZERO)) {
				result = BigDecimal.ZERO;
			}
		}
		return result;
	}


	public BigDecimal getLocalCredit() {
		BigDecimal result = this.localDebitCredit;
		if (result != null) {
			if (MathUtils.isGreaterThan(result, BigDecimal.ZERO)) {
				result = BigDecimal.ZERO;
			}
			else {
				result = result.negate();
			}
		}
		return result;
	}


	public BigDecimal getBaseDebit() {
		BigDecimal result = this.baseDebitCredit;
		if (result != null) {
			if (MathUtils.isLessThan(result, BigDecimal.ZERO)) {
				result = BigDecimal.ZERO;
			}
		}
		return result;
	}


	public BigDecimal getBaseCredit() {
		BigDecimal result = this.baseDebitCredit;
		if (result != null) {
			if (MathUtils.isGreaterThan(result, BigDecimal.ZERO)) {
				result = BigDecimal.ZERO;
			}
			else {
				result = result.negate();
			}
		}
		return result;
	}


	/**
	 * As opposed to parentTransaction, this is the parent from the same journal before parentTransaction has been created.
	 * The parent can be corresponding position being reduced within the same journal by this detail (Asset Backed Security: Current Face < Original Face).
	 * Or the parent can be a related position that this transaction is linked to: parent position for commission, realized, accrued interest, etc.
	 */
	@Override
	public AccountingJournalDetailDefinition getParentDefinition() {
		return this.parentDefinition;
	}


	/**
	 * As opposed to parentTransaction, this is the parent from the same journal before parentTransaction has been created.
	 * The parent can be corresponding position being reduced within the same journal by this detail (Asset Backed Security: Current Face < Original Face).
	 * Or the parent can be a related position that this transaction is linked to: parent position for commission, realized, accrued interest, etc.
	 */
	@Override
	public void setParentDefinition(AccountingJournalDetailDefinition parent) {
		this.parentDefinition = parent;
	}


	@Override
	public String createNaturalKey() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("SE").append(getInvestmentSecurity().getIdentity());
		buffer.append("CA").append(getClientInvestmentAccount().getIdentity());
		buffer.append("HA").append(getHoldingInvestmentAccount().getIdentity());
		buffer.append("IA").append(getAccountingAccount().getIdentity());
		buffer.append("TD").append(getOriginalTransactionDate());
		buffer.append("SD").append(getSettlementDate());
		return buffer.toString();
	}


	@Override
	public AccountingJournal getJournal() {
		return this.journal;
	}


	@Override
	public void setJournal(AccountingJournal journal) {
		this.journal = journal;
	}


	@Override
	public AccountingTransaction getParentTransaction() {
		return this.parentTransaction;
	}


	@Override
	public void setParentTransaction(AccountingTransaction parentTransaction) {
		this.parentTransaction = parentTransaction;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	@Override
	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	@Override
	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	@Override
	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	@Override
	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	@Override
	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	@Override
	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	@Override
	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	/**
	 * Returns positionCostBasis for cash/currencies if quantity field is null.
	 * Returns quantity field otherwise.
	 */
	@Override
	public BigDecimal getQuantityNormalized() {
		// use security instead of accountingAccount because Contribution/Distribution can be made for a currency
		if (this.quantity == null && this.investmentSecurity != null && this.investmentSecurity.isCurrency()) {
			return this.positionCostBasis;
		}
		return this.quantity;
	}


	@Override
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	@Override
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	@Override
	public BigDecimal getLocalDebitCredit() {
		return this.localDebitCredit;
	}


	@Override
	public void setLocalDebitCredit(BigDecimal localDebitCredit) {
		this.localDebitCredit = localDebitCredit;
	}


	@Override
	public BigDecimal getBaseDebitCredit() {
		return this.baseDebitCredit;
	}


	@Override
	public void setBaseDebitCredit(BigDecimal baseDebitCredit) {
		this.baseDebitCredit = baseDebitCredit;
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	@Override
	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	@Override
	public String getDescription() {
		return this.description;
	}


	@Override
	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public Date getTransactionDate() {
		return this.transactionDate;
	}


	@Override
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	@Override
	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	@Override
	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	@Override
	public Date getSettlementDate() {
		return this.settlementDate;
	}


	@Override
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	@Override
	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public BigDecimal getPrice() {
		return this.price;
	}


	@Override
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	@Override
	public boolean isOpening() {
		return this.opening;
	}


	@Override
	public void setOpening(boolean opening) {
		this.opening = opening;
	}


	@Override
	public boolean isClosing() {
		return !this.opening;
	}


	/**
	 * Returns LOCAL cost basis.
	 */
	@Override
	public BigDecimal getPositionCostBasis() {
		return this.positionCostBasis;
	}


	@Override
	public void setPositionCostBasis(BigDecimal positionCostBasis) {
		this.positionCostBasis = positionCostBasis;
	}


	@Override
	public BigDecimal getPositionCommission() {
		return this.positionCommission;
	}


	@Override
	public void setPositionCommission(BigDecimal positionCommission) {
		this.positionCommission = positionCommission;
	}


	@Override
	public BusinessCompany getExecutingOrClearingCompany() {
		if (this.executingCompany == null && this.holdingInvestmentAccount != null) {
			return this.holdingInvestmentAccount.getIssuingCompany();
		}
		return this.executingCompany;
	}


	@Override
	public BusinessCompany getExecutingCompany() {
		return this.executingCompany;
	}


	@Override
	public void setExecutingCompany(BusinessCompany executingCompany) {
		this.executingCompany = executingCompany;
	}


	@Override
	public boolean isFactorChangeQuantityAdjustment() {
		return this.factorChangeQuantityAdjustment;
	}


	public void setFactorChangeQuantityAdjustment(boolean factorChangeQuantityAdjustment) {
		this.factorChangeQuantityAdjustment = factorChangeQuantityAdjustment;
	}
}
