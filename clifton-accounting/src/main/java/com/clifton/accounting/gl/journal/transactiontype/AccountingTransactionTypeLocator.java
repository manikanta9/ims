package com.clifton.accounting.gl.journal.transactiontype;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;


/**
 * The <code>AccountingTransactionTypeLocator</code> locates a {@link AccountingTransactionTypeRetriever} based on a
 * journal type name. Typically there is only one instance used for retrieving locators. Once a retriever is obtained
 * using the journal type name, it can be used to retrieve the {@link AccountingTransactionType} type based on the
 * FK Field ID.
 *
 * @author jgommels
 */
public interface AccountingTransactionTypeLocator {

	/**
	 * Returns the {@link AccountingTransactionType} associated with the {@link AccountingJournalDetailDefinition} and {@link BookableEntity}. If no specific transaction type can be determined, then
	 * <code>null</code> is returned.
	 *
	 * @param journalDetail
	 * @param bookableEntity
	 */
	public <T extends BookableEntity> AccountingTransactionType getAccountingTransactionType(AccountingJournalDetailDefinition journalDetail, T bookableEntity);


	/**
	 * Returns the {@link AccountingTransactionTypeRetriever} for <code>journalTypeName</code> that can be used to
	 * retrieve the {@link AccountingTransactionType}.
	 *
	 * @param journalTypeName the name of the journal type to return a {@link AccountingTransactionTypeRetriever} for
	 * @return the {@link AccountingTransactionTypeRetriever} which can be used to obtain a {@link AccountingTransactionType} type based on the
	 * FK Field ID
	 */
	public AccountingTransactionTypeRetriever<?> getAccountingTransactionTypeRetriever(String journalTypeName);
}
