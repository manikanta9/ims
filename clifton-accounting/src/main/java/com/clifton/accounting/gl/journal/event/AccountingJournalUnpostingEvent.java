package com.clifton.accounting.gl.journal.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.util.event.EventObject;


/**
 * <code>AccountingJournalUnpostingEvent</code> represents an {@link com.clifton.core.util.event.Event} for
 * unposting/unbooking {@link AccountingJournal}s.
 *
 * @author vgomelsky
 */
public class AccountingJournalUnpostingEvent extends EventObject<AccountingJournal, Object> {

	public static final String EVENT_NAME = "Accounting Journal Unposted";

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingJournalUnpostingEvent(AccountingJournal journal) {
		super(EVENT_NAME);
		setTarget(journal);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static AccountingJournalUnpostingEvent ofEventTarget(AccountingJournal journal) {
		return new AccountingJournalUnpostingEvent(journal);
	}
}
