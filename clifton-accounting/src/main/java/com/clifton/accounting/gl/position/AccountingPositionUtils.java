package com.clifton.accounting.gl.position;

import com.clifton.accounting.AccountingUtils;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The static utility class for {@link AccountingPosition} objects.
 *
 * @author MikeH
 */
public class AccountingPositionUtils {

	private static final Comparator<AccountingPosition> CHRONOLOGICAL_POSITION_COMPARATOR = Comparator
			.comparing(AccountingPosition::getTransactionDate, (d1, d2) -> DateUtils.compare(d1, d2, false))
			.thenComparing(AccountingPosition::getAccountingTransactionId, MathUtils::compare);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPositionUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Position Merge Utilities                        ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a new list of all positions with {@link AccountingPosition#isPendingActivity() pending positions} merged.
	 * <p>
	 * Pending positions are applied sequentially to the list of pre-existing positions for any given position {@link
	 * AccountingUtils#createAccountingObjectNaturalKey(AccountingObjectInfo) key}. Pending positions may result in partial closes, full closes, a change from a short position to a
	 * long position or vice versa, or simply the creation of a new position.
	 *
	 * @param positionList                the list of positions within which pending positions shall be merged
	 * @param includeZeroRemainingBalance includes merged positions with remaining quantity of zero in the returned list when true
	 * @return the new list of positions with position merges applied
	 */
	public static List<AccountingPosition> mergePendingAccountingPositionList(List<AccountingPosition> positionList, boolean includeZeroRemainingBalance) {
		// Guard-clause: Handle trivial cases
		if (positionList.size() < 2 || !CollectionUtils.anyMatch(positionList, AccountingPosition::isPendingActivity)) {
			return positionList;
		}

		// Partition position lists
		Map<Boolean, List<AccountingPosition>> positionListByIsCloseOnMaturityOnly = CollectionUtils.getPartitioned(positionList, position -> position.getInvestmentSecurity().getInstrument().getHierarchy().isCloseOnMaturityOnly());
		List<AccountingPosition> nonMergeablePositionList = positionListByIsCloseOnMaturityOnly.get(true);
		List<AccountingPosition> mergeablePositionList = positionListByIsCloseOnMaturityOnly.get(false);
		Map<Boolean, List<AccountingPosition>> mergeablePositionListMapByIsPending = CollectionUtils.getPartitioned(mergeablePositionList, AccountingPosition::isPendingActivity);
		List<AccountingPosition> mergeableExistingPositionList = mergeablePositionListMapByIsPending.get(false);
		List<AccountingPosition> mergeablePendingPositionList = mergeablePositionListMapByIsPending.get(true);

		// Get keys for lists
		Map<String, List<AccountingPosition>> existingPositionListByKey = BeanUtils.getBeansMap(mergeableExistingPositionList, AccountingUtils::createAccountingObjectNaturalKey);
		Map<String, List<AccountingPosition>> pendingPositionListByKey = BeanUtils.getBeansMap(mergeablePendingPositionList, AccountingUtils::createAccountingObjectNaturalKey);
		List<String> positionKeyList = CollectionUtils.buildStream(existingPositionListByKey.keySet(), pendingPositionListByKey.keySet()).distinct().collect(Collectors.toList());

		// Merge positions by with identical keys
		List<AccountingPosition> mergedPositionList = CollectionUtils.getConvertedFlattened(positionKeyList, positionKey -> {
			List<AccountingPosition> existingPositionListForKey = existingPositionListByKey.getOrDefault(positionKey, Collections.emptyList());
			List<AccountingPosition> pendingPositionListForKey = pendingPositionListByKey.getOrDefault(positionKey, Collections.emptyList());
			final List<AccountingPosition> mergedPositionListForKey;
			if (pendingPositionListForKey.isEmpty()) {
				// Trivial case: No pending positions for merge
				mergedPositionListForKey = existingPositionListForKey;
			}
			else {
				// Apply pending positions to existing list one at a time in application order (sequential reduction must be used in order to allow pending positions to interact, too)
				CollectionUtils.sort(pendingPositionListForKey, CHRONOLOGICAL_POSITION_COMPARATOR);
				mergedPositionListForKey = pendingPositionListForKey.stream()
						.reduce(existingPositionListForKey, (existingPositionList, pendingPosition) -> mergePositionIntoList(existingPositionList, pendingPosition, includeZeroRemainingBalance), CollectionUtils.throwingCombiner());
			}
			return mergedPositionListForKey;
		});

		return CollectionUtils.combineCollections(mergedPositionList, nonMergeablePositionList);
	}


	public static List<AccountingPositionBalance> mergeAccountingPositionList(List<AccountingPosition> positionList) {
		if (CollectionUtils.isEmpty(positionList)) {
			return Collections.emptyList();
		}

		Map<String, AccountingPositionBalance> keyToBalanceMap = new HashMap<>();
		for (AccountingPosition position : positionList) {
			String key = AccountingUtils.createAccountingObjectNaturalKey(position);
			keyToBalanceMap.compute(key, (k, currentBalance) -> {
				if (currentBalance == null) {
					return AccountingPositionBalance.of(position);
				}
				return currentBalance.applyAccountingPositionAdjustment(position);
			});
		}
		return new ArrayList<>(keyToBalanceMap.values());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a new list with the provided position merged into the existing position list.
	 * <p>
	 * This method takes into account a {@link AccountingPositionOrders lot-relief order} for the existing position list. Using this order, the <code>mergePosition</code> is
	 * applied to each position in the provided list in-order until the merge position is completely absorbed (i.e., its quantity is nullified to 0) or until the end of the list is
	 * reached, at which point the remaining portion of the merge position is added to the end of the list.
	 * <p>
	 * Positions whose quantities are reduced to zero by this method are not included in the resulting list.
	 *
	 * @param positionList                the list of positions into which the <code>mergePosition</code> shall be applied
	 * @param mergePosition               the position to apply to the given list
	 * @param includeZeroRemainingBalance includes the merged position in the returned list if its remaining quantity becomes zero
	 * @return the list of positions with the <code>mergePosition</code> applied
	 */
	private static List<AccountingPosition> mergePositionIntoList(List<AccountingPosition> positionList, AccountingPosition mergePosition, boolean includeZeroRemainingBalance) {
		// Guard-clause: No existing positions to absorb merge
		if (positionList.isEmpty()) {
			return Collections.singletonList(mergePosition);
		}

		// Iterate over all existing positions in lot-relief order, applying position until completely absorbed
		AccountingPositionOrders.FIFO.sortPositions(positionList);
		AccountingPosition remainingMergePosition = mergePosition;
		List<AccountingPosition> resultPositionList = new ArrayList<>();
		for (AccountingPosition existingPosition : positionList) {
			// Guard-clause: Merge position must be non-empty and positions must have opposite signs
			if (MathUtils.isNullOrZero(remainingMergePosition.getRemainingQuantity())
					|| MathUtils.isSameSignum(existingPosition.getRemainingQuantity(), mergePosition.getRemainingQuantity())) {
				resultPositionList.add(existingPosition);
				continue;
			}

			// Absorb pending changes into existing position
			AccountingPositionMergeResult mergeResult = mergePositions(existingPosition, remainingMergePosition);
			AccountingPosition remainingExistingPosition = mergeResult.getRemainingExistingPosition();
			remainingMergePosition = mergeResult.getRemainingMergePosition();

			// Include existing position if not completely nullified
			if (includeZeroRemainingBalance || !MathUtils.isNullOrZero(remainingExistingPosition.getRemainingQuantity())) {
				resultPositionList.add(remainingExistingPosition);
			}
		}

		// Include pending position in resulting list if not fully absorbed by existing positions
		if (!MathUtils.isNullOrZero(remainingMergePosition.getRemainingQuantity())) {
			resultPositionList.add(remainingMergePosition);
		}

		return resultPositionList;
	}


	/**
	 * Performs reductions on the two provided positions (if necessary) to merge the <code>mergePosition</code> into the <code>existingPosition</code>.
	 * <p>
	 * If the positions are mergeable then this method applies the <code>mergePosition</code> to the full extent possible to the <code>existingPosition</code>. This will reduce the
	 * quantities of both the <code>existingPosition</code> and the <code>mergePosition</code> by the lesser of their remaining quantities. Other values, such as remaining cost
	 * basis, will be reduced proportionally.
	 *
	 * @param existingPosition the existing position
	 * @param mergePosition    the position to merge into the existing position
	 * @return an object describing the resulting reduced positions
	 */
	private static AccountingPositionMergeResult mergePositions(AccountingPosition existingPosition, AccountingPosition mergePosition) {
		// Guard-clause: Require differing sign for merge
		if (MathUtils.isSameSignum(existingPosition.getRemainingQuantity(), mergePosition.getRemainingQuantity())) {
			return new AccountingPositionMergeResult(existingPosition, mergePosition);
		}

		BigDecimal reduceQuantity = MathUtils.min(MathUtils.abs(existingPosition.getRemainingQuantity()), MathUtils.abs(mergePosition.getRemainingQuantity()));
		AccountingPosition reducedExistingPosition = reducePositionQuantity(existingPosition, reduceQuantity);
		AccountingPosition reducedMergePosition = reducePositionQuantity(mergePosition, reduceQuantity);
		return new AccountingPositionMergeResult(reducedExistingPosition, reducedMergePosition);
	}


	/**
	 * Reduces the position by the given quantity.
	 * <p>
	 * This reduces quantity by the amount specified and adjust other properties accordingly.
	 *
	 * @param position    the position to reduce
	 * @param absQuantity the absolute value of the quantity by which to reduce the position
	 * @return the new position with the reduced quantity
	 */
	private static AccountingPosition reducePositionQuantity(AccountingPosition position, BigDecimal absQuantity) {
		BigDecimal originalQuantity = position.getRemainingQuantity();
		if (MathUtils.isNullOrZero(originalQuantity)) {
			// The position is already closed and cannot be reduced. This can happen if pending activity merging requests to include positions with zero quantity.
			return position;
		}

		// Reduce quantity linearly
		AccountingPosition reducedPosition = new AccountingPosition(position);
		reducedPosition.setPendingActivity(true); // Set pending since it is adjusted with pending position
		BigDecimal newQuantity = MathUtils.subtract(originalQuantity, originalQuantity.signum() == -1 ? absQuantity.negate() : absQuantity);
		reducedPosition.setRemainingQuantity(newQuantity);

		// Reduce cost-basis and debit/credit proportionally
		BigDecimal reductionRatio = MathUtils.divide(newQuantity, originalQuantity);
		BigDecimal newCostBasis = MathUtils.multiply(reducedPosition.getRemainingCostBasis(), reductionRatio);
		BigDecimal newBaseDebitCredit = MathUtils.multiply(reducedPosition.getRemainingBaseDebitCredit(), reductionRatio);
		reducedPosition.setRemainingCostBasis(InvestmentCalculatorUtils.roundLocalAmount(newCostBasis, position.getInvestmentSecurity()));
		reducedPosition.setRemainingBaseDebitCredit(InvestmentCalculatorUtils.roundBaseAmount(newBaseDebitCredit, position.getClientInvestmentAccount()));

		return reducedPosition;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The container type for the result from a two-position merge.
	 */
	private static class AccountingPositionMergeResult {

		private final AccountingPosition remainingExistingPosition;
		private final AccountingPosition remainingMergePosition;


		public AccountingPositionMergeResult(AccountingPosition remainingExistingPosition, AccountingPosition remainingMergePosition) {
			this.remainingExistingPosition = remainingExistingPosition;
			this.remainingMergePosition = remainingMergePosition;
		}


		public AccountingPosition getRemainingExistingPosition() {
			return this.remainingExistingPosition;
		}


		public AccountingPosition getRemainingMergePosition() {
			return this.remainingMergePosition;
		}
	}
}
