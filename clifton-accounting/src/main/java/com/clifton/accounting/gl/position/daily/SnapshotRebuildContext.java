package com.clifton.accounting.gl.position.daily;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The SnapshotRebuildContext class is used to cache prices and exchange rates during position snapshot rebuilds.
 * It helps improve performance by avoiding extra expensive data look ups.
 *
 * @author vgomelsky
 */
public class SnapshotRebuildContext {

	private Map<Integer, BigDecimal> securityPriceMap = new HashMap<>();
	private Map<Integer, BigDecimal> priorSecurityPriceMap = new HashMap<>();

	private Map<Integer, BigDecimal> securityTradePriceMap = new HashMap<>();
	private Map<Integer, BigDecimal> priorSecurityTradePriceMap = new HashMap<>();

	private final Map<Integer, InvestmentSecurity> securityMap = new HashMap<>();

	private final Set<InvestmentSecurity> marketDataSecurities = new HashSet<>(); // securities that we rebuild market data for

	private final FxRateLookupCache fxRateLookupCache;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SnapshotRebuildContext(MarketDataExchangeRatesApiService apiService) {
		this.fxRateLookupCache = new FxRateLookupCache(apiService);
	}


	public void clear(boolean clearTradePrices) {
		this.securityPriceMap.clear();
		this.priorSecurityPriceMap.clear();
		this.marketDataSecurities.clear();

		if (clearTradePrices) {
			this.securityTradePriceMap.clear();
			this.priorSecurityTradePriceMap.clear();
		}

		this.fxRateLookupCache.clearFxRatesCache();
	}


	/**
	 * Returns the next Weekday (or month end date) for the specified snapshotDate.
	 * Sets prior Maps to current Maps and clears current Maps to get them ready for next weekday processing.
	 */
	public Date moveToNextWeekDate(Date snapshotDate) {
		this.priorSecurityPriceMap = this.securityPriceMap;
		this.securityPriceMap = new HashMap<>();

		this.priorSecurityTradePriceMap = this.securityTradePriceMap;
		this.securityTradePriceMap = new HashMap<>();

		Date nextWeekday = DateUtils.getNextWeekday(snapshotDate);
		Date monthEnd = DateUtils.getLastDayOfMonth(snapshotDate);

		// If month end is between snapshot date and next week day, return the month end date so that snapshots are still rebuilt on month end
		if (DateUtils.isDateAfter(monthEnd, snapshotDate) && DateUtils.isDateAfter(nextWeekday, monthEnd)) {
			return monthEnd;
		}
		return nextWeekday;
	}

	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getSecurityPrice(int securityId) {
		return this.securityPriceMap.get(securityId);
	}


	public void cacheSecurityPrice(int securityId, BigDecimal price) {
		this.securityPriceMap.put(securityId, price);
	}


	public BigDecimal getSecurityTradePrice(int securityId) {
		return this.securityTradePriceMap.get(securityId);
	}


	public void cacheSecurityTradePrice(int securityId, BigDecimal price) {
		this.securityTradePriceMap.put(securityId, price);
	}

	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPriorSecurityPrice(int securityId) {
		return this.priorSecurityPriceMap.get(securityId);
	}


	public void cachePriorSecurityPrice(int securityId, BigDecimal price) {
		this.priorSecurityPriceMap.put(securityId, price);
	}


	public BigDecimal getPriorSecurityTradePrice(int securityId) {
		return this.priorSecurityTradePriceMap.get(securityId);
	}


	public void cachePriorSecurityTradePrice(int securityId, BigDecimal price) {
		this.priorSecurityTradePriceMap.put(securityId, price);
	}

	////////////////////////////////////////////////////////////////////////////


	public Map<Integer, InvestmentSecurity> getSecurityMap() {
		return this.securityMap;
	}


	public Set<InvestmentSecurity> getMarketDataSecurities() {
		return this.marketDataSecurities;
	}


	public FxRateLookupCache getFxRateLookupCache() {
		return this.fxRateLookupCache;
	}
}
