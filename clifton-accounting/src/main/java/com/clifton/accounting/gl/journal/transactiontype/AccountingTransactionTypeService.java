package com.clifton.accounting.gl.journal.transactiontype;

import java.util.List;


/**
 * The <code>AccountingTransactionTypeService</code> defines the methods for working with {@link AccountingTransactionType} Objects.
 *
 * @author theodorez
 */
public interface AccountingTransactionTypeService {


	public AccountingTransactionType getAccountingTransactionType(short id);


	public AccountingTransactionType getAccountingTransactionTypeByName(String name);


	public List<AccountingTransactionType> getAccountingTransactionTypeList(AccountingTransactionTypeSearchForm searchForm);


	public AccountingTransactionType saveAccountingTransactionType(AccountingTransactionType bean);


	public void deleteAccountingTransactionType(short id);
}
