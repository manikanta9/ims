package com.clifton.accounting.gl.position;


import com.clifton.accounting.AccountingUtils;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl.AccountingAccountIds;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.pending.AccountingPendingActivityHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.dataaccess.sql.SqlSelectCommandUnionAll;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>AccountingPositionServiceImpl</code> class provides basic implementation of the AccountingPositionService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingPositionServiceImpl implements AccountingPositionService {

	private AccountingAccountIdsCache accountingAccountIdsCache;
	private AccountingPositionHandler accountingPositionHandler;
	private AccountingTransactionService accountingTransactionService;
	private AccountingPendingActivityHandler accountingPendingActivityHandler;

	private InvestmentInstrumentService investmentInstrumentService;

	private SqlHandler sqlHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPosition> getAccountingPositionListUsingCommand(AccountingPositionCommand command) {
		return getAccountingPositionListImpl(command);
	}


	@Override
	public List<AccountingPosition> getAccountingPositionList(Integer clientInvestmentAccountId, Integer holdingInvestmentAccountId, Integer investmentSecurityId, Date transactionDate) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setClientInvestmentAccountId(clientInvestmentAccountId);
		command.setHoldingInvestmentAccountId(holdingInvestmentAccountId);
		command.setInvestmentSecurityId(investmentSecurityId);
		return getAccountingPositionListImpl(command);
	}


	@Override
	public List<AccountingPosition> getAccountingPositionListForSecurity(int investmentSecurityId, Date transactionDate) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setInvestmentSecurityId(investmentSecurityId);
		return getAccountingPositionListImpl(command);
	}


	@Override
	public List<AccountingPosition> getAccountingPositionListForInstrument(int investmentInstrumentId, Date transactionDate, boolean includeBigInstrumentPositions) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setInvestmentInstrumentId(investmentInstrumentId);
		command.setIncludeBigInstrumentPositions(includeBigInstrumentPositions);
		return getAccountingPositionListImpl(command);
	}


	@Override
	public List<AccountingPosition> getAccountingPositionListForClientAccount(int clientInvestmentAccountId, Date transactionDate) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setClientInvestmentAccountId(clientInvestmentAccountId);
		return getAccountingPositionListImpl(command);
	}

	///////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Balance Methods          /////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionBalance> getAccountingPositionBalanceList(AccountingPositionCommand accountingPositionCommand) {
		List<AccountingPosition> positionList = getAccountingPositionListUsingCommand(accountingPositionCommand);

		if (CollectionUtils.isEmpty(positionList)) {
			return Collections.emptyList();
		}

		if (accountingPositionCommand.getPendingActivityRequest() == null || accountingPositionCommand.getPendingActivityRequest().isMerge()) {
			return AccountingPositionUtils.mergeAccountingPositionList(positionList);
		}
		// merge pending and GL positions separately and combine
		Map<Boolean, List<AccountingPosition>> pendingPositionListMap = BeanUtils.getBeansMap(positionList, AccountingPosition::isPendingActivity);
		return CollectionUtils.getConvertedFlattened(pendingPositionListMap.values(), AccountingPositionUtils::mergeAccountingPositionList);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Calculated Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getAccountingPositionMarketValueForExistingPosition(long existingPositionId, Date date, BigDecimal overrideQuantity) {
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(date);
		command.setExistingPositionId(existingPositionId);
		AccountingPosition position = CollectionUtils.getOnlyElement(getAccountingPositionListImpl(command));

		if (position == null) {
			throw new ValidationException("Cannot find existing position with AccountingTransactionID = " + existingPositionId + " on " + DateUtils.fromDateShort(date));
		}

		if (overrideQuantity != null) {
			position.setRemainingQuantity(overrideQuantity);
		}

		return getAccountingPositionHandler().getAccountingPositionMarketValue(position, date);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional(readOnly = true, noRollbackFor = ValidationException.class)
	protected List<AccountingPosition> getAccountingPositionListImpl(final AccountingPositionCommand command) {
		// Validate arguments
		command.validate();

		// Retrieve positions
		List<AccountingPosition> positionList = retrievePositionList(command, cmd -> {
			SqlSelectCommand sql = getPositionsSelectCommand(cmd);
			return getSqlHandler().executeSelect(sql, createPositionResultSetExtractor());
		});

		// Include pending activity
		if (command.getPendingActivityRequest() != null) {
			List<AccountingPosition> pendingList = getPendingAccountingPositionList(command);
			if (!CollectionUtils.isEmpty(pendingList)) {
				positionList.addAll(pendingList);
				if (command.getPendingActivityRequest().isMerge()) {
					positionList = AccountingPositionUtils.mergePendingAccountingPositionList(positionList, command.getPendingActivityRequest().isIncludeLocalBalances());
				}
			}
		}

		// Include pending receivables
		if (command.isIncludeUnsettledLegPayments()) {
			List<AccountingPosition> receivablePositions = retrievePositionList(command, this::getPendingReceivablePositionList);
			if (!CollectionUtils.isEmpty(receivablePositions)) {
				Set<String> positionKeySet = CollectionUtils.getStream(positionList)
						.map(AccountingUtils::createAccountingObjectNaturalKey)
						.collect(Collectors.toSet());
				for (AccountingPosition receivablePosition : receivablePositions) {
					if (!positionKeySet.contains(AccountingUtils.createAccountingObjectNaturalKey(receivablePosition))) {
						positionList.add(receivablePosition);
					}
				}
			}
		}

		if (CollectionUtils.getSize(positionList) > 1 && command.getOrder() != null) {
			// sorting is very important: used for position closing
			command.getOrder().sortPositions(positionList);
		}

		return positionList;
	}


	List<AccountingPosition> retrievePositionList(final AccountingPositionCommand command, Function<AccountingPositionCommand, List<AccountingPosition>> positionSupplier) {
		// Configure query to retrieve positions that were open on the specified date
		Date transactionDate = command.getTransactionDate();
		Date settlementDate = command.getSettlementDate();

		// add one day and use less-than restriction in order to include transactions with any time from the specified date
		if (transactionDate != null) {
			command.setTransactionDate(DateUtils.addDays(DateUtils.clearTime(transactionDate), 1));
		}
		else if (settlementDate != null) {
			command.setSettlementDate(DateUtils.addDays(DateUtils.clearTime(settlementDate), 1));
		}
		// Retrieve positions
		List<AccountingPosition> results = positionSupplier.apply(command);

		// restore value in case the same command is used again
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(settlementDate);

		return results;
	}


	/**
	 * Get the unsettled leg payments for closed positions (after maturation).
	 * Swaps may have matured and its position closed but the leg payments have not been received.
	 */
	private List<AccountingPosition> getPendingReceivablePositionList(AccountingPositionCommand command) {
		String accountingAccountIdsInList = ArrayUtils.toString(getAccountingAccountIdsCache().getAccountingAccounts(getAccountingAccountIds(command)));
		SqlSelectCommand parentPositionSql = new SqlSelectCommand();
		parentPositionSql.setSelectClause("CASE WHEN IsOpening = 0 THEN ParentTransactionID ELSE AccountingTransactionID END AS AccountingTransactionID");
		parentPositionSql.setFromClause("AccountingTransaction");
		parentPositionSql.setGroupByClause("CASE WHEN IsOpening = 0 THEN ParentTransactionID ELSE AccountingTransactionID END");
		parentPositionSql.addSqlRestriction("AccountingAccountID IN (", accountingAccountIdsInList, ")")
				.addSqlRestriction("IsDeleted = 0")
				.addSqlRestriction("InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM InvestmentSecurity, InvestmentInstrument, InvestmentInstrumentHierarchy WHERE IsIncludeAccrualReceivables = 1)");
		parentPositionSql.setHavingClause("SUM(Quantity) = 0");
		applyWhereClauseConditions(command, parentPositionSql);
		// since this query is retrieving closed positions, this could present performance problems
		if (CollectionUtils.isEmpty(parentPositionSql.getSqlParameterValues())) {
			LogUtils.error(this.getClass(), "Potential performance problems, query should be limited by holding account or security.");
		}

		SqlSelectCommand childReceivablesSql = new SqlSelectCommand();
		accountingAccountIdsInList = ArrayUtils.toString(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL));
		childReceivablesSql.setSelectClause(new StringBuilder("ParentTransactionID, ")
				.append("SUM(Quantity) AS RemainingQuantity, ")
				.append("SUM(PositionCostBasis) AS RemainingCostBasis, ")
				.append("SUM(BaseDebitCredit) AS RemainingBaseDebitCredit")
				.toString());
		childReceivablesSql.setFromClause("AccountingTransaction");
		childReceivablesSql.setGroupByClause("ParentTransactionID");
		childReceivablesSql.addSqlRestriction("AccountingAccountID IN (", accountingAccountIdsInList, ")")
				.addSqlRestriction("IsDeleted = 0");
		childReceivablesSql.setHavingClause("(SUM(Quantity) IS NULL OR SUM(Quantity) = 0) AND NOT (SUM(LocalDebitCredit) = 0 AND SUM(PositionCostBasis) = 0)");
		childReceivablesSql.addSqlRestriction("ParentTransactionID IN (" + parentPositionSql.getSql() + ")");
		AssertUtils.assertEmpty(childReceivablesSql.getSqlParameterValues(), "Must interleave parent and child parameter maps.");
		childReceivablesSql.setSqlParameterValues(parentPositionSql.getSqlParameterValues());
		childReceivablesSql.addDateRestriction(command.getTransactionOrSettlementDate(), command.getTransactionDate() == null ? "SettlementDate < ?" : "TransactionDate < ?");

		return getSqlHandler().executeSelect(childReceivablesSql, createPositionResultSetExtractor());
	}


	/**
	 * Retrieves the pending position list according to the conditions provided in the given command.
	 *
	 * @param command the command specifying the constraints for which pending activity shall be retrieved
	 * @return the list of generated pending positions
	 */
	private List<AccountingPosition> getPendingAccountingPositionList(AccountingPositionCommand command) {
		// Retrieve pending activity and convert to positions
		List<AccountingJournalDetailDefinition> pendingTransactionList = getAccountingPendingActivityHandler().getAccountingPendingTransactionList(command);
		List<AccountingPosition> pendingPositionList = CollectionUtils.getConverted(pendingTransactionList, AccountingPosition::forOpeningTransaction);
		pendingPositionList.forEach(position -> position.setPendingActivity(true));
		return pendingPositionList;
	}


	/**
	 * Returns SqlSelectCommands that retrieves open positions for the specified arguments. Does not use JOINs to other table in order to be as fast as possible (heavily used place
	 * that may result in deadlocks).
	 */
	private SqlSelectCommand getPositionsSelectCommand(AccountingPositionCommand command) {

		SqlSelectCommand sql = new SqlSelectCommand();
		if (AccountingPositionServiceUtils.isRunWithOptionRecompile()) {
			sql.setOptionRecompile(true);
		}
		AccountingAccountIds accountingAccountIds = getAccountingAccountIds(command);
		final String accountingAccountIdsInList = ArrayUtils.toString(getAccountingAccountIdsCache().getAccountingAccounts(accountingAccountIds));

		if (command.getExistingPositionId() != null || command.getExistingPositionIds() != null) {
			String sqlRestrictionAccountingAccountId = "AccountingAccountID IN (" + accountingAccountIdsInList + ")";
			String sqlRestrictionPositionIdSuffix = command.getExistingPositionId() != null ? "= " + command.getExistingPositionId() : "IN (" + ArrayUtils.toString(command.getExistingPositionIds()) + ")";

			// SqlSelectCommands for use in From Clause (will be turned into a single SqlSelectCommand with UNION ALL and SQL from each command
			SqlSelectCommand sqlSelectCommandForFromClause1 = new SqlSelectCommand();
			sqlSelectCommandForFromClause1.setSelectClause("ParentTransactionID AS AccountingTransactionID, Quantity, PositionCostBasis, BaseDebitCredit, LocalDebitCredit");
			sqlSelectCommandForFromClause1.setFromClause("AccountingTransaction");
			sqlSelectCommandForFromClause1.addSqlRestriction(sqlRestrictionAccountingAccountId);
			sqlSelectCommandForFromClause1.addSqlRestriction("IsDeleted = 0 AND IsOpening = 0 AND ParentTransactionID " + sqlRestrictionPositionIdSuffix);
			applyWhereClauseConditions(command, sqlSelectCommandForFromClause1);

			SqlSelectCommand sqlSelectCommandForFromClause2 = new SqlSelectCommand();
			sqlSelectCommandForFromClause2.setSelectClause("AccountingTransactionID, Quantity, PositionCostBasis, BaseDebitCredit, LocalDebitCredit");
			sqlSelectCommandForFromClause2.setFromClause("AccountingTransaction");
			sqlSelectCommandForFromClause2.addSqlRestriction(sqlRestrictionAccountingAccountId);
			sqlSelectCommandForFromClause2.addSqlRestriction("IsDeleted = 0 AND IsOpening = 1 AND AccountingTransactionID " + sqlRestrictionPositionIdSuffix);
			applyWhereClauseConditions(command, sqlSelectCommandForFromClause2);

			SqlSelectCommandUnionAll sqlSelectCommandUnionForFromClause = SqlSelectCommandUnionAll.of(sqlSelectCommandForFromClause1, sqlSelectCommandForFromClause2);
			List<SqlParameterValue> sqlParameterValuesForFromClause = sqlSelectCommandUnionForFromClause.getSqlSelectCommand().getSqlParameterValues();
			String fromClauseSql = "(" + sqlSelectCommandUnionForFromClause.getSql() + "\n) t";

			// Final SQL statement to query positions
			sql.setSelectClause("AccountingTransactionID, SUM(Quantity) AS RemainingQuantity, SUM(PositionCostBasis) AS RemainingCostBasis, SUM(BaseDebitCredit) AS RemainingBaseDebitCredit");
			sql.setFromClause(fromClauseSql);
			sql.setGroupByClause("t.AccountingTransactionID");
			sql.setHavingClause("NOT (SUM(Quantity) = 0 AND SUM(LocalDebitCredit) = 0 AND SUM(PositionCostBasis) = 0)");

			// Handle SqlValueParameters
			if (!CollectionUtils.isEmpty(sqlParameterValuesForFromClause)) {
				if (sql.getSqlParameterValues() == null) {
					sql.setSqlParameterValues(sqlParameterValuesForFromClause);
				}
				else {
					// parameters for FROM claus must go first in list.
					List<SqlParameterValue> sqlParameterValueList = CollectionUtils.combineCollections(sqlParameterValuesForFromClause, sql.getSqlParameterValues());
					sql.setSqlParameterValues(sqlParameterValueList);
				}
			}
		}
		else {
			// NOTE: parts of the query are the way the are in order to preserve backwards compatibility
			// it maybe cleaner to use "ISNULL(ParentTransactionID, AccountingTransactionID)" and "WHEN ParentTransactionID IS NULL"
			// if parent transaction for position was always used to indicate a closing
			// Maybe cleaner to refactor this once dependencies are changed
			sql.setSelectClause(new StringBuilder("CASE WHEN IsOpening = 0 THEN ParentTransactionID ELSE AccountingTransactionID END AS AccountingTransactionID, ")
					.append("SUM(Quantity) AS RemainingQuantity, ")
					.append("SUM(PositionCostBasis) AS RemainingCostBasis, ")
					.append("SUM(BaseDebitCredit) AS RemainingBaseDebitCredit")
					.toString());
			sql.setFromClause("AccountingTransaction");
			sql.setGroupByClause("CASE WHEN IsOpening = 0 THEN ParentTransactionID ELSE AccountingTransactionID END");
			sql.addSqlRestriction("AccountingAccountID IN (", accountingAccountIdsInList, ")")
					.addSqlRestriction("IsDeleted = 0");
			sql.setHavingClause("NOT (SUM(Quantity) = 0 AND SUM(LocalDebitCredit) = 0 AND SUM(PositionCostBasis) = 0)");

			applyWhereClauseConditions(command, sql);
		}

		return sql;
	}


	/**
	 * Converts the settings in the AccountingPositionCommand object into restrictions that are added to the WHERE clause in
	 * the SqlSelectCommand reference passed in as the second parameter.
	 *
	 * @param command the AccountingPositionCommand containing property settings to convert to SQL WHERE clause restrictions
	 * @param sql     the SqlSelectCommand that will have its WHERE clause updated based on the property values in the AccountingPositionCommand
	 */
	private void applyWhereClauseConditions(AccountingPositionCommand command, SqlSelectCommand sql) {
		if (command.getClientInvestmentAccountId() != null) {
			sql.addIntegerRestriction(command.getClientInvestmentAccountId(), "ClientInvestmentAccountID = ?");
		}
		if (command.getClientInvestmentAccountIds() != null) {
			sql.addSqlRestriction("ClientInvestmentAccountID IN (", ArrayUtils.toString(command.getClientInvestmentAccountIds()), ")");
		}
		if (command.getHoldingInvestmentAccountId() != null) {
			sql.addIntegerRestriction(command.getHoldingInvestmentAccountId(), "HoldingInvestmentAccountID = ?");
		}
		if (command.getHoldingInvestmentAccountIds() != null) {
			sql.addSqlRestriction("HoldingInvestmentAccountID IN (", ArrayUtils.toString(command.getHoldingInvestmentAccountIds()), ")");
		}
		if (command.getHoldingInvestmentCompanyId() != null) {
			sql.addIntegerRestriction(command.getHoldingInvestmentCompanyId(), "HoldingInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE IssuingCompanyId = ?)");
		}
		if (command.getClientInvestmentAccountGroupId() != null) {
			sql.addIntegerRestriction(command.getClientInvestmentAccountGroupId(), "ClientInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccountGroupAccount WHERE InvestmentAccountGroupID = ?)");
		}
		if (command.getHoldingInvestmentAccountGroupId() != null) {
			sql.addIntegerRestriction(command.getHoldingInvestmentAccountGroupId(), "HoldingInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccountGroupAccount WHERE InvestmentAccountGroupID = ?)");
		}
		if (command.getInvestmentSecurityId() != null) {
			sql.addIntegerRestriction(command.getInvestmentSecurityId(), "InvestmentSecurityID = ?");
		}
		if (command.getUnderlyingSecurityId() != null) {
			sql.addIntegerRestriction(command.getUnderlyingSecurityId(), "InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s WHERE s.UnderlyingSecurityID = ?)");
		}
		if (command.getInvestmentSecurityIds() != null) {
			sql.addSqlRestriction("InvestmentSecurityID IN (", ArrayUtils.toString(command.getInvestmentSecurityIds()), ")");
		}
		if (command.getInvestmentTypeId() != null) {
			sql.addShortRestriction(command.getInvestmentTypeId(), "InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s INNER JOIN InvestmentInstrument i ON i.InvestmentInstrumentID = s.InvestmentInstrumentID INNER JOIN InvestmentInstrumentHierarchy h ON h.InvestmentInstrumentHierarchyID = i.InvestmentInstrumentHierarchyID WHERE h.InvestmentTypeID = ?)");
		}
		if (command.getInvestmentGroupId() != null || !ArrayUtils.isEmpty(command.getInvestmentGroupIds())) {
			// process single and array of instrument IDs together
			Short[] instrumentIds = ArrayUtils.add(command.getInvestmentGroupIds(), command.getInvestmentGroupId());
			// Stream for building IN clause and appending query parameter values
			Stream<Short> investmentGroupIdStream = ArrayUtils.getStream(instrumentIds);

			// build the instrument IN clause and append each ID as a parameter for each unique instrument
			StringBuilder investmentGroupArgumentSql = new StringBuilder();
			investmentGroupIdStream.distinct()
					.forEach(id -> {
						investmentGroupArgumentSql.append(", ?");
						sql.addSqlParameterValue(SqlParameterValue.ofShort(id));
					});

			if (investmentGroupArgumentSql.length() > 0) {
				// at least one group ID is used, append query clause for already appended group ID parameter(s)
				sql.addSqlRestriction("InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentGroupItemInstrument ii INNER JOIN InvestmentGroupItem igi ON ii.InvestmentGroupItemID = igi.InvestmentGroupItemID INNER JOIN InvestmentSecurity s ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID WHERE igi.InvestmentGroupID ",
						(investmentGroupArgumentSql.length() > 3 ? "IN (" + investmentGroupArgumentSql.substring(2) + "))" // there are multiple IDs; remove leading ", " from argument clause
								: "= ? )")); // single ID; use = instead of IN clause
			}
		}
		if (command.getInvestmentInstrumentId() != null || !ArrayUtils.isEmpty(command.getInvestmentInstrumentIds())) {
			// process single and array of instrument IDs together
			Integer[] instrumentIds = ArrayUtils.add(command.getInvestmentInstrumentIds(), command.getInvestmentInstrumentId());
			// Stream for building IN clause and appending query parameter values
			Stream<Integer> instrumentIdStream = ArrayUtils.getStream(instrumentIds);

			if (command.isIncludeBigInstrumentPositions()) {
				// update the instrument ID stream to include the big instruments
				instrumentIdStream = instrumentIdStream.distinct() // only lookup big instrument for distinct items
						.map(id -> {
							InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(id);
							if (instrument.getBigInstrument() != null) {
								return ArrayUtils.createArray(id, instrument.getBigInstrument().getId());
							}
							// no big instrument, use single
							return ArrayUtils.createArray(id);
						})
						.flatMap(ArrayUtils::getStream);
			}

			// build the instrument IN clause and append each ID as a parameter for each unique instrument
			StringBuilder instrumentArgumentSql = new StringBuilder();
			instrumentIdStream.distinct()
					.forEach(id -> {
						instrumentArgumentSql.append(", ?");
						sql.addSqlParameterValue(SqlParameterValue.ofInteger(id));
					});

			if (instrumentArgumentSql.length() > 0) {
				// at least one instrument ID is used, append query clause for already appended instrument ID parameter(s)
				sql.addSqlRestriction("InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s WHERE s.InvestmentInstrumentID ",
						(instrumentArgumentSql.length() > 3 ? "IN (" + instrumentArgumentSql.substring(2) + ") )" // there are multiple IDs; remove leading ", " from argument clause
								: "= ? )")); // single ID; use = instead of IN clause
			}
		}
		if (!ArrayUtils.isEmpty(command.getInvestmentSecurityGroupIds())) {
			// build the security group IN clause and append each ID as a parameter for each unique group
			StringBuilder securityGroupArgumentSql = new StringBuilder();
			ArrayUtils.getStream(command.getInvestmentSecurityGroupIds()).distinct()
					.forEach(id -> {
						securityGroupArgumentSql.append(", ?");
						sql.addSqlParameterValue(SqlParameterValue.ofShort(id));
					});

			if (securityGroupArgumentSql.length() > 0) {
				// at least one security group ID is used, append query clause for already appended group ID parameter(s)
				sql.addSqlRestriction("InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurityGroupSecurity s WHERE s.InvestmentSecurityGroupID ",
						(securityGroupArgumentSql.length() > 3 ? "IN (" + securityGroupArgumentSql.substring(2) + ") )" // there are multiple IDs; remove leading ", " from argument clause
								: "= ? )")); // single ID; use = instead of IN clause
			}
		}
		if (command.getInstrumentHierarchyId() != null) {
			sql.addShortRestriction(command.getInstrumentHierarchyId(), "InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s INNER JOIN InvestmentInstrument i ON i.InvestmentInstrumentID = s.InvestmentInstrumentID WHERE i.InvestmentInstrumentHierarchyID = ?)");
		}
		sql.addDateRestriction(command.getTransactionOrSettlementDate(), command.getTransactionDate() == null ? "SettlementDate < ?" : "TransactionDate < ?");

		for (SearchRestriction searchRestriction : CollectionUtils.getIterable(command.getRestrictionList())) {
			Object value = searchRestriction.getValue();
			if (value != null) {
				String comparisonString = searchRestriction.getComparison().getComparisonExpression();
				switch (searchRestriction.getField()) {
					case "clientInvestmentAccountId": {
						sql.addIntegerRestriction(Integer.valueOf(value.toString()), "ClientInvestmentAccountID ", comparisonString, " ?");
						break;
					}
					case "holdingInvestmentAccountId": {
						sql.addIntegerRestriction(Integer.valueOf(value.toString()), "HoldingInvestmentAccountID ", comparisonString, " ?");
						break;
					}
					case "investmentTypeId": {
						sql.addShortRestriction(Short.valueOf(value.toString()), "InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s INNER JOIN InvestmentInstrument i ON i.InvestmentInstrumentID = s.InvestmentInstrumentID INNER JOIN InvestmentInstrumentHierarchy h ON h.InvestmentInstrumentHierarchyID = i.InvestmentInstrumentHierarchyID WHERE h.InvestmentTypeID ", comparisonString, " ?)");
						break;
					}
					case "underlyingSecurityId": {
						sql.addIntegerRestriction(Integer.valueOf(value.toString()), "InvestmentSecurityID IN (SELECT s.InvestmentSecurityID FROM InvestmentSecurity s WHERE s.UnderlyingSecurityID ", comparisonString, " ?)");
						break;
					}
					case "investmentSecurityId": {
						sql.addIntegerRestriction(Integer.valueOf(value.toString()), "InvestmentSecurityID ", comparisonString, " ?");
						break;
					}
					default: {
						LogUtils.warn(getClass(), "Encountered unimplemented position search restriction: {" + searchRestriction + "}");
					}
				}
			}
		}
	}


	private AccountingAccountIds getAccountingAccountIds(AccountingPositionCommand command) {
		if (command.getCollateral() != null) {
			return !command.getCollateral() ? AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_COLLATERAL_AND_RECEIVABLE : AccountingAccountIds.POSITION_COLLATERAL_ACCOUNTS_EXCLUDE_RECEIVABLE;
		}
		else if (BooleanUtils.isTrue(command.getExcludeNotOurGLAccounts()) && (BooleanUtils.isTrue(command.getExcludeReceivableGLAccounts()))) {
			return AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS_AND_RECEIVABLE_COLLATERAL;
		}
		else if (BooleanUtils.isTrue(command.getExcludeReceivableGLAccounts())) {
			return AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE;
		}
		else if (BooleanUtils.isTrue(command.getExcludeNotOurGLAccounts())) {
			return AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS;
		}
		return AccountingAccountIds.POSITION_ACCOUNTS;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ResultSetExtractor<List<AccountingPosition>> createPositionResultSetExtractor() {
		return rs -> {
			List<AccountingPosition> result = new ArrayList<>();
			while (rs.next()) {
				AccountingTransaction openingTransaction = getAccountingTransactionService().getAccountingTransaction(rs.getLong(1));
				AccountingPosition position = AccountingPosition.forOpeningTransaction(openingTransaction);

				position.setRemainingQuantity(rs.getBigDecimal(2));
				position.setRemainingCostBasis(rs.getBigDecimal(3));
				position.setRemainingBaseDebitCredit(rs.getBigDecimal(4));

				result.add(position);
			}
			return result;
		};
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPendingActivityHandler getAccountingPendingActivityHandler() {
		return this.accountingPendingActivityHandler;
	}


	public void setAccountingPendingActivityHandler(AccountingPendingActivityHandler accountingPendingActivityHandler) {
		this.accountingPendingActivityHandler = accountingPendingActivityHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
