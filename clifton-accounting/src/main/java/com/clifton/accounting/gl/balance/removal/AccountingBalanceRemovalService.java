package com.clifton.accounting.gl.balance.removal;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * The <code>AccountingBalanceRemovalService</code> is used to remove all positions and assets / liabilities from the account on a given date
 * Most useful for when an account terminates
 *
 * @author manderson
 */
public interface AccountingBalanceRemovalService {


	/**
	 * For a given account and termination date will remove positions (position transfer out of the account) and/or non position assets and liabilities (general journal for distribution)
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public Status processAccountingBalanceRemoval(AccountingBalanceRemovalCommand command);
}
