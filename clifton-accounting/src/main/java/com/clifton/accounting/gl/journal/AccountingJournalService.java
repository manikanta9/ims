package com.clifton.accounting.gl.journal;


import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>AccountingJournalService</code> interface defines methods for working with AccountingJournal objects.
 * <p/>
 * A journal only makes sense when it has details that pass proper validation (balance, etc.).
 * There should be no journal detail specific methods exposed by this service.
 *
 * @author vgomelsky
 */
public interface AccountingJournalService {

	////////////////////////////////////////////////////////////////////////////
	////////        AccountingJournalType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalType getAccountingJournalType(short id);


	public AccountingJournalType getAccountingJournalTypeByName(String journalTypeName);


	public List<AccountingJournalType> getAccountingJournalTypeList();


	/**
	 * Enables/disables the {@link AccountingJournalType} with the provided id for archival.
	 * TODO remove this after historic archival is complete
	 */
	@RequestMapping("accountingJournalTypeArchivingEnable")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void enableAccountingJournalTypeArchiving(short id, boolean enable);

	////////////////////////////////////////////////////////////////////////////
	////////        AccountingJournalStatus Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalStatus getAccountingJournalStatus(short id);


	public AccountingJournalStatus getAccountingJournalStatusByName(String journalStatusName);


	public List<AccountingJournalStatus> getAccountingJournalStatusList();

	////////////////////////////////////////////////////////////////////////////
	////////          AccountingJournal Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns AccountingJournal object with the specified id.
	 * The object is fully populated including corresponding AccountingJournalDetail list.
	 */
	public AccountingJournal getAccountingJournal(long id);


	public List<AccountingJournal> getAccountingJournalList(AccountingJournalSearchForm searchForm);


	/**
	 * Returns fully populated AccountingJournal for the source entity (trade, simple journal, event journal, etc.) specified.
	 *
	 * @param journalSequence if more than one journal can be generated for a single source entity (accrual and reversal for event journals)
	 *                        then this field specifies which journal to return: first one is 1.
	 */
	public AccountingJournal getAccountingJournalBySourceAndSequence(String sourceTable, int sourceId, Integer journalSequence);


	/**
	 * Returns fully populated AccountingJournal for the source entity (trade, simple journal, event journal, etc.) specified.
	 */
	public AccountingJournal getAccountingJournalBySource(String sourceTable, int sourceId);


	/**
	 * Saves the specified AccountingJournal object including its list of AccountingJournalDetail objects.
	 * Saves the journal only if it passes all validation logic.
	 */
	public AccountingJournal saveAccountingJournal(AccountingJournal journal);


	/**
	 * Deletes AccountingJournal object with the specified id.
	 * Also deletes corresponding AccountingJournalDetail objects.
	 * <p/>
	 * Deletion is performed only if the journal hasn't been posted yet.  Otherwise throws a ValidationException.
	 *
	 * @param deleteSubSystemEntity optionally specifies whether sub-system entity should be deleted
	 */
	public void deleteAccountingJournal(long id, boolean deleteSubSystemEntity);


	@DoNotAddRequestMapping
	public void validateAccountingJournal(AccountingJournal journal);


	@DoNotAddRequestMapping
	public AccountingJournal populateJournal(AccountingJournal journal);
}
