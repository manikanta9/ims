package com.clifton.accounting.gl.position.daily.jobs;


import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * rebuilds AccountingPositionDaily table when a historical change was made in the past 2 days for a transaction
 * with a date prior to today.
 * <p/>
 * By default just checks for PostingDate in last two days, however can be configured to check any # of days back.
 *
 * @author Mary Anderson
 */
public class AccountingPositionDailyHistoricalRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private AccountingTransactionService accountingTransactionService;
	private AccountingPositionDailyService accountingPositionDailyService;


	/////////////////////////////////////////////////////////////////////////

	private static final Integer DEFAULT_DAYS_BACK = 1;

	private Integer daysBack;

	private StatusHolderObject<Status> statusHolder;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		// Go through each client account and find if there were historical changes made in the past two days and rebuild client account snapshots since then
		// i.e. Posted > 2 days back
		// and transaction date < today
		// Don't rebuild today, since that will be handled by current build job.
		Date today = DateUtils.clearTime(new Date());
		Integer days = getDaysBack();
		if (days == null) {
			days = DEFAULT_DAYS_BACK;
		}
		Map<Integer, Date> clientDateMap = getAccountingTransactionService().getAccountingTransactionHistoricalChangeMap(days);

		Status status = this.statusHolder.getStatus();
		if (clientDateMap == null || clientDateMap.isEmpty()) {
			status.setMessage("No historical changes found to rebuild AccountingPositionDaily");
			status.setActionPerformed(false);
		}
		else {
			int totalAccounts = 0;
			int errorCount = 0;
			Date endDate = DateUtils.getPreviousWeekday(today);
			AccountingPositionDailyRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setEndSnapshotDate(endDate);
			command.setStatus(status);
			for (Map.Entry<Integer, Date> integerDateEntry : clientDateMap.entrySet()) {
				try {
					Date startDate = integerDateEntry.getValue();
					command.setClientAccountId(integerDateEntry.getKey());
					command.setStartSnapshotDate(startDate);
					command.setSynchronous(true);
					getAccountingPositionDailyService().rebuildAccountingPositionDaily(command);

					totalAccounts++;
					status.setMessage("Accounts: " + clientDateMap.size() + ", Processed: " + totalAccounts + ", Failed: " + errorCount + ", Snapshots processed: " + command.getResultProcessedSnapshots());
				}
				catch (Exception e) {
					errorCount++;
					status.addError("Failed Client Account ID " + integerDateEntry.getKey() + ": " + ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding daily position for " + command.getRunId(), e);
				}
			}
			status.setMessage("Total Accounts Processed: " + totalAccounts + ", Failed Accounts: " + errorCount + ", Total snapshots processed: " + command.getResultProcessedSnapshots());
			status.setActionPerformed(command.getResultProcessedSnapshots() != 0);
		}
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}
}
