package com.clifton.accounting.gl.position;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPosition</code> represents a lot that a client has a position for. It is used within accounting processing.
 * <p>
 * The primary key "id" points to the opening position/lot/AccountingTransaction that may be fully open, partially closed or fully closed.
 * <p>
 * "Opening" prefix refers to fields of the opening lot.
 * "Remaining" prefix refers to remaining amounts (original minus what's been closed).
 * "Closing" prefix refers to closed amounts (Closing = Opening - Remaining).
 */
@NonPersistentObject
public class AccountingPosition extends BaseSimpleEntity<Long> implements AccountingPositionInfo, AccountingTransactionInfo, AccountingBean {

	private AccountingJournalDetailDefinition openingTransaction;

	private BigDecimal remainingQuantity; // remaining balance for currencies
	private BigDecimal remainingCostBasis;
	private BigDecimal remainingBaseDebitCredit; // important for remaining currency cost in base currency (avoids rounding errors)


	/**
	 * Indicates whether this position is generated from transactions that have not yet been posted to the General Ledger.
	 */
	private boolean pendingActivity;

	// this field should not be here
	private String positionGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPosition(AccountingPosition other) {
		// Copy constructor
		this.openingTransaction = other.openingTransaction;
		this.remainingQuantity = other.remainingQuantity;
		this.remainingCostBasis = other.remainingCostBasis;
		this.remainingBaseDebitCredit = other.remainingBaseDebitCredit;
		this.pendingActivity = other.pendingActivity;
		this.positionGroupId = other.positionGroupId;
		// Super properties
		setId(other.getId());
	}


	public AccountingPosition (AccountingJournalDetailDefinition openingTransaction) {
		setId(openingTransaction.getId());
		setOpeningTransaction(openingTransaction);

		setRemainingQuantity(getQuantity());
		setRemainingCostBasis(getPositionCostBasis());
		setRemainingBaseDebitCredit(getBaseDebitCredit());
	}


	/**
	 * Generate a fully-hydrated position from the provided source object.
	 *
	 * @param openingTransaction the source object
	 * @return the fully-hydrated position using properties from the provided source object
	 */
	public static AccountingPosition forOpeningTransaction(AccountingJournalDetailDefinition openingTransaction) {
		return new AccountingPosition(openingTransaction);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isNewBean() {
		if (getId() == null) {
			return true;
		}
		return getId() < 0; // template uses -10, -11, etc. for new beans
	}


	public AccountingJournalDetailDefinition getOpeningTransaction() {
		return this.openingTransaction;
	}


	protected void setOpeningTransaction(AccountingJournalDetailDefinition openingTransaction) {
		this.openingTransaction = openingTransaction;
	}


	@Override
	public Integer getSourceEntityId() {
		return getOpeningTransaction().getFkFieldId();
	}


	@Override
	public boolean isBuy() {
		return MathUtils.isPositive(getQuantityNormalized());
	}


	@Override
	public boolean isCollateral() {
		if (getAccountingAccount() != null) {
			return getAccountingAccount().isCollateral();
		}
		return false;
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		InvestmentSecurity security = getInvestmentSecurity();
		if (security != null && security.getInstrument() != null) {
			return security.getInstrument().getTradingCurrency();
		}
		return null;
	}


	/**
	 * OpeningQuantity value contains LocalDebitCredit for currency positions.
	 * This method will always return null for currency (doesn't support quantity field).
	 */
	public BigDecimal getOpeningQuantityClean() {
		InvestmentSecurity security = getInvestmentSecurity();
		if (security != null && security.isCurrency()) {
			return null;
		}
		return getQuantity();
	}


	/**
	 * RemainingQuantity value contains LocalDebitCredit for currency positions.
	 * This method will always return null for currency (doesn't support quantity field).
	 */
	public BigDecimal getRemainingQuantityClean() {
		InvestmentSecurity security = getInvestmentSecurity();
		if (security != null && security.isCurrency()) {
			return null;
		}
		return getRemainingQuantity();
	}


	/**
	 * Returns positionCostBasis for cash/currencies if quantity field is null.
	 * Returns quantity field otherwise.
	 */
	@Override
	public BigDecimal getQuantityNormalized() {
		return getOpeningTransaction().getQuantityNormalized();
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return getPositionCostBasis();
	}


	public BigDecimal getInitialMarginPerUnit() {
		return InvestmentCalculatorUtils.calculateRequiredCollateral(getInvestmentSecurity(), getHoldingInvestmentAccount(), BigDecimal.ONE, BigDecimal.ONE, false);
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		if (getOpeningTransaction().getExchangeRateToBase() != null) {
			return getOpeningTransaction().getExchangeRateToBase();
		}
		// if transaction date of a closing is before the opening (same settlement date: sell bond and REPO maturity)
		//		if (this.transaction != null) {
		//			return this.transaction.getExchangeRateToBase();
		//		}
		throw new IllegalStateException("Cannot determine opening FX rate for: " + this.toString());
	}


	@Override
	public Long getAccountingTransactionId() {
		return getId();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		if (getInvestmentSecurity() != null) {
			result.append(", Symbol = ");
			result.append(getInvestmentSecurity().getSymbol());
		}
		if (getAccountingAccount() != null) {
			result.append(", GL Account = ");
			result.append(getAccountingAccount().getName());
		}
		result.append(", Cost Basis = ");
		result.append(CoreMathUtils.formatNumberMoney(getRemainingCostBasis()));
		result.append(", Settlement Date = ");
		result.append(DateUtils.fromDateShort(getSettlementDate()));
		result.append('}');
		return result.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getRemainingCostBasis() {
		return this.remainingCostBasis;
	}


	public void setRemainingCostBasis(BigDecimal remainingCostBasis) {
		this.remainingCostBasis = remainingCostBasis;
	}


	public BigDecimal getRemainingBaseDebitCredit() {
		return this.remainingBaseDebitCredit;
	}


	public void setRemainingBaseDebitCredit(BigDecimal remainingBaseDebitCredit) {
		this.remainingBaseDebitCredit = remainingBaseDebitCredit;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return getOpeningTransaction().getClientInvestmentAccount();
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return getOpeningTransaction().getHoldingInvestmentAccount();
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return getOpeningTransaction().getInvestmentSecurity();
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return getOpeningTransaction().getAccountingAccount();
	}


	@Override
	public BigDecimal getQuantity() {
		return getOpeningTransaction().getQuantity();
	}


	@Override
	public BigDecimal getPrice() {
		return getOpeningTransaction().getPrice();
	}


	public BigDecimal getBaseDebitCredit() {
		return getOpeningTransaction().getBaseDebitCredit();
	}


	@Override
	public BigDecimal getPositionCostBasis() {
		return getOpeningTransaction().getPositionCostBasis();
	}


	@Override
	public Date getTransactionDate() {
		return getOpeningTransaction().getTransactionDate();
	}


	@Override
	public Date getSettlementDate() {
		return getOpeningTransaction().getSettlementDate();
	}


	public Date getOriginalTransactionDate() {
		return getOpeningTransaction().getOriginalTransactionDate();
	}


	@Override
	public BusinessCompany getExecutingCompany() {
		return getOpeningTransaction().getExecutingCompany();
	}


	public boolean isPendingActivity() {
		return this.pendingActivity;
	}


	public void setPendingActivity(boolean pendingActivity) {
		this.pendingActivity = pendingActivity;
	}


	@Override
	public String getPositionGroupId() {
		return this.positionGroupId;
	}


	@Override
	public void setPositionGroupId(String positionGroupId) {
		this.positionGroupId = positionGroupId;
	}
}
