package com.clifton.accounting.gl.position;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPositionService</code> interface defines methods for working with accounting positions (open GL positions).
 *
 * @author vgomelsky
 */
public interface AccountingPositionService {

	/**
	 * Returns a list of open positions/lots using the specified command.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	@RequestMapping("accountingPositionListUsingCommand") // Override request mapping to string "Find" suffix generated due to search form parameter
	public List<AccountingPosition> getAccountingPositionListUsingCommand(AccountingPositionCommand command);


	/**
	 * Returns a list of open positions/lots on the specified date for the specified accounts and security.
	 * Either Integer parameter can be null to skip that filter.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPosition> getAccountingPositionList(Integer clientInvestmentAccountId, Integer holdingInvestmentAccountId, Integer investmentSecurityId, Date transactionDate);


	/**
	 * Returns a list of open positions/lots on the specified date for the specified security for ALL client accounts.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPosition> getAccountingPositionListForSecurity(int investmentSecurityId, Date transactionDate);


	/**
	 * Returns a list of open positions/lots on the specified date for securities for the specified instrument
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPosition> getAccountingPositionListForInstrument(int investmentInstrumentId, Date transactionDate, boolean includeBigInstrumentPositions);


	/**
	 * Returns a list of open positions/lots on the specified date for the specified client account and ALL securities.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPosition> getAccountingPositionListForClientAccount(int clientInvestmentAccountId, Date transactionDate);

	///////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Balance Methods          /////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an aggregated balance of positions for the provided command. Positions are aggregated using unique keys of accounting account, client and holding accounts, and security.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<AccountingPositionBalance> getAccountingPositionBalanceList(AccountingPositionCommand accountingPositionCommand);

	////////////////////////////////////////////////////////////////////////////
	/////////            Accounting Position Calculated Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Using an existing position (opening transaction) returns the calculated market value
	 * with the option to override quantity.
	 * <p>
	 * Currently used by Position Transfer window with the ability to dynamically calculate/display
	 * market value of an existing position with a different quantity
	 */
	@ResponseBody
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public BigDecimal getAccountingPositionMarketValueForExistingPosition(long existingPositionId, Date date, BigDecimal overrideQuantity);
}
