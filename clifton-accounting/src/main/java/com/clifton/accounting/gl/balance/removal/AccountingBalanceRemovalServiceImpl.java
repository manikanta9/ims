package com.clifton.accounting.gl.balance.removal;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentObjectInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class AccountingBalanceRemovalServiceImpl implements AccountingBalanceRemovalService {

	private AccountingAccountService accountingAccountService;

	private AccountingBalanceService accountingBalanceService;

	private AccountingBookingService<?> accountingBookingService;

	private AccountingJournalService accountingJournalService;

	private AccountingPositionService accountingPositionService;

	private AccountingPositionTransferService accountingPositionTransferService;

	private AccountingPostingService accountingPostingService;

	private AccountingTransactionService accountingTransactionService;

	private InvestmentAccountService investmentAccountService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Transactional
	@Override
	public Status processAccountingBalanceRemoval(AccountingBalanceRemovalCommand command) {
		InvestmentAccount clientAccount = command.getClientInvestmentAccountId() == null ? null : getInvestmentAccountService().getInvestmentAccount(command.getClientInvestmentAccountId());
		if (clientAccount != null) {
			ValidationUtils.assertTrue(clientAccount.getType().isOurAccount(), "Selected client account [" + clientAccount.getLabel() + "] is not a client account.");
			ValidationUtils.assertTrue(clientAccount.isActive(), "Client Account [" + clientAccount.getLabel() + "] must currently be active in order to book and post gl transactions to it.");
		}
		InvestmentAccount holdingAccount = command.getHoldingInvestmentAccountId() == null ? null : getInvestmentAccountService().getInvestmentAccount(command.getHoldingInvestmentAccountId());
		if (holdingAccount != null) {
			ValidationUtils.assertFalse(holdingAccount.getType().isOurAccount(), "Selected holding account [" + holdingAccount.getLabel() + "] is a client account.");
			ValidationUtils.assertTrue(holdingAccount.isActive(), "Holding Account [" + holdingAccount.getLabel() + "] must currently be active in order to book and post gl transactions to it.");
		}
		ValidationUtils.assertFalse(clientAccount == null && holdingAccount == null, "No accounts selected");
		ValidationUtils.assertNotNull(command.getRemovalDate(), "Removal Date is required.");
		ValidationUtils.assertFalse(DateUtils.isDateAfter(command.getRemovalDate(), new Date()), "Removal Date cannot be in the future.");
		ValidationUtils.assertTrue(command.isDistributePositions() || command.isDistributeNonPositionAssetsAndLiabilities(), "You must select at least one distribution option.");

		Status status = Status.ofEmptyMessage();
		status.setActionPerformed(false);

		FxRateLookupCache cache = new FxRateLookupCache(getMarketDataExchangeRatesApiService());
		removePositionsFromAccounts(command, clientAccount, holdingAccount, cache, status);
		removeNonPositionAssetsAndLiabilitiesFromAccounts(command, clientAccount, holdingAccount, cache, status);

		status.setMessage("Processing Completed.");
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void removePositionsFromAccounts(AccountingBalanceRemovalCommand command, InvestmentAccount clientAccount, InvestmentAccount holdingAccount, FxRateLookupCache cache, Status status) {
		if (command.isDistributePositions()) {
			// Get all positions as of the terminate date
			List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionList(clientAccount != null ? clientAccount.getId() : null, holdingAccount != null ? holdingAccount.getId() : null, null, command.getRemovalDate());

			if (CollectionUtils.isEmpty(positionList)) {
				status.addSkipped("No positions in the selected account(s) to remove");
				return;
			}

			// Transfers must be unique by client account - holding account - hierarchy and CCY
			Map<String, AccountingPositionTransfer> transferMap = new HashMap<>();
			AccountingPositionTransferType transferType = getAccountingPositionTransferService().getAccountingPositionTransferTypeByName(AccountingPositionTransferType.FROM_TRANSFER);


			for (AccountingPosition position : positionList) {
				Date transferDate = command.getRemovalDate();
				String key = getTransferKeyForPosition(position, transferDate);
				AccountingPositionTransfer transfer = transferMap.get(key);
				if (transfer == null) {
					transfer = new AccountingPositionTransfer();
					transfer.setTransactionDate(transferDate);
					transfer.setNote(command.getRemovalNote());
					transfer.setFromClientInvestmentAccount(position.getClientInvestmentAccount());
					transfer.setFromHoldingInvestmentAccount(position.getHoldingInvestmentAccount());
					transfer.setSettlementDate(transferDate);
					transfer.setType(transferType);
					transfer.setDetailList(new ArrayList<>());
				}

				AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
				detail.setSecurity(position.getInvestmentSecurity());
				detail.setExistingPosition(getAccountingTransactionService().getAccountingTransaction(position.getAccountingTransactionId()));
				detail.setQuantity(position.getRemainingQuantity());
				detail.setOriginalPositionOpenDate(position.getOriginalTransactionDate());
				detail.setCostPrice(position.getOpeningTransaction().getPrice());
				detail.setTransferPrice(getPriceForSecurity(position.getInvestmentSecurity(), transferDate, command.isUseFlexiblePriceLookup()));
				detail.setPositionCostBasis(position.getPositionCostBasis());

				detail.setExchangeRateToBase(getExchangeRateToBaseForInvestmentObject(position, transferDate, command.isUseFlexiblePriceLookup(), cache));
				transfer.getDetailList().add(detail);
				transferMap.put(key, transfer);
			}

			for (AccountingPositionTransfer transfer : transferMap.values()) {
				getAccountingPositionTransferService().saveAccountingPositionTransfer(transfer);
				getAccountingBookingService().bookAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, transfer.getId(), true);
			}
			status.setActionPerformed(true);
			status.addMessage("Booked and posted " + CollectionUtils.getSize(transferMap.values()) + " transfer(s) for " + CollectionUtils.getSize(positionList) + " position(s).");
		}
	}


	private String getTransferKeyForPosition(AccountingPosition position, Date transferDate) {
		// Transfers must be unique by client account - holding account - hierarchy and CCY
		return position.getClientInvestmentAccount().getId() + "_" + position.getHoldingInvestmentAccount() + "_" + position.getInvestmentSecurity().getInstrument().getHierarchy().getId() + "_" + position.getInvestmentSecurity().getInstrument().getTradingCurrency().getId() + "_" + DateUtils.fromDateShort(transferDate);
	}


	private BigDecimal getPriceForSecurity(InvestmentSecurity security, Date date, boolean flexible) {
		if (flexible) {
			return getMarketDataRetriever().getPriceFlexible(security, date, true);
		}
		return getMarketDataRetriever().getPrice(security, date, true, null);
	}


	private BigDecimal getExchangeRateToBaseForInvestmentObject(InvestmentObjectInfo investmentObject, Date date, boolean flexible, FxRateLookupCache cache) {
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(investmentObject);
		String toCurrency = InvestmentUtils.getHoldingAccountBaseCurrencySymbol(investmentObject);
		return cache.getExchangeRate(investmentObject.fxSourceCompany(), investmentObject.fxSourceCompanyOverridden(), fromCurrency, toCurrency, date, flexible);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void removeNonPositionAssetsAndLiabilitiesFromAccounts(AccountingBalanceRemovalCommand command, InvestmentAccount clientAccount, InvestmentAccount holdingAccount, FxRateLookupCache cache, Status status) {
		if (command.isDistributeNonPositionAssetsAndLiabilities()) {
			AccountingBalanceSearchForm accountingBalanceSearchForm = new AccountingBalanceSearchForm();
			accountingBalanceSearchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
			accountingBalanceSearchForm.setPositionAccountingAccount(false);
			accountingBalanceSearchForm.setTransactionDate(command.getRemovalDate());
			if (clientAccount != null) {
				accountingBalanceSearchForm.setClientInvestmentAccountId(clientAccount.getId());
			}
			if (holdingAccount != null) {
				accountingBalanceSearchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
			}

			List<AccountingBalance> accountingBalanceList = getAccountingBalanceService().getAccountingBalanceList(accountingBalanceSearchForm);
			if (CollectionUtils.isEmpty(accountingBalanceList)) {
				status.addSkipped("No non-position assets or liabilities in the selected account(s) to remove");
				return;
			}

			AccountingAccount distributionAccountingAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EQUITY_DISTRIBUTION);

			AccountingJournal journal = new AccountingJournal();
			journal.setJournalType(getAccountingJournalService().getAccountingJournalTypeByName(AccountingJournalType.GENERAL_JOURNAL));
			journal.setDescription(command.getRemovalNote());

			for (AccountingBalance accountingBalance : accountingBalanceList) {
				// Journal To Close Out Balance
				AccountingJournalDetail journalDetail = populateAccountingJournalDetail(accountingBalance, journal, command.getRemovalDate());
				journalDetail.setAccountingAccount(accountingBalance.getAccountingAccount());
				journalDetail.setLocalDebitCredit(MathUtils.negate(accountingBalance.getLocalAmount()));
				journalDetail.setExchangeRateToBase(getExchangeRateToBaseForInvestmentObject(accountingBalance, command.getRemovalDate(), false, cache));
				journalDetail.setBaseDebitCredit(MathUtils.multiply(journalDetail.getLocalDebitCredit(), journalDetail.getExchangeRateToBase()));

				// Off setting Journal
				AccountingJournalDetail offsettingJournalDetail = populateAccountingJournalDetail(accountingBalance, journal, command.getRemovalDate());
				offsettingJournalDetail.setAccountingAccount(distributionAccountingAccount);
				offsettingJournalDetail.setLocalDebitCredit(MathUtils.negate(journalDetail.getLocalDebitCredit()));
				offsettingJournalDetail.setExchangeRateToBase(journalDetail.getExchangeRateToBase());
				offsettingJournalDetail.setBaseDebitCredit(MathUtils.negate(journalDetail.getBaseDebitCredit()));
			}

			journal = getAccountingJournalService().saveAccountingJournal(journal);
			getAccountingPostingService().postAccountingJournal(journal.getId());
			status.setActionPerformed(true);
			status.addMessage("Removed all non-position assets and liabilities [" + StringUtils.collectionToCommaDelimitedString(accountingBalanceList, accountingBalance -> accountingBalance.getAccountingAccount().getName()) + "] from the accounts.");
		}
	}


	private AccountingJournalDetail populateAccountingJournalDetail(AccountingBalance accountingBalance, AccountingJournal journal, Date transactionDate) {
		AccountingJournalDetail accountingJournalDetail = new AccountingJournalDetail();
		accountingJournalDetail.setOpening(true);
		accountingJournalDetail.setClientInvestmentAccount(accountingBalance.getClientInvestmentAccount());
		accountingJournalDetail.setHoldingInvestmentAccount(accountingBalance.getHoldingInvestmentAccount());
		accountingJournalDetail.setInvestmentSecurity(accountingBalance.getInvestmentSecurity());
		accountingJournalDetail.setJournal(journal);
		accountingJournalDetail.setOriginalTransactionDate(transactionDate);
		accountingJournalDetail.setTransactionDate(transactionDate);
		accountingJournalDetail.setSettlementDate(transactionDate);
		accountingJournalDetail.setPositionCostBasis(BigDecimal.ZERO);
		journal.addJournalDetail(accountingJournalDetail);
		return accountingJournalDetail;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingBookingService<?> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<?> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
