package com.clifton.accounting.gl.position.closing;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * @author mwacker
 */
@Service
public class AccountingPositionClosingServiceImpl implements AccountingPositionClosingService {

	private AccountingPositionHandler accountingPositionHandler;
	private AccountingPositionService accountingPositionService;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPositionClosing> getAccountingPositionClosingLotList(AccountingPositionClosingCommand command) {
		command.setExcludeReceivableGLAccounts(true);

		List<AccountingPositionClosing> result = new PagingArrayList<>();
		for (AccountingPositionClosingSecurityQuantity securityQuantity : CollectionUtils.getIterable(command.getSecurityQuantityList())) {
			List<AccountingPosition> positionList = getAccountingPositionListForSecurity(command, securityQuantity.getInvestmentSecurityId());

			BigDecimal total = BigDecimal.ZERO;
			BigDecimal targetTotal = getTargetTotal(command, securityQuantity);
			for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
				boolean fullyFilled = false;

				AccountingPositionClosing positionClosing = new AccountingPositionClosing(position);
				positionClosing.setClosingPrice(position.getPrice());
				AccountingPositionClosingTransaction currentSelectedPosition = !CollectionUtils.isEmpty(command.getSelectedPositionList()) ? getSelectedPosition(position, command.getSelectedPositionList()) : null;
				if (currentSelectedPosition != null && isFullPositionSelected(position, currentSelectedPosition)) {
					positionClosing.setClosingPrice(currentSelectedPosition.getClosingPrice());
				}
				else {
					// if there is a partial selection then adjust the target amount
					if (currentSelectedPosition != null) {
						targetTotal = MathUtils.add(currentSelectedPosition.getClosingQuantity(), targetTotal);
					}

					if (!MathUtils.isNullOrZero(targetTotal)) {
						BigDecimal lastTotal = total;
						total = MathUtils.add(position.getRemainingQuantity(), total);
						if (currentSelectedPosition == null) {
							/*
							 * Ensure the position quantity and target close/transfer quantity are the same sign.
							 * Ignore this check if there was a provided selection as the opposite sign will reduce
							 * or increase the close/transfer quantity.
							 */
							ValidationUtils.assertTrue(MathUtils.isSameSignum(total, targetTotal), "Quantity to transfer or close: " + targetTotal + " must have the same sign as the identified position quantity: " + total);
						}
						if (MathUtils.isGreaterThanOrEqual(MathUtils.abs(total), MathUtils.abs(targetTotal))) {
							positionClosing.setRemainingQuantity(MathUtils.subtract(targetTotal, lastTotal));
							updateRemainingCostBasis(positionClosing, command.getTransactionOrSettlementDate());
							fullyFilled = true;
						}
					}
				}
				populateMarketValue(positionClosing, command.getTransactionOrSettlementDate());
				result.add(positionClosing);
				if (fullyFilled) {
					break;
				}
			}
		}
		return result;
	}


	@Override
	public BigDecimal getAccountingPositionClosingLotMarketValue(AccountingPositionClosingCommand command) {
		BigDecimal currentValue = CoreMathUtils.sumProperty(command.getSelectedPositionList(), AccountingPositionClosingTransaction::getMarketValue);
		List<AccountingPositionClosing> closingLotList = getAccountingPositionClosingLotList(command);
		// One can select closing lots more than once. To calculate latest closing batch, we need to subtract lots that were previously selected from all selected lots.
		return MathUtils.subtract(CoreMathUtils.sumProperty(closingLotList, AccountingPositionClosing::getMarketValue), currentValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private BigDecimal getTargetTotal(AccountingPositionClosingCommand command, AccountingPositionClosingSecurityQuantity securityQuantity) {
		if (MathUtils.isLessThan(securityQuantity.getQuantity(), BigDecimal.ZERO)) {
			List<AccountingPositionClosingTransaction> closingTransactions = BeanUtils.filter(command.getSelectedPositionList(), AccountingPositionClosingTransaction::getInvestmentSecurityId, securityQuantity.getInvestmentSecurityId());
			if (command.getSelectedPositionList() != null) {
				command.getSelectedPositionList().removeAll(closingTransactions);
			}
			return MathUtils.add(securityQuantity.getQuantity(), CoreMathUtils.sumProperty(closingTransactions, AccountingPositionClosingTransaction::getClosingQuantity));
		}
		return securityQuantity.getQuantity();
	}


	private boolean isFullPositionSelected(AccountingPosition position, AccountingPositionClosingTransaction selectedPosition) {
		return MathUtils.isEqual(position.getRemainingQuantity(), selectedPosition.getClosingQuantity());
	}


	private AccountingPositionClosingTransaction getSelectedPosition(AccountingPosition position, List<AccountingPositionClosingTransaction> selectedPositions) {
		Integer index = CoreCollectionUtils.getIndexOfFirstPropertyMatch(selectedPositions, "transactionId", position.getId());
		if (index == null) {
			return null;
		}
		return CollectionUtils.getObjectAtIndex(selectedPositions, index);
	}


	private void updateRemainingCostBasis(AccountingPositionClosing closingPosition, Date transactionDate) {
		if (MathUtils.isEqual(closingPosition.getQuantity(), closingPosition.getRemainingQuantity())) {
			closingPosition.setRemainingCostBasis(closingPosition.getRemainingCostBasis());
		}
		else {
			BigDecimal positionCostBasis = getInvestmentCalculator().calculateNotional(closingPosition.getInvestmentSecurity(), closingPosition.getPrice(), closingPosition.getRemainingQuantity(), transactionDate);

			// if we have a rounding error on the
			if (MathUtils.isLessThanOrEqual(positionCostBasis.abs().subtract(closingPosition.getRemainingCostBasis().abs()).abs(), new BigDecimal("0.01"))) {
				positionCostBasis = closingPosition.getRemainingCostBasis();
			}
			closingPosition.setRemainingCostBasis(positionCostBasis);
		}
	}


	private void populateMarketValue(AccountingPositionClosing closingPosition, Date transactionDate) {
		BigDecimal marketValue = getAccountingPositionHandler().getAccountingPositionMarketValue(closingPosition, transactionDate);
		closingPosition.setMarketValue(marketValue);
	}


	private List<AccountingPosition> getAccountingPositionListForSecurity(AccountingPositionClosingCommand command, Integer investmentSecurity) {
		command.setInvestmentSecurityId(investmentSecurity);
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		return filterCloseOnMaturityPositions(positionList);
	}


	/**
	 * Returns the provided position list if the security is not 'Close on Maturity.' If the security of the position list is 'Close on Maturity,' a subset of net positions will be
	 * returned.
	 * <p>
	 * Close on Maturity (e.g. LME and Currency Forward) security positions stay open until the security matures even if the position is of zero quantity.
	 */
	private List<AccountingPosition> filterCloseOnMaturityPositions(List<AccountingPosition> positionList) {
		AccountingPosition firstPosition = CollectionUtils.getFirstElement(positionList);
		if (firstPosition == null || !InvestmentUtils.isCloseOnMaturityOnly(firstPosition.getInvestmentSecurity())) {
			// empty position list or non-LME security; no filtering needed
			return positionList;
		}

		List<AccountingPosition> longPositions = new ArrayList<>();
		List<AccountingPosition> shortPositions = new ArrayList<>();
		BigDecimal total = BigDecimal.ZERO;
		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			/*
			 * placeholder for remaining quantity since it can be adjusted during processing
			 * total must be calculated at the end so our current position gets applied correctly
			 */
			BigDecimal totalPositionAdjustment = position.getRemainingQuantity();

			if (MathUtils.isLessThan(position.getRemainingQuantity(), BigDecimal.ZERO)) {
				// short
				if (MathUtils.isLessThanOrEqual(total, BigDecimal.ZERO) || processOffsettingPositionListWithOpposingPosition(longPositions, position, true)) {
					// The net position is short and the current short position has remaining quantity
					shortPositions.add(position);
				}
			}
			else {
				// long
				if (MathUtils.isGreaterThanOrEqual(total, BigDecimal.ZERO) || processOffsettingPositionListWithOpposingPosition(shortPositions, position, false)) {
					// The net position is long and the current long position has remaining quantity
					longPositions.add(position);
				}
			}

			total = MathUtils.add(total, totalPositionAdjustment);
		}
		// merge lists
		longPositions.addAll(shortPositions);
		BeanUtils.sortWithFunctions(longPositions, CollectionUtils.createList(AccountingPosition::getTransactionDate, AccountingPosition::getPrice), CollectionUtils.createList(true, true));
		return longPositions;
	}


	/**
	 * Processes the provided opposing position against the provided list of offsetting positions. This method is a helping method for determining net positions for a 'Close on
	 * Maturity' security.
	 *
	 * @param positionList     the list of short positions to process the opposing long position against, or long positions with opposing short position
	 * @param opposingPosition the short/long position to process against the offsetting list
	 * @param opposingShort    true if the opposing position is short, false if long
	 * @return true if the processed offsetting position still has a remaining quantity for future net position processing, false if the position has no more remaining quantity
	 */
	private boolean processOffsettingPositionListWithOpposingPosition(List<AccountingPosition> positionList, AccountingPosition opposingPosition, boolean opposingShort) {
		Iterator<AccountingPosition> positionIterator = positionList.iterator();
		BigDecimal quantityToClose = opposingPosition.getRemainingQuantity();
		while (positionIterator.hasNext() && (opposingShort ? MathUtils.isLessThan(quantityToClose, BigDecimal.ZERO) : MathUtils.isGreaterThan(quantityToClose, BigDecimal.ZERO))) {
			AccountingPosition position = positionIterator.next();
			if (MathUtils.isGreaterThan(MathUtils.abs(position.getRemainingQuantity()), MathUtils.abs(quantityToClose))) {
				position.setRemainingQuantity(MathUtils.add(position.getRemainingQuantity(), quantityToClose));
				quantityToClose = BigDecimal.ZERO;
			}
			else {
				quantityToClose = MathUtils.add(position.getRemainingQuantity(), quantityToClose);
				positionIterator.remove();
			}
		}
		// update state of last added position
		if (MathUtils.isEqual(quantityToClose, BigDecimal.ZERO)) {
			return false;
		}
		opposingPosition.setRemainingQuantity(quantityToClose);
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
