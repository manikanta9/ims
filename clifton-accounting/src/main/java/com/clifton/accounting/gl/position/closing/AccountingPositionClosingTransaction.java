package com.clifton.accounting.gl.position.closing;

import java.math.BigDecimal;


/**
 * Holds the closing transaction data needed to prevent duplicate lot selection.
 *
 * @author mwacker
 */
public class AccountingPositionClosingTransaction {

	private long transactionId;
	private int investmentSecurityId;
	private BigDecimal closingQuantity;
	private BigDecimal closingPrice;
	private BigDecimal marketValue;


	public long getTransactionId() {
		return this.transactionId;
	}


	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}


	public int getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(int investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public BigDecimal getClosingQuantity() {
		return this.closingQuantity;
	}


	public void setClosingQuantity(BigDecimal closingQuantity) {
		this.closingQuantity = closingQuantity;
	}


	public BigDecimal getClosingPrice() {
		return this.closingPrice;
	}


	public void setClosingPrice(BigDecimal closingPrice) {
		this.closingPrice = closingPrice;
	}


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}
}



