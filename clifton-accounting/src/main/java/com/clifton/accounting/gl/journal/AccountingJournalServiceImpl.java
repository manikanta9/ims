package com.clifton.accounting.gl.journal;


import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl.AccountingAccountIds;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.system.schema.SystemSchemaService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;


/**
 * The <code>AccountingJournalServiceImpl</code> class provides basic implementation for the AccountingJournalService interface.
 *
 * @author vgomelsky
 */
@Service
public class AccountingJournalServiceImpl implements AccountingJournalService {

	private AdvancedUpdatableDAO<AccountingJournal, Criteria> accountingJournalDAO;
	private UpdatableDAO<AccountingJournalDetail> accountingJournalDetailDAO;
	private ReadOnlyDAO<AccountingJournalStatus> accountingJournalStatusDAO;
	private UpdatableDAO<AccountingJournalType> accountingJournalTypeDAO; // TODO set back to ReadOnlyDAO when historic archiving is complete.

	@SuppressWarnings("rawtypes")
	private AccountingBookingService accountingBookingService;
	private AccountingPeriodService accountingPeriodService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private SystemSchemaService systemSchemaService;

	private AccountingAccountIdsCache accountingAccountIdsCache;
	private DaoNamedEntityCache<AccountingJournalStatus> accountingJournalStatusCache;
	private DaoNamedEntityCache<AccountingJournalType> accountingJournalTypeCache;


	////////////////////////////////////////////////////////////////////////////
	////////        AccountingJournalType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingJournalType getAccountingJournalType(short id) {
		return getAccountingJournalTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingJournalType getAccountingJournalTypeByName(String journalTypeName) {
		return getAccountingJournalTypeCache().getBeanForKeyValueStrict(getAccountingJournalTypeDAO(), journalTypeName);
	}


	@Override
	public List<AccountingJournalType> getAccountingJournalTypeList() {
		return getAccountingJournalTypeDAO().findAll();
	}


	@Override    // TODO remove this after historic archival is complete
	public void enableAccountingJournalTypeArchiving(short id, boolean enable) {
		AccountingJournalType journalType = getAccountingJournalType(id);
		if (journalType != null && journalType.isArchiveAllowed() != enable) {
			journalType.setArchiveAllowed(enable);
			getAccountingJournalTypeDAO().save(journalType);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////        AccountingJournalStatus Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingJournalStatus getAccountingJournalStatus(short id) {
		return getAccountingJournalStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public AccountingJournalStatus getAccountingJournalStatusByName(String journalStatusName) {
		return getAccountingJournalStatusCache().getBeanForKeyValueStrict(getAccountingJournalStatusDAO(), journalStatusName);
	}


	@Override
	public List<AccountingJournalStatus> getAccountingJournalStatusList() {
		return getAccountingJournalStatusDAO().findAll();
	}

	////////////////////////////////////////////////////////////////////////////
	////////          AccountingJournal Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingJournal getAccountingJournal(long id) {
		AccountingJournal journal = getAccountingJournalDAO().findByPrimaryKey(id);
		return populateJournal(journal);
	}


	@Override
	public AccountingJournal getAccountingJournalBySource(String sourceTable, int sourceId) {
		return getAccountingJournalBySourceAndSequence(sourceTable, sourceId, null);
	}


	@Override
	@Transactional(readOnly = true)
	public AccountingJournal getAccountingJournalBySourceAndSequence(String sourceTable, int sourceId, Integer journalSequence) {
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setSystemTableName(sourceTable);
		searchForm.setFkFieldId(sourceId);
		searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
		searchForm.setOrderBy("id:asc");
		List<AccountingJournal> journalList = getAccountingJournalList(searchForm);

		int size = CollectionUtils.getSize(journalList);
		if (journalSequence == null && size > 1) {
			throw new IllegalStateException("journalSequence must be specified because there are " + size + " journals for sourceTable = " + sourceTable + " and sourceId = " + sourceId);
		}

		int sequence = (journalSequence == null) ? 1 : journalSequence;
		ValidationUtils.assertFalse(sequence < 1, "Illegal argument: journalSequence = " + journalSequence + " cannot be less than 1.");
		if (size < sequence) {
			return null;
		}

		AccountingJournal journal = journalList.get(sequence - 1);
		return populateJournal(journal);
	}


	@Override
	public List<AccountingJournal> getAccountingJournalList(AccountingJournalSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getSystemTableName())) {
			// avoid extra join
			searchForm.setSystemTableId(getSystemSchemaService().getSystemTableByName(searchForm.getSystemTableName()).getId());
			searchForm.setSystemTableName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getJournalStatusName())) {
			searchForm.setJournalStatusId(getAccountingJournalStatusByName(searchForm.getJournalStatusName()).getId());
			searchForm.setJournalStatusName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getExcludeJournalStatusName())) {
			searchForm.setExcludeJournalStatusId(getAccountingJournalStatusByName(searchForm.getExcludeJournalStatusName()).getId());
			searchForm.setExcludeJournalStatusName(null);
		}
		return getAccountingJournalDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public AccountingJournal saveAccountingJournal(AccountingJournal journal) {
		List<AccountingJournalDetail> oldDetails = null;
		if (journal.isNewBean()) {
			journal.setJournalStatus(getAccountingJournalStatusByName(AccountingJournalStatus.STATUS_BOOKED));
		}
		else {
			// get original journal and make sure it's not already posted
			AccountingJournal oldJournal = getAccountingJournal(journal.getId());
			ValidationUtils.assertNull(oldJournal.getPostingDate(), "Cannot modify a journal that has already been posted to GL.", "postingDate");

			if (oldJournal.getJournalType().isSubsystemJournal()) {
				throw new ValidationException("Sub-system journals cannot be modified.");
			}
			oldDetails = (List<AccountingJournalDetail>) oldJournal.getJournalDetailList();
		}

		// don't allow setting posting date
		ValidationUtils.assertNull(journal.getPostingDate(), "Cannot manually change posting date of a Journal. It can only be set by GL posting process.", "postingDate");

		validateAccountingJournal(journal);

		// sort details so that children are after corresponding parents
		List<AccountingJournalDetailDefinition> detailList = (List<AccountingJournalDetailDefinition>) journal.getJournalDetailList();
		int count = CollectionUtils.getSize(detailList);
		for (int i = 0; i < count; i++) {
			AccountingJournalDetailDefinition detail = detailList.get(i);
			boolean swap = false;
			for (int j = i + 1; j < count; j++) {
				AccountingJournalDetailDefinition next = detailList.get(j);
				if (next.equals(detail.getParentDefinition())) {
					if (next.getParentDefinition() != null && next.getParentDefinition().equals(detail)) {
						throw new IllegalStateException("Circular dependency found between " + next + " and " + detail);
					}
					// swap
					detailList.set(j, detail);
					detailList.set(i, next);
					swap = true;
					break;
				}
			}
			if (swap) {
				// start all over in case there are dependencies more than one level deep
				i = 0;
			}
		}

		List<AccountingJournalDetail> newDetailList = (List<AccountingJournalDetail>) journal.getJournalDetailList();
		journal = getAccountingJournalDAO().save(journal);
		getAccountingJournalDetailDAO().saveList(newDetailList, oldDetails);
		journal.setJournalDetailList(newDetailList);
		return journal;
	}


	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public void deleteAccountingJournal(long id, boolean deleteSubSystemEntity) {
		AccountingJournal journal = getAccountingJournal(id);
		ValidationUtils.assertNull(journal.getPostingDate(), "Cannot delete a journal that has already been posted to GL.", "postingDate");

		// for sub-system journals, need to mark them as unbooked
		if (journal.getJournalType().isSubsystemPresent()) {
			getAccountingBookingService().markAccountingSubSystemJournalUnbooked(journal, deleteSubSystemEntity);
		}

		// delete journal details first and then the journal
		getAccountingJournalDetailDAO().deleteList((List<AccountingJournalDetail>) journal.getJournalDetailList());
		getAccountingJournalDAO().delete(journal);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates detailList for the specified journal: either from journal details from from GL and returns populated journal.
	 */
	@Override
	public AccountingJournal populateJournal(AccountingJournal journal) {
		if (journal != null) {
			// posted journal details are stored in GL only: AccountingTransaction
			if (journal.getPostingDate() == null) {
				journal.setJournalDetailList(getAccountingJournalDetailDAO().findByField("journal.id", journal.getId()));
			}
			else {
				journal.setJournalDetailList(getAccountingTransactionService().getAccountingTransactionListByJournal(journal.getId()));
			}
		}
		return journal;
	}


	@Override
	public void validateAccountingJournal(AccountingJournal journal) {
		ValidationUtils.assertNotNull(journal, "The journal cannot be null.");
		ValidationUtils.assertNotNull(journal.getJournalType(), "The journal type cannot be null.");
		ValidationUtils.assertNotEmpty(journal.getJournalDetailList(), "The journal must have at least one detail item.");

		// calculate and verify sums of (Debit - Credit) by client account (multiple client accounts can belong to the same journal)
		// by transaction date and by original transaction date (everything must balance by client account on a given date)
		Map<String, BigDecimal> baseSums = new LinkedHashMap<>();
		BinaryOperator<BigDecimal> baseSumsValueCalculator = (existing, newValue) -> existing == null ? newValue : existing.add(newValue);
		for (AccountingJournalDetailDefinition detail : journal.getJournalDetailList()) {
			detail.setJournal(journal);
			// check required fields
			ValidationUtils.assertNotNull(detail.getAccountingAccount(), "Accounting account is required for each journal detail.");
			ValidationUtils.assertFalse(detail.getAccountingAccount().isPostingNotAllowed(), () -> "GL Account '" + detail.getAccountingAccount().getName() + "' is not allowed to be posted into GL.");
			ValidationUtils.assertNotNull(detail.getClientInvestmentAccount(), "Client investment account is required for each journal detail.");
			ValidationUtils.assertNotNull(detail.getHoldingInvestmentAccount(), "Holding investment account is required for each journal detail.");
			ValidationUtils.assertNotNull(detail.getInvestmentSecurity(), "Investment security is required for each journal detail.");
			ValidationUtils.assertNotNull(detail.getTransactionDate(), () -> "Transaction date is required for each journal detail. Client account: " + detail.getClientInvestmentAccount().getLabel()
					+ "; security: " + detail.getInvestmentSecurity().getLabel());
			ValidationUtils.assertNotNull(detail.getOriginalTransactionDate(), () -> "Original transaction date is required for each journal detail. Client account: "
					+ detail.getClientInvestmentAccount().getLabel() + "; security: " + detail.getInvestmentSecurity().getLabel() + "; transaction date: " + detail.getTransactionDate());
			ValidationUtils.assertNotNull(detail.getSettlementDate(), () -> "Settlement date is required for each journal detail. Client account: " + detail.getClientInvestmentAccount().getLabel()
					+ "; security: " + detail.getInvestmentSecurity().getLabel() + "; transaction date: " + detail.getTransactionDate());
			ValidationUtils.assertNotNull(detail.getLocalDebitCredit(), "Local Debit or Credit amount must be specified for each journal detail.", "localDebitCredit");
			ValidationUtils.assertNotNull(detail.getExchangeRateToBase(), "Exchange Rate must be specified for each journal detail.", "exchangeRateToBase");
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(detail.getExchangeRateToBase(), BigDecimal.ZERO), "Exchange Rate cannot be negative or zero for each journal detail.", "exchangeRateToBase");
			ValidationUtils.assertNotNull(detail.getPositionCostBasis(), "Position Cost Basis must be specified for each journal detail.", "positionCostBasis");
			ValidationUtils.assertFalse(detail.getSettlementDate().before(detail.getTransactionDate()), "Settlement Date can never be before Transaction Date.", "settlementDate");
			if (detail.getPrice() != null) {
				ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(detail.getPrice(), BigDecimal.ZERO), () -> "Price of " + detail.getPrice().toPlainString() + " cannot be negative.", "price");
			}
			AccountingTransaction parent = detail.getParentTransaction();
			if (parent != null) {
				ValidationUtils.assertFalse(parent.isDeleted(), () -> "Journal detail " + detail + " cannot reference a Deleted transaction.");
				ValidationUtils.assertEquals(parent.getClientInvestmentAccount(), detail.getClientInvestmentAccount(), () -> "Client Investment Account for " + detail + " must be the same as the parent transaction.");
			}
			if (detail.isClosing()) {
				ValidationUtils.assertTrue(detail.getParentDefinition() != null || parent != null, () -> "Closing journal detail " + detail + " for journal " + journal + " must be linked to a parent that it closes.");
				if (detail.getAccountingAccount().isPosition() && parent != null) {
					ValidationUtils.assertTrue(detail.getInvestmentSecurity().equals(parent.getInvestmentSecurity()), () -> "Closing position " + detail + " for journal " + journal + " must have parent with the same security.");
					ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(detail.getTransactionDate(), parent.getTransactionDate()), () -> "Transaction Date of closing position must be greater than or equal to the opening position: " + detail);
					ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(detail.getSettlementDate(), parent.getSettlementDate()), () -> "Settlement Date of closing position must be greater than or equal to the opening position: " + detail);
				}
			}
			if (!MathUtils.isNullOrZero(detail.getQuantity()) && !MathUtils.isNullOrZero(detail.getPositionCostBasis())) {
				if (!detail.getInvestmentSecurity().getInstrument().getHierarchy().isDifferentQuantityAndCostBasisSignAllowed()) {
					ValidationUtils.assertTrue(detail.getQuantity().signum() == detail.getPositionCostBasis().signum(), () -> "Quantity and PositionCostBasis sign must be the same for each journal detail. Journal: " + journal);
				}
			}
			// make sure (Base Debit - Base Credit) = FX Rate * (Local Debit - Local Credit)
			BigDecimal baseDC = InvestmentCalculatorUtils.calculateBaseAmount(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), detail.getClientInvestmentAccount());
			if (detail.getBaseDebitCredit() == null) {
				// set base currency debit/credit based on exchange rate if not populated
				detail.setBaseDebitCredit(baseDC);
			}

			if (detail.getAccountingAccount().isGainLoss()) {
				ValidationUtils.assertTrue(parent != null || detail.getParentDefinition() != null, "Gain/Loss GL Account entry must be tied to corresponding Position that the gain/loss is for.");
			}

			// make sure accounting period posted to is still open
			if (getAccountingPeriodService().isAccountingPeriodClosed(detail.getClientInvestmentAccount().getId(), detail.getTransactionDate())) {
				throw new ValidationException("Cannot post " + detail.getClientInvestmentAccount().getLabel() + " entry on " + DateUtils.fromDateShort(detail.getTransactionDate()) + " to a closed accounting period.");
			}

			// cash/currency validation logic
			if (detail.getAccountingAccount().isCurrency()) {
				if (detail.getAccountingAccount().isCash()) {
					// cash
					ValidationUtils.assertTrue(InvestmentUtils.isSecurityEqualToClientAccountBaseCurrency(detail), () -> "Cash GL account '"
							+ detail.getAccountingAccount().getName() + "' cannot be used for a journal detail where the client's base currency is different from security.");
					ValidationUtils.assertTrue(MathUtils.isEqual(detail.getExchangeRateToBase(), BigDecimal.ONE), () -> "Exchange Rate for Cash entry must be 1 and not: " + detail.getExchangeRateToBase());
					ValidationUtils.assertTrue(MathUtils.isEqual(detail.getLocalDebitCredit(), detail.getBaseDebitCredit()), () -> "Base DebitCredit = " + detail.getBaseDebitCredit()
							+ " must equal local DebitCredit = " + detail.getLocalDebitCredit() + " for Cash entries.");
				}
				else {
					// currency
					ValidationUtils.assertFalse(InvestmentUtils.isSecurityCurrencyDenominationEqualToClientAccountBaseCurrency(detail),
							"Currency GL account '" + detail.getAccountingAccount().getName()
									+ "' cannot be used for a journal detail where the client's base currency is the same as the security's trading currency.");
				}
			}

			if (InvestmentUtils.isSecurityCurrencyDenominationEqualToClientAccountBaseCurrency(detail)) {
				ValidationUtils.assertTrue(MathUtils.isEqual(detail.getExchangeRateToBase(), BigDecimal.ONE), "Exchange Rate for security that has currency denomination of client's base currency must be 1 and not: " + detail.getExchangeRateToBase());
				ValidationUtils.assertTrue(MathUtils.isEqual(detail.getLocalDebitCredit(), detail.getBaseDebitCredit()), "Base DebitCredit = " + detail.getBaseDebitCredit()
						+ " must equal local DebitCredit = " + detail.getLocalDebitCredit() + " for security that has currency denomination of client's base currency.");
			}

			// position-based GL Account validation
			if (detail.getAccountingAccount().isPosition()) {
				// Position entry
				if (InvestmentUtils.isNoPaymentOnOpen(detail.getInvestmentSecurity())) {
					ValidationUtils.assertTrue(MathUtils.isEqual(detail.getLocalDebitCredit(), BigDecimal.ZERO),
							"Position entry Local Debit Credit must be 0 for security with no payment on open. Journal: " + journal);
				}
				else {
					ValidationUtils.assertTrue(
							MathUtils.isEqual(detail.getLocalDebitCredit(), detail.getPositionCostBasis()),
							"Position entry Local Debit Credit " + CoreMathUtils.formatNumberMoney(detail.getLocalDebitCredit()) + " must equal Position Cost Basis "
									+ CoreMathUtils.formatNumberMoney(detail.getPositionCostBasis()) + " for security that has a payment on open. Journal: " + journal);
				}
			}
			else {
				// non-position GL Accounts
				ValidationUtils.assertTrue((detail.getPositionCostBasis().compareTo(BigDecimal.ZERO) == 0), "Position Cost Basis must be 0 for non Position records. Journal: " + journal);
			}

			// allow FX rounding within 25 cents (TradeID = 429719 had 18 cents)
			// NOTE: Current logic does proper base rounding at lot level. This can result in total being off by a few cents.
			// Should we have accurate base total instead and allocate pennies to corresponding "opposite" journal details?
			if (!detail.getAccountingAccount().isCurrencyTranslation()) {
				// skip closing positions: foreign positions must close proportion of what was open and the difference goes to currency translation gain/loss
				if (!(detail.getAccountingAccount().isPosition() && (detail.getParentDefinition() != null || parent != null))) {
					ValidationUtils.assertTrue(
							MathUtils.isLessThan(detail.getBaseDebitCredit().subtract(baseDC).abs(), 0.26),
							"(Base Debit - Base Credit) <> FX Rate * (Local Debit - Local Credit): " + detail.getBaseDebitCredit() + " <> " + detail.getExchangeRateToBase() + " * "
									+ detail.getLocalDebitCredit() + " (" + baseDC + ")" + " on " + DateUtils.fromDateShort(detail.getTransactionDate()) + ". Exceeds rounding threshold of 0.25");
				}
			}

			ValidationUtils.assertTrue(detail.getClientInvestmentAccount().isActive(), () -> "Can only post to 'Active' client accounts: " + detail.getClientInvestmentAccount().getLabel());
			ValidationUtils.assertTrue(detail.getHoldingInvestmentAccount().isActive(), () -> "Can only post to 'Active' holding accounts: " + detail.getHoldingInvestmentAccount().getLabel());

			// make sure client and holding account relationship is allowed
			ValidationUtils.assertTrue(detail.getClientInvestmentAccount().getType().isOurAccount(), () -> "Can only post to valid client accounts: " + detail.getClientInvestmentAccount().getLabel());
			ValidationUtils.assertTrue(!detail.getHoldingInvestmentAccount().getType().isOurAccount(), () -> "Can only post to valid holding accounts: " + detail.getHoldingInvestmentAccount().getLabel());
			Integer clientAccountId = detail.getClientInvestmentAccount().getId();
			if (!getInvestmentAccountRelationshipService().isInvestmentAccountRelationshipValid(clientAccountId, detail.getHoldingInvestmentAccount().getId(), detail.getTransactionDate())) {
				// cannot use OriginalTransactionDate because we maybe transferring a very old position and receiving account may not have had an active relationship at that time
				throw new ValidationException("Relationship between client account " + detail.getClientInvestmentAccount().getLabel() + " and holding account "
						+ detail.getHoldingInvestmentAccount().getLabel() + " is not defined on Transaction Date " + DateUtils.fromDateShort(detail.getTransactionDate()));
			}

			InvestmentSecurity security = detail.getInvestmentSecurity();
			InvestmentAccountType holdingAccountType = detail.getHoldingInvestmentAccount().getType();
			InvestmentAccountType securityHoldingAccountType = security.getInstrument().getHierarchy().getHoldingAccountType();
			if (securityHoldingAccountType != null && !securityHoldingAccountType.equals(holdingAccountType)) {
				// allow non position Receivables in "Cash Location" account
				if (detail.getAccountingAccount().isReceivable() && !detail.getAccountingAccount().isPosition()) {
					InvestmentAccount cashAccount = getInvestmentSecurityUtilHandler().getCashLocationHoldingAccount(detail);
					if (!cashAccount.equals(detail.getHoldingInvestmentAccount())) {
						throw new FieldValidationException("Holding Account " + detail.getHoldingInvestmentAccount().getLabel() + " must be of type '" + securityHoldingAccountType.getName() + "' for security: "
								+ security.getLabel(), "holdingInvestmentAccount.id");
					}
				}
			}

			// make sure investment security was active on transaction date
			if (security.getEarlyTerminationDate() != null && !DateUtils.isDateBetween(detail.getTransactionDate(), security.getStartDate(), security.getEarlyTerminationDate(), false)) {
				throw new ValidationException("Transaction Date " + DateUtils.fromDateShort(detail.getTransactionDate()) + " cannot be after Early Termination Date of "
						+ DateUtils.fromDateShort(security.getEarlyTerminationDate()));
			}
			if (!DateUtils.isDateBetween(detail.getTransactionDate(), security.getStartDate(), security.getEndDate(), false)) {
				// allow position closing within delivery dates (commission, etc. could be an opening but still must be within the delivery date range)
				boolean allowDelivery = (security.getFirstDeliveryDate() != null && security.getLastDeliveryDate() != null && (detail.isClosing() || (detail.isOpening() && !detail.getAccountingAccount()
						.isPosition())));
				if (allowDelivery) {
					allowDelivery = DateUtils.isDateBetween(detail.getTransactionDate(), security.getFirstDeliveryDate(), security.getLastDeliveryDate(), false);
				}
				if (!allowDelivery && !security.getInstrument().isDeliverable() && DateUtils.compare(detail.getTransactionDate(), security.getFirstNoticeDate(), false) == 0) {
					// Cash Settled securities can be open close on Valuation Date (firstNoticeDate): get a future on valuation date from option on future exercise
					allowDelivery = true;
				}
				// check if Transaction Date is before Issue Date
				if (security.getStartDate() != null && DateUtils.compare(detail.getTransactionDate(), security.getStartDate(), false) < 0) {
					// use settlementDate comparison: can buy a bond before its IssueDate as long as it settles on or after the issue date
					allowDelivery = (DateUtils.compare(detail.getSettlementDate(), security.getStartDate(), false) >= 0);
				}
				ValidationUtils.assertTrue(allowDelivery, () -> "Investment security " + security.getLabel() + " must be active on transaction date " + DateUtils.fromDateShort(detail.getTransactionDate()), "security.id");
			}

			if (detail.getAccountingAccount().isPosition()) {
				if (security.getInstrument().getHierarchy().isOtc()) {
					if (holdingAccountType.isContractRequired()) {
						// make sure if security is an OTC security, holding account has an ISDA selected
						String contractType = (holdingAccountType.getContractType() == null) ? BusinessContractTypes.ISDA.getName() : holdingAccountType.getContractType().getName();
						ValidationUtils.assertNotNull(detail.getHoldingInvestmentAccount().getBusinessContract(), contractType + " must be selected for holding account ["
								+ detail.getHoldingInvestmentAccount().getLabel() + "] in order to post OTC security [" + security.getSymbol() + "].");
					}
				}
				else if (holdingAccountType.isOtc()) {
					// OTC accounts can only have OTC non-Collateral positions
					ValidationUtils.assertTrue(detail.getAccountingAccount().isCollateral(), "[" + detail.getHoldingInvestmentAccount().getLabel() + "] Holding Account cannot have non OTC position that is not collateral [" + security.getSymbol() + "].");
				}
			}

			// make sure we're not over-closing existing position (ignore dates as there maybe later closing transactions)
			if (detail.getAccountingAccount().isPosition() && parent != null && detail.isClosing()) {
				ValidationUtils.assertFalse(detail.getSettlementDate().before(parent.getSettlementDate()),
						() -> "Cannot close existing position " + parent + " on " + DateUtils.fromDateShort(detail.getSettlementDate()) + " because it has not settled yet.", "settlementDate");
				AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
				searchForm.setParentId(parent.getId());
				searchForm.setInvestmentSecurityId(detail.getInvestmentSecurity().getId());
				searchForm.setAccountingAccountIds(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIds.POSITION_ACCOUNTS));
				searchForm.setOpening(false);
				List<AccountingTransaction> childList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
				if (!CollectionUtils.isEmpty(childList)) {
					BigDecimal quantity = BigDecimal.ZERO;
					BigDecimal localDebitCredit = BigDecimal.ZERO;
					BigDecimal baseDebitCredit = BigDecimal.ZERO;
					BigDecimal costBasis = BigDecimal.ZERO;
					// sum posted amounts
					for (AccountingTransaction child : childList) {
						quantity = MathUtils.add(quantity, child.getQuantity());
						localDebitCredit = MathUtils.add(localDebitCredit, child.getLocalDebitCredit());
						baseDebitCredit = MathUtils.add(baseDebitCredit, child.getBaseDebitCredit());
						costBasis = MathUtils.add(costBasis, child.getPositionCostBasis());
					}
					// add amounts being posted form this journal
					for (AccountingJournalDetailDefinition child : journal.getJournalDetailList()) {
						if (parent.equals(child.getParentTransaction()) && child.isClosing() && child.getAccountingAccount().isPosition()) {
							quantity = MathUtils.add(quantity, child.getQuantity());
							localDebitCredit = MathUtils.add(localDebitCredit, child.getLocalDebitCredit());
							baseDebitCredit = MathUtils.add(baseDebitCredit, child.getBaseDebitCredit());
							costBasis = MathUtils.add(costBasis, child.getPositionCostBasis());
						}
					}

					BigDecimal remainder = MathUtils.add(parent.getQuantity(), quantity);
					if (!MathUtils.isNullOrZero(remainder)) {
						ValidationUtils.assertTrue(remainder.signum() == parent.getQuantity().signum(), "Cannot over-close Quantity for position with Transaction ID = " + parent.getId() + " on " + DateUtils.fromDateShort(detail.getTransactionDate())
								+ ". Security = " + detail.getInvestmentSecurity().getLabel() + ". Overclose Amount = " + CoreMathUtils.formatNumberDecimal(remainder));
					}
					remainder = MathUtils.add(parent.getLocalDebitCredit(), localDebitCredit);
					if (!MathUtils.isNullOrZero(remainder)) {
						ValidationUtils.assertTrue(remainder.signum() == parent.getLocalDebitCredit().signum(),
								"Cannot over-close LocalDebitCredit for position with Transaction ID = " + parent.getId() + ". Security = " + detail.getInvestmentSecurity().getLabel()
										+ ". Overclose Amount = " + CoreMathUtils.formatNumberDecimal(remainder));
					}
					remainder = MathUtils.add(parent.getBaseDebitCredit(), baseDebitCredit);
					if (!MathUtils.isNullOrZero(remainder)) {
						ValidationUtils.assertTrue(remainder.signum() == parent.getBaseDebitCredit().signum(), "Cannot over-close BaseDebitCredit for position with Transaction ID = " + parent.getId()
								+ ". Security = " + detail.getInvestmentSecurity().getLabel() + ". Overclose Amount = " + CoreMathUtils.formatNumberDecimal(remainder));
					}
					remainder = MathUtils.add(parent.getPositionCostBasis(), costBasis);
					if (!MathUtils.isNullOrZero(remainder)) {
						ValidationUtils.assertTrue(remainder.signum() == parent.getPositionCostBasis().signum(),
								"Cannot over-close PositionCostBasis for position with Transaction ID = " + parent.getId() + ". Security = " + detail.getInvestmentSecurity().getLabel()
										+ ". Overclose Amount = " + CoreMathUtils.formatNumberDecimal(remainder));
					}
				}
			}

			// make sure that we are not closing a collateral position owned by another broker
			if (detail.getAccountingAccount().isPosition() && parent != null && detail.isClosing() && detail.getAccountingAccount().isExecutingBrokerSpecific()) {
				if (!detail.getExecutingOrClearingCompany().equals(parent.getExecutingOrClearingCompany())) {
					ValidationUtils.assertTrue(isSeparateExecutingCompanyAllowed(detail.getJournal()),
							() -> "Cannot close existing position [" + parent + "] in Executing Broker Specific GL Account [" + detail.getAccountingAccount().getLabel() + "] " +
									"because the position being closed was opened by [" + parent.getExecutingOrClearingCompany() + "] Executing Broker " +
									"and the closing transaction is for [" + detail.getExecutingOrClearingCompany() + "] Executing Broker.");
				}
			}


			// verify max journal count of same type for same entity (if one is defined)
			Short maxCount = journal.getJournalType().getMaxSubsystemJournalCount();
			if (maxCount != null && detail.getFkFieldId() != null) {
				// MUST USE AccountingTransaction because adjusting journals don't set FKFieldID on the journal
				AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
				searchForm.setJournalTypeId(journal.getJournalType().getId());
				searchForm.setAccountingAccountId(detail.getAccountingAccount().getId());
				searchForm.setFkFieldId(detail.getFkFieldId());
				searchForm.setTableId(detail.getSystemTable().getId());
				if (parent != null) {
					searchForm.setParentId(parent.getId());
				}
				else {
					searchForm.addSearchRestriction(new SearchRestriction("parentId", ComparisonConditions.IS_NULL, null));
				}
				List<AccountingTransaction> duplicateList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
				if (CollectionUtils.getSize(duplicateList) > (maxCount - 1)) { // one for itself
					throw new ValidationException(journal.getJournalType().getName() + " cannot have more than " + maxCount
							+ " journals of same type linked to the same entity. Correct previously booked journal if necessary.");
				}
			}

			// add to corresponding running sums
			String sumKey = getBalanceKey(clientAccountId, detail.getTransactionDate(), false);
			baseSums.compute(sumKey, (key, existing) -> baseSumsValueCalculator.apply(existing, detail.getBaseDebitCredit()));
			sumKey = getBalanceKey(clientAccountId, detail.getOriginalTransactionDate(), true);
			baseSums.compute(sumKey, (key, existing) -> baseSumsValueCalculator.apply(existing, detail.getBaseDebitCredit()));
		}

		validatePositionClosingFromSameJournalType(journal);

		for (Map.Entry<String, BigDecimal> sumEntry : baseSums.entrySet()) {
			BigDecimal baseSum = sumEntry.getValue();
			if (MathUtils.isNotEqual(baseSum, BigDecimal.ZERO)) {
				throw new ValidationException(
						"The sum of base debits for journal "
								+ journal
								+ " does not equal the sum of base credits by client investment account for transaction or original transaction date and the differences does not fall within allowed adjustment range.  The difference is "
								+ baseSum);
			}
		}
	}


	private String getBalanceKey(Integer clientAccountId, Date date, boolean original) {
		return clientAccountId + "-" + (original ? "O-" : "T-") + date;
	}


	private boolean isSeparateExecutingCompanyAllowed(AccountingJournal journal) {
		if (AccountingJournalType.TRADE_JOURNAL.equals(journal.getJournalType().getName())) {
			return true;
		}
		IdentityObject bean = getAccountingBookingService().getBookableEntity(journal);
		if (bean instanceof AccountingEventJournal) {
			return AccountingEventJournalType.SECURITY_MATURITY.equals(((AccountingEventJournal) bean).getJournalType().getName());
		}
		return false;
	}


	private void validatePositionClosingFromSameJournalType(AccountingJournal journal) {
		for (AccountingJournalDetailDefinition journalDetail : journal.getJournalDetailList()) {
			// only apply to closing positions
			if (journalDetail.getAccountingAccount().isPosition() && journalDetail.isClosing() && journalDetail.getParentTransaction() != null) {
				// only apply if the holding account type of the transaction being closed requires it
				if (journalDetail.getParentTransaction().getHoldingInvestmentAccount().getType().isRequirePositionClosingFromSameJournalType()) {
					// check that the journal types match if needed
					if (!journal.getJournalType().equals(journalDetail.getParentTransaction().getJournal().getJournalType())) {

						throw new ValidationException(String.format("Unable to close positions in account [%s] because the account is of type [%s] which requires positions to be closed by journals of the same type that they were opened by.  Opening journal type is [%s], and closing journal type is [%s].",
								journalDetail.getParentTransaction().getHoldingInvestmentAccount().getLabel(),
								journalDetail.getParentTransaction().getHoldingInvestmentAccount().getType().getName(),
								journalDetail.getParentTransaction().getJournal().getJournalType().getName(),
								journal.getJournalType().getName()));
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<AccountingJournalDetail> getAccountingJournalDetailDAO() {
		return this.accountingJournalDetailDAO;
	}


	public void setAccountingJournalDetailDAO(UpdatableDAO<AccountingJournalDetail> accountingJournalDetailDAO) {
		this.accountingJournalDetailDAO = accountingJournalDetailDAO;
	}


	public AdvancedUpdatableDAO<AccountingJournal, Criteria> getAccountingJournalDAO() {
		return this.accountingJournalDAO;
	}


	public void setAccountingJournalDAO(AdvancedUpdatableDAO<AccountingJournal, Criteria> accountingJournalDAO) {
		this.accountingJournalDAO = accountingJournalDAO;
	}


	public UpdatableDAO<AccountingJournalType> getAccountingJournalTypeDAO() {
		return this.accountingJournalTypeDAO;
	}


	public void setAccountingJournalTypeDAO(UpdatableDAO<AccountingJournalType> accountingJournalTypeDAO) {
		this.accountingJournalTypeDAO = accountingJournalTypeDAO;
	}


	public ReadOnlyDAO<AccountingJournalStatus> getAccountingJournalStatusDAO() {
		return this.accountingJournalStatusDAO;
	}


	public void setAccountingJournalStatusDAO(ReadOnlyDAO<AccountingJournalStatus> accountingJournalStatusDAO) {
		this.accountingJournalStatusDAO = accountingJournalStatusDAO;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoNamedEntityCache<AccountingJournalStatus> getAccountingJournalStatusCache() {
		return this.accountingJournalStatusCache;
	}


	public void setAccountingJournalStatusCache(DaoNamedEntityCache<AccountingJournalStatus> accountingJournalStatusCache) {
		this.accountingJournalStatusCache = accountingJournalStatusCache;
	}


	public DaoNamedEntityCache<AccountingJournalType> getAccountingJournalTypeCache() {
		return this.accountingJournalTypeCache;
	}


	public void setAccountingJournalTypeCache(DaoNamedEntityCache<AccountingJournalType> accountingJournalTypeCache) {
		this.accountingJournalTypeCache = accountingJournalTypeCache;
	}


	@SuppressWarnings("rawtypes")
	public AccountingBookingService getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(@SuppressWarnings("rawtypes") AccountingBookingService accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}
}
