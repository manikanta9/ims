package com.clifton.accounting.gl;


import com.clifton.accounting.account.AccountingAccountGroupItemAccountingAccount;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.PropertySubqueryUnionExpression;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.schema.SystemSchemaService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class AccountingTransactionServiceImpl implements AccountingTransactionService {

	// NOTE: inserts/updates to transactions are done by booking service only with strict validation
	private AdvancedReadOnlyDAO<AccountingTransaction, Criteria> accountingTransactionDAO;

	private AccountingAccountIdsCache accountingAccountIdsCache;

	private CalendarBusinessDayService calendarBusinessDayService;
	private DataTableRetrievalHandler dataTableRetrievalHandler;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingTransaction getAccountingTransaction(long id) {
		return getAccountingTransactionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<AccountingTransaction> getAccountingTransactionListByIds(Long[] ids) {
		return getAccountingTransactionDAO().findByPrimaryKeys(ids);
	}


	@Override
	public List<AccountingTransaction> getAccountingTransactionListByJournal(long journalId) {
		return getAccountingTransactionDAO().findByField("journal.id", journalId);
	}


	@Override
	public List<AccountingTransaction> getAccountingTransactionList(final AccountingTransactionSearchForm searchForm) {
		// by default, deleted rows are excluded
		if (!searchForm.isAllowDeleted() && !searchForm.containsSearchRestriction("deleted")) {
			searchForm.setDeleted(false);
		}
		if (!StringUtils.isEmpty(searchForm.getTableName())) {
			// convert to id using cache to avoid extra join
			searchForm.setTableId(getSystemSchemaService().getSystemTableByName(searchForm.getTableName()).getId());
			searchForm.setTableName(null);
		}

		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getAccountingAccountIdName() != null) {
					criteria.add(Restrictions.in("accountingAccount.id", getAccountingAccountIdsCache().getAccountingAccounts(searchForm.getAccountingAccountIdName())));
				}

				if (searchForm.getAccountingAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(AccountingAccountGroupItemAccountingAccount.class)
							.setProjection(Projections.property("referenceTwo.id"))
							.createAlias("referenceOne", "groupItem")
							.add(Restrictions.eq("groupItem.group.id", searchForm.getAccountingAccountGroupId()));
					criteria.add(Subqueries.propertyIn("accountingAccount.id", sub));
				}
				if (searchForm.getParentOrGrandParentId() != null) {
					// use UNION because OR results in very inefficient execution plan
					criteria.add(PropertySubqueryUnionExpression.forPropertyInUnionAll("parentTransaction.id", CollectionUtils.createList(
							DetachedCriteria.forClass(AccountingTransaction.class)
									.setProjection(Projections.property("id"))
									.add(Restrictions.eq("id", searchForm.getParentOrGrandParentId())),
							DetachedCriteria.forClass(AccountingTransaction.class)
									.setProjection(Projections.property("parentTransaction.id"))
									.createAlias("parentTransaction", "pt")
									.add(Restrictions.eq("pt.parentTransaction.id", searchForm.getParentOrGrandParentId()))
					)));
				}
			}
		};

		return getAccountingTransactionDAO().findBySearchCriteria(config);
	}


	@Override
	public Map<Integer, Date> getAccountingTransactionHistoricalChangeMap(int daysBack) {
		Date today = DateUtils.clearTime(new Date());
		Date previous = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(today), -daysBack);
		// also include unposted (IsDeleted = 1) cases
		DataTable dt = getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(
				"SELECT ClientInvestmentAccountID, CAST(MIN(TransactionDate) AS DATETIME) " +
						"FROM AccountingTransaction t INNER JOIN AccountingJournal j ON t.AccountingJournalID = j.AccountingJournalID " +
						"WHERE (j.PostingDate >= ? OR j.UpdateDate >= ?) AND t.TransactionDate < ? " +
						"GROUP BY ClientInvestmentAccountID")
				.addDateParameterValue(previous)
				.addDateParameterValue(previous)
				.addDateParameterValue(today)
		);

		Map<Integer, Date> map = new HashMap<>();
		if (dt != null && dt.getTotalRowCount() > 0) {
			for (int i = 0; i < dt.getTotalRowCount(); i++) {
				DataRow row = dt.getRow(i);
				map.put((Integer) row.getValue(0), (Date) row.getValue(1));
			}
		}
		return map;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<AccountingTransaction, Criteria> getAccountingTransactionDAO() {
		return this.accountingTransactionDAO;
	}


	public void setAccountingTransactionDAO(AdvancedReadOnlyDAO<AccountingTransaction, Criteria> accountingTransactionDAO) {
		this.accountingTransactionDAO = accountingTransactionDAO;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
