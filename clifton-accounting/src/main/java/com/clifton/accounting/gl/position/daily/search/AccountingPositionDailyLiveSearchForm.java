package com.clifton.accounting.gl.position.daily.search;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingPositionDailyLiveSearchForm</code> class is a search form for live AccountingPositionDaily objects.
 * Live means that all calculations are done live vs using snapshot table.
 *
 * @author vgomelsky
 */
@NonPersistentObject
@SearchForm(hasOrmDtoClass = false)
public class AccountingPositionDailyLiveSearchForm {

	private List<SearchRestriction> restrictionList;

	private Integer clientAccountId;
	private Integer[] clientAccountIds;
	private Integer holdingAccountId;
	private Integer[] holdingAccountIds;
	private Integer holdingCompanyId;

	private Integer clientAccountGroupId;
	private Integer holdingAccountGroupId;

	private Integer investmentSecurityId;
	private Integer investmentInstrumentCurrencyDenominationId;
	private Short investmentGroupId;

	/**
	 * Optional start date to return information for a date range.  Because searches uses AccountingBalance look ups two times per date (compares "today" to previous day)
	 * options from snapshot date will be able to loop through the dates and keep track of prior day without having to perform a second lookup for each date that isn't  necessary
	 * Example: Performance Summaries we need CCY Gain/Loss for each day during the month
	 */
	private Date fromSnapshotDate;

	private Date snapshotDate;
	private boolean useSettlementDate;

	/**
	 * Calculates realized gain loss on this date and adjusts snapshotDate realized with this amount.
	 * Used with Cleared Swaps Mark to Market to shift payment one business day earlier for delayed payments (coupons and credit events).
	 * Was implemented to match ICE convention for CDS defaults.
	 */
	private Date realizedAdjustmentDate;

	/**
	 * Will use the T+=1 index ratio for indexRatioAdjusted securities.
	 */
	private boolean useNextDayIndexRatio;

	private Boolean collateralAccountingAccount;

	private boolean includeUnsettledLegPayments;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void validate() {
		ValidationUtils.assertNotNull(this.snapshotDate, "snapshotDate is required");
		if (getFromSnapshotDate() != null) {
			ValidationUtils.assertTrue(DateUtils.compare(getFromSnapshotDate(), getSnapshotDate(), false) <= 0, "From Snapshot Date must be on or before snapshot date.");
		}
		ValidationUtils.assertFalse(getClientAccountId() == null && ArrayUtils.isEmpty(getClientAccountIds()) && getHoldingAccountId() == null && getHoldingAccountGroupId() == null && getHoldingCompanyId() == null && getInvestmentSecurityId() == null && getInvestmentGroupId() == null,
				"At least one of the following restrictions must be present: client account, holding account, holding account group, holding company, investment security, instrument group.");
	}


	public static AccountingPositionDailyLiveSearchForm forSnapshotDate(Date snapshotDate) {
		AccountingPositionDailyLiveSearchForm result = new AccountingPositionDailyLiveSearchForm();
		result.setSnapshotDate(snapshotDate);
		return result;
	}


	public AccountingPositionDailyLiveSearchForm forHoldingAccount(Integer holdingAccountId) {
		setHoldingAccountId(holdingAccountId);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<SearchRestriction> getRestrictionList() {
		return this.restrictionList;
	}


	public void setRestrictionList(List<SearchRestriction> restrictionList) {
		this.restrictionList = restrictionList;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer[] getHoldingAccountIds() {
		return this.holdingAccountIds;
	}


	public void setHoldingAccountIds(Integer[] holdingAccountIds) {
		this.holdingAccountIds = holdingAccountIds;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public Integer getHoldingAccountGroupId() {
		return this.holdingAccountGroupId;
	}


	public void setHoldingAccountGroupId(Integer holdingAccountGroupId) {
		this.holdingAccountGroupId = holdingAccountGroupId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getInvestmentInstrumentCurrencyDenominationId() {
		return this.investmentInstrumentCurrencyDenominationId;
	}


	public void setInvestmentInstrumentCurrencyDenominationId(Integer investmentInstrumentCurrencyDenominationId) {
		this.investmentInstrumentCurrencyDenominationId = investmentInstrumentCurrencyDenominationId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Date getFromSnapshotDate() {
		return this.fromSnapshotDate;
	}


	public void setFromSnapshotDate(Date fromSnapshotDate) {
		this.fromSnapshotDate = fromSnapshotDate;
	}


	public Date getSnapshotDate() {
		return this.snapshotDate;
	}


	public void setSnapshotDate(Date snapshotDate) {
		this.snapshotDate = snapshotDate;
	}


	public boolean isUseSettlementDate() {
		return this.useSettlementDate;
	}


	public void setUseSettlementDate(boolean useSettlementDate) {
		this.useSettlementDate = useSettlementDate;
	}


	public Date getRealizedAdjustmentDate() {
		return this.realizedAdjustmentDate;
	}


	public void setRealizedAdjustmentDate(Date realizedAdjustmentDate) {
		this.realizedAdjustmentDate = realizedAdjustmentDate;
	}


	public boolean isUseNextDayIndexRatio() {
		return this.useNextDayIndexRatio;
	}


	public void setUseNextDayIndexRatio(boolean useNextDayIndexRatio) {
		this.useNextDayIndexRatio = useNextDayIndexRatio;
	}


	public Boolean getCollateralAccountingAccount() {
		return this.collateralAccountingAccount;
	}


	public void setCollateralAccountingAccount(Boolean collateralAccountingAccount) {
		this.collateralAccountingAccount = collateralAccountingAccount;
	}


	public boolean isIncludeUnsettledLegPayments() {
		return this.includeUnsettledLegPayments;
	}


	public void setIncludeUnsettledLegPayments(boolean includeUnsettledLegPayments) {
		this.includeUnsettledLegPayments = includeUnsettledLegPayments;
	}
}
