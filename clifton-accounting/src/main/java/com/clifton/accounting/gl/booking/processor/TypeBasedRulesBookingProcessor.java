package com.clifton.accounting.gl.booking.processor;


import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;

import java.util.List;
import java.util.Map;


/**
 * The <code>TypeBasedRulesBookingProcessor</code> class is a booking processor base class that can be implemented
 * by sub-systems that apply different lists of booking rules for its entities based on entity type.
 * <p>
 * Sub-system specific booking processor must be defined in application context and "bookingRulesMap" property
 * must be set (the order of rules is important).
 *
 * @param <T>
 * @author akorver
 * @author vgomelsky
 */
public abstract class TypeBasedRulesBookingProcessor<T extends BookableEntity> extends BaseBookingProcessor<T> {

	private Map<String, List<AccountingBookingRule<T>>> bookingRulesMap;
	private String typeProperty = "type.name";

	/**
	 * Optionally specifies default booking rules to be used if entity type specific rules (overrides) are not defined.
	 */
	private List<AccountingBookingRule<T>> defaultBookingRules;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingBookingRule<T>> getBookingRules(BookingSession<T> bookingSession) {
		if (this.bookingRulesMap == null) {
			throw new ValidationException("bookingRulesMap must be initialized for booking processor for " + getJournalTypeName());
		}
		String entityType = getBookingEntityType(bookingSession.getBookableEntity());
		List<AccountingBookingRule<T>> bookingRules = this.bookingRulesMap.get(entityType);
		if (CollectionUtils.isEmpty(bookingRules)) {
			bookingRules = getDefaultBookingRules(); // apply default rules if available
		}
		ValidationUtils.assertNotEmpty(bookingRules, "Cannot find booking rules for '" + entityType + "' entity type and no default rules are defined.");
		return bookingRules;
	}


	/**
	 * Returns the key name for the List of booking rules to be applied to the specified entity.
	 */
	protected String getBookingEntityType(T bookingEntity) {
		return (String) BeanUtils.getPropertyValue(bookingEntity, getTypeProperty());
	}


	/**
	 * If override list is defined for the specified security, returns it.  Otherwise returns null which usually means that keyPrefix should be used.
	 * <p>
	 * NOTE: The method will find most specific override that exist or will return null. - See InvestmentUtils.getSecurityKeyInMap for specificity
	 */
	protected String getBookingRulesKeyOverride(String keyPrefix, InvestmentSecurity security) {
		return InvestmentUtils.getSecurityOverrideKey(keyPrefix, security, getBookingRulesMap().keySet());
	}


	public Map<String, List<AccountingBookingRule<T>>> getBookingRulesMap() {
		return this.bookingRulesMap;
	}


	public void setBookingRulesMap(Map<String, List<AccountingBookingRule<T>>> bookingRulesMap) {
		this.bookingRulesMap = bookingRulesMap;
	}


	public String getTypeProperty() {
		return this.typeProperty;
	}


	public void setTypeProperty(String typeProperty) {
		this.typeProperty = typeProperty;
	}


	public List<AccountingBookingRule<T>> getDefaultBookingRules() {
		return this.defaultBookingRules;
	}


	public void setDefaultBookingRules(List<AccountingBookingRule<T>> defaultBookingRules) {
		this.defaultBookingRules = defaultBookingRules;
	}
}
