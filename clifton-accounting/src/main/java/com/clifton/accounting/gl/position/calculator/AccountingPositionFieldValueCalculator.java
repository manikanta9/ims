package com.clifton.accounting.gl.position.calculator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.bean.SystemBeanType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The {@link SystemBeanType} implementation of {@link AccountingPositionValueCalculator} which extracts values for positions based on a configured position field.
 * <p>
 * This calculator computes a value for a position by extracting the value at the specified {@link #valueField value field}. The type of the given value field must be a {@link
 * BigDecimal}. If {@link #convertToBase} is set to <tt>true</tt>, then the provided value will also be converted using the local currency to base currency exchange rate.
 *
 * @author MikeH
 */
public class AccountingPositionFieldValueCalculator implements AccountingPositionValueCalculator, ValidationAware {

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private String valueField;
	private boolean convertToBase;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculate(AccountingPosition position, Date snapshotDate) {
		BigDecimal value = (BigDecimal) BeanUtils.getPropertyValue(position, getValueField());
		if (isConvertToBase()) {
			value = convertLocalToBaseAmount(value, position, snapshotDate);
		}
		return value;
	}


	@Override
	public void validate() throws ValidationException {
		// Validate value field existence
		ValidationUtils.assertTrue(BeanUtils.isPropertyPresent(AccountingPosition.class, getValueField()), () -> String.format("The property [%s] could not be found for objects of type [%s].", getValueField(), AccountingPosition.class.getSimpleName()), "valueField");

		// Validate value field type
		Class<?> valuePropertyType = BeanUtils.getPropertyType(AccountingPosition.class, getValueField());
		ValidationUtils.assertEquals(BigDecimal.class, valuePropertyType, () -> String.format("The selected value field must be numeric. The property [%s] with type [%s] is not of type [%s].", getValueField(), valuePropertyType.getSimpleName(), BigDecimal.class.getSimpleName()), "valueField");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the given local amount to the base amount for the client account using the local/base FX rate. This uses a flexible lookup.
	 *
	 * @param localAmount  the amount to convert from local to base
	 * @param position     the position for which the conversion shall be performed
	 * @param snapshotDate the date for which the FX rate should be retrieved
	 * @return the base amount
	 */
	private BigDecimal convertLocalToBaseAmount(BigDecimal localAmount, AccountingPosition position, Date snapshotDate) {
		String fromCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(position);
		String toCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(position);
		BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(position.fxSourceCompany(), !position.fxSourceCompanyOverridden(), fromCurrency, toCurrency, snapshotDate).flexibleLookup());
		return InvestmentCalculatorUtils.calculateBaseAmount(localAmount, fxRate, position.getClientInvestmentAccount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public String getValueField() {
		return this.valueField;
	}


	public void setValueField(String valueField) {
		this.valueField = valueField;
	}


	public boolean isConvertToBase() {
		return this.convertToBase;
	}


	public void setConvertToBase(boolean convertToBase) {
		this.convertToBase = convertToBase;
	}
}
