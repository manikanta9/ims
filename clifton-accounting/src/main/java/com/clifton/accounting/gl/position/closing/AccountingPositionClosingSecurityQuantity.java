package com.clifton.accounting.gl.position.closing;

import java.math.BigDecimal;


/**
 * Indicates the quantity of a security for which we will find closing lots.
 *
 * @author mwacker
 */
public class AccountingPositionClosingSecurityQuantity {

	private Integer investmentSecurityId;
	private BigDecimal quantity;


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
