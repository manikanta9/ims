package com.clifton.accounting.gl.position;


import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.Set;


/**
 * The <code>AccountingPositionCommand</code> class specifies parameters to be used to retrieve matching AccountingPosition objects.
 *
 * @author vgomelsky
 */
@SearchForm(hasOrmDtoClass = false)
public class AccountingPositionCommand extends AccountingBalanceSearchForm {

	private static final Set<String> REQUIRED_FILTER_OPTIONS = CollectionUtils.createHashSet("clientInvestmentAccountId", "clientInvestmentAccountGroupId",
			"holdingInvestmentAccountId", "holdingInvestmentCompanyId", "holdingInvestmentAccountGroupId",
			"investmentSecurityId", "investmentInstrumentId", "investmentGroupId", "underlyingSecurityId");

	/**
	 * An existing position id that was already retrieved and is the opening transaction id
	 */
	private Long existingPositionId;
	private Long[] existingPositionIds;

	private boolean includeBigInstrumentPositions;

	private AccountingPositionOrders order = AccountingPositionOrders.FIFO;

	private Boolean collateral;
	private Boolean excludeReceivableGLAccounts;
	private Boolean excludeNotOurGLAccounts;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (getExistingPositionId() == null) {
			if (getExistingPositionIds() == null) {
				ValidationUtils.assertMutuallyExclusive("Only Transaction Date or Settlement Date restriction can and must be specified.", getTransactionDate(), getSettlementDate());
			}
			ValidationUtils.assertTrue(getClientInvestmentAccountId() == null || getClientInvestmentAccountId() > 0, "clientInvestmentAccountId must be a positive number not: " + getClientInvestmentAccountId(), "clientInvestmentAccountId");
			ArrayUtils.getStream(getClientInvestmentAccountIds()).filter(id -> id <= 0).findAny().ifPresent(id -> ValidationUtils.failWithField("clientInvestmentAccountIds", "Invalid clientInvestmentAccountIds value found: [%d]. All values must be positive.", id));
			ValidationUtils.assertTrue(getHoldingInvestmentAccountId() == null || getHoldingInvestmentAccountId() > 0, "holdingInvestmentAccountId must be a positive number not: " + getHoldingInvestmentAccountId(), "holdingInvestmentAccountId");
			ArrayUtils.getStream(getHoldingInvestmentAccountIds()).filter(id -> id <= 0).findAny().ifPresent(id -> ValidationUtils.failWithField("holdingInvestmentAccountIds", "Invalid holdingInvestmentAccountIds value found: [%d]. All values must be positive.", id));
			ValidationUtils.assertTrue(getHoldingInvestmentCompanyId() == null || getHoldingInvestmentCompanyId() > 0, "holdingInvestmentCompanyId must be a positive number not: " + getHoldingInvestmentCompanyId(), "holdingInvestmentCompanyId");
			ValidationUtils.assertTrue(getInvestmentSecurityId() == null || getInvestmentSecurityId() > 0, "investmentSecurityId must be a positive number not: " + getInvestmentSecurityId(), "investmentSecurityId");
			ValidationUtils.assertTrue(getInvestmentInstrumentId() == null || getInvestmentInstrumentId() > 0, "investmentInstrumentId must be a positive number not: " + getInvestmentInstrumentId(), "investmentInstrumentId");
			ValidationUtils.assertTrue(getInvestmentGroupId() == null || getInvestmentGroupId() > 0, "investmentInstrumentGroupId must be a positive number not: " + getInvestmentGroupId(), "investmentGroupId");

			boolean requiredRestrictionPresent = CollectionUtils.anyMatch(getRestrictionList(), restriction -> REQUIRED_FILTER_OPTIONS.contains(restriction.getField()));
			ValidationUtils.assertFalse(!requiredRestrictionPresent && getClientInvestmentAccountId() == null && getClientInvestmentAccountIds() == null && getClientInvestmentAccountGroupId() == null
							&& getHoldingInvestmentAccountId() == null && getHoldingInvestmentAccountIds() == null && getHoldingInvestmentCompanyId() == null && getHoldingInvestmentAccountGroupId() == null
							&& getInvestmentSecurityId() == null && getInvestmentSecurityIds() == null && getInvestmentInstrumentId() == null && getInvestmentInstrumentIds() == null && getInvestmentGroupId() == null && getUnderlyingSecurityId() == null
							&& getExistingPositionId() == null && getExistingPositionIds() == null,
					"At least one of the following restrictions must be present: client account, client account group, holding account, holding account group, holding company, investment security, investment instrument, investment group, underlying security, existing position.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingPositionCommand onPositionTransactionDate(Date transactionDate) {
		AccountingPositionCommand command = new AccountingPositionCommand();
		command.setTransactionDate(transactionDate);
		return command;
	}


	public static AccountingPositionCommand onPositionSettlementDate(Date settlementDate) {
		AccountingPositionCommand command = new AccountingPositionCommand();
		command.setSettlementDate(settlementDate);
		return command;
	}


	public static AccountingPositionCommand onDate(boolean useSettlementDate, Date transactionOrSettlementDate) {
		AccountingPositionCommand command = new AccountingPositionCommand();
		if (useSettlementDate) {
			command.setSettlementDate(transactionOrSettlementDate);
		}
		else {
			command.setTransactionDate(transactionOrSettlementDate);
		}
		return command;
	}


	public static AccountingPositionCommand onDate(boolean useSettlementDate, Date transactionDate, Date settlementDate) {
		AccountingPositionCommand command = new AccountingPositionCommand();
		if (useSettlementDate) {
			command.setSettlementDate(settlementDate);
		}
		else {
			command.setTransactionDate(transactionDate);
		}
		return command;
	}


	public AccountingPositionCommand forInvestmentSecurity(Integer investmentSecurityId) {
		setInvestmentSecurityId(investmentSecurityId);
		return this;
	}


	public AccountingPositionCommand forInvestmentGroup(Short investmentGroupId) {
		setInvestmentGroupId(investmentGroupId);
		return this;
	}


	public AccountingPositionCommand forClientAccount(Integer clientInvestmentAccountId) {
		setClientInvestmentAccountId(clientInvestmentAccountId);
		return this;
	}


	public AccountingPositionCommand forClientAccounts(Integer... clientInvestmentAccountIds) {
		if (clientInvestmentAccountIds != null) {
			if (clientInvestmentAccountIds.length == 1) {
				setClientInvestmentAccountId(clientInvestmentAccountIds[0]);
			}
			else {
				setClientInvestmentAccountIds(clientInvestmentAccountIds);
			}
		}
		return this;
	}


	public AccountingPositionCommand forHoldingAccount(Integer holdingInvestmentAccountId) {
		setHoldingInvestmentAccountId(holdingInvestmentAccountId);
		return this;
	}


	public AccountingPositionCommand forHoldingInvestmentAccounts(Integer... holdingInvestmentAccountIds) {
		setHoldingInvestmentAccountIds(holdingInvestmentAccountIds);
		return this;
	}


	public AccountingPositionCommand forHoldingInvestmentCompany(Integer holdingInvestmentCompanyId) {
		setHoldingInvestmentCompanyId(holdingInvestmentCompanyId);
		return this;
	}


	public AccountingPositionCommand forClientInvestmentAccountGroup(Integer clientInvestmentAccountGroupId) {
		setClientInvestmentAccountGroupId(clientInvestmentAccountGroupId);
		return this;
	}


	public AccountingPositionCommand forHoldingInvestmentAccountGroup(Integer holdingInvestmentAccountGroupId) {
		setHoldingInvestmentAccountGroupId(holdingInvestmentAccountGroupId);
		return this;
	}


	public AccountingPositionCommand forExistingPosition(Long existingPositionId) {
		setExistingPositionId(existingPositionId);
		return this;
	}


	public AccountingPositionCommand excludeReceivableGLAccounts() {
		setExcludeReceivableGLAccounts(true);
		return this;
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isIncludeBigInstrumentPositions() {
		return this.includeBigInstrumentPositions;
	}


	public void setIncludeBigInstrumentPositions(boolean includeBigInstrumentPositions) {
		this.includeBigInstrumentPositions = includeBigInstrumentPositions;
	}


	public AccountingPositionOrders getOrder() {
		return this.order;
	}


	public void setOrder(AccountingPositionOrders order) {
		this.order = order;
	}


	public Long getExistingPositionId() {
		return this.existingPositionId;
	}


	public void setExistingPositionId(Long existingPositionId) {
		this.existingPositionId = existingPositionId;
	}


	public Long[] getExistingPositionIds() {
		return this.existingPositionIds;
	}


	public void setExistingPositionIds(Long[] existingPositionIds) {
		this.existingPositionIds = existingPositionIds;
	}


	public Boolean getCollateral() {
		return this.collateral;
	}


	public void setCollateral(Boolean collateral) {
		this.collateral = collateral;
	}


	public Boolean getExcludeReceivableGLAccounts() {
		return this.excludeReceivableGLAccounts;
	}


	public void setExcludeReceivableGLAccounts(Boolean excludeReceivableGLAccounts) {
		this.excludeReceivableGLAccounts = excludeReceivableGLAccounts;
	}


	public Boolean getExcludeNotOurGLAccounts() {
		return this.excludeNotOurGLAccounts;
	}


	public void setExcludeNotOurGLAccounts(Boolean excludeNotOurGLAccounts) {
		this.excludeNotOurGLAccounts = excludeNotOurGLAccounts;
	}
}
