package com.clifton.accounting.positiontransfer.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author mwacker
 */
public class AccountingPositionTransferTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "toAccountingAccount.id")
	private Short toAccountingAccountId;

	@SearchField(searchField = "toOffsettingAccountingAccount.id")
	private Short toOffsettingAccountingAccountId;

	@SearchField(searchField = "fromAccountingAccount.id")
	private Short fromAccountingAccountId;

	@SearchField(searchField = "fromOffsettingAccountingAccount.id")
	private Short fromOffsettingAccountingAccountId;

	@SearchField
	private Boolean receivingAsset;

	@SearchField
	private Boolean sendingAsset;

	@SearchField
	private Boolean internalTransfer;

	@SearchField
	private Boolean cash;

	@SearchField
	private Boolean collateral;

	@SearchField
	private Boolean collateralPosting;

	@SearchField
	private Boolean counterpartyCollateral;


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getToAccountingAccountId() {
		return this.toAccountingAccountId;
	}


	public void setToAccountingAccountId(Short toAccountingAccountId) {
		this.toAccountingAccountId = toAccountingAccountId;
	}


	public Short getToOffsettingAccountingAccountId() {
		return this.toOffsettingAccountingAccountId;
	}


	public void setToOffsettingAccountingAccountId(Short toOffsettingAccountingAccountId) {
		this.toOffsettingAccountingAccountId = toOffsettingAccountingAccountId;
	}


	public Short getFromAccountingAccountId() {
		return this.fromAccountingAccountId;
	}


	public void setFromAccountingAccountId(Short fromAccountingAccountId) {
		this.fromAccountingAccountId = fromAccountingAccountId;
	}


	public Short getFromOffsettingAccountingAccountId() {
		return this.fromOffsettingAccountingAccountId;
	}


	public void setFromOffsettingAccountingAccountId(Short fromOffsettingAccountingAccountId) {
		this.fromOffsettingAccountingAccountId = fromOffsettingAccountingAccountId;
	}


	public Boolean getReceivingAsset() {
		return this.receivingAsset;
	}


	public void setReceivingAsset(Boolean receivingAsset) {
		this.receivingAsset = receivingAsset;
	}


	public Boolean getSendingAsset() {
		return this.sendingAsset;
	}


	public void setSendingAsset(Boolean sendingAsset) {
		this.sendingAsset = sendingAsset;
	}


	public Boolean getInternalTransfer() {
		return this.internalTransfer;
	}


	public void setInternalTransfer(Boolean internalTransfer) {
		this.internalTransfer = internalTransfer;
	}


	public Boolean getCash() {
		return this.cash;
	}


	public void setCash(Boolean cash) {
		this.cash = cash;
	}


	public Boolean getCollateral() {
		return this.collateral;
	}


	public void setCollateral(Boolean collateral) {
		this.collateral = collateral;
	}


	public Boolean getCollateralPosting() {
		return this.collateralPosting;
	}


	public void setCollateralPosting(Boolean collateralPosting) {
		this.collateralPosting = collateralPosting;
	}


	public Boolean getCounterpartyCollateral() {
		return this.counterpartyCollateral;
	}


	public void setCounterpartyCollateral(Boolean counterpartyCollateral) {
		this.counterpartyCollateral = counterpartyCollateral;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
