package com.clifton.accounting.positiontransfer.booking;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author mwacker
 */
public class AccountingPositionTransferCollateralBookingRule extends AccountingPositionTransferBookingRule {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void applyRuleTransferDetail(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, Map<Integer, List<AccountingPosition>> securityPositionMap, Map<Integer, List<AccountingTransaction>> positionsToCloseMap) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		ValidationUtils.assertTrue(positionTransfer.getType().isCollateral(), "[" + this.getClass() + "] does not support transfer type [" + positionTransfer.getType().getLabel() + "].");

		if (positionTransfer.getType().isCash()) {
			applyCashRuleTransferDetail(journal, detail);
		}
		else {
			if (positionTransfer.getType().isCollateralPosting()) {
				if (!positionTransfer.getType().isCounterpartyCollateral()) {
					applyCollateralPostingTransfer(bookingSession, journal, detail, securityPositionMap);
				}
				else {
					applyCounterPartyCollateralPostingTransfer(journal, detail);
				}
			}
			else if (positionTransfer.getType().isCollateralReturn()) {
				List<AccountingTransaction> positionToCloseList = positionsToCloseMap.get(detail.getId());
				if (!positionTransfer.getType().isCounterpartyCollateral()) {
					applyCollateralReturnTransfer(bookingSession, journal, detail, positionToCloseList, securityPositionMap);
				}
				else {
					applyCounterPartyCollateralReturnTransfer(bookingSession, journal, detail, positionToCloseList, securityPositionMap);
				}
			}
		}
	}


	@Override
	protected Map<Integer, List<AccountingTransaction>> getPositionsToCloseMap(List<AccountingPositionTransferDetail> detailList) {
		Map<Integer, List<AccountingTransaction>> transferDetailAccountingTransactionMap = new HashMap<>();
		for (AccountingPositionTransferDetail detail : CollectionUtils.getIterable(detailList)) {
			AccountingPositionTransferType transferType = detail.getPositionTransfer().getType();
			if (!transferType.isCash() && transferType.isCollateralReturn()) {
				List<AccountingTransaction> positionToCloseList = getTransactionListForReturnTransfer(detail);
				transferDetailAccountingTransactionMap.put(detail.getId(), positionToCloseList);
			}
		}
		return transferDetailAccountingTransactionMap;
	}


	private void applyCollateralPostingTransfer(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, Map<Integer, List<AccountingPosition>> securityPositionMap) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		AccountingPositionTransferType transferType = positionTransfer.getType();
		AccountingPosition positionBeingClosed = findTransferPosition(detail, securityPositionMap);
		validateTransferDetail(detail, positionBeingClosed);
		adjustTransferDetailForPennyRounding(detail, positionBeingClosed);

		// add position close and security contribution or distribution
		AccountingJournalDetail closingDetail = generateJournalDetail(journal, detail, true, true, false, positionBeingClosed, detail.getExistingPosition(), transferType.getFromAccountingAccount());
		journal.addJournalDetail(closingDetail);

		// create the opening position
		AccountingJournalDetail openingDetail = generateJournalDetail(journal, detail, false, true, false, null, null, transferType.getToAccountingAccount());
		// needs to have the same parent and the closing detail
		openingDetail.setParentTransaction(closingDetail.getParentTransaction());
		openingDetail.setOriginalTransactionDate(closingDetail.getOriginalTransactionDate());

		if (!isTransferWithinSameAccounts(positionTransfer)) {
			AccountingJournalDetail receivableDetail = generateJournalDetail(journal, detail, false, true, false, null, null, transferType.getFromOffsettingAccountingAccount());
			receivableDetail.setParentDefinition(openingDetail);
			receivableDetail.setOriginalTransactionDate(openingDetail.getOriginalTransactionDate());
			journal.addJournalDetail(receivableDetail);
		}

		// asset backed bond and CDS transfers require full position close and then opening of remaining position with reduction to current remaining current face
		applyFullPositionCloseCollateral(detail, positionBeingClosed, journal, closingDetail);

		// need to populate booking session with position being closed used by gain/loss rule
		populateSessionWithClosingPosition(bookingSession, closingDetail, positionBeingClosed);


		// add opening position
		journal.addJournalDetail(openingDetail);

		if (!detail.getSecurity().isCurrency()) {
			ValidationUtils.assertFalse(MathUtils.isNullOrZero(detail.getQuantity()), "Transfer quantity is required for non-currency transfers: " + detail.getSecurity());
		}

		// asset backed bond transfers require reduction of original face to current face
		handleReductionOfOriginalFaceToCurrent(detail, journal, openingDetail);

		if (!isTransferWithinSameAccounts(positionTransfer)) {
			// add corresponding security contribution
			AccountingJournalDetail payableDetail = generateJournalDetail(journal, detail, false, true, true, null, null, transferType.getToOffsettingAccountingAccount());
			payableDetail.setParentDefinition(openingDetail);
			payableDetail.setOriginalTransactionDate(openingDetail.getOriginalTransactionDate());
			journal.addJournalDetail(payableDetail);
			if (InvestmentUtils.isNoPaymentOnOpen(detail.getSecurity()) && !MathUtils.isNullOrZero(payableDetail.getLocalDebitCredit())) {
				// example: IRS contribution not at cost - need Unrealized Gain / Loss
				AccountingJournalDetail gainLoss = generateJournalDetail(journal, detail, false, false, null);
				gainLoss.setParentDefinition(openingDetail);
				gainLoss.setOriginalTransactionDate(openingDetail.getOriginalTransactionDate());
				gainLoss.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_UNREALIZED));
				gainLoss.setLocalDebitCredit(MathUtils.negate(gainLoss.getLocalDebitCredit()));
				gainLoss.setBaseDebitCredit(MathUtils.negate(gainLoss.getBaseDebitCredit()));
				gainLoss.setDescription("Unrealized Gain / Loss from security contribution from transfer from external account");
				journal.addJournalDetail(gainLoss);
			}
		}
	}


	private void applyCounterPartyCollateralPostingTransfer(AccountingJournal journal, AccountingPositionTransferDetail detail) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		AccountingPositionTransferType transferType = positionTransfer.getType();
		validateTransferDetail(detail, null);

		// open the position in the to account
		journal.addJournalDetail(generateJournalDetail(journal, detail, false, true, false, null, null, transferType.getToAccountingAccount()));
		journal.addJournalDetail(generateJournalDetail(journal, detail, false, true, true, null, null, transferType.getToOffsettingAccountingAccount()));

		journal.addJournalDetail(generateJournalDetail(journal, detail, false, true, true, null, null, transferType.getFromAccountingAccount()));
		journal.addJournalDetail(generateJournalDetail(journal, detail, false, true, false, null, null, transferType.getFromOffsettingAccountingAccount()));
	}


	private void applyCollateralReturnTransfer(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, List<AccountingTransaction> positionToCloseList, Map<Integer, List<AccountingPosition>> securityPositionMap) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		AccountingPositionTransferType transferType = positionTransfer.getType();
		if (detail.getExistingPosition() != null) {
			AccountingPosition positionBeingClosed = findTransferPosition(detail, securityPositionMap);
			validateTransferDetail(detail, positionBeingClosed);
			AccountingTransaction transactionBeingClosed = (AccountingTransaction) positionBeingClosed.getOpeningTransaction();
			adjustTransferDetailForPennyRounding(detail, positionBeingClosed);

			// open the position in the to account
			AccountingJournalDetail open = generateJournalDetail(journal, detail, false, true, false, null, null, transferType.getToAccountingAccount());
			populateOpeningReturnPositionParentTransaction(open, transactionBeingClosed, false);

			journal.addJournalDetail(open);

			if (!detail.getSecurity().isCurrency()) {
				ValidationUtils.assertFalse(MathUtils.isNullOrZero(detail.getQuantity()), "Transfer quantity is required for non-currency transfers: " + detail.getSecurity());
			}

			// asset backed bond transfers require reduction of original face to current face
			handleReductionOfOriginalFaceToCurrent(detail, journal, open);

			// close the offsetting positions
			applyReturnPositionDetails(bookingSession, journal, detail, positionToCloseList, securityPositionMap);
		}
	}


	private void applyCounterPartyCollateralReturnTransfer(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, List<AccountingTransaction> positionToCloseList, Map<Integer, List<AccountingPosition>> securityPositionMap) {
		if (detail.getExistingPosition() != null) {
			AccountingPosition positionBeingClosed = findTransferPosition(detail, securityPositionMap);
			validateTransferDetail(detail, positionBeingClosed);
			adjustTransferDetailForPennyRounding(detail, positionBeingClosed);

			// close the offsetting positions
			applyReturnPositionDetails(bookingSession, journal, detail, positionToCloseList, securityPositionMap);
		}
	}


	private void applyReturnPositionDetails(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, List<AccountingTransaction> positionToCloseList, Map<Integer, List<AccountingPosition>> securityPositionMap) {
		// close the offsetting positions
		for (AccountingTransaction positionToClose : CollectionUtils.getIterable(positionToCloseList)) {
			AccountingPosition positionBeingClosed = findTransferPosition(detail, positionToClose, securityPositionMap);

			AccountingAccount accountingAccount = positionToClose.getAccountingAccount();
			// add position close and security contribution or distribution
			AccountingJournalDetail closingDetail = generateJournalDetail(journal, detail, true, true, false, positionBeingClosed, positionToClose, accountingAccount);
			journal.addJournalDetail(closingDetail);

			// asset backed bond and CDS transfers require full position close and then opening of remaining position with reduction to current remaining current face
			if (detail.isCollateralizedSecurity() && !MathUtils.isEqual(MathUtils.abs(detail.getOriginalFace()), MathUtils.abs(detail.getExistingPosition().getQuantity()))) {
				applyFullPositionCloseCollateral(detail, positionBeingClosed, journal, closingDetail, !closingDetail.isOpening() && MathUtils.isSameSignum(detail.getQuantity(), closingDetail.getQuantity()));
			}

			// need to populate booking session with position being closed used by gain/loss rule
			populateSessionWithClosingPosition(bookingSession, closingDetail, positionBeingClosed);
		}
	}


	private AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingPositionTransferDetail transferDetail, boolean closing, boolean position, boolean reverse, AccountingPosition positionBeingClosed, AccountingTransaction parentTransaction, AccountingAccount accountingAccount) {
		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(transferDetail.getId());
		detail.setSystemTable(getAccountingPositionTransferDetailTable());
		if (closing) {
			detail.setParentTransaction(parentTransaction == null ? transferDetail.getExistingPosition() : parentTransaction);
			if (detail.getParentTransaction() != null) {
				detail.setOriginalTransactionDate(detail.getParentTransaction().getOriginalTransactionDate());
			}
		}

		detail.setClientInvestmentAccount(isFromAccount(positionTransfer.getType(), accountingAccount) ? positionTransfer.getFromClientInvestmentAccount() : positionTransfer.getToClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(isFromAccount(positionTransfer.getType(), accountingAccount) ? positionTransfer.getFromHoldingInvestmentAccount() : positionTransfer.getToHoldingInvestmentAccount());
		detail.setInvestmentSecurity(transferDetail.getSecurity());
		detail.setAccountingAccount(accountingAccount);

		boolean noPaymentOnOpen = InvestmentUtils.isNoPaymentOnOpen(detail.getInvestmentSecurity());

		BigDecimal quantity = transferDetail.getQuantity();
		if ((positionBeingClosed != null) && !MathUtils.isSameSignum(positionBeingClosed.getQuantity(), quantity)) {
			quantity = quantity.negate();
		}

		detail.setQuantity(transferDetail.getSecurity().isCurrency() ? null : negateAmount(quantity, closing));
		if (!transferDetail.getSecurity().isCurrency() && reverse) {
			detail.setQuantity(detail.getQuantity().negate());
		}
		// TO transfers for IRS need to keep cost price (as opposed to FUTURES)
		if ((isInternalTransfer(positionTransfer) || (position && noPaymentOnOpen && InvestmentUtils.isInstrumentOfType(detail.getInvestmentSecurity().getInstrument(), InvestmentType.SWAPS))) && !closing) {
			detail.setPrice(transferDetail.getCostPrice());
		}
		else {
			detail.setPrice(transferDetail.getTransferPrice());
		}
		detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());

		detail.setDescription(getJournalDetailDescription(positionTransfer, closing, position));
		populateJournalDetailDates(transferDetail, detail, closing);

		populateJournalDetailAmounts(transferDetail, detail, closing || reverse, position, positionBeingClosed);
		detail.setOpening(reverse ? reverse : !closing);
		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setExecutingCompany(getExecutingCompany(transferDetail));
		return detail;
	}


	private boolean isFromAccount(AccountingPositionTransferType transferType, AccountingAccount accountingAccount) {
		return transferType.getFromAccountingAccount().equals(accountingAccount) || transferType.getFromOffsettingAccountingAccount().equals(accountingAccount);
	}


	@Override
	protected void validateTransferDetail(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();

		if (isClosingPositionRequired(positionTransfer.getType())) {
			super.validateTransferDetail(detail, positionBeingClosed);
		}
		if (positionBeingClosed != null && positionBeingClosed.getExecutingCompany() != null) {
			BusinessCompany executingCompany = getExecutingCompany(detail);
			ValidationUtils.assertTrue(executingCompany != null && executingCompany.equals(positionBeingClosed.getExecutingCompany()), "[" + (executingCompany != null ? executingCompany.getLabel() : null) + "] cannot close a position executed by [" + positionBeingClosed.getExecutingCompany().getLabel() + "].");
		}
	}


	private boolean isClosingPositionRequired(AccountingPositionTransferType type) {
		if (type.isCash()) {
			return false;
		}
		return ((type.isCollateral() && !type.isCounterpartyCollateral()) || (type.isCollateralReturn() && type.isCounterpartyCollateral()));
	}


	private List<AccountingTransaction> getTransactionListForReturnTransfer(AccountingPositionTransferDetail detail) {
		AccountingTransaction positionToClose = detail.getExistingPosition();
		List<AccountingTransaction> result = new ArrayList<>();
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setJournalId(positionToClose.getJournal().getId());
		searchForm.setFkFieldId(positionToClose.getFkFieldId());
		searchForm.setTableId(positionToClose.getSystemTable().getId());
		searchForm.setPositionAccountingAccount(true);
		List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);


		for (AccountingTransaction transaction : CollectionUtils.getIterable(transactionList)) {
			// leave out any non collateral or closing transactions
			if (!transaction.getAccountingAccount().isCollateral() || (transaction.isClosing() && (transaction.getParentTransaction() != null))) {
				continue;
			}

			if (!positionToClose.getAccountingAccount().equals(transaction.getAccountingAccount()) || positionToClose.getId().equals(transaction.getId())) {
				if (!result.contains(transaction)) {
					result.add(transaction);
				}
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Parent Transaction for closing positions will always be the effectedLot (the lot that it closes).
	 * For opening positions, it will also be affected lot, unless re-opening is for the same security and affected lot has
	 * parent that is also opening transaction (original opening).  In that case, it will point to the original opening
	 * to simplifying lot-level gain/loss attribution to the original opening in case we had multiple splits over time for
	 * the same security and wanted to calculate cumulative gains/losses for the original lot (3 levels deep vs N levels deep).
	 * <p>
	 * NOTE: this method must be called after other fields (InvestmentSecurity) were populated for the method parameters.
	 */
	protected void populateOpeningReturnPositionParentTransaction(AccountingJournalDetail journalDetail, AccountingTransaction affectedLot, boolean close) {
		journalDetail.setParentTransaction(affectedLot);
		AccountingTransaction parent = affectedLot.getParentTransaction();
		if (!close && parent != null && parent.isOpening() && parent.getInvestmentSecurity().equals(journalDetail.getInvestmentSecurity())) {
			parent = ObjectUtils.coalesce(parent.getParentTransaction(), parent);
			journalDetail.setParentTransaction(parent); // point openings to original opening for openings of the same security (re-open on split, spin-off)
			journalDetail.setOriginalTransactionDate(journalDetail.getParentTransaction().getOriginalTransactionDate());
		}
	}


	private void adjustTransferDetailForPennyRounding(AccountingPositionTransferDetail transferDetail, AccountingPosition positionBeingClosed) {
		if (positionBeingClosed != null && MathUtils.isLessThanOrEqual(transferDetail.getPositionCostBasis().abs().subtract(positionBeingClosed.getRemainingCostBasis().abs()).abs(), ROUNDING_THRESHOLD)) {
			transferDetail.setPositionCostBasis(positionBeingClosed.getRemainingCostBasis());
		}
	}


	private void applyCashRuleTransferDetail(AccountingJournal journal, AccountingPositionTransferDetail detail) {
		validateCashTransferDetail(detail);

		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		ValidationUtils.assertTrue(positionTransfer.getType().isCollateral(), "[" + this.getClass() + "] does not support transfer type [" + positionTransfer.getType().getLabel() + "].");

		AccountingPositionTransferType transferType = positionTransfer.getType();

		// open the position in the to account
		journal.addJournalDetail(generateCashJournalDetail(journal, detail, transferType.isCollateralPosting(), transferType.isCollateralPosting() ? transferType.getFromAccountingAccount() : transferType.getToAccountingAccount()));
		journal.addJournalDetail(generateCashJournalDetail(journal, detail, !transferType.isCollateralPosting(), transferType.isCollateralPosting() ? transferType.getFromOffsettingAccountingAccount() : transferType.getToOffsettingAccountingAccount()));

		journal.addJournalDetail(generateCashJournalDetail(journal, detail, !transferType.isCollateralPosting(), transferType.isCollateralPosting() ? transferType.getToAccountingAccount() : transferType.getFromAccountingAccount()));
		journal.addJournalDetail(generateCashJournalDetail(journal, detail, transferType.isCollateralPosting(), transferType.isCollateralPosting() ? transferType.getToOffsettingAccountingAccount() : transferType.getFromOffsettingAccountingAccount()));
	}


	private AccountingJournalDetail generateCashJournalDetail(AccountingJournal journal, AccountingPositionTransferDetail transferDetail, boolean closing, AccountingAccount accountingAccount) {

		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(transferDetail.getId());
		detail.setSystemTable(getAccountingPositionTransferDetailTable());


		detail.setClientInvestmentAccount(isFromAccount(positionTransfer.getType(), accountingAccount) ? positionTransfer.getFromClientInvestmentAccount() : positionTransfer.getToClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(isFromAccount(positionTransfer.getType(), accountingAccount) ? positionTransfer.getFromHoldingInvestmentAccount() : positionTransfer.getToHoldingInvestmentAccount());
		detail.setInvestmentSecurity(transferDetail.getSecurity());
		detail.setAccountingAccount(accountingAccount);


		BigDecimal amount = negateAmount(transferDetail.getPositionCostBasis(), closing);
		detail.setLocalDebitCredit(amount);
		detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());

		detail.setDescription(getJournalDetailDescription(positionTransfer, closing, false));
		populateJournalDetailDates(transferDetail, detail, closing);


		detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());
		detail.setBaseDebitCredit(MathUtils.multiply(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), 2));
		detail.setPositionCostBasis(BigDecimal.ZERO);


		detail.setOpening(true);
		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setExecutingCompany(getExecutingCompany(transferDetail));
		return detail;
	}


	private void validateCashTransferDetail(AccountingPositionTransferDetail detail) {
		ValidationUtils.assertTrue(detail.getSecurity().isCurrency(), "[" + detail.getSecurity().getLabel() + "] is not a valid security for transfer type [" + detail.getPositionTransfer().getType().getLabel() + "].");
	}
}
