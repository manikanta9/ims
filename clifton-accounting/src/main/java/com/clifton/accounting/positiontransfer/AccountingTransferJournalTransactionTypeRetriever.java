package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeRetriever;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingTransactionTypeEventJournalRetriever</code> handles the retrieval of {@link AccountingTransactionType}
 * based on the source entity ID for journal entries that are event journals.
 *
 * @author jgommels
 */
@Component
public class AccountingTransferJournalTransactionTypeRetriever implements AccountingTransactionTypeRetriever<AccountingPositionTransfer> {

	private AccountingTransactionTypeService accountingTransactionTypeService;


	@Override
	public AccountingTransactionType getAccountingTransactionType(@SuppressWarnings("unused") AccountingJournalDetailDefinition journalDetail, AccountingPositionTransfer transfer) {
		AccountingTransactionType transactionType;
		if (transfer.getType().isInternalTransfer()) {
			transactionType = getAccountingTransactionTypeService().getAccountingTransactionTypeByName(AccountingTransactionType.INTERNAL_TRANSFER);
		}
		else {
			transactionType = getAccountingTransactionTypeService().getAccountingTransactionTypeByName(AccountingTransactionType.NON_INTERNAL_TRANSFER);
		}

		return transactionType;
	}


	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.TRANSFER_JOURNAL;
	}


	public AccountingTransactionTypeService getAccountingTransactionTypeService() {
		return this.accountingTransactionTypeService;
	}


	public void setAccountingTransactionTypeService(AccountingTransactionTypeService accountingTransactionTypeService) {
		this.accountingTransactionTypeService = accountingTransactionTypeService;
	}
}
