package com.clifton.accounting.positiontransfer.search;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


/**
 * Search configurer for {@link AccountingPositionTransfer} queries.
 *
 * @author MikeH
 */
public class AccountingPositionTransferSearchFormConfigurer extends HibernateSearchFormConfigurer {

	public AccountingPositionTransferSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		AccountingPositionTransferSearchForm searchForm = (AccountingPositionTransferSearchForm) getSortableSearchForm();
		if (searchForm.getInvestmentGroupId() != null) {
			// Filter on instrument group ID if specified
			DetachedCriteria sub = DetachedCriteria.forClass(AccountingPositionTransferDetail.class, "aptd");

			// Create link to specify instrument group ID
			sub.setProjection(Projections.id());
			sub.createAlias("security", "security");
			sub.createAlias("security.instrument", "instrument");
			sub.createAlias("instrument.groupItemList", "groupMapping");
			sub.createAlias("groupMapping.referenceOne", "groupItem");
			sub.add(Restrictions.eq("groupItem.group.id", searchForm.getInvestmentGroupId()));

			// Create link from sub-query to outer-query
			sub.add(Restrictions.eqProperty("positionTransfer.id", criteria.getAlias() + ".id"));

			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getClientInvestmentAccountGroupId() != null) {
			// Filter on client account group ID if specified
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "iaga");

			// Create link to specify client account group ID
			sub.setProjection(Projections.id());
			sub.add(Restrictions.eq("referenceOne.id", searchForm.getClientInvestmentAccountGroupId()));

			// Create link from sub-query to outer-query
			sub.add(Restrictions.or(
					Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".toClientInvestmentAccount.id"),
					Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".fromClientInvestmentAccount.id")
			));
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getFromExcludeHoldingInvestmentAccountGroupId() != null || searchForm.getFromExcludeHoldingInvestmentAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "efa");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("fromHoldingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getFromExcludeHoldingInvestmentAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getFromExcludeHoldingInvestmentAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getFromExcludeHoldingInvestmentAccountGroupIds()));
			}
			criteria.add(Subqueries.notExists(sub));
		}

		if (searchForm.getToExcludeHoldingInvestmentAccountGroupId() != null || searchForm.getToExcludeHoldingInvestmentAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "eta");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("toHoldingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getToExcludeHoldingInvestmentAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getToExcludeHoldingInvestmentAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getToExcludeHoldingInvestmentAccountGroupIds()));
			}
			criteria.add(Subqueries.notExists(sub));
		}
	}
}
