package com.clifton.accounting.positiontransfer.booking;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>AccountingPositionTransferBookingRule</code> class is a booking rule that creates a closing journal detail for each position being transferred out and an opening
 * journal detail for each position that is transferred in.
 *
 * @author vgomelsky
 */
public class AccountingPositionTransferBookingRule implements AccountingBookingRule<AccountingPositionTransfer> {

	protected static final BigDecimal ROUNDING_THRESHOLD = new BigDecimal("0.05");

	private AccountingAccountService accountingAccountService;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentCalculator investmentCalculator;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<AccountingPositionTransfer> bookingSession) {
		AccountingPositionTransfer positionTransfer = bookingSession.getBookableEntity();

		//Get list of ids for all unique securities
		//Create the command, get the position list, and create the position by security map
		Map<Integer, List<AccountingPosition>> securityPositionMap = new HashMap<>();
		Map<Integer, List<AccountingTransaction>> positionsToCloseMap = new HashMap<>();
		if (positionTransfer.getFromClientInvestmentAccount() != null && positionTransfer.getFromHoldingInvestmentAccount() != null) {
			//First get the positions from the current detail list
			List<Long> existingPositionIdList = positionTransfer.getDetailList().stream().filter(detail -> detail.getExistingPosition() != null).map(detail -> detail.getExistingPosition().getId()).collect(Collectors.toList());
			//Now get the positions to close map, if applicable, and add those transaction ids to the lookup so all positions needed can be retrieved at one time.
			positionsToCloseMap = getPositionsToCloseMap(positionTransfer.getDetailList());
			for (List<AccountingTransaction> positionToCloseList : CollectionUtils.getIterable(positionsToCloseMap.values())) {
				existingPositionIdList.addAll(positionToCloseList.stream().map(BaseSimpleEntity::getId).collect(Collectors.toList()));
			}
			//Now create the command using the distinct set of position ids compiled above
			AccountingPositionCommand command = new AccountingPositionCommand();
			command.setExistingPositionIds(existingPositionIdList.stream().distinct().toArray(Long[]::new));
			//Add them to the position map for easy lookup later.
			securityPositionMap = BeanUtils.getBeansMap(getAccountingPositionService().getAccountingPositionListUsingCommand(command),
					accountingPosition -> accountingPosition.getInvestmentSecurity().getId());
		}
		AccountingJournal journal = bookingSession.getJournal();
		journal.setDescription(positionTransfer.getNote());
		for (AccountingPositionTransferDetail detail : positionTransfer.getDetailList()) {
			applyRuleTransferDetail(bookingSession, journal, detail, securityPositionMap, positionsToCloseMap);
		}
	}


	//Can be overridden to provide additional position ids to look up ahead of time;
	//see implementation in 'AccountingPositionTransferCollateralBookingRule' this is used to
	//increase performance when handling return transfers so that the positionsToClose can be
	//looked up ahead of time rather than inside the loop where 'applyRuleTransferDetail' is executed.
	protected Map<Integer, List<AccountingTransaction>> getPositionsToCloseMap(List<AccountingPositionTransferDetail> detailList) {
		return new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void applyRuleTransferDetail(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournal journal, AccountingPositionTransferDetail detail, Map<Integer, List<AccountingPosition>> securityPositionMap, Map<Integer, List<AccountingTransaction>> positionsToCloseMap) {
		AccountingPositionTransfer positionTransfer = detail.getPositionTransfer();
		// close or reduce existing position if transferring from our account
		if (detail.getExistingPosition() != null) {
			AccountingPosition positionBeingClosed = findTransferPosition(detail, securityPositionMap);
			validateTransferDetail(detail, positionBeingClosed);

			// add position close and security contribution or distribution
			AccountingJournalDetail closingDetail = generateJournalDetail(journal, detail, true, true, positionBeingClosed);
			journal.addJournalDetail(closingDetail);
			if (!isTransferWithinSameAccounts(positionTransfer)) {
				journal.addJournalDetail(generateJournalDetail(journal, detail, true, false, positionBeingClosed));
			}

			// asset backed bond and CDS transfers require full position close and then opening of remaining position with reduction to current remaining current face
			applyFullPositionClose(detail, positionBeingClosed, journal, closingDetail);

			// need to populate booking session with position being closed used by gain/loss rule
			populateSessionWithClosingPosition(bookingSession, closingDetail, positionBeingClosed);
		}

		// open the new position if our account receives it
		if (!positionTransfer.getType().isFromOurAccount()) {
			// add opening position
			AccountingJournalDetail open = generateJournalDetail(journal, detail, false, true, null);
			journal.addJournalDetail(open);

			if (!detail.getSecurity().isCurrency()) {
				ValidationUtils.assertFalse(MathUtils.isNullOrZero(detail.getQuantity()), "Transfer quantity is required for non-currency transfers: " + detail.getSecurity());
			}

			// asset backed bond transfers require reduction of original face to current face
			handleReductionOfOriginalFaceToCurrent(detail, journal, open);

			if (!isTransferWithinSameAccounts(positionTransfer)) {
				// add corresponding security contribution
				AccountingJournalDetail contribution = generateJournalDetail(journal, detail, false, false, null);
				journal.addJournalDetail(contribution);
				if (InvestmentUtils.isNoPaymentOnOpen(detail.getSecurity()) && !MathUtils.isNullOrZero(contribution.getLocalDebitCredit())) {
					// example: IRS contribution not at cost - need Unrealized Gain / Loss
					AccountingJournalDetail gainLoss = generateJournalDetail(journal, detail, false, false, null);
					gainLoss.setParentDefinition(open);
					gainLoss.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_UNREALIZED));
					gainLoss.setLocalDebitCredit(MathUtils.negate(gainLoss.getLocalDebitCredit()));
					gainLoss.setBaseDebitCredit(MathUtils.negate(gainLoss.getBaseDebitCredit()));
					gainLoss.setDescription("Unrealized Gain / Loss from security contribution from transfer from external account");
					journal.addJournalDetail(gainLoss);
				}
			}
		}
	}


	protected void validateTransferDetail(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed) {
		String symbol = detail.getSecurity().getSymbol();
		ValidationUtils.assertNotNull(positionBeingClosed, symbol + " position that is being transferred is no longer held in the FROM client account on the transaction date.");

		boolean shortQuantity = MathUtils.isLessThan(positionBeingClosed.getRemainingQuantity(), BigDecimal.ZERO);
		boolean shortTransfer = MathUtils.isLessThan(detail.getQuantity(), BigDecimal.ZERO);

		// Verify transfers same sign as held
		ValidationUtils.assertTrue(shortQuantity == shortTransfer, symbol + " transfer amount [" + CoreMathUtils.formatNumberDecimal(detail.getQuantity()) + "] must be the same sign as current position ["
				+ CoreMathUtils.formatNumberDecimal(positionBeingClosed.getRemainingQuantity()) + "]. "
				+ ((shortQuantity) ? "Cannot transfer long positions if lot is short positions." : "Cannot transfer short positions if lot is long positions."));
		// If passes above, then same sign, just check absolute value of quantities
		// for Cannot close/transfer more than a lot has
		ValidationUtils.assertTrue(MathUtils.abs(positionBeingClosed.getRemainingQuantity()).compareTo(MathUtils.abs(detail.getQuantity())) >= 0,
				"Can't transfer more than a lot has.  Trying to transfer " + detail.getQuantity() + " and " + symbol + " position only has " + positionBeingClosed.getRemainingQuantity());

		// validation: find any transactions related to the transfer transaction that are after the transaction date
		Date transactionDate = detail.getPositionTransfer().getTransactionDate();
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setParentId(detail.getExistingPosition().getId());
		searchForm.setMinTransactionDate(DateUtils.addDays(transactionDate, 1));
		List<AccountingTransaction> affectedTransactions = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (Iterator<AccountingTransaction> iterator = affectedTransactions.iterator(); iterator.hasNext(); ) {
			// accrual reversals are fine
			AccountingTransaction tran = iterator.next();
			if (!tran.getAccountingAccount().isPosition()) {
				AccountingJournal parentJournal = tran.getJournal().getParent();
				if (parentJournal != null) {
					searchForm = new AccountingTransactionSearchForm();
					searchForm.setJournalId(parentJournal.getId());
					searchForm.setParentId(detail.getExistingPosition().getId());
					searchForm.setMinTransactionDate(DateUtils.addDays(transactionDate, 1));
					if (CollectionUtils.isEmpty(getAccountingTransactionService().getAccountingTransactionList(searchForm))) {
						// accrual records pre-dated this transfer: can remove
						iterator.remove();
					}
				}
			}
		}
		ValidationUtils.assertEmpty(affectedTransactions, "There are " + affectedTransactions.size() + " transactions [" + affectedTransactions + "] after the Transaction Date of "
				+ transactionDate + " that affect transferred lot: [" + detail.getExistingPosition() + "]. Only accrual reversals are allowed.  You need to unbook these transactions first.");
	}


	protected AccountingPosition findTransferPosition(AccountingPositionTransferDetail detail, Map<Integer, List<AccountingPosition>> securityPositionMap) {
		return findTransferPosition(detail, detail.getExistingPosition(), securityPositionMap);
	}


	protected AccountingPosition findTransferPosition(AccountingPositionTransferDetail detail, AccountingTransaction existingPosition, Map<Integer, List<AccountingPosition>> positionsBySecurityMap) {
		List<AccountingPosition> currentPositions = positionsBySecurityMap.get(detail.getSecurity().getId());
		AccountingPosition matchingPosition = CollectionUtils.getFirstElement(CollectionUtils.getStream(currentPositions).filter(position -> position.getId().equals(existingPosition.getId())).collect(Collectors.toList()));
		ValidationUtils.assertNotNull(matchingPosition, detail.getSecurity().getSymbol() + " position that is being transferred is no longer held in the FROM client account on the transaction date.");
		return matchingPosition;
	}


	/**
	 * Returns true if the transfer is between the same client and holding accounts: collateral to non-collateral or vice versa.
	 */
	protected boolean isTransferWithinSameAccounts(AccountingPositionTransfer positionTransfer) {
		if (CompareUtils.isEqual(positionTransfer.getFromHoldingInvestmentAccount(), positionTransfer.getToHoldingInvestmentAccount())) {
			return CompareUtils.isEqual(positionTransfer.getFromClientInvestmentAccount(), positionTransfer.getToClientInvestmentAccount());
		}
		return false;
	}


	protected void populateSessionWithClosingPosition(BookingSession<AccountingPositionTransfer> bookingSession, AccountingJournalDetail closingDetail, AccountingPosition positionBeingClosed) {
		Map<String, List<BookingPosition>> positionsByClient = bookingSession.getCurrentPositions();
		String key = bookingSession.getPositionKey(closingDetail);
		List<BookingPosition> currentOpenPositions = positionsByClient.get(key);
		if (currentOpenPositions == null) {
			currentOpenPositions = new ArrayList<>();
		}
		BookingPosition bookingPosition = new BookingPosition(positionBeingClosed);
		currentOpenPositions.add(bookingPosition);
		bookingSession.getCurrentPositions().put(key, currentOpenPositions);

		// update remaining quantity to close to avoid double closing
		bookingPosition.setRemainingQuantity(bookingPosition.getRemainingQuantity().add(closingDetail.getQuantity()));
	}


	protected void applyFullPositionClose(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed, AccountingJournal journal, AccountingJournalDetail closingDetail) {
		applyFullPositionCloseInternal(detail, positionBeingClosed, journal, closingDetail, false, false);
	}


	protected void applyFullPositionClose(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed, AccountingJournal journal, AccountingJournalDetail closingDetail, boolean reverse) {
		applyFullPositionCloseInternal(detail, positionBeingClosed, journal, closingDetail, reverse, false);
	}


	protected void applyFullPositionCloseCollateral(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed, AccountingJournal journal, AccountingJournalDetail closingDetail) {
		applyFullPositionCloseInternal(detail, positionBeingClosed, journal, closingDetail, false, true);
	}


	protected void applyFullPositionCloseCollateral(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed, AccountingJournal journal, AccountingJournalDetail closingDetail, boolean reverse) {
		applyFullPositionCloseInternal(detail, positionBeingClosed, journal, closingDetail, reverse, true);
	}


	protected void applyFullPositionCloseInternal(AccountingPositionTransferDetail detail, AccountingPosition positionBeingClosed, AccountingJournal journal, AccountingJournalDetail closingDetail, boolean reverse, boolean collateral) {
		// asset backed bond and CDS transfers require full position close and then opening of remaining position with reduction to current remaining current face
		if (detail.isCollateralizedSecurity()) {
			String symbol = detail.getSecurity().getSymbol();
			if (detail.getSecurity().getInstrument().getHierarchy().getFactorChangeEventType() == null) {
				throw new FieldValidationException("Only securities that support Factor Change event can have Original Face that is different from Quantity: " + symbol, "originalFace");
			}
			else {
				ValidationUtils.assertTrue(detail.getOriginalFace().compareTo(detail.getQuantity()) >= 0, "Cannot transfer more quantity than '" + symbol + "' originally had.");

				BigDecimal quantity = reverse ? detail.getOriginalFace().negate() : detail.getOriginalFace();
				BigDecimal positionCostBasis = reverse ? detail.getPositionCostBasis().negate() : detail.getPositionCostBasis();

				boolean factorIsOne = MathUtils.isEqual(detail.getOriginalFace(), detail.getQuantity());
				AccountingJournalDetail remainingPosition = generateJournalDetail(journal, detail, true, true, null);
				remainingPosition.setAccountingAccount(closingDetail.getAccountingAccount());
				remainingPosition.setClientInvestmentAccount(closingDetail.getClientInvestmentAccount());
				remainingPosition.setHoldingInvestmentAccount(closingDetail.getHoldingInvestmentAccount());
				remainingPosition.setOpening(true);
				// for collateral track the original parent
				if (collateral) {
					AccountingTransaction t = getAccountingTransactionService().getAccountingTransaction(positionBeingClosed.getId());
					remainingPosition.setParentTransaction(t.getParentTransaction() == null ? t : t.getParentTransaction());
					if (remainingPosition.getParentTransaction() != null) {
						remainingPosition.setOriginalTransactionDate(remainingPosition.getParentTransaction().getOriginalTransactionDate());
					}
				}
				else {
					remainingPosition.setParentTransaction(null);
				}
				remainingPosition.setQuantity(MathUtils.subtract(positionBeingClosed.getOpeningQuantityClean(), quantity));
				remainingPosition.setLocalDebitCredit(factorIsOne ? MathUtils.subtract(positionBeingClosed.getRemainingCostBasis(), positionCostBasis) : getInvestmentCalculator().calculateNotional(detail.getSecurity(), detail.getCostPrice(), remainingPosition.getQuantity(), BigDecimal.ONE));
				remainingPosition.setBaseDebitCredit(MathUtils.multiply(remainingPosition.getLocalDebitCredit(), detail.getExchangeRateToBase(), 2));
				remainingPosition.setPositionCostBasis(remainingPosition.getLocalDebitCredit());
				remainingPosition.setDescription("Position opening" + remainingPosition.getDescription().substring("Position close".length()));
				remainingPosition.setExecutingCompany(closingDetail.getExecutingCompany());
				journal.addJournalDetail(remainingPosition);

				// update closingDetail to full position close and add new remaining position at remaining original face and reduction of this new position to remaining current face
				closingDetail.setQuantity(positionBeingClosed.getRemainingQuantity().negate());
				closingDetail.setLocalDebitCredit(positionBeingClosed.getRemainingCostBasis().negate());
				closingDetail.setBaseDebitCredit(positionBeingClosed.getRemainingBaseDebitCredit().negate());
				closingDetail.setPositionCostBasis(closingDetail.getLocalDebitCredit());

				if (!factorIsOne) {
					AccountingJournalDetail faceReductionToCurrent = generateJournalDetail(journal, detail, true, true, null);
					faceReductionToCurrent.setParentTransaction(null);
					faceReductionToCurrent.setParentDefinition(remainingPosition);
					faceReductionToCurrent.setOriginalTransactionDate(remainingPosition.getOriginalTransactionDate());
					faceReductionToCurrent.setDescription("Reducing " + InvestmentUtils.getUnadjustedQuantityFieldName(detail.getSecurity()) + " to " + InvestmentUtils.getQuantityFieldName(detail.getSecurity()) + " for position transfer");
					faceReductionToCurrent.setOpening(false);
					faceReductionToCurrent.setQuantity(MathUtils.subtract(MathUtils.add(positionBeingClosed.getRemainingQuantity(), faceReductionToCurrent.getQuantity()), remainingPosition.getQuantity()));
					faceReductionToCurrent.setLocalDebitCredit(MathUtils.subtract(MathUtils.add(positionBeingClosed.getRemainingCostBasis(), faceReductionToCurrent.getLocalDebitCredit()), remainingPosition.getLocalDebitCredit()));
					faceReductionToCurrent.setBaseDebitCredit(MathUtils.subtract(MathUtils.add(positionBeingClosed.getRemainingBaseDebitCredit(), faceReductionToCurrent.getBaseDebitCredit()), remainingPosition.getBaseDebitCredit()));
					faceReductionToCurrent.setPositionCostBasis(faceReductionToCurrent.getLocalDebitCredit());
					journal.addJournalDetail(faceReductionToCurrent);
				}
			}
		}
	}


	protected void handleReductionOfOriginalFaceToCurrent(AccountingPositionTransferDetail detail, AccountingJournal journal, AccountingJournalDetail openingJournalDetail) {
		// asset backed bond transfers require reduction of original face to current face
		if (detail.isCollateralizedSecurity() && !MathUtils.isEqual(detail.getOriginalFace(), detail.getQuantity())) {
			String symbol = detail.getSecurity().getSymbol();
			if (detail.getSecurity().getInstrument().getHierarchy().getFactorChangeEventType() == null) {
				throw new FieldValidationException("Only securities that support Factor Change event can have Original Face that is different from Quantity: " + symbol, "originalFace");
			}
			ValidationUtils.assertTrue(detail.getOriginalFace().compareTo(detail.getQuantity()) >= 0, "Cannot transfer more quantity than '" + symbol + "' originally had.");

			// asset backed bonds with current factor <> 1 generate 2 journal details:
			// open at full face and then reduce to current face
			BigDecimal originalCostBasis = MathUtils.multiply(detail.getPositionCostBasis(), MathUtils.divide(detail.getOriginalFace(), detail.getQuantity()), 2);
			BigDecimal originalCostBasisBase = MathUtils.multiply(originalCostBasis, detail.getExchangeRateToBase(), 2);
			openingJournalDetail.setQuantity(detail.getOriginalFace());
			openingJournalDetail.setLocalDebitCredit(originalCostBasis);
			openingJournalDetail.setBaseDebitCredit(originalCostBasisBase);
			openingJournalDetail.setPositionCostBasis(originalCostBasis);

			AccountingJournalDetail faceReductionToCurrent = generateJournalDetail(journal, detail, false, true, null);
			faceReductionToCurrent.setParentDefinition(openingJournalDetail);
			faceReductionToCurrent.setDescription("Reducing " + InvestmentUtils.getUnadjustedQuantityFieldName(detail.getSecurity()) + " to " + InvestmentUtils.getQuantityFieldName(detail.getSecurity()) + " for position transfer");
			faceReductionToCurrent.setOpening(false);
			faceReductionToCurrent.setQuantity(MathUtils.subtract(detail.getQuantity(), detail.getOriginalFace()));
			faceReductionToCurrent.setLocalDebitCredit(MathUtils.subtract(faceReductionToCurrent.getLocalDebitCredit(), originalCostBasis).setScale(2, BigDecimal.ROUND_HALF_UP));
			faceReductionToCurrent.setBaseDebitCredit(MathUtils.subtract(faceReductionToCurrent.getBaseDebitCredit(), originalCostBasisBase).setScale(2, BigDecimal.ROUND_HALF_UP));
			faceReductionToCurrent.setPositionCostBasis(MathUtils.subtract(faceReductionToCurrent.getPositionCostBasis(), originalCostBasis).setScale(2, BigDecimal.ROUND_HALF_UP));
			journal.addJournalDetail(faceReductionToCurrent);
		}
	}


	protected AccountingJournalDetail generateJournalDetail(AccountingJournal journal, AccountingPositionTransferDetail transferDetail, boolean closing, boolean position, AccountingPosition positionBeingClosed) {
		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();
		AccountingJournalDetail detail = new AccountingJournalDetail();
		detail.setJournal(journal);
		detail.setFkFieldId(transferDetail.getId());
		detail.setSystemTable(getAccountingPositionTransferDetailTable());
		if (closing) {
			detail.setParentTransaction(transferDetail.getExistingPosition());
		}

		detail.setClientInvestmentAccount(closing ? positionTransfer.getFromClientInvestmentAccount() : positionTransfer.getToClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(closing ? positionTransfer.getFromHoldingInvestmentAccount() : positionTransfer.getToHoldingInvestmentAccount());
		detail.setInvestmentSecurity(transferDetail.getSecurity());
		detail.setAccountingAccount(getJournalDetailAccountingAccount(transferDetail, closing, position));

		boolean noPaymentOnOpen = InvestmentUtils.isNoPaymentOnOpen(detail.getInvestmentSecurity());
		detail.setQuantity(transferDetail.getSecurity().isCurrency() ? null : negateAmount(transferDetail.getQuantity(), closing));
		// TO transfers for IRS need to keep cost price (as opposed to FUTURES)
		if ((isInternalTransfer(positionTransfer) || (position && noPaymentOnOpen && InvestmentUtils.isInstrumentOfType(detail.getInvestmentSecurity().getInstrument(), InvestmentType.SWAPS))) && !closing) {
			detail.setPrice(transferDetail.getCostPrice());
		}
		else {
			detail.setPrice(transferDetail.getTransferPrice());
		}
		detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());

		detail.setDescription(getJournalDetailDescription(positionTransfer, closing, position));
		populateJournalDetailDates(transferDetail, detail, closing);

		populateJournalDetailAmounts(transferDetail, detail, closing, position, positionBeingClosed);
		detail.setOpening(!closing);
		detail.setPositionCommission(BigDecimal.ZERO);
		detail.setExecutingCompany(getExecutingCompany(transferDetail));
		return detail;
	}


	protected BusinessCompany getExecutingCompany(AccountingPositionTransferDetail transferDetail) {
		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();
		BusinessCompany result = null;
		if (transferDetail.getExistingPosition() != null && transferDetail.getExistingPosition().getExecutingCompany() != null) {
			result = transferDetail.getExistingPosition().getExecutingCompany();
		}
		else if (positionTransfer.getFromHoldingInvestmentAccount() != null) {
			result = positionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany();
		}
		return result;
	}


	protected BigDecimal negateAmount(BigDecimal amount, boolean negate) {
		if (amount == null) {
			return null;
		}
		return negate ? amount.negate() : amount;
	}


	protected boolean isInternalTransfer(AccountingPositionTransfer positionTransfer) {
		return (positionTransfer.getFromHoldingInvestmentAccount() != null && positionTransfer.getToHoldingInvestmentAccount() != null
				&& positionTransfer.getFromHoldingInvestmentAccount().equals(positionTransfer.getToHoldingInvestmentAccount()));
	}


	protected SystemTable getAccountingPositionTransferDetailTable() {
		SystemTable result = getSystemSchemaService().getSystemTableByName("AccountingPositionTransferDetail");
		AssertUtils.assertNotNull(result, "Cannot find system table with name: AccountingPositionTransferDetail");
		return result;
	}


	protected void populateJournalDetailDates(AccountingPositionTransferDetail transferDetail, AccountingJournalDetail detail, boolean closing) {
		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();
		detail.setSettlementDate(positionTransfer.getSettlementDate());
		detail.setOriginalTransactionDate(positionTransfer.isUseOriginalTransactionDate() ? transferDetail.getOriginalPositionOpenDate() : positionTransfer.getTransactionDate());
		if (closing) {
			if (detail.getParentTransaction() != null) {
				detail.setOriginalTransactionDate(detail.getParentTransaction().getOriginalTransactionDate());
			}
			else {
				detail.setOriginalTransactionDate(transferDetail.getOriginalPositionOpenDate());
			}
		}
		else if (isInternalTransfer(positionTransfer)) {
			detail.setOriginalTransactionDate(transferDetail.getOriginalPositionOpenDate());
		}
		detail.setTransactionDate(positionTransfer.getTransactionDate());
	}


	protected void populateJournalDetailAmounts(AccountingPositionTransferDetail transferDetail, AccountingJournalDetail detail, boolean closing, boolean position, AccountingPosition positionBeingClosed) {
		boolean noPaymentOnOpen = InvestmentUtils.isNoPaymentOnOpen(detail.getInvestmentSecurity());
		AccountingPositionTransfer positionTransfer = transferDetail.getPositionTransfer();

		// NEED TO USE SETTLEMENT DATE OF ORIGINAL OPEN
		Date lookupDate = transferDetail.getExistingPosition() == null ? transferDetail.getOriginalPositionOpenDate() : getTransferPositionOriginalSettlementDate(transferDetail.getExistingPosition());
		if (closing || isInternalTransfer(positionTransfer)) {
			if (transferDetail.getSecurity().isCurrency() && !InvestmentUtils.getClientAccountBaseCurrency(detail).equals(transferDetail.getSecurity())) {
				// CURRENCY TRANSFER
				detail.setPositionCostBasis(negateAmount(getInvestmentCalculator().calculateNotional(transferDetail.getSecurity(), BigDecimal.ONE, transferDetail.getQuantity(), BigDecimal.ONE), closing));
				detail.setLocalDebitCredit(negateAmount(detail.getPositionCostBasis(), !position));
				detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());
				detail.setBaseDebitCredit(MathUtils.multiply(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), 2));
			}
			else {
				// close at current price (non-currency positions)
				detail.setPositionCostBasis(negateAmount(
						getInvestmentCalculator().calculateNotional(transferDetail.getSecurity(), transferDetail.getTransferPrice(), transferDetail.getQuantity(), lookupDate), closing));
			}
		}
		else {
			// open using transfer price: recalc this just in case the UI event didn't fire properly
			detail.setPositionCostBasis(getInvestmentCalculator().calculateNotional(transferDetail.getSecurity(),
					transferDetail.getSecurity().getInstrument().getHierarchy().isCurrency() ? BigDecimal.ONE : detail.getPrice(), transferDetail.getQuantity(), lookupDate));
		}

		// adjust for penny rounding if full close but within a penny
		if (MathUtils.isLessThanOrEqual(detail.getPositionCostBasis().abs().subtract(transferDetail.getPositionCostBasis().abs()).abs(), ROUNDING_THRESHOLD)) {
			detail.setPositionCostBasis(transferDetail.getPositionCostBasis());
		}
		if (!MathUtils.isSameSignum(detail.getPositionCostBasis(), detail.getQuantity())) {
			detail.setPositionCostBasis(detail.getPositionCostBasis().negate());
		}

		if (noPaymentOnOpen) {
			if (positionTransfer.getType().isToOurAccount() && !position && !MathUtils.isEqual(transferDetail.getCostPrice(), transferDetail.getTransferPrice())) {
				// contribution/distribution of securities with no payment on open
				BigDecimal oldNotional = getInvestmentCalculator().calculateNotional(transferDetail.getSecurity(), transferDetail.getCostPrice(), detail.getQuantity(), lookupDate);
				BigDecimal newNotional = getInvestmentCalculator().calculateNotional(transferDetail.getSecurity(), detail.getPrice(), detail.getQuantity(), lookupDate);
				detail.setLocalDebitCredit(MathUtils.subtract(oldNotional, newNotional));
			}
			else {
				detail.setLocalDebitCredit(BigDecimal.ZERO);
			}
		}
		else {
			// negate contribution/distribution amount: the opposite of position
			detail.setLocalDebitCredit(negateAmount(detail.getPositionCostBasis(), !position));
		}
		detail.setExchangeRateToBase(transferDetail.getExchangeRateToBase());
		if (positionBeingClosed != null && MathUtils.isEqual(positionBeingClosed.getRemainingQuantity().abs(), detail.getQuantity().abs())
				&& MathUtils.isEqual(positionBeingClosed.getPrice(), detail.getPrice()) && MathUtils.isEqual(positionBeingClosed.getExchangeRateToBase(), detail.getExchangeRateToBase())) {
			// FULL CLOSE: avoid penny rounding errors for full close when there is no gain/loss: same price, FX Rate
			// NOTE: also use the same logic for openings: must match corresponding close
			if (!InvestmentUtils.isNoPaymentOnOpen(detail.getInvestmentSecurity())) {
				detail.setLocalDebitCredit(negateAmount(positionBeingClosed.getRemainingCostBasis(), position && closing));
			}
			detail.setBaseDebitCredit(negateAmount(positionBeingClosed.getRemainingBaseDebitCredit(), position && closing));
			detail.setPositionCostBasis(negateAmount(positionBeingClosed.getRemainingCostBasis(), position && closing));
		}
		else {
			detail.setBaseDebitCredit(MathUtils.multiply(detail.getLocalDebitCredit(), detail.getExchangeRateToBase(), 2));
		}
		if (!position) {
			detail.setPositionCostBasis(BigDecimal.ZERO);
		}
	}


	/**
	 * Returns the original Settlement Date of the specified position.
	 */
	private Date getTransferPositionOriginalSettlementDate(AccountingTransaction position) {
		position = ObjectUtils.coalesce(position.getParentTransaction(), position);
		if (DateUtils.isDateAfter(position.getTransactionDate(), position.getOriginalTransactionDate()) && position.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted()) {
			// inflation linked security with earlier opening: need original settlement date in order to lookup correct index ratio
			// NOTE: this is simplified implementation that should work most of the time. If transferred lot was on REPO multiple times,
			// then we would need to go recursively through each REPO/Transfer to get to the original lot.  The trick with REPO's is that
			// there is no direct link to the opening lot: would likely need to get the journal for the lot, then find the closing lot
			// that corresponds to current open lot matching on quantity, original transaction date, etc.
			return getInvestmentCalculator().calculateSettlementDate(position.getInvestmentSecurity(), null, position.getOriginalTransactionDate());
		}
		return position.getSettlementDate();
	}


	private AccountingAccount getJournalDetailAccountingAccount(AccountingPositionTransferDetail transferDetail, boolean closing, boolean position) {
		if (!position) {
			// security contribution/distribution line
			String accountName = closing ? AccountingAccount.EQUITY_SECURITY_DISTRIBUTION : AccountingAccount.EQUITY_SECURITY_CONTRIBUTION;
			return getAccountingAccountService().getAccountingAccountByName(accountName);
		}
		else if (closing) {
			// closing position: always close the same GL Account as this of position being closed
			return transferDetail.getExistingPosition().getAccountingAccount();
		}

		// opening position: opening GL Account depends on "collateralTransfer" selection
		String accountName = transferDetail.getPositionTransfer().isCollateralTransfer() ? (transferDetail.getSecurity().isCurrency() ? AccountingAccount.ASSET_CURRENCY_COLLATERAL
				: AccountingAccount.ASSET_POSITION_COLLATERAL) : (transferDetail.getSecurity().isCurrency() ? AccountingAccount.ASSET_CURRENCY : AccountingAccount.ASSET_POSITION);
		return getAccountingAccountService().getAccountingAccountByName(accountName);
	}


	protected String getJournalDetailDescription(AccountingPositionTransfer positionTransfer, boolean closing, boolean position) {
		String result;
		if (position) {
			if (closing) {
				if (positionTransfer.getType().isBetweenOurAccounts()) {
					result = "Position close from transfer to " + positionTransfer.getToClientInvestmentAccount().getLabel();
				}
				else {
					result = "Position close from transfer to external account";
				}
			}
			else {
				if (positionTransfer.getType().isBetweenOurAccounts()) {
					result = "Position opening from transfer from " + positionTransfer.getFromClientInvestmentAccount().getLabel();
				}
				else {
					result = "Position opening from transfer from external account";
				}
			}
		}
		else {
			result = positionTransfer.getType().isCash() ? "Cash" : "Equity security";
			if (closing) {
				if (positionTransfer.getType().isBetweenOurAccounts()) {
					result += " distribution from transfer to " + positionTransfer.getToClientInvestmentAccount().getLabel();
				}
				else {
					result += " distribution form transfer to external account";
				}
			}
			else {
				if (positionTransfer.getType().isBetweenOurAccounts()) {
					result += " contribution from transfer from " + positionTransfer.getFromClientInvestmentAccount().getLabel();
				}
				else {
					result += " contribution from transfer from external account";
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
