package com.clifton.accounting.positiontransfer.command;

import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;


/**
 * <code>AccountingPositionTransferCommand</code> is a class that holds combined information for a
 * potential new {@link AccountingPositionTransfer}, {@link AccountingPositionTransferDetail}, and
 * lot selection information. This information will be used to identify lots to be entered as details
 * of a new transfer upload.
 *
 * @author NickK
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class AccountingPositionTransferCommand extends AccountingPositionTransfer {

	public static final String LOT_SPECIFIED_SELECTION_METHOD_NAME = "SPECIFIED";

	/*
	 * Column values from an import file
	 */
	private String fromClientInvestmentAccountNumber;
	private String fromHoldingInvestmentAccountNumber;
	private String toClientInvestmentAccountNumber;
	private String toHoldingInvestmentAccountNumber;
	private String typeName;
	// Same as exposureDate on parent.
	private Date priceDate;

	private String securitySymbol;
	private String yellowKey;
	// One of the following four need to be specified for amount to transfer
	private BigDecimal quantity;
	private BigDecimal quantityPercent;
	private BigDecimal originalFace;
	private BigDecimal originalFacePercent;
	private BigDecimal originalFaceRounding;
	private RoundingMode percentRoundingMode;

	private BigDecimal costPrice;
	private Date originalPositionOpenDate;
	private BigDecimal commissionPerUnitOverride;
	private BigDecimal transferPrice;
	private BigDecimal exchangeRateToBase;
	private String lotSelectionMethod;
	private Long existingPositionId;

	/*
	 * DTO references for looked up items.
	 */
	private InvestmentSecurity security;
	private AccountingPositionOrders lotSelectionOrder;
	private InvestmentType investmentType;
	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentTypeSubType2 investmentTypeSubType2;
	private InvestmentInstrumentHierarchy instrumentHierarchy;
	private InvestmentInstrument investmentInstrument;
	private InvestmentSecurityGroup securityGroup;
	private InvestmentGroup investmentGroup; // Instrument Group

	/**
	 * The results of generating {@link AccountingPositionTransfer}s with this
	 * object are stored in this field for relaying status to the UI.
	 */
	private String resultString;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFromClientInvestmentAccountNumber() {
		if (getFromClientInvestmentAccount() != null && getFromClientInvestmentAccount().getNumber() != null) {
			return getFromClientInvestmentAccount().getNumber();
		}
		return this.fromClientInvestmentAccountNumber;
	}


	public void setFromClientInvestmentAccountNumber(String fromClientInvestmentAccountNumber) {
		this.fromClientInvestmentAccountNumber = fromClientInvestmentAccountNumber;
	}


	public String getFromHoldingInvestmentAccountNumber() {
		if (getFromHoldingInvestmentAccount() != null && getFromHoldingInvestmentAccount().getNumber() != null) {
			return getFromHoldingInvestmentAccount().getNumber();
		}
		return this.fromHoldingInvestmentAccountNumber;
	}


	public void setFromHoldingInvestmentAccountNumber(String fromHoldingInvestmentAccountNumber) {
		this.fromHoldingInvestmentAccountNumber = fromHoldingInvestmentAccountNumber;
	}


	public String getToClientInvestmentAccountNumber() {
		if (getToClientInvestmentAccount() != null && getToClientInvestmentAccount().getNumber() != null) {
			return getToClientInvestmentAccount().getNumber();
		}
		return this.toClientInvestmentAccountNumber;
	}


	public void setToClientInvestmentAccountNumber(String toClientInvestmentAccountNumber) {
		this.toClientInvestmentAccountNumber = toClientInvestmentAccountNumber;
	}


	public String getToHoldingInvestmentAccountNumber() {
		if (getToHoldingInvestmentAccount() != null && getToHoldingInvestmentAccount().getNumber() != null) {
			return getToHoldingInvestmentAccount().getNumber();
		}
		return this.toHoldingInvestmentAccountNumber;
	}


	public void setToHoldingInvestmentAccountNumber(String toHoldingInvestmentAccountNumber) {
		this.toHoldingInvestmentAccountNumber = toHoldingInvestmentAccountNumber;
	}


	public String getTypeName() {
		return (getType() != null && getType().getName() != null) ? getType().getName() : this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Date getPriceDate() {
		return this.priceDate == null ? super.getExposureDate() : this.priceDate;
	}


	public void setPriceDate(Date priceDate) {
		this.priceDate = priceDate;
	}


	public String getSecuritySymbol() {
		return (getSecurity() != null && getSecurity().getSymbol() != null) ? getSecurity().getSymbol() : this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getYellowKey() {
		return this.yellowKey;
	}


	public void setYellowKey(String yellowKey) {
		this.yellowKey = yellowKey;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getQuantityPercent() {
		return this.quantityPercent;
	}


	public void setQuantityPercent(BigDecimal quantityPercent) {
		this.quantityPercent = quantityPercent;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getOriginalFacePercent() {
		return this.originalFacePercent;
	}


	public void setOriginalFacePercent(BigDecimal originalFacePercent) {
		this.originalFacePercent = originalFacePercent;
	}


	public BigDecimal getOriginalFaceRounding() {
		return this.originalFaceRounding;
	}


	public void setOriginalFaceRounding(BigDecimal originalFaceRounding) {
		this.originalFaceRounding = originalFaceRounding;
	}


	public RoundingMode getPercentRoundingMode() {
		return this.percentRoundingMode;
	}


	public void setPercentRoundingMode(RoundingMode percentRoundingMode) {
		this.percentRoundingMode = percentRoundingMode;
	}


	public BigDecimal getCostPrice() {
		return this.costPrice;
	}


	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}


	public Date getOriginalPositionOpenDate() {
		return this.originalPositionOpenDate;
	}


	public void setOriginalPositionOpenDate(Date originalPositionOpenDate) {
		this.originalPositionOpenDate = originalPositionOpenDate;
	}


	public BigDecimal getCommissionPerUnitOverride() {
		return this.commissionPerUnitOverride;
	}


	public void setCommissionPerUnitOverride(BigDecimal commissionPerUnitOverride) {
		this.commissionPerUnitOverride = commissionPerUnitOverride;
	}


	public BigDecimal getTransferPrice() {
		return this.transferPrice;
	}


	public void setTransferPrice(BigDecimal transferPrice) {
		this.transferPrice = transferPrice;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public String getLotSelectionMethod() {
		return getLotSelectionOrder() != null ? getLotSelectionOrder().name() : this.lotSelectionMethod;
	}


	public void setLotSelectionMethod(String lotSelectionMethod) {
		this.lotSelectionMethod = lotSelectionMethod;
	}


	public Long getExistingPositionId() {
		return this.existingPositionId;
	}


	public void setExistingPositionId(Long existingPositionId) {
		this.existingPositionId = existingPositionId;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public AccountingPositionOrders getLotSelectionOrder() {
		return this.lotSelectionOrder;
	}


	public void setLotSelectionOrder(AccountingPositionOrders lotSelectionOrder) {
		this.lotSelectionOrder = lotSelectionOrder;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public InvestmentSecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(InvestmentSecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public String getResultString() {
		return this.resultString;
	}


	public void setResultString(String resultString) {
		this.resultString = resultString;
	}
}
