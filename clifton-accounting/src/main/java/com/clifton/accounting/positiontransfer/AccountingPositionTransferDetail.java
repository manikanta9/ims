package com.clifton.accounting.positiontransfer;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPositionTransferDetail</code> class represents a single position lot being transferred.
 *
 * @author vgomelsky
 */
public class AccountingPositionTransferDetail extends BaseEntity<Integer> implements LabeledObject {

	public static final String TABLE_NAME = "AccountingPositionTransferDetail";

	private AccountingPositionTransfer positionTransfer;

	/**
	 * When our account position is being transferred then "existingPosition" field points to the lot being transferred/closed.
	 * "existingPosition" client account, holding account and security must match these of the transfer.
	 * <p>
	 * When external position is transferred into our account, this field is null.
	 */
	private AccountingTransaction existingPosition;

	private InvestmentSecurity security;

	/**
	 * Partial transfers will have quantity that is less than the remaining quantity of position being transferred.
	 */
	private BigDecimal quantity;
	/**
	 * Usually null the same as quantity but for bonds that had factor changes prior to this trade (current factor <> 1),
	 * stores Original Face value while quantity will store Purchase Face.
	 */
	private BigDecimal originalFace;
	/**
	 * The price of the lot being transferred.
	 */
	private BigDecimal costPrice;
	/**
	 * Same as cost price if gains/losses are being transferred as well. For futures, may transfer "at market" vs "at cost".
	 */
	private BigDecimal transferPrice;
	private BigDecimal exchangeRateToBase;
	/**
	 * The cost basis being transferred = costPrice * multiplier * quantity
	 */
	private BigDecimal positionCostBasis;

	private BigDecimal commissionPerUnitOverride;

	/**
	 * The date when position being transferred was originally open. Use for transfers from when existingPosition == null.
	 */
	private Date originalPositionOpenDate;

////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Non-null originalFace indicates and asset backed bond transfers or other collateralized security that requires full position closes
	 * and reduction of original face to current face,
	 */
	public boolean isCollateralizedSecurity() {
		return getOriginalFace() != null;
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (this.positionTransfer != null) {
			result.append("Transfer ID = ");
			result.append(this.positionTransfer.getId());
		}
		if (this.existingPosition != null) {
			result.append(" for Accounting Transaction with ID = ");
			result.append(this.existingPosition.getId());
		}
		else {
			result.append(" for Transfer Detail with ID = ");
			result.append(this.getId());
		}
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getPositionCostBasis() {
		return this.positionCostBasis;
	}


	public void setPositionCostBasis(BigDecimal positionCostBasis) {
		this.positionCostBasis = positionCostBasis;
	}


	public BigDecimal getCommissionPerUnitOverride() {
		return this.commissionPerUnitOverride;
	}


	public void setCommissionPerUnitOverride(BigDecimal commissionPerUnitOverride) {
		this.commissionPerUnitOverride = commissionPerUnitOverride;
	}


	public BigDecimal getCostPrice() {
		return this.costPrice;
	}


	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}


	public BigDecimal getTransferPrice() {
		return this.transferPrice;
	}


	public void setTransferPrice(BigDecimal transferPrice) {
		this.transferPrice = transferPrice;
	}


	public AccountingPositionTransfer getPositionTransfer() {
		return this.positionTransfer;
	}


	public void setPositionTransfer(AccountingPositionTransfer positionTransfer) {
		this.positionTransfer = positionTransfer;
	}


	public Date getOriginalPositionOpenDate() {
		return this.originalPositionOpenDate;
	}


	public void setOriginalPositionOpenDate(Date originalPositionOpenDate) {
		this.originalPositionOpenDate = originalPositionOpenDate;
	}


	public AccountingTransaction getExistingPosition() {
		return this.existingPosition;
	}


	public void setExistingPosition(AccountingTransaction existingPosition) {
		this.existingPosition = existingPosition;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}
}
