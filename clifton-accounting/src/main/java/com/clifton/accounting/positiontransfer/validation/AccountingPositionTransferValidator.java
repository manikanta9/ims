package com.clifton.accounting.positiontransfer.validation;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * <code>AccountingPositionTransferValidator</code> validates an {@link AccountingPositionTransfer}
 *
 * @author NickK
 */
@Component
public class AccountingPositionTransferValidator extends SelfRegisteringDaoValidator<AccountingPositionTransfer> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(AccountingPositionTransfer transfer, DaoEventTypes config) throws ValidationException {
		if (transfer.getType().getSourceSystemTable() != null) {
			// Validate the transfer's source table matches the type's source table, and that the FK entity exists.
			ValidationUtils.assertEquals(transfer.getType().getSourceSystemTable(), transfer.getSourceSystemTable(), "Position transfer Source Table must match the transfer type's Source Table of " + transfer.getType().getSourceSystemTable(), "sourceSystemTable");
			ReadOnlyDAO<?> dao = getDaoLocator().locate(transfer.getSourceSystemTable().getName());
			ValidationUtils.assertNotNull(dao.findByPrimaryKey(transfer.getSourceFkFieldId()), "Position transfer of type " + transfer.getType() + " must define a source the transfer is associated with.", "sourceFkFieldId");
		}
	}
}
