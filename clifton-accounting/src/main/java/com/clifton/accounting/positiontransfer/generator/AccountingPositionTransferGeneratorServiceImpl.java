package com.clifton.accounting.positiontransfer.generator;

import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommandHandler;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;


/**
 * @author NickK
 */
@Service
public class AccountingPositionTransferGeneratorServiceImpl implements AccountingPositionTransferGeneratorService {

	private AccountingPositionTransferCommandHandler accountingPositionTransferCommandHandler;

	////////////////////////////////////////////////////////////////////////////
	////////     AccountingPositionTransferGeneratorService Methods      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void generateAccountingPositionTransferList(AccountingPositionTransferCommand transferCommand) {
		AccountingPositionTransferContext context = new AccountingPositionTransferContext();

		if (transferCommand != null) {
			ValidationUtils.assertNotNull(transferCommand.getTransactionDate(), "Cannot get position list for a position transfer transferCommand without a Transaction Date defined.");
			List<InvestmentSecurity> securityList = transferCommand.getSecurity() == null ? getAccountingPositionTransferCommandHandler().getInvestmentSecurityList(transferCommand) : Collections.singletonList(transferCommand.getSecurity());
			/*
			 * If the transfer quantity or original face are not specified, the quantity, and possibly the original face,
			 * is calculated. These values need to be cleared for each security.
			 */
			Consumer<AccountingPositionTransferCommand> quantityResetConsumer = (transferCommand.getQuantity() == null) ? t -> t.setQuantity(null) : t -> {/*no-op to avoid having to check for null*/};
			if (transferCommand.getOriginalFace() == null) {
				quantityResetConsumer = quantityResetConsumer.andThen(t -> t.setOriginalFace(null));
			}
			for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
				transferCommand.setSecurity(security);
				getAccountingPositionTransferCommandHandler().generateAccountingPositionTransfer(transferCommand, true, context);
				quantityResetConsumer.accept(transferCommand);
			}
			transferCommand.setResultString(getAccountingPositionTransferCommandHandler().saveGeneratedAccountingPositionTransferList(context));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                 Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferCommandHandler getAccountingPositionTransferCommandHandler() {
		return this.accountingPositionTransferCommandHandler;
	}


	public void setAccountingPositionTransferCommandHandler(AccountingPositionTransferCommandHandler accountingPositionTransferCommandHandler) {
		this.accountingPositionTransferCommandHandler = accountingPositionTransferCommandHandler;
	}
}
