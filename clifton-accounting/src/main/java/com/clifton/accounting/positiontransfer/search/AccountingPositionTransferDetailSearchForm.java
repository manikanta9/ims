package com.clifton.accounting.positiontransfer.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingPositionTransferDetailSearchForm</code> class defines search configuration for AccountingPositionTransferDetail objects.
 *
 * @author akorver
 */
public class AccountingPositionTransferDetailSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "positionTransfer.id")
	private Integer positionTransferId;

	@SearchField(searchField = "positionTransfer.id")
	private Integer[] positionTransferIds;

	@SearchField
	private Integer id;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchField = "security.instrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchFieldPath = "security.instrument.hierarchy", searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "security", sortField = "underlyingSecurity.symbol")
	private Integer underlyingSecurityId;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal originalFace;

	@SearchField
	private BigDecimal costPrice;

	@SearchField
	private BigDecimal transferPrice;

	@SearchField
	private BigDecimal exchangeRateToBase;

	@SearchField
	private BigDecimal positionCostBasis;

	@SearchField
	private Date originalPositionOpenDate;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "settlementDate")
	private Date settlementDate;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "transactionDate")
	private Date transactionDate;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "transactionDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date transactionDateMin;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "transactionDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date transactionDateMax;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "toClientInvestmentAccount.id")
	private Integer toClientInvestmentAccountId;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "toClientInvestmentAccount.id")
	private Integer[] toClientInvestmentAccountIds;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "toHoldingInvestmentAccount.id")
	private Integer toHoldingInvestmentAccountId;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "toHoldingInvestmentAccount.id")
	private Integer[] toHoldingInvestmentAccountIds;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "fromClientInvestmentAccount.id")
	private Integer fromClientInvestmentAccountId;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "fromClientInvestmentAccount.id")
	private Integer[] fromClientInvestmentAccountIds;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "fromHoldingInvestmentAccount.id")
	private Integer fromHoldingInvestmentAccountId;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "fromHoldingInvestmentAccount.id")
	private Integer[] fromHoldingInvestmentAccountIds;

	@SearchField(searchFieldPath = "positionTransfer", searchField = "bookingDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean journalNotBooked;

	// Custom search filters
	private Short investmentGroupId;
	private Integer clientInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getPositionTransferId() {
		return this.positionTransferId;
	}


	public void setPositionTransferId(Integer positionTransferId) {
		this.positionTransferId = positionTransferId;
	}


	public Integer[] getPositionTransferIds() {
		return this.positionTransferIds;
	}


	public void setPositionTransferIds(Integer[] positionTransferIds) {
		this.positionTransferIds = positionTransferIds;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getCostPrice() {
		return this.costPrice;
	}


	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}


	public BigDecimal getTransferPrice() {
		return this.transferPrice;
	}


	public void setTransferPrice(BigDecimal transferPrice) {
		this.transferPrice = transferPrice;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getPositionCostBasis() {
		return this.positionCostBasis;
	}


	public void setPositionCostBasis(BigDecimal positionCostBasis) {
		this.positionCostBasis = positionCostBasis;
	}


	public Date getOriginalPositionOpenDate() {
		return this.originalPositionOpenDate;
	}


	public void setOriginalPositionOpenDate(Date originalPositionOpenDate) {
		this.originalPositionOpenDate = originalPositionOpenDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getTransactionDateMin() {
		return this.transactionDateMin;
	}


	public void setTransactionDateMin(Date transactionDateMin) {
		this.transactionDateMin = transactionDateMin;
	}


	public Date getTransactionDateMax() {
		return this.transactionDateMax;
	}


	public void setTransactionDateMax(Date transactionDateMax) {
		this.transactionDateMax = transactionDateMax;
	}


	public Integer getToClientInvestmentAccountId() {
		return this.toClientInvestmentAccountId;
	}


	public void setToClientInvestmentAccountId(Integer toClientInvestmentAccountId) {
		this.toClientInvestmentAccountId = toClientInvestmentAccountId;
	}


	public Integer[] getToClientInvestmentAccountIds() {
		return this.toClientInvestmentAccountIds;
	}


	public void setToClientInvestmentAccountIds(Integer[] toClientInvestmentAccountIds) {
		this.toClientInvestmentAccountIds = toClientInvestmentAccountIds;
	}


	public Integer getToHoldingInvestmentAccountId() {
		return this.toHoldingInvestmentAccountId;
	}


	public void setToHoldingInvestmentAccountId(Integer toHoldingInvestmentAccountId) {
		this.toHoldingInvestmentAccountId = toHoldingInvestmentAccountId;
	}


	public Integer[] getToHoldingInvestmentAccountIds() {
		return this.toHoldingInvestmentAccountIds;
	}


	public void setToHoldingInvestmentAccountIds(Integer[] toHoldingInvestmentAccountIds) {
		this.toHoldingInvestmentAccountIds = toHoldingInvestmentAccountIds;
	}


	public Integer getFromClientInvestmentAccountId() {
		return this.fromClientInvestmentAccountId;
	}


	public void setFromClientInvestmentAccountId(Integer fromClientInvestmentAccountId) {
		this.fromClientInvestmentAccountId = fromClientInvestmentAccountId;
	}


	public Integer[] getFromClientInvestmentAccountIds() {
		return this.fromClientInvestmentAccountIds;
	}


	public void setFromClientInvestmentAccountIds(Integer[] fromClientInvestmentAccountIds) {
		this.fromClientInvestmentAccountIds = fromClientInvestmentAccountIds;
	}


	public Integer getFromHoldingInvestmentAccountId() {
		return this.fromHoldingInvestmentAccountId;
	}


	public void setFromHoldingInvestmentAccountId(Integer fromHoldingInvestmentAccountId) {
		this.fromHoldingInvestmentAccountId = fromHoldingInvestmentAccountId;
	}


	public Integer[] getFromHoldingInvestmentAccountIds() {
		return this.fromHoldingInvestmentAccountIds;
	}


	public void setFromHoldingInvestmentAccountIds(Integer[] fromHoldingInvestmentAccountIds) {
		this.fromHoldingInvestmentAccountIds = fromHoldingInvestmentAccountIds;
	}


	public Boolean getJournalNotBooked() {
		return this.journalNotBooked;
	}


	public void setJournalNotBooked(Boolean journalNotBooked) {
		this.journalNotBooked = journalNotBooked;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}
}
